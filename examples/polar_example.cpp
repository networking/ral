/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cmath>
#include <cstdio>
#include <cstring>
#include <vector>

#define SNEW(T, sz) ((T *)calloc(sz, sizeof(T)))

int8_t subblock_interleave_idx[32] = {
    0,  1,  2,  4,  3,  5,  6,  7,  8,  16, 9,  17, 10, 18, 11, 19,
    12, 20, 13, 21, 14, 22, 15, 23, 24, 25, 26, 28, 27, 29, 30, 31};

static void subblock_interleave(int n, const uint8_t *in, uint8_t *out) {
  // performs the sub-block interleaving step of polar encoding, as specified
  // by section 5.4.1.1 of 3GPP TS 38.212 by interleaving 32 blocks of n/32
  // bits according to the sub-block interleaver pattern (table 5.4.1.1-1).
  memset((void *)out, 0, n / 8);
  assert(n % 32 == 0);
  int b = n / 32;
  for (int j = 0; j < n; j += b) {
    int ofs = subblock_interleave_idx[j / b] * b;
    for (int k = 0; k < b; ++k) {
      int src_idx = (ofs + k) / 8;
      int src_bit = 7 - (ofs + k) % 8;
      int dst_idx = (j + k) / 8;
      int dst_bit = 7 - (j + k) % 8;
      uint32_t bit = (in[src_idx] >> src_bit) & 1;
      out[dst_idx] |= bit << dst_bit;
    }
  }
}

static void subblock_deinterleave(int n, const int8_t *in, int8_t *out) {
  // performs the inverse of the sub-block interleaving step. note that this
  // function is operating on 8-bit LLR values rather than individual bits.
  assert(n % 32 == 0);
  int b = n / 32;
  for (int j = 0; j < n; j += b) {
    int ofs = subblock_interleave_idx[j / b] * b;
    for (int k = 0; k < b; ++k) {
      out[ofs + k] = in[j + k];
    }
  }
}

static void channel_interleave(int n, int e, const uint8_t *in, uint8_t *out) {
  // performs the channel interleaving step of polar encoding, as specified by
  // section 5.4.1.3 of 3GPP TS 38.212.
  // we choose T such that (T * (T + 1) / 2) ≥ E, which can be expressed
  // exactly as: T = round_up((sqrt(8E + 1) - 1) / 2).
  int t = std::ceil((std::sqrt(8.F * e + 1.F) - 1.F) / 2.F);
  memset((void *)out, 0, (e + 7) / 8);
  int dst_bit = 0;
  for (int col = 0; col < t; ++col) {
    for (int row = 0; row < (t - col); ++row) {
      int src_bit = ((t * (t + 1)) / 2) - ((t - row) * (t - row + 1) / 2) + col;
      if (src_bit < e) {
        uint8_t bit = (in[src_bit / 8] >> (7 - (src_bit % 8))) & 1;
        out[dst_bit / 8] |= bit << (7 - (dst_bit % 8));
        ++dst_bit;
      }
    }
  }
}

static void channel_deinterleave(int n, int e, const int8_t *in, int8_t *out) {
  // performs the inverse of the channel interleaving step.  section 5.4.1.3 of
  // 3GPP TS 38.212. note that this function is operating on 8-bit LLR values
  // rather than individual bits.
  int t = std::ceil((std::sqrt(8.F * e + 1.F) - 1.F) / 2.F);
  memset((void *)out, 0, (e + 7) / 8);
  int src_bit = 0;
  for (int col = 0; col < t; ++col) {
    for (int row = 0; row < (t - col); ++row) {
      int dst_bit = ((t * (t + 1)) / 2) - ((t - row) * (t - row + 1) / 2) + col;
      if (dst_bit < e) {
        out[dst_bit] |= in[src_bit];
        ++src_bit;
      }
    }
  }
}

static void dump_binary_data(const char *prefix, int n, const uint8_t *data) {
  printf("%26s: ", prefix);
  for (int i = 0; i < (n + 7) / 8; ++i) {
    int limit = 7 - std::min(7, n - i * 8 - 1);
    for (int j = 7; j >= limit; --j) {
      printf("%u", (data[i] >> j) & 1);
    }
    printf(" ");
  }
  printf("\n");
}

static void dump_llr_data(const char *prefix, int n, const int8_t *data) {
  uint8_t *data_hd = SNEW(uint8_t, (n + 7) / 8);
  for (int i = 0; i < n; ++i) {
    int idx = i ^ 7;
    data_hd[i / 8] |= static_cast<int>(data[i] < 0) << (idx % 8);
  }
  dump_binary_data(prefix, n, data_hd);
  free(data_hd);
}

static void dump_polar_frozen_mask(uint32_t n, const uint8_t *frozen) {
  printf("%26s: ", "frozen mask");
  for (int i = 0; i < n;) {
    for (int j = 0; j < 8; ++j, ++i) {
      char frozen_ch = frozen[i] == ARMRAL_POLAR_INFO_BIT     ? 'i'
                       : frozen[i] == ARMRAL_POLAR_PARITY_BIT ? 'p'
                                                              : '.';
      printf("%c", frozen_ch);
    }
    printf(" ");
  }
  printf("\n");
}

static void example_polar_encode_and_decode(uint32_t n, uint32_t e, uint32_t k,
                                            uint32_t n_pc, uint32_t n_pc_wm) {
  printf("n=%u  e=%u  k=%u  n_pc=%u  n_pc_wm=%u\n", n, e, k, n_pc, n_pc_wm);

  // reference result
  int ref_unpacked[k];
  for (int i = 0; i < k; ++i) {
    ref_unpacked[i] = rand() % 2;
  }

  uint8_t *ref = SNEW(uint8_t, (k + 7) / 8);
  for (int i = 0; i < k; ++i) {
    ref[i / 8] |= ref_unpacked[i] << (7 - (i % 8));
  }
  dump_binary_data("input message", k, ref);

  // compute which indices are used for message bits, which indices are used
  // for frozen bits.
  uint8_t *frozen_bits = SNEW(uint8_t, n);
  armral_polar_frozen_mask(n, e, k, n_pc, n_pc_wm, frozen_bits);
  dump_polar_frozen_mask(n, frozen_bits);

  // interleave data into frozen bit pattern
  // interleaving calculates parity bits, so K + n_pc rather than just K.
  uint8_t *data_in = SNEW(uint8_t, n / 8);
  armral_polar_subchannel_interleave(n, k + n_pc, frozen_bits, ref, data_in);
  dump_binary_data("polar input data", n, data_in);

  // run polar encoding
  uint8_t *data_encoded = SNEW(uint8_t, n / 8);
  armral_polar_encode_block(n, data_in, data_encoded);
  dump_binary_data("polar encoded", n, data_encoded);

  // run sub-block interleaving
  uint8_t *data_encoded2 = SNEW(uint8_t, n / 8);
  subblock_interleave(n, data_encoded, data_encoded2);
  dump_binary_data("sub-block interleaved", n, data_encoded2);

  // run rate matching (puncturing)
  uint8_t *data_encoded3 = SNEW(uint8_t, (e + 7) / 8);
  for (int i = 0; i < e; ++i) {
    int src_bit = i + (n - e);
    uint32_t bit = (data_encoded2[src_bit / 8] >> (7 - (src_bit % 8))) & 1;
    data_encoded3[i / 8] |= bit << (7 - (i % 8));
  }
  dump_binary_data("punctured", e, data_encoded3);

  // run channel interleaving
  uint8_t *data_encoded4 = SNEW(uint8_t, (e + 7) / 8);
  channel_interleave(n, e, data_encoded3, data_encoded4);
  dump_binary_data("channel interleaved", e, data_encoded4);
  free(data_encoded3);

  // run modulation
  armral_modulation_type mod_type = ARMRAL_MOD_256QAM;
  int mod_num_symbols = (e + 7) / 8;
  armral_cmplx_int16_t *data_mod = SNEW(armral_cmplx_int16_t, mod_num_symbols);
  armral_modulation(mod_num_symbols * 8, mod_type, data_encoded4, data_mod);

  // ... sent over e.g. an AWGN channel ...

  // run demodulation
  uint16_t llr_ulp = 37;
  int8_t *llrs = SNEW(int8_t, e);
  armral_demodulation(mod_num_symbols, llr_ulp, mod_type, data_mod, llrs);

  // take a hard decision before deinterleave
  dump_llr_data("demodulated", e, llrs);

  // run channel deinterleaving
  int8_t *llrs_deint = SNEW(int8_t, e);
  channel_deinterleave(n, e, llrs, llrs_deint);
  dump_llr_data("channel deinterleaved", e, llrs_deint);
  free(llrs);

  // apply rate matching (puncturing)
  llrs = SNEW(int8_t, n);
  for (int i = e - 1; i >= 0; --i) {
    llrs[i + n - e] = llrs_deint[i];
  }
  for (int i = n - e - 1; i >= 0; --i) {
    llrs[i] = 0;
  }
  dump_llr_data("de-punctured", n, llrs);

  int8_t *llrs2 = SNEW(int8_t, n);
  subblock_deinterleave(n, llrs, llrs2);
  dump_llr_data("de-sub-block-interleaved", n, llrs2);

  // try polar decoding using the modified LLRs
  uint8_t *data_out = SNEW(uint8_t, 1 * n / 8);
  armral_polar_decode_block(n, frozen_bits, 1, llrs2, data_out);
  dump_binary_data("decoded data", n, data_out);

  // verify
  int data_bit_errors = 0;
  for (int i = 0; i < n / 8; ++i) {
    data_bit_errors += __builtin_popcount(data_in[i] ^ data_out[i]);
  }

  // deinterleaving ignores parity bits, so K rather than K + n_pc.
  uint8_t *recv = SNEW(uint8_t, (k + 7) / 8);
  armral_polar_subchannel_deinterleave(k, frozen_bits, data_out, recv);
  dump_binary_data("output message", k, recv);

  // verify
  int msg_bit_errors = 0;
  for (int i = 0; i < (k + 7) / 8; ++i) {
    msg_bit_errors += __builtin_popcount(ref[i] ^ recv[i]);
  }

  printf("got %d bit errors in encoded/decoded data\n", data_bit_errors);
  printf("got %d bit errors in input/output message\n", msg_bit_errors);

  free(data_encoded);
  free(data_encoded2);
  free(data_encoded4);
  free(data_in);
  free(data_mod);
  free(data_out);
  free(frozen_bits);
  free(llrs);
  free(llrs2);
  free(llrs_deint);
  free(recv);
  free(ref);
}

int main(int argc, char **argv) {
  if (argc < 4) {
    printf("Usage: %s n e k [n_pc [n_pc_wm]]\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  int n = atoi(argv[1]);
  int e = atoi(argv[2]);
  int k = atoi(argv[3]);
  int n_pc = argc > 4 ? atoi(argv[4]) : 0;
  int n_pc_wm = argc > 5 ? atoi(argv[5]) : 0;
  example_polar_encode_and_decode(n, e, k, n_pc, n_pc_wm);
}
