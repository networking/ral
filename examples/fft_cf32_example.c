/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <stdio.h>
#include <stdlib.h>

// This function shows how to create a plan and execute an FFT using the ArmRAL
// library
static void example_fft_plan_and_execute(int n) {
  armral_fft_plan_t *p;
  printf("Planning FFT of length %d\n", n);
  // In the planning, the direction of the FFT is indicated by the last
  // parameter, which is either -1 (for forwards) or 1 (for backwards)
  armral_fft_create_plan_cf32(&p, n, -1);

  // Create the data that is to be used in FFTs. The input array (x) needs to
  // be initialized. The output array (y) does not.
  armral_cmplx_f32_t *x =
      (armral_cmplx_f32_t *)malloc(n * sizeof(armral_cmplx_f32_t));
  armral_cmplx_f32_t *y =
      (armral_cmplx_f32_t *)malloc(n * sizeof(armral_cmplx_f32_t));
  for (int i = 0; i < n; ++i) {
    x[i] = (armral_cmplx_f32_t){(float)i, (float)-i};
    y[i] = (armral_cmplx_f32_t){0.F, 0.F};
  }

  printf("Input Data:\n");
  for (int i = 0; i < n; ++i) {
    printf("  (%f + %fi)\n", x[i].re, x[i].im);
  }
  printf("\n");

  // The FFTs are executed with different input and output data. The length
  // of the input and output arrays needs to be at least the same as that of
  // the length parameter with which the plan was created. No checks are
  // performed that this is the case in the library.
  printf("Performing FFT of length %d\n", n);
  armral_fft_execute_cf32(p, x, y);

  // A plan can be re-used to solve other FFTs, but once a plan is no longer
  // needed, it needs to be destroyed to avoid leaking memory.
  printf("Destroying plan for FFT of length %d\n", n);
  armral_fft_destroy_plan_cf32(&p);

  printf("Result:\n");
  for (int i = 0; i < n; ++i) {
    printf("  (%f + %fi)\n", y[i].re, y[i].im);
  }
  printf("\n");

  // Need to free the pointers to data. These are not owned by the FFT plan,
  // and it is the user's responsibility to manage the memory.
  free(x);
  free(y);
}

int main(int argc, char **argv) {
  if (argc < 2) {
    printf("Usage: %s len\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  int n = atoi(argv[1]);
  if (n < 1) {
    printf("Length parameter must be positive and non-zero\n");
    exit(EXIT_FAILURE);
  }

  example_fft_plan_and_execute(n);
}
