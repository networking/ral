/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <stdio.h>
#include <stdlib.h>

// This function shows how to use the ArmRAL library for 9-bit block-float
// compression and decompression.
static void example_block_float_compression_and_decompression() {

  armral_cmplx_int16_t input[ARMRAL_NUM_COMPLEX_SAMPLES];
  input[0].re = 24393;
  input[0].im = 1324;
  input[1].re = 22652;
  input[1].im = 17105;
  input[2].re = 9698;
  input[2].im = -6853;
  input[3].re = 14659;
  input[3].im = 31174;
  input[4].re = -15287;
  input[4].im = -15037;
  input[5].re = -24774;
  input[5].im = 8527;
  input[6].re = -22882;
  input[6].im = 10947;
  input[7].re = 11004;
  input[7].im = -12509;
  input[8].re = -10712;
  input[8].im = -17725;
  input[9].re = 7713;
  input[9].im = 12328;
  input[10].re = 18044;
  input[10].im = 14366;
  input[11].re = 15266;
  input[11].im = -1413;

  // Compressing the data will cause some information to be lost, where input
  // numbers are large enough. We show this by compressing and then
  // decompressing the data, and comparing this to the original data.
  armral_compressed_data_9bit compressed;
  armral_block_float_compr_9bit(1, input, &compressed, NULL);

  armral_cmplx_int16_t output[ARMRAL_NUM_COMPLEX_SAMPLES];
  armral_block_float_decompr_9bit(1, &compressed, output, NULL);

  for (int i = 0; i < ARMRAL_NUM_COMPLEX_SAMPLES; ++i) {
    printf("(%d + %di) vs (%d + %di)\n", input[i].re, input[i].im, output[i].re,
           output[i].im);
  }
}

int main(int argc, char **argv) {
  example_block_float_compression_and_decompression();
}
