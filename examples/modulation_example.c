/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <stdio.h>
#include <stdlib.h>

static void dump_binary_data(const char *prefix, int n, const uint8_t *data) {
  printf("%s: ", prefix);
  for (int i = 0; i < n / 8; ++i) {
    for (int j = 7; j >= 0; --j) {
      printf("%u", (data[i] >> j) & 1);
    }
    printf(" ");
  }
  printf("\n");
}

int main() {
  // input/output data size in bits
  int n = 128;

  // construct random input vector
  uint8_t *data_in = calloc(n / 8, sizeof(uint8_t));
  for (int i = 0; i < n / 8; ++i) {
    data_in[i] = rand();
  }
  dump_binary_data("input  ", n, data_in);

  // run modulation
  armral_modulation_type mod_type = ARMRAL_MOD_256QAM;
  int mod_num_symbols = n / 8;
  armral_cmplx_int16_t *data_mod =
      calloc(mod_num_symbols, sizeof(armral_cmplx_int16_t));
  armral_modulation(n, mod_type, data_in, data_mod);

  // ... sent over e.g. an AWGN channel ...

  // run demodulation
  uint16_t llr_ulp = 37;
  int8_t *llrs = calloc(n, sizeof(int8_t));
  armral_demodulation(mod_num_symbols, llr_ulp, mod_type, data_mod, llrs);

  // just take a hard decision since no noise in this example
  uint8_t *data_out = calloc(n / 8, sizeof(uint8_t));
  for (int i = 0; i < n; ++i) {
    data_out[i / 8] |= (llrs[i] < 0) << (7 - (i % 8));
  }
  dump_binary_data("output ", n, data_out);

  // verify
  int bit_errors = 0;
  for (int i = 0; i < n / 8; ++i) {
    bit_errors += __builtin_popcount(data_in[i] ^ data_out[i]);
  }
  printf("got %d bit errors\n", bit_errors);

  free(data_in);
  free(data_mod);
  free(llrs);
  free(data_out);
}
