# Arm RAN Acceleration Library (ArmRAL) Reference Guide

Copyright © 2020-2025 Arm Limited (or its affiliates). All rights reserved.

## About this book

This book contains reference documentation for Arm RAN Acceleration Library
(ArmRAL). The book was generated from the source code using Doxygen.

## Feedback

### Feedback on this product

If you have any comments or suggestions about this product, contact your
supplier and give:

* The product name.
* The product revision or version.
* An explanation with as much information as you can provide. Include symptoms
  and diagnostic procedures if appropriate.

### Feedback on content

If you have any comments on content, send an e-mail to errata@arm.com. Give:

* The title Arm RAN Acceleration Library Reference Guide.
* The number 102249_2501_00_en.
* If applicable, the relevant page number(s) to which your comments refer.
* A concise explanation of your comments.

Arm also welcomes general suggestions for additions and improvements.

## Non-Confidential Proprietary Notice

This document is protected by copyright and other related rights and the
practice or implementation of the information contained in this document may be
protected by one or more patents or pending patent applications. No part of this
document may be reproduced in any form by any means without the express prior
written permission of Arm. No license, express or implied, by estoppel or
otherwise to any intellectual property rights is granted by this document unless
specifically stated.

Your access to the information in this document is conditional upon your
acceptance that you will not use or permit others to use the information for the
purposes of determining whether implementations infringe any third party
patents.

THIS DOCUMENT IS PROVIDED "AS IS". ARM PROVIDES NO REPRESENTATIONS AND NO
WARRANTIES, EXPRESS, IMPLIED OR STATUTORY, INCLUDING, WITHOUT LIMITATION, THE
IMPLIED WARRANTIES OF MERCHANTABILITY, SATISFACTORY QUALITY, NON-INFRINGEMENT OR
FITNESS FOR A PARTICULAR PURPOSE WITH RESPECT TO THE DOCUMENT. For the avoidance
of doubt, Arm makes no representation with respect to, has undertaken no
analysis to identify or understand the scope and content of, patents,
copyrights, trade secrets, or other rights.

This document may include technical inaccuracies or typographical errors.

TO THE EXTENT NOT PROHIBITED BY LAW, IN NO EVENT WILL ARM BE LIABLE FOR ANY
DAMAGES, INCLUDING WITHOUT LIMITATION ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
PUNITIVE, OR CONSEQUENTIAL DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY
OF LIABILITY, ARISING OUT OF ANY USE OF THIS DOCUMENT, EVEN IF ARM HAS BEEN
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

This document consists solely of commercial items. You shall be responsible for
ensuring that any use, duplication or disclosure of this document complies fully
with any relevant export laws and regulations to assure that this document or
any portion thereof is not exported, directly or indirectly, in violation of
such export laws. Use of the word "partner" in reference to Arm's customers is
not intended to create or refer to any partnership relationship with any other
company. Arm may make changes to this document at any time and without notice.

This document may be translated into other languages for convenience, and you
agree that if there is any conflict between the English version of this document
and any translation, the terms of the English version of the Agreement shall
prevail.

The Arm corporate logo and words marked with ® or ™ are registered trademarks or
trademarks of Arm Limited (or its affiliates) in the US and/or elsewhere. All
rights reserved. Other brands and names mentioned in this document may be the
trademarks of their respective owners. Please follow Arm's trademark usage
guidelines at <https://www.arm.com/company/policies/trademarks>.

Copyright © 2020-2025 Arm Limited (or its affiliates). All rights reserved.

Arm Limited. Company 02557590 registered in England.

110 Fulbourn Road, Cambridge, England CB1 9NJ.

(LES-PRE-20349)

## Confidentiality Status

This document is Non-Confidential. The right to use, copy and disclose this
document may be subject to license restrictions in accordance with the terms of
the agreement entered into by Arm and the party that Arm delivered this document
to.

Unrestricted Access is an Arm internal classification.

## Product Status

The information in this document is Final, that is for a developed product.

## Web Address

<https://developer.arm.com>

## Progressive terminology commitment

Arm values inclusive communities. Arm recognizes that we and our industry have
used language that can be offensive. Arm strives to lead the industry and create
change.

We believe that this document contains no offensive terms.
If you find offensive terms in this document, please contact terms@arm.com.

## Release Information

### Document History

Issue   | Date            | Confidentiality  | Change
--------|-----------------|------------------|-----------------------------------------------------
2010-00 | 02 October 2020 | Non-Confidential | New document for Arm RAN Acceleration Library v20.10
2101-00 | 08 January 2021 | Non-Confidential | Update for Arm RAN Acceleration Library v21.01
2104-00 | 09 April 2021   | Non-Confidential | Update for Arm RAN Acceleration Library v21.04
2107-00 | 09 July 2021    | Non-Confidential | Update for Arm RAN Acceleration Library v21.07
2110-00 | 08 October 2021 | Non-Confidential | Update for Arm RAN Acceleration Library v21.10
2201-00 | 14 January 2022 | Non-Confidential | Update for Arm RAN Acceleration Library v22.01
2204-00 | 08 April 2022   | Non-Confidential | Update for Arm RAN Acceleration Library v22.04
2207-00 | 15 July 2022    | Non-Confidential | Update for Arm RAN Acceleration Library v22.07
2210-00 | 07 October 2022 | Non-Confidential | Update for Arm RAN Acceleration Library v22.10
2301-00 | 27 January 2023 | Non-Confidential | Update for Arm RAN Acceleration Library v23.01
2304-00 | 21 April 2023   | Non-Confidential | Update for Arm RAN Acceleration Library v23.04
2307-00 | 07 July 2023    | Non-Confidential | Update for Arm RAN Acceleration Library v23.07
2310-00 | 06 October 2023 | Non-Confidential | Update for Arm RAN Acceleration Library v23.10
2401-00 | 19 January 2024 | Non-Confidential | Update for Arm RAN Acceleration Library v24.01
2404-00 | 19 April 2024   | Non-Confidential | Update for Arm RAN Acceleration Library v24.04
2407-00 | 18 July 2024    | Non-Confidential | Update for Arm RAN Acceleration Library v24.07
2410-00 | 17 October 2024 | Non-Confidential | Update for Arm RAN Acceleration Library v24.10
2501-00 | 23 January 2025 | Non-Confidential | Update for Arm RAN Acceleration Library v25.01
