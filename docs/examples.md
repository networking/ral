# Use Arm RAN Acceleration Library

This topic describes how to compile and link your application code to Arm RAN
Acceleration Library (ArmRAL).

## Before you begin

* Ensure you have a recent version of a C/C++ compiler, such as GCC. See the
  Release Notes for a full list of supported GCC versions.

  If required, configure your environment. If you have multiple compilers
  installed on your machine, you can set the `CC` and `CXX` environment
  variables to the path to the C compiler and C++ compiler that you want to use.

* You must build ArmRAL before you can use it in your application development,
  or to run the example programs.

  To build the library, use:

      git clone -b armral-25.01 https://git.gitlab.arm.com/networking/ral.git
      mkdir ral/build
      cd ral/build
      cmake ..
      make -j

* To use the ArmRAL functions in your application development, include the
  `armral.h` header file in your C or C++ source code.

      #include "armral.h"

## Procedure

1. Build and link your program with ArmRAL. For GCC, use:

       gcc -c -o <code-filename>.o <code-filename>.c -I <path/to/armral/source>/include -O2
       gcc -o <binary-filename> <code-filename>.o <path/to/armral/build>/libarmral.a -lm

   Substituting:

   * `<code-filename>` with the name of your own source code file

   * `<path/to/armral/source>` with the path to your copy of the ArmRAL source
     code

   * `<path/to/armral/build>` with the path to your build of ArmRAL

2. Run your binary:

       ./<binary-filename>

## Example: Run 'fft_cf32_example.c'

In this example, we use ArmRAL to compute and solve a simple Fast Fourier
Transform (FFT) problem.

The following source file can be found in the ArmRAL source directory under
`examples/fft_cf32_example.c`:

\include fft_cf32_example.c

1. To build and link the example program with GCC, use:

       gcc -c -o fft_cf32_example.o fft_cf32_example.c -I <path/to/armral/source>/include -O2
       gcc -o fft_cf32_example fft_cf32_example.o <path/to/armral/build>/libarmral.a -lm

   Substituting:

   * `<path/to/armral/source>` with the path to your copy of the ArmRAL source
     code

   * `<path/to/armral/build>` with the path to your build of ArmRAL

   **Note:** For this example, there is a requirement to link against libm
   (`-lm`). libm is used in several functions in ArmRAL, and so might be
   required for your own programs.

   An executable called `fft_cf32_example` is built.

2. Run the `fft_cf32_example` executable. To input the length of FFT to compute,
   the example program takes the length as an argument. To run with the length
   of FFT set to 5, use:

       ./fft_cf32_example 5

   which gives:

       Planning FFT of length 5
       Input Data:
         (0.000000 + 0.000000i)
         (1.000000 + -1.000000i)
         (2.000000 + -2.000000i)
         (3.000000 + -3.000000i)
         (4.000000 + -4.000000i)

       Performing FFT of length 5
       Destroying plan for FFT of length 5
       Result:
         (10.000000 + -10.000000i)
         (0.940955 + 5.940955i)
         (-1.687701 + 3.312299i)
         (-3.312299 + 1.687701i)
         (-5.940955 + -0.940955i)

## Other examples: block-float, modulation, and polar examples

ArmRAL also includes block-float, modulation, and polar examples. These example
files can also be found in the `/examples/` directory.

In addition to the `fft_cf32_example.c` FFT example, the following examples are
included:

* `block_float_9b_example.c`

  Fills a single Resource Block (RB) with a set of random numbers and uses the
  block floating-point compression API to compress the numbers into a 9-bit
  compressed format. `block_float_9b_example.c` then uses the decompression
  function to convert the numbers to their original format, then returns the
  numbers side-by-side for comparison.

  The example binary does not take an argument. For example, to run a compiled
  binary of the `block_float_9b_example.c`, called, `block_float_9b_example`,
  use:

      ./block_float_9b_example

* `modulation_example.c`

  Uses the modulation and demodulation API to simulate applying 256QAM
  modulation to an array of random input bits. To show that taking a
  hard-decision with no noise applied gives the original input,
  `modulation_example.c` then demodulates the data, before returning the values.

  The example binary does not take an argument. For example, to run a compiled
  binary of the `modulation_example.c`, called, `modulation_example`,
  use:

      ./modulation_example

* `polar_example.cpp`

  Uses the polar coding and modulation APIs to simulate a complete flow from an
  original input codeword to the final polar-decoded output. In particular, the
  Polar encoder and decoder are used, as well as the subchannel interleaving
  functionality. Example implementations of other parts of the coding process,
  such as sub-block interleaving and rate-matching, are also provided.

  The example binary takes three arguments, in the following order:

  1. The polar code size (`N`)
  2. The rate-matched codeword length (`E`)
  3. The number of information bits (`K`)

  For example, to run a compiled binary of the `polar_example.cpp`, called,
  `polar_example`, with an input array of `N = 128`, `E = 100`, and `K = 35`,
  use:

      ./polar_example 128 100 35

Each example can be run according to the **Procedure** described above, as
demonstrated in the **Example: Run 'fft_cf32_example.c'** section.

## Related information

For more information, see the **README** file.
