# Get started with Arm RAN Acceleration Library

This document describes how to build, install, run tests and
benchmarks, and uninstall Arm RAN Acceleration Library (ArmRAL).

## Introducing ArmRAL

ArmRAL provides optimized signal processing and related maths functions for
enabling 5G Radio Access Network (RAN) deployments. It leverages the efficient
vector units available on Arm cores that support the Armv8-a architecture to
accelerate 5G NR and LTE signal processing workloads, including:

* Matrix and vector arithmetic, such as matrix multiplication.
* Fast Fourier Transforms (FFTs).
* Digital modulation and demodulation.
* Cyclic Redundancy Check (CRC).
* Encoding and decoding schemes, including Polar, Low-Density Parity
  Check (LDPC), and Turbo.
* Compression and decompression.

You can download ArmRAL from <https://gitlab.arm.com/networking/ral>.

ArmRAL is built as a static library and must be linked in to any executable that
needs to use the library. Users can build and modify the source code to
integrate with their components or clients. The `include` directory contains the
header files, the `src` directory contains the source code, the `test` directory
contains the testing code, the `bench` directory contains the benchmarking code,
and the `examples` directory contains the examples.

## Before you begin

* Ensure you have installed all the tools listed in the **Tools** section of the
  `RELEASE_NOTES.md` file.
* To use the Cyclic Redundancy Check (CRC) functions, the Gold sequence
  generator, and the convolutional encoder, you must run the library on a core
  that supports the AArch64 PMULL extension. If your machine supports the PMULL
  extension, pmull is listed under the **Features** list given in the
  `/proc/cpuinfo` file.

## Build ArmRAL

1. Configure your environment. If you have multiple compilers installed on your
   machine, you can set the `CC` and `CXX` environment variables to the path to
   the C compiler and C++ compiler that you want to use.

   If you are compiling natively on an AArch64-based machine, you must set
   suitable AArch64 native compilers. If you are cross-compiling for AArch64 on
   a machine that is based on a different architecture, you must set suitable
   AArch64 cross-compilers.

   Alternatively, your C and C++ compilers can be defined at build time using
   the `-DCMAKE_C_COMPILER` and `-DCMAKE_CXX_COMPILER` CMake options. You can
   read more about these options in the following section.

   **Note:** If you are building the SVE or SVE2 version of the library, you
   must compile with GCC 11.1.0 or newer.

2. Build ArmRAL. Navigate to the unpacked product directory and use the
   following commands:

       mkdir <build>
       cd <build>
       cmake {options} -DBUILD_TESTING=On -DBUILD_EXAMPLES=On -DCMAKE_INSTALL_PREFIX=<install-dir> <path>
       make

   Substituting:

   * `<build>` with a build directory name. The library builds in the
     specified directory.
   * `{options}` with the CMake options to use to build the library.
   * (Optional) `<install-dir>` with an installation directory name. When you
     install ArmRAL (see **Install ArmRAL**), the library installs to the
     specified directory. If `<install-dir>` is not specified, the default is
     `/usr/local`.
   * `<path>` with the path to the root directory of the library source.

   Notes:

   * The `-DBUILD_TESTING=On` and `-DBUILD_EXAMPLES=On` options are required
     if you want to run the library tests and benchmarks (`-DBUILD_TESTING`)
     and examples (`-DBUILD_EXAMPLES`).

   * The `-DCMAKE_INSTALL_PREFIX=<install-dir>` option specifies the base
     directory used to install the library. The library archive is installed to
     `<install-dir>/lib` and headers are installed to `<install-dir>/include`.
     The default location is `/usr/local`.

   * By default, a static library is built. To build a dynamic or a static
     library use the `-DBUILD_SHARED_LIBS={On|Off}` option.

   * By default, a Neon-optimized library is built. To specify which type of
     optimized library to build (Neon, SVE, or SVE2), use the
     `-DARMRAL_ARCH={NEON|SVE|SVE2}` option.

   Other common CMake `{options}` include:

   * `-DCMAKE_BUILD_TYPE={Debug|Release}`

       Specifies the set of flags used to build the library. The default is
       `Release` which gives the optimal performance, however `Debug` might give
       a superior debugging experience. To optimize the performance of *Release*
       builds, assertions are disabled. Assertions are enabled in *Debug*
       builds.

       Default is `Release`.

   * `-DCMAKE_C_COMPILER=<name>`

       Specifies the executable to use as the C compiler. If a compiler is not
       specified, the compiler used defaults to the contents of the `CC`
       environment variable. If neither are set, CMake attempts to use the
       generic system compiler `cc`. If `<name>` is not an absolute
       path, it must be findable in your current environment `PATH`.

   * `-DCMAKE_CXX_COMPILER=<name>`

       Specifies the executable to use as the C++ compiler. If a compiler is not
       specified, the compiler used defaults to the contents of the `CXX`
       environment variable. If neither are set, CMake attempts to use the
       generic system compiler `c++`. If `<name>` is not an absolute
       path, it must be findable in your current environment `PATH`.

   * `-DBUILD_TESTING={On|Off}`

       Specifies whether to build (`On`), or not build (`Off`), the correctness
       tests and benchmarking code for the library. `-DBUILD_TESTING=On` enables
       the `check` and `bench` targets described later. If after you build the
       library, you want to run the included tests and benchmarks, you must
       build your library with `-DBUILD_TESTING=On`.

       Default is `Off`.

   * `-DARMRAL_TEST_RUNNER=<command>`

       Specifies a command that is used as a prefix before each test executable,
       such as where an emulator might be required. To see an example where
       `-DARMRAL_TEST_RUNNER` is used, see the **Run the tests** section.

   * `-DSTATIC_TESTING={On|Off}`

       Most C/C++ toolchains dynamically link to system libraries like
       `libc.so`, however this dynamic link is unsuitable or unsupported in some
       use cases. Setting `-DSTATIC_TESTING=On` forces the compiler to link the
       tests statically by appending the `-static` flag to the link line.

       Default is `Off`.

   * `-DBUILD_EXAMPLES={On|Off}`

       Specifies whether to build (`On`), or not build (`Off`), the examples in
       the examples folder. The example programs are simpler than the tests, and
       show how different parts of the library can be used.
       `-DBUILD_EXAMPLES=On` enables the `examples` and `run_examples` targets
       described later. If after you build the library, you want to run the
       included examples, you must build your library with
       `-DBUILD_EXAMPLES=On`.

       Default is `Off`.

   * `-DBUILD_SHARED_LIBS={On|Off}`

       Specifies whether to generate a shared library (`On`) or a static library
       (`Off`). To generate `libarmral.so`, use `-DBUILD_SHARED_LIBS=On`. To
       generate `libarmral.a`, use `-DBUILD_SHARED_LIBS=Off`.

       Default is `Off`.

   * `-DARMRAL_ENABLE_WEXTRA={On|Off}`

       Use (`On`), or do not use (`Off`), `-Wextra` to build the library and
       tests. `-Wextra` enables additional compiler warnings over the default
       `-Wall`. Disabled by default to aid compatibility with untested and future
       compiler releases.

       Default is `Off`.

   * `-DARMRAL_ENABLE_WERROR={On|Off}`

       Use (`On`), or do not use (`Off`), `-Werror` to build the library and
       tests. `-Werror` converts any compiler warnings into errors. Disabled by
       default to aid compatibility with untested and future compiler releases.

       Default is `Off`.

   * `-DARMRAL_ENABLE_ASAN={On|Off}`

       Enable AddressSanitizer when building the library and tests.
       AddressSanitizer adds extra runtime checks to enable you to catch
       memory errors, such as reading or writing past the end of an array.
       `-DARMRAL_ENABLE_ASAN=On` incurs some reduction in runtime performance.

       Default is `Off`.

   * `-DARMRAL_ENABLE_EFENCE={On|Off}`

       Enable Electric Fence when building the library and tests.
       Electric Fence will cause tests to segmentation fault in the presence
       of memory errors, such as reading or writing past the end of an array.
       This option allows you to test executables running under a test runner
       such as QEMU.

       Default is `Off`.

   * `-DARMRAL_ENABLE_COVERAGE={On|Off}`

       Enable (`On`), or disable (`Off`), code coverage instrumentation when
       building the library and tests. When analyzing code coverage, it can be
       useful to enable debug information (`-DCMAKE_BUILD_TYPE=Debug`) to ensure
       that compiler-optimized lines of code are not missed. For more
       information, see the **Code coverage** section.

       Default is `Off`.

   * `-DARMRAL_ARCH={NEON|SVE|SVE2}`

       Enable code that is optimized for a specific architecture: `NEON`,
       `SVE`, or `SVE2`. To use `-DARMRAL_ARCH=SVE`, you must use a compiler
       that supports `-march=armv8-a+sve`. To use `-DARMRAL_ARCH=SVE2`, you must
       use a compiler that supports `-march=armv8-a+sve2`.

       Default is `NEON`.

   * `-DARMRAL_SEMIHOSTING={On|Off}`

       Enable (`On`), or disable (`Off`), building ArmRAL with semihosting
       support enabled. When semihosting support is enabled,
       `--specs=rdimon.specs` is passed as an additional flag during compilation
       and `-lrdimon` is added to the link line for testing and benchmarking.

       **Note:** If you use `-DARMRAL_SEMIHOSTING=On` you must also use a
       compiler with the `aarch64-none-elf` target triple.

       Default is `Off`.

   * `-DBUILD_SIMULATION={On|Off}`

       Enable (`On`), or disable (`Off`), building channel simulation programs.
       This allows you to simulate Additive White Gaussian Noise (AWGN) channels
       in order to quantify the quality of the forward error correction for a
       given encoding scheme and modulation scheme. For more information,
       please see the section called `Run the simulations`.

       Default is `On`.

## Install ArmRAL

After you have built ArmRAL, you can install the library.

1. Ensure you have write access for the installation directories:

   * For a default installation, you must have write access for
     `/usr/local/lib/`, for the library, and `/usr/local/include/`, for the
     header files.
   * For a custom installation, you must have write access for
     `<install-dir>/lib/`, for the library, and `<install-dir>/include/`, for
     the header files.

2. Install the library. Run:

       make install

   An install creates an `install_manifest.txt` file in the library build
   directory. `install_manifest.txt` lists the installation locations for the
   library and the header files.

## Run the tests

The ArmRAL package includes tests for the available functions in the library.

**Note:** To run the library tests, you must have built ArmRAL with the
 `-DBUILD_TESTING=On` CMake option.

To build and run the tests, use:

    make check

If you are not developing on an AArch64 machine, or if you want to test the SVE
or SVE2 version of the library on an AArch64 machine that does not support the
extension, you can use the `-DARMRAL_TEST_RUNNER` option to prefix each test
executable invocation with a wrapper. Example wrappers include QEMU and Arm
Instruction Emulator. For example, for QEMU you could configure the library to
prefix the tests with `qemu-aarch64` using:

    cmake .. -DBUILD_TESTING=On -DARMRAL_TEST_RUNNER=qemu-aarch64
    make check

## Run the benchmarks

All the functions in ArmRAL contain benchmarking code that contains preset
problem sizes.

**Note:** To run the benchmark tests, you must have built ArmRAL with the
`-DBUILD_TESTING=On` CMake option. You must also have the executable `perf`
available on your system. This can be installed via your package manager.

To build and run the benchmarks, use:

    make bench

Benchmark results print as JSON objects. To further process the results, you can
collect the results to a file or pipe the results into other scripts.
Alternatively, the Makefile target:

    make bench_excel_summary

will run the benchmarks and produce an Excel spreadsheet of the results, in
addition to printing them as JSON objects. To install the required Python
packages for this target, use:

    pip install -r <path>/python/requirements.txt

where `<path>` is the path to the root directory of the library source.

## Run the examples

The source for the example programs is available in the `examples` directory,
found in the ArmRAL root directory.

**Note:** To compile and execute the example programs, you must have built
  ArmRAL with the `-DBUILD_EXAMPLES=On` CMake option.

* To both build and run the example programs, use:

      make run_examples

* To only build the example programs so that, for example, you can later choose
  which example programs to specifically run, use:

      make examples

The built binaries can be found in the `examples` subdirectory of the build
directory.

More information about the examples that are available in ArmRAL, and how to use
the library in general, is available in **Use Arm RAN Acceleration Library**,
see `docs/examples.md`.

## Run the simulations

You can evaluate the quality of the error correction of the different encoding
schemes against the signal-to-noise ratio using a set of noisy channel
simulation programs. ArmRAL currently only supports zero-mean Additive White
Gaussian Noise (AWGN) channel simulation.

**Note:** The simulation programs do not simulate a full codec, and are intended
to be used to evaluate just the forward error correction properties of the
encoding and decoding of a single code block. We do not consider channel
properties. The source code for the simulations and documentation for their use
are available in the `simulation` directory, found in the ArmRAL root directory.

**Note:** To compile and execute the simulation programs, you must have built
ArmRAL with the `-DBUILD_SIMULATION=On` CMake option. This option is set to `On`
by default.

The following assumes that you are running commands from the build directory.

* To build all the simulation programs, use:

      make simulation

The built binaries can be found in the `simulation` subdirectory of the build
directory.

More information about the simulation programs that are available in ArmRAL is
available in `simulation/README.md`.

## Code coverage

You can generate information that describes how much of the library is used by
your application, or is covered by the included tests. To collect code coverage
information, you must have built ArmRAL with `-DARMRAL_ENABLE_COVERAGE=On`.

An example workflow could be:

    mkdir build
    cd build
    cmake .. -DCMAKE_BUILD_TYPE=Debug -DBUILD_TESTING=On -DARMRAL_ENABLE_COVERAGE=On
    make check
    gcovr --html-details index.html -r ..

Here, the `-r ..` flag points `gcovr` to the ArmRAL source tree, rather than
attempting to find the source in the build directory. The `gcovr` command
generates a series of HTML pages, viewable with a web browser, that give
information on the lines of code executed by the test suite.

To generate a plain-text summary about the lines of code executed by the test
suite, use:

    gcovr -r ..

If you run into an issue when running the `gcovr` command, you might need to
update to a newer version of `gcovr`. To find out what versions of `gcovr` have
been tested with ArmRAL, see the **Tools** section of the `RELEASE_NOTES.md`
file.

## Documentation

The Arm RAN Acceleration Library Reference Guide is available online at:

    https://developer.arm.com/documentation/102249/2501

If you have Doxygen installed on your system, you can build a local HTML version
of the ArmRAL documentation using CMake.

To build the documentation, run:

    make docs

The HTML builds and is output to `docs/html/`. To view the documentation, open
the `index.html` file in a browser.

## Uninstall ArmRAL

To uninstall ArmRAL:

1. Navigate to the library build directory (where you previously ran `make install`)
2. Run:

       make uninstall

   `make uninstall` removes all the files listed in `install_manifest.txt` and
   any empty directories. `make uninstall` also attempts to remove any
   directories which might have been created.

   **Note:** To only remove the installed files (but not any directories),
   instead run:

       cat install_manifest.txt | xargs rm
