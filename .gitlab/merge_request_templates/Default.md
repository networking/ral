## Description

This template is intended for general Merge Requests that add or change library functionality.

Summarize the purpose of this Merge Request here. See the [contribution guidelines](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md) for more details.

If this Merge Request addresses an [Issue](https://gitlab.arm.com/networking/ral/-/issues) that has already been raised, please provide a link to the Issue here. If an [Issue](https://gitlab.arm.com/networking/ral/-/issues) does not exist, please provide information on how to reproduce the problem here.

## Checklist

* [] [Contribution meets RAL's licence terms](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-licensing-information)
* [] [New functions adhere to RAL's naming scheme](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-function-naming)
* [] [Contribution conforms to RAL's directory structure](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-directory-structure)
* [] [Documentation updated](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-documentation)
* [] ["Unreleased" section of the Changelog updated](https://gitlab.arm.com/networking/ral/-/blob/main/CHANGELOG.md#unreleased)
* [] [`clang-format` and `clang-tidy` run and changes included (C/C++ code)](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-cc-code-style)
* [] [`flake8` run and changes included (Python code)](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-python-code-style)
* [] [`cmake-format` run and changes included (CMake code)](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-cmake-code-style)
* [] [Tests added or updated](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-writing-tests)
* [] [Tests pass when run with AddressSanitizer](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md?ref_type=heads#user-content-testing-with-addresssanitizer)
* [] [Benchmarks added or updated](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-writing-benchmarks)

For any items that are not checked, please provide details.
