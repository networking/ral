## Description

This template is intended for Merge Requests that fix a bug in the library.

If an [Issue](https://gitlab.arm.com/networking/ral/-/issues) already exists for this bug, please provide a link to the Issue here. If an [Issue](https://gitlab.arm.com/networking/ral/-/issues) does not exist, please provide information on how to reproduce the bug here.

## Checklist

* [] [Contribution meets RAL's licence terms](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-licensing-information)
* [] [Documentation updated](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-documentation)
* [] ["Unreleased" section of the Changelog updated](https://gitlab.arm.com/networking/ral/-/blob/main/CHANGELOG.md#unreleased)
* [] [`clang-format` and `clang-tidy` run and changes included (C/C++ code)](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-cc-code-style)
* [] [`flake8` run and changes included (Python code)](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-python-code-style)
* [] [`cmake-format` run and changes included (CMake code)](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-cmake-code-style)
* [] Commit message includes information on how to reproduce the issue(s)
* [] [Tests added or updated](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-writing-tests)
* [] [Tests pass when run with AddressSanitizer](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md?ref_type=heads#user-content-testing-with-addresssanitizer)

For any items that are not checked, please provide details.
