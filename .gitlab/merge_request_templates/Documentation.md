## Description

This template is intended for documentation-only changes.

Summarize the purpose of this Merge Request here. See the [contribution guidelines](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md) for more details.

If this Merge Request addresses an [Issue](https://gitlab.arm.com/networking/ral/-/issues) that has already been raised, please provide a link to the Issue here. If an [Issue](https://gitlab.arm.com/networking/ral/-/issues) does not exist, please provide information on how to reproduce the problem here.

## Checklist

* [] [Contribution meets RAL's licence terms](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-licensing-information)
* [] ["Unreleased" section of the Changelog updated](https://gitlab.arm.com/networking/ral/-/blob/main/CHANGELOG.md#unreleased)
* [] [`make docs` target runs successfully](https://gitlab.arm.com/networking/ral/-/blob/main/README.md?ref_type=heads#user-content-documentation)
* [] [`clang-format` and `clang-tidy` run and changes included (C/C++ code)](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-cc-code-style)
* [] [`flake8` run and changes included (Python code)](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-python-code-style)
* [] [`cmake-format` run and changes included (CMake code)](https://gitlab.arm.com/networking/ral/-/blob/main/CONTRIBUTING.md#user-content-cmake-code-style)

For any items that are not checked, please provide details.
