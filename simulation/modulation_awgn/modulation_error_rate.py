#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

from argparse import ArgumentParser
import pandas as pd
import os
import sys
currentdir = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.join(parentdir, "include"))
# flake8 E402 errors require imports to be before code. We update the path to
# be able to import, so we ignore this one for the following imports.
from simulation_common import (plot_awgn_table,  # noqa: E402
                               plot_theoretical_and_shannon,
                               plot_format, ModulationAWGNTable, fname_str)


# Plots a graph for the data that is filtered out
def plot_graph(vals, error_rate, field, k, x, bw, save_plot):
    # Create and plot a pivot table that has one row per Eb/N0 or SNR value, and
    # columns that are the experimental bit or block error rates
    index = [x] if x == "snr" else ["Eb/N0"]
    t = ModulationAWGNTable(vals, index, field, k)
    ax = plot_awgn_table(t)

    # For modulation without forward error correction, the number of output
    # bits is equal to the number of input bits
    coding_rate = 1.0

    # Symbol rate
    symb_rate = bw

    # Plot theoretical maxima and Shannon limits for Eb/N0 vs bit error rate graphs
    plot_theoretical_and_shannon(t, bw, symb_rate, coding_rate, True, ax)

    # Format the plot and save it if required
    save_name = fname_str("modulation", error_rate, k, x) if save_plot else None
    plot_format(ax, t, None, error_rate, "Modulation", save_plot, save_name)


def main():
    parser = ArgumentParser(description="Plots data obtained from running the ldpc_awgn simulation program")
    parser.add_argument("infile", help="Name of the file to read data from")
    parser.add_argument("--error-rate", default="bit", choices=["bit", "block"], help="The error rate to plot")
    parser.add_argument("--k", type=int, help="The data length to plot a graph for")
    parser.add_argument("--x-unit", default="ebn0", choices=["ebn0", "snr"], help="The units to plot on the x-axis")
    parser.add_argument("--to-file", help="If set, save the plot to a file instead of plotting",
                        action="store_true")

    args = parser.parse_args()

    field = "ber" if args.error_rate == "bit" else "bler"
    vals = pd.read_json(args.infile, lines=True)

    bw = 1e6  # Bandwidth = 1 MHz

    plot_graph(vals, args.error_rate, field, args.k, args.x_unit, bw, args.to_file)


if __name__ == "__main__":
    main()
