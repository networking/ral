#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

from dataclasses import dataclass
from datetime import datetime
from math import sqrt, log
import matplotlib.pyplot as plt
import pandas as pd
from matplotlib import axes
from scipy import special as sp


# MxM-QAM class
class QAM:
    def __init__(self, m):
        self.m = m  # Number of unique Re{X} (M)
        symbols = m * m  # Total number of symbols (M^2)
        self.symbols = symbols
        self.bits_per_symbol = log(symbols, 2)

        # An MxM-QAM signal set is equivalent to two independent M-PAM transmissions. The
        # average signal energy per M-PAM symbol is E(A) = alpha^2 * (M^2 - 1)) / 3, where
        # A is the constellation A = alpha * {+-1, +-3, ..., +-(M-1)}. The average signal
        # energy per two dimension for a QAM symbol is Es = 2*E(A); alpha has been set in
        # order to have Es = 1.

        even = [-15, -13, -11, -9, -7, -5, -3, -1, 1, 3, 5, 7, 9, 11, 13, 15]
        if symbols == 4:
            name = "QPSK"
            alpha = 1 / sqrt(2)
            color = 'tab:red'
        elif symbols == 16:
            name = "16-QAM"
            alpha = 1 / sqrt(10)
            color = 'tab:blue'
        elif symbols == 64:
            name = "64-QAM"
            alpha = 1 / sqrt(42)
            color = 'tab:green'
        elif symbols == 256:
            name = "256-QAM"
            alpha = 1 / sqrt(170)
            color = 'tab:orange'
        else:
            raise ValueError("Unsupported number of modulation symbols: {}".format(symbols))
        self.coords = [e * alpha for e in even[(len(even) - m) // 2:(len(even) + m) // 2]]
        self.name = name
        self.color = color


# Modulation array
_qpsk = QAM(2)
_qam16 = QAM(4)
_qam64 = QAM(8)
_qam256 = QAM(16)
mod_vec = [_qpsk, _qam16, _qam64, _qam256]


# Compute Q(x)
def qfunc(x):
    # The Q-function Q(x) is the tail distribution function of the standard
    # normal distribution (with PDF fx(x) = 1 / sqrt(2 * pi) exp(-(x^2)/2)):
    #     Q(x) = 1 / sqrt(2 * pi) int_x^infinity exp(-(t^2) / 2) dt
    # The Q-function can be computed using the error function (erf(z)):
    #     Q(x) = 1/2 - 1/2 * erf(x/sqrt(2))
    return 0.5 - 0.5 * sp.erf(x / sqrt(2))


# Compute the probability of error per symbol for an (MxM)-QAM constellation
def th_error_rate(m, ebn0_db):
    ebn0 = pow(10, ebn0_db * 0.1)

    # For an M-PAM constellation A, the average energy per symbol is:
    #     E[A] = alpha^2 (M^2 - 1) / 3
    # The probability of error per symbol in the AWGN case is:
    #     Pr(E) = 2 * (M - 1) / M * Q(alpha / sigma)
    # The average energy per 2 dimension is Es = Eb * log2(M) = 2 * E[A], hence
    #     alpha = sqrt((3 * Eb * log2(M)) / (2 * (M^2 - 1))
    # Since sigma = sqrt(sigma^2) = sqrt(N0 / 2), we have
    #     Pr(E) = 2 * (M - 1) / M * Q(sqrt(Eb/N0 * (3 * log2(M)) / (M^2 - 1))
    pr_e = 2 * (m - 1) / m * qfunc(sqrt(ebn0 * (3 * log(m, 2)) / (m * m - 1)))

    # An (M x M)-QAM signal set is equivalent to two independent M-PAM
    # transmissions. The error probability per QAM symbol per 2 dimensions is:
    #     Ps(E) = 1 - (1 - Pr(E))^2
    ps_e = 2 * pr_e - pr_e * pr_e

    return ps_e


# Compute the Shannon limit on Eb/N0 (Eb/N0_min)
def shannon_limit(rho):
    # The Shannon limit on the spectral efficiency is:
    #     rho < C / B = log2(1 + SNR)
    # By definition SNR = Es / N0 = (rho * Eb) / N0, hence:
    #     Eb/N0 > (2^rho - 1) / rho
    # As a consequence:
    #     Eb/N0_min = (2^rho - 1) / rho
    # and lim rho->0 (Eb/N0_min) = ln(2)

    if rho != 0:
        ebn0_min = (pow(2, rho) - 1) / float(rho)
    else:
        ebn0_min = log(2)

    return 10 * log(ebn0_min, 10)


# Compute the spectral efficiency
def spectral_efficiency(coding_rate, rate, bps, bw):
    modulation_efficiency = rate * bps / bw
    spectral_efficiency = modulation_efficiency * coding_rate
    return spectral_efficiency


# Draw the Shannon limits (Eb/N0 min)
def draw_shannon_limits(ax, bw, symb_rate, coding_rate, draw_ultimate, ymin):
    for mod in mod_vec:
        rho = spectral_efficiency(coding_rate, symb_rate, mod.bits_per_symbol, bw)
        sh_l = shannon_limit(rho)
        ax.axvline(sh_l, color=mod.color, linestyle="dashed", ymax=.1, label="_nolegend_")
        ax.text(sh_l + 0.05, ymin, "Shannon\nlimit\n" r"($\rho$=" + '{:.2}'.format(rho) + ')\n{}'.format(mod.name))

    if draw_ultimate:
        sh_l = shannon_limit(0)
        ax.axvline(sh_l, color='k', linestyle="dashed", ymax=0.1, label="_nolegend_")
        ax.text(sh_l + 0.05, ymin, 'Ultimate\nShannon\nlimit')


# Return the error rate file name
def fname_str(mod, field, n, x):
    date_today = datetime.now().strftime("%Y%m%d")
    suffix = "_{}".format(n) if n is not None else ""
    return "{}_{}_{}_error_rate{}_{}.png".format(date_today, mod, field, suffix, x)


# Plot theoretical maxima and Shannon limits for Eb/N0 vs bit error rate graphs
def plot_theoretical_and_shannon(t, bw, symb_rate, coding_rate, draw_ultimate, ax):
    if t._field == "ber" and t._index == ["Eb/N0"]:
        x_axis = t._vals[t._index].drop_duplicates().to_numpy()
        ymin = t._vals[t._vals[t._field] != 0][t._field].min()
        for mod in mod_vec:
            th = [th_error_rate(mod.m, x) for x in x_axis]
            ax.plot(x_axis, th, color=mod.color, alpha=0.3, label="_nolegend_")
            ymin = min(ymin, min(i for i in th if i != 0))

        draw_shannon_limits(ax, bw, symb_rate, coding_rate, True, ymin)


# Class for creating and plotting a pivot table using data obtained from the
# coded AWGN simulation programs
class BaseAWGNTable:
    def __init__(self, vals, index, columns, field, filter_dict):
        self._vals = vals
        self._index = index
        self._columns = columns
        self._field = field
        self._filter_dict = filter_dict

    def filter_table(self):
        # Filter the data
        for key in self._filter_dict:
            if self._filter_dict.get(key) is not None:
                self._vals = self._vals[self._vals[key] == self._filter_dict[key]]

    def pivot_table(self):
        # Filter the data
        self.filter_table()

        if self._vals.empty:
            return None

        # Create and return the pivot table
        sub_cols = self._index + self._columns + [self._field]
        pt = self._vals[sub_cols].pivot_table(index=self._index, columns=self._columns)
        return pt


# Plot AWGN pivot table data
def plot_awgn_table(awgn_table, ax=None):
    pt = awgn_table.pivot_table()
    ax = pt.interpolate("index").plot(ax=ax)
    return ax


# Class for creating and plotting a pivot table using data obtained using the
# modulation_awgn simulation program
class ModulationAWGNTable(BaseAWGNTable):
    def __init__(self, vals, index, field, k):
        super().__init__(vals, index, ["k", "mod_type", "ulp"], field, {"k": k})


@dataclass
class PlotFormattedBase:
    ax: axes.Axes
    df: pd.DataFrame
    mod_df: pd.DataFrame
    error_rate: float
    name: str

    def set_grid(self):
        self.ax.grid(visible=True, axis="y", color="0.75")
        self.ax.grid(visible=True, axis="x", color="0.75")

    def set_xlabel(self, xlabel=None):
        if xlabel is not None:
            self.ax.set_xlabel(xlabel)
        else:
            xlabel = "SNR" if self.df._index[0] == "snr" else "$E_b/N_0$"
            self.set_xlabel(xlabel)

    def set_ylabel(self, ylabel=None):
        if ylabel is not None:
            self.ax.set_ylabel(ylabel)
        else:
            self.ax.set_ylabel("{} error rate".format(self.error_rate))

    def set_axes_scale(self):
        self.ax.set_yscale("log")

    def set_title(self, title=None):
        if title is not None:
            self.ax.set_title(title)
        else:
            self.ax.set_title(self.name)

    def _legend_label_str(self, labels):
        return ", ".join(str(p) for p in labels)

    def _legend_mod_label_str(self, labels):
        return ", ".join(str(p) for p in labels)

    def set_legend(self):
        pt = self.df.pivot_table()
        line_handles = self.ax.get_lines()

        # Add a dummy item to plot, with no marker or line style, with a heading
        # string for the legend
        legend_header = self.ax.plot(
            [0], marker=None, linestyle=None, color=None, linewidth=0,
            label=self._legend_label_str(pt.columns.names[1:]))

        legend_labels = [self._legend_label_str(s[1:]) for s in pt.columns]
        for lh, l in zip(line_handles, legend_labels):
            lh.set(label=l)

        if self.mod_df is not None:
            # Assume that the modulation line handles are after the handles for the
            # coding experiments
            mod_pt = self.mod_df.pivot_table()
            legend_labels = (self._legend_mod_label_str(s[1:]) for s in mod_pt.columns)
            for lh, l in zip(line_handles[len(legend_labels):], legend_labels):
                lh.set(label=l)

        line_handles = legend_header + line_handles
        # Use a monospace font so that we can align labels in columns
        plt.legend(handles=line_handles, prop={'family': 'monospace'})

    def __call__(self, save_plot, save_name):
        # Plot labels, legend, grid and title
        self.set_grid()
        self.set_xlabel()
        self.set_ylabel()
        self.set_axes_scale()
        self.set_title()
        self.set_legend()

        plt.tight_layout()

        if save_plot:
            fig = plt.gcf()
            fig.set_size_inches(18.5, 10.5)
            plt.savefig(save_name)
        else:
            plt.show()
