/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include <iostream>
#include <set>
#include <sstream>

#define SNEW(T, sz) ((T *)calloc(sz, sizeof(T)))

namespace armral::simulation {

uint8_t bits_per_symbol(armral_modulation_type mod_type) {
  uint8_t nb_bits;
  switch (mod_type) {
  case ARMRAL_MOD_QPSK:
    nb_bits = 2;
    break;
  case ARMRAL_MOD_16QAM:
    nb_bits = 4;
    break;
  case ARMRAL_MOD_64QAM:
    nb_bits = 6;
    break;
  case ARMRAL_MOD_256QAM:
    nb_bits = 8;
    break;
  default:
    std::cout << "Invalid mod_type" << std::endl;
    exit(EXIT_FAILURE);
  }
  return nb_bits;
}

const char *mod_to_str(armral_modulation_type mod_type) {
  switch (mod_type) {
  case ARMRAL_MOD_QPSK:
    return "QPSK";
  case ARMRAL_MOD_16QAM:
    return "16QAM";
  case ARMRAL_MOD_64QAM:
    return "64QAM";
  case ARMRAL_MOD_256QAM:
    return "256QAM";
  default:
    return "Invalid modulation type";
  }
}

// List of valid values of modulation type (mod_type).
const std::set<armral_modulation_type> valid_mod_type = {
    ARMRAL_MOD_QPSK, ARMRAL_MOD_16QAM, ARMRAL_MOD_64QAM, ARMRAL_MOD_256QAM};

// Check that the value of mod_type is valid.
bool is_valid_mod_type(armral_modulation_type mod_type) {
  return valid_mod_type.count(mod_type) != 0;
}

// Pretty print all the valid values of mod_type.
std::string print_valid_mod_type(int num_tabs) {
  std::ostringstream os;
  std::ostringstream tab_stream;
  for (int i = 0; i < num_tabs; i++) {
    tab_stream << "\t";
  }
  for (auto mod_type : valid_mod_type) {
    os << tab_stream.str() << mod_type << "\t " << mod_to_str(mod_type)
       << std::endl;
  }
  return os.str();
}

} // namespace armral::simulation
