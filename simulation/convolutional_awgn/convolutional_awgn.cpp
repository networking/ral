/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "awgn.hpp"
#include "simulation_common.hpp"
#include "utils/bits_to_bytes.hpp"

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <getopt.h>
#include <iostream>
#include <random>
#include <set>
#include <sstream>
#include <vector>

namespace {

// Print usage instructions on the command-line.
void usage(const char *exe_name) {
  std::cout
      << "Usage: " << exe_name
      << " -k num_bits -m mod_type [-u demod_ulp] [-i iter_max]\n\n"
      << "The arguments required by " << exe_name << " are:\n\n"
      << "  <num_bits>  Number of bits in the encoded message.\n"
      << "  <mod_type>  Type of modulation. Supported values are:\n"
      << armral::simulation::print_valid_mod_type(2)
      << "  <demod_ulp> Scaling parameter used in demodulation when\n"
      << "              using fixed-point Q2.13 representation for symbols.\n"
      << "              <demod_ulp> is an integer.\n"
      << "              such that the symbol amplitudes are multiplied by a\n"
      << "              scaling factor of 0x1p15/<demod_ulp>.\n"
      << "              Default value is 128.\n"
      << "  <iter_max>  The maximum number of iterations the decoder\n"
      << "              is allowed to perform.\n"
      << "              Default value is 3.\n"
      << std::endl;
}

struct convolutional_example_data {
  uint32_t len_in;      // k, the number of bits in the input block
  uint32_t len_encoded; // length (in bits) of the outputs of the encoder
  uint32_t len_out;     // k, the number of bits in the final decoded output
  armral_modulation_type mod_type; // the type of modulation used
  uint32_t num_mod_symbols; // the number of symbols in the modulated output
  uint8_t bit_per_symbol;   // the number of bits per symbol, given mod_type
  uint8_t *data_in;         // buffer to hold the input block to be transmitted
  uint8_t *data_in_bytes;   // buffer for 1 byte-per-bit version of data_in
  uint8_t *data0_encoded;   // the 1st output of the encoder
  uint8_t *data1_encoded;   // the 2nd output of the encoder
  uint8_t *data2_encoded;   // the 3rd output of the encoder
  armral_cmplx_int16_t *data0_mod; // the 1st modulated signal
  armral_cmplx_int16_t *data1_mod; // the 2nd modulated signal
  armral_cmplx_int16_t *data2_mod; // the 3rd modulated signal
  int8_t *data0_demod_soft;        // the 1st demodualted signal, stored as LLRs
  int8_t *data1_demod_soft;        // the 2nd demodualted signal, stored as LLRs
  int8_t *data2_demod_soft;        // the 3rd demodualted signal, stored as LLRs
  uint8_t *data_decoded;           // the decoded data, one bit per input bit
  uint8_t *data_decoded_bytes;     // the decoded data, one byte per input bit

  convolutional_example_data(uint32_t k, armral_modulation_type mod) {
    mod_type = mod;
    bit_per_symbol = armral::simulation::bits_per_symbol(mod_type);
    len_in = k;
    len_encoded = k;
    len_out = k;
    num_mod_symbols = (len_encoded + bit_per_symbol - 1) / bit_per_symbol;

    data_in = SNEW(uint8_t, (len_in + 7) / 8);
    data_in_bytes = SNEW(uint8_t, len_in);

    data0_encoded = SNEW(uint8_t, ((len_encoded + 7) / 8) + 1);
    data1_encoded = SNEW(uint8_t, ((len_encoded + 7) / 8) + 1);
    data2_encoded = SNEW(uint8_t, ((len_encoded + 7) / 8) + 1);

    data0_mod = SNEW(armral_cmplx_int16_t, num_mod_symbols);
    data1_mod = SNEW(armral_cmplx_int16_t, num_mod_symbols);
    data2_mod = SNEW(armral_cmplx_int16_t, num_mod_symbols);

    data0_demod_soft = SNEW(int8_t, len_encoded + 8);
    data1_demod_soft = SNEW(int8_t, len_encoded + 8);
    data2_demod_soft = SNEW(int8_t, len_encoded + 8);

    data_decoded = SNEW(uint8_t, (len_out + 7) / 8);
    data_decoded_bytes = SNEW(uint8_t, len_out);
  }

  ~convolutional_example_data() {
    free(data_in);
    free(data_in_bytes);
    free(data0_encoded);
    free(data1_encoded);
    free(data2_encoded);
    free(data0_mod);
    free(data1_mod);
    free(data2_mod);
    free(data0_demod_soft);
    free(data1_demod_soft);
    free(data2_demod_soft);
    free(data_decoded);
    free(data_decoded_bytes);
  }
};

// Perform an end-to-end encoding, modulation, transmission, demodulation, and
// decoding and count the number of errors
int run_check(armral::utils::random_state *state, double snr_db, uint32_t ulp,
              uint32_t iter_max, convolutional_example_data *data) {
  // Init data
  memset(data->data_in, 0, (data->len_in + 7) / 8 * sizeof(uint8_t));
  for (uint32_t i = 0; i < data->len_in; ++i) {
    uint8_t bit = static_cast<uint8_t>(
        armral::utils::linear_congruential_generator{}.one<bool>(state));
    uint16_t byte_ind = i / 8;
    // The most significant bit is the first bit (in wire order). Not sure if
    // that is an issue with randomly generated data, but we are paying
    // attention to it here.
    uint16_t idx = 7 - (i % 8);
    data->data_in[byte_ind] |= bit << idx;
  }

  // Run tail biting convolutional encoding for a single block
  armral_tail_biting_convolutional_encode_block(
      data->data_in, data->len_in, data->data0_encoded, data->data1_encoded,
      data->data2_encoded);

  // Run modulation
  armral_modulation(data->num_mod_symbols * data->bit_per_symbol,
                    data->mod_type, data->data0_encoded, data->data0_mod);
  armral_modulation(data->num_mod_symbols * data->bit_per_symbol,
                    data->mod_type, data->data1_encoded, data->data1_mod);
  armral_modulation(data->num_mod_symbols * data->bit_per_symbol,
                    data->mod_type, data->data2_encoded, data->data2_mod);

  // AWGN channel effects - add some noise to all the encoded bits
  armral::simulation::add_awgn(state, data->num_mod_symbols, snr_db,
                               ARMRAL_FIXED_POINT_INDEX_Q2_13, data->data0_mod);
  armral::simulation::add_awgn(state, data->num_mod_symbols, snr_db,
                               ARMRAL_FIXED_POINT_INDEX_Q2_13, data->data1_mod);
  armral::simulation::add_awgn(state, data->num_mod_symbols, snr_db,
                               ARMRAL_FIXED_POINT_INDEX_Q2_13, data->data2_mod);

  // Run demodulation
  armral_demodulation(data->num_mod_symbols, ulp, data->mod_type,
                      data->data0_mod, data->data0_demod_soft);
  armral_demodulation(data->num_mod_symbols, ulp, data->mod_type,
                      data->data1_mod, data->data1_demod_soft);
  armral_demodulation(data->num_mod_symbols, ulp, data->mod_type,
                      data->data2_mod, data->data2_demod_soft);

  // Run tail biting convolutional decoding for a single block.
  armral_tail_biting_convolutional_decode_block(
      data->data0_demod_soft, data->data1_demod_soft, data->data2_demod_soft,
      data->len_out, iter_max, data->data_decoded);
  // To make it easier to compare the values, convert the bit array to a byte
  // array
  armral::bits_to_bytes(data->len_out, data->data_decoded,
                        data->data_decoded_bytes);

  // Check the number of errors in decoding
  int num_errors = 0;
  armral::bits_to_bytes(data->len_in, data->data_in, data->data_in_bytes);
  for (uint32_t i = 0; i < data->len_in; ++i) {
    if (data->data_decoded_bytes[i] != data->data_in_bytes[i]) {
      num_errors++;
    }
  }
  return num_errors;
}

struct sim_result {
  sim_result(uint32_t k_in, armral_modulation_type mod, uint32_t ulp_in,
             double ebn0_in, double snr_in, uint32_t nb, uint32_t nm,
             uint32_t num_messages, uint32_t iter_max_in)
    : k(k_in), mod_type(armral::simulation::mod_to_str(mod)), ulp(ulp_in),
      ebn0(ebn0_in), snr(snr_in), bler(static_cast<double>(nm) / num_messages),
      ber(static_cast<double>(nb) / (num_messages * k)), iter_max(iter_max_in) {
  }

  uint32_t k;
  const char *mod_type;
  uint16_t ulp;
  double ebn0;
  double snr;
  double bler;
  double ber;
  uint32_t iter_max;

  std::string to_str() const {
    std::ostringstream s;
    s.precision(10);
    s.setf(std::ios::fixed, std::ios::floatfield);
    s << "{\"k\": " << k << ", \"mod_type\": \"" << mod_type
      << "\", \"iter_max\": " << iter_max << ", \"ulp\": " << ulp
      << ", \"Eb/N0\": " << ebn0 << ", \"snr\": " << snr
      << ", \"bler\": " << bler << ", \"ber\": " << ber << "}";
    return std::move(s).str();
  }
};

bool run_snr(uint32_t k, uint32_t iter_max, armral_modulation_type mod_type,
             uint16_t ulp, double ebn0_db) {
  // Compute SNR in dB
  int bits_per_symb = armral::simulation::bits_per_symbol(mod_type);
  // The coding ratio (k/n) of the LTE convolutional codes
  // is 1/3, see 3GPP TS 36.212
  double coding_rate = 1.0 / 3.0;
  double bw = 1e6; // Bandwidth (B) = 1 MHz
  // The symbol rate R [symbols/s] is proportional to the bandwidth. For
  // passband transmission using QAM modulation the maximum spectral efficiency
  // is equal to the number of bits per symbol. To meet this criteria we take
  // the symbol rate equal to the bandwidth.
  double symb_rate = bw;
  double snr_db = armral::simulation::ebn0_to_snr(coding_rate, bits_per_symb,
                                                  symb_rate, bw, ebn0_db);

  double tolerance = 1.0e-9;
  int nb = 0;
  uint64_t nr_total = 0;
  uint32_t num_message_errors = 0;
  while (nb < 10 && nr_total < 1e6) {
    uint64_t nr = 1e4;
#pragma omp parallel reduction(+ : nb, num_message_errors)
    {
      convolutional_example_data data(k, mod_type);
#pragma omp for
      for (uint64_t r = 0; r < nr; ++r) {
        auto state = armral::utils::random_state::from_seeds({r, nr_total});
        uint32_t num_bit_errors =
            run_check(&state, snr_db, ulp, iter_max, &data);
        nb += num_bit_errors;
        num_message_errors += num_bit_errors == 0 ? 0 : 1;
      }
    }
    nr_total += nr;
  }
  double message_error_rate =
      static_cast<double>(num_message_errors) / nr_total;

  // Write out data in JSON format
  std::cout << sim_result(k, mod_type, ulp, ebn0_db, snr_db, nb,
                          num_message_errors, nr_total, iter_max)
                   .to_str()
            << std::endl;

  return message_error_rate > tolerance;
}

} // anonymous namespace

int main(int argc, char **argv) {

  // Initialization
  uint32_t k = 0;
  uint16_t ulp = 0;
  uint32_t iter_max = 0;
  armral_modulation_type mod_type = ARMRAL_MOD_256QAM;
  bool is_k_set = false;
  bool is_mod_set = false;
  bool print_usage = false;

  // Parse arguments
  int option;
  while ((option = getopt(argc, argv, "k:m:u:i:")) != -1) {
    switch (option) {
    case 'k':
      k = (uint32_t)atoi(optarg);
      is_k_set = true;
      break;
    case 'm':
      mod_type = (armral_modulation_type)atoi(optarg);
      is_mod_set = true;
      break;
    case 'u':
      ulp = (uint16_t)atoi(optarg);
      break;
    case 'i':
      iter_max = (uint32_t)atoi(optarg);
      break;
    default:
      print_usage = true;
    }
  }

  // Check user input
  if (!is_k_set) {
    std::cerr
        << "Please specify the number of bits (k) in the encoded message.\n"
        << std::endl;
    print_usage = true;
  }
  if (!is_mod_set || !armral::simulation::is_valid_mod_type(mod_type)) {
    std::cerr << "Modulation type is invalid or not specified.\n"
              << "Must be one of:\n"
              << armral::simulation::print_valid_mod_type(1) << std::endl;
    print_usage = true;
  }

  // Print usage if required, then exit and return failure.
  if (print_usage) {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  // Set auto tuned ulp values
  if (ulp == 0) {
    // A default ulp value is 128, which removes some low order bits when
    // demodulating. Care should be taken that this is not too large. In
    // particular, when this value exceeds half the distance between
    // neighboring symbols, it is no longer possible to tell which symbol has
    // been transmitted in the case where we have a noiseless channel. This may
    // cause the simulation to never decode in an error-free manner.
    ulp = 128;
  }

  if (iter_max == 0) {
    // A default iter_max value is 3
    iter_max = 3;
  }

  for (double ebn0 = -2; run_snr(k, iter_max, mod_type, ulp, ebn0);
       ebn0 += 0.5) {
  }

  return 0;
}
