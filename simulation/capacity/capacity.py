#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

from argparse import ArgumentParser
from math import sqrt, exp, pi, log
import matplotlib.pyplot as plt
import os
import sys
currentdir = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.join(parentdir, "include"))
# flake8 E402 errors require imports to be before code. We update the path to
# be able to import, so we ignore this one for the following imports.
from simulation_common import QAM  # noqa: E402


def non_zero_condition(num, den):
    return den != 0 and num / den > 0


def compute_rate(n0, mod):
    # Claude Shannon* defined the entropy, a measure of the rate at which the information
    # is produced by a source x, as:
    #
    #    H(x) = - sum_i p_i(x) log2(p_i(x))
    #
    # If the source has entropy H (bits/symbol) and a channel has capacity C (bits/s), it
    # is possible to encode the output of the source to transmit at the average rate
    # C/H - epsilon symbols/s, where epsilon is arbitrarily small.
    #
    # If the channel is noisy, it is not possible to reconstruct with certainty the
    # transmitted signal by the receiver. The rate of transmission of information will be
    # reduced by the amount of information missing at the receiver side. The rate of the
    # actual transmission is obtained by subtracting the average rate of conditional
    # entropy from the entropy of the source. This is given by:
    #
    #    R = H(x) - H(x|y)
    #
    # where
    #
    #    H(x|y) = - sum_y sum_x P(Y=y, X=x) * log2(P(X=x|Y=y))
    #           = - sum_y sum_x P(X=x) * P(Y=y|X=x) * log2(P(Y=y|X=x) / sum_x P(Y=y|X=x))
    #
    # Y represents the received signal. It is continuous, so:
    #
    #    sum_y P(Y=y|X=x) = sum_k P(y_k < Y <= y_k+1|X=x)
    #                     ~= sum_k (y_k+1 - y_k) * f(y|X=x)((y_k + y_k+1) / 2 | X=x)
    #
    # In the AWGN case, Re{y} = Re{x} + Re{z} and Im{y} = Im{x} + Im{z}. Z represents the
    # Gaussian noise and the variance (sigma squared) of Re{z} (and Im{z}) is N0 / 2.
    # Hence, Re{y|x} ~ N(x, N0 / 2) (Im{y|x}, too) and its density function is:
    #
    #    f(Re{y}|Re{x}) = 1 / sqrt(pi * N0) * exp((y - Re{x})^2 / N0)
    #
    # H(x|y) can be computed as 2 * H(Re{x}|Re{y}).
    #
    #
    # * Shannon, Claude Elwood (July 1948). "A Mathematical Theory of Communication". Bell
    # System Technical Journal. 27 (3): 379-423.

    m = mod.m

    # Compute horizontal axis [-M, +M]
    step = 0.0125
    left = -m
    right = m
    length = int((right - left) / step)
    k = [left + i * step for i in range(length)]

    # Compute M times l f(Re{y}|Re{x}) values
    f = [[exp(-pow((k[ii] + step / 2 - mod.coords[i]), 2) / n0) / sqrt(pi * n0) for ii in range(length)] for i in range(m)]

    # Compute sum_x(f(Re{y}|Re{x}))
    tot_f = [sum((f[ii][i] for ii in range(m))) for i in range(length)]

    # Compute H(x|y) / 2
    hx_y = [sum(f[i][ii] * log(f[i][ii] / tot_f[ii], 2) if non_zero_condition(f[i][ii], tot_f[ii]) else 0 for ii in range(length)) for i in range(m)]
    hx_y_avg = -sum(hx_y) * step / m

    # Compute capacity C = H(x) - H(x|y)
    C = mod.bits_per_symbol - 2 * hx_y_avg

    return C


def plot_graph(r, snr_db, mod):
    # Compute capacity C = log_2(1 + SNR)
    c = [log(1 + (pow(10, s * 0.1)), 2) for s in snr_db]
    plt.plot(snr_db, c, 'k--', label="AWGN channel capacity")

    # Add spectral efficiencies of different modulations
    for i, m in enumerate(mod):
        plt.plot(snr_db, r[i], label=m.name)

    # Add labels and legend
    plt.xlabel("SNR [dB]")
    plt.ylabel("Spectral efficiencies [bits/2D]")
    plt.legend()

    # Save final image
    fig = plt.gcf()
    fig.set_size_inches(18.5, 10.5)
    plt.savefig("capacity.png")


def main():
    # Parse arguments
    parser = ArgumentParser(description="Plots data rates against SNR")
    parser.add_argument("--max_snr", type=float, help="Maximum SNR value in the graph in dB (default: 30 dB)", default=30)
    parser.add_argument("--step", type=float, help="Step for the SNRs in the graph in dB (default: 1 dB)", default=1)
    args = parser.parse_args()

    # Create modulation vector
    qpsk = QAM(2)
    qam16 = QAM(4)
    qam64 = QAM(8)
    qam256 = QAM(16)
    mod_vec = [qpsk, qam16, qam64, qam256]

    # Create SNR vector (from 0 to max_snr)
    # SNR = Es/N0 = 1/N0, where N0 is the noise spectral density
    step = args.step
    max_snr = args.max_snr
    snr_db = [s * step for s in range(0, int((max_snr + step) / step), 1)]

    # Create N0 vector
    n0_vec = [(pow(10, -snr * 0.1)) for snr in snr_db]

    # Compute spectral efficiencies rho = R/W [bits/symbol/Hz (or bits/2D)],
    # where R [bits/symbol] is the transmission rate and W [Hz] is the bandwidth
    rho = [[compute_rate(n0, mod) for n0 in n0_vec] for mod in mod_vec]

    # Create plot rho vs. SNR
    plot_graph(rho, snr_db, mod_vec)


if __name__ == "__main__":
    main()
