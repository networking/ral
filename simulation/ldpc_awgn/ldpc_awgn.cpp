/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "awgn.hpp"
#include "simulation_common.hpp"
#include "utils/bits_to_bytes.hpp"

#include <algorithm>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <getopt.h>
#include <iostream>
#include <random>
#include <set>
#include <sstream>
#include <vector>

namespace {

const std::vector<std::set<uint32_t>> valid_lifting = {
    {2, 4, 8, 16, 32, 64, 128, 256}, {3, 6, 12, 24, 48, 96, 192, 384},
    {5, 10, 20, 40, 80, 160, 320},   {7, 14, 28, 56, 112, 224},
    {9, 18, 36, 72, 144, 288},       {11, 22, 44, 88, 176, 352},
    {13, 26, 52, 104, 208},          {15, 30, 60, 120, 240}};

const std::set<uint32_t> valid_rv = {0, 1, 2, 3};

const std::set<uint32_t> valid_bg = {LDPC_BASE_GRAPH_1, LDPC_BASE_GRAPH_2};

bool is_valid_lifting_size(uint32_t z) {
  auto is_valid = [z](const std::set<uint32_t> &s) { return s.count(z) != 0; };
  return std::any_of(valid_lifting.begin(), valid_lifting.end(), is_valid);
}

std::string print_valid_lifting(const std::string &margin) {
  std::ostringstream os;
  for (const auto &lifting_size_set : valid_lifting) {
    std::string sep;
    os << margin;
    for (const auto &lifting_size : lifting_size_set) {
      os << sep << lifting_size;
      sep = ", ";
    }
    os << sep << std::endl;
  }
  return os.str();
}

bool is_valid_redundancy_version(uint32_t rv) {
  return valid_rv.count(rv) != 0;
}

std::string print_valid_redundancy_version() {
  std::ostringstream os;
  std::string sep;
  for (const auto &rv : valid_rv) {
    os << sep << rv;
    sep = ", ";
  }
  os << std::endl;
  return os.str();
}

bool is_valid_base_graph(uint32_t bg) {
  return valid_bg.count(bg) != 0;
}

std::string print_valid_base_graph() {
  std::ostringstream os;
  std::string sep;
  for (const auto &bg : valid_bg) {
    os << sep << int(bg) + 1;
    sep = ", ";
  }
  os << std::endl;
  return os.str();
}

void usage(const char *exe_name) {
  std::cout
      << "Usage: " << exe_name << " -z lifting_size -b base_graph -m mod_type "
      << "[-r redundancy_version] [-u demod_ulp] [-f len_filler_bits]\n\n"
      << "The arguments required by " << exe_name << " are:\n\n"
      << "  <lifting_size>       Lifting size. Supported values are:\n"
      << print_valid_lifting("\t\t\t")
      << "  <base_graph>         Identifier of the base graph. Supported\n"
      << "                       values are:\n"
      << "                       " << print_valid_base_graph()
      << "  <mod_type>           Type of modulation. Supported values are:\n"
      << armral::simulation::print_valid_mod_type(3)
      << "  <redundancy_version> The redundancy version to be used. Supported\n"
      << "                       values are:\n"
      << "                       " << print_valid_redundancy_version()
      << "                       Default value is 0.\n"
      << "  <demod_ulp>          Scaling parameter used in demodulation when\n"
      << "                       using fixed-point Q2.13 representation for\n"
      << "                       symbols. <demod_ulp> is an integer such that\n"
      << "                       the symbol amplitudes are multiplied by a\n"
      << "                       scaling factor of 0x1p15/<demod_ulp>.\n"
      << "                       Default value is 128.\n"
      << "  <len_filler_bits>    Number of filler bits to use when simulating\n"
      << "                       cases where the transport block length is\n"
      << "                       not a multiple of the lifting size.\n"
      << "                       Default length is 0.\n"
      << std::endl;
}

struct ldpc_example_data {
  uint32_t len_in;
  uint32_t len_filler_bits;
  uint32_t len_encoded;
  uint32_t len_rate_matched;
  uint32_t len_out;
  uint32_t nref;
  armral_modulation_type mod_type;
  uint32_t num_mod_symbols;
  uint8_t bit_per_symbol;
  uint8_t *data_in;
  uint8_t *data_in_bytes;
  uint8_t *data_encoded;
  uint8_t *data_encoded_bytes;
  uint8_t *data_matched;
  armral_cmplx_int16_t *data_mod;
  int8_t *data_demod_soft;
  int8_t *data_recovered;
  uint8_t *data_decoded;
  uint8_t *data_decoded_bytes;

  ldpc_example_data(uint32_t z, armral_modulation_type mod,
                    const armral_ldpc_base_graph_t *graph,
                    uint16_t filler_bits_len) {
    mod_type = mod;
    len_in = z * graph->nmessage_bits;
    len_filler_bits = filler_bits_len;
    len_encoded = z * graph->ncodeword_bits;
    len_out = len_encoded + 2 * z;
    data_in = SNEW(uint8_t, (len_in + 7) / 8);
    data_in_bytes = SNEW(uint8_t, 2 * z);
    data_encoded = SNEW(uint8_t, (len_encoded + 7) / 8);
    data_encoded_bytes = SNEW(uint8_t, len_encoded);
    bit_per_symbol = armral::simulation::bits_per_symbol(mod_type);
    nref = 0;
    len_rate_matched =
        bit_per_symbol * ((len_encoded + bit_per_symbol - 1) / bit_per_symbol);
    data_matched = SNEW(uint8_t, len_rate_matched);
    num_mod_symbols = (len_encoded + bit_per_symbol - 1) / bit_per_symbol;
    data_mod = SNEW(armral_cmplx_int16_t, num_mod_symbols);
    data_demod_soft = SNEW(int8_t, len_rate_matched);
    data_recovered = SNEW(int8_t, len_encoded);
    data_decoded = SNEW(uint8_t, (len_out + 7) / 8);
    data_decoded_bytes = SNEW(uint8_t, len_out);
  }

  ~ldpc_example_data() {
    free(data_in);
    free(data_in_bytes);
    free(data_encoded);
    free(data_encoded_bytes);
    free(data_matched);
    free(data_mod);
    free(data_demod_soft);
    free(data_recovered);
    free(data_decoded);
    free(data_decoded_bytes);
  }
};

int run_check(armral::utils::random_state *state, uint32_t z,
              armral_ldpc_graph_t bg, uint32_t rv, double snr_db, uint32_t ulp,
              ldpc_example_data *data) {
  // Init data
  memset(data->data_in, 0, (data->len_in + 7) / 8 * sizeof(uint8_t));
  for (uint32_t i = 0; i < (data->len_in - data->len_filler_bits); ++i) {
    uint8_t bit = static_cast<uint8_t>(
        armral::utils::linear_congruential_generator{}.one<bool>(state));
    uint16_t byte_ind = i / 8;
    // The most significant bit is the first bit (in wire order). Not sure if
    // that is an issue with randomly generated data, but we are paying
    // attention to it here.
    uint16_t idx = 7 - (i % 8);
    data->data_in[byte_ind] |= bit << idx;
  }

  // Run ldpc encoding for a single block
  armral_ldpc_encode_block(data->data_in, bg, z, data->len_filler_bits,
                           data->data_encoded);
  // To make it easier to compare the bits, convert the bit array to a byte
  // array
  armral::bits_to_bytes(data->len_encoded, data->data_encoded,
                        data->data_encoded_bytes);

  // Rate match data_encoded to create an array of length e bits from
  // num_mod_symbols * bit_per_symbol bits.
  armral_ldpc_rate_matching(
      bg, z, data->len_rate_matched, data->nref, data->len_filler_bits,
      data->len_in, rv, data->mod_type, data->data_encoded, data->data_matched);

  // Run modulation
  armral_modulation(data->num_mod_symbols * data->bit_per_symbol,
                    data->mod_type, data->data_matched, data->data_mod);

  // AWGN channel effects - add some noise
  armral::simulation::add_awgn(state, data->num_mod_symbols, snr_db,
                               ARMRAL_FIXED_POINT_INDEX_Q2_13, data->data_mod);

  // Run demodulation
  armral_demodulation(data->num_mod_symbols, ulp, data->mod_type,
                      data->data_mod, data->data_demod_soft);

  // Rate recovery updates LLRs. Since we only send one message, we have
  // to ensure that the input LLRs are set to zero.
  memset(data->data_recovered, 0, data->len_encoded);

  // Rate recovery inverses rate matching
  armral_ldpc_rate_recovery(bg, z, data->len_rate_matched, data->nref,
                            data->len_filler_bits, data->len_in, rv,
                            data->mod_type, data->data_demod_soft,
                            data->data_recovered);

  // Run LDPC decoding for a single block
  armral_ldpc_decode_block(data->data_recovered, bg, z, ARMRAL_LDPC_NO_CRC, 10,
                           data->data_decoded);

  // To make it easier to compare the values, convert the bit array to a byte
  // array
  armral::bits_to_bytes(data->len_in, data->data_decoded,
                        data->data_decoded_bytes);

  // Check the number of errors in decoding
  int num_errors = 0;
  armral::bits_to_bytes(2 * z, data->data_in, data->data_in_bytes);
  // Check that the punctured columns are the same as the input data
  for (uint32_t i = 0; i < 2 * z; ++i) {
    if (data->data_decoded_bytes[i] != data->data_in_bytes[i]) {
      num_errors++;
    }
  }
  // For the remainder of the columns check that the data is the same
  // as what came out of the encoding
  const uint8_t *out_ptr = data->data_decoded_bytes + 2 * z;
  for (uint32_t i = 0; i < data->len_in - 2 * z; ++i) {
    if (out_ptr[i] != data->data_encoded_bytes[i]) {
      num_errors++;
    }
  }

  return num_errors;
}

struct sim_result {
  sim_result(uint32_t n_in, armral_ldpc_graph_t bg_in,
             armral_modulation_type mod, uint32_t rv_in, double ebn0_in,
             double snr_in, uint16_t ulp_in, uint16_t filler_bits_len,
             uint32_t nb, uint32_t nm, uint32_t num_messages)
    : n(n_in), bg((int)bg_in + 1),
      mod_type(armral::simulation::mod_to_str(mod)), rv(rv_in), ebn0(ebn0_in),
      snr(snr_in), ulp(ulp_in), len_filler_bits(filler_bits_len),
      bler(static_cast<double>(nm) / num_messages),
      ber(static_cast<double>(nb) / (num_messages * n_in)) {}

  uint32_t n;
  int bg;
  const char *mod_type;
  uint32_t rv;
  double ebn0;
  double snr;
  uint16_t ulp;
  uint16_t len_filler_bits;
  double bler;
  double ber;

  std::string to_str() const {
    std::ostringstream s;
    s.precision(10);
    s.setf(std::ios::fixed, std::ios::floatfield);
    s << "{\"n\": " << n << ", \"bg\": " << bg << ", \"mod_type\": \""
      << mod_type << "\", \"rv\": " << rv << ", \"Eb/N0\": " << ebn0
      << ", \"snr\": " << snr << ", \"ulp\": " << ulp
      << ", \"len_filler_bits\": " << len_filler_bits << ",\"bler\": " << bler
      << ", \"ber\": " << ber << "}";
    return std::move(s).str();
  }
};

bool run_snr(uint32_t z, armral_modulation_type mod_type,
             armral_ldpc_graph_t bg, uint32_t rv, uint16_t ulp,
             uint16_t len_filler_bits, double ebn0_db) {
  const auto *graph = armral_ldpc_get_base_graph(bg);
  // Compute SNR in dB
  int bits_per_symb = armral::simulation::bits_per_symbol(mod_type);
  // The coding rate (k/n) for LDPC base graph 1 is 1/3 (k = 22 Z, n = 66 Z)
  // and 1/5 for LDPC base graph 2 (k = 10 Z, n = 50 Z), see 3GPP TS 38.212
  double coding_rate;
  if (bg == LDPC_BASE_GRAPH_1) {
    coding_rate = 1.0 / 3.0;
  } else {
    coding_rate = 1.0 / 5.0;
  }

  double bw = 1e6; // Bandwidth (B) = 1 MHz
  // The symbol rate R [symbols/s] is proportional to the bandwidth. For
  // passband transmission using QAM modulation the maximum spectral efficiency
  // is equal to the number of bits per symbol. To meet this criteria we take
  // the symbol rate equal to the bandwidth.
  double symb_rate = bw;
  double snr_db = armral::simulation::ebn0_to_snr(coding_rate, bits_per_symb,
                                                  symb_rate, bw, ebn0_db);

  double tolerance = 1.0e-9;
  int nb = 0;
  int n = z * graph->ncodeword_bits;
  uint64_t nr_total = 0;
  uint32_t num_message_errors = 0;
  while (nb < 10 && nr_total < 1e6) {
    uint64_t nr = 1e4;
#pragma omp parallel reduction(+ : nb, num_message_errors)
    {
      ldpc_example_data data(z, mod_type, graph, len_filler_bits);
#pragma omp for
      for (uint64_t r = 0; r < nr; ++r) {
        auto state = armral::utils::random_state::from_seeds({r, nr_total});
        uint32_t num_bit_errors =
            run_check(&state, z, bg, rv, snr_db, ulp, &data);
        nb += num_bit_errors;
        num_message_errors += num_bit_errors == 0 ? 0 : 1;
      }
    }
    nr_total += nr;
  }
  double message_error_rate =
      static_cast<double>(num_message_errors) / nr_total;

  // Write out data in JSON format
  std::cout << sim_result(n, bg, mod_type, rv, ebn0_db, snr_db, ulp,
                          len_filler_bits, nb, num_message_errors, nr_total)
                   .to_str()
            << std::endl;

  return message_error_rate > tolerance;
}

} // anonymous namespace

int main(int argc, char **argv) {

  // Initialization
  bool is_z_set = false;
  uint32_t z = 0;
  armral_ldpc_graph_t bg = (armral_ldpc_graph_t)0;
  bool is_bg_set = false;
  uint32_t rv = 0;
  uint16_t ulp = 0;
  uint16_t len_filler_bits = 0;
  armral_modulation_type mod_type = ARMRAL_MOD_256QAM;
  bool is_mod_set = false;
  bool print_usage = false;

  // Parse arguments
  int option;
  while ((option = getopt(argc, argv, "z:b:m:r:u:f:")) != -1) {
    switch (option) {
    case 'z':
      z = (uint32_t)atoi(optarg);
      is_z_set = true;
      break;
    case 'b':
      bg = (armral_ldpc_graph_t)(atoi(optarg) - 1);
      is_bg_set = true;
      break;
    case 'm':
      mod_type = (armral_modulation_type)atoi(optarg);
      is_mod_set = true;
      break;
    case 'r':
      rv = (uint32_t)atoi(optarg);
      break;
    case 'u':
      ulp = (uint16_t)atoi(optarg);
      break;
    case 'f':
      len_filler_bits = (uint16_t)atoi(optarg);
      break;
    default:
      print_usage = true;
    }
  }

  // Check user input
  if (!is_z_set || !is_valid_lifting_size(z)) {
    std::cerr << "Lifting size is invalid or not specified. Must be one of:\n"
              << print_valid_lifting("\t") << std::endl;
    print_usage = true;
  }
  if (!is_valid_redundancy_version(rv)) {
    std::cerr << "Redundancy version is invalid. Must be one of:\n"
              << "\t" << print_valid_redundancy_version() << std::endl;
    print_usage = true;
  }
  if (!is_bg_set || !is_valid_base_graph(bg)) {
    std::cerr << "Base graph identifier is invalid or not specified.\n"
              << "Must be one of:\n"
              << "\t" << print_valid_base_graph() << std::endl;
    print_usage = true;
  }
  if (!is_mod_set || !armral::simulation::is_valid_mod_type(mod_type)) {
    std::cerr << "Modulation type is invalid or not specified.\n"
              << "Must be one of:\n"
              << armral::simulation::print_valid_mod_type(1) << std::endl;
    print_usage = true;
  }

  // Print usage if required, then exit and return failure.
  if (print_usage) {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  // Set auto tuned ulp values
  if (ulp == 0) {
    // A default ulp value is 128, which removes some low order bits when
    // demodulating. Care should be taken that this is not too large. In
    // particular, when this value exceeds half the distance between
    // neighboring symbols, it is no longer possible to tell which symbol has
    // been transmitted in the case where we have a noise-free channel. This may
    // cause the simulation to never decode in an error-free manner.
    ulp = 128;
  }

  for (double ebn0 = -2;
       run_snr(z, mod_type, bg, rv, ulp, len_filler_bits, ebn0); ebn0 += 0.5) {
  }

  return 0;
}
