#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

from argparse import ArgumentParser
from dataclasses import dataclass
import pandas as pd
import os
import sys
currentdir = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.join(parentdir, "include"))
# flake8 E402 errors require imports to be before code. We update the path to
# be able to import, so we ignore this one for the following imports.
from simulation_common import (plot_awgn_table,  # noqa: E402
                               plot_theoretical_and_shannon,
                               PlotFormattedBase, BaseAWGNTable, ModulationAWGNTable, fname_str)


@dataclass
class PlotFormattedLDPC(PlotFormattedBase):
    def _legend_label_str(self, labels):
        # We have labels of the form n, bg, rv, ulp, mod_type
        assert len(labels) == 5
        widths = [7, 5, 5, 6, 9]
        return "".join(f"{str(lab):<{w}}" for lab, w in zip(labels, widths))


# Convert number of bits encoded to lifting size
def z2l(z, g):
    if g == 1:
        return int(z * 66)
    else:
        return int(z * 50)


# Return the coding rate
def code_rate(g):
    if g == 1:
        return 1.0 / 3.0
    else:
        return 1.0 / 5.0


# Plots a graph for the data that is filtered out
def plot_graph(vals, error_rate, field, z, bg, rv, x, bw, save_plot, mod_vals):
    # Compute the input length
    if z is None:
        n = None
    else:
        n = z2l(z, bg)

    # Filter on the n, bg and rv passed in
    filters = {"bg": bg, "n": n, "rv": rv}

    # Create and plot a pivot table that has one row per Eb/N0 or SNR value, and
    # columns that are the experimental bit or block error rates
    index = [x] if x == "snr" else ["Eb/N0"]
    cols = ["n", "bg", "rv", "ulp", "mod_type"]
    t = BaseAWGNTable(vals, index, cols, field, filters)
    ax = plot_awgn_table(t)

    # Coding rate k/n (1/3 for base graph 1 and 1/5 for base graph 2)
    coding_rate = code_rate(bg)

    # Symbol rate
    symb_rate = bw

    # Plot theoretical maxima and Shannon limits for Eb/N0 vs bit error rate graphs
    plot_theoretical_and_shannon(t, bw, symb_rate, coding_rate, True, ax)

    # Plot error rates for when there is no forward error correction
    mod_t = ModulationAWGNTable(mod_vals, index, field, n) if mod_vals is not None else None
    if mod_t is not None:
        plot_awgn_table(mod_t, ax)

    # Format the plot and save it if required
    if save_plot is None:
        save_name = None
        save_plot = False
    elif save_plot == "":
        save_name = fname_str("ldpc", error_rate, n, x)
        save_plot = True
    else:
        save_name = save_plot
        save_plot = True
    plotter = PlotFormattedLDPC(ax, t, mod_t, error_rate, "LDPC code")
    plotter(save_plot, save_name)


def main():
    parser = ArgumentParser(description="Plots data obtained from running the ldpc_awgn simulation program")
    parser.add_argument("infile", help="Name of the file to read data from")
    parser.add_argument("--error-rate", default="bit", choices=["bit", "block"], help="The error rate to plot")
    parser.add_argument("--z", type=int, help="Lifting size to plot a graph for")
    parser.add_argument("--graph", type=int, default=1, choices=[1, 2], help="The base graph to show results for")
    parser.add_argument("--rv", type=int, choices=[0, 1, 2, 3], help="Only show results for the given redundancy version")
    parser.add_argument("--x-unit", default="ebn0", choices=["ebn0", "snr"], help="The units to plot on the x-axis")
    parser.add_argument(
        "--to-file",
        help="If set, save the plot to a file instead of plotting. Passing "
             "a value specifies the filename, otherwise a filename is "
             "auto generated",
        nargs="?",
        const="",
        action="store"
    )
    parser.add_argument("--mod-file", help="Name of file containing data obtained \
                         from running the modulation_awgn simulation program")

    args = parser.parse_args()

    field = "ber" if args.error_rate == "bit" else "bler"
    vals = pd.read_json(args.infile, lines=True)

    mod_vals = pd.read_json(args.mod_file, lines=True) if args.mod_file else None

    bw = 1e6  # Bandwidth = 1 MHz

    plot_graph(vals, args.error_rate, field, args.z, args.graph, args.rv, args.x_unit, bw, args.to_file, mod_vals)


if __name__ == "__main__":
    main()
