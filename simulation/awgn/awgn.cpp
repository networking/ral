/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "awgn.hpp"
#include "rng.hpp"

#include <algorithm>
#include <cmath>

namespace armral::simulation {

/*
 * Return a random double floating-point number in the range [-1, 1).
 */
static double sample_double_unit(armral::utils::random_state *state) {
  armral::utils::linear_congruential_generator lcg;
  return lcg.one<double>(state) * 2.0 - 1.0;
}

/*
 * Return a number taken from a normal distribution with mean=0 and
 * the specified stddev.
 */
static double sample_normal(armral::utils::random_state *state, double sigma) {
  double u;
  double r;
  do {
    u = sample_double_unit(state);
    double v = sample_double_unit(state);
    r = u * u + v * v;
  } while (r == 0 || r > 1);
  double c = sqrt(-2 * log(r) / r);
  return u * c * sigma;
}

/*
 * Add noise to a channel where it is assumed that the power (mean square
 * amplitude) of the signal is 1. We target a specific signal to noise ratio in
 * decibels, and deduce the random field required to produce noise of the
 * appropriate power (mean square amplitude). This noise is added to the signal.
 */
void add_awgn(armral::utils::random_state *state, int num_mod_symbols,
              double snr_db, armral_fixed_point_index frac_bits,
              armral_cmplx_int16_t *xs) {

  //    snr_db = 10 * log_10(s / r)
  // => r = 10^(-snr/10)
  // where r is the mean square amplitude of the noise
  double noise_square_amp = pow(10, -snr_db / 10);

  // r = (1 / n) * sum_{i} |noise_i|^2
  //   = (1 / n) * sum_{i} (|phase_i|^2 + |quad_i|^2)
  //   = (1 / n) * sum_{i} |phase_i|^2 + (1 / n) sum_{i} |quad_i|^2
  //   = variance(phase) + variance(quad)
  //
  // where variance(x) is the variance of a zero mean normal distribution over
  // scalar field x. Letting variance of phase and quadrature components be the
  // same, we have that
  //    variance(phase) = r / 2
  // => stddev(phase) = sqrt(r / 2)
  double sigma = sqrt(noise_square_amp / 2);

  for (int i = 0; i < num_mod_symbols; ++i) {
    // Generate noise, convert to fixed-point, and add to
    // result with saturation
    double red = sample_normal(state, sigma) * (1 << (int)frac_bits) + xs[i].re;
    double imd = sample_normal(state, sigma) * (1 << (int)frac_bits) + xs[i].im;
    int re = std::round(red);
    int im = std::round(imd);
    xs[i].re = std::clamp<int>(re, INT16_MIN, INT16_MAX);
    xs[i].im = std::clamp<int>(im, INT16_MIN, INT16_MAX);
  }
}

/*
 * Return the SNR in dB given Eb/N0, the coding rate, the number of bits per
 * symbol and the bandwidth.
 */
double ebn0_to_snr(double coding_rate, int bits_per_symb, double symb_rate,
                   double bw, double ebn0_db) {
  // The modulation efficiency [(bit/s)/Hz] is the gross rate (or raw rate)
  // divided by the bandwidth. The gross rate is measured in bit/s and it is
  // given by the symbol rate multiplied by the number of bits per symbol
  double mod_eff = symb_rate * bits_per_symb / bw;

  // The spectral efficiency (rho) [(bit/s)/Hz] is the information rate (or net
  // rate) divided by the bandwidth. Since the information rate is the gross
  // rate multiplied by the coding rate k/n, the spectral efficiency is k/n
  // times the modulation efficiency
  double spec_eff = mod_eff * coding_rate;

  // Compute SNR = Es / N0 = rho * Eb / N0 in dB (rho in dB + EB/N0 in dB)
  double snr_db = 10 * log10(spec_eff) + ebn0_db;

  return snr_db;
}

} // namespace armral::simulation
