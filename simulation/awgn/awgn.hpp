/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "rng.hpp"

namespace armral::simulation {

/*
 * Add noise to a channel where it is assumed that the power (mean square
 * amplitude) of the signal is 1. We target a specific signal to noise ratio in
 * decibels, and deduce the random field required to produce noise of the
 * appropriate power (mean square amplitude). This noise is added to the signal.
 * param [in,out] state           State for random number generation.
 * param [in] num_mod_symbols     The number of modulation symbols to add noise
 *                                to.
 * param [in] snr_db              The target signal to noise ratio.
 * param [in] frac_bits           The number of fractional bits in the fixed
 *                                point representation to convert to.
 * param [in,out] xs              On input, the signal to add noise to. On
 *                                output the signal, disturbed by random noise.
 */
void add_awgn(armral::utils::random_state *state, int num_mod_symbols,
              double snr_db, armral_fixed_point_index frac_bits,
              armral_cmplx_int16_t *xs);

/*
 * Compute the SNR in db given the coding rate, the bits per symbol, the
 * bandwidth and Eb/N0.
 * param [in] coding_rate         The coding rate k/n
 * param [in] bits_per_symb       The number of bits per symbol
 * param [in] symb_rate           The symbol rate
 * param [in] bw                  The available bandwidth measured in Hz
 * param [in] ebn0_db             The energy per bit to noise power spectral
 *                                density ratio (Eb/N0) in dB
 * return snr_db                  The SNR in dB
 */
double ebn0_to_snr(double coding_rate, int bits_per_symb, double symb_rate,
                   double bw, double ebn0_db);

} // namespace armral::simulation
