/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "awgn.hpp"
#include "simulation_common.hpp"
#include "utils/bits_to_bytes.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstdlib>
#include <cstring>
#include <getopt.h>
#include <iostream>
#include <random>
#include <set>
#include <sstream>
#include <vector>

namespace {

// List of valid values of list decode length (l).
const std::set<uint32_t> valid_l = {1, 2, 4, 8};

// Check that the value of l specified is actually one we can do.
bool l_is_valid(uint32_t l) {
  return valid_l.count(l) != 0;
}

// Pretty print all the valid values of l.
std::string print_valid_l() {
  std::ostringstream os;
  std::string sep;
  for (const auto &l : valid_l) {
    os << sep << l;
    sep = ", ";
  }
  os << std::endl;
  return os.str();
}

// Print usage instructions on the command-line.
void usage(const char *exe_name) {
  std::cout
      << "Usage: " << exe_name << " -k num_info_bits -e num_trans_bits\n"
      << " -m mod_type -i i_bil [-u demod_ulp] [-l list_size]\n\n"
      << "The arguments required by " << exe_name << " are:\n\n"
      << "  <num_info_bits>   Number of information bits in the encoded\n"
      << "                    message.\n"
      << "  <num_trans_bits>  Number of transmitted bits, matched to the\n"
      << "                    rate of the channel.\n"
      << "                    To ensure a code rate inferior or equal to 1,\n"
      << "                    this program expects <num_trans_bits> greater\n"
      << "                    than or equal to <num_info_bits>.\n"
      << "  <mod_type>        Type of modulation. Supported values are:\n"
      << armral::simulation::print_valid_mod_type(3)
      << "  <i_bil>           Flag to enable/disable the interleaving of \n"
      << "                    coded bits in Polar rate matching.\n"
      << "                    Type 0 : Downlink, Type 1 : Uplink.\n"
      << "  <demod_ulp>       Scaling parameter used in demodulation when\n"
      << "                    using fixed-point Q2.13 representation for\n"
      << "                    the symbols. <demod_ulp> is an integer\n"
      << "                    such that the symbol amplitudes are multiplied\n"
      << "                    by a scaling factor of 0x1p15/<demod_ulp>.\n"
      << "                    Default value is 128.\n"
      << "  <list_size>       Number of possible decodings to return.\n"
      << "                    Supported values are: " << print_valid_l()
      << "                    Default value is 1.\n"
      << std::endl;
}

uint32_t get_codeword_length(uint32_t e, uint32_t k) {
  // As per TS 38.212 section 5.3.1, the codeword size is determined by the
  // rate-matched length and the number of information bits to send.
  uint32_t pow2_f = static_cast<uint32_t>(powf(2., ceilf(log2f(e))));
  if ((e <= (9 / 16.) * pow2_f) && (k / e < (9 / 16.))) {
    pow2_f /= 2;
  }
  return pow2_f;
}

struct polar_example_data {
  // The codeword length to perform Polar encoding on. Must be a power of two
  // greater than or equal to 32, and less than or equal to 1024.
  uint32_t n;
  // The transmitted data length. To match the rate of the channel.
  // Should be greater or equal to the number of information bits `k`.
  uint32_t e;
  // The number of information bits in the encoded message
  uint32_t k;
  // The list decode length (1, 2, 4 or 8)
  uint32_t l;
  // Polar rate matching/rate recovery ibil type
  armral_polar_ibil_type i_bil;
  // Type of modulation
  armral_modulation_type mod_type;
  // Demodulation scaling factor
  uint16_t demod_ulp;
  // The length of the decoded word (is the length of the data in times the list
  // size)
  uint32_t len_out;
  uint32_t bits_per_mod_symbol;
  uint32_t num_mod_symbols;
  uint8_t *data_in;
  uint8_t *frozen_mask;
  uint8_t *data_interleave;
  uint8_t *data_interleave_bytes;
  uint8_t *data_encoded;
  uint8_t *data_matched;
  armral_cmplx_int16_t *data_mod;
  int8_t *data_demod_soft;
  int8_t *data_recovered;
  uint8_t *data_decoded;
  uint8_t *data_decoded_bytes;

  polar_example_data(uint32_t n_in, uint32_t e_in, uint32_t k_in, uint32_t l_in,
                     armral_polar_ibil_type i_bil_in,
                     armral_modulation_type mod, uint16_t demod_ulp_in) {
    assert(k_in <= e_in);
    assert(l_in == 1 || l_in == 2 || l_in == 4);
    n = n_in;
    e = e_in;
    k = k_in;
    l = l_in;
    i_bil = i_bil_in;
    mod_type = mod;
    demod_ulp = demod_ulp_in;
    bits_per_mod_symbol = armral::simulation::bits_per_symbol(mod_type);
    // We want to encode k bits of data
    data_in = SNEW(uint8_t, (k + 7) / 8);
    // Codeword length is n, which is what we interleave
    data_interleave = SNEW(uint8_t, n / 8);
    data_interleave_bytes = SNEW(uint8_t, n);
    // The frozen mask applies to each bit in the codeword, also length n
    frozen_mask = SNEW(uint8_t, n);
    // The number of modulation symbols required is the ceiling of the
    // rate-matched length divided by the number of bits per symbol
    num_mod_symbols = (e + bits_per_mod_symbol - 1) / bits_per_mod_symbol;
    data_mod = SNEW(armral_cmplx_int16_t, num_mod_symbols);
    // We encode before modulation, and we encode a codeword
    data_encoded = SNEW(uint8_t, (n + 7) / 8);
    data_matched =
        SNEW(uint8_t, (num_mod_symbols * bits_per_mod_symbol + 7) / 8);
    // The demodulated length is the number of modulated symbols, times the bits
    // per symbol
    data_demod_soft = SNEW(int8_t, num_mod_symbols * bits_per_mod_symbol);
    data_recovered = SNEW(int8_t, n);
    // The length of the data out is the number of llrs we store per bit, times
    // the number of bits in the codeword
    len_out = l * n;
    data_decoded = SNEW(uint8_t, (len_out + 7) / 8);
    data_decoded_bytes = SNEW(uint8_t, len_out);
  }

  ~polar_example_data() {
    free(data_in);
    free(frozen_mask);
    free(data_interleave);
    free(data_encoded);
    free(data_matched);
    free(data_interleave_bytes);
    free(data_mod);
    free(data_demod_soft);
    free(data_recovered);
    free(data_decoded);
    free(data_decoded_bytes);
  }
};

int run_check(armral::utils::random_state *state, double snr_db,
              polar_example_data *data) {

  uint32_t crc_bits = 24;                 // CRC-24 (L = 24)
  uint32_t msg_bits = data->k - crc_bits; // message length (A = K - L)

  std::vector<uint8_t> msg((msg_bits + 7) / 8);
  for (uint32_t i = 0; i < msg_bits; ++i) {
    uint8_t bit = static_cast<uint8_t>(
        armral::utils::linear_congruential_generator{}.one<bool>(state));
    uint16_t byte_ind = i / 8;
    // The most significant bit is the first bit (in wire order). Not sure if
    // that is an issue with randomly generated data, but we are paying
    // attention to it here.
    uint16_t idx = 7 - (i % 8);
    msg[byte_ind] |= bit << idx;
  }

  // Initialize data
  memset(data->data_in, 0, (data->k + 7) / 8 * sizeof(uint8_t));
  armral_polar_crc_attachment(msg.data(), msg_bits, data->data_in);

  // Create a frozen mask to use in the polar coding
  armral_polar_frozen_mask(data->n, data->e, data->k, 0, 0, data->frozen_mask);

  armral_polar_subchannel_interleave(data->n, data->k, data->frozen_mask,
                                     data->data_in, data->data_interleave);

  // Convert the data to a byte array rather than a bit string. This makes
  // comparison of the data easier later on, where we want to count the number
  // of incorrect bits
  armral::bits_to_bytes(data->n, data->data_interleave,
                        data->data_interleave_bytes);

  armral_polar_encode_block(data->n, data->data_interleave, data->data_encoded);

  // Rate match data_encoded to create an array of length e bits from n
  // bits
  armral_polar_rate_matching(data->n, data->e, data->k, data->i_bil,
                             data->data_encoded, data->data_matched);

  // Simulate a noisy channel. This involves modulating data, adding noise to
  // the modulated symbols, and then demodulating
  armral_modulation(data->num_mod_symbols * data->bits_per_mod_symbol,
                    data->mod_type, data->data_matched, data->data_mod);

  // AWGN channel effects - add some noise
  armral::simulation::add_awgn(state, data->num_mod_symbols, snr_db,
                               ARMRAL_FIXED_POINT_INDEX_Q2_13, data->data_mod);

  armral_demodulation(data->num_mod_symbols, data->demod_ulp, data->mod_type,
                      data->data_mod, data->data_demod_soft);

  // Rate recovery to get N LLRs from E LLRs
  armral_polar_rate_recovery(data->n, data->e, data->k, data->i_bil,
                             data->data_demod_soft, data->data_recovered);

  // Now run the decoding
  armral_polar_decode_block(data->n, data->frozen_mask, data->l,
                            data->data_recovered, data->data_decoded);

  // Deinterleave and compute CRC check on L codewords, stop if CRC == 0 and
  // return a BER equal to zero.
  // Deinterleaving ignores parity bits, so K rather than K + n_pc.
  std::vector<uint8_t> data_deint0((data->k + 7) / 8);
  std::vector<uint8_t> data_deint((data->k + 7) / 8);
  armral_polar_subchannel_deinterleave(data->k, data->frozen_mask,
                                       data->data_decoded, data_deint0.data());
  if (armral_polar_crc_check(data_deint0.data(), data->k)) {
    return 0;
  }
  for (uint32_t i = 1; i < data->l; i++) {
    const uint8_t *data_dec_l_ptr = data->data_decoded + i * data->n / 8;
    armral_polar_subchannel_deinterleave(data->k, data->frozen_mask,
                                         data_dec_l_ptr, data_deint.data());
    if (armral_polar_crc_check(data_deint.data(), data->k)) {
      return 0;
    }
  }

  // If all L CRC checks failed, return the BER of the ML codeword
  int num_errors = 0;
  // To make it easier to compare the values, convert the bit arrays to byte
  // arrays
  std::vector<uint8_t> data_in_bytes(data->k);
  std::vector<uint8_t> data_deint0_bytes(data->k);
  armral::bits_to_bytes(data->k, data->data_in, data_in_bytes.data());
  armral::bits_to_bytes(data->k, data_deint0.data(), data_deint0_bytes.data());
  for (uint32_t i = 0; i < data->k; ++i) {
    if (data_deint0_bytes[i] != data_in_bytes[i]) {
      num_errors++;
    }
  }
  return num_errors;
}

struct sim_result {
  sim_result(uint32_t n, uint32_t e_in, uint32_t k_in, uint32_t l_in,
             armral_modulation_type mod, armral_polar_ibil_type i_bil_in,
             uint16_t ulp_in, double ebn0_in, double snr_in, uint32_t nb,
             uint32_t nm, uint32_t num_messages)
    : len(n), e(e_in), k(k_in), l(l_in),
      mod_type(armral::simulation::mod_to_str(mod)), i_bil(i_bil_in),
      ulp(ulp_in), ebn0(ebn0_in), snr(snr_in),
      bler(static_cast<double>(nm) / num_messages),
      ber(static_cast<double>(nb) / (num_messages * k)) {}

  uint32_t len;
  uint32_t e;
  uint32_t k;
  uint32_t l;
  const char *mod_type;
  armral_polar_ibil_type i_bil;
  uint16_t ulp;
  double ebn0;
  double snr;
  double bler;
  double ber;

  std::string to_str() const {
    std::ostringstream s;
    s.precision(10);
    s.setf(std::ios::fixed, std::ios::floatfield);
    s << "{\"len\": " << len << ", \"e\": " << e << ", \"k\": " << k
      << ", \"l\": " << l << ", \"mod_type\": \"" << mod_type
      << "\", \"i_bil\": " << i_bil << ", \"ulp\": " << ulp
      << ", \"Eb/N0\": " << ebn0 << ", \"snr\": " << snr
      << ", \"bler\": " << bler << ", \"ber\": " << ber << "}";
    return std::move(s).str();
  }
};

bool run_snr(uint32_t e, uint32_t k, uint32_t l,
             armral_modulation_type mod_type, armral_polar_ibil_type i_bil,
             uint16_t ulp, double ebn0_db) {
  uint32_t n = get_codeword_length(e, k);
  // Compute SNR in dB
  int bits_per_symb = armral::simulation::bits_per_symbol(mod_type);
  double coding_rate = (double)k / n;
  double bw = 1e6; // Bandwidth (B) = 1 MHz
  // The symbol rate R [symbols/s] is proportional to the bandwidth. For
  // passband transmission using QAM modulation the maximum spectral efficiency
  // is equal to the number of bits per symbol. To meet this criteria we take
  // the symbol rate equal to the bandwidth.
  double symb_rate = bw;
  double snr_db = armral::simulation::ebn0_to_snr(coding_rate, bits_per_symb,
                                                  symb_rate, bw, ebn0_db);

  int nb = 0;
  uint64_t nr_total = 0;
  uint32_t num_message_errors = 0;
  while (nb < 100 && nr_total < 1e7) {
    uint64_t nr = 1e5;
#pragma omp parallel reduction(+ : nb, num_message_errors)
    {
      polar_example_data data(n, e, k, l, i_bil, mod_type, ulp);
#pragma omp for
      for (uint64_t r = 0; r < nr; ++r) {
        auto state = armral::utils::random_state::from_seeds({r, nr_total});
        uint32_t num_bit_errors = run_check(&state, snr_db, &data);
        nb += num_bit_errors;
        num_message_errors += num_bit_errors == 0 ? 0 : 1;
      }
    }
    nr_total += nr;
  }

  // Write out data in JSON format
  std::cout << sim_result(n, e, k, l, mod_type, i_bil, ulp, ebn0_db, snr_db, nb,
                          num_message_errors, nr_total)
                   .to_str()
            << std::endl;
  return nb > 0;
}

} // anonymous namespace

int main(int argc, char **argv) {

  // Initialization
  uint32_t k = 0;
  bool is_k_set = false;
  uint32_t e = 0;
  bool is_e_set = false;
  uint32_t l = 1;
  uint16_t ulp = 0;
  armral_modulation_type mod_type = ARMRAL_MOD_256QAM;
  armral_polar_ibil_type i_bil = ARMRAL_POLAR_IBIL_ENABLE;
  bool is_i_bil_set = false;
  bool is_mod_set = false;
  bool print_usage = false;
  // Parse arguments
  int option;
  while ((option = getopt(argc, argv, "k:e:m:i:u:l:")) != -1) {
    switch (option) {
    case 'k':
      k = (uint32_t)atoi(optarg);
      is_k_set = true;
      break;
    case 'e':
      e = (uint32_t)atoi(optarg);
      is_e_set = true;
      break;
    case 'm':
      mod_type = (armral_modulation_type)atoi(optarg);
      is_mod_set = true;
      break;
    case 'i':
      i_bil = (armral_polar_ibil_type)atoi(optarg);
      is_i_bil_set = true;
      break;
    case 'u':
      ulp = (uint16_t)atoi(optarg);
      break;
    case 'l':
      l = (uint32_t)atoi(optarg);
      break;
    default:
      // An unsupported option as been passed
      print_usage = true;
    }
  }
  // Check user input
  if (!is_k_set) {
    std::cerr << "Please specify a number of information bits (k).\n"
              << std::endl;
    print_usage = true;
  }
  if (!is_e_set || (k > e)) {
    std::cerr << "Number of transmitted bits (e) is invalid or not specified.\n"
              << "\t"
              << "Program expects k <= e." << std::endl;
    print_usage = true;
  }
  if (!l_is_valid(l)) {
    std::cerr << "List decode length l is invalid. Must be one of:\n"
              << "\t" << print_valid_l() << std::endl;
    print_usage = true;
  }
  if (!is_mod_set || !armral::simulation::is_valid_mod_type(mod_type)) {
    std::cerr << "Modulation type is invalid or not specified.\n"
              << "Must be one of:\n"
              << armral::simulation::print_valid_mod_type(1) << std::endl;
    print_usage = true;
  }
  if (!is_i_bil_set) {
    std::cerr << "Please specify the flag i_bil to enable/disable\n"
              << "the interleaving of coded bits.\n"
              << std::endl;
    print_usage = true;
  }

  // Print usage if required, then exit and return failure.
  if (print_usage) {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  // Set auto tuned ulp values
  if (ulp == 0) {
    // A default ulp value is 128, which removes some low order bits when
    // demodulating. Care should be taken that this is not too large. In
    // particular, when this value exceeds half the distance between
    // neighboring symbols, it is no longer possible to tell which symbol has
    // been transmitted in the case where we have a noiseless channel. This may
    // cause the simulation to never decode in an error-free manner.
    ulp = 128;
  }
  for (double snr = -2; run_snr(e, k, l, mod_type, i_bil, ulp, snr);
       snr += 0.5) {
  }

  return 0;
}
