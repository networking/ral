#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

from argparse import ArgumentParser
from dataclasses import dataclass
import pandas as pd
import os
import sys
currentdir = os.path.dirname(os.path.abspath(__file__))
parentdir = os.path.dirname(currentdir)
sys.path.insert(0, os.path.join(parentdir, "include"))
# flake8 E402 errors require imports to be before code. We update the path to
# be able to import, so we ignore this one for the following imports.
from simulation_common import (plot_awgn_table,  # noqa: E402
                               plot_theoretical_and_shannon,
                               PlotFormattedBase, BaseAWGNTable,
                               ModulationAWGNTable, fname_str)


@dataclass
class PlotFormattedPolar(PlotFormattedBase):
    def _legend_label_str(self, labels):
        # we have labels of the form: len, e, k, l, mod type, ulp, coding rate
        assert len(labels) == 7
        widths = [7, 7, 7, 3, 10, 5, 12]

        def to_str(x):
            if isinstance(x, float):
                return f"{x:.2f}"
            return str(x)

        return "".join(f"{to_str(lab):<{w}}" for lab, w in zip(labels, widths))


# Plots a graph for the data that is filtered out
def plot_graph(vals, error_rate, field, n, x, bw, save_plot, mod_vals):
    # Compute the coding rate and add column
    vals["coding rate"] = vals["k"] / vals["len"]

    # Definitions for creating the pivot table
    index = [x] if x == "snr" else ["Eb/N0"]
    cols = ["len", "e", "k", "l", "mod_type", "ulp", "coding rate"]

    # Filter on the n, if given
    filters = {"len": n}
    t = BaseAWGNTable(vals, index, cols, field, filters)
    t.filter_table()

    # Symbol rate
    symb_rate = bw

    # Plot a graph for each coding rate
    for _, vals_cr in t._vals.groupby("coding rate"):
        # Create and plot a pivot table that has one row per Eb/N0 or SNR value, and
        # columns that are the experimental bit or block error rates
        t_cr = BaseAWGNTable(vals_cr, index, cols, field, filters)
        ax = plot_awgn_table(t_cr)

        # Compute the coding rate
        coding_rate = float(vals_cr["coding rate"].drop_duplicates().values)

        # Plot theoretical maxima and Shannon limits for Eb/N0 vs bit error rate graphs
        plot_theoretical_and_shannon(t_cr, bw, symb_rate, coding_rate, True, ax)

        # Plot error rates for when there is no forward error correction
        mod_t = ModulationAWGNTable(mod_vals, index, field, n) if mod_vals is not None else None
        if mod_t is not None:
            plot_awgn_table(mod_t, ax)

        # Format the plot and save it if required
        if save_plot is None:
            save_name = None
            save_plot = False
        elif save_plot == "":
            save_name = fname_str("polar", error_rate, n, x).replace(
                      ".png", "_{:.2f}".format(coding_rate).replace(".", "") + ".png")
            save_plot = True
        else:
            save_name = save_plot
            save_plot = True

        plotter = PlotFormattedPolar(ax, t_cr, mod_t, error_rate, "Polar code")
        plotter(save_plot, save_name)


def main():
    parser = ArgumentParser(description="Plots data obtained from running the polar_awgn simulation program")
    parser.add_argument("infile", help="Name of the file to read data from")
    parser.add_argument("--error-rate", default="bit", choices=["bit", "block"], help="The error rate to plot")
    parser.add_argument("--len", type=int, help="The data length to plot a graph for")
    parser.add_argument("--x-unit", default="ebn0", choices=["ebn0", "snr"], help="The units to plot on the x-axis")
    parser.add_argument(
        "--to-file",
        help="If set, save the plot to a file instead of plotting. Passing "
             "a value specifies the filename, otherwise a filename is "
             "auto generated",
        nargs="?",
        action="store",
        const=""
    )
    parser.add_argument("--mod-file", help="Name of file containing data obtained \
                         from running the modulation_awgn simulation program")

    args = parser.parse_args()

    field = "ber" if args.error_rate == "bit" else "bler"
    vals = pd.read_json(args.infile, lines=True)

    mod_vals = pd.read_json(args.mod_file, lines=True) if args.mod_file else None

    bw = 1e6  # Bandwidth = 1 MHz

    plot_graph(vals, args.error_rate, field, args.len, args.x_unit, bw, args.to_file, mod_vals)


if __name__ == "__main__":
    main()
