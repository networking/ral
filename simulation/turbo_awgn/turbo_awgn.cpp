/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "awgn.hpp"
#include "simulation_common.hpp"
#include "utils/bits_to_bytes.hpp"

#include <algorithm>
#include <cstdlib>
#include <cstring>
#include <getopt.h>
#include <iostream>
#include <random>
#include <set>
#include <sstream>
#include <vector>

namespace {

// The valid values of k specified in TS 36.212.
// We use a vector of vectors so we can pretty print them
// on the command-line.
const std::vector<std::set<uint32_t>> valid_block_size = {
    {40, 48, 56, 64, 72, 80, 88, 96, 104, 112, 120, 128},
    {136, 144, 152, 160, 168, 176, 184, 192, 200, 208, 216, 224},
    {232, 240, 248, 256, 264, 272, 280, 288, 296, 304, 312, 320},
    {328, 336, 344, 352, 360, 368, 376, 384, 392, 400, 408, 416},
    {424, 432, 440, 448, 456, 464, 472, 480, 488, 496, 504, 512},
    {528, 544, 560, 576, 592, 608, 624, 640, 656, 672, 688, 704},
    {720, 736, 752, 768, 784, 800, 816, 832, 848, 864, 880, 896},
    {912, 928, 944, 960, 976, 992, 1008, 1024, 1056, 1088, 1120, 1152},
    {1184, 1216, 1248, 1280, 1312, 1344, 1376, 1408, 1440, 1472, 1504, 1536},
    {1568, 1600, 1632, 1664, 1696, 1728, 1760, 1792, 1824, 1856, 1888, 1920},
    {1952, 1984, 2016, 2048, 2112, 2176, 2240, 2304, 2368, 2432, 2496, 2560},
    {2624, 2688, 2752, 2816, 2880, 2944, 3008, 3072, 3136, 3200, 3264, 3328},
    {3392, 3456, 3520, 3584, 3648, 3712, 3776, 3840, 3904, 3968, 4032, 4096},
    {4160, 4224, 4288, 4352, 4416, 4480, 4544, 4608, 4672, 4736, 4800, 4864},
    {4928, 4992, 5056, 5120, 5184, 5248, 5312, 5376, 5440, 5504, 5568, 5632},
    {5696, 5760, 5824, 5888, 5952, 6016, 6080, 6144}};

// Check that the value of k specified is actually one we can do.
bool block_size_valid(uint32_t k) {
  auto is_valid = [k](const std::set<uint32_t> &s) { return s.count(k) != 0; };
  return std::any_of(valid_block_size.begin(), valid_block_size.end(),
                     is_valid);
}

// Pretty print all the valid values of k.
std::string print_valid_block_size(const std::string &margin) {
  std::ostringstream os;
  for (const auto &block_size_set : valid_block_size) {
    std::string sep;
    os << margin;
    for (const auto &block_size : block_size_set) {
      os << sep << block_size;
      sep = ", ";
    }
    os << sep << std::endl;
  }
  return os.str();
}

// Check that the redundancy version is supported.
bool is_valid_rv(uint32_t rv) {
  return rv < 4;
}

// Get a string of the valid redundancy version values for printing.
std::string valid_rv_str() {
  std::ostringstream os;
  os << "0";
  for (uint32_t rv = 1; rv < 4; ++rv) {
    os << ", " << rv;
  }
  os << std::endl;
  return os.str();
}

// Print usage instructions on the command-line.
void usage(const char *exe_name) {
  std::cout
      << "Usage: " << exe_name
      << " -k num_bits -m mod_type -e num_matched_bits [-b num_blocks] [-r rv]"
      << " [-u demod_ulp] [-i iter_max]\n\n"
      << "The arguments required by " << exe_name << " are:\n\n"
      << "  <num_bits>         Number of bits in each block of the encoded\n"
      << "                     message. This must be one of:\n"
      << print_valid_block_size("\t\t\t")
      << "  <mod_type>         Type of modulation. Supported values are:\n"
      << armral::simulation::print_valid_mod_type(3)
      << "  <num_matched_bits> Number of bits in the rate-matched message.\n"
      << "  <num_blocks>       Number of blocks of data to decode. Values\n"
      << "                     greater than or equal to 8 will use the\n"
      << "                     8-block batched turbo decoder, with any\n"
      << "                     remaining blocks decoded with the single turbo\n"
      << "                     decoder. Default value is 1.\n"
      << "  <rv>               The redundancy version used for rate matching\n"
      << "                     and recovery. Supported values are:\n"
      << "                     " << valid_rv_str()
      << "                     Default value is 0.\n"
      << "  <demod_ulp>        Scaling parameter used in demodulation when\n"
      << "                     using fixed-point Q2.13 representation for\n"
      << "                     symbols. <demod_ulp> is an integer such that\n"
      << "                     the symbol amplitudes are multiplied by a\n"
      << "                     scaling factor of 0x1p15/<demod_ulp>.\n"
      << "                     Default value is 128.\n"
      << "  <iter_max>         The maximum number of iterations the Turbo\n"
      << "                     decoder is allowed to perform.\n"
      << "                     Default value is 10.\n"
      << std::endl;
}

struct turbo_example_data {
  uint16_t num_blocks;  // Number of blocks. All other lengths are per block.
  uint32_t len_in;      // k, the number of bits in the input block
  uint32_t len_encoded; // length (in bits) of the outputs of the encoder
  uint32_t len_matched; // length (in bits) of the rate-matched message
  uint32_t rv;          // redundancy version number
  uint32_t len_out;     // k, the number of bits in the final decoded output
  armral_modulation_type mod_type; // the type of modulation used
  uint32_t num_mod_symbols; // the number of symbols in the modulated output
  uint8_t bit_per_symbol;   // the number of bits per symbol, given mod_type
  uint8_t *data_in;         // buffer to hold the input block to be transmitted
  uint8_t *data_in_bytes;   // buffer for 1 byte-per-bit version of data_in
  uint8_t *sys_encoded;     // the systematic output from the turbo encoder
  uint8_t *par_encoded;     // the parity output from the turbo encoder
  uint8_t *itl_encoded;  // the interleaved parity output from the turbo encoder
  uint8_t *data_matched; // the output from rate matching
  armral_cmplx_int16_t *data_mod; // the modulated rate-matched values
  int8_t *data_demod_soft;        // the demodulated values, stored as LLRs
  int8_t *sys_recovered; // the recovered systematic values, stored as LLRs
  int8_t *par_recovered; // the recovered parity values, stored as LLRs
  int8_t *itl_recovered; // the recovered interleaved parity values, stored
  uint8_t *data_decoded; // the decoded data, one bit per input bit
  uint8_t *data_decoded_bytes;   // the decoded data, one byte per input bit
  uint16_t *permutation_indices; // buffer to hold all permutation indices

  turbo_example_data(uint32_t size_of_batch, uint32_t k,
                     armral_modulation_type mod, uint32_t e, uint32_t r,
                     uint16_t *perm_idxs) {
    num_blocks = size_of_batch;
    mod_type = mod;
    len_in = k;
    len_encoded = k + 4;
    len_matched = e;
    rv = r;
    len_out = k;
    permutation_indices = perm_idxs;
    data_in = SNEW(uint8_t, num_blocks * (len_in + 7) / 8);
    data_in_bytes = SNEW(uint8_t, num_blocks * k);
    sys_encoded = SNEW(uint8_t, num_blocks * (len_encoded + 7) / 8);
    par_encoded = SNEW(uint8_t, num_blocks * (len_encoded + 7) / 8);
    itl_encoded = SNEW(uint8_t, num_blocks * (len_encoded + 7) / 8);
    data_matched = SNEW(uint8_t, num_blocks * (len_matched + 7) / 8);
    bit_per_symbol = armral::simulation::bits_per_symbol(mod_type);
    num_mod_symbols = (len_matched + bit_per_symbol - 1) / bit_per_symbol;
    data_mod = SNEW(armral_cmplx_int16_t, num_blocks * num_mod_symbols);
    sys_recovered = SNEW(int8_t, num_blocks * len_encoded);
    par_recovered = SNEW(int8_t, num_blocks * len_encoded);
    itl_recovered = SNEW(int8_t, num_blocks * len_encoded);
    data_demod_soft =
        SNEW(int8_t, num_blocks * num_mod_symbols * bit_per_symbol);
    data_decoded = SNEW(uint8_t, num_blocks * (len_out + 7) / 8);
    data_decoded_bytes = SNEW(uint8_t, num_blocks * len_out);
  }

  ~turbo_example_data() {
    free(data_in);
    free(data_in_bytes);
    free(sys_encoded);
    free(par_encoded);
    free(itl_encoded);
    free(data_matched);
    free(data_mod);
    free(sys_recovered);
    free(par_recovered);
    free(itl_recovered);
    free(data_demod_soft);
    free(data_decoded);
    free(data_decoded_bytes);
  }
};

// A structure to encapsulate the bit/block error counts from one simulation
struct turbo_error_counts {
  uint32_t num_bit_errors;
  uint32_t num_block_errors;

  turbo_error_counts() {
    num_bit_errors = 0;
    num_block_errors = 0;
  }
};

// Perform an end-to-end encoding, rate matching, modulation, transmission,
// demodulation, rate recovery, and decoding and count the number of errors
void run_check(armral::utils::random_state *state, double snr_db, uint32_t ulp,
               uint32_t iter_max, turbo_example_data *data,
               turbo_error_counts *results) {
  // Setup num_blocks blocks of data, one block at a time
  for (uint32_t batch_idx = 0; batch_idx < data->num_blocks; ++batch_idx) {
    uint32_t encoded_offset = batch_idx * ((data->len_encoded + 7) / 8);
    uint32_t recovered_offset = batch_idx * data->len_encoded;
    uint32_t matched_offset = batch_idx * ((data->len_matched + 7) / 8);
    uint32_t in_offset = batch_idx * ((data->len_in + 7) / 8);
    uint32_t mod_offset = batch_idx * data->num_mod_symbols;
    uint32_t demod_soft_offset =
        batch_idx * data->num_mod_symbols * data->bit_per_symbol;

    // Init data
    memset(data->data_in + in_offset, 0,
           (data->len_in + 7) / 8 * sizeof(uint8_t));
    for (uint32_t i = 0; i < data->len_in; ++i) {
      uint8_t bit = static_cast<uint8_t>(
          armral::utils::linear_congruential_generator{}.one<bool>(state));
      uint16_t byte_ind = i / 8;
      // The most significant bit is the first bit (in wire order). Not sure if
      // that is an issue with randomly generated data, but we are paying
      // attention to it here.
      uint16_t idx = 7 - (i % 8);
      data->data_in[in_offset + byte_ind] |= bit << idx;
    }

    // Run turbo encoding for a single block
    armral_turbo_encode_block(data->data_in + in_offset, data->len_in,
                              data->sys_encoded + encoded_offset,
                              data->par_encoded + encoded_offset,
                              data->itl_encoded + encoded_offset);

    // Run turbo rate matching. This performs the operations described in
    // section 5.1.4 of TS 36.212 to match the rate of the encoded code block to
    // the rate of the channel. The output is an array which stores e bits.
    armral_turbo_rate_matching(data->len_encoded, data->len_matched, data->rv,
                               data->sys_encoded + encoded_offset,
                               data->par_encoded + encoded_offset,
                               data->itl_encoded + encoded_offset,
                               data->data_matched + matched_offset);

    // Run modulation
    armral_modulation(data->num_mod_symbols * data->bit_per_symbol,
                      data->mod_type, data->data_matched + matched_offset,
                      data->data_mod + mod_offset);

    // AWGN channel effects - add some noise to all the encoded bits
    armral::simulation::add_awgn(state, data->num_mod_symbols, snr_db,
                                 ARMRAL_FIXED_POINT_INDEX_Q2_13,
                                 data->data_mod + mod_offset);

    // Run demodulation
    armral_demodulation(data->num_mod_symbols, ulp, data->mod_type,
                        data->data_mod + mod_offset,
                        data->data_demod_soft + demod_soft_offset);

    // The LLRs are updated by rate recovery and must be zero the first time
    // rate recovery is performed. Since different input data is created for
    // every loop iteration, we need to reset the LLRs each time.
    memset(data->sys_recovered + recovered_offset, 0, data->len_encoded);
    memset(data->par_recovered + recovered_offset, 0, data->len_encoded);
    memset(data->itl_recovered + recovered_offset, 0, data->len_encoded);

    // Run turbo rate recovery. This performs the inverse operations of rate
    // matching to output the rate-recovered LLRs.
    armral_turbo_rate_recovery(data->len_encoded, data->len_matched, data->rv,
                               data->data_demod_soft + demod_soft_offset,
                               data->sys_recovered + recovered_offset,
                               data->par_recovered + recovered_offset,
                               data->itl_recovered + recovered_offset);
  }

  // Run turbo decoding for num_blocks blocks
  armral_turbo_decode_batch(data->num_blocks, data->sys_recovered,
                            data->par_recovered, data->itl_recovered,
                            data->len_out, data->data_decoded, iter_max,
                            data->permutation_indices);

  results->num_bit_errors = 0;
  results->num_block_errors = 0;

  // To make it easier to compare the values, convert the bit array to a byte
  // array
  for (uint32_t b = 0; b < data->num_blocks; ++b) {
    uint32_t decoded_offset = b * (data->len_out + 7) / 8;
    uint32_t in_offset = b * (data->len_in + 7) / 8;

    armral::bits_to_bytes(data->len_out, data->data_decoded + decoded_offset,
                          data->data_decoded_bytes + b * data->len_out);

    // Check the number of errors in decoding
    armral::bits_to_bytes(data->len_in, data->data_in + in_offset,
                          data->data_in_bytes + b * data->len_in);

    bool block_error = false;
    for (uint32_t i = 0; i < data->len_in; ++i) {
      if (data->data_decoded_bytes[i + b * data->len_out] !=
          data->data_in_bytes[i + b * data->len_in]) {
        results->num_bit_errors++;
        block_error = true;
      }
    }
    if (block_error) {
      results->num_block_errors++;
    }
  }
}

struct sim_result {
  sim_result(uint32_t k_in, uint32_t e_in, armral_modulation_type mod,
             uint32_t ulp_in, double ebn0_in, double snr_in,
             uint32_t iter_max_in, uint32_t nb, uint32_t nm,
             uint32_t num_messages, uint32_t num_blocks_in)
    : k(k_in), e(e_in), mod_type(armral::simulation::mod_to_str(mod)),
      ulp(ulp_in), ebn0(ebn0_in), snr(snr_in), iter_max(iter_max_in),
      bler(static_cast<double>(nm) / num_messages),
      ber(static_cast<double>(nb) / (num_messages * k)),
      num_blocks(num_blocks_in) {}

  uint32_t k;
  uint32_t e;
  const char *mod_type;
  uint16_t ulp;
  double ebn0;
  double snr;
  uint32_t iter_max;
  double bler;
  double ber;
  uint32_t num_blocks;

  std::string to_str() const {
    std::ostringstream s;
    s.precision(10);
    s.setf(std::ios::fixed, std::ios::floatfield);
    s << "{\"k\": " << k << ", \"e\": " << e
      << ", \"num_blocks\": " << num_blocks << ", \"mod_type\": \"" << mod_type
      << "\", \"ulp\": " << ulp << ", \"Eb/N0\": " << ebn0
      << ", \"snr\": " << snr << ", \"iter_max\": " << iter_max
      << ", \"bler\": " << bler << ", \"ber\": " << ber << "}";
    return std::move(s).str();
  }
};

bool run_snr(uint32_t k, uint32_t iter_max, armral_modulation_type mod_type,
             uint32_t e, uint32_t rv, uint16_t ulp, double ebn0_db,
             uint16_t *perm_idxs, uint32_t num_blocks) {
  // Compute SNR in dB
  // The coding rate is the ratio of input information bits, k, to the number of
  // rate-matched bits, e.
  double coding_rate = (double)k / e;
  int bits_per_symb = armral::simulation::bits_per_symbol(mod_type);
  double bw = 1e6; // Bandwidth (B) = 1 MHz
  // The symbol rate R [symbols/s] is proportional to the bandwidth. For
  // passband transmission using QAM modulation the maximum spectral efficiency
  // is equal to the number of bits per symbol. To meet this criteria we take
  // the symbol rate equal to the bandwidth.
  double symb_rate = bw;
  double snr_db = armral::simulation::ebn0_to_snr(coding_rate, bits_per_symb,
                                                  symb_rate, bw, ebn0_db);

  double tolerance = 1.0e-9;
  int nb = 0;
  uint64_t nr_total = 0;
  uint32_t num_message_errors = 0;
  while (nb < 10 && nr_total < 1e6) {
    uint64_t nr = 1e4;
#pragma omp parallel reduction(+ : nb, num_message_errors)
    {
      turbo_example_data data(num_blocks, k, mod_type, e, rv, perm_idxs);
#pragma omp for schedule(static)
      for (uint64_t r = 0; r < nr / num_blocks; ++r) {
        auto state = armral::utils::random_state::from_seeds({r, nr_total});
        turbo_error_counts results;
        run_check(&state, snr_db, ulp, iter_max, &data, &results);
        nb += results.num_bit_errors;
        num_message_errors += results.num_block_errors;
      }
    }
    // Account for cases where nr is not divisible by num_blocks
    nr_total += (nr / num_blocks) * num_blocks;
  }
  double message_error_rate =
      static_cast<double>(num_message_errors) / nr_total;

  // Write out data in JSON format
  std::cout << sim_result(k, e, mod_type, ulp, ebn0_db, snr_db, iter_max, nb,
                          num_message_errors, nr_total, num_blocks)
                   .to_str()
            << std::endl;

  return message_error_rate > tolerance;
}

} // anonymous namespace

int main(int argc, char **argv) {

  // Initialization
  uint32_t k = 0;
  uint32_t e = 0;
  uint32_t rv = 0;
  uint16_t ulp = 0;
  uint32_t iter_max = 10;
  armral_modulation_type mod_type = ARMRAL_MOD_256QAM;
  uint32_t num_blocks = 1;
  bool is_k_set = false;
  bool is_mod_set = false;
  bool is_e_set = false;
  bool print_usage = false;

  // Parse arguments
  int option;
  while ((option = getopt(argc, argv, "k:m:e:r:u:i:b:")) != -1) {
    switch (option) {
    case 'k':
      k = (uint32_t)atoi(optarg);
      is_k_set = true;
      break;
    case 'm':
      mod_type = (armral_modulation_type)atoi(optarg);
      is_mod_set = true;
      break;
    case 'e':
      e = (uint32_t)atoi(optarg);
      is_e_set = true;
      break;
    case 'r':
      rv = (uint32_t)atoi(optarg);
      break;
    case 'u':
      ulp = (uint16_t)atoi(optarg);
      break;
    case 'i':
      iter_max = (uint32_t)atoi(optarg);
      break;
    case 'b':
      num_blocks = (uint32_t)atoi(optarg);
      break;
    default:
      print_usage = true;
    }
  }

  // Check user input
  if (!is_k_set || !block_size_valid(k)) {
    std::cerr << "Number of bits to encode is not valid or not specified.\n"
              << "Must be one of:\n"
              << print_valid_block_size("\t") << std::endl;
    print_usage = true;
  }
  if (!is_mod_set || !armral::simulation::is_valid_mod_type(mod_type)) {
    std::cerr << "Modulation type is invalid or not specified.\n"
              << "Must be one of:\n"
              << armral::simulation::print_valid_mod_type(1) << std::endl;
    print_usage = true;
  }
  if (!is_e_set) {
    std::cerr << "Number of rate matched bits not specified.\n" << std::endl;
    print_usage = true;
  }
  if (!is_valid_rv(rv)) {
    std::cerr << "Redundancy version is invalid.\n"
              << "Must be one of:\n"
              << valid_rv_str() << std::endl;
    print_usage = true;
  }

  // Print usage if required, then exit and return failure.
  if (print_usage) {
    usage(argv[0]);
    exit(EXIT_FAILURE);
  }

  // Set auto tuned ulp values
  if (ulp == 0) {
    // A default ulp value is 128, which removes some low order bits when
    // demodulating. Care should be taken that this is not too large. In
    // particular, when this value exceeds half the distance between
    // neighboring symbols, it is no longer possible to tell which symbol has
    // been transmitted in the case where we have a noiseless channel. This may
    // cause the simulation to never decode in an error-free manner.
    ulp = 128;
  }

  // Setup permutation indices for Turbo encoding/decoding
  uint16_t *perm_idxs = SNEW(uint16_t, 355248 * 3); // sum of all valid ks * 3
  armral_turbo_perm_idx_init(perm_idxs);

  for (double snr = -2;
       run_snr(k, iter_max, mod_type, e, rv, ulp, snr, perm_idxs, num_blocks);
       snr += 0.5) {
  }
  free(perm_idxs);
  return 0;
}
