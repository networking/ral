#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import argparse
import subprocess
import hashlib
from statistics import median


def exec_and_check_cmd(cmd):
    result = subprocess.run(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    result.check_returncode()
    return result


def run_perf(args):
    cmd = ['perf', 'stat', '-x', ' ', '-e', 'cycles:u', *args]
    result = exec_and_check_cmd(cmd)
    cycles = int(result.stderr.split(b' ')[0])
    return cycles


def md5(exe_path):
    hash_md5 = hashlib.md5()
    with open(exe_path, "rb") as f:
        for chunk in iter(lambda: f.read(4096), b""):
            hash_md5.update(chunk)
    return hash_md5.hexdigest()


def run(exe_path, benchargs):
    reps = int(benchargs["reps"])
    args = [exe_path, *benchargs["args"], str(benchargs["reps"])]

    all_cycles = [run_perf(args) / float(reps) for _ in range(10)]
    median_cycles = median(all_cycles)
    j = {
        "name": benchargs["name"],
        "all_cycles": all_cycles,
        "median_cycles": median_cycles,
        "md5": md5(exe_path)
    }
    print(json.dumps(j))


def main():
    parser = argparse.ArgumentParser(
        description="Run specified benchmark through perf, and dump the result")
    parser.add_argument("exe_path", help="Path to executable file to run")
    parser.add_argument("case_json",
                        help=("Description of a benchmark case to run, in " +
                              "JSON format."))

    args = parser.parse_args()

    case_json = json.loads(args.case_json)
    case_json["args"] = case_json["args"].split()

    run(args.exe_path, case_json)


if __name__ == "__main__":
    main()
