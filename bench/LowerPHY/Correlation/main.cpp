/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_correlation_perf(uint32_t n, uint32_t num_reps) {
  printf("[CORRELATION] - number of samples = %u, number of iterations = %u\n",
         n, num_reps);

  const std::vector<armral_cmplx_int16_t> a(n);
  const std::vector<armral_cmplx_int16_t> b(n);
  std::vector<armral_cmplx_int16_t> c(1);

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_corr_coeff_i16(n, a.data(), b.data(), c.data());
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 3) {
    // nsamples - The number of complex samples in each vector
    // nreps    - The number of times to repeat the function
    fprintf(stderr, "usage: %s nsamples nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto num_samples = (uint32_t)atoi(argv[1]);
  auto num_reps = (uint32_t)atoi(argv[2]);

  run_correlation_perf(num_samples, num_reps);

  return EXIT_SUCCESS;
}
