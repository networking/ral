/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_fft_f32_perf(uint32_t n, armral_fft_direction_t dir,
                      uint32_t num_reps) {
  const char *dir_name = dir < 0 ? "forwards" : "backwards";
  printf("[FFT f32] - problem size = %u, direction = %s, number of iterations "
         "= %u\n",
         n, dir_name, num_reps);

  const std::vector<armral_cmplx_f32_t> x(n);
  std::vector<armral_cmplx_f32_t> y(n);

  armral_fft_plan_t *p = nullptr;
  armral_fft_create_plan_cf32(&p, n, dir);
  assert(p);
  const auto *x_ptr = x.data();
  auto *y_ptr = y.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_fft_execute_cf32(p, x_ptr, y_ptr);
  }

  armral_fft_destroy_plan_cf32(&p);
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // nsamples - The problem size
    // dir      - The direction
    // nreps    - The number of times to repeat the function
    fprintf(stderr, "usage: %s nsamples dir nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto num_samples = (uint32_t)atoi(argv[1]);
  auto dir = (armral_fft_direction_t)atoi(argv[2]);
  auto num_reps = (uint32_t)atoi(argv[3]);

  run_fft_f32_perf(num_samples, dir, num_reps);

  return EXIT_SUCCESS;
}
