#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_fft_cf32")

reps_base = 10000000
nArr = [2, 4, 8, 16, 23, 32, 47, 64, 128, 256,
        288, 512, 529, 1024, 1063, 1728, 2048, 3240]

j = {
    "exe_name": exe_name,
    "cases": []
}

for n in nArr:
    cases = [
        {
            "name": "fft_cf32_fwd_{}".format(n),
            "args": "{} -1".format(n),
            "reps": reps_base // n
        },
        {
            "name": "fft_cf32_bwk_{}".format(n),
            "args": "{} 1".format(n),
            "reps": reps_base // n
        }
    ]
    j["cases"].extend(cases)

print(json.dumps(j))
