/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_scrambling_perf(uint32_t len, uint32_t num_reps) {
  printf("[SCRAMBLING] - length = %u, number of iterations = %u\n", len,
         num_reps);

  uint32_t bytes = (len + 7) / 8;

  const std::vector<uint8_t> src(bytes);
  std::vector<uint8_t> seq(bytes);
  std::vector<uint8_t> dst(bytes);

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_scramble_code_block(src.data(), seq.data(), len, dst.data());
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 3) {
    // len   - The number of input bits
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s len nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  auto len = (uint32_t)atoi(argv[1]);
  auto num_reps = (uint32_t)atoi(argv[2]);

  run_scrambling_perf(len, num_reps);

  return EXIT_SUCCESS;
}
