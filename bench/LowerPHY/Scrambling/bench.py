#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_scrambling")

reps = 300000
lenArr = [31, 64, 95, 184, 192, 255, 512, 1152,
          2880, 3351, 4480, 5185, 5816, 6144]

j = {
    "exe_name": exe_name,
    "cases": []
}

for length in lenArr:
    case = {
        "name": "scrambling_{}".format(length),
        "args": str(length),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
