#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_arm_fir_filter_cs16_decimate_2")

reps = 3000
nSamples = 10240
nTapsArr = [4, 8, 16, 32]

j = {
    "exe_name": exe_name,
    "cases": []
}

for nTaps in nTapsArr:
    case = {
        "name": "fir_filter_i16_decimate2_{}_{}".format(nSamples, nTaps),
        "args": "{} {}".format(nSamples, nTaps),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
