/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

uint32_t iround_4(uint32_t x) {
  // Round up to the next multiple of 4
  return (x + 3) & ~0x3;
}

uint32_t iround_8(uint32_t x) {
  // Round up to the next multiple of 8
  return (x + 7) & ~0x7;
}

void run_fir_f32_decimate_2_perf(uint32_t num_samples, uint32_t num_taps,
                                 uint32_t num_reps) {
  printf("[FIR f32 DECIMATE_2] - number of samples = %u number of taps = %u, "
         "number of iterations = %u\n",
         num_samples, num_taps, num_reps);

  assert(num_samples % 8 == 0);
  auto input =
      std::vector<armral_cmplx_f32_t>(iround_8(num_samples + num_taps));
  auto coeffs = std::vector<armral_cmplx_f32_t>(iround_4(num_taps));
  auto output = std::vector<armral_cmplx_f32_t>(num_samples / 2);

  const auto *in_ptr = input.data();
  const auto *co_ptr = coeffs.data();
  auto *out_ptr = output.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_fir_filter_cf32_decimate_2(num_samples, num_taps, in_ptr, co_ptr,
                                      out_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // nsamples - The number of complex samples
    // ntaps    - The number of taps
    // nreps    - The number of times to repeat the function
    fprintf(stderr, "usage: %s nsamples ntaps nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto num_samples = (uint32_t)atoi(argv[1]);
  auto num_taps = (uint32_t)atoi(argv[2]);
  auto num_reps = (uint32_t)atoi(argv[3]);

  run_fir_f32_decimate_2_perf(num_samples, num_taps, num_reps);

  return EXIT_SUCCESS;
}
