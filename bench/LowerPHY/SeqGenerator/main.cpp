/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "int_utils.hpp"

static void run_sequence_generator_perf(uint32_t len, int num_reps) {
  uint32_t len_bytes = (((uint64_t)len) + 7) / 8;
  std::vector<uint8_t> dst(len_bytes);

  // the seed makes no algorithmic distance, so chosen arbitrarily.
  int seed = 5;

  for (int i = 0; i < num_reps; ++i) {
    armral_seq_generator(len, seed, dst.data());
  }
}

/**
  @brief         main
 */
int main(int argc, char **argv) {
  if (argc != 3) {
    fprintf(stderr, "usage: %s len nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  uint32_t len = atoi(argv[1]);
  int num_reps = atoi(argv[2]);

  run_sequence_generator_perf(len, num_reps);
}
