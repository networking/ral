#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_seq_generator")

reps = 300000
lenArr = [64, 65, 73, 81, 89, 128, 192, 512, 1024, 2048, 2057]

j = {
    "exe_name": exe_name,
    "cases": []
}

# PERF TESTS for SEQUENCE GENERATION, all lengths
for length in lenArr:
    case = {
        "name": "sequence_generator_{}".format(length),
        "args": str(length),
        "reps": 300000
    }
    j["cases"].append(case)

print(json.dumps(j))
