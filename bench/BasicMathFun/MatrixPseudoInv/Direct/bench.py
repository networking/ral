#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_matrix_pseudo_inv_direct")

reps = 300000

j = {
    "exe_name": exe_name,
    "cases": []
}

size1 = [1, 2, 3, 4, 8, 16]
size2 = [32, 64, 128, 256]

for (m, n) in itertools.chain(zip(size1, size2), zip(size2, size1)):
    case = {
        "name": "mat_pseudo_inv_direct_{}_{}".format(m, n),
        "args": "{} {}".format(m, n),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
