/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_mat_pinv_perf(uint32_t m, uint32_t n, uint32_t num_reps) {
  printf("[MATRIX PSEUDO INVERSE] - number of rows = %u, number of columns = "
         "%u, number of repetitions = %u\n",
         m, n, num_reps);

  const std::vector<armral_cmplx_f32_t> mat_in(m * n);
  std::vector<armral_cmplx_f32_t> mat_out(n * m);
  float32_t lambda = 1.0F;

  const auto *in_ptr = mat_in.data();
  auto *out_ptr = mat_out.data();

#ifdef ARMRAL_BENCH_NOALLOC
  // Benchmark only added for interest. This is not expected to show any major
  // performance difference.
  auto size = m > n ? n : m;
  std::vector<uint8_t> buffer(size * size * sizeof(armral_cmplx_f32_t) + 3);
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_pseudo_inverse_direct_f32_noalloc(m, n, lambda, in_ptr,
                                                   out_ptr, buffer.data());
  }
#else
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_pseudo_inverse_direct_f32(m, n, lambda, in_ptr, out_ptr);
  }
#endif
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // m        - The number of rows in the input matrix
    // n        - The number of columns in the input matrix
    // num_reps - The number of times to repeat the function
    fprintf(stderr, "usage: %s m n nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto m = (uint32_t)atoi(argv[1]);
  auto n = (uint32_t)atoi(argv[2]);
  auto num_reps = (uint32_t)atoi(argv[3]);

  [[maybe_unused]] auto size = m > n ? n : m;
  assert(size == 2 || size == 3 || size == 4 || size == 8 || size == 16);

  run_mat_pinv_perf(m, n, num_reps);

  return EXIT_SUCCESS;
}
