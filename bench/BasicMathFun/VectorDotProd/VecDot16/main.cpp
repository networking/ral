/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_vec_dot_i16_perf(uint32_t num_samples, uint32_t num_reps) {
  printf("[VECDOT i16] - number of samples = %u, number of iterations = %u\n",
         num_samples, num_reps);

  const std::vector<armral_cmplx_int16_t> a(num_samples);
  const std::vector<armral_cmplx_int16_t> b(num_samples);
  armral_cmplx_int16_t c;

  const auto *a_ptr = a.data();
  const auto *b_ptr = b.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_vecdot_i16(num_samples, a_ptr, b_ptr, &c);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 3) {
    // nsamples - The number of samples in each vector
    // nreps    - The number of times to repeat the function
    fprintf(stderr, "usage: %s nsamples nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto num_samples = (uint32_t)atoi(argv[1]);
  auto num_reps = (uint32_t)atoi(argv[2]);

  run_vec_dot_i16_perf(num_samples, num_reps);

  return EXIT_SUCCESS;
}
