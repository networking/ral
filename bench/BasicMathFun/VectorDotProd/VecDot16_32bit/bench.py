#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_vec_dot_16_32_bit")

reps = 300000
lenArr = [32, 64, 128, 256, 512, 1024]

j = {
    "exe_name": exe_name,
    "cases": []
}

for length in lenArr:
    case = {
        "name": "dot_product_i16_32b_{}".format(length),
        "args": str(length),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
