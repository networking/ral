#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_matrix_inv_single_general")

reps = 300000
sizes = [2, 3, 4, 8, 16]

j = {
    "exe_name": exe_name,
    "cases": []
}

for size in sizes:
    case = {
        "name": "gen_matinv_{}_one".format(size),
        "args": "{}".format(size),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
