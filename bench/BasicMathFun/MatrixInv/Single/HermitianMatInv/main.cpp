/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "matrix_utils.hpp"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_hermitian_matinv_perf(uint32_t dim, uint32_t num_reps) {
  printf("[Hermitian MATINV] - dimension = %u,  number of "
         "iterations = %u\n",
         dim, num_reps);

  const auto a =
      armral::utils::gen_hermitian_matrix(dim, false, 1.0, 1.0, true);
  auto res = std::vector<armral_cmplx_f32_t>(dim * dim);

  const auto *a_ptr = a.data();
  auto *res_ptr = res.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_hermitian_mat_inverse_f32(dim, a_ptr, res_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 3) {
    // dim   - The size of the input matrix
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s dim nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto dim = (uint32_t)atoi(argv[1]);
  auto num_reps = (uint32_t)atoi(argv[2]);

  assert(dim == 2 || dim == 3 || dim == 4 || dim == 8 || dim == 16);

  run_hermitian_matinv_perf(dim, num_reps);

  return EXIT_SUCCESS;
}
