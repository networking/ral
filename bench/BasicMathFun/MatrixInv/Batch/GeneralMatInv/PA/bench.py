#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_matrix_inv_batch_general_pa")

reps = 300000
sizes = [
    (2, 2),
    (20, 2),
    (200, 2),
    (4, 3),
    (40, 3),
    (400, 3),
    (4, 4),
    (40, 4),
    (400, 4),
]

j = {
    "exe_name": exe_name,
    "cases": []
}

for num_mats, size in sizes:
    case = {
        "name": "gen_matinv_{}_batch_pa_{}".format(size, num_mats),
        "args": "{} {}".format(num_mats, size),
        "reps": (reps * size) // num_mats
    }
    j["cases"].append(case)

print(json.dumps(j))
