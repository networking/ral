/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "matrix_utils.hpp"

#include <cstdio>
#include <vector>

namespace {

void run_general_batch_matinv_perf(uint32_t num_mats, uint32_t dim,
                                   uint32_t num_reps) {
  printf("[BATCH GENERAL MATINV] - dimension = %u, number of "
         "iterations = "
         "%u\n",
         dim, num_reps);

  const auto a = armral::utils::gen_invertible_matrix_batch(num_mats, dim);
  auto res = std::vector<armral_cmplx_f32_t>(num_mats * dim * dim);

  const auto *a_ptr = a.data();
  auto *res_ptr = res.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_mat_inverse_batch_f32(num_mats, dim, a_ptr, res_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // num_mats - The number of input and output matrices
    // dim      - The size of each matrix
    // nreps    - The number of times to repeat the function
    fprintf(stderr, "usage: %s num_mats dim nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto num_mats = (uint32_t)atoi(argv[1]);
  auto dim = (uint32_t)atoi(argv[2]);
  auto num_reps = (uint32_t)atoi(argv[3]);

  if (dim != 2 && dim != 3 && dim != 4) {
    fprintf(stderr, "unsupported matrix dimension: %u", dim);
    exit(EXIT_FAILURE);
  }

  if (dim >= 3 && num_mats % 4 != 0) {
    fprintf(stderr,
            "unsupported batch size for matrix dimension >= 3: %u %% 4 != 0",
            num_mats);
    exit(EXIT_FAILURE);
  }

  if (dim == 2 && num_mats % 2 != 0) {
    fprintf(stderr,
            "unsupported batch size for matrix dimension = 2: %u %% 2 != 0",
            num_mats);
    exit(EXIT_FAILURE);
  }

  run_general_batch_matinv_perf(num_mats, dim, num_reps);

  return EXIT_SUCCESS;
}
