/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_vec_mul_i16_2_perf(uint32_t num_samples, uint32_t num_reps) {
  printf("[VECMUL i16_2] - number of samples = %u, number of iterations = %u\n",
         num_samples, num_reps);

  const std::vector<int16_t> a_re(num_samples);
  const std::vector<int16_t> a_im(num_samples);
  const std::vector<int16_t> b_re(num_samples);
  const std::vector<int16_t> b_im(num_samples);
  std::vector<int16_t> c_re(num_samples);
  std::vector<int16_t> c_im(num_samples);

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_vecmul_i16_2(num_samples, a_re.data(), a_im.data(),
                              b_re.data(), b_im.data(), c_re.data(),
                              c_im.data());
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 3) {
    // nsamples - The number of samples in each vector
    // nreps    - The number of times to repeat the function
    fprintf(stderr, "usage: %s nsamples nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  uint32_t num_samples = (uint32_t)atoi(argv[1]);
  uint32_t num_reps = (uint32_t)atoi(argv[2]);

  run_vec_mul_i16_2_perf(num_samples, num_reps);

  return EXIT_SUCCESS;
}
