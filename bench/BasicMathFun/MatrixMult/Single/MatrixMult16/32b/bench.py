#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_matrix_mult_i16_32b")

full_reps = 300000
lenArr = [2, 4, 8, 16, 64, 128]

j = {
    "exe_name": exe_name,
    "cases": []
}

for length in lenArr:
    combined_size = length * 3
    reps = full_reps // combined_size if combined_size > 190 else full_reps

    case = {
        "name": "matmul_i16_32b_{}".format(length),
        "args": "{}".format(length),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
