/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_matmul_i16_32b_perf(uint32_t dim, uint32_t num_reps) {
  printf("[MATMUL i16 32BIT] - dimension = %u, number of iterations "
         "= %u\n",
         dim, num_reps);

  const std::vector<armral_cmplx_int16_t> a(dim * dim);
  const std::vector<armral_cmplx_int16_t> b(dim * dim);
  std::vector<armral_cmplx_int16_t> c(dim * dim);

  const auto *a_ptr = a.data();
  const auto *b_ptr = b.data();
  auto *c_ptr = c.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_matmul_i16_32bit(dim, dim, dim, a_ptr, b_ptr, c_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 3) {
    // dim   - The number of rows and columns in the input matrices
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s dim nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto dim = (uint32_t)atoi(argv[1]);
  auto num_reps = (uint32_t)atoi(argv[2]);

  run_matmul_i16_32b_perf(dim, num_reps);

  return EXIT_SUCCESS;
}
