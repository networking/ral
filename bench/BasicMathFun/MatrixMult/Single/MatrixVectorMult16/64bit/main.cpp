/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_mat_vec_mult_i16_64b_perf(const uint32_t m, const uint32_t n,
                                   const uint32_t num_reps) {
  printf("[MATVECMUL armral_cmplx_int16_t 64b]  number of rows = %u,  number "
         "of columns = %u, number of iterations = %u\n",
         m, n, num_reps);

  const std::vector<armral_cmplx_int16_t> a(m * n);
  const std::vector<armral_cmplx_int16_t> x(n);
  std::vector<armral_cmplx_int16_t> y(m);

  const auto *a_ptr = a.data();
  const auto *x_ptr = x.data();
  auto *y_ptr = y.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_mat_vec_mult_i16(m, n, a_ptr, x_ptr, y_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // m        - The number of rows in the matrix, and the length of the output
    //            vector
    // n        - The number of columns in the matrix, and the length of
    //            the input vector
    // num_reps - The number of times to repeat the function
    fprintf(stderr, "usage: %s m n nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  const uint32_t m = atoi(argv[1]);
  const uint32_t n = atoi(argv[2]);
  const uint32_t num_reps = atoi(argv[3]);

  run_mat_vec_mult_i16_64b_perf(m, n, num_reps);
}
