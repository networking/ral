#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_matrix_vector_mult_i16_32b")

j = {
    "exe_name": exe_name,
    "cases": []
}

reps = 300000
sizeArr = [1, 2, 4, 8, 16]

for (m, n) in itertools.product(sizeArr, sizeArr):
    case = {
        "name": "matvecmul_i16_32b_{}_{}".format(m, n),
        "args": "{} {}".format(m, n),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
