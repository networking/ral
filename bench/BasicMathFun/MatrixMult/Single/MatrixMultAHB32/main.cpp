/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <string>
#include <vector>

namespace {

void run_matmul_ahb_cf32_perf(uint16_t m, uint16_t n, uint16_t k,
                              int num_reps) {
  printf("[MATMUL AHB] - dimensions (%u-by-%u)^H x "
         "%u-by-%u, number of iterations = %d\n",
         k, m, k, n, num_reps);

  const std::vector<armral_cmplx_f32_t> a(k * m);
  const std::vector<armral_cmplx_f32_t> b(k * n);
  std::vector<armral_cmplx_f32_t> c(m * n);

  const auto *a_ptr = a.data();
  const auto *b_ptr = b.data();
  auto *c_ptr = c.data();

  for (int i = 0; i < num_reps; ++i) {
    armral_cmplx_matmul_ahb_f32(m, n, k, a_ptr, b_ptr, c_ptr);
  }
}

} // anonymous namespace

int main(int argc, const char *argv[]) {
  if (argc != 5) {
    // m     - The number of columns in A and rows in C
    // n     - The number of columns in B and C
    // k     - The number of rows in A and B
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s m n k nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto m = std::stoi(argv[1]);
  auto n = std::stoi(argv[2]);
  auto k = std::stoi(argv[3]);
  auto num_reps = std::stoi(argv[4]);

  run_matmul_ahb_cf32_perf(m, n, k, num_reps);

  return EXIT_SUCCESS;
}
