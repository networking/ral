#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_matrix_mult_ahb_32")

j = {
    "exe_name": exe_name,
    "cases": []
}

reps = 300000

# The A^H * B function is being added for use in computing the pseudo inverse.
# That means we know B will be a square matrix (and due to using the matrix
# inverse code) will have a size of 2x2, 3x3, 4x4, 8x8, or 16x16. So benchmark
# those cases:
mArr32_256 = [32, 64, 128, 256]
nkArrInverseSizes = [2, 3, 4, 8, 16]

for (m, nk) in itertools.product(mArr32_256, nkArrInverseSizes):
    case = {
        "name": f"matmulahb_f32_{m}_{nk}_{nk}",
        "args": f"{m} {nk} {nk}",
        "reps": reps
    }
    j["cases"].append(case)


# Try a (smaller) range of non-square B matrices too:
mArr32_64 = [32, 64]
nkArr2_16 = [2, 4, 16]

for (m, n, k) in itertools.product(mArr32_64, nkArr2_16, nkArr2_16):
    if n != k:
        case = {
            "name": f"matmulahb_f32_{m}_{n}_{k}",
            "args": f"{m} {n} {k}",
            "reps": reps
        }
        j["cases"].append(case)

print(json.dumps(j))
