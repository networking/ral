/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cf32_utils.hpp"

#include <cstdio>

static void run_general_matvecmul_perf(int m, int n, int num_reps) {
  assert(m > 0);
  assert(n > 0);
  assert(num_reps > 0);
  const auto a = std::vector<armral_cmplx_f32_t>(m * n);
  const auto x = std::vector<armral_cmplx_f32_t>(n);
  auto y = std::vector<armral_cmplx_f32_t>(m);

  printf("[MATVECMUL armral_cmplx_f32_t] - m %d - n %d - "
         "number of iterations = %d\n",
         m, n, num_reps);

  const auto *a_ptr = a.data();
  const auto *x_ptr = x.data();
  auto *y_ptr = y.data();

  for (int i = 0; i < num_reps; ++i) {
    armral_cmplx_mat_vec_mult_f32(m, n, a_ptr, x_ptr, y_ptr);
  }
}

int main(int argc, char **argv) {
  if (argc != 4) {
    fprintf(stderr, "usage: %s m n nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto m = atoi(argv[1]);
  auto n = atoi(argv[2]);
  auto num_reps = atoi(argv[3]);

  run_general_matvecmul_perf(m, n, num_reps);
}
