#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_matmul_f32_general")

j = {
    "exe_name": exe_name,
    "cases": []
}

full_reps = 300000
lenArr = [2, 4, 8, 16, 128]
mArr = [4, 8]
nArr = [2, 3, 4]
kArr = [32, 64, 128, 256]

for (m, n, k) in itertools.chain(zip(lenArr, lenArr, lenArr), itertools.product(mArr, nArr, kArr)):
    combined_size = m + n + k
    reps = full_reps // combined_size if combined_size > 300 else full_reps
    case = {
        "name": "matmul_f32_{}_{}_{}".format(m, n, k),
        "args": "{} {} {}".format(m, n, k),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
