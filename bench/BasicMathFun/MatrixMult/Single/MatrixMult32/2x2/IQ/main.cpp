/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cf32_utils.hpp"

namespace {

void run_matmul_f32_2x2_iq_perf(uint32_t num_reps) {
  printf("[MATMUL f32 2x2 IQ] - number of iterations = %u\n", num_reps);

  const auto a = std::vector<armral_cmplx_f32_t>(4);
  const auto b = std::vector<armral_cmplx_f32_t>(4);
  const auto c = std::vector<armral_cmplx_f32_t>(4);
  const auto a_re = armral::utils::unpack_real_cf32(a);
  const auto a_im = armral::utils::unpack_imag_cf32(a);
  const auto b_re = armral::utils::unpack_real_cf32(b);
  const auto b_im = armral::utils::unpack_imag_cf32(b);
  auto c_re = armral::utils::unpack_real_cf32(c);
  auto c_im = armral::utils::unpack_imag_cf32(c);

  const auto *a_re_ptr = a_re.data();
  const auto *a_im_ptr = a_im.data();
  const auto *b_re_ptr = b_re.data();
  const auto *b_im_ptr = b_im.data();
  auto *c_re_ptr = c_re.data();
  auto *c_im_ptr = c_im.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_mat_mult_2x2_f32_iq(a_re_ptr, a_im_ptr, b_re_ptr, b_im_ptr,
                                     c_re_ptr, c_im_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 2) {
    fprintf(stderr, "usage: %s nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  auto num_reps = (uint32_t)atoi(argv[1]);

  run_matmul_f32_2x2_iq_perf(num_reps);

  return EXIT_SUCCESS;
}
