/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_specific_2x2_matmul_perf(uint32_t num_reps) {
  printf("[MATMUL f32 2x2] - number of iterations = %u\n", num_reps);

  const auto a = std::vector<armral_cmplx_f32_t>(4);
  const auto b = std::vector<armral_cmplx_f32_t>(4);
  auto c = std::vector<armral_cmplx_f32_t>(4);

  const auto *a_ptr = a.data();
  const auto *b_ptr = b.data();
  auto *c_ptr = c.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_mat_mult_2x2_f32(a_ptr, b_ptr, c_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 2) {
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  auto num_reps = (uint32_t)atoi(argv[1]);

  run_specific_2x2_matmul_perf(num_reps);

  return EXIT_SUCCESS;
}
