#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_matrix_mult_f32_4x4_iq")

j = {
    "exe_name": exe_name,
    "cases": []
}

reps = 300000

j["cases"].append({
        "name": "matmul_f32_specific_4_iq",
        "args": "",
        "reps": reps
})

print(json.dumps(j))
