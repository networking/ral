#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_matrix_mult_aah_32")

j = {
    "exe_name": exe_name,
    "cases": []
}

reps = 300000
mArr = [2, 3, 4, 8, 16]
nArr = [32, 64, 128, 256]

for m, n in itertools.chain(zip(mArr, mArr), itertools.product(mArr, nArr)):
    case = {
        "name": "matmul_aah_f32_{}_{}".format(m, n),
        "args": "{} {}".format(m, n),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
