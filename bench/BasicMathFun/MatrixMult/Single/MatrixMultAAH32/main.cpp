/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_matmul_aah_perf(uint32_t dim1, uint32_t dim2, uint32_t num_reps) {
  printf("[MATMUL AAH] - dimensions = %u-by-%u, "
         "number of iterations = "
         "%u\n",
         dim1, dim2, num_reps);

  const auto a = std::vector<armral_cmplx_f32_t>(dim1 * dim2);
  auto c = std::vector<armral_cmplx_f32_t>(dim1 * dim1);

  const auto *a_ptr = a.data();
  auto *c_ptr = c.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_matmul_aah_f32(dim1, dim2, a_ptr, c_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // dim1  - The number of rows in the input matrix
    // dim2  - The number of columns in the input matrix
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s dim1 dim2 nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto dim1 = (uint32_t)atoi(argv[1]);
  auto dim2 = (uint32_t)atoi(argv[2]);
  auto num_reps = (uint32_t)atoi(argv[3]);

  run_matmul_aah_perf(dim1, dim2, num_reps);

  return EXIT_SUCCESS;
}
