#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import itertools
import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_matrix_vector_mult_batch_f32_pa")

reps = 300000

matArr = [48]
vecArr = [1, 12, 24]
mArr = [1, 2, 4, 8]
nArr = [1, 2, 4, 8]

j = {
    "exe_name": exe_name,
    "cases": []
}

for mat, vec, m, n in itertools.product(matArr, vecArr, mArr, nArr):
    case = {
         "name": "matvecmul_batch_pa_f32_{}_{}_{}_{}".format(mat, vec, m, n),
         "args": "{} {} {} {}".format(mat, vec, m, n),
         "reps": reps // (mat * vec)
    }
    j["cases"].append(case)

print(json.dumps(j))
