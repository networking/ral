/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_matvecmul_batch_i16_32b_pa_perf(uint32_t num_mats,
                                         uint32_t vecs_per_mat, uint32_t m,
                                         uint32_t n, uint32_t num_reps) {
  printf("[MATVECMUL BATCH i16 32BIT PA] - number of input matrices = %u, "
         "vectors per matrix = %u, number of rows = %u, number of columns = "
         "%u, number of iterations = %u\n",
         num_mats, vecs_per_mat, m, n, num_reps);

  assert(num_mats > 0);
  assert(vecs_per_mat > 0);
  assert(m > 0);
  assert(n > 0);
  assert(num_reps > 0);

  auto total_vectors = num_mats * vecs_per_mat;

  const std::vector<armral_cmplx_int16_t> a(num_mats * m * n);
  const std::vector<armral_cmplx_int16_t> x(total_vectors * n);
  std::vector<armral_cmplx_int16_t> y(total_vectors * m);

  std::vector<const armral_cmplx_int16_t *> a_ptrs(m * n);
  std::vector<const armral_cmplx_int16_t *> x_ptrs(n);
  std::vector<armral_cmplx_int16_t *> y_ptrs(m);
  for (uint32_t i = 0; i < m * n; ++i) {
    a_ptrs[i] = &a[num_mats * i];
  }
  for (uint32_t i = 0; i < n; i++) {
    x_ptrs[i] = &x[total_vectors * i];
  }
  for (uint32_t i = 0; i < m; i++) {
    y_ptrs[i] = &y[total_vectors * i];
  }

  auto *a_ptrs_data = a_ptrs.data();
  auto *x_ptrs_data = x_ptrs.data();
  auto *y_ptrs_data = y_ptrs.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_cmplx_mat_vec_mult_batch_i16_32bit_pa(
        num_mats, vecs_per_mat, m, n, a_ptrs_data, x_ptrs_data, y_ptrs_data);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 6) {
    // num_mats     - The number of input matrices
    // vecs_per_mat - The number of input and output vectors for each
    //                input matrix
    // m            - The number of rows in each matrix and the length of
    //                each output vector
    // n            - The number of columns in each matrix and the length of
    //                each input vector
    // nreps        - The number of times to repeat the function
    fprintf(stderr, "usage: %s num_mats vecs_per_mat m n nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto num_mats = (uint32_t)atoi(argv[1]);
  auto vecs_per_mat = (uint32_t)atoi(argv[2]);
  auto m = (uint32_t)atoi(argv[3]);
  auto n = (uint32_t)atoi(argv[4]);
  auto num_reps = (uint32_t)atoi(argv[5]);

  run_matvecmul_batch_i16_32b_pa_perf(num_mats, vecs_per_mat, m, n, num_reps);

  return EXIT_SUCCESS;
}
