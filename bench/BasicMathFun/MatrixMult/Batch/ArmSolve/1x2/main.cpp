/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"

#include <vector>

namespace {

void run_solve_1x2_perf(const uint32_t sc_per_g, const uint32_t num_samples,
                        const uint32_t num_reps) {
  assert(num_samples % 12 == 0);
  printf("[SOLVE 1x2] subcarriers per G matrix = %u number of subcarriers = %u "
         "size of G matrix = 1-by-2, number of iterations = %u\n",
         sc_per_g, num_samples, num_reps);

  constexpr uint32_t x_rows = 1;
  constexpr uint32_t y_rows = 2;

  const armral_fixed_point_index num_fract_bits_x =
      ARMRAL_FIXED_POINT_INDEX_Q15;
  const std::vector<armral_fixed_point_index> num_fract_bits_y(
      y_rows, ARMRAL_FIXED_POINT_INDEX_Q15);

  std::vector<armral_cmplx_int16_t> x(num_samples * x_rows);
  const std::vector<armral_cmplx_int16_t> y(num_samples * y_rows);
  const uint32_t xy_stride = num_samples * 2;

  const uint32_t num_blocks = num_samples / sc_per_g;
  const std::vector<float32_t> g_real(x_rows * y_rows * num_blocks);
  const std::vector<float32_t> g_imag(x_rows * y_rows * num_blocks);
  const uint32_t g_stride = num_blocks;

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_solve_1x2_f32(num_samples, sc_per_g, y.data(), xy_stride,
                         num_fract_bits_y.data(), g_real.data(), g_imag.data(),
                         g_stride, x.data(), num_fract_bits_x);
  }
}

} // namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // sc_per_g - The number of subcarriers per G matrix
    // len      - The number of subcarriers
    // num_reps - The number of times to repeat the function
    fprintf(stderr, "usage: %s sc_per_g len nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  const uint32_t sc_per_g = atoi(argv[1]);
  const uint32_t num_samples = atoi(argv[2]);
  const uint32_t num_reps = atoi(argv[3]);

  run_solve_1x2_perf(sc_per_g, num_samples, num_reps);
}
