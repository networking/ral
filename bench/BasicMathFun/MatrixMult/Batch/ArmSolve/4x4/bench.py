#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_arm_solve_4x4")

reps = 300000
scPerG = [1, 4, 6]
lenArr = [12, 60, 96]

j = {
    "exe_name": exe_name,
    "cases": []
}

for (s, l) in itertools.product(scPerG, lenArr):
    case = {
        "name": "solve_{}sc_4x4_{}".format(s, l),
        "args": "{} {}".format(s, l),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
