#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause
import collections
import os
import subprocess

ShellResult = collections.namedtuple("ShellResult", "returncode, stdout, stderr")

SECONDS_TO_MICROS = 1000000
ARMRAL_EXPECTED_TIMEOUT_RETCODE = 3
ARMRAL_ALLOW_ERROR_RETCODE = 4


def shell(cmd, check=True, **kwargs):
    """
    Run cmd on the command line and return stdout. Throws an exception
    if the return code is non-zero. Remaining kwargs are passed on to
    subprocess.run().
    """
    result = subprocess.run(cmd, stdout=subprocess.PIPE,
                            stderr=subprocess.PIPE, check=check,
                            **kwargs)
    return ShellResult(result.returncode,
                       result.stdout.decode("utf-8"),
                       result.stderr.decode("utf-8"))


def assert_file_exists(filepath):
    assert os.path.isfile(filepath), "File {} does not exist".format(filepath)


def file_exists(filepath):
    return os.path.isfile(filepath)


def assert_file_is_executable(filepath):
    assert os.access(
        filepath, os.X_OK), "File {} is not executable".format(filepath)


def file_is_executable(filepath):
    return os.access(filepath, os.X_OK)


def get_files_recursively(path, name):
    if os.path.isfile(path):
        return [path]
    return [os.path.abspath(os.path.join(dirpath, fname))
            for dirpath, _, fnames in os.walk(path)
            for fname in fnames if fname == name]
