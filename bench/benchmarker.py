#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause


# This program is for benchmarking the performance of armral functions.
# Files named bench.py in all directories in and under the [source dir] cli arg will be executed.
# bench.py files are expected to write parsable json to stdout as follows:
#       {
#           "exe_name":"benchmark/name",
#           "cases":[
#                {
#                    "name":"benchmark_name",
#                    "args": "1 2 3",
#                    "reps": 1000
#                },
#                { ... }
#           ],
#       }
#
# A runner is a python class which formats the exe_name, args and reps to run a benchmark.
# A runner allows changing what bash code to wrap around the benchmark, how to run the benchmark
# and how to format the output results.

import argparse
import benchmarker_utils as utils
import collections
import json
import multiprocessing
import os
import signal
import sys
import time

START_TIME = time.time()

Case = collections.namedtuple("Case", "runner, exe_path, case_data")
CaseResult = collections.namedtuple("CaseResult", "returncode, cmd, stdout, stderr, begin, end, name")


def decode_if_bytes(in_str):
    if hasattr(in_str, "decode"):
        return in_str.decode("utf-8")
    else:
        return in_str


def display_result(res):
    if res.returncode == utils.ARMRAL_EXPECTED_TIMEOUT_RETCODE:
        print("Did not run command due to expected timeout: {}".format(res.cmd), file=sys.stderr, flush=True)
        return 0
    elif res.returncode != 0:
        # Report error, and only fail the script if we don't want to
        # allow an error in a benchmark

        # Check if we have a bytes-like object for the error, and convert to
        # string. If we don't, newlines get printed as plain text.
        print("Failed to run command: {}\n{}".format(
            res.cmd, decode_if_bytes(res.stderr)), file=sys.stderr, flush=True)
        # Despite reporting an error, we still want to print out the runtime
        # for this case
        j = {
            "name": res.name,
            "error": True,
            "ts": int(res.begin * utils.SECONDS_TO_MICROS),
            "dur": int((res.end - res.begin) * utils.SECONDS_TO_MICROS),
        }
        print(json.dumps(j), flush=True)
        return (0 if
                res.returncode == utils.ARMRAL_ALLOW_ERROR_RETCODE
                else res.returncode)

    # add begin/duration times in microseconds into existing json structure
    try:
        j = json.loads(res.stdout)
    except Exception:
        print("Exception encountered after running command: {}\n"
              "Error trying to load json from string: '{}'"
              .format(res.cmd, decode_if_bytes(res.stdout)),
              file=sys.stderr, flush=True)
        raise
    j['ts'] = int(res.begin * utils.SECONDS_TO_MICROS)
    j['dur'] = int((res.end - res.begin) * utils.SECONDS_TO_MICROS)
    print(json.dumps(j), flush=True)
    return 0


def run_case(c):
    cmd = [c.runner, c.exe_path, json.dumps(c.case_data)]
    begin = time.time() - START_TIME
    returncode, stdout, stderr = utils.shell(cmd, check=False)
    end = time.time() - START_TIME
    return CaseResult(returncode, cmd, stdout, stderr, begin, end, c.case_data["name"])


def get_cases_from_bench_file(fname, build_dir):
    utils.assert_file_exists(fname)
    utils.assert_file_is_executable(fname)
    try:
        data = json.loads(utils.shell(fname).stdout)
    except json.JSONDecodeError:
        # default exception doesn't say which file failed to parse
        path = os.path.join(build_dir, fname)
        print("Failed to parse json output from '{}'".format(path))
        raise
    assert "exe_name" in data, "expected benchmark object to contain 'exe_name' key, benchmark was loaded from '{}', object was:\n{}".format(
        fname, data)
    assert "cases" in data, "expected benchmark object to contain 'cases' key, benchmark was loaded from '{}', object was:\n{}".format(
        fname, data)
    data["filepath"] = fname
    exe_path = os.path.join(os.path.abspath(build_dir), data["exe_name"])
    # If the executable doesn't exist, we don't try and run it
    if not utils.file_exists(exe_path) or not utils.file_is_executable(exe_path):
        print("WARNING: Executable {} not found - skipping benchmarks".format(
            data["exe_name"]), file=sys.stderr)
        return []
    return [data]


# Execute all bench.py files under and including dir. Return a list of all benchmark batches.
def get_benchmarks_data(path, build_dir):
    bench_script_paths = utils.get_files_recursively(path, "bench.py")
    benchmarks = []
    for path in bench_script_paths:
        benchmarks.extend(get_cases_from_bench_file(path, build_dir))
    return benchmarks


def build_cases_from_script_json(benchmarks, cliargs):
    # The json generated from the bench.py scripts in the
    # benchmark directories is per suite, and so the exe name
    # to run is stored once for all the cases to run.
    cases = []
    for benchmark in benchmarks:
        exe_path = os.path.join(os.path.abspath(
            cliargs.build_dir), benchmark["exe_name"])
        for case in benchmark["cases"]:
            cases.append(Case(cliargs.runner, exe_path, case))
    return cases


def build_cases_from_file_json(benchmarks, cliargs):
    # The json passed in from file requires that the exe name
    # to run is in each JSON record. This is because we don't
    # require the benchmarks to be grouped by executable.
    return [Case(cliargs.runner, case["exe_path"], case) for case in benchmarks]


def build_cases(benchmarks, cliargs):
    return (build_cases_from_script_json(benchmarks, cliargs)
            if cliargs.bench_cases is None else
            build_cases_from_file_json(benchmarks, cliargs))


def run_benchmarks(cases):
    return max(map(display_result, map(run_case, cases)))


def ignore_sigint():
    # By default SIGINT is sent to all child processes as well as the parent,
    # which just leads to Python writing a lot of useless stack traces to the
    # console. The child processes will clean up when the Pool is destroyed
    # anyway, so just ignore interrupts in them.
    signal.signal(signal.SIGINT, signal.SIG_IGN)


# Create one process for each benchmark case and run them concurrently
def run_benchmarks_concurrent(cases):
    num_procs = multiprocessing.cpu_count()
    with multiprocessing.Pool(processes=num_procs, initializer=ignore_sigint, maxtasksperchild=1) as pool:
        # serialize display_result rather than using imap to avoid racing prints.
        return max(map(display_result, pool.imap(run_case, cases)))


# Find all benchmarks recursively from a source directory
def get_benchmarks(source, build_dir):
    if source == '-':
        return json.loads(sys.stdin.read())
    return get_benchmarks_data(source, build_dir)


def main():
    parser = argparse.ArgumentParser(description="Run ArmRAL benchmarks")
    parser.add_argument("source", nargs="+", help=(
        "Path to either a directory or file. Given a file, executes this to "
        "obtain a JSON object describing benchmarks to run. Given a "
        "directory, recursively searches for bench.py files to execute to "
        "obtain JSON objects describing benchmarks to run. Given '-', a JSON "
        "object will be read from stdin."
    ))
    parser.add_argument(
        "build_dir", help="Used to build absolute paths to the bench exes")
    parser.add_argument("--runner", required=True,
                        help="Path to python script to use to run benchmarks")
    parser.add_argument("--concurrent", help="Run benchmarks concurrently",
                        default=False, action="store_true")
    parser.add_argument(
        "--bench-cases",
        help="Name of a file containing a JSON list of benchmark cases to "
             "run. If this is passed, we ignore the source directory. The "
             "JSON records must contain the following fields:\n"
             "  name, args, reps, exe_path"
    )

    cliargs = parser.parse_args()
    if cliargs.bench_cases is not None:
        with open(cliargs.bench_cases) as f:
            benchmarks = json.load(f)
    else:
        benchmarks = [
            bench
            for path in cliargs.source
            for bench in get_benchmarks(path, cliargs.build_dir)
        ]

    cases = build_cases(benchmarks, cliargs)
    if len(cases) == 0:
        print("Error: no cases to run")
        return 1

    print("Have {} benchmarks to run".format(len(cases)), file=sys.stderr, flush=True)
    exec_method = run_benchmarks_concurrent if cliargs.concurrent else run_benchmarks
    return exec_method(cases)


if __name__ == '__main__':
    try:
        sys.exit(main())
    except KeyboardInterrupt:
        print("\nInterrupted...")
        sys.exit(1)
