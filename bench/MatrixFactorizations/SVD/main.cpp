/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {
void bench_svd(bool gen_singular_vectors, int m, int n, int nreps) {
  int size = m * n;

  // The input matrix is initialized with ones
  // except the diagonals to avoid generating
  // a singular matrix
  std::vector<armral_cmplx_f32_t> a(size, {1.0F, 1.0F});
  const int lda = n;
  for (int i = 0; i < n; ++i) {
    a[i + lda * i] = armral_cmplx_f32_t{static_cast<float32_t>(i + 2), 0};
  }
  std::vector<float32_t> s(n);

  // Left and right singular vectors.
  std::vector<armral_cmplx_f32_t> u;
  std::vector<armral_cmplx_f32_t> vt;
  if (gen_singular_vectors) {
    u.resize(size);
    vt.resize(n * n);
  }

  // SVD decomposition.
#ifdef ARMRAL_BENCH_NOALLOC
  auto buffer_size =
      armral_svd_cf32_noalloc_buffer_size(gen_singular_vectors, m, n);
  std::vector<uint8_t> buffer(buffer_size);
  for (int i = 0; i < nreps; ++i) {
    armral_svd_cf32_noalloc(gen_singular_vectors, m, n, a.data(), s.data(),
                            u.data(), vt.data(), buffer.data());
  }
#else
  for (int i = 0; i < nreps; ++i) {
    armral_svd_cf32(gen_singular_vectors, m, n, a.data(), s.data(), u.data(),
                    vt.data());
  }
#endif
}

void svd_bench_print_usage(const char *exe_name) {
  printf("Usage: %s nrows ncols full_svd nreps\n", exe_name);
  printf(
      "\twhere nrows >= ncols, and full_svd is an indicator (0 or not zero)\n"
      "\tof whether or not to generate singular vectors\n");
}
} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 5) {
    svd_bench_print_usage(argv[0]);
    exit(EXIT_FAILURE);
  }
  int nrows = static_cast<int>(atoi(argv[1]));
  int ncols = static_cast<int>(atoi(argv[2]));
  bool full_svd = atoi(argv[3]) != 0;
  int nreps = atoi(argv[4]);
  if (nrows < ncols) {
    svd_bench_print_usage(argv[0]);
    exit(EXIT_FAILURE);
  }
  bench_svd(full_svd, nrows, ncols, nreps);
  exit(EXIT_SUCCESS);
}
