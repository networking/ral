#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_svd")

j = {
    "exe_name": exe_name,
    "cases": []
}

m_arr = [32]
n_arr = [4]

full_svd_arr = [1, 0]
base_reps = 1e5

for m, n, full_svd in itertools.product(m_arr, n_arr, full_svd_arr):
    reps = base_reps // (m * m)
    case = {
        "name": "svd_{}_{}_{}".format(
            m, n, "vec" if (full_svd == 1) else "novec"),
        "args": "{} {} {}".format(m, n, full_svd),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
