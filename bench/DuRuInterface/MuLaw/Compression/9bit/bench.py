#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_mu_law_compression_9bit")

reps = 500000
prbArr = [1, 2, 10, 100]
scale = 4

j = {
    "exe_name": exe_name,
    "cases": []
}

for prb in prbArr:
    cases = [
        {
            "name": "mu_law_compression_9b_{}".format(prb),
            "args": "{} 0".format(prb),
            "reps": reps
        },
        {
            "name": "mu_law_compression_9b_{}_scale".format(prb),
            "args": "{} {}".format(prb, scale),
            "reps": reps
        }
    ]
    j["cases"].extend(cases)

print(json.dumps(j))
