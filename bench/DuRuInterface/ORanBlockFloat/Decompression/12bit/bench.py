#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_block_float_decompression_12bit")

reps = 500000
prbArr = [1, 2, 10, 100]
scale = 4

j = {
    "exe_name": exe_name,
    "cases": []
}


for prb in prbArr:
    cases = [
        {
            "name": "block_float_decompression_12b_{}".format(prb),
            "args": "{} 0".format(prb),
            "reps": reps
        },
        {
            "name": "block_float_decompression_12b_{}_scale".format(prb),
            "args": "{} {}".format(prb, scale),
            "reps": reps
        }
    ]
    j["cases"].extend(cases)

print(json.dumps(j))
