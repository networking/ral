/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <algorithm>
#include <cstdio>
#include <vector>

namespace {

void run_block_float_decompression_9bit_perf(
    const uint32_t num_prbs, const uint32_t num_reps,
    const armral_cmplx_int16_t *scale) {
  printf("[BLOCK FLOAT DECOMPRESSION 9BIT] - number of resource blocks = %u, "
         "number of iterations = %u\n",
         num_prbs, num_reps);

  const std::vector<armral_compressed_data_9bit> src(num_prbs);
  std::vector<armral_cmplx_int16_t> dst(num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES);
  const auto *src_ptr = src.data();
  auto *dst_ptr = dst.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_block_float_decompr_9bit(num_prbs, src_ptr, dst_ptr, scale);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // nprbs    - The number of physical resource blocks
    // scale    - Phase compensation term
    // num_reps - The number of times to repeat the function
    fprintf(stderr, "usage: %s nprbs scale nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  const uint32_t num_prbs = atoi(argv[1]);
  const uint32_t scale_arg = atoi(argv[2]);
  const uint32_t num_reps = atoi(argv[3]);

  const armral_cmplx_int16_t *scale_ptr = NULL;
  armral_cmplx_int16_t scale;
  if (scale_arg != 0) {
    scale.re = scale_arg;
    scale.im = scale_arg;
    scale_ptr = &scale;
  }

  run_block_float_decompression_9bit_perf(num_prbs, num_reps, scale_ptr);

  return EXIT_SUCCESS;
}
