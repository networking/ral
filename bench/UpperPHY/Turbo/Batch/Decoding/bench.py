#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2024-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_turbo_decoding_batch")

j = {
    "exe_name": exe_name,
    "cases": []
}

reps = 30000000
prbArr = [1, 2]
bitArr = [40, 408, 1088, 3136, 6144]
blocksArr = [4, 8, 12]

for bit, prb, blocks in itertools.product(bitArr, prbArr, blocksArr):
    case = {
        "name": "turbo_decoding_batch_{}blocks_{}bits_{}".format(blocks, bit, prb),
        "args": "{} {} {}".format(prb, bit, blocks),
        "reps": reps // bit
    }
    j["cases"].append(case)

print(json.dumps(j))
