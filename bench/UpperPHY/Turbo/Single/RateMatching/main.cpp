/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_turbo_rate_matching_perf(uint32_t d, uint32_t e, uint32_t rv,
                                  uint32_t num_reps) {
  printf("[TURBO RATE MATCHING] - number of encoded bits = %u, number of "
         "transmitted bits = %u, redundancy version = %u, number of iterations "
         "= %u\n",
         d, e, rv, num_reps);

  assert(rv <= 3);
  std::vector<uint8_t> encoded_bits_0((d + 7) / 8);
  std::vector<uint8_t> encoded_bits_1((d + 7) / 8);
  std::vector<uint8_t> encoded_bits_2((d + 7) / 8);
  std::vector<uint8_t> matched_bits((e + 7) / 8);

  const auto *encoded_bits_0_ptr = encoded_bits_0.data();
  const auto *encoded_bits_1_ptr = encoded_bits_1.data();
  const auto *encoded_bits_2_ptr = encoded_bits_2.data();
  auto *matched_bits_ptr = matched_bits.data();

#ifdef ARMRAL_BENCH_NOALLOC
  auto buffer_size = armral_turbo_rate_matching_noalloc_buffer_size(d, e, rv);
  std::vector<uint8_t> buffer(buffer_size);
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_turbo_rate_matching_noalloc(d, e, rv, encoded_bits_0_ptr,
                                       encoded_bits_1_ptr, encoded_bits_2_ptr,
                                       matched_bits_ptr, buffer.data());
  }
#else
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_turbo_rate_matching(d, e, rv, encoded_bits_0_ptr, encoded_bits_1_ptr,
                               encoded_bits_2_ptr, matched_bits_ptr);
  }
#endif
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 5) {
    // d     - The number of bits in code
    // e     - The number of transmitted bits
    // rv    - The redundancy version number of the transmission
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s d e rv nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto d = (uint32_t)atoi(argv[1]);
  auto e = (uint32_t)atoi(argv[2]);
  auto rv = (uint32_t)atoi(argv[3]);
  auto num_reps = (uint32_t)atoi(argv[4]);

  run_turbo_rate_matching_perf(d, e, rv, num_reps);

  return EXIT_SUCCESS;
}
