/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "arm_turbo_decoder_single.hpp"
#include "armral.h"
#include "turbo_code_common.hpp"
#include "utils/allocators.hpp"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_turbo_decoding_perf(const uint32_t num_prbs, const uint32_t num_bits,
                             const uint32_t num_reps) {
  printf("[TURBO DECODING] - number of resources = %u, number of input bits = "
         "%u, number of iterations = %u\n",
         num_prbs, num_bits, num_reps);

  uint32_t num_bytes = num_bits / 8;
  std::vector<uint8_t> ans(num_prbs * num_bytes);

  std::vector<int8_t> sys(num_prbs * (num_bits + 4));
  std::vector<int8_t> par(num_prbs * (num_bits + 4));
  std::vector<int8_t> itl(num_prbs * (num_bits + 4));
  auto *sys_ptr = sys.data();
  auto *par_ptr = par.data();
  auto *itl_ptr = itl.data();
  auto *ans_ptr = ans.data();

  // Set the maximum number of decoder iterations to 2. We disable early
  // exit checking by setting the template parameter to false.
  constexpr auto num_iters = 2;

  [[maybe_unused]] std::vector<uint8_t> buffer(
      armral_turbo_decode_block_noalloc_buffer_size(num_bits));

  for (uint32_t i = 0; i < num_reps; ++i) {
    for (uint32_t j = 0; j < num_prbs; ++j) {
#ifdef ARMRAL_BENCH_NOALLOC
      buffer_bump_allocator allocator{buffer.data()};
      armral::turbo::decode<false, buffer_bump_allocator>(
          sys_ptr + j * (num_bits + 4), par_ptr + j * (num_bits + 4),
          itl_ptr + j * (num_bits + 4), num_bits, ans_ptr + j * num_bytes, 2.F,
          num_iters, 1, nullptr, allocator, trellis_termination,
          decode_block_step, nullptr, nullptr);
#else
      heap_allocator allocator{};
      armral::turbo::decode<false, heap_allocator>(
          sys_ptr + j * (num_bits + 4), par_ptr + j * (num_bits + 4),
          itl_ptr + j * (num_bits + 4), num_bits, ans_ptr + j * num_bytes, 2.F,
          num_iters, 1, nullptr, allocator, trellis_termination,
          decode_block_step, nullptr, nullptr);
#endif
    }
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // nprbs - The number of resources
    // nbits - The number of bits in the code block
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s nprbs nbits nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  const auto num_prbs = (uint32_t)atoi(argv[1]);
  const auto num_bits = (uint32_t)atoi(argv[2]);
  const auto num_reps = (uint32_t)atoi(argv[3]);

  if (armral::turbo::valid_num_bits(num_bits)) {
    run_turbo_decoding_perf(num_prbs, num_bits, num_reps);
  } else {
    printf("ERROR: Unsupported number of bits (%u) specified for turbo "
           "decoding.\n",
           num_bits);
    exit(EXIT_FAILURE);
  }

  return EXIT_SUCCESS;
}
