#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_turbo_encoding")

j = {
    "exe_name": exe_name,
    "cases": []
}

reps = 60000000
prbArr = [1, 2]
bitArr = [40, 408, 1088, 3136, 6144]

for bit, prb in itertools.product(bitArr, prbArr):
    case = {
        "name": "turbo_encoding_{}b_{}".format(bit, prb),
        "args": "{} {}".format(prb, bit),
        "reps": reps // bit
    }
    j["cases"].append(case)

print(json.dumps(j))
