/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_turbo_encoding_perf(const uint32_t num_prbs, const uint32_t num_bits,
                             const uint32_t num_reps) {
  printf("[TURBO ENCODING] - number of resources = %u, number of bits = %u, "
         "number of iterations = %u\n",
         num_prbs, num_bits, num_reps);

  uint32_t num_bytes = num_bits / 8;
  const std::vector<uint8_t> src(num_prbs * num_bytes);
  std::vector<uint8_t> dst0(num_prbs * (num_bytes + 1));
  std::vector<uint8_t> dst1(num_prbs * (num_bytes + 1));
  std::vector<uint8_t> dst2(num_prbs * (num_bytes + 1));

  const auto *src_ptr = src.data();
  auto *dst0_ptr = dst0.data();
  auto *dst1_ptr = dst1.data();
  auto *dst2_ptr = dst2.data();

  [[maybe_unused]] std::vector<uint8_t> buffer(num_bytes);

  for (uint32_t i = 0; i < num_reps; ++i) {
    for (uint32_t j = 0; j < num_prbs; ++j) {
#ifdef ARMRAL_BENCH_NOALLOC
      armral_turbo_encode_block_noalloc(
          src_ptr + j * num_bytes, num_bits, dst0_ptr + j * (num_bytes + 1),
          dst1_ptr + j * (num_bytes + 1), dst2_ptr + j * (num_bytes + 1),
          buffer.data());
#else
      armral_turbo_encode_block(
          src_ptr + j * num_bytes, num_bits, dst0_ptr + j * (num_bytes + 1),
          dst1_ptr + j * (num_bytes + 1), dst2_ptr + j * (num_bytes + 1));
#endif
    }
  }
}

bool valid_num_bits(uint32_t k) {
  if (k < 40) {
    return false;
  }
  if (k <= 512) {
    return k % 8 == 0;
  }
  if (k <= 1024) {
    return k % 16 == 0;
  }
  if (k <= 2048) {
    return k % 32 == 0;
  }
  if (k <= 6144) {
    return k % 64 == 0;
  }
  return false;
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // nprbs - The number of resources
    // nbits - The number of bits in the code block
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s nprbs nbits nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  const auto num_prbs = atoi(argv[1]);
  const auto num_bits = atoi(argv[2]);
  const auto num_reps = atoi(argv[3]);

  if (valid_num_bits(num_bits)) {
    run_turbo_encoding_perf(num_prbs, num_bits, num_reps);
  } else {
    printf("ERROR: Unsupported number of bits (%d) specified for turbo "
           "encoding.\n",
           num_bits);
    exit(EXIT_FAILURE);
  }

  return EXIT_SUCCESS;
}
