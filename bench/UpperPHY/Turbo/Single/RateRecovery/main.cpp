/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_turbo_rate_recovery_perf(uint32_t d, uint32_t e, uint32_t rv,
                                  uint32_t num_reps) {
  printf("[TURBO RATE RECOVERY] - number of recovered LLRs = %u, number of "
         "demodulated LLRs = %u, redundancy version = %u, number of iterations "
         "= %u\n",
         d, e, rv, num_reps);

  assert(rv <= 3);
  std::vector<int8_t> demodulated_llrs(e);
  std::vector<int8_t> recovered_llrs_0(d);
  std::vector<int8_t> recovered_llrs_1(d);
  std::vector<int8_t> recovered_llrs_2(d);

  const auto *demodulated_llrs_ptr = demodulated_llrs.data();
  auto *recovered_llrs_0_ptr = recovered_llrs_0.data();
  auto *recovered_llrs_1_ptr = recovered_llrs_1.data();
  auto *recovered_llrs_2_ptr = recovered_llrs_2.data();

#ifdef ARMRAL_BENCH_NOALLOC
  auto buffer_size = armral_turbo_rate_recovery_noalloc_buffer_size(d, e, rv);
  std::vector<uint8_t> buffer(buffer_size);
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_turbo_rate_recovery_noalloc(
        d, e, rv, demodulated_llrs_ptr, recovered_llrs_0_ptr,
        recovered_llrs_1_ptr, recovered_llrs_2_ptr, buffer.data());
  }
#else
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_turbo_rate_recovery(d, e, rv, demodulated_llrs_ptr,
                               recovered_llrs_0_ptr, recovered_llrs_1_ptr,
                               recovered_llrs_2_ptr);
  }
#endif
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 5) {
    // d     - The number of rate recovered LLRs
    // e     - The number of demodulated LLRs
    // rv    - The redundancy version number of the transmission
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s d e rv nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto d = (uint32_t)atoi(argv[1]);
  auto e = (uint32_t)atoi(argv[2]);
  auto rv = (uint32_t)atoi(argv[3]);
  auto num_reps = (uint32_t)atoi(argv[4]);

  run_turbo_rate_recovery_perf(d, e, rv, num_reps);

  return EXIT_SUCCESS;
}
