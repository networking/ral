#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_turbo_rate_recovery")

j = {
    "exe_name": exe_name,
    "cases": []
}

items = [
    (32, 23, 0),
    (32, 32, 0),
    (32, 71, 0),
    (128, 71, 0),
    (128, 128, 0),
    (128, 263, 0),
    (512, 263, 0),
    (512, 512, 0),
    (512, 1031, 0),
]

reps = 50000
for d, e, rv in items:
    name = "turbo_rate_recovery_d{}_e{}_rv{}".format(d, e, rv)
    case = {
        "name": name,
        "args": "{} {} {}".format(d, e, rv),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
