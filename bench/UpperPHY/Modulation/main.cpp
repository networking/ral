/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <string>
#include <vector>

namespace {

struct mod_test_params {
  armral_modulation_type mod_type;
  uint32_t bits_per_symbol;
  std::string name;
  uint32_t num_symbols;

  // Default constructor
  mod_test_params() = default;

  // Constructor from a set of parameters
  mod_test_params(armral_modulation_type m, uint32_t bps, std::string n,
                  uint32_t l)
    : mod_type(m), bits_per_symbol(bps), name(std::move(n)),
      num_symbols((l * 8) / bps) {}
};

mod_test_params get_test_params(armral_modulation_type mod_type, uint32_t len) {
  switch (mod_type) {
  case ARMRAL_MOD_QPSK:
    return mod_test_params(mod_type, 2, "QPSK", len);
  case ARMRAL_MOD_16QAM:
    return mod_test_params(mod_type, 4, "16QAM", len);
  case ARMRAL_MOD_64QAM:
    return mod_test_params(mod_type, 6, "64QAM", len);
  case ARMRAL_MOD_256QAM:
    return mod_test_params(mod_type, 8, "256QAM", len);
  }
  assert(false);
  return {};
}

void run_modulation_perf(armral_modulation_type mod_type, uint32_t num_bytes,
                         uint32_t num_reps) {
  auto test_params = get_test_params(mod_type, num_bytes);
  printf("[MODULATION] - modulation type = %s, number of input bytes = %u, "
         "number of iterations = %u\n",
         test_params.name.c_str(), num_bytes, num_reps);

  auto tv_input = std::vector<int8_t>(num_bytes);
  for (uint32_t i = 0; i < num_bytes; ++i) {
    tv_input[i] = (int8_t)i;
  }

  std::vector<armral_cmplx_int16_t> p_out(test_params.num_symbols);

  // The number of bits needs to be a multiple of the bits per symbol
  uint32_t num_bits = test_params.num_symbols * test_params.bits_per_symbol;
  for (uint32_t i = 0; i < num_reps; i++) {
    armral_modulation(num_bits, mod_type, (uint8_t *)tv_input.data(),
                      p_out.data());
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // mod    - The modulation type
    // nbytes - The number of input bytes
    // nreps  - The number of times to repeat the function
    fprintf(stderr, "usage: %s mod nbytes nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto mod_type = (armral_modulation_type)atoi(argv[1]);
  auto num_bytes = (uint32_t)atoi(argv[2]);
  auto num_reps = (uint32_t)atoi(argv[3]);

  run_modulation_perf(mod_type, num_bytes, num_reps);

  return EXIT_SUCCESS;
}
