#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_modulation")

base_reps = 6000000
lenArr = [4, 32, 64, 127, 128, 129, 256, 1024, 2047]
modArr = ["qpsk", "qam16", "qam64", "qam256"]

j = {
    "exe_name": exe_name,
    "cases": []
}

for (mod_index, mod), length in itertools.product(enumerate(modArr), lenArr):
    case = {
        "name": "modulation_{}_{}".format(mod, length),
        "args": "{} {}".format(mod_index, length),
        "reps": base_reps // length
    }
    j["cases"].append(case)

print(json.dumps(j))
