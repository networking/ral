/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_tail_biting_convolutional_decoding_perf(const uint32_t num_bits,
                                                 const uint32_t num_reps) {
  printf("[CONVOLUTIONAL DECODING] - number of bits in code block = %u, number "
         "of iterations = %u\n",
         num_bits, num_reps);

  uint32_t num_bytes = (num_bits + 7) / 8;
  const std::vector<int8_t> src0(num_bits);
  const std::vector<int8_t> src1(num_bits);
  const std::vector<int8_t> src2(num_bits);
  std::vector<uint8_t> dst(num_bytes);

  const auto *src0_ptr = src0.data();
  const auto *src1_ptr = src1.data();
  const auto *src2_ptr = src2.data();
  auto *dst_ptr = dst.data();

#ifdef ARMRAL_BENCH_NOALLOC
  // Benchmark only added for interest. This is not expected to show any major
  // performance difference.
  auto buffer_size =
      armral_tail_biting_convolutional_decode_block_noalloc_buffer_size(
          num_bits, 2);
  std::vector<uint8_t> buffer(buffer_size);
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_tail_biting_convolutional_decode_block_noalloc(
        src0_ptr, src1_ptr, src2_ptr, num_bits, 2, dst_ptr, buffer.data());
  }
#else
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_tail_biting_convolutional_decode_block(src0_ptr, src1_ptr, src2_ptr,
                                                  num_bits, 2, dst_ptr);
  }
#endif
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 3) {
    // nbits - The length of the input code block in bits
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s nbits nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  const auto num_bits = (uint32_t)atoi(argv[1]);
  const auto num_reps = (uint32_t)atoi(argv[2]);

  run_tail_biting_convolutional_decoding_perf(num_bits, num_reps);

  return EXIT_SUCCESS;
}
