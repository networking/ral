/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_tail_biting_convolutional_encoding_perf(const uint32_t num_bits,
                                                 const uint32_t num_reps) {
  printf("[CONVOLUTIONAL ENCODING] - number of bits in code block = %u, number "
         "of iterations = %u\n",
         num_bits, num_reps);

  uint32_t num_bytes = (num_bits + 7) / 8;
  const std::vector<uint8_t> src(num_bytes);
  std::vector<uint8_t> dst0(num_bytes);
  std::vector<uint8_t> dst1(num_bytes);
  std::vector<uint8_t> dst2(num_bytes);

  const auto *src_ptr = src.data();
  auto *dst0_ptr = dst0.data();
  auto *dst1_ptr = dst1.data();
  auto *dst2_ptr = dst2.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_tail_biting_convolutional_encode_block(src_ptr, num_bits, dst0_ptr,
                                                  dst1_ptr, dst2_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 3) {
    // nbits - The length of the input code block in bits
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s nbits nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  const auto num_bits = (uint32_t)atoi(argv[1]);
  const auto num_reps = (uint32_t)atoi(argv[2]);

  run_tail_biting_convolutional_encoding_perf(num_bits, num_reps);

  return EXIT_SUCCESS;
}
