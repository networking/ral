/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_crc_11_le_perf(uint32_t n, uint32_t num_reps) {
  printf("[CRC 11 LITTLE ENDIAN] - number of bytes in buffer = %u, number of "
         "iterations = %u\n",
         n, num_reps);

  // from the interface doxygen:
  // a) It is assumed that input data is padded to 64bits, when the original
  // size is 32bits. b) If the input size is > 64bits, then a padding to 128bits
  // is assumed.
  assert(n == 8 || n % 16 == 0);
  const std::vector<uint64_t> buf(n);
  uint64_t crc_res = 0;

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_crc11_le(n, buf.data(), &crc_res);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 3) {
    // nbytes - The number of bytes of the given buffer
    // nreps  - The number of times to repeat the function
    fprintf(stderr, "usage: %s nbytes nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto num_bytes = (uint32_t)atoi(argv[1]);
  auto num_reps = (uint32_t)atoi(argv[2]);

  run_crc_11_le_perf(num_bytes, num_reps);

  return EXIT_SUCCESS;
}
