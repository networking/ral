#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_crc_24a_le")

j = {
    "exe_name": exe_name,
    "cases": []
}

length_arr = [8, 16, 32, 64, 128, 192, 224, 240, 256, 272, 288,
              320, 384, 512, 1024, 2048, 4096, 8192, 16384]
base_reps = 120000000

for length in length_arr:
    reps = base_reps // length
    case = {
        "name": "crc24a_1234_{}".format(length),
        "args": "{}".format(length),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
