/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <string>
#include <vector>

namespace {

struct demod_test_params {
  armral_modulation_type mod_type;
  uint32_t bits_per_symbol;
  std::string name;

  // Default constructor
  demod_test_params() = default;

  // Constructor from a set of parameters
  demod_test_params(armral_modulation_type m, unsigned bps, std::string n)
    : mod_type(m), bits_per_symbol(bps), name(std::move(n)) {}
};

demod_test_params get_test_params(armral_modulation_type mod_type) {
  switch (mod_type) {
  case ARMRAL_MOD_QPSK:
    return demod_test_params(mod_type, 2, "QPSK");
  case ARMRAL_MOD_16QAM:
    return demod_test_params(mod_type, 4, "16QAM");
  case ARMRAL_MOD_64QAM:
    return demod_test_params(mod_type, 6, "64QAM");
  case ARMRAL_MOD_256QAM:
    return demod_test_params(mod_type, 8, "256QAM");
  }
  assert(false);
  return {};
}

void run_demod_perf(armral_modulation_type mod_type, uint32_t num_symbols,
                    uint32_t num_reps) {
  constexpr uint16_t ulp = 256; // 1/2^5 in Q(2.13)
  auto test_params = get_test_params(mod_type);

  printf("[DEMODULATION] - modulation type = %s, number of symbols = %u, "
         "number of iterations = %u\n",
         test_params.name.c_str(), num_symbols, num_reps);

  std::vector<int8_t> llrs(num_symbols * test_params.bits_per_symbol);
  auto src_symbols = std::vector<armral_cmplx_int16_t>(num_symbols);
  for (uint32_t i = 0; i < num_symbols; ++i) {
    src_symbols[i].re = i;
    src_symbols[i].im = i;
  }

  for (uint32_t i = 0; i < num_reps; i++) {
    armral_demodulation(num_symbols, ulp, mod_type, src_symbols.data(),
                        llrs.data());
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // mod      - The modulation type
    // nsymbols - The number of complex symbols
    // nreps    - The number of times to repeat the function
    fprintf(stderr, "usage: %s mod nsymbols nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto mod = (armral_modulation_type)atoi(argv[1]);
  auto num_symbols = (uint32_t)atoi(argv[2]);
  auto num_reps = (uint32_t)atoi(argv[3]);

  run_demod_perf(mod, num_symbols, num_reps);

  return EXIT_SUCCESS;
}
