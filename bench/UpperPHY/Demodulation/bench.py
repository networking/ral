#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_demodulation")

j = {
    "exe_name": exe_name,
    "cases": []
}

reps = 300000
lenArr = [8, 31, 276, 1024]
modArr = ["qpsk", "qam16", "qam64", "qam256"]

for (mod_index, mod), length in itertools.product(enumerate(modArr), lenArr):
    case = {
        "name": "demodulation_{}_{}".format(mod, length),
        "args": "{} {}".format(mod_index, length),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
