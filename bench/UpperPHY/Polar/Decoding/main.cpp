/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "rng.hpp"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_polar_decoding_perf(uint32_t n, uint32_t k, uint32_t l,
                             uint32_t num_reps) {
  printf("[POLAR DECODING] - number of LLRs = %u, number of information bits = "
         "%u, list size = %u, number of iterations = %u\n",
         n, k, l, num_reps);

  assert(n % 32 == 0);
  std::vector<int8_t> a(n);
  std::vector<uint8_t> b(l * n / 8);
  std::vector<uint8_t> frozen_mask(n);
  // just assume e=n and no parity bits, since it shouldn't affect performance
  // significantly.
  armral_polar_frozen_mask(n, n, k, 0, 0, frozen_mask.data());

  // populate input llrs randomly, since otherwise the effects of
  // microarchitectural branch prediction are too optimistic and give an
  // unrealistically fast result. We use a linear congruential generator to
  // avoid calling rand() or C++ random number generators, which are both slow.
  armral::utils::linear_congruential_generator lcg;
  auto state = armral::utils::random_state::from_seeds({42});
  for (uint32_t i = 0; i < n; ++i) {
    ((uint8_t *)a.data())[i] = lcg.one<uint32_t>(&state);
  }

  const auto *frozen_ptr = frozen_mask.data();
  const auto *a_ptr = a.data();
  auto *b_ptr = b.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_polar_decode_block(n, frozen_ptr, l, a_ptr, b_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 5) {
    // n     - The number of input beliefs (LLRs)
    // k     - The number of information bits to output
    // l     - The list size to use for decoding
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s n k l nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto n = (uint32_t)atoi(argv[1]);
  auto k = (uint32_t)atoi(argv[2]);
  auto l = (uint32_t)atoi(argv[3]);
  auto num_reps = (uint32_t)atoi(argv[4]);

  run_polar_decoding_perf(n, k, l, num_reps);

  return EXIT_SUCCESS;
}
