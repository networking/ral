#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_polar_decoder")

j = {
    "exe_name": exe_name,
    "cases": []
}

paramArr_l8 = [[32, 8], [64, 50]]
paramArr_l1_l4 = paramArr_l8 + [[128, 96], [256, 250], [512, 300], [1024, 600]]
lArr = [1, 2, 4, 8]

# PERF TESTS for POLAR_DECODER, all lengths
reps = 60000
for listSize in lArr:
    paramArr = paramArr_l1_l4 if listSize < 8 else paramArr_l8
    for length, k in paramArr:
        case = {
            "name": "polar_decoder_n{}_k{}_l{}".format(length, k, listSize),
            "args": "{} {} {}".format(length, k, listSize),
            "reps": reps
        }
        j["cases"].append(case)

print(json.dumps(j))
