/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_polar_frozen_mask_perf(uint32_t n, uint32_t e, uint32_t k,
                                uint32_t n_pc, uint32_t n_pc_wm,
                                uint32_t num_reps) {
  printf("[POLAR FROZEN MASK] - number of LLRs = %u, number of encoded bits = "
         "%u, number of output information bits = %u, number of parity bits = "
         "%u, number of row-weight-selected parity bits = %u, number of "
         "iterations = %u\n",
         n, e, k, n_pc, n_pc_wm, num_reps);

  assert(n % 32 == 0);
  std::vector<uint8_t> frozen_mask(n);

  auto *frozen_ptr = frozen_mask.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_polar_frozen_mask(n, e, k, n_pc, n_pc_wm, frozen_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 7) {
    // n       - The number of input beliefs (LLRs)
    // e       - The encoded length in bits
    // k       - The number of information bits to output
    // n_pc    - The number of parity bits in the encoded message
    // n_pc_wm - The number of row-weight-selected parity bits in the encoded
    //           message
    // nreps   - The number of times to repeat the function
    fprintf(stderr, "usage: %s n e k n_pc n_pc_wm nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto n = (uint32_t)atoi(argv[1]);
  auto e = (uint32_t)atoi(argv[2]);
  auto k = (uint32_t)atoi(argv[3]);
  auto n_pc = (uint32_t)atoi(argv[4]);
  auto n_pc_wm = (uint32_t)atoi(argv[5]);
  auto num_reps = (uint32_t)atoi(argv[6]);

  run_polar_frozen_mask_perf(n, e, k, n_pc, n_pc_wm, num_reps);

  return EXIT_SUCCESS;
}
