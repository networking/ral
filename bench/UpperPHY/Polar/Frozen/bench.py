#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_polar_frozen_mask")

j = {
    "exe_name": exe_name,
    "cases": []
}

items = [
    (32, 10, 8),
    (32, 24, 8),
    (32, 32, 8),
    (32, 20, 16),
    (32, 32, 16),
    (32, 32, 24),
    (64, 20, 16),
    (64, 48, 16),
    (64, 64, 16),
    (64, 40, 32),
    (64, 64, 32),
    (64, 64, 48),
    (128, 40, 32),
    (128, 96, 32),
    (128, 128, 32),
    (128, 80, 64),
    (128, 128, 64),
    (128, 128, 96),
    (256, 80, 64),
    (256, 160, 64),
    (256, 256, 64),
    (256, 160, 128),
    (256, 256, 128),
    (256, 256, 192),
    (512, 160, 128),
    (512, 320, 128),
    (512, 512, 128),
    (512, 320, 256),
    (512, 512, 256),
    (512, 512, 384),
    (1024, 320, 256),
    (1024, 640, 256),
    (1024, 1024, 256),
    (1024, 640, 512),
    (1024, 1024, 512),
    (1024, 1024, 768),
]

pc_bits_items = [
    (0, 0, ""),
    (3, 0, "_3pc"),
    (3, 1, "_3pc_1wm"),
]

reps = 600000
for n, e, kplus in items:
    for n_pc, n_pc_wm, pc_suffix in pc_bits_items:
        k = kplus - n_pc
        name = "polar_frozen_mask_n{}_e{}_k{}{}".format(n, e, k, pc_suffix)
        case = {
            "name": name,
            "args": "{} {} {} {} {}".format(n, e, k, n_pc, n_pc_wm),
            "reps": reps
        }
        j["cases"].append(case)

print(json.dumps(j))
