/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_polar_encoding_perf(uint32_t n, uint32_t num_reps) {
  printf("[POLAR ENCODING] - number of LLRs = %u, number of iterations = %u\n",
         n, num_reps);

  assert(n % 32 == 0);
  const std::vector<uint8_t> a(n / 8);
  std::vector<uint8_t> b(n / 8);

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_polar_encode_block(n, a.data(), b.data());
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 3) {
    // n     - the number of input beliefs (LLRs)
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s n nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto n = (uint32_t)atoi(argv[1]);
  auto num_reps = (uint32_t)atoi(argv[2]);

  run_polar_encoding_perf(n, num_reps);

  return EXIT_SUCCESS;
}
