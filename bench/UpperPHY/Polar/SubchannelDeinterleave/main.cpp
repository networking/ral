/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_polar_subchannel_deinterleave_perf(uint32_t n, uint32_t k,
                                            uint32_t num_reps) {
  printf("[POLAR SUBCHANNEL DEINTERLEAVE] - number of LLRs = %u, number of "
         "output information bits = %u, number of iterations = %u\n",
         n, k, num_reps);

  assert(n % 32 == 0);
  std::vector<uint8_t> frozen(n);
  std::vector<uint8_t> c((k + 7) / 8);
  std::vector<uint8_t> u(n / 8);

  // build a frozen mask to pass to the interleaving.
  // not handling parity bits for now.
  armral_polar_frozen_mask(n, n, k, 0, 0, frozen.data());

  const auto *frozen_ptr = frozen.data();
  const auto *u_ptr = u.data();
  auto *c_ptr = c.data();

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_polar_subchannel_deinterleave(k, frozen_ptr, u_ptr, c_ptr);
  }
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 4) {
    // n     - The number of input beliefs (LLRs)
    // k     - The number of information bits to output
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s n k nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto n = (uint32_t)atoi(argv[1]);
  auto k = (uint32_t)atoi(argv[2]);
  auto num_reps = (uint32_t)atoi(argv[3]);

  run_polar_subchannel_deinterleave_perf(n, k, num_reps);

  return EXIT_SUCCESS;
}
