#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_polar_subchannel_interleave")

j = {
    "exe_name": exe_name,
    "cases": []
}

items = [
    (32, 8),
    (32, 16),
    (32, 24),
    (64, 16),
    (64, 32),
    (64, 48),
    (128, 32),
    (128, 64),
    (128, 96),
    (256, 64),
    (256, 128),
    (256, 192),
    (512, 128),
    (512, 256),
    (512, 384),
    (1024, 256),
    (1024, 512),
    (1024, 768),
]

reps = 600000
for n, k in items:
    case = {
        "name": "polar_subchannel_interleave_n{}_k{}".format(n, k),
        "args": "{} {}".format(n, k),
        "reps": reps
    }
    j["cases"].append(case)

print(json.dumps(j))
