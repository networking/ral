#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_polar_rate_matching")

j = {
    "exe_name": exe_name,
    "cases": []
}

items = [
    (32, 8, 5),
    (32, 27, 8),
    (32, 64, 11),
    (64, 16, 13),
    (64, 51, 16),
    (64, 128, 19),
    (128, 32, 29),
    (128, 99, 32),
    (128, 256, 35),
    (256, 64, 61),
    (256, 195, 64),
    (256, 512, 67),
    (512, 128, 125),
    (512, 387, 128),
    (512, 1024, 131),
    (1024, 256, 253),
    (1024, 771, 256),
    (1024, 2048, 259),
]

reps = 600000

for n, e, k in items:
    cases = [
        {
            "name": "polar_rate_matching_n{}_e{}_k{}_i_bil0".format(n, e, k),
            "args": "{} {} {} 0".format(n, e, k),
            "reps": reps
        },
        {
            "name": "polar_rate_matching_n{}_e{}_k{}_i_bil1".format(n, e, k),
            "args": "{} {} {} 1".format(n, e, k),
            "reps": reps
        }
    ]
    j["cases"].extend(cases)

print(json.dumps(j))
