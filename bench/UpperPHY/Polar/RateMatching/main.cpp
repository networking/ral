/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_polar_rate_matching_perf(uint32_t n, uint32_t e, uint32_t k,
                                  armral_polar_ibil_type i_bil,
                                  uint32_t num_reps) {
  printf("[POLAR RATE MATCHING] - number of bits in code block = %u, number of "
         "transmitted bits = %u, number of output information bits = %u, "
         "i_bil = %u, number of iterations = %u\n",
         n, e, k, (uint32_t)i_bil, num_reps);

  assert(n % 32 == 0);
  std::vector<uint8_t> encoded_bits((n + 7) / 8);
  std::vector<uint8_t> matched_bits((e + 7) / 8);

  const auto *encoded_bits_ptr = encoded_bits.data();
  auto *matched_bits_ptr = matched_bits.data();

#ifdef ARMRAL_BENCH_NOALLOC
  std::vector<uint8_t> buffer((n + e + 7) / 8);

  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_polar_rate_matching_noalloc(n, e, k, i_bil, encoded_bits_ptr,
                                       matched_bits_ptr, buffer.data());
  }
#else
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_polar_rate_matching(n, e, k, i_bil, encoded_bits_ptr,
                               matched_bits_ptr);
  }
#endif
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 6) {
    // n     - The number of bits in the code block
    // e     - The number of transmitted bits
    // k     - The number of information bits to output
    // i_bil - A flag to enable/disable the interleaving of coded bits
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s n e k i_bil nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  int n = atoi(argv[1]);
  int e = atoi(argv[2]);
  int k = atoi(argv[3]);
  armral_polar_ibil_type i_bil = armral_polar_ibil_type(atoi(argv[4]));
  int num_reps = atoi(argv[5]);

  run_polar_rate_matching_perf(n, e, k, i_bil, num_reps);

  return EXIT_SUCCESS;
}
