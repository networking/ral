/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cassert>
#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_polar_rate_recovery_perf(uint32_t n, uint32_t e, uint32_t k,
                                  armral_polar_ibil_type i_bil,
                                  uint32_t num_reps) {
  printf("[POLAR RATE RECOVERY] - number of bits in code block = %u, number of "
         "transmitted bits = %u, number of output information bits = %u, "
         "i_bil=%u, number of iterations = %u\n",
         n, e, k, (uint32_t)i_bil, num_reps);

  assert(n % 32 == 0);
  std::vector<int8_t> llrs(e);
  std::vector<int8_t> recovered_llrs(n);

  const auto *llrs_ptr = llrs.data();
  auto *recovered_llrs_ptr = recovered_llrs.data();

#ifdef ARMRAL_BENCH_NOALLOC
  std::vector<uint8_t> buffer(n + e);
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_polar_rate_recovery_noalloc(n, e, k, i_bil, llrs_ptr,
                                       recovered_llrs_ptr, buffer.data());
  }
#else
  for (uint32_t i = 0; i < num_reps; ++i) {
    armral_polar_rate_recovery(n, e, k, i_bil, llrs_ptr, recovered_llrs_ptr);
  }
#endif
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 6) {
    // n     - The number of bits in code/number of llrs to recover
    // e     - The number of transmitted bits/number of input beliefs (LLRs)
    // k     - The number of information bits to output
    // i_bil - A flag to enable/disable the interleaving of coded bits
    // nreps - The number of times to repeat the function
    fprintf(stderr, "usage: %s n e k i_bil nreps\n", argv[0]);
    exit(EXIT_FAILURE);
  }

  auto n = (uint32_t)atoi(argv[1]);
  auto e = (uint32_t)atoi(argv[2]);
  auto k = (uint32_t)atoi(argv[3]);
  armral_polar_ibil_type i_bil = armral_polar_ibil_type(atoi(argv[4]));
  auto num_reps = (uint32_t)atoi(argv[5]);

  run_polar_rate_recovery_perf(n, e, k, i_bil, num_reps);

  return EXIT_SUCCESS;
}
