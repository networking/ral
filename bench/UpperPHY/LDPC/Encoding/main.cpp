/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"
#include "ldpc_coding.hpp"

namespace {

void run_ldpc_encoding_perf(armral_ldpc_graph_t bg, uint32_t z,
                            uint32_t len_filler_bits, uint32_t reps) {
  printf("[LDPC ENCODING] - base graph = %u, lifting size = %u, filler bits "
         "length = %u, number of "
         "repetitions = %u\n",
         (uint32_t)bg, z, len_filler_bits, reps);

  const auto *graph = armral_ldpc_get_base_graph(bg);

  uint32_t in_size = graph->nmessage_bits * z;
  uint32_t out_size = (graph->ncodeword_bits + 2) * z;

  std::vector<uint8_t> in((in_size + 7) / 8);
  std::vector<uint8_t> out((out_size + 7) / 8);
  const auto *in_ptr = in.data();
  auto *out_ptr = out.data();

#ifdef ARMRAL_BENCH_NOALLOC
  // Benchmark only added for interest. This is not expected to show any major
  // performance difference.
  auto buffer_size =
      armral_ldpc_encode_block_noalloc_buffer_size(bg, z, len_filler_bits);
  std::vector<uint8_t> buffer(buffer_size);
  for (uint32_t r = 0; r < reps; ++r) {
    armral_ldpc_encode_block_noalloc(in_ptr, bg, z, len_filler_bits, out_ptr,
                                     buffer.data());
  }
#else
  for (uint32_t r = 0; r < reps; ++r) {
    armral_ldpc_encode_block(in_ptr, bg, z, len_filler_bits, out_ptr);
  }
#endif
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 5) {
    // base_graph:  integer representing the base graph to use.
    //              This gets converted into the enum representing the base
    //              graph 1 -> LDPC_BASE_GRAPH_1 2 -> LDPC_BASE_GRAPH_2
    // lifting_size:  The lifting size Z to use in the block encoding
    // len_filler_bits:  Length of filler bits as per section 5.2.2 of TS 38.212
    // num_reps:      The number of times to repeat the encoding, so as to get a
    //                stable performance number
    printf("Usage: %s base_graph lifting_size len_filler_bits num_reps\n",
           argv[0]);
    exit(EXIT_FAILURE);
  }

  auto bg = (armral_ldpc_graph_t)(atoi(argv[1]) - 1);
  auto z = (uint32_t)atoi(argv[2]);
  auto len_filler_bits = (uint32_t)atoi(argv[3]);
  auto reps = (uint32_t)atoi(argv[4]);

  run_ldpc_encoding_perf(bg, z, len_filler_bits, reps);

  return EXIT_SUCCESS;
}
