#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_ldpc_encoding")

j = {
    "exe_name": exe_name,
    "cases": []
}

base_graphs = [1, 2]
lifting_sizes = [2, 11, 16, 18, 30, 36, 52, 112, 160, 208, 384]
len_filler_bits = [0, 0, 0, 76, 0, 0, 0, 72, 0, 0, 0]

# PERF TESTS for ldpc_encoder, all base graphs and lifting sets
# target_reps are the number of repetitions to get something
# sensible for z = 2, but that gives a long running test
# for the larger size. We scale the number of reps
# according to the lifting size.
target_reps = 150000

for bg, (z, f) in itertools.product(base_graphs, zip(lifting_sizes, len_filler_bits)):
    case = {
        "name": "ldpc_encoding_bg{}_z{}_f{}".format(bg, z, f),
        "args": "{} {} {}".format(bg, z, f),
        "reps": target_reps * 2 // z
    }
    j["cases"].append(case)

print(json.dumps(j))
