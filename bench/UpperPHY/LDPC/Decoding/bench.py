#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
import itertools
from pathlib import Path
import os


ARMRAL_LDPC_NO_CRC = 0


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_ldpc_decoding")

j = {
    "exe_name": exe_name,
    "cases": []
}

base_graphs = [1, 2]
lifting_sizes = [2, 3, 5, 7, 9, 11, 13, 15, 56, 80, 256, 384]
crc_idx_1 = [5257, 7721]
crc_idx_2 = [2377, 3497]
num_its = [5, 10]

# PERF TESTS for ldpc_decoder, all lengths
# This is a good number of repetitions for z = 2, and
# num_its = 5. Work is (should be) linear in num_its and z
# so we scale by this for the rest of the lifting sizes
target_reps = 15000

for bg, its in itertools.product(base_graphs, num_its):
    crc_ids = crc_idx_1 if bg == 1 else crc_idx_2
    for i, z in enumerate(lifting_sizes):
        assert z < 208 or i >= 10
        crc_idx = crc_ids[i - 10] if z >= 208 else ARMRAL_LDPC_NO_CRC
        case = {
            "name": "ldpc_decoding_bg{}_z{}_crc{}_its{}".format(bg, z,
                                                                crc_idx,
                                                                its),
            "args": "{} {} {} {}".format(bg, z, crc_idx, its),
            "reps": target_reps * 2 * 5 // (z * its)
        }
        j["cases"].append(case)

print(json.dumps(j))
