/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "armral.h"
#include "int_utils.hpp"
#include "ldpc_coding.hpp"
#include "utils/allocators.hpp"

namespace {

void run_ldpc_decoding_perf(armral_ldpc_graph_t bg, uint32_t z,
                            uint32_t crc_idx, uint32_t num_its,
                            uint32_t num_reps) {
  printf("[LDPC DECODING] - base graph = %u, lifting size = %u, number of "
         "decoding "
         "iterations = %u, index where CRC information begins = %u , number of "
         "repetitions = %u\n",
         (uint32_t)bg, z, num_its, crc_idx, num_reps);

  const auto *graph = armral_ldpc_get_base_graph(bg);

  uint32_t num_llrs = (graph->ncodeword_bits + 2) * z;
  std::vector<int8_t> llrs(num_llrs);
  std::vector<uint8_t> data_out((num_llrs + 7) / 8);
  const auto *llr_ptr = llrs.data();
  auto *out_ptr = data_out.data();

#ifdef ARMRAL_BENCH_NOALLOC
  auto buffer_size =
      armral_ldpc_decode_block_noalloc_buffer_size(bg, z, crc_idx, num_its);
  std::vector<uint8_t> buffer(buffer_size);
  for (uint32_t r = 0; r < num_reps; ++r) {
    buffer_bump_allocator allocator{buffer.data()};
    armral::ldpc::decode_block<buffer_bump_allocator>(
        llr_ptr, bg, z, crc_idx, num_its, out_ptr, allocator);
  }
#else
  for (uint32_t r = 0; r < num_reps; ++r) {
    heap_allocator allocator{};
    armral::ldpc::decode_block<heap_allocator>(llr_ptr, bg, z, crc_idx, num_its,
                                               out_ptr, allocator);
  }
#endif
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 6) {
    // base_graph - integer representing the base graph to use.
    //              This gets converted into the enum representing the base
    //              graph 1 -> LDPC_BASE_GRAPH_1 2 -> LDPC_BASE_GRAPH_2
    // z          - Lifting size
    // crc_idx    - Index in the code block where the CRC information begins,
    //              or ARMRAL_LDPC_NO_CRC if there are no CRC bits attached
    // num_its    - The number of iterations to perform in the decoding
    // num_reps   - The number of times to repeat the decoding so as
    //              to get a stable performance number
    printf("Usage: %s base_graph z crc_idx num_its num_reps\n", argv[0]);
    exit(EXIT_FAILURE);
  }
  auto bg = (armral_ldpc_graph_t)(atoi(argv[1]) - 1);
  auto z = (uint32_t)atoi(argv[2]);
  auto crc_idx = (uint32_t)atoi(argv[3]);
  auto num_its = (uint32_t)atoi(argv[4]);
  auto num_reps = (uint32_t)atoi(argv[5]);

  run_ldpc_decoding_perf(bg, z, crc_idx, num_its, num_reps);

  return EXIT_SUCCESS;
}
