/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "ldpc_coding.hpp"

#include <cstdio>
#include <cstdlib>
#include <vector>

namespace {

void run_ldpc_rate_matching_perf(armral_ldpc_graph_t bg, uint32_t z, uint32_t e,
                                 uint32_t nref, uint32_t rv,
                                 armral_modulation_type mod, uint32_t reps) {
  printf("[LDPC RATE MATCHING] bg = %u, z = %u, e = %u, nref = %u, rv = %u, "
         "mod = %u, number of repetitions = %u\n",
         (uint32_t)bg, z, e, nref, rv, (uint32_t)mod, reps);

  const auto *graph = armral_ldpc_get_base_graph(bg);

  uint32_t len_filler_bits = 0;
  uint32_t in_size = graph->nmessage_bits * z;
  uint32_t out_size = (graph->ncodeword_bits + 2) * z;

  std::vector<uint8_t> in((in_size + 7) / 8);
  std::vector<uint8_t> out((out_size + 7) / 8);
  const auto *in_ptr = in.data();
  auto *out_ptr = out.data();
#ifdef ARMRAL_BENCH_NOALLOC
  std::vector<uint8_t> buffer((2 * z * 66) + e);
  auto *buffer_ptr = buffer.data();

  for (uint32_t r = 0; r < reps; ++r) {
    armral_ldpc_rate_matching_noalloc(bg, z, e, nref, len_filler_bits, in_size,
                                      rv, mod, in_ptr, out_ptr, buffer_ptr);
  }
#else
  for (uint32_t r = 0; r < reps; ++r) {
    armral_ldpc_rate_matching(bg, z, e, nref, len_filler_bits, in_size, rv, mod,
                              in_ptr, out_ptr);
  }
#endif
}

} // anonymous namespace

int main(int argc, char **argv) {
  if (argc != 8) {
    // base_graph:    Integer representing the base graph to use.
    //                This gets converted into the enum representing the base
    //                graph 1 -> LDPC_BASE_GRAPH_1 2 -> LDPC_BASE_GRAPH_2
    // lifting_size:  The lifting size Z.
    // e:             The number of bits in the rate-matched message.
    //                This is assumed to be a multiple of the number of
    //                bits per modulation symbol.
    // nref:          The soft buffer size for limited buffer rate matching.
    // rv:            Redundancy version used in rate matching.
    //                Must be in the set {0, 1, 2, 3}.
    // mod:           The type of modulation to perform.
    // num_reps:      The number of times to repeat the function.
    printf("Usage: %s base_graph lifting_size e nref rv mod num_reps\n",
           argv[0]);
    exit(EXIT_FAILURE);
  }

  auto bg = (armral_ldpc_graph_t)(atoi(argv[1]) - 1);
  auto z = (uint32_t)atoi(argv[2]);
  auto e = (uint32_t)atoi(argv[3]);
  auto nref = (uint32_t)atoi(argv[4]);
  auto rv = (uint32_t)atoi(argv[5]);
  auto mod = (armral_modulation_type)atoi(argv[6]);
  auto reps = (uint32_t)atoi(argv[7]);

  run_ldpc_rate_matching_perf(bg, z, e, nref, rv, mod, reps);

  return EXIT_SUCCESS;
}
