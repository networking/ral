#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import json
from pathlib import Path
import os


def get_path(x): return x if Path(x).is_file() else os.path.join("armral", x)


exe_name = get_path("bench_ldpc_rate_matching")

j = {
    "exe_name": exe_name,
    "cases": []
}

para_list = [
    (1, 2, 44, 25344, 0, 0),
    (1, 11, 242, 25344, 0, 0),
    (1, 112, 2464, 25344, 0, 0),
    (1, 208, 4576, 25344, 0, 0),
    (1, 384, 8448, 25344, 0, 0),
    (2, 2, 22, 19200, 0, 0),
    (2, 11, 112, 19200, 0, 0),
    (2, 112, 1232, 19200, 0, 0),
    (2, 208, 2288, 19200, 0, 0),
    (2, 384, 4224, 19200, 0, 0)
]

# We scale the number of reps according to the lifting size.
target_reps = 150000

for bg, z, e, nref, rv, mod in para_list:
    case = {
        "name": "ldpc_rate_matching_bg{}_z{}_e{}_nref{}_rv{}_mod{}".format(
            bg, z, e, nref, rv, mod
        ),
        "args": "{} {} {} {} {} {}".format(bg, z, e, nref, rv, mod),
        "reps": target_reps * 2 // z,
    }
    j["cases"].append(case)
print(json.dumps(j))
