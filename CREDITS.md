In addition to the primary development being done by Arm, the
following people and organizations have contributed to Arm RAN
Acceleration Library:

- Work on `armral_ldpc_rate_recovery` to correctly set the
  log-likelihood ratios of filler bits was contributed upstream by
  4g5g Consultants. See
  <https://gitlab.arm.com/networking/ral/-/merge_requests/6>.

- Work on `armral_ldpc_rate_matching` and `armral_ldpc_rate_recovery`
  to support the addition and removal of filler bits when the soft
  buffer size is less than the full buffer size was contributed
  upstream by 4g5g Consultants. See
  <https://gitlab.arm.com/networking/ral/-/merge_requests/5>.

- Work on `armral_ldpc_encode_block`, `armral_ldpc_rate_matching` and
  `armral_ldpc_rate_recovery` to support the addition and removal of
  filler bits when the code block size is not a multiple of lifting
  set size was contributed upstream by 4g5g Consultants. See
  <https://gitlab.arm.com/networking/ral/-/merge_requests/4>.

- Work on `armral_seq_generator` to extend the `sequence_len`
  parameter to `uint32_t` was contributed upstream by 4g5g
  Consultants. See
  <https://gitlab.arm.com/networking/ral/-/merge_requests/3>.

- Work on `armral_polar_rate_matching` and
  `armral_polar_rate_recovery` to enable or disable bit interleaving
  was contributed upstream by 4g5g Consultants. See
  <https://gitlab.arm.com/networking/ral/-/merge_requests/2>.

- Work on `armral_ldpc_rate_matching` and `armral_ldpc_rate_recovery`
  to support soft buffer sizes was contributed upstream by 4g5g
  Consultants. See
  <https://gitlab.arm.com/networking/ral/-/merge_requests/1>.
