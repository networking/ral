/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include <arm_neon.h>
#include <stdint.h>

#ifdef __ARM_FEATURE_SVE
#include <arm_sve.h>
#endif

static inline int8x8_t vld1s_s8(const int8_t *ptr) {
  int8x8_t ret;
  asm("ldr %s0, %1" : "=w"(ret) : "m"(*(const int32_t *)ptr));
  return ret;
}

static inline int8x16_t vld1sq_s8(const int8_t *ptr) {
  int8x16_t ret;
  asm("ldr %s0, %1" : "=w"(ret) : "m"(*(const int32_t *)ptr));
  return ret;
}

static inline int8x16_t vld1hq_s8(const int8_t *ptr) {
  int8x16_t ret;
  asm("ldr %h0, %1" : "=w"(ret) : "m"(*(const int16_t *)ptr));
  return ret;
}

static inline uint8x8_t vld1s_u8(const uint8_t *ptr) {
  uint8x8_t ret;
  asm("ldr %s0, %1" : "=w"(ret) : "m"(*(const uint32_t *)ptr));
  return ret;
}

static inline uint8x8_t vld1h_u8(const uint8_t *ptr) {
  uint8x8_t ret;
  asm("ldr %h0, %1" : "=w"(ret) : "m"(*(const uint16_t *)ptr));
  return ret;
}

static inline uint16x4_t vld1s_u16(const uint16_t *ptr) {
  uint16x4_t ret;
  asm("ldr %s0, %1" : "=w"(ret) : "m"(*(const uint32_t *)ptr));
  return ret;
}

static inline int8x16_t vzip1l_s8(int8x8_t a, int8x8_t b) {
  int8x16_t ret;
  asm("zip1 %0.16b, %1.16b, %2.16b" : "=w"(ret) : "w"(a), "w"(b));
  return ret;
}

static inline uint8x16_t vzip1l_u8(uint8x8_t a, uint8x8_t b) {
  uint8x16_t ret;
  asm("zip1 %0.16b, %1.16b, %2.16b" : "=w"(ret) : "w"(a), "w"(b));
  return ret;
}

static inline uint16x8_t vzip1l_u16(uint16x4_t a, uint16x4_t b) {
  uint16x8_t ret;
  asm("zip1 %0.8h, %1.8h, %2.8h" : "=w"(ret) : "w"(a), "w"(b));
  return ret;
}

static inline uint32x4_t vzip1l_u32(uint32x2_t a, uint32x2_t b) {
  uint32x4_t ret;
  asm("zip1 %0.4s, %1.4s, %2.4s" : "=w"(ret) : "w"(a), "w"(b));
  return ret;
}

static inline uint8x16_t __attribute__((always_inline, artificial))
vld1d_u8(const uint8_t *src) {
  uint8x16_t result;
  __asm__("ldr %d0, %1"
          : "=w"(result)
          : "m"(*(const uint64_t *)src)
          : /* No clobbers */);
  return result;
}

static inline int32x4_t __attribute__((always_inline, artificial))
vmlal_low_s16(int32x4_t a, int16x8_t b, int16x8_t c) {
#if __GNUC__ >= 11 || __clang__
  return vmlal_s16(a, vget_low_s16(b), vget_low_s16(c));
#else
  int32x4_t result;
  __asm__("smlal %0.4s,%2.4h,%3.4h"
          : "=w"(result)
          : "0"(a), "w"(b), "w"(c)
          : /* No clobbers */);
  return result;
#endif
}

static inline int64x2_t __attribute__((always_inline, artificial))
vmlal_low_s32(int64x2_t a, int32x4_t b, int32x4_t c) {
#if __GNUC__ >= 11 || __clang__
  return vmlal_s32(a, vget_low_s32(b), vget_low_s32(c));
#else
  int64x2_t result;
  __asm__("smlal %0.2d,%2.2s,%3.2s"
          : "=w"(result)
          : "0"(a), "w"(b), "w"(c)
          : /* No clobbers */);
  return result;
#endif
}

#if __GNUC__ >= 11 || __clang__
// NOLINTNEXTLINE(readability-identifier-naming)
#define vmlal_low_laneq_s32(a, b, c, lane)                                     \
  vmlal_laneq_s32(a, vget_low_s32(b), c, lane)
#else
static inline int64x2_t __attribute__((always_inline, artificial))
vmlal_low_laneq_s32(int64x2_t a, int32x4_t b, int32x4_t c, const int lane) {
  int64x2_t result;
  __asm__("smlal %0.2d,%2.2s,%3.2s[%4]"
          : "=w"(result)
          : "0"(a), "w"(b), "w"(c), "M"(lane)
          : /* No clobbers */);
  return result;
}
#endif

static inline int32x4_t __attribute__((always_inline, artificial))
vmlsl_low_s16(int32x4_t a, int16x8_t b, int16x8_t c) {
#if __GNUC__ >= 11 || __clang__
  return vmlsl_s16(a, vget_low_s16(b), vget_low_s16(c));
#else
  int32x4_t result;
  __asm__("smlsl %0.4s,%2.4h,%3.4h"
          : "=w"(result)
          : "0"(a), "w"(b), "w"(c)
          : /* No clobbers */);
  return result;
#endif
}

static inline int64x2_t __attribute__((always_inline, artificial))
vmlsl_low_s32(int64x2_t a, int32x4_t b, int32x4_t c) {
#if __GNUC__ >= 11 || __clang__
  return vmlsl_s32(a, vget_low_s32(b), vget_low_s32(c));
#else
  int64x2_t result;
  __asm__("smlsl %0.2d,%2.2s,%3.2s"
          : "=w"(result)
          : "0"(a), "w"(b), "w"(c)
          : /* No clobbers */);
  return result;
#endif
}

static inline int32x4_t __attribute__((always_inline, artificial))
vmovl_low_s16(int16x8_t a) {
#if __GNUC__ >= 11 || __clang__
  return vmovl_s16(vget_low_s16(a));
#else
  int32x4_t result;
  __asm__("sshll %0.4s,%1.4h,#0" : "=w"(result) : "w"(a) : /* No clobbers */);
  return result;
#endif
}

static inline int32x4_t __attribute__((always_inline, artificial))
vmull_low_s16(int16x8_t a, int16x8_t b) {
#if __GNUC__ >= 11 || __clang__
  return vmull_s16(vget_low_s16(a), vget_low_s16(b));
#else
  int32x4_t result;
  __asm__("smull %0.4s,%1.4h,%2.4h"
          : "=w"(result)
          : "w"(a), "w"(b)
          : /* No clobbers */);
  return result;
#endif
}

static inline int64x2_t __attribute__((always_inline, artificial))
vmull_low_s32(int32x4_t a, int32x4_t b) {
#if __GNUC__ >= 11 || __clang__
  return vmull_s32(vget_low_s32(a), vget_low_s32(b));
#else
  int64x2_t result;
  __asm__("smull %0.2d,%1.2s,%2.2s"
          : "=w"(result)
          : "w"(a), "w"(b)
          : /* No clobbers */);
  return result;
#endif
}

#if __GNUC__ >= 11 || __clang__
// NOLINTNEXTLINE(readability-identifier-naming)
#define vmull_low_laneq_s32(a, b, lane)                                        \
  vmull_laneq_s32(vget_low_s32(a), b, lane);
#else
static inline int64x2_t __attribute__((always_inline, artificial))
vmull_low_laneq_s32(int32x4_t a, int32x4_t b, const int lane) {
  int64x2_t result;
  __asm__("smull %0.2d,%1.2s,%2.4s[%3]"
          : "=w"(result)
          : "w"(a), "w"(b), "M"(lane)
          : /* No clobbers */);
  return result;
}
#endif

static inline int32x4_t __attribute__((always_inline, artificial))
vqdmlal_low_s16(int32x4_t a, int16x8_t b, int16x8_t c) {
#if __GNUC__ >= 11 || __clang__
  return vqdmlal_s16(a, vget_low_s16(b), vget_low_s16(c));
#else
  int32x4_t result;
  __asm__("sqdmlal %0.4s,%2.4h,%3.4h"
          : "=w"(result)
          : "0"(a), "w"(b), "w"(c)
          : /* No clobbers */);
  return result;
#endif
}

#if __GNUC__ >= 11 || __clang__
// NOLINTNEXTLINE(readability-identifier-naming)
#define vqdmlal_low_lane_s16(a, b, c, lane)                                    \
  vqdmlal_lane_s16(a, vget_low_s16(b), c, lane)
#else
static inline int32x4_t __attribute__((always_inline, artificial))
vqdmlal_low_lane_s16(int32x4_t a, int16x8_t b, int16x4_t c, const int lane) {
  int32x4_t result;
  __asm__("sqdmlal %0.4s,%2.4h,%3.h[%4]"
          : "=w"(result)
          : "0"(a), "w"(b), "w"(c), "M"(lane)
          : /* No clobbers */);
  return result;
}
#endif

static inline int32x4_t __attribute__((always_inline, artificial))
vqdmlsl_low_s16(int32x4_t a, int16x8_t b, int16x8_t c) {
#if __GNUC__ >= 11 || __clang__
  return vqdmlsl_s16(a, vget_low_s16(b), vget_low_s16(c));
#else
  int32x4_t result;
  __asm__("sqdmlsl %0.4s,%2.4h,%3.4h"
          : "=w"(result)
          : "0"(a), "w"(b), "w"(c)
          : /* No clobbers */);
  return result;
#endif
}

static inline int32x4_t __attribute__((always_inline, artificial))
vqdmull_low_s16(int16x8_t a, int16x8_t b) {
#if __GNUC__ >= 11 || __clang__
  return vqdmull_s16(vget_low_s16(a), vget_low_s16(b));
#else
  int32x4_t result;
  __asm__("sqdmull %0.4s,%1.4h,%2.4h"
          : "=w"(result)
          : "w"(a), "w"(b)
          : /* No clobbers */);
  return result;
#endif
}

#if __GNUC__ >= 11 || __clang__
// NOLINTNEXTLINE(readability-identifier-naming)
#define vqdmull_low_lane_s16(a, b, lane)                                       \
  vqdmull_lane_s16(vget_low_s16(a), b, lane)
#else
static inline int32x4_t __attribute__((always_inline, artificial))
vqdmull_low_lane_s16(int16x8_t a, int16x4_t b, const int lane) {
  int32x4_t result;
  __asm__("sqdmull %0.4s,%1.4h,%2.h[%3]"
          : "=w"(result)
          : "w"(a), "w"(b), "M"(lane)
          : /* No clobbers */);
  return result;
}
#endif

static inline int32x4_t __attribute__((always_inline, artificial))
vmlal_low_n_s16(int32x4_t a, int16x8_t b, int16_t c) {
#if __GNUC__ >= 11 || __clang__
  return vmlal_n_s16(a, vget_low_s16(b), c);
#else
  int32x4_t result;
  __asm__("smlal %0.4s,%2.4h,%3.h[0]"
          : "=w"(result)
          : "0"(a), "w"(b), "x"(c)
          : /* No clobbers */);
  return result;
#endif
}

static inline int32x4_t __attribute__((always_inline, artificial))
vmlsl_low_n_s16(int32x4_t a, int16x8_t b, int16_t c) {
#if __GNUC__ >= 11 || __clang__
  return vmlsl_n_s16(a, vget_low_s16(b), c);
#else
  int32x4_t result;
  __asm__("smlsl %0.4s, %2.4h, %3.h[0]"
          : "=w"(result)
          : "0"(a), "w"(b), "x"(c)
          : /* No clobbers */);
  return result;
#endif
}

static inline int8x16_t __attribute__((always_inline, artificial))
vtbl1q_s8(int8x8_t a, int8x16_t idx) {
  int8x16_t result;
  asm("tbl %0.16b, {%1.16b}, %2.16b"
      : "=w"(result)
      : "w"(a), "w"(idx)
      : /* No clobbers */);
  return result;
}

static inline void __attribute__((always_inline, artificial))
// NOLINTNEXTLINE(readability-non-const-parameter)
vstps_u8(uint8_t *ptr, uint8x8_t a, uint8x8_t b) {
  // clang does not support Ump constraint, so use Q instead as a fallback.
  // for the list of constraints supported, see:
  // https://llvm.org/docs/LangRef.html#supported-constraint-code-list
  // https://gcc.gnu.org/onlinedocs/gcc/Machine-Constraints.html#Machine-Constraints
#if __clang__
  asm("stp %s1, %s2, %0" : "=Q"(*ptr) : "w"(a), "w"(b));
#else
  asm("stp %s1, %s2, %0" : "=Ump"(*ptr) : "w"(a), "w"(b));
#endif
}

static inline int16_t __attribute__((always_inline)) sat(int32_t a) {
  if (a > INT16_MAX) {
    return INT16_MAX;
  }
  if (a < INT16_MIN) {
    return INT16_MIN;
  }
  return a;
}

static inline int8_t __attribute__((always_inline)) sat_8(int16_t a) {
  if (a > INT8_MAX) {
    return INT8_MAX;
  }
  if (a < INT8_MIN) {
    return INT8_MIN;
  }
  return a;
}

#if !defined(__clang__) && __GNUC__ <= 7
// For compatibility with GCC 7 we define vld1q_f32_x2/vst1q_f32_x2, though
// these implementations are not as good as the real intrinsics.

static inline float32x4x2_t __attribute__((always_inline, artificial))
vld1q_f32_x2(float32_t const *ptr) {
  float32x4_t lo = vld1q_f32(ptr);
  float32x4_t hi = vld1q_f32(ptr + 4);
  return (struct float32x4x2_t){{lo, hi}};
}

static inline float32x2x2_t __attribute__((always_inline, artificial))
vld1_f32_x2(float32_t const *ptr) {
  float32x2_t lo = vld1_f32(ptr);
  float32x2_t hi = vld1_f32(ptr + 2);
  return (struct float32x2x2_t){{lo, hi}};
}

static inline void __attribute__((always_inline, artificial))
vst1q_f32_x2(float32_t *dest, float32x4x2_t value) {
  vst1q_f32(dest, value.val[0]);
  vst1q_f32(dest + 4, value.val[1]);
}
#endif

#ifdef __ARM_FEATURE_SVE
static inline svuint16_t __attribute__((always_inline, artificial))
svld1rh_u16(svbool_t pg, const uint8_t *ptr) {
  svuint16_t ret;
  asm("ld1rh {%0.h}, %1/Z, %2"
      : "=w"(ret)
      : "Upl"(pg), "Q"(*(const uint16_t *)ptr));
  return ret;
}

static inline svuint32_t __attribute__((always_inline, artificial))
svld1rw_u32(svbool_t pg, const uint8_t *ptr) {
  svuint32_t ret;
  asm("ld1rw {%0.s}, %1/Z, %2"
      : "=w"(ret)
      : "Upl"(pg), "Q"(*(const uint32_t *)ptr));
  return ret;
}

static inline svuint64_t __attribute__((always_inline, artificial))
svld1rd_u64(svbool_t pg, const uint8_t *ptr) {
  svuint64_t ret;
  asm("ld1rd {%0.d}, %1/Z, %2"
      : "=w"(ret)
      : "Upl"(pg), "Q"(*(const uint64_t *)ptr));
  return ret;
}

// Reverses pairs of floats within a SVE vector
// [a.re a.im b.re b.im] --> [a.im a.re b.im b.re]
static inline svfloat32_t __attribute__((always_inline, artificial))
svrev64_f32(svbool_t pg, svfloat32_t vec) {
  return svreinterpret_f32_u64(svrevw_u64_x(pg, svreinterpret_u64_f32(vec)));
}

static inline svbool_t __attribute__((always_inline, artificial))
svldr_b(const uint8_t *p_src) {
  // There is no ACLE function to load a predicate directly.
  return *(const svbool_t *)p_src;
}

// Reverses the order of complex numbers
// [a.re a.im b.re b.im] --> [b.re b.im a.re a.im]
static inline svfloat32_t __attribute__((always_inline, artificial))
svrev_cmplx_f32(svfloat32_t vec) {
  return svreinterpret_f32_f64(svrev_f64(svreinterpret_f64_f32(vec)));
}

// Interleaves even-indexed complex numbers
// [a.re a.im b.re b.im], [c.re c.im d.re d.im] -->
// [a.re a.im c.re c.im]
static inline svfloat32_t __attribute__((always_inline, artificial))
svtrn1_cmplx_f32(svfloat32_t vec1, svfloat32_t vec2) {
  return svreinterpret_f32_f64(
      svtrn1_f64(svreinterpret_f64_f32(vec1), svreinterpret_f64_f32(vec2)));
}

// Interleaves odd-indexed complex numbers
// [a.re a.im b.re b.im], [c.re c.im d.re d.im] -->
// [b.re b.im d.re d.im]
static inline svfloat32_t __attribute__((always_inline, artificial))
svtrn2_cmplx_f32(svfloat32_t vec1, svfloat32_t vec2) {
  return svreinterpret_f32_f64(
      svtrn2_f64(svreinterpret_f64_f32(vec1), svreinterpret_f64_f32(vec2)));
}
#endif

// Negates 64 bit elements (every second float) in a 128-bit Neon vector.
static inline float32x4_t __attribute__((always_inline, artificial))
vnegq64_f32(float32x4_t vec) {
  return vreinterpretq_f32_f64(vnegq_f64(vreinterpretq_f64_f32(vec)));
}

// Negates 64 bit elements (every second float) in a 64-bit Neon vector.
static inline float32x2_t __attribute__((always_inline, artificial))
vneg64_f32(float32x2_t vec) {
  return vreinterpret_f32_f64(vneg_f64(vreinterpret_f64_f32(vec)));
}
