/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#if ARMRAL_ARCH_SVE >= 2
template<uint32_t N>
static inline void pack_9bit_and_store_int16(svbool_t pg,
                                             const svint16_t *reg[],
                                             armral_compressed_data_9bit *out) {
  // Pack 24 9-bit values into 27 bytes
  // [-------Aaaaaaaaa | -------Bbbbbbbbb | -------Ccccccccc | ... ]
  // -->
  // [Aaaaaaaa | aBbbbbbb | bbCcccccc | cccDdddd | ... ]

  // Set true predicate corresponding to 9x8-bit elements
  const svbool_t pg9_b8 = svwhilelt_b8(0, 9);

  const svuint8_t indices_07 = svindex_u8(0, 2);
  const svuint8_t indices_18 = svreinterpret_u8_s8(svindex_s8(-2, 2));

  const svuint8_t right_shifts =
      svdupq_n_u8(0, 1, 2, 3, 4, 5, 6, 7, 8, 0, 0, 0, 0, 0, 0, 0);
  const svuint8_t left_shifts =
      svlsl_u8_x(pg9_b8, svdup_n_u8(1), svreinterpret_u8_s8(svindex_s8(8, -1)));

  for (uint32_t i = 0; i < N; i++) {
    int8_t *dst = out[i].mantissa;
    svint16_t reg1 = *reg[i * 3];
    svint16_t reg2 = *reg[1 + i * 3];
    svint16_t reg3 = *reg[2 + i * 3];

    // px_07 holds the 8 most significant bits of each 9-bit value
    svuint8_t p1_07 =
        svtbl_u8(svreinterpret_u8_s16(svasr_n_s16_x(pg, reg1, 1)), indices_07);
    svuint8_t p2_07 =
        svtbl_u8(svreinterpret_u8_s16(svasr_n_s16_x(pg, reg2, 1)), indices_07);
    svuint8_t p3_07 =
        svtbl_u8(svreinterpret_u8_s16(svasr_n_s16_x(pg, reg3, 1)), indices_07);

    // px_18 holds the 8 least significant bits of each 9-bit value
    svuint8_t p1_18 = svtbl_u8(svreinterpret_u8_s16(reg1), indices_18);
    svuint8_t p2_18 = svtbl_u8(svreinterpret_u8_s16(reg2), indices_18);
    svuint8_t p3_18 = svtbl_u8(svreinterpret_u8_s16(reg3), indices_18);

    // Shift right by {0, 1, 2, 3, 4, 5, 6, 7, 8}.
    // The 9th element of px_07 (which is 0) is shifted rather than defining
    // another predicate to ignore it.
    p1_07 = svlsr_u8_x(pg9_b8, p1_07, right_shifts);
    p2_07 = svlsr_u8_x(pg9_b8, p2_07, right_shifts);
    p3_07 = svlsr_u8_x(pg9_b8, p3_07, right_shifts);

    // px_18 is shifted left by multiplying by
    // {2^8, 2^7, 2^6, 2^5, 2^4, 2^3, 2^2, 2^1, 2^0}.
    // The first element of px_18 (which is 0) is shifted rather than defining
    // another predicate to ignore it.
    // LSL followed by ORR is equivalent to MLA in this case, since at most one
    // bit is set in each position in the addends.
    p1_07 = svmla_u8_x(pg9_b8, p1_07, p1_18, left_shifts);
    p2_07 = svmla_u8_x(pg9_b8, p2_07, p2_18, left_shifts);
    p3_07 = svmla_u8_x(pg9_b8, p3_07, p3_18, left_shifts);

    svst1_s8(pg9_b8, dst, svreinterpret_s8_u8(p1_07));
    svst1_s8(pg9_b8, dst + 9, svreinterpret_s8_u8(p2_07));
    svst1_s8(pg9_b8, dst + 18, svreinterpret_s8_u8(p3_07));
  }
}

template<bool is_block_float>
static inline void pack_14bit_and_store_int16(svbool_t pg,
                                              const svint16_t *regs[],
                                              armral_compressed_data_14bit *out,
                                              const unsigned int exp = {}) {
  const svbool_t pg14 = svwhilelt_b8(0, 14);

  svuint16_t shift_left = svdupq_n_u16(2, 4, 6, 8, 2, 4, 6, 8);
  svuint16_t shift_right = svdupq_n_u16(0, 4, 2, 0, 0, 4, 2, 0);

  const svuint8_t contig_idx =
      svdupq_n_u8(1, 0, 3, 2, 5, 4, 7, 9, 8, 11, 10, 13, 12, 15, 14, 255);
  const svuint8_t contig2_idx = svdupq_n_u8(
      255, 3, 255, 5, 255, 7, 255, 255, 11, 255, 13, 255, 15, 255, 255, 255);
  const svuint8_t sbli = svdupq_n_u8(255, 252, 255, 240, 255, 192, 255, 255,
                                     252, 255, 240, 255, 192, 255, 255, 255);

  if constexpr (is_block_float) {
    shift_left = svsub_n_u16_x(pg, shift_left, exp);
    shift_right = svadd_n_u16_x(pg, shift_right, exp);
  }

  int8_t *dst = out[0].mantissa;

  for (uint32_t i = 0; i < 3; i++) {
    // in the following comments we refer to A, B, C and D:
    //   A = reg1[0]
    //   B = reg1[1]
    //   C = reg1[2]
    //   D = reg1[3]
    // from E onwards the patterns repeat so we only consider A to D.
    // compression goes from
    // [Dddddddddddddddd|Cccccccccccccccc|Bbbbbbbbbbbbbbbb|Aaaaaaaaaaaaaaaa]
    // (capital indicates msb of original data)
    // to
    // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
    svuint16_t reg = svreinterpret_u16_s16(*regs[i]);

    // Move the 14 bits of A, 12 LSBs of B, 10 LSBs of C and 8 LSBs of D to the
    // top of the 16-bit elements
    // [dddddddd00000000|cccccccccc000000|bbbbbbbbbbbb0000|Aaaaaaaaaaaaaa00]
    svuint16_t ucomp_lsh = svlsl_u16_x(pg, reg, shift_left);

    // Move the 2 MSBs of B and 4 MSBs of C to where they need to be for
    // reordering
    // [--Dddddddddddddd|00--Cccccccccccc|0000--Bbbbbbbbbb|--Aaaaaaaaaaaaaa]
    reg = svlsr_u16_x(pg, reg, shift_right);

    // Reorder left-shifted data so all bits of A and LSBs of B, C and D are in
    // correct position
    // [dddddddd|cc000000|cccccccc|bbbb0000|bbbbbbbb|aaaaaa00|Aaaaaaaa]
    svuint8_t contig = svtbl_u8(svreinterpret_u8_u16(ucomp_lsh), contig_idx);

    // Reorder right-shifted data so that MSBs of B, C and D are in correct
    // position [00000000|--Dddddd|00000000|00--Cccc|00000000|0000--Bb|00000000]
    svuint8_t contig2 = svtbl_u8(svreinterpret_u8_u16(reg), contig2_idx);

    // Combine MSBs and LSBs using mask
    // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
    svuint8_t res = svbsl_u8(contig, contig2, sbli);

    // Write 42x8-bit elements using predication to store 14 bytes at a time
    svst1_s8(pg14, dst, svreinterpret_s8_u8(res));
    dst += 14;
  }
}

#else
template<uint32_t N>
static inline void pack_9bit_and_store_int16(const int16x8x3_t reg[],
                                             armral_compressed_data_9bit *out) {
  int8x8_t d07_shifts = {0, -1, -2, -3, -4, -5, -6, -7};
  int8x8_t d18_shifts = {7, 6, 5, 4, 3, 2, 1, 0};

  for (uint32_t i = 0; i < N; i++) {
    int8_t *dst = out[i].mantissa;
    for (uint32_t j = 0; j < 3; j++) {
      uint16x8_t ureg = vreinterpretq_u16_s16(reg[i].val[j]);

      // d07 holds the 8 most significant bits of each 9-bit value
      // [Aaaaaaaa | Bbbbbbbb | Cccccccc | ...]
      uint8x8_t d07 = vmovn_u16(ureg >> 1);
      // d18 holds the 8 least significant bits of each 9-bit value
      // [aaaaaaaa | bbbbbbbb | cccccccc | ...]
      uint8x8_t d18 = vmovn_u16(ureg);

      // [Aaaaaaaa | 0Bbbbbbb | 00Cccccc | ...]
      d07 = vshl_u8(d07, d07_shifts);
      // [a0000000 | bb000000 | ccc00000 | ...]
      d18 = vshl_u8(d18, d18_shifts);

      // The final element is now correct, so store it
      vst1_lane_s8(dst + 8, vreinterpret_s8_u8(d18), 7);

      // ORR d07 with [00000000 | a0000000 | bb000000 | ccc00000 | ...]
      d07 = vorr_u8(d07, vext_u8(vdup_n_u8(0), d18, 7));
      vst1_s8(dst, vreinterpret_s8_u8(d07));

      dst += 9;
    }
  }
}

template<uint32_t N, bool is_block_float>
static inline void pack_14bit_and_store_int16(const int16x8x3_t reg[],
                                              armral_compressed_data_14bit *out,
                                              const int exps[] = {}) {
  // Shifts, indices and mask used during bit-packing
  uint16x8_t fixed_shift_left = {2, 4, 6, 8, 2, 4, 6, 8};
  uint16x8_t fixed_shift_right = {0, 4, 2, 0, 0, 4, 2, 0};
  uint8x16_t contig1_12_idx = {1, 0,  3,  2,  5,  4,  7,   9,
                               8, 11, 10, 13, 12, 15, 255, 255};
  uint8x16_t contig1_3_idx = {255, 255, 1, 0,  3,  2,  5,  4,
                              7,   9,   8, 11, 10, 13, 12, 15};
  uint8x16_t contig2_12_idx = {255, 3,   255, 5,   255, 7,   255, 255,
                               11,  255, 13,  255, 15,  255, 255, 255};
  uint8x16_t contig2_3_idx = {255, 255, 255, 3,   255, 5,   255, 7,
                              255, 255, 11,  255, 13,  255, 15,  255};
  uint8x16_t sbli12 = {0xff, 0xfc, 0xff, 0xf0, 0xff, 0xc0, 0xff, 0xff,
                       0xfc, 0xff, 0xf0, 0xff, 0xc0, 0xff, 0xff, 0xff};
  uint8x16_t sbli3 = {0xff, 0xff, 0xff, 0xfc, 0xff, 0xf0, 0xff, 0xc0,
                      0xff, 0xff, 0xfc, 0xff, 0xf0, 0xff, 0xc0, 0xff};
  uint8x16_t contig1_array[] = {contig1_12_idx, contig1_12_idx, contig1_3_idx};
  uint8x16_t contig2_array[] = {contig2_12_idx, contig2_12_idx, contig2_3_idx};
  uint8x16_t sbli_array[] = {sbli12, sbli12, sbli3};
  uint16x8_t shift_left = fixed_shift_left;
  uint16x8_t shift_right = fixed_shift_right;

  for (uint32_t i = 0; i < N; i++) {
    int8_t *dst = out[i].mantissa;

    uint16x8x3_t ureg_lsh;
    uint16x8x3_t ureg_rsh;
    uint8x16x3_t contig;
    uint8x16x3_t contig2;
    int8x16x3_t result;

    if constexpr (is_block_float) {
      shift_left = vsubq_u16(fixed_shift_left, vdupq_n_u16(exps[i]));
      shift_right = vaddq_u16(fixed_shift_right, vdupq_n_u16(exps[i]));
    }

    for (uint32_t j = 0; j < 3; j++) {

      // Move the 16 bits of A, 14 LSBs of B, 12 LSBs of C and 10 LSBs of D to
      // the top of the 16-bit elements
      // [ddddddddxx000000|ccccccccccxx0000|bbbbbbbbbbbbxx00|Aaaaaaaaaaaaaaxx]
      // (x denotes bits we will ignore when merging later)
      ureg_lsh.val[j] = vreinterpretq_u16_s16(reg[i].val[j]) << shift_left;

      // Shift A and D by exp into bottom 14-bits of each 16-bit lane,
      // and the 2 MSBs of B and 4 MSBs of C to where they need to be for
      // reordering
      // [00Dddddddddddddd|0000Cccccccccccc|000000Bbbbbbbbbb|00Aaaaaaaaaaaaaa]
      ureg_rsh.val[j] = vreinterpretq_u16_s16(reg[i].val[j]) >> shift_right;

      // Reorder left-shifted data so all bits of A and LSBs of B, C and D are
      // in correct position
      // [dddddddd|ccxx0000|cccccccc|bbbbxx00|bbbbbbbb|aaaaaaxx|Aaaaaaaa]
      contig.val[j] =
          vqtbl1q_u8(vreinterpretq_u8_u16(ureg_lsh.val[j]), contig1_array[j]);

      // Reorder right-shifted data so that MSBs of B, C and D are in correct
      // position
      // [00000000|00Dddddd|00000000|0000Cccc|00000000|000000Bb|00000000]
      contig2.val[j] =
          vqtbl1q_u8(vreinterpretq_u8_u16(ureg_rsh.val[j]), contig2_array[j]);

      // Combine MSBs and LSBs using mask
      // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
      result.val[j] = vreinterpretq_s8_u8(
          vbslq_u8(sbli_array[j], contig.val[j], contig2.val[j]));
    }

    result.val[2] = vreinterpretq_s8_u16(
        vcopyq_laneq_u16(vreinterpretq_u16_s8(result.val[2]), 0,
                         vreinterpretq_u16_s8(result.val[1]), 6));

    // write 42-bytes
    vst1q_s8(dst, result.val[0]);
    vst1q_s8(&dst[14], result.val[1]);
    vst1q_s8(&dst[26], result.val[2]);
  }
}

#endif
