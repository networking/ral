/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#if ARMRAL_ARCH_SVE >= 2
#include <arm_sve.h>
#endif

#include "../bit_packing_common.hpp"
#include "utils/vec_mul.hpp"

#if ARMRAL_ARCH_SVE >= 2
static inline unsigned int calculate_exp_sve_128(svbool_t pg, svint16_t reg1,
                                                 svint16_t reg2, svint16_t reg3,
                                                 int iq_width) {
  // Calculate number of redundant sign bits
  svuint16_t bits1 = svcls_s16_x(pg, reg1);
  svuint16_t bits2 = svcls_s16_x(pg, reg2);
  svuint16_t bits3 = svcls_s16_x(pg, reg3);

  // Min across vectors
  svuint16_t minv = svmin_u16_x(pg, svmin_u16_x(pg, bits1, bits2), bits3);
  int leading_sign_bits = svminv_u16(pg, minv);

  // Calc final exp
  int exp = 16 - leading_sign_bits - iq_width;
  return exp < 0 ? 0 : exp;
}
#endif

#if !defined(ARMRAL_ARCH_SVE) || ARMRAL_ARCH_SVE < 2
static inline int calculate_exp(int16x8x3_t in, int iq_width) {
  // Calculate number of redundant sign bits
  int16x8_t reg1 = vclsq_s16(in.val[0]);
  int16x8_t reg2 = vclsq_s16(in.val[1]);
  int16x8_t reg3 = vclsq_s16(in.val[2]);

  // Min across vectors
  int16x8_t minv = vminq_s16(vminq_s16(reg1, reg2), reg3);
  int leading_sign_bits = vminvq_s16(minv);

  // Calc final exp
  int exp = 16 - leading_sign_bits - iq_width;
  return exp < 0 ? 0 : exp;
}
#endif

armral_status armral_block_float_compr_8bit(uint32_t n_prb,
                                            const armral_cmplx_int16_t *src,
                                            armral_compressed_data_8bit *dst,
                                            const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const int16_t *data_in = (const int16_t *)src;

  /*Set true predicate corresponding to 8x 16-bit elements*/
  /*(128 bits in total)*/
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  svint16_t scale_im = svdup_n_s16(0);
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }
  for (int32_t num_prb = 0; num_prb < (int32_t)n_prb - 1; num_prb += 2) {
    /*Load 2 PRB = 24 complex symbols*/
    svint16_t reg1 = svld1_s16(pg, data_in);
    svint16_t reg2 = svld1_s16(pg, &data_in[8]);
    svint16_t reg3 = svld1_s16(pg, &data_in[16]);
    svint16_t reg4 = svld1_s16(pg, &data_in[24]);
    svint16_t reg5 = svld1_s16(pg, &data_in[32]);
    svint16_t reg6 = svld1_s16(pg, &data_in[40]);

    if (scale != nullptr) {
      reg1 = sv_cmplx_mul_combined_re_im(reg1, scale->re, scale_im);
      reg2 = sv_cmplx_mul_combined_re_im(reg2, scale->re, scale_im);
      reg3 = sv_cmplx_mul_combined_re_im(reg3, scale->re, scale_im);
      reg4 = sv_cmplx_mul_combined_re_im(reg4, scale->re, scale_im);
      reg5 = sv_cmplx_mul_combined_re_im(reg5, scale->re, scale_im);
      reg6 = sv_cmplx_mul_combined_re_im(reg6, scale->re, scale_im);
    }

    data_in += 48;
    const unsigned int exp[] = {calculate_exp_sve_128(pg, reg1, reg2, reg3, 8),
                                calculate_exp_sve_128(pg, reg4, reg5, reg6, 8)};
    dst[0].exp = (int16_t)exp[0];
    dst[1].exp = (int16_t)exp[1];

    int8_t *data_out[] = {dst[0].mantissa, dst[1].mantissa};
    svint16_t shifted_output1 = svasr_n_s16_x(pg, reg1, exp[0]);
    svint16_t shifted_output2 = svasr_n_s16_x(pg, reg4, exp[1]);
    /*Store truncating to 8bits*/
    svst1b_s16(pg, data_out[0], shifted_output1);
    svst1b_s16(pg, data_out[1], shifted_output2);
    data_out[0] = data_out[0] + 8;
    data_out[1] = data_out[1] + 8;
    shifted_output1 = svasr_n_s16_x(pg, reg2, exp[0]);
    shifted_output2 = svasr_n_s16_x(pg, reg5, exp[1]);
    svst1b_s16(pg, data_out[0], shifted_output1);
    svst1b_s16(pg, data_out[1], shifted_output2);
    data_out[0] = data_out[0] + 8;
    data_out[1] = data_out[1] + 8;
    shifted_output1 = svasr_n_s16_x(pg, reg3, exp[0]);
    shifted_output2 = svasr_n_s16_x(pg, reg6, exp[1]);
    svst1b_s16(pg, data_out[0], shifted_output1);
    svst1b_s16(pg, data_out[1], shifted_output2);

    /*Next compressed struct*/
    dst += 2;
  }
  if (n_prb % 2 != 0) {
    /*Load 1 PRB = 12 complex symbols*/
    svint16_t reg1 = svld1_s16(pg, data_in);
    svint16_t reg2 = svld1_s16(pg, &data_in[8]);
    svint16_t reg3 = svld1_s16(pg, &data_in[16]);

    if (scale != nullptr) {
      reg1 = sv_cmplx_mul_combined_re_im(reg1, scale->re, scale_im);
      reg2 = sv_cmplx_mul_combined_re_im(reg2, scale->re, scale_im);
      reg3 = sv_cmplx_mul_combined_re_im(reg3, scale->re, scale_im);
    }

    data_in += 24;
    unsigned int exp = calculate_exp_sve_128(pg, reg1, reg2, reg3, 8);
    dst->exp = (int16_t)exp;

    int8_t *data_out = dst->mantissa;
    svint16_t shifted_output = svasr_n_s16_x(pg, reg1, exp);
    /*Store truncating to 8bits*/
    svst1b_s16(pg, data_out, shifted_output);
    data_out = data_out + 8;
    shifted_output = svasr_n_s16_x(pg, reg2, exp);
    svst1b_s16(pg, data_out, shifted_output);
    data_out = data_out + 8;
    shifted_output = svasr_n_s16_x(pg, reg3, exp);
    svst1b_s16(pg, data_out, shifted_output);

    /*Next compressed struct*/
    dst++;
  }
  return ARMRAL_SUCCESS;
#else
  const int16_t *data_in = (const int16_t *)src;

  for (int32_t num_prb = 0; num_prb < (int32_t)n_prb - 1; num_prb += 2) {
    /*Load 2 PRBs = 24 complex symbols*/
    int16x8x3_t in[2];
    if (scale != nullptr) {
      in[0] = load3_cmplx_and_scale(data_in, *scale);
      in[1] = load3_cmplx_and_scale(data_in + 24, *scale);
    } else {
      in[0].val[0] = vld1q_s16(data_in);
      in[0].val[1] = vld1q_s16(&data_in[8]);
      in[0].val[2] = vld1q_s16(&data_in[16]);
      in[1].val[0] = vld1q_s16(&data_in[24]);
      in[1].val[1] = vld1q_s16(&data_in[32]);
      in[1].val[2] = vld1q_s16(&data_in[40]);
    }
    data_in += 48;

    const int exp[] = {calculate_exp(in[0], 8), calculate_exp(in[1], 8)};
    dst[0].exp = exp[0];
    dst[1].exp = exp[1];
    int16x8_t exp_vec[] = {vmovq_n_s16(-exp[0]), vmovq_n_s16(-exp[1])};

    int8_t *data_out[] = {dst[0].mantissa, dst[1].mantissa};
    int16x8_t shifted_output[] = {vshlq_s16(in[0].val[0], exp_vec[0]),
                                  vshlq_s16(in[1].val[0], exp_vec[1])};
    int8x8_t compressed_output[] = {vmovn_s16(shifted_output[0]),
                                    vmovn_s16(shifted_output[1])};
    vst1_s8(data_out[0], compressed_output[0]);
    vst1_s8(data_out[1], compressed_output[1]);
    data_out[0] = data_out[0] + 8;
    data_out[1] = data_out[1] + 8;
    shifted_output[0] = vshlq_s16(in[0].val[1], exp_vec[0]);
    shifted_output[1] = vshlq_s16(in[1].val[1], exp_vec[1]);
    compressed_output[0] = vmovn_s16(shifted_output[0]);
    compressed_output[1] = vmovn_s16(shifted_output[1]);
    vst1_s8(data_out[0], compressed_output[0]);
    vst1_s8(data_out[1], compressed_output[1]);
    data_out[0] = data_out[0] + 8;
    data_out[1] = data_out[1] + 8;
    shifted_output[0] = vshlq_s16(in[0].val[2], exp_vec[0]);
    shifted_output[1] = vshlq_s16(in[1].val[2], exp_vec[1]);
    compressed_output[0] = vmovn_s16(shifted_output[0]);
    compressed_output[1] = vmovn_s16(shifted_output[1]);
    vst1_s8(data_out[0], compressed_output[0]);
    vst1_s8(data_out[1], compressed_output[1]);

    /*Next compressed struct*/
    dst += 2;
  }
  if (n_prb % 2 != 0) {
    /*Load 1 PRB = 12 complex symbols*/
    int16x8x3_t in;
    if (scale != nullptr) {
      in = load3_cmplx_and_scale(data_in, *scale);
    } else {
      in.val[0] = vld1q_s16(data_in);
      in.val[1] = vld1q_s16(&data_in[8]);
      in.val[2] = vld1q_s16(&data_in[16]);
    }
    data_in += 24;

    int exp = calculate_exp(in, 8);
    dst->exp = exp;
    int16x8_t exp_vec = vmovq_n_s16(-exp);

    int8_t *data_out = dst->mantissa;
    int16x8_t shifted_output = vshlq_s16(in.val[0], exp_vec);
    int8x8_t compressed_output = vmovn_s16(shifted_output);
    vst1_s8(data_out, compressed_output);
    data_out = data_out + 8;
    shifted_output = vshlq_s16(in.val[1], exp_vec);
    compressed_output = vmovn_s16(shifted_output);
    vst1_s8(data_out, compressed_output);
    data_out = data_out + 8;
    shifted_output = vshlq_s16(in.val[2], exp_vec);
    compressed_output = vmovn_s16(shifted_output);
    vst1_s8(data_out, compressed_output);
  }
  return ARMRAL_SUCCESS;
#endif
}

armral_status armral_block_float_compr_9bit(uint32_t n_prb,
                                            const armral_cmplx_int16_t *src,
                                            armral_compressed_data_9bit *dst,
                                            const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const int16_t *data_in = (const int16_t *)src;

  // Set true predicate corresponding to 8x 16-bit elements
  // (128 bits in total)
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  svint16_t scale_im = svdup_n_s16(0);
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  for (uint32_t num_prb = 0; num_prb < n_prb; num_prb++) {
    // Load 1 PRB = 12 complex symbols
    svint16_t reg1 = svld1_s16(pg, data_in);
    svint16_t reg2 = svld1_s16(pg, &data_in[8]);
    svint16_t reg3 = svld1_s16(pg, &data_in[16]);
    data_in += 24;

    if (scale != nullptr) {
      reg1 = sv_cmplx_mul_combined_re_im(reg1, scale->re, scale_im);
      reg2 = sv_cmplx_mul_combined_re_im(reg2, scale->re, scale_im);
      reg3 = sv_cmplx_mul_combined_re_im(reg3, scale->re, scale_im);
    }

    unsigned int exp = calculate_exp_sve_128(pg, reg1, reg2, reg3, 9);
    dst->exp = (int16_t)exp;

    reg1 = svasr_n_s16_x(pg, reg1, exp);
    reg2 = svasr_n_s16_x(pg, reg2, exp);
    reg3 = svasr_n_s16_x(pg, reg3, exp);

    const svint16_t *regs[] = {&reg1, &reg2, &reg3};
    pack_9bit_and_store_int16<1>(pg, regs, dst);

    dst++;
  }
  return ARMRAL_SUCCESS;
#else
  const int16_t *data_in = (const int16_t *)src;
  int16x8x3_t reg[2];
  for (int32_t num_prb = 0; num_prb < (int32_t)n_prb - 1; num_prb += 2) {
    /*Load 2 PRBs = 24 complex symbols*/
    int16x8x3_t in_1;
    int16x8x3_t in_2;

    if (scale != nullptr) {
      in_1 = load3_cmplx_and_scale(data_in, *scale);
      in_2 = load3_cmplx_and_scale(data_in + 24, *scale);
    } else {
      in_1.val[0] = vld1q_s16(data_in);
      in_1.val[1] = vld1q_s16(&data_in[8]);
      in_1.val[2] = vld1q_s16(&data_in[16]);
      in_2.val[0] = vld1q_s16(&data_in[24]);
      in_2.val[1] = vld1q_s16(&data_in[32]);
      in_2.val[2] = vld1q_s16(&data_in[40]);
    }

    int exp1 = calculate_exp(in_1, 9);
    int exp2 = calculate_exp(in_2, 9);
    dst[0].exp = exp1;
    dst[1].exp = exp2;

    for (uint32_t i = 0; i < 3; i++) {
      reg[0].val[i] = in_1.val[i] >> exp1;
      reg[1].val[i] = in_2.val[i] >> exp2;
    }

    pack_9bit_and_store_int16<2>(reg, dst);

    dst += 2;
    data_in += 48;
  }
  if (n_prb % 2 != 0) {
    /*Load 1 PRB = 12 complex symbols*/
    int16x8x3_t in;
    if (scale != nullptr) {
      in = load3_cmplx_and_scale(data_in, *scale);
    } else {
      in.val[0] = vld1q_s16(data_in);
      in.val[1] = vld1q_s16(&data_in[8]);
      in.val[2] = vld1q_s16(&data_in[16]);
    }

    int exp = calculate_exp(in, 9);
    dst->exp = exp;

    for (uint32_t i = 0; i < 3; i++) {
      reg[0].val[i] = in.val[i] >> exp;
    }

    pack_9bit_and_store_int16<1>(reg, dst);

    data_in += 24;
  }
  return ARMRAL_SUCCESS;
#endif
}

armral_status
armral_block_float_compr_12bit(uint32_t n_prb, const armral_cmplx_int16_t *src,
                               armral_compressed_data_12bit *dst,
                               const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const int16_t *data_in = (const int16_t *)src;

  // Set true predicate corresponding to 8x 16-bit elements
  // (128 bits in total)
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  svint16_t scale_im = svdup_n_s16(0);
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  // Predicate to select 12x 8-bit (1 byte) elements
  const svbool_t pg12 = svwhilelt_b8(0, 12);

  // tbl indices for building real and imaginary parts
  // only care about 12 out of every 16 bytes here due to compression.
  svuint8_t idx_re = svdupq_n_u8(1, 0, 255, 5, 4, 255, 9, 8, 255, 13, 12, 255,
                                 255, 255, 255, 255);
  svuint8_t idx_im = svdupq_n_u8(255, 3, 2, 255, 7, 6, 255, 11, 10, 255, 15, 14,
                                 255, 255, 255, 255);

  // masks for combining real/imag parts
  svuint8_t sbli = svdupq_n_u8(0xff, 0xf0, 0x0, 0xff, 0xf0, 0x0, 0xff, 0xf0,
                               0x0, 0xff, 0xf0, 0x0, 0, 0, 0, 0);

  for (int32_t num_prb = 0; num_prb < (int32_t)n_prb - 1; num_prb += 2) {
    // load 2 PRBs = 24 complex symbols
    svint16_t reg1 = svld1_s16(pg, data_in);
    svint16_t reg2 = svld1_s16(pg, &data_in[8]);
    svint16_t reg3 = svld1_s16(pg, &data_in[16]);
    svint16_t reg4 = svld1_s16(pg, &data_in[24]);
    svint16_t reg5 = svld1_s16(pg, &data_in[32]);
    svint16_t reg6 = svld1_s16(pg, &data_in[40]);
    data_in += 48;

    if (scale != nullptr) {
      reg1 = sv_cmplx_mul_combined_re_im(reg1, scale->re, scale_im);
      reg2 = sv_cmplx_mul_combined_re_im(reg2, scale->re, scale_im);
      reg3 = sv_cmplx_mul_combined_re_im(reg3, scale->re, scale_im);
      reg4 = sv_cmplx_mul_combined_re_im(reg4, scale->re, scale_im);
      reg5 = sv_cmplx_mul_combined_re_im(reg5, scale->re, scale_im);
      reg6 = sv_cmplx_mul_combined_re_im(reg6, scale->re, scale_im);
    }

    const unsigned int exp[] = {
        calculate_exp_sve_128(pg, reg1, reg2, reg3, 12),
        calculate_exp_sve_128(pg, reg4, reg5, reg6, 12)};
    dst[0].exp = (int16_t)exp[0];
    dst[1].exp = (int16_t)exp[1];

    // the format of the tbl is such that we want real components in the top
    // 12-bits of each 16-bit value, and the imaginary components in the bottom
    // 12-bits of each 16-bit value.
    // with SVE2 intrinsics all shift values are unsigned (unlike NEON) so we
    // have to do left shifts separately from right shifts

    // the left-shifts for the real parts are (4 - exp) and 0 <= exp <= 4
    // we shift everything and discard the unwanted parts in the tbl later
    uint16_t shift_re[2];
    shift_re[0] = 4 - exp[0];
    shift_re[1] = 4 - exp[1];
    svuint16_t ureg1_re =
        svlsl_n_u16_x(pg, svreinterpret_u16_s16(reg1), shift_re[0]);
    svuint16_t ureg2_re =
        svlsl_n_u16_x(pg, svreinterpret_u16_s16(reg2), shift_re[0]);
    svuint16_t ureg3_re =
        svlsl_n_u16_x(pg, svreinterpret_u16_s16(reg3), shift_re[0]);
    svuint16_t ureg4_re =
        svlsl_n_u16_x(pg, svreinterpret_u16_s16(reg4), shift_re[1]);
    svuint16_t ureg5_re =
        svlsl_n_u16_x(pg, svreinterpret_u16_s16(reg5), shift_re[1]);
    svuint16_t ureg6_re =
        svlsl_n_u16_x(pg, svreinterpret_u16_s16(reg6), shift_re[1]);

    // the right-shifts for the imaginary parts are (exp)
    // again we shift everything and discard unwanted values in the tbl later
    uint16_t shift_im[2];
    shift_im[0] = exp[0];
    shift_im[1] = exp[1];
    svuint16_t ureg1_im =
        svlsr_n_u16_x(pg, svreinterpret_u16_s16(reg1), shift_im[0]);
    svuint16_t ureg2_im =
        svlsr_n_u16_x(pg, svreinterpret_u16_s16(reg2), shift_im[0]);
    svuint16_t ureg3_im =
        svlsr_n_u16_x(pg, svreinterpret_u16_s16(reg3), shift_im[0]);
    svuint16_t ureg4_im =
        svlsr_n_u16_x(pg, svreinterpret_u16_s16(reg4), shift_im[1]);
    svuint16_t ureg5_im =
        svlsr_n_u16_x(pg, svreinterpret_u16_s16(reg5), shift_im[1]);
    svuint16_t ureg6_im =
        svlsr_n_u16_x(pg, svreinterpret_u16_s16(reg6), shift_im[1]);

    // shuffle real/imag components into contiguous blocks, fix endianness
    svuint8_t u0_re = svtbl_u8(svreinterpret_u8_u16(ureg1_re), idx_re);
    svuint8_t u0_im = svtbl_u8(svreinterpret_u8_u16(ureg1_im), idx_im);
    svuint8_t u1_re = svtbl_u8(svreinterpret_u8_u16(ureg2_re), idx_re);
    svuint8_t u1_im = svtbl_u8(svreinterpret_u8_u16(ureg2_im), idx_im);
    svuint8_t u2_re = svtbl_u8(svreinterpret_u8_u16(ureg3_re), idx_re);
    svuint8_t u2_im = svtbl_u8(svreinterpret_u8_u16(ureg3_im), idx_im);
    svuint8_t u3_re = svtbl_u8(svreinterpret_u8_u16(ureg4_re), idx_re);
    svuint8_t u3_im = svtbl_u8(svreinterpret_u8_u16(ureg4_im), idx_im);
    svuint8_t u4_re = svtbl_u8(svreinterpret_u8_u16(ureg5_re), idx_re);
    svuint8_t u4_im = svtbl_u8(svreinterpret_u8_u16(ureg5_im), idx_im);
    svuint8_t u5_re = svtbl_u8(svreinterpret_u8_u16(ureg6_re), idx_re);
    svuint8_t u5_im = svtbl_u8(svreinterpret_u8_u16(ureg6_im), idx_im);

    // combine real/imag parts (cannot do this in the tbl since some bytes
    // contain both)
    svint8_t s0 = svreinterpret_s8_u8(svbsl_u8(u0_re, u0_im, sbli));
    svint8_t s1 = svreinterpret_s8_u8(svbsl_u8(u1_re, u1_im, sbli));
    svint8_t s2 = svreinterpret_s8_u8(svbsl_u8(u2_re, u2_im, sbli));
    svint8_t s3 = svreinterpret_s8_u8(svbsl_u8(u3_re, u3_im, sbli));
    svint8_t s4 = svreinterpret_s8_u8(svbsl_u8(u4_re, u4_im, sbli));
    svint8_t s5 = svreinterpret_s8_u8(svbsl_u8(u5_re, u5_im, sbli));

    // write 36-bytes using predication to write 12-bytes at a time
    int8_t *data_out[] = {dst[0].mantissa, dst[1].mantissa};
    svst1_s8(pg12, &data_out[0][0], s0);
    svst1_s8(pg12, &data_out[0][12], s1);
    svst1_s8(pg12, &data_out[0][24], s2);
    svst1_s8(pg12, &data_out[1][0], s3);
    svst1_s8(pg12, &data_out[1][12], s4);
    svst1_s8(pg12, &data_out[1][24], s5);
    dst += 2;
  }
  if (n_prb % 2 != 0) {
    // load 1 PRB = 12 complex symbols
    svint16_t reg1 = svld1_s16(pg, data_in);
    svint16_t reg2 = svld1_s16(pg, &data_in[8]);
    svint16_t reg3 = svld1_s16(pg, &data_in[16]);
    data_in += 24;

    if (scale != nullptr) {
      reg1 = sv_cmplx_mul_combined_re_im(reg1, scale->re, scale_im);
      reg2 = sv_cmplx_mul_combined_re_im(reg2, scale->re, scale_im);
      reg3 = sv_cmplx_mul_combined_re_im(reg3, scale->re, scale_im);
    }

    unsigned int exp = calculate_exp_sve_128(pg, reg1, reg2, reg3, 12);
    dst->exp = (int16_t)exp;

    // the format of the tbl is such that we want real components in the top
    // 12-bits of each 16-bit value, and the imaginary components in the bottom
    // 12-bits of each 16-bit value.
    // with SVE2 intrinsics all shift values are unsigned (unlike NEON) so we
    // have to do left shifts separately from right shifts

    // the left-shifts for the real parts are (4 - exp) and 0 <= exp <= 4
    // we shift everything and discard the unwanted parts in the tbl later
    uint16_t shift_re = 4 - exp;
    svuint16_t ureg1_re =
        svlsl_n_u16_x(pg, svreinterpret_u16_s16(reg1), shift_re);
    svuint16_t ureg2_re =
        svlsl_n_u16_x(pg, svreinterpret_u16_s16(reg2), shift_re);
    svuint16_t ureg3_re =
        svlsl_n_u16_x(pg, svreinterpret_u16_s16(reg3), shift_re);

    // the right-shifts for the imaginary parts are (exp)
    // again we shift everything and discard unwanted values in the tbl later
    uint16_t shift_im = exp;
    svuint16_t ureg1_im =
        svlsr_n_u16_x(pg, svreinterpret_u16_s16(reg1), shift_im);
    svuint16_t ureg2_im =
        svlsr_n_u16_x(pg, svreinterpret_u16_s16(reg2), shift_im);
    svuint16_t ureg3_im =
        svlsr_n_u16_x(pg, svreinterpret_u16_s16(reg3), shift_im);

    // shuffle real/imag components into contiguous blocks, fix endianness
    svuint8_t u0_re = svtbl_u8(svreinterpret_u8_u16(ureg1_re), idx_re);
    svuint8_t u0_im = svtbl_u8(svreinterpret_u8_u16(ureg1_im), idx_im);
    svuint8_t u1_re = svtbl_u8(svreinterpret_u8_u16(ureg2_re), idx_re);
    svuint8_t u1_im = svtbl_u8(svreinterpret_u8_u16(ureg2_im), idx_im);
    svuint8_t u2_re = svtbl_u8(svreinterpret_u8_u16(ureg3_re), idx_re);
    svuint8_t u2_im = svtbl_u8(svreinterpret_u8_u16(ureg3_im), idx_im);

    // combine real/imag parts (cannot do this in the tbl since some bytes
    // contain both)
    svint8_t s0 = svreinterpret_s8_u8(svbsl_u8(u0_re, u0_im, sbli));
    svint8_t s1 = svreinterpret_s8_u8(svbsl_u8(u1_re, u1_im, sbli));
    svint8_t s2 = svreinterpret_s8_u8(svbsl_u8(u2_re, u2_im, sbli));

    // write 36-bytes using predication to write 12-bytes at a time
    int8_t *data_out = dst->mantissa;
    svst1_s8(pg12, data_out, s0);
    svst1_s8(pg12, &data_out[12], s1);
    svst1_s8(pg12, &data_out[24], s2);
    dst++;
  }
  return ARMRAL_SUCCESS;
#else
  const int16_t *data_in = (const int16_t *)src;

  uint16x8_t shiftre4 = {4, 0, 4, 0, 4, 0, 4, 0};

  // only care about 12 out of every 16 bytes here due to compression.
  // need two different versions since we cannot store partial vectors, the
  // last vector requires us to patch the first four bytes at the end.
  uint8x16_t idx01_re = {1,   0,  255, 5,   4,   255, 9,   8,
                         255, 13, 12,  255, 255, 255, 255, 255};
  uint8x16_t idx01_im = {255, 3,   2,  255, 7,   6,   255, 11,
                         10,  255, 15, 14,  255, 255, 255, 255};
  uint8x16_t idx2_re = {255, 255, 255, 255, 1,   0,  255, 5,
                        4,   255, 9,   8,   255, 13, 12,  255};
  uint8x16_t idx2_im = {255, 255, 255, 255, 255, 3,   2,  255,
                        7,   6,   255, 11,  10,  255, 15, 14};

  // masks for combining real/imag parts, again we need two versions.
  uint8x16_t sbli01 = {0xff, 0xf0, 0x0,  0xff, 0xf0, 0x0, 0xff, 0xf0,
                       0x0,  0xff, 0xf0, 0x0,  0,    0,   0,    0};
  uint8x16_t sbli2 = {0,    0,   0,    0,    0xff, 0xf0, 0x0,  0xff,
                      0xf0, 0x0, 0xff, 0xf0, 0x0,  0xff, 0xf0, 0};

  for (int32_t num_prb = 0; num_prb < (int32_t)n_prb - 1; num_prb += 2) {
    // load 2 PRBs = 24 complex symbols
    int16x8x3_t in_1;
    int16x8x3_t in_2;

    if (scale != nullptr) {
      in_1 = load3_cmplx_and_scale(data_in, *scale);
      in_2 = load3_cmplx_and_scale(data_in + 24, *scale);
    } else {
      in_1.val[0] = vld1q_s16(data_in);
      in_1.val[1] = vld1q_s16(&data_in[8]);
      in_1.val[2] = vld1q_s16(&data_in[16]);
      in_2.val[0] = vld1q_s16(&data_in[24]);
      in_2.val[1] = vld1q_s16(&data_in[32]);
      in_2.val[2] = vld1q_s16(&data_in[40]);
    }

    int exp1 = calculate_exp(in_1, 12);
    int exp2 = calculate_exp(in_2, 12);
    dst[0].exp = exp1;
    dst[1].exp = exp2;

    int exps[] = {exp1, exp2};
    int16x8_t regs[] = {in_1.val[0], in_1.val[1], in_1.val[2],
                        in_2.val[0], in_2.val[1], in_2.val[2]};
    for (int i = 0; i < 2; ++i) {
      // the format of the tbl is such that we want real components in the top
      // 12-bits of each 16-bit lane, and the imaginary components in the bottom
      // 12-bits of each 16-bit lane.
      uint16x8_t ureg1 = vreinterpretq_u16_s16(regs[3 * i + 0])
                         << (shiftre4 - vdupq_n_u16(exps[i]));
      uint16x8_t ureg2 = vreinterpretq_u16_s16(regs[3 * i + 1])
                         << (shiftre4 - vdupq_n_u16(exps[i]));
      uint16x8_t ureg3 = vreinterpretq_u16_s16(regs[3 * i + 2])
                         << (shiftre4 - vdupq_n_u16(exps[i]));

      // shuffle real/imag components into contiguous blocks, fix endianness.
      uint8x16_t u0_re = vqtbl1q_u8(vreinterpretq_u8_u16(ureg1), idx01_re);
      uint8x16_t u0_im = vqtbl1q_u8(vreinterpretq_u8_u16(ureg1), idx01_im);
      uint8x16_t u1_re = vqtbl1q_u8(vreinterpretq_u8_u16(ureg2), idx01_re);
      uint8x16_t u1_im = vqtbl1q_u8(vreinterpretq_u8_u16(ureg2), idx01_im);
      uint8x16_t u2_re = vqtbl1q_u8(vreinterpretq_u8_u16(ureg3), idx2_re);
      uint8x16_t u2_im = vqtbl1q_u8(vreinterpretq_u8_u16(ureg3), idx2_im);

      // combine real/imag parts (cannot do this in the tbl since some bytes
      // contain both).
      int8x16_t s0 = vreinterpretq_s8_u8(vbslq_u8(sbli01, u0_re, u0_im));
      int8x16_t s1 = vreinterpretq_s8_u8(vbslq_u8(sbli01, u1_re, u1_im));
      int8x16_t s2 = vreinterpretq_s8_u8(vbslq_u8(sbli2, u2_re, u2_im));

      // finally, make final vector a full 16 byte vector by taking the high
      // four bytes out of the previous one.
      s2 = vreinterpretq_s8_u32(vcopyq_laneq_u32(vreinterpretq_u32_s8(s2), 0,
                                                 vreinterpretq_u32_s8(s1), 2));

      // write 36-bytes
      int8_t *data_out = dst[i].mantissa;
      vst1q_s8(data_out, s0);
      vst1q_s8(&data_out[12], s1);
      vst1q_s8(&data_out[20], s2);
    }
    data_in += 48;
    dst += 2;
  }
  if (n_prb % 2 != 0) {
    // load 1 PRB = 12 complex symbols
    int16x8x3_t in;
    if (scale != nullptr) {
      in = load3_cmplx_and_scale(data_in, *scale);
    } else {
      in.val[0] = vld1q_s16(data_in);
      in.val[1] = vld1q_s16(&data_in[8]);
      in.val[2] = vld1q_s16(&data_in[16]);
    }

    int exp = calculate_exp(in, 12);
    dst->exp = exp;

    // the format of the tbl is such that we want real components in the top
    // 12-bits of each 16-bit lane, and the imaginary components in the bottom
    // 12-bits of each 16-bit lane.
    uint16x8_t ureg1 = vreinterpretq_u16_s16(in.val[0])
                       << (shiftre4 - vdupq_n_u16(exp));
    uint16x8_t ureg2 = vreinterpretq_u16_s16(in.val[1])
                       << (shiftre4 - vdupq_n_u16(exp));
    uint16x8_t ureg3 = vreinterpretq_u16_s16(in.val[2])
                       << (shiftre4 - vdupq_n_u16(exp));

    // shuffle real/imag components into contiguous blocks, fix endianness.
    uint8x16_t u0_re = vqtbl1q_u8(vreinterpretq_u8_u16(ureg1), idx01_re);
    uint8x16_t u0_im = vqtbl1q_u8(vreinterpretq_u8_u16(ureg1), idx01_im);
    uint8x16_t u1_re = vqtbl1q_u8(vreinterpretq_u8_u16(ureg2), idx01_re);
    uint8x16_t u1_im = vqtbl1q_u8(vreinterpretq_u8_u16(ureg2), idx01_im);
    uint8x16_t u2_re = vqtbl1q_u8(vreinterpretq_u8_u16(ureg3), idx2_re);
    uint8x16_t u2_im = vqtbl1q_u8(vreinterpretq_u8_u16(ureg3), idx2_im);

    // combine real/imag parts (cannot do this in the tbl since some bytes
    // contain both).
    int8x16_t s0 = vreinterpretq_s8_u8(vbslq_u8(sbli01, u0_re, u0_im));
    int8x16_t s1 = vreinterpretq_s8_u8(vbslq_u8(sbli01, u1_re, u1_im));
    int8x16_t s2 = vreinterpretq_s8_u8(vbslq_u8(sbli2, u2_re, u2_im));

    // finally, make final vector a full 16 byte vector by taking the high four
    // bytes out of the previous one.
    s2 = vreinterpretq_s8_u32(vcopyq_laneq_u32(vreinterpretq_u32_s8(s2), 0,
                                               vreinterpretq_u32_s8(s1), 2));

    // write 36-bytes
    int8_t *data_out = dst->mantissa;
    vst1q_s8(data_out, s0);
    vst1q_s8(&data_out[12], s1);
    vst1q_s8(&data_out[20], s2);
  }
  return ARMRAL_SUCCESS;
#endif
}

armral_status
armral_block_float_compr_14bit(uint32_t n_prb, const armral_cmplx_int16_t *src,
                               armral_compressed_data_14bit *dst,
                               const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const int16_t *data_in = (const int16_t *)src;

  // Set true predicate corresponding to 8x 16-bit elements
  // (128 bits in total)
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  svint16_t scale_im = svdup_n_s16(0);
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  for (uint32_t num_prb = 0; num_prb < n_prb; num_prb++) {

    // load 1 PRB = 12 complex symbols
    svint16_t reg1 = svld1_s16(pg, data_in);
    svint16_t reg2 = svld1_s16(pg, &data_in[8]);
    svint16_t reg3 = svld1_s16(pg, &data_in[16]);
    data_in += 24;

    if (scale != nullptr) {
      reg1 = sv_cmplx_mul_combined_re_im(reg1, scale->re, scale_im);
      reg2 = sv_cmplx_mul_combined_re_im(reg2, scale->re, scale_im);
      reg3 = sv_cmplx_mul_combined_re_im(reg3, scale->re, scale_im);
    }

    const unsigned int exp = calculate_exp_sve_128(pg, reg1, reg2, reg3, 14);
    dst->exp = (int16_t)exp;

    const svint16_t *regs[] = {&reg1, &reg2, &reg3};
    pack_14bit_and_store_int16<true>(pg, regs, dst, exp);

    dst++;
  }
  return ARMRAL_SUCCESS;
#else
  int16x8x3_t reg[2];
  const int16_t *data_in = (const int16_t *)src;

  for (int32_t num_prb = 0; num_prb < (int32_t)n_prb - 1; num_prb += 2) {

    if (scale != nullptr) {
      reg[0] = load3_cmplx_and_scale(data_in, *scale);
      reg[1] = load3_cmplx_and_scale(data_in + 24, *scale);
    } else {
      reg[0].val[0] = vld1q_s16(data_in);
      reg[0].val[1] = vld1q_s16(&data_in[8]);
      reg[0].val[2] = vld1q_s16(&data_in[16]);
      reg[1].val[0] = vld1q_s16(&data_in[24]);
      reg[1].val[1] = vld1q_s16(&data_in[32]);
      reg[1].val[2] = vld1q_s16(&data_in[40]);
    }

    int exps[] = {calculate_exp(reg[0], 14), calculate_exp(reg[1], 14)};
    dst[0].exp = exps[0];
    dst[1].exp = exps[1];

    pack_14bit_and_store_int16<2, true>(reg, dst, exps);
    data_in += 48;
    dst += 2;
  }
  if (n_prb % 2 != 0) {

    if (scale != nullptr) {
      reg[0] = load3_cmplx_and_scale(data_in, *scale);
    } else {
      reg[0].val[0] = vld1q_s16(data_in);
      reg[0].val[1] = vld1q_s16(&data_in[8]);
      reg[0].val[2] = vld1q_s16(&data_in[16]);
    }

    int exps[] = {calculate_exp(reg[0], 14)};
    dst->exp = exps[0];

    pack_14bit_and_store_int16<1, true>(reg, dst, exps);
  }
  return ARMRAL_SUCCESS;
#endif
}
