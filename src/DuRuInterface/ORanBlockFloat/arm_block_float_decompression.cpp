/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#if ARMRAL_ARCH_SVE >= 2
#include <arm_sve.h>
#endif

#include "../bit_unpacking_common.hpp"
#include "utils/vec_mul.hpp"

armral_status armral_block_float_decompr_8bit(
    uint32_t n_prb, const armral_compressed_data_8bit *src,
    armral_cmplx_int16_t *dst, const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }
  int16_t *data_out = (int16_t *)dst;

  // Set true predicate corresponding to 8x 16-bit elements
  // (128 bits in total)
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  for (uint32_t num_prb = 0; num_prb < n_prb; num_prb++) {
    const int8_t *data_in = src->mantissa;
    uint16_t exp = (uint16_t)src->exp;
    // Load 1 PRB = 12 complex symbols
    // Sign-extend load 8bits to 16bits
    svint16_t reg1 = svld1sb_s16(pg, data_in);
    data_in = data_in + 8;
    svint16_t reg2 = svld1sb_s16(pg, data_in);
    data_in = data_in + 8;
    svint16_t reg3 = svld1sb_s16(pg, data_in);

    svint16_t decomp1 = svlsl_n_s16_x(pg, reg1, exp);
    svint16_t decomp2 = svlsl_n_s16_x(pg, reg2, exp);
    svint16_t decomp3 = svlsl_n_s16_x(pg, reg3, exp);

    if (scale != nullptr) {
      svst1_s16(pg, data_out,
                sv_cmplx_mul_combined_re_im(decomp1, scale->re, scale_im));
      data_out += 8;
      svst1_s16(pg, data_out,
                sv_cmplx_mul_combined_re_im(decomp2, scale->re, scale_im));
      data_out += 8;
      svst1_s16(pg, data_out,
                sv_cmplx_mul_combined_re_im(decomp3, scale->re, scale_im));
      data_out += 8;
    } else {
      svst1_s16(pg, data_out, decomp1);
      data_out = data_out + 8;
      svst1_s16(pg, data_out, decomp2);
      data_out = data_out + 8;
      svst1_s16(pg, data_out, decomp3);
      data_out = data_out + 8;
    }
    src++;
  }
  return ARMRAL_SUCCESS;
#else
  int16_t *data_out = (int16_t *)dst;

  for (uint32_t num_prb = 0; num_prb < n_prb; num_prb++) {
    const int8_t *data_in = src->mantissa;
    int16x8_t exp = vdupq_n_s16((int16_t)src->exp);
    // Load 1 PRB = 12 complex symbols
    int8x8_t reg1 = vld1_s8(data_in);
    data_in = data_in + 8;
    int8x8_t reg2 = vld1_s8(data_in);
    data_in = data_in + 8;
    int8x8_t reg3 = vld1_s8(data_in);

    int16x8x3_t extended_input;
    extended_input.val[0] = vmovl_s8(reg1);
    extended_input.val[1] = vmovl_s8(reg2);
    extended_input.val[2] = vmovl_s8(reg3);

    int16x8x3_t decompressed_output;
    decompressed_output.val[0] = vshlq_s16(extended_input.val[0], exp);
    decompressed_output.val[1] = vshlq_s16(extended_input.val[1], exp);
    decompressed_output.val[2] = vshlq_s16(extended_input.val[2], exp);

    if (scale != nullptr) {
      vst1q_s16((int16_t *)&data_out[0],
                cmplx_mul_combined_re_im(decompressed_output.val[0], *scale));
      vst1q_s16((int16_t *)&data_out[8],
                cmplx_mul_combined_re_im(decompressed_output.val[1], *scale));
      vst1q_s16((int16_t *)&data_out[16],
                cmplx_mul_combined_re_im(decompressed_output.val[2], *scale));
      data_out += 24;
    } else {
      vst1q_s16(data_out, decompressed_output.val[0]);
      data_out = data_out + 8;
      vst1q_s16(data_out, decompressed_output.val[1]);
      data_out = data_out + 8;
      vst1q_s16(data_out, decompressed_output.val[2]);
      data_out = data_out + 8;
    }
    src++;
  }
  return ARMRAL_SUCCESS;
#endif
}

armral_status armral_block_float_decompr_9bit(
    uint32_t n_prb, const armral_compressed_data_9bit *src,
    armral_cmplx_int16_t *dst, const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }
  int16_t *data_out = (int16_t *)dst;

  // set true predicate corresponding to 8x 16-bit elements
  // (128 bits in total)
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  // left shifts will be { 8, 9, 10, 11, ... }
  const svuint16_t left_shifts = svindex_u16(8, 1);
  // right shifts need to be { 7, 6, 5, 4, ... }
  // therefore the following only works if we limit ourselves to using the
  // first 128 bits of vectors (which is what we currently do)
  const svuint16_t right_shifts = svreinterpret_u16_s16(svindex_s16(7, -1));

  for (uint32_t num_prb = 0; num_prb < n_prb; num_prb++) {
    const uint8_t *data_in = (const uint8_t *)src->mantissa;
    uint16_t exp = (uint16_t)src->exp;
    // load 1 PRB = 12 complex symbols
    // zero-extend load 8bits to 16bits
    // loads bits 0-7
    svuint16_t a07 = svld1ub_u16(pg, &data_in[0]);
    svuint16_t b07 = svld1ub_u16(pg, &data_in[9]);
    svuint16_t c07 = svld1ub_u16(pg, &data_in[18]);
    // loads bits 1-8
    svuint16_t a18 = svld1ub_u16(pg, &data_in[1]);
    svuint16_t b18 = svld1ub_u16(pg, &data_in[10]);
    svuint16_t c18 = svld1ub_u16(pg, &data_in[19]);

    // e.g. for second lanes (x07=B, x18=C), extracting 9-bit bbbbbbbbb
    //  x07[1] = 00000000_abbbbbbb
    //  x18[1] = 00000000_bbcccccc
    // > a_left = svlsl_u16_x(pg, x07, left_shifts)
    //                         = svlsl_u16_x(pg, 00000000_abbbbbbb, 9)
    //                         = bbbbbbb0_00000000
    // > svlsr_u16_x(pg, x18, right_shifts)
    //                         = svlsr_u16_x(pg, 00000000_bbcccccc, 6)
    //                         = 00000000_000000bb
    // > b_left = svlsl_n_u16_x(pg, 0000000_000000bb, 7) = 0000000b_b0000000
    // note how we have populated the sign-bit of the 16-bit lane in a_left.
    // A shift right (dealing with the block exponent) therefore preserves
    // the sign of the original value as expected.
    svuint16_t a_left = svlsl_u16_x(pg, a07, left_shifts);
    svuint16_t b_left = svlsl_u16_x(pg, b07, left_shifts);
    svuint16_t c_left = svlsl_u16_x(pg, c07, left_shifts);
    svuint16_t a_right =
        svlsl_n_u16_x(pg, svlsr_u16_x(pg, a18, right_shifts), 7);
    svuint16_t b_right =
        svlsl_n_u16_x(pg, svlsr_u16_x(pg, b18, right_shifts), 7);
    svuint16_t c_right =
        svlsl_n_u16_x(pg, svlsr_u16_x(pg, c18, right_shifts), 7);
    svint16_t a_comb = svreinterpret_s16_u16(svorr_u16_x(pg, a_left, a_right));
    svint16_t b_comb = svreinterpret_s16_u16(svorr_u16_x(pg, b_left, b_right));
    svint16_t c_comb = svreinterpret_s16_u16(svorr_u16_x(pg, c_left, c_right));
    a_comb = svasr_n_s16_x(pg, a_comb, 7 - exp);
    b_comb = svasr_n_s16_x(pg, b_comb, 7 - exp);
    c_comb = svasr_n_s16_x(pg, c_comb, 7 - exp);

    if (scale != nullptr) {
      svst1_s16(pg, data_out,
                sv_cmplx_mul_combined_re_im(a_comb, scale->re, scale_im));
      data_out += 8;
      svst1_s16(pg, data_out,
                sv_cmplx_mul_combined_re_im(b_comb, scale->re, scale_im));
      data_out += 8;
      svst1_s16(pg, data_out,
                sv_cmplx_mul_combined_re_im(c_comb, scale->re, scale_im));
      data_out += 8;
    } else {
      svst1_s16(pg, data_out, a_comb);
      data_out = data_out + 8;
      svst1_s16(pg, data_out, b_comb);
      data_out = data_out + 8;
      svst1_s16(pg, data_out, c_comb);
      data_out = data_out + 8;
    }
    src++;
  }
  return ARMRAL_SUCCESS;
#else
  common_decompr_9bit_neon<false, true>(n_prb, src, dst, scale);
  return ARMRAL_SUCCESS;
#endif
}

armral_status armral_block_float_decompr_12bit(
    uint32_t n_prb, const armral_compressed_data_12bit *src,
    armral_cmplx_int16_t *dst, const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  // Set true predicate corresponding to 12x 8-bit elements
  // (96 bits in total)
  const svbool_t pg12 = svwhilelt_b8(0, 12);

  // Set true predicate corresponding to 8x 16-bit elements
  // (128 bits in total)
  const svbool_t pg16 = svptrue_pat_b16(SV_VL8);

  // indices are {1, 0, 2, 1, 4, 3, 5, 4, 7, 6, 8, 7, 10, 9, 11, 10}
  const svuint8_t idx =
      svreinterpret_u8_u32(svindex_u32(0x01020001, 0x03030303));

  // masks are {0xfff0, 0x0fff, 0xfff0, 0x0fff, 0xfff0, 0x0fff, 0xfff0, 0x0fff}
  const svint16_t mask = svreinterpret_s16_s32(svdup_n_s32(0x0ffffff0));

  // shifts are {0, 4, 0, 4, 0, 4, 0, 4}
  const svuint16_t shift = svreinterpret_u16_u32(svdup_n_u32(0x00040000));

  for (uint32_t num_prb = 0; num_prb < n_prb; num_prb++) {
    const uint8_t *data_in = (const uint8_t *)&src->mantissa[0];
    uint16_t exp = (uint16_t)src->exp;

    // need to load 12*24/8 = 36 bytes total.
    svuint8_t in0 = svld1_u8(pg12, data_in);
    svuint8_t in1 = svld1_u8(pg12, data_in + 12);
    svuint8_t in2 = svld1_u8(pg12, data_in + 24);

    // permute bytes [ ...    bb|aB|Aa] (capital indicates msb of original data)
    // into          [ ... aB|bb|Aa|aB] (note Bbb and Aaa now contiguous)
    svint16_t pack0 = svreinterpret_s16_u8(svtbl_u8(in0, idx));
    svint16_t pack1 = svreinterpret_s16_u8(svtbl_u8(in1, idx));
    svint16_t pack2 = svreinterpret_s16_u8(svtbl_u8(in2, idx));

    // shift and mask from [ ... aBbb|AaaB] (from above)
    // into                [ ... Bbb0|Aaa0]
    pack0 = svlsl_s16_x(pg16, svand_s16_x(pg16, pack0, mask), shift);
    pack1 = svlsl_s16_x(pg16, svand_s16_x(pg16, pack1, mask), shift);
    pack2 = svlsl_s16_x(pg16, svand_s16_x(pg16, pack2, mask), shift);

    // finally, apply exp and store
    pack0 = svasr_n_s16_x(pg16, pack0, 4 - exp);
    pack1 = svasr_n_s16_x(pg16, pack1, 4 - exp);
    pack2 = svasr_n_s16_x(pg16, pack2, 4 - exp);

    if (scale != nullptr) {
      svst1_s16(pg16, (int16_t *)&dst[0],
                sv_cmplx_mul_combined_re_im(pack0, scale->re, scale_im));
      svst1_s16(pg16, (int16_t *)&dst[4],
                sv_cmplx_mul_combined_re_im(pack1, scale->re, scale_im));
      svst1_s16(pg16, (int16_t *)&dst[8],
                sv_cmplx_mul_combined_re_im(pack2, scale->re, scale_im));
    } else {
      svst1_s16(pg16, (int16_t *)&dst[0], pack0);
      svst1_s16(pg16, (int16_t *)&dst[4], pack1);
      svst1_s16(pg16, (int16_t *)&dst[8], pack2);
    }
    src++;
    dst += 12;
  }
  return ARMRAL_SUCCESS;
#else
  for (uint32_t num_prb = 0; num_prb < n_prb; num_prb++) {
    const uint8_t *data_in = (const uint8_t *)&src->mantissa[0];
    int16_t exp = src->exp;

    // need to load 12*24/8 = 36 bytes total.
    uint8x16_t in0 = vld1q_u8(data_in);      // first 12 bytes are interesting.
    uint8x16_t in1 = vld1q_u8(data_in + 12); // first 12 bytes are interesting.
    uint8x16_t in2 = vld1q_u8(data_in + 20); // last 12 bytes are interesting.

    // permute bytes [ ...    bb|aB|Aa] (capital indicates msb of original data)
    // into          [ ... aB|bb|Aa|aB] (note Bbb and Aaa now contiguous)
    uint8x16_t idx01 =
        uint8x16_t{1, 0, 2, 1, 4, 3, 5, 4, 7, 6, 8, 7, 10, 9, 11, 10};
    uint8x16_t idx2 =
        uint8x16_t{5, 4, 6, 5, 8, 7, 9, 8, 11, 10, 12, 11, 14, 13, 15, 14};
    int16x8_t pack0 = vreinterpretq_s16_u8(vqtbl1q_u8(in0, idx01));
    int16x8_t pack1 = vreinterpretq_s16_u8(vqtbl1q_u8(in1, idx01));
    int16x8_t pack2 = vreinterpretq_s16_u8(vqtbl1q_u8(in2, idx2));

    // shift and mask from [ ... aBbb|AaaB] (from above)
    // into                [ ... Bbb0|Aaa0]
    int16x8_t mask = int16x8_t{
        (int16_t)0xfff0, (int16_t)0x0fff, (int16_t)0xfff0, (int16_t)0x0fff,
        (int16_t)0xfff0, (int16_t)0x0fff, (int16_t)0xfff0, (int16_t)0x0fff};
    int16x8_t shift = int16x8_t{0, 4, 0, 4, 0, 4, 0, 4};
    pack0 = vshlq_s16(vandq_s16(pack0, mask), shift);
    pack1 = vshlq_s16(vandq_s16(pack1, mask), shift);
    pack2 = vshlq_s16(vandq_s16(pack2, mask), shift);

    // finally, apply exp and store
    pack0 = vshlq_s16(pack0, vdupq_n_s16(exp - 4));
    pack1 = vshlq_s16(pack1, vdupq_n_s16(exp - 4));
    pack2 = vshlq_s16(pack2, vdupq_n_s16(exp - 4));

    if (scale != nullptr) {
      vst1q_s16((int16_t *)&dst[0], cmplx_mul_combined_re_im(pack0, *scale));
      vst1q_s16((int16_t *)&dst[4], cmplx_mul_combined_re_im(pack1, *scale));
      vst1q_s16((int16_t *)&dst[8], cmplx_mul_combined_re_im(pack2, *scale));
    } else {
      vst1q_s16((int16_t *)&dst[0], pack0);
      vst1q_s16((int16_t *)&dst[4], pack1);
      vst1q_s16((int16_t *)&dst[8], pack2);
    }
    src++;
    dst += 12;
  }
  return ARMRAL_SUCCESS;
#endif
}

armral_status armral_block_float_decompr_14bit(
    uint32_t n_prb, const armral_compressed_data_14bit *src,
    armral_cmplx_int16_t *dst, const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  // Set true predicate corresponding to 14x 8-bit elements
  // (112 bits in total)
  const svbool_t pg14 = svwhilelt_b8(0, 14);

  // Set true predicate corresponding to 8x 16-bit elements
  // (128 bits in total)
  const svbool_t pg16 = svptrue_pat_b16(SV_VL8);

  // Indices used during unpacking
  const svuint8_t contig_idx =
      svdupq_n_u8(1, 0, 3, 2, 5, 4, 255, 6, 8, 7, 10, 9, 12, 11, 255, 13);
  const svuint8_t contig_idx2 = svdupq_n_u8(255, 255, 255, 1, 255, 3, 255, 5,
                                            255, 255, 255, 8, 255, 10, 255, 12);

  // Shifts and masks used during unpacking
  const svuint16_t contig_lshift = svdupq_n_u16(0, 6, 4, 2, 0, 6, 4, 2);
  const svuint16_t contig_rshift = svdupq_n_u16(0, 2, 4, 6, 0, 2, 4, 6);
  // masks are {0xfc00, 0xf003, 0xc003, 0x0003,
  //            0xfc00, 0xf003, 0xc003, 0x0003}
  const svuint16_t sbli =
      svreinterpret_u16_u64(svdup_n_u64(0xfc00f003c0030003));

  for (uint32_t num_prb = 0; num_prb < n_prb; num_prb++) {
    const uint8_t *data_in = (const uint8_t *)&src->mantissa[0];
    uint16_t exp = (uint16_t)src->exp;

    // need to load 14*24/8 = 42 bytes total
    svuint8_t in1 = svld1_u8(pg14, data_in);
    svuint8_t in2 = svld1_u8(pg14, data_in + 14);
    svuint8_t in3 = svld1_u8(pg14, data_in + 28);

    // permute
    // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
    // (capital indicates msb of original data)
    // into
    // [ccDddddddddddddd|bbbbCccccccccccc|bbbbbbbbbbbbCccc|Aaaaaaaaaaaaaabb]
    // (Aa... and Dd... now contiguous)
    // note this pattern repeats for bytes 7-13 as well

    // Get 14 bits of A, 12 LSBs of B, 10 LSBs of C and 8 LSBs of D at the top
    // of each 16-bit element
    // [dddddddd00000000|ccccccccccDddddd|bbbbbbbbbbbbCccc|AaaaaaaaaaaaaaBb]
    svuint16_t contig1_r = svreinterpret_u16_u8(svtbl_u8(in1, contig_idx));
    svuint16_t contig2_r = svreinterpret_u16_u8(svtbl_u8(in2, contig_idx));
    svuint16_t contig3_r = svreinterpret_u16_u8(svtbl_u8(in3, contig_idx));

    // contig_rshift = {0, 2, 4, 6, ...}
    // [000000dddddddd00|0000ccccccccccDd|00bbbbbbbbbbbbCc|AaaaaaaaaaaaaaBb]
    contig1_r = svlsr_u16_x(pg16, contig1_r, contig_rshift);
    contig2_r = svlsr_u16_x(pg16, contig2_r, contig_rshift);
    contig3_r = svlsr_u16_x(pg16, contig3_r, contig_rshift);

    // Get 0 MSBs of A, 2 MSBs of B, 4 MSBs of C and 6 MSBs of D
    // [ccDddddd00000000|bbbbCccc00000000|aaaaaaBb00000000|0000000000000000]
    svuint16_t contig1_l = svreinterpret_u16_u8(svtbl_u8(in1, contig_idx2));
    svuint16_t contig2_l = svreinterpret_u16_u8(svtbl_u8(in2, contig_idx2));
    svuint16_t contig3_l = svreinterpret_u16_u8(svtbl_u8(in3, contig_idx2));

    // Shift left to get MSBs in correct place
    // [Dddddd0000000000|Cccc000000000000|Bb00000000000000|0000000000000000]
    contig1_l = svlsl_u16_x(pg16, contig1_l, contig_lshift);
    contig2_l = svlsl_u16_x(pg16, contig2_l, contig_lshift);
    contig3_l = svlsl_u16_x(pg16, contig3_l, contig_lshift);

    // Combine LSBs and MSBs by selecting the appropriate bits
    svint16_t prb1_comp =
        svreinterpret_s16_u16(svbsl_u16(contig1_l, contig1_r, sbli));
    svint16_t prb2_comp =
        svreinterpret_s16_u16(svbsl_u16(contig2_l, contig2_r, sbli));
    svint16_t prb3_comp =
        svreinterpret_s16_u16(svbsl_u16(contig3_l, contig3_r, sbli));

    // Finally, apply exp and store
    // [--Dddddddddddddd|--Cccccccccccccc|--Bbbbbbbbbbbbbb|--Aaaaaaaaaaaaaa]
    svint16_t pack1 = svasr_n_s16_x(pg16, prb1_comp, 2 - exp);
    svint16_t pack2 = svasr_n_s16_x(pg16, prb2_comp, 2 - exp);
    svint16_t pack3 = svasr_n_s16_x(pg16, prb3_comp, 2 - exp);

    if (scale != nullptr) {
      svst1_s16(pg16, (int16_t *)&dst[0],
                sv_cmplx_mul_combined_re_im(pack1, scale->re, scale_im));
      svst1_s16(pg16, (int16_t *)&dst[4],
                sv_cmplx_mul_combined_re_im(pack2, scale->re, scale_im));
      svst1_s16(pg16, (int16_t *)&dst[8],
                sv_cmplx_mul_combined_re_im(pack3, scale->re, scale_im));
    } else {
      svst1_s16(pg16, (int16_t *)&dst[0], pack1);
      svst1_s16(pg16, (int16_t *)&dst[4], pack2);
      svst1_s16(pg16, (int16_t *)&dst[8], pack3);
    }

    src++;
    dst += 12;
  }
  return ARMRAL_SUCCESS;
#else
  for (uint32_t num_prb = 0; num_prb < n_prb; num_prb++) {
    const uint8_t *data_in = (const uint8_t *)&src->mantissa[0];
    int16_t exp = src->exp;

    uint8x8_t shift0 = {1, 64, 16, 4, 1, 64, 16, 4};
    int8x8_t shift1 = {0, -2, -4, 0, 0, -2, -4, 0};
    uint8x8_t shift2 = {1, 64, 16, 4, 1, 64, 16, 4};

    uint8x8_t idx0 = {255, 2, 4, 6, 255, 9, 11, 13};
    uint8x8_t idx1 = {1, 3, 5, 255, 8, 10, 12, 255};
    uint8x8_t idx2 = {0, 1, 3, 5, 7, 8, 10, 12};

    uint8x16_t in_a = vld1q_u8(data_in);
    uint8x16_t in_b = vld1q_u8(data_in + 12);
    uint8x16_t in_c = vld1q_u8(data_in + 26);

    uint8x8_t in0_8b = vqtbl1_u8(in_a, idx0);
    uint8x8_t in1_8b = vqtbl1_u8(in_a, idx1);
    uint8x8_t in2_8b = vqtbl1_u8(in_a, idx2);
    uint16x8_t in0 = vmull_u8(in0_8b, shift0);
    uint16x8_t in1 = vmovl_u8(vshl_u8(in1_8b, shift1) & 0xfcU);
    uint16x8_t in2 = vshll_n_u8(vmul_u8(in2_8b, shift2), 8);
    int16x8_t out = vreinterpretq_s16_u16(in0 | in1 | in2);
    int16x8_t pack0 = out >> (2 - exp);

    idx0 = uint8x8_t{255, 4, 6, 8, 255, 11, 13, 15};
    idx1 = uint8x8_t{3, 5, 7, 255, 10, 12, 14, 255};
    idx2 = uint8x8_t{2, 3, 5, 7, 9, 10, 12, 14};

    in0_8b = vqtbl1_u8(in_b, idx0);
    in1_8b = vqtbl1_u8(in_b, idx1);
    in2_8b = vqtbl1_u8(in_b, idx2);
    in0 = vmull_u8(in0_8b, shift0);
    in1 = vmovl_u8(vshl_u8(in1_8b, shift1) & 0xfcU);
    in2 = vshll_n_u8(vmul_u8(in2_8b, shift2), 8);
    out = vreinterpretq_s16_u16(in0 | in1 | in2);
    int16x8_t pack1 = out >> (2 - exp);

    in0_8b = vqtbl1_u8(in_c, idx0);
    in1_8b = vqtbl1_u8(in_c, idx1);
    in2_8b = vqtbl1_u8(in_c, idx2);
    in0 = vmull_u8(in0_8b, shift0);
    in1 = vmovl_u8(vshl_u8(in1_8b, shift1) & 0xfcU);
    in2 = vshll_n_u8(vmul_u8(in2_8b, shift2), 8);
    out = vreinterpretq_s16_u16(in0 | in1 | in2);
    int16x8_t pack2 = out >> (2 - exp);

    if (scale != nullptr) {
      vst1q_s16((int16_t *)&dst[0], cmplx_mul_combined_re_im(pack0, *scale));
      vst1q_s16((int16_t *)&dst[4], cmplx_mul_combined_re_im(pack1, *scale));
      vst1q_s16((int16_t *)&dst[8], cmplx_mul_combined_re_im(pack2, *scale));
    } else {
      vst1q_s16((int16_t *)&dst[0], pack0);
      vst1q_s16((int16_t *)&dst[4], pack1);
      vst1q_s16((int16_t *)&dst[8], pack2);
    }

    src++;
    dst += 12;
  }
  return ARMRAL_SUCCESS;
#endif
}
