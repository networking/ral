/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#if ARMRAL_ARCH_SVE >= 2
#include <arm_sve.h>
#endif

#include "../bit_packing_common.hpp"
#include "utils/vec_mul.hpp"

armral_status
armral_block_scaling_compr_8bit(uint32_t n_prb, const armral_cmplx_int16_t *src,
                                armral_compressed_data_8bit *dst,
                                const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const int16_t *data_in = (const int16_t *)src;

  // Set true predicate corresponding to 8x16-bit elements
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  svint16_t scale_im = svdup_n_s16(0);
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  for (unsigned int i = 0; i < n_prb; i++) {
    svint16_t prb1_in = svld1_s16(pg, data_in);
    svint16_t prb2_in = svld1_s16(pg, &data_in[8]);
    svint16_t prb3_in = svld1_s16(pg, &data_in[16]);
    data_in += 24;

    if (scale != nullptr) {
      prb1_in = sv_cmplx_mul_combined_re_im(prb1_in, scale->re, scale_im);
      prb2_in = sv_cmplx_mul_combined_re_im(prb2_in, scale->re, scale_im);
      prb3_in = sv_cmplx_mul_combined_re_im(prb3_in, scale->re, scale_im);
    }

    // Extract absolute values
    svint16_t prb1_abs = svqabs_s16_z(pg, prb1_in);
    svint16_t prb2_abs = svqabs_s16_z(pg, prb2_in);
    svint16_t prb3_abs = svqabs_s16_z(pg, prb3_in);

    // Find max in absRe and absIm
    svint16_t v_max =
        svmax_s16_x(pg, prb1_abs, svmax_s16_x(pg, prb2_abs, prb3_abs));
    int16_t max_val = svmaxv_s16(pg, v_max);

    // Determine the scaling factor
    uint8_t scaling_factor = vqmovnh_s16((255 + max_val) >> 8);

    dst[i].exp = (scaling_factor == 0) ? 1 : scaling_factor;

    int16_t inv_scale = 128 / dst[i].exp;

    // Apply scaling
    svint16_t comp_mod1 =
        svasrd_n_s16_x(pg, svmul_n_s16_x(pg, prb1_in, inv_scale), 8);
    svint16_t comp_mod2 =
        svasrd_n_s16_x(pg, svmul_n_s16_x(pg, prb2_in, inv_scale), 8);
    svint16_t comp_mod3 =
        svasrd_n_s16_x(pg, svmul_n_s16_x(pg, prb3_in, inv_scale), 8);

    // Saturate and narrow results
    svint8_t comp1 = svqxtnb_s16(comp_mod1);
    svint8_t comp2 = svqxtnb_s16(comp_mod2);
    svint8_t comp3 = svqxtnb_s16(comp_mod3);

    // Store compressed data
    int8_t *data_out = dst[i].mantissa;
    svst1b_s16(pg, data_out, svreinterpret_s16_s8(comp1));
    svst1b_s16(pg, &data_out[8], svreinterpret_s16_s8(comp2));
    svst1b_s16(pg, &data_out[16], svreinterpret_s16_s8(comp3));
#else
  for (unsigned int i = 0; i < n_prb; i++) {
    int16x8_t prb_in[3];
    if (scale != nullptr) {
      int16x8x3_t in = load3_cmplx_and_scale((const int16_t *)src, *scale);
      prb_in[0] = in.val[0];
      prb_in[1] = in.val[1];
      prb_in[2] = in.val[2];
      src += 12;
    } else {
      prb_in[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[2] = vld1q_s16((const int16_t *)src);
      src += 4;
    }

    int16x8_t prb_abs[3];
    prb_abs[0] = vqabsq_s16(prb_in[0]);
    prb_abs[1] = vqabsq_s16(prb_in[1]);
    prb_abs[2] = vqabsq_s16(prb_in[2]);

    // Find the maximum in absRe and absIm
    int16x8_t v_max = vmaxq_s16(prb_abs[0], vmaxq_s16(prb_abs[1], prb_abs[2]));
    int16_t max_val = vmaxvq_s16(v_max);

    uint8_t scaling_factor = vqmovnh_s16((255 + max_val) >> 8);

    dst[i].exp = (scaling_factor == 0) ? 1 : scaling_factor;

    int16_t inv_scal = 128 / dst[i].exp;
    int16x8_t inv_scaling_fact = vdupq_n_s16(inv_scal);

    uint16x8_t sign[3];
    // either UINT16_MAX or 0.
    sign[0] = vreinterpretq_u16_s16(vshrq_n_s16(prb_in[0], 15));
    sign[1] = vreinterpretq_u16_s16(vshrq_n_s16(prb_in[1], 15));
    sign[2] = vreinterpretq_u16_s16(vshrq_n_s16(prb_in[2], 15));

    uint16x8_t comp_mod[3];
    // Operation to force rounding to zero
    comp_mod[0] = vsraq_n_u16(
        vreinterpretq_u16_s16(vmulq_s16(prb_in[0], inv_scaling_fact)), sign[0],
        8);
    comp_mod[1] = vsraq_n_u16(
        vreinterpretq_u16_s16(vmulq_s16(prb_in[1], inv_scaling_fact)), sign[1],
        8);
    comp_mod[2] = vsraq_n_u16(
        vreinterpretq_u16_s16(vmulq_s16(prb_in[2], inv_scaling_fact)), sign[2],
        8);

    int8x8_t comp[3];
    comp[0] = vqmovn_s16(vshrq_n_s16(vreinterpretq_s16_u16(comp_mod[0]), 8));
    comp[1] = vqmovn_s16(vshrq_n_s16(vreinterpretq_s16_u16(comp_mod[1]), 8));
    comp[2] = vqmovn_s16(vshrq_n_s16(vreinterpretq_s16_u16(comp_mod[2]), 8));

    // store compressed data
    int8_t *data_out = dst[i].mantissa;
    vst1_s8(data_out, comp[0]);
    data_out += 8;
    vst1_s8(data_out, comp[1]);
    data_out += 8;
    vst1_s8(data_out, comp[2]);
#endif
  }
  return ARMRAL_SUCCESS;
}

armral_status
armral_block_scaling_compr_9bit(uint32_t n_prb, const armral_cmplx_int16_t *src,
                                armral_compressed_data_9bit *dst,
                                const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const int16_t *data_in = (const int16_t *)src;

  // Set true predicate corresponding to 8x16-bit elements
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  svint16_t scale_im = svdup_n_s16(0);
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  for (unsigned int i = 0; i < n_prb; i++) {
    svint16_t prb1_in = svld1_s16(pg, data_in);
    svint16_t prb2_in = svld1_s16(pg, &data_in[8]);
    svint16_t prb3_in = svld1_s16(pg, &data_in[16]);
    data_in += 24;

    if (scale != nullptr) {
      prb1_in = sv_cmplx_mul_combined_re_im(prb1_in, scale->re, scale_im);
      prb2_in = sv_cmplx_mul_combined_re_im(prb2_in, scale->re, scale_im);
      prb3_in = sv_cmplx_mul_combined_re_im(prb3_in, scale->re, scale_im);
    }

    // Extract absolute values
    svint16_t prb1_abs = svqabs_s16_z(pg, prb1_in);
    svint16_t prb2_abs = svqabs_s16_z(pg, prb2_in);
    svint16_t prb3_abs = svqabs_s16_z(pg, prb3_in);

    // Find max in absRe and absIm
    svint16_t v_max =
        svmax_s16_x(pg, prb1_abs, svmax_s16_x(pg, prb2_abs, prb3_abs));
    int16_t max_val = svmaxv_s16(pg, v_max);

    // Determine the scaling factor and save with 8 bits
    int compressed_bound = 1 << 9;
    uint8_t scaling_fact = (max_val + compressed_bound - 1) / compressed_bound;
    scaling_fact = (scaling_fact < INT8_MAX) ? scaling_fact : INT8_MAX;
    dst->exp = (scaling_fact == 0) ? 1 : scaling_fact;

    // Calculate inverse_scaling_fact = 2^(16- N -1) /scaling_fact
    // 2^(16- N -1) = 64
    int16_t inv_scale = 64 / dst->exp;

    // Apply scaling
    svint16_t reg1 =
        svasrd_n_s16_x(pg, svmul_n_s16_x(pg, prb1_in, inv_scale), 7);
    svint16_t reg2 =
        svasrd_n_s16_x(pg, svmul_n_s16_x(pg, prb2_in, inv_scale), 7);
    svint16_t reg3 =
        svasrd_n_s16_x(pg, svmul_n_s16_x(pg, prb3_in, inv_scale), 7);

    const svint16_t *regs[] = {&reg1, &reg2, &reg3};
    pack_9bit_and_store_int16<1>(pg, regs, dst);

    dst++;
#else
  for (unsigned int i = 0; i < n_prb; i++) {
    int16x8x3_t prb_in;
    if (scale != nullptr) {
      int16x8x3_t in = load3_cmplx_and_scale((const int16_t *)src, *scale);
      prb_in.val[0] = in.val[0];
      prb_in.val[1] = in.val[1];
      prb_in.val[2] = in.val[2];
      src += 12;
    } else {
      prb_in.val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in.val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in.val[2] = vld1q_s16((const int16_t *)src);
      src += 4;
    }

    int16x8x3_t prb_abs;
    prb_abs.val[0] = vqabsq_s16(prb_in.val[0]);
    prb_abs.val[1] = vqabsq_s16(prb_in.val[1]);
    prb_abs.val[2] = vqabsq_s16(prb_in.val[2]);

    // Find the maximum in absRe and absIm
    int16x8_t v_max =
        vmaxq_s16(prb_abs.val[0], vmaxq_s16(prb_abs.val[1], prb_abs.val[2]));
    int16_t max_val = vmaxvq_s16(v_max);

    // Calculate scaling factor and save with 8 bits
    int compressed_bound = 1 << 9;
    uint8_t scaling_fact = (max_val + compressed_bound - 1) / compressed_bound;
    scaling_fact = (scaling_fact < INT8_MAX) ? scaling_fact : INT8_MAX;
    dst->exp = (scaling_fact == 0) ? 1 : scaling_fact;

    // Calculate inverse_scaling_fact = 2^(16- N -1) /scaling_fact
    // 2^(16- N -1) = 64
    int16_t inv_scal = 64 / dst->exp;
    int16x8_t inv_scaling_fact = vdupq_n_s16(inv_scal);

    uint16x8_t sign;
    int16x8_t comp_mod;
    int16x8x3_t comp;
    for (uint32_t j = 0; j < 3; j++) {
      // Either UINT16_MAX or 0.
      sign = vreinterpretq_u16_s16(vshrq_n_s16(prb_in.val[j], 15));

      // Calculate inv_scaling_fact * prb_in/ 2^(16 -9)
      // Operation to force rounding to zero
      comp_mod = vreinterpretq_s16_u16(vsraq_n_u16(
          vreinterpretq_u16_s16(vmulq_s16(prb_in.val[j], inv_scaling_fact)),
          sign, 9));

      // We want to saturate in the range [-256, 255]. The shift right of
      // a signed 16-bit number guarantees that we are in this range already
      // so we don't have to explicitly do anything.
      comp.val[j] = vshrq_n_s16(comp_mod, 7);
    }

    pack_9bit_and_store_int16<1>(&comp, dst);

    dst++;
#endif
  }
  return ARMRAL_SUCCESS;
}

armral_status armral_block_scaling_compr_14bit(
    uint32_t n_prb, const armral_cmplx_int16_t *src,
    armral_compressed_data_14bit *dst, const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const int16_t *data_in = (const int16_t *)src;

  // Set true predicate corresponding to 8x16-bit elements
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  for (unsigned int i = 0; i < n_prb; i++) {
    svint16_t prb1_in = svld1_s16(pg, data_in);
    svint16_t prb2_in = svld1_s16(pg, &data_in[8]);
    svint16_t prb3_in = svld1_s16(pg, &data_in[16]);
    data_in += 24;

    if (scale != nullptr) {
      prb1_in = sv_cmplx_mul_combined_re_im(prb1_in, scale->re, scale_im);
      prb2_in = sv_cmplx_mul_combined_re_im(prb2_in, scale->re, scale_im);
      prb3_in = sv_cmplx_mul_combined_re_im(prb3_in, scale->re, scale_im);
    }

    // Extract absolute values
    svint16_t prb1_abs = svqabs_s16_z(pg, prb1_in);
    svint16_t prb2_abs = svqabs_s16_z(pg, prb2_in);
    svint16_t prb3_abs = svqabs_s16_z(pg, prb3_in);

    // Find max in absRe and absIm
    svint16_t v_max =
        svmax_s16_x(pg, prb1_abs, svmax_s16_x(pg, prb2_abs, prb3_abs));
    int16_t max_val = svmaxv_s16(pg, v_max);

    // Determine the scaling factor and save with 8 bits
    int compressed_bound = 1 << 14;
    uint8_t scaling_fact = (max_val + compressed_bound - 1) / compressed_bound;
    scaling_fact = (scaling_fact < INT8_MAX) ? scaling_fact : INT8_MAX;
    dst->exp = (scaling_fact == 0) ? 1 : scaling_fact;

    // Calculate inverse_scaling_fact = 2^(16- N -1) /scaling_fact
    // 2^(16- N -1) = 2
    int16_t inv_scale = 2 / dst->exp;

    // Apply scaling
    svint16_t comp1 =
        svasrd_n_s16_x(pg, svmul_n_s16_x(pg, prb1_in, inv_scale), 2);
    svint16_t comp2 =
        svasrd_n_s16_x(pg, svmul_n_s16_x(pg, prb2_in, inv_scale), 2);
    svint16_t comp3 =
        svasrd_n_s16_x(pg, svmul_n_s16_x(pg, prb3_in, inv_scale), 2);

    // Saturate
    constexpr int16_t sat_pos = 8191;
    constexpr int16_t sat_neg = -8192;

    svint16_t reg1 =
        svmin_n_s16_z(pg, svmax_n_s16_z(pg, comp1, sat_neg), sat_pos);
    svint16_t reg2 =
        svmin_n_s16_z(pg, svmax_n_s16_z(pg, comp2, sat_neg), sat_pos);
    svint16_t reg3 =
        svmin_n_s16_z(pg, svmax_n_s16_z(pg, comp3, sat_neg), sat_pos);

    const svint16_t *regs[] = {&reg1, &reg2, &reg3};
    pack_14bit_and_store_int16<false>(pg, regs, dst);
    dst++;
#else
  for (unsigned int i = 0; i < n_prb; i++) {
    int16x8x3_t prb_in;
    if (scale != nullptr) {
      int16x8x3_t in = load3_cmplx_and_scale((const int16_t *)src, *scale);
      prb_in.val[0] = in.val[0];
      prb_in.val[1] = in.val[1];
      prb_in.val[2] = in.val[2];
      src += 12;
    } else {
      prb_in.val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in.val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in.val[2] = vld1q_s16((const int16_t *)src);
      src += 4;
    }

    // Get absolute value
    int16x8x3_t prb_abs;
    prb_abs.val[0] = vqabsq_s16(prb_in.val[0]);
    prb_abs.val[1] = vqabsq_s16(prb_in.val[1]);
    prb_abs.val[2] = vqabsq_s16(prb_in.val[2]);

    // Find the maximum in absRe and absIm
    int16x8_t v_max =
        vmaxq_s16(prb_abs.val[0], vmaxq_s16(prb_abs.val[1], prb_abs.val[2]));
    int16_t max_val = vmaxvq_s16(v_max);

    // Calculate scaling factor and save with 8 bits
    int compressed_bound = 1 << 14;
    uint8_t scaling_fact = (max_val + compressed_bound - 1) / compressed_bound;
    scaling_fact = (scaling_fact < INT8_MAX) ? scaling_fact : INT8_MAX;
    dst->exp = (scaling_fact == 0) ? 1 : scaling_fact;

    // Calculate inverse_scaling_fact = 2^(16- N -1) /scaling_fact
    int16_t inv_scal = 2 / dst->exp;
    int16x8_t inv_scaling_fact = vdupq_n_s16(inv_scal);

    uint16x8_t sign;
    int16x8_t comp_mod;
    int16x8x3_t comp;
    for (uint32_t j = 0; j < 3; j++) {
      // Either UINT16_MAX or 0.
      sign = vreinterpretq_u16_s16(vshrq_n_s16(prb_in.val[j], 15));

      // Calculate inv_scaling_fact * prb_in/ 2^(16 -14)
      // Operation to force rounding to zero
      comp_mod = vreinterpretq_s16_u16(vsraq_n_u16(
          vreinterpretq_u16_s16(vmulq_s16(prb_in.val[j], inv_scaling_fact)),
          sign, 14));

      // We want to saturate in the range [-8192, 8192]. The shift right of
      // a signed 16-bit number guarantees that we are in this range already
      // so we don't have to explicitly do anything.
      comp.val[j] = vshrq_n_s16(comp_mod, 2);
    }

    pack_14bit_and_store_int16<1, false>(&comp, dst);
    dst++;
#endif
  }
  return ARMRAL_SUCCESS;
}
