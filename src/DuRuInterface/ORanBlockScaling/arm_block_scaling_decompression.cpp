/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include "../bit_unpacking_common.hpp"
#include "intrinsics.h"
#include "utils/vec_mul.hpp"

#if ARMRAL_ARCH_SVE >= 2
#include <arm_sve.h>
#endif

armral_status armral_block_scaling_decompr_8bit(
    uint32_t n_prb, const armral_compressed_data_8bit *src,
    armral_cmplx_int16_t *dst, const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  for (unsigned int i = 0; i < n_prb; i++) {
    svint16_t prb_comp0 = svld1sb_s16(pg, (const int8_t *)&src[i].mantissa[0]);
    svint16_t prb_comp1 = svld1sb_s16(pg, (const int8_t *)&src[i].mantissa[8]);
    svint16_t prb_comp2 = svld1sb_s16(pg, (const int8_t *)&src[i].mantissa[16]);

    int16_t scaling_factor = src[i].exp * 2;
    svint16_t prb_decomp0 = svmul_n_s16_x(pg, prb_comp0, scaling_factor);
    svint16_t prb_decomp1 = svmul_n_s16_x(pg, prb_comp1, scaling_factor);
    svint16_t prb_decomp2 = svmul_n_s16_x(pg, prb_comp2, scaling_factor);

    if (scale != nullptr) {
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(prb_decomp0, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(prb_decomp1, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(prb_decomp2, scale->re, scale_im));
      dst += 4;
    } else {
      svst1_s16(pg, (int16_t *)dst, prb_decomp0);
      dst += 4;
      svst1_s16(pg, (int16_t *)dst, prb_decomp1);
      dst += 4;
      svst1_s16(pg, (int16_t *)dst, prb_decomp2);
      dst += 4;
    }
  }
#else
  for (unsigned int i = 0; i < n_prb; i++) {

    // Load 8bit compressed data and convert into int16_t
    int16x8_t prb_comp_in[3];
    prb_comp_in[0] = vmovl_s8(vld1_s8((const int8_t *)&src[i].mantissa[0]));
    prb_comp_in[1] = vmovl_s8(vld1_s8((const int8_t *)&src[i].mantissa[8]));
    prb_comp_in[2] = vmovl_s8(vld1_s8((const int8_t *)&src[i].mantissa[16]));

    // Decompression process
    int16x8_t prb_decomp[3];
    // prb_decomp = prb_comp_in * scaling_factor/ 2^(bitwidth -9);
    int16_t scaling_factor = src[i].exp * 2;
    prb_decomp[0] = vmulq_n_s16(prb_comp_in[0], scaling_factor);
    prb_decomp[1] = vmulq_n_s16(prb_comp_in[1], scaling_factor);
    prb_decomp[2] = vmulq_n_s16(prb_comp_in[2], scaling_factor);

    if (scale != nullptr) {
      vst1q_s16((int16_t *)&dst[0],
                cmplx_mul_combined_re_im(prb_decomp[0], *scale));
      vst1q_s16((int16_t *)&dst[4],
                cmplx_mul_combined_re_im(prb_decomp[1], *scale));
      vst1q_s16((int16_t *)&dst[8],
                cmplx_mul_combined_re_im(prb_decomp[2], *scale));
      dst += 12;
    } else {
      vst1q_s16((int16_t *)dst, prb_decomp[0]);
      dst += 4;
      vst1q_s16((int16_t *)dst, prb_decomp[1]);
      dst += 4;
      vst1q_s16((int16_t *)dst, prb_decomp[2]);
      dst += 4;
    }
  }
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_block_scaling_decompr_9bit(
    uint32_t n_prb, const armral_compressed_data_9bit *src,
    armral_cmplx_int16_t *dst, const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  // Shifts for high bits are actually performed as a multiply, so
  // { 2^8, 2^9, 2^10, ...}
  const svuint16_t high_shifts =
      svdupq_n_u16(256, 512, 1024, 2048, 4096, 8192, 16384, 32768);
  // Shifts for low bits need to be { 0, 1, 2, 3, ..., 7 }
  // Therefore the following only works if we limit ourselves to using the
  // first 128 bits of vectors (which is what we currently do)
  const svuint16_t low_shifts = svindex_u16(0, 1);
  // Bitmask to identify which bits we want to preserve of the bottom bits.
  const svuint16_t mask = svdupq_n_u16(0x0080, 0x00c0, 0x00e0, 0x00f0, 0x00f8,
                                       0x00fc, 0x00fe, 0x00ff);

  for (unsigned int i = 0; i < n_prb; i++) {
    const uint8_t *data_in = (const uint8_t *)&src->mantissa[0];

    // Load in the input data byte by byte, zero-extending to 16-bits.
    // Load top bits (0-7)
    svuint16_t a07 = svld1ub_u16(pg, &data_in[0]);
    svuint16_t b07 = svld1ub_u16(pg, &data_in[9]);
    svuint16_t c07 = svld1ub_u16(pg, &data_in[18]);

    // Load bottom bits (1-8)
    svuint16_t a18 = svld1ub_u16(pg, &data_in[1]);
    svuint16_t b18 = svld1ub_u16(pg, &data_in[10]);
    svuint16_t c18 = svld1ub_u16(pg, &data_in[19]);

    // Isolate bottom bits of each value, and align with the end of the top
    // bits.
    svuint16_t a_low = svlsl_u16_x(pg, svand_u16_x(pg, a18, mask), low_shifts);
    svuint16_t b_low = svlsl_u16_x(pg, svand_u16_x(pg, b18, mask), low_shifts);
    svuint16_t c_low = svlsl_u16_x(pg, svand_u16_x(pg, c18, mask), low_shifts);

    // Combine parts into wholes.
    // Here an LSL followed by an ORR is equivalent to an MLA.
    svint16_t a_comp_in =
        svreinterpret_s16_u16(svmla_u16_x(pg, a_low, a07, high_shifts));
    svint16_t b_comp_in =
        svreinterpret_s16_u16(svmla_u16_x(pg, b_low, b07, high_shifts));
    svint16_t c_comp_in =
        svreinterpret_s16_u16(svmla_u16_x(pg, c_low, c07, high_shifts));

    a_comp_in = svasr_n_s16_x(pg, a_comp_in, 7);
    b_comp_in = svasr_n_s16_x(pg, b_comp_in, 7);
    c_comp_in = svasr_n_s16_x(pg, c_comp_in, 7);

    const int16_t scaling_factor = src->exp;

    // Decompression process
    svint16_t a_decomp = svmul_n_s16_x(pg, a_comp_in, scaling_factor);
    svint16_t b_decomp = svmul_n_s16_x(pg, b_comp_in, scaling_factor);
    svint16_t c_decomp = svmul_n_s16_x(pg, c_comp_in, scaling_factor);

    // Store decompressed data
    if (scale != nullptr) {
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(a_decomp, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(b_decomp, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(c_decomp, scale->re, scale_im));
      dst += 4;
    } else {
      svst1_s16(pg, (int16_t *)dst, a_decomp);
      dst += 4;
      svst1_s16(pg, (int16_t *)dst, b_decomp);
      dst += 4;
      svst1_s16(pg, (int16_t *)dst, c_decomp);
      dst += 4;
    }
    src++;
  }
#else
  common_decompr_9bit_neon<false, false>(n_prb, src, dst, scale);
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_block_scaling_decompr_14bit(
    uint32_t n_prb, const armral_compressed_data_14bit *src,
    armral_cmplx_int16_t *dst, const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  // Set true predicate corresponding to 14x 8-bit elements
  // (112 bits in total)
  const svbool_t pg14 = svwhilelt_b8(0, 14);

  // Set true predicate corresponding to 8x 16-bit elements
  // (128 bits in total)
  const svbool_t pg16 = svptrue_pat_b16(SV_VL8);

  // Shifts and masks used during unpacking
  const svuint8_t contig_idx =
      svdupq_n_u8(1, 0, 3, 2, 5, 4, 255, 6, 8, 7, 10, 9, 12, 11, 255, 13);
  const svuint8_t contig_idx2 = svdupq_n_u8(255, 255, 255, 1, 255, 3, 255, 5,
                                            255, 255, 255, 8, 255, 10, 255, 12);
  const svuint16_t contig_rshift = svdupq_n_u16(0, 2, 4, 6, 0, 2, 4, 6);
  // The left shifts are performed as multiplies, so contig_lshift = {2^0, 2^6,
  // 2^4, 2^2, ...}
  const svuint16_t contig_lshift = svdupq_n_u16(1, 64, 16, 4, 1, 64, 16, 4);

  for (unsigned int i = 0; i < n_prb; i++) {
    const uint8_t *data_in = (const uint8_t *)&src->mantissa[0];
    // load 14*24/8 = 42 bytes total
    svuint8_t uprb_in0 = svld1_u8(pg14, data_in);
    svuint8_t uprb_in1 = svld1_u8(pg14, data_in + 14);
    svuint8_t uprb_in2 = svld1_u8(pg14, data_in + 28);

    // permute
    // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
    // (capital indicates msb of original data)
    // into
    // [dddddddd00000000|ccccccccccDddddd|bbbbbbbbbbbbCccc|AaaaaaaaaaaaaaBb]
    // (Aa... now contiguous)
    // note this pattern repeats for bytes 7-13 as well
    svuint16_t contig0_r = svreinterpret_u16_u8(svtbl_u8(uprb_in0, contig_idx));
    svuint16_t contig1_r = svreinterpret_u16_u8(svtbl_u8(uprb_in1, contig_idx));
    svuint16_t contig2_r = svreinterpret_u16_u8(svtbl_u8(uprb_in2, contig_idx));

    // contig_rshift = {0, 2, 4, 6, ...}
    // [000000dddddddd00|0000ccccccccccDd|00bbbbbbbbbbbbCc|AaaaaaaaaaaaaaBb]
    contig0_r = svlsr_u16_x(pg16, contig0_r, contig_rshift);
    contig1_r = svlsr_u16_x(pg16, contig1_r, contig_rshift);
    contig2_r = svlsr_u16_x(pg16, contig2_r, contig_rshift);

    // Get 0 MSBs of A, 2 MSBs of B, 4 MSBs of C and 6 MSBs of D
    // [ccDddddd00000000|bbbbCccc00000000|aaaaaaBb00000000|0000000000000000]
    svuint16_t contig0_l =
        svreinterpret_u16_u8(svtbl_u8(uprb_in0, contig_idx2));
    svuint16_t contig1_l =
        svreinterpret_u16_u8(svtbl_u8(uprb_in1, contig_idx2));
    svuint16_t contig2_l =
        svreinterpret_u16_u8(svtbl_u8(uprb_in2, contig_idx2));

    // Combine LSBs and MSBs. Here LSL followed by ORR is equivalent to MLA.
    // contig_lshift = {2^0, 2^6, 2^4, 2^2, ...}
    // [Dddddddddddddd00|CcccccccccccccDd|BbbbbbbbbbbbbbCc|AaaaaaaaaaaaaaBb]
    svint16_t pack0 = svreinterpret_s16_u16(
        svmla_u16_x(pg16, contig0_r, contig0_l, contig_lshift));
    svint16_t pack1 = svreinterpret_s16_u16(
        svmla_u16_x(pg16, contig1_r, contig1_l, contig_lshift));
    svint16_t pack2 = svreinterpret_s16_u16(
        svmla_u16_x(pg16, contig2_r, contig2_l, contig_lshift));

    // Decompression process
    svint16_t prb_in0 = svasr_n_s16_x(pg16, pack0, 2);
    svint16_t prb_in1 = svasr_n_s16_x(pg16, pack1, 2);
    svint16_t prb_in2 = svasr_n_s16_x(pg16, pack2, 2);

    // Construct predicates matching negative values
    svbool_t pgneg0 = svcmplt_n_s16(pg16, pack0, 0);
    svbool_t pgneg1 = svcmplt_n_s16(pg16, pack1, 0);
    svbool_t pgneg2 = svcmplt_n_s16(pg16, pack2, 0);

    svint16_t sign0 = svdup_n_s16_z(pgneg0, 31);
    svint16_t sign1 = svdup_n_s16_z(pgneg1, 31);
    svint16_t sign2 = svdup_n_s16_z(pgneg2, 31);

    // Scale and shift right
    int16_t scaling = src->exp;
    svint16_t comp_mod0 = svmla_n_s16_x(pg16, sign0, prb_in0, scaling);
    svint16_t comp_mod1 = svmla_n_s16_x(pg16, sign1, prb_in1, scaling);
    svint16_t comp_mod2 = svmla_n_s16_x(pg16, sign2, prb_in2, scaling);

    svint16_t prb_decomp0 = svasr_n_s16_x(pg16, comp_mod0, 5);
    svint16_t prb_decomp1 = svasr_n_s16_x(pg16, comp_mod1, 5);
    svint16_t prb_decomp2 = svasr_n_s16_x(pg16, comp_mod2, 5);

    if (scale != nullptr) {
      svst1_s16(pg16, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(prb_decomp0, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg16, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(prb_decomp1, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg16, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(prb_decomp2, scale->re, scale_im));
      dst += 4;
    } else {
      svst1_s16(pg16, (int16_t *)dst, prb_decomp0);
      dst += 4;
      svst1_s16(pg16, (int16_t *)dst, prb_decomp1);
      dst += 4;
      svst1_s16(pg16, (int16_t *)dst, prb_decomp2);
      dst += 4;
    }
    src++;
  }
  return ARMRAL_SUCCESS;
#else
  for (unsigned int i = 0; i < n_prb; i++) {
    const uint8_t *data_in = (const uint8_t *)&src->mantissa[0];

    // need to load 14*24/8 = 42 bytes total.
    uint8x16_t uprb_in[3];
    uprb_in[0] = vld1q_u8(data_in); // first 14 bytes are interesting
    data_in += 14;
    uprb_in[1] = vld1q_u8(data_in); // first 14 bytes are interesting
    data_in += 12;
    uprb_in[2] = vld1q_u8(data_in); // last 14 bytes are interesting

    // permute
    //     6        5         4       3        2       1         0
    // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
    // (capital indicates msb of original data)
    // into
    //      6,5                4,3             3,2               1,0
    // [ccDddddddddddddd|bbbbCccccccccccc|bbbbbbbbbbbbCccc|AaaaaaaaaaaaaaBb]
    // (Aa... and Dd... now contiguous)
    // note this pattern repeats for bytes 7-13 as well
    uint8x16_t contig01_idx =
        uint8x16_t{1, 0, 3, 2, 4, 3, 6, 5, 8, 7, 10, 9, 11, 10, 13, 12};
    uint8x16_t contig2_idx =
        uint8x16_t{3, 2, 5, 4, 6, 5, 8, 7, 10, 9, 12, 11, 13, 12, 15, 14};
    uint16x8_t contig[3];
    contig[0] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[0], contig01_idx));
    contig[1] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[1], contig01_idx));
    contig[2] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[2], contig2_idx));

    // shift MSBs to most significant bit positions
    int16x8_t contig_shift = int16x8_t{0, -2, 4, 2, 0, -2, 4, 2};
    contig[0] = vshlq_u16(contig[0], contig_shift);
    contig[1] = vshlq_u16(contig[1], contig_shift);
    contig[2] = vshlq_u16(contig[2], contig_shift);

    // get the missing bits of B, C, F and G
    // permute
    // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
    // into
    // [00000000|00000000|00000000|0000ccDd|Bb000000|00000000|00000000|00000000]
    uint8x16_t fill01_idx = uint8x16_t{255, 255, 255, 1, 5,  255, 255, 255,
                                       255, 255, 255, 8, 12, 255, 255, 255};
    uint8x16_t fill2_idx = uint8x16_t{255, 255, 255, 3,  7,  255, 255, 255,
                                      255, 255, 255, 10, 14, 255, 255, 255};

    uint16x8_t fill[3];
    fill[0] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[0], fill01_idx));
    fill[1] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[1], fill01_idx));
    fill[2] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[2], fill2_idx));

    int16x8_t fill_shift = int16x8_t{0, 6, -4, 0, 0, 6, -4, 0};
    fill[0] = vshlq_u16(fill[0], fill_shift);
    fill[1] = vshlq_u16(fill[1], fill_shift);
    fill[2] = vshlq_u16(fill[2], fill_shift);

    // combine everything together
    uint16x8_t sbli = {0xfffc, 0x3ffc, 0xfff3, 0xfffc,
                       0xfffc, 0x3ffc, 0xfff3, 0xfffc};
    int16x8_t pack[3];
    pack[0] = vreinterpretq_s16_u16(vbslq_u16(sbli, contig[0], fill[0]));
    pack[1] = vreinterpretq_s16_u16(vbslq_u16(sbli, contig[1], fill[1]));
    pack[2] = vreinterpretq_s16_u16(vbslq_u16(sbli, contig[2], fill[2]));

    // Decompression
    int16x8_t prb_in[3];
    prb_in[0] = vshrq_n_s16(pack[0], 2);
    prb_in[1] = vshrq_n_s16(pack[1], 2);
    prb_in[2] = vshrq_n_s16(pack[2], 2);

    // Decompression process
    uint16x8_t sign[3];
    sign[0] = vcltzq_s16(prb_in[0]);
    sign[1] = vcltzq_s16(prb_in[1]);
    sign[2] = vcltzq_s16(prb_in[2]);

    uint16x8_t comp_mod[3];
    int16x8_t scaling = vdupq_n_s16(src->exp);
    // Scale, shift right and round towards zero
    comp_mod[0] = vsraq_n_u16(
        vreinterpretq_u16_s16(vmulq_s16(prb_in[0], scaling)), sign[0], 11);
    comp_mod[1] = vsraq_n_u16(
        vreinterpretq_u16_s16(vmulq_s16(prb_in[1], scaling)), sign[1], 11);
    comp_mod[2] = vsraq_n_u16(
        vreinterpretq_u16_s16(vmulq_s16(prb_in[2], scaling)), sign[2], 11);

    int16x8_t prb_decomp[3];
    prb_decomp[0] = vshrq_n_s16(vreinterpretq_s16_u16(comp_mod[0]), 5);
    prb_decomp[1] = vshrq_n_s16(vreinterpretq_s16_u16(comp_mod[1]), 5);
    prb_decomp[2] = vshrq_n_s16(vreinterpretq_s16_u16(comp_mod[2]), 5);

    // Store decompressed data
    if (scale != nullptr) {
      vst1q_s16((int16_t *)&dst[0],
                cmplx_mul_combined_re_im(prb_decomp[0], *scale));
      vst1q_s16((int16_t *)&dst[4],
                cmplx_mul_combined_re_im(prb_decomp[1], *scale));
      vst1q_s16((int16_t *)&dst[8],
                cmplx_mul_combined_re_im(prb_decomp[2], *scale));
      dst += 12;
    } else {
      vst1q_s16((int16_t *)dst, prb_decomp[0]);
      dst += 4;
      vst1q_s16((int16_t *)dst, prb_decomp[1]);
      dst += 4;
      vst1q_s16((int16_t *)dst, prb_decomp[2]);
      dst += 4;
    }
    src++;
  }
  return ARMRAL_SUCCESS;
#endif
}
