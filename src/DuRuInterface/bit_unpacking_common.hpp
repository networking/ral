/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2024-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "utils/vec_mul.hpp"

namespace {

inline void mu_law_decomp_and_store_9bit_neon(int16x8x3_t &prb_comp,
                                              armral_cmplx_int16_t *dst,
                                              const armral_cmplx_int16_t *scale,
                                              int16_t shift) {
  // Extract the sign bit and absolute values for the PRB
  int16x8x3_t prb_signs;
  prb_signs.val[0] = vreinterpretq_s16_u16(
      vsubq_u16(vcltzq_s16(prb_comp.val[0]), vcgezq_s16(prb_comp.val[0])));
  prb_signs.val[1] = vreinterpretq_s16_u16(
      vsubq_u16(vcltzq_s16(prb_comp.val[1]), vcgezq_s16(prb_comp.val[1])));
  prb_signs.val[2] = vreinterpretq_s16_u16(
      vsubq_u16(vcltzq_s16(prb_comp.val[2]), vcgezq_s16(prb_comp.val[2])));

  int16x8x3_t prb_comp_abs;
  int16x8_t sat_pos = vdupq_n_s16(255);
  prb_comp_abs.val[0] = vminq_s16(sat_pos, vqabsq_s16(prb_comp.val[0]));
  prb_comp_abs.val[1] = vminq_s16(sat_pos, vqabsq_s16(prb_comp.val[1]));
  prb_comp_abs.val[2] = vminq_s16(sat_pos, vqabsq_s16(prb_comp.val[2]));

  // Expand each sample, absBitWidth=15, compBitWidth=9
  uint16x8x3_t check_thr1;
  uint16x8x3_t check_thr3;

  // Expand - First Step: Set bitmasks based on prbCompAbs values
  // Check1: if prbCompAbs <= 2^(compBitWidth - 2) = 128
  int16x8_t thr1_b9 = vdupq_n_s16(128);
  check_thr1.val[0] = vcleq_s16(prb_comp_abs.val[0], thr1_b9);
  check_thr1.val[1] = vcleq_s16(prb_comp_abs.val[1], thr1_b9);
  check_thr1.val[2] = vcleq_s16(prb_comp_abs.val[2], thr1_b9);

  // Check3: if prbCompAbs > (2^(compBitWidth - 2) + 2^(compBitWidth - 3))
  int16x8_t thr2_b9 = vdupq_n_s16(192);
  check_thr3.val[0] = vcgtq_s16(prb_comp_abs.val[0], thr2_b9);
  check_thr3.val[1] = vcgtq_s16(prb_comp_abs.val[1], thr2_b9);
  check_thr3.val[2] = vcgtq_s16(prb_comp_abs.val[2], thr2_b9);

  // Expand - Second Step: Perform decompression calculation
  int16x8x3_t prb_abs_res1;
  int16x8x3_t prb_abs_res2;
  int16x8x3_t prb_abs_res3;

  // Case1: prbAbsRes1 = prbCompAbs * 2^(input_bits - output_bits)
  //  input_bits - output_bits = 6
  prb_abs_res1.val[0] = vqshlq_n_s16(prb_comp_abs.val[0], 6);
  prb_abs_res1.val[1] = vqshlq_n_s16(prb_comp_abs.val[1], 6);
  prb_abs_res1.val[2] = vqshlq_n_s16(prb_comp_abs.val[2], 6);

  // Case2: prbAbsRes2 = prbCompAbs * 2^(input_bits - output_bits + 1) - 2^13
  //  input_bits - output_bits + 1 = 7
  int16x8_t sub_thr2_b9 = vdupq_n_s16(8192);
  prb_abs_res2.val[0] =
      vsubq_s16(vqshlq_n_s16(prb_comp_abs.val[0], 7), sub_thr2_b9);
  prb_abs_res2.val[1] =
      vsubq_s16(vqshlq_n_s16(prb_comp_abs.val[1], 7), sub_thr2_b9);
  prb_abs_res2.val[2] =
      vsubq_s16(vqshlq_n_s16(prb_comp_abs.val[2], 7), sub_thr2_b9);

  // Case3: prbAbsRes3 = prbCompAbs * 2^(absBitWidth - compBitWidth + 2) -
  // 2^15
  //  input_bits - output_bits + 2 = 8
  uint16x8_t sub_comm_b9 = vdupq_n_u16(32768);
  prb_abs_res3.val[0] = vreinterpretq_s16_u16(
      vsubq_u16(vqshluq_n_s16(prb_comp_abs.val[0], 8), sub_comm_b9));
  prb_abs_res3.val[1] = vreinterpretq_s16_u16(
      vsubq_u16(vqshluq_n_s16(prb_comp_abs.val[1], 8), sub_comm_b9));
  prb_abs_res3.val[2] = vreinterpretq_s16_u16(
      vsubq_u16(vqshluq_n_s16(prb_comp_abs.val[2], 8), sub_comm_b9));

  // Expand - Fourth Step: OR among prbAbsRes vectors
  int16x8x3_t exp_samples;
  exp_samples.val[0] = vbslq_s16(
      check_thr1.val[0], prb_abs_res1.val[0],
      vbslq_s16(check_thr3.val[0], prb_abs_res3.val[0], prb_abs_res2.val[0]));
  exp_samples.val[1] = vbslq_s16(
      check_thr1.val[1], prb_abs_res1.val[1],
      vbslq_s16(check_thr3.val[1], prb_abs_res3.val[1], prb_abs_res2.val[1]));
  exp_samples.val[2] = vbslq_s16(
      check_thr1.val[2], prb_abs_res1.val[2],
      vbslq_s16(check_thr3.val[2], prb_abs_res3.val[2], prb_abs_res2.val[2]));

  // Apply sign and shift
  exp_samples.val[0] = vmulq_s16(exp_samples.val[0], prb_signs.val[0]);
  exp_samples.val[1] = vmulq_s16(exp_samples.val[1], prb_signs.val[1]);
  exp_samples.val[2] = vmulq_s16(exp_samples.val[2], prb_signs.val[2]);

  int16x8_t comp_shift_vec = vdupq_n_s16(-shift);

  exp_samples.val[0] = vshlq_s16(exp_samples.val[0], comp_shift_vec);
  exp_samples.val[1] = vshlq_s16(exp_samples.val[1], comp_shift_vec);
  exp_samples.val[2] = vshlq_s16(exp_samples.val[2], comp_shift_vec);

  // Store
  if (scale != nullptr) {
    int16x8x2_t scale_v;
    scale_v.val[0] = vdupq_n_s16(scale->re);
    scale_v.val[1] = vdupq_n_s16(scale->im);
    scale_and_store3_cmplx((int16_t *)dst, exp_samples.val[0],
                           exp_samples.val[1], exp_samples.val[2], scale_v);
  } else {
    vst1q_s16((int16_t *)&dst[0], exp_samples.val[0]);
    vst1q_s16((int16_t *)&dst[4], exp_samples.val[1]);
    vst1q_s16((int16_t *)&dst[8], exp_samples.val[2]);
  }
}

template<bool is_mu_law, bool is_block_float>
void common_decompr_9bit_neon(uint32_t n_prb,
                              const armral_compressed_data_9bit *src,
                              armral_cmplx_int16_t *dst,
                              const armral_cmplx_int16_t *scale) {
  for (uint32_t i = 0; i < n_prb; i++) {
    int16_t exp = src->exp;
    const uint8_t *data_in = (const uint8_t *)&src->mantissa[0];

    // Load in the input data byte by byte
    // ABCDEFGH
    uint8x8_t a07 = vld1_u8(&data_in[0]);
    uint8x8_t b07 = vld1_u8(&data_in[9]);
    uint8x8_t c07 = vld1_u8(&data_in[18]);
    // BCDEFGHI
    uint8x8_t a18 = vld1_u8(&data_in[1]);
    uint8x8_t b18 = vld1_u8(&data_in[10]);
    uint8x8_t c18 = vld1_u8(&data_in[19]);

    uint8x8_t left_shifts = {1, 2, 4, 8, 16, 32, 64, 128};
    int8x8_t right_shifts = {-7, -6, -5, -4, -3, -2, -1, 0};

    uint16x8_t a_left = vshll_n_u8(vmul_u8(a07, left_shifts), 8);
    uint16x8_t b_left = vshll_n_u8(vmul_u8(b07, left_shifts), 8);
    uint16x8_t c_left = vshll_n_u8(vmul_u8(c07, left_shifts), 8);
    uint16x8_t a_right = vshll_n_u8(vshl_u8(a18, right_shifts), 7);
    uint16x8_t b_right = vshll_n_u8(vshl_u8(b18, right_shifts), 7);
    uint16x8_t c_right = vshll_n_u8(vshl_u8(c18, right_shifts), 7);

    int16x8x3_t prb_comp;
    int16_t shift = 7;
    if constexpr (is_block_float) {
      shift -= exp;
    }
    prb_comp.val[0] =
        vreinterpretq_s16_u16(vorrq_u16(a_left, a_right)) >> shift;
    prb_comp.val[1] =
        vreinterpretq_s16_u16(vorrq_u16(b_left, b_right)) >> shift;
    prb_comp.val[2] =
        vreinterpretq_s16_u16(vorrq_u16(c_left, c_right)) >> shift;

    if constexpr (is_mu_law) {
      mu_law_decomp_and_store_9bit_neon(prb_comp, dst, scale, exp);
    } else {
      if constexpr (not is_block_float) { // Block Scaling
        prb_comp.val[0] = vmulq_n_s16(prb_comp.val[0], exp);
        prb_comp.val[1] = vmulq_n_s16(prb_comp.val[1], exp);
        prb_comp.val[2] = vmulq_n_s16(prb_comp.val[2], exp);
      }

      // Block Float and Scaling store
      if (scale != nullptr) {
        vst1q_s16((int16_t *)&dst[0],
                  cmplx_mul_combined_re_im(prb_comp.val[0], *scale));
        vst1q_s16((int16_t *)&dst[4],
                  cmplx_mul_combined_re_im(prb_comp.val[1], *scale));
        vst1q_s16((int16_t *)&dst[8],
                  cmplx_mul_combined_re_im(prb_comp.val[2], *scale));
      } else {
        vst1q_s16((int16_t *)&dst[0], prb_comp.val[0]);
        vst1q_s16((int16_t *)&dst[4], prb_comp.val[1]);
        vst1q_s16((int16_t *)&dst[8], prb_comp.val[2]);
      }
    }
    dst += 12;
    src++;
  }
}

} // namespace
