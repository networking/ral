/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include "../bit_unpacking_common.hpp"
#include "utils/vec_mul.hpp"

#if ARMRAL_ARCH_SVE >= 2
#include <arm_sve.h>
#endif

// Input size is 15 bits wide. Output size is 8, 9 or 14 bits wide. We use the
// names input_bits and output_bits in the comments below to refer to these
// values
#if ARMRAL_ARCH_SVE >= 2
// output_bits = 8
static const int16_t thr1 = 64; // 2^(8 - 2) = 2^6
static const int16_t thr2 = 96; // (2^(8 - 2) + 2^(8 - 3)) = 96
// output_bits = 9
static const int16_t thr1_b9 = 128; // 2^(9 - 2) = 2^7
static const int16_t thr2_b9 = 192; // (2^(9 - 2) + 2^(9 - 3)) = 192
// output_bits = 14
static const int16_t thr1_b14 = 4096; // 2^(14 - 2) = 2^12
static const int16_t thr2_b14 = 6144; // (2^(14 - 2) + 2^(14 - 3)) = 6144

static const int16_t sub_thr2 = 8192;   // 2^13
static const uint16_t sub_comm = 32768; // 2^15
#else
static const int16x8_t thr1 __attribute__((aligned(16))) = {
    64, 64, 64, 64, 64, 64, 64, 64}; // 2^(output_bits - 2) = 2^6
static const int16x8_t thr2 __attribute__((aligned(16))) = {
    96, 96, 96, 96, 96,
    96, 96, 96}; // (2^(output_bits - 2) + 2^(output_bits - 3)) = 2^6 + 2^5 = 96

static const int16x8_t sub_thr2 __attribute__((aligned(16))) = {
    8192, 8192, 8192, 8192, 8192, 8192, 8192, 8192}; // 2^13

static const uint16x8_t sub_comm __attribute__((aligned(16))) = {
    32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768}; // 2^15
#endif

armral_status armral_mu_law_decompr_8bit(const uint32_t n_prb,
                                         const armral_compressed_data_8bit *src,
                                         armral_cmplx_int16_t *dst,
                                         const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  // Set true predicate corresponding to 8x 16-bit elements
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  // Set true predicate corresponding to 16x 8-bit elements
  const svbool_t pg_b8x16 = svptrue_pat_b8(SV_VL16);

  for (uint32_t i = 0; i < n_prb; i++) {
    const uint8_t *data_in = (const uint8_t *)src[i].mantissa;

    svint16_t prb1_comp = svld1ub_s16(pg, &data_in[0]);
    svint16_t prb2_comp = svld1ub_s16(pg, &data_in[8]);
    svint16_t prb3_comp = svld1ub_s16(pg, &data_in[16]);

    // Set sign predicate
    const int16_t thr_neg = 128; // 2^7
    svbool_t pg_prb1_comp_signs = svcmpge_n_s16(pg, prb1_comp, thr_neg);
    svbool_t pg_prb2_comp_signs = svcmpge_n_s16(pg, prb2_comp, thr_neg);
    svbool_t pg_prb3_comp_signs = svcmpge_n_s16(pg, prb3_comp, thr_neg);

    // Extract absolute values for the PRB
    svuint8_t trunc1 = svqxtunb_s16(prb1_comp);
    svuint8_t trunc2 = svqxtunb_s16(prb2_comp);
    svuint8_t trunc3 = svqxtunb_s16(prb3_comp);
    svint16_t prb1_comp_abs =
        svmovlb_s16(svqabs_s8_x(pg_b8x16, svreinterpret_s8_u8(trunc1)));
    svint16_t prb2_comp_abs =
        svmovlb_s16(svqabs_s8_x(pg_b8x16, svreinterpret_s8_u8(trunc2)));
    svint16_t prb3_comp_abs =
        svmovlb_s16(svqabs_s8_x(pg_b8x16, svreinterpret_s8_u8(trunc3)));

    // Expand each sample, absBitWidth=15, compBitWidth=8

    // Case1: check if prbCompAbs <= 2^(compBitWidth - 2) = 64
    svbool_t pg1_case1 = svcmple_n_s16(pg, prb1_comp_abs, thr1);
    svbool_t pg2_case1 = svcmple_n_s16(pg, prb2_comp_abs, thr1);
    svbool_t pg3_case1 = svcmple_n_s16(pg, prb3_comp_abs, thr1);

    // Case3: check if prbCompAbs > (2^(compBitWidth - 2) + 2^(compBitWidth -
    // 3))
    svbool_t pg1_case3 = svcmpgt_n_s16(pg, prb1_comp_abs, thr2);
    svbool_t pg2_case3 = svcmpgt_n_s16(pg, prb2_comp_abs, thr2);
    svbool_t pg3_case3 = svcmpgt_n_s16(pg, prb3_comp_abs, thr2);

    // Case2: check if(prbCompAbs > 2^(compBitWidth - 2)) && (prbCompAbs <=
    // (2^(compBitWidth - 2) + 2^( compBitWidth - 3)))
    svbool_t pg1_case2 = svnor_b_z(pg, pg1_case1, pg1_case3);
    svbool_t pg2_case2 = svnor_b_z(pg, pg2_case1, pg2_case3);
    svbool_t pg3_case2 = svnor_b_z(pg, pg3_case1, pg3_case3);

    // Expand - Second Step: Perform decompression calculation

    // Case1: prbAbsRes1 = prbCompAbs * 2^(input_bits - output_bits)
    //  input_bits - output_bits = 7
    svint16_t result1 = svrshl_n_s16_m(pg1_case1, prb1_comp_abs, 7);
    svint16_t result2 = svrshl_n_s16_m(pg2_case1, prb2_comp_abs, 7);
    svint16_t result3 = svrshl_n_s16_m(pg3_case1, prb3_comp_abs, 7);

    // Case2: prbAbsRes2 = prbCompAbs * 2^(input_bits - output_bits + 1) - 2^13
    //  input_bits - output_bits + 1 = 8
    result1 = svsub_n_s16_m(pg1_case2, svrshl_n_s16_m(pg1_case2, result1, 8),
                            sub_thr2);
    result2 = svsub_n_s16_m(pg2_case2, svrshl_n_s16_m(pg2_case2, result2, 8),
                            sub_thr2);
    result3 = svsub_n_s16_m(pg3_case2, svrshl_n_s16_m(pg3_case2, result3, 8),
                            sub_thr2);

    // Case3: prbAbsRes3 = prbCompAbs * 2^(absBitWidth - compBitWidth + 2) -
    // 2^15
    //  input_bits - output_bits + 2 = 9
    result1 = svsub_n_s16_m(pg1_case3, svrshl_n_s16_m(pg1_case3, result1, 9),
                            sub_comm);
    result2 = svsub_n_s16_m(pg2_case3, svrshl_n_s16_m(pg2_case3, result2, 9),
                            sub_comm);
    result3 = svsub_n_s16_m(pg3_case3, svrshl_n_s16_m(pg3_case3, result3, 9),
                            sub_comm);

    // Apply sign
    result1 = svneg_s16_m(result1, pg_prb1_comp_signs, result1);
    result2 = svneg_s16_m(result2, pg_prb2_comp_signs, result2);
    result3 = svneg_s16_m(result3, pg_prb3_comp_signs, result3);

    // Shift
    svint16_t comp_shift_vec = svdup_n_s16_z(pg, -src[i].exp);
    result1 = svqshl_s16_x(pg, result1, comp_shift_vec);
    result2 = svqshl_s16_x(pg, result2, comp_shift_vec);
    result3 = svqshl_s16_x(pg, result3, comp_shift_vec);

    if (scale != nullptr) {
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(result1, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(result2, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(result3, scale->re, scale_im));
      dst += 4;
    } else {
      svst1_s16(pg, (int16_t *)dst, result1);
      dst += 4;
      svst1_s16(pg, (int16_t *)dst, result2);
      dst += 4;
      svst1_s16(pg, (int16_t *)dst, result3);
      dst += 4;
    }
  }
  return ARMRAL_SUCCESS;
#else
  int16x8x2_t scale_v;
  if (scale != nullptr) {
    scale_v.val[0] = vdupq_n_s16(scale->re);
    scale_v.val[1] = vdupq_n_s16(scale->im);
  }

  for (uint32_t i = 0; i < n_prb; i++) {
    int8x8x3_t prb_comp_in;
    prb_comp_in.val[0] = vld1_s8((const int8_t *)&src[i].mantissa[0]);
    prb_comp_in.val[1] = vld1_s8((const int8_t *)&src[i].mantissa[8]);
    prb_comp_in.val[2] = vld1_s8((const int8_t *)&src[i].mantissa[16]);

    // Extract the sign bit and absolute values for the PRB
    int8x8x3_t prb_signs;
    prb_signs.val[0] = vreinterpret_s8_u8(
        vsub_u8(vcltz_s8(prb_comp_in.val[0]), vcgez_s8(prb_comp_in.val[0])));
    prb_signs.val[1] = vreinterpret_s8_u8(
        vsub_u8(vcltz_s8(prb_comp_in.val[1]), vcgez_s8(prb_comp_in.val[1])));
    prb_signs.val[2] = vreinterpret_s8_u8(
        vsub_u8(vcltz_s8(prb_comp_in.val[2]), vcgez_s8(prb_comp_in.val[2])));

    int16x8x3_t prb_comp_abs;
    prb_comp_abs.val[0] = vmovl_s8(vqabs_s8(prb_comp_in.val[0]));
    prb_comp_abs.val[1] = vmovl_s8(vqabs_s8(prb_comp_in.val[1]));
    prb_comp_abs.val[2] = vmovl_s8(vqabs_s8(prb_comp_in.val[2]));

    // Expand each sample, absBitWidth=15, compBitWidth=8
    uint16x8x3_t check_thr1;
    uint16x8x3_t check_thr3;

    // Expand - First Step: Set bitmasks based on prbCompAbs values
    // Check1: if prbCompAbs <= 2^(compBitWidth - 2) = 64
    check_thr1.val[0] = vcleq_s16(prb_comp_abs.val[0], thr1);
    check_thr1.val[1] = vcleq_s16(prb_comp_abs.val[1], thr1);
    check_thr1.val[2] = vcleq_s16(prb_comp_abs.val[2], thr1);

    // Check3: if prbCompAbs > (2^(compBitWidth - 2) + 2^(compBitWidth - 3))
    check_thr3.val[0] = vcgtq_s16(prb_comp_abs.val[0], thr2);
    check_thr3.val[1] = vcgtq_s16(prb_comp_abs.val[1], thr2);
    check_thr3.val[2] = vcgtq_s16(prb_comp_abs.val[2], thr2);

    // Expand - Second Step: Perform decompression calculation
    int16x8x3_t prb_abs_res1;
    int16x8x3_t prb_abs_res2;
    int16x8x3_t prb_abs_res3;

    // Case1: prbAbsRes1 = prbCompAbs * 2^(input_bits - output_bits)
    //  input_bits - output_bits = 7
    prb_abs_res1.val[0] = vqshlq_n_s16(prb_comp_abs.val[0], 7);
    prb_abs_res1.val[1] = vqshlq_n_s16(prb_comp_abs.val[1], 7);
    prb_abs_res1.val[2] = vqshlq_n_s16(prb_comp_abs.val[2], 7);

    // Case2: prbAbsRes2 = prbCompAbs * 2^(input_bits - output_bits + 1) - 2^13
    //  input_bits - output_bits + 1 = 8
    prb_abs_res2.val[0] =
        vsubq_s16(vqshlq_n_s16(prb_comp_abs.val[0], 8), sub_thr2);
    prb_abs_res2.val[1] =
        vsubq_s16(vqshlq_n_s16(prb_comp_abs.val[1], 8), sub_thr2);
    prb_abs_res2.val[2] =
        vsubq_s16(vqshlq_n_s16(prb_comp_abs.val[2], 8), sub_thr2);

    // Case3: prbAbsRes3 = prbCompAbs * 2^(absBitWidth - compBitWidth + 2) -
    // 2^15
    //  input_bits - output_bits + 2 = 9
    prb_abs_res3.val[0] = vreinterpretq_s16_u16(
        vsubq_u16(vqshluq_n_s16(prb_comp_abs.val[0], 9), sub_comm));
    prb_abs_res3.val[1] = vreinterpretq_s16_u16(
        vsubq_u16(vqshluq_n_s16(prb_comp_abs.val[1], 9), sub_comm));
    prb_abs_res3.val[2] = vreinterpretq_s16_u16(
        vsubq_u16(vqshluq_n_s16(prb_comp_abs.val[2], 9), sub_comm));

    // Expand - Fourth Step: OR among prbAbsRes vectors
    int16x8x3_t exp_samples;
    exp_samples.val[0] = vbslq_s16(
        check_thr1.val[0], prb_abs_res1.val[0],
        vbslq_s16(check_thr3.val[0], prb_abs_res3.val[0], prb_abs_res2.val[0]));
    exp_samples.val[1] = vbslq_s16(
        check_thr1.val[1], prb_abs_res1.val[1],
        vbslq_s16(check_thr3.val[1], prb_abs_res3.val[1], prb_abs_res2.val[1]));
    exp_samples.val[2] = vbslq_s16(
        check_thr1.val[2], prb_abs_res1.val[2],
        vbslq_s16(check_thr3.val[2], prb_abs_res3.val[2], prb_abs_res2.val[2]));

    // Apply sign and shift
    exp_samples.val[0] =
        vmulq_s16(exp_samples.val[0], vmovl_s8(prb_signs.val[0]));
    exp_samples.val[1] =
        vmulq_s16(exp_samples.val[1], vmovl_s8(prb_signs.val[1]));
    exp_samples.val[2] =
        vmulq_s16(exp_samples.val[2], vmovl_s8(prb_signs.val[2]));

    int16x8_t comp_shift_vec = vdupq_n_s16(-src[i].exp);

    exp_samples.val[0] = vshlq_s16(exp_samples.val[0], comp_shift_vec);
    exp_samples.val[1] = vshlq_s16(exp_samples.val[1], comp_shift_vec);
    exp_samples.val[2] = vshlq_s16(exp_samples.val[2], comp_shift_vec);

    if (scale != nullptr) {
      scale_and_store3_cmplx((int16_t *)dst, exp_samples.val[0],
                             exp_samples.val[1], exp_samples.val[2], scale_v);
      dst += 12;
    } else {
      vst1q_s16((int16_t *)dst, exp_samples.val[0]);
      dst += 4;
      vst1q_s16((int16_t *)dst, exp_samples.val[1]);
      dst += 4;
      vst1q_s16((int16_t *)dst, exp_samples.val[2]);
      dst += 4;
    }
  }
  return ARMRAL_SUCCESS;
#endif
}

armral_status armral_mu_law_decompr_9bit(const uint32_t n_prb,
                                         const armral_compressed_data_9bit *src,
                                         armral_cmplx_int16_t *dst,
                                         const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  // Set true predicate corresponding to 8x 16-bit elements
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  // Shifts for unpacking
  const svuint16_t left_shifts1 = svindex_u16(8, 1);
  const svuint16_t left_shifts2 =
      svlsl_u16_x(pg, svdup_n_u16(1), svindex_u16(0, 1));

  for (uint32_t i = 0; i < n_prb; i++) {
    const uint8_t *data_in = (const uint8_t *)src[i].mantissa;

    // Unpack the data
    // In the following, a to h represent 9-bit values. Capitals indicate the
    // MSB of the data

    // Get 8 MSBs of a, 7 MSBs of b, 6 MSBs of c, etc.
    // [00000000gggggggH|00000000ffffffGg|00000000eeeeeFff|00000000ddddEeee|
    //  00000000cccDdddd|00000000bbCccccc|00000000aBbbbbbb|00000000Aaaaaaaa]
    svuint16_t a07 = svld1ub_u16(pg, &data_in[0]);
    svuint16_t b07 = svld1ub_u16(pg, &data_in[9]);
    svuint16_t c07 = svld1ub_u16(pg, &data_in[18]);

    // Get 1 LSB of a, 2 LSBs of b, 3 LSBs of c, etc.
    // [00000000hhhhhhhh|00000000gggggggH|00000000ffffffGg|00000000eeeeeFff|
    //  00000000ddddEeee|00000000cccDdddd|00000000bbCccccc|00000000aBbbbbbb]
    svuint16_t a18 = svld1ub_u16(pg, &data_in[1]);
    svuint16_t b18 = svld1ub_u16(pg, &data_in[10]);
    svuint16_t c18 = svld1ub_u16(pg, &data_in[19]);

    // Put MSBs to the top of the 16-bit elements
    // left_shifts1 = {8, 9, 10, 11, ....}
    // [H000000000000000|Gg00000000000000|Fff0000000000000|Eeee000000000000|
    //  Ddddd00000000000|Cccccc0000000000|Bbbbbbb000000000|Aaaaaaaa00000000]
    svuint16_t a_left = svlsl_u16_x(pg, a07, left_shifts1);
    svuint16_t b_left = svlsl_u16_x(pg, b07, left_shifts1);
    svuint16_t c_left = svlsl_u16_x(pg, c07, left_shifts1);

    // Combine MSBs and LSBs. Here, LSL followed by ORR is equivalent to MLA.
    // left_shifts2 = {2^0, 2^1, 2^2, 2^3, ...}, so a18 * left_shifts2 gives
    // [0hhhhhhhh0000000|00gggggggH000000|000ffffffGg00000|0000eeeeeFff0000|
    //  00000ddddEeee000|000000cccDdddd00|0000000bbCccccc0|00000000aBbbbbbb]
    // which is added to a_left
    // [Hhhhhhhhh0000000|GggggggggH000000|FffffffffGg00000|EeeeeeeeeFff0000|
    //  DddddddddEeee000|CccccccccDdddd00|BbbbbbbbbCccccc0|AaaaaaaaaBbbbbbb]
    svint16_t prb1_comp =
        svreinterpret_s16_u16(svmla_u16_x(pg, a_left, a18, left_shifts2));
    svint16_t prb2_comp =
        svreinterpret_s16_u16(svmla_u16_x(pg, b_left, b18, left_shifts2));
    svint16_t prb3_comp =
        svreinterpret_s16_u16(svmla_u16_x(pg, c_left, c18, left_shifts2));

    // Set sign predicate
    svbool_t pg_prb1_comp_signs = svcmplt_n_s16(pg, prb1_comp, 0);
    svbool_t pg_prb2_comp_signs = svcmplt_n_s16(pg, prb2_comp, 0);
    svbool_t pg_prb3_comp_signs = svcmplt_n_s16(pg, prb3_comp, 0);

    // Shift values to the bottom 9 bits of the 16-bit elements
    // [-------Hhhhhhhhh|-------Ggggggggg|-------Fffffffff|-------Eeeeeeeee|
    //  -------Ddddddddd|-------Ccccccccc|-------Bbbbbbbbb|-------Aaaaaaaaa]
    // where '-' indicates leading sign bits
    prb1_comp = svasr_n_s16_x(pg, prb1_comp, 7);
    prb2_comp = svasr_n_s16_x(pg, prb2_comp, 7);
    prb3_comp = svasr_n_s16_x(pg, prb3_comp, 7);

    // Extract absolute values for the PRB
    svint16_t prb1_comp_abs = svqabs_s16_x(pg, prb1_comp);
    svint16_t prb2_comp_abs = svqabs_s16_x(pg, prb2_comp);
    svint16_t prb3_comp_abs = svqabs_s16_x(pg, prb3_comp);
    prb1_comp_abs = svmin_n_s16_x(pg, prb1_comp_abs, 255);
    prb2_comp_abs = svmin_n_s16_x(pg, prb2_comp_abs, 255);
    prb3_comp_abs = svmin_n_s16_x(pg, prb3_comp_abs, 255);

    // Expand each sample, input_bits=15, output_bits=9

    // Case1: check if prbCompAbs <= 2^(output_bits - 2)
    svbool_t pg1_case1 = svcmple_n_s16(pg, prb1_comp_abs, thr1_b9);
    svbool_t pg2_case1 = svcmple_n_s16(pg, prb2_comp_abs, thr1_b9);
    svbool_t pg3_case1 = svcmple_n_s16(pg, prb3_comp_abs, thr1_b9);

    // Case3: check if prbCompAbs > (2^(output_bits - 2) + 2^(output_bits - 3))
    svbool_t pg1_case3 = svcmpgt_n_s16(pg, prb1_comp_abs, thr2_b9);
    svbool_t pg2_case3 = svcmpgt_n_s16(pg, prb2_comp_abs, thr2_b9);
    svbool_t pg3_case3 = svcmpgt_n_s16(pg, prb3_comp_abs, thr2_b9);

    // Expand - Second Step: Perform decompression calculation

    // Case2: prbAbsRes2 = prbCompAbs * 2^(input_bits - output_bits + 1) - 2^13
    // input_bits - output_bits + 1 = 7
    svint16_t result1 =
        svsub_n_s16_x(pg, svrshl_n_s16_x(pg, prb1_comp_abs, 7), sub_thr2);
    svint16_t result2 =
        svsub_n_s16_x(pg, svrshl_n_s16_x(pg, prb2_comp_abs, 7), sub_thr2);
    svint16_t result3 =
        svsub_n_s16_x(pg, svrshl_n_s16_x(pg, prb3_comp_abs, 7), sub_thr2);

    // Case1: prbAbsRes1 = prbCompAbs * 2^(input_bits - output_bits)
    // input_bits - output_bits = 6
    result1 =
        svsel_s16(pg1_case1, svrshl_n_s16_x(pg, prb1_comp_abs, 6), result1);
    result2 =
        svsel_s16(pg2_case1, svrshl_n_s16_x(pg, prb2_comp_abs, 6), result2);
    result3 =
        svsel_s16(pg3_case1, svrshl_n_s16_x(pg, prb3_comp_abs, 6), result3);

    // Case3: prbAbsRes3 = prbCompAbs * 2^(input_bits - output_bits + 2) - 2^15
    // input_bits - output_bits + 2 = 8
    result1 = svsel_s16(
        pg1_case3,
        svsub_n_s16_x(pg, svrshl_n_s16_x(pg, prb1_comp_abs, 8), sub_comm),
        result1);
    result2 = svsel_s16(
        pg2_case3,
        svsub_n_s16_x(pg, svrshl_n_s16_x(pg, prb2_comp_abs, 8), sub_comm),
        result2);
    result3 = svsel_s16(
        pg3_case3,
        svsub_n_s16_x(pg, svrshl_n_s16_x(pg, prb3_comp_abs, 8), sub_comm),
        result3);

    // Apply sign
    result1 = svneg_s16_m(result1, pg_prb1_comp_signs, result1);
    result2 = svneg_s16_m(result2, pg_prb2_comp_signs, result2);
    result3 = svneg_s16_m(result3, pg_prb3_comp_signs, result3);

    // Shift
    svint16_t comp_shift_vec = svdup_n_s16_x(pg, -src[i].exp);
    result1 = svqshl_s16_x(pg, result1, comp_shift_vec);
    result2 = svqshl_s16_x(pg, result2, comp_shift_vec);
    result3 = svqshl_s16_x(pg, result3, comp_shift_vec);

    if (scale != nullptr) {
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(result1, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(result2, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(result3, scale->re, scale_im));
      dst += 4;
    } else {
      svst1_s16(pg, (int16_t *)dst, result1);
      dst += 4;
      svst1_s16(pg, (int16_t *)dst, result2);
      dst += 4;
      svst1_s16(pg, (int16_t *)dst, result3);
      dst += 4;
    }
  }
  return ARMRAL_SUCCESS;
#else
  common_decompr_9bit_neon<true, false>(n_prb, src, dst, scale);
  return ARMRAL_SUCCESS;
#endif
}

armral_status armral_mu_law_decompr_14bit(
    const uint32_t n_prb, const armral_compressed_data_14bit *src,
    armral_cmplx_int16_t *dst, const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  svint16_t scale_im;
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  // Set true predicate corresponding to 8x 14-bit elements
  const svbool_t pg14 = svwhilelt_b8(0, 14);

  // Set true predicate corresponding to 8x 16-bit elements
  const svbool_t pg16 = svptrue_pat_b16(SV_VL8);

  // Indices used during unpacking
  const svuint8_t contig_idx =
      svdupq_n_u8(1, 0, 3, 2, 5, 4, 255, 6, 8, 7, 10, 9, 12, 11, 255, 13);
  const svuint8_t contig_idx2 = svdupq_n_u8(255, 255, 255, 1, 255, 3, 255, 5,
                                            255, 255, 255, 8, 255, 10, 255, 12);

  // Shifts used during unpacking
  const svuint16_t contig_rshift = svdupq_n_u16(0, 2, 4, 6, 0, 2, 4, 6);
  // The left shifts are performed as multiplies, so contig_lshift = {2^0, 2^6,
  // 2^4, 2^2, ...}
  const svuint16_t contig_lshift = svdupq_n_u16(1, 64, 16, 4, 1, 64, 16, 4);

  for (uint32_t i = 0; i < n_prb; i++) {
    const uint8_t *data_in = (const uint8_t *)src[i].mantissa;

    // Unpack the data
    // In the following, a to d represent 7 14-bit values (from e onwards the
    // pattern repeats). Capitals indicate the MSB of the data

    // Load in 14*24/8 = 42 bytes
    // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
    svuint8_t uprb1_in = svld1_u8(pg14, &data_in[0]);
    svuint8_t uprb2_in = svld1_u8(pg14, &data_in[14]);
    svuint8_t uprb3_in = svld1_u8(pg14, &data_in[28]);

    // Get 14 bits of A, 12 LSBs of B, 10 LSBs of C and 8 LSBs of D at the top
    // of each 16-bit element
    // [dddddddd00000000|ccccccccccDddddd|bbbbbbbbbbbbCccc|AaaaaaaaaaaaaaBb]
    svuint16_t contig1_r = svreinterpret_u16_u8(svtbl_u8(uprb1_in, contig_idx));
    svuint16_t contig2_r = svreinterpret_u16_u8(svtbl_u8(uprb2_in, contig_idx));
    svuint16_t contig3_r = svreinterpret_u16_u8(svtbl_u8(uprb3_in, contig_idx));

    // contig_rshift = {0, 2, 4, 6, ...}
    // [000000dddddd00|0000ccccccccccDd|00bbbbbbbbbbbbCc|AaaaaaaaaaaaaaBb]
    contig1_r = svlsr_u16_x(pg16, contig1_r, contig_rshift);
    contig2_r = svlsr_u16_x(pg16, contig2_r, contig_rshift);
    contig3_r = svlsr_u16_x(pg16, contig3_r, contig_rshift);

    // Get 0 MSBs of A, 2 MSBs of B, 4 MSBs of C and 6 MSBs of D
    // [ccDddddd00000000|bbbbCccc00000000|aaaaaaBb00000000|0000000000000000]
    svuint16_t contig1_l =
        svreinterpret_u16_u8(svtbl_u8(uprb1_in, contig_idx2));
    svuint16_t contig2_l =
        svreinterpret_u16_u8(svtbl_u8(uprb2_in, contig_idx2));
    svuint16_t contig3_l =
        svreinterpret_u16_u8(svtbl_u8(uprb3_in, contig_idx2));

    // Combine LSBs and MSBs. Here LSL followed by ORR is equivalent to MLA.
    // contig_lshift = {2^0, 2^6, 2^4, 2^2, ...}
    // [Dddddddddddddd00|CcccccccccccccDd|BbbbbbbbbbbbbbCc|AaaaaaaaaaaaaaBb]
    svint16_t prb1_comp = svreinterpret_s16_u16(
        svmla_u16_x(pg16, contig1_r, contig1_l, contig_lshift));
    svint16_t prb2_comp = svreinterpret_s16_u16(
        svmla_u16_x(pg16, contig2_r, contig2_l, contig_lshift));
    svint16_t prb3_comp = svreinterpret_s16_u16(
        svmla_u16_x(pg16, contig3_r, contig3_l, contig_lshift));

    // Set sign predicate
    svbool_t pg_prb1_comp_signs = svcmplt_n_s16(pg16, prb1_comp, 0);
    svbool_t pg_prb2_comp_signs = svcmplt_n_s16(pg16, prb2_comp, 0);
    svbool_t pg_prb3_comp_signs = svcmplt_n_s16(pg16, prb3_comp, 0);

    // Shift values to the bottom 14 bits of the 16-bit elements
    // [--Dddddddddddddd|--Cccccccccccccc|--Bbbbbbbbbbbbbb|--Aaaaaaaaaaaaaa]
    prb1_comp = svasr_n_s16_x(pg16, prb1_comp, 2);
    prb2_comp = svasr_n_s16_x(pg16, prb2_comp, 2);
    prb3_comp = svasr_n_s16_x(pg16, prb3_comp, 2);

    // Extract absolute values for the PRB
    svint16_t prb1_comp_abs = svqabs_s16_x(pg16, prb1_comp);
    svint16_t prb2_comp_abs = svqabs_s16_x(pg16, prb2_comp);
    svint16_t prb3_comp_abs = svqabs_s16_x(pg16, prb3_comp);
    prb1_comp_abs = svmin_n_s16_x(pg16, prb1_comp_abs, 8191);
    prb2_comp_abs = svmin_n_s16_x(pg16, prb2_comp_abs, 8191);
    prb3_comp_abs = svmin_n_s16_x(pg16, prb3_comp_abs, 8191);

    // Expand each sample, input_bits=15, output_bits=14

    // Case1: check if prbCompAbs <= 2^(output_bits - 2)
    svbool_t pg1_case1 = svcmple_n_s16(pg16, prb1_comp_abs, thr1_b14);
    svbool_t pg2_case1 = svcmple_n_s16(pg16, prb2_comp_abs, thr1_b14);
    svbool_t pg3_case1 = svcmple_n_s16(pg16, prb3_comp_abs, thr1_b14);

    // Case3: check if prbCompAbs > (2^(output_bits - 2) + 2^(output_bits - 3))
    svbool_t pg1_case3 = svcmpgt_n_s16(pg16, prb1_comp_abs, thr2_b14);
    svbool_t pg2_case3 = svcmpgt_n_s16(pg16, prb2_comp_abs, thr2_b14);
    svbool_t pg3_case3 = svcmpgt_n_s16(pg16, prb3_comp_abs, thr2_b14);

    // Expand - Second Step: Perform decompression calculation

    // Case2: prbAbsRes2 = prbCompAbs * 2^(input_bits - output_bits + 1) - 2^13
    // input_bits - output_bits + 1 = 2
    svint16_t result1 =
        svsub_n_s16_x(pg16, svrshl_n_s16_x(pg16, prb1_comp_abs, 2), sub_thr2);
    svint16_t result2 =
        svsub_n_s16_x(pg16, svrshl_n_s16_x(pg16, prb2_comp_abs, 2), sub_thr2);
    svint16_t result3 =
        svsub_n_s16_x(pg16, svrshl_n_s16_x(pg16, prb3_comp_abs, 2), sub_thr2);

    // Case1: prbAbsRes1 = prbCompAbs * 2^(input_bits - output_bits)
    // input_bits - output_bits = 1
    result1 =
        svsel_s16(pg1_case1, svrshl_n_s16_x(pg16, prb1_comp_abs, 1), result1);
    result2 =
        svsel_s16(pg2_case1, svrshl_n_s16_x(pg16, prb2_comp_abs, 1), result2);
    result3 =
        svsel_s16(pg3_case1, svrshl_n_s16_x(pg16, prb3_comp_abs, 1), result3);

    // Case3: prbAbsRes3 = prbCompAbs * 2^(input_bits - output_bits + 2) - 2^15
    // input_bits - output_bits + 2 = 3
    result1 = svsel_s16(
        pg1_case3,
        svsub_n_s16_x(pg16, svrshl_n_s16_x(pg16, prb1_comp_abs, 3), sub_comm),
        result1);
    result2 = svsel_s16(
        pg2_case3,
        svsub_n_s16_x(pg16, svrshl_n_s16_x(pg16, prb2_comp_abs, 3), sub_comm),
        result2);
    result3 = svsel_s16(
        pg3_case3,
        svsub_n_s16_x(pg16, svrshl_n_s16_x(pg16, prb3_comp_abs, 3), sub_comm),
        result3);

    // Apply sign
    result1 = svneg_s16_m(result1, pg_prb1_comp_signs, result1);
    result2 = svneg_s16_m(result2, pg_prb2_comp_signs, result2);
    result3 = svneg_s16_m(result3, pg_prb3_comp_signs, result3);

    // Shift
    svint16_t comp_shift_vec = svdup_n_s16_x(pg16, -src[i].exp);
    result1 = svqshl_s16_x(pg16, result1, comp_shift_vec);
    result2 = svqshl_s16_x(pg16, result2, comp_shift_vec);
    result3 = svqshl_s16_x(pg16, result3, comp_shift_vec);

    if (scale != nullptr) {
      svst1_s16(pg16, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(result1, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg16, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(result2, scale->re, scale_im));
      dst += 4;
      svst1_s16(pg16, (int16_t *)dst,
                sv_cmplx_mul_combined_re_im(result3, scale->re, scale_im));
      dst += 4;
    } else {
      svst1_s16(pg16, (int16_t *)dst, result1);
      dst += 4;
      svst1_s16(pg16, (int16_t *)dst, result2);
      dst += 4;
      svst1_s16(pg16, (int16_t *)dst, result3);
      dst += 4;
    }
  }
  return ARMRAL_SUCCESS;
#else
  int16x8x2_t scale_v;
  if (scale != nullptr) {
    scale_v.val[0] = vdupq_n_s16(scale->re);
    scale_v.val[1] = vdupq_n_s16(scale->im);
  }
  for (uint32_t i = 0; i < n_prb; i++) {

    const uint8_t *data_in = (const uint8_t *)&src->mantissa[0];
    int16_t shift = src->exp;

    // need to load 14*24/8 = 42 bytes total.
    uint8x16_t uprb_in[3];
    uprb_in[0] = vld1q_u8(data_in); // first 14 bytes are interesting
    data_in += 14;
    uprb_in[1] = vld1q_u8(data_in); // first 14 bytes are interesting
    data_in += 12;
    uprb_in[2] = vld1q_u8(data_in); // last 14 bytes are interesting

    // permute
    // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
    // (capital indicates msb of original data)
    // into
    // [ccDddddddddddddd|bbbbCccccccccccc|bbbbbbbbbbbbCccc|Aaaaaaaaaaaaaabb]
    // (Aa... and Dd... now contiguous)
    // note this pattern repeats for bytes 7-13 as well
    uint8x16_t contig01_idx =
        uint8x16_t{1, 0, 3, 2, 4, 3, 6, 5, 8, 7, 10, 9, 11, 10, 13, 12};
    uint8x16_t contig2_idx =
        uint8x16_t{3, 2, 5, 4, 6, 5, 8, 7, 10, 9, 12, 11, 13, 12, 15, 14};
    uint16x8_t contig[3];
    contig[0] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[0], contig01_idx));
    contig[1] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[1], contig01_idx));
    contig[2] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[2], contig2_idx));

    // shift MSBs to most significant bit positions
    int16x8_t contig_shift = int16x8_t{0, -2, 4, 2, 0, -2, 4, 2};
    contig[0] = vshlq_u16(contig[0], contig_shift);
    contig[1] = vshlq_u16(contig[1], contig_shift);
    contig[2] = vshlq_u16(contig[2], contig_shift);

    // get the missing bits of B, C, F and G
    // permute
    // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
    // into
    // [00000000|00000000|00000000|0000ccDd|Bb000000|00000000|00000000|00000000]
    uint8x16_t fill01_idx = uint8x16_t{255, 255, 255, 1, 5,  255, 255, 255,
                                       255, 255, 255, 8, 12, 255, 255, 255};
    uint8x16_t fill2_idx = uint8x16_t{255, 255, 255, 3,  7,  255, 255, 255,
                                      255, 255, 255, 10, 14, 255, 255, 255};

    uint16x8_t fill[3];
    fill[0] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[0], fill01_idx));
    fill[1] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[1], fill01_idx));
    fill[2] = vreinterpretq_u16_u8(vqtbl1q_u8(uprb_in[2], fill2_idx));

    int16x8_t fill_shift = int16x8_t{0, 6, -4, 0, 0, 6, -4, 0};
    fill[0] = vshlq_u16(fill[0], fill_shift);
    fill[1] = vshlq_u16(fill[1], fill_shift);
    fill[2] = vshlq_u16(fill[2], fill_shift);

    // combine everything together
    uint16x8_t sbli = {0xfffc, 0x3ffc, 0xfff3, 0xfffc,
                       0xfffc, 0x3ffc, 0xfff3, 0xfffc};
    int16x8_t pack[3];
    pack[0] = vreinterpretq_s16_u16(vbslq_u16(sbli, contig[0], fill[0]));
    pack[1] = vreinterpretq_s16_u16(vbslq_u16(sbli, contig[1], fill[1]));
    pack[2] = vreinterpretq_s16_u16(vbslq_u16(sbli, contig[2], fill[2]));

    // Decompression
    int16x8_t prb_comp_in[3];
    int16x8_t correction_shift = vdupq_n_s16(-2);
    prb_comp_in[0] = vshlq_s16(pack[0], correction_shift);
    prb_comp_in[1] = vshlq_s16(pack[1], correction_shift);
    prb_comp_in[2] = vshlq_s16(pack[2], correction_shift);

    // Extract the sign bit and absolute values for the PRB
    int16x8_t prb_signs[3];
    prb_signs[0] = vreinterpretq_s16_u16(
        vsubq_u16(vcltzq_s16(prb_comp_in[0]), vcgezq_s16(prb_comp_in[0])));
    prb_signs[1] = vreinterpretq_s16_u16(
        vsubq_u16(vcltzq_s16(prb_comp_in[1]), vcgezq_s16(prb_comp_in[1])));
    prb_signs[2] = vreinterpretq_s16_u16(
        vsubq_u16(vcltzq_s16(prb_comp_in[2]), vcgezq_s16(prb_comp_in[2])));

    int16x8_t prb_comp_abs[3];
    // saturation to 2^13 -1
    int16x8_t sat_pos = vdupq_n_s16(8191);
    prb_comp_abs[0] = vminq_s16(sat_pos, vqabsq_s16(prb_comp_in[0]));
    prb_comp_abs[1] = vminq_s16(sat_pos, vqabsq_s16(prb_comp_in[1]));
    prb_comp_abs[2] = vminq_s16(sat_pos, vqabsq_s16(prb_comp_in[2]));

    // Expand each sample, absBitWidth=15, compBitWidth=9
    uint16x8_t check_thr1[3];
    uint16x8_t check_thr3[3];

    // Expand - First Step: Set bitmasks based on prbCompAbs values
    // Check1: if prbCompAbs <= 2^(compBitWidth - 2) = 4096
    int16x8_t thr1_b14 = vdupq_n_s16(4096);
    check_thr1[0] = vcleq_s16(prb_comp_abs[0], thr1_b14);
    check_thr1[1] = vcleq_s16(prb_comp_abs[1], thr1_b14);
    check_thr1[2] = vcleq_s16(prb_comp_abs[2], thr1_b14);

    // Check3: if prbCompAbs > (2^(compBitWidth - 2) + 2^(compBitWidth - 3))
    int16x8_t thr2_b14 = vdupq_n_s16(6144);
    check_thr3[0] = vcgtq_s16(prb_comp_abs[0], thr2_b14);
    check_thr3[1] = vcgtq_s16(prb_comp_abs[1], thr2_b14);
    check_thr3[2] = vcgtq_s16(prb_comp_abs[2], thr2_b14);

    // Expand - Second Step: Perform decompression calculation
    int16x8_t prb_abs_res1[3];
    int16x8_t prb_abs_res2[3];
    int16x8_t prb_abs_res3[3];

    // Case1: prbAbsRes1 = prbCompAbs * 2^(input_bits - output_bits)
    //  input_bits - output_bits = 1
    prb_abs_res1[0] = vqshlq_n_s16(prb_comp_abs[0], 1);
    prb_abs_res1[1] = vqshlq_n_s16(prb_comp_abs[1], 1);
    prb_abs_res1[2] = vqshlq_n_s16(prb_comp_abs[2], 1);

    // Case2: prbAbsRes2 = prbCompAbs * 2^(input_bits - output_bits + 1) - 2^13
    //  input_bits - output_bits + 1 = 2
    int16x8_t sub_thr2_b14 = vdupq_n_s16(8192);
    prb_abs_res2[0] = vsubq_s16(vqshlq_n_s16(prb_comp_abs[0], 2), sub_thr2_b14);
    prb_abs_res2[1] = vsubq_s16(vqshlq_n_s16(prb_comp_abs[1], 2), sub_thr2_b14);
    prb_abs_res2[2] = vsubq_s16(vqshlq_n_s16(prb_comp_abs[2], 2), sub_thr2_b14);

    // Case3: prbAbsRes3 = prbCompAbs * 2^(absBitWidth - compBitWidth + 2) -
    // 2^15
    //  input_bits - output_bits + 2 = 3
    uint16x8_t sub_comm_b14 = vdupq_n_u16(32768);
    prb_abs_res3[0] = vreinterpretq_s16_u16(
        vsubq_u16(vqshluq_n_s16(prb_comp_abs[0], 3), sub_comm_b14));
    prb_abs_res3[1] = vreinterpretq_s16_u16(
        vsubq_u16(vqshluq_n_s16(prb_comp_abs[1], 3), sub_comm_b14));
    prb_abs_res3[2] = vreinterpretq_s16_u16(
        vsubq_u16(vqshluq_n_s16(prb_comp_abs[2], 3), sub_comm_b14));

    // Expand - Fourth Step: OR among prbAbsRes vectors
    int16x8_t decompressed[3];
    decompressed[0] =
        vbslq_s16(check_thr1[0], prb_abs_res1[0],
                  vbslq_s16(check_thr3[0], prb_abs_res3[0], prb_abs_res2[0]));
    decompressed[1] =
        vbslq_s16(check_thr1[1], prb_abs_res1[1],
                  vbslq_s16(check_thr3[1], prb_abs_res3[1], prb_abs_res2[1]));
    decompressed[2] =
        vbslq_s16(check_thr1[2], prb_abs_res1[2],
                  vbslq_s16(check_thr3[2], prb_abs_res3[2], prb_abs_res2[2]));

    // Apply sign and shift
    decompressed[0] = vmulq_s16(decompressed[0], prb_signs[0]);
    decompressed[1] = vmulq_s16(decompressed[1], prb_signs[1]);
    decompressed[2] = vmulq_s16(decompressed[2], prb_signs[2]);

    int16x8_t comp_shift_vec = vdupq_n_s16(-shift);

    decompressed[0] = vshlq_s16(decompressed[0], comp_shift_vec);
    decompressed[1] = vshlq_s16(decompressed[1], comp_shift_vec);
    decompressed[2] = vshlq_s16(decompressed[2], comp_shift_vec);

    if (scale != nullptr) {
      scale_and_store3_cmplx((int16_t *)dst, decompressed[0], decompressed[1],
                             decompressed[2], scale_v);
      dst += 12;
    } else {
      vst1q_s16((int16_t *)dst, decompressed[0]);
      dst += 4;
      vst1q_s16((int16_t *)dst, decompressed[1]);
      dst += 4;
      vst1q_s16((int16_t *)dst, decompressed[2]);
      dst += 4;
    }
    src++;
  }
  return ARMRAL_SUCCESS;
#endif
}
