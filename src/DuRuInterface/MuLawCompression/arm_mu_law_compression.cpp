/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

#include "../bit_packing_common.hpp"
#include "utils/vec_mul.hpp"

// Input size is 15 bits wide. Output size is 8 bits wide. We use the names
// input_bits and output_bits in the comments below to refer to these values

#if ARMRAL_ARCH_SVE >= 2
static const int16_t thr1 = 8192;  // 2^(input_bits - 2) = 2^13
static const int16_t thr2 = 16384; // 2^(input_bits - 1) = 2^14
#else
static const int16x8_t thr1 __attribute__((aligned(16))) = {
    8192, 8192, 8192, 8192,
    8192, 8192, 8192, 8192}; // 2^(input_bits - 2) = 2^13

static const int16x8_t thr2 __attribute__((aligned(16))) = {
    16384, 16384, 16384, 16384,
    16384, 16384, 16384, 16384}; // 2^(input_bits - 1) = 2^14
#endif

static const int16_t add_thr2_b8 = 32; // 2^(output_bits - 3)
static const int16_t add_comm_b8 = 64; // 2^(output_Bits - 2)

static const int16_t add_thr2_b9 = 64;  // 2^(output_bits - 3)
static const int16_t add_comm_b9 = 128; // 2^(output_Bits - 2)

static const int16_t add_thr2_b14 = 2048; // 2^(output_bits - 3)
static const int16_t add_comm_b14 = 4096; // 2^(output_Bits - 2)

#if !(ARMRAL_ARCH_SVE >= 2)
static inline int8x8x3_t extract_sign_8b_s16x3q(const int16x8x3_t *prb_in) {
  int8x8x3_t prb_sign;
  for (uint32_t j = 0; j < 3; j++) {
    prb_sign.val[j] = vqmovn_s16(vreinterpretq_s16_u16(
        vsubq_u16(vcltzq_s16(prb_in->val[j]), vcgezq_s16(prb_in->val[j]))));
  }
  return prb_sign;
}

static inline int16x8x3_t extract_sign_s16x3q(const int16x8x3_t *prb_in) {
  int16x8x3_t prb_sign;
  for (uint32_t j = 0; j < 3; j++) {
    prb_sign.val[j] = vreinterpretq_s16_u16(
        vsubq_u16(vcltzq_s16(prb_in->val[j]), vcgezq_s16(prb_in->val[j])));
  }
  return prb_sign;
}

static inline int16x8x3_t abs_s16x3q(const int16x8x3_t *prb_in) {
  int16x8x3_t prb_abs;
  for (uint32_t j = 0; j < 3; j++) {
    prb_abs.val[j] = vqabsq_s16(prb_in->val[j]);
  }
  return prb_abs;
}

static inline int max_s16x3q(const int16x8x3_t *prb_abs) {
  int16x8_t v_max =
      vmaxq_s16(prb_abs->val[0], vmaxq_s16(prb_abs->val[1], prb_abs->val[2]));
  int max_val = vmaxvq_s16(v_max);
  return max_val;
}

/// Apply shift left (make greater) with saturation
static inline void shift_abs_s16x3q(int16x8x3_t *__restrict prb_abs,
                                    const int16x8_t *comp_shift_vec) {
  for (uint32_t j = 0; j < 3; j++) {
    prb_abs->val[j] = vqrshlq_s16(prb_abs->val[j], *comp_shift_vec);
  }
}

/// Check1: if prbAbs <= 2^(absBitWidth - 2) = 8192
static inline uint16x8x3_t compress_s1_c1(const int16x8x3_t *prb_abs,
                                          int16x8_t thr1_variant) {
  uint16x8x3_t check_thr1;
  for (uint32_t j = 0; j < 3; j++) {
    check_thr1.val[j] = vcleq_s16(prb_abs->val[j], thr1_variant);
  }
  return check_thr1;
}

/// Check3: Other cases (prbAbs > 2^(absBitWidth - 1) = 16384)
static inline uint16x8x3_t compress_s1_c3(const int16x8x3_t *prb_abs,
                                          int16x8_t thr2_variant) {
  uint16x8x3_t check_thr3;
  for (uint32_t j = 0; j < 3; j++) {
    check_thr3.val[j] = vcgtq_s16(prb_abs->val[j], thr2_variant);
  }
  return check_thr3;
}

static inline int16x8x3_t dup_s16x3q(int16_t val) {
  int16x8x3_t v;
  for (uint32_t j = 0; j < 3; j++) {
    v.val[j] = vdupq_n_s16(val);
  }
  return v;
}

static inline int16x8x3_t bsl_s16x3q(const uint16x8x3_t *check,
                                     const int16x8x3_t *in1,
                                     const int16x8x3_t *in2) {
  int16x8x3_t out;
  for (uint32_t j = 0; j < 3; j++) {
    out.val[j] = vbslq_s16(check->val[j], in1->val[j], in2->val[j]);
  }
  return out;
}

static inline int8x8x3_t narrow_s16x3q(const int16x8x3_t *prb_abs_res1) {
  int8x8x3_t comp_samples;
  for (uint32_t j = 0; j < 3; j++) {
    comp_samples.val[j] = vqmovn_s16(prb_abs_res1->val[j]);
  }
  return comp_samples;
}
#endif

armral_status armral_mu_law_compr_8bit(uint32_t n_prb,
                                       const armral_cmplx_int16_t *src,
                                       armral_compressed_data_8bit *dst,
                                       const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const int16_t *data_in = (const int16_t *)src;
  // Set true predicate corresponding to 8x16-bit elements
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  svint16_t scale_im = svdup_n_s16(0);
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  for (unsigned int i = 0; i < n_prb >> 1; i++) {
    // Load 2 PRBs = 24 complex symbols
    svint16_t prb1_in = svld1_s16(pg, data_in);
    svint16_t prb2_in = svld1_s16(pg, &data_in[8]);
    svint16_t prb3_in = svld1_s16(pg, &data_in[16]);
    svint16_t prb4_in = svld1_s16(pg, &data_in[24]);
    svint16_t prb5_in = svld1_s16(pg, &data_in[32]);
    svint16_t prb6_in = svld1_s16(pg, &data_in[40]);
    data_in += 48;

    if (scale != nullptr) {
      prb1_in = sv_cmplx_mul_combined_re_im(prb1_in, scale->re, scale_im);
      prb2_in = sv_cmplx_mul_combined_re_im(prb2_in, scale->re, scale_im);
      prb3_in = sv_cmplx_mul_combined_re_im(prb3_in, scale->re, scale_im);
      prb4_in = sv_cmplx_mul_combined_re_im(prb4_in, scale->re, scale_im);
      prb5_in = sv_cmplx_mul_combined_re_im(prb5_in, scale->re, scale_im);
      prb6_in = sv_cmplx_mul_combined_re_im(prb6_in, scale->re, scale_im);
    }

    // Extract the sign bit: use of predicate to identify
    // entries less than zero, the predicate will be used
    // to multiply by sign after compression
    const int16_t zero = 0;
    svbool_t pg_prb1_signs = svcmplt_n_s16(pg, prb1_in, zero);
    svbool_t pg_prb2_signs = svcmplt_n_s16(pg, prb2_in, zero);
    svbool_t pg_prb3_signs = svcmplt_n_s16(pg, prb3_in, zero);
    svbool_t pg_prb4_signs = svcmplt_n_s16(pg, prb4_in, zero);
    svbool_t pg_prb5_signs = svcmplt_n_s16(pg, prb5_in, zero);
    svbool_t pg_prb6_signs = svcmplt_n_s16(pg, prb6_in, zero);

    // Extract absolute values for the PRB
    svint16_t prb1_abs = svqabs_s16_z(pg, prb1_in);
    svint16_t prb2_abs = svqabs_s16_z(pg, prb2_in);
    svint16_t prb3_abs = svqabs_s16_z(pg, prb3_in);
    svint16_t prb4_abs = svqabs_s16_z(pg, prb4_in);
    svint16_t prb5_abs = svqabs_s16_z(pg, prb5_in);
    svint16_t prb6_abs = svqabs_s16_z(pg, prb6_in);

    // Find the maximum in absRe and absIm
    svint16_t v_max1 =
        svmax_s16_x(pg, prb1_abs, svmax_s16_x(pg, prb2_abs, prb3_abs));
    svint16_t v_max2 =
        svmax_s16_x(pg, prb4_abs, svmax_s16_x(pg, prb5_abs, prb6_abs));
    int16_t max_val[] = {svmaxv_s16(pg, v_max1), svmaxv_s16(pg, v_max2)};

    // Determine compShift, the shift to be applied to the entire PRB
    dst[0].exp = __builtin_clz(max_val[0]) - 16 - 1;
    dst[0].exp = dst[0].exp < 8 ? dst[0].exp : 7;
    dst[1].exp = __builtin_clz(max_val[1]) - 16 - 1;
    dst[1].exp = dst[1].exp < 8 ? dst[1].exp : 7;

    // Apply shift left (make greater) with saturation
    int16_t comp_shift[] = {dst[0].exp, dst[1].exp};
    prb1_abs = svqshl_n_s16_z(pg, prb1_abs, comp_shift[0]);
    prb2_abs = svqshl_n_s16_z(pg, prb2_abs, comp_shift[0]);
    prb3_abs = svqshl_n_s16_z(pg, prb3_abs, comp_shift[0]);
    prb4_abs = svqshl_n_s16_z(pg, prb4_abs, comp_shift[1]);
    prb5_abs = svqshl_n_s16_z(pg, prb5_abs, comp_shift[1]);
    prb6_abs = svqshl_n_s16_z(pg, prb6_abs, comp_shift[1]);

    // Compress each sample, absBitWidth=15

    // Compress - First Step: create predicates based on prbAbs values
    // Case 1: mark entries where prbAbs <= 2^(absBitWidth - 2) = 8192
    svbool_t pg1_case1 = svcmple_n_s16(pg, prb1_abs, thr1);
    svbool_t pg2_case1 = svcmple_n_s16(pg, prb2_abs, thr1);
    svbool_t pg3_case1 = svcmple_n_s16(pg, prb3_abs, thr1);
    svbool_t pg4_case1 = svcmple_n_s16(pg, prb4_abs, thr1);
    svbool_t pg5_case1 = svcmple_n_s16(pg, prb5_abs, thr1);
    svbool_t pg6_case1 = svcmple_n_s16(pg, prb6_abs, thr1);

    // Case 3: mark entries where (prbAbs > 2^(absBitWidth - 1) = 16384)
    svbool_t pg1_case3 = svcmpgt_n_s16(pg, prb1_abs, thr2);
    svbool_t pg2_case3 = svcmpgt_n_s16(pg, prb2_abs, thr2);
    svbool_t pg3_case3 = svcmpgt_n_s16(pg, prb3_abs, thr2);
    svbool_t pg4_case3 = svcmpgt_n_s16(pg, prb4_abs, thr2);
    svbool_t pg5_case3 = svcmpgt_n_s16(pg, prb5_abs, thr2);
    svbool_t pg6_case3 = svcmpgt_n_s16(pg, prb6_abs, thr2);

    // Case 2: mark entries where (prbAbs > 2^(absBitWidth - 2)
    // && prbAbs <= 2^(absBitWidth - 1) : 8192 < prbAbs <= 16384
    svbool_t pg1_case2 = svnor_b_z(pg, pg1_case1, pg1_case3);
    svbool_t pg2_case2 = svnor_b_z(pg, pg2_case1, pg2_case3);
    svbool_t pg3_case2 = svnor_b_z(pg, pg3_case1, pg3_case3);
    svbool_t pg4_case2 = svnor_b_z(pg, pg4_case1, pg4_case3);
    svbool_t pg5_case2 = svnor_b_z(pg, pg5_case1, pg5_case3);
    svbool_t pg6_case2 = svnor_b_z(pg, pg6_case1, pg6_case3);

    // Compress - Second Step: Perform compression calculation

    // Case 1:  CompSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 7
    svint16_t comp_samples1 = svrshr_n_s16_m(pg1_case1, prb1_abs, 7);
    svint16_t comp_samples2 = svrshr_n_s16_m(pg2_case1, prb2_abs, 7);
    svint16_t comp_samples3 = svrshr_n_s16_m(pg3_case1, prb3_abs, 7);
    svint16_t comp_samples4 = svrshr_n_s16_m(pg4_case1, prb4_abs, 7);
    svint16_t comp_samples5 = svrshr_n_s16_m(pg5_case1, prb5_abs, 7);
    svint16_t comp_samples6 = svrshr_n_s16_m(pg6_case1, prb6_abs, 7);

    // Case 2:  CompSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    comp_samples1 = svadd_n_s16_m(
        pg1_case2, svrshr_n_s16_m(pg1_case2, comp_samples1, 8), add_thr2_b8);
    comp_samples2 = svadd_n_s16_m(
        pg2_case2, svrshr_n_s16_m(pg2_case2, comp_samples2, 8), add_thr2_b8);
    comp_samples3 = svadd_n_s16_m(
        pg3_case2, svrshr_n_s16_m(pg3_case2, comp_samples3, 8), add_thr2_b8);
    comp_samples4 = svadd_n_s16_m(
        pg4_case2, svrshr_n_s16_m(pg4_case2, comp_samples4, 8), add_thr2_b8);
    comp_samples5 = svadd_n_s16_m(
        pg5_case2, svrshr_n_s16_m(pg5_case2, comp_samples5, 8), add_thr2_b8);
    comp_samples6 = svadd_n_s16_m(
        pg6_case2, svrshr_n_s16_m(pg6_case2, comp_samples6, 8), add_thr2_b8);

    // Case 3:  CompSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2) */
    comp_samples1 = svadd_n_s16_m(
        pg1_case3, svrshr_n_s16_m(pg1_case3, comp_samples1, 9), add_comm_b8);
    comp_samples2 = svadd_n_s16_m(
        pg2_case3, svrshr_n_s16_m(pg2_case3, comp_samples2, 9), add_comm_b8);
    comp_samples3 = svadd_n_s16_m(
        pg3_case3, svrshr_n_s16_m(pg3_case3, comp_samples3, 9), add_comm_b8);
    comp_samples4 = svadd_n_s16_m(
        pg4_case3, svrshr_n_s16_m(pg4_case3, comp_samples4, 9), add_comm_b8);
    comp_samples5 = svadd_n_s16_m(
        pg5_case3, svrshr_n_s16_m(pg5_case3, comp_samples5, 9), add_comm_b8);
    comp_samples6 = svadd_n_s16_m(
        pg6_case3, svrshr_n_s16_m(pg6_case3, comp_samples6, 9), add_comm_b8);

    // Saturate  and narrow results
    svint8_t trunc1 = svqxtnb_s16(comp_samples1);
    svint8_t trunc2 = svqxtnb_s16(comp_samples2);
    svint8_t trunc3 = svqxtnb_s16(comp_samples3);
    svint8_t trunc4 = svqxtnb_s16(comp_samples4);
    svint8_t trunc5 = svqxtnb_s16(comp_samples5);
    svint8_t trunc6 = svqxtnb_s16(comp_samples6);

    // Multiply by sign using predicate on svint16
    trunc1 = svneg_s8_m(trunc1, pg_prb1_signs, trunc1);
    trunc2 = svneg_s8_m(trunc2, pg_prb2_signs, trunc2);
    trunc3 = svneg_s8_m(trunc3, pg_prb3_signs, trunc3);
    trunc4 = svneg_s8_m(trunc4, pg_prb4_signs, trunc4);
    trunc5 = svneg_s8_m(trunc5, pg_prb5_signs, trunc5);
    trunc6 = svneg_s8_m(trunc6, pg_prb6_signs, trunc6);

    // Store even-indexed elements
    int8_t *p_comp_out[] = {dst[0].mantissa, dst[1].mantissa};
    svst1b_s16(pg, p_comp_out[0], svreinterpret_s16_s8(trunc1));
    svst1b_s16(pg, p_comp_out[1], svreinterpret_s16_s8(trunc4));
    p_comp_out[0] += 8;
    p_comp_out[1] += 8;
    svst1b_s16(pg, p_comp_out[0], svreinterpret_s16_s8(trunc2));
    svst1b_s16(pg, p_comp_out[1], svreinterpret_s16_s8(trunc5));
    p_comp_out[0] += 8;
    p_comp_out[1] += 8;
    svst1b_s16(pg, p_comp_out[0], svreinterpret_s16_s8(trunc3));
    svst1b_s16(pg, p_comp_out[1], svreinterpret_s16_s8(trunc6));

    dst += 2;
  }
  if (n_prb % 2 != 0) {
    // Load 1 PRB = 12 complex symbols
    svint16_t prb1_in = svld1_s16(pg, data_in);
    svint16_t prb2_in = svld1_s16(pg, &data_in[8]);
    svint16_t prb3_in = svld1_s16(pg, &data_in[16]);
    data_in += 24;

    if (scale != nullptr) {
      prb1_in = sv_cmplx_mul_combined_re_im(prb1_in, scale->re, scale_im);
      prb2_in = sv_cmplx_mul_combined_re_im(prb2_in, scale->re, scale_im);
      prb3_in = sv_cmplx_mul_combined_re_im(prb3_in, scale->re, scale_im);
    }

    // Extract the sign bit: use of predicate to identify
    // entries less than zero, the predicate will be used
    // to multiply by sign after compression
    const int16_t zero = 0;
    svbool_t pg_prb1_signs = svcmplt_n_s16(pg, prb1_in, zero);
    svbool_t pg_prb2_signs = svcmplt_n_s16(pg, prb2_in, zero);
    svbool_t pg_prb3_signs = svcmplt_n_s16(pg, prb3_in, zero);

    // Extract absolute values for the PRB
    svint16_t prb1_abs = svqabs_s16_z(pg, prb1_in);
    svint16_t prb2_abs = svqabs_s16_z(pg, prb2_in);
    svint16_t prb3_abs = svqabs_s16_z(pg, prb3_in);

    // Find the maximum in absRe and absIm
    svint16_t v_max =
        svmax_s16_x(pg, prb1_abs, svmax_s16_x(pg, prb2_abs, prb3_abs));
    int16_t max_val = svmaxv_s16(pg, v_max);

    // Determine compShift, the shift to be applied to the entire PRB
    dst->exp = __builtin_clz(max_val) - 16 - 1;
    dst->exp = dst->exp < 8 ? dst->exp : 7;

    // Apply shift left (make greater) with saturation
    int16_t comp_shift = dst->exp;
    prb1_abs = svqshl_n_s16_z(pg, prb1_abs, comp_shift);
    prb2_abs = svqshl_n_s16_z(pg, prb2_abs, comp_shift);
    prb3_abs = svqshl_n_s16_z(pg, prb3_abs, comp_shift);

    // Compress each sample, absBitWidth=15

    // Compress - First Step: create predicates based on prbAbs values
    // Case 1: mark entries where prbAbs <= 2^(absBitWidth - 2) = 8192
    svbool_t pg1_case1 = svcmple_n_s16(pg, prb1_abs, thr1);
    svbool_t pg2_case1 = svcmple_n_s16(pg, prb2_abs, thr1);
    svbool_t pg3_case1 = svcmple_n_s16(pg, prb3_abs, thr1);

    // Case 3: mark entries where (prbAbs > 2^(absBitWidth - 1) = 16384)
    svbool_t pg1_case3 = svcmpgt_n_s16(pg, prb1_abs, thr2);
    svbool_t pg2_case3 = svcmpgt_n_s16(pg, prb2_abs, thr2);
    svbool_t pg3_case3 = svcmpgt_n_s16(pg, prb3_abs, thr2);

    // Case 2: mark entries where (prbAbs > 2^(absBitWidth - 2)
    // && prbAbs <= 2^(absBitWidth - 1) : 8192 < prbAbs <= 16384
    svbool_t pg1_case2 = svnor_b_z(pg, pg1_case1, pg1_case3);
    svbool_t pg2_case2 = svnor_b_z(pg, pg2_case1, pg2_case3);
    svbool_t pg3_case2 = svnor_b_z(pg, pg3_case1, pg3_case3);

    // Compress - Second Step: Perform compression calculation

    // Case 1:  CompSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 7
    svint16_t comp_samples1 = svrshr_n_s16_m(pg1_case1, prb1_abs, 7);
    svint16_t comp_samples2 = svrshr_n_s16_m(pg2_case1, prb2_abs, 7);
    svint16_t comp_samples3 = svrshr_n_s16_m(pg3_case1, prb3_abs, 7);

    // Case 2:  CompSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    comp_samples1 = svadd_n_s16_m(
        pg1_case2, svrshr_n_s16_m(pg1_case2, comp_samples1, 8), add_thr2_b8);
    comp_samples2 = svadd_n_s16_m(
        pg2_case2, svrshr_n_s16_m(pg2_case2, comp_samples2, 8), add_thr2_b8);
    comp_samples3 = svadd_n_s16_m(
        pg3_case2, svrshr_n_s16_m(pg3_case2, comp_samples3, 8), add_thr2_b8);

    // Case 3:  CompSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2) */
    comp_samples1 = svadd_n_s16_m(
        pg1_case3, svrshr_n_s16_m(pg1_case3, comp_samples1, 9), add_comm_b8);
    comp_samples2 = svadd_n_s16_m(
        pg2_case3, svrshr_n_s16_m(pg2_case3, comp_samples2, 9), add_comm_b8);
    comp_samples3 = svadd_n_s16_m(
        pg3_case3, svrshr_n_s16_m(pg3_case3, comp_samples3, 9), add_comm_b8);

    // Saturate  and narrow results
    svint8_t trunc1 = svqxtnb_s16(comp_samples1);
    svint8_t trunc2 = svqxtnb_s16(comp_samples2);
    svint8_t trunc3 = svqxtnb_s16(comp_samples3);

    // Multiply by sign using predicate on svint16
    trunc1 = svneg_s8_m(trunc1, pg_prb1_signs, trunc1);
    trunc2 = svneg_s8_m(trunc2, pg_prb2_signs, trunc2);
    trunc3 = svneg_s8_m(trunc3, pg_prb3_signs, trunc3);

    // Store even-indexed elements
    int8_t *p_comp_out = dst->mantissa;
    svst1b_s16(pg, p_comp_out, svreinterpret_s16_s8(trunc1));
    p_comp_out += 8;
    svst1b_s16(pg, p_comp_out, svreinterpret_s16_s8(trunc2));
    p_comp_out += 8;
    svst1b_s16(pg, p_comp_out, svreinterpret_s16_s8(trunc3));
  }
  return ARMRAL_SUCCESS;
#else
  for (int32_t i = 0; i < (int32_t)n_prb - 1; i += 2) {
    int16x8x3_t prb_in[2];

    if (scale != nullptr) {
      prb_in[0] = load3_cmplx_and_scale((const int16_t *)src, *scale);
      src += 12;
      prb_in[1] = load3_cmplx_and_scale((const int16_t *)src, *scale);
      src += 12;
    } else {
      prb_in[0].val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[0].val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[0].val[2] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[1].val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[1].val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[1].val[2] = vld1q_s16((const int16_t *)src);
      src += 4;
    }

    // Extract the sign bit and absolute values for the PRB
    int8x8x3_t prb_signs[] = {extract_sign_8b_s16x3q(&prb_in[0]),
                              extract_sign_8b_s16x3q(&prb_in[1])};

    int16x8x3_t prb_abs[] = {abs_s16x3q(&prb_in[0]), abs_s16x3q(&prb_in[1])};

    // Find the maximum in absRe and absIm
    int max_val[] = {max_s16x3q(&prb_abs[0]), max_s16x3q(&prb_abs[1])};

    // Determine compShift, the shift to be applied to the entire PRB
    int exp[] = {__builtin_clz(max_val[0]) - 16 - 1,
                 __builtin_clz(max_val[1]) - 16 - 1};
    exp[0] = exp[0] < 8 ? exp[0] : 7;
    exp[1] = exp[1] < 8 ? exp[1] : 7;
    dst[0].exp = exp[0];
    dst[1].exp = exp[1];

    int16x8_t comp_shift_vec[] = {vdupq_n_s16(exp[0]), vdupq_n_s16(exp[1])};

    shift_abs_s16x3q(&prb_abs[0], &comp_shift_vec[0]);
    shift_abs_s16x3q(&prb_abs[1], &comp_shift_vec[1]);

    // Compress each sample, absBitWidth=15
    // Compress - First Step: Set bitmasks based on prbAbs values
    uint16x8x3_t check_thr1[] = {compress_s1_c1(&prb_abs[0], thr1),
                                 compress_s1_c1(&prb_abs[1], thr1)};
    uint16x8x3_t check_thr3[] = {compress_s1_c3(&prb_abs[0], thr2),
                                 compress_s1_c3(&prb_abs[1], thr2)};

    // Compress - Second Step: Perform compression calculation
    int16x8x3_t prb_abs_res1[2];
    int16x8x3_t prb_abs_res2[] = {dup_s16x3q(add_thr2_b8),
                                  dup_s16x3q(add_thr2_b8)};
    int16x8x3_t prb_abs_res3[] = {dup_s16x3q(add_comm_b8),
                                  dup_s16x3q(add_comm_b8)};

    // Case1: compSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 7
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res1[0].val[j] = vrshrq_n_s16(prb_abs[0].val[j], 7);
      prb_abs_res1[1].val[j] = vrshrq_n_s16(prb_abs[1].val[j], 7);
    }

    // Case2: compSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    //  input_bits - output_bits + 1 = 8
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res2[0].val[j] =
          vrsraq_n_s16(prb_abs_res2[0].val[j], prb_abs[0].val[j], 8);
      prb_abs_res2[1].val[j] =
          vrsraq_n_s16(prb_abs_res2[1].val[j], prb_abs[1].val[j], 8);
    }

    // Case3: compSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2)
    //  input_bits - output_bits + 2 = 9
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res3[0].val[j] =
          vrsraq_n_s16(prb_abs_res3[0].val[j], prb_abs[0].val[j], 9);
      prb_abs_res3[1].val[j] =
          vrsraq_n_s16(prb_abs_res3[1].val[j], prb_abs[1].val[j], 9);
    }

    prb_abs_res1[0] =
        bsl_s16x3q(&check_thr1[0], &prb_abs_res1[0], &prb_abs_res2[0]);
    prb_abs_res1[1] =
        bsl_s16x3q(&check_thr1[1], &prb_abs_res1[1], &prb_abs_res2[1]);

    prb_abs_res1[0] =
        bsl_s16x3q(&check_thr3[0], &prb_abs_res3[0], &prb_abs_res1[0]);
    prb_abs_res1[1] =
        bsl_s16x3q(&check_thr3[1], &prb_abs_res3[1], &prb_abs_res1[1]);

    int8x8x3_t comp_samples[] = {narrow_s16x3q(&prb_abs_res1[0]),
                                 narrow_s16x3q(&prb_abs_res1[1])};

    // Re-apply sign
    int8x8x3_t *p_comp_out[] = {(int8x8x3_t *)dst[0].mantissa,
                                (int8x8x3_t *)dst[1].mantissa};
    for (uint32_t j = 0; j < 3; j++) {
      p_comp_out[0]->val[j] =
          vmul_s8(comp_samples[0].val[j], prb_signs[0].val[j]);
      p_comp_out[1]->val[j] =
          vmul_s8(comp_samples[1].val[j], prb_signs[1].val[j]);
    }

    dst += 2;
  }
  if (n_prb % 2 != 0) {
    int16x8x3_t prb_in;

    if (scale != nullptr) {
      prb_in = load3_cmplx_and_scale((const int16_t *)src, *scale);
    } else {
      prb_in.val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in.val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in.val[2] = vld1q_s16((const int16_t *)src);
    }

    // Extract the sign bit and absolute values for the PRB
    int8x8x3_t prb_signs = extract_sign_8b_s16x3q(&prb_in);

    int16x8x3_t prb_abs = abs_s16x3q(&prb_in);

    // Find the maximum in absRe and absIm
    int max_val = max_s16x3q(&prb_abs);

    // Determine compShift, the shift to be applied to the entire PRB
    int exp = __builtin_clz(max_val) - 16 - 1;
    exp = exp < 8 ? exp : 7;
    dst->exp = exp;

    int16x8_t comp_shift_vec = vdupq_n_s16(exp);

    shift_abs_s16x3q(&prb_abs, &comp_shift_vec);

    // Compress each sample, absBitWidth=15
    uint16x8x3_t check_thr1 = compress_s1_c1(&prb_abs, thr1);
    uint16x8x3_t check_thr3 = compress_s1_c3(&prb_abs, thr2);

    // Compress - Second Step: Perform compression calculation
    int16x8x3_t prb_abs_res1;
    int16x8x3_t prb_abs_res2 = dup_s16x3q(add_thr2_b8);
    int16x8x3_t prb_abs_res3 = dup_s16x3q(add_comm_b8);

    // Case1: compSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 7
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res1.val[j] = vrshrq_n_s16(prb_abs.val[j], 7);
    }

    // Case2: compSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    //  input_bits - output_bits + 1 = 8
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res2.val[j] =
          vrsraq_n_s16(prb_abs_res2.val[j], prb_abs.val[j], 8);
    }

    // Case3: compSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2)
    //  input_bits - output_bits + 2 = 9
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res3.val[j] =
          vrsraq_n_s16(prb_abs_res3.val[j], prb_abs.val[j], 9);
    }

    prb_abs_res1 = bsl_s16x3q(&check_thr1, &prb_abs_res1, &prb_abs_res2);

    prb_abs_res1 = bsl_s16x3q(&check_thr3, &prb_abs_res3, &prb_abs_res1);

    int8x8x3_t comp_samples = narrow_s16x3q(&prb_abs_res1);

    // Re-apply sign
    int8x8x3_t *p_comp_out = (int8x8x3_t *)dst->mantissa;
    for (uint32_t j = 0; j < 3; j++) {
      p_comp_out->val[j] = vmul_s8(comp_samples.val[j], prb_signs.val[j]);
    }
  }
  return ARMRAL_SUCCESS;
#endif
}

armral_status armral_mu_law_compr_9bit(uint32_t n_prb,
                                       const armral_cmplx_int16_t *src,
                                       armral_compressed_data_9bit *dst,
                                       const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const int16_t *data_in = (const int16_t *)src;

  // Set true predicate corresponding to 8x16-bit elements
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  svint16_t scale_im = svdup_n_s16(0);
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  // Values used during compression
  const svint16_t add_thr2_b9_vec = svdup_n_s16((int16_t)add_thr2_b9);
  const svint16_t add_comm_b9_vec = svdup_n_s16((int16_t)add_comm_b9);

  for (unsigned int i = 0; i < n_prb >> 1; i++) {
    // Load 2 PRBs = 24 complex symbols
    svint16_t prb1_in = svld1_s16(pg, data_in);
    svint16_t prb2_in = svld1_s16(pg, &data_in[8]);
    svint16_t prb3_in = svld1_s16(pg, &data_in[16]);
    svint16_t prb4_in = svld1_s16(pg, &data_in[24]);
    svint16_t prb5_in = svld1_s16(pg, &data_in[32]);
    svint16_t prb6_in = svld1_s16(pg, &data_in[40]);
    data_in += 48;

    if (scale != nullptr) {
      prb1_in = sv_cmplx_mul_combined_re_im(prb1_in, scale->re, scale_im);
      prb2_in = sv_cmplx_mul_combined_re_im(prb2_in, scale->re, scale_im);
      prb3_in = sv_cmplx_mul_combined_re_im(prb3_in, scale->re, scale_im);
      prb4_in = sv_cmplx_mul_combined_re_im(prb4_in, scale->re, scale_im);
      prb5_in = sv_cmplx_mul_combined_re_im(prb5_in, scale->re, scale_im);
      prb6_in = sv_cmplx_mul_combined_re_im(prb6_in, scale->re, scale_im);
    }

    // Extract the sign bit: use of predicate to identify
    // entries less than zero, the predicate will be used
    // to multiply by sign after compression
    const int16_t zero = 0;
    svbool_t pg_prb1_signs = svcmplt_n_s16(pg, prb1_in, zero);
    svbool_t pg_prb2_signs = svcmplt_n_s16(pg, prb2_in, zero);
    svbool_t pg_prb3_signs = svcmplt_n_s16(pg, prb3_in, zero);
    svbool_t pg_prb4_signs = svcmplt_n_s16(pg, prb4_in, zero);
    svbool_t pg_prb5_signs = svcmplt_n_s16(pg, prb5_in, zero);
    svbool_t pg_prb6_signs = svcmplt_n_s16(pg, prb6_in, zero);

    // Extract absolute values for the PRB
    svint16_t prb1_abs = svqabs_s16_m(prb1_in, pg, prb1_in);
    svint16_t prb2_abs = svqabs_s16_m(prb2_in, pg, prb2_in);
    svint16_t prb3_abs = svqabs_s16_m(prb3_in, pg, prb3_in);
    svint16_t prb4_abs = svqabs_s16_m(prb4_in, pg, prb4_in);
    svint16_t prb5_abs = svqabs_s16_m(prb5_in, pg, prb5_in);
    svint16_t prb6_abs = svqabs_s16_m(prb6_in, pg, prb6_in);

    // Find the maximum in absRe and absIm
    svint16_t v_max1 =
        svmax_s16_x(pg, prb1_abs, svmax_s16_x(pg, prb2_abs, prb3_abs));
    svint16_t v_max2 =
        svmax_s16_x(pg, prb4_abs, svmax_s16_x(pg, prb5_abs, prb6_abs));
    int16_t max_val[] = {svmaxv_s16(pg, v_max1), svmaxv_s16(pg, v_max2)};

    // Determine compShift, the shift to be applied to the entire PRB
    dst[0].exp = __builtin_clz(max_val[0]) - 16 - 1;
    dst[0].exp = dst[0].exp < 9 ? dst[0].exp : 8;
    dst[1].exp = __builtin_clz(max_val[1]) - 16 - 1;
    dst[1].exp = dst[1].exp < 9 ? dst[1].exp : 8;

    // Apply shift left (make greater) with saturation
    int16_t comp_shift[] = {dst[0].exp, dst[1].exp};
    prb1_abs = svqshl_n_s16_x(pg, prb1_abs, comp_shift[0]);
    prb2_abs = svqshl_n_s16_x(pg, prb2_abs, comp_shift[0]);
    prb3_abs = svqshl_n_s16_x(pg, prb3_abs, comp_shift[0]);
    prb4_abs = svqshl_n_s16_x(pg, prb4_abs, comp_shift[1]);
    prb5_abs = svqshl_n_s16_x(pg, prb5_abs, comp_shift[1]);
    prb6_abs = svqshl_n_s16_x(pg, prb6_abs, comp_shift[1]);

    // Compress each sample, absBitWidth=15

    // Compress - First Step: create predicates based on prbAbs values
    // Case 1: mark entries where prbAbs <= 2^(absBitWidth - 2) = 8192
    svbool_t pg1_case1 = svcmple_n_s16(pg, prb1_abs, thr1);
    svbool_t pg2_case1 = svcmple_n_s16(pg, prb2_abs, thr1);
    svbool_t pg3_case1 = svcmple_n_s16(pg, prb3_abs, thr1);
    svbool_t pg4_case1 = svcmple_n_s16(pg, prb4_abs, thr1);
    svbool_t pg5_case1 = svcmple_n_s16(pg, prb5_abs, thr1);
    svbool_t pg6_case1 = svcmple_n_s16(pg, prb6_abs, thr1);

    // Case 3: mark entries where (prbAbs > 2^(absBitWidth - 1) = 16384)
    svbool_t pg1_case3 = svcmpgt_n_s16(pg, prb1_abs, thr2);
    svbool_t pg2_case3 = svcmpgt_n_s16(pg, prb2_abs, thr2);
    svbool_t pg3_case3 = svcmpgt_n_s16(pg, prb3_abs, thr2);
    svbool_t pg4_case3 = svcmpgt_n_s16(pg, prb4_abs, thr2);
    svbool_t pg5_case3 = svcmpgt_n_s16(pg, prb5_abs, thr2);
    svbool_t pg6_case3 = svcmpgt_n_s16(pg, prb6_abs, thr2);

    // Compress - Second Step: Perform compression calculation

    // Case 2:  CompSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    svint16_t comp_samples1 = svrsra_n_s16(add_thr2_b9_vec, prb1_abs, 7);
    svint16_t comp_samples2 = svrsra_n_s16(add_thr2_b9_vec, prb2_abs, 7);
    svint16_t comp_samples3 = svrsra_n_s16(add_thr2_b9_vec, prb3_abs, 7);
    svint16_t comp_samples4 = svrsra_n_s16(add_thr2_b9_vec, prb4_abs, 7);
    svint16_t comp_samples5 = svrsra_n_s16(add_thr2_b9_vec, prb5_abs, 7);
    svint16_t comp_samples6 = svrsra_n_s16(add_thr2_b9_vec, prb6_abs, 7);

    // Case 1:  CompSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 6
    comp_samples1 =
        svsel_s16(pg1_case1, svrshr_n_s16_x(pg, prb1_abs, 6), comp_samples1);
    comp_samples2 =
        svsel_s16(pg2_case1, svrshr_n_s16_x(pg, prb2_abs, 6), comp_samples2);
    comp_samples3 =
        svsel_s16(pg3_case1, svrshr_n_s16_x(pg, prb3_abs, 6), comp_samples3);
    comp_samples4 =
        svsel_s16(pg4_case1, svrshr_n_s16_x(pg, prb4_abs, 6), comp_samples4);
    comp_samples5 =
        svsel_s16(pg5_case1, svrshr_n_s16_x(pg, prb5_abs, 6), comp_samples5);
    comp_samples6 =
        svsel_s16(pg6_case1, svrshr_n_s16_x(pg, prb6_abs, 6), comp_samples6);

    // Case 3:  CompSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2)
    comp_samples1 = svsel_s16(
        pg1_case3, svrsra_n_s16(add_comm_b9_vec, prb1_abs, 8), comp_samples1);
    comp_samples2 = svsel_s16(
        pg2_case3, svrsra_n_s16(add_comm_b9_vec, prb2_abs, 8), comp_samples2);
    comp_samples3 = svsel_s16(
        pg3_case3, svrsra_n_s16(add_comm_b9_vec, prb3_abs, 8), comp_samples3);
    comp_samples4 = svsel_s16(
        pg4_case3, svrsra_n_s16(add_comm_b9_vec, prb4_abs, 8), comp_samples4);
    comp_samples5 = svsel_s16(
        pg5_case3, svrsra_n_s16(add_comm_b9_vec, prb5_abs, 8), comp_samples5);
    comp_samples6 = svsel_s16(
        pg6_case3, svrsra_n_s16(add_comm_b9_vec, prb6_abs, 8), comp_samples6);

    // Compress 3x4 complex pairs into 3x9 bytes

    // Saturate to 2^(output_bits - 1) - 1 = 255
    comp_samples1 = svmin_n_s16_x(pg, comp_samples1, 255);
    comp_samples2 = svmin_n_s16_x(pg, comp_samples2, 255);
    comp_samples3 = svmin_n_s16_x(pg, comp_samples3, 255);
    comp_samples4 = svmin_n_s16_x(pg, comp_samples4, 255);
    comp_samples5 = svmin_n_s16_x(pg, comp_samples5, 255);
    comp_samples6 = svmin_n_s16_x(pg, comp_samples6, 255);

    // Multiply by sign using predicate on svint16
    comp_samples1 = svneg_s16_m(comp_samples1, pg_prb1_signs, comp_samples1);
    comp_samples2 = svneg_s16_m(comp_samples2, pg_prb2_signs, comp_samples2);
    comp_samples3 = svneg_s16_m(comp_samples3, pg_prb3_signs, comp_samples3);
    comp_samples4 = svneg_s16_m(comp_samples4, pg_prb4_signs, comp_samples4);
    comp_samples5 = svneg_s16_m(comp_samples5, pg_prb5_signs, comp_samples5);
    comp_samples6 = svneg_s16_m(comp_samples6, pg_prb6_signs, comp_samples6);

    const svint16_t *comps[] = {&comp_samples1, &comp_samples2, &comp_samples3,
                                &comp_samples4, &comp_samples5, &comp_samples6};
    pack_9bit_and_store_int16<2>(pg, comps, dst);

    dst += 2;
  }
  if (n_prb % 2 != 0) {
    // Load 1 PRB = 12 complex symbols
    svint16_t prb1_in = svld1_s16(pg, data_in);
    svint16_t prb2_in = svld1_s16(pg, &data_in[8]);
    svint16_t prb3_in = svld1_s16(pg, &data_in[16]);
    data_in += 24;

    if (scale != nullptr) {
      prb1_in = sv_cmplx_mul_combined_re_im(prb1_in, scale->re, scale_im);
      prb2_in = sv_cmplx_mul_combined_re_im(prb2_in, scale->re, scale_im);
      prb3_in = sv_cmplx_mul_combined_re_im(prb3_in, scale->re, scale_im);
    }

    // Extract the sign bit: use of predicate to identify
    // entries less than zero, the predicate will be used
    // to multiply by sign after compression
    const int16_t zero = 0;
    svbool_t pg_prb1_signs = svcmplt_n_s16(pg, prb1_in, zero);
    svbool_t pg_prb2_signs = svcmplt_n_s16(pg, prb2_in, zero);
    svbool_t pg_prb3_signs = svcmplt_n_s16(pg, prb3_in, zero);

    // Extract absolute values for the PRB
    svint16_t prb1_abs = svqabs_s16_m(prb1_in, pg, prb1_in);
    svint16_t prb2_abs = svqabs_s16_m(prb2_in, pg, prb2_in);
    svint16_t prb3_abs = svqabs_s16_m(prb3_in, pg, prb3_in);

    // Find the maximum in absRe and absIm
    svint16_t v_max =
        svmax_s16_x(pg, prb1_abs, svmax_s16_x(pg, prb2_abs, prb3_abs));
    int16_t max_val = svmaxv_s16(pg, v_max);

    // Determine compShift, the shift to be applied to the entire PRB
    dst->exp = __builtin_clz(max_val) - 16 - 1;
    dst->exp = dst->exp < 9 ? dst->exp : 8;

    // Apply shift left (make greater) with saturation
    int16_t comp_shift = dst->exp;
    prb1_abs = svqshl_n_s16_x(pg, prb1_abs, comp_shift);
    prb2_abs = svqshl_n_s16_x(pg, prb2_abs, comp_shift);
    prb3_abs = svqshl_n_s16_x(pg, prb3_abs, comp_shift);

    // Compress each sample, absBitWidth=15

    // Compress - First Step: create predicates based on prbAbs values
    // Case 1: mark entries where prbAbs <= 2^(absBitWidth - 2) = 8192
    svbool_t pg1_case1 = svcmple_n_s16(pg, prb1_abs, thr1);
    svbool_t pg2_case1 = svcmple_n_s16(pg, prb2_abs, thr1);
    svbool_t pg3_case1 = svcmple_n_s16(pg, prb3_abs, thr1);

    // Case 3: mark entries where (prbAbs > 2^(absBitWidth - 1) = 16384)
    svbool_t pg1_case3 = svcmpgt_n_s16(pg, prb1_abs, thr2);
    svbool_t pg2_case3 = svcmpgt_n_s16(pg, prb2_abs, thr2);
    svbool_t pg3_case3 = svcmpgt_n_s16(pg, prb3_abs, thr2);

    // Compress - Second Step: Perform compression calculation

    // Case 2:  CompSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    svint16_t comp_samples1 = svrsra_n_s16(add_thr2_b9_vec, prb1_abs, 7);
    svint16_t comp_samples2 = svrsra_n_s16(add_thr2_b9_vec, prb2_abs, 7);
    svint16_t comp_samples3 = svrsra_n_s16(add_thr2_b9_vec, prb3_abs, 7);

    // Case 1:  CompSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 6
    comp_samples1 =
        svsel_s16(pg1_case1, svrshr_n_s16_x(pg, prb1_abs, 6), comp_samples1);
    comp_samples2 =
        svsel_s16(pg2_case1, svrshr_n_s16_x(pg, prb2_abs, 6), comp_samples2);
    comp_samples3 =
        svsel_s16(pg3_case1, svrshr_n_s16_x(pg, prb3_abs, 6), comp_samples3);

    // Case 3:  CompSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2)
    comp_samples1 = svsel_s16(
        pg1_case3, svrsra_n_s16(add_comm_b9_vec, prb1_abs, 8), comp_samples1);
    comp_samples2 = svsel_s16(
        pg2_case3, svrsra_n_s16(add_comm_b9_vec, prb2_abs, 8), comp_samples2);
    comp_samples3 = svsel_s16(
        pg3_case3, svrsra_n_s16(add_comm_b9_vec, prb3_abs, 8), comp_samples3);

    // Compress 3x4 complex pairs into 3x9 bytes

    // Saturate to 2^(output_bits - 1) - 1 = 255
    comp_samples1 = svmin_n_s16_x(pg, comp_samples1, 255);
    comp_samples2 = svmin_n_s16_x(pg, comp_samples2, 255);
    comp_samples3 = svmin_n_s16_x(pg, comp_samples3, 255);

    // Multiply by sign using predicate on svint16
    comp_samples1 = svneg_s16_m(comp_samples1, pg_prb1_signs, comp_samples1);
    comp_samples2 = svneg_s16_m(comp_samples2, pg_prb2_signs, comp_samples2);
    comp_samples3 = svneg_s16_m(comp_samples3, pg_prb3_signs, comp_samples3);

    const svint16_t *regs[] = {&comp_samples1, &comp_samples2, &comp_samples3};
    pack_9bit_and_store_int16<1>(pg, regs, dst);
  }
  return ARMRAL_SUCCESS;
#else
  // Definitions for saturating results to signed 9-bit values
  int16x8x3_t reg[2];
  const int16x8_t sat_pos = vdupq_n_s16(255);
  const int16x8_t sat_neg = vdupq_n_s16(-255);

  for (int32_t i = 0; i < (int32_t)n_prb - 1; i += 2) {
    int16x8x3_t prb_in[2];

    if (scale != nullptr) {
      prb_in[0] = load3_cmplx_and_scale((const int16_t *)src, *scale);
      src += 12;
      prb_in[1] = load3_cmplx_and_scale((const int16_t *)src, *scale);
      src += 12;
    } else {
      prb_in[0].val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[0].val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[0].val[2] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[1].val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[1].val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[1].val[2] = vld1q_s16((const int16_t *)src);
      src += 4;
    }

    // Extract the sign bit and absolute values for the PRB
    int16x8x3_t prb_signs[] = {extract_sign_s16x3q(&prb_in[0]),
                               extract_sign_s16x3q(&prb_in[1])};

    int16x8x3_t prb_abs[] = {abs_s16x3q(&prb_in[0]), abs_s16x3q(&prb_in[1])};

    // Find the maximum in absRe and absIm
    int max_val[] = {max_s16x3q(&prb_abs[0]), max_s16x3q(&prb_abs[1])};

    // Determine compShift, the shift to be applied to the entire PRB
    int exp[] = {__builtin_clz(max_val[0]) - 16 - 1,
                 __builtin_clz(max_val[1]) - 16 - 1};
    exp[0] = exp[0] < 9 ? exp[0] : 8;
    exp[1] = exp[1] < 9 ? exp[1] : 8;
    dst[0].exp = exp[0];
    dst[1].exp = exp[1];

    int16x8_t comp_shift_vec[] = {vdupq_n_s16(exp[0]), vdupq_n_s16(exp[1])};

    shift_abs_s16x3q(&prb_abs[0], &comp_shift_vec[0]);
    shift_abs_s16x3q(&prb_abs[1], &comp_shift_vec[1]);

    int16x8_t thr1_b9 = vdupq_n_s16(8192);
    int16x8_t thr2_b9 = vdupq_n_s16(16384);

    // Compress - First Step: Set bitmasks based on prbAbs values
    uint16x8x3_t check_thr1[] = {compress_s1_c1(&prb_abs[0], thr1_b9),
                                 compress_s1_c1(&prb_abs[1], thr1_b9)};
    uint16x8x3_t check_thr3[] = {compress_s1_c3(&prb_abs[0], thr2_b9),
                                 compress_s1_c3(&prb_abs[1], thr2_b9)};

    // Compress - Second Step: Perform compression calculation
    int16x8x3_t prb_abs_res1[2];
    int16x8x3_t prb_abs_res2[] = {dup_s16x3q(add_thr2_b9),
                                  dup_s16x3q(add_thr2_b9)};
    int16x8x3_t prb_abs_res3[] = {dup_s16x3q(add_comm_b9),
                                  dup_s16x3q(add_comm_b9)};

    // Case1: compSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 6
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res1[0].val[j] = vrshrq_n_s16(prb_abs[0].val[j], 6);
      prb_abs_res1[1].val[j] = vrshrq_n_s16(prb_abs[1].val[j], 6);
    }

    // Case2: compSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    //  input_bits - output_bits + 1 = 7
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res2[0].val[j] =
          vrsraq_n_s16(prb_abs_res2[0].val[j], prb_abs[0].val[j], 7);
      prb_abs_res2[1].val[j] =
          vrsraq_n_s16(prb_abs_res2[1].val[j], prb_abs[1].val[j], 7);
    }

    // Case3: compSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2)
    //  input_bits - output_bits + 2 = 8
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res3[0].val[j] =
          vrsraq_n_s16(prb_abs_res3[0].val[j], prb_abs[0].val[j], 8);
      prb_abs_res3[1].val[j] =
          vrsraq_n_s16(prb_abs_res3[1].val[j], prb_abs[1].val[j], 8);
    }

    prb_abs_res1[0] =
        bsl_s16x3q(&check_thr1[0], &prb_abs_res1[0], &prb_abs_res2[0]);
    prb_abs_res1[1] =
        bsl_s16x3q(&check_thr1[1], &prb_abs_res1[1], &prb_abs_res2[1]);

    int16x8x3_t comp_samples[2];

    comp_samples[0] =
        bsl_s16x3q(&check_thr3[0], &prb_abs_res3[0], &prb_abs_res1[0]);
    comp_samples[1] =
        bsl_s16x3q(&check_thr3[1], &prb_abs_res3[1], &prb_abs_res1[1]);

    // Saturate to 2^(output_bits - 1) - 1 = 255
    for (uint32_t j = 0; j < 3; j++) {
      reg[0].val[j] = vmaxq_s16(
          sat_neg, vminq_s16(sat_pos, vmulq_s16(comp_samples[0].val[j],
                                                prb_signs[0].val[j])));
      reg[1].val[j] = vmaxq_s16(
          sat_neg, vminq_s16(sat_pos, vmulq_s16(comp_samples[1].val[j],
                                                prb_signs[1].val[j])));
    }

    pack_9bit_and_store_int16<2>(reg, dst);

    dst += 2;
  }
  if (n_prb % 2 != 0) {
    // Load 12 armral_cpmplx_int16_t value (24 * int16_t)
    int16x8x3_t prb_in;
    if (scale != nullptr) {
      prb_in = load3_cmplx_and_scale((const int16_t *)src, *scale);
    } else {
      prb_in.val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in.val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in.val[2] = vld1q_s16((const int16_t *)src);
    }

    // Extract the sign bit and absolute values for the PRB
    int16x8x3_t prb_signs = extract_sign_s16x3q(&prb_in);

    int16x8x3_t prb_abs = abs_s16x3q(&prb_in);

    // Find the maximum in absRe and absIm
    int max_val = max_s16x3q(&prb_abs);

    // Determine compShift, the shift to be applied to the entire PRB
    int exp = __builtin_clz(max_val) - 16 - 1;
    exp = exp < 9 ? exp : 8;
    dst->exp = exp;

    int16x8_t comp_shift_vec = vdupq_n_s16(exp);

    shift_abs_s16x3q(&prb_abs, &comp_shift_vec);

    int16x8_t thr1_b9 = vdupq_n_s16(8192);
    int16x8_t thr2_b9 = vdupq_n_s16(16384);

    // Compress each sample, absBitWidth=15
    uint16x8x3_t check_thr1 = compress_s1_c1(&prb_abs, thr1_b9);
    uint16x8x3_t check_thr3 = compress_s1_c3(&prb_abs, thr2_b9);

    // Compress - Second Step: Perform compression calculation
    int16x8x3_t prb_abs_res1;
    int16x8x3_t prb_abs_res2 = dup_s16x3q(add_thr2_b9);
    int16x8x3_t prb_abs_res3 = dup_s16x3q(add_comm_b9);

    // Case1: compSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 7
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res1.val[j] = vrshrq_n_s16(prb_abs.val[j], 6);
    }

    // Case2: compSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    //  input_bits - output_bits + 1 = 8
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res2.val[j] =
          vrsraq_n_s16(prb_abs_res2.val[j], prb_abs.val[j], 7);
    }

    // Case3: compSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2)
    //  input_bits - output_bits + 2 = 9
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res3.val[j] =
          vrsraq_n_s16(prb_abs_res3.val[j], prb_abs.val[j], 8);
    }

    prb_abs_res1 = bsl_s16x3q(&check_thr1, &prb_abs_res1, &prb_abs_res2);

    int16x8x3_t comp_samples =
        bsl_s16x3q(&check_thr3, &prb_abs_res3, &prb_abs_res1);

    // Saturate to 2^(output_bits - 1) - 1 = 255
    for (uint32_t j = 0; j < 3; j++) {
      reg[0].val[j] = vmaxq_s16(
          sat_neg,
          vminq_s16(sat_pos, vmulq_s16(comp_samples.val[j], prb_signs.val[j])));
    }

    pack_9bit_and_store_int16<1>(reg, dst);
  }
  return ARMRAL_SUCCESS;
#endif
}

armral_status armral_mu_law_compr_14bit(uint32_t n_prb,
                                        const armral_cmplx_int16_t *src,
                                        armral_compressed_data_14bit *dst,
                                        const armral_cmplx_int16_t *scale) {
#if ARMRAL_ARCH_SVE >= 2
  const int16_t *data_in = (const int16_t *)src;

  // Set true predicate corresponding to 8x16-bit elements
  const svbool_t pg = svptrue_pat_b16(SV_VL8);

  // Set predicate to select 14x8-bit elements
  const svbool_t pg14 = svwhilelt_b8(0, 14);

  svint16_t scale_im = svdup_n_s16(0);
  if (scale != nullptr) {
    scale_im = svdup_n_s16(scale->im);
  }

  // Values used during compression
  const svint16_t add_thr2_b14_vec = svdup_n_s16((int16_t)add_thr2_b14);
  const svint16_t add_comm_b14_vec = svdup_n_s16((int16_t)add_comm_b14);

  // Shifts, indices and mask used during bit-packing
  const svuint16_t shift_left = svdupq_n_u16(2, 4, 6, 8, 2, 4, 6, 8);
  const svuint16_t shift_right = svdupq_n_u16(0, 4, 2, 0, 0, 4, 2, 0);
  const svuint8_t contig_idx =
      svdupq_n_u8(1, 0, 3, 2, 5, 4, 7, 9, 8, 11, 10, 13, 12, 15, 14, 255);
  const svuint8_t contig2_idx = svdupq_n_u8(
      255, 3, 255, 5, 255, 7, 255, 255, 11, 255, 13, 255, 15, 255, 255, 255);
  const svuint8_t sbli = svdupq_n_u8(255, 252, 255, 240, 255, 192, 255, 255,
                                     252, 255, 240, 255, 192, 255, 255, 255);

  for (unsigned int i = 0; i < n_prb >> 1; i++) {
    // Load 2 PRBs = 24 complex symbols
    svint16_t prb1_in = svld1_s16(pg, data_in);
    svint16_t prb2_in = svld1_s16(pg, &data_in[8]);
    svint16_t prb3_in = svld1_s16(pg, &data_in[16]);
    svint16_t prb4_in = svld1_s16(pg, &data_in[24]);
    svint16_t prb5_in = svld1_s16(pg, &data_in[32]);
    svint16_t prb6_in = svld1_s16(pg, &data_in[40]);
    data_in += 48;

    if (scale != nullptr) {
      prb1_in = sv_cmplx_mul_combined_re_im(prb1_in, scale->re, scale_im);
      prb2_in = sv_cmplx_mul_combined_re_im(prb2_in, scale->re, scale_im);
      prb3_in = sv_cmplx_mul_combined_re_im(prb3_in, scale->re, scale_im);
      prb4_in = sv_cmplx_mul_combined_re_im(prb4_in, scale->re, scale_im);
      prb5_in = sv_cmplx_mul_combined_re_im(prb5_in, scale->re, scale_im);
      prb6_in = sv_cmplx_mul_combined_re_im(prb6_in, scale->re, scale_im);
    }

    // Extract the sign bit: use of predicate to identify
    // entries less than zero, the predicate will be used
    // to multiply by sign after compression
    const int16_t zero = 0;
    svbool_t pg_prb1_signs = svcmplt_n_s16(pg, prb1_in, zero);
    svbool_t pg_prb2_signs = svcmplt_n_s16(pg, prb2_in, zero);
    svbool_t pg_prb3_signs = svcmplt_n_s16(pg, prb3_in, zero);
    svbool_t pg_prb4_signs = svcmplt_n_s16(pg, prb4_in, zero);
    svbool_t pg_prb5_signs = svcmplt_n_s16(pg, prb5_in, zero);
    svbool_t pg_prb6_signs = svcmplt_n_s16(pg, prb6_in, zero);

    // Extract absolute values for the PRB
    svint16_t prb1_abs = svqabs_s16_m(prb1_in, pg, prb1_in);
    svint16_t prb2_abs = svqabs_s16_m(prb2_in, pg, prb2_in);
    svint16_t prb3_abs = svqabs_s16_m(prb3_in, pg, prb3_in);
    svint16_t prb4_abs = svqabs_s16_m(prb4_in, pg, prb4_in);
    svint16_t prb5_abs = svqabs_s16_m(prb5_in, pg, prb5_in);
    svint16_t prb6_abs = svqabs_s16_m(prb6_in, pg, prb6_in);

    // Find the maximum in absRe and absIm
    svint16_t v_max1 =
        svmax_s16_x(pg, prb1_abs, svmax_s16_x(pg, prb2_abs, prb3_abs));
    svint16_t v_max2 =
        svmax_s16_x(pg, prb4_abs, svmax_s16_x(pg, prb5_abs, prb6_abs));
    int16_t max_val[] = {svmaxv_s16(pg, v_max1), svmaxv_s16(pg, v_max2)};

    // Determine compShift, the shift to be applied to the entire PRB
    dst[0].exp = __builtin_clz(max_val[0]) - 16 - 1;
    dst[0].exp = dst[0].exp < 14 ? dst[0].exp : 13;
    dst[1].exp = __builtin_clz(max_val[1]) - 16 - 1;
    dst[1].exp = dst[1].exp < 14 ? dst[1].exp : 13;

    // Apply shift left (make greater) with saturation
    int16_t comp_shift[] = {dst[0].exp, dst[1].exp};
    prb1_abs = svqshl_n_s16_z(pg, prb1_abs, comp_shift[0]);
    prb2_abs = svqshl_n_s16_z(pg, prb2_abs, comp_shift[0]);
    prb3_abs = svqshl_n_s16_z(pg, prb3_abs, comp_shift[0]);
    prb4_abs = svqshl_n_s16_z(pg, prb4_abs, comp_shift[1]);
    prb5_abs = svqshl_n_s16_z(pg, prb5_abs, comp_shift[1]);
    prb6_abs = svqshl_n_s16_z(pg, prb6_abs, comp_shift[1]);

    // Compress each sample, absBitWidth=15

    // Compress - First Step: create predicates based on prbAbs values
    // Case 1: mark entries where prbAbs <= 2^(absBitWidth - 2) = 8192
    svbool_t pg1_case1 = svcmple_n_s16(pg, prb1_abs, thr1);
    svbool_t pg2_case1 = svcmple_n_s16(pg, prb2_abs, thr1);
    svbool_t pg3_case1 = svcmple_n_s16(pg, prb3_abs, thr1);
    svbool_t pg4_case1 = svcmple_n_s16(pg, prb4_abs, thr1);
    svbool_t pg5_case1 = svcmple_n_s16(pg, prb5_abs, thr1);
    svbool_t pg6_case1 = svcmple_n_s16(pg, prb6_abs, thr1);

    // Case 3: mark entries where (prbAbs > 2^(absBitWidth - 1) = 16384)
    svbool_t pg1_case3 = svcmpgt_n_s16(pg, prb1_abs, thr2);
    svbool_t pg2_case3 = svcmpgt_n_s16(pg, prb2_abs, thr2);
    svbool_t pg3_case3 = svcmpgt_n_s16(pg, prb3_abs, thr2);
    svbool_t pg4_case3 = svcmpgt_n_s16(pg, prb4_abs, thr2);
    svbool_t pg5_case3 = svcmpgt_n_s16(pg, prb5_abs, thr2);
    svbool_t pg6_case3 = svcmpgt_n_s16(pg, prb6_abs, thr2);

    // Compress - Second Step: Perform compression calculation

    // Case 2:  CompSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    // input_bits - output_bits + 1 = 2
    svint16_t comp_samples1 = svrsra_n_s16(add_thr2_b14_vec, prb1_abs, 2);
    svint16_t comp_samples2 = svrsra_n_s16(add_thr2_b14_vec, prb2_abs, 2);
    svint16_t comp_samples3 = svrsra_n_s16(add_thr2_b14_vec, prb3_abs, 2);
    svint16_t comp_samples4 = svrsra_n_s16(add_thr2_b14_vec, prb4_abs, 2);
    svint16_t comp_samples5 = svrsra_n_s16(add_thr2_b14_vec, prb5_abs, 2);
    svint16_t comp_samples6 = svrsra_n_s16(add_thr2_b14_vec, prb6_abs, 2);

    // Case 1:  CompSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 1
    comp_samples1 =
        svsel_s16(pg1_case1, svrshr_n_s16_x(pg, prb1_abs, 1), comp_samples1);
    comp_samples2 =
        svsel_s16(pg2_case1, svrshr_n_s16_x(pg, prb2_abs, 1), comp_samples2);
    comp_samples3 =
        svsel_s16(pg3_case1, svrshr_n_s16_x(pg, prb3_abs, 1), comp_samples3);
    comp_samples4 =
        svsel_s16(pg4_case1, svrshr_n_s16_x(pg, prb4_abs, 1), comp_samples4);
    comp_samples5 =
        svsel_s16(pg5_case1, svrshr_n_s16_x(pg, prb5_abs, 1), comp_samples5);
    comp_samples6 =
        svsel_s16(pg6_case1, svrshr_n_s16_x(pg, prb6_abs, 1), comp_samples6);

    // Case 3:  CompSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2)
    // input_bits - output_bits + 2 = 3
    comp_samples1 = svsel_s16(
        pg1_case3, svrsra_n_s16(add_comm_b14_vec, prb1_abs, 3), comp_samples1);
    comp_samples2 = svsel_s16(
        pg2_case3, svrsra_n_s16(add_comm_b14_vec, prb2_abs, 3), comp_samples2);
    comp_samples3 = svsel_s16(
        pg3_case3, svrsra_n_s16(add_comm_b14_vec, prb3_abs, 3), comp_samples3);
    comp_samples4 = svsel_s16(
        pg4_case3, svrsra_n_s16(add_comm_b14_vec, prb4_abs, 3), comp_samples4);
    comp_samples5 = svsel_s16(
        pg5_case3, svrsra_n_s16(add_comm_b14_vec, prb5_abs, 3), comp_samples5);
    comp_samples6 = svsel_s16(
        pg6_case3, svrsra_n_s16(add_comm_b14_vec, prb6_abs, 3), comp_samples6);

    // Compress 3x4 complex pairs into 3x9 bytes

    // Saturate to 2^(output_bits - 1) - 1 = 8191
    comp_samples1 = svmin_n_s16_x(pg, comp_samples1, 8191);
    comp_samples2 = svmin_n_s16_x(pg, comp_samples2, 8191);
    comp_samples3 = svmin_n_s16_x(pg, comp_samples3, 8191);
    comp_samples4 = svmin_n_s16_x(pg, comp_samples4, 8191);
    comp_samples5 = svmin_n_s16_x(pg, comp_samples5, 8191);
    comp_samples6 = svmin_n_s16_x(pg, comp_samples6, 8191);

    // Multiply by sign using predicate on svint16
    comp_samples1 = svneg_s16_m(comp_samples1, pg_prb1_signs, comp_samples1);
    comp_samples2 = svneg_s16_m(comp_samples2, pg_prb2_signs, comp_samples2);
    comp_samples3 = svneg_s16_m(comp_samples3, pg_prb3_signs, comp_samples3);
    comp_samples4 = svneg_s16_m(comp_samples4, pg_prb4_signs, comp_samples4);
    comp_samples5 = svneg_s16_m(comp_samples5, pg_prb5_signs, comp_samples5);
    comp_samples6 = svneg_s16_m(comp_samples6, pg_prb6_signs, comp_samples6);

    // In the comments A, B, C and D are referred to according to:
    // A = comp_samplesx[0]
    // B = comp_samplesx[1]
    // C = comp_samplesx[2]
    // D = comp_samplesx[3]
    // where x = 1, 2 or 3. From E onwards the pattern repeats
    // Compression starts from:
    // [--Dddddddddddddd|--Cccccccccccccc|--Bbbbbbbbbbbbbb|--Aaaaaaaaaaaaaa]
    // where "-" denotes leading sign bits that we don't care about and capitals
    // indicate the MSB of the data

    svuint16_t ucomp1 = svreinterpret_u16_s16(comp_samples1);
    svuint16_t ucomp2 = svreinterpret_u16_s16(comp_samples2);
    svuint16_t ucomp3 = svreinterpret_u16_s16(comp_samples3);
    svuint16_t ucomp4 = svreinterpret_u16_s16(comp_samples4);
    svuint16_t ucomp5 = svreinterpret_u16_s16(comp_samples5);
    svuint16_t ucomp6 = svreinterpret_u16_s16(comp_samples6);

    // Move the 14 bits of A, 12 LSBs of B, 10 LSBs of C and 8 LSBs of D to the
    // top of the 16-bit elements
    // [dddddddd00000000|cccccccccc000000|bbbbbbbbbbbb0000|Aaaaaaaaaaaaaa00]
    svuint16_t ucomp_lsh1 = svlsl_u16_x(pg, ucomp1, shift_left);
    svuint16_t ucomp_lsh2 = svlsl_u16_x(pg, ucomp2, shift_left);
    svuint16_t ucomp_lsh3 = svlsl_u16_x(pg, ucomp3, shift_left);
    svuint16_t ucomp_lsh4 = svlsl_u16_x(pg, ucomp4, shift_left);
    svuint16_t ucomp_lsh5 = svlsl_u16_x(pg, ucomp5, shift_left);
    svuint16_t ucomp_lsh6 = svlsl_u16_x(pg, ucomp6, shift_left);

    // Move the 2 MSBs of B and 4 MSBs of C to where they need to be for
    // reordering
    // [--Dddddddddddddd|00--Cccccccccccc|0000--Bbbbbbbbbb|--Aaaaaaaaaaaaaa]
    ucomp1 = svlsr_u16_x(pg, ucomp1, shift_right);
    ucomp2 = svlsr_u16_x(pg, ucomp2, shift_right);
    ucomp3 = svlsr_u16_x(pg, ucomp3, shift_right);
    ucomp4 = svlsr_u16_x(pg, ucomp4, shift_right);
    ucomp5 = svlsr_u16_x(pg, ucomp5, shift_right);
    ucomp6 = svlsr_u16_x(pg, ucomp6, shift_right);

    // Reorder left-shifted data so all bits of A and LSBs of B, C and D are in
    // correct position
    // [dddddddd|cc000000|cccccccc|bbbb0000|bbbbbbbb|aaaaaa00|Aaaaaaaa]
    svuint8_t contig1 = svtbl_u8(svreinterpret_u8_u16(ucomp_lsh1), contig_idx);
    svuint8_t contig2 = svtbl_u8(svreinterpret_u8_u16(ucomp_lsh2), contig_idx);
    svuint8_t contig3 = svtbl_u8(svreinterpret_u8_u16(ucomp_lsh3), contig_idx);
    svuint8_t contig4 = svtbl_u8(svreinterpret_u8_u16(ucomp_lsh4), contig_idx);
    svuint8_t contig5 = svtbl_u8(svreinterpret_u8_u16(ucomp_lsh5), contig_idx);
    svuint8_t contig6 = svtbl_u8(svreinterpret_u8_u16(ucomp_lsh6), contig_idx);

    // Reorder right-shifted data so that MSBs of B, C and D are in correct
    // position [00000000|--Dddddd|00000000|00--Cccc|00000000|0000--Bb|00000000]
    svuint8_t contig2_1 = svtbl_u8(svreinterpret_u8_u16(ucomp1), contig2_idx);
    svuint8_t contig2_2 = svtbl_u8(svreinterpret_u8_u16(ucomp2), contig2_idx);
    svuint8_t contig2_3 = svtbl_u8(svreinterpret_u8_u16(ucomp3), contig2_idx);
    svuint8_t contig2_4 = svtbl_u8(svreinterpret_u8_u16(ucomp4), contig2_idx);
    svuint8_t contig2_5 = svtbl_u8(svreinterpret_u8_u16(ucomp5), contig2_idx);
    svuint8_t contig2_6 = svtbl_u8(svreinterpret_u8_u16(ucomp6), contig2_idx);

    // Combine MSBs and LSBs using mask
    // [dddddddd|ccDddddd|cccccccc|bbbbCccc|bbbbbbbb|aaaaaaBb|Aaaaaaaa]
    svuint8_t res1 = svbsl_u8(contig1, contig2_1, sbli);
    svuint8_t res2 = svbsl_u8(contig2, contig2_2, sbli);
    svuint8_t res3 = svbsl_u8(contig3, contig2_3, sbli);
    svuint8_t res4 = svbsl_u8(contig4, contig2_4, sbli);
    svuint8_t res5 = svbsl_u8(contig5, contig2_5, sbli);
    svuint8_t res6 = svbsl_u8(contig6, contig2_6, sbli);

    // Write 42x8-bit elements using predication to store 14 bytes at a time
    int8_t *data_out[] = {dst[0].mantissa, dst[1].mantissa};
    svst1_s8(pg14, data_out[0], svreinterpret_s8_u8(res1));
    svst1_s8(pg14, data_out[1], svreinterpret_s8_u8(res4));
    data_out[0] += 14;
    data_out[1] += 14;
    svst1_s8(pg14, data_out[0], svreinterpret_s8_u8(res2));
    svst1_s8(pg14, data_out[1], svreinterpret_s8_u8(res5));
    data_out[0] += 14;
    data_out[1] += 14;
    svst1_s8(pg14, data_out[0], svreinterpret_s8_u8(res3));
    svst1_s8(pg14, data_out[1], svreinterpret_s8_u8(res6));

    dst += 2;
  }
  if (n_prb % 2 != 0) {
    // Load 1 PRB = 12 complex symbols
    svint16_t prb1_in = svld1_s16(pg, data_in);
    svint16_t prb2_in = svld1_s16(pg, &data_in[8]);
    svint16_t prb3_in = svld1_s16(pg, &data_in[16]);
    data_in += 24;

    if (scale != nullptr) {
      prb1_in = sv_cmplx_mul_combined_re_im(prb1_in, scale->re, scale_im);
      prb2_in = sv_cmplx_mul_combined_re_im(prb2_in, scale->re, scale_im);
      prb3_in = sv_cmplx_mul_combined_re_im(prb3_in, scale->re, scale_im);
    }

    // Extract the sign bit: use of predicate to identify
    // entries less than zero, the predicate will be used
    // to multiply by sign after compression
    const int16_t zero = 0;
    svbool_t pg_prb1_signs = svcmplt_n_s16(pg, prb1_in, zero);
    svbool_t pg_prb2_signs = svcmplt_n_s16(pg, prb2_in, zero);
    svbool_t pg_prb3_signs = svcmplt_n_s16(pg, prb3_in, zero);

    // Extract absolute values for the PRB
    svint16_t prb1_abs = svqabs_s16_m(prb1_in, pg, prb1_in);
    svint16_t prb2_abs = svqabs_s16_m(prb2_in, pg, prb2_in);
    svint16_t prb3_abs = svqabs_s16_m(prb3_in, pg, prb3_in);

    // Find the maximum in absRe and absIm
    svint16_t v_max =
        svmax_s16_x(pg, prb1_abs, svmax_s16_x(pg, prb2_abs, prb3_abs));
    int16_t max_val = svmaxv_s16(pg, v_max);

    // Determine compShift, the shift to be applied to the entire PRB
    dst->exp = __builtin_clz(max_val) - 16 - 1;
    dst->exp = dst->exp < 14 ? dst->exp : 13;

    // Apply shift left (make greater) with saturation
    int16_t comp_shift = dst->exp;
    prb1_abs = svqshl_n_s16_z(pg, prb1_abs, comp_shift);
    prb2_abs = svqshl_n_s16_z(pg, prb2_abs, comp_shift);
    prb3_abs = svqshl_n_s16_z(pg, prb3_abs, comp_shift);

    // Compress each sample, absBitWidth=15

    // Compress - First Step: create predicates based on prbAbs values
    // Case 1: mark entries where prbAbs <= 2^(absBitWidth - 2) = 8192
    svbool_t pg1_case1 = svcmple_n_s16(pg, prb1_abs, thr1);
    svbool_t pg2_case1 = svcmple_n_s16(pg, prb2_abs, thr1);
    svbool_t pg3_case1 = svcmple_n_s16(pg, prb3_abs, thr1);

    // Case 3: mark entries where (prbAbs > 2^(absBitWidth - 1) = 16384)
    svbool_t pg1_case3 = svcmpgt_n_s16(pg, prb1_abs, thr2);
    svbool_t pg2_case3 = svcmpgt_n_s16(pg, prb2_abs, thr2);
    svbool_t pg3_case3 = svcmpgt_n_s16(pg, prb3_abs, thr2);

    // Compress - Second Step: Perform compression calculation

    // Case 2:  CompSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    // input_bits - output_bits + 1 = 2
    svint16_t comp_samples1 = svrsra_n_s16(add_thr2_b14_vec, prb1_abs, 2);
    svint16_t comp_samples2 = svrsra_n_s16(add_thr2_b14_vec, prb2_abs, 2);
    svint16_t comp_samples3 = svrsra_n_s16(add_thr2_b14_vec, prb3_abs, 2);

    // Case 1:  CompSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 1
    comp_samples1 =
        svsel_s16(pg1_case1, svrshr_n_s16_x(pg, prb1_abs, 1), comp_samples1);
    comp_samples2 =
        svsel_s16(pg2_case1, svrshr_n_s16_x(pg, prb2_abs, 1), comp_samples2);
    comp_samples3 =
        svsel_s16(pg3_case1, svrshr_n_s16_x(pg, prb3_abs, 1), comp_samples3);

    // Case 3:  CompSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2)
    // input_bits - output_bits + 2 = 3
    comp_samples1 = svsel_s16(
        pg1_case3, svrsra_n_s16(add_comm_b14_vec, prb1_abs, 3), comp_samples1);
    comp_samples2 = svsel_s16(
        pg2_case3, svrsra_n_s16(add_comm_b14_vec, prb2_abs, 3), comp_samples2);
    comp_samples3 = svsel_s16(
        pg3_case3, svrsra_n_s16(add_comm_b14_vec, prb3_abs, 3), comp_samples3);

    // Compress 3x4 complex pairs into 3x9 bytes

    // Saturate to 2^(output_bits - 1) - 1 = 8191
    comp_samples1 = svmin_n_s16_x(pg, comp_samples1, 8191);
    comp_samples2 = svmin_n_s16_x(pg, comp_samples2, 8191);
    comp_samples3 = svmin_n_s16_x(pg, comp_samples3, 8191);

    // Multiply by sign using predicate on svint16
    comp_samples1 = svneg_s16_m(comp_samples1, pg_prb1_signs, comp_samples1);
    comp_samples2 = svneg_s16_m(comp_samples2, pg_prb2_signs, comp_samples2);
    comp_samples3 = svneg_s16_m(comp_samples3, pg_prb3_signs, comp_samples3);

    const svint16_t *regs[] = {&comp_samples1, &comp_samples2, &comp_samples3};
    pack_14bit_and_store_int16<false>(pg, regs, dst);
  }
  return ARMRAL_SUCCESS;
#else
  int16x8x3_t regs[2];
  for (int i = 0; i < (int)n_prb - 1; i += 2) {
    // Load 24 armral_cpmplx_int16_t value (48 * int16_t)
    int16x8x3_t prb_in[2];
    if (scale != nullptr) {
      prb_in[0] = load3_cmplx_and_scale((const int16_t *)src, *scale);
      src += 12;
      prb_in[1] = load3_cmplx_and_scale((const int16_t *)src, *scale);
      src += 12;
    } else {
      prb_in[0].val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[0].val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[0].val[2] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[1].val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[1].val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in[1].val[2] = vld1q_s16((const int16_t *)src);
      src += 4;
    }

    // Extract the sign bit and absolute values for the PRB
    int16x8x3_t prb_signs[] = {extract_sign_s16x3q(&prb_in[0]),
                               extract_sign_s16x3q(&prb_in[1])};

    int16x8x3_t prb_abs[] = {abs_s16x3q(&prb_in[0]), abs_s16x3q(&prb_in[1])};

    // Find the maximum in absRe and absIm
    int max_val[] = {max_s16x3q(&prb_abs[0]), max_s16x3q(&prb_abs[1])};

    // Determine compShift, the shift to be applied to the entire PRB
    int exp[] = {__builtin_clz(max_val[0]) - 16 - 1,
                 __builtin_clz(max_val[1]) - 16 - 1};
    exp[0] = exp[0] < 14 ? exp[0] : 13;
    exp[1] = exp[1] < 14 ? exp[1] : 13;
    dst[0].exp = exp[0];
    dst[1].exp = exp[1];

    int16x8_t comp_shift_vec[] = {vdupq_n_s16(exp[0]), vdupq_n_s16(exp[1])};

    shift_abs_s16x3q(&prb_abs[0], &comp_shift_vec[0]);
    shift_abs_s16x3q(&prb_abs[1], &comp_shift_vec[1]);

    int16x8_t thr1_b14 = vdupq_n_s16(8192);
    int16x8_t thr2_b14 = vdupq_n_s16(16384);

    // Compress each sample, absBitWidth=15
    uint16x8x3_t check_thr1[] = {compress_s1_c1(&prb_abs[0], thr1_b14),
                                 compress_s1_c1(&prb_abs[1], thr1_b14)};
    uint16x8x3_t check_thr3[] = {compress_s1_c3(&prb_abs[0], thr2_b14),
                                 compress_s1_c3(&prb_abs[1], thr2_b14)};

    // Compress - Second Step: Perform compression calculation
    int16x8x3_t prb_abs_res1[2];
    int16x8x3_t prb_abs_res2[] = {dup_s16x3q(add_thr2_b14),
                                  dup_s16x3q(add_thr2_b14)};
    int16x8x3_t prb_abs_res3[] = {dup_s16x3q(add_comm_b14),
                                  dup_s16x3q(add_comm_b14)};

    // Case1: compSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 1
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res1[0].val[j] = vrshrq_n_s16(prb_abs[0].val[j], 1);
      prb_abs_res1[1].val[j] = vrshrq_n_s16(prb_abs[1].val[j], 1);
    }

    // Case2: compSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    //  input_bits - output_bits + 1 = 2
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res2[0].val[j] =
          vrsraq_n_s16(prb_abs_res2[0].val[j], prb_abs[0].val[j], 2);
      prb_abs_res2[1].val[j] =
          vrsraq_n_s16(prb_abs_res2[1].val[j], prb_abs[1].val[j], 2);
    }

    // Case3: compSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2)
    //  input_bits - output_bits + 2 = 3
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res3[0].val[j] =
          vrsraq_n_s16(prb_abs_res3[0].val[j], prb_abs[0].val[j], 3);
      prb_abs_res3[1].val[j] =
          vrsraq_n_s16(prb_abs_res3[1].val[j], prb_abs[1].val[j], 3);
    }

    prb_abs_res1[0] =
        bsl_s16x3q(&check_thr1[0], &prb_abs_res1[0], &prb_abs_res2[0]);
    prb_abs_res1[1] =
        bsl_s16x3q(&check_thr1[1], &prb_abs_res1[1], &prb_abs_res2[1]);

    int16x8x3_t comp_samples[2];

    comp_samples[0] =
        bsl_s16x3q(&check_thr3[0], &prb_abs_res3[0], &prb_abs_res1[0]);
    comp_samples[1] =
        bsl_s16x3q(&check_thr3[1], &prb_abs_res3[1], &prb_abs_res1[1]);

    int16x8_t sat_pos = vdupq_n_s16(8191);
    int16x8_t sat_neg = vdupq_n_s16(-8191);

    // Re-apply sign and saturate to 2^(N-1) -1 =8191
    for (uint32_t j = 0; j < 3; j++) {
      regs[0].val[j] = vmaxq_s16(
          sat_neg, vminq_s16(sat_pos, vmulq_s16(comp_samples[0].val[j],
                                                prb_signs[0].val[j])));
      regs[1].val[j] = vmaxq_s16(
          sat_neg, vminq_s16(sat_pos, vmulq_s16(comp_samples[1].val[j],
                                                prb_signs[1].val[j])));
    }

    pack_14bit_and_store_int16<2, false>(regs, dst);
    dst += 2;
  }
  if (n_prb % 2 != 0) {
    // Load 12 armral_cpmplx_int16_t value (24 * int16_t)
    int16x8x3_t prb_in;
    if (scale != nullptr) {
      prb_in = load3_cmplx_and_scale((const int16_t *)src, *scale);
    } else {
      prb_in.val[0] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in.val[1] = vld1q_s16((const int16_t *)src);
      src += 4;
      prb_in.val[2] = vld1q_s16((const int16_t *)src);
    }

    // Extract the sign bit and absolute values for the PRB
    int16x8x3_t prb_signs = extract_sign_s16x3q(&prb_in);

    int16x8x3_t prb_abs = abs_s16x3q(&prb_in);

    // Find the maximum in absRe and absIm
    int max_val = max_s16x3q(&prb_abs);

    // Determine compShift, the shift to be applied to the entire PRB
    int exp = __builtin_clz(max_val) - 16 - 1;
    exp = exp < 14 ? exp : 13;
    dst->exp = exp;

    int16x8_t comp_shift_vec = vdupq_n_s16(exp);

    shift_abs_s16x3q(&prb_abs, &comp_shift_vec);

    int16x8_t thr1_b14 = vdupq_n_s16(8192);
    int16x8_t thr2_b14 = vdupq_n_s16(16384);

    // Compress each sample, absBitWidth=15
    uint16x8x3_t check_thr1 = compress_s1_c1(&prb_abs, thr1_b14);
    uint16x8x3_t check_thr3 = compress_s1_c3(&prb_abs, thr2_b14);

    // Compress - Second Step: Perform compression calculation
    int16x8x3_t prb_abs_res1;
    int16x8x3_t prb_abs_res2 = dup_s16x3q(add_thr2_b14);
    int16x8x3_t prb_abs_res3 = dup_s16x3q(add_comm_b14);

    // Case1: compSamples = prbAbs / 2^(input_bits - output_bits)
    //  input_bits - output_bits = 1
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res1.val[j] = vrshrq_n_s16(prb_abs.val[j], 1);
    }

    // Case2: compSamples = prbAbs / 2^(input_bits - output_bits + 1) +
    // 2^(output_bits - 3)
    //  input_bits - output_bits + 1 = 2
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res2.val[j] =
          vrsraq_n_s16(prb_abs_res2.val[j], prb_abs.val[j], 2);
    }

    // Case3: compSamples = prbAbs / 2^(input_bits - output_bits + 2) +
    // 2^(output_bits - 2)
    //  input_bits - output_bits + 2 = 3
    for (uint32_t j = 0; j < 3; j++) {
      prb_abs_res3.val[j] =
          vrsraq_n_s16(prb_abs_res3.val[j], prb_abs.val[j], 3);
    }

    prb_abs_res1 = bsl_s16x3q(&check_thr1, &prb_abs_res1, &prb_abs_res2);

    int16x8x3_t comp_samples;

    comp_samples = bsl_s16x3q(&check_thr3, &prb_abs_res3, &prb_abs_res1);

    int16x8_t sat_pos = vdupq_n_s16(8191);
    int16x8_t sat_neg = vdupq_n_s16(-8191);

    // Re-apply sign and saturate to 2^(N-1) -1 =8191
    for (uint32_t j = 0; j < 3; j++) {
      regs[0].val[j] = vmaxq_s16(
          sat_neg,
          vminq_s16(sat_pos, vmulq_s16(comp_samples.val[j], prb_signs.val[j])));
    }

    pack_14bit_and_store_int16<1, false>(regs, dst);
  }
  return ARMRAL_SUCCESS;
#endif
}
