/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "fft_cs16_kernel_lookup.h"

#include <stddef.h>

#define NUM_FFT_CS16_BASE_KERNELS 41

static cs16_cf32_cs16_ac_n_uun_fft_t
    *base_cs16_cf32_cs16_ac_n_uun_kernels[NUM_FFT_CS16_BASE_KERNELS] = {
        NULL,
        NULL,
        armral_fft_cs16_cf32_cs16_ac_n_uun2,
        armral_fft_cs16_cf32_cs16_ac_n_uun3,
        armral_fft_cs16_cf32_cs16_ac_n_uun4,
        armral_fft_cs16_cf32_cs16_ac_n_uun5,
        armral_fft_cs16_cf32_cs16_ac_n_uun6,
        armral_fft_cs16_cf32_cs16_ac_n_uun7,
        armral_fft_cs16_cf32_cs16_ac_n_uun8,
        armral_fft_cs16_cf32_cs16_ac_n_uun9,
        armral_fft_cs16_cf32_cs16_ac_n_uun10,
        armral_fft_cs16_cf32_cs16_ac_n_uun11,
        armral_fft_cs16_cf32_cs16_ac_n_uun12,
        armral_fft_cs16_cf32_cs16_ac_n_uun13,
        armral_fft_cs16_cf32_cs16_ac_n_uun14,
        armral_fft_cs16_cf32_cs16_ac_n_uun15,
        armral_fft_cs16_cf32_cs16_ac_n_uun16,
        armral_fft_cs16_cf32_cs16_ac_n_uun17,
        armral_fft_cs16_cf32_cs16_ac_n_uun18,
        armral_fft_cs16_cf32_cs16_ac_n_uun19,
        armral_fft_cs16_cf32_cs16_ac_n_uun20,
        armral_fft_cs16_cf32_cs16_ac_n_uun21,
        armral_fft_cs16_cf32_cs16_ac_n_uun22,
        NULL,
        armral_fft_cs16_cf32_cs16_ac_n_uun24,
        armral_fft_cs16_cf32_cs16_ac_n_uun25,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cs16_cf32_cs16_ac_n_uun32,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
};

static cs16_cf32_cf32_ac_n_uu_fft_t
    *base_cs16_cf32_cf32_ac_n_uu_kernels[NUM_FFT_CS16_BASE_KERNELS] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cs16_cf32_cf32_ac_n_uu7,
        NULL,
        armral_fft_cs16_cf32_cf32_ac_n_uu9,
        NULL,
        armral_fft_cs16_cf32_cf32_ac_n_uu11,
        NULL,
        armral_fft_cs16_cf32_cf32_ac_n_uu13,
        armral_fft_cs16_cf32_cf32_ac_n_uu14,
        armral_fft_cs16_cf32_cf32_ac_n_uu15,
        armral_fft_cs16_cf32_cf32_ac_n_uu16,
        armral_fft_cs16_cf32_cf32_ac_n_uu17,
        armral_fft_cs16_cf32_cf32_ac_n_uu18,
        armral_fft_cs16_cf32_cf32_ac_n_uu19,
        armral_fft_cs16_cf32_cf32_ac_n_uu20,
        armral_fft_cs16_cf32_cf32_ac_n_uu21,
        armral_fft_cs16_cf32_cf32_ac_n_uu22,
        NULL,
        armral_fft_cs16_cf32_cf32_ac_n_uu24,
        armral_fft_cs16_cf32_cf32_ac_n_uu25,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cs16_cf32_cf32_ac_n_uu32,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
};

static cf32_cf32_cs16_ab_t_gu_fft_t
    *base_cf32_cf32_cs16_ab_t_gu_kernels[NUM_FFT_CS16_BASE_KERNELS] = {
        NULL,
        NULL,
        armral_fft_cf32_cf32_cs16_ab_t_gu2,
        armral_fft_cf32_cf32_cs16_ab_t_gu3,
        armral_fft_cf32_cf32_cs16_ab_t_gu4,
        armral_fft_cf32_cf32_cs16_ab_t_gu5,
        armral_fft_cf32_cf32_cs16_ab_t_gu6,
        armral_fft_cf32_cf32_cs16_ab_t_gu7,
        armral_fft_cf32_cf32_cs16_ab_t_gu8,
        armral_fft_cf32_cf32_cs16_ab_t_gu9,
        armral_fft_cf32_cf32_cs16_ab_t_gu10,
        armral_fft_cf32_cf32_cs16_ab_t_gu11,
        armral_fft_cf32_cf32_cs16_ab_t_gu12,
        armral_fft_cf32_cf32_cs16_ab_t_gu13,
        armral_fft_cf32_cf32_cs16_ab_t_gu14,
        armral_fft_cf32_cf32_cs16_ab_t_gu15,
        armral_fft_cf32_cf32_cs16_ab_t_gu16,
        armral_fft_cf32_cf32_cs16_ab_t_gu17,
        armral_fft_cf32_cf32_cs16_ab_t_gu18,
        armral_fft_cf32_cf32_cs16_ab_t_gu19,
        armral_fft_cf32_cf32_cs16_ab_t_gu20,
        armral_fft_cf32_cf32_cs16_ab_t_gu21,
        armral_fft_cf32_cf32_cs16_ab_t_gu22,
        NULL,
        armral_fft_cf32_cf32_cs16_ab_t_gu24,
        armral_fft_cf32_cf32_cs16_ab_t_gu25,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cs16_ab_t_gu32,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
};

static cf32_cf32_cs16_ac_n_uu_fft_t
    *base_cf32_cf32_cs16_ac_n_uu_kernels[NUM_FFT_CS16_BASE_KERNELS] = {
        NULL,
        NULL,
        armral_fft_cf32_cf32_cs16_ac_n_uu2,
        armral_fft_cf32_cf32_cs16_ac_n_uu3,
        armral_fft_cf32_cf32_cs16_ac_n_uu4,
        armral_fft_cf32_cf32_cs16_ac_n_uu5,
        armral_fft_cf32_cf32_cs16_ac_n_uu6,
        armral_fft_cf32_cf32_cs16_ac_n_uu7,
        armral_fft_cf32_cf32_cs16_ac_n_uu8,
        armral_fft_cf32_cf32_cs16_ac_n_uu9,
        armral_fft_cf32_cf32_cs16_ac_n_uu10,
        armral_fft_cf32_cf32_cs16_ac_n_uu11,
        armral_fft_cf32_cf32_cs16_ac_n_uu12,
        armral_fft_cf32_cf32_cs16_ac_n_uu13,
        armral_fft_cf32_cf32_cs16_ac_n_uu14,
        armral_fft_cf32_cf32_cs16_ac_n_uu15,
        armral_fft_cf32_cf32_cs16_ac_n_uu16,
        armral_fft_cf32_cf32_cs16_ac_n_uu17,
        armral_fft_cf32_cf32_cs16_ac_n_uu18,
        armral_fft_cf32_cf32_cs16_ac_n_uu19,
        armral_fft_cf32_cf32_cs16_ac_n_uu20,
        armral_fft_cf32_cf32_cs16_ac_n_uu21,
        armral_fft_cf32_cf32_cs16_ac_n_uu22,
        NULL,
        armral_fft_cf32_cf32_cs16_ac_n_uu24,
        armral_fft_cf32_cf32_cs16_ac_n_uu25,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cs16_ac_n_uu32,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
};

cs16_cf32_cs16_ac_n_uun_fft_t *
lookup_ac_uun_base_kernel_cs16_cs16(int n, armral_fft_direction_t dir) {
  if (n >= NUM_FFT_CS16_BASE_KERNELS) {
    return NULL;
  }
  return base_cs16_cf32_cs16_ac_n_uun_kernels[n];
}

cs16_cf32_cf32_ac_n_uu_fft_t *
lookup_ac_uu_base_kernel_cs16_cf32(int n, armral_fft_direction_t dir) {
  if (n >= NUM_FFT_CS16_BASE_KERNELS) {
    return NULL;
  }
  return base_cs16_cf32_cf32_ac_n_uu_kernels[n];
}

cf32_cf32_cs16_ab_t_gu_fft_t *
lookup_ab_twiddle_gu_base_kernel_cf32_cs16(int n, armral_fft_direction_t dir) {
  if (n >= NUM_FFT_CS16_BASE_KERNELS) {
    return NULL;
  }
  return base_cf32_cf32_cs16_ab_t_gu_kernels[n];
}

cf32_cf32_cs16_ac_n_uu_fft_t *
lookup_ac_uu_base_kernel_cf32_cs16(int n, armral_fft_direction_t dir) {
  if (n >= NUM_FFT_CS16_BASE_KERNELS) {
    return NULL;
  }
  return base_cf32_cf32_cs16_ac_n_uu_kernels[n];
}
