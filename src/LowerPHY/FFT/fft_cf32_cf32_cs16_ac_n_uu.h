/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "fft_helper.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void(cf32_cf32_cs16_ac_n_uu_fft_t)(const armral_cmplx_f32_t *x,
                                           armral_cmplx_int16_t *y, int istride,
                                           int ostride, int howmany, float dir);

cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu2;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu3;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu4;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu5;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu6;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu7;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu8;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu9;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu10;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu11;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu12;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu13;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu14;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu15;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu16;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu17;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu18;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu19;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu20;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu21;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu22;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu24;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu25;
cf32_cf32_cs16_ac_n_uu_fft_t armral_fft_cf32_cf32_cs16_ac_n_uu32;

#ifdef __cplusplus
} // extern "C"
#endif