/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "fft_level.hpp"

namespace armral::fft {

template<typename Tx, typename Ty, typename Tw>
lev_t<Tx, Ty, Tw>::~lev_t() {
  if (twids) {
    free(twids);
    twids = NULL;
  }
}

template lev_t<armral_cmplx_f32_t, armral_cmplx_f32_t,
               armral_cmplx_f32_t>::~lev_t();
template lev_t<armral_cmplx_int16_t, armral_cmplx_int16_t,
               armral_cmplx_f32_t>::~lev_t();
template lev_t<armral_cmplx_int16_t, armral_cmplx_f32_t,
               armral_cmplx_f32_t>::~lev_t();
template lev_t<armral_cmplx_f32_t, armral_cmplx_int16_t,
               armral_cmplx_f32_t>::~lev_t();
} // namespace armral::fft