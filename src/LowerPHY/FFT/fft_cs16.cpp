/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "fft_execute.hpp"
#include "fft_plan.hpp"

extern "C" {

armral_status armral_fft_create_plan_cs16(armral_fft_plan_t **p, int n,
                                          armral_fft_direction_t dir) {
  return armral::fft::create_plan<armral_cmplx_int16_t, armral_cmplx_int16_t,
                                  armral_cmplx_f32_t>(p, n, dir, true);
}

armral_status armral_fft_execute_cs16(const armral_fft_plan_t *p,
                                      const armral_cmplx_int16_t *x,
                                      armral_cmplx_int16_t *y) {
  return armral::fft::execute<armral_cmplx_int16_t, armral_cmplx_int16_t,
                              armral_cmplx_f32_t>(p, x, y, 1, 1, 1);
}

armral_status armral_fft_destroy_plan_cs16(armral_fft_plan_t **p) {
  return armral::fft::destroy_plan(p);
}

} // extern "C"
