/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "fft_cf32_cf32_cf32_ab_t_gs.h"
#include "fft_cf32_cf32_cf32_ab_t_gu.h"
#include "fft_cf32_cf32_cf32_ac_n_gu.h"
#include "fft_cf32_cf32_cf32_ac_n_uu.h"
#include "fft_cf32_cf32_cf32_ac_n_uun.h"
#include "fft_cf32_cf32_cf32_ac_t_uu.h"

#ifdef __cplusplus
extern "C" {
#endif

cf32_cf32_cf32_ac_n_uu_fft_t *
lookup_ac_uu_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir);

cf32_cf32_cf32_ac_n_uun_fft_t *
lookup_ac_uun_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir);

cf32_cf32_cf32_ac_n_gu_fft_t *
lookup_ac_gu_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir);

cf32_cf32_cf32_ab_t_gu_fft_t *
lookup_ab_twiddle_gu_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir);

cf32_cf32_cf32_ab_t_gs_fft_t *
lookup_ab_twiddle_gs_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir);

cf32_cf32_cf32_ac_t_uu_fft_t *
lookup_ac_twiddle_uu_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir);

#ifdef __cplusplus
} // extern "C"
#endif