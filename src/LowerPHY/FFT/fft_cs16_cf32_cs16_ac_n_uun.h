/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "fft_helper.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void(cs16_cf32_cs16_ac_n_uun_fft_t)(const armral_cmplx_int16_t *x,
                                            armral_cmplx_int16_t *y,
                                            int istride, int ostride,
                                            int howmany, float dir);

cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun2;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun3;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun4;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun5;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun6;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun7;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun8;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun9;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun10;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun11;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun12;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun13;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun14;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun15;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun16;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun17;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun18;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun19;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun20;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun21;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun22;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun24;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun25;
cs16_cf32_cs16_ac_n_uun_fft_t armral_fft_cs16_cf32_cs16_ac_n_uun32;

#ifdef __cplusplus
} // extern "C"
#endif