/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "fft_cf32_kernel_lookup.h"

#include <stddef.h>

#define NUM_FFT_CF32_BASE_KERNELS 41

static cf32_cf32_cf32_ac_n_uu_fft_t
    *base_cf32_cf32_cf32_ac_n_uu_kernels[NUM_FFT_CF32_BASE_KERNELS] = {
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_uu2,
        armral_fft_cf32_cf32_cf32_ac_n_uu3,
        armral_fft_cf32_cf32_cf32_ac_n_uu4,
        armral_fft_cf32_cf32_cf32_ac_n_uu5,
        armral_fft_cf32_cf32_cf32_ac_n_uu6,
        armral_fft_cf32_cf32_cf32_ac_n_uu7,
        armral_fft_cf32_cf32_cf32_ac_n_uu8,
        armral_fft_cf32_cf32_cf32_ac_n_uu9,
        armral_fft_cf32_cf32_cf32_ac_n_uu10,
        armral_fft_cf32_cf32_cf32_ac_n_uu11,
        armral_fft_cf32_cf32_cf32_ac_n_uu12,
        armral_fft_cf32_cf32_cf32_ac_n_uu13,
        armral_fft_cf32_cf32_cf32_ac_n_uu14,
        armral_fft_cf32_cf32_cf32_ac_n_uu15,
        armral_fft_cf32_cf32_cf32_ac_n_uu16,
        armral_fft_cf32_cf32_cf32_ac_n_uu17,
        armral_fft_cf32_cf32_cf32_ac_n_uu18,
        armral_fft_cf32_cf32_cf32_ac_n_uu19,
        armral_fft_cf32_cf32_cf32_ac_n_uu20,
        armral_fft_cf32_cf32_cf32_ac_n_uu21,
        armral_fft_cf32_cf32_cf32_ac_n_uu22,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_uu24,
        armral_fft_cf32_cf32_cf32_ac_n_uu25,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_uu28,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_uu30,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_uu32,
        NULL,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_uu36,
        NULL,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_uu40,
};

static cf32_cf32_cf32_ac_n_uun_fft_t
    *base_cf32_cf32_cf32_ac_n_uun_kernels[NUM_FFT_CF32_BASE_KERNELS] = {
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_uun2,
        armral_fft_cf32_cf32_cf32_ac_n_uun3,
        armral_fft_cf32_cf32_cf32_ac_n_uun4,
        armral_fft_cf32_cf32_cf32_ac_n_uun5,
        armral_fft_cf32_cf32_cf32_ac_n_uun6,
        armral_fft_cf32_cf32_cf32_ac_n_uun7,
        armral_fft_cf32_cf32_cf32_ac_n_uun8,
        armral_fft_cf32_cf32_cf32_ac_n_uun9,
        armral_fft_cf32_cf32_cf32_ac_n_uun10,
        armral_fft_cf32_cf32_cf32_ac_n_uun11,
        armral_fft_cf32_cf32_cf32_ac_n_uun12,
        armral_fft_cf32_cf32_cf32_ac_n_uun13,
        armral_fft_cf32_cf32_cf32_ac_n_uun14,
        armral_fft_cf32_cf32_cf32_ac_n_uun15,
        armral_fft_cf32_cf32_cf32_ac_n_uun16,
        armral_fft_cf32_cf32_cf32_ac_n_uun17,
        armral_fft_cf32_cf32_cf32_ac_n_uun18,
        armral_fft_cf32_cf32_cf32_ac_n_uun19,
        armral_fft_cf32_cf32_cf32_ac_n_uun20,
        armral_fft_cf32_cf32_cf32_ac_n_uun21,
        armral_fft_cf32_cf32_cf32_ac_n_uun22,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_uun24,
        armral_fft_cf32_cf32_cf32_ac_n_uun25,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_uun32,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
};

static cf32_cf32_cf32_ac_n_gu_fft_t
    *base_cf32_cf32_cf32_ac_n_gu_kernels[NUM_FFT_CF32_BASE_KERNELS] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_gu13,
        armral_fft_cf32_cf32_cf32_ac_n_gu14,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_gu16,
        armral_fft_cf32_cf32_cf32_ac_n_gu17,
        armral_fft_cf32_cf32_cf32_ac_n_gu18,
        armral_fft_cf32_cf32_cf32_ac_n_gu19,
        armral_fft_cf32_cf32_cf32_ac_n_gu20,
        armral_fft_cf32_cf32_cf32_ac_n_gu21,
        armral_fft_cf32_cf32_cf32_ac_n_gu22,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_gu24,
        armral_fft_cf32_cf32_cf32_ac_n_gu25,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_n_gu32,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
};

static cf32_cf32_cf32_ab_t_gu_fft_t
    *base_cf32_cf32_cf32_ab_t_gu_kernels[NUM_FFT_CF32_BASE_KERNELS] = {
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ab_t_gu2,
        armral_fft_cf32_cf32_cf32_ab_t_gu3,
        armral_fft_cf32_cf32_cf32_ab_t_gu4,
        armral_fft_cf32_cf32_cf32_ab_t_gu5,
        armral_fft_cf32_cf32_cf32_ab_t_gu6,
        armral_fft_cf32_cf32_cf32_ab_t_gu7,
        armral_fft_cf32_cf32_cf32_ab_t_gu8,
        armral_fft_cf32_cf32_cf32_ab_t_gu9,
        armral_fft_cf32_cf32_cf32_ab_t_gu10,
        armral_fft_cf32_cf32_cf32_ab_t_gu11,
        armral_fft_cf32_cf32_cf32_ab_t_gu12,
        armral_fft_cf32_cf32_cf32_ab_t_gu13,
        armral_fft_cf32_cf32_cf32_ab_t_gu14,
        armral_fft_cf32_cf32_cf32_ab_t_gu15,
        armral_fft_cf32_cf32_cf32_ab_t_gu16,
        armral_fft_cf32_cf32_cf32_ab_t_gu17,
        armral_fft_cf32_cf32_cf32_ab_t_gu18,
        armral_fft_cf32_cf32_cf32_ab_t_gu19,
        armral_fft_cf32_cf32_cf32_ab_t_gu20,
        armral_fft_cf32_cf32_cf32_ab_t_gu21,
        armral_fft_cf32_cf32_cf32_ab_t_gu22,
        NULL,
        armral_fft_cf32_cf32_cf32_ab_t_gu24,
        armral_fft_cf32_cf32_cf32_ab_t_gu25,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ab_t_gu32,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
};

static cf32_cf32_cf32_ab_t_gs_fft_t
    *base_cf32_cf32_cf32_ab_t_gs_kernels[NUM_FFT_CF32_BASE_KERNELS] = {
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ab_t_gs2,
        armral_fft_cf32_cf32_cf32_ab_t_gs3,
        armral_fft_cf32_cf32_cf32_ab_t_gs4,
        armral_fft_cf32_cf32_cf32_ab_t_gs5,
        armral_fft_cf32_cf32_cf32_ab_t_gs6,
        armral_fft_cf32_cf32_cf32_ab_t_gs7,
        armral_fft_cf32_cf32_cf32_ab_t_gs8,
        armral_fft_cf32_cf32_cf32_ab_t_gs9,
        armral_fft_cf32_cf32_cf32_ab_t_gs10,
        armral_fft_cf32_cf32_cf32_ab_t_gs11,
        armral_fft_cf32_cf32_cf32_ab_t_gs12,
        armral_fft_cf32_cf32_cf32_ab_t_gs13,
        armral_fft_cf32_cf32_cf32_ab_t_gs14,
        armral_fft_cf32_cf32_cf32_ab_t_gs15,
        armral_fft_cf32_cf32_cf32_ab_t_gs16,
        armral_fft_cf32_cf32_cf32_ab_t_gs17,
        armral_fft_cf32_cf32_cf32_ab_t_gs18,
        armral_fft_cf32_cf32_cf32_ab_t_gs19,
        armral_fft_cf32_cf32_cf32_ab_t_gs20,
        armral_fft_cf32_cf32_cf32_ab_t_gs21,
        armral_fft_cf32_cf32_cf32_ab_t_gs22,
        NULL,
        armral_fft_cf32_cf32_cf32_ab_t_gs24,
        armral_fft_cf32_cf32_cf32_ab_t_gs25,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
};

static cf32_cf32_cf32_ac_t_uu_fft_t
    *base_cf32_cf32_cf32_ac_t_uu_kernels[NUM_FFT_CF32_BASE_KERNELS] = {
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_t_uu7,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_t_uu9,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_t_uu11,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_t_uu13,
        armral_fft_cf32_cf32_cf32_ac_t_uu14,
        armral_fft_cf32_cf32_cf32_ac_t_uu15,
        armral_fft_cf32_cf32_cf32_ac_t_uu16,
        armral_fft_cf32_cf32_cf32_ac_t_uu17,
        armral_fft_cf32_cf32_cf32_ac_t_uu18,
        armral_fft_cf32_cf32_cf32_ac_t_uu19,
        armral_fft_cf32_cf32_cf32_ac_t_uu20,
        armral_fft_cf32_cf32_cf32_ac_t_uu21,
        armral_fft_cf32_cf32_cf32_ac_t_uu22,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_t_uu24,
        armral_fft_cf32_cf32_cf32_ac_t_uu25,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        armral_fft_cf32_cf32_cf32_ac_t_uu32,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
        NULL,
};

cf32_cf32_cf32_ac_n_uu_fft_t *
lookup_ac_uu_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir) {
  if (n >= NUM_FFT_CF32_BASE_KERNELS) {
    return NULL;
  }
  return base_cf32_cf32_cf32_ac_n_uu_kernels[n];
}

cf32_cf32_cf32_ac_n_uun_fft_t *
lookup_ac_uun_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir) {
  if (n >= NUM_FFT_CF32_BASE_KERNELS) {
    return NULL;
  }
  return base_cf32_cf32_cf32_ac_n_uun_kernels[n];
}

cf32_cf32_cf32_ac_n_gu_fft_t *
lookup_ac_gu_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir) {
  if (n >= NUM_FFT_CF32_BASE_KERNELS) {
    return NULL;
  }
  return base_cf32_cf32_cf32_ac_n_gu_kernels[n];
}

cf32_cf32_cf32_ab_t_gu_fft_t *
lookup_ab_twiddle_gu_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir) {
  if (n >= NUM_FFT_CF32_BASE_KERNELS) {
    return NULL;
  }
  return base_cf32_cf32_cf32_ab_t_gu_kernels[n];
}

cf32_cf32_cf32_ab_t_gs_fft_t *
lookup_ab_twiddle_gs_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir) {
  if (n >= NUM_FFT_CF32_BASE_KERNELS) {
    return NULL;
  }
  return base_cf32_cf32_cf32_ab_t_gs_kernels[n];
}

cf32_cf32_cf32_ac_t_uu_fft_t *
lookup_ac_twiddle_uu_base_kernel_cf32_cf32(int n, armral_fft_direction_t dir) {
  if (n >= NUM_FFT_CF32_BASE_KERNELS) {
    return NULL;
  }
  return base_cf32_cf32_cf32_ac_t_uu_kernels[n];
}
