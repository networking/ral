/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "fft_plan.hpp"
#include "bluestein.hpp"
#include "fft_cf32_kernel_lookup.h"
#include "fft_cs16_kernel_lookup.h"
#include "rader.hpp"

#include <cmath>
#include <cstring>

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

namespace {

constexpr int len_base_kernels = 41;
// Extra kernel sizes are available when use_all_kernels = true. The 2 least
// significant bits in each element are used to distinguish between these
// sizes, and those that are always available (regardless of whether
// use_all_kernels = true or not).
// Bit 2 is set for sizes that are always available. Bit 1 is set for sizes
// that are available when use_all_kernels = true (this includes the sizes
// that are always available plus the extra sizes).
// This means we can query whether bit 1 is set when we want to include the
// extra kernels, or query whether bit 2 is set for the reduced set of kernel
// sizes. For example, size 2 is always available so both bits are set and
// base_kernels[2] = 3. However, size 28 is only available when
// use_all_kernels = true so only bit 1 is set and base_kernels[28] = 1.
constexpr int base_kernels[len_base_kernels] = {
    0x0, 0x0, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3,
    0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x3, 0x0, 0x3, 0x3, 0x0, 0x0,
    0x1, 0x0, 0x1, 0x0, 0x3, 0x0, 0x0, 0x0, 0x1, 0x0, 0x0, 0x0, 0x1};

template<typename Tw>
Tw *make_twiddles(int n1, int n2, armral_fft_direction_t dir,
                  int twid_interleave, bool want_conj_twids) {
  // The twiddle factors are generated as per the description for the WFTA
  // algorithm. This means that we can perform complex multiplications more
  // easily on the data, without having to transform it in the kernels, as this
  // would be done for every kernel call.
  int twids_len = (n1 - 1) * (n2 + twid_interleave - 1);
  if (want_conj_twids) {
    twids_len *= 2;
  }
  Tw *twids = static_cast<Tw *>(malloc(twids_len * sizeof(Tw)));
  int x = 0;
  float base_m = ((int)dir) * 2 * M_PI / (n1 * n2);
  for (int j = 1; j < n2; j += twid_interleave) {
    for (int i = 1; i < n1; ++i) {
      for (int jj = 0; jj < twid_interleave; ++jj) {
        float input = base_m * i * (j + jj);
        float a = j + jj < n2 ? cosf(input) : 0;
        float b = j + jj < n2 ? sinf(input) : 0;
        twids[x++] = Tw{a, b};
        if (want_conj_twids) {
          twids[x++] = Tw{-b, a};
        }
      }
    }
  }
  return twids;
}

inline int kernel_exists(int i, bool use_all_kernels = false) {
  if (i >= len_base_kernels) {
    return 0;
  }
  if (use_all_kernels) {
    return base_kernels[i] & 0x1;
  }
  return base_kernels[i] & 0x2;
}

template<typename Tx, typename Ty, typename Tw>
inline armral::fft::fft_ac_uu_func_t<Tx, Ty, Tw>
get_base_kernel(int n, armral_fft_direction_t dir, bool want_uun);

template<>
inline armral::fft::fft_ac_uu_func_t<armral_cmplx_f32_t, armral_cmplx_f32_t,
                                     armral_cmplx_f32_t>
get_base_kernel<armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t>(
    int n, armral_fft_direction_t dir, bool want_uun) {
  if (want_uun) {
    return lookup_ac_uun_base_kernel_cf32_cf32(n, dir);
  }
  return lookup_ac_uu_base_kernel_cf32_cf32(n, dir);
}

template<>
inline armral::fft::fft_ac_uu_func_t<armral_cmplx_int16_t, armral_cmplx_int16_t,
                                     armral_cmplx_f32_t>
get_base_kernel<armral_cmplx_int16_t, armral_cmplx_int16_t, armral_cmplx_f32_t>(
    int n, armral_fft_direction_t dir, bool want_uun) {
  assert(want_uun);
  return lookup_ac_uun_base_kernel_cs16_cs16(n, dir);
}

template<>
inline armral::fft::fft_ac_uu_func_t<armral_cmplx_int16_t, armral_cmplx_f32_t,
                                     armral_cmplx_f32_t>
get_base_kernel<armral_cmplx_int16_t, armral_cmplx_f32_t, armral_cmplx_f32_t>(
    int n, armral_fft_direction_t dir, bool want_uun) {
  return lookup_ac_uu_base_kernel_cs16_cf32(n, dir);
}

template<>
inline armral::fft::fft_ac_uu_func_t<armral_cmplx_f32_t, armral_cmplx_int16_t,
                                     armral_cmplx_f32_t>
get_base_kernel<armral_cmplx_f32_t, armral_cmplx_int16_t, armral_cmplx_f32_t>(
    int n, armral_fft_direction_t dir, bool want_uun) {
  return lookup_ac_uu_base_kernel_cf32_cs16(n, dir);
}

template<typename Tx, typename Ty, typename Tw>
inline armral::fft::fft_ac_gu_func_t<Tx, Ty, Tw>
get_ac_gu_base_kernel(int n, armral_fft_direction_t dir);

template<>
inline armral::fft::fft_ac_gu_func_t<armral_cmplx_f32_t, armral_cmplx_f32_t,
                                     armral_cmplx_f32_t>
get_ac_gu_base_kernel<armral_cmplx_f32_t, armral_cmplx_f32_t,
                      armral_cmplx_f32_t>(int n, armral_fft_direction_t dir) {
  return lookup_ac_gu_base_kernel_cf32_cf32(n, dir);
}

template<typename Tx, typename Ty, typename Tw>
inline armral::fft::fft_ab_twid_gu_func_t<Tx, Ty, Tw>
get_ab_twiddle_gu_base_kernel(int n, armral_fft_direction_t dir) {
  return nullptr;
}

template<typename Tx, typename Ty, typename Tw>
inline armral::fft::fft_ab_twid_gs_func_t<Tx, Ty, Tw>
get_ab_twiddle_gs_base_kernel(int n, armral_fft_direction_t dir) {
  return nullptr;
}

template<>
inline armral::fft::fft_ab_twid_gu_func_t<
    armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t>
get_ab_twiddle_gu_base_kernel<armral_cmplx_f32_t, armral_cmplx_f32_t,
                              armral_cmplx_f32_t>(int n,
                                                  armral_fft_direction_t dir) {
  return lookup_ab_twiddle_gu_base_kernel_cf32_cf32(n, dir);
}

template<>
inline armral::fft::fft_ab_twid_gu_func_t<
    armral_cmplx_f32_t, armral_cmplx_int16_t, armral_cmplx_f32_t>
get_ab_twiddle_gu_base_kernel<armral_cmplx_f32_t, armral_cmplx_int16_t,
                              armral_cmplx_f32_t>(int n,
                                                  armral_fft_direction_t dir) {
  return lookup_ab_twiddle_gu_base_kernel_cf32_cs16(n, dir);
}

template<>
inline armral::fft::fft_ab_twid_gs_func_t<
    armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t>
get_ab_twiddle_gs_base_kernel<armral_cmplx_f32_t, armral_cmplx_f32_t,
                              armral_cmplx_f32_t>(int n,
                                                  armral_fft_direction_t dir) {
  return lookup_ab_twiddle_gs_base_kernel_cf32_cf32(n, dir);
}

template<typename Tx, typename Ty, typename Tw>
inline armral::fft::fft_ac_twid_func_t<Tx, Ty, Tw>
get_ac_twiddle_base_kernel(int n, armral_fft_direction_t dir) {
  return nullptr;
}

template<>
inline armral::fft::fft_ac_twid_func_t<armral_cmplx_f32_t, armral_cmplx_f32_t,
                                       armral_cmplx_f32_t>
get_ac_twiddle_base_kernel<armral_cmplx_f32_t, armral_cmplx_f32_t,
                           armral_cmplx_f32_t>(int n,
                                               armral_fft_direction_t dir) {
  return lookup_ac_twiddle_uu_base_kernel_cf32_cf32(n, dir);
}

template<typename Tx, typename Ty, typename Tw>
struct kernel_selection {
  armral::fft::fft_ac_uu_func_t<Tx, Ty, Tw> base_kernel;
  armral::fft::fft_ac_gu_func_t<Tw, Tw, Tw> ac_gu_kernel;
  armral::fft::fft_ab_twid_gu_func_t<Tx, Ty, Tw> ab_twid_gu_kernel;
  armral::fft::fft_ab_twid_gs_func_t<Tx, Ty, Tw> ab_twid_gs_kernel;
  armral::fft::fft_ac_twid_func_t<Tx, Ty, Tw> ac_twid_kernel;

  explicit kernel_selection(decltype(base_kernel) base,
                            decltype(ac_gu_kernel) ac_gu,
                            decltype(ab_twid_gu_kernel) ab_twid_gu,
                            decltype(ab_twid_gs_kernel) ab_twid_gs,
                            decltype(ac_twid_kernel) ac_twid)
    : base_kernel(base), ac_gu_kernel(ac_gu), ab_twid_gu_kernel(ab_twid_gu),
      ab_twid_gs_kernel(ab_twid_gs), ac_twid_kernel(ac_twid) {}
};

template<typename Tx, typename Ty, typename Tw>
kernel_selection<Tx, Ty, Tw> get_kernels(int n1, armral_fft_direction_t dir,
                                         bool want_twids, bool want_uun) {
  auto kernel = get_base_kernel<Tx, Ty, Tw>(n1, dir, want_uun);
  auto ac_gu_kernel = (std::is_same_v<Tx, Ty> && std::is_same_v<Ty, Tw>)
                          ? get_ac_gu_base_kernel<Tw, Tw, Tw>(n1, dir)
                          : nullptr;
  auto ab_twid_gu_kernel =
      want_twids ? get_ab_twiddle_gu_base_kernel<Tx, Ty, Tw>(n1, dir) : nullptr;
  auto ab_twid_gs_kernel =
      want_twids ? get_ab_twiddle_gs_base_kernel<Tx, Ty, Tw>(n1, dir) : nullptr;
  auto ac_twid_kernel =
      want_twids ? get_ac_twiddle_base_kernel<Tx, Ty, Tw>(n1, dir) : nullptr;
  assert(kernel);
  return kernel_selection<Tx, Ty, Tw>{kernel, ac_gu_kernel, ab_twid_gu_kernel,
                                      ab_twid_gs_kernel, ac_twid_kernel};
}

struct factorize_result {
  int num_factors;
  int remainder;

  explicit factorize_result(int n) : num_factors(0), remainder(n) {}

  factorize_result(const factorize_result &other) = delete;
  factorize_result(factorize_result &&other) = default;
};

factorize_result factorize_descending_base_kernels(int n,
                                                   armral_fft_direction_t dir,
                                                   int max_nfacts, int *factors,
                                                   bool use_all_kernels) {
  factorize_result fr(n);

  // We get a performance benefit using additional kernels for Rader's cases
  // provided n is the only factor
  if (use_all_kernels && kernel_exists(n, true) != 0) {
    factors[fr.num_factors++] = n;
    fr.remainder = 1;
    return fr;
  }

  // Factorize using the reduced set of kernel lengths we have available, up to
  // a maximum number of factors
  for (int factor = 32; factor >= 2 && fr.num_factors < max_nfacts;) {
    if ((kernel_exists(factor) != 0) && fr.remainder % factor == 0) {
      factors[fr.num_factors++] = factor;
      fr.remainder /= factor;
      factor = factor < fr.remainder ? factor : fr.remainder;
    } else {
      --factor;
    }
  }
  return fr;
}

void factorize_primes(int *factors, factorize_result *fr, int max_nfacts) {
  for (int factor = 23;
       factor < sqrt(fr->remainder) + 1 && fr->num_factors < max_nfacts;) {
    if (fr->remainder % factor == 0) {
      factors[fr->num_factors++] = factor;
      fr->remainder /= factor;
      factor = factor < fr->remainder ? factor : fr->remainder;
    } else {
      factor += 2;
    }
  }

  // If there is a remainder, it must be prime (as long as we haven't gotten to
  // the maximum number of factors)
  if (fr->remainder > 1 && fr->num_factors < max_nfacts) {
    factors[fr->num_factors++] = fr->remainder;
    fr->remainder = 1;
  }
}

int factorize_descending(int n, armral_fft_direction_t dir, int max_nfacts,
                         int *factors, bool use_all_kernels) {
  factorize_result fr = factorize_descending_base_kernels(
      n, dir, max_nfacts, factors, use_all_kernels);
  if (fr.remainder == 1) {
    return fr.num_factors;
  }

  // We can't factorize using only the base kernels. See if we can use Rader's
  // algorithm for the remaining factors. For this, get prime factors of the
  // remaining value
  factorize_primes(factors, &fr, max_nfacts);
  if (fr.remainder == 1) {
    // Sort descending
    std::sort(factors, factors + fr.num_factors,
              [](int a, int b) { return a > b; });
    return fr.num_factors;
  }
  return 0;
}

template<typename Tx, typename Ty, typename Tw>
armral::fft::lev_base_t *
make_level_data(int n, int n1, int n2, int how_many, armral_fft_direction_t dir,
                bool want_twiddles, bool want_ac, bool allow_raders,
                bool use_all_kernels, bool want_uun) {
  using level_type = armral::fft::lev_t<Tx, Ty, Tw>;
  if (kernel_exists(n1, use_all_kernels)) {
    auto [kernel, ac_gu_kernel, ab_twid_gu_kernel, ab_twid_gs_kernel,
          ac_twid_kernel] =
        get_kernels<Tx, Ty, Tw>(n1, dir, want_twiddles, want_uun);
    Tw *twids = nullptr;
    if (want_twiddles) {
#ifdef ARMRAL_ARCH_SVE
      twids = make_twiddles<Tw>(n1, n2, dir, want_ac ? 1 : svcntd(), false);
#else
      twids = make_twiddles<Tw>(n1, n2, dir, 1, true);
#endif
    }
    return new level_type(n, n1, n2, how_many, dir, twids, kernel, ac_gu_kernel,
                          ab_twid_gu_kernel, ab_twid_gs_kernel, ac_twid_kernel,
                          {}, {});
  }
  if (!allow_raders) {
    return nullptr;
  }
  Tw *twids = want_twiddles ? make_twiddles<Tw>(n1, n2, dir, 1, true) : nullptr;
  auto maybe_r = armral::fft::make_rader<Tx, Ty, Tw>(n1, dir, n);
  if (maybe_r) {
    auto r = std::move(*maybe_r);
    if (r.n == 0) {
      return nullptr;
    }
    return new level_type(n, n1, n2, how_many, dir, twids, nullptr, nullptr,
                          nullptr, nullptr, nullptr, std::move(r), {});
  }
  auto maybe_bs = armral::fft::make_bluestein<Tx, Ty, Tw>(n1, dir, base_kernels,
                                                          len_base_kernels);
  if (maybe_bs) {
    auto bs = std::move(*maybe_bs);
    if (bs.n == 0) {
      return nullptr;
    }
    return new level_type(n, n1, n2, how_many, dir, twids, nullptr, nullptr,
                          nullptr, nullptr, nullptr, {}, std::move(bs));
  }
  return nullptr;
}

template<typename Tx, typename Ty, typename Tw>
int factorize(int n, armral_fft_direction_t dir, int max_levels,
              armral::fft::lev_base_t **levels, bool allow_raders,
              bool use_all_kernels, bool want_uun) {
  // Search through the set of supported factors to find a suitable
  // factorization, then use that to build the level data structures.
  int factors[max_levels];
  int num_factors =
      factorize_descending(n, dir, max_levels, factors, use_all_kernels);
  if (num_factors == 0) {
    assert(false && "Unable to factorize this FFT length");
    return 0;
  }

  int running_product = 1;

  for (int fi = 0; fi < num_factors; fi++) {
    auto n1 = factors[fi];
    int n2 = fi != 0 ? running_product : num_factors > 1 ? factors[1] : 1;
    int how_many = n / (n1 * n2);
    running_product *= n1;
    if (fi == 0) {
      if (num_factors == 1) {
        // Operating on a single level - input output and working types are as
        // specified for this function
        levels[fi] = make_level_data<Tx, Ty, Tw>(n, n1, n2, how_many, dir,
                                                 false, false, allow_raders,
                                                 use_all_kernels, want_uun);
      } else {
        // We have multiple levels, and are currently dealing with the first
        // level. Transform data to the working type from the input type
        levels[fi] =
            make_level_data<Tx, Tw, Tw>(n, n1, n2, how_many, dir, false, false,
                                        allow_raders, use_all_kernels, false);
      }
    } else if (fi == num_factors - 1) {
      // We have multiple levels and are currently dealing with the last level.
      // Transform data from the working type to the output type
      levels[fi] =
          make_level_data<Tw, Ty, Tw>(n, n1, n2, how_many, dir, true, false,
                                      allow_raders, use_all_kernels, false);
    } else {
      // We have multiple levels and are currently dealing with an intermediate
      // level (i.e. not first or last). All work is done in the working type
      levels[fi] =
          make_level_data<Tw, Tw, Tw>(n, n1, n2, how_many, dir, true, true,
                                      allow_raders, use_all_kernels, false);
    }

    if (!levels[fi]) {
      return 0;
    }
  }
  return num_factors;
}

} // anonymous namespace

namespace armral::fft {

template<typename Tx, typename Ty, typename Tw>
armral_status create_plan(armral_fft_plan_t **p, int n,
                          armral_fft_direction_t dir, bool allow_raders,
                          bool use_all_kernels, bool want_uun) {
  if (n > 42012) {
    // This length is currently unsupported due to the limit on the number of
    // allowed factors/levels, which is defined by armral_fft_plan_t::max_levels
    return ARMRAL_ARGUMENT_ERROR;
  }
  assert(p);
  // Try and find a suitable decomposition, else give up.
  armral_fft_plan_t tmp_plan = {};
  tmp_plan.n = n;
  tmp_plan.dir = dir;
  tmp_plan.num_levels = factorize<Tx, Ty, Tw>(
      n, dir, armral_fft_plan_t::max_levels, tmp_plan.levels, allow_raders,
      use_all_kernels, want_uun);
  if (tmp_plan.num_levels == 0) {
    return ARMRAL_ARGUMENT_ERROR;
  }

  // Only allocate once we're sure we actually have a plan to return.
  *p = static_cast<armral_fft_plan_t *>(malloc(sizeof(armral_fft_plan_t)));
  memcpy(*p, &tmp_plan, sizeof(armral_fft_plan_t));
  return ARMRAL_SUCCESS;
}

template armral_status
create_plan<armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t>(
    armral_fft_plan_t **p, int n, armral_fft_direction_t dir, bool allow_raders,
    bool use_all_kernels, bool want_uun);
template armral_status
create_plan<armral_cmplx_int16_t, armral_cmplx_int16_t, armral_cmplx_f32_t>(
    armral_fft_plan_t **p, int n, armral_fft_direction_t dir, bool allow_raders,
    bool use_all_kernels, bool want_uun);

armral_status destroy_plan(armral_fft_plan_t **p) {
  if (p == nullptr || *p == nullptr) {
    assert(false && "Invalid plan");
    return ARMRAL_ARGUMENT_ERROR;
  }
  for (int i = 0; i < (*p)->num_levels; ++i) {
    assert((*p)->levels[i]);
    // Call the virtual destructor of the level data
    delete (*p)->levels[i];
    (*p)->levels[i] = NULL;
  }
  free(*p);
  *p = NULL;
  return ARMRAL_SUCCESS;
}

} // namespace armral::fft
