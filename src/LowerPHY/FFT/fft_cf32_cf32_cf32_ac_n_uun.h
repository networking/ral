/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "fft_helper.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void(cf32_cf32_cf32_ac_n_uun_fft_t)(const armral_cmplx_f32_t *x,
                                            armral_cmplx_f32_t *y, int istride,
                                            int ostride, int howmany,
                                            float dir);

cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun2;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun3;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun4;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun5;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun6;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun7;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun8;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun9;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun10;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun11;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun12;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun13;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun14;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun15;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun16;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun17;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun18;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun19;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun20;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun21;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun22;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun24;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun25;
cf32_cf32_cf32_ac_n_uun_fft_t armral_fft_cf32_cf32_cf32_ac_n_uun32;

#ifdef __cplusplus
} // extern "C"
#endif