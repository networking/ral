/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "fft_cs16_cf32_cs16_ac_n_uun.h"

#include <arm_neon.h>
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun2(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v19 = vld1s_s16(&v5[istride]);
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  int16x4_t v33 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v21, 15), (int32x2_t){0, 0}));
  int16x4_t v39 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v22, 15), (int32x2_t){0, 0}));
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v33), 0);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v39), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun2(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  const int32_t *v74 = &v5[v0];
  int32_t *v95 = &v6[v2];
  const int32_t *v65 = &v5[0];
  int32_t *v86 = &v6[0];
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v74[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v65[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svint16_t v44 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v30, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v52 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v31, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v86), svreinterpret_u64_s16(v44));
  svst1w_u64(pred_full, (unsigned *)(v95), svreinterpret_u64_s16(v52));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun3(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v35 = -1.4999999999999998e+00F;
  float v38 = 8.6602540378443871e-01F;
  float v39 = -8.6602540378443871e-01F;
  int16x4_t v13 = vld1s_s16(&v5[istride]);
  int16x4_t v27 = vld1s_s16(&v5[0]);
  float32x2_t v36 = (float32x2_t){v35, v35};
  float32x2_t v40 = (float32x2_t){v38, v39};
  float32x2_t v41 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 2]);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v42 = vmul_f32(v41, v40);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v29 = vadd_f32(v21, v28);
  float32x2_t v37 = vmul_f32(v21, v36);
  float32x2_t v43 = vrev64_f32(v22);
  float32x2_t v44 = vmul_f32(v43, v42);
  float32x2_t v45 = vadd_f32(v29, v37);
  int16x4_t v50 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v29, 15), (int32x2_t){0, 0}));
  float32x2_t v46 = vadd_f32(v45, v44);
  float32x2_t v47 = vsub_f32(v45, v44);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v50), 0);
  int16x4_t v56 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v47, 15), (int32x2_t){0, 0}));
  int16x4_t v62 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v46, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v56), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v62), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun3(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v47 = -1.4999999999999998e+00F;
  float v52 = -8.6602540378443871e-01F;
  const int32_t *v91 = &v5[v0];
  int32_t *v132 = &v6[v2];
  int64_t v23 = v0 * 2;
  float v55 = v4 * v52;
  int64_t v78 = v2 * 2;
  const int32_t *v110 = &v5[0];
  svfloat32_t v114 = svdup_n_f32(v47);
  int32_t *v123 = &v6[0];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v91[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v100 = &v5[v23];
  svfloat32_t v115 = svdup_n_f32(v55);
  int32_t *v141 = &v6[v78];
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v110[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v100[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v40 = svadd_f32_x(svptrue_b32(), v30, v39);
  svfloat32_t zero57 = svdup_n_f32(0);
  svfloat32_t v57 = svcmla_f32_x(pred_full, zero57, v115, v31, 90);
  svfloat32_t v58 = svmla_f32_x(pred_full, v40, v30, v114);
  svint16_t v63 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v40, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v59 = svadd_f32_x(svptrue_b32(), v58, v57);
  svfloat32_t v60 = svsub_f32_x(svptrue_b32(), v58, v57);
  svst1w_u64(pred_full, (unsigned *)(v123), svreinterpret_u64_s16(v63));
  svint16_t v71 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v60, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v79 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v59, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v132), svreinterpret_u64_s16(v71));
  svst1w_u64(pred_full, (unsigned *)(v141), svreinterpret_u64_s16(v79));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun4(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v51 = 1.0000000000000000e+00F;
  float v52 = -1.0000000000000000e+00F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v27 = vld1s_s16(&v5[istride]);
  float32x2_t v53 = (float32x2_t){v51, v52};
  float32x2_t v54 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 2]);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  int16x4_t v33 = vld1s_s16(&v5[istride * 3]);
  float32x2_t v55 = vmul_f32(v54, v53);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v37 = vadd_f32(v21, v35);
  float32x2_t v38 = vsub_f32(v21, v35);
  float32x2_t v56 = vrev64_f32(v36);
  float32x2_t v57 = vmul_f32(v56, v55);
  int16x4_t v62 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v37, 15), (int32x2_t){0, 0}));
  int16x4_t v74 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v38, 15), (int32x2_t){0, 0}));
  float32x2_t v58 = vadd_f32(v22, v57);
  float32x2_t v59 = vsub_f32(v22, v57);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v62), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v74), 0);
  int16x4_t v68 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v59, 15), (int32x2_t){0, 0}));
  int16x4_t v80 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v58, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v68), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v80), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun4(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v68 = -1.0000000000000000e+00F;
  const int32_t *v133 = &v5[v0];
  int32_t *v165 = &v6[v2];
  int64_t v23 = v0 * 2;
  int64_t v41 = v0 * 3;
  float v71 = v4 * v68;
  int64_t v93 = v2 * 2;
  int64_t v101 = v2 * 3;
  const int32_t *v115 = &v5[0];
  int32_t *v156 = &v6[0];
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v133[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v124 = &v5[v23];
  const int32_t *v142 = &v5[v41];
  svfloat32_t v148 = svdup_n_f32(v71);
  int32_t *v174 = &v6[v93];
  int32_t *v183 = &v6[v101];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v115[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v124[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v142[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v50 = svadd_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v51 = svsub_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t zero73 = svdup_n_f32(0);
  svfloat32_t v73 = svcmla_f32_x(pred_full, zero73, v148, v49, 90);
  svfloat32_t v74 = svadd_f32_x(svptrue_b32(), v31, v73);
  svfloat32_t v75 = svsub_f32_x(svptrue_b32(), v31, v73);
  svint16_t v78 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v50, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v94 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v51, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v86 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v75, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v102 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v74, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v156), svreinterpret_u64_s16(v78));
  svst1w_u64(pred_full, (unsigned *)(v174), svreinterpret_u64_s16(v94));
  svst1w_u64(pred_full, (unsigned *)(v165), svreinterpret_u64_s16(v86));
  svst1w_u64(pred_full, (unsigned *)(v183), svreinterpret_u64_s16(v102));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun5(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v52 = -1.2500000000000000e+00F;
  float v56 = 5.5901699437494745e-01F;
  float v59 = 1.5388417685876268e+00F;
  float v60 = -1.5388417685876268e+00F;
  float v66 = 5.8778525229247325e-01F;
  float v67 = -5.8778525229247325e-01F;
  float v73 = 3.6327126400268028e-01F;
  float v74 = -3.6327126400268028e-01F;
  int16x4_t v13 = vld1s_s16(&v5[istride]);
  int16x4_t v44 = vld1s_s16(&v5[0]);
  float32x2_t v53 = (float32x2_t){v52, v52};
  float32x2_t v57 = (float32x2_t){v56, v56};
  float32x2_t v61 = (float32x2_t){v59, v60};
  float32x2_t v68 = (float32x2_t){v66, v67};
  float32x2_t v75 = (float32x2_t){v73, v74};
  float32x2_t v76 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 2]);
  float32x2_t v45 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v44)), 15);
  float32x2_t v63 = vmul_f32(v76, v61);
  float32x2_t v70 = vmul_f32(v76, v68);
  float32x2_t v77 = vmul_f32(v76, v75);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v37 = vadd_f32(v21, v35);
  float32x2_t v38 = vsub_f32(v21, v35);
  float32x2_t v39 = vadd_f32(v22, v36);
  float32x2_t v64 = vrev64_f32(v22);
  float32x2_t v78 = vrev64_f32(v36);
  float32x2_t v46 = vadd_f32(v37, v45);
  float32x2_t v54 = vmul_f32(v37, v53);
  float32x2_t v58 = vmul_f32(v38, v57);
  float32x2_t v65 = vmul_f32(v64, v63);
  float32x2_t v71 = vrev64_f32(v39);
  float32x2_t v79 = vmul_f32(v78, v77);
  float32x2_t v72 = vmul_f32(v71, v70);
  float32x2_t v80 = vadd_f32(v46, v54);
  int16x4_t v91 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v46, 15), (int32x2_t){0, 0}));
  float32x2_t v81 = vadd_f32(v80, v58);
  float32x2_t v82 = vsub_f32(v80, v58);
  float32x2_t v83 = vsub_f32(v65, v72);
  float32x2_t v84 = vadd_f32(v72, v79);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v91), 0);
  float32x2_t v85 = vadd_f32(v81, v83);
  float32x2_t v86 = vsub_f32(v81, v83);
  float32x2_t v87 = vadd_f32(v82, v84);
  float32x2_t v88 = vsub_f32(v82, v84);
  int16x4_t v97 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v86, 15), (int32x2_t){0, 0}));
  int16x4_t v103 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v88, 15), (int32x2_t){0, 0}));
  int16x4_t v109 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v87, 15), (int32x2_t){0, 0}));
  int16x4_t v115 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v85, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v97), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v103), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v109), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v115), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun5(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v68 = -1.2500000000000000e+00F;
  float v73 = 5.5901699437494745e-01F;
  float v78 = -1.5388417685876268e+00F;
  float v85 = -5.8778525229247325e-01F;
  float v92 = -3.6327126400268028e-01F;
  const int32_t *v153 = &v5[v0];
  int32_t *v215 = &v6[v2];
  int64_t v23 = v0 * 4;
  int64_t v33 = v0 * 3;
  int64_t v41 = v0 * 2;
  float v81 = v4 * v78;
  float v88 = v4 * v85;
  float v95 = v4 * v92;
  int64_t v124 = v2 * 2;
  int64_t v132 = v2 * 3;
  int64_t v140 = v2 * 4;
  const int32_t *v190 = &v5[0];
  svfloat32_t v194 = svdup_n_f32(v68);
  svfloat32_t v195 = svdup_n_f32(v73);
  int32_t *v206 = &v6[0];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v153[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v162 = &v5[v23];
  const int32_t *v171 = &v5[v33];
  const int32_t *v180 = &v5[v41];
  svfloat32_t v196 = svdup_n_f32(v81);
  svfloat32_t v197 = svdup_n_f32(v88);
  svfloat32_t v198 = svdup_n_f32(v95);
  int32_t *v224 = &v6[v124];
  int32_t *v233 = &v6[v132];
  int32_t *v242 = &v6[v140];
  svfloat32_t v60 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v190[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v162[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v171[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v180[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v50 = svadd_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v51 = svsub_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v52 = svadd_f32_x(svptrue_b32(), v31, v49);
  svfloat32_t zero83 = svdup_n_f32(0);
  svfloat32_t v83 = svcmla_f32_x(pred_full, zero83, v196, v31, 90);
  svfloat32_t v61 = svadd_f32_x(svptrue_b32(), v50, v60);
  svfloat32_t zero90 = svdup_n_f32(0);
  svfloat32_t v90 = svcmla_f32_x(pred_full, zero90, v197, v52, 90);
  svfloat32_t v98 = svmla_f32_x(pred_full, v61, v50, v194);
  svfloat32_t v101 = svsub_f32_x(svptrue_b32(), v83, v90);
  svfloat32_t v102 = svcmla_f32_x(pred_full, v90, v198, v49, 90);
  svint16_t v109 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v61, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v99 = svmla_f32_x(pred_full, v98, v51, v195);
  svfloat32_t v100 = svmls_f32_x(pred_full, v98, v51, v195);
  svst1w_u64(pred_full, (unsigned *)(v206), svreinterpret_u64_s16(v109));
  svfloat32_t v103 = svadd_f32_x(svptrue_b32(), v99, v101);
  svfloat32_t v104 = svsub_f32_x(svptrue_b32(), v99, v101);
  svfloat32_t v105 = svadd_f32_x(svptrue_b32(), v100, v102);
  svfloat32_t v106 = svsub_f32_x(svptrue_b32(), v100, v102);
  svint16_t v117 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v104, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v125 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v106, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v133 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v105, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v141 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v103, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v215), svreinterpret_u64_s16(v117));
  svst1w_u64(pred_full, (unsigned *)(v224), svreinterpret_u64_s16(v125));
  svst1w_u64(pred_full, (unsigned *)(v233), svreinterpret_u64_s16(v133));
  svst1w_u64(pred_full, (unsigned *)(v242), svreinterpret_u64_s16(v141));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun6(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v80 = -1.4999999999999998e+00F;
  float v83 = 8.6602540378443871e-01F;
  float v84 = -8.6602540378443871e-01F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v47 = vld1s_s16(&v5[istride]);
  float32x2_t v81 = (float32x2_t){v80, v80};
  float32x2_t v85 = (float32x2_t){v83, v84};
  float32x2_t v86 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v41 = vld1s_s16(&v5[istride * 4]);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v87 = vmul_f32(v86, v85);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v51 = vadd_f32(v35, v49);
  float32x2_t v52 = vsub_f32(v35, v49);
  float32x2_t v72 = vadd_f32(v36, v50);
  float32x2_t v73 = vsub_f32(v36, v50);
  float32x2_t v53 = vadd_f32(v51, v21);
  float32x2_t v61 = vmul_f32(v51, v81);
  float32x2_t v67 = vrev64_f32(v52);
  float32x2_t v74 = vadd_f32(v72, v22);
  float32x2_t v82 = vmul_f32(v72, v81);
  float32x2_t v88 = vrev64_f32(v73);
  float32x2_t v68 = vmul_f32(v67, v87);
  float32x2_t v69 = vadd_f32(v53, v61);
  float32x2_t v89 = vmul_f32(v88, v87);
  float32x2_t v90 = vadd_f32(v74, v82);
  int16x4_t v95 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v53, 15), (int32x2_t){0, 0}));
  int16x4_t v101 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v74, 15), (int32x2_t){0, 0}));
  float32x2_t v70 = vadd_f32(v69, v68);
  float32x2_t v71 = vsub_f32(v69, v68);
  float32x2_t v91 = vadd_f32(v90, v89);
  float32x2_t v92 = vsub_f32(v90, v89);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v95), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v101), 0);
  int16x4_t v107 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v71, 15), (int32x2_t){0, 0}));
  int16x4_t v113 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v92, 15), (int32x2_t){0, 0}));
  int16x4_t v119 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v70, 15), (int32x2_t){0, 0}));
  int16x4_t v125 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v91, 15), (int32x2_t){0, 0}));
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v107), 0);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v113), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v119), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v125), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun6(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v100 = -1.4999999999999998e+00F;
  float v105 = -8.6602540378443871e-01F;
  const int32_t *v214 = &v5[v0];
  int32_t *v257 = &v6[v2];
  int64_t v23 = v0 * 3;
  int64_t v33 = v0 * 2;
  int64_t v41 = v0 * 5;
  int64_t v51 = v0 * 4;
  float v108 = v4 * v105;
  int64_t v123 = v2 * 3;
  int64_t v131 = v2 * 4;
  int64_t v147 = v2 * 2;
  int64_t v155 = v2 * 5;
  const int32_t *v169 = &v5[0];
  svfloat32_t v221 = svdup_n_f32(v100);
  int32_t *v230 = &v6[0];
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v214[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v178 = &v5[v23];
  const int32_t *v187 = &v5[v33];
  const int32_t *v196 = &v5[v41];
  const int32_t *v205 = &v5[v51];
  svfloat32_t v222 = svdup_n_f32(v108);
  int32_t *v239 = &v6[v123];
  int32_t *v248 = &v6[v131];
  int32_t *v266 = &v6[v147];
  int32_t *v275 = &v6[v155];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v169[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v178[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v187[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v196[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v205[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v68 = svadd_f32_x(svptrue_b32(), v48, v66);
  svfloat32_t v69 = svsub_f32_x(svptrue_b32(), v48, v66);
  svfloat32_t v91 = svadd_f32_x(svptrue_b32(), v49, v67);
  svfloat32_t v92 = svsub_f32_x(svptrue_b32(), v49, v67);
  svfloat32_t v70 = svadd_f32_x(svptrue_b32(), v68, v30);
  svfloat32_t zero87 = svdup_n_f32(0);
  svfloat32_t v87 = svcmla_f32_x(pred_full, zero87, v222, v69, 90);
  svfloat32_t v93 = svadd_f32_x(svptrue_b32(), v91, v31);
  svfloat32_t zero110 = svdup_n_f32(0);
  svfloat32_t v110 = svcmla_f32_x(pred_full, zero110, v222, v92, 90);
  svfloat32_t v88 = svmla_f32_x(pred_full, v70, v68, v221);
  svfloat32_t v111 = svmla_f32_x(pred_full, v93, v91, v221);
  svint16_t v116 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v70, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v124 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v93, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v89 = svadd_f32_x(svptrue_b32(), v88, v87);
  svfloat32_t v90 = svsub_f32_x(svptrue_b32(), v88, v87);
  svfloat32_t v112 = svadd_f32_x(svptrue_b32(), v111, v110);
  svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v111, v110);
  svst1w_u64(pred_full, (unsigned *)(v230), svreinterpret_u64_s16(v116));
  svst1w_u64(pred_full, (unsigned *)(v239), svreinterpret_u64_s16(v124));
  svint16_t v132 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v90, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v140 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v113, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v148 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v89, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v156 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v112, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v248), svreinterpret_u64_s16(v132));
  svst1w_u64(pred_full, (unsigned *)(v257), svreinterpret_u64_s16(v140));
  svst1w_u64(pred_full, (unsigned *)(v266), svreinterpret_u64_s16(v148));
  svst1w_u64(pred_full, (unsigned *)(v275), svreinterpret_u64_s16(v156));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun7(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v73 = -1.1666666666666665e+00F;
  float v77 = 7.9015646852540022e-01F;
  float v81 = 5.5854267289647742e-02F;
  float v85 = 7.3430220123575241e-01F;
  float v88 = 4.4095855184409838e-01F;
  float v89 = -4.4095855184409838e-01F;
  float v95 = 3.4087293062393137e-01F;
  float v96 = -3.4087293062393137e-01F;
  float v102 = -5.3396936033772524e-01F;
  float v103 = 5.3396936033772524e-01F;
  float v109 = 8.7484229096165667e-01F;
  float v110 = -8.7484229096165667e-01F;
  int16x4_t v13 = vld1s_s16(&v5[istride]);
  int16x4_t v57 = vld1s_s16(&v5[0]);
  float32x2_t v74 = (float32x2_t){v73, v73};
  float32x2_t v78 = (float32x2_t){v77, v77};
  float32x2_t v82 = (float32x2_t){v81, v81};
  float32x2_t v86 = (float32x2_t){v85, v85};
  float32x2_t v90 = (float32x2_t){v88, v89};
  float32x2_t v97 = (float32x2_t){v95, v96};
  float32x2_t v104 = (float32x2_t){v102, v103};
  float32x2_t v111 = (float32x2_t){v109, v110};
  float32x2_t v112 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v41 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v47 = vld1s_s16(&v5[istride * 5]);
  float32x2_t v58 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v57)), 15);
  float32x2_t v92 = vmul_f32(v112, v90);
  float32x2_t v99 = vmul_f32(v112, v97);
  float32x2_t v106 = vmul_f32(v112, v104);
  float32x2_t v113 = vmul_f32(v112, v111);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v51 = vadd_f32(v21, v35);
  float32x2_t v60 = vsub_f32(v21, v35);
  float32x2_t v61 = vsub_f32(v35, v49);
  float32x2_t v62 = vsub_f32(v49, v21);
  float32x2_t v63 = vadd_f32(v22, v36);
  float32x2_t v65 = vsub_f32(v22, v36);
  float32x2_t v66 = vsub_f32(v36, v50);
  float32x2_t v67 = vsub_f32(v50, v22);
  float32x2_t v52 = vadd_f32(v51, v49);
  float32x2_t v64 = vadd_f32(v63, v50);
  float32x2_t v79 = vmul_f32(v60, v78);
  float32x2_t v83 = vmul_f32(v61, v82);
  float32x2_t v87 = vmul_f32(v62, v86);
  float32x2_t v100 = vrev64_f32(v65);
  float32x2_t v107 = vrev64_f32(v66);
  float32x2_t v114 = vrev64_f32(v67);
  float32x2_t v59 = vadd_f32(v52, v58);
  float32x2_t v75 = vmul_f32(v52, v74);
  float32x2_t v93 = vrev64_f32(v64);
  float32x2_t v101 = vmul_f32(v100, v99);
  float32x2_t v108 = vmul_f32(v107, v106);
  float32x2_t v115 = vmul_f32(v114, v113);
  float32x2_t v94 = vmul_f32(v93, v92);
  float32x2_t v116 = vadd_f32(v59, v75);
  int16x4_t v137 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v59, 15), (int32x2_t){0, 0}));
  float32x2_t v117 = vadd_f32(v116, v79);
  float32x2_t v119 = vsub_f32(v116, v79);
  float32x2_t v121 = vsub_f32(v116, v83);
  float32x2_t v123 = vadd_f32(v94, v101);
  float32x2_t v125 = vsub_f32(v94, v101);
  float32x2_t v127 = vsub_f32(v94, v108);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v137), 0);
  float32x2_t v118 = vadd_f32(v117, v83);
  float32x2_t v120 = vsub_f32(v119, v87);
  float32x2_t v122 = vadd_f32(v121, v87);
  float32x2_t v124 = vadd_f32(v123, v108);
  float32x2_t v126 = vsub_f32(v125, v115);
  float32x2_t v128 = vadd_f32(v127, v115);
  float32x2_t v129 = vadd_f32(v118, v124);
  float32x2_t v130 = vsub_f32(v118, v124);
  float32x2_t v131 = vadd_f32(v120, v126);
  float32x2_t v132 = vsub_f32(v120, v126);
  float32x2_t v133 = vadd_f32(v122, v128);
  float32x2_t v134 = vsub_f32(v122, v128);
  int16x4_t v143 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v130, 15), (int32x2_t){0, 0}));
  int16x4_t v149 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v132, 15), (int32x2_t){0, 0}));
  int16x4_t v155 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v133, 15), (int32x2_t){0, 0}));
  int16x4_t v161 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v134, 15), (int32x2_t){0, 0}));
  int16x4_t v167 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v131, 15), (int32x2_t){0, 0}));
  int16x4_t v173 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v129, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v143), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v149), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v155), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v161), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v167), 0);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v173), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun7(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v93 = -1.1666666666666665e+00F;
  float v98 = 7.9015646852540022e-01F;
  float v103 = 5.5854267289647742e-02F;
  float v108 = 7.3430220123575241e-01F;
  float v113 = -4.4095855184409838e-01F;
  float v120 = -3.4087293062393137e-01F;
  float v127 = 5.3396936033772524e-01F;
  float v134 = -8.7484229096165667e-01F;
  const int32_t *v221 = &v5[v0];
  int32_t *v304 = &v6[v2];
  int64_t v23 = v0 * 6;
  int64_t v33 = v0 * 4;
  int64_t v41 = v0 * 3;
  int64_t v51 = v0 * 2;
  int64_t v59 = v0 * 5;
  float v116 = v4 * v113;
  float v123 = v4 * v120;
  float v130 = v4 * v127;
  float v137 = v4 * v134;
  int64_t v176 = v2 * 2;
  int64_t v184 = v2 * 3;
  int64_t v192 = v2 * 4;
  int64_t v200 = v2 * 5;
  int64_t v208 = v2 * 6;
  const int32_t *v276 = &v5[0];
  svfloat32_t v280 = svdup_n_f32(v93);
  svfloat32_t v281 = svdup_n_f32(v98);
  svfloat32_t v282 = svdup_n_f32(v103);
  svfloat32_t v283 = svdup_n_f32(v108);
  int32_t *v295 = &v6[0];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v221[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v230 = &v5[v23];
  const int32_t *v239 = &v5[v33];
  const int32_t *v248 = &v5[v41];
  const int32_t *v257 = &v5[v51];
  const int32_t *v266 = &v5[v59];
  svfloat32_t v284 = svdup_n_f32(v116);
  svfloat32_t v285 = svdup_n_f32(v123);
  svfloat32_t v286 = svdup_n_f32(v130);
  svfloat32_t v287 = svdup_n_f32(v137);
  int32_t *v313 = &v6[v176];
  int32_t *v322 = &v6[v184];
  int32_t *v331 = &v6[v192];
  int32_t *v340 = &v6[v200];
  int32_t *v349 = &v6[v208];
  svfloat32_t v77 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v276[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v230[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v239[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v248[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v257[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v266[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v68 = svadd_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v79 = svsub_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v80 = svsub_f32_x(svptrue_b32(), v48, v66);
  svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v66, v30);
  svfloat32_t v82 = svadd_f32_x(svptrue_b32(), v31, v49);
  svfloat32_t v84 = svsub_f32_x(svptrue_b32(), v31, v49);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v49, v67);
  svfloat32_t v86 = svsub_f32_x(svptrue_b32(), v67, v31);
  svfloat32_t v69 = svadd_f32_x(svptrue_b32(), v68, v66);
  svfloat32_t v83 = svadd_f32_x(svptrue_b32(), v82, v67);
  svfloat32_t zero125 = svdup_n_f32(0);
  svfloat32_t v125 = svcmla_f32_x(pred_full, zero125, v285, v84, 90);
  svfloat32_t zero132 = svdup_n_f32(0);
  svfloat32_t v132 = svcmla_f32_x(pred_full, zero132, v286, v85, 90);
  svfloat32_t zero139 = svdup_n_f32(0);
  svfloat32_t v139 = svcmla_f32_x(pred_full, zero139, v287, v86, 90);
  svfloat32_t v78 = svadd_f32_x(svptrue_b32(), v69, v77);
  svfloat32_t zero118 = svdup_n_f32(0);
  svfloat32_t v118 = svcmla_f32_x(pred_full, zero118, v284, v83, 90);
  svfloat32_t v140 = svmla_f32_x(pred_full, v78, v69, v280);
  svfloat32_t v147 = svadd_f32_x(svptrue_b32(), v118, v125);
  svfloat32_t v149 = svsub_f32_x(svptrue_b32(), v118, v125);
  svfloat32_t v151 = svsub_f32_x(svptrue_b32(), v118, v132);
  svint16_t v161 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v78, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v141 = svmla_f32_x(pred_full, v140, v79, v281);
  svfloat32_t v143 = svmls_f32_x(pred_full, v140, v79, v281);
  svfloat32_t v145 = svmls_f32_x(pred_full, v140, v80, v282);
  svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v147, v132);
  svfloat32_t v150 = svsub_f32_x(svptrue_b32(), v149, v139);
  svfloat32_t v152 = svadd_f32_x(svptrue_b32(), v151, v139);
  svst1w_u64(pred_full, (unsigned *)(v295), svreinterpret_u64_s16(v161));
  svfloat32_t v142 = svmla_f32_x(pred_full, v141, v80, v282);
  svfloat32_t v144 = svmls_f32_x(pred_full, v143, v81, v283);
  svfloat32_t v146 = svmla_f32_x(pred_full, v145, v81, v283);
  svfloat32_t v153 = svadd_f32_x(svptrue_b32(), v142, v148);
  svfloat32_t v154 = svsub_f32_x(svptrue_b32(), v142, v148);
  svfloat32_t v155 = svadd_f32_x(svptrue_b32(), v144, v150);
  svfloat32_t v156 = svsub_f32_x(svptrue_b32(), v144, v150);
  svfloat32_t v157 = svadd_f32_x(svptrue_b32(), v146, v152);
  svfloat32_t v158 = svsub_f32_x(svptrue_b32(), v146, v152);
  svint16_t v169 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v154, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v177 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v156, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v185 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v157, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v193 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v158, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v201 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v155, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v209 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v153, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v304), svreinterpret_u64_s16(v169));
  svst1w_u64(pred_full, (unsigned *)(v313), svreinterpret_u64_s16(v177));
  svst1w_u64(pred_full, (unsigned *)(v322), svreinterpret_u64_s16(v185));
  svst1w_u64(pred_full, (unsigned *)(v331), svreinterpret_u64_s16(v193));
  svst1w_u64(pred_full, (unsigned *)(v340), svreinterpret_u64_s16(v201));
  svst1w_u64(pred_full, (unsigned *)(v349), svreinterpret_u64_s16(v209));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun8(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v96 = 1.0000000000000000e+00F;
  float v97 = -1.0000000000000000e+00F;
  float v104 = -7.0710678118654746e-01F;
  float v111 = 7.0710678118654757e-01F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v41 = vld1s_s16(&v5[istride]);
  float32x2_t v98 = (float32x2_t){v96, v97};
  float32x2_t v105 = (float32x2_t){v111, v104};
  float32x2_t v106 = (float32x2_t){v4, v4};
  float32x2_t v112 = (float32x2_t){v111, v111};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 6]);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  int16x4_t v47 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 7]);
  float32x2_t v100 = vmul_f32(v106, v98);
  float32x2_t v107 = vmul_f32(v106, v105);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v65 = vadd_f32(v21, v35);
  float32x2_t v66 = vsub_f32(v21, v35);
  float32x2_t v67 = vadd_f32(v49, v63);
  float32x2_t v68 = vsub_f32(v49, v63);
  float32x2_t v71 = vadd_f32(v50, v64);
  float32x2_t v72 = vsub_f32(v50, v64);
  float32x2_t v101 = vrev64_f32(v36);
  float32x2_t v69 = vadd_f32(v65, v67);
  float32x2_t v70 = vsub_f32(v65, v67);
  float32x2_t v90 = vrev64_f32(v68);
  float32x2_t v102 = vmul_f32(v101, v100);
  float32x2_t v108 = vrev64_f32(v71);
  float32x2_t v113 = vmul_f32(v72, v112);
  float32x2_t v91 = vmul_f32(v90, v100);
  float32x2_t v109 = vmul_f32(v108, v107);
  float32x2_t v116 = vadd_f32(v22, v113);
  float32x2_t v117 = vsub_f32(v22, v113);
  int16x4_t v126 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v69, 15), (int32x2_t){0, 0}));
  int16x4_t v150 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v70, 15), (int32x2_t){0, 0}));
  float32x2_t v114 = vadd_f32(v66, v91);
  float32x2_t v115 = vsub_f32(v66, v91);
  float32x2_t v118 = vadd_f32(v102, v109);
  float32x2_t v119 = vsub_f32(v102, v109);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v126), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v150), 0);
  float32x2_t v120 = vadd_f32(v116, v118);
  float32x2_t v121 = vsub_f32(v116, v118);
  float32x2_t v122 = vadd_f32(v117, v119);
  float32x2_t v123 = vsub_f32(v117, v119);
  int16x4_t v138 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v115, 15), (int32x2_t){0, 0}));
  int16x4_t v162 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v114, 15), (int32x2_t){0, 0}));
  int16x4_t v132 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v121, 15), (int32x2_t){0, 0}));
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v138), 0);
  int16x4_t v144 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v122, 15), (int32x2_t){0, 0}));
  int16x4_t v156 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v123, 15), (int32x2_t){0, 0}));
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v162), 0);
  int16x4_t v168 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v120, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v132), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v144), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v156), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v168), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun8(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v122 = -1.0000000000000000e+00F;
  float v129 = -7.0710678118654746e-01F;
  float v136 = 7.0710678118654757e-01F;
  const int32_t *v257 = &v5[v0];
  int32_t *v311 = &v6[v2];
  int64_t v23 = v0 * 4;
  int64_t v33 = v0 * 2;
  int64_t v41 = v0 * 6;
  int64_t v59 = v0 * 5;
  int64_t v69 = v0 * 3;
  int64_t v77 = v0 * 7;
  float v125 = v4 * v122;
  float v132 = v4 * v129;
  int64_t v167 = v2 * 2;
  int64_t v175 = v2 * 3;
  int64_t v183 = v2 * 4;
  int64_t v191 = v2 * 5;
  int64_t v199 = v2 * 6;
  int64_t v207 = v2 * 7;
  const int32_t *v221 = &v5[0];
  svfloat32_t v294 = svdup_n_f32(v136);
  int32_t *v302 = &v6[0];
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v257[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v230 = &v5[v23];
  const int32_t *v239 = &v5[v33];
  const int32_t *v248 = &v5[v41];
  const int32_t *v266 = &v5[v59];
  const int32_t *v275 = &v5[v69];
  const int32_t *v284 = &v5[v77];
  svfloat32_t v292 = svdup_n_f32(v125);
  svfloat32_t v293 = svdup_n_f32(v132);
  int32_t *v320 = &v6[v167];
  int32_t *v329 = &v6[v175];
  int32_t *v338 = &v6[v183];
  int32_t *v347 = &v6[v191];
  int32_t *v356 = &v6[v199];
  int32_t *v365 = &v6[v207];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v221[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v230[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v239[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v248[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v266[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v275[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v284[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v86 = svadd_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v87 = svsub_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v88 = svadd_f32_x(svptrue_b32(), v66, v84);
  svfloat32_t v89 = svsub_f32_x(svptrue_b32(), v66, v84);
  svfloat32_t v92 = svadd_f32_x(svptrue_b32(), v67, v85);
  svfloat32_t v93 = svsub_f32_x(svptrue_b32(), v67, v85);
  svfloat32_t zero127 = svdup_n_f32(0);
  svfloat32_t v127 = svcmla_f32_x(pred_full, zero127, v292, v49, 90);
  svfloat32_t v90 = svadd_f32_x(svptrue_b32(), v86, v88);
  svfloat32_t v91 = svsub_f32_x(svptrue_b32(), v86, v88);
  svfloat32_t zero115 = svdup_n_f32(0);
  svfloat32_t v115 = svcmla_f32_x(pred_full, zero115, v292, v89, 90);
  svfloat32_t zero134 = svdup_n_f32(0);
  svfloat32_t v134 = svcmla_f32_x(pred_full, zero134, v293, v92, 90);
  svfloat32_t v140 = svadd_f32_x(svptrue_b32(), v87, v115);
  svfloat32_t v141 = svsub_f32_x(svptrue_b32(), v87, v115);
  svfloat32_t v142 = svmla_f32_x(pred_full, v31, v93, v294);
  svfloat32_t v143 = svmls_f32_x(pred_full, v31, v93, v294);
  svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v127, v134);
  svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v127, v134);
  svint16_t v152 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v90, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v184 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v91, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v146 = svadd_f32_x(svptrue_b32(), v142, v144);
  svfloat32_t v147 = svsub_f32_x(svptrue_b32(), v142, v144);
  svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v143, v145);
  svfloat32_t v149 = svsub_f32_x(svptrue_b32(), v143, v145);
  svint16_t v168 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v141, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v200 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v140, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v302), svreinterpret_u64_s16(v152));
  svst1w_u64(pred_full, (unsigned *)(v338), svreinterpret_u64_s16(v184));
  svint16_t v160 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v147, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v176 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v148, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v192 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v149, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v208 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v146, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v320), svreinterpret_u64_s16(v168));
  svst1w_u64(pred_full, (unsigned *)(v356), svreinterpret_u64_s16(v200));
  svst1w_u64(pred_full, (unsigned *)(v311), svreinterpret_u64_s16(v160));
  svst1w_u64(pred_full, (unsigned *)(v329), svreinterpret_u64_s16(v176));
  svst1w_u64(pred_full, (unsigned *)(v347), svreinterpret_u64_s16(v192));
  svst1w_u64(pred_full, (unsigned *)(v365), svreinterpret_u64_s16(v208));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun9(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v88 = -5.0000000000000000e-01F;
  float v99 = -1.4999999999999998e+00F;
  float v102 = 8.6602540378443871e-01F;
  float v103 = -8.6602540378443871e-01F;
  float v110 = 7.6604444311897801e-01F;
  float v114 = 9.3969262078590832e-01F;
  float v118 = -1.7364817766693039e-01F;
  float v121 = 6.4278760968653925e-01F;
  float v122 = -6.4278760968653925e-01F;
  float v128 = -3.4202014332566888e-01F;
  float v129 = 3.4202014332566888e-01F;
  float v135 = 9.8480775301220802e-01F;
  float v136 = -9.8480775301220802e-01F;
  int16x4_t v13 = vld1s_s16(&v5[istride]);
  int16x4_t v72 = vld1s_s16(&v5[0]);
  float32x2_t v89 = (float32x2_t){v88, v88};
  float32x2_t v100 = (float32x2_t){v99, v99};
  float32x2_t v104 = (float32x2_t){v102, v103};
  float32x2_t v111 = (float32x2_t){v110, v110};
  float32x2_t v115 = (float32x2_t){v114, v114};
  float32x2_t v119 = (float32x2_t){v118, v118};
  float32x2_t v123 = (float32x2_t){v121, v122};
  float32x2_t v130 = (float32x2_t){v128, v129};
  float32x2_t v137 = (float32x2_t){v135, v136};
  float32x2_t v138 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v41 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v47 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 5]);
  float32x2_t v73 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v72)), 15);
  float32x2_t v106 = vmul_f32(v138, v104);
  float32x2_t v125 = vmul_f32(v138, v123);
  float32x2_t v132 = vmul_f32(v138, v130);
  float32x2_t v139 = vmul_f32(v138, v137);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v65 = vadd_f32(v21, v35);
  float32x2_t v75 = vadd_f32(v22, v36);
  float32x2_t v77 = vsub_f32(v21, v35);
  float32x2_t v78 = vsub_f32(v35, v63);
  float32x2_t v79 = vsub_f32(v63, v21);
  float32x2_t v80 = vsub_f32(v22, v36);
  float32x2_t v81 = vsub_f32(v36, v64);
  float32x2_t v82 = vsub_f32(v64, v22);
  float32x2_t v101 = vmul_f32(v49, v100);
  float32x2_t v107 = vrev64_f32(v50);
  float32x2_t v66 = vadd_f32(v65, v63);
  float32x2_t v76 = vadd_f32(v75, v64);
  float32x2_t v108 = vmul_f32(v107, v106);
  float32x2_t v112 = vmul_f32(v77, v111);
  float32x2_t v116 = vmul_f32(v78, v115);
  float32x2_t v120 = vmul_f32(v79, v119);
  float32x2_t v126 = vrev64_f32(v80);
  float32x2_t v133 = vrev64_f32(v81);
  float32x2_t v140 = vrev64_f32(v82);
  float32x2_t v67 = vadd_f32(v66, v49);
  float32x2_t v90 = vmul_f32(v66, v89);
  float32x2_t v96 = vrev64_f32(v76);
  float32x2_t v127 = vmul_f32(v126, v125);
  float32x2_t v134 = vmul_f32(v133, v132);
  float32x2_t v141 = vmul_f32(v140, v139);
  float32x2_t v74 = vadd_f32(v67, v73);
  float32x2_t v97 = vmul_f32(v96, v106);
  float32x2_t v142 = vadd_f32(v90, v90);
  float32x2_t v155 = vadd_f32(v108, v127);
  float32x2_t v157 = vsub_f32(v108, v134);
  float32x2_t v159 = vsub_f32(v108, v127);
  float32x2_t v143 = vadd_f32(v142, v90);
  float32x2_t v147 = vadd_f32(v74, v101);
  float32x2_t v156 = vadd_f32(v155, v134);
  float32x2_t v158 = vadd_f32(v157, v141);
  float32x2_t v160 = vsub_f32(v159, v141);
  int16x4_t v169 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v74, 15), (int32x2_t){0, 0}));
  float32x2_t v144 = vadd_f32(v74, v143);
  float32x2_t v148 = vadd_f32(v147, v142);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v169), 0);
  float32x2_t v145 = vadd_f32(v144, v97);
  float32x2_t v146 = vsub_f32(v144, v97);
  float32x2_t v149 = vadd_f32(v148, v112);
  float32x2_t v151 = vsub_f32(v148, v116);
  float32x2_t v153 = vsub_f32(v148, v112);
  float32x2_t v150 = vadd_f32(v149, v116);
  float32x2_t v152 = vadd_f32(v151, v120);
  float32x2_t v154 = vsub_f32(v153, v120);
  int16x4_t v187 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v146, 15), (int32x2_t){0, 0}));
  int16x4_t v205 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v145, 15), (int32x2_t){0, 0}));
  float32x2_t v161 = vadd_f32(v150, v156);
  float32x2_t v162 = vsub_f32(v150, v156);
  float32x2_t v163 = vadd_f32(v152, v158);
  float32x2_t v164 = vsub_f32(v152, v158);
  float32x2_t v165 = vadd_f32(v154, v160);
  float32x2_t v166 = vsub_f32(v154, v160);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v187), 0);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v205), 0);
  int16x4_t v175 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v162, 15), (int32x2_t){0, 0}));
  int16x4_t v181 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v163, 15), (int32x2_t){0, 0}));
  int16x4_t v193 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v166, 15), (int32x2_t){0, 0}));
  int16x4_t v199 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v165, 15), (int32x2_t){0, 0}));
  int16x4_t v211 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v164, 15), (int32x2_t){0, 0}));
  int16x4_t v217 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v161, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v175), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v181), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v193), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v199), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v211), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v217), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun9(const armral_cmplx_int16_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v112 = -5.0000000000000000e-01F;
  float v124 = -1.4999999999999998e+00F;
  float v129 = -8.6602540378443871e-01F;
  float v136 = 7.6604444311897801e-01F;
  float v141 = 9.3969262078590832e-01F;
  float v146 = -1.7364817766693039e-01F;
  float v151 = -6.4278760968653925e-01F;
  float v158 = 3.4202014332566888e-01F;
  float v165 = -9.8480775301220802e-01F;
  const int32_t *v274 = &v5[v0];
  int32_t *v377 = &v6[v2];
  int64_t v23 = v0 * 8;
  int64_t v33 = v0 * 7;
  int64_t v41 = v0 * 2;
  int64_t v51 = v0 * 3;
  int64_t v59 = v0 * 6;
  int64_t v69 = v0 * 4;
  int64_t v77 = v0 * 5;
  float v132 = v4 * v129;
  float v154 = v4 * v151;
  float v161 = v4 * v158;
  float v168 = v4 * v165;
  int64_t v213 = v2 * 2;
  int64_t v221 = v2 * 3;
  int64_t v229 = v2 * 4;
  int64_t v237 = v2 * 5;
  int64_t v245 = v2 * 6;
  int64_t v253 = v2 * 7;
  int64_t v261 = v2 * 8;
  const int32_t *v347 = &v5[0];
  svfloat32_t v351 = svdup_n_f32(v112);
  svfloat32_t v353 = svdup_n_f32(v124);
  svfloat32_t v355 = svdup_n_f32(v136);
  svfloat32_t v356 = svdup_n_f32(v141);
  svfloat32_t v357 = svdup_n_f32(v146);
  int32_t *v368 = &v6[0];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v274[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v283 = &v5[v23];
  const int32_t *v292 = &v5[v33];
  const int32_t *v301 = &v5[v41];
  const int32_t *v310 = &v5[v51];
  const int32_t *v319 = &v5[v59];
  const int32_t *v328 = &v5[v69];
  const int32_t *v337 = &v5[v77];
  svfloat32_t v354 = svdup_n_f32(v132);
  svfloat32_t v358 = svdup_n_f32(v154);
  svfloat32_t v359 = svdup_n_f32(v161);
  svfloat32_t v360 = svdup_n_f32(v168);
  int32_t *v386 = &v6[v213];
  int32_t *v395 = &v6[v221];
  int32_t *v404 = &v6[v229];
  int32_t *v413 = &v6[v237];
  int32_t *v422 = &v6[v245];
  int32_t *v431 = &v6[v253];
  int32_t *v440 = &v6[v261];
  svfloat32_t v96 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v347[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v283[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v292[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v301[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v310[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v319[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v328[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v337[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v86 = svadd_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v98 = svadd_f32_x(svptrue_b32(), v31, v49);
  svfloat32_t v100 = svsub_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v101 = svsub_f32_x(svptrue_b32(), v48, v84);
  svfloat32_t v102 = svsub_f32_x(svptrue_b32(), v84, v30);
  svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v31, v49);
  svfloat32_t v104 = svsub_f32_x(svptrue_b32(), v49, v85);
  svfloat32_t v105 = svsub_f32_x(svptrue_b32(), v85, v31);
  svfloat32_t zero134 = svdup_n_f32(0);
  svfloat32_t v134 = svcmla_f32_x(pred_full, zero134, v354, v67, 90);
  svfloat32_t v87 = svadd_f32_x(svptrue_b32(), v86, v84);
  svfloat32_t v99 = svadd_f32_x(svptrue_b32(), v98, v85);
  svfloat32_t zero156 = svdup_n_f32(0);
  svfloat32_t v156 = svcmla_f32_x(pred_full, zero156, v358, v103, 90);
  svfloat32_t zero163 = svdup_n_f32(0);
  svfloat32_t v163 = svcmla_f32_x(pred_full, zero163, v359, v104, 90);
  svfloat32_t zero170 = svdup_n_f32(0);
  svfloat32_t v170 = svcmla_f32_x(pred_full, zero170, v360, v105, 90);
  svfloat32_t v88 = svadd_f32_x(svptrue_b32(), v87, v66);
  svfloat32_t v115 = svmul_f32_x(svptrue_b32(), v87, v351);
  svfloat32_t zero122 = svdup_n_f32(0);
  svfloat32_t v122 = svcmla_f32_x(pred_full, zero122, v354, v99, 90);
  svfloat32_t v184 = svadd_f32_x(svptrue_b32(), v134, v156);
  svfloat32_t v186 = svsub_f32_x(svptrue_b32(), v134, v163);
  svfloat32_t v188 = svsub_f32_x(svptrue_b32(), v134, v156);
  svfloat32_t v97 = svadd_f32_x(svptrue_b32(), v88, v96);
  svfloat32_t v171 = svadd_f32_x(svptrue_b32(), v115, v115);
  svfloat32_t v185 = svadd_f32_x(svptrue_b32(), v184, v163);
  svfloat32_t v187 = svadd_f32_x(svptrue_b32(), v186, v170);
  svfloat32_t v189 = svsub_f32_x(svptrue_b32(), v188, v170);
  svfloat32_t v172 = svmla_f32_x(pred_full, v171, v87, v351);
  svfloat32_t v176 = svmla_f32_x(pred_full, v97, v66, v353);
  svint16_t v198 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v97, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v173 = svadd_f32_x(svptrue_b32(), v97, v172);
  svfloat32_t v177 = svadd_f32_x(svptrue_b32(), v176, v171);
  svst1w_u64(pred_full, (unsigned *)(v368), svreinterpret_u64_s16(v198));
  svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v173, v122);
  svfloat32_t v175 = svsub_f32_x(svptrue_b32(), v173, v122);
  svfloat32_t v178 = svmla_f32_x(pred_full, v177, v100, v355);
  svfloat32_t v180 = svmls_f32_x(pred_full, v177, v101, v356);
  svfloat32_t v182 = svmls_f32_x(pred_full, v177, v100, v355);
  svfloat32_t v179 = svmla_f32_x(pred_full, v178, v101, v356);
  svfloat32_t v181 = svmla_f32_x(pred_full, v180, v102, v357);
  svfloat32_t v183 = svmls_f32_x(pred_full, v182, v102, v357);
  svint16_t v222 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v175, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v246 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v174, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v190 = svadd_f32_x(svptrue_b32(), v179, v185);
  svfloat32_t v191 = svsub_f32_x(svptrue_b32(), v179, v185);
  svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v181, v187);
  svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v181, v187);
  svfloat32_t v194 = svadd_f32_x(svptrue_b32(), v183, v189);
  svfloat32_t v195 = svsub_f32_x(svptrue_b32(), v183, v189);
  svst1w_u64(pred_full, (unsigned *)(v395), svreinterpret_u64_s16(v222));
  svst1w_u64(pred_full, (unsigned *)(v422), svreinterpret_u64_s16(v246));
  svint16_t v206 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v191, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v214 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v192, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v230 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v195, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v238 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v194, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v254 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v193, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v262 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v190, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v377), svreinterpret_u64_s16(v206));
  svst1w_u64(pred_full, (unsigned *)(v386), svreinterpret_u64_s16(v214));
  svst1w_u64(pred_full, (unsigned *)(v404), svreinterpret_u64_s16(v230));
  svst1w_u64(pred_full, (unsigned *)(v413), svreinterpret_u64_s16(v238));
  svst1w_u64(pred_full, (unsigned *)(v431), svreinterpret_u64_s16(v254));
  svst1w_u64(pred_full, (unsigned *)(v440), svreinterpret_u64_s16(v262));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun10(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v142 = -1.2500000000000000e+00F;
  float v146 = 5.5901699437494745e-01F;
  float v149 = 1.5388417685876268e+00F;
  float v150 = -1.5388417685876268e+00F;
  float v156 = 5.8778525229247325e-01F;
  float v157 = -5.8778525229247325e-01F;
  float v163 = 3.6327126400268028e-01F;
  float v164 = -3.6327126400268028e-01F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v61 = vld1s_s16(&v5[istride]);
  float32x2_t v143 = (float32x2_t){v142, v142};
  float32x2_t v147 = (float32x2_t){v146, v146};
  float32x2_t v151 = (float32x2_t){v149, v150};
  float32x2_t v158 = (float32x2_t){v156, v157};
  float32x2_t v165 = (float32x2_t){v163, v164};
  float32x2_t v166 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v41 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v47 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 6]);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  int16x4_t v69 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v75 = vld1s_s16(&v5[istride * 3]);
  float32x2_t v153 = vmul_f32(v166, v151);
  float32x2_t v160 = vmul_f32(v166, v158);
  float32x2_t v167 = vmul_f32(v166, v165);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  float32x2_t v76 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v75)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v77 = vadd_f32(v70, v76);
  float32x2_t v78 = vsub_f32(v70, v76);
  float32x2_t v79 = vadd_f32(v35, v77);
  float32x2_t v80 = vsub_f32(v35, v77);
  float32x2_t v81 = vadd_f32(v63, v49);
  float32x2_t v82 = vsub_f32(v63, v49);
  float32x2_t v129 = vadd_f32(v36, v78);
  float32x2_t v130 = vsub_f32(v36, v78);
  float32x2_t v131 = vadd_f32(v64, v50);
  float32x2_t v132 = vsub_f32(v64, v50);
  float32x2_t v83 = vadd_f32(v79, v81);
  float32x2_t v84 = vsub_f32(v79, v81);
  float32x2_t v85 = vadd_f32(v80, v82);
  float32x2_t v104 = vrev64_f32(v80);
  float32x2_t v118 = vrev64_f32(v82);
  float32x2_t v133 = vadd_f32(v129, v131);
  float32x2_t v134 = vsub_f32(v129, v131);
  float32x2_t v135 = vadd_f32(v130, v132);
  float32x2_t v154 = vrev64_f32(v130);
  float32x2_t v168 = vrev64_f32(v132);
  float32x2_t v86 = vadd_f32(v83, v21);
  float32x2_t v94 = vmul_f32(v83, v143);
  float32x2_t v98 = vmul_f32(v84, v147);
  float32x2_t v105 = vmul_f32(v104, v153);
  float32x2_t v111 = vrev64_f32(v85);
  float32x2_t v119 = vmul_f32(v118, v167);
  float32x2_t v136 = vadd_f32(v133, v22);
  float32x2_t v144 = vmul_f32(v133, v143);
  float32x2_t v148 = vmul_f32(v134, v147);
  float32x2_t v155 = vmul_f32(v154, v153);
  float32x2_t v161 = vrev64_f32(v135);
  float32x2_t v169 = vmul_f32(v168, v167);
  float32x2_t v112 = vmul_f32(v111, v160);
  float32x2_t v120 = vadd_f32(v86, v94);
  float32x2_t v162 = vmul_f32(v161, v160);
  float32x2_t v170 = vadd_f32(v136, v144);
  int16x4_t v181 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v86, 15), (int32x2_t){0, 0}));
  int16x4_t v187 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v136, 15), (int32x2_t){0, 0}));
  float32x2_t v121 = vadd_f32(v120, v98);
  float32x2_t v122 = vsub_f32(v120, v98);
  float32x2_t v123 = vsub_f32(v105, v112);
  float32x2_t v124 = vadd_f32(v112, v119);
  float32x2_t v171 = vadd_f32(v170, v148);
  float32x2_t v172 = vsub_f32(v170, v148);
  float32x2_t v173 = vsub_f32(v155, v162);
  float32x2_t v174 = vadd_f32(v162, v169);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v181), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v187), 0);
  float32x2_t v125 = vadd_f32(v121, v123);
  float32x2_t v126 = vsub_f32(v121, v123);
  float32x2_t v127 = vadd_f32(v122, v124);
  float32x2_t v128 = vsub_f32(v122, v124);
  float32x2_t v175 = vadd_f32(v171, v173);
  float32x2_t v176 = vsub_f32(v171, v173);
  float32x2_t v177 = vadd_f32(v172, v174);
  float32x2_t v178 = vsub_f32(v172, v174);
  int16x4_t v193 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v126, 15), (int32x2_t){0, 0}));
  int16x4_t v199 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v176, 15), (int32x2_t){0, 0}));
  int16x4_t v205 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v128, 15), (int32x2_t){0, 0}));
  int16x4_t v211 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v178, 15), (int32x2_t){0, 0}));
  int16x4_t v217 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v127, 15), (int32x2_t){0, 0}));
  int16x4_t v223 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v177, 15), (int32x2_t){0, 0}));
  int16x4_t v229 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v125, 15), (int32x2_t){0, 0}));
  int16x4_t v235 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v175, 15), (int32x2_t){0, 0}));
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v193), 0);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v199), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v205), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v211), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v217), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v223), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v229), 0);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v235), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun10(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v171 = -1.2500000000000000e+00F;
  float v176 = 5.5901699437494745e-01F;
  float v181 = -1.5388417685876268e+00F;
  float v188 = -5.8778525229247325e-01F;
  float v195 = -3.6327126400268028e-01F;
  const int32_t *v360 = &v5[v0];
  int32_t *v427 = &v6[v2];
  int64_t v23 = v0 * 5;
  int64_t v33 = v0 * 2;
  int64_t v41 = v0 * 7;
  int64_t v51 = v0 * 4;
  int64_t v59 = v0 * 9;
  int64_t v69 = v0 * 6;
  int64_t v87 = v0 * 8;
  int64_t v95 = v0 * 3;
  float v184 = v4 * v181;
  float v191 = v4 * v188;
  float v198 = v4 * v195;
  int64_t v219 = v2 * 5;
  int64_t v227 = v2 * 6;
  int64_t v243 = v2 * 2;
  int64_t v251 = v2 * 7;
  int64_t v259 = v2 * 8;
  int64_t v267 = v2 * 3;
  int64_t v275 = v2 * 4;
  int64_t v283 = v2 * 9;
  const int32_t *v297 = &v5[0];
  svfloat32_t v388 = svdup_n_f32(v171);
  svfloat32_t v389 = svdup_n_f32(v176);
  int32_t *v400 = &v6[0];
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v360[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v306 = &v5[v23];
  const int32_t *v315 = &v5[v33];
  const int32_t *v324 = &v5[v41];
  const int32_t *v333 = &v5[v51];
  const int32_t *v342 = &v5[v59];
  const int32_t *v351 = &v5[v69];
  const int32_t *v369 = &v5[v87];
  const int32_t *v378 = &v5[v95];
  svfloat32_t v390 = svdup_n_f32(v184);
  svfloat32_t v391 = svdup_n_f32(v191);
  svfloat32_t v392 = svdup_n_f32(v198);
  int32_t *v409 = &v6[v219];
  int32_t *v418 = &v6[v227];
  int32_t *v436 = &v6[v243];
  int32_t *v445 = &v6[v251];
  int32_t *v454 = &v6[v259];
  int32_t *v463 = &v6[v267];
  int32_t *v472 = &v6[v275];
  int32_t *v481 = &v6[v283];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v297[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v306[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v315[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v324[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v333[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v342[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v351[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v369[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v101 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v378[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v104 = svadd_f32_x(svptrue_b32(), v48, v102);
  svfloat32_t v105 = svsub_f32_x(svptrue_b32(), v48, v102);
  svfloat32_t v106 = svadd_f32_x(svptrue_b32(), v84, v66);
  svfloat32_t v107 = svsub_f32_x(svptrue_b32(), v84, v66);
  svfloat32_t v157 = svadd_f32_x(svptrue_b32(), v49, v103);
  svfloat32_t v158 = svsub_f32_x(svptrue_b32(), v49, v103);
  svfloat32_t v159 = svadd_f32_x(svptrue_b32(), v85, v67);
  svfloat32_t v160 = svsub_f32_x(svptrue_b32(), v85, v67);
  svfloat32_t v108 = svadd_f32_x(svptrue_b32(), v104, v106);
  svfloat32_t v109 = svsub_f32_x(svptrue_b32(), v104, v106);
  svfloat32_t v110 = svadd_f32_x(svptrue_b32(), v105, v107);
  svfloat32_t zero133 = svdup_n_f32(0);
  svfloat32_t v133 = svcmla_f32_x(pred_full, zero133, v390, v105, 90);
  svfloat32_t v161 = svadd_f32_x(svptrue_b32(), v157, v159);
  svfloat32_t v162 = svsub_f32_x(svptrue_b32(), v157, v159);
  svfloat32_t v163 = svadd_f32_x(svptrue_b32(), v158, v160);
  svfloat32_t zero186 = svdup_n_f32(0);
  svfloat32_t v186 = svcmla_f32_x(pred_full, zero186, v390, v158, 90);
  svfloat32_t v111 = svadd_f32_x(svptrue_b32(), v108, v30);
  svfloat32_t zero140 = svdup_n_f32(0);
  svfloat32_t v140 = svcmla_f32_x(pred_full, zero140, v391, v110, 90);
  svfloat32_t v164 = svadd_f32_x(svptrue_b32(), v161, v31);
  svfloat32_t zero193 = svdup_n_f32(0);
  svfloat32_t v193 = svcmla_f32_x(pred_full, zero193, v391, v163, 90);
  svfloat32_t v148 = svmla_f32_x(pred_full, v111, v108, v388);
  svfloat32_t v151 = svsub_f32_x(svptrue_b32(), v133, v140);
  svfloat32_t v152 = svcmla_f32_x(pred_full, v140, v392, v107, 90);
  svfloat32_t v201 = svmla_f32_x(pred_full, v164, v161, v388);
  svfloat32_t v204 = svsub_f32_x(svptrue_b32(), v186, v193);
  svfloat32_t v205 = svcmla_f32_x(pred_full, v193, v392, v160, 90);
  svint16_t v212 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v111, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v220 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v164, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v149 = svmla_f32_x(pred_full, v148, v109, v389);
  svfloat32_t v150 = svmls_f32_x(pred_full, v148, v109, v389);
  svfloat32_t v202 = svmla_f32_x(pred_full, v201, v162, v389);
  svfloat32_t v203 = svmls_f32_x(pred_full, v201, v162, v389);
  svst1w_u64(pred_full, (unsigned *)(v400), svreinterpret_u64_s16(v212));
  svst1w_u64(pred_full, (unsigned *)(v409), svreinterpret_u64_s16(v220));
  svfloat32_t v153 = svadd_f32_x(svptrue_b32(), v149, v151);
  svfloat32_t v154 = svsub_f32_x(svptrue_b32(), v149, v151);
  svfloat32_t v155 = svadd_f32_x(svptrue_b32(), v150, v152);
  svfloat32_t v156 = svsub_f32_x(svptrue_b32(), v150, v152);
  svfloat32_t v206 = svadd_f32_x(svptrue_b32(), v202, v204);
  svfloat32_t v207 = svsub_f32_x(svptrue_b32(), v202, v204);
  svfloat32_t v208 = svadd_f32_x(svptrue_b32(), v203, v205);
  svfloat32_t v209 = svsub_f32_x(svptrue_b32(), v203, v205);
  svint16_t v228 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v154, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v236 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v207, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v244 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v156, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v252 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v209, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v260 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v155, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v268 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v208, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v276 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v153, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v284 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v206, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v418), svreinterpret_u64_s16(v228));
  svst1w_u64(pred_full, (unsigned *)(v427), svreinterpret_u64_s16(v236));
  svst1w_u64(pred_full, (unsigned *)(v436), svreinterpret_u64_s16(v244));
  svst1w_u64(pred_full, (unsigned *)(v445), svreinterpret_u64_s16(v252));
  svst1w_u64(pred_full, (unsigned *)(v454), svreinterpret_u64_s16(v260));
  svst1w_u64(pred_full, (unsigned *)(v463), svreinterpret_u64_s16(v268));
  svst1w_u64(pred_full, (unsigned *)(v472), svreinterpret_u64_s16(v276));
  svst1w_u64(pred_full, (unsigned *)(v481), svreinterpret_u64_s16(v284));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun11(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v117 = 1.1000000000000001e+00F;
  float v120 = 3.3166247903554003e-01F;
  float v121 = -3.3166247903554003e-01F;
  float v128 = 5.1541501300188641e-01F;
  float v132 = 9.4125353283118118e-01F;
  float v136 = 1.4143537075597825e+00F;
  float v140 = 8.5949297361449750e-01F;
  float v144 = 4.2314838273285138e-02F;
  float v148 = 3.8639279888589606e-01F;
  float v152 = 5.1254589567200015e-01F;
  float v156 = 1.0702757469471715e+00F;
  float v160 = 5.5486073394528512e-01F;
  float v163 = 1.2412944743900585e+00F;
  float v164 = -1.2412944743900585e+00F;
  float v170 = 2.0897833842005756e-01F;
  float v171 = -2.0897833842005756e-01F;
  float v177 = 3.7415717312460811e-01F;
  float v178 = -3.7415717312460811e-01F;
  float v184 = 4.9929922194110327e-02F;
  float v185 = -4.9929922194110327e-02F;
  float v191 = 6.5815896284539266e-01F;
  float v192 = -6.5815896284539266e-01F;
  float v198 = 6.3306543373877577e-01F;
  float v199 = -6.3306543373877577e-01F;
  float v205 = 1.0822460581641109e+00F;
  float v206 = -1.0822460581641109e+00F;
  float v212 = 8.1720737907134022e-01F;
  float v213 = -8.1720737907134022e-01F;
  float v219 = 4.2408709531871824e-01F;
  float v220 = -4.2408709531871824e-01F;
  int16x4_t v13 = vld1s_s16(&v5[istride]);
  int16x4_t v89 = vld1s_s16(&v5[0]);
  float32x2_t v118 = (float32x2_t){v117, v117};
  float32x2_t v122 = (float32x2_t){v120, v121};
  float32x2_t v129 = (float32x2_t){v128, v128};
  float32x2_t v133 = (float32x2_t){v132, v132};
  float32x2_t v137 = (float32x2_t){v136, v136};
  float32x2_t v141 = (float32x2_t){v140, v140};
  float32x2_t v145 = (float32x2_t){v144, v144};
  float32x2_t v149 = (float32x2_t){v148, v148};
  float32x2_t v153 = (float32x2_t){v152, v152};
  float32x2_t v157 = (float32x2_t){v156, v156};
  float32x2_t v161 = (float32x2_t){v160, v160};
  float32x2_t v165 = (float32x2_t){v163, v164};
  float32x2_t v172 = (float32x2_t){v170, v171};
  float32x2_t v179 = (float32x2_t){v177, v178};
  float32x2_t v186 = (float32x2_t){v184, v185};
  float32x2_t v193 = (float32x2_t){v191, v192};
  float32x2_t v200 = (float32x2_t){v198, v199};
  float32x2_t v207 = (float32x2_t){v205, v206};
  float32x2_t v214 = (float32x2_t){v212, v213};
  float32x2_t v221 = (float32x2_t){v219, v220};
  float32x2_t v222 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v26 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v32 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v39 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v45 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v52 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v58 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v65 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v71 = vld1s_s16(&v5[istride * 6]);
  float32x2_t v90 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v89)), 15);
  float32x2_t v124 = vmul_f32(v222, v122);
  float32x2_t v167 = vmul_f32(v222, v165);
  float32x2_t v174 = vmul_f32(v222, v172);
  float32x2_t v181 = vmul_f32(v222, v179);
  float32x2_t v188 = vmul_f32(v222, v186);
  float32x2_t v195 = vmul_f32(v222, v193);
  float32x2_t v202 = vmul_f32(v222, v200);
  float32x2_t v209 = vmul_f32(v222, v207);
  float32x2_t v216 = vmul_f32(v222, v214);
  float32x2_t v223 = vmul_f32(v222, v221);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v27 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v26)), 15);
  float32x2_t v33 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v32)), 15);
  float32x2_t v40 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v39)), 15);
  float32x2_t v46 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v45)), 15);
  float32x2_t v53 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v52)), 15);
  float32x2_t v59 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v58)), 15);
  float32x2_t v66 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v65)), 15);
  float32x2_t v72 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v71)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v34 = vadd_f32(v27, v33);
  float32x2_t v47 = vadd_f32(v40, v46);
  float32x2_t v60 = vadd_f32(v53, v59);
  float32x2_t v73 = vadd_f32(v66, v72);
  float32x2_t v74 = vsub_f32(v14, v20);
  float32x2_t v75 = vsub_f32(v27, v33);
  float32x2_t v76 = vsub_f32(v40, v46);
  float32x2_t v77 = vsub_f32(v53, v59);
  float32x2_t v78 = vsub_f32(v66, v72);
  float32x2_t v79 = vadd_f32(v21, v34);
  float32x2_t v80 = vadd_f32(v47, v73);
  float32x2_t v82 = vsub_f32(v75, v76);
  float32x2_t v83 = vadd_f32(v74, v78);
  float32x2_t v94 = vsub_f32(v34, v60);
  float32x2_t v95 = vsub_f32(v21, v60);
  float32x2_t v96 = vsub_f32(v34, v21);
  float32x2_t v97 = vsub_f32(v73, v60);
  float32x2_t v98 = vsub_f32(v47, v60);
  float32x2_t v99 = vsub_f32(v73, v47);
  float32x2_t v100 = vsub_f32(v34, v73);
  float32x2_t v101 = vsub_f32(v21, v47);
  float32x2_t v103 = vadd_f32(v75, v77);
  float32x2_t v104 = vsub_f32(v74, v77);
  float32x2_t v105 = vadd_f32(v74, v75);
  float32x2_t v106 = vsub_f32(v77, v78);
  float32x2_t v107 = vsub_f32(v76, v77);
  float32x2_t v108 = vsub_f32(v76, v78);
  float32x2_t v109 = vadd_f32(v75, v78);
  float32x2_t v110 = vsub_f32(v74, v76);
  float32x2_t v81 = vadd_f32(v60, v79);
  float32x2_t v92 = vsub_f32(v82, v83);
  float32x2_t v102 = vsub_f32(v80, v79);
  float32x2_t v111 = vadd_f32(v82, v83);
  float32x2_t v130 = vmul_f32(v94, v129);
  float32x2_t v134 = vmul_f32(v95, v133);
  float32x2_t v138 = vmul_f32(v96, v137);
  float32x2_t v142 = vmul_f32(v97, v141);
  float32x2_t v146 = vmul_f32(v98, v145);
  float32x2_t v150 = vmul_f32(v99, v149);
  float32x2_t v154 = vmul_f32(v100, v153);
  float32x2_t v158 = vmul_f32(v101, v157);
  float32x2_t v168 = vrev64_f32(v103);
  float32x2_t v175 = vrev64_f32(v104);
  float32x2_t v182 = vrev64_f32(v105);
  float32x2_t v189 = vrev64_f32(v106);
  float32x2_t v196 = vrev64_f32(v107);
  float32x2_t v203 = vrev64_f32(v108);
  float32x2_t v210 = vrev64_f32(v109);
  float32x2_t v217 = vrev64_f32(v110);
  float32x2_t v84 = vadd_f32(v81, v80);
  float32x2_t v93 = vsub_f32(v92, v77);
  float32x2_t v162 = vmul_f32(v102, v161);
  float32x2_t v169 = vmul_f32(v168, v167);
  float32x2_t v176 = vmul_f32(v175, v174);
  float32x2_t v183 = vmul_f32(v182, v181);
  float32x2_t v190 = vmul_f32(v189, v188);
  float32x2_t v197 = vmul_f32(v196, v195);
  float32x2_t v204 = vmul_f32(v203, v202);
  float32x2_t v211 = vmul_f32(v210, v209);
  float32x2_t v218 = vmul_f32(v217, v216);
  float32x2_t v224 = vrev64_f32(v111);
  float32x2_t v227 = vadd_f32(v130, v134);
  float32x2_t v228 = vadd_f32(v134, v138);
  float32x2_t v229 = vsub_f32(v130, v138);
  float32x2_t v230 = vadd_f32(v142, v146);
  float32x2_t v231 = vadd_f32(v146, v150);
  float32x2_t v232 = vsub_f32(v142, v150);
  float32x2_t v91 = vadd_f32(v90, v84);
  float32x2_t v119 = vmul_f32(v84, v118);
  float32x2_t v125 = vrev64_f32(v93);
  float32x2_t v225 = vmul_f32(v224, v223);
  float32x2_t v233 = vadd_f32(v158, v162);
  float32x2_t v234 = vadd_f32(v154, v162);
  float32x2_t v235 = vadd_f32(v176, v183);
  float32x2_t v236 = vsub_f32(v169, v183);
  float32x2_t v237 = vadd_f32(v197, v204);
  float32x2_t v238 = vsub_f32(v190, v204);
  float32x2_t v126 = vmul_f32(v125, v124);
  float32x2_t v226 = vsub_f32(v91, v119);
  float32x2_t v239 = vadd_f32(v218, v225);
  float32x2_t v240 = vsub_f32(v211, v225);
  float32x2_t v241 = vadd_f32(v231, v233);
  float32x2_t v259 = vadd_f32(v235, v236);
  int16x4_t v275 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v91, 15), (int32x2_t){0, 0}));
  float32x2_t v242 = vadd_f32(v241, v226);
  float32x2_t v243 = vsub_f32(v226, v228);
  float32x2_t v245 = vadd_f32(v226, v232);
  float32x2_t v247 = vsub_f32(v226, v229);
  float32x2_t v249 = vadd_f32(v226, v227);
  float32x2_t v251 = vadd_f32(v126, v237);
  float32x2_t v253 = vsub_f32(v239, v235);
  float32x2_t v255 = vadd_f32(v126, v240);
  float32x2_t v257 = vsub_f32(v240, v236);
  float32x2_t v260 = vadd_f32(v259, v237);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v275), 0);
  float32x2_t v244 = vsub_f32(v243, v233);
  float32x2_t v246 = vadd_f32(v245, v234);
  float32x2_t v248 = vsub_f32(v247, v234);
  float32x2_t v250 = vsub_f32(v249, v230);
  float32x2_t v252 = vadd_f32(v251, v239);
  float32x2_t v254 = vsub_f32(v253, v126);
  float32x2_t v256 = vadd_f32(v255, v238);
  float32x2_t v258 = vsub_f32(v257, v126);
  float32x2_t v261 = vadd_f32(v260, v238);
  float32x2_t v262 = vsub_f32(v261, v126);
  float32x2_t v264 = vadd_f32(v242, v252);
  float32x2_t v265 = vadd_f32(v244, v254);
  float32x2_t v266 = vsub_f32(v246, v256);
  float32x2_t v267 = vadd_f32(v248, v258);
  float32x2_t v268 = vsub_f32(v248, v258);
  float32x2_t v269 = vadd_f32(v246, v256);
  float32x2_t v270 = vsub_f32(v244, v254);
  float32x2_t v271 = vsub_f32(v242, v252);
  float32x2_t v263 = vadd_f32(v250, v262);
  float32x2_t v272 = vsub_f32(v250, v262);
  int16x4_t v287 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v264, 15), (int32x2_t){0, 0}));
  int16x4_t v293 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v265, 15), (int32x2_t){0, 0}));
  int16x4_t v299 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v266, 15), (int32x2_t){0, 0}));
  int16x4_t v305 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v267, 15), (int32x2_t){0, 0}));
  int16x4_t v311 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v268, 15), (int32x2_t){0, 0}));
  int16x4_t v317 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v269, 15), (int32x2_t){0, 0}));
  int16x4_t v323 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v270, 15), (int32x2_t){0, 0}));
  int16x4_t v329 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v271, 15), (int32x2_t){0, 0}));
  int16x4_t v281 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v263, 15), (int32x2_t){0, 0}));
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v287), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v293), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v299), 0);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v305), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v311), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v317), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v323), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v329), 0);
  int16x4_t v335 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v272, 15), (int32x2_t){0, 0}));
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v281), 0);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v335), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun11(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v145 = 1.1000000000000001e+00F;
  float v150 = -3.3166247903554003e-01F;
  float v157 = 5.1541501300188641e-01F;
  float v162 = 9.4125353283118118e-01F;
  float v167 = 1.4143537075597825e+00F;
  float v172 = 8.5949297361449750e-01F;
  float v177 = 4.2314838273285138e-02F;
  float v182 = 3.8639279888589606e-01F;
  float v187 = 5.1254589567200015e-01F;
  float v192 = 1.0702757469471715e+00F;
  float v197 = 5.5486073394528512e-01F;
  float v202 = -1.2412944743900585e+00F;
  float v209 = -2.0897833842005756e-01F;
  float v216 = -3.7415717312460811e-01F;
  float v223 = -4.9929922194110327e-02F;
  float v230 = -6.5815896284539266e-01F;
  float v237 = -6.3306543373877577e-01F;
  float v244 = -1.0822460581641109e+00F;
  float v251 = -8.1720737907134022e-01F;
  float v258 = -4.2408709531871824e-01F;
  const int32_t *v405 = &v5[v0];
  int32_t *v617 = &v6[v2];
  int64_t v23 = v0 * 10;
  int64_t v32 = v0 * 2;
  int64_t v40 = v0 * 9;
  int64_t v49 = v0 * 3;
  int64_t v57 = v0 * 8;
  int64_t v66 = v0 * 4;
  int64_t v74 = v0 * 7;
  int64_t v83 = v0 * 5;
  int64_t v91 = v0 * 6;
  float v153 = v4 * v150;
  float v205 = v4 * v202;
  float v212 = v4 * v209;
  float v219 = v4 * v216;
  float v226 = v4 * v223;
  float v233 = v4 * v230;
  float v240 = v4 * v237;
  float v247 = v4 * v244;
  float v254 = v4 * v251;
  float v261 = v4 * v258;
  int64_t v320 = v2 * 10;
  int64_t v328 = v2 * 9;
  int64_t v336 = v2 * 8;
  int64_t v344 = v2 * 7;
  int64_t v352 = v2 * 6;
  int64_t v360 = v2 * 5;
  int64_t v368 = v2 * 4;
  int64_t v376 = v2 * 3;
  int64_t v384 = v2 * 2;
  const int32_t *v496 = &v5[0];
  svfloat32_t v500 = svdup_n_f32(v145);
  svfloat32_t v502 = svdup_n_f32(v157);
  svfloat32_t v503 = svdup_n_f32(v162);
  svfloat32_t v504 = svdup_n_f32(v167);
  svfloat32_t v505 = svdup_n_f32(v172);
  svfloat32_t v506 = svdup_n_f32(v177);
  svfloat32_t v507 = svdup_n_f32(v182);
  svfloat32_t v508 = svdup_n_f32(v187);
  svfloat32_t v509 = svdup_n_f32(v192);
  svfloat32_t v510 = svdup_n_f32(v197);
  int32_t *v527 = &v6[0];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v405[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v414 = &v5[v23];
  const int32_t *v423 = &v5[v32];
  const int32_t *v432 = &v5[v40];
  const int32_t *v441 = &v5[v49];
  const int32_t *v450 = &v5[v57];
  const int32_t *v459 = &v5[v66];
  const int32_t *v468 = &v5[v74];
  const int32_t *v477 = &v5[v83];
  const int32_t *v486 = &v5[v91];
  svfloat32_t v501 = svdup_n_f32(v153);
  svfloat32_t v511 = svdup_n_f32(v205);
  svfloat32_t v512 = svdup_n_f32(v212);
  svfloat32_t v513 = svdup_n_f32(v219);
  svfloat32_t v514 = svdup_n_f32(v226);
  svfloat32_t v515 = svdup_n_f32(v233);
  svfloat32_t v516 = svdup_n_f32(v240);
  svfloat32_t v517 = svdup_n_f32(v247);
  svfloat32_t v518 = svdup_n_f32(v254);
  svfloat32_t v519 = svdup_n_f32(v261);
  int32_t *v536 = &v6[v320];
  int32_t *v545 = &v6[v328];
  int32_t *v554 = &v6[v336];
  int32_t *v563 = &v6[v344];
  int32_t *v572 = &v6[v352];
  int32_t *v581 = &v6[v360];
  int32_t *v590 = &v6[v368];
  int32_t *v599 = &v6[v376];
  int32_t *v608 = &v6[v384];
  svfloat32_t v117 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v496[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v414[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v38 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v423[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v46 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v432[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v55 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v441[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v63 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v450[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v72 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v459[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v80 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v468[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v89 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v477[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v97 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v486[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v47 = svadd_f32_x(svptrue_b32(), v38, v46);
  svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v55, v63);
  svfloat32_t v81 = svadd_f32_x(svptrue_b32(), v72, v80);
  svfloat32_t v98 = svadd_f32_x(svptrue_b32(), v89, v97);
  svfloat32_t v99 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v100 = svsub_f32_x(svptrue_b32(), v38, v46);
  svfloat32_t v101 = svsub_f32_x(svptrue_b32(), v55, v63);
  svfloat32_t v102 = svsub_f32_x(svptrue_b32(), v72, v80);
  svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v89, v97);
  svfloat32_t v104 = svadd_f32_x(svptrue_b32(), v30, v47);
  svfloat32_t v105 = svadd_f32_x(svptrue_b32(), v64, v98);
  svfloat32_t v107 = svsub_f32_x(svptrue_b32(), v100, v101);
  svfloat32_t v108 = svadd_f32_x(svptrue_b32(), v99, v103);
  svfloat32_t v121 = svsub_f32_x(svptrue_b32(), v47, v81);
  svfloat32_t v122 = svsub_f32_x(svptrue_b32(), v30, v81);
  svfloat32_t v123 = svsub_f32_x(svptrue_b32(), v47, v30);
  svfloat32_t v124 = svsub_f32_x(svptrue_b32(), v98, v81);
  svfloat32_t v125 = svsub_f32_x(svptrue_b32(), v64, v81);
  svfloat32_t v126 = svsub_f32_x(svptrue_b32(), v98, v64);
  svfloat32_t v127 = svsub_f32_x(svptrue_b32(), v47, v98);
  svfloat32_t v128 = svsub_f32_x(svptrue_b32(), v30, v64);
  svfloat32_t v130 = svadd_f32_x(svptrue_b32(), v100, v102);
  svfloat32_t v131 = svsub_f32_x(svptrue_b32(), v99, v102);
  svfloat32_t v132 = svadd_f32_x(svptrue_b32(), v99, v100);
  svfloat32_t v133 = svsub_f32_x(svptrue_b32(), v102, v103);
  svfloat32_t v134 = svsub_f32_x(svptrue_b32(), v101, v102);
  svfloat32_t v135 = svsub_f32_x(svptrue_b32(), v101, v103);
  svfloat32_t v136 = svadd_f32_x(svptrue_b32(), v100, v103);
  svfloat32_t v137 = svsub_f32_x(svptrue_b32(), v99, v101);
  svfloat32_t v106 = svadd_f32_x(svptrue_b32(), v81, v104);
  svfloat32_t v119 = svsub_f32_x(svptrue_b32(), v107, v108);
  svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v105, v104);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v107, v108);
  svfloat32_t v165 = svmul_f32_x(svptrue_b32(), v122, v503);
  svfloat32_t v170 = svmul_f32_x(svptrue_b32(), v123, v504);
  svfloat32_t v180 = svmul_f32_x(svptrue_b32(), v125, v506);
  svfloat32_t v185 = svmul_f32_x(svptrue_b32(), v126, v507);
  svfloat32_t zero207 = svdup_n_f32(0);
  svfloat32_t v207 = svcmla_f32_x(pred_full, zero207, v511, v130, 90);
  svfloat32_t zero221 = svdup_n_f32(0);
  svfloat32_t v221 = svcmla_f32_x(pred_full, zero221, v513, v132, 90);
  svfloat32_t zero228 = svdup_n_f32(0);
  svfloat32_t v228 = svcmla_f32_x(pred_full, zero228, v514, v133, 90);
  svfloat32_t zero242 = svdup_n_f32(0);
  svfloat32_t v242 = svcmla_f32_x(pred_full, zero242, v516, v135, 90);
  svfloat32_t zero249 = svdup_n_f32(0);
  svfloat32_t v249 = svcmla_f32_x(pred_full, zero249, v517, v136, 90);
  svfloat32_t v109 = svadd_f32_x(svptrue_b32(), v106, v105);
  svfloat32_t v120 = svsub_f32_x(svptrue_b32(), v119, v102);
  svfloat32_t v200 = svmul_f32_x(svptrue_b32(), v129, v510);
  svfloat32_t zero263 = svdup_n_f32(0);
  svfloat32_t v263 = svcmla_f32_x(pred_full, zero263, v519, v138, 90);
  svfloat32_t v265 = svmla_f32_x(pred_full, v165, v121, v502);
  svfloat32_t v266 = svmla_f32_x(pred_full, v170, v122, v503);
  svfloat32_t v267 = svnmls_f32_x(pred_full, v170, v121, v502);
  svfloat32_t v268 = svmla_f32_x(pred_full, v180, v124, v505);
  svfloat32_t v269 = svmla_f32_x(pred_full, v185, v125, v506);
  svfloat32_t v270 = svnmls_f32_x(pred_full, v185, v124, v505);
  svfloat32_t v273 = svcmla_f32_x(pred_full, v221, v512, v131, 90);
  svfloat32_t v274 = svsub_f32_x(svptrue_b32(), v207, v221);
  svfloat32_t v275 = svcmla_f32_x(pred_full, v242, v515, v134, 90);
  svfloat32_t v276 = svsub_f32_x(svptrue_b32(), v228, v242);
  svfloat32_t v118 = svadd_f32_x(svptrue_b32(), v117, v109);
  svfloat32_t zero155 = svdup_n_f32(0);
  svfloat32_t v155 = svcmla_f32_x(pred_full, zero155, v501, v120, 90);
  svfloat32_t v271 = svmla_f32_x(pred_full, v200, v128, v509);
  svfloat32_t v272 = svmla_f32_x(pred_full, v200, v127, v508);
  svfloat32_t v277 = svcmla_f32_x(pred_full, v263, v518, v137, 90);
  svfloat32_t v278 = svsub_f32_x(svptrue_b32(), v249, v263);
  svfloat32_t v297 = svadd_f32_x(svptrue_b32(), v273, v274);
  svfloat32_t v264 = svmls_f32_x(pred_full, v118, v109, v500);
  svfloat32_t v279 = svadd_f32_x(svptrue_b32(), v269, v271);
  svfloat32_t v289 = svadd_f32_x(svptrue_b32(), v155, v275);
  svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v277, v273);
  svfloat32_t v293 = svadd_f32_x(svptrue_b32(), v155, v278);
  svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v278, v274);
  svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v297, v275);
  svint16_t v313 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v118, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v280 = svadd_f32_x(svptrue_b32(), v279, v264);
  svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v264, v266);
  svfloat32_t v283 = svadd_f32_x(svptrue_b32(), v264, v270);
  svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v264, v267);
  svfloat32_t v287 = svadd_f32_x(svptrue_b32(), v264, v265);
  svfloat32_t v290 = svadd_f32_x(svptrue_b32(), v289, v277);
  svfloat32_t v292 = svsub_f32_x(svptrue_b32(), v291, v155);
  svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v293, v276);
  svfloat32_t v296 = svsub_f32_x(svptrue_b32(), v295, v155);
  svfloat32_t v299 = svadd_f32_x(svptrue_b32(), v298, v276);
  svst1w_u64(pred_full, (unsigned *)(v527), svreinterpret_u64_s16(v313));
  svfloat32_t v282 = svsub_f32_x(svptrue_b32(), v281, v271);
  svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v283, v272);
  svfloat32_t v286 = svsub_f32_x(svptrue_b32(), v285, v272);
  svfloat32_t v288 = svsub_f32_x(svptrue_b32(), v287, v268);
  svfloat32_t v300 = svsub_f32_x(svptrue_b32(), v299, v155);
  svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v280, v290);
  svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v280, v290);
  svfloat32_t v301 = svadd_f32_x(svptrue_b32(), v288, v300);
  svfloat32_t v303 = svadd_f32_x(svptrue_b32(), v282, v292);
  svfloat32_t v304 = svsub_f32_x(svptrue_b32(), v284, v294);
  svfloat32_t v305 = svadd_f32_x(svptrue_b32(), v286, v296);
  svfloat32_t v306 = svsub_f32_x(svptrue_b32(), v286, v296);
  svfloat32_t v307 = svadd_f32_x(svptrue_b32(), v284, v294);
  svfloat32_t v308 = svsub_f32_x(svptrue_b32(), v282, v292);
  svfloat32_t v310 = svsub_f32_x(svptrue_b32(), v288, v300);
  svint16_t v329 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v302, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v385 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v309, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v321 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v301, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v337 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v303, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v345 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v304, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v353 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v305, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v361 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v306, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v369 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v307, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v377 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v308, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v393 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v310, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v545), svreinterpret_u64_s16(v329));
  svst1w_u64(pred_full, (unsigned *)(v608), svreinterpret_u64_s16(v385));
  svst1w_u64(pred_full, (unsigned *)(v536), svreinterpret_u64_s16(v321));
  svst1w_u64(pred_full, (unsigned *)(v554), svreinterpret_u64_s16(v337));
  svst1w_u64(pred_full, (unsigned *)(v563), svreinterpret_u64_s16(v345));
  svst1w_u64(pred_full, (unsigned *)(v572), svreinterpret_u64_s16(v353));
  svst1w_u64(pred_full, (unsigned *)(v581), svreinterpret_u64_s16(v361));
  svst1w_u64(pred_full, (unsigned *)(v590), svreinterpret_u64_s16(v369));
  svst1w_u64(pred_full, (unsigned *)(v599), svreinterpret_u64_s16(v377));
  svst1w_u64(pred_full, (unsigned *)(v617), svreinterpret_u64_s16(v393));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun12(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v111 = 1.0000000000000000e+00F;
  float v112 = -1.0000000000000000e+00F;
  float v138 = -1.4999999999999998e+00F;
  float v139 = 1.4999999999999998e+00F;
  float v167 = 8.6602540378443871e-01F;
  float v175 = -8.6602540378443871e-01F;
  int16x4_t v27 = vld1s_s16(&v5[0]);
  int16x4_t v76 = vld1s_s16(&v5[istride]);
  float32x2_t v113 = (float32x2_t){v111, v112};
  float32x2_t v136 = (float32x2_t){v138, v138};
  float32x2_t v140 = (float32x2_t){v138, v139};
  float32x2_t v169 = (float32x2_t){v167, v175};
  float32x2_t v170 = (float32x2_t){v4, v4};
  float32x2_t v176 = (float32x2_t){v175, v175};
  int16x4_t v13 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v19 = vld1s_s16(&v5[istride * 8]);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  int16x4_t v34 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v40 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v48 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v69 = vld1s_s16(&v5[istride * 6]);
  float32x2_t v77 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v76)), 15);
  int16x4_t v82 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v90 = vld1s_s16(&v5[istride * 9]);
  float32x2_t v115 = vmul_f32(v170, v113);
  float32x2_t v142 = vmul_f32(v170, v140);
  float32x2_t v171 = vmul_f32(v170, v169);
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v35 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v34)), 15);
  float32x2_t v41 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v40)), 15);
  float32x2_t v49 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v48)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  float32x2_t v83 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v82)), 15);
  float32x2_t v91 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v90)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v42 = vadd_f32(v35, v41);
  float32x2_t v43 = vsub_f32(v35, v41);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v84 = vadd_f32(v77, v83);
  float32x2_t v85 = vsub_f32(v77, v83);
  float32x2_t v29 = vadd_f32(v21, v28);
  float32x2_t v50 = vadd_f32(v42, v49);
  float32x2_t v71 = vadd_f32(v63, v70);
  float32x2_t v92 = vadd_f32(v84, v91);
  float32x2_t v120 = vadd_f32(v21, v63);
  float32x2_t v121 = vsub_f32(v21, v63);
  float32x2_t v122 = vadd_f32(v42, v84);
  float32x2_t v123 = vsub_f32(v42, v84);
  float32x2_t v147 = vadd_f32(v22, v64);
  float32x2_t v148 = vsub_f32(v22, v64);
  float32x2_t v149 = vadd_f32(v43, v85);
  float32x2_t v150 = vsub_f32(v43, v85);
  float32x2_t v93 = vadd_f32(v29, v71);
  float32x2_t v94 = vsub_f32(v29, v71);
  float32x2_t v95 = vadd_f32(v50, v92);
  float32x2_t v96 = vsub_f32(v50, v92);
  float32x2_t v124 = vadd_f32(v120, v122);
  float32x2_t v125 = vsub_f32(v120, v122);
  float32x2_t v137 = vmul_f32(v121, v136);
  float32x2_t v143 = vrev64_f32(v123);
  float32x2_t v151 = vadd_f32(v147, v149);
  float32x2_t v152 = vsub_f32(v147, v149);
  float32x2_t v172 = vrev64_f32(v148);
  float32x2_t v177 = vmul_f32(v150, v176);
  float32x2_t v97 = vadd_f32(v93, v95);
  float32x2_t v98 = vsub_f32(v93, v95);
  float32x2_t v116 = vrev64_f32(v96);
  float32x2_t v129 = vmul_f32(v124, v136);
  float32x2_t v133 = vmul_f32(v125, v136);
  float32x2_t v144 = vmul_f32(v143, v142);
  float32x2_t v158 = vrev64_f32(v151);
  float32x2_t v165 = vrev64_f32(v152);
  float32x2_t v173 = vmul_f32(v172, v171);
  float32x2_t v117 = vmul_f32(v116, v115);
  float32x2_t v145 = vadd_f32(v137, v144);
  float32x2_t v146 = vsub_f32(v137, v144);
  float32x2_t v159 = vmul_f32(v158, v171);
  float32x2_t v166 = vmul_f32(v165, v171);
  float32x2_t v178 = vadd_f32(v173, v177);
  float32x2_t v179 = vsub_f32(v173, v177);
  float32x2_t v180 = vadd_f32(v97, v129);
  int16x4_t v185 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v97, 15), (int32x2_t){0, 0}));
  float32x2_t v222 = vadd_f32(v98, v133);
  int16x4_t v227 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v98, 15), (int32x2_t){0, 0}));
  float32x2_t v118 = vadd_f32(v94, v117);
  float32x2_t v119 = vsub_f32(v94, v117);
  float32x2_t v181 = vadd_f32(v180, v159);
  float32x2_t v182 = vsub_f32(v180, v159);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v185), 0);
  float32x2_t v223 = vadd_f32(v222, v166);
  float32x2_t v224 = vsub_f32(v222, v166);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v227), 0);
  int16x4_t v191 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v182, 15), (int32x2_t){0, 0}));
  int16x4_t v197 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v181, 15), (int32x2_t){0, 0}));
  float32x2_t v201 = vadd_f32(v119, v146);
  int16x4_t v206 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v119, 15), (int32x2_t){0, 0}));
  int16x4_t v233 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v224, 15), (int32x2_t){0, 0}));
  int16x4_t v239 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v223, 15), (int32x2_t){0, 0}));
  float32x2_t v243 = vadd_f32(v118, v145);
  int16x4_t v248 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v118, 15), (int32x2_t){0, 0}));
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v191), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v197), 0);
  float32x2_t v202 = vadd_f32(v201, v179);
  float32x2_t v203 = vsub_f32(v201, v179);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v206), 0);
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v233), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v239), 0);
  float32x2_t v244 = vadd_f32(v243, v178);
  float32x2_t v245 = vsub_f32(v243, v178);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v248), 0);
  int16x4_t v212 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v203, 15), (int32x2_t){0, 0}));
  int16x4_t v218 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v202, 15), (int32x2_t){0, 0}));
  int16x4_t v254 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v245, 15), (int32x2_t){0, 0}));
  int16x4_t v260 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v244, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v212), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v218), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v254), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v260), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun12(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v144 = -1.0000000000000000e+00F;
  float v169 = -1.4999999999999998e+00F;
  float v174 = 1.4999999999999998e+00F;
  float v210 = -8.6602540378443871e-01F;
  const int32_t *v412 = &v5[v0];
  int32_t *v488 = &v6[v2];
  int64_t v15 = v0 * 4;
  int64_t v23 = v0 * 8;
  int64_t v42 = v0 * 7;
  int64_t v50 = v0 * 11;
  int64_t v60 = v0 * 3;
  int64_t v69 = v0 * 10;
  int64_t v77 = v0 * 2;
  int64_t v87 = v0 * 6;
  int64_t v104 = v0 * 5;
  int64_t v114 = v0 * 9;
  float v147 = v4 * v144;
  float v177 = v4 * v174;
  float v206 = v4 * v210;
  int64_t v228 = v2 * 4;
  int64_t v236 = v2 * 8;
  int64_t v247 = v2 * 9;
  int64_t v263 = v2 * 5;
  int64_t v274 = v2 * 6;
  int64_t v282 = v2 * 10;
  int64_t v290 = v2 * 2;
  int64_t v301 = v2 * 3;
  int64_t v309 = v2 * 7;
  int64_t v317 = v2 * 11;
  const int32_t *v349 = &v5[0];
  svfloat32_t v439 = svdup_n_f32(v169);
  svfloat32_t v444 = svdup_n_f32(v210);
  int32_t *v452 = &v6[0];
  svfloat32_t v102 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v412[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v330 = &v5[v15];
  const int32_t *v339 = &v5[v23];
  const int32_t *v358 = &v5[v42];
  const int32_t *v367 = &v5[v50];
  const int32_t *v376 = &v5[v60];
  const int32_t *v385 = &v5[v69];
  const int32_t *v394 = &v5[v77];
  const int32_t *v403 = &v5[v87];
  const int32_t *v421 = &v5[v104];
  const int32_t *v430 = &v5[v114];
  svfloat32_t v436 = svdup_n_f32(v147);
  svfloat32_t v440 = svdup_n_f32(v177);
  svfloat32_t v443 = svdup_n_f32(v206);
  int32_t *v461 = &v6[v228];
  int32_t *v470 = &v6[v236];
  int32_t *v479 = &v6[v247];
  int32_t *v497 = &v6[v263];
  int32_t *v506 = &v6[v274];
  int32_t *v515 = &v6[v282];
  int32_t *v524 = &v6[v290];
  int32_t *v533 = &v6[v301];
  int32_t *v542 = &v6[v309];
  int32_t *v551 = &v6[v317];
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v349[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v330[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v339[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v48 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v358[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v56 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v367[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v66 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v376[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v385[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v394[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v403[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v110 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v421[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v120 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v430[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v57 = svadd_f32_x(svptrue_b32(), v48, v56);
  svfloat32_t v58 = svsub_f32_x(svptrue_b32(), v48, v56);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v111 = svadd_f32_x(svptrue_b32(), v102, v110);
  svfloat32_t v112 = svsub_f32_x(svptrue_b32(), v102, v110);
  svfloat32_t v40 = svadd_f32_x(svptrue_b32(), v30, v39);
  svfloat32_t v67 = svadd_f32_x(svptrue_b32(), v57, v66);
  svfloat32_t v94 = svadd_f32_x(svptrue_b32(), v84, v93);
  svfloat32_t v121 = svadd_f32_x(svptrue_b32(), v111, v120);
  svfloat32_t v152 = svadd_f32_x(svptrue_b32(), v30, v84);
  svfloat32_t v153 = svsub_f32_x(svptrue_b32(), v30, v84);
  svfloat32_t v154 = svadd_f32_x(svptrue_b32(), v57, v111);
  svfloat32_t v155 = svsub_f32_x(svptrue_b32(), v57, v111);
  svfloat32_t v182 = svadd_f32_x(svptrue_b32(), v31, v85);
  svfloat32_t v183 = svsub_f32_x(svptrue_b32(), v31, v85);
  svfloat32_t v184 = svadd_f32_x(svptrue_b32(), v58, v112);
  svfloat32_t v185 = svsub_f32_x(svptrue_b32(), v58, v112);
  svfloat32_t v122 = svadd_f32_x(svptrue_b32(), v40, v94);
  svfloat32_t v123 = svsub_f32_x(svptrue_b32(), v40, v94);
  svfloat32_t v124 = svadd_f32_x(svptrue_b32(), v67, v121);
  svfloat32_t v125 = svsub_f32_x(svptrue_b32(), v67, v121);
  svfloat32_t v156 = svadd_f32_x(svptrue_b32(), v152, v154);
  svfloat32_t v157 = svsub_f32_x(svptrue_b32(), v152, v154);
  svfloat32_t zero179 = svdup_n_f32(0);
  svfloat32_t v179 = svcmla_f32_x(pred_full, zero179, v440, v155, 90);
  svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v182, v184);
  svfloat32_t v187 = svsub_f32_x(svptrue_b32(), v182, v184);
  svfloat32_t zero208 = svdup_n_f32(0);
  svfloat32_t v208 = svcmla_f32_x(pred_full, zero208, v443, v183, 90);
  svfloat32_t v126 = svadd_f32_x(svptrue_b32(), v122, v124);
  svfloat32_t v127 = svsub_f32_x(svptrue_b32(), v122, v124);
  svfloat32_t zero149 = svdup_n_f32(0);
  svfloat32_t v149 = svcmla_f32_x(pred_full, zero149, v436, v125, 90);
  svfloat32_t v180 = svmla_f32_x(pred_full, v179, v153, v439);
  svfloat32_t v181 = svnmls_f32_x(pred_full, v179, v153, v439);
  svfloat32_t zero194 = svdup_n_f32(0);
  svfloat32_t v194 = svcmla_f32_x(pred_full, zero194, v443, v186, 90);
  svfloat32_t zero201 = svdup_n_f32(0);
  svfloat32_t v201 = svcmla_f32_x(pred_full, zero201, v443, v187, 90);
  svfloat32_t v214 = svmla_f32_x(pred_full, v208, v185, v444);
  svfloat32_t v215 = svmls_f32_x(pred_full, v208, v185, v444);
  svfloat32_t v150 = svadd_f32_x(svptrue_b32(), v123, v149);
  svfloat32_t v151 = svsub_f32_x(svptrue_b32(), v123, v149);
  svfloat32_t v216 = svmla_f32_x(pred_full, v126, v156, v439);
  svint16_t v221 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v126, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v270 = svmla_f32_x(pred_full, v127, v157, v439);
  svint16_t v275 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v127, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v217 = svadd_f32_x(svptrue_b32(), v216, v194);
  svfloat32_t v218 = svsub_f32_x(svptrue_b32(), v216, v194);
  svfloat32_t v243 = svadd_f32_x(svptrue_b32(), v151, v181);
  svint16_t v248 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v151, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v271 = svadd_f32_x(svptrue_b32(), v270, v201);
  svfloat32_t v272 = svsub_f32_x(svptrue_b32(), v270, v201);
  svfloat32_t v297 = svadd_f32_x(svptrue_b32(), v150, v180);
  svint16_t v302 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v150, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v452), svreinterpret_u64_s16(v221));
  svst1w_u64(pred_full, (unsigned *)(v506), svreinterpret_u64_s16(v275));
  svint16_t v229 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v218, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v237 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v217, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v244 = svadd_f32_x(svptrue_b32(), v243, v215);
  svfloat32_t v245 = svsub_f32_x(svptrue_b32(), v243, v215);
  svint16_t v283 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v272, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v291 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v271, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v297, v214);
  svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v297, v214);
  svst1w_u64(pred_full, (unsigned *)(v479), svreinterpret_u64_s16(v248));
  svst1w_u64(pred_full, (unsigned *)(v533), svreinterpret_u64_s16(v302));
  svint16_t v256 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v245, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v264 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v244, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v310 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v299, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v318 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v298, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v461), svreinterpret_u64_s16(v229));
  svst1w_u64(pred_full, (unsigned *)(v470), svreinterpret_u64_s16(v237));
  svst1w_u64(pred_full, (unsigned *)(v515), svreinterpret_u64_s16(v283));
  svst1w_u64(pred_full, (unsigned *)(v524), svreinterpret_u64_s16(v291));
  svst1w_u64(pred_full, (unsigned *)(v488), svreinterpret_u64_s16(v256));
  svst1w_u64(pred_full, (unsigned *)(v497), svreinterpret_u64_s16(v264));
  svst1w_u64(pred_full, (unsigned *)(v542), svreinterpret_u64_s16(v310));
  svst1w_u64(pred_full, (unsigned *)(v551), svreinterpret_u64_s16(v318));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun13(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v135 = 1.0833333333333333e+00F;
  float v139 = -3.0046260628866578e-01F;
  float v142 = 7.4927933062613905e-01F;
  float v143 = -7.4927933062613905e-01F;
  float v149 = 4.0100212832186721e-01F;
  float v150 = -4.0100212832186721e-01F;
  float v156 = 5.7514072947400308e-01F;
  float v157 = -5.7514072947400308e-01F;
  float v164 = 5.2422663952658211e-01F;
  float v168 = 5.1652078062348972e-01F;
  float v172 = 7.7058589030924258e-03F;
  float v176 = 4.2763404682656941e-01F;
  float v180 = 1.5180597207438440e-01F;
  float v184 = 5.7944001890096386e-01F;
  float v187 = 1.1543953381323635e+00F;
  float v188 = -1.1543953381323635e+00F;
  float v194 = 9.0655220171271012e-01F;
  float v195 = -9.0655220171271012e-01F;
  float v201 = 8.1857027294591811e-01F;
  float v202 = -8.1857027294591811e-01F;
  float v208 = 1.1971367726043427e+00F;
  float v209 = -1.1971367726043427e+00F;
  float v215 = 8.6131170741789742e-01F;
  float v216 = -8.6131170741789742e-01F;
  float v222 = 1.1091548438375507e+00F;
  float v223 = -1.1091548438375507e+00F;
  float v229 = 4.2741434471979367e-02F;
  float v230 = -4.2741434471979367e-02F;
  float v236 = -4.5240494294812715e-02F;
  float v237 = 4.5240494294812715e-02F;
  float v243 = 2.9058457089163264e-01F;
  float v244 = -2.9058457089163264e-01F;
  int16x4_t v13 = vld1s_s16(&v5[istride]);
  int16x4_t v120 = vld1s_s16(&v5[0]);
  float32x2_t v136 = (float32x2_t){v135, v135};
  float32x2_t v140 = (float32x2_t){v139, v139};
  float32x2_t v144 = (float32x2_t){v142, v143};
  float32x2_t v151 = (float32x2_t){v149, v150};
  float32x2_t v158 = (float32x2_t){v156, v157};
  float32x2_t v165 = (float32x2_t){v164, v164};
  float32x2_t v169 = (float32x2_t){v168, v168};
  float32x2_t v173 = (float32x2_t){v172, v172};
  float32x2_t v177 = (float32x2_t){v176, v176};
  float32x2_t v181 = (float32x2_t){v180, v180};
  float32x2_t v185 = (float32x2_t){v184, v184};
  float32x2_t v189 = (float32x2_t){v187, v188};
  float32x2_t v196 = (float32x2_t){v194, v195};
  float32x2_t v203 = (float32x2_t){v201, v202};
  float32x2_t v210 = (float32x2_t){v208, v209};
  float32x2_t v217 = (float32x2_t){v215, v216};
  float32x2_t v224 = (float32x2_t){v222, v223};
  float32x2_t v231 = (float32x2_t){v229, v230};
  float32x2_t v238 = (float32x2_t){v236, v237};
  float32x2_t v245 = (float32x2_t){v243, v244};
  float32x2_t v246 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 12]);
  int16x4_t v26 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v32 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v39 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v45 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v52 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v58 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v65 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v71 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v78 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v84 = vld1s_s16(&v5[istride * 7]);
  float32x2_t v121 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v120)), 15);
  float32x2_t v146 = vmul_f32(v246, v144);
  float32x2_t v153 = vmul_f32(v246, v151);
  float32x2_t v160 = vmul_f32(v246, v158);
  float32x2_t v191 = vmul_f32(v246, v189);
  float32x2_t v198 = vmul_f32(v246, v196);
  float32x2_t v205 = vmul_f32(v246, v203);
  float32x2_t v212 = vmul_f32(v246, v210);
  float32x2_t v219 = vmul_f32(v246, v217);
  float32x2_t v226 = vmul_f32(v246, v224);
  float32x2_t v233 = vmul_f32(v246, v231);
  float32x2_t v240 = vmul_f32(v246, v238);
  float32x2_t v247 = vmul_f32(v246, v245);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v27 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v26)), 15);
  float32x2_t v33 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v32)), 15);
  float32x2_t v40 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v39)), 15);
  float32x2_t v46 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v45)), 15);
  float32x2_t v53 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v52)), 15);
  float32x2_t v59 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v58)), 15);
  float32x2_t v66 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v65)), 15);
  float32x2_t v72 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v71)), 15);
  float32x2_t v79 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v78)), 15);
  float32x2_t v85 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v84)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v34 = vadd_f32(v27, v33);
  float32x2_t v47 = vadd_f32(v40, v46);
  float32x2_t v60 = vadd_f32(v53, v59);
  float32x2_t v73 = vadd_f32(v66, v72);
  float32x2_t v86 = vadd_f32(v79, v85);
  float32x2_t v87 = vsub_f32(v14, v20);
  float32x2_t v88 = vsub_f32(v27, v33);
  float32x2_t v89 = vsub_f32(v40, v46);
  float32x2_t v90 = vsub_f32(v53, v59);
  float32x2_t v91 = vsub_f32(v66, v72);
  float32x2_t v92 = vsub_f32(v79, v85);
  float32x2_t v93 = vadd_f32(v34, v73);
  float32x2_t v95 = vadd_f32(v21, v47);
  float32x2_t v98 = vadd_f32(v88, v91);
  float32x2_t v100 = vadd_f32(v87, v89);
  float32x2_t v102 = vsub_f32(v34, v86);
  float32x2_t v103 = vsub_f32(v47, v60);
  float32x2_t v104 = vsub_f32(v21, v60);
  float32x2_t v105 = vsub_f32(v73, v86);
  float32x2_t v110 = vsub_f32(v88, v92);
  float32x2_t v111 = vsub_f32(v87, v89);
  float32x2_t v112 = vsub_f32(v88, v91);
  float32x2_t v113 = vadd_f32(v87, v90);
  float32x2_t v114 = vsub_f32(v91, v92);
  float32x2_t v115 = vadd_f32(v89, v90);
  float32x2_t v94 = vadd_f32(v93, v86);
  float32x2_t v96 = vadd_f32(v95, v60);
  float32x2_t v99 = vadd_f32(v98, v92);
  float32x2_t v101 = vsub_f32(v100, v90);
  float32x2_t v106 = vsub_f32(v102, v103);
  float32x2_t v107 = vsub_f32(v104, v105);
  float32x2_t v108 = vadd_f32(v102, v103);
  float32x2_t v109 = vadd_f32(v104, v105);
  float32x2_t v127 = vadd_f32(v110, v111);
  float32x2_t v128 = vadd_f32(v112, v113);
  float32x2_t v129 = vsub_f32(v114, v115);
  float32x2_t v192 = vrev64_f32(v110);
  float32x2_t v199 = vrev64_f32(v111);
  float32x2_t v213 = vrev64_f32(v112);
  float32x2_t v220 = vrev64_f32(v113);
  float32x2_t v234 = vrev64_f32(v114);
  float32x2_t v241 = vrev64_f32(v115);
  float32x2_t v97 = vadd_f32(v94, v96);
  float32x2_t v123 = vsub_f32(v96, v94);
  float32x2_t v124 = vadd_f32(v99, v101);
  float32x2_t v125 = vadd_f32(v106, v107);
  float32x2_t v126 = vsub_f32(v108, v109);
  float32x2_t v147 = vrev64_f32(v99);
  float32x2_t v154 = vrev64_f32(v101);
  float32x2_t v166 = vmul_f32(v106, v165);
  float32x2_t v170 = vmul_f32(v107, v169);
  float32x2_t v178 = vmul_f32(v108, v177);
  float32x2_t v182 = vmul_f32(v109, v181);
  float32x2_t v193 = vmul_f32(v192, v191);
  float32x2_t v200 = vmul_f32(v199, v198);
  float32x2_t v206 = vrev64_f32(v127);
  float32x2_t v214 = vmul_f32(v213, v212);
  float32x2_t v221 = vmul_f32(v220, v219);
  float32x2_t v227 = vrev64_f32(v128);
  float32x2_t v235 = vmul_f32(v234, v233);
  float32x2_t v242 = vmul_f32(v241, v240);
  float32x2_t v248 = vrev64_f32(v129);
  float32x2_t v122 = vadd_f32(v121, v97);
  float32x2_t v137 = vmul_f32(v97, v136);
  float32x2_t v141 = vmul_f32(v123, v140);
  float32x2_t v148 = vmul_f32(v147, v146);
  float32x2_t v155 = vmul_f32(v154, v153);
  float32x2_t v161 = vrev64_f32(v124);
  float32x2_t v174 = vmul_f32(v125, v173);
  float32x2_t v186 = vmul_f32(v126, v185);
  float32x2_t v207 = vmul_f32(v206, v205);
  float32x2_t v228 = vmul_f32(v227, v226);
  float32x2_t v249 = vmul_f32(v248, v247);
  float32x2_t v251 = vadd_f32(v170, v166);
  float32x2_t v162 = vmul_f32(v161, v160);
  float32x2_t v250 = vsub_f32(v122, v137);
  float32x2_t v252 = vsub_f32(v251, v141);
  float32x2_t v253 = vadd_f32(v170, v174);
  float32x2_t v255 = vsub_f32(v174, v166);
  float32x2_t v263 = vsub_f32(v193, v207);
  float32x2_t v264 = vsub_f32(v200, v207);
  float32x2_t v265 = vsub_f32(v214, v228);
  float32x2_t v266 = vsub_f32(v221, v228);
  float32x2_t v267 = vsub_f32(v235, v249);
  float32x2_t v268 = vadd_f32(v242, v249);
  int16x4_t v303 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v122, 15), (int32x2_t){0, 0}));
  float32x2_t v254 = vadd_f32(v253, v141);
  float32x2_t v256 = vsub_f32(v255, v141);
  float32x2_t v257 = vadd_f32(v250, v178);
  float32x2_t v259 = vsub_f32(v250, v182);
  float32x2_t v261 = vsub_f32(v250, v178);
  float32x2_t v269 = vsub_f32(v148, v162);
  float32x2_t v270 = vsub_f32(v155, v162);
  float32x2_t v281 = vadd_f32(v263, v267);
  float32x2_t v283 = vadd_f32(v265, v267);
  float32x2_t v285 = vsub_f32(v264, v268);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v303), 0);
  float32x2_t v258 = vadd_f32(v257, v182);
  float32x2_t v260 = vsub_f32(v259, v186);
  float32x2_t v262 = vadd_f32(v261, v186);
  float32x2_t v277 = vsub_f32(v270, v263);
  float32x2_t v279 = vsub_f32(v268, v269);
  float32x2_t v282 = vadd_f32(v281, v270);
  float32x2_t v284 = vsub_f32(v283, v270);
  float32x2_t v286 = vsub_f32(v285, v269);
  float32x2_t v287 = vadd_f32(v269, v264);
  float32x2_t v271 = vadd_f32(v252, v258);
  float32x2_t v272 = vadd_f32(v254, v260);
  float32x2_t v273 = vsub_f32(v260, v254);
  float32x2_t v274 = vadd_f32(v256, v262);
  float32x2_t v275 = vsub_f32(v258, v252);
  float32x2_t v276 = vsub_f32(v262, v256);
  float32x2_t v278 = vadd_f32(v277, v265);
  float32x2_t v280 = vsub_f32(v279, v266);
  float32x2_t v288 = vsub_f32(v287, v266);
  float32x2_t v289 = vsub_f32(v271, v278);
  float32x2_t v290 = vadd_f32(v272, v280);
  float32x2_t v291 = vsub_f32(v273, v282);
  float32x2_t v292 = vsub_f32(v274, v284);
  float32x2_t v293 = vadd_f32(v275, v286);
  float32x2_t v294 = vsub_f32(v276, v288);
  float32x2_t v295 = vadd_f32(v276, v288);
  float32x2_t v296 = vsub_f32(v275, v286);
  float32x2_t v297 = vadd_f32(v274, v284);
  float32x2_t v298 = vadd_f32(v273, v282);
  float32x2_t v299 = vsub_f32(v272, v280);
  float32x2_t v300 = vadd_f32(v271, v278);
  int16x4_t v309 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v289, 15), (int32x2_t){0, 0}));
  int16x4_t v315 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v290, 15), (int32x2_t){0, 0}));
  int16x4_t v321 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v291, 15), (int32x2_t){0, 0}));
  int16x4_t v327 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v292, 15), (int32x2_t){0, 0}));
  int16x4_t v333 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v293, 15), (int32x2_t){0, 0}));
  int16x4_t v339 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v294, 15), (int32x2_t){0, 0}));
  int16x4_t v345 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v295, 15), (int32x2_t){0, 0}));
  int16x4_t v351 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v296, 15), (int32x2_t){0, 0}));
  int16x4_t v357 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v297, 15), (int32x2_t){0, 0}));
  int16x4_t v363 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v298, 15), (int32x2_t){0, 0}));
  int16x4_t v369 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v299, 15), (int32x2_t){0, 0}));
  int16x4_t v375 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v300, 15), (int32x2_t){0, 0}));
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v309), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v315), 0);
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v321), 0);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v327), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v333), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v339), 0);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v345), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v351), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v357), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v363), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v369), 0);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v375), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun13(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v167 = 1.0833333333333333e+00F;
  float v172 = -3.0046260628866578e-01F;
  float v177 = -7.4927933062613905e-01F;
  float v184 = -4.0100212832186721e-01F;
  float v191 = -5.7514072947400308e-01F;
  float v198 = 5.2422663952658211e-01F;
  float v203 = 5.1652078062348972e-01F;
  float v208 = 7.7058589030924258e-03F;
  float v213 = 4.2763404682656941e-01F;
  float v218 = 1.5180597207438440e-01F;
  float v223 = 5.7944001890096386e-01F;
  float v228 = -1.1543953381323635e+00F;
  float v235 = -9.0655220171271012e-01F;
  float v242 = -8.1857027294591811e-01F;
  float v249 = -1.1971367726043427e+00F;
  float v256 = -8.6131170741789742e-01F;
  float v263 = -1.1091548438375507e+00F;
  float v270 = -4.2741434471979367e-02F;
  float v277 = 4.5240494294812715e-02F;
  float v284 = -2.9058457089163264e-01F;
  const int32_t *v451 = &v5[v0];
  int32_t *v699 = &v6[v2];
  int64_t v23 = v0 * 12;
  int64_t v32 = v0 * 2;
  int64_t v40 = v0 * 11;
  int64_t v49 = v0 * 3;
  int64_t v57 = v0 * 10;
  int64_t v66 = v0 * 4;
  int64_t v74 = v0 * 9;
  int64_t v83 = v0 * 5;
  int64_t v91 = v0 * 8;
  int64_t v100 = v0 * 6;
  int64_t v108 = v0 * 7;
  float v180 = v4 * v177;
  float v187 = v4 * v184;
  float v194 = v4 * v191;
  float v231 = v4 * v228;
  float v238 = v4 * v235;
  float v245 = v4 * v242;
  float v252 = v4 * v249;
  float v259 = v4 * v256;
  float v266 = v4 * v263;
  float v273 = v4 * v270;
  float v280 = v4 * v277;
  float v287 = v4 * v284;
  int64_t v350 = v2 * 12;
  int64_t v358 = v2 * 11;
  int64_t v366 = v2 * 10;
  int64_t v374 = v2 * 9;
  int64_t v382 = v2 * 8;
  int64_t v390 = v2 * 7;
  int64_t v398 = v2 * 6;
  int64_t v406 = v2 * 5;
  int64_t v414 = v2 * 4;
  int64_t v422 = v2 * 3;
  int64_t v430 = v2 * 2;
  const int32_t *v560 = &v5[0];
  svfloat32_t v564 = svdup_n_f32(v167);
  svfloat32_t v565 = svdup_n_f32(v172);
  svfloat32_t v569 = svdup_n_f32(v198);
  svfloat32_t v570 = svdup_n_f32(v203);
  svfloat32_t v571 = svdup_n_f32(v208);
  svfloat32_t v572 = svdup_n_f32(v213);
  svfloat32_t v573 = svdup_n_f32(v218);
  svfloat32_t v574 = svdup_n_f32(v223);
  int32_t *v591 = &v6[0];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v451[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v460 = &v5[v23];
  const int32_t *v469 = &v5[v32];
  const int32_t *v478 = &v5[v40];
  const int32_t *v487 = &v5[v49];
  const int32_t *v496 = &v5[v57];
  const int32_t *v505 = &v5[v66];
  const int32_t *v514 = &v5[v74];
  const int32_t *v523 = &v5[v83];
  const int32_t *v532 = &v5[v91];
  const int32_t *v541 = &v5[v100];
  const int32_t *v550 = &v5[v108];
  svfloat32_t v566 = svdup_n_f32(v180);
  svfloat32_t v567 = svdup_n_f32(v187);
  svfloat32_t v568 = svdup_n_f32(v194);
  svfloat32_t v575 = svdup_n_f32(v231);
  svfloat32_t v576 = svdup_n_f32(v238);
  svfloat32_t v577 = svdup_n_f32(v245);
  svfloat32_t v578 = svdup_n_f32(v252);
  svfloat32_t v579 = svdup_n_f32(v259);
  svfloat32_t v580 = svdup_n_f32(v266);
  svfloat32_t v581 = svdup_n_f32(v273);
  svfloat32_t v582 = svdup_n_f32(v280);
  svfloat32_t v583 = svdup_n_f32(v287);
  int32_t *v600 = &v6[v350];
  int32_t *v609 = &v6[v358];
  int32_t *v618 = &v6[v366];
  int32_t *v627 = &v6[v374];
  int32_t *v636 = &v6[v382];
  int32_t *v645 = &v6[v390];
  int32_t *v654 = &v6[v398];
  int32_t *v663 = &v6[v406];
  int32_t *v672 = &v6[v414];
  int32_t *v681 = &v6[v422];
  int32_t *v690 = &v6[v430];
  svfloat32_t v152 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v560[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v460[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v38 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v469[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v46 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v478[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v55 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v487[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v63 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v496[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v72 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v505[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v80 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v514[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v89 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v523[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v97 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v532[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v106 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v541[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v114 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v550[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v47 = svadd_f32_x(svptrue_b32(), v38, v46);
  svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v55, v63);
  svfloat32_t v81 = svadd_f32_x(svptrue_b32(), v72, v80);
  svfloat32_t v98 = svadd_f32_x(svptrue_b32(), v89, v97);
  svfloat32_t v115 = svadd_f32_x(svptrue_b32(), v106, v114);
  svfloat32_t v116 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v117 = svsub_f32_x(svptrue_b32(), v38, v46);
  svfloat32_t v118 = svsub_f32_x(svptrue_b32(), v55, v63);
  svfloat32_t v119 = svsub_f32_x(svptrue_b32(), v72, v80);
  svfloat32_t v120 = svsub_f32_x(svptrue_b32(), v89, v97);
  svfloat32_t v121 = svsub_f32_x(svptrue_b32(), v106, v114);
  svfloat32_t v122 = svadd_f32_x(svptrue_b32(), v47, v98);
  svfloat32_t v124 = svadd_f32_x(svptrue_b32(), v30, v64);
  svfloat32_t v127 = svadd_f32_x(svptrue_b32(), v117, v120);
  svfloat32_t v129 = svadd_f32_x(svptrue_b32(), v116, v118);
  svfloat32_t v131 = svsub_f32_x(svptrue_b32(), v47, v115);
  svfloat32_t v132 = svsub_f32_x(svptrue_b32(), v64, v81);
  svfloat32_t v133 = svsub_f32_x(svptrue_b32(), v30, v81);
  svfloat32_t v134 = svsub_f32_x(svptrue_b32(), v98, v115);
  svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v117, v121);
  svfloat32_t v140 = svsub_f32_x(svptrue_b32(), v116, v118);
  svfloat32_t v141 = svsub_f32_x(svptrue_b32(), v117, v120);
  svfloat32_t v142 = svadd_f32_x(svptrue_b32(), v116, v119);
  svfloat32_t v143 = svsub_f32_x(svptrue_b32(), v120, v121);
  svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v118, v119);
  svfloat32_t v123 = svadd_f32_x(svptrue_b32(), v122, v115);
  svfloat32_t v125 = svadd_f32_x(svptrue_b32(), v124, v81);
  svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v127, v121);
  svfloat32_t v130 = svsub_f32_x(svptrue_b32(), v129, v119);
  svfloat32_t v135 = svsub_f32_x(svptrue_b32(), v131, v132);
  svfloat32_t v136 = svsub_f32_x(svptrue_b32(), v133, v134);
  svfloat32_t v137 = svadd_f32_x(svptrue_b32(), v131, v132);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v133, v134);
  svfloat32_t v158 = svadd_f32_x(svptrue_b32(), v139, v140);
  svfloat32_t v159 = svadd_f32_x(svptrue_b32(), v141, v142);
  svfloat32_t v160 = svsub_f32_x(svptrue_b32(), v143, v144);
  svfloat32_t zero233 = svdup_n_f32(0);
  svfloat32_t v233 = svcmla_f32_x(pred_full, zero233, v575, v139, 90);
  svfloat32_t zero240 = svdup_n_f32(0);
  svfloat32_t v240 = svcmla_f32_x(pred_full, zero240, v576, v140, 90);
  svfloat32_t zero254 = svdup_n_f32(0);
  svfloat32_t v254 = svcmla_f32_x(pred_full, zero254, v578, v141, 90);
  svfloat32_t zero261 = svdup_n_f32(0);
  svfloat32_t v261 = svcmla_f32_x(pred_full, zero261, v579, v142, 90);
  svfloat32_t zero275 = svdup_n_f32(0);
  svfloat32_t v275 = svcmla_f32_x(pred_full, zero275, v581, v143, 90);
  svfloat32_t v126 = svadd_f32_x(svptrue_b32(), v123, v125);
  svfloat32_t v154 = svsub_f32_x(svptrue_b32(), v125, v123);
  svfloat32_t v155 = svadd_f32_x(svptrue_b32(), v128, v130);
  svfloat32_t v156 = svadd_f32_x(svptrue_b32(), v135, v136);
  svfloat32_t v157 = svsub_f32_x(svptrue_b32(), v137, v138);
  svfloat32_t zero182 = svdup_n_f32(0);
  svfloat32_t v182 = svcmla_f32_x(pred_full, zero182, v566, v128, 90);
  svfloat32_t zero189 = svdup_n_f32(0);
  svfloat32_t v189 = svcmla_f32_x(pred_full, zero189, v567, v130, 90);
  svfloat32_t v201 = svmul_f32_x(svptrue_b32(), v135, v569);
  svfloat32_t zero247 = svdup_n_f32(0);
  svfloat32_t v247 = svcmla_f32_x(pred_full, zero247, v577, v158, 90);
  svfloat32_t zero268 = svdup_n_f32(0);
  svfloat32_t v268 = svcmla_f32_x(pred_full, zero268, v580, v159, 90);
  svfloat32_t zero289 = svdup_n_f32(0);
  svfloat32_t v289 = svcmla_f32_x(pred_full, zero289, v583, v160, 90);
  svfloat32_t v153 = svadd_f32_x(svptrue_b32(), v152, v126);
  svfloat32_t zero196 = svdup_n_f32(0);
  svfloat32_t v196 = svcmla_f32_x(pred_full, zero196, v568, v155, 90);
  svfloat32_t v211 = svmul_f32_x(svptrue_b32(), v156, v571);
  svfloat32_t v291 = svmla_f32_x(pred_full, v201, v136, v570);
  svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v233, v247);
  svfloat32_t v304 = svsub_f32_x(svptrue_b32(), v240, v247);
  svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v254, v268);
  svfloat32_t v306 = svsub_f32_x(svptrue_b32(), v261, v268);
  svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v275, v289);
  svfloat32_t v308 = svcmla_f32_x(pred_full, v289, v582, v144, 90);
  svfloat32_t v290 = svmls_f32_x(pred_full, v153, v126, v564);
  svfloat32_t v292 = svmls_f32_x(pred_full, v291, v154, v565);
  svfloat32_t v293 = svmla_f32_x(pred_full, v211, v136, v570);
  svfloat32_t v295 = svnmls_f32_x(pred_full, v201, v156, v571);
  svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v182, v196);
  svfloat32_t v310 = svsub_f32_x(svptrue_b32(), v189, v196);
  svfloat32_t v321 = svadd_f32_x(svptrue_b32(), v303, v307);
  svfloat32_t v323 = svadd_f32_x(svptrue_b32(), v305, v307);
  svfloat32_t v325 = svsub_f32_x(svptrue_b32(), v304, v308);
  svint16_t v343 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v153, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v294 = svmla_f32_x(pred_full, v293, v154, v565);
  svfloat32_t v296 = svmls_f32_x(pred_full, v295, v154, v565);
  svfloat32_t v297 = svmla_f32_x(pred_full, v290, v137, v572);
  svfloat32_t v299 = svmls_f32_x(pred_full, v290, v138, v573);
  svfloat32_t v301 = svmls_f32_x(pred_full, v290, v137, v572);
  svfloat32_t v317 = svsub_f32_x(svptrue_b32(), v310, v303);
  svfloat32_t v319 = svsub_f32_x(svptrue_b32(), v308, v309);
  svfloat32_t v322 = svadd_f32_x(svptrue_b32(), v321, v310);
  svfloat32_t v324 = svsub_f32_x(svptrue_b32(), v323, v310);
  svfloat32_t v326 = svsub_f32_x(svptrue_b32(), v325, v309);
  svfloat32_t v327 = svadd_f32_x(svptrue_b32(), v309, v304);
  svst1w_u64(pred_full, (unsigned *)(v591), svreinterpret_u64_s16(v343));
  svfloat32_t v298 = svmla_f32_x(pred_full, v297, v138, v573);
  svfloat32_t v300 = svmls_f32_x(pred_full, v299, v157, v574);
  svfloat32_t v302 = svmla_f32_x(pred_full, v301, v157, v574);
  svfloat32_t v318 = svadd_f32_x(svptrue_b32(), v317, v305);
  svfloat32_t v320 = svsub_f32_x(svptrue_b32(), v319, v306);
  svfloat32_t v328 = svsub_f32_x(svptrue_b32(), v327, v306);
  svfloat32_t v311 = svadd_f32_x(svptrue_b32(), v292, v298);
  svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v294, v300);
  svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v300, v294);
  svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v296, v302);
  svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v298, v292);
  svfloat32_t v316 = svsub_f32_x(svptrue_b32(), v302, v296);
  svfloat32_t v329 = svsub_f32_x(svptrue_b32(), v311, v318);
  svfloat32_t v330 = svadd_f32_x(svptrue_b32(), v312, v320);
  svfloat32_t v331 = svsub_f32_x(svptrue_b32(), v313, v322);
  svfloat32_t v332 = svsub_f32_x(svptrue_b32(), v314, v324);
  svfloat32_t v333 = svadd_f32_x(svptrue_b32(), v315, v326);
  svfloat32_t v334 = svsub_f32_x(svptrue_b32(), v316, v328);
  svfloat32_t v335 = svadd_f32_x(svptrue_b32(), v316, v328);
  svfloat32_t v336 = svsub_f32_x(svptrue_b32(), v315, v326);
  svfloat32_t v337 = svadd_f32_x(svptrue_b32(), v314, v324);
  svfloat32_t v338 = svadd_f32_x(svptrue_b32(), v313, v322);
  svfloat32_t v339 = svsub_f32_x(svptrue_b32(), v312, v320);
  svfloat32_t v340 = svadd_f32_x(svptrue_b32(), v311, v318);
  svint16_t v351 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v329, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v359 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v330, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v367 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v331, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v375 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v332, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v383 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v333, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v391 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v334, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v399 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v335, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v407 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v336, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v415 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v337, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v423 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v338, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v431 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v339, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v439 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v340, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v600), svreinterpret_u64_s16(v351));
  svst1w_u64(pred_full, (unsigned *)(v609), svreinterpret_u64_s16(v359));
  svst1w_u64(pred_full, (unsigned *)(v618), svreinterpret_u64_s16(v367));
  svst1w_u64(pred_full, (unsigned *)(v627), svreinterpret_u64_s16(v375));
  svst1w_u64(pred_full, (unsigned *)(v636), svreinterpret_u64_s16(v383));
  svst1w_u64(pred_full, (unsigned *)(v645), svreinterpret_u64_s16(v391));
  svst1w_u64(pred_full, (unsigned *)(v654), svreinterpret_u64_s16(v399));
  svst1w_u64(pred_full, (unsigned *)(v663), svreinterpret_u64_s16(v407));
  svst1w_u64(pred_full, (unsigned *)(v672), svreinterpret_u64_s16(v415));
  svst1w_u64(pred_full, (unsigned *)(v681), svreinterpret_u64_s16(v423));
  svst1w_u64(pred_full, (unsigned *)(v690), svreinterpret_u64_s16(v431));
  svst1w_u64(pred_full, (unsigned *)(v699), svreinterpret_u64_s16(v439));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun14(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v213 = -1.1666666666666665e+00F;
  float v217 = 7.9015646852540022e-01F;
  float v221 = 5.5854267289647742e-02F;
  float v225 = 7.3430220123575241e-01F;
  float v228 = 4.4095855184409838e-01F;
  float v229 = -4.4095855184409838e-01F;
  float v235 = 3.4087293062393137e-01F;
  float v236 = -3.4087293062393137e-01F;
  float v242 = -5.3396936033772524e-01F;
  float v243 = 5.3396936033772524e-01F;
  float v249 = 8.7484229096165667e-01F;
  float v250 = -8.7484229096165667e-01F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v75 = vld1s_s16(&v5[istride]);
  float32x2_t v214 = (float32x2_t){v213, v213};
  float32x2_t v218 = (float32x2_t){v217, v217};
  float32x2_t v222 = (float32x2_t){v221, v221};
  float32x2_t v226 = (float32x2_t){v225, v225};
  float32x2_t v230 = (float32x2_t){v228, v229};
  float32x2_t v237 = (float32x2_t){v235, v236};
  float32x2_t v244 = (float32x2_t){v242, v243};
  float32x2_t v251 = (float32x2_t){v249, v250};
  float32x2_t v252 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v41 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v47 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v69 = vld1s_s16(&v5[istride * 8]);
  float32x2_t v76 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v75)), 15);
  int16x4_t v83 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v89 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v97 = vld1s_s16(&v5[istride * 12]);
  int16x4_t v103 = vld1s_s16(&v5[istride * 5]);
  float32x2_t v232 = vmul_f32(v252, v230);
  float32x2_t v239 = vmul_f32(v252, v237);
  float32x2_t v246 = vmul_f32(v252, v244);
  float32x2_t v253 = vmul_f32(v252, v251);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  float32x2_t v84 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v83)), 15);
  float32x2_t v90 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v89)), 15);
  float32x2_t v98 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v97)), 15);
  float32x2_t v104 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v103)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v77 = vadd_f32(v70, v76);
  float32x2_t v78 = vsub_f32(v70, v76);
  float32x2_t v91 = vadd_f32(v84, v90);
  float32x2_t v92 = vsub_f32(v84, v90);
  float32x2_t v105 = vadd_f32(v98, v104);
  float32x2_t v106 = vsub_f32(v98, v104);
  float32x2_t v107 = vadd_f32(v35, v105);
  float32x2_t v108 = vsub_f32(v35, v105);
  float32x2_t v109 = vadd_f32(v77, v63);
  float32x2_t v110 = vsub_f32(v77, v63);
  float32x2_t v111 = vadd_f32(v49, v91);
  float32x2_t v112 = vsub_f32(v49, v91);
  float32x2_t v191 = vadd_f32(v36, v106);
  float32x2_t v192 = vsub_f32(v36, v106);
  float32x2_t v193 = vadd_f32(v78, v64);
  float32x2_t v194 = vsub_f32(v78, v64);
  float32x2_t v195 = vadd_f32(v50, v92);
  float32x2_t v196 = vsub_f32(v50, v92);
  float32x2_t v113 = vadd_f32(v107, v109);
  float32x2_t v116 = vsub_f32(v107, v109);
  float32x2_t v117 = vsub_f32(v109, v111);
  float32x2_t v118 = vsub_f32(v111, v107);
  float32x2_t v119 = vadd_f32(v108, v110);
  float32x2_t v121 = vsub_f32(v108, v110);
  float32x2_t v122 = vsub_f32(v110, v112);
  float32x2_t v123 = vsub_f32(v112, v108);
  float32x2_t v197 = vadd_f32(v191, v193);
  float32x2_t v200 = vsub_f32(v191, v193);
  float32x2_t v201 = vsub_f32(v193, v195);
  float32x2_t v202 = vsub_f32(v195, v191);
  float32x2_t v203 = vadd_f32(v192, v194);
  float32x2_t v205 = vsub_f32(v192, v194);
  float32x2_t v206 = vsub_f32(v194, v196);
  float32x2_t v207 = vsub_f32(v196, v192);
  float32x2_t v114 = vadd_f32(v113, v111);
  float32x2_t v120 = vadd_f32(v119, v112);
  float32x2_t v135 = vmul_f32(v116, v218);
  float32x2_t v139 = vmul_f32(v117, v222);
  float32x2_t v143 = vmul_f32(v118, v226);
  float32x2_t v156 = vrev64_f32(v121);
  float32x2_t v163 = vrev64_f32(v122);
  float32x2_t v170 = vrev64_f32(v123);
  float32x2_t v198 = vadd_f32(v197, v195);
  float32x2_t v204 = vadd_f32(v203, v196);
  float32x2_t v219 = vmul_f32(v200, v218);
  float32x2_t v223 = vmul_f32(v201, v222);
  float32x2_t v227 = vmul_f32(v202, v226);
  float32x2_t v240 = vrev64_f32(v205);
  float32x2_t v247 = vrev64_f32(v206);
  float32x2_t v254 = vrev64_f32(v207);
  float32x2_t v115 = vadd_f32(v114, v21);
  float32x2_t v131 = vmul_f32(v114, v214);
  float32x2_t v149 = vrev64_f32(v120);
  float32x2_t v157 = vmul_f32(v156, v239);
  float32x2_t v164 = vmul_f32(v163, v246);
  float32x2_t v171 = vmul_f32(v170, v253);
  float32x2_t v199 = vadd_f32(v198, v22);
  float32x2_t v215 = vmul_f32(v198, v214);
  float32x2_t v233 = vrev64_f32(v204);
  float32x2_t v241 = vmul_f32(v240, v239);
  float32x2_t v248 = vmul_f32(v247, v246);
  float32x2_t v255 = vmul_f32(v254, v253);
  float32x2_t v150 = vmul_f32(v149, v232);
  float32x2_t v172 = vadd_f32(v115, v131);
  float32x2_t v234 = vmul_f32(v233, v232);
  float32x2_t v256 = vadd_f32(v199, v215);
  int16x4_t v277 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v115, 15), (int32x2_t){0, 0}));
  int16x4_t v283 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v199, 15), (int32x2_t){0, 0}));
  float32x2_t v173 = vadd_f32(v172, v135);
  float32x2_t v175 = vsub_f32(v172, v135);
  float32x2_t v177 = vsub_f32(v172, v139);
  float32x2_t v179 = vadd_f32(v150, v157);
  float32x2_t v181 = vsub_f32(v150, v157);
  float32x2_t v183 = vsub_f32(v150, v164);
  float32x2_t v257 = vadd_f32(v256, v219);
  float32x2_t v259 = vsub_f32(v256, v219);
  float32x2_t v261 = vsub_f32(v256, v223);
  float32x2_t v263 = vadd_f32(v234, v241);
  float32x2_t v265 = vsub_f32(v234, v241);
  float32x2_t v267 = vsub_f32(v234, v248);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v277), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v283), 0);
  float32x2_t v174 = vadd_f32(v173, v139);
  float32x2_t v176 = vsub_f32(v175, v143);
  float32x2_t v178 = vadd_f32(v177, v143);
  float32x2_t v180 = vadd_f32(v179, v164);
  float32x2_t v182 = vsub_f32(v181, v171);
  float32x2_t v184 = vadd_f32(v183, v171);
  float32x2_t v258 = vadd_f32(v257, v223);
  float32x2_t v260 = vsub_f32(v259, v227);
  float32x2_t v262 = vadd_f32(v261, v227);
  float32x2_t v264 = vadd_f32(v263, v248);
  float32x2_t v266 = vsub_f32(v265, v255);
  float32x2_t v268 = vadd_f32(v267, v255);
  float32x2_t v185 = vadd_f32(v174, v180);
  float32x2_t v186 = vsub_f32(v174, v180);
  float32x2_t v187 = vadd_f32(v176, v182);
  float32x2_t v188 = vsub_f32(v176, v182);
  float32x2_t v189 = vadd_f32(v178, v184);
  float32x2_t v190 = vsub_f32(v178, v184);
  float32x2_t v269 = vadd_f32(v258, v264);
  float32x2_t v270 = vsub_f32(v258, v264);
  float32x2_t v271 = vadd_f32(v260, v266);
  float32x2_t v272 = vsub_f32(v260, v266);
  float32x2_t v273 = vadd_f32(v262, v268);
  float32x2_t v274 = vsub_f32(v262, v268);
  int16x4_t v289 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v186, 15), (int32x2_t){0, 0}));
  int16x4_t v295 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v270, 15), (int32x2_t){0, 0}));
  int16x4_t v301 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v188, 15), (int32x2_t){0, 0}));
  int16x4_t v307 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v272, 15), (int32x2_t){0, 0}));
  int16x4_t v313 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v189, 15), (int32x2_t){0, 0}));
  int16x4_t v319 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v273, 15), (int32x2_t){0, 0}));
  int16x4_t v325 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v190, 15), (int32x2_t){0, 0}));
  int16x4_t v331 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v274, 15), (int32x2_t){0, 0}));
  int16x4_t v337 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v187, 15), (int32x2_t){0, 0}));
  int16x4_t v343 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v271, 15), (int32x2_t){0, 0}));
  int16x4_t v349 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v185, 15), (int32x2_t){0, 0}));
  int16x4_t v355 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v269, 15), (int32x2_t){0, 0}));
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v289), 0);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v295), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v301), 0);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v307), 0);
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v313), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v319), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v325), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v331), 0);
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v337), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v343), 0);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v349), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v355), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun14(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v252 = -1.1666666666666665e+00F;
  float v257 = 7.9015646852540022e-01F;
  float v262 = 5.5854267289647742e-02F;
  float v267 = 7.3430220123575241e-01F;
  float v272 = -4.4095855184409838e-01F;
  float v279 = -3.4087293062393137e-01F;
  float v286 = 5.3396936033772524e-01F;
  float v293 = -8.7484229096165667e-01F;
  const int32_t *v518 = &v5[v0];
  int32_t *v609 = &v6[v2];
  int64_t v23 = v0 * 7;
  int64_t v33 = v0 * 2;
  int64_t v41 = v0 * 9;
  int64_t v51 = v0 * 4;
  int64_t v59 = v0 * 11;
  int64_t v69 = v0 * 6;
  int64_t v77 = v0 * 13;
  int64_t v87 = v0 * 8;
  int64_t v105 = v0 * 10;
  int64_t v113 = v0 * 3;
  int64_t v123 = v0 * 12;
  int64_t v131 = v0 * 5;
  float v275 = v4 * v272;
  float v282 = v4 * v279;
  float v289 = v4 * v286;
  float v296 = v4 * v293;
  int64_t v327 = v2 * 7;
  int64_t v335 = v2 * 8;
  int64_t v351 = v2 * 2;
  int64_t v359 = v2 * 9;
  int64_t v367 = v2 * 10;
  int64_t v375 = v2 * 3;
  int64_t v383 = v2 * 4;
  int64_t v391 = v2 * 11;
  int64_t v399 = v2 * 12;
  int64_t v407 = v2 * 5;
  int64_t v415 = v2 * 6;
  int64_t v423 = v2 * 13;
  const int32_t *v437 = &v5[0];
  svfloat32_t v567 = svdup_n_f32(v252);
  svfloat32_t v568 = svdup_n_f32(v257);
  svfloat32_t v569 = svdup_n_f32(v262);
  svfloat32_t v570 = svdup_n_f32(v267);
  int32_t *v582 = &v6[0];
  svfloat32_t v101 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v518[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v446 = &v5[v23];
  const int32_t *v455 = &v5[v33];
  const int32_t *v464 = &v5[v41];
  const int32_t *v473 = &v5[v51];
  const int32_t *v482 = &v5[v59];
  const int32_t *v491 = &v5[v69];
  const int32_t *v500 = &v5[v77];
  const int32_t *v509 = &v5[v87];
  const int32_t *v527 = &v5[v105];
  const int32_t *v536 = &v5[v113];
  const int32_t *v545 = &v5[v123];
  const int32_t *v554 = &v5[v131];
  svfloat32_t v571 = svdup_n_f32(v275);
  svfloat32_t v572 = svdup_n_f32(v282);
  svfloat32_t v573 = svdup_n_f32(v289);
  svfloat32_t v574 = svdup_n_f32(v296);
  int32_t *v591 = &v6[v327];
  int32_t *v600 = &v6[v335];
  int32_t *v618 = &v6[v351];
  int32_t *v627 = &v6[v359];
  int32_t *v636 = &v6[v367];
  int32_t *v645 = &v6[v375];
  int32_t *v654 = &v6[v383];
  int32_t *v663 = &v6[v391];
  int32_t *v672 = &v6[v399];
  int32_t *v681 = &v6[v407];
  int32_t *v690 = &v6[v415];
  int32_t *v699 = &v6[v423];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v437[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v446[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v455[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v464[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v473[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v482[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v491[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v500[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v509[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v111 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v527[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v119 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v536[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v129 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v545[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v137 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v554[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v120 = svadd_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v121 = svsub_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v140 = svadd_f32_x(svptrue_b32(), v48, v138);
  svfloat32_t v141 = svsub_f32_x(svptrue_b32(), v48, v138);
  svfloat32_t v142 = svadd_f32_x(svptrue_b32(), v102, v84);
  svfloat32_t v143 = svsub_f32_x(svptrue_b32(), v102, v84);
  svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v66, v120);
  svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v66, v120);
  svfloat32_t v229 = svadd_f32_x(svptrue_b32(), v49, v139);
  svfloat32_t v230 = svsub_f32_x(svptrue_b32(), v49, v139);
  svfloat32_t v231 = svadd_f32_x(svptrue_b32(), v103, v85);
  svfloat32_t v232 = svsub_f32_x(svptrue_b32(), v103, v85);
  svfloat32_t v233 = svadd_f32_x(svptrue_b32(), v67, v121);
  svfloat32_t v234 = svsub_f32_x(svptrue_b32(), v67, v121);
  svfloat32_t v146 = svadd_f32_x(svptrue_b32(), v140, v142);
  svfloat32_t v149 = svsub_f32_x(svptrue_b32(), v140, v142);
  svfloat32_t v150 = svsub_f32_x(svptrue_b32(), v142, v144);
  svfloat32_t v151 = svsub_f32_x(svptrue_b32(), v144, v140);
  svfloat32_t v152 = svadd_f32_x(svptrue_b32(), v141, v143);
  svfloat32_t v154 = svsub_f32_x(svptrue_b32(), v141, v143);
  svfloat32_t v155 = svsub_f32_x(svptrue_b32(), v143, v145);
  svfloat32_t v156 = svsub_f32_x(svptrue_b32(), v145, v141);
  svfloat32_t v235 = svadd_f32_x(svptrue_b32(), v229, v231);
  svfloat32_t v238 = svsub_f32_x(svptrue_b32(), v229, v231);
  svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v231, v233);
  svfloat32_t v240 = svsub_f32_x(svptrue_b32(), v233, v229);
  svfloat32_t v241 = svadd_f32_x(svptrue_b32(), v230, v232);
  svfloat32_t v243 = svsub_f32_x(svptrue_b32(), v230, v232);
  svfloat32_t v244 = svsub_f32_x(svptrue_b32(), v232, v234);
  svfloat32_t v245 = svsub_f32_x(svptrue_b32(), v234, v230);
  svfloat32_t v147 = svadd_f32_x(svptrue_b32(), v146, v144);
  svfloat32_t v153 = svadd_f32_x(svptrue_b32(), v152, v145);
  svfloat32_t zero195 = svdup_n_f32(0);
  svfloat32_t v195 = svcmla_f32_x(pred_full, zero195, v572, v154, 90);
  svfloat32_t zero202 = svdup_n_f32(0);
  svfloat32_t v202 = svcmla_f32_x(pred_full, zero202, v573, v155, 90);
  svfloat32_t zero209 = svdup_n_f32(0);
  svfloat32_t v209 = svcmla_f32_x(pred_full, zero209, v574, v156, 90);
  svfloat32_t v236 = svadd_f32_x(svptrue_b32(), v235, v233);
  svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v241, v234);
  svfloat32_t zero284 = svdup_n_f32(0);
  svfloat32_t v284 = svcmla_f32_x(pred_full, zero284, v572, v243, 90);
  svfloat32_t zero291 = svdup_n_f32(0);
  svfloat32_t v291 = svcmla_f32_x(pred_full, zero291, v573, v244, 90);
  svfloat32_t zero298 = svdup_n_f32(0);
  svfloat32_t v298 = svcmla_f32_x(pred_full, zero298, v574, v245, 90);
  svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v147, v30);
  svfloat32_t zero188 = svdup_n_f32(0);
  svfloat32_t v188 = svcmla_f32_x(pred_full, zero188, v571, v153, 90);
  svfloat32_t v237 = svadd_f32_x(svptrue_b32(), v236, v31);
  svfloat32_t zero277 = svdup_n_f32(0);
  svfloat32_t v277 = svcmla_f32_x(pred_full, zero277, v571, v242, 90);
  svfloat32_t v210 = svmla_f32_x(pred_full, v148, v147, v567);
  svfloat32_t v217 = svadd_f32_x(svptrue_b32(), v188, v195);
  svfloat32_t v219 = svsub_f32_x(svptrue_b32(), v188, v195);
  svfloat32_t v221 = svsub_f32_x(svptrue_b32(), v188, v202);
  svfloat32_t v299 = svmla_f32_x(pred_full, v237, v236, v567);
  svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v277, v284);
  svfloat32_t v308 = svsub_f32_x(svptrue_b32(), v277, v284);
  svfloat32_t v310 = svsub_f32_x(svptrue_b32(), v277, v291);
  svint16_t v320 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v148, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v328 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v237, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v211 = svmla_f32_x(pred_full, v210, v149, v568);
  svfloat32_t v213 = svmls_f32_x(pred_full, v210, v149, v568);
  svfloat32_t v215 = svmls_f32_x(pred_full, v210, v150, v569);
  svfloat32_t v218 = svadd_f32_x(svptrue_b32(), v217, v202);
  svfloat32_t v220 = svsub_f32_x(svptrue_b32(), v219, v209);
  svfloat32_t v222 = svadd_f32_x(svptrue_b32(), v221, v209);
  svfloat32_t v300 = svmla_f32_x(pred_full, v299, v238, v568);
  svfloat32_t v302 = svmls_f32_x(pred_full, v299, v238, v568);
  svfloat32_t v304 = svmls_f32_x(pred_full, v299, v239, v569);
  svfloat32_t v307 = svadd_f32_x(svptrue_b32(), v306, v291);
  svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v308, v298);
  svfloat32_t v311 = svadd_f32_x(svptrue_b32(), v310, v298);
  svst1w_u64(pred_full, (unsigned *)(v582), svreinterpret_u64_s16(v320));
  svst1w_u64(pred_full, (unsigned *)(v591), svreinterpret_u64_s16(v328));
  svfloat32_t v212 = svmla_f32_x(pred_full, v211, v150, v569);
  svfloat32_t v214 = svmls_f32_x(pred_full, v213, v151, v570);
  svfloat32_t v216 = svmla_f32_x(pred_full, v215, v151, v570);
  svfloat32_t v301 = svmla_f32_x(pred_full, v300, v239, v569);
  svfloat32_t v303 = svmls_f32_x(pred_full, v302, v240, v570);
  svfloat32_t v305 = svmla_f32_x(pred_full, v304, v240, v570);
  svfloat32_t v223 = svadd_f32_x(svptrue_b32(), v212, v218);
  svfloat32_t v224 = svsub_f32_x(svptrue_b32(), v212, v218);
  svfloat32_t v225 = svadd_f32_x(svptrue_b32(), v214, v220);
  svfloat32_t v226 = svsub_f32_x(svptrue_b32(), v214, v220);
  svfloat32_t v227 = svadd_f32_x(svptrue_b32(), v216, v222);
  svfloat32_t v228 = svsub_f32_x(svptrue_b32(), v216, v222);
  svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v301, v307);
  svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v301, v307);
  svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v303, v309);
  svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v303, v309);
  svfloat32_t v316 = svadd_f32_x(svptrue_b32(), v305, v311);
  svfloat32_t v317 = svsub_f32_x(svptrue_b32(), v305, v311);
  svint16_t v336 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v224, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v344 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v313, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v352 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v226, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v360 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v315, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v368 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v227, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v376 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v316, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v384 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v228, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v392 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v317, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v400 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v225, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v408 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v314, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v416 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v223, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v424 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v312, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v600), svreinterpret_u64_s16(v336));
  svst1w_u64(pred_full, (unsigned *)(v609), svreinterpret_u64_s16(v344));
  svst1w_u64(pred_full, (unsigned *)(v618), svreinterpret_u64_s16(v352));
  svst1w_u64(pred_full, (unsigned *)(v627), svreinterpret_u64_s16(v360));
  svst1w_u64(pred_full, (unsigned *)(v636), svreinterpret_u64_s16(v368));
  svst1w_u64(pred_full, (unsigned *)(v645), svreinterpret_u64_s16(v376));
  svst1w_u64(pred_full, (unsigned *)(v654), svreinterpret_u64_s16(v384));
  svst1w_u64(pred_full, (unsigned *)(v663), svreinterpret_u64_s16(v392));
  svst1w_u64(pred_full, (unsigned *)(v672), svreinterpret_u64_s16(v400));
  svst1w_u64(pred_full, (unsigned *)(v681), svreinterpret_u64_s16(v408));
  svst1w_u64(pred_full, (unsigned *)(v690), svreinterpret_u64_s16(v416));
  svst1w_u64(pred_full, (unsigned *)(v699), svreinterpret_u64_s16(v424));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun15(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v127 = -1.2500000000000000e+00F;
  float v131 = 5.5901699437494745e-01F;
  float v134 = 1.5388417685876268e+00F;
  float v135 = -1.5388417685876268e+00F;
  float v141 = 5.8778525229247325e-01F;
  float v142 = -5.8778525229247325e-01F;
  float v148 = 3.6327126400268028e-01F;
  float v149 = -3.6327126400268028e-01F;
  float v173 = -1.4999999999999998e+00F;
  float v177 = 1.8749999999999998e+00F;
  float v181 = -8.3852549156242107e-01F;
  float v184 = -2.3082626528814396e+00F;
  float v185 = 2.3082626528814396e+00F;
  float v191 = -8.8167787843870971e-01F;
  float v192 = 8.8167787843870971e-01F;
  float v198 = -5.4490689600402031e-01F;
  float v199 = 5.4490689600402031e-01F;
  float v222 = 8.6602540378443871e-01F;
  float v223 = -8.6602540378443871e-01F;
  float v229 = -1.0825317547305484e+00F;
  float v230 = 1.0825317547305484e+00F;
  float v236 = 4.8412291827592718e-01F;
  float v237 = -4.8412291827592718e-01F;
  float v244 = -1.3326760640014592e+00F;
  float v248 = -5.0903696045512736e-01F;
  float v252 = -3.1460214309120460e-01F;
  int16x4_t v27 = vld1s_s16(&v5[0]);
  int16x4_t v61 = vld1s_s16(&v5[istride]);
  float32x2_t v128 = (float32x2_t){v127, v127};
  float32x2_t v132 = (float32x2_t){v131, v131};
  float32x2_t v136 = (float32x2_t){v134, v135};
  float32x2_t v143 = (float32x2_t){v141, v142};
  float32x2_t v150 = (float32x2_t){v148, v149};
  float32x2_t v174 = (float32x2_t){v173, v173};
  float32x2_t v178 = (float32x2_t){v177, v177};
  float32x2_t v182 = (float32x2_t){v181, v181};
  float32x2_t v186 = (float32x2_t){v184, v185};
  float32x2_t v193 = (float32x2_t){v191, v192};
  float32x2_t v200 = (float32x2_t){v198, v199};
  float32x2_t v224 = (float32x2_t){v222, v223};
  float32x2_t v231 = (float32x2_t){v229, v230};
  float32x2_t v238 = (float32x2_t){v236, v237};
  float32x2_t v239 = (float32x2_t){v4, v4};
  float32x2_t v245 = (float32x2_t){v244, v244};
  float32x2_t v249 = (float32x2_t){v248, v248};
  float32x2_t v253 = (float32x2_t){v252, v252};
  int16x4_t v13 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v19 = vld1s_s16(&v5[istride * 10]);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  int16x4_t v34 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v40 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v48 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 11]);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  int16x4_t v69 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v76 = vld1s_s16(&v5[istride * 14]);
  int16x4_t v82 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v90 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v97 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v103 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v111 = vld1s_s16(&v5[istride * 12]);
  float32x2_t v138 = vmul_f32(v239, v136);
  float32x2_t v145 = vmul_f32(v239, v143);
  float32x2_t v152 = vmul_f32(v239, v150);
  float32x2_t v188 = vmul_f32(v239, v186);
  float32x2_t v195 = vmul_f32(v239, v193);
  float32x2_t v202 = vmul_f32(v239, v200);
  float32x2_t v226 = vmul_f32(v239, v224);
  float32x2_t v233 = vmul_f32(v239, v231);
  float32x2_t v240 = vmul_f32(v239, v238);
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v35 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v34)), 15);
  float32x2_t v41 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v40)), 15);
  float32x2_t v49 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v48)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  float32x2_t v77 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v76)), 15);
  float32x2_t v83 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v82)), 15);
  float32x2_t v91 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v90)), 15);
  float32x2_t v98 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v97)), 15);
  float32x2_t v104 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v103)), 15);
  float32x2_t v112 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v111)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v42 = vadd_f32(v35, v41);
  float32x2_t v43 = vsub_f32(v35, v41);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v84 = vadd_f32(v77, v83);
  float32x2_t v85 = vsub_f32(v77, v83);
  float32x2_t v105 = vadd_f32(v98, v104);
  float32x2_t v106 = vsub_f32(v98, v104);
  float32x2_t v29 = vadd_f32(v21, v28);
  float32x2_t v50 = vadd_f32(v42, v49);
  float32x2_t v71 = vadd_f32(v63, v70);
  float32x2_t v92 = vadd_f32(v84, v91);
  float32x2_t v113 = vadd_f32(v105, v112);
  float32x2_t v164 = vadd_f32(v42, v105);
  float32x2_t v165 = vsub_f32(v42, v105);
  float32x2_t v166 = vadd_f32(v84, v63);
  float32x2_t v167 = vsub_f32(v84, v63);
  float32x2_t v214 = vadd_f32(v43, v106);
  float32x2_t v215 = vsub_f32(v43, v106);
  float32x2_t v216 = vadd_f32(v85, v64);
  float32x2_t v217 = vsub_f32(v85, v64);
  float32x2_t v114 = vadd_f32(v50, v113);
  float32x2_t v115 = vsub_f32(v50, v113);
  float32x2_t v116 = vadd_f32(v92, v71);
  float32x2_t v117 = vsub_f32(v92, v71);
  float32x2_t v168 = vadd_f32(v164, v166);
  float32x2_t v169 = vsub_f32(v164, v166);
  float32x2_t v170 = vadd_f32(v165, v167);
  float32x2_t v189 = vrev64_f32(v165);
  float32x2_t v203 = vrev64_f32(v167);
  float32x2_t v218 = vadd_f32(v214, v216);
  float32x2_t v219 = vsub_f32(v214, v216);
  float32x2_t v220 = vadd_f32(v215, v217);
  float32x2_t v246 = vmul_f32(v215, v245);
  float32x2_t v254 = vmul_f32(v217, v253);
  float32x2_t v118 = vadd_f32(v114, v116);
  float32x2_t v119 = vsub_f32(v114, v116);
  float32x2_t v120 = vadd_f32(v115, v117);
  float32x2_t v139 = vrev64_f32(v115);
  float32x2_t v153 = vrev64_f32(v117);
  float32x2_t v171 = vadd_f32(v168, v21);
  float32x2_t v179 = vmul_f32(v168, v178);
  float32x2_t v183 = vmul_f32(v169, v182);
  float32x2_t v190 = vmul_f32(v189, v188);
  float32x2_t v196 = vrev64_f32(v170);
  float32x2_t v204 = vmul_f32(v203, v202);
  float32x2_t v221 = vadd_f32(v218, v22);
  float32x2_t v234 = vrev64_f32(v218);
  float32x2_t v241 = vrev64_f32(v219);
  float32x2_t v250 = vmul_f32(v220, v249);
  float32x2_t v121 = vadd_f32(v118, v29);
  float32x2_t v129 = vmul_f32(v118, v128);
  float32x2_t v133 = vmul_f32(v119, v132);
  float32x2_t v140 = vmul_f32(v139, v138);
  float32x2_t v146 = vrev64_f32(v120);
  float32x2_t v154 = vmul_f32(v153, v152);
  float32x2_t v175 = vmul_f32(v171, v174);
  float32x2_t v197 = vmul_f32(v196, v195);
  float32x2_t v227 = vrev64_f32(v221);
  float32x2_t v235 = vmul_f32(v234, v233);
  float32x2_t v242 = vmul_f32(v241, v240);
  float32x2_t v258 = vsub_f32(v246, v250);
  float32x2_t v259 = vadd_f32(v250, v254);
  float32x2_t v147 = vmul_f32(v146, v145);
  float32x2_t v155 = vadd_f32(v121, v129);
  float32x2_t v205 = vadd_f32(v175, v179);
  float32x2_t v208 = vsub_f32(v190, v197);
  float32x2_t v209 = vadd_f32(v197, v204);
  float32x2_t v228 = vmul_f32(v227, v226);
  float32x2_t v264 = vadd_f32(v121, v175);
  int16x4_t v269 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v121, 15), (int32x2_t){0, 0}));
  float32x2_t v156 = vadd_f32(v155, v133);
  float32x2_t v157 = vsub_f32(v155, v133);
  float32x2_t v158 = vsub_f32(v140, v147);
  float32x2_t v159 = vadd_f32(v147, v154);
  float32x2_t v206 = vadd_f32(v205, v183);
  float32x2_t v207 = vsub_f32(v205, v183);
  float32x2_t v255 = vadd_f32(v228, v235);
  float32x2_t v265 = vadd_f32(v264, v228);
  float32x2_t v266 = vsub_f32(v264, v228);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v269), 0);
  float32x2_t v160 = vadd_f32(v156, v158);
  float32x2_t v161 = vsub_f32(v156, v158);
  float32x2_t v162 = vadd_f32(v157, v159);
  float32x2_t v163 = vsub_f32(v157, v159);
  float32x2_t v210 = vadd_f32(v206, v208);
  float32x2_t v211 = vsub_f32(v206, v208);
  float32x2_t v212 = vadd_f32(v207, v209);
  float32x2_t v213 = vsub_f32(v207, v209);
  float32x2_t v256 = vadd_f32(v255, v242);
  float32x2_t v257 = vsub_f32(v255, v242);
  int16x4_t v275 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v266, 15), (int32x2_t){0, 0}));
  int16x4_t v281 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v265, 15), (int32x2_t){0, 0}));
  float32x2_t v260 = vadd_f32(v256, v258);
  float32x2_t v261 = vsub_f32(v256, v258);
  float32x2_t v262 = vadd_f32(v257, v259);
  float32x2_t v263 = vsub_f32(v257, v259);
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v275), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v281), 0);
  float32x2_t v285 = vadd_f32(v161, v211);
  int16x4_t v290 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v161, 15), (int32x2_t){0, 0}));
  float32x2_t v306 = vadd_f32(v163, v213);
  int16x4_t v311 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v163, 15), (int32x2_t){0, 0}));
  float32x2_t v327 = vadd_f32(v162, v212);
  int16x4_t v332 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v162, 15), (int32x2_t){0, 0}));
  float32x2_t v348 = vadd_f32(v160, v210);
  int16x4_t v353 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v160, 15), (int32x2_t){0, 0}));
  float32x2_t v286 = vadd_f32(v285, v261);
  float32x2_t v287 = vsub_f32(v285, v261);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v290), 0);
  float32x2_t v307 = vadd_f32(v306, v263);
  float32x2_t v308 = vsub_f32(v306, v263);
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v311), 0);
  float32x2_t v328 = vadd_f32(v327, v262);
  float32x2_t v329 = vsub_f32(v327, v262);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v332), 0);
  float32x2_t v349 = vadd_f32(v348, v260);
  float32x2_t v350 = vsub_f32(v348, v260);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v353), 0);
  int16x4_t v296 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v287, 15), (int32x2_t){0, 0}));
  int16x4_t v302 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v286, 15), (int32x2_t){0, 0}));
  int16x4_t v317 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v308, 15), (int32x2_t){0, 0}));
  int16x4_t v323 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v307, 15), (int32x2_t){0, 0}));
  int16x4_t v338 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v329, 15), (int32x2_t){0, 0}));
  int16x4_t v344 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v328, 15), (int32x2_t){0, 0}));
  int16x4_t v359 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v350, 15), (int32x2_t){0, 0}));
  int16x4_t v365 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v349, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v296), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v302), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v317), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v323), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v338), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v344), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v359), 0);
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v365), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun15(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v163 = -1.2500000000000000e+00F;
  float v168 = 5.5901699437494745e-01F;
  float v173 = -1.5388417685876268e+00F;
  float v180 = -5.8778525229247325e-01F;
  float v187 = -3.6327126400268028e-01F;
  float v211 = -1.4999999999999998e+00F;
  float v216 = 1.8749999999999998e+00F;
  float v221 = -8.3852549156242107e-01F;
  float v226 = 2.3082626528814396e+00F;
  float v233 = 8.8167787843870971e-01F;
  float v240 = 5.4490689600402031e-01F;
  float v264 = -8.6602540378443871e-01F;
  float v271 = 1.0825317547305484e+00F;
  float v278 = -4.8412291827592718e-01F;
  float v285 = -1.3326760640014592e+00F;
  float v290 = -5.0903696045512736e-01F;
  float v295 = -3.1460214309120460e-01F;
  const int32_t *v513 = &v5[v0];
  int32_t *v640 = &v6[v2];
  int64_t v15 = v0 * 5;
  int64_t v23 = v0 * 10;
  int64_t v42 = v0 * 8;
  int64_t v50 = v0 * 13;
  int64_t v60 = v0 * 3;
  int64_t v69 = v0 * 11;
  int64_t v87 = v0 * 6;
  int64_t v96 = v0 * 14;
  int64_t v104 = v0 * 4;
  int64_t v114 = v0 * 9;
  int64_t v123 = v0 * 2;
  int64_t v131 = v0 * 7;
  int64_t v141 = v0 * 12;
  float v176 = v4 * v173;
  float v183 = v4 * v180;
  float v190 = v4 * v187;
  float v229 = v4 * v226;
  float v236 = v4 * v233;
  float v243 = v4 * v240;
  float v267 = v4 * v264;
  float v274 = v4 * v271;
  float v281 = v4 * v278;
  int64_t v320 = v2 * 10;
  int64_t v328 = v2 * 5;
  int64_t v339 = v2 * 6;
  int64_t v355 = v2 * 11;
  int64_t v366 = v2 * 12;
  int64_t v374 = v2 * 7;
  int64_t v382 = v2 * 2;
  int64_t v393 = v2 * 3;
  int64_t v401 = v2 * 13;
  int64_t v409 = v2 * 8;
  int64_t v420 = v2 * 9;
  int64_t v428 = v2 * 4;
  int64_t v436 = v2 * 14;
  const int32_t *v468 = &v5[0];
  svfloat32_t v580 = svdup_n_f32(v163);
  svfloat32_t v581 = svdup_n_f32(v168);
  svfloat32_t v585 = svdup_n_f32(v211);
  svfloat32_t v586 = svdup_n_f32(v216);
  svfloat32_t v587 = svdup_n_f32(v221);
  svfloat32_t v594 = svdup_n_f32(v285);
  svfloat32_t v595 = svdup_n_f32(v290);
  svfloat32_t v596 = svdup_n_f32(v295);
  int32_t *v604 = &v6[0];
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v513[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v449 = &v5[v15];
  const int32_t *v458 = &v5[v23];
  const int32_t *v477 = &v5[v42];
  const int32_t *v486 = &v5[v50];
  const int32_t *v495 = &v5[v60];
  const int32_t *v504 = &v5[v69];
  const int32_t *v522 = &v5[v87];
  const int32_t *v531 = &v5[v96];
  const int32_t *v540 = &v5[v104];
  const int32_t *v549 = &v5[v114];
  const int32_t *v558 = &v5[v123];
  const int32_t *v567 = &v5[v131];
  const int32_t *v576 = &v5[v141];
  svfloat32_t v582 = svdup_n_f32(v176);
  svfloat32_t v583 = svdup_n_f32(v183);
  svfloat32_t v584 = svdup_n_f32(v190);
  svfloat32_t v588 = svdup_n_f32(v229);
  svfloat32_t v589 = svdup_n_f32(v236);
  svfloat32_t v590 = svdup_n_f32(v243);
  svfloat32_t v591 = svdup_n_f32(v267);
  svfloat32_t v592 = svdup_n_f32(v274);
  svfloat32_t v593 = svdup_n_f32(v281);
  int32_t *v613 = &v6[v320];
  int32_t *v622 = &v6[v328];
  int32_t *v631 = &v6[v339];
  int32_t *v649 = &v6[v355];
  int32_t *v658 = &v6[v366];
  int32_t *v667 = &v6[v374];
  int32_t *v676 = &v6[v382];
  int32_t *v685 = &v6[v393];
  int32_t *v694 = &v6[v401];
  int32_t *v703 = &v6[v409];
  int32_t *v712 = &v6[v420];
  int32_t *v721 = &v6[v428];
  int32_t *v730 = &v6[v436];
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v468[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v449[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v458[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v48 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v477[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v56 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v486[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v66 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v495[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v504[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v522[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v102 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v531[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v110 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v540[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v120 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v549[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v129 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v558[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v137 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v567[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v147 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v576[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v57 = svadd_f32_x(svptrue_b32(), v48, v56);
  svfloat32_t v58 = svsub_f32_x(svptrue_b32(), v48, v56);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v111 = svadd_f32_x(svptrue_b32(), v102, v110);
  svfloat32_t v112 = svsub_f32_x(svptrue_b32(), v102, v110);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v40 = svadd_f32_x(svptrue_b32(), v30, v39);
  svfloat32_t v67 = svadd_f32_x(svptrue_b32(), v57, v66);
  svfloat32_t v94 = svadd_f32_x(svptrue_b32(), v84, v93);
  svfloat32_t v121 = svadd_f32_x(svptrue_b32(), v111, v120);
  svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v138, v147);
  svfloat32_t v202 = svadd_f32_x(svptrue_b32(), v57, v138);
  svfloat32_t v203 = svsub_f32_x(svptrue_b32(), v57, v138);
  svfloat32_t v204 = svadd_f32_x(svptrue_b32(), v111, v84);
  svfloat32_t v205 = svsub_f32_x(svptrue_b32(), v111, v84);
  svfloat32_t v255 = svadd_f32_x(svptrue_b32(), v58, v139);
  svfloat32_t v256 = svsub_f32_x(svptrue_b32(), v58, v139);
  svfloat32_t v257 = svadd_f32_x(svptrue_b32(), v112, v85);
  svfloat32_t v258 = svsub_f32_x(svptrue_b32(), v112, v85);
  svfloat32_t v149 = svadd_f32_x(svptrue_b32(), v67, v148);
  svfloat32_t v150 = svsub_f32_x(svptrue_b32(), v67, v148);
  svfloat32_t v151 = svadd_f32_x(svptrue_b32(), v121, v94);
  svfloat32_t v152 = svsub_f32_x(svptrue_b32(), v121, v94);
  svfloat32_t v206 = svadd_f32_x(svptrue_b32(), v202, v204);
  svfloat32_t v207 = svsub_f32_x(svptrue_b32(), v202, v204);
  svfloat32_t v208 = svadd_f32_x(svptrue_b32(), v203, v205);
  svfloat32_t zero231 = svdup_n_f32(0);
  svfloat32_t v231 = svcmla_f32_x(pred_full, zero231, v588, v203, 90);
  svfloat32_t v259 = svadd_f32_x(svptrue_b32(), v255, v257);
  svfloat32_t v260 = svsub_f32_x(svptrue_b32(), v255, v257);
  svfloat32_t v261 = svadd_f32_x(svptrue_b32(), v256, v258);
  svfloat32_t v298 = svmul_f32_x(svptrue_b32(), v258, v596);
  svfloat32_t v153 = svadd_f32_x(svptrue_b32(), v149, v151);
  svfloat32_t v154 = svsub_f32_x(svptrue_b32(), v149, v151);
  svfloat32_t v155 = svadd_f32_x(svptrue_b32(), v150, v152);
  svfloat32_t zero178 = svdup_n_f32(0);
  svfloat32_t v178 = svcmla_f32_x(pred_full, zero178, v582, v150, 90);
  svfloat32_t v209 = svadd_f32_x(svptrue_b32(), v206, v30);
  svfloat32_t v219 = svmul_f32_x(svptrue_b32(), v206, v586);
  svfloat32_t zero238 = svdup_n_f32(0);
  svfloat32_t v238 = svcmla_f32_x(pred_full, zero238, v589, v208, 90);
  svfloat32_t v262 = svadd_f32_x(svptrue_b32(), v259, v31);
  svfloat32_t zero283 = svdup_n_f32(0);
  svfloat32_t v283 = svcmla_f32_x(pred_full, zero283, v593, v260, 90);
  svfloat32_t v293 = svmul_f32_x(svptrue_b32(), v261, v595);
  svfloat32_t v156 = svadd_f32_x(svptrue_b32(), v153, v40);
  svfloat32_t zero185 = svdup_n_f32(0);
  svfloat32_t v185 = svcmla_f32_x(pred_full, zero185, v583, v155, 90);
  svfloat32_t v249 = svsub_f32_x(svptrue_b32(), v231, v238);
  svfloat32_t v250 = svcmla_f32_x(pred_full, v238, v590, v205, 90);
  svfloat32_t zero269 = svdup_n_f32(0);
  svfloat32_t v269 = svcmla_f32_x(pred_full, zero269, v591, v262, 90);
  svfloat32_t v302 = svnmls_f32_x(pred_full, v293, v256, v594);
  svfloat32_t v303 = svmla_f32_x(pred_full, v298, v261, v595);
  svfloat32_t v193 = svmla_f32_x(pred_full, v156, v153, v580);
  svfloat32_t v196 = svsub_f32_x(svptrue_b32(), v178, v185);
  svfloat32_t v197 = svcmla_f32_x(pred_full, v185, v584, v152, 90);
  svfloat32_t v246 = svmla_f32_x(pred_full, v219, v209, v585);
  svfloat32_t v299 = svcmla_f32_x(pred_full, v269, v592, v259, 90);
  svfloat32_t v308 = svmla_f32_x(pred_full, v156, v209, v585);
  svint16_t v313 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v156, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v194 = svmla_f32_x(pred_full, v193, v154, v581);
  svfloat32_t v195 = svmls_f32_x(pred_full, v193, v154, v581);
  svfloat32_t v247 = svmla_f32_x(pred_full, v246, v207, v587);
  svfloat32_t v248 = svmls_f32_x(pred_full, v246, v207, v587);
  svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v299, v283);
  svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v299, v283);
  svfloat32_t v309 = svadd_f32_x(svptrue_b32(), v308, v269);
  svfloat32_t v310 = svsub_f32_x(svptrue_b32(), v308, v269);
  svst1w_u64(pred_full, (unsigned *)(v604), svreinterpret_u64_s16(v313));
  svfloat32_t v198 = svadd_f32_x(svptrue_b32(), v194, v196);
  svfloat32_t v199 = svsub_f32_x(svptrue_b32(), v194, v196);
  svfloat32_t v200 = svadd_f32_x(svptrue_b32(), v195, v197);
  svfloat32_t v201 = svsub_f32_x(svptrue_b32(), v195, v197);
  svfloat32_t v251 = svadd_f32_x(svptrue_b32(), v247, v249);
  svfloat32_t v252 = svsub_f32_x(svptrue_b32(), v247, v249);
  svfloat32_t v253 = svadd_f32_x(svptrue_b32(), v248, v250);
  svfloat32_t v254 = svsub_f32_x(svptrue_b32(), v248, v250);
  svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v300, v302);
  svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v300, v302);
  svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v301, v303);
  svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v301, v303);
  svint16_t v321 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v310, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v329 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v309, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v335 = svadd_f32_x(svptrue_b32(), v199, v252);
  svint16_t v340 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v199, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v362 = svadd_f32_x(svptrue_b32(), v201, v254);
  svint16_t v367 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v201, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v200, v253);
  svint16_t v394 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v200, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v198, v251);
  svint16_t v421 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v198, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v613), svreinterpret_u64_s16(v321));
  svst1w_u64(pred_full, (unsigned *)(v622), svreinterpret_u64_s16(v329));
  svfloat32_t v336 = svadd_f32_x(svptrue_b32(), v335, v305);
  svfloat32_t v337 = svsub_f32_x(svptrue_b32(), v335, v305);
  svfloat32_t v363 = svadd_f32_x(svptrue_b32(), v362, v307);
  svfloat32_t v364 = svsub_f32_x(svptrue_b32(), v362, v307);
  svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v389, v306);
  svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v389, v306);
  svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v416, v304);
  svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v416, v304);
  svst1w_u64(pred_full, (unsigned *)(v631), svreinterpret_u64_s16(v340));
  svst1w_u64(pred_full, (unsigned *)(v658), svreinterpret_u64_s16(v367));
  svst1w_u64(pred_full, (unsigned *)(v685), svreinterpret_u64_s16(v394));
  svst1w_u64(pred_full, (unsigned *)(v712), svreinterpret_u64_s16(v421));
  svint16_t v348 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v337, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v356 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v336, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v375 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v364, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v383 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v363, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v402 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v391, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v410 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v390, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v429 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v418, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v437 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v417, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v640), svreinterpret_u64_s16(v348));
  svst1w_u64(pred_full, (unsigned *)(v649), svreinterpret_u64_s16(v356));
  svst1w_u64(pred_full, (unsigned *)(v667), svreinterpret_u64_s16(v375));
  svst1w_u64(pred_full, (unsigned *)(v676), svreinterpret_u64_s16(v383));
  svst1w_u64(pred_full, (unsigned *)(v694), svreinterpret_u64_s16(v402));
  svst1w_u64(pred_full, (unsigned *)(v703), svreinterpret_u64_s16(v410));
  svst1w_u64(pred_full, (unsigned *)(v721), svreinterpret_u64_s16(v429));
  svst1w_u64(pred_full, (unsigned *)(v730), svreinterpret_u64_s16(v437));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun16(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v190 = 1.0000000000000000e+00F;
  float v191 = -1.0000000000000000e+00F;
  float v198 = -7.0710678118654746e-01F;
  float v205 = 7.0710678118654757e-01F;
  float v208 = 9.2387953251128674e-01F;
  float v209 = -9.2387953251128674e-01F;
  float v216 = 5.4119610014619690e-01F;
  float v223 = -1.3065629648763766e+00F;
  float v230 = 3.8268343236508984e-01F;
  float v234 = 1.3065629648763766e+00F;
  float v238 = -5.4119610014619690e-01F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v69 = vld1s_s16(&v5[istride]);
  float32x2_t v192 = (float32x2_t){v190, v191};
  float32x2_t v199 = (float32x2_t){v205, v198};
  float32x2_t v206 = (float32x2_t){v205, v205};
  float32x2_t v210 = (float32x2_t){v208, v209};
  float32x2_t v217 = (float32x2_t){v238, v216};
  float32x2_t v224 = (float32x2_t){v234, v223};
  float32x2_t v225 = (float32x2_t){v4, v4};
  float32x2_t v231 = (float32x2_t){v230, v230};
  float32x2_t v235 = (float32x2_t){v234, v234};
  float32x2_t v239 = (float32x2_t){v238, v238};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 12]);
  int16x4_t v41 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v47 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 14]);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  int16x4_t v75 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v83 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v89 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v97 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v103 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v111 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v117 = vld1s_s16(&v5[istride * 15]);
  float32x2_t v194 = vmul_f32(v225, v192);
  float32x2_t v201 = vmul_f32(v225, v199);
  float32x2_t v212 = vmul_f32(v225, v210);
  float32x2_t v219 = vmul_f32(v225, v217);
  float32x2_t v226 = vmul_f32(v225, v224);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v76 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v75)), 15);
  float32x2_t v84 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v83)), 15);
  float32x2_t v90 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v89)), 15);
  float32x2_t v98 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v97)), 15);
  float32x2_t v104 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v103)), 15);
  float32x2_t v112 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v111)), 15);
  float32x2_t v118 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v117)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v77 = vadd_f32(v70, v76);
  float32x2_t v78 = vsub_f32(v70, v76);
  float32x2_t v91 = vadd_f32(v84, v90);
  float32x2_t v92 = vsub_f32(v84, v90);
  float32x2_t v105 = vadd_f32(v98, v104);
  float32x2_t v106 = vsub_f32(v98, v104);
  float32x2_t v119 = vadd_f32(v112, v118);
  float32x2_t v120 = vsub_f32(v112, v118);
  float32x2_t v121 = vadd_f32(v21, v35);
  float32x2_t v122 = vsub_f32(v21, v35);
  float32x2_t v123 = vadd_f32(v49, v63);
  float32x2_t v124 = vsub_f32(v49, v63);
  float32x2_t v125 = vadd_f32(v77, v91);
  float32x2_t v126 = vsub_f32(v77, v91);
  float32x2_t v127 = vadd_f32(v105, v119);
  float32x2_t v128 = vsub_f32(v105, v119);
  float32x2_t v137 = vadd_f32(v50, v64);
  float32x2_t v138 = vsub_f32(v50, v64);
  float32x2_t v139 = vadd_f32(v78, v120);
  float32x2_t v140 = vsub_f32(v78, v120);
  float32x2_t v141 = vadd_f32(v92, v106);
  float32x2_t v142 = vsub_f32(v92, v106);
  float32x2_t v195 = vrev64_f32(v36);
  float32x2_t v129 = vadd_f32(v121, v123);
  float32x2_t v130 = vsub_f32(v121, v123);
  float32x2_t v131 = vadd_f32(v125, v127);
  float32x2_t v132 = vsub_f32(v125, v127);
  float32x2_t v135 = vadd_f32(v126, v128);
  float32x2_t v136 = vsub_f32(v126, v128);
  float32x2_t v143 = vadd_f32(v139, v141);
  float32x2_t v144 = vadd_f32(v140, v142);
  float32x2_t v173 = vrev64_f32(v124);
  float32x2_t v196 = vmul_f32(v195, v194);
  float32x2_t v202 = vrev64_f32(v137);
  float32x2_t v207 = vmul_f32(v138, v206);
  float32x2_t v220 = vrev64_f32(v139);
  float32x2_t v227 = vrev64_f32(v141);
  float32x2_t v236 = vmul_f32(v140, v235);
  float32x2_t v240 = vmul_f32(v142, v239);
  float32x2_t v133 = vadd_f32(v129, v131);
  float32x2_t v134 = vsub_f32(v129, v131);
  float32x2_t v162 = vrev64_f32(v132);
  float32x2_t v174 = vmul_f32(v173, v194);
  float32x2_t v180 = vrev64_f32(v135);
  float32x2_t v185 = vmul_f32(v136, v206);
  float32x2_t v203 = vmul_f32(v202, v201);
  float32x2_t v213 = vrev64_f32(v143);
  float32x2_t v221 = vmul_f32(v220, v219);
  float32x2_t v228 = vmul_f32(v227, v226);
  float32x2_t v232 = vmul_f32(v144, v231);
  float32x2_t v251 = vadd_f32(v22, v207);
  float32x2_t v252 = vsub_f32(v22, v207);
  float32x2_t v163 = vmul_f32(v162, v194);
  float32x2_t v181 = vmul_f32(v180, v201);
  float32x2_t v214 = vmul_f32(v213, v212);
  float32x2_t v243 = vadd_f32(v122, v185);
  float32x2_t v245 = vsub_f32(v122, v185);
  float32x2_t v253 = vadd_f32(v196, v203);
  float32x2_t v254 = vsub_f32(v196, v203);
  float32x2_t v257 = vsub_f32(v236, v232);
  float32x2_t v258 = vsub_f32(v240, v232);
  float32x2_t v259 = vsub_f32(v232, v236);
  float32x2_t v260 = vsub_f32(v232, v240);
  int16x4_t v287 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v133, 15), (int32x2_t){0, 0}));
  int16x4_t v335 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v134, 15), (int32x2_t){0, 0}));
  float32x2_t v241 = vadd_f32(v130, v163);
  float32x2_t v242 = vsub_f32(v130, v163);
  float32x2_t v244 = vadd_f32(v174, v181);
  float32x2_t v246 = vsub_f32(v181, v174);
  float32x2_t v255 = vadd_f32(v214, v221);
  float32x2_t v256 = vsub_f32(v214, v228);
  float32x2_t v261 = vadd_f32(v251, v257);
  float32x2_t v262 = vsub_f32(v251, v257);
  float32x2_t v263 = vadd_f32(v251, v259);
  float32x2_t v264 = vsub_f32(v251, v259);
  float32x2_t v265 = vadd_f32(v252, v254);
  float32x2_t v266 = vsub_f32(v252, v254);
  float32x2_t v267 = vadd_f32(v252, v260);
  float32x2_t v268 = vsub_f32(v252, v260);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v287), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v335), 0);
  float32x2_t v247 = vadd_f32(v243, v244);
  float32x2_t v248 = vadd_f32(v245, v246);
  float32x2_t v249 = vsub_f32(v245, v246);
  float32x2_t v250 = vsub_f32(v243, v244);
  float32x2_t v271 = vadd_f32(v255, v253);
  float32x2_t v272 = vsub_f32(v255, v253);
  float32x2_t v273 = vadd_f32(v256, v258);
  float32x2_t v274 = vsub_f32(v256, v258);
  float32x2_t v275 = vadd_f32(v256, v254);
  float32x2_t v276 = vsub_f32(v256, v254);
  int16x4_t v311 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v242, 15), (int32x2_t){0, 0}));
  int16x4_t v359 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v241, 15), (int32x2_t){0, 0}));
  float32x2_t v277 = vadd_f32(v261, v271);
  float32x2_t v278 = vadd_f32(v262, v272);
  float32x2_t v279 = vsub_f32(v263, v272);
  float32x2_t v280 = vsub_f32(v264, v271);
  float32x2_t v281 = vadd_f32(v265, v273);
  float32x2_t v282 = vadd_f32(v266, v274);
  float32x2_t v283 = vsub_f32(v267, v276);
  float32x2_t v284 = vsub_f32(v268, v275);
  int16x4_t v299 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v250, 15), (int32x2_t){0, 0}));
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v311), 0);
  int16x4_t v323 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v249, 15), (int32x2_t){0, 0}));
  int16x4_t v347 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v248, 15), (int32x2_t){0, 0}));
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v359), 0);
  int16x4_t v371 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v247, 15), (int32x2_t){0, 0}));
  int16x4_t v293 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v280, 15), (int32x2_t){0, 0}));
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v299), 0);
  int16x4_t v305 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v283, 15), (int32x2_t){0, 0}));
  int16x4_t v317 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v284, 15), (int32x2_t){0, 0}));
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v323), 0);
  int16x4_t v329 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v279, 15), (int32x2_t){0, 0}));
  int16x4_t v341 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v278, 15), (int32x2_t){0, 0}));
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v347), 0);
  int16x4_t v353 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v281, 15), (int32x2_t){0, 0}));
  int16x4_t v365 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v282, 15), (int32x2_t){0, 0}));
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v371), 0);
  int16x4_t v377 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v277, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v293), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v305), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v317), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v329), 0);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v341), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v353), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v365), 0);
  v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v377), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun16(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v234 = -1.0000000000000000e+00F;
  float v241 = -7.0710678118654746e-01F;
  float v248 = 7.0710678118654757e-01F;
  float v253 = -9.2387953251128674e-01F;
  float v260 = 5.4119610014619690e-01F;
  float v267 = -1.3065629648763766e+00F;
  float v274 = 3.8268343236508984e-01F;
  float v279 = 1.3065629648763766e+00F;
  float v284 = -5.4119610014619690e-01F;
  const int32_t *v539 = &v5[v0];
  int32_t *v639 = &v6[v2];
  int64_t v23 = v0 * 8;
  int64_t v33 = v0 * 4;
  int64_t v41 = v0 * 12;
  int64_t v51 = v0 * 2;
  int64_t v59 = v0 * 10;
  int64_t v69 = v0 * 6;
  int64_t v77 = v0 * 14;
  int64_t v95 = v0 * 9;
  int64_t v105 = v0 * 5;
  int64_t v113 = v0 * 13;
  int64_t v123 = v0 * 3;
  int64_t v131 = v0 * 11;
  int64_t v141 = v0 * 7;
  int64_t v149 = v0 * 15;
  float v237 = v4 * v234;
  float v244 = v4 * v241;
  float v256 = v4 * v253;
  float v263 = v4 * v260;
  float v270 = v4 * v267;
  int64_t v349 = v2 * 2;
  int64_t v357 = v2 * 3;
  int64_t v365 = v2 * 4;
  int64_t v373 = v2 * 5;
  int64_t v381 = v2 * 6;
  int64_t v389 = v2 * 7;
  int64_t v397 = v2 * 8;
  int64_t v405 = v2 * 9;
  int64_t v413 = v2 * 10;
  int64_t v421 = v2 * 11;
  int64_t v429 = v2 * 12;
  int64_t v437 = v2 * 13;
  int64_t v445 = v2 * 14;
  int64_t v453 = v2 * 15;
  const int32_t *v467 = &v5[0];
  svfloat32_t v616 = svdup_n_f32(v248);
  svfloat32_t v620 = svdup_n_f32(v274);
  svfloat32_t v621 = svdup_n_f32(v279);
  svfloat32_t v622 = svdup_n_f32(v284);
  int32_t *v630 = &v6[0];
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v539[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v476 = &v5[v23];
  const int32_t *v485 = &v5[v33];
  const int32_t *v494 = &v5[v41];
  const int32_t *v503 = &v5[v51];
  const int32_t *v512 = &v5[v59];
  const int32_t *v521 = &v5[v69];
  const int32_t *v530 = &v5[v77];
  const int32_t *v548 = &v5[v95];
  const int32_t *v557 = &v5[v105];
  const int32_t *v566 = &v5[v113];
  const int32_t *v575 = &v5[v123];
  const int32_t *v584 = &v5[v131];
  const int32_t *v593 = &v5[v141];
  const int32_t *v602 = &v5[v149];
  svfloat32_t v614 = svdup_n_f32(v237);
  svfloat32_t v615 = svdup_n_f32(v244);
  svfloat32_t v617 = svdup_n_f32(v256);
  svfloat32_t v618 = svdup_n_f32(v263);
  svfloat32_t v619 = svdup_n_f32(v270);
  int32_t *v648 = &v6[v349];
  int32_t *v657 = &v6[v357];
  int32_t *v666 = &v6[v365];
  int32_t *v675 = &v6[v373];
  int32_t *v684 = &v6[v381];
  int32_t *v693 = &v6[v389];
  int32_t *v702 = &v6[v397];
  int32_t *v711 = &v6[v405];
  int32_t *v720 = &v6[v413];
  int32_t *v729 = &v6[v421];
  int32_t *v738 = &v6[v429];
  int32_t *v747 = &v6[v437];
  int32_t *v756 = &v6[v445];
  int32_t *v765 = &v6[v453];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v467[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v476[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v485[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v494[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v503[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v512[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v521[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v530[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v101 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v548[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v111 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v557[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v119 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v566[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v129 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v575[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v137 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v584[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v147 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v593[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v155 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v602[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v120 = svadd_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v121 = svsub_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v156 = svadd_f32_x(svptrue_b32(), v147, v155);
  svfloat32_t v157 = svsub_f32_x(svptrue_b32(), v147, v155);
  svfloat32_t v158 = svadd_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v159 = svsub_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v160 = svadd_f32_x(svptrue_b32(), v66, v84);
  svfloat32_t v161 = svsub_f32_x(svptrue_b32(), v66, v84);
  svfloat32_t v162 = svadd_f32_x(svptrue_b32(), v102, v120);
  svfloat32_t v163 = svsub_f32_x(svptrue_b32(), v102, v120);
  svfloat32_t v164 = svadd_f32_x(svptrue_b32(), v138, v156);
  svfloat32_t v165 = svsub_f32_x(svptrue_b32(), v138, v156);
  svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v67, v85);
  svfloat32_t v175 = svsub_f32_x(svptrue_b32(), v67, v85);
  svfloat32_t v176 = svadd_f32_x(svptrue_b32(), v103, v157);
  svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v103, v157);
  svfloat32_t v178 = svadd_f32_x(svptrue_b32(), v121, v139);
  svfloat32_t v179 = svsub_f32_x(svptrue_b32(), v121, v139);
  svfloat32_t zero239 = svdup_n_f32(0);
  svfloat32_t v239 = svcmla_f32_x(pred_full, zero239, v614, v49, 90);
  svfloat32_t v166 = svadd_f32_x(svptrue_b32(), v158, v160);
  svfloat32_t v167 = svsub_f32_x(svptrue_b32(), v158, v160);
  svfloat32_t v168 = svadd_f32_x(svptrue_b32(), v162, v164);
  svfloat32_t v169 = svsub_f32_x(svptrue_b32(), v162, v164);
  svfloat32_t v172 = svadd_f32_x(svptrue_b32(), v163, v165);
  svfloat32_t v173 = svsub_f32_x(svptrue_b32(), v163, v165);
  svfloat32_t v180 = svadd_f32_x(svptrue_b32(), v176, v178);
  svfloat32_t v181 = svadd_f32_x(svptrue_b32(), v177, v179);
  svfloat32_t zero215 = svdup_n_f32(0);
  svfloat32_t v215 = svcmla_f32_x(pred_full, zero215, v614, v161, 90);
  svfloat32_t zero246 = svdup_n_f32(0);
  svfloat32_t v246 = svcmla_f32_x(pred_full, zero246, v615, v174, 90);
  svfloat32_t zero272 = svdup_n_f32(0);
  svfloat32_t v272 = svcmla_f32_x(pred_full, zero272, v619, v178, 90);
  svfloat32_t v282 = svmul_f32_x(svptrue_b32(), v177, v621);
  svfloat32_t v287 = svmul_f32_x(svptrue_b32(), v179, v622);
  svfloat32_t v170 = svadd_f32_x(svptrue_b32(), v166, v168);
  svfloat32_t v171 = svsub_f32_x(svptrue_b32(), v166, v168);
  svfloat32_t zero203 = svdup_n_f32(0);
  svfloat32_t v203 = svcmla_f32_x(pred_full, zero203, v614, v169, 90);
  svfloat32_t zero222 = svdup_n_f32(0);
  svfloat32_t v222 = svcmla_f32_x(pred_full, zero222, v615, v172, 90);
  svfloat32_t zero258 = svdup_n_f32(0);
  svfloat32_t v258 = svcmla_f32_x(pred_full, zero258, v617, v180, 90);
  svfloat32_t v277 = svmul_f32_x(svptrue_b32(), v181, v620);
  svfloat32_t v298 = svmla_f32_x(pred_full, v31, v175, v616);
  svfloat32_t v299 = svmls_f32_x(pred_full, v31, v175, v616);
  svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v239, v246);
  svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v239, v246);
  svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v167, v203);
  svfloat32_t v289 = svsub_f32_x(svptrue_b32(), v167, v203);
  svfloat32_t v290 = svmla_f32_x(pred_full, v159, v173, v616);
  svfloat32_t v291 = svadd_f32_x(svptrue_b32(), v215, v222);
  svfloat32_t v292 = svmls_f32_x(pred_full, v159, v173, v616);
  svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v222, v215);
  svfloat32_t v302 = svcmla_f32_x(pred_full, v258, v618, v176, 90);
  svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v258, v272);
  svfloat32_t v304 = svnmls_f32_x(pred_full, v277, v177, v621);
  svfloat32_t v305 = svnmls_f32_x(pred_full, v277, v179, v622);
  svfloat32_t v306 = svnmls_f32_x(pred_full, v282, v181, v620);
  svfloat32_t v307 = svnmls_f32_x(pred_full, v287, v181, v620);
  svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v299, v301);
  svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v299, v301);
  svint16_t v334 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v170, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v398 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v171, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v290, v291);
  svfloat32_t v295 = svadd_f32_x(svptrue_b32(), v292, v293);
  svfloat32_t v296 = svsub_f32_x(svptrue_b32(), v292, v293);
  svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v290, v291);
  svfloat32_t v308 = svadd_f32_x(svptrue_b32(), v298, v304);
  svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v298, v304);
  svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v298, v306);
  svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v298, v306);
  svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v299, v307);
  svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v299, v307);
  svfloat32_t v318 = svadd_f32_x(svptrue_b32(), v302, v300);
  svfloat32_t v319 = svsub_f32_x(svptrue_b32(), v302, v300);
  svfloat32_t v320 = svadd_f32_x(svptrue_b32(), v303, v305);
  svfloat32_t v321 = svsub_f32_x(svptrue_b32(), v303, v305);
  svfloat32_t v322 = svadd_f32_x(svptrue_b32(), v303, v301);
  svfloat32_t v323 = svsub_f32_x(svptrue_b32(), v303, v301);
  svint16_t v366 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v289, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v430 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v288, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v630), svreinterpret_u64_s16(v334));
  svst1w_u64(pred_full, (unsigned *)(v702), svreinterpret_u64_s16(v398));
  svfloat32_t v324 = svadd_f32_x(svptrue_b32(), v308, v318);
  svfloat32_t v325 = svadd_f32_x(svptrue_b32(), v309, v319);
  svfloat32_t v326 = svsub_f32_x(svptrue_b32(), v310, v319);
  svfloat32_t v327 = svsub_f32_x(svptrue_b32(), v311, v318);
  svfloat32_t v328 = svadd_f32_x(svptrue_b32(), v312, v320);
  svfloat32_t v329 = svadd_f32_x(svptrue_b32(), v313, v321);
  svfloat32_t v330 = svsub_f32_x(svptrue_b32(), v314, v323);
  svfloat32_t v331 = svsub_f32_x(svptrue_b32(), v315, v322);
  svint16_t v350 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v297, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v382 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v296, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v414 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v295, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v446 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v294, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v666), svreinterpret_u64_s16(v366));
  svst1w_u64(pred_full, (unsigned *)(v738), svreinterpret_u64_s16(v430));
  svint16_t v342 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v327, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v358 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v330, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v374 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v331, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v390 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v326, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v406 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v325, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v422 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v328, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v438 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v329, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v454 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v324, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v648), svreinterpret_u64_s16(v350));
  svst1w_u64(pred_full, (unsigned *)(v684), svreinterpret_u64_s16(v382));
  svst1w_u64(pred_full, (unsigned *)(v720), svreinterpret_u64_s16(v414));
  svst1w_u64(pred_full, (unsigned *)(v756), svreinterpret_u64_s16(v446));
  svst1w_u64(pred_full, (unsigned *)(v639), svreinterpret_u64_s16(v342));
  svst1w_u64(pred_full, (unsigned *)(v657), svreinterpret_u64_s16(v358));
  svst1w_u64(pred_full, (unsigned *)(v675), svreinterpret_u64_s16(v374));
  svst1w_u64(pred_full, (unsigned *)(v693), svreinterpret_u64_s16(v390));
  svst1w_u64(pred_full, (unsigned *)(v711), svreinterpret_u64_s16(v406));
  svst1w_u64(pred_full, (unsigned *)(v729), svreinterpret_u64_s16(v422));
  svst1w_u64(pred_full, (unsigned *)(v747), svreinterpret_u64_s16(v438));
  svst1w_u64(pred_full, (unsigned *)(v765), svreinterpret_u64_s16(v454));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun17(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v183 = -4.2602849117736000e-02F;
  float v187 = 2.0497965023262180e-01F;
  float v191 = 1.0451835201736759e+00F;
  float v195 = 1.7645848660222969e+00F;
  float v199 = -7.2340797728605655e-01F;
  float v203 = -8.9055591620606403e-02F;
  float v207 = -1.0625000000000000e+00F;
  float v211 = 2.5769410160110379e-01F;
  float v215 = 7.7980260789483757e-01F;
  float v219 = 5.4389318464570580e-01F;
  float v223 = 4.2010193497052700e-01F;
  float v227 = 1.2810929434228073e+00F;
  float v231 = 4.4088907348175338e-01F;
  float v235 = 3.1717619283272508e-01F;
  float v238 = -9.0138318648016680e-01F;
  float v239 = 9.0138318648016680e-01F;
  float v245 = -4.3248756360072310e-01F;
  float v246 = 4.3248756360072310e-01F;
  float v252 = 6.6693537504044498e-01F;
  float v253 = -6.6693537504044498e-01F;
  float v259 = -6.0389004312516970e-01F;
  float v260 = 6.0389004312516970e-01F;
  float v266 = -3.6924873198582547e-01F;
  float v267 = 3.6924873198582547e-01F;
  float v273 = 4.8656938755549761e-01F;
  float v274 = -4.8656938755549761e-01F;
  float v280 = 2.3813712136760609e-01F;
  float v281 = -2.3813712136760609e-01F;
  float v287 = -1.5573820617422458e+00F;
  float v288 = 1.5573820617422458e+00F;
  float v294 = 6.5962247018731990e-01F;
  float v295 = -6.5962247018731990e-01F;
  float v301 = -1.4316961569866241e-01F;
  float v302 = 1.4316961569866241e-01F;
  float v308 = 2.3903469959860771e-01F;
  float v309 = -2.3903469959860771e-01F;
  float v315 = -4.7932541949972603e-02F;
  float v316 = 4.7932541949972603e-02F;
  float v322 = -2.3188014856550065e+00F;
  float v323 = 2.3188014856550065e+00F;
  float v329 = 7.8914568419206255e-01F;
  float v330 = -7.8914568419206255e-01F;
  float v336 = 3.8484572871179505e+00F;
  float v337 = -3.8484572871179505e+00F;
  float v343 = -1.3003804568801376e+00F;
  float v344 = 1.3003804568801376e+00F;
  float v350 = 4.0814769046889037e+00F;
  float v351 = -4.0814769046889037e+00F;
  float v357 = -1.4807159909286283e+00F;
  float v358 = 1.4807159909286283e+00F;
  float v364 = -1.3332470363551400e-02F;
  float v365 = 1.3332470363551400e-02F;
  float v371 = -3.7139778690557629e-01F;
  float v372 = 3.7139778690557629e-01F;
  float v378 = 1.9236512863456379e-01F;
  float v379 = -1.9236512863456379e-01F;
  int16x4_t v13 = vld1s_s16(&v5[istride]);
  int16x4_t v175 = vld1s_s16(&v5[0]);
  float32x2_t v184 = (float32x2_t){v183, v183};
  float32x2_t v188 = (float32x2_t){v187, v187};
  float32x2_t v192 = (float32x2_t){v191, v191};
  float32x2_t v196 = (float32x2_t){v195, v195};
  float32x2_t v200 = (float32x2_t){v199, v199};
  float32x2_t v204 = (float32x2_t){v203, v203};
  float32x2_t v208 = (float32x2_t){v207, v207};
  float32x2_t v212 = (float32x2_t){v211, v211};
  float32x2_t v216 = (float32x2_t){v215, v215};
  float32x2_t v220 = (float32x2_t){v219, v219};
  float32x2_t v224 = (float32x2_t){v223, v223};
  float32x2_t v228 = (float32x2_t){v227, v227};
  float32x2_t v232 = (float32x2_t){v231, v231};
  float32x2_t v236 = (float32x2_t){v235, v235};
  float32x2_t v240 = (float32x2_t){v238, v239};
  float32x2_t v247 = (float32x2_t){v245, v246};
  float32x2_t v254 = (float32x2_t){v252, v253};
  float32x2_t v261 = (float32x2_t){v259, v260};
  float32x2_t v268 = (float32x2_t){v266, v267};
  float32x2_t v275 = (float32x2_t){v273, v274};
  float32x2_t v282 = (float32x2_t){v280, v281};
  float32x2_t v289 = (float32x2_t){v287, v288};
  float32x2_t v296 = (float32x2_t){v294, v295};
  float32x2_t v303 = (float32x2_t){v301, v302};
  float32x2_t v310 = (float32x2_t){v308, v309};
  float32x2_t v317 = (float32x2_t){v315, v316};
  float32x2_t v324 = (float32x2_t){v322, v323};
  float32x2_t v331 = (float32x2_t){v329, v330};
  float32x2_t v338 = (float32x2_t){v336, v337};
  float32x2_t v345 = (float32x2_t){v343, v344};
  float32x2_t v352 = (float32x2_t){v350, v351};
  float32x2_t v359 = (float32x2_t){v357, v358};
  float32x2_t v366 = (float32x2_t){v364, v365};
  float32x2_t v373 = (float32x2_t){v371, v372};
  float32x2_t v380 = (float32x2_t){v378, v379};
  float32x2_t v381 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 16]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 14]);
  int16x4_t v41 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v47 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v69 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v75 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v83 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v89 = vld1s_s16(&v5[istride * 12]);
  int16x4_t v97 = vld1s_s16(&v5[istride * 15]);
  int16x4_t v103 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v111 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v117 = vld1s_s16(&v5[istride * 6]);
  float32x2_t v176 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v175)), 15);
  float32x2_t v242 = vmul_f32(v381, v240);
  float32x2_t v249 = vmul_f32(v381, v247);
  float32x2_t v256 = vmul_f32(v381, v254);
  float32x2_t v263 = vmul_f32(v381, v261);
  float32x2_t v270 = vmul_f32(v381, v268);
  float32x2_t v277 = vmul_f32(v381, v275);
  float32x2_t v284 = vmul_f32(v381, v282);
  float32x2_t v291 = vmul_f32(v381, v289);
  float32x2_t v298 = vmul_f32(v381, v296);
  float32x2_t v305 = vmul_f32(v381, v303);
  float32x2_t v312 = vmul_f32(v381, v310);
  float32x2_t v319 = vmul_f32(v381, v317);
  float32x2_t v326 = vmul_f32(v381, v324);
  float32x2_t v333 = vmul_f32(v381, v331);
  float32x2_t v340 = vmul_f32(v381, v338);
  float32x2_t v347 = vmul_f32(v381, v345);
  float32x2_t v354 = vmul_f32(v381, v352);
  float32x2_t v361 = vmul_f32(v381, v359);
  float32x2_t v368 = vmul_f32(v381, v366);
  float32x2_t v375 = vmul_f32(v381, v373);
  float32x2_t v382 = vmul_f32(v381, v380);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  float32x2_t v76 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v75)), 15);
  float32x2_t v84 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v83)), 15);
  float32x2_t v90 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v89)), 15);
  float32x2_t v98 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v97)), 15);
  float32x2_t v104 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v103)), 15);
  float32x2_t v112 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v111)), 15);
  float32x2_t v118 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v117)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v77 = vadd_f32(v70, v76);
  float32x2_t v78 = vsub_f32(v70, v76);
  float32x2_t v91 = vadd_f32(v84, v90);
  float32x2_t v92 = vsub_f32(v84, v90);
  float32x2_t v105 = vadd_f32(v98, v104);
  float32x2_t v106 = vsub_f32(v98, v104);
  float32x2_t v119 = vadd_f32(v112, v118);
  float32x2_t v120 = vsub_f32(v112, v118);
  float32x2_t v121 = vadd_f32(v21, v77);
  float32x2_t v122 = vadd_f32(v35, v91);
  float32x2_t v123 = vadd_f32(v49, v105);
  float32x2_t v124 = vadd_f32(v63, v119);
  float32x2_t v127 = vsub_f32(v21, v77);
  float32x2_t v128 = vsub_f32(v35, v91);
  float32x2_t v129 = vsub_f32(v49, v105);
  float32x2_t v130 = vsub_f32(v63, v119);
  float32x2_t v141 = vadd_f32(v22, v50);
  float32x2_t v142 = vadd_f32(v36, v64);
  float32x2_t v143 = vsub_f32(v22, v50);
  float32x2_t v144 = vsub_f32(v120, v92);
  float32x2_t v145 = vadd_f32(v78, v106);
  float32x2_t v146 = vadd_f32(v92, v120);
  float32x2_t v147 = vsub_f32(v78, v106);
  float32x2_t v148 = vsub_f32(v36, v64);
  float32x2_t v161 = vadd_f32(v22, v78);
  float32x2_t v162 = vadd_f32(v64, v120);
  float32x2_t v334 = vrev64_f32(v22);
  float32x2_t v341 = vrev64_f32(v78);
  float32x2_t v355 = vrev64_f32(v64);
  float32x2_t v362 = vrev64_f32(v120);
  float32x2_t v125 = vadd_f32(v121, v123);
  float32x2_t v126 = vadd_f32(v122, v124);
  float32x2_t v131 = vsub_f32(v121, v123);
  float32x2_t v132 = vsub_f32(v122, v124);
  float32x2_t v135 = vadd_f32(v128, v130);
  float32x2_t v136 = vadd_f32(v127, v129);
  float32x2_t v138 = vsub_f32(v129, v130);
  float32x2_t v139 = vsub_f32(v127, v128);
  float32x2_t v149 = vadd_f32(v141, v142);
  float32x2_t v150 = vadd_f32(v145, v146);
  float32x2_t v152 = vsub_f32(v141, v142);
  float32x2_t v153 = vsub_f32(v145, v146);
  float32x2_t v155 = vadd_f32(v143, v144);
  float32x2_t v156 = vadd_f32(v147, v148);
  float32x2_t v158 = vsub_f32(v143, v144);
  float32x2_t v159 = vsub_f32(v147, v148);
  float32x2_t v185 = vmul_f32(v127, v184);
  float32x2_t v189 = vmul_f32(v128, v188);
  float32x2_t v193 = vmul_f32(v129, v192);
  float32x2_t v197 = vmul_f32(v130, v196);
  float32x2_t v327 = vrev64_f32(v161);
  float32x2_t v335 = vmul_f32(v334, v333);
  float32x2_t v342 = vmul_f32(v341, v340);
  float32x2_t v348 = vrev64_f32(v162);
  float32x2_t v356 = vmul_f32(v355, v354);
  float32x2_t v363 = vmul_f32(v362, v361);
  float32x2_t v133 = vadd_f32(v125, v126);
  float32x2_t v134 = vsub_f32(v125, v126);
  float32x2_t v137 = vsub_f32(v136, v135);
  float32x2_t v140 = vadd_f32(v131, v132);
  float32x2_t v151 = vadd_f32(v149, v150);
  float32x2_t v154 = vadd_f32(v152, v153);
  float32x2_t v157 = vadd_f32(v155, v156);
  float32x2_t v160 = vadd_f32(v158, v159);
  float32x2_t v163 = vsub_f32(v156, v150);
  float32x2_t v166 = vsub_f32(v149, v155);
  float32x2_t v201 = vmul_f32(v131, v200);
  float32x2_t v205 = vmul_f32(v132, v204);
  float32x2_t v217 = vmul_f32(v135, v216);
  float32x2_t v221 = vmul_f32(v136, v220);
  float32x2_t v229 = vmul_f32(v138, v228);
  float32x2_t v233 = vmul_f32(v139, v232);
  float32x2_t v243 = vrev64_f32(v149);
  float32x2_t v250 = vrev64_f32(v150);
  float32x2_t v264 = vrev64_f32(v152);
  float32x2_t v271 = vrev64_f32(v153);
  float32x2_t v285 = vrev64_f32(v155);
  float32x2_t v292 = vrev64_f32(v156);
  float32x2_t v306 = vrev64_f32(v158);
  float32x2_t v313 = vrev64_f32(v159);
  float32x2_t v328 = vmul_f32(v327, v326);
  float32x2_t v349 = vmul_f32(v348, v347);
  float32x2_t v164 = vadd_f32(v163, v22);
  float32x2_t v167 = vadd_f32(v166, v64);
  float32x2_t v177 = vadd_f32(v176, v133);
  float32x2_t v209 = vmul_f32(v133, v208);
  float32x2_t v213 = vmul_f32(v134, v212);
  float32x2_t v225 = vmul_f32(v137, v224);
  float32x2_t v237 = vmul_f32(v140, v236);
  float32x2_t v244 = vmul_f32(v243, v242);
  float32x2_t v251 = vmul_f32(v250, v249);
  float32x2_t v257 = vrev64_f32(v151);
  float32x2_t v265 = vmul_f32(v264, v263);
  float32x2_t v272 = vmul_f32(v271, v270);
  float32x2_t v278 = vrev64_f32(v154);
  float32x2_t v286 = vmul_f32(v285, v284);
  float32x2_t v293 = vmul_f32(v292, v291);
  float32x2_t v299 = vrev64_f32(v157);
  float32x2_t v307 = vmul_f32(v306, v305);
  float32x2_t v314 = vmul_f32(v313, v312);
  float32x2_t v320 = vrev64_f32(v160);
  float32x2_t v387 = vadd_f32(v197, v229);
  float32x2_t v388 = vsub_f32(v229, v193);
  float32x2_t v389 = vadd_f32(v189, v233);
  float32x2_t v390 = vsub_f32(v185, v233);
  float32x2_t v165 = vsub_f32(v164, v162);
  float32x2_t v168 = vadd_f32(v167, v78);
  float32x2_t v258 = vmul_f32(v257, v256);
  float32x2_t v279 = vmul_f32(v278, v277);
  float32x2_t v300 = vmul_f32(v299, v298);
  float32x2_t v321 = vmul_f32(v320, v319);
  float32x2_t v385 = vadd_f32(v217, v225);
  float32x2_t v386 = vsub_f32(v221, v225);
  float32x2_t v391 = vsub_f32(v237, v205);
  float32x2_t v392 = vadd_f32(v237, v201);
  float32x2_t v393 = vadd_f32(v209, v177);
  int16x4_t v461 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v177, 15), (int32x2_t){0, 0}));
  float32x2_t v169 = vsub_f32(v168, v120);
  float32x2_t v369 = vrev64_f32(v165);
  float32x2_t v394 = vadd_f32(v213, v393);
  float32x2_t v395 = vsub_f32(v393, v213);
  float32x2_t v396 = vsub_f32(v385, v387);
  float32x2_t v398 = vadd_f32(v386, v388);
  float32x2_t v400 = vadd_f32(v385, v389);
  float32x2_t v402 = vadd_f32(v386, v390);
  float32x2_t v412 = vadd_f32(v244, v258);
  float32x2_t v413 = vadd_f32(v251, v258);
  float32x2_t v414 = vadd_f32(v265, v279);
  float32x2_t v415 = vadd_f32(v272, v279);
  float32x2_t v416 = vadd_f32(v286, v300);
  float32x2_t v417 = vadd_f32(v293, v300);
  float32x2_t v418 = vadd_f32(v307, v321);
  float32x2_t v419 = vadd_f32(v314, v321);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v461), 0);
  float32x2_t v170 = vadd_f32(v165, v169);
  float32x2_t v370 = vmul_f32(v369, v368);
  float32x2_t v376 = vrev64_f32(v169);
  float32x2_t v397 = vadd_f32(v391, v394);
  float32x2_t v399 = vadd_f32(v392, v395);
  float32x2_t v401 = vsub_f32(v394, v391);
  float32x2_t v403 = vsub_f32(v395, v392);
  float32x2_t v423 = vadd_f32(v412, v414);
  float32x2_t v424 = vsub_f32(v412, v414);
  float32x2_t v425 = vadd_f32(v413, v415);
  float32x2_t v426 = vsub_f32(v413, v415);
  float32x2_t v427 = vadd_f32(v416, v418);
  float32x2_t v428 = vsub_f32(v418, v416);
  float32x2_t v429 = vadd_f32(v417, v419);
  float32x2_t v430 = vsub_f32(v419, v417);
  float32x2_t v377 = vmul_f32(v376, v375);
  float32x2_t v383 = vrev64_f32(v170);
  float32x2_t v404 = vadd_f32(v396, v397);
  float32x2_t v405 = vadd_f32(v398, v399);
  float32x2_t v406 = vadd_f32(v400, v401);
  float32x2_t v407 = vadd_f32(v402, v403);
  float32x2_t v408 = vsub_f32(v397, v396);
  float32x2_t v409 = vsub_f32(v399, v398);
  float32x2_t v410 = vsub_f32(v401, v400);
  float32x2_t v411 = vsub_f32(v403, v402);
  float32x2_t v440 = vadd_f32(v425, v429);
  float32x2_t v442 = vadd_f32(v424, v430);
  float32x2_t v444 = vsub_f32(v423, v427);
  float32x2_t v446 = vsub_f32(v430, v424);
  float32x2_t v448 = vadd_f32(v423, v427);
  float32x2_t v451 = vsub_f32(v428, v426);
  float32x2_t v454 = vsub_f32(v429, v425);
  float32x2_t v457 = vadd_f32(v426, v428);
  float32x2_t v384 = vmul_f32(v383, v382);
  float32x2_t v431 = vsub_f32(v370, v377);
  float32x2_t v420 = vadd_f32(v384, v377);
  float32x2_t v433 = vadd_f32(v431, v431);
  float32x2_t v458 = vsub_f32(v457, v431);
  float32x2_t v421 = vadd_f32(v328, v420);
  float32x2_t v434 = vsub_f32(v349, v433);
  float32x2_t v437 = vadd_f32(v420, v420);
  float32x2_t v455 = vadd_f32(v454, v433);
  float32x2_t v493 = vadd_f32(v411, v458);
  float32x2_t v500 = vsub_f32(v411, v458);
  float32x2_t v422 = vadd_f32(v421, v335);
  float32x2_t v432 = vadd_f32(v421, v342);
  float32x2_t v435 = vadd_f32(v434, v356);
  float32x2_t v436 = vadd_f32(v434, v363);
  float32x2_t v438 = vadd_f32(v437, v437);
  float32x2_t v439 = vadd_f32(v431, v437);
  float32x2_t v445 = vadd_f32(v444, v437);
  float32x2_t v456 = vadd_f32(v455, v437);
  int16x4_t v496 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v493, 15), (int32x2_t){0, 0}));
  int16x4_t v503 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v500, 15), (int32x2_t){0, 0}));
  float32x2_t v441 = vadd_f32(v440, v432);
  float32x2_t v443 = vadd_f32(v442, v435);
  float32x2_t v447 = vsub_f32(v446, v439);
  float32x2_t v449 = vadd_f32(v448, v422);
  float32x2_t v452 = vsub_f32(v451, v436);
  float32x2_t v479 = vadd_f32(v406, v445);
  float32x2_t v486 = vsub_f32(v406, v445);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v496), 0);
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v503), 0);
  float32x2_t v563 = vadd_f32(v410, v456);
  float32x2_t v570 = vsub_f32(v410, v456);
  float32x2_t v450 = vadd_f32(v449, v431);
  float32x2_t v453 = vadd_f32(v452, v438);
  float32x2_t v465 = vadd_f32(v404, v441);
  float32x2_t v472 = vsub_f32(v404, v441);
  int16x4_t v482 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v479, 15), (int32x2_t){0, 0}));
  int16x4_t v489 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v486, 15), (int32x2_t){0, 0}));
  float32x2_t v521 = vadd_f32(v407, v447);
  float32x2_t v528 = vsub_f32(v407, v447);
  float32x2_t v535 = vadd_f32(v405, v443);
  float32x2_t v542 = vsub_f32(v405, v443);
  int16x4_t v566 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v563, 15), (int32x2_t){0, 0}));
  int16x4_t v573 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v570, 15), (int32x2_t){0, 0}));
  int16x4_t v468 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v465, 15), (int32x2_t){0, 0}));
  int16x4_t v475 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v472, 15), (int32x2_t){0, 0}));
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v482), 0);
  v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v489), 0);
  float32x2_t v507 = vadd_f32(v408, v450);
  float32x2_t v514 = vsub_f32(v408, v450);
  int16x4_t v524 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v521, 15), (int32x2_t){0, 0}));
  int16x4_t v531 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v528, 15), (int32x2_t){0, 0}));
  int16x4_t v538 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v535, 15), (int32x2_t){0, 0}));
  int16x4_t v545 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v542, 15), (int32x2_t){0, 0}));
  float32x2_t v549 = vadd_f32(v409, v453);
  float32x2_t v556 = vsub_f32(v409, v453);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v566), 0);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v573), 0);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v468), 0);
  v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v475), 0);
  int16x4_t v510 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v507, 15), (int32x2_t){0, 0}));
  int16x4_t v517 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v514, 15), (int32x2_t){0, 0}));
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v524), 0);
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v531), 0);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v538), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v545), 0);
  int16x4_t v552 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v549, 15), (int32x2_t){0, 0}));
  int16x4_t v559 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v556, 15), (int32x2_t){0, 0}));
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v510), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v517), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v552), 0);
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v559), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun17(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v223 = -4.2602849117736000e-02F;
  float v228 = 2.0497965023262180e-01F;
  float v233 = 1.0451835201736759e+00F;
  float v238 = 1.7645848660222969e+00F;
  float v243 = -7.2340797728605655e-01F;
  float v248 = -8.9055591620606403e-02F;
  float v253 = -1.0625000000000000e+00F;
  float v258 = 2.5769410160110379e-01F;
  float v263 = 7.7980260789483757e-01F;
  float v268 = 5.4389318464570580e-01F;
  float v273 = 4.2010193497052700e-01F;
  float v278 = 1.2810929434228073e+00F;
  float v283 = 4.4088907348175338e-01F;
  float v288 = 3.1717619283272508e-01F;
  float v293 = 9.0138318648016680e-01F;
  float v300 = 4.3248756360072310e-01F;
  float v307 = -6.6693537504044498e-01F;
  float v314 = 6.0389004312516970e-01F;
  float v321 = 3.6924873198582547e-01F;
  float v328 = -4.8656938755549761e-01F;
  float v335 = -2.3813712136760609e-01F;
  float v342 = 1.5573820617422458e+00F;
  float v349 = -6.5962247018731990e-01F;
  float v356 = 1.4316961569866241e-01F;
  float v363 = -2.3903469959860771e-01F;
  float v370 = 4.7932541949972603e-02F;
  float v377 = 2.3188014856550065e+00F;
  float v384 = -7.8914568419206255e-01F;
  float v391 = -3.8484572871179505e+00F;
  float v398 = 1.3003804568801376e+00F;
  float v405 = -4.0814769046889037e+00F;
  float v412 = 1.4807159909286283e+00F;
  float v419 = 1.3332470363551400e-02F;
  float v426 = 3.7139778690557629e-01F;
  float v433 = -1.9236512863456379e-01F;
  const int32_t *v671 = &v5[v0];
  int32_t *v871 = &v6[v2];
  int64_t v23 = v0 * 16;
  int64_t v33 = v0 * 3;
  int64_t v41 = v0 * 14;
  int64_t v51 = v0 * 9;
  int64_t v59 = v0 * 8;
  int64_t v69 = v0 * 10;
  int64_t v77 = v0 * 7;
  int64_t v87 = v0 * 13;
  int64_t v95 = v0 * 4;
  int64_t v105 = v0 * 5;
  int64_t v113 = v0 * 12;
  int64_t v123 = v0 * 15;
  int64_t v131 = v0 * 2;
  int64_t v141 = v0 * 11;
  int64_t v149 = v0 * 6;
  float v296 = v4 * v293;
  float v303 = v4 * v300;
  float v310 = v4 * v307;
  float v317 = v4 * v314;
  float v324 = v4 * v321;
  float v331 = v4 * v328;
  float v338 = v4 * v335;
  float v345 = v4 * v342;
  float v352 = v4 * v349;
  float v359 = v4 * v356;
  float v366 = v4 * v363;
  float v373 = v4 * v370;
  float v380 = v4 * v377;
  float v387 = v4 * v384;
  float v394 = v4 * v391;
  float v401 = v4 * v398;
  float v408 = v4 * v405;
  float v415 = v4 * v412;
  float v422 = v4 * v419;
  float v429 = v4 * v426;
  float v436 = v4 * v433;
  int64_t v532 = v2 * 16;
  int64_t v541 = v2 * 2;
  int64_t v550 = v2 * 15;
  int64_t v559 = v2 * 3;
  int64_t v568 = v2 * 14;
  int64_t v577 = v2 * 4;
  int64_t v586 = v2 * 13;
  int64_t v595 = v2 * 5;
  int64_t v604 = v2 * 12;
  int64_t v613 = v2 * 6;
  int64_t v622 = v2 * 11;
  int64_t v631 = v2 * 7;
  int64_t v640 = v2 * 10;
  int64_t v649 = v2 * 8;
  int64_t v658 = v2 * 9;
  const int32_t *v816 = &v5[0];
  svfloat32_t v820 = svdup_n_f32(v223);
  svfloat32_t v821 = svdup_n_f32(v228);
  svfloat32_t v822 = svdup_n_f32(v233);
  svfloat32_t v823 = svdup_n_f32(v238);
  svfloat32_t v824 = svdup_n_f32(v243);
  svfloat32_t v825 = svdup_n_f32(v248);
  svfloat32_t v826 = svdup_n_f32(v253);
  svfloat32_t v827 = svdup_n_f32(v258);
  svfloat32_t v828 = svdup_n_f32(v263);
  svfloat32_t v829 = svdup_n_f32(v268);
  svfloat32_t v830 = svdup_n_f32(v273);
  svfloat32_t v831 = svdup_n_f32(v278);
  svfloat32_t v832 = svdup_n_f32(v283);
  svfloat32_t v833 = svdup_n_f32(v288);
  int32_t *v862 = &v6[0];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v671[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v680 = &v5[v23];
  const int32_t *v689 = &v5[v33];
  const int32_t *v698 = &v5[v41];
  const int32_t *v707 = &v5[v51];
  const int32_t *v716 = &v5[v59];
  const int32_t *v725 = &v5[v69];
  const int32_t *v734 = &v5[v77];
  const int32_t *v743 = &v5[v87];
  const int32_t *v752 = &v5[v95];
  const int32_t *v761 = &v5[v105];
  const int32_t *v770 = &v5[v113];
  const int32_t *v779 = &v5[v123];
  const int32_t *v788 = &v5[v131];
  const int32_t *v797 = &v5[v141];
  const int32_t *v806 = &v5[v149];
  svfloat32_t v834 = svdup_n_f32(v296);
  svfloat32_t v835 = svdup_n_f32(v303);
  svfloat32_t v836 = svdup_n_f32(v310);
  svfloat32_t v837 = svdup_n_f32(v317);
  svfloat32_t v838 = svdup_n_f32(v324);
  svfloat32_t v839 = svdup_n_f32(v331);
  svfloat32_t v840 = svdup_n_f32(v338);
  svfloat32_t v841 = svdup_n_f32(v345);
  svfloat32_t v842 = svdup_n_f32(v352);
  svfloat32_t v843 = svdup_n_f32(v359);
  svfloat32_t v844 = svdup_n_f32(v366);
  svfloat32_t v845 = svdup_n_f32(v373);
  svfloat32_t v846 = svdup_n_f32(v380);
  svfloat32_t v847 = svdup_n_f32(v387);
  svfloat32_t v848 = svdup_n_f32(v394);
  svfloat32_t v849 = svdup_n_f32(v401);
  svfloat32_t v850 = svdup_n_f32(v408);
  svfloat32_t v851 = svdup_n_f32(v415);
  svfloat32_t v852 = svdup_n_f32(v422);
  svfloat32_t v853 = svdup_n_f32(v429);
  svfloat32_t v854 = svdup_n_f32(v436);
  int32_t *v880 = &v6[v532];
  int32_t *v889 = &v6[v541];
  int32_t *v898 = &v6[v550];
  int32_t *v907 = &v6[v559];
  int32_t *v916 = &v6[v568];
  int32_t *v925 = &v6[v577];
  int32_t *v934 = &v6[v586];
  int32_t *v943 = &v6[v595];
  int32_t *v952 = &v6[v604];
  int32_t *v961 = &v6[v613];
  int32_t *v970 = &v6[v622];
  int32_t *v979 = &v6[v631];
  int32_t *v988 = &v6[v640];
  int32_t *v997 = &v6[v649];
  int32_t *v1006 = &v6[v658];
  svfloat32_t v215 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v816[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v680[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v689[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v698[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v707[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v716[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v725[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v734[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v743[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v101 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v752[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v111 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v761[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v119 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v770[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v129 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v779[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v137 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v788[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v147 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v797[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v155 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v806[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v120 = svadd_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v121 = svsub_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v156 = svadd_f32_x(svptrue_b32(), v147, v155);
  svfloat32_t v157 = svsub_f32_x(svptrue_b32(), v147, v155);
  svfloat32_t v158 = svadd_f32_x(svptrue_b32(), v30, v102);
  svfloat32_t v159 = svadd_f32_x(svptrue_b32(), v48, v120);
  svfloat32_t v160 = svadd_f32_x(svptrue_b32(), v66, v138);
  svfloat32_t v161 = svadd_f32_x(svptrue_b32(), v84, v156);
  svfloat32_t v164 = svsub_f32_x(svptrue_b32(), v30, v102);
  svfloat32_t v165 = svsub_f32_x(svptrue_b32(), v48, v120);
  svfloat32_t v166 = svsub_f32_x(svptrue_b32(), v66, v138);
  svfloat32_t v167 = svsub_f32_x(svptrue_b32(), v84, v156);
  svfloat32_t v178 = svadd_f32_x(svptrue_b32(), v31, v67);
  svfloat32_t v179 = svadd_f32_x(svptrue_b32(), v49, v85);
  svfloat32_t v180 = svsub_f32_x(svptrue_b32(), v31, v67);
  svfloat32_t v181 = svsub_f32_x(svptrue_b32(), v157, v121);
  svfloat32_t v182 = svadd_f32_x(svptrue_b32(), v103, v139);
  svfloat32_t v183 = svadd_f32_x(svptrue_b32(), v121, v157);
  svfloat32_t v184 = svsub_f32_x(svptrue_b32(), v103, v139);
  svfloat32_t v185 = svsub_f32_x(svptrue_b32(), v49, v85);
  svfloat32_t v198 = svadd_f32_x(svptrue_b32(), v31, v103);
  svfloat32_t v199 = svadd_f32_x(svptrue_b32(), v85, v157);
  svfloat32_t v162 = svadd_f32_x(svptrue_b32(), v158, v160);
  svfloat32_t v163 = svadd_f32_x(svptrue_b32(), v159, v161);
  svfloat32_t v168 = svsub_f32_x(svptrue_b32(), v158, v160);
  svfloat32_t v169 = svsub_f32_x(svptrue_b32(), v159, v161);
  svfloat32_t v172 = svadd_f32_x(svptrue_b32(), v165, v167);
  svfloat32_t v173 = svadd_f32_x(svptrue_b32(), v164, v166);
  svfloat32_t v175 = svsub_f32_x(svptrue_b32(), v166, v167);
  svfloat32_t v176 = svsub_f32_x(svptrue_b32(), v164, v165);
  svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v178, v179);
  svfloat32_t v187 = svadd_f32_x(svptrue_b32(), v182, v183);
  svfloat32_t v189 = svsub_f32_x(svptrue_b32(), v178, v179);
  svfloat32_t v190 = svsub_f32_x(svptrue_b32(), v182, v183);
  svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v180, v181);
  svfloat32_t v193 = svadd_f32_x(svptrue_b32(), v184, v185);
  svfloat32_t v195 = svsub_f32_x(svptrue_b32(), v180, v181);
  svfloat32_t v196 = svsub_f32_x(svptrue_b32(), v184, v185);
  svfloat32_t v236 = svmul_f32_x(svptrue_b32(), v166, v822);
  svfloat32_t zero403 = svdup_n_f32(0);
  svfloat32_t v403 = svcmla_f32_x(pred_full, zero403, v849, v199, 90);
  svfloat32_t v170 = svadd_f32_x(svptrue_b32(), v162, v163);
  svfloat32_t v171 = svsub_f32_x(svptrue_b32(), v162, v163);
  svfloat32_t v174 = svsub_f32_x(svptrue_b32(), v173, v172);
  svfloat32_t v177 = svadd_f32_x(svptrue_b32(), v168, v169);
  svfloat32_t v188 = svadd_f32_x(svptrue_b32(), v186, v187);
  svfloat32_t v191 = svadd_f32_x(svptrue_b32(), v189, v190);
  svfloat32_t v194 = svadd_f32_x(svptrue_b32(), v192, v193);
  svfloat32_t v197 = svadd_f32_x(svptrue_b32(), v195, v196);
  svfloat32_t v200 = svsub_f32_x(svptrue_b32(), v193, v187);
  svfloat32_t v203 = svsub_f32_x(svptrue_b32(), v186, v192);
  svfloat32_t v246 = svmul_f32_x(svptrue_b32(), v168, v824);
  svfloat32_t v251 = svmul_f32_x(svptrue_b32(), v169, v825);
  svfloat32_t v281 = svmul_f32_x(svptrue_b32(), v175, v831);
  svfloat32_t v286 = svmul_f32_x(svptrue_b32(), v176, v832);
  svfloat32_t v201 = svadd_f32_x(svptrue_b32(), v200, v31);
  svfloat32_t v204 = svadd_f32_x(svptrue_b32(), v203, v85);
  svfloat32_t v216 = svadd_f32_x(svptrue_b32(), v215, v170);
  svfloat32_t v276 = svmul_f32_x(svptrue_b32(), v174, v830);
  svfloat32_t zero312 = svdup_n_f32(0);
  svfloat32_t v312 = svcmla_f32_x(pred_full, zero312, v836, v188, 90);
  svfloat32_t zero333 = svdup_n_f32(0);
  svfloat32_t v333 = svcmla_f32_x(pred_full, zero333, v839, v191, 90);
  svfloat32_t zero354 = svdup_n_f32(0);
  svfloat32_t v354 = svcmla_f32_x(pred_full, zero354, v842, v194, 90);
  svfloat32_t zero375 = svdup_n_f32(0);
  svfloat32_t v375 = svcmla_f32_x(pred_full, zero375, v845, v197, 90);
  svfloat32_t v441 = svmla_f32_x(pred_full, v281, v167, v823);
  svfloat32_t v442 = svnmls_f32_x(pred_full, v236, v175, v831);
  svfloat32_t v443 = svmla_f32_x(pred_full, v286, v165, v821);
  svfloat32_t v444 = svnmls_f32_x(pred_full, v286, v164, v820);
  svfloat32_t v202 = svsub_f32_x(svptrue_b32(), v201, v199);
  svfloat32_t v205 = svadd_f32_x(svptrue_b32(), v204, v103);
  svfloat32_t v439 = svmla_f32_x(pred_full, v276, v172, v828);
  svfloat32_t v440 = svnmls_f32_x(pred_full, v276, v173, v829);
  svfloat32_t v445 = svnmls_f32_x(pred_full, v251, v177, v833);
  svfloat32_t v446 = svmla_f32_x(pred_full, v246, v177, v833);
  svfloat32_t v447 = svmla_f32_x(pred_full, v216, v170, v826);
  svfloat32_t v466 = svcmla_f32_x(pred_full, v312, v834, v186, 90);
  svfloat32_t v467 = svcmla_f32_x(pred_full, v312, v835, v187, 90);
  svfloat32_t v468 = svcmla_f32_x(pred_full, v333, v837, v189, 90);
  svfloat32_t v469 = svcmla_f32_x(pred_full, v333, v838, v190, 90);
  svfloat32_t v470 = svcmla_f32_x(pred_full, v354, v840, v192, 90);
  svfloat32_t v471 = svcmla_f32_x(pred_full, v354, v841, v193, 90);
  svfloat32_t v472 = svcmla_f32_x(pred_full, v375, v843, v195, 90);
  svfloat32_t v473 = svcmla_f32_x(pred_full, v375, v844, v196, 90);
  svint16_t v515 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v216, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v206 = svsub_f32_x(svptrue_b32(), v205, v157);
  svfloat32_t zero424 = svdup_n_f32(0);
  svfloat32_t v424 = svcmla_f32_x(pred_full, zero424, v852, v202, 90);
  svfloat32_t v448 = svmla_f32_x(pred_full, v447, v171, v827);
  svfloat32_t v449 = svmls_f32_x(pred_full, v447, v171, v827);
  svfloat32_t v450 = svsub_f32_x(svptrue_b32(), v439, v441);
  svfloat32_t v452 = svadd_f32_x(svptrue_b32(), v440, v442);
  svfloat32_t v454 = svadd_f32_x(svptrue_b32(), v439, v443);
  svfloat32_t v456 = svadd_f32_x(svptrue_b32(), v440, v444);
  svfloat32_t v477 = svadd_f32_x(svptrue_b32(), v466, v468);
  svfloat32_t v478 = svsub_f32_x(svptrue_b32(), v466, v468);
  svfloat32_t v479 = svadd_f32_x(svptrue_b32(), v467, v469);
  svfloat32_t v480 = svsub_f32_x(svptrue_b32(), v467, v469);
  svfloat32_t v481 = svadd_f32_x(svptrue_b32(), v470, v472);
  svfloat32_t v482 = svsub_f32_x(svptrue_b32(), v472, v470);
  svfloat32_t v483 = svadd_f32_x(svptrue_b32(), v471, v473);
  svfloat32_t v484 = svsub_f32_x(svptrue_b32(), v473, v471);
  svst1w_u64(pred_full, (unsigned *)(v862), svreinterpret_u64_s16(v515));
  svfloat32_t v207 = svadd_f32_x(svptrue_b32(), v202, v206);
  svfloat32_t zero431 = svdup_n_f32(0);
  svfloat32_t v431 = svcmla_f32_x(pred_full, zero431, v853, v206, 90);
  svfloat32_t v451 = svadd_f32_x(svptrue_b32(), v445, v448);
  svfloat32_t v453 = svadd_f32_x(svptrue_b32(), v446, v449);
  svfloat32_t v455 = svsub_f32_x(svptrue_b32(), v448, v445);
  svfloat32_t v457 = svsub_f32_x(svptrue_b32(), v449, v446);
  svfloat32_t v494 = svadd_f32_x(svptrue_b32(), v479, v483);
  svfloat32_t v496 = svadd_f32_x(svptrue_b32(), v478, v484);
  svfloat32_t v498 = svsub_f32_x(svptrue_b32(), v477, v481);
  svfloat32_t v500 = svsub_f32_x(svptrue_b32(), v484, v478);
  svfloat32_t v502 = svadd_f32_x(svptrue_b32(), v477, v481);
  svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v482, v480);
  svfloat32_t v508 = svsub_f32_x(svptrue_b32(), v483, v479);
  svfloat32_t v511 = svadd_f32_x(svptrue_b32(), v480, v482);
  svfloat32_t v458 = svadd_f32_x(svptrue_b32(), v450, v451);
  svfloat32_t v459 = svadd_f32_x(svptrue_b32(), v452, v453);
  svfloat32_t v460 = svadd_f32_x(svptrue_b32(), v454, v455);
  svfloat32_t v461 = svadd_f32_x(svptrue_b32(), v456, v457);
  svfloat32_t v462 = svsub_f32_x(svptrue_b32(), v451, v450);
  svfloat32_t v463 = svsub_f32_x(svptrue_b32(), v453, v452);
  svfloat32_t v464 = svsub_f32_x(svptrue_b32(), v455, v454);
  svfloat32_t v465 = svsub_f32_x(svptrue_b32(), v457, v456);
  svfloat32_t v485 = svsub_f32_x(svptrue_b32(), v424, v431);
  svfloat32_t v474 = svcmla_f32_x(pred_full, v431, v854, v207, 90);
  svfloat32_t v487 = svadd_f32_x(svptrue_b32(), v485, v485);
  svfloat32_t v512 = svsub_f32_x(svptrue_b32(), v511, v485);
  svfloat32_t v475 = svcmla_f32_x(pred_full, v474, v846, v198, 90);
  svfloat32_t v488 = svsub_f32_x(svptrue_b32(), v403, v487);
  svfloat32_t v491 = svadd_f32_x(svptrue_b32(), v474, v474);
  svfloat32_t v509 = svadd_f32_x(svptrue_b32(), v508, v487);
  svfloat32_t v557 = svadd_f32_x(svptrue_b32(), v465, v512);
  svfloat32_t v566 = svsub_f32_x(svptrue_b32(), v465, v512);
  svfloat32_t v476 = svcmla_f32_x(pred_full, v475, v847, v31, 90);
  svfloat32_t v486 = svcmla_f32_x(pred_full, v475, v848, v103, 90);
  svfloat32_t v489 = svcmla_f32_x(pred_full, v488, v850, v85, 90);
  svfloat32_t v490 = svcmla_f32_x(pred_full, v488, v851, v157, 90);
  svfloat32_t v492 = svadd_f32_x(svptrue_b32(), v491, v491);
  svfloat32_t v493 = svadd_f32_x(svptrue_b32(), v485, v491);
  svfloat32_t v499 = svadd_f32_x(svptrue_b32(), v498, v491);
  svfloat32_t v510 = svadd_f32_x(svptrue_b32(), v509, v491);
  svint16_t v560 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v557, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v569 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v566, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v495 = svadd_f32_x(svptrue_b32(), v494, v486);
  svfloat32_t v497 = svadd_f32_x(svptrue_b32(), v496, v489);
  svfloat32_t v501 = svsub_f32_x(svptrue_b32(), v500, v493);
  svfloat32_t v503 = svadd_f32_x(svptrue_b32(), v502, v476);
  svfloat32_t v506 = svsub_f32_x(svptrue_b32(), v505, v490);
  svfloat32_t v539 = svadd_f32_x(svptrue_b32(), v460, v499);
  svfloat32_t v548 = svsub_f32_x(svptrue_b32(), v460, v499);
  svfloat32_t v647 = svadd_f32_x(svptrue_b32(), v464, v510);
  svfloat32_t v656 = svsub_f32_x(svptrue_b32(), v464, v510);
  svst1w_u64(pred_full, (unsigned *)(v907), svreinterpret_u64_s16(v560));
  svst1w_u64(pred_full, (unsigned *)(v916), svreinterpret_u64_s16(v569));
  svfloat32_t v504 = svadd_f32_x(svptrue_b32(), v503, v485);
  svfloat32_t v507 = svadd_f32_x(svptrue_b32(), v506, v492);
  svfloat32_t v521 = svadd_f32_x(svptrue_b32(), v458, v495);
  svfloat32_t v530 = svsub_f32_x(svptrue_b32(), v458, v495);
  svint16_t v542 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v539, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v551 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v548, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v593 = svadd_f32_x(svptrue_b32(), v461, v501);
  svfloat32_t v602 = svsub_f32_x(svptrue_b32(), v461, v501);
  svfloat32_t v611 = svadd_f32_x(svptrue_b32(), v459, v497);
  svfloat32_t v620 = svsub_f32_x(svptrue_b32(), v459, v497);
  svint16_t v650 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v647, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v659 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v656, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v524 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v521, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v533 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v530, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v575 = svadd_f32_x(svptrue_b32(), v462, v504);
  svfloat32_t v584 = svsub_f32_x(svptrue_b32(), v462, v504);
  svint16_t v596 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v593, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v605 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v602, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v614 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v611, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v623 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v620, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v629 = svadd_f32_x(svptrue_b32(), v463, v507);
  svfloat32_t v638 = svsub_f32_x(svptrue_b32(), v463, v507);
  svst1w_u64(pred_full, (unsigned *)(v889), svreinterpret_u64_s16(v542));
  svst1w_u64(pred_full, (unsigned *)(v898), svreinterpret_u64_s16(v551));
  svst1w_u64(pred_full, (unsigned *)(v997), svreinterpret_u64_s16(v650));
  svst1w_u64(pred_full, (unsigned *)(v1006), svreinterpret_u64_s16(v659));
  svint16_t v578 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v575, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v587 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v584, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v632 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v629, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v641 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v638, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v871), svreinterpret_u64_s16(v524));
  svst1w_u64(pred_full, (unsigned *)(v880), svreinterpret_u64_s16(v533));
  svst1w_u64(pred_full, (unsigned *)(v943), svreinterpret_u64_s16(v596));
  svst1w_u64(pred_full, (unsigned *)(v952), svreinterpret_u64_s16(v605));
  svst1w_u64(pred_full, (unsigned *)(v961), svreinterpret_u64_s16(v614));
  svst1w_u64(pred_full, (unsigned *)(v970), svreinterpret_u64_s16(v623));
  svst1w_u64(pred_full, (unsigned *)(v925), svreinterpret_u64_s16(v578));
  svst1w_u64(pred_full, (unsigned *)(v934), svreinterpret_u64_s16(v587));
  svst1w_u64(pred_full, (unsigned *)(v979), svreinterpret_u64_s16(v632));
  svst1w_u64(pred_full, (unsigned *)(v988), svreinterpret_u64_s16(v641));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun18(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v264 = -5.0000000000000000e-01F;
  float v275 = -1.4999999999999998e+00F;
  float v278 = 8.6602540378443871e-01F;
  float v279 = -8.6602540378443871e-01F;
  float v286 = 7.6604444311897801e-01F;
  float v290 = 9.3969262078590832e-01F;
  float v294 = -1.7364817766693039e-01F;
  float v297 = 6.4278760968653925e-01F;
  float v298 = -6.4278760968653925e-01F;
  float v304 = -3.4202014332566888e-01F;
  float v305 = 3.4202014332566888e-01F;
  float v311 = 9.8480775301220802e-01F;
  float v312 = -9.8480775301220802e-01F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v89 = vld1s_s16(&v5[istride]);
  float32x2_t v265 = (float32x2_t){v264, v264};
  float32x2_t v276 = (float32x2_t){v275, v275};
  float32x2_t v280 = (float32x2_t){v278, v279};
  float32x2_t v287 = (float32x2_t){v286, v286};
  float32x2_t v291 = (float32x2_t){v290, v290};
  float32x2_t v295 = (float32x2_t){v294, v294};
  float32x2_t v299 = (float32x2_t){v297, v298};
  float32x2_t v306 = (float32x2_t){v304, v305};
  float32x2_t v313 = (float32x2_t){v311, v312};
  float32x2_t v314 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v41 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v47 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 15]);
  int16x4_t v69 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v75 = vld1s_s16(&v5[istride * 17]);
  int16x4_t v83 = vld1s_s16(&v5[istride * 10]);
  float32x2_t v90 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v89)), 15);
  int16x4_t v97 = vld1s_s16(&v5[istride * 12]);
  int16x4_t v103 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v111 = vld1s_s16(&v5[istride * 14]);
  int16x4_t v117 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v125 = vld1s_s16(&v5[istride * 16]);
  int16x4_t v131 = vld1s_s16(&v5[istride * 7]);
  float32x2_t v282 = vmul_f32(v314, v280);
  float32x2_t v301 = vmul_f32(v314, v299);
  float32x2_t v308 = vmul_f32(v314, v306);
  float32x2_t v315 = vmul_f32(v314, v313);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  float32x2_t v76 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v75)), 15);
  float32x2_t v84 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v83)), 15);
  float32x2_t v98 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v97)), 15);
  float32x2_t v104 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v103)), 15);
  float32x2_t v112 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v111)), 15);
  float32x2_t v118 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v117)), 15);
  float32x2_t v126 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v125)), 15);
  float32x2_t v132 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v131)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v77 = vadd_f32(v70, v76);
  float32x2_t v78 = vsub_f32(v70, v76);
  float32x2_t v91 = vadd_f32(v84, v90);
  float32x2_t v92 = vsub_f32(v84, v90);
  float32x2_t v105 = vadd_f32(v98, v104);
  float32x2_t v106 = vsub_f32(v98, v104);
  float32x2_t v119 = vadd_f32(v112, v118);
  float32x2_t v120 = vsub_f32(v112, v118);
  float32x2_t v133 = vadd_f32(v126, v132);
  float32x2_t v134 = vsub_f32(v126, v132);
  float32x2_t v135 = vadd_f32(v35, v133);
  float32x2_t v136 = vsub_f32(v35, v133);
  float32x2_t v137 = vadd_f32(v119, v49);
  float32x2_t v138 = vsub_f32(v119, v49);
  float32x2_t v139 = vadd_f32(v63, v105);
  float32x2_t v140 = vsub_f32(v63, v105);
  float32x2_t v141 = vadd_f32(v77, v91);
  float32x2_t v142 = vsub_f32(v77, v91);
  float32x2_t v239 = vadd_f32(v36, v134);
  float32x2_t v240 = vsub_f32(v36, v134);
  float32x2_t v241 = vadd_f32(v120, v50);
  float32x2_t v242 = vsub_f32(v120, v50);
  float32x2_t v243 = vadd_f32(v64, v106);
  float32x2_t v244 = vsub_f32(v64, v106);
  float32x2_t v245 = vadd_f32(v78, v92);
  float32x2_t v246 = vsub_f32(v78, v92);
  float32x2_t v143 = vadd_f32(v135, v137);
  float32x2_t v147 = vadd_f32(v136, v138);
  float32x2_t v149 = vsub_f32(v135, v137);
  float32x2_t v150 = vsub_f32(v137, v141);
  float32x2_t v151 = vsub_f32(v141, v135);
  float32x2_t v152 = vsub_f32(v136, v138);
  float32x2_t v153 = vsub_f32(v138, v142);
  float32x2_t v154 = vsub_f32(v142, v136);
  float32x2_t v173 = vmul_f32(v139, v276);
  float32x2_t v179 = vrev64_f32(v140);
  float32x2_t v247 = vadd_f32(v239, v241);
  float32x2_t v251 = vadd_f32(v240, v242);
  float32x2_t v253 = vsub_f32(v239, v241);
  float32x2_t v254 = vsub_f32(v241, v245);
  float32x2_t v255 = vsub_f32(v245, v239);
  float32x2_t v256 = vsub_f32(v240, v242);
  float32x2_t v257 = vsub_f32(v242, v246);
  float32x2_t v258 = vsub_f32(v246, v240);
  float32x2_t v277 = vmul_f32(v243, v276);
  float32x2_t v283 = vrev64_f32(v244);
  float32x2_t v144 = vadd_f32(v143, v141);
  float32x2_t v148 = vadd_f32(v147, v142);
  float32x2_t v180 = vmul_f32(v179, v282);
  float32x2_t v184 = vmul_f32(v149, v287);
  float32x2_t v188 = vmul_f32(v150, v291);
  float32x2_t v192 = vmul_f32(v151, v295);
  float32x2_t v198 = vrev64_f32(v152);
  float32x2_t v205 = vrev64_f32(v153);
  float32x2_t v212 = vrev64_f32(v154);
  float32x2_t v248 = vadd_f32(v247, v245);
  float32x2_t v252 = vadd_f32(v251, v246);
  float32x2_t v284 = vmul_f32(v283, v282);
  float32x2_t v288 = vmul_f32(v253, v287);
  float32x2_t v292 = vmul_f32(v254, v291);
  float32x2_t v296 = vmul_f32(v255, v295);
  float32x2_t v302 = vrev64_f32(v256);
  float32x2_t v309 = vrev64_f32(v257);
  float32x2_t v316 = vrev64_f32(v258);
  float32x2_t v145 = vadd_f32(v144, v139);
  float32x2_t v162 = vmul_f32(v144, v265);
  float32x2_t v168 = vrev64_f32(v148);
  float32x2_t v199 = vmul_f32(v198, v301);
  float32x2_t v206 = vmul_f32(v205, v308);
  float32x2_t v213 = vmul_f32(v212, v315);
  float32x2_t v249 = vadd_f32(v248, v243);
  float32x2_t v266 = vmul_f32(v248, v265);
  float32x2_t v272 = vrev64_f32(v252);
  float32x2_t v303 = vmul_f32(v302, v301);
  float32x2_t v310 = vmul_f32(v309, v308);
  float32x2_t v317 = vmul_f32(v316, v315);
  float32x2_t v146 = vadd_f32(v145, v21);
  float32x2_t v169 = vmul_f32(v168, v282);
  float32x2_t v214 = vadd_f32(v162, v162);
  float32x2_t v227 = vadd_f32(v180, v199);
  float32x2_t v229 = vsub_f32(v180, v206);
  float32x2_t v231 = vsub_f32(v180, v199);
  float32x2_t v250 = vadd_f32(v249, v22);
  float32x2_t v273 = vmul_f32(v272, v282);
  float32x2_t v318 = vadd_f32(v266, v266);
  float32x2_t v331 = vadd_f32(v284, v303);
  float32x2_t v333 = vsub_f32(v284, v310);
  float32x2_t v335 = vsub_f32(v284, v303);
  float32x2_t v215 = vadd_f32(v214, v162);
  float32x2_t v219 = vadd_f32(v146, v173);
  float32x2_t v228 = vadd_f32(v227, v206);
  float32x2_t v230 = vadd_f32(v229, v213);
  float32x2_t v232 = vsub_f32(v231, v213);
  float32x2_t v319 = vadd_f32(v318, v266);
  float32x2_t v323 = vadd_f32(v250, v277);
  float32x2_t v332 = vadd_f32(v331, v310);
  float32x2_t v334 = vadd_f32(v333, v317);
  float32x2_t v336 = vsub_f32(v335, v317);
  int16x4_t v345 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v146, 15), (int32x2_t){0, 0}));
  int16x4_t v351 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v250, 15), (int32x2_t){0, 0}));
  float32x2_t v216 = vadd_f32(v146, v215);
  float32x2_t v220 = vadd_f32(v219, v214);
  float32x2_t v320 = vadd_f32(v250, v319);
  float32x2_t v324 = vadd_f32(v323, v318);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v345), 0);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v351), 0);
  float32x2_t v217 = vadd_f32(v216, v169);
  float32x2_t v218 = vsub_f32(v216, v169);
  float32x2_t v221 = vadd_f32(v220, v184);
  float32x2_t v223 = vsub_f32(v220, v188);
  float32x2_t v225 = vsub_f32(v220, v184);
  float32x2_t v321 = vadd_f32(v320, v273);
  float32x2_t v322 = vsub_f32(v320, v273);
  float32x2_t v325 = vadd_f32(v324, v288);
  float32x2_t v327 = vsub_f32(v324, v292);
  float32x2_t v329 = vsub_f32(v324, v288);
  float32x2_t v222 = vadd_f32(v221, v188);
  float32x2_t v224 = vadd_f32(v223, v192);
  float32x2_t v226 = vsub_f32(v225, v192);
  float32x2_t v326 = vadd_f32(v325, v292);
  float32x2_t v328 = vadd_f32(v327, v296);
  float32x2_t v330 = vsub_f32(v329, v296);
  int16x4_t v381 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v218, 15), (int32x2_t){0, 0}));
  int16x4_t v387 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v322, 15), (int32x2_t){0, 0}));
  int16x4_t v417 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v217, 15), (int32x2_t){0, 0}));
  int16x4_t v423 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v321, 15), (int32x2_t){0, 0}));
  float32x2_t v233 = vadd_f32(v222, v228);
  float32x2_t v234 = vsub_f32(v222, v228);
  float32x2_t v235 = vadd_f32(v224, v230);
  float32x2_t v236 = vsub_f32(v224, v230);
  float32x2_t v237 = vadd_f32(v226, v232);
  float32x2_t v238 = vsub_f32(v226, v232);
  float32x2_t v337 = vadd_f32(v326, v332);
  float32x2_t v338 = vsub_f32(v326, v332);
  float32x2_t v339 = vadd_f32(v328, v334);
  float32x2_t v340 = vsub_f32(v328, v334);
  float32x2_t v341 = vadd_f32(v330, v336);
  float32x2_t v342 = vsub_f32(v330, v336);
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v381), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v387), 0);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v417), 0);
  v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v423), 0);
  int16x4_t v357 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v234, 15), (int32x2_t){0, 0}));
  int16x4_t v363 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v338, 15), (int32x2_t){0, 0}));
  int16x4_t v369 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v235, 15), (int32x2_t){0, 0}));
  int16x4_t v375 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v339, 15), (int32x2_t){0, 0}));
  int16x4_t v393 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v238, 15), (int32x2_t){0, 0}));
  int16x4_t v399 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v342, 15), (int32x2_t){0, 0}));
  int16x4_t v405 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v237, 15), (int32x2_t){0, 0}));
  int16x4_t v411 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v341, 15), (int32x2_t){0, 0}));
  int16x4_t v429 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v236, 15), (int32x2_t){0, 0}));
  int16x4_t v435 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v340, 15), (int32x2_t){0, 0}));
  int16x4_t v441 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v233, 15), (int32x2_t){0, 0}));
  int16x4_t v447 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v337, 15), (int32x2_t){0, 0}));
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v357), 0);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v363), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v369), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v375), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v393), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v399), 0);
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v405), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v411), 0);
  v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v429), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v435), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v441), 0);
  v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v447), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun18(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v312 = -5.0000000000000000e-01F;
  float v324 = -1.4999999999999998e+00F;
  float v329 = -8.6602540378443871e-01F;
  float v336 = 7.6604444311897801e-01F;
  float v341 = 9.3969262078590832e-01F;
  float v346 = -1.7364817766693039e-01F;
  float v351 = -6.4278760968653925e-01F;
  float v358 = 3.4202014332566888e-01F;
  float v365 = -9.8480775301220802e-01F;
  const int32_t *v646 = &v5[v0];
  int32_t *v759 = &v6[v2];
  int64_t v23 = v0 * 9;
  int64_t v33 = v0 * 2;
  int64_t v41 = v0 * 11;
  int64_t v51 = v0 * 4;
  int64_t v59 = v0 * 13;
  int64_t v69 = v0 * 6;
  int64_t v77 = v0 * 15;
  int64_t v87 = v0 * 8;
  int64_t v95 = v0 * 17;
  int64_t v105 = v0 * 10;
  int64_t v123 = v0 * 12;
  int64_t v131 = v0 * 3;
  int64_t v141 = v0 * 14;
  int64_t v149 = v0 * 5;
  int64_t v159 = v0 * 16;
  int64_t v167 = v0 * 7;
  float v332 = v4 * v329;
  float v354 = v4 * v351;
  float v361 = v4 * v358;
  float v368 = v4 * v365;
  int64_t v405 = v2 * 9;
  int64_t v413 = v2 * 10;
  int64_t v429 = v2 * 2;
  int64_t v437 = v2 * 11;
  int64_t v445 = v2 * 12;
  int64_t v453 = v2 * 3;
  int64_t v461 = v2 * 4;
  int64_t v469 = v2 * 13;
  int64_t v477 = v2 * 14;
  int64_t v485 = v2 * 5;
  int64_t v493 = v2 * 6;
  int64_t v501 = v2 * 15;
  int64_t v509 = v2 * 16;
  int64_t v517 = v2 * 7;
  int64_t v525 = v2 * 8;
  int64_t v533 = v2 * 17;
  const int32_t *v547 = &v5[0];
  svfloat32_t v715 = svdup_n_f32(v312);
  svfloat32_t v717 = svdup_n_f32(v324);
  svfloat32_t v719 = svdup_n_f32(v336);
  svfloat32_t v720 = svdup_n_f32(v341);
  svfloat32_t v721 = svdup_n_f32(v346);
  int32_t *v732 = &v6[0];
  svfloat32_t v119 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v646[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v556 = &v5[v23];
  const int32_t *v565 = &v5[v33];
  const int32_t *v574 = &v5[v41];
  const int32_t *v583 = &v5[v51];
  const int32_t *v592 = &v5[v59];
  const int32_t *v601 = &v5[v69];
  const int32_t *v610 = &v5[v77];
  const int32_t *v619 = &v5[v87];
  const int32_t *v628 = &v5[v95];
  const int32_t *v637 = &v5[v105];
  const int32_t *v655 = &v5[v123];
  const int32_t *v664 = &v5[v131];
  const int32_t *v673 = &v5[v141];
  const int32_t *v682 = &v5[v149];
  const int32_t *v691 = &v5[v159];
  const int32_t *v700 = &v5[v167];
  svfloat32_t v718 = svdup_n_f32(v332);
  svfloat32_t v722 = svdup_n_f32(v354);
  svfloat32_t v723 = svdup_n_f32(v361);
  svfloat32_t v724 = svdup_n_f32(v368);
  int32_t *v741 = &v6[v405];
  int32_t *v750 = &v6[v413];
  int32_t *v768 = &v6[v429];
  int32_t *v777 = &v6[v437];
  int32_t *v786 = &v6[v445];
  int32_t *v795 = &v6[v453];
  int32_t *v804 = &v6[v461];
  int32_t *v813 = &v6[v469];
  int32_t *v822 = &v6[v477];
  int32_t *v831 = &v6[v485];
  int32_t *v840 = &v6[v493];
  int32_t *v849 = &v6[v501];
  int32_t *v858 = &v6[v509];
  int32_t *v867 = &v6[v517];
  int32_t *v876 = &v6[v525];
  int32_t *v885 = &v6[v533];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v547[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v556[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v565[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v574[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v583[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v592[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v601[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v610[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v619[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v101 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v628[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v111 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v637[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v129 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v655[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v137 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v664[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v147 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v673[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v155 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v682[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v165 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v691[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v173 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v700[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v120 = svadd_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v121 = svsub_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v156 = svadd_f32_x(svptrue_b32(), v147, v155);
  svfloat32_t v157 = svsub_f32_x(svptrue_b32(), v147, v155);
  svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v165, v173);
  svfloat32_t v175 = svsub_f32_x(svptrue_b32(), v165, v173);
  svfloat32_t v176 = svadd_f32_x(svptrue_b32(), v48, v174);
  svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v48, v174);
  svfloat32_t v178 = svadd_f32_x(svptrue_b32(), v156, v66);
  svfloat32_t v179 = svsub_f32_x(svptrue_b32(), v156, v66);
  svfloat32_t v180 = svadd_f32_x(svptrue_b32(), v84, v138);
  svfloat32_t v181 = svsub_f32_x(svptrue_b32(), v84, v138);
  svfloat32_t v182 = svadd_f32_x(svptrue_b32(), v102, v120);
  svfloat32_t v183 = svsub_f32_x(svptrue_b32(), v102, v120);
  svfloat32_t v286 = svadd_f32_x(svptrue_b32(), v49, v175);
  svfloat32_t v287 = svsub_f32_x(svptrue_b32(), v49, v175);
  svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v157, v67);
  svfloat32_t v289 = svsub_f32_x(svptrue_b32(), v157, v67);
  svfloat32_t v290 = svadd_f32_x(svptrue_b32(), v85, v139);
  svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v85, v139);
  svfloat32_t v292 = svadd_f32_x(svptrue_b32(), v103, v121);
  svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v103, v121);
  svfloat32_t v184 = svadd_f32_x(svptrue_b32(), v176, v178);
  svfloat32_t v188 = svadd_f32_x(svptrue_b32(), v177, v179);
  svfloat32_t v190 = svsub_f32_x(svptrue_b32(), v176, v178);
  svfloat32_t v191 = svsub_f32_x(svptrue_b32(), v178, v182);
  svfloat32_t v192 = svsub_f32_x(svptrue_b32(), v182, v176);
  svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v177, v179);
  svfloat32_t v194 = svsub_f32_x(svptrue_b32(), v179, v183);
  svfloat32_t v195 = svsub_f32_x(svptrue_b32(), v183, v177);
  svfloat32_t zero224 = svdup_n_f32(0);
  svfloat32_t v224 = svcmla_f32_x(pred_full, zero224, v718, v181, 90);
  svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v286, v288);
  svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v287, v289);
  svfloat32_t v300 = svsub_f32_x(svptrue_b32(), v286, v288);
  svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v288, v292);
  svfloat32_t v302 = svsub_f32_x(svptrue_b32(), v292, v286);
  svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v287, v289);
  svfloat32_t v304 = svsub_f32_x(svptrue_b32(), v289, v293);
  svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v293, v287);
  svfloat32_t zero334 = svdup_n_f32(0);
  svfloat32_t v334 = svcmla_f32_x(pred_full, zero334, v718, v291, 90);
  svfloat32_t v185 = svadd_f32_x(svptrue_b32(), v184, v182);
  svfloat32_t v189 = svadd_f32_x(svptrue_b32(), v188, v183);
  svfloat32_t zero246 = svdup_n_f32(0);
  svfloat32_t v246 = svcmla_f32_x(pred_full, zero246, v722, v193, 90);
  svfloat32_t zero253 = svdup_n_f32(0);
  svfloat32_t v253 = svcmla_f32_x(pred_full, zero253, v723, v194, 90);
  svfloat32_t zero260 = svdup_n_f32(0);
  svfloat32_t v260 = svcmla_f32_x(pred_full, zero260, v724, v195, 90);
  svfloat32_t v295 = svadd_f32_x(svptrue_b32(), v294, v292);
  svfloat32_t v299 = svadd_f32_x(svptrue_b32(), v298, v293);
  svfloat32_t zero356 = svdup_n_f32(0);
  svfloat32_t v356 = svcmla_f32_x(pred_full, zero356, v722, v303, 90);
  svfloat32_t zero363 = svdup_n_f32(0);
  svfloat32_t v363 = svcmla_f32_x(pred_full, zero363, v723, v304, 90);
  svfloat32_t zero370 = svdup_n_f32(0);
  svfloat32_t v370 = svcmla_f32_x(pred_full, zero370, v724, v305, 90);
  svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v185, v180);
  svfloat32_t v205 = svmul_f32_x(svptrue_b32(), v185, v715);
  svfloat32_t zero212 = svdup_n_f32(0);
  svfloat32_t v212 = svcmla_f32_x(pred_full, zero212, v718, v189, 90);
  svfloat32_t v274 = svadd_f32_x(svptrue_b32(), v224, v246);
  svfloat32_t v276 = svsub_f32_x(svptrue_b32(), v224, v253);
  svfloat32_t v278 = svsub_f32_x(svptrue_b32(), v224, v246);
  svfloat32_t v296 = svadd_f32_x(svptrue_b32(), v295, v290);
  svfloat32_t v315 = svmul_f32_x(svptrue_b32(), v295, v715);
  svfloat32_t zero322 = svdup_n_f32(0);
  svfloat32_t v322 = svcmla_f32_x(pred_full, zero322, v718, v299, 90);
  svfloat32_t v384 = svadd_f32_x(svptrue_b32(), v334, v356);
  svfloat32_t v386 = svsub_f32_x(svptrue_b32(), v334, v363);
  svfloat32_t v388 = svsub_f32_x(svptrue_b32(), v334, v356);
  svfloat32_t v187 = svadd_f32_x(svptrue_b32(), v186, v30);
  svfloat32_t v261 = svadd_f32_x(svptrue_b32(), v205, v205);
  svfloat32_t v275 = svadd_f32_x(svptrue_b32(), v274, v253);
  svfloat32_t v277 = svadd_f32_x(svptrue_b32(), v276, v260);
  svfloat32_t v279 = svsub_f32_x(svptrue_b32(), v278, v260);
  svfloat32_t v297 = svadd_f32_x(svptrue_b32(), v296, v31);
  svfloat32_t v371 = svadd_f32_x(svptrue_b32(), v315, v315);
  svfloat32_t v385 = svadd_f32_x(svptrue_b32(), v384, v363);
  svfloat32_t v387 = svadd_f32_x(svptrue_b32(), v386, v370);
  svfloat32_t v389 = svsub_f32_x(svptrue_b32(), v388, v370);
  svfloat32_t v262 = svmla_f32_x(pred_full, v261, v185, v715);
  svfloat32_t v266 = svmla_f32_x(pred_full, v187, v180, v717);
  svfloat32_t v372 = svmla_f32_x(pred_full, v371, v295, v715);
  svfloat32_t v376 = svmla_f32_x(pred_full, v297, v290, v717);
  svint16_t v398 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v187, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v406 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v297, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v263 = svadd_f32_x(svptrue_b32(), v187, v262);
  svfloat32_t v267 = svadd_f32_x(svptrue_b32(), v266, v261);
  svfloat32_t v373 = svadd_f32_x(svptrue_b32(), v297, v372);
  svfloat32_t v377 = svadd_f32_x(svptrue_b32(), v376, v371);
  svst1w_u64(pred_full, (unsigned *)(v732), svreinterpret_u64_s16(v398));
  svst1w_u64(pred_full, (unsigned *)(v741), svreinterpret_u64_s16(v406));
  svfloat32_t v264 = svadd_f32_x(svptrue_b32(), v263, v212);
  svfloat32_t v265 = svsub_f32_x(svptrue_b32(), v263, v212);
  svfloat32_t v268 = svmla_f32_x(pred_full, v267, v190, v719);
  svfloat32_t v270 = svmls_f32_x(pred_full, v267, v191, v720);
  svfloat32_t v272 = svmls_f32_x(pred_full, v267, v190, v719);
  svfloat32_t v374 = svadd_f32_x(svptrue_b32(), v373, v322);
  svfloat32_t v375 = svsub_f32_x(svptrue_b32(), v373, v322);
  svfloat32_t v378 = svmla_f32_x(pred_full, v377, v300, v719);
  svfloat32_t v380 = svmls_f32_x(pred_full, v377, v301, v720);
  svfloat32_t v382 = svmls_f32_x(pred_full, v377, v300, v719);
  svfloat32_t v269 = svmla_f32_x(pred_full, v268, v191, v720);
  svfloat32_t v271 = svmla_f32_x(pred_full, v270, v192, v721);
  svfloat32_t v273 = svmls_f32_x(pred_full, v272, v192, v721);
  svfloat32_t v379 = svmla_f32_x(pred_full, v378, v301, v720);
  svfloat32_t v381 = svmla_f32_x(pred_full, v380, v302, v721);
  svfloat32_t v383 = svmls_f32_x(pred_full, v382, v302, v721);
  svint16_t v446 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v265, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v454 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v375, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v494 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v264, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v502 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v374, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v280 = svadd_f32_x(svptrue_b32(), v269, v275);
  svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v269, v275);
  svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v271, v277);
  svfloat32_t v283 = svsub_f32_x(svptrue_b32(), v271, v277);
  svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v273, v279);
  svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v273, v279);
  svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v379, v385);
  svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v379, v385);
  svfloat32_t v392 = svadd_f32_x(svptrue_b32(), v381, v387);
  svfloat32_t v393 = svsub_f32_x(svptrue_b32(), v381, v387);
  svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v383, v389);
  svfloat32_t v395 = svsub_f32_x(svptrue_b32(), v383, v389);
  svst1w_u64(pred_full, (unsigned *)(v786), svreinterpret_u64_s16(v446));
  svst1w_u64(pred_full, (unsigned *)(v795), svreinterpret_u64_s16(v454));
  svst1w_u64(pred_full, (unsigned *)(v840), svreinterpret_u64_s16(v494));
  svst1w_u64(pred_full, (unsigned *)(v849), svreinterpret_u64_s16(v502));
  svint16_t v414 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v281, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v422 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v391, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v430 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v282, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v438 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v392, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v462 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v285, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v470 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v395, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v478 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v284, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v486 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v394, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v510 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v283, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v518 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v393, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v526 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v280, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v534 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v390, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v750), svreinterpret_u64_s16(v414));
  svst1w_u64(pred_full, (unsigned *)(v759), svreinterpret_u64_s16(v422));
  svst1w_u64(pred_full, (unsigned *)(v768), svreinterpret_u64_s16(v430));
  svst1w_u64(pred_full, (unsigned *)(v777), svreinterpret_u64_s16(v438));
  svst1w_u64(pred_full, (unsigned *)(v804), svreinterpret_u64_s16(v462));
  svst1w_u64(pred_full, (unsigned *)(v813), svreinterpret_u64_s16(v470));
  svst1w_u64(pred_full, (unsigned *)(v822), svreinterpret_u64_s16(v478));
  svst1w_u64(pred_full, (unsigned *)(v831), svreinterpret_u64_s16(v486));
  svst1w_u64(pred_full, (unsigned *)(v858), svreinterpret_u64_s16(v510));
  svst1w_u64(pred_full, (unsigned *)(v867), svreinterpret_u64_s16(v518));
  svst1w_u64(pred_full, (unsigned *)(v876), svreinterpret_u64_s16(v526));
  svst1w_u64(pred_full, (unsigned *)(v885), svreinterpret_u64_s16(v534));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun19(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v211 = -1.0555555555555556e+00F;
  float v215 = 1.7752228513927079e-01F;
  float v219 = -1.2820077502191529e-01F;
  float v223 = 4.9321510117355499e-02F;
  float v227 = 5.7611011491005903e-01F;
  float v231 = -7.4996449655536279e-01F;
  float v235 = -1.7385438164530381e-01F;
  float v239 = -2.1729997561977314e+00F;
  float v243 = -1.7021211726914738e+00F;
  float v247 = 4.7087858350625778e-01F;
  float v251 = -2.0239400846888440e+00F;
  float v255 = 1.0551641201664090e-01F;
  float v259 = 2.1294564967054850e+00F;
  float v263 = -7.5087543897371167e-01F;
  float v267 = 1.4812817695157160e-01F;
  float v271 = 8.9900361592528333e-01F;
  float v275 = -6.2148246772602778e-01F;
  float v279 = -7.9869352098712687e-01F;
  float v283 = -4.7339199623771833e-01F;
  float v286 = -2.4216105241892630e-01F;
  float v287 = 2.4216105241892630e-01F;
  float v293 = -5.9368607967505101e-02F;
  float v294 = 5.9368607967505101e-02F;
  float v300 = 1.2578688255176201e-02F;
  float v301 = -1.2578688255176201e-02F;
  float v307 = -4.6789919712328903e-02F;
  float v308 = 4.6789919712328903e-02F;
  float v314 = -9.3750121913782358e-01F;
  float v315 = 9.3750121913782358e-01F;
  float v321 = -5.0111537043352902e-02F;
  float v322 = 5.0111537043352902e-02F;
  float v328 = -9.8761275618117661e-01F;
  float v329 = 9.8761275618117661e-01F;
  float v335 = -1.1745786501205959e+00F;
  float v336 = 1.1745786501205959e+00F;
  float v342 = 1.1114482296234993e+00F;
  float v343 = -1.1114482296234993e+00F;
  float v349 = 2.2860268797440955e+00F;
  float v350 = -2.2860268797440955e+00F;
  float v356 = 2.6420523257930939e-01F;
  float v357 = -2.6420523257930939e-01F;
  float v363 = 2.1981792779352136e+00F;
  float v364 = -2.1981792779352136e+00F;
  float v370 = 1.9339740453559042e+00F;
  float v371 = -1.9339740453559042e+00F;
  float v377 = -7.4825847091254893e-01F;
  float v378 = 7.4825847091254893e-01F;
  float v384 = -4.7820835642768872e-01F;
  float v385 = 4.7820835642768872e-01F;
  float v391 = 2.7005011448486022e-01F;
  float v392 = -2.7005011448486022e-01F;
  float v398 = -3.4642356159542270e-01F;
  float v399 = 3.4642356159542270e-01F;
  float v405 = -8.3485429360688279e-01F;
  float v406 = 8.3485429360688279e-01F;
  float v412 = -3.9375928506743518e-01F;
  float v413 = 3.9375928506743518e-01F;
  int16x4_t v13 = vld1s_s16(&v5[istride]);
  int16x4_t v155 = vld1s_s16(&v5[0]);
  float32x2_t v212 = (float32x2_t){v211, v211};
  float32x2_t v216 = (float32x2_t){v215, v215};
  float32x2_t v220 = (float32x2_t){v219, v219};
  float32x2_t v224 = (float32x2_t){v223, v223};
  float32x2_t v228 = (float32x2_t){v227, v227};
  float32x2_t v232 = (float32x2_t){v231, v231};
  float32x2_t v236 = (float32x2_t){v235, v235};
  float32x2_t v240 = (float32x2_t){v239, v239};
  float32x2_t v244 = (float32x2_t){v243, v243};
  float32x2_t v248 = (float32x2_t){v247, v247};
  float32x2_t v252 = (float32x2_t){v251, v251};
  float32x2_t v256 = (float32x2_t){v255, v255};
  float32x2_t v260 = (float32x2_t){v259, v259};
  float32x2_t v264 = (float32x2_t){v263, v263};
  float32x2_t v268 = (float32x2_t){v267, v267};
  float32x2_t v272 = (float32x2_t){v271, v271};
  float32x2_t v276 = (float32x2_t){v275, v275};
  float32x2_t v280 = (float32x2_t){v279, v279};
  float32x2_t v284 = (float32x2_t){v283, v283};
  float32x2_t v288 = (float32x2_t){v286, v287};
  float32x2_t v295 = (float32x2_t){v293, v294};
  float32x2_t v302 = (float32x2_t){v300, v301};
  float32x2_t v309 = (float32x2_t){v307, v308};
  float32x2_t v316 = (float32x2_t){v314, v315};
  float32x2_t v323 = (float32x2_t){v321, v322};
  float32x2_t v330 = (float32x2_t){v328, v329};
  float32x2_t v337 = (float32x2_t){v335, v336};
  float32x2_t v344 = (float32x2_t){v342, v343};
  float32x2_t v351 = (float32x2_t){v349, v350};
  float32x2_t v358 = (float32x2_t){v356, v357};
  float32x2_t v365 = (float32x2_t){v363, v364};
  float32x2_t v372 = (float32x2_t){v370, v371};
  float32x2_t v379 = (float32x2_t){v377, v378};
  float32x2_t v386 = (float32x2_t){v384, v385};
  float32x2_t v393 = (float32x2_t){v391, v392};
  float32x2_t v400 = (float32x2_t){v398, v399};
  float32x2_t v407 = (float32x2_t){v405, v406};
  float32x2_t v414 = (float32x2_t){v412, v413};
  float32x2_t v415 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 18]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 17]);
  int16x4_t v41 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v47 = vld1s_s16(&v5[istride * 15]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v69 = vld1s_s16(&v5[istride * 16]);
  int16x4_t v75 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v83 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v89 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v97 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v103 = vld1s_s16(&v5[istride * 12]);
  int16x4_t v111 = vld1s_s16(&v5[istride * 14]);
  int16x4_t v117 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v125 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v131 = vld1s_s16(&v5[istride * 10]);
  float32x2_t v156 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v155)), 15);
  float32x2_t v290 = vmul_f32(v415, v288);
  float32x2_t v297 = vmul_f32(v415, v295);
  float32x2_t v304 = vmul_f32(v415, v302);
  float32x2_t v311 = vmul_f32(v415, v309);
  float32x2_t v318 = vmul_f32(v415, v316);
  float32x2_t v325 = vmul_f32(v415, v323);
  float32x2_t v332 = vmul_f32(v415, v330);
  float32x2_t v339 = vmul_f32(v415, v337);
  float32x2_t v346 = vmul_f32(v415, v344);
  float32x2_t v353 = vmul_f32(v415, v351);
  float32x2_t v360 = vmul_f32(v415, v358);
  float32x2_t v367 = vmul_f32(v415, v365);
  float32x2_t v374 = vmul_f32(v415, v372);
  float32x2_t v381 = vmul_f32(v415, v379);
  float32x2_t v388 = vmul_f32(v415, v386);
  float32x2_t v395 = vmul_f32(v415, v393);
  float32x2_t v402 = vmul_f32(v415, v400);
  float32x2_t v409 = vmul_f32(v415, v407);
  float32x2_t v416 = vmul_f32(v415, v414);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  float32x2_t v76 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v75)), 15);
  float32x2_t v84 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v83)), 15);
  float32x2_t v90 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v89)), 15);
  float32x2_t v98 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v97)), 15);
  float32x2_t v104 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v103)), 15);
  float32x2_t v112 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v111)), 15);
  float32x2_t v118 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v117)), 15);
  float32x2_t v126 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v125)), 15);
  float32x2_t v132 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v131)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v34, v28);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v62, v56);
  float32x2_t v77 = vadd_f32(v70, v76);
  float32x2_t v78 = vsub_f32(v70, v76);
  float32x2_t v91 = vadd_f32(v84, v90);
  float32x2_t v92 = vsub_f32(v90, v84);
  float32x2_t v105 = vadd_f32(v98, v104);
  float32x2_t v106 = vsub_f32(v98, v104);
  float32x2_t v119 = vadd_f32(v112, v118);
  float32x2_t v120 = vsub_f32(v118, v112);
  float32x2_t v133 = vadd_f32(v126, v132);
  float32x2_t v134 = vsub_f32(v126, v132);
  float32x2_t v135 = vsub_f32(v21, v105);
  float32x2_t v136 = vsub_f32(v35, v119);
  float32x2_t v137 = vsub_f32(v49, v133);
  float32x2_t v138 = vsub_f32(v63, v105);
  float32x2_t v139 = vsub_f32(v77, v119);
  float32x2_t v140 = vsub_f32(v91, v133);
  float32x2_t v141 = vadd_f32(v21, v63);
  float32x2_t v143 = vadd_f32(v35, v77);
  float32x2_t v145 = vadd_f32(v49, v91);
  float32x2_t v174 = vsub_f32(v22, v106);
  float32x2_t v175 = vsub_f32(v36, v120);
  float32x2_t v176 = vsub_f32(v50, v134);
  float32x2_t v177 = vsub_f32(v64, v106);
  float32x2_t v178 = vsub_f32(v78, v120);
  float32x2_t v179 = vsub_f32(v92, v134);
  float32x2_t v180 = vadd_f32(v22, v64);
  float32x2_t v182 = vadd_f32(v36, v78);
  float32x2_t v184 = vadd_f32(v50, v92);
  float32x2_t v142 = vadd_f32(v141, v105);
  float32x2_t v144 = vadd_f32(v143, v119);
  float32x2_t v146 = vadd_f32(v145, v133);
  float32x2_t v147 = vadd_f32(v135, v137);
  float32x2_t v148 = vadd_f32(v138, v140);
  float32x2_t v164 = vsub_f32(v135, v138);
  float32x2_t v165 = vsub_f32(v137, v140);
  float32x2_t v181 = vadd_f32(v180, v106);
  float32x2_t v183 = vadd_f32(v182, v120);
  float32x2_t v185 = vadd_f32(v184, v134);
  float32x2_t v186 = vadd_f32(v174, v176);
  float32x2_t v187 = vadd_f32(v177, v179);
  float32x2_t v196 = vsub_f32(v174, v177);
  float32x2_t v197 = vsub_f32(v176, v179);
  float32x2_t v241 = vmul_f32(v138, v240);
  float32x2_t v253 = vmul_f32(v140, v252);
  float32x2_t v261 = vmul_f32(v137, v260);
  float32x2_t v340 = vrev64_f32(v177);
  float32x2_t v354 = vrev64_f32(v174);
  float32x2_t v361 = vrev64_f32(v179);
  float32x2_t v375 = vrev64_f32(v176);
  float32x2_t v149 = vadd_f32(v142, v144);
  float32x2_t v158 = vadd_f32(v148, v139);
  float32x2_t v159 = vadd_f32(v147, v136);
  float32x2_t v161 = vsub_f32(v148, v139);
  float32x2_t v162 = vsub_f32(v147, v136);
  float32x2_t v166 = vsub_f32(v135, v165);
  float32x2_t v168 = vadd_f32(v164, v140);
  float32x2_t v171 = vsub_f32(v142, v146);
  float32x2_t v172 = vsub_f32(v144, v146);
  float32x2_t v188 = vadd_f32(v181, v183);
  float32x2_t v190 = vadd_f32(v187, v178);
  float32x2_t v191 = vadd_f32(v186, v175);
  float32x2_t v193 = vsub_f32(v187, v178);
  float32x2_t v194 = vsub_f32(v186, v175);
  float32x2_t v198 = vsub_f32(v174, v197);
  float32x2_t v200 = vadd_f32(v196, v179);
  float32x2_t v203 = vsub_f32(v181, v185);
  float32x2_t v204 = vsub_f32(v183, v185);
  float32x2_t v245 = vmul_f32(v164, v244);
  float32x2_t v257 = vmul_f32(v165, v256);
  float32x2_t v341 = vmul_f32(v340, v339);
  float32x2_t v347 = vrev64_f32(v196);
  float32x2_t v362 = vmul_f32(v361, v360);
  float32x2_t v368 = vrev64_f32(v197);
  float32x2_t v376 = vmul_f32(v375, v374);
  float32x2_t v150 = vadd_f32(v149, v146);
  float32x2_t v160 = vsub_f32(v159, v158);
  float32x2_t v163 = vsub_f32(v162, v161);
  float32x2_t v167 = vsub_f32(v166, v139);
  float32x2_t v169 = vsub_f32(v168, v136);
  float32x2_t v173 = vadd_f32(v171, v172);
  float32x2_t v189 = vadd_f32(v188, v185);
  float32x2_t v192 = vsub_f32(v191, v190);
  float32x2_t v195 = vsub_f32(v194, v193);
  float32x2_t v199 = vsub_f32(v198, v178);
  float32x2_t v201 = vsub_f32(v200, v175);
  float32x2_t v205 = vadd_f32(v203, v204);
  float32x2_t v217 = vmul_f32(v158, v216);
  float32x2_t v221 = vmul_f32(v159, v220);
  float32x2_t v229 = vmul_f32(v161, v228);
  float32x2_t v233 = vmul_f32(v162, v232);
  float32x2_t v277 = vmul_f32(v171, v276);
  float32x2_t v281 = vmul_f32(v172, v280);
  float32x2_t v298 = vrev64_f32(v190);
  float32x2_t v305 = vrev64_f32(v191);
  float32x2_t v319 = vrev64_f32(v193);
  float32x2_t v326 = vrev64_f32(v194);
  float32x2_t v348 = vmul_f32(v347, v346);
  float32x2_t v369 = vmul_f32(v368, v367);
  float32x2_t v403 = vrev64_f32(v203);
  float32x2_t v410 = vrev64_f32(v204);
  float32x2_t v157 = vadd_f32(v156, v150);
  float32x2_t v170 = vsub_f32(v167, v169);
  float32x2_t v202 = vsub_f32(v199, v201);
  float32x2_t v213 = vmul_f32(v150, v212);
  float32x2_t v225 = vmul_f32(v160, v224);
  float32x2_t v237 = vmul_f32(v163, v236);
  float32x2_t v265 = vmul_f32(v167, v264);
  float32x2_t v269 = vmul_f32(v169, v268);
  float32x2_t v285 = vmul_f32(v173, v284);
  float32x2_t v291 = vrev64_f32(v189);
  float32x2_t v299 = vmul_f32(v298, v297);
  float32x2_t v306 = vmul_f32(v305, v304);
  float32x2_t v312 = vrev64_f32(v192);
  float32x2_t v320 = vmul_f32(v319, v318);
  float32x2_t v327 = vmul_f32(v326, v325);
  float32x2_t v333 = vrev64_f32(v195);
  float32x2_t v382 = vrev64_f32(v199);
  float32x2_t v389 = vrev64_f32(v201);
  float32x2_t v404 = vmul_f32(v403, v402);
  float32x2_t v411 = vmul_f32(v410, v409);
  float32x2_t v417 = vrev64_f32(v205);
  float32x2_t v419 = vadd_f32(v217, v221);
  float32x2_t v420 = vadd_f32(v229, v233);
  float32x2_t v273 = vmul_f32(v170, v272);
  float32x2_t v292 = vmul_f32(v291, v290);
  float32x2_t v313 = vmul_f32(v312, v311);
  float32x2_t v334 = vmul_f32(v333, v332);
  float32x2_t v383 = vmul_f32(v382, v381);
  float32x2_t v390 = vmul_f32(v389, v388);
  float32x2_t v396 = vrev64_f32(v202);
  float32x2_t v418 = vmul_f32(v417, v416);
  float32x2_t v422 = vadd_f32(v419, v420);
  float32x2_t v423 = vadd_f32(v217, v225);
  float32x2_t v424 = vadd_f32(v229, v237);
  float32x2_t v441 = vsub_f32(v419, v420);
  float32x2_t v443 = vsub_f32(v277, v285);
  float32x2_t v444 = vsub_f32(v281, v285);
  float32x2_t v445 = vadd_f32(v213, v157);
  float32x2_t v450 = vadd_f32(v299, v306);
  float32x2_t v451 = vadd_f32(v320, v327);
  int16x4_t v506 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v157, 15), (int32x2_t){0, 0}));
  float32x2_t v397 = vmul_f32(v396, v395);
  float32x2_t v421 = vadd_f32(v269, v273);
  float32x2_t v425 = vadd_f32(v265, v273);
  float32x2_t v426 = vsub_f32(v241, v422);
  float32x2_t v427 = vadd_f32(v423, v424);
  float32x2_t v433 = vsub_f32(v423, v424);
  float32x2_t v438 = vadd_f32(v422, v261);
  float32x2_t v446 = vadd_f32(v445, v443);
  float32x2_t v447 = vsub_f32(v445, v443);
  float32x2_t v449 = vadd_f32(v445, v444);
  float32x2_t v453 = vadd_f32(v450, v451);
  float32x2_t v454 = vadd_f32(v299, v313);
  float32x2_t v455 = vadd_f32(v320, v334);
  float32x2_t v472 = vsub_f32(v450, v451);
  float32x2_t v474 = vsub_f32(v404, v418);
  float32x2_t v475 = vsub_f32(v411, v418);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v506), 0);
  float32x2_t v428 = vsub_f32(v253, v425);
  float32x2_t v429 = vadd_f32(v245, v421);
  float32x2_t v431 = vadd_f32(v427, v257);
  float32x2_t v434 = vadd_f32(v433, v421);
  float32x2_t v435 = vadd_f32(v426, v427);
  float32x2_t v442 = vadd_f32(v441, v425);
  float32x2_t v448 = vsub_f32(v447, v444);
  float32x2_t v452 = vadd_f32(v390, v397);
  float32x2_t v456 = vadd_f32(v383, v397);
  float32x2_t v457 = vsub_f32(v341, v453);
  float32x2_t v458 = vadd_f32(v454, v455);
  float32x2_t v464 = vsub_f32(v454, v455);
  float32x2_t v469 = vadd_f32(v453, v376);
  float32x2_t v476 = vadd_f32(v292, v474);
  float32x2_t v477 = vsub_f32(v292, v474);
  float32x2_t v479 = vadd_f32(v292, v475);
  float32x2_t v430 = vadd_f32(v429, v426);
  float32x2_t v432 = vadd_f32(v431, v428);
  float32x2_t v436 = vfma_f32(v435, v135, v248);
  float32x2_t v439 = vadd_f32(v438, v428);
  float32x2_t v459 = vsub_f32(v362, v456);
  float32x2_t v460 = vadd_f32(v348, v452);
  float32x2_t v462 = vadd_f32(v458, v369);
  float32x2_t v465 = vadd_f32(v464, v452);
  float32x2_t v466 = vadd_f32(v457, v458);
  float32x2_t v473 = vadd_f32(v472, v456);
  float32x2_t v478 = vsub_f32(v477, v475);
  float32x2_t v484 = vsub_f32(v442, v434);
  float32x2_t v488 = vsub_f32(v449, v442);
  float32x2_t v491 = vadd_f32(v434, v449);
  float32x2_t v437 = vadd_f32(v436, v425);
  float32x2_t v440 = vadd_f32(v439, v421);
  float32x2_t v461 = vadd_f32(v460, v457);
  float32x2_t v463 = vadd_f32(v462, v459);
  float32x2_t v467 = vfma_f32(v466, v354, v353);
  float32x2_t v470 = vadd_f32(v469, v459);
  float32x2_t v485 = vadd_f32(v484, v449);
  float32x2_t v489 = vadd_f32(v430, v446);
  float32x2_t v490 = vadd_f32(v432, v448);
  float32x2_t v496 = vsub_f32(v473, v465);
  float32x2_t v500 = vsub_f32(v473, v479);
  float32x2_t v503 = vadd_f32(v465, v479);
  float32x2_t v468 = vadd_f32(v467, v456);
  float32x2_t v471 = vadd_f32(v470, v452);
  float32x2_t v480 = vsub_f32(v437, v430);
  float32x2_t v482 = vsub_f32(v440, v432);
  float32x2_t v486 = vsub_f32(v446, v437);
  float32x2_t v487 = vsub_f32(v448, v440);
  float32x2_t v497 = vadd_f32(v496, v479);
  float32x2_t v501 = vadd_f32(v461, v476);
  float32x2_t v502 = vadd_f32(v463, v478);
  float32x2_t v524 = vsub_f32(v491, v503);
  float32x2_t v531 = vadd_f32(v491, v503);
  float32x2_t v538 = vadd_f32(v488, v500);
  float32x2_t v545 = vsub_f32(v488, v500);
  float32x2_t v481 = vadd_f32(v480, v446);
  float32x2_t v483 = vadd_f32(v482, v448);
  float32x2_t v492 = vsub_f32(v468, v461);
  float32x2_t v494 = vsub_f32(v471, v463);
  float32x2_t v498 = vsub_f32(v476, v468);
  float32x2_t v499 = vsub_f32(v478, v471);
  int16x4_t v527 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v524, 15), (int32x2_t){0, 0}));
  int16x4_t v534 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v531, 15), (int32x2_t){0, 0}));
  int16x4_t v541 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v538, 15), (int32x2_t){0, 0}));
  int16x4_t v548 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v545, 15), (int32x2_t){0, 0}));
  float32x2_t v552 = vadd_f32(v490, v502);
  float32x2_t v559 = vsub_f32(v490, v502);
  float32x2_t v566 = vadd_f32(v485, v497);
  float32x2_t v573 = vsub_f32(v485, v497);
  float32x2_t v608 = vsub_f32(v489, v501);
  float32x2_t v615 = vadd_f32(v489, v501);
  float32x2_t v493 = vadd_f32(v492, v476);
  float32x2_t v495 = vadd_f32(v494, v478);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v527), 0);
  v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v534), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v541), 0);
  v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v548), 0);
  int16x4_t v555 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v552, 15), (int32x2_t){0, 0}));
  int16x4_t v562 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v559, 15), (int32x2_t){0, 0}));
  int16x4_t v569 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v566, 15), (int32x2_t){0, 0}));
  int16x4_t v576 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v573, 15), (int32x2_t){0, 0}));
  float32x2_t v580 = vadd_f32(v487, v499);
  float32x2_t v587 = vsub_f32(v487, v499);
  float32x2_t v594 = vadd_f32(v486, v498);
  float32x2_t v601 = vsub_f32(v486, v498);
  int16x4_t v611 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v608, 15), (int32x2_t){0, 0}));
  int16x4_t v618 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v615, 15), (int32x2_t){0, 0}));
  float32x2_t v510 = vadd_f32(v481, v493);
  float32x2_t v517 = vsub_f32(v481, v493);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v555), 0);
  v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v562), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v569), 0);
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v576), 0);
  int16x4_t v583 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v580, 15), (int32x2_t){0, 0}));
  int16x4_t v590 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v587, 15), (int32x2_t){0, 0}));
  int16x4_t v597 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v594, 15), (int32x2_t){0, 0}));
  int16x4_t v604 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v601, 15), (int32x2_t){0, 0}));
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v611), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v618), 0);
  float32x2_t v622 = vadd_f32(v483, v495);
  float32x2_t v629 = vsub_f32(v483, v495);
  int16x4_t v513 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v510, 15), (int32x2_t){0, 0}));
  int16x4_t v520 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v517, 15), (int32x2_t){0, 0}));
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v583), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v590), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v597), 0);
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v604), 0);
  int16x4_t v625 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v622, 15), (int32x2_t){0, 0}));
  int16x4_t v632 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v629, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v513), 0);
  v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v520), 0);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v625), 0);
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v632), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun19(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v255 = -1.0555555555555556e+00F;
  float v260 = 1.7752228513927079e-01F;
  float v265 = -1.2820077502191529e-01F;
  float v270 = 4.9321510117355499e-02F;
  float v275 = 5.7611011491005903e-01F;
  float v280 = -7.4996449655536279e-01F;
  float v285 = -1.7385438164530381e-01F;
  float v290 = -2.1729997561977314e+00F;
  float v295 = -1.7021211726914738e+00F;
  float v300 = 4.7087858350625778e-01F;
  float v305 = -2.0239400846888440e+00F;
  float v310 = 1.0551641201664090e-01F;
  float v315 = 2.1294564967054850e+00F;
  float v320 = -7.5087543897371167e-01F;
  float v325 = 1.4812817695157160e-01F;
  float v330 = 8.9900361592528333e-01F;
  float v335 = -6.2148246772602778e-01F;
  float v340 = -7.9869352098712687e-01F;
  float v345 = -4.7339199623771833e-01F;
  float v350 = 2.4216105241892630e-01F;
  float v357 = 5.9368607967505101e-02F;
  float v364 = -1.2578688255176201e-02F;
  float v371 = 4.6789919712328903e-02F;
  float v378 = 9.3750121913782358e-01F;
  float v385 = 5.0111537043352902e-02F;
  float v392 = 9.8761275618117661e-01F;
  float v399 = 1.1745786501205959e+00F;
  float v406 = -1.1114482296234993e+00F;
  float v413 = -2.2860268797440955e+00F;
  float v420 = -2.6420523257930939e-01F;
  float v427 = -2.1981792779352136e+00F;
  float v434 = -1.9339740453559042e+00F;
  float v441 = 7.4825847091254893e-01F;
  float v448 = 4.7820835642768872e-01F;
  float v455 = -2.7005011448486022e-01F;
  float v462 = 3.4642356159542270e-01F;
  float v469 = 8.3485429360688279e-01F;
  float v476 = 3.9375928506743518e-01F;
  const int32_t *v743 = &v5[v0];
  int32_t *v964 = &v6[v2];
  int64_t v23 = v0 * 18;
  int64_t v33 = v0 * 2;
  int64_t v41 = v0 * 17;
  int64_t v51 = v0 * 4;
  int64_t v59 = v0 * 15;
  int64_t v69 = v0 * 8;
  int64_t v77 = v0 * 11;
  int64_t v87 = v0 * 16;
  int64_t v95 = v0 * 3;
  int64_t v105 = v0 * 13;
  int64_t v113 = v0 * 6;
  int64_t v123 = v0 * 7;
  int64_t v131 = v0 * 12;
  int64_t v141 = v0 * 14;
  int64_t v149 = v0 * 5;
  int64_t v159 = v0 * 9;
  int64_t v167 = v0 * 10;
  float v353 = v4 * v350;
  float v360 = v4 * v357;
  float v367 = v4 * v364;
  float v374 = v4 * v371;
  float v381 = v4 * v378;
  float v388 = v4 * v385;
  float v395 = v4 * v392;
  float v402 = v4 * v399;
  float v409 = v4 * v406;
  float v416 = v4 * v413;
  float v423 = v4 * v420;
  float v430 = v4 * v427;
  float v437 = v4 * v434;
  float v444 = v4 * v441;
  float v451 = v4 * v448;
  float v458 = v4 * v455;
  float v465 = v4 * v462;
  float v472 = v4 * v469;
  float v479 = v4 * v476;
  int64_t v586 = v2 * 18;
  int64_t v595 = v2 * 2;
  int64_t v604 = v2 * 17;
  int64_t v613 = v2 * 3;
  int64_t v622 = v2 * 16;
  int64_t v631 = v2 * 4;
  int64_t v640 = v2 * 15;
  int64_t v649 = v2 * 5;
  int64_t v658 = v2 * 14;
  int64_t v667 = v2 * 6;
  int64_t v676 = v2 * 13;
  int64_t v685 = v2 * 7;
  int64_t v694 = v2 * 12;
  int64_t v703 = v2 * 8;
  int64_t v712 = v2 * 11;
  int64_t v721 = v2 * 9;
  int64_t v730 = v2 * 10;
  const int32_t *v906 = &v5[0];
  svfloat32_t v910 = svdup_n_f32(v255);
  svfloat32_t v911 = svdup_n_f32(v260);
  svfloat32_t v912 = svdup_n_f32(v265);
  svfloat32_t v913 = svdup_n_f32(v270);
  svfloat32_t v914 = svdup_n_f32(v275);
  svfloat32_t v915 = svdup_n_f32(v280);
  svfloat32_t v916 = svdup_n_f32(v285);
  svfloat32_t v917 = svdup_n_f32(v290);
  svfloat32_t v918 = svdup_n_f32(v295);
  svfloat32_t v919 = svdup_n_f32(v300);
  svfloat32_t v920 = svdup_n_f32(v305);
  svfloat32_t v921 = svdup_n_f32(v310);
  svfloat32_t v922 = svdup_n_f32(v315);
  svfloat32_t v923 = svdup_n_f32(v320);
  svfloat32_t v924 = svdup_n_f32(v325);
  svfloat32_t v925 = svdup_n_f32(v330);
  svfloat32_t v926 = svdup_n_f32(v335);
  svfloat32_t v927 = svdup_n_f32(v340);
  svfloat32_t v928 = svdup_n_f32(v345);
  int32_t *v955 = &v6[0];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v743[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v752 = &v5[v23];
  const int32_t *v761 = &v5[v33];
  const int32_t *v770 = &v5[v41];
  const int32_t *v779 = &v5[v51];
  const int32_t *v788 = &v5[v59];
  const int32_t *v797 = &v5[v69];
  const int32_t *v806 = &v5[v77];
  const int32_t *v815 = &v5[v87];
  const int32_t *v824 = &v5[v95];
  const int32_t *v833 = &v5[v105];
  const int32_t *v842 = &v5[v113];
  const int32_t *v851 = &v5[v123];
  const int32_t *v860 = &v5[v131];
  const int32_t *v869 = &v5[v141];
  const int32_t *v878 = &v5[v149];
  const int32_t *v887 = &v5[v159];
  const int32_t *v896 = &v5[v167];
  svfloat32_t v929 = svdup_n_f32(v353);
  svfloat32_t v930 = svdup_n_f32(v360);
  svfloat32_t v931 = svdup_n_f32(v367);
  svfloat32_t v932 = svdup_n_f32(v374);
  svfloat32_t v933 = svdup_n_f32(v381);
  svfloat32_t v934 = svdup_n_f32(v388);
  svfloat32_t v935 = svdup_n_f32(v395);
  svfloat32_t v936 = svdup_n_f32(v402);
  svfloat32_t v937 = svdup_n_f32(v409);
  svfloat32_t v938 = svdup_n_f32(v416);
  svfloat32_t v939 = svdup_n_f32(v423);
  svfloat32_t v940 = svdup_n_f32(v430);
  svfloat32_t v941 = svdup_n_f32(v437);
  svfloat32_t v942 = svdup_n_f32(v444);
  svfloat32_t v943 = svdup_n_f32(v451);
  svfloat32_t v944 = svdup_n_f32(v458);
  svfloat32_t v945 = svdup_n_f32(v465);
  svfloat32_t v946 = svdup_n_f32(v472);
  svfloat32_t v947 = svdup_n_f32(v479);
  int32_t *v973 = &v6[v586];
  int32_t *v982 = &v6[v595];
  int32_t *v991 = &v6[v604];
  int32_t *v1000 = &v6[v613];
  int32_t *v1009 = &v6[v622];
  int32_t *v1018 = &v6[v631];
  int32_t *v1027 = &v6[v640];
  int32_t *v1036 = &v6[v649];
  int32_t *v1045 = &v6[v658];
  int32_t *v1054 = &v6[v667];
  int32_t *v1063 = &v6[v676];
  int32_t *v1072 = &v6[v685];
  int32_t *v1081 = &v6[v694];
  int32_t *v1090 = &v6[v703];
  int32_t *v1099 = &v6[v712];
  int32_t *v1108 = &v6[v721];
  int32_t *v1117 = &v6[v730];
  svfloat32_t v199 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v906[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v752[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v761[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v770[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v779[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v788[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v797[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v806[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v815[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v101 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v824[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v111 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v833[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v119 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v842[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v129 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v851[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v137 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v860[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v147 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v869[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v155 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v878[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v165 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v887[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v173 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v896[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v47, v39);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v83, v75);
  svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v120 = svadd_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v121 = svsub_f32_x(svptrue_b32(), v119, v111);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v156 = svadd_f32_x(svptrue_b32(), v147, v155);
  svfloat32_t v157 = svsub_f32_x(svptrue_b32(), v155, v147);
  svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v165, v173);
  svfloat32_t v175 = svsub_f32_x(svptrue_b32(), v165, v173);
  svfloat32_t v176 = svsub_f32_x(svptrue_b32(), v30, v138);
  svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v48, v156);
  svfloat32_t v178 = svsub_f32_x(svptrue_b32(), v66, v174);
  svfloat32_t v179 = svsub_f32_x(svptrue_b32(), v84, v138);
  svfloat32_t v180 = svsub_f32_x(svptrue_b32(), v102, v156);
  svfloat32_t v181 = svsub_f32_x(svptrue_b32(), v120, v174);
  svfloat32_t v182 = svadd_f32_x(svptrue_b32(), v30, v84);
  svfloat32_t v184 = svadd_f32_x(svptrue_b32(), v48, v102);
  svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v66, v120);
  svfloat32_t v217 = svsub_f32_x(svptrue_b32(), v31, v139);
  svfloat32_t v218 = svsub_f32_x(svptrue_b32(), v49, v157);
  svfloat32_t v219 = svsub_f32_x(svptrue_b32(), v67, v175);
  svfloat32_t v220 = svsub_f32_x(svptrue_b32(), v85, v139);
  svfloat32_t v221 = svsub_f32_x(svptrue_b32(), v103, v157);
  svfloat32_t v222 = svsub_f32_x(svptrue_b32(), v121, v175);
  svfloat32_t v223 = svadd_f32_x(svptrue_b32(), v31, v85);
  svfloat32_t v225 = svadd_f32_x(svptrue_b32(), v49, v103);
  svfloat32_t v227 = svadd_f32_x(svptrue_b32(), v67, v121);
  svfloat32_t v183 = svadd_f32_x(svptrue_b32(), v182, v138);
  svfloat32_t v185 = svadd_f32_x(svptrue_b32(), v184, v156);
  svfloat32_t v187 = svadd_f32_x(svptrue_b32(), v186, v174);
  svfloat32_t v188 = svadd_f32_x(svptrue_b32(), v176, v178);
  svfloat32_t v189 = svadd_f32_x(svptrue_b32(), v179, v181);
  svfloat32_t v207 = svsub_f32_x(svptrue_b32(), v176, v179);
  svfloat32_t v208 = svsub_f32_x(svptrue_b32(), v178, v181);
  svfloat32_t v224 = svadd_f32_x(svptrue_b32(), v223, v139);
  svfloat32_t v226 = svadd_f32_x(svptrue_b32(), v225, v157);
  svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v227, v175);
  svfloat32_t v229 = svadd_f32_x(svptrue_b32(), v217, v219);
  svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v220, v222);
  svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v217, v220);
  svfloat32_t v240 = svsub_f32_x(svptrue_b32(), v219, v222);
  svfloat32_t zero404 = svdup_n_f32(0);
  svfloat32_t v404 = svcmla_f32_x(pred_full, zero404, v936, v220, 90);
  svfloat32_t zero425 = svdup_n_f32(0);
  svfloat32_t v425 = svcmla_f32_x(pred_full, zero425, v939, v222, 90);
  svfloat32_t v190 = svadd_f32_x(svptrue_b32(), v183, v185);
  svfloat32_t v201 = svadd_f32_x(svptrue_b32(), v189, v180);
  svfloat32_t v202 = svadd_f32_x(svptrue_b32(), v188, v177);
  svfloat32_t v204 = svsub_f32_x(svptrue_b32(), v189, v180);
  svfloat32_t v205 = svsub_f32_x(svptrue_b32(), v188, v177);
  svfloat32_t v209 = svsub_f32_x(svptrue_b32(), v176, v208);
  svfloat32_t v211 = svadd_f32_x(svptrue_b32(), v207, v181);
  svfloat32_t v214 = svsub_f32_x(svptrue_b32(), v183, v187);
  svfloat32_t v215 = svsub_f32_x(svptrue_b32(), v185, v187);
  svfloat32_t v231 = svadd_f32_x(svptrue_b32(), v224, v226);
  svfloat32_t v233 = svadd_f32_x(svptrue_b32(), v230, v221);
  svfloat32_t v234 = svadd_f32_x(svptrue_b32(), v229, v218);
  svfloat32_t v236 = svsub_f32_x(svptrue_b32(), v230, v221);
  svfloat32_t v237 = svsub_f32_x(svptrue_b32(), v229, v218);
  svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v217, v240);
  svfloat32_t v243 = svadd_f32_x(svptrue_b32(), v239, v222);
  svfloat32_t v246 = svsub_f32_x(svptrue_b32(), v224, v228);
  svfloat32_t v247 = svsub_f32_x(svptrue_b32(), v226, v228);
  svfloat32_t v191 = svadd_f32_x(svptrue_b32(), v190, v187);
  svfloat32_t v203 = svsub_f32_x(svptrue_b32(), v202, v201);
  svfloat32_t v206 = svsub_f32_x(svptrue_b32(), v205, v204);
  svfloat32_t v210 = svsub_f32_x(svptrue_b32(), v209, v180);
  svfloat32_t v212 = svsub_f32_x(svptrue_b32(), v211, v177);
  svfloat32_t v216 = svadd_f32_x(svptrue_b32(), v214, v215);
  svfloat32_t v232 = svadd_f32_x(svptrue_b32(), v231, v228);
  svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v234, v233);
  svfloat32_t v238 = svsub_f32_x(svptrue_b32(), v237, v236);
  svfloat32_t v242 = svsub_f32_x(svptrue_b32(), v241, v221);
  svfloat32_t v244 = svsub_f32_x(svptrue_b32(), v243, v218);
  svfloat32_t v248 = svadd_f32_x(svptrue_b32(), v246, v247);
  svfloat32_t v268 = svmul_f32_x(svptrue_b32(), v202, v912);
  svfloat32_t v283 = svmul_f32_x(svptrue_b32(), v205, v915);
  svfloat32_t zero362 = svdup_n_f32(0);
  svfloat32_t v362 = svcmla_f32_x(pred_full, zero362, v930, v233, 90);
  svfloat32_t zero383 = svdup_n_f32(0);
  svfloat32_t v383 = svcmla_f32_x(pred_full, zero383, v933, v236, 90);
  svfloat32_t zero467 = svdup_n_f32(0);
  svfloat32_t v467 = svcmla_f32_x(pred_full, zero467, v945, v246, 90);
  svfloat32_t zero474 = svdup_n_f32(0);
  svfloat32_t v474 = svcmla_f32_x(pred_full, zero474, v946, v247, 90);
  svfloat32_t v200 = svadd_f32_x(svptrue_b32(), v199, v191);
  svfloat32_t v213 = svsub_f32_x(svptrue_b32(), v210, v212);
  svfloat32_t v245 = svsub_f32_x(svptrue_b32(), v242, v244);
  svfloat32_t v273 = svmul_f32_x(svptrue_b32(), v203, v913);
  svfloat32_t v288 = svmul_f32_x(svptrue_b32(), v206, v916);
  svfloat32_t v348 = svmul_f32_x(svptrue_b32(), v216, v928);
  svfloat32_t zero355 = svdup_n_f32(0);
  svfloat32_t v355 = svcmla_f32_x(pred_full, zero355, v929, v232, 90);
  svfloat32_t zero481 = svdup_n_f32(0);
  svfloat32_t v481 = svcmla_f32_x(pred_full, zero481, v947, v248, 90);
  svfloat32_t v482 = svmla_f32_x(pred_full, v268, v201, v911);
  svfloat32_t v483 = svmla_f32_x(pred_full, v283, v204, v914);
  svfloat32_t v513 = svcmla_f32_x(pred_full, v362, v931, v234, 90);
  svfloat32_t v514 = svcmla_f32_x(pred_full, v383, v934, v237, 90);
  svfloat32_t v333 = svmul_f32_x(svptrue_b32(), v213, v925);
  svfloat32_t zero460 = svdup_n_f32(0);
  svfloat32_t v460 = svcmla_f32_x(pred_full, zero460, v944, v245, 90);
  svfloat32_t v485 = svadd_f32_x(svptrue_b32(), v482, v483);
  svfloat32_t v486 = svmla_f32_x(pred_full, v273, v201, v911);
  svfloat32_t v487 = svmla_f32_x(pred_full, v288, v204, v914);
  svfloat32_t v504 = svsub_f32_x(svptrue_b32(), v482, v483);
  svfloat32_t v506 = svnmls_f32_x(pred_full, v348, v214, v926);
  svfloat32_t v507 = svnmls_f32_x(pred_full, v348, v215, v927);
  svfloat32_t v508 = svmla_f32_x(pred_full, v200, v191, v910);
  svfloat32_t v516 = svadd_f32_x(svptrue_b32(), v513, v514);
  svfloat32_t v517 = svcmla_f32_x(pred_full, v362, v932, v235, 90);
  svfloat32_t v518 = svcmla_f32_x(pred_full, v383, v935, v238, 90);
  svfloat32_t v535 = svsub_f32_x(svptrue_b32(), v513, v514);
  svfloat32_t v537 = svsub_f32_x(svptrue_b32(), v467, v481);
  svfloat32_t v538 = svsub_f32_x(svptrue_b32(), v474, v481);
  svint16_t v569 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v200, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v484 = svmla_f32_x(pred_full, v333, v212, v924);
  svfloat32_t v488 = svmla_f32_x(pred_full, v333, v210, v923);
  svfloat32_t v489 = svnmls_f32_x(pred_full, v485, v179, v917);
  svfloat32_t v490 = svadd_f32_x(svptrue_b32(), v486, v487);
  svfloat32_t v496 = svsub_f32_x(svptrue_b32(), v486, v487);
  svfloat32_t v501 = svmla_f32_x(pred_full, v485, v178, v922);
  svfloat32_t v509 = svadd_f32_x(svptrue_b32(), v508, v506);
  svfloat32_t v510 = svsub_f32_x(svptrue_b32(), v508, v506);
  svfloat32_t v512 = svadd_f32_x(svptrue_b32(), v508, v507);
  svfloat32_t v515 = svcmla_f32_x(pred_full, v460, v943, v244, 90);
  svfloat32_t v519 = svcmla_f32_x(pred_full, v460, v942, v242, 90);
  svfloat32_t v520 = svsub_f32_x(svptrue_b32(), v404, v516);
  svfloat32_t v521 = svadd_f32_x(svptrue_b32(), v517, v518);
  svfloat32_t v527 = svsub_f32_x(svptrue_b32(), v517, v518);
  svfloat32_t v532 = svcmla_f32_x(pred_full, v516, v941, v219, 90);
  svfloat32_t v539 = svadd_f32_x(svptrue_b32(), v355, v537);
  svfloat32_t v540 = svsub_f32_x(svptrue_b32(), v355, v537);
  svfloat32_t v542 = svadd_f32_x(svptrue_b32(), v355, v538);
  svst1w_u64(pred_full, (unsigned *)(v955), svreinterpret_u64_s16(v569));
  svfloat32_t v491 = svnmls_f32_x(pred_full, v488, v181, v920);
  svfloat32_t v492 = svmla_f32_x(pred_full, v484, v207, v918);
  svfloat32_t v494 = svmla_f32_x(pred_full, v490, v208, v921);
  svfloat32_t v497 = svadd_f32_x(svptrue_b32(), v496, v484);
  svfloat32_t v498 = svadd_f32_x(svptrue_b32(), v489, v490);
  svfloat32_t v505 = svadd_f32_x(svptrue_b32(), v504, v488);
  svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v510, v507);
  svfloat32_t v522 = svsub_f32_x(svptrue_b32(), v425, v519);
  svfloat32_t v523 = svcmla_f32_x(pred_full, v515, v937, v239, 90);
  svfloat32_t v525 = svcmla_f32_x(pred_full, v521, v940, v240, 90);
  svfloat32_t v528 = svadd_f32_x(svptrue_b32(), v527, v515);
  svfloat32_t v529 = svadd_f32_x(svptrue_b32(), v520, v521);
  svfloat32_t v536 = svadd_f32_x(svptrue_b32(), v535, v519);
  svfloat32_t v541 = svsub_f32_x(svptrue_b32(), v540, v538);
  svfloat32_t v493 = svadd_f32_x(svptrue_b32(), v492, v489);
  svfloat32_t v495 = svadd_f32_x(svptrue_b32(), v494, v491);
  svfloat32_t v499 = svmla_f32_x(pred_full, v498, v176, v919);
  svfloat32_t v502 = svadd_f32_x(svptrue_b32(), v501, v491);
  svfloat32_t v524 = svadd_f32_x(svptrue_b32(), v523, v520);
  svfloat32_t v526 = svadd_f32_x(svptrue_b32(), v525, v522);
  svfloat32_t v530 = svcmla_f32_x(pred_full, v529, v938, v217, 90);
  svfloat32_t v533 = svadd_f32_x(svptrue_b32(), v532, v522);
  svfloat32_t v547 = svsub_f32_x(svptrue_b32(), v505, v497);
  svfloat32_t v551 = svsub_f32_x(svptrue_b32(), v512, v505);
  svfloat32_t v554 = svadd_f32_x(svptrue_b32(), v497, v512);
  svfloat32_t v559 = svsub_f32_x(svptrue_b32(), v536, v528);
  svfloat32_t v563 = svsub_f32_x(svptrue_b32(), v536, v542);
  svfloat32_t v566 = svadd_f32_x(svptrue_b32(), v528, v542);
  svfloat32_t v500 = svadd_f32_x(svptrue_b32(), v499, v488);
  svfloat32_t v503 = svadd_f32_x(svptrue_b32(), v502, v484);
  svfloat32_t v531 = svadd_f32_x(svptrue_b32(), v530, v519);
  svfloat32_t v534 = svadd_f32_x(svptrue_b32(), v533, v515);
  svfloat32_t v548 = svadd_f32_x(svptrue_b32(), v547, v512);
  svfloat32_t v552 = svadd_f32_x(svptrue_b32(), v493, v509);
  svfloat32_t v553 = svadd_f32_x(svptrue_b32(), v495, v511);
  svfloat32_t v560 = svadd_f32_x(svptrue_b32(), v559, v542);
  svfloat32_t v564 = svadd_f32_x(svptrue_b32(), v524, v539);
  svfloat32_t v565 = svadd_f32_x(svptrue_b32(), v526, v541);
  svfloat32_t v593 = svsub_f32_x(svptrue_b32(), v554, v566);
  svfloat32_t v602 = svadd_f32_x(svptrue_b32(), v554, v566);
  svfloat32_t v611 = svadd_f32_x(svptrue_b32(), v551, v563);
  svfloat32_t v620 = svsub_f32_x(svptrue_b32(), v551, v563);
  svfloat32_t v543 = svsub_f32_x(svptrue_b32(), v500, v493);
  svfloat32_t v545 = svsub_f32_x(svptrue_b32(), v503, v495);
  svfloat32_t v549 = svsub_f32_x(svptrue_b32(), v509, v500);
  svfloat32_t v550 = svsub_f32_x(svptrue_b32(), v511, v503);
  svfloat32_t v555 = svsub_f32_x(svptrue_b32(), v531, v524);
  svfloat32_t v557 = svsub_f32_x(svptrue_b32(), v534, v526);
  svfloat32_t v561 = svsub_f32_x(svptrue_b32(), v539, v531);
  svfloat32_t v562 = svsub_f32_x(svptrue_b32(), v541, v534);
  svint16_t v596 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v593, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v605 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v602, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v614 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v611, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v623 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v620, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v629 = svadd_f32_x(svptrue_b32(), v553, v565);
  svfloat32_t v638 = svsub_f32_x(svptrue_b32(), v553, v565);
  svfloat32_t v647 = svadd_f32_x(svptrue_b32(), v548, v560);
  svfloat32_t v656 = svsub_f32_x(svptrue_b32(), v548, v560);
  svfloat32_t v701 = svsub_f32_x(svptrue_b32(), v552, v564);
  svfloat32_t v710 = svadd_f32_x(svptrue_b32(), v552, v564);
  svfloat32_t v544 = svadd_f32_x(svptrue_b32(), v543, v509);
  svfloat32_t v546 = svadd_f32_x(svptrue_b32(), v545, v511);
  svfloat32_t v556 = svadd_f32_x(svptrue_b32(), v555, v539);
  svfloat32_t v558 = svadd_f32_x(svptrue_b32(), v557, v541);
  svint16_t v632 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v629, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v641 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v638, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v650 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v647, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v659 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v656, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v665 = svadd_f32_x(svptrue_b32(), v550, v562);
  svfloat32_t v674 = svsub_f32_x(svptrue_b32(), v550, v562);
  svfloat32_t v683 = svadd_f32_x(svptrue_b32(), v549, v561);
  svfloat32_t v692 = svsub_f32_x(svptrue_b32(), v549, v561);
  svint16_t v704 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v701, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v713 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v710, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v982), svreinterpret_u64_s16(v596));
  svst1w_u64(pred_full, (unsigned *)(v991), svreinterpret_u64_s16(v605));
  svst1w_u64(pred_full, (unsigned *)(v1000), svreinterpret_u64_s16(v614));
  svst1w_u64(pred_full, (unsigned *)(v1009), svreinterpret_u64_s16(v623));
  svfloat32_t v575 = svadd_f32_x(svptrue_b32(), v544, v556);
  svfloat32_t v584 = svsub_f32_x(svptrue_b32(), v544, v556);
  svint16_t v668 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v665, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v677 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v674, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v686 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v683, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v695 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v692, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v719 = svadd_f32_x(svptrue_b32(), v546, v558);
  svfloat32_t v728 = svsub_f32_x(svptrue_b32(), v546, v558);
  svst1w_u64(pred_full, (unsigned *)(v1018), svreinterpret_u64_s16(v632));
  svst1w_u64(pred_full, (unsigned *)(v1027), svreinterpret_u64_s16(v641));
  svst1w_u64(pred_full, (unsigned *)(v1036), svreinterpret_u64_s16(v650));
  svst1w_u64(pred_full, (unsigned *)(v1045), svreinterpret_u64_s16(v659));
  svst1w_u64(pred_full, (unsigned *)(v1090), svreinterpret_u64_s16(v704));
  svst1w_u64(pred_full, (unsigned *)(v1099), svreinterpret_u64_s16(v713));
  svint16_t v578 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v575, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v587 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v584, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v722 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v719, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v731 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v728, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v1054), svreinterpret_u64_s16(v668));
  svst1w_u64(pred_full, (unsigned *)(v1063), svreinterpret_u64_s16(v677));
  svst1w_u64(pred_full, (unsigned *)(v1072), svreinterpret_u64_s16(v686));
  svst1w_u64(pred_full, (unsigned *)(v1081), svreinterpret_u64_s16(v695));
  svst1w_u64(pred_full, (unsigned *)(v964), svreinterpret_u64_s16(v578));
  svst1w_u64(pred_full, (unsigned *)(v973), svreinterpret_u64_s16(v587));
  svst1w_u64(pred_full, (unsigned *)(v1108), svreinterpret_u64_s16(v722));
  svst1w_u64(pred_full, (unsigned *)(v1117), svreinterpret_u64_s16(v731));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun20(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v279 = 1.5388417685876268e+00F;
  float v286 = 5.8778525229247325e-01F;
  float v293 = 3.6327126400268028e-01F;
  float v317 = 1.0000000000000000e+00F;
  float v318 = -1.0000000000000000e+00F;
  float v324 = -1.2500000000000000e+00F;
  float v325 = 1.2500000000000000e+00F;
  float v331 = 5.5901699437494745e-01F;
  float v332 = -5.5901699437494745e-01F;
  float v339 = -1.5388417685876268e+00F;
  float v343 = -5.8778525229247325e-01F;
  float v347 = -3.6327126400268028e-01F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v147 = vld1s_s16(&v5[istride]);
  float32x2_t v273 = (float32x2_t){v324, v324};
  float32x2_t v277 = (float32x2_t){v331, v331};
  float32x2_t v281 = (float32x2_t){v279, v339};
  float32x2_t v288 = (float32x2_t){v286, v343};
  float32x2_t v295 = (float32x2_t){v293, v347};
  float32x2_t v319 = (float32x2_t){v317, v318};
  float32x2_t v326 = (float32x2_t){v324, v325};
  float32x2_t v333 = (float32x2_t){v331, v332};
  float32x2_t v334 = (float32x2_t){v4, v4};
  float32x2_t v340 = (float32x2_t){v339, v339};
  float32x2_t v344 = (float32x2_t){v343, v343};
  float32x2_t v348 = (float32x2_t){v347, v347};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 15]);
  int16x4_t v43 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v49 = vld1s_s16(&v5[istride * 14]);
  int16x4_t v57 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v63 = vld1s_s16(&v5[istride * 19]);
  int16x4_t v73 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v79 = vld1s_s16(&v5[istride * 18]);
  int16x4_t v87 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v93 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v103 = vld1s_s16(&v5[istride * 12]);
  int16x4_t v109 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v117 = vld1s_s16(&v5[istride * 17]);
  int16x4_t v123 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v133 = vld1s_s16(&v5[istride * 16]);
  int16x4_t v139 = vld1s_s16(&v5[istride * 6]);
  float32x2_t v148 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v147)), 15);
  int16x4_t v153 = vld1s_s16(&v5[istride * 11]);
  float32x2_t v283 = vmul_f32(v334, v281);
  float32x2_t v290 = vmul_f32(v334, v288);
  float32x2_t v297 = vmul_f32(v334, v295);
  float32x2_t v321 = vmul_f32(v334, v319);
  float32x2_t v328 = vmul_f32(v334, v326);
  float32x2_t v335 = vmul_f32(v334, v333);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v44 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v43)), 15);
  float32x2_t v50 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v49)), 15);
  float32x2_t v58 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v57)), 15);
  float32x2_t v64 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v63)), 15);
  float32x2_t v74 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v73)), 15);
  float32x2_t v80 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v79)), 15);
  float32x2_t v88 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v87)), 15);
  float32x2_t v94 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v93)), 15);
  float32x2_t v104 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v103)), 15);
  float32x2_t v110 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v109)), 15);
  float32x2_t v118 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v117)), 15);
  float32x2_t v124 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v123)), 15);
  float32x2_t v134 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v133)), 15);
  float32x2_t v140 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v139)), 15);
  float32x2_t v154 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v153)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v51 = vadd_f32(v44, v50);
  float32x2_t v52 = vsub_f32(v44, v50);
  float32x2_t v65 = vadd_f32(v58, v64);
  float32x2_t v66 = vsub_f32(v58, v64);
  float32x2_t v81 = vadd_f32(v74, v80);
  float32x2_t v82 = vsub_f32(v74, v80);
  float32x2_t v95 = vadd_f32(v88, v94);
  float32x2_t v96 = vsub_f32(v88, v94);
  float32x2_t v111 = vadd_f32(v104, v110);
  float32x2_t v112 = vsub_f32(v104, v110);
  float32x2_t v125 = vadd_f32(v118, v124);
  float32x2_t v126 = vsub_f32(v118, v124);
  float32x2_t v141 = vadd_f32(v134, v140);
  float32x2_t v142 = vsub_f32(v134, v140);
  float32x2_t v155 = vadd_f32(v148, v154);
  float32x2_t v156 = vsub_f32(v148, v154);
  float32x2_t v37 = vadd_f32(v21, v35);
  float32x2_t v38 = vsub_f32(v21, v35);
  float32x2_t v67 = vadd_f32(v51, v65);
  float32x2_t v68 = vsub_f32(v51, v65);
  float32x2_t v97 = vadd_f32(v81, v95);
  float32x2_t v98 = vsub_f32(v81, v95);
  float32x2_t v127 = vadd_f32(v111, v125);
  float32x2_t v128 = vsub_f32(v111, v125);
  float32x2_t v157 = vadd_f32(v141, v155);
  float32x2_t v158 = vsub_f32(v141, v155);
  float32x2_t v259 = vadd_f32(v52, v142);
  float32x2_t v260 = vsub_f32(v52, v142);
  float32x2_t v261 = vadd_f32(v112, v82);
  float32x2_t v262 = vsub_f32(v112, v82);
  float32x2_t v309 = vadd_f32(v66, v156);
  float32x2_t v310 = vsub_f32(v66, v156);
  float32x2_t v311 = vadd_f32(v126, v96);
  float32x2_t v312 = vsub_f32(v126, v96);
  float32x2_t v159 = vadd_f32(v67, v157);
  float32x2_t v160 = vsub_f32(v67, v157);
  float32x2_t v161 = vadd_f32(v127, v97);
  float32x2_t v162 = vsub_f32(v127, v97);
  float32x2_t v209 = vadd_f32(v68, v158);
  float32x2_t v210 = vsub_f32(v68, v158);
  float32x2_t v211 = vadd_f32(v128, v98);
  float32x2_t v212 = vsub_f32(v128, v98);
  float32x2_t v263 = vadd_f32(v259, v261);
  float32x2_t v264 = vsub_f32(v259, v261);
  float32x2_t v265 = vadd_f32(v260, v262);
  float32x2_t v284 = vrev64_f32(v260);
  float32x2_t v298 = vrev64_f32(v262);
  float32x2_t v313 = vadd_f32(v309, v311);
  float32x2_t v314 = vsub_f32(v309, v311);
  float32x2_t v315 = vadd_f32(v310, v312);
  float32x2_t v341 = vmul_f32(v310, v340);
  float32x2_t v349 = vmul_f32(v312, v348);
  float32x2_t v163 = vadd_f32(v159, v161);
  float32x2_t v164 = vsub_f32(v159, v161);
  float32x2_t v165 = vadd_f32(v160, v162);
  float32x2_t v184 = vrev64_f32(v160);
  float32x2_t v198 = vrev64_f32(v162);
  float32x2_t v213 = vadd_f32(v209, v211);
  float32x2_t v214 = vsub_f32(v209, v211);
  float32x2_t v215 = vadd_f32(v210, v212);
  float32x2_t v234 = vrev64_f32(v210);
  float32x2_t v248 = vrev64_f32(v212);
  float32x2_t v266 = vadd_f32(v263, v22);
  float32x2_t v274 = vmul_f32(v263, v273);
  float32x2_t v278 = vmul_f32(v264, v277);
  float32x2_t v285 = vmul_f32(v284, v283);
  float32x2_t v291 = vrev64_f32(v265);
  float32x2_t v299 = vmul_f32(v298, v297);
  float32x2_t v316 = vadd_f32(v313, v36);
  float32x2_t v329 = vrev64_f32(v313);
  float32x2_t v336 = vrev64_f32(v314);
  float32x2_t v345 = vmul_f32(v315, v344);
  float32x2_t v166 = vadd_f32(v163, v37);
  float32x2_t v174 = vmul_f32(v163, v273);
  float32x2_t v178 = vmul_f32(v164, v277);
  float32x2_t v185 = vmul_f32(v184, v283);
  float32x2_t v191 = vrev64_f32(v165);
  float32x2_t v199 = vmul_f32(v198, v297);
  float32x2_t v216 = vadd_f32(v213, v38);
  float32x2_t v224 = vmul_f32(v213, v273);
  float32x2_t v228 = vmul_f32(v214, v277);
  float32x2_t v235 = vmul_f32(v234, v283);
  float32x2_t v241 = vrev64_f32(v215);
  float32x2_t v249 = vmul_f32(v248, v297);
  float32x2_t v292 = vmul_f32(v291, v290);
  float32x2_t v300 = vadd_f32(v266, v274);
  float32x2_t v322 = vrev64_f32(v316);
  float32x2_t v330 = vmul_f32(v329, v328);
  float32x2_t v337 = vmul_f32(v336, v335);
  float32x2_t v353 = vsub_f32(v341, v345);
  float32x2_t v354 = vadd_f32(v345, v349);
  float32x2_t v192 = vmul_f32(v191, v290);
  float32x2_t v200 = vadd_f32(v166, v174);
  float32x2_t v242 = vmul_f32(v241, v290);
  float32x2_t v250 = vadd_f32(v216, v224);
  float32x2_t v301 = vadd_f32(v300, v278);
  float32x2_t v302 = vsub_f32(v300, v278);
  float32x2_t v303 = vsub_f32(v285, v292);
  float32x2_t v304 = vadd_f32(v292, v299);
  float32x2_t v323 = vmul_f32(v322, v321);
  int16x4_t v363 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v166, 15), (int32x2_t){0, 0}));
  int16x4_t v375 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v216, 15), (int32x2_t){0, 0}));
  float32x2_t v201 = vadd_f32(v200, v178);
  float32x2_t v202 = vsub_f32(v200, v178);
  float32x2_t v203 = vsub_f32(v185, v192);
  float32x2_t v204 = vadd_f32(v192, v199);
  float32x2_t v251 = vadd_f32(v250, v228);
  float32x2_t v252 = vsub_f32(v250, v228);
  float32x2_t v253 = vsub_f32(v235, v242);
  float32x2_t v254 = vadd_f32(v242, v249);
  float32x2_t v305 = vadd_f32(v301, v303);
  float32x2_t v306 = vsub_f32(v301, v303);
  float32x2_t v307 = vadd_f32(v302, v304);
  float32x2_t v308 = vsub_f32(v302, v304);
  float32x2_t v350 = vadd_f32(v323, v330);
  float32x2_t v359 = vadd_f32(v266, v323);
  float32x2_t v360 = vsub_f32(v266, v323);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v363), 0);
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v375), 0);
  float32x2_t v205 = vadd_f32(v201, v203);
  float32x2_t v206 = vsub_f32(v201, v203);
  float32x2_t v207 = vadd_f32(v202, v204);
  float32x2_t v208 = vsub_f32(v202, v204);
  float32x2_t v255 = vadd_f32(v251, v253);
  float32x2_t v256 = vsub_f32(v251, v253);
  float32x2_t v257 = vadd_f32(v252, v254);
  float32x2_t v258 = vsub_f32(v252, v254);
  float32x2_t v351 = vadd_f32(v350, v337);
  float32x2_t v352 = vsub_f32(v350, v337);
  int16x4_t v369 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v360, 15), (int32x2_t){0, 0}));
  int16x4_t v381 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v359, 15), (int32x2_t){0, 0}));
  float32x2_t v355 = vadd_f32(v351, v353);
  float32x2_t v356 = vsub_f32(v351, v353);
  float32x2_t v357 = vadd_f32(v352, v354);
  float32x2_t v358 = vsub_f32(v352, v354);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v369), 0);
  v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v381), 0);
  int16x4_t v389 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v206, 15), (int32x2_t){0, 0}));
  int16x4_t v401 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v256, 15), (int32x2_t){0, 0}));
  int16x4_t v415 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v208, 15), (int32x2_t){0, 0}));
  int16x4_t v427 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v258, 15), (int32x2_t){0, 0}));
  int16x4_t v441 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v207, 15), (int32x2_t){0, 0}));
  int16x4_t v453 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v257, 15), (int32x2_t){0, 0}));
  int16x4_t v467 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v205, 15), (int32x2_t){0, 0}));
  int16x4_t v479 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v255, 15), (int32x2_t){0, 0}));
  float32x2_t v385 = vadd_f32(v306, v356);
  float32x2_t v386 = vsub_f32(v306, v356);
  v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v389), 0);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v401), 0);
  float32x2_t v411 = vadd_f32(v308, v358);
  float32x2_t v412 = vsub_f32(v308, v358);
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v415), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v427), 0);
  float32x2_t v437 = vadd_f32(v307, v357);
  float32x2_t v438 = vsub_f32(v307, v357);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v441), 0);
  v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v453), 0);
  float32x2_t v463 = vadd_f32(v305, v355);
  float32x2_t v464 = vsub_f32(v305, v355);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v467), 0);
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v479), 0);
  int16x4_t v395 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v386, 15), (int32x2_t){0, 0}));
  int16x4_t v407 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v385, 15), (int32x2_t){0, 0}));
  int16x4_t v421 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v412, 15), (int32x2_t){0, 0}));
  int16x4_t v433 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v411, 15), (int32x2_t){0, 0}));
  int16x4_t v447 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v438, 15), (int32x2_t){0, 0}));
  int16x4_t v459 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v437, 15), (int32x2_t){0, 0}));
  int16x4_t v473 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v464, 15), (int32x2_t){0, 0}));
  int16x4_t v485 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v463, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v395), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v407), 0);
  v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v421), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v433), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v447), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v459), 0);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v473), 0);
  v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v485), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun20(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v324 = -1.2500000000000000e+00F;
  float v329 = 5.5901699437494745e-01F;
  float v372 = -1.0000000000000000e+00F;
  float v379 = 1.2500000000000000e+00F;
  float v386 = -5.5901699437494745e-01F;
  float v393 = -1.5388417685876268e+00F;
  float v398 = -5.8778525229247325e-01F;
  float v403 = -3.6327126400268028e-01F;
  const int32_t *v755 = &v5[v0];
  int32_t *v843 = &v6[v2];
  int64_t v23 = v0 * 10;
  int64_t v33 = v0 * 5;
  int64_t v41 = v0 * 15;
  int64_t v53 = v0 * 4;
  int64_t v61 = v0 * 14;
  int64_t v71 = v0 * 9;
  int64_t v79 = v0 * 19;
  int64_t v91 = v0 * 8;
  int64_t v99 = v0 * 18;
  int64_t v109 = v0 * 13;
  int64_t v117 = v0 * 3;
  int64_t v129 = v0 * 12;
  int64_t v137 = v0 * 2;
  int64_t v147 = v0 * 17;
  int64_t v155 = v0 * 7;
  int64_t v167 = v0 * 16;
  int64_t v175 = v0 * 6;
  int64_t v193 = v0 * 11;
  float v337 = v4 * v393;
  float v344 = v4 * v398;
  float v351 = v4 * v403;
  float v375 = v4 * v372;
  float v382 = v4 * v379;
  float v389 = v4 * v386;
  int64_t v427 = v2 * 5;
  int64_t v435 = v2 * 10;
  int64_t v443 = v2 * 15;
  int64_t v453 = v2 * 16;
  int64_t v469 = v2 * 6;
  int64_t v477 = v2 * 11;
  int64_t v487 = v2 * 12;
  int64_t v495 = v2 * 17;
  int64_t v503 = v2 * 2;
  int64_t v511 = v2 * 7;
  int64_t v521 = v2 * 8;
  int64_t v529 = v2 * 13;
  int64_t v537 = v2 * 18;
  int64_t v545 = v2 * 3;
  int64_t v555 = v2 * 4;
  int64_t v563 = v2 * 9;
  int64_t v571 = v2 * 14;
  int64_t v579 = v2 * 19;
  const int32_t *v593 = &v5[0];
  svfloat32_t v780 = svdup_n_f32(v324);
  svfloat32_t v781 = svdup_n_f32(v329);
  svfloat32_t v788 = svdup_n_f32(v393);
  svfloat32_t v789 = svdup_n_f32(v398);
  svfloat32_t v790 = svdup_n_f32(v403);
  int32_t *v798 = &v6[0];
  svfloat32_t v191 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v755[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v602 = &v5[v23];
  const int32_t *v611 = &v5[v33];
  const int32_t *v620 = &v5[v41];
  const int32_t *v629 = &v5[v53];
  const int32_t *v638 = &v5[v61];
  const int32_t *v647 = &v5[v71];
  const int32_t *v656 = &v5[v79];
  const int32_t *v665 = &v5[v91];
  const int32_t *v674 = &v5[v99];
  const int32_t *v683 = &v5[v109];
  const int32_t *v692 = &v5[v117];
  const int32_t *v701 = &v5[v129];
  const int32_t *v710 = &v5[v137];
  const int32_t *v719 = &v5[v147];
  const int32_t *v728 = &v5[v155];
  const int32_t *v737 = &v5[v167];
  const int32_t *v746 = &v5[v175];
  const int32_t *v764 = &v5[v193];
  svfloat32_t v782 = svdup_n_f32(v337);
  svfloat32_t v783 = svdup_n_f32(v344);
  svfloat32_t v784 = svdup_n_f32(v351);
  svfloat32_t v785 = svdup_n_f32(v375);
  svfloat32_t v786 = svdup_n_f32(v382);
  svfloat32_t v787 = svdup_n_f32(v389);
  int32_t *v807 = &v6[v427];
  int32_t *v816 = &v6[v435];
  int32_t *v825 = &v6[v443];
  int32_t *v834 = &v6[v453];
  int32_t *v852 = &v6[v469];
  int32_t *v861 = &v6[v477];
  int32_t *v870 = &v6[v487];
  int32_t *v879 = &v6[v495];
  int32_t *v888 = &v6[v503];
  int32_t *v897 = &v6[v511];
  int32_t *v906 = &v6[v521];
  int32_t *v915 = &v6[v529];
  int32_t *v924 = &v6[v537];
  int32_t *v933 = &v6[v545];
  int32_t *v942 = &v6[v555];
  int32_t *v951 = &v6[v563];
  int32_t *v960 = &v6[v571];
  int32_t *v969 = &v6[v579];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v593[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v602[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v611[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v620[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v59 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v629[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v67 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v638[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v77 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v647[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v85 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v656[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v97 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v665[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v105 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v674[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v115 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v683[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v123 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v692[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v135 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v701[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v143 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v710[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v153 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v719[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v161 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v728[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v173 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v737[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v181 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v746[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v199 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v764[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v68 = svadd_f32_x(svptrue_b32(), v59, v67);
  svfloat32_t v69 = svsub_f32_x(svptrue_b32(), v59, v67);
  svfloat32_t v86 = svadd_f32_x(svptrue_b32(), v77, v85);
  svfloat32_t v87 = svsub_f32_x(svptrue_b32(), v77, v85);
  svfloat32_t v106 = svadd_f32_x(svptrue_b32(), v97, v105);
  svfloat32_t v107 = svsub_f32_x(svptrue_b32(), v97, v105);
  svfloat32_t v124 = svadd_f32_x(svptrue_b32(), v115, v123);
  svfloat32_t v125 = svsub_f32_x(svptrue_b32(), v115, v123);
  svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v135, v143);
  svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v135, v143);
  svfloat32_t v162 = svadd_f32_x(svptrue_b32(), v153, v161);
  svfloat32_t v163 = svsub_f32_x(svptrue_b32(), v153, v161);
  svfloat32_t v182 = svadd_f32_x(svptrue_b32(), v173, v181);
  svfloat32_t v183 = svsub_f32_x(svptrue_b32(), v173, v181);
  svfloat32_t v200 = svadd_f32_x(svptrue_b32(), v191, v199);
  svfloat32_t v201 = svsub_f32_x(svptrue_b32(), v191, v199);
  svfloat32_t v50 = svadd_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v51 = svsub_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v88 = svadd_f32_x(svptrue_b32(), v68, v86);
  svfloat32_t v89 = svsub_f32_x(svptrue_b32(), v68, v86);
  svfloat32_t v126 = svadd_f32_x(svptrue_b32(), v106, v124);
  svfloat32_t v127 = svsub_f32_x(svptrue_b32(), v106, v124);
  svfloat32_t v164 = svadd_f32_x(svptrue_b32(), v144, v162);
  svfloat32_t v165 = svsub_f32_x(svptrue_b32(), v144, v162);
  svfloat32_t v202 = svadd_f32_x(svptrue_b32(), v182, v200);
  svfloat32_t v203 = svsub_f32_x(svptrue_b32(), v182, v200);
  svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v69, v183);
  svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v69, v183);
  svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v145, v107);
  svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v145, v107);
  svfloat32_t v363 = svadd_f32_x(svptrue_b32(), v87, v201);
  svfloat32_t v364 = svsub_f32_x(svptrue_b32(), v87, v201);
  svfloat32_t v365 = svadd_f32_x(svptrue_b32(), v163, v125);
  svfloat32_t v366 = svsub_f32_x(svptrue_b32(), v163, v125);
  svfloat32_t v204 = svadd_f32_x(svptrue_b32(), v88, v202);
  svfloat32_t v205 = svsub_f32_x(svptrue_b32(), v88, v202);
  svfloat32_t v206 = svadd_f32_x(svptrue_b32(), v164, v126);
  svfloat32_t v207 = svsub_f32_x(svptrue_b32(), v164, v126);
  svfloat32_t v257 = svadd_f32_x(svptrue_b32(), v89, v203);
  svfloat32_t v258 = svsub_f32_x(svptrue_b32(), v89, v203);
  svfloat32_t v259 = svadd_f32_x(svptrue_b32(), v165, v127);
  svfloat32_t v260 = svsub_f32_x(svptrue_b32(), v165, v127);
  svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v310, v312);
  svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v310, v312);
  svfloat32_t v316 = svadd_f32_x(svptrue_b32(), v311, v313);
  svfloat32_t zero339 = svdup_n_f32(0);
  svfloat32_t v339 = svcmla_f32_x(pred_full, zero339, v782, v311, 90);
  svfloat32_t v367 = svadd_f32_x(svptrue_b32(), v363, v365);
  svfloat32_t v368 = svsub_f32_x(svptrue_b32(), v363, v365);
  svfloat32_t v369 = svadd_f32_x(svptrue_b32(), v364, v366);
  svfloat32_t v406 = svmul_f32_x(svptrue_b32(), v366, v790);
  svfloat32_t v208 = svadd_f32_x(svptrue_b32(), v204, v206);
  svfloat32_t v209 = svsub_f32_x(svptrue_b32(), v204, v206);
  svfloat32_t v210 = svadd_f32_x(svptrue_b32(), v205, v207);
  svfloat32_t zero233 = svdup_n_f32(0);
  svfloat32_t v233 = svcmla_f32_x(pred_full, zero233, v782, v205, 90);
  svfloat32_t v261 = svadd_f32_x(svptrue_b32(), v257, v259);
  svfloat32_t v262 = svsub_f32_x(svptrue_b32(), v257, v259);
  svfloat32_t v263 = svadd_f32_x(svptrue_b32(), v258, v260);
  svfloat32_t zero286 = svdup_n_f32(0);
  svfloat32_t v286 = svcmla_f32_x(pred_full, zero286, v782, v258, 90);
  svfloat32_t v317 = svadd_f32_x(svptrue_b32(), v314, v31);
  svfloat32_t zero346 = svdup_n_f32(0);
  svfloat32_t v346 = svcmla_f32_x(pred_full, zero346, v783, v316, 90);
  svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v367, v49);
  svfloat32_t zero391 = svdup_n_f32(0);
  svfloat32_t v391 = svcmla_f32_x(pred_full, zero391, v787, v368, 90);
  svfloat32_t v401 = svmul_f32_x(svptrue_b32(), v369, v789);
  svfloat32_t v211 = svadd_f32_x(svptrue_b32(), v208, v50);
  svfloat32_t zero240 = svdup_n_f32(0);
  svfloat32_t v240 = svcmla_f32_x(pred_full, zero240, v783, v210, 90);
  svfloat32_t v264 = svadd_f32_x(svptrue_b32(), v261, v51);
  svfloat32_t zero293 = svdup_n_f32(0);
  svfloat32_t v293 = svcmla_f32_x(pred_full, zero293, v783, v263, 90);
  svfloat32_t v354 = svmla_f32_x(pred_full, v317, v314, v780);
  svfloat32_t v357 = svsub_f32_x(svptrue_b32(), v339, v346);
  svfloat32_t v358 = svcmla_f32_x(pred_full, v346, v784, v313, 90);
  svfloat32_t zero377 = svdup_n_f32(0);
  svfloat32_t v377 = svcmla_f32_x(pred_full, zero377, v785, v370, 90);
  svfloat32_t v410 = svnmls_f32_x(pred_full, v401, v364, v788);
  svfloat32_t v411 = svmla_f32_x(pred_full, v406, v369, v789);
  svfloat32_t v248 = svmla_f32_x(pred_full, v211, v208, v780);
  svfloat32_t v251 = svsub_f32_x(svptrue_b32(), v233, v240);
  svfloat32_t v252 = svcmla_f32_x(pred_full, v240, v784, v207, 90);
  svfloat32_t v301 = svmla_f32_x(pred_full, v264, v261, v780);
  svfloat32_t v304 = svsub_f32_x(svptrue_b32(), v286, v293);
  svfloat32_t v305 = svcmla_f32_x(pred_full, v293, v784, v260, 90);
  svfloat32_t v355 = svmla_f32_x(pred_full, v354, v315, v781);
  svfloat32_t v356 = svmls_f32_x(pred_full, v354, v315, v781);
  svfloat32_t v407 = svcmla_f32_x(pred_full, v377, v786, v367, 90);
  svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v317, v377);
  svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v317, v377);
  svint16_t v420 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v211, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v436 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v264, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v249 = svmla_f32_x(pred_full, v248, v209, v781);
  svfloat32_t v250 = svmls_f32_x(pred_full, v248, v209, v781);
  svfloat32_t v302 = svmla_f32_x(pred_full, v301, v262, v781);
  svfloat32_t v303 = svmls_f32_x(pred_full, v301, v262, v781);
  svfloat32_t v359 = svadd_f32_x(svptrue_b32(), v355, v357);
  svfloat32_t v360 = svsub_f32_x(svptrue_b32(), v355, v357);
  svfloat32_t v361 = svadd_f32_x(svptrue_b32(), v356, v358);
  svfloat32_t v362 = svsub_f32_x(svptrue_b32(), v356, v358);
  svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v407, v391);
  svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v407, v391);
  svint16_t v428 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v417, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v444 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v416, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v798), svreinterpret_u64_s16(v420));
  svst1w_u64(pred_full, (unsigned *)(v816), svreinterpret_u64_s16(v436));
  svfloat32_t v253 = svadd_f32_x(svptrue_b32(), v249, v251);
  svfloat32_t v254 = svsub_f32_x(svptrue_b32(), v249, v251);
  svfloat32_t v255 = svadd_f32_x(svptrue_b32(), v250, v252);
  svfloat32_t v256 = svsub_f32_x(svptrue_b32(), v250, v252);
  svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v302, v304);
  svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v302, v304);
  svfloat32_t v308 = svadd_f32_x(svptrue_b32(), v303, v305);
  svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v303, v305);
  svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v408, v410);
  svfloat32_t v413 = svsub_f32_x(svptrue_b32(), v408, v410);
  svfloat32_t v414 = svadd_f32_x(svptrue_b32(), v409, v411);
  svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v409, v411);
  svst1w_u64(pred_full, (unsigned *)(v807), svreinterpret_u64_s16(v428));
  svst1w_u64(pred_full, (unsigned *)(v825), svreinterpret_u64_s16(v444));
  svfloat32_t v450 = svadd_f32_x(svptrue_b32(), v360, v413);
  svfloat32_t v451 = svsub_f32_x(svptrue_b32(), v360, v413);
  svint16_t v454 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v254, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v470 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v307, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v484 = svadd_f32_x(svptrue_b32(), v362, v415);
  svfloat32_t v485 = svsub_f32_x(svptrue_b32(), v362, v415);
  svint16_t v488 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v256, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v504 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v309, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v518 = svadd_f32_x(svptrue_b32(), v361, v414);
  svfloat32_t v519 = svsub_f32_x(svptrue_b32(), v361, v414);
  svint16_t v522 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v255, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v538 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v308, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v552 = svadd_f32_x(svptrue_b32(), v359, v412);
  svfloat32_t v553 = svsub_f32_x(svptrue_b32(), v359, v412);
  svint16_t v556 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v253, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v572 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v306, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v462 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v451, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v478 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v450, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v496 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v485, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v512 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v484, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v530 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v519, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v546 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v518, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v564 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v553, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v580 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v552, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v834), svreinterpret_u64_s16(v454));
  svst1w_u64(pred_full, (unsigned *)(v852), svreinterpret_u64_s16(v470));
  svst1w_u64(pred_full, (unsigned *)(v870), svreinterpret_u64_s16(v488));
  svst1w_u64(pred_full, (unsigned *)(v888), svreinterpret_u64_s16(v504));
  svst1w_u64(pred_full, (unsigned *)(v906), svreinterpret_u64_s16(v522));
  svst1w_u64(pred_full, (unsigned *)(v924), svreinterpret_u64_s16(v538));
  svst1w_u64(pred_full, (unsigned *)(v942), svreinterpret_u64_s16(v556));
  svst1w_u64(pred_full, (unsigned *)(v960), svreinterpret_u64_s16(v572));
  svst1w_u64(pred_full, (unsigned *)(v843), svreinterpret_u64_s16(v462));
  svst1w_u64(pred_full, (unsigned *)(v861), svreinterpret_u64_s16(v478));
  svst1w_u64(pred_full, (unsigned *)(v879), svreinterpret_u64_s16(v496));
  svst1w_u64(pred_full, (unsigned *)(v897), svreinterpret_u64_s16(v512));
  svst1w_u64(pred_full, (unsigned *)(v915), svreinterpret_u64_s16(v530));
  svst1w_u64(pred_full, (unsigned *)(v933), svreinterpret_u64_s16(v546));
  svst1w_u64(pred_full, (unsigned *)(v951), svreinterpret_u64_s16(v564));
  svst1w_u64(pred_full, (unsigned *)(v969), svreinterpret_u64_s16(v580));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun21(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v178 = -1.1666666666666665e+00F;
  float v182 = 7.9015646852540022e-01F;
  float v186 = 5.5854267289647742e-02F;
  float v190 = 7.3430220123575241e-01F;
  float v193 = 4.4095855184409838e-01F;
  float v194 = -4.4095855184409838e-01F;
  float v200 = 3.4087293062393137e-01F;
  float v201 = -3.4087293062393137e-01F;
  float v207 = -5.3396936033772524e-01F;
  float v208 = 5.3396936033772524e-01F;
  float v214 = 8.7484229096165667e-01F;
  float v215 = -8.7484229096165667e-01F;
  float v258 = -1.4999999999999998e+00F;
  float v262 = 1.7499999999999996e+00F;
  float v266 = -1.1852347027881001e+00F;
  float v270 = -8.3781400934471603e-02F;
  float v274 = -1.1014533018536286e+00F;
  float v277 = -6.6143782776614746e-01F;
  float v278 = 6.6143782776614746e-01F;
  float v284 = -5.1130939593589697e-01F;
  float v285 = 5.1130939593589697e-01F;
  float v291 = 8.0095404050658769e-01F;
  float v292 = -8.0095404050658769e-01F;
  float v298 = -1.3122634364424848e+00F;
  float v299 = 1.3122634364424848e+00F;
  float v341 = 8.6602540378443871e-01F;
  float v342 = -8.6602540378443871e-01F;
  float v348 = -1.0103629710818451e+00F;
  float v349 = 1.0103629710818451e+00F;
  float v355 = 6.8429557470759583e-01F;
  float v356 = -6.8429557470759583e-01F;
  float v362 = 4.8371214382601155e-02F;
  float v363 = -4.8371214382601155e-02F;
  float v369 = 6.3592436032499466e-01F;
  float v370 = -6.3592436032499466e-01F;
  float v377 = -3.8188130791298663e-01F;
  float v381 = -2.9520461738277515e-01F;
  float v385 = 4.6243103089499693e-01F;
  float v389 = -7.5763564827777208e-01F;
  int16x4_t v27 = vld1s_s16(&v5[0]);
  int16x4_t v118 = vld1s_s16(&v5[istride]);
  float32x2_t v179 = (float32x2_t){v178, v178};
  float32x2_t v183 = (float32x2_t){v182, v182};
  float32x2_t v187 = (float32x2_t){v186, v186};
  float32x2_t v191 = (float32x2_t){v190, v190};
  float32x2_t v195 = (float32x2_t){v193, v194};
  float32x2_t v202 = (float32x2_t){v200, v201};
  float32x2_t v209 = (float32x2_t){v207, v208};
  float32x2_t v216 = (float32x2_t){v214, v215};
  float32x2_t v259 = (float32x2_t){v258, v258};
  float32x2_t v263 = (float32x2_t){v262, v262};
  float32x2_t v267 = (float32x2_t){v266, v266};
  float32x2_t v271 = (float32x2_t){v270, v270};
  float32x2_t v275 = (float32x2_t){v274, v274};
  float32x2_t v279 = (float32x2_t){v277, v278};
  float32x2_t v286 = (float32x2_t){v284, v285};
  float32x2_t v293 = (float32x2_t){v291, v292};
  float32x2_t v300 = (float32x2_t){v298, v299};
  float32x2_t v343 = (float32x2_t){v341, v342};
  float32x2_t v350 = (float32x2_t){v348, v349};
  float32x2_t v357 = (float32x2_t){v355, v356};
  float32x2_t v364 = (float32x2_t){v362, v363};
  float32x2_t v371 = (float32x2_t){v369, v370};
  float32x2_t v372 = (float32x2_t){v4, v4};
  float32x2_t v378 = (float32x2_t){v377, v377};
  float32x2_t v382 = (float32x2_t){v381, v381};
  float32x2_t v386 = (float32x2_t){v385, v385};
  float32x2_t v390 = (float32x2_t){v389, v389};
  int16x4_t v13 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v19 = vld1s_s16(&v5[istride * 14]);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  int16x4_t v34 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v40 = vld1s_s16(&v5[istride * 17]);
  int16x4_t v48 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 20]);
  int16x4_t v69 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v76 = vld1s_s16(&v5[istride * 16]);
  int16x4_t v82 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v90 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v97 = vld1s_s16(&v5[istride * 19]);
  int16x4_t v103 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v111 = vld1s_s16(&v5[istride * 12]);
  float32x2_t v119 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v118)), 15);
  int16x4_t v124 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v132 = vld1s_s16(&v5[istride * 15]);
  int16x4_t v139 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v145 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v153 = vld1s_s16(&v5[istride * 18]);
  float32x2_t v197 = vmul_f32(v372, v195);
  float32x2_t v204 = vmul_f32(v372, v202);
  float32x2_t v211 = vmul_f32(v372, v209);
  float32x2_t v218 = vmul_f32(v372, v216);
  float32x2_t v281 = vmul_f32(v372, v279);
  float32x2_t v288 = vmul_f32(v372, v286);
  float32x2_t v295 = vmul_f32(v372, v293);
  float32x2_t v302 = vmul_f32(v372, v300);
  float32x2_t v345 = vmul_f32(v372, v343);
  float32x2_t v352 = vmul_f32(v372, v350);
  float32x2_t v359 = vmul_f32(v372, v357);
  float32x2_t v366 = vmul_f32(v372, v364);
  float32x2_t v373 = vmul_f32(v372, v371);
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v35 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v34)), 15);
  float32x2_t v41 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v40)), 15);
  float32x2_t v49 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v48)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  float32x2_t v77 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v76)), 15);
  float32x2_t v83 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v82)), 15);
  float32x2_t v91 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v90)), 15);
  float32x2_t v98 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v97)), 15);
  float32x2_t v104 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v103)), 15);
  float32x2_t v112 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v111)), 15);
  float32x2_t v125 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v124)), 15);
  float32x2_t v133 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v132)), 15);
  float32x2_t v140 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v139)), 15);
  float32x2_t v146 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v145)), 15);
  float32x2_t v154 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v153)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v42 = vadd_f32(v35, v41);
  float32x2_t v43 = vsub_f32(v35, v41);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v84 = vadd_f32(v77, v83);
  float32x2_t v85 = vsub_f32(v77, v83);
  float32x2_t v105 = vadd_f32(v98, v104);
  float32x2_t v106 = vsub_f32(v98, v104);
  float32x2_t v126 = vadd_f32(v119, v125);
  float32x2_t v127 = vsub_f32(v119, v125);
  float32x2_t v147 = vadd_f32(v140, v146);
  float32x2_t v148 = vsub_f32(v140, v146);
  float32x2_t v29 = vadd_f32(v21, v28);
  float32x2_t v50 = vadd_f32(v42, v49);
  float32x2_t v71 = vadd_f32(v63, v70);
  float32x2_t v92 = vadd_f32(v84, v91);
  float32x2_t v113 = vadd_f32(v105, v112);
  float32x2_t v134 = vadd_f32(v126, v133);
  float32x2_t v155 = vadd_f32(v147, v154);
  float32x2_t v240 = vadd_f32(v42, v147);
  float32x2_t v241 = vsub_f32(v42, v147);
  float32x2_t v242 = vadd_f32(v105, v84);
  float32x2_t v243 = vsub_f32(v105, v84);
  float32x2_t v244 = vadd_f32(v63, v126);
  float32x2_t v245 = vsub_f32(v63, v126);
  float32x2_t v324 = vadd_f32(v43, v148);
  float32x2_t v325 = vsub_f32(v43, v148);
  float32x2_t v326 = vadd_f32(v106, v85);
  float32x2_t v327 = vsub_f32(v106, v85);
  float32x2_t v328 = vadd_f32(v64, v127);
  float32x2_t v329 = vsub_f32(v64, v127);
  float32x2_t v156 = vadd_f32(v50, v155);
  float32x2_t v157 = vsub_f32(v50, v155);
  float32x2_t v158 = vadd_f32(v113, v92);
  float32x2_t v159 = vsub_f32(v113, v92);
  float32x2_t v160 = vadd_f32(v71, v134);
  float32x2_t v161 = vsub_f32(v71, v134);
  float32x2_t v246 = vadd_f32(v240, v242);
  float32x2_t v249 = vsub_f32(v240, v242);
  float32x2_t v250 = vsub_f32(v242, v244);
  float32x2_t v251 = vsub_f32(v244, v240);
  float32x2_t v252 = vadd_f32(v241, v243);
  float32x2_t v254 = vsub_f32(v241, v243);
  float32x2_t v255 = vsub_f32(v243, v245);
  float32x2_t v256 = vsub_f32(v245, v241);
  float32x2_t v330 = vadd_f32(v324, v326);
  float32x2_t v333 = vsub_f32(v324, v326);
  float32x2_t v334 = vsub_f32(v326, v328);
  float32x2_t v335 = vsub_f32(v328, v324);
  float32x2_t v336 = vadd_f32(v325, v327);
  float32x2_t v338 = vsub_f32(v325, v327);
  float32x2_t v339 = vsub_f32(v327, v329);
  float32x2_t v340 = vsub_f32(v329, v325);
  float32x2_t v162 = vadd_f32(v156, v158);
  float32x2_t v165 = vsub_f32(v156, v158);
  float32x2_t v166 = vsub_f32(v158, v160);
  float32x2_t v167 = vsub_f32(v160, v156);
  float32x2_t v168 = vadd_f32(v157, v159);
  float32x2_t v170 = vsub_f32(v157, v159);
  float32x2_t v171 = vsub_f32(v159, v161);
  float32x2_t v172 = vsub_f32(v161, v157);
  float32x2_t v247 = vadd_f32(v246, v244);
  float32x2_t v253 = vadd_f32(v252, v245);
  float32x2_t v268 = vmul_f32(v249, v267);
  float32x2_t v272 = vmul_f32(v250, v271);
  float32x2_t v276 = vmul_f32(v251, v275);
  float32x2_t v289 = vrev64_f32(v254);
  float32x2_t v296 = vrev64_f32(v255);
  float32x2_t v303 = vrev64_f32(v256);
  float32x2_t v331 = vadd_f32(v330, v328);
  float32x2_t v337 = vadd_f32(v336, v329);
  float32x2_t v360 = vrev64_f32(v333);
  float32x2_t v367 = vrev64_f32(v334);
  float32x2_t v374 = vrev64_f32(v335);
  float32x2_t v383 = vmul_f32(v338, v382);
  float32x2_t v387 = vmul_f32(v339, v386);
  float32x2_t v391 = vmul_f32(v340, v390);
  float32x2_t v163 = vadd_f32(v162, v160);
  float32x2_t v169 = vadd_f32(v168, v161);
  float32x2_t v184 = vmul_f32(v165, v183);
  float32x2_t v188 = vmul_f32(v166, v187);
  float32x2_t v192 = vmul_f32(v167, v191);
  float32x2_t v205 = vrev64_f32(v170);
  float32x2_t v212 = vrev64_f32(v171);
  float32x2_t v219 = vrev64_f32(v172);
  float32x2_t v248 = vadd_f32(v247, v21);
  float32x2_t v264 = vmul_f32(v247, v263);
  float32x2_t v282 = vrev64_f32(v253);
  float32x2_t v290 = vmul_f32(v289, v288);
  float32x2_t v297 = vmul_f32(v296, v295);
  float32x2_t v304 = vmul_f32(v303, v302);
  float32x2_t v332 = vadd_f32(v331, v22);
  float32x2_t v353 = vrev64_f32(v331);
  float32x2_t v361 = vmul_f32(v360, v359);
  float32x2_t v368 = vmul_f32(v367, v366);
  float32x2_t v375 = vmul_f32(v374, v373);
  float32x2_t v379 = vmul_f32(v337, v378);
  float32x2_t v164 = vadd_f32(v163, v29);
  float32x2_t v180 = vmul_f32(v163, v179);
  float32x2_t v198 = vrev64_f32(v169);
  float32x2_t v206 = vmul_f32(v205, v204);
  float32x2_t v213 = vmul_f32(v212, v211);
  float32x2_t v220 = vmul_f32(v219, v218);
  float32x2_t v260 = vmul_f32(v248, v259);
  float32x2_t v283 = vmul_f32(v282, v281);
  float32x2_t v346 = vrev64_f32(v332);
  float32x2_t v354 = vmul_f32(v353, v352);
  float32x2_t v399 = vadd_f32(v379, v383);
  float32x2_t v401 = vsub_f32(v379, v383);
  float32x2_t v403 = vsub_f32(v379, v387);
  float32x2_t v199 = vmul_f32(v198, v197);
  float32x2_t v221 = vadd_f32(v164, v180);
  float32x2_t v305 = vadd_f32(v260, v264);
  float32x2_t v312 = vadd_f32(v283, v290);
  float32x2_t v314 = vsub_f32(v283, v290);
  float32x2_t v316 = vsub_f32(v283, v297);
  float32x2_t v347 = vmul_f32(v346, v345);
  float32x2_t v400 = vadd_f32(v399, v387);
  float32x2_t v402 = vsub_f32(v401, v391);
  float32x2_t v404 = vadd_f32(v403, v391);
  float32x2_t v411 = vadd_f32(v164, v260);
  int16x4_t v416 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v164, 15), (int32x2_t){0, 0}));
  float32x2_t v222 = vadd_f32(v221, v184);
  float32x2_t v224 = vsub_f32(v221, v184);
  float32x2_t v226 = vsub_f32(v221, v188);
  float32x2_t v228 = vadd_f32(v199, v206);
  float32x2_t v230 = vsub_f32(v199, v206);
  float32x2_t v232 = vsub_f32(v199, v213);
  float32x2_t v306 = vadd_f32(v305, v268);
  float32x2_t v308 = vsub_f32(v305, v268);
  float32x2_t v310 = vsub_f32(v305, v272);
  float32x2_t v313 = vadd_f32(v312, v297);
  float32x2_t v315 = vsub_f32(v314, v304);
  float32x2_t v317 = vadd_f32(v316, v304);
  float32x2_t v392 = vadd_f32(v347, v354);
  float32x2_t v412 = vadd_f32(v411, v347);
  float32x2_t v413 = vsub_f32(v411, v347);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v416), 0);
  float32x2_t v223 = vadd_f32(v222, v188);
  float32x2_t v225 = vsub_f32(v224, v192);
  float32x2_t v227 = vadd_f32(v226, v192);
  float32x2_t v229 = vadd_f32(v228, v213);
  float32x2_t v231 = vsub_f32(v230, v220);
  float32x2_t v233 = vadd_f32(v232, v220);
  float32x2_t v307 = vadd_f32(v306, v272);
  float32x2_t v309 = vsub_f32(v308, v276);
  float32x2_t v311 = vadd_f32(v310, v276);
  float32x2_t v393 = vadd_f32(v392, v361);
  float32x2_t v395 = vsub_f32(v392, v361);
  float32x2_t v397 = vsub_f32(v392, v368);
  int16x4_t v422 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v413, 15), (int32x2_t){0, 0}));
  int16x4_t v428 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v412, 15), (int32x2_t){0, 0}));
  float32x2_t v234 = vadd_f32(v223, v229);
  float32x2_t v235 = vsub_f32(v223, v229);
  float32x2_t v236 = vadd_f32(v225, v231);
  float32x2_t v237 = vsub_f32(v225, v231);
  float32x2_t v238 = vadd_f32(v227, v233);
  float32x2_t v239 = vsub_f32(v227, v233);
  float32x2_t v318 = vadd_f32(v307, v313);
  float32x2_t v319 = vsub_f32(v307, v313);
  float32x2_t v320 = vadd_f32(v309, v315);
  float32x2_t v321 = vsub_f32(v309, v315);
  float32x2_t v322 = vadd_f32(v311, v317);
  float32x2_t v323 = vsub_f32(v311, v317);
  float32x2_t v394 = vadd_f32(v393, v368);
  float32x2_t v396 = vsub_f32(v395, v375);
  float32x2_t v398 = vadd_f32(v397, v375);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v422), 0);
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v428), 0);
  float32x2_t v405 = vadd_f32(v394, v400);
  float32x2_t v406 = vsub_f32(v394, v400);
  float32x2_t v407 = vadd_f32(v396, v402);
  float32x2_t v408 = vsub_f32(v396, v402);
  float32x2_t v409 = vadd_f32(v398, v404);
  float32x2_t v410 = vsub_f32(v398, v404);
  float32x2_t v432 = vadd_f32(v235, v319);
  int16x4_t v437 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v235, 15), (int32x2_t){0, 0}));
  float32x2_t v453 = vadd_f32(v237, v321);
  int16x4_t v458 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v237, 15), (int32x2_t){0, 0}));
  float32x2_t v474 = vadd_f32(v238, v322);
  int16x4_t v479 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v238, 15), (int32x2_t){0, 0}));
  float32x2_t v495 = vadd_f32(v239, v323);
  int16x4_t v500 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v239, 15), (int32x2_t){0, 0}));
  float32x2_t v516 = vadd_f32(v236, v320);
  int16x4_t v521 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v236, 15), (int32x2_t){0, 0}));
  float32x2_t v537 = vadd_f32(v234, v318);
  int16x4_t v542 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v234, 15), (int32x2_t){0, 0}));
  float32x2_t v433 = vadd_f32(v432, v406);
  float32x2_t v434 = vsub_f32(v432, v406);
  v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v437), 0);
  float32x2_t v454 = vadd_f32(v453, v408);
  float32x2_t v455 = vsub_f32(v453, v408);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v458), 0);
  float32x2_t v475 = vadd_f32(v474, v409);
  float32x2_t v476 = vsub_f32(v474, v409);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v479), 0);
  float32x2_t v496 = vadd_f32(v495, v410);
  float32x2_t v497 = vsub_f32(v495, v410);
  v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v500), 0);
  float32x2_t v517 = vadd_f32(v516, v407);
  float32x2_t v518 = vsub_f32(v516, v407);
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v521), 0);
  float32x2_t v538 = vadd_f32(v537, v405);
  float32x2_t v539 = vsub_f32(v537, v405);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v542), 0);
  int16x4_t v443 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v434, 15), (int32x2_t){0, 0}));
  int16x4_t v449 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v433, 15), (int32x2_t){0, 0}));
  int16x4_t v464 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v455, 15), (int32x2_t){0, 0}));
  int16x4_t v470 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v454, 15), (int32x2_t){0, 0}));
  int16x4_t v485 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v476, 15), (int32x2_t){0, 0}));
  int16x4_t v491 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v475, 15), (int32x2_t){0, 0}));
  int16x4_t v506 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v497, 15), (int32x2_t){0, 0}));
  int16x4_t v512 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v496, 15), (int32x2_t){0, 0}));
  int16x4_t v527 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v518, 15), (int32x2_t){0, 0}));
  int16x4_t v533 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v517, 15), (int32x2_t){0, 0}));
  int16x4_t v548 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v539, 15), (int32x2_t){0, 0}));
  int16x4_t v554 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v538, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v443), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v449), 0);
  v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v464), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v470), 0);
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v485), 0);
  v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v491), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v506), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v512), 0);
  v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v527), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v533), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v548), 0);
  v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v554), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun21(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v226 = -1.1666666666666665e+00F;
  float v231 = 7.9015646852540022e-01F;
  float v236 = 5.5854267289647742e-02F;
  float v241 = 7.3430220123575241e-01F;
  float v246 = -4.4095855184409838e-01F;
  float v253 = -3.4087293062393137e-01F;
  float v260 = 5.3396936033772524e-01F;
  float v267 = -8.7484229096165667e-01F;
  float v310 = -1.4999999999999998e+00F;
  float v315 = 1.7499999999999996e+00F;
  float v320 = -1.1852347027881001e+00F;
  float v325 = -8.3781400934471603e-02F;
  float v330 = -1.1014533018536286e+00F;
  float v335 = 6.6143782776614746e-01F;
  float v342 = 5.1130939593589697e-01F;
  float v349 = -8.0095404050658769e-01F;
  float v356 = 1.3122634364424848e+00F;
  float v399 = -8.6602540378443871e-01F;
  float v406 = 1.0103629710818451e+00F;
  float v413 = -6.8429557470759583e-01F;
  float v420 = -4.8371214382601155e-02F;
  float v427 = -6.3592436032499466e-01F;
  float v434 = -3.8188130791298663e-01F;
  float v439 = -2.9520461738277515e-01F;
  float v444 = 4.6243103089499693e-01F;
  float v449 = -7.5763564827777208e-01F;
  const int32_t *v803 = &v5[v0];
  int32_t *v921 = &v6[v2];
  int64_t v15 = v0 * 7;
  int64_t v23 = v0 * 14;
  int64_t v42 = v0 * 10;
  int64_t v50 = v0 * 17;
  int64_t v60 = v0 * 3;
  int64_t v69 = v0 * 13;
  int64_t v77 = v0 * 20;
  int64_t v87 = v0 * 6;
  int64_t v96 = v0 * 16;
  int64_t v104 = v0 * 2;
  int64_t v114 = v0 * 9;
  int64_t v123 = v0 * 19;
  int64_t v131 = v0 * 5;
  int64_t v141 = v0 * 12;
  int64_t v158 = v0 * 8;
  int64_t v168 = v0 * 15;
  int64_t v177 = v0 * 4;
  int64_t v185 = v0 * 11;
  int64_t v195 = v0 * 18;
  float v249 = v4 * v246;
  float v256 = v4 * v253;
  float v263 = v4 * v260;
  float v270 = v4 * v267;
  float v338 = v4 * v335;
  float v345 = v4 * v342;
  float v352 = v4 * v349;
  float v359 = v4 * v356;
  float v402 = v4 * v399;
  float v409 = v4 * v406;
  float v416 = v4 * v413;
  float v423 = v4 * v420;
  float v430 = v4 * v427;
  int64_t v484 = v2 * 7;
  int64_t v492 = v2 * 14;
  int64_t v503 = v2 * 15;
  int64_t v519 = v2 * 8;
  int64_t v530 = v2 * 9;
  int64_t v538 = v2 * 16;
  int64_t v546 = v2 * 2;
  int64_t v557 = v2 * 3;
  int64_t v565 = v2 * 10;
  int64_t v573 = v2 * 17;
  int64_t v584 = v2 * 18;
  int64_t v592 = v2 * 4;
  int64_t v600 = v2 * 11;
  int64_t v611 = v2 * 12;
  int64_t v619 = v2 * 19;
  int64_t v627 = v2 * 5;
  int64_t v638 = v2 * 6;
  int64_t v646 = v2 * 13;
  int64_t v654 = v2 * 20;
  const int32_t *v686 = &v5[0];
  svfloat32_t v852 = svdup_n_f32(v226);
  svfloat32_t v853 = svdup_n_f32(v231);
  svfloat32_t v854 = svdup_n_f32(v236);
  svfloat32_t v855 = svdup_n_f32(v241);
  svfloat32_t v860 = svdup_n_f32(v310);
  svfloat32_t v861 = svdup_n_f32(v315);
  svfloat32_t v862 = svdup_n_f32(v320);
  svfloat32_t v863 = svdup_n_f32(v325);
  svfloat32_t v864 = svdup_n_f32(v330);
  svfloat32_t v874 = svdup_n_f32(v434);
  svfloat32_t v875 = svdup_n_f32(v439);
  svfloat32_t v876 = svdup_n_f32(v444);
  svfloat32_t v877 = svdup_n_f32(v449);
  int32_t *v885 = &v6[0];
  svfloat32_t v156 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v803[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v667 = &v5[v15];
  const int32_t *v676 = &v5[v23];
  const int32_t *v695 = &v5[v42];
  const int32_t *v704 = &v5[v50];
  const int32_t *v713 = &v5[v60];
  const int32_t *v722 = &v5[v69];
  const int32_t *v731 = &v5[v77];
  const int32_t *v740 = &v5[v87];
  const int32_t *v749 = &v5[v96];
  const int32_t *v758 = &v5[v104];
  const int32_t *v767 = &v5[v114];
  const int32_t *v776 = &v5[v123];
  const int32_t *v785 = &v5[v131];
  const int32_t *v794 = &v5[v141];
  const int32_t *v812 = &v5[v158];
  const int32_t *v821 = &v5[v168];
  const int32_t *v830 = &v5[v177];
  const int32_t *v839 = &v5[v185];
  const int32_t *v848 = &v5[v195];
  svfloat32_t v856 = svdup_n_f32(v249);
  svfloat32_t v857 = svdup_n_f32(v256);
  svfloat32_t v858 = svdup_n_f32(v263);
  svfloat32_t v859 = svdup_n_f32(v270);
  svfloat32_t v865 = svdup_n_f32(v338);
  svfloat32_t v866 = svdup_n_f32(v345);
  svfloat32_t v867 = svdup_n_f32(v352);
  svfloat32_t v868 = svdup_n_f32(v359);
  svfloat32_t v869 = svdup_n_f32(v402);
  svfloat32_t v870 = svdup_n_f32(v409);
  svfloat32_t v871 = svdup_n_f32(v416);
  svfloat32_t v872 = svdup_n_f32(v423);
  svfloat32_t v873 = svdup_n_f32(v430);
  int32_t *v894 = &v6[v484];
  int32_t *v903 = &v6[v492];
  int32_t *v912 = &v6[v503];
  int32_t *v930 = &v6[v519];
  int32_t *v939 = &v6[v530];
  int32_t *v948 = &v6[v538];
  int32_t *v957 = &v6[v546];
  int32_t *v966 = &v6[v557];
  int32_t *v975 = &v6[v565];
  int32_t *v984 = &v6[v573];
  int32_t *v993 = &v6[v584];
  int32_t *v1002 = &v6[v592];
  int32_t *v1011 = &v6[v600];
  int32_t *v1020 = &v6[v611];
  int32_t *v1029 = &v6[v619];
  int32_t *v1038 = &v6[v627];
  int32_t *v1047 = &v6[v638];
  int32_t *v1056 = &v6[v646];
  int32_t *v1065 = &v6[v654];
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v686[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v667[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v676[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v48 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v695[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v56 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v704[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v66 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v713[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v722[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v731[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v740[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v102 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v749[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v110 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v758[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v120 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v767[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v129 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v776[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v137 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v785[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v147 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v794[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v164 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v812[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v174 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v821[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v183 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v830[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v191 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v839[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v201 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v848[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v57 = svadd_f32_x(svptrue_b32(), v48, v56);
  svfloat32_t v58 = svsub_f32_x(svptrue_b32(), v48, v56);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v111 = svadd_f32_x(svptrue_b32(), v102, v110);
  svfloat32_t v112 = svsub_f32_x(svptrue_b32(), v102, v110);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v165 = svadd_f32_x(svptrue_b32(), v156, v164);
  svfloat32_t v166 = svsub_f32_x(svptrue_b32(), v156, v164);
  svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v183, v191);
  svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v183, v191);
  svfloat32_t v40 = svadd_f32_x(svptrue_b32(), v30, v39);
  svfloat32_t v67 = svadd_f32_x(svptrue_b32(), v57, v66);
  svfloat32_t v94 = svadd_f32_x(svptrue_b32(), v84, v93);
  svfloat32_t v121 = svadd_f32_x(svptrue_b32(), v111, v120);
  svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v138, v147);
  svfloat32_t v175 = svadd_f32_x(svptrue_b32(), v165, v174);
  svfloat32_t v202 = svadd_f32_x(svptrue_b32(), v192, v201);
  svfloat32_t v292 = svadd_f32_x(svptrue_b32(), v57, v192);
  svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v57, v192);
  svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v138, v111);
  svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v138, v111);
  svfloat32_t v296 = svadd_f32_x(svptrue_b32(), v84, v165);
  svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v84, v165);
  svfloat32_t v381 = svadd_f32_x(svptrue_b32(), v58, v193);
  svfloat32_t v382 = svsub_f32_x(svptrue_b32(), v58, v193);
  svfloat32_t v383 = svadd_f32_x(svptrue_b32(), v139, v112);
  svfloat32_t v384 = svsub_f32_x(svptrue_b32(), v139, v112);
  svfloat32_t v385 = svadd_f32_x(svptrue_b32(), v85, v166);
  svfloat32_t v386 = svsub_f32_x(svptrue_b32(), v85, v166);
  svfloat32_t v203 = svadd_f32_x(svptrue_b32(), v67, v202);
  svfloat32_t v204 = svsub_f32_x(svptrue_b32(), v67, v202);
  svfloat32_t v205 = svadd_f32_x(svptrue_b32(), v148, v121);
  svfloat32_t v206 = svsub_f32_x(svptrue_b32(), v148, v121);
  svfloat32_t v207 = svadd_f32_x(svptrue_b32(), v94, v175);
  svfloat32_t v208 = svsub_f32_x(svptrue_b32(), v94, v175);
  svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v292, v294);
  svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v292, v294);
  svfloat32_t v302 = svsub_f32_x(svptrue_b32(), v294, v296);
  svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v296, v292);
  svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v293, v295);
  svfloat32_t v306 = svsub_f32_x(svptrue_b32(), v293, v295);
  svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v295, v297);
  svfloat32_t v308 = svsub_f32_x(svptrue_b32(), v297, v293);
  svfloat32_t v387 = svadd_f32_x(svptrue_b32(), v381, v383);
  svfloat32_t v390 = svsub_f32_x(svptrue_b32(), v381, v383);
  svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v383, v385);
  svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v385, v381);
  svfloat32_t v393 = svadd_f32_x(svptrue_b32(), v382, v384);
  svfloat32_t v395 = svsub_f32_x(svptrue_b32(), v382, v384);
  svfloat32_t v396 = svsub_f32_x(svptrue_b32(), v384, v386);
  svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v386, v382);
  svfloat32_t v209 = svadd_f32_x(svptrue_b32(), v203, v205);
  svfloat32_t v212 = svsub_f32_x(svptrue_b32(), v203, v205);
  svfloat32_t v213 = svsub_f32_x(svptrue_b32(), v205, v207);
  svfloat32_t v214 = svsub_f32_x(svptrue_b32(), v207, v203);
  svfloat32_t v215 = svadd_f32_x(svptrue_b32(), v204, v206);
  svfloat32_t v217 = svsub_f32_x(svptrue_b32(), v204, v206);
  svfloat32_t v218 = svsub_f32_x(svptrue_b32(), v206, v208);
  svfloat32_t v219 = svsub_f32_x(svptrue_b32(), v208, v204);
  svfloat32_t v299 = svadd_f32_x(svptrue_b32(), v298, v296);
  svfloat32_t v305 = svadd_f32_x(svptrue_b32(), v304, v297);
  svfloat32_t zero347 = svdup_n_f32(0);
  svfloat32_t v347 = svcmla_f32_x(pred_full, zero347, v866, v306, 90);
  svfloat32_t zero354 = svdup_n_f32(0);
  svfloat32_t v354 = svcmla_f32_x(pred_full, zero354, v867, v307, 90);
  svfloat32_t zero361 = svdup_n_f32(0);
  svfloat32_t v361 = svcmla_f32_x(pred_full, zero361, v868, v308, 90);
  svfloat32_t v388 = svadd_f32_x(svptrue_b32(), v387, v385);
  svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v393, v386);
  svfloat32_t zero418 = svdup_n_f32(0);
  svfloat32_t v418 = svcmla_f32_x(pred_full, zero418, v871, v390, 90);
  svfloat32_t zero425 = svdup_n_f32(0);
  svfloat32_t v425 = svcmla_f32_x(pred_full, zero425, v872, v391, 90);
  svfloat32_t zero432 = svdup_n_f32(0);
  svfloat32_t v432 = svcmla_f32_x(pred_full, zero432, v873, v392, 90);
  svfloat32_t v442 = svmul_f32_x(svptrue_b32(), v395, v875);
  svfloat32_t v447 = svmul_f32_x(svptrue_b32(), v396, v876);
  svfloat32_t v210 = svadd_f32_x(svptrue_b32(), v209, v207);
  svfloat32_t v216 = svadd_f32_x(svptrue_b32(), v215, v208);
  svfloat32_t zero258 = svdup_n_f32(0);
  svfloat32_t v258 = svcmla_f32_x(pred_full, zero258, v857, v217, 90);
  svfloat32_t zero265 = svdup_n_f32(0);
  svfloat32_t v265 = svcmla_f32_x(pred_full, zero265, v858, v218, 90);
  svfloat32_t zero272 = svdup_n_f32(0);
  svfloat32_t v272 = svcmla_f32_x(pred_full, zero272, v859, v219, 90);
  svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v299, v30);
  svfloat32_t v318 = svmul_f32_x(svptrue_b32(), v299, v861);
  svfloat32_t zero340 = svdup_n_f32(0);
  svfloat32_t v340 = svcmla_f32_x(pred_full, zero340, v865, v305, 90);
  svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v388, v31);
  svfloat32_t v211 = svadd_f32_x(svptrue_b32(), v210, v40);
  svfloat32_t zero251 = svdup_n_f32(0);
  svfloat32_t v251 = svcmla_f32_x(pred_full, zero251, v856, v216, 90);
  svfloat32_t v369 = svadd_f32_x(svptrue_b32(), v340, v347);
  svfloat32_t v371 = svsub_f32_x(svptrue_b32(), v340, v347);
  svfloat32_t v373 = svsub_f32_x(svptrue_b32(), v340, v354);
  svfloat32_t zero404 = svdup_n_f32(0);
  svfloat32_t v404 = svcmla_f32_x(pred_full, zero404, v869, v389, 90);
  svfloat32_t v460 = svmla_f32_x(pred_full, v442, v394, v874);
  svfloat32_t v462 = svnmls_f32_x(pred_full, v442, v394, v874);
  svfloat32_t v464 = svnmls_f32_x(pred_full, v447, v394, v874);
  svfloat32_t v273 = svmla_f32_x(pred_full, v211, v210, v852);
  svfloat32_t v280 = svadd_f32_x(svptrue_b32(), v251, v258);
  svfloat32_t v282 = svsub_f32_x(svptrue_b32(), v251, v258);
  svfloat32_t v284 = svsub_f32_x(svptrue_b32(), v251, v265);
  svfloat32_t v362 = svmla_f32_x(pred_full, v318, v300, v860);
  svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v369, v354);
  svfloat32_t v372 = svsub_f32_x(svptrue_b32(), v371, v361);
  svfloat32_t v374 = svadd_f32_x(svptrue_b32(), v373, v361);
  svfloat32_t v453 = svcmla_f32_x(pred_full, v404, v870, v388, 90);
  svfloat32_t v461 = svmla_f32_x(pred_full, v460, v396, v876);
  svfloat32_t v463 = svmls_f32_x(pred_full, v462, v397, v877);
  svfloat32_t v465 = svmla_f32_x(pred_full, v464, v397, v877);
  svfloat32_t v472 = svmla_f32_x(pred_full, v211, v300, v860);
  svint16_t v477 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v211, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v274 = svmla_f32_x(pred_full, v273, v212, v853);
  svfloat32_t v276 = svmls_f32_x(pred_full, v273, v212, v853);
  svfloat32_t v278 = svmls_f32_x(pred_full, v273, v213, v854);
  svfloat32_t v281 = svadd_f32_x(svptrue_b32(), v280, v265);
  svfloat32_t v283 = svsub_f32_x(svptrue_b32(), v282, v272);
  svfloat32_t v285 = svadd_f32_x(svptrue_b32(), v284, v272);
  svfloat32_t v363 = svmla_f32_x(pred_full, v362, v301, v862);
  svfloat32_t v365 = svmls_f32_x(pred_full, v362, v301, v862);
  svfloat32_t v367 = svmls_f32_x(pred_full, v362, v302, v863);
  svfloat32_t v454 = svadd_f32_x(svptrue_b32(), v453, v418);
  svfloat32_t v456 = svsub_f32_x(svptrue_b32(), v453, v418);
  svfloat32_t v458 = svsub_f32_x(svptrue_b32(), v453, v425);
  svfloat32_t v473 = svadd_f32_x(svptrue_b32(), v472, v404);
  svfloat32_t v474 = svsub_f32_x(svptrue_b32(), v472, v404);
  svst1w_u64(pred_full, (unsigned *)(v885), svreinterpret_u64_s16(v477));
  svfloat32_t v275 = svmla_f32_x(pred_full, v274, v213, v854);
  svfloat32_t v277 = svmls_f32_x(pred_full, v276, v214, v855);
  svfloat32_t v279 = svmla_f32_x(pred_full, v278, v214, v855);
  svfloat32_t v364 = svmla_f32_x(pred_full, v363, v302, v863);
  svfloat32_t v366 = svmls_f32_x(pred_full, v365, v303, v864);
  svfloat32_t v368 = svmla_f32_x(pred_full, v367, v303, v864);
  svfloat32_t v455 = svadd_f32_x(svptrue_b32(), v454, v425);
  svfloat32_t v457 = svsub_f32_x(svptrue_b32(), v456, v432);
  svfloat32_t v459 = svadd_f32_x(svptrue_b32(), v458, v432);
  svint16_t v485 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v474, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v493 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v473, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v286 = svadd_f32_x(svptrue_b32(), v275, v281);
  svfloat32_t v287 = svsub_f32_x(svptrue_b32(), v275, v281);
  svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v277, v283);
  svfloat32_t v289 = svsub_f32_x(svptrue_b32(), v277, v283);
  svfloat32_t v290 = svadd_f32_x(svptrue_b32(), v279, v285);
  svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v279, v285);
  svfloat32_t v375 = svadd_f32_x(svptrue_b32(), v364, v370);
  svfloat32_t v376 = svsub_f32_x(svptrue_b32(), v364, v370);
  svfloat32_t v377 = svadd_f32_x(svptrue_b32(), v366, v372);
  svfloat32_t v378 = svsub_f32_x(svptrue_b32(), v366, v372);
  svfloat32_t v379 = svadd_f32_x(svptrue_b32(), v368, v374);
  svfloat32_t v380 = svsub_f32_x(svptrue_b32(), v368, v374);
  svfloat32_t v466 = svadd_f32_x(svptrue_b32(), v455, v461);
  svfloat32_t v467 = svsub_f32_x(svptrue_b32(), v455, v461);
  svfloat32_t v468 = svadd_f32_x(svptrue_b32(), v457, v463);
  svfloat32_t v469 = svsub_f32_x(svptrue_b32(), v457, v463);
  svfloat32_t v470 = svadd_f32_x(svptrue_b32(), v459, v465);
  svfloat32_t v471 = svsub_f32_x(svptrue_b32(), v459, v465);
  svst1w_u64(pred_full, (unsigned *)(v894), svreinterpret_u64_s16(v485));
  svst1w_u64(pred_full, (unsigned *)(v903), svreinterpret_u64_s16(v493));
  svfloat32_t v499 = svadd_f32_x(svptrue_b32(), v287, v376);
  svint16_t v504 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v287, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v526 = svadd_f32_x(svptrue_b32(), v289, v378);
  svint16_t v531 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v289, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v553 = svadd_f32_x(svptrue_b32(), v290, v379);
  svint16_t v558 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v290, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v580 = svadd_f32_x(svptrue_b32(), v291, v380);
  svint16_t v585 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v291, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v607 = svadd_f32_x(svptrue_b32(), v288, v377);
  svint16_t v612 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v288, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v634 = svadd_f32_x(svptrue_b32(), v286, v375);
  svint16_t v639 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v286, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v500 = svadd_f32_x(svptrue_b32(), v499, v467);
  svfloat32_t v501 = svsub_f32_x(svptrue_b32(), v499, v467);
  svfloat32_t v527 = svadd_f32_x(svptrue_b32(), v526, v469);
  svfloat32_t v528 = svsub_f32_x(svptrue_b32(), v526, v469);
  svfloat32_t v554 = svadd_f32_x(svptrue_b32(), v553, v470);
  svfloat32_t v555 = svsub_f32_x(svptrue_b32(), v553, v470);
  svfloat32_t v581 = svadd_f32_x(svptrue_b32(), v580, v471);
  svfloat32_t v582 = svsub_f32_x(svptrue_b32(), v580, v471);
  svfloat32_t v608 = svadd_f32_x(svptrue_b32(), v607, v468);
  svfloat32_t v609 = svsub_f32_x(svptrue_b32(), v607, v468);
  svfloat32_t v635 = svadd_f32_x(svptrue_b32(), v634, v466);
  svfloat32_t v636 = svsub_f32_x(svptrue_b32(), v634, v466);
  svst1w_u64(pred_full, (unsigned *)(v912), svreinterpret_u64_s16(v504));
  svst1w_u64(pred_full, (unsigned *)(v939), svreinterpret_u64_s16(v531));
  svst1w_u64(pred_full, (unsigned *)(v966), svreinterpret_u64_s16(v558));
  svst1w_u64(pred_full, (unsigned *)(v993), svreinterpret_u64_s16(v585));
  svst1w_u64(pred_full, (unsigned *)(v1020), svreinterpret_u64_s16(v612));
  svst1w_u64(pred_full, (unsigned *)(v1047), svreinterpret_u64_s16(v639));
  svint16_t v512 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v501, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v520 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v500, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v539 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v528, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v547 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v527, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v566 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v555, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v574 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v554, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v593 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v582, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v601 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v581, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v620 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v609, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v628 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v608, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v647 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v636, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v655 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v635, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v921), svreinterpret_u64_s16(v512));
  svst1w_u64(pred_full, (unsigned *)(v930), svreinterpret_u64_s16(v520));
  svst1w_u64(pred_full, (unsigned *)(v948), svreinterpret_u64_s16(v539));
  svst1w_u64(pred_full, (unsigned *)(v957), svreinterpret_u64_s16(v547));
  svst1w_u64(pred_full, (unsigned *)(v975), svreinterpret_u64_s16(v566));
  svst1w_u64(pred_full, (unsigned *)(v984), svreinterpret_u64_s16(v574));
  svst1w_u64(pred_full, (unsigned *)(v1002), svreinterpret_u64_s16(v593));
  svst1w_u64(pred_full, (unsigned *)(v1011), svreinterpret_u64_s16(v601));
  svst1w_u64(pred_full, (unsigned *)(v1029), svreinterpret_u64_s16(v620));
  svst1w_u64(pred_full, (unsigned *)(v1038), svreinterpret_u64_s16(v628));
  svst1w_u64(pred_full, (unsigned *)(v1056), svreinterpret_u64_s16(v647));
  svst1w_u64(pred_full, (unsigned *)(v1065), svreinterpret_u64_s16(v655));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun22(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v403 = 1.1000000000000001e+00F;
  float v406 = 3.3166247903554003e-01F;
  float v407 = -3.3166247903554003e-01F;
  float v414 = 5.1541501300188641e-01F;
  float v418 = 9.4125353283118118e-01F;
  float v422 = 1.4143537075597825e+00F;
  float v426 = 8.5949297361449750e-01F;
  float v430 = 4.2314838273285138e-02F;
  float v434 = 3.8639279888589606e-01F;
  float v438 = 5.1254589567200015e-01F;
  float v442 = 1.0702757469471715e+00F;
  float v446 = 5.5486073394528512e-01F;
  float v449 = 1.2412944743900585e+00F;
  float v450 = -1.2412944743900585e+00F;
  float v456 = 2.0897833842005756e-01F;
  float v457 = -2.0897833842005756e-01F;
  float v463 = 3.7415717312460811e-01F;
  float v464 = -3.7415717312460811e-01F;
  float v470 = 4.9929922194110327e-02F;
  float v471 = -4.9929922194110327e-02F;
  float v477 = 6.5815896284539266e-01F;
  float v478 = -6.5815896284539266e-01F;
  float v484 = 6.3306543373877577e-01F;
  float v485 = -6.3306543373877577e-01F;
  float v491 = 1.0822460581641109e+00F;
  float v492 = -1.0822460581641109e+00F;
  float v498 = 8.1720737907134022e-01F;
  float v499 = -8.1720737907134022e-01F;
  float v505 = 4.2408709531871824e-01F;
  float v506 = -4.2408709531871824e-01F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v103 = vld1s_s16(&v5[istride]);
  float32x2_t v404 = (float32x2_t){v403, v403};
  float32x2_t v408 = (float32x2_t){v406, v407};
  float32x2_t v415 = (float32x2_t){v414, v414};
  float32x2_t v419 = (float32x2_t){v418, v418};
  float32x2_t v423 = (float32x2_t){v422, v422};
  float32x2_t v427 = (float32x2_t){v426, v426};
  float32x2_t v431 = (float32x2_t){v430, v430};
  float32x2_t v435 = (float32x2_t){v434, v434};
  float32x2_t v439 = (float32x2_t){v438, v438};
  float32x2_t v443 = (float32x2_t){v442, v442};
  float32x2_t v447 = (float32x2_t){v446, v446};
  float32x2_t v451 = (float32x2_t){v449, v450};
  float32x2_t v458 = (float32x2_t){v456, v457};
  float32x2_t v465 = (float32x2_t){v463, v464};
  float32x2_t v472 = (float32x2_t){v470, v471};
  float32x2_t v479 = (float32x2_t){v477, v478};
  float32x2_t v486 = (float32x2_t){v484, v485};
  float32x2_t v493 = (float32x2_t){v491, v492};
  float32x2_t v500 = (float32x2_t){v498, v499};
  float32x2_t v507 = (float32x2_t){v505, v506};
  float32x2_t v508 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v41 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v47 = vld1s_s16(&v5[istride * 15]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 17]);
  int16x4_t v69 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v75 = vld1s_s16(&v5[istride * 19]);
  int16x4_t v83 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v89 = vld1s_s16(&v5[istride * 21]);
  int16x4_t v97 = vld1s_s16(&v5[istride * 12]);
  float32x2_t v104 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v103)), 15);
  int16x4_t v111 = vld1s_s16(&v5[istride * 14]);
  int16x4_t v117 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v125 = vld1s_s16(&v5[istride * 16]);
  int16x4_t v131 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v139 = vld1s_s16(&v5[istride * 18]);
  int16x4_t v145 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v153 = vld1s_s16(&v5[istride * 20]);
  int16x4_t v159 = vld1s_s16(&v5[istride * 9]);
  float32x2_t v410 = vmul_f32(v508, v408);
  float32x2_t v453 = vmul_f32(v508, v451);
  float32x2_t v460 = vmul_f32(v508, v458);
  float32x2_t v467 = vmul_f32(v508, v465);
  float32x2_t v474 = vmul_f32(v508, v472);
  float32x2_t v481 = vmul_f32(v508, v479);
  float32x2_t v488 = vmul_f32(v508, v486);
  float32x2_t v495 = vmul_f32(v508, v493);
  float32x2_t v502 = vmul_f32(v508, v500);
  float32x2_t v509 = vmul_f32(v508, v507);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v42 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v41)), 15);
  float32x2_t v48 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v47)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  float32x2_t v76 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v75)), 15);
  float32x2_t v84 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v83)), 15);
  float32x2_t v90 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v89)), 15);
  float32x2_t v98 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v97)), 15);
  float32x2_t v112 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v111)), 15);
  float32x2_t v118 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v117)), 15);
  float32x2_t v126 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v125)), 15);
  float32x2_t v132 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v131)), 15);
  float32x2_t v140 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v139)), 15);
  float32x2_t v146 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v145)), 15);
  float32x2_t v154 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v153)), 15);
  float32x2_t v160 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v159)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v49 = vadd_f32(v42, v48);
  float32x2_t v50 = vsub_f32(v42, v48);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v77 = vadd_f32(v70, v76);
  float32x2_t v78 = vsub_f32(v70, v76);
  float32x2_t v91 = vadd_f32(v84, v90);
  float32x2_t v92 = vsub_f32(v84, v90);
  float32x2_t v105 = vadd_f32(v98, v104);
  float32x2_t v106 = vsub_f32(v98, v104);
  float32x2_t v119 = vadd_f32(v112, v118);
  float32x2_t v120 = vsub_f32(v112, v118);
  float32x2_t v133 = vadd_f32(v126, v132);
  float32x2_t v134 = vsub_f32(v126, v132);
  float32x2_t v147 = vadd_f32(v140, v146);
  float32x2_t v148 = vsub_f32(v140, v146);
  float32x2_t v161 = vadd_f32(v154, v160);
  float32x2_t v162 = vsub_f32(v154, v160);
  float32x2_t v163 = vadd_f32(v35, v161);
  float32x2_t v164 = vadd_f32(v49, v147);
  float32x2_t v165 = vadd_f32(v63, v133);
  float32x2_t v166 = vadd_f32(v77, v119);
  float32x2_t v167 = vadd_f32(v91, v105);
  float32x2_t v168 = vsub_f32(v35, v161);
  float32x2_t v169 = vsub_f32(v49, v147);
  float32x2_t v170 = vsub_f32(v63, v133);
  float32x2_t v171 = vsub_f32(v77, v119);
  float32x2_t v172 = vsub_f32(v91, v105);
  float32x2_t v361 = vadd_f32(v36, v162);
  float32x2_t v362 = vadd_f32(v50, v148);
  float32x2_t v363 = vadd_f32(v64, v134);
  float32x2_t v364 = vadd_f32(v78, v120);
  float32x2_t v365 = vadd_f32(v92, v106);
  float32x2_t v366 = vsub_f32(v36, v162);
  float32x2_t v367 = vsub_f32(v50, v148);
  float32x2_t v368 = vsub_f32(v64, v134);
  float32x2_t v369 = vsub_f32(v78, v120);
  float32x2_t v370 = vsub_f32(v92, v106);
  float32x2_t v173 = vadd_f32(v163, v164);
  float32x2_t v174 = vadd_f32(v165, v167);
  float32x2_t v176 = vsub_f32(v169, v170);
  float32x2_t v177 = vadd_f32(v168, v172);
  float32x2_t v182 = vsub_f32(v164, v166);
  float32x2_t v183 = vsub_f32(v163, v166);
  float32x2_t v184 = vsub_f32(v164, v163);
  float32x2_t v185 = vsub_f32(v167, v166);
  float32x2_t v186 = vsub_f32(v165, v166);
  float32x2_t v187 = vsub_f32(v167, v165);
  float32x2_t v188 = vsub_f32(v164, v167);
  float32x2_t v189 = vsub_f32(v163, v165);
  float32x2_t v191 = vadd_f32(v169, v171);
  float32x2_t v192 = vsub_f32(v168, v171);
  float32x2_t v193 = vadd_f32(v168, v169);
  float32x2_t v194 = vsub_f32(v171, v172);
  float32x2_t v195 = vsub_f32(v170, v171);
  float32x2_t v196 = vsub_f32(v170, v172);
  float32x2_t v197 = vadd_f32(v169, v172);
  float32x2_t v198 = vsub_f32(v168, v170);
  float32x2_t v371 = vadd_f32(v361, v362);
  float32x2_t v372 = vadd_f32(v363, v365);
  float32x2_t v374 = vsub_f32(v367, v368);
  float32x2_t v375 = vadd_f32(v366, v370);
  float32x2_t v380 = vsub_f32(v362, v364);
  float32x2_t v381 = vsub_f32(v361, v364);
  float32x2_t v382 = vsub_f32(v362, v361);
  float32x2_t v383 = vsub_f32(v365, v364);
  float32x2_t v384 = vsub_f32(v363, v364);
  float32x2_t v385 = vsub_f32(v365, v363);
  float32x2_t v386 = vsub_f32(v362, v365);
  float32x2_t v387 = vsub_f32(v361, v363);
  float32x2_t v389 = vadd_f32(v367, v369);
  float32x2_t v390 = vsub_f32(v366, v369);
  float32x2_t v391 = vadd_f32(v366, v367);
  float32x2_t v392 = vsub_f32(v369, v370);
  float32x2_t v393 = vsub_f32(v368, v369);
  float32x2_t v394 = vsub_f32(v368, v370);
  float32x2_t v395 = vadd_f32(v367, v370);
  float32x2_t v396 = vsub_f32(v366, v368);
  float32x2_t v175 = vadd_f32(v166, v173);
  float32x2_t v180 = vsub_f32(v176, v177);
  float32x2_t v190 = vsub_f32(v174, v173);
  float32x2_t v199 = vadd_f32(v176, v177);
  float32x2_t v218 = vmul_f32(v182, v415);
  float32x2_t v222 = vmul_f32(v183, v419);
  float32x2_t v226 = vmul_f32(v184, v423);
  float32x2_t v230 = vmul_f32(v185, v427);
  float32x2_t v234 = vmul_f32(v186, v431);
  float32x2_t v238 = vmul_f32(v187, v435);
  float32x2_t v242 = vmul_f32(v188, v439);
  float32x2_t v246 = vmul_f32(v189, v443);
  float32x2_t v256 = vrev64_f32(v191);
  float32x2_t v263 = vrev64_f32(v192);
  float32x2_t v270 = vrev64_f32(v193);
  float32x2_t v277 = vrev64_f32(v194);
  float32x2_t v284 = vrev64_f32(v195);
  float32x2_t v291 = vrev64_f32(v196);
  float32x2_t v298 = vrev64_f32(v197);
  float32x2_t v305 = vrev64_f32(v198);
  float32x2_t v373 = vadd_f32(v364, v371);
  float32x2_t v378 = vsub_f32(v374, v375);
  float32x2_t v388 = vsub_f32(v372, v371);
  float32x2_t v397 = vadd_f32(v374, v375);
  float32x2_t v416 = vmul_f32(v380, v415);
  float32x2_t v420 = vmul_f32(v381, v419);
  float32x2_t v424 = vmul_f32(v382, v423);
  float32x2_t v428 = vmul_f32(v383, v427);
  float32x2_t v432 = vmul_f32(v384, v431);
  float32x2_t v436 = vmul_f32(v385, v435);
  float32x2_t v440 = vmul_f32(v386, v439);
  float32x2_t v444 = vmul_f32(v387, v443);
  float32x2_t v454 = vrev64_f32(v389);
  float32x2_t v461 = vrev64_f32(v390);
  float32x2_t v468 = vrev64_f32(v391);
  float32x2_t v475 = vrev64_f32(v392);
  float32x2_t v482 = vrev64_f32(v393);
  float32x2_t v489 = vrev64_f32(v394);
  float32x2_t v496 = vrev64_f32(v395);
  float32x2_t v503 = vrev64_f32(v396);
  float32x2_t v178 = vadd_f32(v175, v174);
  float32x2_t v181 = vsub_f32(v180, v171);
  float32x2_t v250 = vmul_f32(v190, v447);
  float32x2_t v257 = vmul_f32(v256, v453);
  float32x2_t v264 = vmul_f32(v263, v460);
  float32x2_t v271 = vmul_f32(v270, v467);
  float32x2_t v278 = vmul_f32(v277, v474);
  float32x2_t v285 = vmul_f32(v284, v481);
  float32x2_t v292 = vmul_f32(v291, v488);
  float32x2_t v299 = vmul_f32(v298, v495);
  float32x2_t v306 = vmul_f32(v305, v502);
  float32x2_t v312 = vrev64_f32(v199);
  float32x2_t v315 = vadd_f32(v218, v222);
  float32x2_t v316 = vadd_f32(v222, v226);
  float32x2_t v317 = vsub_f32(v218, v226);
  float32x2_t v318 = vadd_f32(v230, v234);
  float32x2_t v319 = vadd_f32(v234, v238);
  float32x2_t v320 = vsub_f32(v230, v238);
  float32x2_t v376 = vadd_f32(v373, v372);
  float32x2_t v379 = vsub_f32(v378, v369);
  float32x2_t v448 = vmul_f32(v388, v447);
  float32x2_t v455 = vmul_f32(v454, v453);
  float32x2_t v462 = vmul_f32(v461, v460);
  float32x2_t v469 = vmul_f32(v468, v467);
  float32x2_t v476 = vmul_f32(v475, v474);
  float32x2_t v483 = vmul_f32(v482, v481);
  float32x2_t v490 = vmul_f32(v489, v488);
  float32x2_t v497 = vmul_f32(v496, v495);
  float32x2_t v504 = vmul_f32(v503, v502);
  float32x2_t v510 = vrev64_f32(v397);
  float32x2_t v513 = vadd_f32(v416, v420);
  float32x2_t v514 = vadd_f32(v420, v424);
  float32x2_t v515 = vsub_f32(v416, v424);
  float32x2_t v516 = vadd_f32(v428, v432);
  float32x2_t v517 = vadd_f32(v432, v436);
  float32x2_t v518 = vsub_f32(v428, v436);
  float32x2_t v179 = vadd_f32(v21, v178);
  float32x2_t v207 = vmul_f32(v178, v404);
  float32x2_t v213 = vrev64_f32(v181);
  float32x2_t v313 = vmul_f32(v312, v509);
  float32x2_t v321 = vadd_f32(v246, v250);
  float32x2_t v322 = vadd_f32(v242, v250);
  float32x2_t v323 = vadd_f32(v264, v271);
  float32x2_t v324 = vsub_f32(v257, v271);
  float32x2_t v325 = vadd_f32(v285, v292);
  float32x2_t v326 = vsub_f32(v278, v292);
  float32x2_t v377 = vadd_f32(v22, v376);
  float32x2_t v405 = vmul_f32(v376, v404);
  float32x2_t v411 = vrev64_f32(v379);
  float32x2_t v511 = vmul_f32(v510, v509);
  float32x2_t v519 = vadd_f32(v444, v448);
  float32x2_t v520 = vadd_f32(v440, v448);
  float32x2_t v521 = vadd_f32(v462, v469);
  float32x2_t v522 = vsub_f32(v455, v469);
  float32x2_t v523 = vadd_f32(v483, v490);
  float32x2_t v524 = vsub_f32(v476, v490);
  float32x2_t v214 = vmul_f32(v213, v410);
  float32x2_t v314 = vsub_f32(v179, v207);
  float32x2_t v327 = vadd_f32(v306, v313);
  float32x2_t v328 = vsub_f32(v299, v313);
  float32x2_t v329 = vadd_f32(v319, v321);
  float32x2_t v347 = vadd_f32(v323, v324);
  float32x2_t v412 = vmul_f32(v411, v410);
  float32x2_t v512 = vsub_f32(v377, v405);
  float32x2_t v525 = vadd_f32(v504, v511);
  float32x2_t v526 = vsub_f32(v497, v511);
  float32x2_t v527 = vadd_f32(v517, v519);
  float32x2_t v545 = vadd_f32(v521, v522);
  int16x4_t v561 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v179, 15), (int32x2_t){0, 0}));
  int16x4_t v567 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v377, 15), (int32x2_t){0, 0}));
  float32x2_t v330 = vadd_f32(v329, v314);
  float32x2_t v331 = vsub_f32(v314, v316);
  float32x2_t v333 = vadd_f32(v314, v320);
  float32x2_t v335 = vsub_f32(v314, v317);
  float32x2_t v337 = vadd_f32(v314, v315);
  float32x2_t v339 = vadd_f32(v214, v325);
  float32x2_t v341 = vsub_f32(v327, v323);
  float32x2_t v343 = vadd_f32(v214, v328);
  float32x2_t v345 = vsub_f32(v328, v324);
  float32x2_t v348 = vadd_f32(v347, v325);
  float32x2_t v528 = vadd_f32(v527, v512);
  float32x2_t v529 = vsub_f32(v512, v514);
  float32x2_t v531 = vadd_f32(v512, v518);
  float32x2_t v533 = vsub_f32(v512, v515);
  float32x2_t v535 = vadd_f32(v512, v513);
  float32x2_t v537 = vadd_f32(v412, v523);
  float32x2_t v539 = vsub_f32(v525, v521);
  float32x2_t v541 = vadd_f32(v412, v526);
  float32x2_t v543 = vsub_f32(v526, v522);
  float32x2_t v546 = vadd_f32(v545, v523);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v561), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v567), 0);
  float32x2_t v332 = vsub_f32(v331, v321);
  float32x2_t v334 = vadd_f32(v333, v322);
  float32x2_t v336 = vsub_f32(v335, v322);
  float32x2_t v338 = vsub_f32(v337, v318);
  float32x2_t v340 = vadd_f32(v339, v327);
  float32x2_t v342 = vsub_f32(v341, v214);
  float32x2_t v344 = vadd_f32(v343, v326);
  float32x2_t v346 = vsub_f32(v345, v214);
  float32x2_t v349 = vadd_f32(v348, v326);
  float32x2_t v530 = vsub_f32(v529, v519);
  float32x2_t v532 = vadd_f32(v531, v520);
  float32x2_t v534 = vsub_f32(v533, v520);
  float32x2_t v536 = vsub_f32(v535, v516);
  float32x2_t v538 = vadd_f32(v537, v525);
  float32x2_t v540 = vsub_f32(v539, v412);
  float32x2_t v542 = vadd_f32(v541, v524);
  float32x2_t v544 = vsub_f32(v543, v412);
  float32x2_t v547 = vadd_f32(v546, v524);
  float32x2_t v350 = vsub_f32(v349, v214);
  float32x2_t v352 = vadd_f32(v330, v340);
  float32x2_t v353 = vadd_f32(v332, v342);
  float32x2_t v354 = vsub_f32(v334, v344);
  float32x2_t v355 = vadd_f32(v336, v346);
  float32x2_t v356 = vsub_f32(v336, v346);
  float32x2_t v357 = vadd_f32(v334, v344);
  float32x2_t v358 = vsub_f32(v332, v342);
  float32x2_t v359 = vsub_f32(v330, v340);
  float32x2_t v548 = vsub_f32(v547, v412);
  float32x2_t v550 = vadd_f32(v528, v538);
  float32x2_t v551 = vadd_f32(v530, v540);
  float32x2_t v552 = vsub_f32(v532, v542);
  float32x2_t v553 = vadd_f32(v534, v544);
  float32x2_t v554 = vsub_f32(v534, v544);
  float32x2_t v555 = vadd_f32(v532, v542);
  float32x2_t v556 = vsub_f32(v530, v540);
  float32x2_t v557 = vsub_f32(v528, v538);
  float32x2_t v351 = vadd_f32(v338, v350);
  float32x2_t v360 = vsub_f32(v338, v350);
  float32x2_t v549 = vadd_f32(v536, v548);
  float32x2_t v558 = vsub_f32(v536, v548);
  int16x4_t v585 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v359, 15), (int32x2_t){0, 0}));
  int16x4_t v591 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v557, 15), (int32x2_t){0, 0}));
  int16x4_t v597 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v358, 15), (int32x2_t){0, 0}));
  int16x4_t v603 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v556, 15), (int32x2_t){0, 0}));
  int16x4_t v609 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v357, 15), (int32x2_t){0, 0}));
  int16x4_t v615 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v555, 15), (int32x2_t){0, 0}));
  int16x4_t v621 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v356, 15), (int32x2_t){0, 0}));
  int16x4_t v627 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v554, 15), (int32x2_t){0, 0}));
  int16x4_t v633 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v355, 15), (int32x2_t){0, 0}));
  int16x4_t v639 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v553, 15), (int32x2_t){0, 0}));
  int16x4_t v645 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v354, 15), (int32x2_t){0, 0}));
  int16x4_t v651 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v552, 15), (int32x2_t){0, 0}));
  int16x4_t v657 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v353, 15), (int32x2_t){0, 0}));
  int16x4_t v663 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v551, 15), (int32x2_t){0, 0}));
  int16x4_t v669 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v352, 15), (int32x2_t){0, 0}));
  int16x4_t v675 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v550, 15), (int32x2_t){0, 0}));
  int16x4_t v573 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v360, 15), (int32x2_t){0, 0}));
  int16x4_t v579 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v558, 15), (int32x2_t){0, 0}));
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v585), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v591), 0);
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v597), 0);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v603), 0);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v609), 0);
  v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v615), 0);
  v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v621), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v627), 0);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v633), 0);
  v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v639), 0);
  v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v645), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v651), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v657), 0);
  v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v663), 0);
  v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v669), 0);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v675), 0);
  int16x4_t v681 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v351, 15), (int32x2_t){0, 0}));
  int16x4_t v687 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v549, 15), (int32x2_t){0, 0}));
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v573), 0);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v579), 0);
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v681), 0);
  v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v687), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun22(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v464 = 1.1000000000000001e+00F;
  float v469 = -3.3166247903554003e-01F;
  float v476 = 5.1541501300188641e-01F;
  float v481 = 9.4125353283118118e-01F;
  float v486 = 1.4143537075597825e+00F;
  float v491 = 8.5949297361449750e-01F;
  float v496 = 4.2314838273285138e-02F;
  float v501 = 3.8639279888589606e-01F;
  float v506 = 5.1254589567200015e-01F;
  float v511 = 1.0702757469471715e+00F;
  float v516 = 5.5486073394528512e-01F;
  float v521 = -1.2412944743900585e+00F;
  float v528 = -2.0897833842005756e-01F;
  float v535 = -3.7415717312460811e-01F;
  float v542 = -4.9929922194110327e-02F;
  float v549 = -6.5815896284539266e-01F;
  float v556 = -6.3306543373877577e-01F;
  float v563 = -1.0822460581641109e+00F;
  float v570 = -8.1720737907134022e-01F;
  float v577 = -4.2408709531871824e-01F;
  const int32_t *v930 = &v5[v0];
  int32_t *v1081 = &v6[v2];
  int64_t v23 = v0 * 11;
  int64_t v33 = v0 * 2;
  int64_t v41 = v0 * 13;
  int64_t v51 = v0 * 4;
  int64_t v59 = v0 * 15;
  int64_t v69 = v0 * 6;
  int64_t v77 = v0 * 17;
  int64_t v87 = v0 * 8;
  int64_t v95 = v0 * 19;
  int64_t v105 = v0 * 10;
  int64_t v113 = v0 * 21;
  int64_t v123 = v0 * 12;
  int64_t v141 = v0 * 14;
  int64_t v149 = v0 * 3;
  int64_t v159 = v0 * 16;
  int64_t v167 = v0 * 5;
  int64_t v177 = v0 * 18;
  int64_t v185 = v0 * 7;
  int64_t v195 = v0 * 20;
  int64_t v203 = v0 * 9;
  float v472 = v4 * v469;
  float v524 = v4 * v521;
  float v531 = v4 * v528;
  float v538 = v4 * v535;
  float v545 = v4 * v542;
  float v552 = v4 * v549;
  float v559 = v4 * v556;
  float v566 = v4 * v563;
  float v573 = v4 * v570;
  float v580 = v4 * v577;
  int64_t v639 = v2 * 11;
  int64_t v647 = v2 * 12;
  int64_t v663 = v2 * 2;
  int64_t v671 = v2 * 13;
  int64_t v679 = v2 * 14;
  int64_t v687 = v2 * 3;
  int64_t v695 = v2 * 4;
  int64_t v703 = v2 * 15;
  int64_t v711 = v2 * 16;
  int64_t v719 = v2 * 5;
  int64_t v727 = v2 * 6;
  int64_t v735 = v2 * 17;
  int64_t v743 = v2 * 18;
  int64_t v751 = v2 * 7;
  int64_t v759 = v2 * 8;
  int64_t v767 = v2 * 19;
  int64_t v775 = v2 * 20;
  int64_t v783 = v2 * 9;
  int64_t v791 = v2 * 10;
  int64_t v799 = v2 * 21;
  const int32_t *v813 = &v5[0];
  svfloat32_t v1027 = svdup_n_f32(v464);
  svfloat32_t v1029 = svdup_n_f32(v476);
  svfloat32_t v1030 = svdup_n_f32(v481);
  svfloat32_t v1031 = svdup_n_f32(v486);
  svfloat32_t v1032 = svdup_n_f32(v491);
  svfloat32_t v1033 = svdup_n_f32(v496);
  svfloat32_t v1034 = svdup_n_f32(v501);
  svfloat32_t v1035 = svdup_n_f32(v506);
  svfloat32_t v1036 = svdup_n_f32(v511);
  svfloat32_t v1037 = svdup_n_f32(v516);
  int32_t *v1054 = &v6[0];
  svfloat32_t v137 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v930[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v822 = &v5[v23];
  const int32_t *v831 = &v5[v33];
  const int32_t *v840 = &v5[v41];
  const int32_t *v849 = &v5[v51];
  const int32_t *v858 = &v5[v59];
  const int32_t *v867 = &v5[v69];
  const int32_t *v876 = &v5[v77];
  const int32_t *v885 = &v5[v87];
  const int32_t *v894 = &v5[v95];
  const int32_t *v903 = &v5[v105];
  const int32_t *v912 = &v5[v113];
  const int32_t *v921 = &v5[v123];
  const int32_t *v939 = &v5[v141];
  const int32_t *v948 = &v5[v149];
  const int32_t *v957 = &v5[v159];
  const int32_t *v966 = &v5[v167];
  const int32_t *v975 = &v5[v177];
  const int32_t *v984 = &v5[v185];
  const int32_t *v993 = &v5[v195];
  const int32_t *v1002 = &v5[v203];
  svfloat32_t v1028 = svdup_n_f32(v472);
  svfloat32_t v1038 = svdup_n_f32(v524);
  svfloat32_t v1039 = svdup_n_f32(v531);
  svfloat32_t v1040 = svdup_n_f32(v538);
  svfloat32_t v1041 = svdup_n_f32(v545);
  svfloat32_t v1042 = svdup_n_f32(v552);
  svfloat32_t v1043 = svdup_n_f32(v559);
  svfloat32_t v1044 = svdup_n_f32(v566);
  svfloat32_t v1045 = svdup_n_f32(v573);
  svfloat32_t v1046 = svdup_n_f32(v580);
  int32_t *v1063 = &v6[v639];
  int32_t *v1072 = &v6[v647];
  int32_t *v1090 = &v6[v663];
  int32_t *v1099 = &v6[v671];
  int32_t *v1108 = &v6[v679];
  int32_t *v1117 = &v6[v687];
  int32_t *v1126 = &v6[v695];
  int32_t *v1135 = &v6[v703];
  int32_t *v1144 = &v6[v711];
  int32_t *v1153 = &v6[v719];
  int32_t *v1162 = &v6[v727];
  int32_t *v1171 = &v6[v735];
  int32_t *v1180 = &v6[v743];
  int32_t *v1189 = &v6[v751];
  int32_t *v1198 = &v6[v759];
  int32_t *v1207 = &v6[v767];
  int32_t *v1216 = &v6[v775];
  int32_t *v1225 = &v6[v783];
  int32_t *v1234 = &v6[v791];
  int32_t *v1243 = &v6[v799];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v813[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v822[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v831[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v840[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v57 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v849[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v65 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v858[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v867[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v876[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v885[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v101 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v894[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v111 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v903[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v119 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v912[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v129 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v921[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v147 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v939[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v155 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v948[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v165 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v957[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v173 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v966[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v183 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v975[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v191 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v984[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v201 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v993[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v209 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1002[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v57, v65);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v93, v101);
  svfloat32_t v120 = svadd_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v121 = svsub_f32_x(svptrue_b32(), v111, v119);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v156 = svadd_f32_x(svptrue_b32(), v147, v155);
  svfloat32_t v157 = svsub_f32_x(svptrue_b32(), v147, v155);
  svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v165, v173);
  svfloat32_t v175 = svsub_f32_x(svptrue_b32(), v165, v173);
  svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v183, v191);
  svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v183, v191);
  svfloat32_t v210 = svadd_f32_x(svptrue_b32(), v201, v209);
  svfloat32_t v211 = svsub_f32_x(svptrue_b32(), v201, v209);
  svfloat32_t v212 = svadd_f32_x(svptrue_b32(), v48, v210);
  svfloat32_t v213 = svadd_f32_x(svptrue_b32(), v66, v192);
  svfloat32_t v214 = svadd_f32_x(svptrue_b32(), v84, v174);
  svfloat32_t v215 = svadd_f32_x(svptrue_b32(), v102, v156);
  svfloat32_t v216 = svadd_f32_x(svptrue_b32(), v120, v138);
  svfloat32_t v217 = svsub_f32_x(svptrue_b32(), v48, v210);
  svfloat32_t v218 = svsub_f32_x(svptrue_b32(), v66, v192);
  svfloat32_t v219 = svsub_f32_x(svptrue_b32(), v84, v174);
  svfloat32_t v220 = svsub_f32_x(svptrue_b32(), v102, v156);
  svfloat32_t v221 = svsub_f32_x(svptrue_b32(), v120, v138);
  svfloat32_t v421 = svadd_f32_x(svptrue_b32(), v49, v211);
  svfloat32_t v422 = svadd_f32_x(svptrue_b32(), v67, v193);
  svfloat32_t v423 = svadd_f32_x(svptrue_b32(), v85, v175);
  svfloat32_t v424 = svadd_f32_x(svptrue_b32(), v103, v157);
  svfloat32_t v425 = svadd_f32_x(svptrue_b32(), v121, v139);
  svfloat32_t v426 = svsub_f32_x(svptrue_b32(), v49, v211);
  svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v67, v193);
  svfloat32_t v428 = svsub_f32_x(svptrue_b32(), v85, v175);
  svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v103, v157);
  svfloat32_t v430 = svsub_f32_x(svptrue_b32(), v121, v139);
  svfloat32_t v222 = svadd_f32_x(svptrue_b32(), v212, v213);
  svfloat32_t v223 = svadd_f32_x(svptrue_b32(), v214, v216);
  svfloat32_t v225 = svsub_f32_x(svptrue_b32(), v218, v219);
  svfloat32_t v226 = svadd_f32_x(svptrue_b32(), v217, v221);
  svfloat32_t v231 = svsub_f32_x(svptrue_b32(), v213, v215);
  svfloat32_t v232 = svsub_f32_x(svptrue_b32(), v212, v215);
  svfloat32_t v233 = svsub_f32_x(svptrue_b32(), v213, v212);
  svfloat32_t v234 = svsub_f32_x(svptrue_b32(), v216, v215);
  svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v214, v215);
  svfloat32_t v236 = svsub_f32_x(svptrue_b32(), v216, v214);
  svfloat32_t v237 = svsub_f32_x(svptrue_b32(), v213, v216);
  svfloat32_t v238 = svsub_f32_x(svptrue_b32(), v212, v214);
  svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v218, v220);
  svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v217, v220);
  svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v217, v218);
  svfloat32_t v243 = svsub_f32_x(svptrue_b32(), v220, v221);
  svfloat32_t v244 = svsub_f32_x(svptrue_b32(), v219, v220);
  svfloat32_t v245 = svsub_f32_x(svptrue_b32(), v219, v221);
  svfloat32_t v246 = svadd_f32_x(svptrue_b32(), v218, v221);
  svfloat32_t v247 = svsub_f32_x(svptrue_b32(), v217, v219);
  svfloat32_t v431 = svadd_f32_x(svptrue_b32(), v421, v422);
  svfloat32_t v432 = svadd_f32_x(svptrue_b32(), v423, v425);
  svfloat32_t v434 = svsub_f32_x(svptrue_b32(), v427, v428);
  svfloat32_t v435 = svadd_f32_x(svptrue_b32(), v426, v430);
  svfloat32_t v440 = svsub_f32_x(svptrue_b32(), v422, v424);
  svfloat32_t v441 = svsub_f32_x(svptrue_b32(), v421, v424);
  svfloat32_t v442 = svsub_f32_x(svptrue_b32(), v422, v421);
  svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v425, v424);
  svfloat32_t v444 = svsub_f32_x(svptrue_b32(), v423, v424);
  svfloat32_t v445 = svsub_f32_x(svptrue_b32(), v425, v423);
  svfloat32_t v446 = svsub_f32_x(svptrue_b32(), v422, v425);
  svfloat32_t v447 = svsub_f32_x(svptrue_b32(), v421, v423);
  svfloat32_t v449 = svadd_f32_x(svptrue_b32(), v427, v429);
  svfloat32_t v450 = svsub_f32_x(svptrue_b32(), v426, v429);
  svfloat32_t v451 = svadd_f32_x(svptrue_b32(), v426, v427);
  svfloat32_t v452 = svsub_f32_x(svptrue_b32(), v429, v430);
  svfloat32_t v453 = svsub_f32_x(svptrue_b32(), v428, v429);
  svfloat32_t v454 = svsub_f32_x(svptrue_b32(), v428, v430);
  svfloat32_t v455 = svadd_f32_x(svptrue_b32(), v427, v430);
  svfloat32_t v456 = svsub_f32_x(svptrue_b32(), v426, v428);
  svfloat32_t v224 = svadd_f32_x(svptrue_b32(), v215, v222);
  svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v225, v226);
  svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v223, v222);
  svfloat32_t v248 = svadd_f32_x(svptrue_b32(), v225, v226);
  svfloat32_t v275 = svmul_f32_x(svptrue_b32(), v232, v1030);
  svfloat32_t v280 = svmul_f32_x(svptrue_b32(), v233, v1031);
  svfloat32_t v290 = svmul_f32_x(svptrue_b32(), v235, v1033);
  svfloat32_t v295 = svmul_f32_x(svptrue_b32(), v236, v1034);
  svfloat32_t zero317 = svdup_n_f32(0);
  svfloat32_t v317 = svcmla_f32_x(pred_full, zero317, v1038, v240, 90);
  svfloat32_t zero331 = svdup_n_f32(0);
  svfloat32_t v331 = svcmla_f32_x(pred_full, zero331, v1040, v242, 90);
  svfloat32_t zero338 = svdup_n_f32(0);
  svfloat32_t v338 = svcmla_f32_x(pred_full, zero338, v1041, v243, 90);
  svfloat32_t zero352 = svdup_n_f32(0);
  svfloat32_t v352 = svcmla_f32_x(pred_full, zero352, v1043, v245, 90);
  svfloat32_t zero359 = svdup_n_f32(0);
  svfloat32_t v359 = svcmla_f32_x(pred_full, zero359, v1044, v246, 90);
  svfloat32_t v433 = svadd_f32_x(svptrue_b32(), v424, v431);
  svfloat32_t v438 = svsub_f32_x(svptrue_b32(), v434, v435);
  svfloat32_t v448 = svsub_f32_x(svptrue_b32(), v432, v431);
  svfloat32_t v457 = svadd_f32_x(svptrue_b32(), v434, v435);
  svfloat32_t v484 = svmul_f32_x(svptrue_b32(), v441, v1030);
  svfloat32_t v489 = svmul_f32_x(svptrue_b32(), v442, v1031);
  svfloat32_t v499 = svmul_f32_x(svptrue_b32(), v444, v1033);
  svfloat32_t v504 = svmul_f32_x(svptrue_b32(), v445, v1034);
  svfloat32_t zero526 = svdup_n_f32(0);
  svfloat32_t v526 = svcmla_f32_x(pred_full, zero526, v1038, v449, 90);
  svfloat32_t zero540 = svdup_n_f32(0);
  svfloat32_t v540 = svcmla_f32_x(pred_full, zero540, v1040, v451, 90);
  svfloat32_t zero547 = svdup_n_f32(0);
  svfloat32_t v547 = svcmla_f32_x(pred_full, zero547, v1041, v452, 90);
  svfloat32_t zero561 = svdup_n_f32(0);
  svfloat32_t v561 = svcmla_f32_x(pred_full, zero561, v1043, v454, 90);
  svfloat32_t zero568 = svdup_n_f32(0);
  svfloat32_t v568 = svcmla_f32_x(pred_full, zero568, v1044, v455, 90);
  svfloat32_t v227 = svadd_f32_x(svptrue_b32(), v224, v223);
  svfloat32_t v230 = svsub_f32_x(svptrue_b32(), v229, v220);
  svfloat32_t v310 = svmul_f32_x(svptrue_b32(), v239, v1037);
  svfloat32_t zero373 = svdup_n_f32(0);
  svfloat32_t v373 = svcmla_f32_x(pred_full, zero373, v1046, v248, 90);
  svfloat32_t v375 = svmla_f32_x(pred_full, v275, v231, v1029);
  svfloat32_t v376 = svmla_f32_x(pred_full, v280, v232, v1030);
  svfloat32_t v377 = svnmls_f32_x(pred_full, v280, v231, v1029);
  svfloat32_t v378 = svmla_f32_x(pred_full, v290, v234, v1032);
  svfloat32_t v379 = svmla_f32_x(pred_full, v295, v235, v1033);
  svfloat32_t v380 = svnmls_f32_x(pred_full, v295, v234, v1032);
  svfloat32_t v383 = svcmla_f32_x(pred_full, v331, v1039, v241, 90);
  svfloat32_t v384 = svsub_f32_x(svptrue_b32(), v317, v331);
  svfloat32_t v385 = svcmla_f32_x(pred_full, v352, v1042, v244, 90);
  svfloat32_t v386 = svsub_f32_x(svptrue_b32(), v338, v352);
  svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v433, v432);
  svfloat32_t v439 = svsub_f32_x(svptrue_b32(), v438, v429);
  svfloat32_t v519 = svmul_f32_x(svptrue_b32(), v448, v1037);
  svfloat32_t zero582 = svdup_n_f32(0);
  svfloat32_t v582 = svcmla_f32_x(pred_full, zero582, v1046, v457, 90);
  svfloat32_t v584 = svmla_f32_x(pred_full, v484, v440, v1029);
  svfloat32_t v585 = svmla_f32_x(pred_full, v489, v441, v1030);
  svfloat32_t v586 = svnmls_f32_x(pred_full, v489, v440, v1029);
  svfloat32_t v587 = svmla_f32_x(pred_full, v499, v443, v1032);
  svfloat32_t v588 = svmla_f32_x(pred_full, v504, v444, v1033);
  svfloat32_t v589 = svnmls_f32_x(pred_full, v504, v443, v1032);
  svfloat32_t v592 = svcmla_f32_x(pred_full, v540, v1039, v450, 90);
  svfloat32_t v593 = svsub_f32_x(svptrue_b32(), v526, v540);
  svfloat32_t v594 = svcmla_f32_x(pred_full, v561, v1042, v453, 90);
  svfloat32_t v595 = svsub_f32_x(svptrue_b32(), v547, v561);
  svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v30, v227);
  svfloat32_t zero265 = svdup_n_f32(0);
  svfloat32_t v265 = svcmla_f32_x(pred_full, zero265, v1028, v230, 90);
  svfloat32_t v381 = svmla_f32_x(pred_full, v310, v238, v1036);
  svfloat32_t v382 = svmla_f32_x(pred_full, v310, v237, v1035);
  svfloat32_t v387 = svcmla_f32_x(pred_full, v373, v1045, v247, 90);
  svfloat32_t v388 = svsub_f32_x(svptrue_b32(), v359, v373);
  svfloat32_t v407 = svadd_f32_x(svptrue_b32(), v383, v384);
  svfloat32_t v437 = svadd_f32_x(svptrue_b32(), v31, v436);
  svfloat32_t zero474 = svdup_n_f32(0);
  svfloat32_t v474 = svcmla_f32_x(pred_full, zero474, v1028, v439, 90);
  svfloat32_t v590 = svmla_f32_x(pred_full, v519, v447, v1036);
  svfloat32_t v591 = svmla_f32_x(pred_full, v519, v446, v1035);
  svfloat32_t v596 = svcmla_f32_x(pred_full, v582, v1045, v456, 90);
  svfloat32_t v597 = svsub_f32_x(svptrue_b32(), v568, v582);
  svfloat32_t v616 = svadd_f32_x(svptrue_b32(), v592, v593);
  svfloat32_t v374 = svmls_f32_x(pred_full, v228, v227, v1027);
  svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v379, v381);
  svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v265, v385);
  svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v387, v383);
  svfloat32_t v403 = svadd_f32_x(svptrue_b32(), v265, v388);
  svfloat32_t v405 = svsub_f32_x(svptrue_b32(), v388, v384);
  svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v407, v385);
  svfloat32_t v583 = svmls_f32_x(pred_full, v437, v436, v1027);
  svfloat32_t v598 = svadd_f32_x(svptrue_b32(), v588, v590);
  svfloat32_t v608 = svadd_f32_x(svptrue_b32(), v474, v594);
  svfloat32_t v610 = svsub_f32_x(svptrue_b32(), v596, v592);
  svfloat32_t v612 = svadd_f32_x(svptrue_b32(), v474, v597);
  svfloat32_t v614 = svsub_f32_x(svptrue_b32(), v597, v593);
  svfloat32_t v617 = svadd_f32_x(svptrue_b32(), v616, v594);
  svint16_t v632 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v228, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v640 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v437, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v389, v374);
  svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v374, v376);
  svfloat32_t v393 = svadd_f32_x(svptrue_b32(), v374, v380);
  svfloat32_t v395 = svsub_f32_x(svptrue_b32(), v374, v377);
  svfloat32_t v397 = svadd_f32_x(svptrue_b32(), v374, v375);
  svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v399, v387);
  svfloat32_t v402 = svsub_f32_x(svptrue_b32(), v401, v265);
  svfloat32_t v404 = svadd_f32_x(svptrue_b32(), v403, v386);
  svfloat32_t v406 = svsub_f32_x(svptrue_b32(), v405, v265);
  svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v408, v386);
  svfloat32_t v599 = svadd_f32_x(svptrue_b32(), v598, v583);
  svfloat32_t v600 = svsub_f32_x(svptrue_b32(), v583, v585);
  svfloat32_t v602 = svadd_f32_x(svptrue_b32(), v583, v589);
  svfloat32_t v604 = svsub_f32_x(svptrue_b32(), v583, v586);
  svfloat32_t v606 = svadd_f32_x(svptrue_b32(), v583, v584);
  svfloat32_t v609 = svadd_f32_x(svptrue_b32(), v608, v596);
  svfloat32_t v611 = svsub_f32_x(svptrue_b32(), v610, v474);
  svfloat32_t v613 = svadd_f32_x(svptrue_b32(), v612, v595);
  svfloat32_t v615 = svsub_f32_x(svptrue_b32(), v614, v474);
  svfloat32_t v618 = svadd_f32_x(svptrue_b32(), v617, v595);
  svst1w_u64(pred_full, (unsigned *)(v1054), svreinterpret_u64_s16(v632));
  svst1w_u64(pred_full, (unsigned *)(v1063), svreinterpret_u64_s16(v640));
  svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v391, v381);
  svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v393, v382);
  svfloat32_t v396 = svsub_f32_x(svptrue_b32(), v395, v382);
  svfloat32_t v398 = svsub_f32_x(svptrue_b32(), v397, v378);
  svfloat32_t v410 = svsub_f32_x(svptrue_b32(), v409, v265);
  svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v390, v400);
  svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v390, v400);
  svfloat32_t v601 = svsub_f32_x(svptrue_b32(), v600, v590);
  svfloat32_t v603 = svadd_f32_x(svptrue_b32(), v602, v591);
  svfloat32_t v605 = svsub_f32_x(svptrue_b32(), v604, v591);
  svfloat32_t v607 = svsub_f32_x(svptrue_b32(), v606, v587);
  svfloat32_t v619 = svsub_f32_x(svptrue_b32(), v618, v474);
  svfloat32_t v621 = svadd_f32_x(svptrue_b32(), v599, v609);
  svfloat32_t v628 = svsub_f32_x(svptrue_b32(), v599, v609);
  svfloat32_t v411 = svadd_f32_x(svptrue_b32(), v398, v410);
  svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v392, v402);
  svfloat32_t v414 = svsub_f32_x(svptrue_b32(), v394, v404);
  svfloat32_t v415 = svadd_f32_x(svptrue_b32(), v396, v406);
  svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v396, v406);
  svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v394, v404);
  svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v392, v402);
  svfloat32_t v420 = svsub_f32_x(svptrue_b32(), v398, v410);
  svfloat32_t v620 = svadd_f32_x(svptrue_b32(), v607, v619);
  svfloat32_t v622 = svadd_f32_x(svptrue_b32(), v601, v611);
  svfloat32_t v623 = svsub_f32_x(svptrue_b32(), v603, v613);
  svfloat32_t v624 = svadd_f32_x(svptrue_b32(), v605, v615);
  svfloat32_t v625 = svsub_f32_x(svptrue_b32(), v605, v615);
  svfloat32_t v626 = svadd_f32_x(svptrue_b32(), v603, v613);
  svfloat32_t v627 = svsub_f32_x(svptrue_b32(), v601, v611);
  svfloat32_t v629 = svsub_f32_x(svptrue_b32(), v607, v619);
  svint16_t v664 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v419, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v672 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v628, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v776 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v412, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v784 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v621, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v648 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v420, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v656 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v629, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v680 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v418, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v688 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v627, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v696 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v417, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v704 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v626, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v712 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v416, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v720 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v625, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v728 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v415, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v736 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v624, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v744 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v414, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v752 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v623, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v760 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v413, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v768 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v622, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v792 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v411, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v800 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v620, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v1090), svreinterpret_u64_s16(v664));
  svst1w_u64(pred_full, (unsigned *)(v1099), svreinterpret_u64_s16(v672));
  svst1w_u64(pred_full, (unsigned *)(v1216), svreinterpret_u64_s16(v776));
  svst1w_u64(pred_full, (unsigned *)(v1225), svreinterpret_u64_s16(v784));
  svst1w_u64(pred_full, (unsigned *)(v1072), svreinterpret_u64_s16(v648));
  svst1w_u64(pred_full, (unsigned *)(v1081), svreinterpret_u64_s16(v656));
  svst1w_u64(pred_full, (unsigned *)(v1108), svreinterpret_u64_s16(v680));
  svst1w_u64(pred_full, (unsigned *)(v1117), svreinterpret_u64_s16(v688));
  svst1w_u64(pred_full, (unsigned *)(v1126), svreinterpret_u64_s16(v696));
  svst1w_u64(pred_full, (unsigned *)(v1135), svreinterpret_u64_s16(v704));
  svst1w_u64(pred_full, (unsigned *)(v1144), svreinterpret_u64_s16(v712));
  svst1w_u64(pred_full, (unsigned *)(v1153), svreinterpret_u64_s16(v720));
  svst1w_u64(pred_full, (unsigned *)(v1162), svreinterpret_u64_s16(v728));
  svst1w_u64(pred_full, (unsigned *)(v1171), svreinterpret_u64_s16(v736));
  svst1w_u64(pred_full, (unsigned *)(v1180), svreinterpret_u64_s16(v744));
  svst1w_u64(pred_full, (unsigned *)(v1189), svreinterpret_u64_s16(v752));
  svst1w_u64(pred_full, (unsigned *)(v1198), svreinterpret_u64_s16(v760));
  svst1w_u64(pred_full, (unsigned *)(v1207), svreinterpret_u64_s16(v768));
  svst1w_u64(pred_full, (unsigned *)(v1234), svreinterpret_u64_s16(v792));
  svst1w_u64(pred_full, (unsigned *)(v1243), svreinterpret_u64_s16(v800));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun24(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v216 = 1.0000000000000000e+00F;
  float v217 = -1.0000000000000000e+00F;
  float v224 = -7.0710678118654746e-01F;
  float v231 = 7.0710678118654757e-01F;
  float v283 = -1.4999999999999998e+00F;
  float v284 = 1.4999999999999998e+00F;
  float v291 = 1.0606601717798210e+00F;
  float v298 = -1.0606601717798212e+00F;
  float v352 = 8.6602540378443871e-01F;
  float v360 = -8.6602540378443871e-01F;
  float v367 = 6.1237243569579458e-01F;
  float v368 = -6.1237243569579458e-01F;
  int16x4_t v27 = vld1s_s16(&v5[0]);
  int16x4_t v82 = vld1s_s16(&v5[istride]);
  float32x2_t v218 = (float32x2_t){v216, v217};
  float32x2_t v225 = (float32x2_t){v231, v224};
  float32x2_t v232 = (float32x2_t){v231, v231};
  float32x2_t v281 = (float32x2_t){v283, v283};
  float32x2_t v285 = (float32x2_t){v283, v284};
  float32x2_t v292 = (float32x2_t){v298, v291};
  float32x2_t v299 = (float32x2_t){v298, v298};
  float32x2_t v354 = (float32x2_t){v352, v360};
  float32x2_t v361 = (float32x2_t){v360, v360};
  float32x2_t v365 = (float32x2_t){v368, v368};
  float32x2_t v369 = (float32x2_t){v367, v368};
  float32x2_t v370 = (float32x2_t){v4, v4};
  int16x4_t v13 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v19 = vld1s_s16(&v5[istride * 16]);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  int16x4_t v34 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v40 = vld1s_s16(&v5[istride * 19]);
  int16x4_t v48 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v55 = vld1s_s16(&v5[istride * 14]);
  int16x4_t v61 = vld1s_s16(&v5[istride * 22]);
  int16x4_t v69 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v76 = vld1s_s16(&v5[istride * 17]);
  float32x2_t v83 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v82)), 15);
  int16x4_t v90 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v97 = vld1s_s16(&v5[istride * 20]);
  int16x4_t v103 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v111 = vld1s_s16(&v5[istride * 12]);
  int16x4_t v118 = vld1s_s16(&v5[istride * 23]);
  int16x4_t v124 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v132 = vld1s_s16(&v5[istride * 15]);
  int16x4_t v139 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v145 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v153 = vld1s_s16(&v5[istride * 18]);
  int16x4_t v160 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v166 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v174 = vld1s_s16(&v5[istride * 21]);
  float32x2_t v220 = vmul_f32(v370, v218);
  float32x2_t v227 = vmul_f32(v370, v225);
  float32x2_t v287 = vmul_f32(v370, v285);
  float32x2_t v294 = vmul_f32(v370, v292);
  float32x2_t v356 = vmul_f32(v370, v354);
  float32x2_t v371 = vmul_f32(v370, v369);
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v35 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v34)), 15);
  float32x2_t v41 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v40)), 15);
  float32x2_t v49 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v48)), 15);
  float32x2_t v56 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v55)), 15);
  float32x2_t v62 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v61)), 15);
  float32x2_t v70 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v69)), 15);
  float32x2_t v77 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v76)), 15);
  float32x2_t v91 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v90)), 15);
  float32x2_t v98 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v97)), 15);
  float32x2_t v104 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v103)), 15);
  float32x2_t v112 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v111)), 15);
  float32x2_t v119 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v118)), 15);
  float32x2_t v125 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v124)), 15);
  float32x2_t v133 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v132)), 15);
  float32x2_t v140 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v139)), 15);
  float32x2_t v146 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v145)), 15);
  float32x2_t v154 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v153)), 15);
  float32x2_t v161 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v160)), 15);
  float32x2_t v167 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v166)), 15);
  float32x2_t v175 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v174)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v42 = vadd_f32(v35, v41);
  float32x2_t v43 = vsub_f32(v35, v41);
  float32x2_t v63 = vadd_f32(v56, v62);
  float32x2_t v64 = vsub_f32(v56, v62);
  float32x2_t v84 = vadd_f32(v77, v83);
  float32x2_t v85 = vsub_f32(v77, v83);
  float32x2_t v105 = vadd_f32(v98, v104);
  float32x2_t v106 = vsub_f32(v98, v104);
  float32x2_t v126 = vadd_f32(v119, v125);
  float32x2_t v127 = vsub_f32(v119, v125);
  float32x2_t v147 = vadd_f32(v140, v146);
  float32x2_t v148 = vsub_f32(v140, v146);
  float32x2_t v168 = vadd_f32(v161, v167);
  float32x2_t v169 = vsub_f32(v161, v167);
  float32x2_t v29 = vadd_f32(v21, v28);
  float32x2_t v50 = vadd_f32(v42, v49);
  float32x2_t v71 = vadd_f32(v63, v70);
  float32x2_t v92 = vadd_f32(v84, v91);
  float32x2_t v113 = vadd_f32(v105, v112);
  float32x2_t v134 = vadd_f32(v126, v133);
  float32x2_t v155 = vadd_f32(v147, v154);
  float32x2_t v176 = vadd_f32(v168, v175);
  float32x2_t v244 = vadd_f32(v21, v105);
  float32x2_t v245 = vsub_f32(v21, v105);
  float32x2_t v246 = vadd_f32(v63, v147);
  float32x2_t v247 = vsub_f32(v63, v147);
  float32x2_t v248 = vadd_f32(v42, v126);
  float32x2_t v249 = vsub_f32(v42, v126);
  float32x2_t v250 = vadd_f32(v84, v168);
  float32x2_t v251 = vsub_f32(v84, v168);
  float32x2_t v311 = vadd_f32(v22, v106);
  float32x2_t v312 = vsub_f32(v22, v106);
  float32x2_t v313 = vadd_f32(v64, v148);
  float32x2_t v314 = vsub_f32(v64, v148);
  float32x2_t v315 = vadd_f32(v43, v127);
  float32x2_t v316 = vsub_f32(v43, v127);
  float32x2_t v317 = vadd_f32(v85, v169);
  float32x2_t v318 = vsub_f32(v85, v169);
  float32x2_t v177 = vadd_f32(v29, v113);
  float32x2_t v178 = vsub_f32(v29, v113);
  float32x2_t v179 = vadd_f32(v71, v155);
  float32x2_t v180 = vsub_f32(v71, v155);
  float32x2_t v181 = vadd_f32(v50, v134);
  float32x2_t v182 = vsub_f32(v50, v134);
  float32x2_t v183 = vadd_f32(v92, v176);
  float32x2_t v184 = vsub_f32(v92, v176);
  float32x2_t v252 = vadd_f32(v244, v246);
  float32x2_t v253 = vsub_f32(v244, v246);
  float32x2_t v254 = vadd_f32(v248, v250);
  float32x2_t v255 = vsub_f32(v248, v250);
  float32x2_t v258 = vadd_f32(v249, v251);
  float32x2_t v259 = vsub_f32(v249, v251);
  float32x2_t v282 = vmul_f32(v245, v281);
  float32x2_t v288 = vrev64_f32(v247);
  float32x2_t v319 = vadd_f32(v311, v313);
  float32x2_t v320 = vsub_f32(v311, v313);
  float32x2_t v321 = vadd_f32(v315, v317);
  float32x2_t v322 = vsub_f32(v315, v317);
  float32x2_t v325 = vadd_f32(v316, v318);
  float32x2_t v326 = vsub_f32(v316, v318);
  float32x2_t v357 = vrev64_f32(v312);
  float32x2_t v362 = vmul_f32(v314, v361);
  float32x2_t v185 = vadd_f32(v177, v179);
  float32x2_t v186 = vsub_f32(v177, v179);
  float32x2_t v187 = vadd_f32(v181, v183);
  float32x2_t v188 = vsub_f32(v181, v183);
  float32x2_t v191 = vadd_f32(v182, v184);
  float32x2_t v192 = vsub_f32(v182, v184);
  float32x2_t v221 = vrev64_f32(v180);
  float32x2_t v256 = vadd_f32(v252, v254);
  float32x2_t v257 = vsub_f32(v252, v254);
  float32x2_t v271 = vmul_f32(v253, v281);
  float32x2_t v277 = vrev64_f32(v255);
  float32x2_t v289 = vmul_f32(v288, v287);
  float32x2_t v295 = vrev64_f32(v258);
  float32x2_t v300 = vmul_f32(v259, v299);
  float32x2_t v323 = vadd_f32(v319, v321);
  float32x2_t v324 = vsub_f32(v319, v321);
  float32x2_t v346 = vrev64_f32(v320);
  float32x2_t v351 = vmul_f32(v322, v361);
  float32x2_t v358 = vmul_f32(v357, v356);
  float32x2_t v366 = vmul_f32(v325, v365);
  float32x2_t v372 = vrev64_f32(v326);
  float32x2_t v189 = vadd_f32(v185, v187);
  float32x2_t v190 = vsub_f32(v185, v187);
  float32x2_t v210 = vrev64_f32(v188);
  float32x2_t v222 = vmul_f32(v221, v220);
  float32x2_t v228 = vrev64_f32(v191);
  float32x2_t v233 = vmul_f32(v192, v232);
  float32x2_t v263 = vmul_f32(v256, v281);
  float32x2_t v267 = vmul_f32(v257, v281);
  float32x2_t v278 = vmul_f32(v277, v287);
  float32x2_t v296 = vmul_f32(v295, v294);
  float32x2_t v303 = vadd_f32(v282, v300);
  float32x2_t v304 = vsub_f32(v282, v300);
  float32x2_t v332 = vrev64_f32(v323);
  float32x2_t v339 = vrev64_f32(v324);
  float32x2_t v347 = vmul_f32(v346, v356);
  float32x2_t v373 = vmul_f32(v372, v371);
  float32x2_t v378 = vadd_f32(v362, v366);
  float32x2_t v379 = vsub_f32(v362, v366);
  float32x2_t v211 = vmul_f32(v210, v220);
  float32x2_t v229 = vmul_f32(v228, v227);
  float32x2_t v236 = vadd_f32(v178, v233);
  float32x2_t v237 = vsub_f32(v178, v233);
  float32x2_t v301 = vadd_f32(v271, v278);
  float32x2_t v302 = vsub_f32(v271, v278);
  float32x2_t v305 = vadd_f32(v289, v296);
  float32x2_t v306 = vsub_f32(v289, v296);
  float32x2_t v333 = vmul_f32(v332, v356);
  float32x2_t v340 = vmul_f32(v339, v356);
  float32x2_t v374 = vadd_f32(v347, v351);
  float32x2_t v375 = vsub_f32(v347, v351);
  float32x2_t v376 = vadd_f32(v358, v373);
  float32x2_t v377 = vsub_f32(v358, v373);
  float32x2_t v384 = vadd_f32(v189, v263);
  int16x4_t v389 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v189, 15), (int32x2_t){0, 0}));
  float32x2_t v468 = vadd_f32(v190, v267);
  int16x4_t v473 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v190, 15), (int32x2_t){0, 0}));
  float32x2_t v234 = vadd_f32(v186, v211);
  float32x2_t v235 = vsub_f32(v186, v211);
  float32x2_t v238 = vadd_f32(v222, v229);
  float32x2_t v239 = vsub_f32(v222, v229);
  float32x2_t v307 = vadd_f32(v303, v305);
  float32x2_t v308 = vsub_f32(v303, v305);
  float32x2_t v309 = vadd_f32(v304, v306);
  float32x2_t v310 = vsub_f32(v304, v306);
  float32x2_t v380 = vadd_f32(v376, v378);
  float32x2_t v381 = vsub_f32(v376, v378);
  float32x2_t v382 = vadd_f32(v377, v379);
  float32x2_t v383 = vsub_f32(v377, v379);
  float32x2_t v385 = vadd_f32(v384, v333);
  float32x2_t v386 = vsub_f32(v384, v333);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v389), 0);
  float32x2_t v469 = vadd_f32(v468, v340);
  float32x2_t v470 = vsub_f32(v468, v340);
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v473), 0);
  float32x2_t v240 = vadd_f32(v236, v238);
  float32x2_t v241 = vsub_f32(v236, v238);
  float32x2_t v242 = vadd_f32(v237, v239);
  float32x2_t v243 = vsub_f32(v237, v239);
  int16x4_t v395 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v386, 15), (int32x2_t){0, 0}));
  int16x4_t v401 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v385, 15), (int32x2_t){0, 0}));
  float32x2_t v426 = vadd_f32(v235, v302);
  int16x4_t v431 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v235, 15), (int32x2_t){0, 0}));
  int16x4_t v479 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v470, 15), (int32x2_t){0, 0}));
  int16x4_t v485 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v469, 15), (int32x2_t){0, 0}));
  float32x2_t v510 = vadd_f32(v234, v301);
  int16x4_t v515 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v234, 15), (int32x2_t){0, 0}));
  v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v395), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v401), 0);
  float32x2_t v405 = vadd_f32(v241, v308);
  int16x4_t v410 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v241, 15), (int32x2_t){0, 0}));
  float32x2_t v427 = vadd_f32(v426, v375);
  float32x2_t v428 = vsub_f32(v426, v375);
  v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v431), 0);
  float32x2_t v447 = vadd_f32(v242, v309);
  int16x4_t v452 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v242, 15), (int32x2_t){0, 0}));
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v479), 0);
  v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v485), 0);
  float32x2_t v489 = vadd_f32(v243, v310);
  int16x4_t v494 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v243, 15), (int32x2_t){0, 0}));
  float32x2_t v511 = vadd_f32(v510, v374);
  float32x2_t v512 = vsub_f32(v510, v374);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v515), 0);
  float32x2_t v531 = vadd_f32(v240, v307);
  int16x4_t v536 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v240, 15), (int32x2_t){0, 0}));
  float32x2_t v406 = vadd_f32(v405, v381);
  float32x2_t v407 = vsub_f32(v405, v381);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v410), 0);
  int16x4_t v437 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v428, 15), (int32x2_t){0, 0}));
  int16x4_t v443 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v427, 15), (int32x2_t){0, 0}));
  float32x2_t v448 = vadd_f32(v447, v382);
  float32x2_t v449 = vsub_f32(v447, v382);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v452), 0);
  float32x2_t v490 = vadd_f32(v489, v383);
  float32x2_t v491 = vsub_f32(v489, v383);
  v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v494), 0);
  int16x4_t v521 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v512, 15), (int32x2_t){0, 0}));
  int16x4_t v527 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v511, 15), (int32x2_t){0, 0}));
  float32x2_t v532 = vadd_f32(v531, v380);
  float32x2_t v533 = vsub_f32(v531, v380);
  v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v536), 0);
  int16x4_t v416 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v407, 15), (int32x2_t){0, 0}));
  int16x4_t v422 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v406, 15), (int32x2_t){0, 0}));
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v437), 0);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v443), 0);
  int16x4_t v458 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v449, 15), (int32x2_t){0, 0}));
  int16x4_t v464 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v448, 15), (int32x2_t){0, 0}));
  int16x4_t v500 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v491, 15), (int32x2_t){0, 0}));
  int16x4_t v506 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v490, 15), (int32x2_t){0, 0}));
  v6[ostride * 22] = vget_lane_s32(vreinterpret_s32_s16(v521), 0);
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v527), 0);
  int16x4_t v542 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v533, 15), (int32x2_t){0, 0}));
  int16x4_t v548 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v532, 15), (int32x2_t){0, 0}));
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v416), 0);
  v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v422), 0);
  v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v458), 0);
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v464), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v500), 0);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v506), 0);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v542), 0);
  v6[ostride * 23] = vget_lane_s32(vreinterpret_s32_s16(v548), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun24(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v274 = -1.0000000000000000e+00F;
  float v281 = -7.0710678118654746e-01F;
  float v288 = 7.0710678118654757e-01F;
  float v341 = -1.4999999999999998e+00F;
  float v346 = 1.4999999999999998e+00F;
  float v353 = 1.0606601717798210e+00F;
  float v360 = -1.0606601717798212e+00F;
  float v424 = -8.6602540378443871e-01F;
  float v434 = -6.1237243569579458e-01F;
  const int32_t *v763 = &v5[v0];
  int32_t *v950 = &v6[v2];
  int64_t v15 = v0 * 8;
  int64_t v23 = v0 * 16;
  int64_t v42 = v0 * 11;
  int64_t v50 = v0 * 19;
  int64_t v60 = v0 * 3;
  int64_t v69 = v0 * 14;
  int64_t v77 = v0 * 22;
  int64_t v87 = v0 * 6;
  int64_t v96 = v0 * 17;
  int64_t v114 = v0 * 9;
  int64_t v123 = v0 * 20;
  int64_t v131 = v0 * 4;
  int64_t v141 = v0 * 12;
  int64_t v150 = v0 * 23;
  int64_t v158 = v0 * 7;
  int64_t v168 = v0 * 15;
  int64_t v177 = v0 * 2;
  int64_t v185 = v0 * 10;
  int64_t v195 = v0 * 18;
  int64_t v204 = v0 * 5;
  int64_t v212 = v0 * 13;
  int64_t v222 = v0 * 21;
  float v277 = v4 * v274;
  float v284 = v4 * v281;
  float v349 = v4 * v346;
  float v356 = v4 * v353;
  float v420 = v4 * v424;
  float v437 = v4 * v434;
  int64_t v462 = v2 * 16;
  int64_t v470 = v2 * 8;
  int64_t v481 = v2 * 9;
  int64_t v497 = v2 * 17;
  int64_t v508 = v2 * 18;
  int64_t v516 = v2 * 10;
  int64_t v524 = v2 * 2;
  int64_t v535 = v2 * 3;
  int64_t v543 = v2 * 19;
  int64_t v551 = v2 * 11;
  int64_t v562 = v2 * 12;
  int64_t v570 = v2 * 4;
  int64_t v578 = v2 * 20;
  int64_t v589 = v2 * 21;
  int64_t v597 = v2 * 13;
  int64_t v605 = v2 * 5;
  int64_t v616 = v2 * 6;
  int64_t v624 = v2 * 22;
  int64_t v632 = v2 * 14;
  int64_t v643 = v2 * 15;
  int64_t v651 = v2 * 7;
  int64_t v659 = v2 * 23;
  const int32_t *v691 = &v5[0];
  svfloat32_t v890 = svdup_n_f32(v288);
  svfloat32_t v895 = svdup_n_f32(v341);
  svfloat32_t v898 = svdup_n_f32(v360);
  svfloat32_t v904 = svdup_n_f32(v424);
  svfloat32_t v905 = svdup_n_f32(v434);
  int32_t *v914 = &v6[0];
  svfloat32_t v110 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v763[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v672 = &v5[v15];
  const int32_t *v681 = &v5[v23];
  const int32_t *v700 = &v5[v42];
  const int32_t *v709 = &v5[v50];
  const int32_t *v718 = &v5[v60];
  const int32_t *v727 = &v5[v69];
  const int32_t *v736 = &v5[v77];
  const int32_t *v745 = &v5[v87];
  const int32_t *v754 = &v5[v96];
  const int32_t *v772 = &v5[v114];
  const int32_t *v781 = &v5[v123];
  const int32_t *v790 = &v5[v131];
  const int32_t *v799 = &v5[v141];
  const int32_t *v808 = &v5[v150];
  const int32_t *v817 = &v5[v158];
  const int32_t *v826 = &v5[v168];
  const int32_t *v835 = &v5[v177];
  const int32_t *v844 = &v5[v185];
  const int32_t *v853 = &v5[v195];
  const int32_t *v862 = &v5[v204];
  const int32_t *v871 = &v5[v212];
  const int32_t *v880 = &v5[v222];
  svfloat32_t v888 = svdup_n_f32(v277);
  svfloat32_t v889 = svdup_n_f32(v284);
  svfloat32_t v896 = svdup_n_f32(v349);
  svfloat32_t v897 = svdup_n_f32(v356);
  svfloat32_t v903 = svdup_n_f32(v420);
  svfloat32_t v906 = svdup_n_f32(v437);
  int32_t *v923 = &v6[v462];
  int32_t *v932 = &v6[v470];
  int32_t *v941 = &v6[v481];
  int32_t *v959 = &v6[v497];
  int32_t *v968 = &v6[v508];
  int32_t *v977 = &v6[v516];
  int32_t *v986 = &v6[v524];
  int32_t *v995 = &v6[v535];
  int32_t *v1004 = &v6[v543];
  int32_t *v1013 = &v6[v551];
  int32_t *v1022 = &v6[v562];
  int32_t *v1031 = &v6[v570];
  int32_t *v1040 = &v6[v578];
  int32_t *v1049 = &v6[v589];
  int32_t *v1058 = &v6[v597];
  int32_t *v1067 = &v6[v605];
  int32_t *v1076 = &v6[v616];
  int32_t *v1085 = &v6[v624];
  int32_t *v1094 = &v6[v632];
  int32_t *v1103 = &v6[v643];
  int32_t *v1112 = &v6[v651];
  int32_t *v1121 = &v6[v659];
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v691[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v672[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v681[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v48 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v700[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v56 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v709[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v66 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v718[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v75 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v727[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v83 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v736[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v93 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v745[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v102 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v754[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v120 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v772[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v129 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v781[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v137 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v790[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v147 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v799[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v156 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v808[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v164 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v817[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v174 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v826[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v183 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v835[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v191 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v844[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v201 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v853[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v210 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v862[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v218 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v871[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v228 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v880[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v57 = svadd_f32_x(svptrue_b32(), v48, v56);
  svfloat32_t v58 = svsub_f32_x(svptrue_b32(), v48, v56);
  svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v75, v83);
  svfloat32_t v111 = svadd_f32_x(svptrue_b32(), v102, v110);
  svfloat32_t v112 = svsub_f32_x(svptrue_b32(), v102, v110);
  svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v129, v137);
  svfloat32_t v165 = svadd_f32_x(svptrue_b32(), v156, v164);
  svfloat32_t v166 = svsub_f32_x(svptrue_b32(), v156, v164);
  svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v183, v191);
  svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v183, v191);
  svfloat32_t v219 = svadd_f32_x(svptrue_b32(), v210, v218);
  svfloat32_t v220 = svsub_f32_x(svptrue_b32(), v210, v218);
  svfloat32_t v40 = svadd_f32_x(svptrue_b32(), v30, v39);
  svfloat32_t v67 = svadd_f32_x(svptrue_b32(), v57, v66);
  svfloat32_t v94 = svadd_f32_x(svptrue_b32(), v84, v93);
  svfloat32_t v121 = svadd_f32_x(svptrue_b32(), v111, v120);
  svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v138, v147);
  svfloat32_t v175 = svadd_f32_x(svptrue_b32(), v165, v174);
  svfloat32_t v202 = svadd_f32_x(svptrue_b32(), v192, v201);
  svfloat32_t v229 = svadd_f32_x(svptrue_b32(), v219, v228);
  svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v30, v138);
  svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v30, v138);
  svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v84, v192);
  svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v84, v192);
  svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v57, v165);
  svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v57, v165);
  svfloat32_t v308 = svadd_f32_x(svptrue_b32(), v111, v219);
  svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v111, v219);
  svfloat32_t v374 = svadd_f32_x(svptrue_b32(), v31, v139);
  svfloat32_t v375 = svsub_f32_x(svptrue_b32(), v31, v139);
  svfloat32_t v376 = svadd_f32_x(svptrue_b32(), v85, v193);
  svfloat32_t v377 = svsub_f32_x(svptrue_b32(), v85, v193);
  svfloat32_t v378 = svadd_f32_x(svptrue_b32(), v58, v166);
  svfloat32_t v379 = svsub_f32_x(svptrue_b32(), v58, v166);
  svfloat32_t v380 = svadd_f32_x(svptrue_b32(), v112, v220);
  svfloat32_t v381 = svsub_f32_x(svptrue_b32(), v112, v220);
  svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v40, v148);
  svfloat32_t v231 = svsub_f32_x(svptrue_b32(), v40, v148);
  svfloat32_t v232 = svadd_f32_x(svptrue_b32(), v94, v202);
  svfloat32_t v233 = svsub_f32_x(svptrue_b32(), v94, v202);
  svfloat32_t v234 = svadd_f32_x(svptrue_b32(), v67, v175);
  svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v67, v175);
  svfloat32_t v236 = svadd_f32_x(svptrue_b32(), v121, v229);
  svfloat32_t v237 = svsub_f32_x(svptrue_b32(), v121, v229);
  svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v302, v304);
  svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v302, v304);
  svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v306, v308);
  svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v306, v308);
  svfloat32_t v316 = svadd_f32_x(svptrue_b32(), v307, v309);
  svfloat32_t v317 = svsub_f32_x(svptrue_b32(), v307, v309);
  svfloat32_t zero351 = svdup_n_f32(0);
  svfloat32_t v351 = svcmla_f32_x(pred_full, zero351, v896, v305, 90);
  svfloat32_t v382 = svadd_f32_x(svptrue_b32(), v374, v376);
  svfloat32_t v383 = svsub_f32_x(svptrue_b32(), v374, v376);
  svfloat32_t v384 = svadd_f32_x(svptrue_b32(), v378, v380);
  svfloat32_t v385 = svsub_f32_x(svptrue_b32(), v378, v380);
  svfloat32_t v388 = svadd_f32_x(svptrue_b32(), v379, v381);
  svfloat32_t v389 = svsub_f32_x(svptrue_b32(), v379, v381);
  svfloat32_t zero422 = svdup_n_f32(0);
  svfloat32_t v422 = svcmla_f32_x(pred_full, zero422, v903, v375, 90);
  svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v230, v232);
  svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v230, v232);
  svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v234, v236);
  svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v234, v236);
  svfloat32_t v244 = svadd_f32_x(svptrue_b32(), v235, v237);
  svfloat32_t v245 = svsub_f32_x(svptrue_b32(), v235, v237);
  svfloat32_t zero279 = svdup_n_f32(0);
  svfloat32_t v279 = svcmla_f32_x(pred_full, zero279, v888, v233, 90);
  svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v310, v312);
  svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v310, v312);
  svfloat32_t zero339 = svdup_n_f32(0);
  svfloat32_t v339 = svcmla_f32_x(pred_full, zero339, v896, v313, 90);
  svfloat32_t zero358 = svdup_n_f32(0);
  svfloat32_t v358 = svcmla_f32_x(pred_full, zero358, v897, v316, 90);
  svfloat32_t v363 = svmul_f32_x(svptrue_b32(), v317, v898);
  svfloat32_t v386 = svadd_f32_x(svptrue_b32(), v382, v384);
  svfloat32_t v387 = svsub_f32_x(svptrue_b32(), v382, v384);
  svfloat32_t zero410 = svdup_n_f32(0);
  svfloat32_t v410 = svcmla_f32_x(pred_full, zero410, v903, v383, 90);
  svfloat32_t v432 = svmul_f32_x(svptrue_b32(), v388, v905);
  svfloat32_t zero439 = svdup_n_f32(0);
  svfloat32_t v439 = svcmla_f32_x(pred_full, zero439, v906, v389, 90);
  svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v238, v240);
  svfloat32_t v243 = svsub_f32_x(svptrue_b32(), v238, v240);
  svfloat32_t zero267 = svdup_n_f32(0);
  svfloat32_t v267 = svcmla_f32_x(pred_full, zero267, v888, v241, 90);
  svfloat32_t zero286 = svdup_n_f32(0);
  svfloat32_t v286 = svcmla_f32_x(pred_full, zero286, v889, v244, 90);
  svfloat32_t v364 = svmla_f32_x(pred_full, v339, v311, v895);
  svfloat32_t v365 = svnmls_f32_x(pred_full, v339, v311, v895);
  svfloat32_t v366 = svmla_f32_x(pred_full, v363, v303, v895);
  svfloat32_t v367 = svnmls_f32_x(pred_full, v363, v303, v895);
  svfloat32_t v368 = svadd_f32_x(svptrue_b32(), v351, v358);
  svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v351, v358);
  svfloat32_t zero396 = svdup_n_f32(0);
  svfloat32_t v396 = svcmla_f32_x(pred_full, zero396, v903, v386, 90);
  svfloat32_t zero403 = svdup_n_f32(0);
  svfloat32_t v403 = svcmla_f32_x(pred_full, zero403, v903, v387, 90);
  svfloat32_t v440 = svmla_f32_x(pred_full, v410, v385, v904);
  svfloat32_t v441 = svmls_f32_x(pred_full, v410, v385, v904);
  svfloat32_t v442 = svadd_f32_x(svptrue_b32(), v422, v439);
  svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v422, v439);
  svfloat32_t v444 = svmla_f32_x(pred_full, v432, v377, v904);
  svfloat32_t v445 = svnmls_f32_x(pred_full, v432, v377, v904);
  svfloat32_t v292 = svadd_f32_x(svptrue_b32(), v239, v267);
  svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v239, v267);
  svfloat32_t v294 = svmla_f32_x(pred_full, v231, v245, v890);
  svfloat32_t v295 = svmls_f32_x(pred_full, v231, v245, v890);
  svfloat32_t v296 = svadd_f32_x(svptrue_b32(), v279, v286);
  svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v279, v286);
  svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v366, v368);
  svfloat32_t v371 = svsub_f32_x(svptrue_b32(), v366, v368);
  svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v367, v369);
  svfloat32_t v373 = svsub_f32_x(svptrue_b32(), v367, v369);
  svfloat32_t v446 = svadd_f32_x(svptrue_b32(), v442, v444);
  svfloat32_t v447 = svsub_f32_x(svptrue_b32(), v442, v444);
  svfloat32_t v448 = svadd_f32_x(svptrue_b32(), v443, v445);
  svfloat32_t v449 = svsub_f32_x(svptrue_b32(), v443, v445);
  svfloat32_t v450 = svmla_f32_x(pred_full, v242, v314, v895);
  svint16_t v455 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v242, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v558 = svmla_f32_x(pred_full, v243, v315, v895);
  svint16_t v563 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v243, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v294, v296);
  svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v294, v296);
  svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v295, v297);
  svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v295, v297);
  svfloat32_t v451 = svadd_f32_x(svptrue_b32(), v450, v396);
  svfloat32_t v452 = svsub_f32_x(svptrue_b32(), v450, v396);
  svfloat32_t v504 = svadd_f32_x(svptrue_b32(), v293, v365);
  svint16_t v509 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v293, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v559 = svadd_f32_x(svptrue_b32(), v558, v403);
  svfloat32_t v560 = svsub_f32_x(svptrue_b32(), v558, v403);
  svfloat32_t v612 = svadd_f32_x(svptrue_b32(), v292, v364);
  svint16_t v617 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v292, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v914), svreinterpret_u64_s16(v455));
  svst1w_u64(pred_full, (unsigned *)(v1022), svreinterpret_u64_s16(v563));
  svint16_t v463 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v452, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v471 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v451, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v477 = svadd_f32_x(svptrue_b32(), v299, v371);
  svint16_t v482 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v299, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v505 = svadd_f32_x(svptrue_b32(), v504, v441);
  svfloat32_t v506 = svsub_f32_x(svptrue_b32(), v504, v441);
  svfloat32_t v531 = svadd_f32_x(svptrue_b32(), v300, v372);
  svint16_t v536 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v300, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v571 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v560, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v579 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v559, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v585 = svadd_f32_x(svptrue_b32(), v301, v373);
  svint16_t v590 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v301, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v613 = svadd_f32_x(svptrue_b32(), v612, v440);
  svfloat32_t v614 = svsub_f32_x(svptrue_b32(), v612, v440);
  svfloat32_t v639 = svadd_f32_x(svptrue_b32(), v298, v370);
  svint16_t v644 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v298, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v968), svreinterpret_u64_s16(v509));
  svst1w_u64(pred_full, (unsigned *)(v1076), svreinterpret_u64_s16(v617));
  svfloat32_t v478 = svadd_f32_x(svptrue_b32(), v477, v447);
  svfloat32_t v479 = svsub_f32_x(svptrue_b32(), v477, v447);
  svint16_t v517 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v506, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v525 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v505, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v532 = svadd_f32_x(svptrue_b32(), v531, v448);
  svfloat32_t v533 = svsub_f32_x(svptrue_b32(), v531, v448);
  svfloat32_t v586 = svadd_f32_x(svptrue_b32(), v585, v449);
  svfloat32_t v587 = svsub_f32_x(svptrue_b32(), v585, v449);
  svint16_t v625 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v614, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v633 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v613, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v640 = svadd_f32_x(svptrue_b32(), v639, v446);
  svfloat32_t v641 = svsub_f32_x(svptrue_b32(), v639, v446);
  svst1w_u64(pred_full, (unsigned *)(v923), svreinterpret_u64_s16(v463));
  svst1w_u64(pred_full, (unsigned *)(v932), svreinterpret_u64_s16(v471));
  svst1w_u64(pred_full, (unsigned *)(v941), svreinterpret_u64_s16(v482));
  svst1w_u64(pred_full, (unsigned *)(v995), svreinterpret_u64_s16(v536));
  svst1w_u64(pred_full, (unsigned *)(v1031), svreinterpret_u64_s16(v571));
  svst1w_u64(pred_full, (unsigned *)(v1040), svreinterpret_u64_s16(v579));
  svst1w_u64(pred_full, (unsigned *)(v1049), svreinterpret_u64_s16(v590));
  svst1w_u64(pred_full, (unsigned *)(v1103), svreinterpret_u64_s16(v644));
  svint16_t v490 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v479, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v498 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v478, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v544 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v533, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v552 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v532, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v598 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v587, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v606 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v586, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v652 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v641, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v660 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v640, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v977), svreinterpret_u64_s16(v517));
  svst1w_u64(pred_full, (unsigned *)(v986), svreinterpret_u64_s16(v525));
  svst1w_u64(pred_full, (unsigned *)(v1085), svreinterpret_u64_s16(v625));
  svst1w_u64(pred_full, (unsigned *)(v1094), svreinterpret_u64_s16(v633));
  svst1w_u64(pred_full, (unsigned *)(v950), svreinterpret_u64_s16(v490));
  svst1w_u64(pred_full, (unsigned *)(v959), svreinterpret_u64_s16(v498));
  svst1w_u64(pred_full, (unsigned *)(v1004), svreinterpret_u64_s16(v544));
  svst1w_u64(pred_full, (unsigned *)(v1013), svreinterpret_u64_s16(v552));
  svst1w_u64(pred_full, (unsigned *)(v1058), svreinterpret_u64_s16(v598));
  svst1w_u64(pred_full, (unsigned *)(v1067), svreinterpret_u64_s16(v606));
  svst1w_u64(pred_full, (unsigned *)(v1112), svreinterpret_u64_s16(v652));
  svst1w_u64(pred_full, (unsigned *)(v1121), svreinterpret_u64_s16(v660));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun25(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v874 = 9.6858316112863108e-01F;
  float v877 = -2.4868988716485479e-01F;
  float v878 = 2.4868988716485479e-01F;
  float v1018 = 8.7630668004386358e-01F;
  float v1021 = -4.8175367410171532e-01F;
  float v1022 = 4.8175367410171532e-01F;
  float v1162 = 7.2896862742141155e-01F;
  float v1165 = -6.8454710592868862e-01F;
  float v1166 = 6.8454710592868862e-01F;
  float v1174 = 6.2790519529313527e-02F;
  float v1177 = -9.9802672842827156e-01F;
  float v1178 = 9.9802672842827156e-01F;
  float v1306 = 5.3582679497899655e-01F;
  float v1309 = -8.4432792550201508e-01F;
  float v1310 = 8.4432792550201508e-01F;
  float v1318 = -4.2577929156507272e-01F;
  float v1321 = -9.0482705246601947e-01F;
  float v1322 = 9.0482705246601947e-01F;
  float v1330 = -6.3742398974868952e-01F;
  float v1333 = 7.7051324277578936e-01F;
  float v1334 = -7.7051324277578936e-01F;
  float v1348 = -9.9211470131447776e-01F;
  float v1351 = -1.2533323356430454e-01F;
  float v1352 = 1.2533323356430454e-01F;
  float v1368 = 2.5000000000000000e-01F;
  float v1378 = 5.5901699437494745e-01F;
  float v1388 = 6.1803398874989490e-01F;
  float v1413 = 9.5105651629515353e-01F;
  float v1414 = -9.5105651629515353e-01F;
  float v1439 = 2.0000000000000000e+00F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v157 = vld1s_s16(&v5[istride]);
  float32x2_t v875 = (float32x2_t){v874, v874};
  float32x2_t v879 = (float32x2_t){v877, v878};
  float32x2_t v1019 = (float32x2_t){v1018, v1018};
  float32x2_t v1023 = (float32x2_t){v1021, v1022};
  float32x2_t v1163 = (float32x2_t){v1162, v1162};
  float32x2_t v1167 = (float32x2_t){v1165, v1166};
  float32x2_t v1175 = (float32x2_t){v1174, v1174};
  float32x2_t v1179 = (float32x2_t){v1177, v1178};
  float32x2_t v1209 = (float32x2_t){v1334, v1333};
  float32x2_t v1307 = (float32x2_t){v1306, v1306};
  float32x2_t v1311 = (float32x2_t){v1309, v1310};
  float32x2_t v1319 = (float32x2_t){v1318, v1318};
  float32x2_t v1323 = (float32x2_t){v1321, v1322};
  float32x2_t v1331 = (float32x2_t){v1330, v1330};
  float32x2_t v1335 = (float32x2_t){v1333, v1334};
  float32x2_t v1349 = (float32x2_t){v1348, v1348};
  float32x2_t v1353 = (float32x2_t){v1351, v1352};
  float32x2_t v1369 = (float32x2_t){v1368, v1368};
  float32x2_t v1379 = (float32x2_t){v1378, v1378};
  float32x2_t v1389 = (float32x2_t){v1388, v1388};
  float32x2_t v1415 = (float32x2_t){v1413, v1414};
  float32x2_t v1416 = (float32x2_t){v4, v4};
  float32x2_t v1440 = (float32x2_t){v1439, v1439};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v25 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v31 = vld1s_s16(&v5[istride * 15]);
  int16x4_t v37 = vld1s_s16(&v5[istride * 20]);
  float32x2_t v158 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v157)), 15);
  int16x4_t v163 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v169 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v175 = vld1s_s16(&v5[istride * 16]);
  int16x4_t v181 = vld1s_s16(&v5[istride * 21]);
  int16x4_t v301 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v307 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v313 = vld1s_s16(&v5[istride * 12]);
  int16x4_t v319 = vld1s_s16(&v5[istride * 17]);
  int16x4_t v325 = vld1s_s16(&v5[istride * 22]);
  int16x4_t v445 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v451 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v457 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v463 = vld1s_s16(&v5[istride * 18]);
  int16x4_t v469 = vld1s_s16(&v5[istride * 23]);
  int16x4_t v589 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v595 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v601 = vld1s_s16(&v5[istride * 14]);
  int16x4_t v607 = vld1s_s16(&v5[istride * 19]);
  int16x4_t v613 = vld1s_s16(&v5[istride * 24]);
  float32x2_t v881 = vmul_f32(v1416, v879);
  float32x2_t v1025 = vmul_f32(v1416, v1023);
  float32x2_t v1169 = vmul_f32(v1416, v1167);
  float32x2_t v1181 = vmul_f32(v1416, v1179);
  float32x2_t v1211 = vmul_f32(v1416, v1209);
  float32x2_t v1313 = vmul_f32(v1416, v1311);
  float32x2_t v1325 = vmul_f32(v1416, v1323);
  float32x2_t v1337 = vmul_f32(v1416, v1335);
  float32x2_t v1355 = vmul_f32(v1416, v1353);
  float32x2_t v1417 = vmul_f32(v1416, v1415);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v26 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v25)), 15);
  float32x2_t v32 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v31)), 15);
  float32x2_t v38 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v37)), 15);
  float32x2_t v164 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v163)), 15);
  float32x2_t v170 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v169)), 15);
  float32x2_t v176 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v175)), 15);
  float32x2_t v182 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v181)), 15);
  float32x2_t v302 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v301)), 15);
  float32x2_t v308 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v307)), 15);
  float32x2_t v314 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v313)), 15);
  float32x2_t v320 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v319)), 15);
  float32x2_t v326 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v325)), 15);
  float32x2_t v446 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v445)), 15);
  float32x2_t v452 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v451)), 15);
  float32x2_t v458 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v457)), 15);
  float32x2_t v464 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v463)), 15);
  float32x2_t v470 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v469)), 15);
  float32x2_t v590 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v589)), 15);
  float32x2_t v596 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v595)), 15);
  float32x2_t v602 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v601)), 15);
  float32x2_t v608 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v607)), 15);
  float32x2_t v614 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v613)), 15);
  float32x2_t v75 = vsub_f32(v20, v38);
  float32x2_t v79 = vmul_f32(v20, v1440);
  float32x2_t v93 = vsub_f32(v26, v32);
  float32x2_t v97 = vmul_f32(v26, v1440);
  float32x2_t v219 = vsub_f32(v164, v182);
  float32x2_t v223 = vmul_f32(v164, v1440);
  float32x2_t v237 = vsub_f32(v170, v176);
  float32x2_t v241 = vmul_f32(v170, v1440);
  float32x2_t v363 = vsub_f32(v308, v326);
  float32x2_t v367 = vmul_f32(v308, v1440);
  float32x2_t v381 = vsub_f32(v314, v320);
  float32x2_t v385 = vmul_f32(v314, v1440);
  float32x2_t v507 = vsub_f32(v452, v470);
  float32x2_t v511 = vmul_f32(v452, v1440);
  float32x2_t v525 = vsub_f32(v458, v464);
  float32x2_t v529 = vmul_f32(v458, v1440);
  float32x2_t v651 = vsub_f32(v596, v614);
  float32x2_t v655 = vmul_f32(v596, v1440);
  float32x2_t v669 = vsub_f32(v602, v608);
  float32x2_t v673 = vmul_f32(v602, v1440);
  float32x2_t v80 = vsub_f32(v79, v75);
  float32x2_t v98 = vsub_f32(v97, v93);
  float32x2_t v109 = vmul_f32(v93, v1389);
  float32x2_t v124 = vmul_f32(v75, v1389);
  float32x2_t v224 = vsub_f32(v223, v219);
  float32x2_t v242 = vsub_f32(v241, v237);
  float32x2_t v253 = vmul_f32(v237, v1389);
  float32x2_t v268 = vmul_f32(v219, v1389);
  float32x2_t v368 = vsub_f32(v367, v363);
  float32x2_t v386 = vsub_f32(v385, v381);
  float32x2_t v397 = vmul_f32(v381, v1389);
  float32x2_t v412 = vmul_f32(v363, v1389);
  float32x2_t v512 = vsub_f32(v511, v507);
  float32x2_t v530 = vsub_f32(v529, v525);
  float32x2_t v541 = vmul_f32(v525, v1389);
  float32x2_t v556 = vmul_f32(v507, v1389);
  float32x2_t v656 = vsub_f32(v655, v651);
  float32x2_t v674 = vsub_f32(v673, v669);
  float32x2_t v685 = vmul_f32(v669, v1389);
  float32x2_t v700 = vmul_f32(v651, v1389);
  float32x2_t v99 = vadd_f32(v80, v98);
  float32x2_t v100 = vsub_f32(v80, v98);
  float32x2_t v110 = vadd_f32(v75, v109);
  float32x2_t v125 = vsub_f32(v124, v93);
  float32x2_t v243 = vadd_f32(v224, v242);
  float32x2_t v244 = vsub_f32(v224, v242);
  float32x2_t v254 = vadd_f32(v219, v253);
  float32x2_t v269 = vsub_f32(v268, v237);
  float32x2_t v387 = vadd_f32(v368, v386);
  float32x2_t v388 = vsub_f32(v368, v386);
  float32x2_t v398 = vadd_f32(v363, v397);
  float32x2_t v413 = vsub_f32(v412, v381);
  float32x2_t v531 = vadd_f32(v512, v530);
  float32x2_t v532 = vsub_f32(v512, v530);
  float32x2_t v542 = vadd_f32(v507, v541);
  float32x2_t v557 = vsub_f32(v556, v525);
  float32x2_t v675 = vadd_f32(v656, v674);
  float32x2_t v676 = vsub_f32(v656, v674);
  float32x2_t v686 = vadd_f32(v651, v685);
  float32x2_t v701 = vsub_f32(v700, v669);
  float32x2_t v104 = vmul_f32(v99, v1369);
  float32x2_t v114 = vmul_f32(v100, v1379);
  float32x2_t v126 = vadd_f32(v14, v99);
  float32x2_t v132 = vrev64_f32(v110);
  float32x2_t v140 = vrev64_f32(v125);
  float32x2_t v248 = vmul_f32(v243, v1369);
  float32x2_t v258 = vmul_f32(v244, v1379);
  float32x2_t v270 = vadd_f32(v158, v243);
  float32x2_t v276 = vrev64_f32(v254);
  float32x2_t v284 = vrev64_f32(v269);
  float32x2_t v392 = vmul_f32(v387, v1369);
  float32x2_t v402 = vmul_f32(v388, v1379);
  float32x2_t v414 = vadd_f32(v302, v387);
  float32x2_t v420 = vrev64_f32(v398);
  float32x2_t v428 = vrev64_f32(v413);
  float32x2_t v536 = vmul_f32(v531, v1369);
  float32x2_t v546 = vmul_f32(v532, v1379);
  float32x2_t v558 = vadd_f32(v446, v531);
  float32x2_t v564 = vrev64_f32(v542);
  float32x2_t v572 = vrev64_f32(v557);
  float32x2_t v680 = vmul_f32(v675, v1369);
  float32x2_t v690 = vmul_f32(v676, v1379);
  float32x2_t v702 = vadd_f32(v590, v675);
  float32x2_t v708 = vrev64_f32(v686);
  float32x2_t v716 = vrev64_f32(v701);
  float32x2_t v105 = vsub_f32(v14, v104);
  float32x2_t v133 = vmul_f32(v132, v1417);
  float32x2_t v141 = vmul_f32(v140, v1417);
  float32x2_t v249 = vsub_f32(v158, v248);
  float32x2_t v277 = vmul_f32(v276, v1417);
  float32x2_t v285 = vmul_f32(v284, v1417);
  float32x2_t v393 = vsub_f32(v302, v392);
  float32x2_t v421 = vmul_f32(v420, v1417);
  float32x2_t v429 = vmul_f32(v428, v1417);
  float32x2_t v537 = vsub_f32(v446, v536);
  float32x2_t v565 = vmul_f32(v564, v1417);
  float32x2_t v573 = vmul_f32(v572, v1417);
  float32x2_t v681 = vsub_f32(v590, v680);
  float32x2_t v709 = vmul_f32(v708, v1417);
  float32x2_t v717 = vmul_f32(v716, v1417);
  float32x2_t v765 = vsub_f32(v270, v702);
  float32x2_t v769 = vmul_f32(v270, v1440);
  float32x2_t v783 = vsub_f32(v414, v558);
  float32x2_t v787 = vmul_f32(v414, v1440);
  float32x2_t v115 = vsub_f32(v105, v114);
  float32x2_t v119 = vmul_f32(v105, v1440);
  float32x2_t v259 = vsub_f32(v249, v258);
  float32x2_t v263 = vmul_f32(v249, v1440);
  float32x2_t v403 = vsub_f32(v393, v402);
  float32x2_t v407 = vmul_f32(v393, v1440);
  float32x2_t v547 = vsub_f32(v537, v546);
  float32x2_t v551 = vmul_f32(v537, v1440);
  float32x2_t v691 = vsub_f32(v681, v690);
  float32x2_t v695 = vmul_f32(v681, v1440);
  float32x2_t v770 = vsub_f32(v769, v765);
  float32x2_t v788 = vsub_f32(v787, v783);
  float32x2_t v799 = vmul_f32(v783, v1389);
  float32x2_t v814 = vmul_f32(v765, v1389);
  float32x2_t v120 = vsub_f32(v119, v115);
  float32x2_t v142 = vsub_f32(v115, v141);
  float32x2_t v146 = vmul_f32(v115, v1440);
  float32x2_t v264 = vsub_f32(v263, v259);
  float32x2_t v286 = vsub_f32(v259, v285);
  float32x2_t v290 = vmul_f32(v259, v1440);
  float32x2_t v408 = vsub_f32(v407, v403);
  float32x2_t v430 = vsub_f32(v403, v429);
  float32x2_t v434 = vmul_f32(v403, v1440);
  float32x2_t v552 = vsub_f32(v551, v547);
  float32x2_t v574 = vsub_f32(v547, v573);
  float32x2_t v578 = vmul_f32(v547, v1440);
  float32x2_t v696 = vsub_f32(v695, v691);
  float32x2_t v718 = vsub_f32(v691, v717);
  float32x2_t v722 = vmul_f32(v691, v1440);
  float32x2_t v789 = vadd_f32(v770, v788);
  float32x2_t v790 = vsub_f32(v770, v788);
  float32x2_t v800 = vadd_f32(v765, v799);
  float32x2_t v815 = vsub_f32(v814, v783);
  float32x2_t v134 = vsub_f32(v120, v133);
  float32x2_t v147 = vsub_f32(v146, v142);
  float32x2_t v151 = vmul_f32(v120, v1440);
  float32x2_t v278 = vsub_f32(v264, v277);
  float32x2_t v291 = vsub_f32(v290, v286);
  float32x2_t v295 = vmul_f32(v264, v1440);
  float32x2_t v422 = vsub_f32(v408, v421);
  float32x2_t v435 = vsub_f32(v434, v430);
  float32x2_t v439 = vmul_f32(v408, v1440);
  float32x2_t v566 = vsub_f32(v552, v565);
  float32x2_t v579 = vsub_f32(v578, v574);
  float32x2_t v583 = vmul_f32(v552, v1440);
  float32x2_t v710 = vsub_f32(v696, v709);
  float32x2_t v723 = vsub_f32(v722, v718);
  float32x2_t v727 = vmul_f32(v696, v1440);
  float32x2_t v794 = vmul_f32(v789, v1369);
  float32x2_t v804 = vmul_f32(v790, v1379);
  float32x2_t v816 = vadd_f32(v126, v789);
  float32x2_t v828 = vrev64_f32(v800);
  float32x2_t v842 = vrev64_f32(v815);
  float32x2_t v1026 = vrev64_f32(v286);
  float32x2_t v1038 = vrev64_f32(v430);
  float32x2_t v1050 = vrev64_f32(v718);
  float32x2_t v1068 = vrev64_f32(v574);
  float32x2_t v152 = vsub_f32(v151, v134);
  float32x2_t v296 = vsub_f32(v295, v278);
  float32x2_t v440 = vsub_f32(v439, v422);
  float32x2_t v584 = vsub_f32(v583, v566);
  float32x2_t v728 = vsub_f32(v727, v710);
  float32x2_t v795 = vsub_f32(v126, v794);
  int16x4_t v819 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v816, 15), (int32x2_t){0, 0}));
  float32x2_t v829 = vmul_f32(v828, v1417);
  float32x2_t v843 = vmul_f32(v842, v1417);
  float32x2_t v882 = vrev64_f32(v278);
  float32x2_t v894 = vrev64_f32(v422);
  float32x2_t v906 = vrev64_f32(v710);
  float32x2_t v924 = vrev64_f32(v566);
  float32x2_t v1027 = vmul_f32(v1026, v1025);
  float32x2_t v1039 = vmul_f32(v1038, v1313);
  float32x2_t v1051 = vmul_f32(v1050, v1325);
  float32x2_t v1069 = vmul_f32(v1068, v1181);
  float32x2_t v1170 = vrev64_f32(v291);
  float32x2_t v1182 = vrev64_f32(v435);
  float32x2_t v1194 = vrev64_f32(v723);
  float32x2_t v1212 = vrev64_f32(v579);
  float32x2_t v805 = vsub_f32(v795, v804);
  float32x2_t v809 = vmul_f32(v795, v1440);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v819), 0);
  float32x2_t v883 = vmul_f32(v882, v881);
  float32x2_t v895 = vmul_f32(v894, v1025);
  float32x2_t v907 = vmul_f32(v906, v1313);
  float32x2_t v925 = vmul_f32(v924, v1169);
  float32x2_t v1028 = vfma_f32(v1027, v286, v1019);
  float32x2_t v1040 = vfma_f32(v1039, v430, v1307);
  float32x2_t v1052 = vfma_f32(v1051, v718, v1319);
  float32x2_t v1070 = vfma_f32(v1069, v574, v1175);
  float32x2_t v1171 = vmul_f32(v1170, v1169);
  float32x2_t v1183 = vmul_f32(v1182, v1181);
  float32x2_t v1195 = vmul_f32(v1194, v1355);
  float32x2_t v1213 = vmul_f32(v1212, v1211);
  float32x2_t v1314 = vrev64_f32(v296);
  float32x2_t v1326 = vrev64_f32(v440);
  float32x2_t v1338 = vrev64_f32(v728);
  float32x2_t v1356 = vrev64_f32(v584);
  float32x2_t v810 = vsub_f32(v809, v805);
  float32x2_t v844 = vsub_f32(v805, v843);
  float32x2_t v854 = vmul_f32(v805, v1440);
  float32x2_t v884 = vfma_f32(v883, v278, v875);
  float32x2_t v896 = vfma_f32(v895, v422, v1019);
  float32x2_t v908 = vfma_f32(v907, v710, v1307);
  float32x2_t v926 = vfma_f32(v925, v566, v1163);
  float32x2_t v1053 = vsub_f32(v1028, v1052);
  float32x2_t v1057 = vmul_f32(v1028, v1440);
  float32x2_t v1071 = vsub_f32(v1040, v1070);
  float32x2_t v1075 = vmul_f32(v1040, v1440);
  float32x2_t v1172 = vfma_f32(v1171, v291, v1163);
  float32x2_t v1184 = vfma_f32(v1183, v435, v1175);
  float32x2_t v1196 = vfma_f32(v1195, v723, v1349);
  float32x2_t v1214 = vfma_f32(v1213, v579, v1331);
  float32x2_t v1315 = vmul_f32(v1314, v1313);
  float32x2_t v1327 = vmul_f32(v1326, v1325);
  float32x2_t v1339 = vmul_f32(v1338, v1337);
  float32x2_t v1357 = vmul_f32(v1356, v1355);
  float32x2_t v830 = vsub_f32(v810, v829);
  int16x4_t v847 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v844, 15), (int32x2_t){0, 0}));
  float32x2_t v855 = vsub_f32(v854, v844);
  float32x2_t v865 = vmul_f32(v810, v1440);
  float32x2_t v909 = vsub_f32(v884, v908);
  float32x2_t v913 = vmul_f32(v884, v1440);
  float32x2_t v927 = vsub_f32(v896, v926);
  float32x2_t v931 = vmul_f32(v896, v1440);
  float32x2_t v1058 = vsub_f32(v1057, v1053);
  float32x2_t v1076 = vsub_f32(v1075, v1071);
  float32x2_t v1087 = vmul_f32(v1071, v1389);
  float32x2_t v1102 = vmul_f32(v1053, v1389);
  float32x2_t v1197 = vsub_f32(v1172, v1196);
  float32x2_t v1201 = vmul_f32(v1172, v1440);
  float32x2_t v1215 = vsub_f32(v1184, v1214);
  float32x2_t v1219 = vmul_f32(v1184, v1440);
  float32x2_t v1316 = vfma_f32(v1315, v296, v1307);
  float32x2_t v1328 = vfma_f32(v1327, v440, v1319);
  float32x2_t v1340 = vfma_f32(v1339, v728, v1331);
  float32x2_t v1358 = vfma_f32(v1357, v584, v1349);
  int16x4_t v833 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v830, 15), (int32x2_t){0, 0}));
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v847), 0);
  int16x4_t v858 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v855, 15), (int32x2_t){0, 0}));
  float32x2_t v866 = vsub_f32(v865, v830);
  float32x2_t v914 = vsub_f32(v913, v909);
  float32x2_t v932 = vsub_f32(v931, v927);
  float32x2_t v943 = vmul_f32(v927, v1389);
  float32x2_t v958 = vmul_f32(v909, v1389);
  float32x2_t v1077 = vadd_f32(v1058, v1076);
  float32x2_t v1078 = vsub_f32(v1058, v1076);
  float32x2_t v1088 = vadd_f32(v1053, v1087);
  float32x2_t v1103 = vsub_f32(v1102, v1071);
  float32x2_t v1202 = vsub_f32(v1201, v1197);
  float32x2_t v1220 = vsub_f32(v1219, v1215);
  float32x2_t v1231 = vmul_f32(v1215, v1389);
  float32x2_t v1246 = vmul_f32(v1197, v1389);
  float32x2_t v1341 = vsub_f32(v1316, v1340);
  float32x2_t v1345 = vmul_f32(v1316, v1440);
  float32x2_t v1359 = vsub_f32(v1328, v1358);
  float32x2_t v1363 = vmul_f32(v1328, v1440);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v833), 0);
  v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v858), 0);
  int16x4_t v869 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v866, 15), (int32x2_t){0, 0}));
  float32x2_t v933 = vadd_f32(v914, v932);
  float32x2_t v934 = vsub_f32(v914, v932);
  float32x2_t v944 = vadd_f32(v909, v943);
  float32x2_t v959 = vsub_f32(v958, v927);
  float32x2_t v1082 = vmul_f32(v1077, v1369);
  float32x2_t v1092 = vmul_f32(v1078, v1379);
  float32x2_t v1104 = vadd_f32(v142, v1077);
  float32x2_t v1116 = vrev64_f32(v1088);
  float32x2_t v1130 = vrev64_f32(v1103);
  float32x2_t v1221 = vadd_f32(v1202, v1220);
  float32x2_t v1222 = vsub_f32(v1202, v1220);
  float32x2_t v1232 = vadd_f32(v1197, v1231);
  float32x2_t v1247 = vsub_f32(v1246, v1215);
  float32x2_t v1346 = vsub_f32(v1345, v1341);
  float32x2_t v1364 = vsub_f32(v1363, v1359);
  float32x2_t v1375 = vmul_f32(v1359, v1389);
  float32x2_t v1390 = vmul_f32(v1341, v1389);
  v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v869), 0);
  float32x2_t v938 = vmul_f32(v933, v1369);
  float32x2_t v948 = vmul_f32(v934, v1379);
  float32x2_t v960 = vadd_f32(v134, v933);
  float32x2_t v972 = vrev64_f32(v944);
  float32x2_t v986 = vrev64_f32(v959);
  float32x2_t v1083 = vsub_f32(v142, v1082);
  int16x4_t v1107 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1104, 15), (int32x2_t){0, 0}));
  float32x2_t v1117 = vmul_f32(v1116, v1417);
  float32x2_t v1131 = vmul_f32(v1130, v1417);
  float32x2_t v1226 = vmul_f32(v1221, v1369);
  float32x2_t v1236 = vmul_f32(v1222, v1379);
  float32x2_t v1248 = vadd_f32(v147, v1221);
  float32x2_t v1260 = vrev64_f32(v1232);
  float32x2_t v1274 = vrev64_f32(v1247);
  float32x2_t v1365 = vadd_f32(v1346, v1364);
  float32x2_t v1366 = vsub_f32(v1346, v1364);
  float32x2_t v1376 = vadd_f32(v1341, v1375);
  float32x2_t v1391 = vsub_f32(v1390, v1359);
  float32x2_t v939 = vsub_f32(v134, v938);
  int16x4_t v963 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v960, 15), (int32x2_t){0, 0}));
  float32x2_t v973 = vmul_f32(v972, v1417);
  float32x2_t v987 = vmul_f32(v986, v1417);
  float32x2_t v1093 = vsub_f32(v1083, v1092);
  float32x2_t v1097 = vmul_f32(v1083, v1440);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v1107), 0);
  float32x2_t v1227 = vsub_f32(v147, v1226);
  int16x4_t v1251 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1248, 15), (int32x2_t){0, 0}));
  float32x2_t v1261 = vmul_f32(v1260, v1417);
  float32x2_t v1275 = vmul_f32(v1274, v1417);
  float32x2_t v1370 = vmul_f32(v1365, v1369);
  float32x2_t v1380 = vmul_f32(v1366, v1379);
  float32x2_t v1392 = vadd_f32(v152, v1365);
  float32x2_t v1404 = vrev64_f32(v1376);
  float32x2_t v1418 = vrev64_f32(v1391);
  float32x2_t v949 = vsub_f32(v939, v948);
  float32x2_t v953 = vmul_f32(v939, v1440);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v963), 0);
  float32x2_t v1098 = vsub_f32(v1097, v1093);
  float32x2_t v1132 = vsub_f32(v1093, v1131);
  float32x2_t v1142 = vmul_f32(v1093, v1440);
  float32x2_t v1237 = vsub_f32(v1227, v1236);
  float32x2_t v1241 = vmul_f32(v1227, v1440);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v1251), 0);
  float32x2_t v1371 = vsub_f32(v152, v1370);
  int16x4_t v1395 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1392, 15), (int32x2_t){0, 0}));
  float32x2_t v1405 = vmul_f32(v1404, v1417);
  float32x2_t v1419 = vmul_f32(v1418, v1417);
  float32x2_t v954 = vsub_f32(v953, v949);
  float32x2_t v988 = vsub_f32(v949, v987);
  float32x2_t v998 = vmul_f32(v949, v1440);
  float32x2_t v1118 = vsub_f32(v1098, v1117);
  int16x4_t v1135 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1132, 15), (int32x2_t){0, 0}));
  float32x2_t v1143 = vsub_f32(v1142, v1132);
  float32x2_t v1153 = vmul_f32(v1098, v1440);
  float32x2_t v1242 = vsub_f32(v1241, v1237);
  float32x2_t v1276 = vsub_f32(v1237, v1275);
  float32x2_t v1286 = vmul_f32(v1237, v1440);
  float32x2_t v1381 = vsub_f32(v1371, v1380);
  float32x2_t v1385 = vmul_f32(v1371, v1440);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v1395), 0);
  float32x2_t v974 = vsub_f32(v954, v973);
  int16x4_t v991 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v988, 15), (int32x2_t){0, 0}));
  float32x2_t v999 = vsub_f32(v998, v988);
  float32x2_t v1009 = vmul_f32(v954, v1440);
  int16x4_t v1121 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1118, 15), (int32x2_t){0, 0}));
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v1135), 0);
  int16x4_t v1146 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1143, 15), (int32x2_t){0, 0}));
  float32x2_t v1154 = vsub_f32(v1153, v1118);
  float32x2_t v1262 = vsub_f32(v1242, v1261);
  int16x4_t v1279 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1276, 15), (int32x2_t){0, 0}));
  float32x2_t v1287 = vsub_f32(v1286, v1276);
  float32x2_t v1297 = vmul_f32(v1242, v1440);
  float32x2_t v1386 = vsub_f32(v1385, v1381);
  float32x2_t v1420 = vsub_f32(v1381, v1419);
  float32x2_t v1430 = vmul_f32(v1381, v1440);
  int16x4_t v977 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v974, 15), (int32x2_t){0, 0}));
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v991), 0);
  int16x4_t v1002 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v999, 15), (int32x2_t){0, 0}));
  float32x2_t v1010 = vsub_f32(v1009, v974);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v1121), 0);
  v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v1146), 0);
  int16x4_t v1157 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1154, 15), (int32x2_t){0, 0}));
  int16x4_t v1265 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1262, 15), (int32x2_t){0, 0}));
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v1279), 0);
  int16x4_t v1290 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1287, 15), (int32x2_t){0, 0}));
  float32x2_t v1298 = vsub_f32(v1297, v1262);
  float32x2_t v1406 = vsub_f32(v1386, v1405);
  int16x4_t v1423 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1420, 15), (int32x2_t){0, 0}));
  float32x2_t v1431 = vsub_f32(v1430, v1420);
  float32x2_t v1441 = vmul_f32(v1386, v1440);
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v977), 0);
  v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v1002), 0);
  int16x4_t v1013 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1010, 15), (int32x2_t){0, 0}));
  v6[ostride * 22] = vget_lane_s32(vreinterpret_s32_s16(v1157), 0);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v1265), 0);
  v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v1290), 0);
  int16x4_t v1301 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1298, 15), (int32x2_t){0, 0}));
  int16x4_t v1409 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1406, 15), (int32x2_t){0, 0}));
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v1423), 0);
  int16x4_t v1434 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1431, 15), (int32x2_t){0, 0}));
  float32x2_t v1442 = vsub_f32(v1441, v1406);
  v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v1013), 0);
  v6[ostride * 23] = vget_lane_s32(vreinterpret_s32_s16(v1301), 0);
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v1409), 0);
  v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v1434), 0);
  int16x4_t v1445 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1442, 15), (int32x2_t){0, 0}));
  v6[ostride * 24] = vget_lane_s32(vreinterpret_s32_s16(v1445), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun25(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v1017 = 9.6858316112863108e-01F;
  float v1022 = 2.4868988716485479e-01F;
  float v1184 = 8.7630668004386358e-01F;
  float v1189 = 4.8175367410171532e-01F;
  float v1351 = 7.2896862742141155e-01F;
  float v1356 = 6.8454710592868862e-01F;
  float v1364 = 6.2790519529313527e-02F;
  float v1369 = 9.9802672842827156e-01F;
  float v1402 = 7.7051324277578925e-01F;
  float v1518 = 5.3582679497899655e-01F;
  float v1523 = 8.4432792550201508e-01F;
  float v1531 = -4.2577929156507272e-01F;
  float v1536 = 9.0482705246601947e-01F;
  float v1544 = -6.3742398974868952e-01F;
  float v1549 = -7.7051324277578936e-01F;
  float v1564 = -9.9211470131447776e-01F;
  float v1569 = 1.2533323356430454e-01F;
  float v1586 = 2.5000000000000000e-01F;
  float v1598 = 5.5901699437494745e-01F;
  float v1610 = 6.1803398874989490e-01F;
  float v1641 = -9.5105651629515353e-01F;
  float v1671 = 2.0000000000000000e+00F;
  const int32_t *v1755 = &v5[v0];
  int32_t *v2091 = &v6[v2];
  int64_t v23 = v0 * 5;
  int64_t v31 = v0 * 10;
  int64_t v39 = v0 * 15;
  int64_t v47 = v0 * 20;
  int64_t v190 = v0 * 6;
  int64_t v198 = v0 * 11;
  int64_t v206 = v0 * 16;
  int64_t v214 = v0 * 21;
  int64_t v349 = v0 * 2;
  int64_t v357 = v0 * 7;
  int64_t v365 = v0 * 12;
  int64_t v373 = v0 * 17;
  int64_t v381 = v0 * 22;
  int64_t v516 = v0 * 3;
  int64_t v524 = v0 * 8;
  int64_t v532 = v0 * 13;
  int64_t v540 = v0 * 18;
  int64_t v548 = v0 * 23;
  int64_t v683 = v0 * 4;
  int64_t v691 = v0 * 9;
  int64_t v699 = v0 * 14;
  int64_t v707 = v0 * 19;
  int64_t v715 = v0 * 24;
  int64_t v965 = v2 * 5;
  int64_t v981 = v2 * 10;
  int64_t v995 = v2 * 15;
  int64_t v1009 = v2 * 20;
  float v1025 = v4 * v1022;
  int64_t v1132 = v2 * 6;
  int64_t v1148 = v2 * 11;
  int64_t v1162 = v2 * 16;
  int64_t v1176 = v2 * 21;
  float v1192 = v4 * v1189;
  int64_t v1283 = v2 * 2;
  int64_t v1299 = v2 * 7;
  int64_t v1315 = v2 * 12;
  int64_t v1329 = v2 * 17;
  int64_t v1343 = v2 * 22;
  float v1359 = v4 * v1356;
  float v1372 = v4 * v1369;
  float v1405 = v4 * v1402;
  int64_t v1450 = v2 * 3;
  int64_t v1466 = v2 * 8;
  int64_t v1482 = v2 * 13;
  int64_t v1496 = v2 * 18;
  int64_t v1510 = v2 * 23;
  float v1526 = v4 * v1523;
  float v1539 = v4 * v1536;
  float v1552 = v4 * v1549;
  float v1572 = v4 * v1569;
  int64_t v1617 = v2 * 4;
  int64_t v1633 = v2 * 9;
  float v1644 = v4 * v1641;
  int64_t v1649 = v2 * 14;
  int64_t v1663 = v2 * 19;
  int64_t v1677 = v2 * 24;
  const int32_t *v1691 = &v5[0];
  svfloat32_t v2013 = svdup_n_f32(0);
  int32_t *v2027 = &v6[0];
  svfloat32_t v2070 = svdup_n_f32(v1017);
  svfloat32_t v2134 = svdup_n_f32(v1184);
  svfloat32_t v2198 = svdup_n_f32(v1351);
  svfloat32_t v2200 = svdup_n_f32(v1364);
  svfloat32_t v2262 = svdup_n_f32(v1518);
  svfloat32_t v2264 = svdup_n_f32(v1531);
  svfloat32_t v2266 = svdup_n_f32(v1544);
  svfloat32_t v2269 = svdup_n_f32(v1564);
  svfloat32_t v2272 = svdup_n_f32(v1586);
  svfloat32_t v2274 = svdup_n_f32(v1598);
  svfloat32_t v2276 = svdup_n_f32(v1610);
  svfloat32_t v2316 = svdup_n_f32(v1671);
  svfloat32_t v188 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1755[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v1700 = &v5[v23];
  const int32_t *v1709 = &v5[v31];
  const int32_t *v1718 = &v5[v39];
  const int32_t *v1727 = &v5[v47];
  const int32_t *v1764 = &v5[v190];
  const int32_t *v1773 = &v5[v198];
  const int32_t *v1782 = &v5[v206];
  const int32_t *v1791 = &v5[v214];
  const int32_t *v1819 = &v5[v349];
  const int32_t *v1828 = &v5[v357];
  const int32_t *v1837 = &v5[v365];
  const int32_t *v1846 = &v5[v373];
  const int32_t *v1855 = &v5[v381];
  const int32_t *v1883 = &v5[v516];
  const int32_t *v1892 = &v5[v524];
  const int32_t *v1901 = &v5[v532];
  const int32_t *v1910 = &v5[v540];
  const int32_t *v1919 = &v5[v548];
  const int32_t *v1947 = &v5[v683];
  const int32_t *v1956 = &v5[v691];
  const int32_t *v1965 = &v5[v699];
  const int32_t *v1974 = &v5[v707];
  const int32_t *v1983 = &v5[v715];
  int32_t *v2037 = &v6[v965];
  int32_t *v2047 = &v6[v981];
  int32_t *v2057 = &v6[v995];
  int32_t *v2067 = &v6[v1009];
  svfloat32_t v2071 = svdup_n_f32(v1025);
  int32_t *v2101 = &v6[v1132];
  int32_t *v2111 = &v6[v1148];
  int32_t *v2121 = &v6[v1162];
  int32_t *v2131 = &v6[v1176];
  svfloat32_t v2135 = svdup_n_f32(v1192);
  int32_t *v2155 = &v6[v1283];
  int32_t *v2165 = &v6[v1299];
  int32_t *v2175 = &v6[v1315];
  int32_t *v2185 = &v6[v1329];
  int32_t *v2195 = &v6[v1343];
  svfloat32_t v2199 = svdup_n_f32(v1359);
  svfloat32_t v2201 = svdup_n_f32(v1372);
  svfloat32_t v2206 = svdup_n_f32(v1405);
  int32_t *v2219 = &v6[v1450];
  int32_t *v2229 = &v6[v1466];
  int32_t *v2239 = &v6[v1482];
  int32_t *v2249 = &v6[v1496];
  int32_t *v2259 = &v6[v1510];
  svfloat32_t v2263 = svdup_n_f32(v1526);
  svfloat32_t v2265 = svdup_n_f32(v1539);
  svfloat32_t v2267 = svdup_n_f32(v1552);
  svfloat32_t v2270 = svdup_n_f32(v1572);
  int32_t *v2283 = &v6[v1617];
  int32_t *v2293 = &v6[v1633];
  svfloat32_t v2296 = svdup_n_f32(v1644);
  int32_t *v2303 = &v6[v1649];
  int32_t *v2313 = &v6[v1663];
  int32_t *v2323 = &v6[v1677];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1691[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1700[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v37 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1709[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v45 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1718[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v53 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1727[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v196 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1764[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v204 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1773[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v212 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1782[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v220 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1791[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v355 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1819[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v363 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1828[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v371 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1837[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v379 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1846[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v387 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1855[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v522 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1883[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v530 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1892[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v538 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1901[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v546 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1910[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v554 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1919[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v689 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1947[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v697 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1956[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v705 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1965[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v713 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1974[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v721 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1983[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v66 = svcmla_f32_x(pred_full, v29, v2013, v29, 90);
  svfloat32_t v79 = svcmla_f32_x(pred_full, v37, v2013, v37, 90);
  svfloat32_t v92 = svcmla_f32_x(pred_full, v53, v2013, v53, 90);
  svfloat32_t v112 = svcmla_f32_x(pred_full, v45, v2013, v45, 90);
  svfloat32_t v233 = svcmla_f32_x(pred_full, v196, v2013, v196, 90);
  svfloat32_t v246 = svcmla_f32_x(pred_full, v204, v2013, v204, 90);
  svfloat32_t v259 = svcmla_f32_x(pred_full, v220, v2013, v220, 90);
  svfloat32_t v279 = svcmla_f32_x(pred_full, v212, v2013, v212, 90);
  svfloat32_t v400 = svcmla_f32_x(pred_full, v363, v2013, v363, 90);
  svfloat32_t v413 = svcmla_f32_x(pred_full, v371, v2013, v371, 90);
  svfloat32_t v426 = svcmla_f32_x(pred_full, v387, v2013, v387, 90);
  svfloat32_t v446 = svcmla_f32_x(pred_full, v379, v2013, v379, 90);
  svfloat32_t v567 = svcmla_f32_x(pred_full, v530, v2013, v530, 90);
  svfloat32_t v580 = svcmla_f32_x(pred_full, v538, v2013, v538, 90);
  svfloat32_t v593 = svcmla_f32_x(pred_full, v554, v2013, v554, 90);
  svfloat32_t v613 = svcmla_f32_x(pred_full, v546, v2013, v546, 90);
  svfloat32_t v734 = svcmla_f32_x(pred_full, v697, v2013, v697, 90);
  svfloat32_t v747 = svcmla_f32_x(pred_full, v705, v2013, v705, 90);
  svfloat32_t v760 = svcmla_f32_x(pred_full, v721, v2013, v721, 90);
  svfloat32_t v780 = svcmla_f32_x(pred_full, v713, v2013, v713, 90);
  svfloat32_t v93 = svsub_f32_x(svptrue_b32(), v66, v92);
  svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v79, v112);
  svfloat32_t v260 = svsub_f32_x(svptrue_b32(), v233, v259);
  svfloat32_t v280 = svsub_f32_x(svptrue_b32(), v246, v279);
  svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v400, v426);
  svfloat32_t v447 = svsub_f32_x(svptrue_b32(), v413, v446);
  svfloat32_t v594 = svsub_f32_x(svptrue_b32(), v567, v593);
  svfloat32_t v614 = svsub_f32_x(svptrue_b32(), v580, v613);
  svfloat32_t v761 = svsub_f32_x(svptrue_b32(), v734, v760);
  svfloat32_t v781 = svsub_f32_x(svptrue_b32(), v747, v780);
  svfloat32_t v99 = svnmls_f32_x(pred_full, v93, v66, v2316);
  svfloat32_t v119 = svnmls_f32_x(pred_full, v113, v79, v2316);
  svfloat32_t v266 = svnmls_f32_x(pred_full, v260, v233, v2316);
  svfloat32_t v286 = svnmls_f32_x(pred_full, v280, v246, v2316);
  svfloat32_t v433 = svnmls_f32_x(pred_full, v427, v400, v2316);
  svfloat32_t v453 = svnmls_f32_x(pred_full, v447, v413, v2316);
  svfloat32_t v600 = svnmls_f32_x(pred_full, v594, v567, v2316);
  svfloat32_t v620 = svnmls_f32_x(pred_full, v614, v580, v2316);
  svfloat32_t v767 = svnmls_f32_x(pred_full, v761, v734, v2316);
  svfloat32_t v787 = svnmls_f32_x(pred_full, v781, v747, v2316);
  svfloat32_t v120 = svadd_f32_x(svptrue_b32(), v99, v119);
  svfloat32_t v121 = svsub_f32_x(svptrue_b32(), v99, v119);
  svfloat32_t v133 = svmla_f32_x(pred_full, v93, v113, v2276);
  svfloat32_t v151 = svnmls_f32_x(pred_full, v113, v93, v2276);
  svfloat32_t v287 = svadd_f32_x(svptrue_b32(), v266, v286);
  svfloat32_t v288 = svsub_f32_x(svptrue_b32(), v266, v286);
  svfloat32_t v300 = svmla_f32_x(pred_full, v260, v280, v2276);
  svfloat32_t v318 = svnmls_f32_x(pred_full, v280, v260, v2276);
  svfloat32_t v454 = svadd_f32_x(svptrue_b32(), v433, v453);
  svfloat32_t v455 = svsub_f32_x(svptrue_b32(), v433, v453);
  svfloat32_t v467 = svmla_f32_x(pred_full, v427, v447, v2276);
  svfloat32_t v485 = svnmls_f32_x(pred_full, v447, v427, v2276);
  svfloat32_t v621 = svadd_f32_x(svptrue_b32(), v600, v620);
  svfloat32_t v622 = svsub_f32_x(svptrue_b32(), v600, v620);
  svfloat32_t v634 = svmla_f32_x(pred_full, v594, v614, v2276);
  svfloat32_t v652 = svnmls_f32_x(pred_full, v614, v594, v2276);
  svfloat32_t v788 = svadd_f32_x(svptrue_b32(), v767, v787);
  svfloat32_t v789 = svsub_f32_x(svptrue_b32(), v767, v787);
  svfloat32_t v801 = svmla_f32_x(pred_full, v761, v781, v2276);
  svfloat32_t v819 = svnmls_f32_x(pred_full, v781, v761, v2276);
  svfloat32_t v152 = svadd_f32_x(svptrue_b32(), v21, v120);
  svfloat32_t zero159 = svdup_n_f32(0);
  svfloat32_t v159 = svcmla_f32_x(pred_full, zero159, v2296, v133, 90);
  svfloat32_t zero167 = svdup_n_f32(0);
  svfloat32_t v167 = svcmla_f32_x(pred_full, zero167, v2296, v151, 90);
  svfloat32_t v319 = svadd_f32_x(svptrue_b32(), v188, v287);
  svfloat32_t zero326 = svdup_n_f32(0);
  svfloat32_t v326 = svcmla_f32_x(pred_full, zero326, v2296, v300, 90);
  svfloat32_t zero334 = svdup_n_f32(0);
  svfloat32_t v334 = svcmla_f32_x(pred_full, zero334, v2296, v318, 90);
  svfloat32_t v486 = svadd_f32_x(svptrue_b32(), v355, v454);
  svfloat32_t zero493 = svdup_n_f32(0);
  svfloat32_t v493 = svcmla_f32_x(pred_full, zero493, v2296, v467, 90);
  svfloat32_t zero501 = svdup_n_f32(0);
  svfloat32_t v501 = svcmla_f32_x(pred_full, zero501, v2296, v485, 90);
  svfloat32_t v653 = svadd_f32_x(svptrue_b32(), v522, v621);
  svfloat32_t zero660 = svdup_n_f32(0);
  svfloat32_t v660 = svcmla_f32_x(pred_full, zero660, v2296, v634, 90);
  svfloat32_t zero668 = svdup_n_f32(0);
  svfloat32_t v668 = svcmla_f32_x(pred_full, zero668, v2296, v652, 90);
  svfloat32_t v820 = svadd_f32_x(svptrue_b32(), v689, v788);
  svfloat32_t zero827 = svdup_n_f32(0);
  svfloat32_t v827 = svcmla_f32_x(pred_full, zero827, v2296, v801, 90);
  svfloat32_t zero835 = svdup_n_f32(0);
  svfloat32_t v835 = svcmla_f32_x(pred_full, zero835, v2296, v819, 90);
  svfloat32_t v127 = svmls_f32_x(pred_full, v21, v120, v2272);
  svfloat32_t v294 = svmls_f32_x(pred_full, v188, v287, v2272);
  svfloat32_t v461 = svmls_f32_x(pred_full, v355, v454, v2272);
  svfloat32_t v628 = svmls_f32_x(pred_full, v522, v621, v2272);
  svfloat32_t v795 = svmls_f32_x(pred_full, v689, v788, v2272);
  svfloat32_t v139 = svmls_f32_x(pred_full, v127, v121, v2274);
  svfloat32_t v306 = svmls_f32_x(pred_full, v294, v288, v2274);
  svfloat32_t v473 = svmls_f32_x(pred_full, v461, v455, v2274);
  svfloat32_t v640 = svmls_f32_x(pred_full, v628, v622, v2274);
  svfloat32_t v807 = svmls_f32_x(pred_full, v795, v789, v2274);
  svfloat32_t v861 = svcmla_f32_x(pred_full, v319, v2013, v319, 90);
  svfloat32_t v874 = svcmla_f32_x(pred_full, v486, v2013, v486, 90);
  svfloat32_t v887 = svcmla_f32_x(pred_full, v820, v2013, v820, 90);
  svfloat32_t v907 = svcmla_f32_x(pred_full, v653, v2013, v653, 90);
  svfloat32_t v145 = svnmls_f32_x(pred_full, v139, v127, v2316);
  svfloat32_t v168 = svsub_f32_x(svptrue_b32(), v139, v167);
  svfloat32_t v312 = svnmls_f32_x(pred_full, v306, v294, v2316);
  svfloat32_t v335 = svsub_f32_x(svptrue_b32(), v306, v334);
  svfloat32_t v479 = svnmls_f32_x(pred_full, v473, v461, v2316);
  svfloat32_t v502 = svsub_f32_x(svptrue_b32(), v473, v501);
  svfloat32_t v646 = svnmls_f32_x(pred_full, v640, v628, v2316);
  svfloat32_t v669 = svsub_f32_x(svptrue_b32(), v640, v668);
  svfloat32_t v813 = svnmls_f32_x(pred_full, v807, v795, v2316);
  svfloat32_t v836 = svsub_f32_x(svptrue_b32(), v807, v835);
  svfloat32_t v888 = svsub_f32_x(svptrue_b32(), v861, v887);
  svfloat32_t v908 = svsub_f32_x(svptrue_b32(), v874, v907);
  svfloat32_t v160 = svsub_f32_x(svptrue_b32(), v145, v159);
  svfloat32_t v174 = svnmls_f32_x(pred_full, v168, v139, v2316);
  svfloat32_t v327 = svsub_f32_x(svptrue_b32(), v312, v326);
  svfloat32_t v341 = svnmls_f32_x(pred_full, v335, v306, v2316);
  svfloat32_t v494 = svsub_f32_x(svptrue_b32(), v479, v493);
  svfloat32_t v508 = svnmls_f32_x(pred_full, v502, v473, v2316);
  svfloat32_t v661 = svsub_f32_x(svptrue_b32(), v646, v660);
  svfloat32_t v675 = svnmls_f32_x(pred_full, v669, v640, v2316);
  svfloat32_t v828 = svsub_f32_x(svptrue_b32(), v813, v827);
  svfloat32_t v842 = svnmls_f32_x(pred_full, v836, v807, v2316);
  svfloat32_t v894 = svnmls_f32_x(pred_full, v888, v861, v2316);
  svfloat32_t v914 = svnmls_f32_x(pred_full, v908, v874, v2316);
  svfloat32_t v1187 = svmul_f32_x(svptrue_b32(), v335, v2134);
  svfloat32_t v1200 = svmul_f32_x(svptrue_b32(), v502, v2262);
  svfloat32_t v1213 = svmul_f32_x(svptrue_b32(), v836, v2264);
  svfloat32_t v1233 = svmul_f32_x(svptrue_b32(), v669, v2200);
  svfloat32_t v180 = svnmls_f32_x(pred_full, v160, v145, v2316);
  svfloat32_t v347 = svnmls_f32_x(pred_full, v327, v312, v2316);
  svfloat32_t v514 = svnmls_f32_x(pred_full, v494, v479, v2316);
  svfloat32_t v681 = svnmls_f32_x(pred_full, v661, v646, v2316);
  svfloat32_t v848 = svnmls_f32_x(pred_full, v828, v813, v2316);
  svfloat32_t v915 = svadd_f32_x(svptrue_b32(), v894, v914);
  svfloat32_t v916 = svsub_f32_x(svptrue_b32(), v894, v914);
  svfloat32_t v928 = svmla_f32_x(pred_full, v888, v908, v2276);
  svfloat32_t v946 = svnmls_f32_x(pred_full, v908, v888, v2276);
  svfloat32_t v1020 = svmul_f32_x(svptrue_b32(), v327, v2070);
  svfloat32_t v1033 = svmul_f32_x(svptrue_b32(), v494, v2134);
  svfloat32_t v1046 = svmul_f32_x(svptrue_b32(), v828, v2262);
  svfloat32_t v1066 = svmul_f32_x(svptrue_b32(), v661, v2198);
  svfloat32_t v1195 = svcmla_f32_x(pred_full, v1187, v2135, v335, 90);
  svfloat32_t v1208 = svcmla_f32_x(pred_full, v1200, v2263, v502, 90);
  svfloat32_t v1221 = svcmla_f32_x(pred_full, v1213, v2265, v836, 90);
  svfloat32_t v1241 = svcmla_f32_x(pred_full, v1233, v2201, v669, 90);
  svfloat32_t v1354 = svmul_f32_x(svptrue_b32(), v341, v2198);
  svfloat32_t v1367 = svmul_f32_x(svptrue_b32(), v508, v2200);
  svfloat32_t v1380 = svmul_f32_x(svptrue_b32(), v842, v2269);
  svfloat32_t v1400 = svmul_f32_x(svptrue_b32(), v675, v2266);
  svfloat32_t v947 = svadd_f32_x(svptrue_b32(), v152, v915);
  svfloat32_t zero962 = svdup_n_f32(0);
  svfloat32_t v962 = svcmla_f32_x(pred_full, zero962, v2296, v928, 90);
  svfloat32_t zero978 = svdup_n_f32(0);
  svfloat32_t v978 = svcmla_f32_x(pred_full, zero978, v2296, v946, 90);
  svfloat32_t v1028 = svcmla_f32_x(pred_full, v1020, v2071, v327, 90);
  svfloat32_t v1041 = svcmla_f32_x(pred_full, v1033, v2135, v494, 90);
  svfloat32_t v1054 = svcmla_f32_x(pred_full, v1046, v2263, v828, 90);
  svfloat32_t v1074 = svcmla_f32_x(pred_full, v1066, v2199, v661, 90);
  svfloat32_t v1222 = svsub_f32_x(svptrue_b32(), v1195, v1221);
  svfloat32_t v1242 = svsub_f32_x(svptrue_b32(), v1208, v1241);
  svfloat32_t v1362 = svcmla_f32_x(pred_full, v1354, v2199, v341, 90);
  svfloat32_t v1375 = svcmla_f32_x(pred_full, v1367, v2201, v508, 90);
  svfloat32_t v1388 = svcmla_f32_x(pred_full, v1380, v2270, v842, 90);
  svfloat32_t v1408 = svcmla_f32_x(pred_full, v1400, v2206, v675, 90);
  svfloat32_t v1521 = svmul_f32_x(svptrue_b32(), v347, v2262);
  svfloat32_t v1534 = svmul_f32_x(svptrue_b32(), v514, v2264);
  svfloat32_t v1547 = svmul_f32_x(svptrue_b32(), v848, v2266);
  svfloat32_t v1567 = svmul_f32_x(svptrue_b32(), v681, v2269);
  svfloat32_t v922 = svmls_f32_x(pred_full, v152, v915, v2272);
  svint16_t v950 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v947, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1055 = svsub_f32_x(svptrue_b32(), v1028, v1054);
  svfloat32_t v1075 = svsub_f32_x(svptrue_b32(), v1041, v1074);
  svfloat32_t v1228 = svnmls_f32_x(pred_full, v1222, v1195, v2316);
  svfloat32_t v1248 = svnmls_f32_x(pred_full, v1242, v1208, v2316);
  svfloat32_t v1389 = svsub_f32_x(svptrue_b32(), v1362, v1388);
  svfloat32_t v1409 = svsub_f32_x(svptrue_b32(), v1375, v1408);
  svfloat32_t v1529 = svcmla_f32_x(pred_full, v1521, v2263, v347, 90);
  svfloat32_t v1542 = svcmla_f32_x(pred_full, v1534, v2265, v514, 90);
  svfloat32_t v1555 = svcmla_f32_x(pred_full, v1547, v2267, v848, 90);
  svfloat32_t v1575 = svcmla_f32_x(pred_full, v1567, v2270, v681, 90);
  svfloat32_t v934 = svmls_f32_x(pred_full, v922, v916, v2274);
  svfloat32_t v1061 = svnmls_f32_x(pred_full, v1055, v1028, v2316);
  svfloat32_t v1081 = svnmls_f32_x(pred_full, v1075, v1041, v2316);
  svfloat32_t v1249 = svadd_f32_x(svptrue_b32(), v1228, v1248);
  svfloat32_t v1250 = svsub_f32_x(svptrue_b32(), v1228, v1248);
  svfloat32_t v1262 = svmla_f32_x(pred_full, v1222, v1242, v2276);
  svfloat32_t v1280 = svnmls_f32_x(pred_full, v1242, v1222, v2276);
  svfloat32_t v1395 = svnmls_f32_x(pred_full, v1389, v1362, v2316);
  svfloat32_t v1415 = svnmls_f32_x(pred_full, v1409, v1375, v2316);
  svfloat32_t v1556 = svsub_f32_x(svptrue_b32(), v1529, v1555);
  svfloat32_t v1576 = svsub_f32_x(svptrue_b32(), v1542, v1575);
  svst1w_u64(pred_full, (unsigned *)(v2027), svreinterpret_u64_s16(v950));
  svfloat32_t v940 = svnmls_f32_x(pred_full, v934, v922, v2316);
  svfloat32_t v979 = svsub_f32_x(svptrue_b32(), v934, v978);
  svfloat32_t v1082 = svadd_f32_x(svptrue_b32(), v1061, v1081);
  svfloat32_t v1083 = svsub_f32_x(svptrue_b32(), v1061, v1081);
  svfloat32_t v1095 = svmla_f32_x(pred_full, v1055, v1075, v2276);
  svfloat32_t v1113 = svnmls_f32_x(pred_full, v1075, v1055, v2276);
  svfloat32_t v1281 = svadd_f32_x(svptrue_b32(), v168, v1249);
  svfloat32_t zero1296 = svdup_n_f32(0);
  svfloat32_t v1296 = svcmla_f32_x(pred_full, zero1296, v2296, v1262, 90);
  svfloat32_t zero1312 = svdup_n_f32(0);
  svfloat32_t v1312 = svcmla_f32_x(pred_full, zero1312, v2296, v1280, 90);
  svfloat32_t v1416 = svadd_f32_x(svptrue_b32(), v1395, v1415);
  svfloat32_t v1417 = svsub_f32_x(svptrue_b32(), v1395, v1415);
  svfloat32_t v1429 = svmla_f32_x(pred_full, v1389, v1409, v2276);
  svfloat32_t v1447 = svnmls_f32_x(pred_full, v1409, v1389, v2276);
  svfloat32_t v1562 = svnmls_f32_x(pred_full, v1556, v1529, v2316);
  svfloat32_t v1582 = svnmls_f32_x(pred_full, v1576, v1542, v2316);
  svfloat32_t v963 = svsub_f32_x(svptrue_b32(), v940, v962);
  svint16_t v982 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v979, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v993 = svnmls_f32_x(pred_full, v979, v934, v2316);
  svfloat32_t v1114 = svadd_f32_x(svptrue_b32(), v160, v1082);
  svfloat32_t zero1129 = svdup_n_f32(0);
  svfloat32_t v1129 = svcmla_f32_x(pred_full, zero1129, v2296, v1095, 90);
  svfloat32_t zero1145 = svdup_n_f32(0);
  svfloat32_t v1145 = svcmla_f32_x(pred_full, zero1145, v2296, v1113, 90);
  svfloat32_t v1256 = svmls_f32_x(pred_full, v168, v1249, v2272);
  svint16_t v1284 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1281, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1448 = svadd_f32_x(svptrue_b32(), v174, v1416);
  svfloat32_t zero1463 = svdup_n_f32(0);
  svfloat32_t v1463 = svcmla_f32_x(pred_full, zero1463, v2296, v1429, 90);
  svfloat32_t zero1479 = svdup_n_f32(0);
  svfloat32_t v1479 = svcmla_f32_x(pred_full, zero1479, v2296, v1447, 90);
  svfloat32_t v1583 = svadd_f32_x(svptrue_b32(), v1562, v1582);
  svfloat32_t v1584 = svsub_f32_x(svptrue_b32(), v1562, v1582);
  svfloat32_t v1596 = svmla_f32_x(pred_full, v1556, v1576, v2276);
  svfloat32_t v1614 = svnmls_f32_x(pred_full, v1576, v1556, v2276);
  svint16_t v966 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v963, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v996 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v993, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1007 = svnmls_f32_x(pred_full, v963, v940, v2316);
  svfloat32_t v1089 = svmls_f32_x(pred_full, v160, v1082, v2272);
  svint16_t v1117 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1114, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1268 = svmls_f32_x(pred_full, v1256, v1250, v2274);
  svfloat32_t v1423 = svmls_f32_x(pred_full, v174, v1416, v2272);
  svint16_t v1451 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1448, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1615 = svadd_f32_x(svptrue_b32(), v180, v1583);
  svfloat32_t zero1630 = svdup_n_f32(0);
  svfloat32_t v1630 = svcmla_f32_x(pred_full, zero1630, v2296, v1596, 90);
  svfloat32_t zero1646 = svdup_n_f32(0);
  svfloat32_t v1646 = svcmla_f32_x(pred_full, zero1646, v2296, v1614, 90);
  svst1w_u64(pred_full, (unsigned *)(v2047), svreinterpret_u64_s16(v982));
  svst1w_u64(pred_full, (unsigned *)(v2155), svreinterpret_u64_s16(v1284));
  svint16_t v1010 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1007, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1101 = svmls_f32_x(pred_full, v1089, v1083, v2274);
  svfloat32_t v1274 = svnmls_f32_x(pred_full, v1268, v1256, v2316);
  svfloat32_t v1313 = svsub_f32_x(svptrue_b32(), v1268, v1312);
  svfloat32_t v1435 = svmls_f32_x(pred_full, v1423, v1417, v2274);
  svfloat32_t v1590 = svmls_f32_x(pred_full, v180, v1583, v2272);
  svint16_t v1618 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1615, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v2037), svreinterpret_u64_s16(v966));
  svst1w_u64(pred_full, (unsigned *)(v2057), svreinterpret_u64_s16(v996));
  svst1w_u64(pred_full, (unsigned *)(v2091), svreinterpret_u64_s16(v1117));
  svst1w_u64(pred_full, (unsigned *)(v2219), svreinterpret_u64_s16(v1451));
  svfloat32_t v1107 = svnmls_f32_x(pred_full, v1101, v1089, v2316);
  svfloat32_t v1146 = svsub_f32_x(svptrue_b32(), v1101, v1145);
  svfloat32_t v1297 = svsub_f32_x(svptrue_b32(), v1274, v1296);
  svint16_t v1316 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1313, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1327 = svnmls_f32_x(pred_full, v1313, v1268, v2316);
  svfloat32_t v1441 = svnmls_f32_x(pred_full, v1435, v1423, v2316);
  svfloat32_t v1480 = svsub_f32_x(svptrue_b32(), v1435, v1479);
  svfloat32_t v1602 = svmls_f32_x(pred_full, v1590, v1584, v2274);
  svst1w_u64(pred_full, (unsigned *)(v2067), svreinterpret_u64_s16(v1010));
  svst1w_u64(pred_full, (unsigned *)(v2283), svreinterpret_u64_s16(v1618));
  svfloat32_t v1130 = svsub_f32_x(svptrue_b32(), v1107, v1129);
  svint16_t v1149 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1146, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1160 = svnmls_f32_x(pred_full, v1146, v1101, v2316);
  svint16_t v1300 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1297, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1330 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1327, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1341 = svnmls_f32_x(pred_full, v1297, v1274, v2316);
  svfloat32_t v1464 = svsub_f32_x(svptrue_b32(), v1441, v1463);
  svint16_t v1483 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1480, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1494 = svnmls_f32_x(pred_full, v1480, v1435, v2316);
  svfloat32_t v1608 = svnmls_f32_x(pred_full, v1602, v1590, v2316);
  svfloat32_t v1647 = svsub_f32_x(svptrue_b32(), v1602, v1646);
  svst1w_u64(pred_full, (unsigned *)(v2175), svreinterpret_u64_s16(v1316));
  svint16_t v1133 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1130, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1163 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1160, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1174 = svnmls_f32_x(pred_full, v1130, v1107, v2316);
  svint16_t v1344 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1341, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1467 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1464, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1497 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1494, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1508 = svnmls_f32_x(pred_full, v1464, v1441, v2316);
  svfloat32_t v1631 = svsub_f32_x(svptrue_b32(), v1608, v1630);
  svint16_t v1650 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1647, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1661 = svnmls_f32_x(pred_full, v1647, v1602, v2316);
  svst1w_u64(pred_full, (unsigned *)(v2111), svreinterpret_u64_s16(v1149));
  svst1w_u64(pred_full, (unsigned *)(v2165), svreinterpret_u64_s16(v1300));
  svst1w_u64(pred_full, (unsigned *)(v2185), svreinterpret_u64_s16(v1330));
  svst1w_u64(pred_full, (unsigned *)(v2239), svreinterpret_u64_s16(v1483));
  svint16_t v1177 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1174, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1511 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1508, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1634 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1631, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1664 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1661, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1675 = svnmls_f32_x(pred_full, v1631, v1608, v2316);
  svst1w_u64(pred_full, (unsigned *)(v2101), svreinterpret_u64_s16(v1133));
  svst1w_u64(pred_full, (unsigned *)(v2121), svreinterpret_u64_s16(v1163));
  svst1w_u64(pred_full, (unsigned *)(v2195), svreinterpret_u64_s16(v1344));
  svst1w_u64(pred_full, (unsigned *)(v2229), svreinterpret_u64_s16(v1467));
  svst1w_u64(pred_full, (unsigned *)(v2249), svreinterpret_u64_s16(v1497));
  svst1w_u64(pred_full, (unsigned *)(v2303), svreinterpret_u64_s16(v1650));
  svint16_t v1678 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1675, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v2131), svreinterpret_u64_s16(v1177));
  svst1w_u64(pred_full, (unsigned *)(v2259), svreinterpret_u64_s16(v1511));
  svst1w_u64(pred_full, (unsigned *)(v2293), svreinterpret_u64_s16(v1634));
  svst1w_u64(pred_full, (unsigned *)(v2313), svreinterpret_u64_s16(v1664));
  svst1w_u64(pred_full, (unsigned *)(v2323), svreinterpret_u64_s16(v1678));
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun32(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  float v783 = 7.0710678118654757e-01F;
  float v794 = -7.0710678118654746e-01F;
  float v844 = 5.5557023301960229e-01F;
  float v858 = -1.9509032201612861e-01F;
  float v909 = 9.2387953251128674e-01F;
  float v916 = -9.2387953251128685e-01F;
  float v919 = 3.8268343236508967e-01F;
  float v920 = -3.8268343236508967e-01F;
  float v966 = 1.9509032201612833e-01F;
  float v969 = -9.8078528040323043e-01F;
  float v970 = 9.8078528040323043e-01F;
  float v977 = -5.5557023301960218e-01F;
  float v980 = 8.3146961230254524e-01F;
  float v981 = -8.3146961230254524e-01F;
  float v991 = -1.0000000000000000e+00F;
  float v992 = 1.0000000000000000e+00F;
  int16x4_t v13 = vld1s_s16(&v5[0]);
  int16x4_t v332 = vld1s_s16(&v5[istride]);
  float32x2_t v601 = (float32x2_t){v970, v970};
  float32x2_t v662 = (float32x2_t){v909, v909};
  float32x2_t v666 = (float32x2_t){v920, v919};
  float32x2_t v723 = (float32x2_t){v980, v980};
  float32x2_t v727 = (float32x2_t){v977, v844};
  float32x2_t v734 = (float32x2_t){v858, v858};
  float32x2_t v784 = (float32x2_t){v783, v783};
  float32x2_t v795 = (float32x2_t){v794, v794};
  float32x2_t v799 = (float32x2_t){v992, v991};
  float32x2_t v845 = (float32x2_t){v844, v844};
  float32x2_t v849 = (float32x2_t){v981, v980};
  float32x2_t v856 = (float32x2_t){v969, v969};
  float32x2_t v860 = (float32x2_t){v858, v966};
  float32x2_t v906 = (float32x2_t){v919, v919};
  float32x2_t v910 = (float32x2_t){v916, v909};
  float32x2_t v917 = (float32x2_t){v916, v916};
  float32x2_t v921 = (float32x2_t){v919, v920};
  float32x2_t v967 = (float32x2_t){v966, v966};
  float32x2_t v971 = (float32x2_t){v969, v970};
  float32x2_t v978 = (float32x2_t){v977, v977};
  float32x2_t v982 = (float32x2_t){v980, v981};
  float32x2_t v993 = (float32x2_t){v991, v992};
  float32x2_t v994 = (float32x2_t){v4, v4};
  float32x2_t v14 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v13)), 15);
  int16x4_t v19 = vld1s_s16(&v5[istride * 16]);
  int16x4_t v27 = vld1s_s16(&v5[istride * 8]);
  int16x4_t v33 = vld1s_s16(&v5[istride * 24]);
  int16x4_t v52 = vld1s_s16(&v5[istride * 4]);
  int16x4_t v58 = vld1s_s16(&v5[istride * 20]);
  int16x4_t v66 = vld1s_s16(&v5[istride * 12]);
  int16x4_t v72 = vld1s_s16(&v5[istride * 28]);
  int16x4_t v130 = vld1s_s16(&v5[istride * 2]);
  int16x4_t v136 = vld1s_s16(&v5[istride * 18]);
  int16x4_t v144 = vld1s_s16(&v5[istride * 10]);
  int16x4_t v150 = vld1s_s16(&v5[istride * 26]);
  int16x4_t v169 = vld1s_s16(&v5[istride * 6]);
  int16x4_t v175 = vld1s_s16(&v5[istride * 22]);
  int16x4_t v183 = vld1s_s16(&v5[istride * 14]);
  int16x4_t v189 = vld1s_s16(&v5[istride * 30]);
  float32x2_t v333 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v332)), 15);
  int16x4_t v338 = vld1s_s16(&v5[istride * 17]);
  int16x4_t v346 = vld1s_s16(&v5[istride * 9]);
  int16x4_t v352 = vld1s_s16(&v5[istride * 25]);
  int16x4_t v371 = vld1s_s16(&v5[istride * 5]);
  int16x4_t v377 = vld1s_s16(&v5[istride * 21]);
  int16x4_t v385 = vld1s_s16(&v5[istride * 13]);
  int16x4_t v391 = vld1s_s16(&v5[istride * 29]);
  int16x4_t v449 = vld1s_s16(&v5[istride * 3]);
  int16x4_t v455 = vld1s_s16(&v5[istride * 19]);
  int16x4_t v463 = vld1s_s16(&v5[istride * 11]);
  int16x4_t v469 = vld1s_s16(&v5[istride * 27]);
  int16x4_t v488 = vld1s_s16(&v5[istride * 7]);
  int16x4_t v494 = vld1s_s16(&v5[istride * 23]);
  int16x4_t v502 = vld1s_s16(&v5[istride * 15]);
  int16x4_t v508 = vld1s_s16(&v5[istride * 31]);
  float32x2_t v668 = vmul_f32(v994, v666);
  float32x2_t v729 = vmul_f32(v994, v727);
  float32x2_t v801 = vmul_f32(v994, v799);
  float32x2_t v851 = vmul_f32(v994, v849);
  float32x2_t v862 = vmul_f32(v994, v860);
  float32x2_t v912 = vmul_f32(v994, v910);
  float32x2_t v923 = vmul_f32(v994, v921);
  float32x2_t v973 = vmul_f32(v994, v971);
  float32x2_t v984 = vmul_f32(v994, v982);
  float32x2_t v995 = vmul_f32(v994, v993);
  float32x2_t v20 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v19)), 15);
  float32x2_t v28 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v27)), 15);
  float32x2_t v34 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v33)), 15);
  float32x2_t v53 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v52)), 15);
  float32x2_t v59 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v58)), 15);
  float32x2_t v67 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v66)), 15);
  float32x2_t v73 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v72)), 15);
  float32x2_t v131 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v130)), 15);
  float32x2_t v137 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v136)), 15);
  float32x2_t v145 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v144)), 15);
  float32x2_t v151 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v150)), 15);
  float32x2_t v170 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v169)), 15);
  float32x2_t v176 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v175)), 15);
  float32x2_t v184 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v183)), 15);
  float32x2_t v190 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v189)), 15);
  float32x2_t v339 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v338)), 15);
  float32x2_t v347 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v346)), 15);
  float32x2_t v353 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v352)), 15);
  float32x2_t v372 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v371)), 15);
  float32x2_t v378 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v377)), 15);
  float32x2_t v386 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v385)), 15);
  float32x2_t v392 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v391)), 15);
  float32x2_t v450 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v449)), 15);
  float32x2_t v456 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v455)), 15);
  float32x2_t v464 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v463)), 15);
  float32x2_t v470 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v469)), 15);
  float32x2_t v489 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v488)), 15);
  float32x2_t v495 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v494)), 15);
  float32x2_t v503 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v502)), 15);
  float32x2_t v509 = vcvt_n_f32_s32(vget_low_s32(vmovl_s16(v508)), 15);
  float32x2_t v21 = vadd_f32(v14, v20);
  float32x2_t v22 = vsub_f32(v14, v20);
  float32x2_t v35 = vadd_f32(v28, v34);
  float32x2_t v36 = vsub_f32(v28, v34);
  float32x2_t v60 = vadd_f32(v53, v59);
  float32x2_t v61 = vsub_f32(v53, v59);
  float32x2_t v74 = vadd_f32(v67, v73);
  float32x2_t v75 = vsub_f32(v67, v73);
  float32x2_t v138 = vadd_f32(v131, v137);
  float32x2_t v139 = vsub_f32(v131, v137);
  float32x2_t v152 = vadd_f32(v145, v151);
  float32x2_t v153 = vsub_f32(v145, v151);
  float32x2_t v177 = vadd_f32(v170, v176);
  float32x2_t v178 = vsub_f32(v170, v176);
  float32x2_t v191 = vadd_f32(v184, v190);
  float32x2_t v192 = vsub_f32(v184, v190);
  float32x2_t v340 = vadd_f32(v333, v339);
  float32x2_t v341 = vsub_f32(v333, v339);
  float32x2_t v354 = vadd_f32(v347, v353);
  float32x2_t v355 = vsub_f32(v347, v353);
  float32x2_t v379 = vadd_f32(v372, v378);
  float32x2_t v380 = vsub_f32(v372, v378);
  float32x2_t v393 = vadd_f32(v386, v392);
  float32x2_t v394 = vsub_f32(v386, v392);
  float32x2_t v457 = vadd_f32(v450, v456);
  float32x2_t v458 = vsub_f32(v450, v456);
  float32x2_t v471 = vadd_f32(v464, v470);
  float32x2_t v472 = vsub_f32(v464, v470);
  float32x2_t v496 = vadd_f32(v489, v495);
  float32x2_t v497 = vsub_f32(v489, v495);
  float32x2_t v510 = vadd_f32(v503, v509);
  float32x2_t v511 = vsub_f32(v503, v509);
  float32x2_t v42 = vrev64_f32(v36);
  float32x2_t v44 = vadd_f32(v21, v35);
  float32x2_t v45 = vsub_f32(v21, v35);
  float32x2_t v76 = vadd_f32(v60, v74);
  float32x2_t v77 = vsub_f32(v60, v74);
  float32x2_t v92 = vmul_f32(v61, v784);
  float32x2_t v103 = vmul_f32(v75, v795);
  float32x2_t v159 = vrev64_f32(v153);
  float32x2_t v161 = vadd_f32(v138, v152);
  float32x2_t v162 = vsub_f32(v138, v152);
  float32x2_t v198 = vrev64_f32(v192);
  float32x2_t v200 = vadd_f32(v177, v191);
  float32x2_t v201 = vsub_f32(v177, v191);
  float32x2_t v361 = vrev64_f32(v355);
  float32x2_t v363 = vadd_f32(v340, v354);
  float32x2_t v364 = vsub_f32(v340, v354);
  float32x2_t v395 = vadd_f32(v379, v393);
  float32x2_t v396 = vsub_f32(v379, v393);
  float32x2_t v411 = vmul_f32(v380, v784);
  float32x2_t v422 = vmul_f32(v394, v795);
  float32x2_t v478 = vrev64_f32(v472);
  float32x2_t v480 = vadd_f32(v457, v471);
  float32x2_t v481 = vsub_f32(v457, v471);
  float32x2_t v512 = vadd_f32(v496, v510);
  float32x2_t v513 = vsub_f32(v496, v510);
  float32x2_t v528 = vmul_f32(v497, v784);
  float32x2_t v539 = vmul_f32(v511, v795);
  float32x2_t v43 = vmul_f32(v42, v801);
  float32x2_t v83 = vrev64_f32(v77);
  float32x2_t v85 = vadd_f32(v44, v76);
  float32x2_t v86 = vsub_f32(v44, v76);
  float32x2_t v98 = vrev64_f32(v92);
  float32x2_t v109 = vrev64_f32(v103);
  float32x2_t v160 = vmul_f32(v159, v801);
  float32x2_t v199 = vmul_f32(v198, v801);
  float32x2_t v204 = vadd_f32(v161, v200);
  float32x2_t v205 = vsub_f32(v161, v200);
  float32x2_t v257 = vmul_f32(v162, v784);
  float32x2_t v268 = vmul_f32(v201, v795);
  float32x2_t v362 = vmul_f32(v361, v801);
  float32x2_t v402 = vrev64_f32(v396);
  float32x2_t v404 = vadd_f32(v363, v395);
  float32x2_t v405 = vsub_f32(v363, v395);
  float32x2_t v417 = vrev64_f32(v411);
  float32x2_t v428 = vrev64_f32(v422);
  float32x2_t v479 = vmul_f32(v478, v801);
  float32x2_t v519 = vrev64_f32(v513);
  float32x2_t v521 = vadd_f32(v480, v512);
  float32x2_t v522 = vsub_f32(v480, v512);
  float32x2_t v534 = vrev64_f32(v528);
  float32x2_t v545 = vrev64_f32(v539);
  float32x2_t v46 = vsub_f32(v22, v43);
  float32x2_t v47 = vadd_f32(v22, v43);
  float32x2_t v84 = vmul_f32(v83, v801);
  float32x2_t v99 = vmul_f32(v98, v995);
  float32x2_t v110 = vmul_f32(v109, v801);
  float32x2_t v163 = vsub_f32(v139, v160);
  float32x2_t v164 = vadd_f32(v139, v160);
  float32x2_t v202 = vsub_f32(v178, v199);
  float32x2_t v203 = vadd_f32(v178, v199);
  float32x2_t v211 = vrev64_f32(v205);
  float32x2_t v213 = vadd_f32(v85, v204);
  float32x2_t v214 = vsub_f32(v85, v204);
  float32x2_t v263 = vrev64_f32(v257);
  float32x2_t v274 = vrev64_f32(v268);
  float32x2_t v365 = vsub_f32(v341, v362);
  float32x2_t v366 = vadd_f32(v341, v362);
  float32x2_t v403 = vmul_f32(v402, v801);
  float32x2_t v418 = vmul_f32(v417, v995);
  float32x2_t v429 = vmul_f32(v428, v801);
  float32x2_t v482 = vsub_f32(v458, v479);
  float32x2_t v483 = vadd_f32(v458, v479);
  float32x2_t v520 = vmul_f32(v519, v801);
  float32x2_t v535 = vmul_f32(v534, v995);
  float32x2_t v546 = vmul_f32(v545, v801);
  float32x2_t v562 = vadd_f32(v404, v521);
  float32x2_t v563 = vsub_f32(v404, v521);
  float32x2_t v785 = vmul_f32(v405, v784);
  float32x2_t v796 = vmul_f32(v522, v795);
  float32x2_t v87 = vsub_f32(v45, v84);
  float32x2_t v88 = vadd_f32(v45, v84);
  float32x2_t v111 = vadd_f32(v92, v99);
  float32x2_t v112 = vadd_f32(v103, v110);
  float32x2_t v212 = vmul_f32(v211, v801);
  float32x2_t v220 = vmul_f32(v163, v662);
  float32x2_t v226 = vrev64_f32(v163);
  float32x2_t v231 = vmul_f32(v202, v906);
  float32x2_t v237 = vrev64_f32(v202);
  float32x2_t v264 = vmul_f32(v263, v995);
  float32x2_t v275 = vmul_f32(v274, v801);
  float32x2_t v294 = vmul_f32(v164, v906);
  float32x2_t v300 = vrev64_f32(v164);
  float32x2_t v305 = vmul_f32(v203, v917);
  float32x2_t v311 = vrev64_f32(v203);
  float32x2_t v406 = vsub_f32(v364, v403);
  float32x2_t v407 = vadd_f32(v364, v403);
  float32x2_t v430 = vadd_f32(v411, v418);
  float32x2_t v431 = vadd_f32(v422, v429);
  float32x2_t v523 = vsub_f32(v481, v520);
  float32x2_t v524 = vadd_f32(v481, v520);
  float32x2_t v547 = vadd_f32(v528, v535);
  float32x2_t v548 = vadd_f32(v539, v546);
  float32x2_t v569 = vrev64_f32(v563);
  float32x2_t v571 = vadd_f32(v213, v562);
  float32x2_t v572 = vsub_f32(v213, v562);
  float32x2_t v791 = vrev64_f32(v785);
  float32x2_t v802 = vrev64_f32(v796);
  float32x2_t v113 = vadd_f32(v111, v112);
  float32x2_t v114 = vsub_f32(v112, v111);
  float32x2_t v215 = vsub_f32(v86, v212);
  float32x2_t v216 = vadd_f32(v86, v212);
  float32x2_t v276 = vadd_f32(v257, v264);
  float32x2_t v277 = vadd_f32(v268, v275);
  float32x2_t v432 = vadd_f32(v430, v431);
  float32x2_t v433 = vsub_f32(v431, v430);
  float32x2_t v549 = vadd_f32(v547, v548);
  float32x2_t v550 = vsub_f32(v548, v547);
  float32x2_t v570 = vmul_f32(v569, v801);
  int16x4_t v577 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v571, 15), (int32x2_t){0, 0}));
  int16x4_t v589 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v572, 15), (int32x2_t){0, 0}));
  float32x2_t v663 = vmul_f32(v406, v662);
  float32x2_t v669 = vrev64_f32(v406);
  float32x2_t v674 = vmul_f32(v523, v906);
  float32x2_t v680 = vrev64_f32(v523);
  float32x2_t v792 = vmul_f32(v791, v995);
  float32x2_t v803 = vmul_f32(v802, v801);
  float32x2_t v907 = vmul_f32(v407, v906);
  float32x2_t v913 = vrev64_f32(v407);
  float32x2_t v918 = vmul_f32(v524, v917);
  float32x2_t v924 = vrev64_f32(v524);
  float32x2_t v120 = vrev64_f32(v114);
  float32x2_t v122 = vadd_f32(v46, v113);
  float32x2_t v123 = vsub_f32(v46, v113);
  float32x2_t v239 = vfma_f32(v220, v226, v668);
  float32x2_t v240 = vfma_f32(v231, v237, v912);
  float32x2_t v278 = vadd_f32(v276, v277);
  float32x2_t v279 = vsub_f32(v277, v276);
  float32x2_t v313 = vfma_f32(v294, v300, v912);
  float32x2_t v314 = vfma_f32(v305, v311, v923);
  float32x2_t v439 = vrev64_f32(v433);
  float32x2_t v441 = vadd_f32(v365, v432);
  float32x2_t v442 = vsub_f32(v365, v432);
  float32x2_t v556 = vrev64_f32(v550);
  float32x2_t v558 = vadd_f32(v482, v549);
  float32x2_t v559 = vsub_f32(v482, v549);
  float32x2_t v573 = vsub_f32(v214, v570);
  float32x2_t v574 = vadd_f32(v214, v570);
  v6[0] = vget_lane_s32(vreinterpret_s32_s16(v577), 0);
  v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v589), 0);
  float32x2_t v804 = vadd_f32(v785, v792);
  float32x2_t v805 = vadd_f32(v796, v803);
  float32x2_t v121 = vmul_f32(v120, v995);
  float32x2_t v241 = vadd_f32(v239, v240);
  float32x2_t v242 = vsub_f32(v240, v239);
  float32x2_t v285 = vrev64_f32(v279);
  float32x2_t v287 = vadd_f32(v87, v278);
  float32x2_t v288 = vsub_f32(v87, v278);
  float32x2_t v315 = vadd_f32(v313, v314);
  float32x2_t v316 = vsub_f32(v314, v313);
  float32x2_t v440 = vmul_f32(v439, v995);
  float32x2_t v557 = vmul_f32(v556, v995);
  int16x4_t v583 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v573, 15), (int32x2_t){0, 0}));
  int16x4_t v595 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v574, 15), (int32x2_t){0, 0}));
  float32x2_t v602 = vmul_f32(v441, v601);
  float32x2_t v608 = vrev64_f32(v441);
  float32x2_t v613 = vmul_f32(v558, v723);
  float32x2_t v619 = vrev64_f32(v558);
  float32x2_t v682 = vfma_f32(v663, v669, v668);
  float32x2_t v683 = vfma_f32(v674, v680, v912);
  float32x2_t v806 = vadd_f32(v804, v805);
  float32x2_t v807 = vsub_f32(v805, v804);
  float32x2_t v846 = vmul_f32(v442, v845);
  float32x2_t v852 = vrev64_f32(v442);
  float32x2_t v857 = vmul_f32(v559, v856);
  float32x2_t v863 = vrev64_f32(v559);
  float32x2_t v926 = vfma_f32(v907, v913, v912);
  float32x2_t v927 = vfma_f32(v918, v924, v923);
  float32x2_t v124 = vsub_f32(v47, v121);
  float32x2_t v125 = vadd_f32(v47, v121);
  float32x2_t v248 = vrev64_f32(v242);
  float32x2_t v250 = vadd_f32(v122, v241);
  float32x2_t v251 = vsub_f32(v122, v241);
  float32x2_t v286 = vmul_f32(v285, v995);
  float32x2_t v322 = vrev64_f32(v316);
  float32x2_t v443 = vsub_f32(v366, v440);
  float32x2_t v444 = vadd_f32(v366, v440);
  float32x2_t v560 = vsub_f32(v483, v557);
  float32x2_t v561 = vadd_f32(v483, v557);
  v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v583), 0);
  v6[ostride * 24] = vget_lane_s32(vreinterpret_s32_s16(v595), 0);
  float32x2_t v684 = vadd_f32(v682, v683);
  float32x2_t v685 = vsub_f32(v683, v682);
  float32x2_t v813 = vrev64_f32(v807);
  float32x2_t v815 = vadd_f32(v215, v806);
  float32x2_t v816 = vsub_f32(v215, v806);
  float32x2_t v928 = vadd_f32(v926, v927);
  float32x2_t v929 = vsub_f32(v927, v926);
  float32x2_t v249 = vmul_f32(v248, v995);
  float32x2_t v289 = vsub_f32(v88, v286);
  float32x2_t v290 = vadd_f32(v88, v286);
  float32x2_t v323 = vmul_f32(v322, v995);
  float32x2_t v324 = vadd_f32(v124, v315);
  float32x2_t v325 = vsub_f32(v124, v315);
  float32x2_t v621 = vfma_f32(v602, v608, v862);
  float32x2_t v622 = vfma_f32(v613, v619, v729);
  float32x2_t v691 = vrev64_f32(v685);
  float32x2_t v693 = vadd_f32(v287, v684);
  float32x2_t v694 = vsub_f32(v287, v684);
  float32x2_t v724 = vmul_f32(v443, v723);
  float32x2_t v730 = vrev64_f32(v443);
  float32x2_t v735 = vmul_f32(v560, v734);
  float32x2_t v741 = vrev64_f32(v560);
  float32x2_t v814 = vmul_f32(v813, v995);
  int16x4_t v821 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v815, 15), (int32x2_t){0, 0}));
  int16x4_t v833 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v816, 15), (int32x2_t){0, 0}));
  float32x2_t v865 = vfma_f32(v846, v852, v851);
  float32x2_t v866 = vfma_f32(v857, v863, v862);
  float32x2_t v935 = vrev64_f32(v929);
  float32x2_t v968 = vmul_f32(v444, v967);
  float32x2_t v974 = vrev64_f32(v444);
  float32x2_t v979 = vmul_f32(v561, v978);
  float32x2_t v985 = vrev64_f32(v561);
  float32x2_t v252 = vsub_f32(v123, v249);
  float32x2_t v253 = vadd_f32(v123, v249);
  float32x2_t v326 = vsub_f32(v125, v323);
  float32x2_t v327 = vadd_f32(v125, v323);
  float32x2_t v623 = vadd_f32(v621, v622);
  float32x2_t v624 = vsub_f32(v622, v621);
  float32x2_t v692 = vmul_f32(v691, v995);
  int16x4_t v699 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v693, 15), (int32x2_t){0, 0}));
  int16x4_t v711 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v694, 15), (int32x2_t){0, 0}));
  float32x2_t v817 = vsub_f32(v216, v814);
  float32x2_t v818 = vadd_f32(v216, v814);
  v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v821), 0);
  v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v833), 0);
  float32x2_t v867 = vadd_f32(v865, v866);
  float32x2_t v868 = vsub_f32(v866, v865);
  float32x2_t v936 = vmul_f32(v935, v995);
  float32x2_t v937 = vadd_f32(v289, v928);
  float32x2_t v938 = vsub_f32(v289, v928);
  float32x2_t v630 = vrev64_f32(v624);
  float32x2_t v632 = vadd_f32(v250, v623);
  float32x2_t v633 = vsub_f32(v250, v623);
  float32x2_t v695 = vsub_f32(v288, v692);
  float32x2_t v696 = vadd_f32(v288, v692);
  v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v699), 0);
  v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v711), 0);
  float32x2_t v743 = vfma_f32(v724, v730, v729);
  float32x2_t v744 = vfma_f32(v735, v741, v973);
  int16x4_t v827 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v817, 15), (int32x2_t){0, 0}));
  int16x4_t v839 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v818, 15), (int32x2_t){0, 0}));
  float32x2_t v874 = vrev64_f32(v868);
  float32x2_t v876 = vadd_f32(v252, v867);
  float32x2_t v877 = vsub_f32(v252, v867);
  float32x2_t v939 = vsub_f32(v290, v936);
  float32x2_t v940 = vadd_f32(v290, v936);
  int16x4_t v943 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v937, 15), (int32x2_t){0, 0}));
  int16x4_t v955 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v938, 15), (int32x2_t){0, 0}));
  float32x2_t v987 = vfma_f32(v968, v974, v973);
  float32x2_t v988 = vfma_f32(v979, v985, v984);
  float32x2_t v631 = vmul_f32(v630, v995);
  int16x4_t v638 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v632, 15), (int32x2_t){0, 0}));
  int16x4_t v650 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v633, 15), (int32x2_t){0, 0}));
  int16x4_t v705 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v695, 15), (int32x2_t){0, 0}));
  int16x4_t v717 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v696, 15), (int32x2_t){0, 0}));
  float32x2_t v745 = vadd_f32(v743, v744);
  float32x2_t v746 = vsub_f32(v744, v743);
  v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v827), 0);
  v6[ostride * 28] = vget_lane_s32(vreinterpret_s32_s16(v839), 0);
  float32x2_t v875 = vmul_f32(v874, v995);
  int16x4_t v882 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v876, 15), (int32x2_t){0, 0}));
  int16x4_t v894 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v877, 15), (int32x2_t){0, 0}));
  v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v943), 0);
  int16x4_t v949 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v939, 15), (int32x2_t){0, 0}));
  v6[ostride * 22] = vget_lane_s32(vreinterpret_s32_s16(v955), 0);
  int16x4_t v961 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v940, 15), (int32x2_t){0, 0}));
  float32x2_t v989 = vadd_f32(v987, v988);
  float32x2_t v990 = vsub_f32(v988, v987);
  float32x2_t v634 = vsub_f32(v251, v631);
  float32x2_t v635 = vadd_f32(v251, v631);
  v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v638), 0);
  v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v650), 0);
  v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v705), 0);
  v6[ostride * 26] = vget_lane_s32(vreinterpret_s32_s16(v717), 0);
  float32x2_t v752 = vrev64_f32(v746);
  float32x2_t v754 = vadd_f32(v324, v745);
  float32x2_t v755 = vsub_f32(v324, v745);
  float32x2_t v878 = vsub_f32(v253, v875);
  float32x2_t v879 = vadd_f32(v253, v875);
  v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v882), 0);
  v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v894), 0);
  v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v949), 0);
  v6[ostride * 30] = vget_lane_s32(vreinterpret_s32_s16(v961), 0);
  float32x2_t v996 = vrev64_f32(v990);
  float32x2_t v998 = vadd_f32(v326, v989);
  float32x2_t v999 = vsub_f32(v326, v989);
  int16x4_t v644 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v634, 15), (int32x2_t){0, 0}));
  int16x4_t v656 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v635, 15), (int32x2_t){0, 0}));
  float32x2_t v753 = vmul_f32(v752, v995);
  int16x4_t v760 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v754, 15), (int32x2_t){0, 0}));
  int16x4_t v772 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v755, 15), (int32x2_t){0, 0}));
  int16x4_t v888 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v878, 15), (int32x2_t){0, 0}));
  int16x4_t v900 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v879, 15), (int32x2_t){0, 0}));
  float32x2_t v997 = vmul_f32(v996, v995);
  int16x4_t v1004 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v998, 15), (int32x2_t){0, 0}));
  int16x4_t v1016 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v999, 15), (int32x2_t){0, 0}));
  v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v644), 0);
  v6[ostride * 25] = vget_lane_s32(vreinterpret_s32_s16(v656), 0);
  float32x2_t v756 = vsub_f32(v325, v753);
  float32x2_t v757 = vadd_f32(v325, v753);
  v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v760), 0);
  v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v772), 0);
  v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v888), 0);
  v6[ostride * 29] = vget_lane_s32(vreinterpret_s32_s16(v900), 0);
  float32x2_t v1000 = vsub_f32(v327, v997);
  float32x2_t v1001 = vadd_f32(v327, v997);
  v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v1004), 0);
  v6[ostride * 23] = vget_lane_s32(vreinterpret_s32_s16(v1016), 0);
  int16x4_t v766 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v756, 15), (int32x2_t){0, 0}));
  int16x4_t v778 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v757, 15), (int32x2_t){0, 0}));
  int16x4_t v1010 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1000, 15), (int32x2_t){0, 0}));
  int16x4_t v1022 =
      vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1001, 15), (int32x2_t){0, 0}));
  v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v766), 0);
  v6[ostride * 27] = vget_lane_s32(vreinterpret_s32_s16(v778), 0);
  v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v1010), 0);
  v6[ostride * 31] = vget_lane_s32(vreinterpret_s32_s16(v1022), 0);
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cs16_cf32_cs16_ac_n_uun32(
    const armral_cmplx_int16_t *restrict x, armral_cmplx_int16_t *restrict y,
    int istride, int ostride, int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const int32_t *v5 = (const int32_t *)x;
  int32_t *v6 = (int32_t *)y;
  svbool_t pred_full = svptrue_pat_b32(SV_VL2);
  float v843 = -1.9509032201612819e-01F;
  float v902 = 7.0710678118654757e-01F;
  float v914 = -7.0710678118654746e-01F;
  float v919 = -1.0000000000000000e+00F;
  float v973 = 5.5557023301960229e-01F;
  float v978 = 8.3146961230254524e-01F;
  float v985 = -9.8078528040323043e-01F;
  float v1044 = 3.8268343236508984e-01F;
  float v1049 = 9.2387953251128674e-01F;
  float v1056 = -9.2387953251128685e-01F;
  float v1061 = -3.8268343236508967e-01F;
  float v1115 = 1.9509032201612833e-01F;
  float v1120 = 9.8078528040323043e-01F;
  float v1127 = -5.5557023301960218e-01F;
  float v1132 = -8.3146961230254524e-01F;
  const int32_t *v1361 = &v5[v0];
  int32_t *v1562 = &v6[v2];
  int64_t v23 = v0 * 16;
  int64_t v33 = v0 * 8;
  int64_t v41 = v0 * 24;
  int64_t v62 = v0 * 4;
  int64_t v70 = v0 * 20;
  int64_t v80 = v0 * 12;
  int64_t v88 = v0 * 28;
  int64_t v150 = v0 * 2;
  int64_t v158 = v0 * 18;
  int64_t v168 = v0 * 10;
  int64_t v176 = v0 * 26;
  int64_t v197 = v0 * 6;
  int64_t v205 = v0 * 22;
  int64_t v215 = v0 * 14;
  int64_t v223 = v0 * 30;
  int64_t v382 = v0 * 17;
  int64_t v392 = v0 * 9;
  int64_t v400 = v0 * 25;
  int64_t v421 = v0 * 5;
  int64_t v429 = v0 * 21;
  int64_t v439 = v0 * 13;
  int64_t v447 = v0 * 29;
  int64_t v509 = v0 * 3;
  int64_t v517 = v0 * 19;
  int64_t v527 = v0 * 11;
  int64_t v535 = v0 * 27;
  int64_t v556 = v0 * 7;
  int64_t v564 = v0 * 23;
  int64_t v574 = v0 * 15;
  int64_t v582 = v0 * 31;
  int64_t v665 = v2 * 8;
  int64_t v673 = v2 * 16;
  int64_t v681 = v2 * 24;
  int64_t v736 = v2 * 9;
  int64_t v744 = v2 * 17;
  int64_t v752 = v2 * 25;
  float v768 = v4 * v1044;
  int64_t v799 = v2 * 2;
  int64_t v807 = v2 * 10;
  int64_t v815 = v2 * 18;
  int64_t v823 = v2 * 26;
  float v839 = v4 * v973;
  int64_t v870 = v2 * 3;
  int64_t v878 = v2 * 11;
  int64_t v886 = v2 * 19;
  int64_t v894 = v2 * 27;
  float v922 = v4 * v919;
  int64_t v941 = v2 * 4;
  int64_t v949 = v2 * 12;
  int64_t v957 = v2 * 20;
  int64_t v965 = v2 * 28;
  float v981 = v4 * v978;
  float v993 = v4 * v1115;
  int64_t v1012 = v2 * 5;
  int64_t v1020 = v2 * 13;
  int64_t v1028 = v2 * 21;
  int64_t v1036 = v2 * 29;
  float v1052 = v4 * v1049;
  float v1064 = v4 * v1061;
  int64_t v1083 = v2 * 6;
  int64_t v1091 = v2 * 14;
  int64_t v1099 = v2 * 22;
  int64_t v1107 = v2 * 30;
  float v1123 = v4 * v1120;
  float v1135 = v4 * v1132;
  int64_t v1154 = v2 * 7;
  int64_t v1162 = v2 * 15;
  int64_t v1170 = v2 * 23;
  int64_t v1178 = v2 * 31;
  const int32_t *v1192 = &v5[0];
  int32_t *v1521 = &v6[0];
  svfloat32_t v1551 = svdup_n_f32(v1120);
  svfloat32_t v1592 = svdup_n_f32(v1049);
  svfloat32_t v1633 = svdup_n_f32(v978);
  svfloat32_t v1635 = svdup_n_f32(v843);
  svfloat32_t v1674 = svdup_n_f32(v902);
  svfloat32_t v1676 = svdup_n_f32(v914);
  svfloat32_t v1715 = svdup_n_f32(v973);
  svfloat32_t v1717 = svdup_n_f32(v985);
  svfloat32_t v1756 = svdup_n_f32(v1044);
  svfloat32_t v1758 = svdup_n_f32(v1056);
  svfloat32_t v1797 = svdup_n_f32(v1115);
  svfloat32_t v1799 = svdup_n_f32(v1127);
  svfloat32_t v1801 = svdup_n_f32(v4);
  svfloat32_t v380 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1361[0])),
      1.F / (1ULL << 15ULL));
  const int32_t *v1201 = &v5[v23];
  const int32_t *v1210 = &v5[v33];
  const int32_t *v1219 = &v5[v41];
  const int32_t *v1229 = &v5[v62];
  const int32_t *v1238 = &v5[v70];
  const int32_t *v1247 = &v5[v80];
  const int32_t *v1256 = &v5[v88];
  const int32_t *v1271 = &v5[v150];
  const int32_t *v1280 = &v5[v158];
  const int32_t *v1289 = &v5[v168];
  const int32_t *v1298 = &v5[v176];
  const int32_t *v1308 = &v5[v197];
  const int32_t *v1317 = &v5[v205];
  const int32_t *v1326 = &v5[v215];
  const int32_t *v1335 = &v5[v223];
  const int32_t *v1370 = &v5[v382];
  const int32_t *v1379 = &v5[v392];
  const int32_t *v1388 = &v5[v400];
  const int32_t *v1398 = &v5[v421];
  const int32_t *v1407 = &v5[v429];
  const int32_t *v1416 = &v5[v439];
  const int32_t *v1425 = &v5[v447];
  const int32_t *v1440 = &v5[v509];
  const int32_t *v1449 = &v5[v517];
  const int32_t *v1458 = &v5[v527];
  const int32_t *v1467 = &v5[v535];
  const int32_t *v1477 = &v5[v556];
  const int32_t *v1486 = &v5[v564];
  const int32_t *v1495 = &v5[v574];
  const int32_t *v1504 = &v5[v582];
  int32_t *v1530 = &v6[v665];
  int32_t *v1539 = &v6[v673];
  int32_t *v1548 = &v6[v681];
  int32_t *v1571 = &v6[v736];
  int32_t *v1580 = &v6[v744];
  int32_t *v1589 = &v6[v752];
  svfloat32_t v1593 = svdup_n_f32(v768);
  int32_t *v1603 = &v6[v799];
  int32_t *v1612 = &v6[v807];
  int32_t *v1621 = &v6[v815];
  int32_t *v1630 = &v6[v823];
  svfloat32_t v1634 = svdup_n_f32(v839);
  int32_t *v1644 = &v6[v870];
  int32_t *v1653 = &v6[v878];
  int32_t *v1662 = &v6[v886];
  int32_t *v1671 = &v6[v894];
  svfloat32_t v1677 = svdup_n_f32(v922);
  int32_t *v1685 = &v6[v941];
  int32_t *v1694 = &v6[v949];
  int32_t *v1703 = &v6[v957];
  int32_t *v1712 = &v6[v965];
  svfloat32_t v1716 = svdup_n_f32(v981);
  svfloat32_t v1718 = svdup_n_f32(v993);
  int32_t *v1726 = &v6[v1012];
  int32_t *v1735 = &v6[v1020];
  int32_t *v1744 = &v6[v1028];
  int32_t *v1753 = &v6[v1036];
  svfloat32_t v1757 = svdup_n_f32(v1052);
  svfloat32_t v1759 = svdup_n_f32(v1064);
  int32_t *v1767 = &v6[v1083];
  int32_t *v1776 = &v6[v1091];
  int32_t *v1785 = &v6[v1099];
  int32_t *v1794 = &v6[v1107];
  svfloat32_t v1798 = svdup_n_f32(v1123);
  svfloat32_t v1800 = svdup_n_f32(v1135);
  int32_t *v1808 = &v6[v1154];
  int32_t *v1817 = &v6[v1162];
  int32_t *v1826 = &v6[v1170];
  int32_t *v1835 = &v6[v1178];
  svfloat32_t v21 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1192[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v29 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1201[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v39 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1210[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v47 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1219[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v68 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1229[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v76 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1238[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v86 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1247[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v94 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1256[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v156 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1271[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v164 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1280[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v174 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1289[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v182 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1298[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v203 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1308[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v211 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1317[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v221 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1326[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v229 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1335[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v388 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1370[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v398 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1379[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v406 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1388[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v427 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1398[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v435 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1407[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v445 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1416[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v453 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1425[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v515 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1440[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v523 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1449[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v533 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1458[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v541 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1467[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v562 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1477[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v570 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1486[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v580 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1495[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v588 = svmul_n_f32_x(
      pred_full,
      svcvt_f32_s32_x(pred_full,
                      svld1sh_s32(pred_full, (const int16_t *)&v1504[0])),
      1.F / (1ULL << 15ULL));
  svfloat32_t v30 = svadd_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v31 = svsub_f32_x(svptrue_b32(), v21, v29);
  svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v39, v47);
  svfloat32_t v77 = svadd_f32_x(svptrue_b32(), v68, v76);
  svfloat32_t v78 = svsub_f32_x(svptrue_b32(), v68, v76);
  svfloat32_t v95 = svadd_f32_x(svptrue_b32(), v86, v94);
  svfloat32_t v96 = svsub_f32_x(svptrue_b32(), v86, v94);
  svfloat32_t v165 = svadd_f32_x(svptrue_b32(), v156, v164);
  svfloat32_t v166 = svsub_f32_x(svptrue_b32(), v156, v164);
  svfloat32_t v183 = svadd_f32_x(svptrue_b32(), v174, v182);
  svfloat32_t v184 = svsub_f32_x(svptrue_b32(), v174, v182);
  svfloat32_t v212 = svadd_f32_x(svptrue_b32(), v203, v211);
  svfloat32_t v213 = svsub_f32_x(svptrue_b32(), v203, v211);
  svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v221, v229);
  svfloat32_t v231 = svsub_f32_x(svptrue_b32(), v221, v229);
  svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v380, v388);
  svfloat32_t v390 = svsub_f32_x(svptrue_b32(), v380, v388);
  svfloat32_t v407 = svadd_f32_x(svptrue_b32(), v398, v406);
  svfloat32_t v408 = svsub_f32_x(svptrue_b32(), v398, v406);
  svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v427, v435);
  svfloat32_t v437 = svsub_f32_x(svptrue_b32(), v427, v435);
  svfloat32_t v454 = svadd_f32_x(svptrue_b32(), v445, v453);
  svfloat32_t v455 = svsub_f32_x(svptrue_b32(), v445, v453);
  svfloat32_t v524 = svadd_f32_x(svptrue_b32(), v515, v523);
  svfloat32_t v525 = svsub_f32_x(svptrue_b32(), v515, v523);
  svfloat32_t v542 = svadd_f32_x(svptrue_b32(), v533, v541);
  svfloat32_t v543 = svsub_f32_x(svptrue_b32(), v533, v541);
  svfloat32_t v571 = svadd_f32_x(svptrue_b32(), v562, v570);
  svfloat32_t v572 = svsub_f32_x(svptrue_b32(), v562, v570);
  svfloat32_t v589 = svadd_f32_x(svptrue_b32(), v580, v588);
  svfloat32_t v590 = svsub_f32_x(svptrue_b32(), v580, v588);
  svfloat32_t zero56 = svdup_n_f32(0);
  svfloat32_t v56 = svcmla_f32_x(pred_full, zero56, v1677, v49, 90);
  svfloat32_t v57 = svadd_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v58 = svsub_f32_x(svptrue_b32(), v30, v48);
  svfloat32_t v97 = svadd_f32_x(svptrue_b32(), v77, v95);
  svfloat32_t v98 = svsub_f32_x(svptrue_b32(), v77, v95);
  svfloat32_t v114 = svmul_f32_x(svptrue_b32(), v78, v1674);
  svfloat32_t v126 = svmul_f32_x(svptrue_b32(), v96, v1676);
  svfloat32_t zero191 = svdup_n_f32(0);
  svfloat32_t v191 = svcmla_f32_x(pred_full, zero191, v1677, v184, 90);
  svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v165, v183);
  svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v165, v183);
  svfloat32_t zero238 = svdup_n_f32(0);
  svfloat32_t v238 = svcmla_f32_x(pred_full, zero238, v1677, v231, 90);
  svfloat32_t v239 = svadd_f32_x(svptrue_b32(), v212, v230);
  svfloat32_t v240 = svsub_f32_x(svptrue_b32(), v212, v230);
  svfloat32_t zero415 = svdup_n_f32(0);
  svfloat32_t v415 = svcmla_f32_x(pred_full, zero415, v1677, v408, 90);
  svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v389, v407);
  svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v389, v407);
  svfloat32_t v456 = svadd_f32_x(svptrue_b32(), v436, v454);
  svfloat32_t v457 = svsub_f32_x(svptrue_b32(), v436, v454);
  svfloat32_t v473 = svmul_f32_x(svptrue_b32(), v437, v1674);
  svfloat32_t v485 = svmul_f32_x(svptrue_b32(), v455, v1676);
  svfloat32_t zero550 = svdup_n_f32(0);
  svfloat32_t v550 = svcmla_f32_x(pred_full, zero550, v1677, v543, 90);
  svfloat32_t v551 = svadd_f32_x(svptrue_b32(), v524, v542);
  svfloat32_t v552 = svsub_f32_x(svptrue_b32(), v524, v542);
  svfloat32_t v591 = svadd_f32_x(svptrue_b32(), v571, v589);
  svfloat32_t v592 = svsub_f32_x(svptrue_b32(), v571, v589);
  svfloat32_t v608 = svmul_f32_x(svptrue_b32(), v572, v1674);
  svfloat32_t v620 = svmul_f32_x(svptrue_b32(), v590, v1676);
  svfloat32_t v59 = svsub_f32_x(svptrue_b32(), v31, v56);
  svfloat32_t v60 = svadd_f32_x(svptrue_b32(), v31, v56);
  svfloat32_t zero105 = svdup_n_f32(0);
  svfloat32_t v105 = svcmla_f32_x(pred_full, zero105, v1677, v98, 90);
  svfloat32_t v106 = svadd_f32_x(svptrue_b32(), v57, v97);
  svfloat32_t v107 = svsub_f32_x(svptrue_b32(), v57, v97);
  svfloat32_t v194 = svsub_f32_x(svptrue_b32(), v166, v191);
  svfloat32_t v195 = svadd_f32_x(svptrue_b32(), v166, v191);
  svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v213, v238);
  svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v213, v238);
  svfloat32_t v243 = svadd_f32_x(svptrue_b32(), v192, v239);
  svfloat32_t v244 = svsub_f32_x(svptrue_b32(), v192, v239);
  svfloat32_t v299 = svmul_f32_x(svptrue_b32(), v193, v1674);
  svfloat32_t v311 = svmul_f32_x(svptrue_b32(), v240, v1676);
  svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v390, v415);
  svfloat32_t v419 = svadd_f32_x(svptrue_b32(), v390, v415);
  svfloat32_t zero464 = svdup_n_f32(0);
  svfloat32_t v464 = svcmla_f32_x(pred_full, zero464, v1677, v457, 90);
  svfloat32_t v465 = svadd_f32_x(svptrue_b32(), v416, v456);
  svfloat32_t v466 = svsub_f32_x(svptrue_b32(), v416, v456);
  svfloat32_t v553 = svsub_f32_x(svptrue_b32(), v525, v550);
  svfloat32_t v554 = svadd_f32_x(svptrue_b32(), v525, v550);
  svfloat32_t zero599 = svdup_n_f32(0);
  svfloat32_t v599 = svcmla_f32_x(pred_full, zero599, v1677, v592, 90);
  svfloat32_t v600 = svadd_f32_x(svptrue_b32(), v551, v591);
  svfloat32_t v601 = svsub_f32_x(svptrue_b32(), v551, v591);
  svfloat32_t v108 = svsub_f32_x(svptrue_b32(), v58, v105);
  svfloat32_t v109 = svadd_f32_x(svptrue_b32(), v58, v105);
  svfloat32_t v134 = svcmla_f32_x(pred_full, v114, v1801, v114, 90);
  svfloat32_t v135 = svcmla_f32_x(pred_full, v126, v1677, v126, 90);
  svfloat32_t zero251 = svdup_n_f32(0);
  svfloat32_t v251 = svcmla_f32_x(pred_full, zero251, v1677, v244, 90);
  svfloat32_t v252 = svadd_f32_x(svptrue_b32(), v106, v243);
  svfloat32_t v253 = svsub_f32_x(svptrue_b32(), v106, v243);
  svfloat32_t v260 = svmul_f32_x(svptrue_b32(), v194, v1592);
  svfloat32_t v272 = svmul_f32_x(svptrue_b32(), v241, v1756);
  svfloat32_t v338 = svmul_f32_x(svptrue_b32(), v195, v1756);
  svfloat32_t v350 = svmul_f32_x(svptrue_b32(), v242, v1758);
  svfloat32_t v467 = svsub_f32_x(svptrue_b32(), v417, v464);
  svfloat32_t v468 = svadd_f32_x(svptrue_b32(), v417, v464);
  svfloat32_t v493 = svcmla_f32_x(pred_full, v473, v1801, v473, 90);
  svfloat32_t v494 = svcmla_f32_x(pred_full, v485, v1677, v485, 90);
  svfloat32_t v602 = svsub_f32_x(svptrue_b32(), v552, v599);
  svfloat32_t v603 = svadd_f32_x(svptrue_b32(), v552, v599);
  svfloat32_t v628 = svcmla_f32_x(pred_full, v608, v1801, v608, 90);
  svfloat32_t v629 = svcmla_f32_x(pred_full, v620, v1677, v620, 90);
  svfloat32_t v643 = svadd_f32_x(svptrue_b32(), v465, v600);
  svfloat32_t v644 = svsub_f32_x(svptrue_b32(), v465, v600);
  svfloat32_t v905 = svmul_f32_x(svptrue_b32(), v466, v1674);
  svfloat32_t v917 = svmul_f32_x(svptrue_b32(), v601, v1676);
  svfloat32_t v136 = svadd_f32_x(svptrue_b32(), v134, v135);
  svfloat32_t v137 = svsub_f32_x(svptrue_b32(), v135, v134);
  svfloat32_t v254 = svsub_f32_x(svptrue_b32(), v107, v251);
  svfloat32_t v255 = svadd_f32_x(svptrue_b32(), v107, v251);
  svfloat32_t v280 = svcmla_f32_x(pred_full, v260, v1593, v194, 90);
  svfloat32_t v281 = svcmla_f32_x(pred_full, v272, v1757, v241, 90);
  svfloat32_t v319 = svcmla_f32_x(pred_full, v299, v1801, v299, 90);
  svfloat32_t v320 = svcmla_f32_x(pred_full, v311, v1677, v311, 90);
  svfloat32_t v358 = svcmla_f32_x(pred_full, v338, v1757, v195, 90);
  svfloat32_t v359 = svcmla_f32_x(pred_full, v350, v1759, v242, 90);
  svfloat32_t v495 = svadd_f32_x(svptrue_b32(), v493, v494);
  svfloat32_t v496 = svsub_f32_x(svptrue_b32(), v494, v493);
  svfloat32_t v630 = svadd_f32_x(svptrue_b32(), v628, v629);
  svfloat32_t v631 = svsub_f32_x(svptrue_b32(), v629, v628);
  svfloat32_t zero651 = svdup_n_f32(0);
  svfloat32_t v651 = svcmla_f32_x(pred_full, zero651, v1677, v644, 90);
  svfloat32_t v652 = svadd_f32_x(svptrue_b32(), v252, v643);
  svfloat32_t v653 = svsub_f32_x(svptrue_b32(), v252, v643);
  svfloat32_t v763 = svmul_f32_x(svptrue_b32(), v467, v1592);
  svfloat32_t v775 = svmul_f32_x(svptrue_b32(), v602, v1756);
  svfloat32_t v1047 = svmul_f32_x(svptrue_b32(), v468, v1756);
  svfloat32_t v1059 = svmul_f32_x(svptrue_b32(), v603, v1758);
  svfloat32_t zero144 = svdup_n_f32(0);
  svfloat32_t v144 = svcmla_f32_x(pred_full, zero144, v1801, v137, 90);
  svfloat32_t v145 = svadd_f32_x(svptrue_b32(), v59, v136);
  svfloat32_t v146 = svsub_f32_x(svptrue_b32(), v59, v136);
  svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v280, v281);
  svfloat32_t v283 = svsub_f32_x(svptrue_b32(), v281, v280);
  svfloat32_t v321 = svadd_f32_x(svptrue_b32(), v319, v320);
  svfloat32_t v322 = svsub_f32_x(svptrue_b32(), v320, v319);
  svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v358, v359);
  svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v359, v358);
  svfloat32_t zero503 = svdup_n_f32(0);
  svfloat32_t v503 = svcmla_f32_x(pred_full, zero503, v1801, v496, 90);
  svfloat32_t v504 = svadd_f32_x(svptrue_b32(), v418, v495);
  svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v418, v495);
  svfloat32_t zero638 = svdup_n_f32(0);
  svfloat32_t v638 = svcmla_f32_x(pred_full, zero638, v1801, v631, 90);
  svfloat32_t v639 = svadd_f32_x(svptrue_b32(), v553, v630);
  svfloat32_t v640 = svsub_f32_x(svptrue_b32(), v553, v630);
  svfloat32_t v654 = svsub_f32_x(svptrue_b32(), v253, v651);
  svfloat32_t v655 = svadd_f32_x(svptrue_b32(), v253, v651);
  svint16_t v658 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v652, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v674 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v653, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v783 = svcmla_f32_x(pred_full, v763, v1593, v467, 90);
  svfloat32_t v784 = svcmla_f32_x(pred_full, v775, v1757, v602, 90);
  svfloat32_t v925 = svcmla_f32_x(pred_full, v905, v1801, v905, 90);
  svfloat32_t v926 = svcmla_f32_x(pred_full, v917, v1677, v917, 90);
  svfloat32_t v1067 = svcmla_f32_x(pred_full, v1047, v1757, v468, 90);
  svfloat32_t v1068 = svcmla_f32_x(pred_full, v1059, v1759, v603, 90);
  svfloat32_t v147 = svsub_f32_x(svptrue_b32(), v60, v144);
  svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v60, v144);
  svfloat32_t zero290 = svdup_n_f32(0);
  svfloat32_t v290 = svcmla_f32_x(pred_full, zero290, v1801, v283, 90);
  svfloat32_t v291 = svadd_f32_x(svptrue_b32(), v145, v282);
  svfloat32_t v292 = svsub_f32_x(svptrue_b32(), v145, v282);
  svfloat32_t zero329 = svdup_n_f32(0);
  svfloat32_t v329 = svcmla_f32_x(pred_full, zero329, v1801, v322, 90);
  svfloat32_t v330 = svadd_f32_x(svptrue_b32(), v108, v321);
  svfloat32_t v331 = svsub_f32_x(svptrue_b32(), v108, v321);
  svfloat32_t zero368 = svdup_n_f32(0);
  svfloat32_t v368 = svcmla_f32_x(pred_full, zero368, v1801, v361, 90);
  svfloat32_t v506 = svsub_f32_x(svptrue_b32(), v419, v503);
  svfloat32_t v507 = svadd_f32_x(svptrue_b32(), v419, v503);
  svfloat32_t v641 = svsub_f32_x(svptrue_b32(), v554, v638);
  svfloat32_t v642 = svadd_f32_x(svptrue_b32(), v554, v638);
  svint16_t v666 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v654, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v682 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v655, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v692 = svmul_f32_x(svptrue_b32(), v504, v1551);
  svfloat32_t v704 = svmul_f32_x(svptrue_b32(), v639, v1633);
  svfloat32_t v785 = svadd_f32_x(svptrue_b32(), v783, v784);
  svfloat32_t v786 = svsub_f32_x(svptrue_b32(), v784, v783);
  svfloat32_t v927 = svadd_f32_x(svptrue_b32(), v925, v926);
  svfloat32_t v928 = svsub_f32_x(svptrue_b32(), v926, v925);
  svfloat32_t v976 = svmul_f32_x(svptrue_b32(), v505, v1715);
  svfloat32_t v988 = svmul_f32_x(svptrue_b32(), v640, v1717);
  svfloat32_t v1069 = svadd_f32_x(svptrue_b32(), v1067, v1068);
  svfloat32_t v1070 = svsub_f32_x(svptrue_b32(), v1068, v1067);
  svst1w_u64(pred_full, (unsigned *)(v1521), svreinterpret_u64_s16(v658));
  svst1w_u64(pred_full, (unsigned *)(v1539), svreinterpret_u64_s16(v674));
  svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v146, v290);
  svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v146, v290);
  svfloat32_t v332 = svsub_f32_x(svptrue_b32(), v109, v329);
  svfloat32_t v333 = svadd_f32_x(svptrue_b32(), v109, v329);
  svfloat32_t v369 = svadd_f32_x(svptrue_b32(), v147, v360);
  svfloat32_t v370 = svsub_f32_x(svptrue_b32(), v147, v360);
  svfloat32_t v371 = svsub_f32_x(svptrue_b32(), v148, v368);
  svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v148, v368);
  svfloat32_t v712 = svcmla_f32_x(pred_full, v692, v1718, v504, 90);
  svfloat32_t v713 = svcmla_f32_x(pred_full, v704, v1634, v639, 90);
  svfloat32_t zero793 = svdup_n_f32(0);
  svfloat32_t v793 = svcmla_f32_x(pred_full, zero793, v1801, v786, 90);
  svfloat32_t v794 = svadd_f32_x(svptrue_b32(), v330, v785);
  svfloat32_t v795 = svsub_f32_x(svptrue_b32(), v330, v785);
  svfloat32_t v834 = svmul_f32_x(svptrue_b32(), v506, v1633);
  svfloat32_t v846 = svmul_f32_x(svptrue_b32(), v641, v1635);
  svfloat32_t zero935 = svdup_n_f32(0);
  svfloat32_t v935 = svcmla_f32_x(pred_full, zero935, v1801, v928, 90);
  svfloat32_t v936 = svadd_f32_x(svptrue_b32(), v254, v927);
  svfloat32_t v937 = svsub_f32_x(svptrue_b32(), v254, v927);
  svfloat32_t v996 = svcmla_f32_x(pred_full, v976, v1716, v505, 90);
  svfloat32_t v997 = svcmla_f32_x(pred_full, v988, v1718, v640, 90);
  svfloat32_t zero1077 = svdup_n_f32(0);
  svfloat32_t v1077 = svcmla_f32_x(pred_full, zero1077, v1801, v1070, 90);
  svfloat32_t v1118 = svmul_f32_x(svptrue_b32(), v507, v1797);
  svfloat32_t v1130 = svmul_f32_x(svptrue_b32(), v642, v1799);
  svst1w_u64(pred_full, (unsigned *)(v1530), svreinterpret_u64_s16(v666));
  svst1w_u64(pred_full, (unsigned *)(v1548), svreinterpret_u64_s16(v682));
  svfloat32_t v714 = svadd_f32_x(svptrue_b32(), v712, v713);
  svfloat32_t v715 = svsub_f32_x(svptrue_b32(), v713, v712);
  svfloat32_t v796 = svsub_f32_x(svptrue_b32(), v331, v793);
  svfloat32_t v797 = svadd_f32_x(svptrue_b32(), v331, v793);
  svint16_t v800 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v794, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v816 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v795, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v854 = svcmla_f32_x(pred_full, v834, v1634, v506, 90);
  svfloat32_t v855 = svcmla_f32_x(pred_full, v846, v1798, v641, 90);
  svfloat32_t v938 = svsub_f32_x(svptrue_b32(), v255, v935);
  svfloat32_t v939 = svadd_f32_x(svptrue_b32(), v255, v935);
  svint16_t v942 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v936, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v958 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v937, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v998 = svadd_f32_x(svptrue_b32(), v996, v997);
  svfloat32_t v999 = svsub_f32_x(svptrue_b32(), v997, v996);
  svfloat32_t v1078 = svadd_f32_x(svptrue_b32(), v332, v1069);
  svfloat32_t v1079 = svsub_f32_x(svptrue_b32(), v332, v1069);
  svfloat32_t v1080 = svsub_f32_x(svptrue_b32(), v333, v1077);
  svfloat32_t v1081 = svadd_f32_x(svptrue_b32(), v333, v1077);
  svfloat32_t v1138 = svcmla_f32_x(pred_full, v1118, v1798, v507, 90);
  svfloat32_t v1139 = svcmla_f32_x(pred_full, v1130, v1800, v642, 90);
  svfloat32_t zero722 = svdup_n_f32(0);
  svfloat32_t v722 = svcmla_f32_x(pred_full, zero722, v1801, v715, 90);
  svfloat32_t v723 = svadd_f32_x(svptrue_b32(), v291, v714);
  svfloat32_t v724 = svsub_f32_x(svptrue_b32(), v291, v714);
  svint16_t v808 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v796, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v824 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v797, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v856 = svadd_f32_x(svptrue_b32(), v854, v855);
  svfloat32_t v857 = svsub_f32_x(svptrue_b32(), v855, v854);
  svint16_t v950 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v938, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v966 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v939, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t zero1006 = svdup_n_f32(0);
  svfloat32_t v1006 = svcmla_f32_x(pred_full, zero1006, v1801, v999, 90);
  svfloat32_t v1007 = svadd_f32_x(svptrue_b32(), v293, v998);
  svfloat32_t v1008 = svsub_f32_x(svptrue_b32(), v293, v998);
  svint16_t v1084 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1078, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1092 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1080, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1100 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1079, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1108 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1081, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1140 = svadd_f32_x(svptrue_b32(), v1138, v1139);
  svfloat32_t v1141 = svsub_f32_x(svptrue_b32(), v1139, v1138);
  svst1w_u64(pred_full, (unsigned *)(v1603), svreinterpret_u64_s16(v800));
  svst1w_u64(pred_full, (unsigned *)(v1621), svreinterpret_u64_s16(v816));
  svst1w_u64(pred_full, (unsigned *)(v1685), svreinterpret_u64_s16(v942));
  svst1w_u64(pred_full, (unsigned *)(v1703), svreinterpret_u64_s16(v958));
  svfloat32_t v725 = svsub_f32_x(svptrue_b32(), v292, v722);
  svfloat32_t v726 = svadd_f32_x(svptrue_b32(), v292, v722);
  svint16_t v729 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v723, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v745 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v724, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t zero864 = svdup_n_f32(0);
  svfloat32_t v864 = svcmla_f32_x(pred_full, zero864, v1801, v857, 90);
  svfloat32_t v865 = svadd_f32_x(svptrue_b32(), v369, v856);
  svfloat32_t v866 = svsub_f32_x(svptrue_b32(), v369, v856);
  svfloat32_t v1009 = svsub_f32_x(svptrue_b32(), v294, v1006);
  svfloat32_t v1010 = svadd_f32_x(svptrue_b32(), v294, v1006);
  svint16_t v1013 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1007, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1029 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1008, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t zero1148 = svdup_n_f32(0);
  svfloat32_t v1148 = svcmla_f32_x(pred_full, zero1148, v1801, v1141, 90);
  svfloat32_t v1149 = svadd_f32_x(svptrue_b32(), v371, v1140);
  svfloat32_t v1150 = svsub_f32_x(svptrue_b32(), v371, v1140);
  svst1w_u64(pred_full, (unsigned *)(v1612), svreinterpret_u64_s16(v808));
  svst1w_u64(pred_full, (unsigned *)(v1630), svreinterpret_u64_s16(v824));
  svst1w_u64(pred_full, (unsigned *)(v1694), svreinterpret_u64_s16(v950));
  svst1w_u64(pred_full, (unsigned *)(v1712), svreinterpret_u64_s16(v966));
  svst1w_u64(pred_full, (unsigned *)(v1767), svreinterpret_u64_s16(v1084));
  svst1w_u64(pred_full, (unsigned *)(v1776), svreinterpret_u64_s16(v1092));
  svst1w_u64(pred_full, (unsigned *)(v1785), svreinterpret_u64_s16(v1100));
  svst1w_u64(pred_full, (unsigned *)(v1794), svreinterpret_u64_s16(v1108));
  svint16_t v737 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v725, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v753 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v726, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v867 = svsub_f32_x(svptrue_b32(), v370, v864);
  svfloat32_t v868 = svadd_f32_x(svptrue_b32(), v370, v864);
  svint16_t v871 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v865, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v887 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v866, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1021 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1009, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1037 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1010, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svfloat32_t v1151 = svsub_f32_x(svptrue_b32(), v372, v1148);
  svfloat32_t v1152 = svadd_f32_x(svptrue_b32(), v372, v1148);
  svint16_t v1155 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1149, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1171 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1150, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v1562), svreinterpret_u64_s16(v729));
  svst1w_u64(pred_full, (unsigned *)(v1580), svreinterpret_u64_s16(v745));
  svst1w_u64(pred_full, (unsigned *)(v1726), svreinterpret_u64_s16(v1013));
  svst1w_u64(pred_full, (unsigned *)(v1744), svreinterpret_u64_s16(v1029));
  svint16_t v879 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v867, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v895 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v868, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1163 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1151, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svint16_t v1179 = svtbl_s16(
      svreinterpret_s16_s32(svcvt_s32_f32_x(
          pred_full, svmul_n_f32_x(pred_full, v1152, (float)(1ULL << 31ULL)))),
      svreinterpret_u16_u64(
          svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
  svst1w_u64(pred_full, (unsigned *)(v1571), svreinterpret_u64_s16(v737));
  svst1w_u64(pred_full, (unsigned *)(v1589), svreinterpret_u64_s16(v753));
  svst1w_u64(pred_full, (unsigned *)(v1644), svreinterpret_u64_s16(v871));
  svst1w_u64(pred_full, (unsigned *)(v1662), svreinterpret_u64_s16(v887));
  svst1w_u64(pred_full, (unsigned *)(v1735), svreinterpret_u64_s16(v1021));
  svst1w_u64(pred_full, (unsigned *)(v1753), svreinterpret_u64_s16(v1037));
  svst1w_u64(pred_full, (unsigned *)(v1808), svreinterpret_u64_s16(v1155));
  svst1w_u64(pred_full, (unsigned *)(v1826), svreinterpret_u64_s16(v1171));
  svst1w_u64(pred_full, (unsigned *)(v1653), svreinterpret_u64_s16(v879));
  svst1w_u64(pred_full, (unsigned *)(v1671), svreinterpret_u64_s16(v895));
  svst1w_u64(pred_full, (unsigned *)(v1817), svreinterpret_u64_s16(v1163));
  svst1w_u64(pred_full, (unsigned *)(v1835), svreinterpret_u64_s16(v1179));
}
#endif
