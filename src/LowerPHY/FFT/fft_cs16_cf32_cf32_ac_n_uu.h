/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "fft_helper.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void(cs16_cf32_cf32_ac_n_uu_fft_t)(const armral_cmplx_int16_t *x,
                                           armral_cmplx_f32_t *y, int istride,
                                           int ostride, int howmany, float dir);

cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu7;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu9;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu11;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu13;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu14;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu15;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu16;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu17;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu18;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu19;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu20;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu21;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu22;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu24;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu25;
cs16_cf32_cf32_ac_n_uu_fft_t armral_fft_cs16_cf32_cf32_ac_n_uu32;

#ifdef __cplusplus
} // extern "C"
#endif