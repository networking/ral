/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

namespace armral::fft {

/**
 * Find generator of a group [1, N] under multiplication by brute-force.
 * Returns 0 if no generator can be found for the entire group (this will
 * be the case if a non-prime n is passed).
 */
int find_group_generator(int n);

/**
 * Find the inverse generator of a group, given the group generator gen.
 * The inverse generator \c i_gen is such that <tt>(gen^j * i_gen^j) % n =
 * 1</tt> In addition, for prime \c n, the inverse generator, generates the
 * group in reverse order to \c gen (always starting from 1). E.g., if
 * <tt>n = 5</tt> then \c 2 is a generator, generating the list
 * <tt>1, 2, 4, 3</tt>. The inverse generator is \c 3 and this generates the
 * list <tt>1, 3, 4, 2</tt>.
 */
int find_inverse_mod_n(int gen, int n);

} // namespace armral::fft
