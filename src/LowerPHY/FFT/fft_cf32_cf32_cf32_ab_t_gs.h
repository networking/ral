/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "fft_helper.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void(cf32_cf32_cf32_ab_t_gs_fft_t)(const armral_cmplx_f32_t *x,
                                           armral_cmplx_f32_t *y, int istride,
                                           int ostride,
                                           const armral_cmplx_f32_t *w,
                                           int howmany, int idist, int odist,
                                           float dir);

cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs2;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs3;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs4;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs5;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs6;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs7;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs8;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs9;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs10;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs11;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs12;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs13;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs14;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs15;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs16;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs17;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs18;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs19;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs20;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs21;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs22;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs24;
cf32_cf32_cf32_ab_t_gs_fft_t armral_fft_cf32_cf32_cf32_ab_t_gs25;

#ifdef __cplusplus
} // extern "C"
#endif