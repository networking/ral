/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "bluestein.hpp"
#include "fft_types.hpp"
#include "rader.hpp"

#include <cassert>
#include <cstdlib>
#include <utility>

namespace armral::fft {

template<typename Tx, typename Ty, typename Tw>
struct rader;

template<typename Tx, typename Ty, typename Tw>
struct bluestein;

struct lev_base_t {
  int n;
  int n1;
  int n2;
  int how_many;
  armral_fft_direction_t dir;

  lev_base_t() = delete;
  lev_base_t(const lev_base_t &other) = delete;

  lev_base_t(int n_in, int n1_in, int n2_in, int how_many_in,
             armral_fft_direction_t dir_in)
    : n(n_in), n1(n1_in), n2(n2_in), how_many(how_many_in), dir(dir_in) {}

  virtual ~lev_base_t() = default;

  // GCOVR_EXCL_START
  void operator delete(void *ptr) noexcept {
    // For some reason the compilers complain about symbol not found in the
    // case that we do a debug build. We should not end up in the operator
    // delete for the base case, so we simply add an assert in here
    assert(false &&
           "Operator delete in lev_base_t called. This should not be possible");
  }

  // GCOVR_EXCL_STOP
};

template<typename Tx, typename Ty, typename Tw>
struct lev_t : public lev_base_t {
  Tw *twids;
  fft_ac_uu_func_t<Tx, Ty, Tw> kernel;
  // The only time that we have a gu kernel is in the case that
  // we perform a Rader's, and decompose into a 2D problem. At this point we are
  // already in the working type, so we have input, output and working type all
  // the same
  fft_ac_gu_func_t<Tw, Tw, Tw> ac_gu_kernel;
  fft_ab_twid_gu_func_t<Tx, Ty, Tw> ab_twid_gu_kernel;
  fft_ab_twid_gs_func_t<Tx, Ty, Tw> ab_twid_gs_kernel;
  fft_ac_twid_func_t<Tx, Ty, Tw> ac_twid_kernel;
  rader<Tx, Ty, Tw> r;
  bluestein<Tx, Ty, Tw> bs;

  lev_t() = delete;

  lev_t(int n_in, int n1_in, int n2_in, int how_many_in,
        armral_fft_direction_t dir_in, Tw *twids_in,
        fft_ac_uu_func_t<Tx, Ty, Tw> kernel_in,
        fft_ac_gu_func_t<Tw, Tw, Tw> ac_gu_kernel_in,
        fft_ab_twid_gu_func_t<Tx, Ty, Tw> ab_twid_gu_kernel_in,
        fft_ab_twid_gs_func_t<Tx, Ty, Tw> ab_twid_gs_kernel_in,
        fft_ac_twid_func_t<Tx, Ty, Tw> ac_twid_kernel_in,
        rader<Tx, Ty, Tw> r_in, bluestein<Tx, Ty, Tw> bs_in)
    : lev_base_t(n_in, n1_in, n2_in, how_many_in, dir_in), twids(twids_in),
      kernel(kernel_in), ac_gu_kernel(ac_gu_kernel_in),
      ab_twid_gu_kernel(ab_twid_gu_kernel_in),
      ab_twid_gs_kernel(ab_twid_gs_kernel_in),
      ac_twid_kernel(ac_twid_kernel_in), r(std::move(r_in)),
      bs(std::move(bs_in)) {}

  ~lev_t() override;

  void *operator new(size_t s) noexcept {
    return malloc(s);
  }

  void operator delete(void *ptr) noexcept {
    free(ptr);
  }
};

} // namespace armral::fft
