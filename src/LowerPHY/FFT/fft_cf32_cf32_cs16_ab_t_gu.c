/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "fft_cf32_cf32_cs16_ab_t_gu.h"

#include <arm_neon.h>
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu2(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    float32x2_t v51 = v5[0];
    float32x2_t v38 = v7[j * 2];
    int64_t v42 = j * 2 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v52 = vadd_f32(v51, v46);
    float32x2_t v53 = vsub_f32(v51, v46);
    int16x4_t v64 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v52, 15), (int32x2_t){0, 0}));
    int16x4_t v70 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v53, 15), (int32x2_t){0, 0}));
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v64), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v70), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu2(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    const float32x2_t *v80 = &v5[v0];
    int32_t *v113 = &v6[v2];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v13]));
    const float32x2_t *v92 = &v5[0];
    svint64_t v93 = svindex_s64(0, v1);
    int32_t *v104 = &v6[0];
    svfloat32_t v82 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v80), v93));
    svfloat32_t v94 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v92), v93));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero38, v82, v37, 0), v82, v37, 90);
    svfloat32_t v46 = svadd_f32_x(svptrue_b32(), v94, v38);
    svfloat32_t v47 = svsub_f32_x(svptrue_b32(), v94, v38);
    svint16_t v60 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v46, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v68 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v47, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v104), svreinterpret_u64_s16(v60));
    svst1w_u64(pred_full, (unsigned *)(v113), svreinterpret_u64_s16(v68));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu3(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v91 = -1.4999999999999998e+00F;
    float v94 = 8.6602540378443871e-01F;
    float v95 = -8.6602540378443871e-01F;
    float32x2_t v97 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v84 = v5[0];
    float32x2_t v92 = (float32x2_t){v91, v91};
    float32x2_t v96 = (float32x2_t){v94, v95};
    float32x2_t v38 = v5[istride * 2];
    float32x2_t v56 = v7[j * 4];
    int64_t v60 = j * 4 + 1;
    int64_t v68 = 2 + j * 4;
    float32x2_t v98 = vmul_f32(v97, v96);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v78 = vadd_f32(v64, v77);
    float32x2_t v79 = vsub_f32(v64, v77);
    float32x2_t v85 = vadd_f32(v78, v84);
    float32x2_t v93 = vmul_f32(v78, v92);
    float32x2_t v99 = vrev64_f32(v79);
    float32x2_t v100 = vmul_f32(v99, v98);
    float32x2_t v101 = vadd_f32(v85, v93);
    int16x4_t v106 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v85, 15), (int32x2_t){0, 0}));
    float32x2_t v102 = vadd_f32(v101, v100);
    float32x2_t v103 = vsub_f32(v101, v100);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v106), 0);
    int16x4_t v112 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v103, 15), (int32x2_t){0, 0}));
    int16x4_t v118 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v102, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v112), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v118), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu3(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v76 = -1.4999999999999998e+00F;
    float v81 = -8.6602540378443871e-01F;
    const float32x2_t *v120 = &v5[v0];
    int32_t *v163 = &v6[v2];
    int64_t v33 = v0 * 2;
    int64_t v56 = v13 * 2;
    float v84 = v4 * v81;
    int64_t v107 = v2 * 2;
    const float32x2_t *v141 = &v5[0];
    svint64_t v142 = svindex_s64(0, v1);
    svfloat32_t v145 = svdup_n_f32(v76);
    int32_t *v154 = &v6[0];
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v56]));
    int64_t v57 = v10 + v56;
    svfloat32_t v122 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v120), v142));
    const float32x2_t *v130 = &v5[v33];
    svfloat32_t v143 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v141), v142));
    svfloat32_t v146 = svdup_n_f32(v84);
    int32_t *v172 = &v6[v107];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v122, v51, 0),
                     v122, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v132 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v130), v142));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v132, v58, 0),
                     v132, v58, 90);
    svfloat32_t v60 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v61 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v69 = svadd_f32_x(svptrue_b32(), v60, v143);
    svfloat32_t zero86 = svdup_n_f32(0);
    svfloat32_t v86 = svcmla_f32_x(pred_full, zero86, v146, v61, 90);
    svfloat32_t v87 = svmla_f32_x(pred_full, v69, v60, v145);
    svint16_t v92 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v69, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v88 = svadd_f32_x(svptrue_b32(), v87, v86);
    svfloat32_t v89 = svsub_f32_x(svptrue_b32(), v87, v86);
    svst1w_u64(pred_full, (unsigned *)(v154), svreinterpret_u64_s16(v92));
    svint16_t v100 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v89, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v108 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v88, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v163), svreinterpret_u64_s16(v100));
    svst1w_u64(pred_full, (unsigned *)(v172), svreinterpret_u64_s16(v108));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu4(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v51 = v5[istride];
    float v132 = 1.0000000000000000e+00F;
    float v133 = -1.0000000000000000e+00F;
    float32x2_t v135 = (float32x2_t){v4, v4};
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    float32x2_t v113 = v5[0];
    float32x2_t v134 = (float32x2_t){v132, v133};
    float32x2_t v20 = v5[istride * 2];
    int64_t v37 = 2 + j * 6;
    float32x2_t v69 = v5[istride * 3];
    float32x2_t v87 = v7[j * 6];
    int64_t v91 = j * 6 + 1;
    int64_t v99 = 4 + j * 6;
    float32x2_t v136 = vmul_f32(v135, v134);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v114 = vadd_f32(v113, v46);
    float32x2_t v115 = vsub_f32(v113, v46);
    float32x2_t v116 = vadd_f32(v95, v108);
    float32x2_t v117 = vsub_f32(v95, v108);
    float32x2_t v118 = vadd_f32(v114, v116);
    float32x2_t v119 = vsub_f32(v114, v116);
    float32x2_t v137 = vrev64_f32(v117);
    float32x2_t v138 = vmul_f32(v137, v136);
    int16x4_t v143 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v118, 15), (int32x2_t){0, 0}));
    int16x4_t v155 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v119, 15), (int32x2_t){0, 0}));
    float32x2_t v139 = vadd_f32(v115, v138);
    float32x2_t v140 = vsub_f32(v115, v138);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v143), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v155), 0);
    int16x4_t v149 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v140, 15), (int32x2_t){0, 0}));
    int16x4_t v161 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v139, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v149), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v161), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu4(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v110 = -1.0000000000000000e+00F;
    const float32x2_t *v165 = &v5[v0];
    int32_t *v209 = &v6[v2];
    int64_t v19 = v0 * 2;
    int64_t v54 = v0 * 3;
    int64_t v76 = v10 * 2;
    int64_t v77 = v13 * 3;
    float v113 = v4 * v110;
    int64_t v135 = v2 * 2;
    int64_t v143 = v2 * 3;
    const float32x2_t *v186 = &v5[0];
    svint64_t v187 = svindex_s64(0, v1);
    int32_t *v200 = &v6[0];
    int64_t v36 = v10 + v77;
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v77]));
    int64_t v78 = v76 + v77;
    const float32x2_t *v156 = &v5[v19];
    svfloat32_t v167 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v165), v187));
    const float32x2_t *v175 = &v5[v54];
    svfloat32_t v188 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v186), v187));
    svfloat32_t v192 = svdup_n_f32(v113);
    int32_t *v218 = &v6[v135];
    int32_t *v227 = &v6[v143];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v167, v72, 0),
                     v167, v72, 90);
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v158 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v156), v187));
    svfloat32_t v177 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v175), v187));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v158, v37, 0),
                     v158, v37, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v177, v79, 0),
                     v177, v79, 90);
    svfloat32_t v88 = svadd_f32_x(svptrue_b32(), v188, v38);
    svfloat32_t v89 = svsub_f32_x(svptrue_b32(), v188, v38);
    svfloat32_t v90 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v91 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v92 = svadd_f32_x(svptrue_b32(), v88, v90);
    svfloat32_t v93 = svsub_f32_x(svptrue_b32(), v88, v90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 = svcmla_f32_x(pred_full, zero115, v192, v91, 90);
    svfloat32_t v116 = svadd_f32_x(svptrue_b32(), v89, v115);
    svfloat32_t v117 = svsub_f32_x(svptrue_b32(), v89, v115);
    svint16_t v120 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v92, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v136 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v93, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v128 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v117, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v144 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v116, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v200), svreinterpret_u64_s16(v120));
    svst1w_u64(pred_full, (unsigned *)(v218), svreinterpret_u64_s16(v136));
    svst1w_u64(pred_full, (unsigned *)(v209), svreinterpret_u64_s16(v128));
    svst1w_u64(pred_full, (unsigned *)(v227), svreinterpret_u64_s16(v144));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu5(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v158 = -1.2500000000000000e+00F;
    float v162 = 5.5901699437494745e-01F;
    float v165 = 1.5388417685876268e+00F;
    float v166 = -1.5388417685876268e+00F;
    float v172 = 5.8778525229247325e-01F;
    float v173 = -5.8778525229247325e-01F;
    float v179 = 3.6327126400268028e-01F;
    float v180 = -3.6327126400268028e-01F;
    float32x2_t v182 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v151 = v5[0];
    float32x2_t v159 = (float32x2_t){v158, v158};
    float32x2_t v163 = (float32x2_t){v162, v162};
    float32x2_t v167 = (float32x2_t){v165, v166};
    float32x2_t v174 = (float32x2_t){v172, v173};
    float32x2_t v181 = (float32x2_t){v179, v180};
    float32x2_t v38 = v5[istride * 4];
    float32x2_t v56 = v7[j * 8];
    int64_t v60 = j * 8 + 1;
    int64_t v68 = 6 + j * 8;
    float32x2_t v82 = v5[istride * 3];
    float32x2_t v100 = v5[istride * 2];
    int64_t v117 = 4 + j * 8;
    int64_t v130 = 2 + j * 8;
    float32x2_t v169 = vmul_f32(v182, v167);
    float32x2_t v176 = vmul_f32(v182, v174);
    float32x2_t v183 = vmul_f32(v182, v181);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v140 = vadd_f32(v64, v77);
    float32x2_t v141 = vsub_f32(v64, v77);
    float32x2_t v142 = vadd_f32(v126, v139);
    float32x2_t v143 = vsub_f32(v126, v139);
    float32x2_t v144 = vadd_f32(v140, v142);
    float32x2_t v145 = vsub_f32(v140, v142);
    float32x2_t v146 = vadd_f32(v141, v143);
    float32x2_t v170 = vrev64_f32(v141);
    float32x2_t v184 = vrev64_f32(v143);
    float32x2_t v152 = vadd_f32(v144, v151);
    float32x2_t v160 = vmul_f32(v144, v159);
    float32x2_t v164 = vmul_f32(v145, v163);
    float32x2_t v171 = vmul_f32(v170, v169);
    float32x2_t v177 = vrev64_f32(v146);
    float32x2_t v185 = vmul_f32(v184, v183);
    float32x2_t v178 = vmul_f32(v177, v176);
    float32x2_t v186 = vadd_f32(v152, v160);
    int16x4_t v197 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v152, 15), (int32x2_t){0, 0}));
    float32x2_t v187 = vadd_f32(v186, v164);
    float32x2_t v188 = vsub_f32(v186, v164);
    float32x2_t v189 = vsub_f32(v171, v178);
    float32x2_t v190 = vadd_f32(v178, v185);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v197), 0);
    float32x2_t v191 = vadd_f32(v187, v189);
    float32x2_t v192 = vsub_f32(v187, v189);
    float32x2_t v193 = vadd_f32(v188, v190);
    float32x2_t v194 = vsub_f32(v188, v190);
    int16x4_t v203 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v192, 15), (int32x2_t){0, 0}));
    int16x4_t v209 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v194, 15), (int32x2_t){0, 0}));
    int16x4_t v215 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v193, 15), (int32x2_t){0, 0}));
    int16x4_t v221 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v191, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v203), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v209), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v215), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v221), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu5(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v123 = -1.2500000000000000e+00F;
    float v128 = 5.5901699437494745e-01F;
    float v133 = -1.5388417685876268e+00F;
    float v140 = -5.8778525229247325e-01F;
    float v147 = -3.6327126400268028e-01F;
    const float32x2_t *v208 = &v5[v0];
    int32_t *v272 = &v6[v2];
    int64_t v33 = v0 * 4;
    int64_t v55 = v10 * 3;
    int64_t v61 = v0 * 3;
    int64_t v75 = v0 * 2;
    int64_t v90 = v10 * 2;
    int64_t v98 = v13 * 4;
    float v136 = v4 * v133;
    float v143 = v4 * v140;
    float v150 = v4 * v147;
    int64_t v179 = v2 * 2;
    int64_t v187 = v2 * 3;
    int64_t v195 = v2 * 4;
    const float32x2_t *v247 = &v5[0];
    svint64_t v248 = svindex_s64(0, v1);
    svfloat32_t v251 = svdup_n_f32(v123);
    svfloat32_t v252 = svdup_n_f32(v128);
    int32_t *v263 = &v6[0];
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v98]));
    int64_t v57 = v55 + v98;
    int64_t v92 = v90 + v98;
    int64_t v99 = v10 + v98;
    svfloat32_t v210 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v208), v248));
    const float32x2_t *v218 = &v5[v33];
    const float32x2_t *v228 = &v5[v61];
    const float32x2_t *v237 = &v5[v75];
    svfloat32_t v249 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v247), v248));
    svfloat32_t v253 = svdup_n_f32(v136);
    svfloat32_t v254 = svdup_n_f32(v143);
    svfloat32_t v255 = svdup_n_f32(v150);
    int32_t *v281 = &v6[v179];
    int32_t *v290 = &v6[v187];
    int32_t *v299 = &v6[v195];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v210, v51, 0),
                     v210, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v220 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v218), v248));
    svfloat32_t v230 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v228), v248));
    svfloat32_t v239 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v237), v248));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v220, v58, 0),
                     v220, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v230, v93, 0),
                     v230, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v239, v100, 0),
                     v239, v100, 90);
    svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v104 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v105 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v106 = svadd_f32_x(svptrue_b32(), v102, v104);
    svfloat32_t v107 = svsub_f32_x(svptrue_b32(), v102, v104);
    svfloat32_t v108 = svadd_f32_x(svptrue_b32(), v103, v105);
    svfloat32_t zero138 = svdup_n_f32(0);
    svfloat32_t v138 = svcmla_f32_x(pred_full, zero138, v253, v103, 90);
    svfloat32_t v116 = svadd_f32_x(svptrue_b32(), v106, v249);
    svfloat32_t zero145 = svdup_n_f32(0);
    svfloat32_t v145 = svcmla_f32_x(pred_full, zero145, v254, v108, 90);
    svfloat32_t v153 = svmla_f32_x(pred_full, v116, v106, v251);
    svfloat32_t v156 = svsub_f32_x(svptrue_b32(), v138, v145);
    svfloat32_t v157 = svcmla_f32_x(pred_full, v145, v255, v105, 90);
    svint16_t v164 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v116, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v154 = svmla_f32_x(pred_full, v153, v107, v252);
    svfloat32_t v155 = svmls_f32_x(pred_full, v153, v107, v252);
    svst1w_u64(pred_full, (unsigned *)(v263), svreinterpret_u64_s16(v164));
    svfloat32_t v158 = svadd_f32_x(svptrue_b32(), v154, v156);
    svfloat32_t v159 = svsub_f32_x(svptrue_b32(), v154, v156);
    svfloat32_t v160 = svadd_f32_x(svptrue_b32(), v155, v157);
    svfloat32_t v161 = svsub_f32_x(svptrue_b32(), v155, v157);
    svint16_t v172 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v159, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v180 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v161, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v188 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v160, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v196 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v158, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v272), svreinterpret_u64_s16(v172));
    svst1w_u64(pred_full, (unsigned *)(v281), svreinterpret_u64_s16(v180));
    svst1w_u64(pred_full, (unsigned *)(v290), svreinterpret_u64_s16(v188));
    svst1w_u64(pred_full, (unsigned *)(v299), svreinterpret_u64_s16(v196));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu6(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v131 = v5[istride];
    float v211 = -1.4999999999999998e+00F;
    float v214 = 8.6602540378443871e-01F;
    float v215 = -8.6602540378443871e-01F;
    float32x2_t v217 = (float32x2_t){v4, v4};
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    float32x2_t v175 = v5[0];
    float32x2_t v212 = (float32x2_t){v211, v211};
    float32x2_t v216 = (float32x2_t){v214, v215};
    float32x2_t v20 = v5[istride * 3];
    int64_t v37 = 4 + j * 10;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 5];
    int64_t v86 = 2 + j * 10;
    int64_t v99 = 8 + j * 10;
    float32x2_t v113 = v5[istride * 4];
    int64_t v148 = 6 + j * 10;
    float32x2_t v162 = v7[j * 10];
    int64_t v166 = j * 10 + 1;
    float32x2_t v218 = vmul_f32(v217, v216);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v176 = vadd_f32(v175, v46);
    float32x2_t v177 = vsub_f32(v175, v46);
    float32x2_t v178 = vadd_f32(v95, v108);
    float32x2_t v179 = vsub_f32(v95, v108);
    float32x2_t v180 = vadd_f32(v157, v170);
    float32x2_t v181 = vsub_f32(v157, v170);
    float32x2_t v182 = vadd_f32(v178, v180);
    float32x2_t v183 = vsub_f32(v178, v180);
    float32x2_t v203 = vadd_f32(v179, v181);
    float32x2_t v204 = vsub_f32(v179, v181);
    float32x2_t v184 = vadd_f32(v182, v176);
    float32x2_t v192 = vmul_f32(v182, v212);
    float32x2_t v198 = vrev64_f32(v183);
    float32x2_t v205 = vadd_f32(v203, v177);
    float32x2_t v213 = vmul_f32(v203, v212);
    float32x2_t v219 = vrev64_f32(v204);
    float32x2_t v199 = vmul_f32(v198, v218);
    float32x2_t v200 = vadd_f32(v184, v192);
    float32x2_t v220 = vmul_f32(v219, v218);
    float32x2_t v221 = vadd_f32(v205, v213);
    int16x4_t v226 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v184, 15), (int32x2_t){0, 0}));
    int16x4_t v232 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v205, 15), (int32x2_t){0, 0}));
    float32x2_t v201 = vadd_f32(v200, v199);
    float32x2_t v202 = vsub_f32(v200, v199);
    float32x2_t v222 = vadd_f32(v221, v220);
    float32x2_t v223 = vsub_f32(v221, v220);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v226), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v232), 0);
    int16x4_t v238 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v202, 15), (int32x2_t){0, 0}));
    int16x4_t v244 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v223, 15), (int32x2_t){0, 0}));
    int16x4_t v250 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v201, 15), (int32x2_t){0, 0}));
    int16x4_t v256 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v222, 15), (int32x2_t){0, 0}));
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v238), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v244), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v250), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v256), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu6(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v168 = -1.4999999999999998e+00F;
    float v173 = -8.6602540378443871e-01F;
    const float32x2_t *v272 = &v5[v0];
    int32_t *v327 = &v6[v2];
    int64_t v19 = v0 * 3;
    int64_t v34 = v10 * 2;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 5;
    int64_t v76 = v10 * 4;
    int64_t v82 = v0 * 4;
    int64_t v111 = v10 * 3;
    int64_t v119 = v13 * 5;
    float v176 = v4 * v173;
    int64_t v191 = v2 * 3;
    int64_t v199 = v2 * 4;
    int64_t v215 = v2 * 2;
    int64_t v223 = v2 * 5;
    const float32x2_t *v284 = &v5[0];
    svint64_t v285 = svindex_s64(0, v1);
    svfloat32_t v291 = svdup_n_f32(v168);
    int32_t *v300 = &v6[0];
    int64_t v36 = v34 + v119;
    int64_t v71 = v10 + v119;
    int64_t v78 = v76 + v119;
    int64_t v113 = v111 + v119;
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v119]));
    const float32x2_t *v236 = &v5[v19];
    const float32x2_t *v245 = &v5[v40];
    const float32x2_t *v254 = &v5[v54];
    const float32x2_t *v263 = &v5[v82];
    svfloat32_t v274 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v272), v285));
    svfloat32_t v286 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v284), v285));
    svfloat32_t v292 = svdup_n_f32(v176);
    int32_t *v309 = &v6[v191];
    int32_t *v318 = &v6[v199];
    int32_t *v336 = &v6[v215];
    int32_t *v345 = &v6[v223];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v274, v121, 0),
                     v274, v121, 90);
    svfloat32_t v238 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v236), v285));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v245), v285));
    svfloat32_t v256 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v254), v285));
    svfloat32_t v265 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v263), v285));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v238, v37, 0),
                     v238, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v247, v72, 0),
                     v247, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v256, v79, 0),
                     v256, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v265, v114, 0),
                     v265, v114, 90);
    svfloat32_t v130 = svadd_f32_x(svptrue_b32(), v286, v38);
    svfloat32_t v131 = svsub_f32_x(svptrue_b32(), v286, v38);
    svfloat32_t v132 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v133 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v134 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v135 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v136 = svadd_f32_x(svptrue_b32(), v132, v134);
    svfloat32_t v137 = svsub_f32_x(svptrue_b32(), v132, v134);
    svfloat32_t v159 = svadd_f32_x(svptrue_b32(), v133, v135);
    svfloat32_t v160 = svsub_f32_x(svptrue_b32(), v133, v135);
    svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v136, v130);
    svfloat32_t zero155 = svdup_n_f32(0);
    svfloat32_t v155 = svcmla_f32_x(pred_full, zero155, v292, v137, 90);
    svfloat32_t v161 = svadd_f32_x(svptrue_b32(), v159, v131);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 = svcmla_f32_x(pred_full, zero178, v292, v160, 90);
    svfloat32_t v156 = svmla_f32_x(pred_full, v138, v136, v291);
    svfloat32_t v179 = svmla_f32_x(pred_full, v161, v159, v291);
    svint16_t v184 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v138, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v192 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v161, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v157 = svadd_f32_x(svptrue_b32(), v156, v155);
    svfloat32_t v158 = svsub_f32_x(svptrue_b32(), v156, v155);
    svfloat32_t v180 = svadd_f32_x(svptrue_b32(), v179, v178);
    svfloat32_t v181 = svsub_f32_x(svptrue_b32(), v179, v178);
    svst1w_u64(pred_full, (unsigned *)(v300), svreinterpret_u64_s16(v184));
    svst1w_u64(pred_full, (unsigned *)(v309), svreinterpret_u64_s16(v192));
    svint16_t v200 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v158, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v208 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v181, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v216 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v157, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v224 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v180, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v318), svreinterpret_u64_s16(v200));
    svst1w_u64(pred_full, (unsigned *)(v327), svreinterpret_u64_s16(v208));
    svst1w_u64(pred_full, (unsigned *)(v336), svreinterpret_u64_s16(v216));
    svst1w_u64(pred_full, (unsigned *)(v345), svreinterpret_u64_s16(v224));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu7(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v229 = -1.1666666666666665e+00F;
    float v233 = 7.9015646852540022e-01F;
    float v237 = 5.5854267289647742e-02F;
    float v241 = 7.3430220123575241e-01F;
    float v244 = 4.4095855184409838e-01F;
    float v245 = -4.4095855184409838e-01F;
    float v251 = 3.4087293062393137e-01F;
    float v252 = -3.4087293062393137e-01F;
    float v258 = -5.3396936033772524e-01F;
    float v259 = 5.3396936033772524e-01F;
    float v265 = 8.7484229096165667e-01F;
    float v266 = -8.7484229096165667e-01F;
    float32x2_t v268 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v214 = v5[0];
    float32x2_t v230 = (float32x2_t){v229, v229};
    float32x2_t v234 = (float32x2_t){v233, v233};
    float32x2_t v238 = (float32x2_t){v237, v237};
    float32x2_t v242 = (float32x2_t){v241, v241};
    float32x2_t v246 = (float32x2_t){v244, v245};
    float32x2_t v253 = (float32x2_t){v251, v252};
    float32x2_t v260 = (float32x2_t){v258, v259};
    float32x2_t v267 = (float32x2_t){v265, v266};
    float32x2_t v38 = v5[istride * 6];
    float32x2_t v56 = v7[j * 12];
    int64_t v60 = j * 12 + 1;
    int64_t v68 = 10 + j * 12;
    float32x2_t v82 = v5[istride * 4];
    float32x2_t v100 = v5[istride * 3];
    int64_t v117 = 6 + j * 12;
    int64_t v130 = 4 + j * 12;
    float32x2_t v144 = v5[istride * 2];
    float32x2_t v162 = v5[istride * 5];
    int64_t v179 = 2 + j * 12;
    int64_t v192 = 8 + j * 12;
    float32x2_t v248 = vmul_f32(v268, v246);
    float32x2_t v255 = vmul_f32(v268, v253);
    float32x2_t v262 = vmul_f32(v268, v260);
    float32x2_t v269 = vmul_f32(v268, v267);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v180 = v7[v179];
    float32x2_t v181 = vtrn1_f32(v144, v144);
    float32x2_t v182 = vtrn2_f32(v144, v144);
    int64_t v184 = v179 + 1;
    float32x2_t v193 = v7[v192];
    float32x2_t v194 = vtrn1_f32(v162, v162);
    float32x2_t v195 = vtrn2_f32(v162, v162);
    int64_t v197 = v192 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v185 = v7[v184];
    float32x2_t v186 = vmul_f32(v181, v180);
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vmul_f32(v194, v193);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v188 = vfma_f32(v186, v182, v185);
    float32x2_t v201 = vfma_f32(v199, v195, v198);
    float32x2_t v202 = vadd_f32(v64, v77);
    float32x2_t v203 = vsub_f32(v64, v77);
    float32x2_t v204 = vadd_f32(v126, v139);
    float32x2_t v205 = vsub_f32(v126, v139);
    float32x2_t v206 = vadd_f32(v188, v201);
    float32x2_t v207 = vsub_f32(v188, v201);
    float32x2_t v208 = vadd_f32(v202, v204);
    float32x2_t v216 = vsub_f32(v202, v204);
    float32x2_t v217 = vsub_f32(v204, v206);
    float32x2_t v218 = vsub_f32(v206, v202);
    float32x2_t v219 = vadd_f32(v203, v205);
    float32x2_t v221 = vsub_f32(v203, v205);
    float32x2_t v222 = vsub_f32(v205, v207);
    float32x2_t v223 = vsub_f32(v207, v203);
    float32x2_t v209 = vadd_f32(v208, v206);
    float32x2_t v220 = vadd_f32(v219, v207);
    float32x2_t v235 = vmul_f32(v216, v234);
    float32x2_t v239 = vmul_f32(v217, v238);
    float32x2_t v243 = vmul_f32(v218, v242);
    float32x2_t v256 = vrev64_f32(v221);
    float32x2_t v263 = vrev64_f32(v222);
    float32x2_t v270 = vrev64_f32(v223);
    float32x2_t v215 = vadd_f32(v209, v214);
    float32x2_t v231 = vmul_f32(v209, v230);
    float32x2_t v249 = vrev64_f32(v220);
    float32x2_t v257 = vmul_f32(v256, v255);
    float32x2_t v264 = vmul_f32(v263, v262);
    float32x2_t v271 = vmul_f32(v270, v269);
    float32x2_t v250 = vmul_f32(v249, v248);
    float32x2_t v272 = vadd_f32(v215, v231);
    int16x4_t v293 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v215, 15), (int32x2_t){0, 0}));
    float32x2_t v273 = vadd_f32(v272, v235);
    float32x2_t v275 = vsub_f32(v272, v235);
    float32x2_t v277 = vsub_f32(v272, v239);
    float32x2_t v279 = vadd_f32(v250, v257);
    float32x2_t v281 = vsub_f32(v250, v257);
    float32x2_t v283 = vsub_f32(v250, v264);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v293), 0);
    float32x2_t v274 = vadd_f32(v273, v239);
    float32x2_t v276 = vsub_f32(v275, v243);
    float32x2_t v278 = vadd_f32(v277, v243);
    float32x2_t v280 = vadd_f32(v279, v264);
    float32x2_t v282 = vsub_f32(v281, v271);
    float32x2_t v284 = vadd_f32(v283, v271);
    float32x2_t v285 = vadd_f32(v274, v280);
    float32x2_t v286 = vsub_f32(v274, v280);
    float32x2_t v287 = vadd_f32(v276, v282);
    float32x2_t v288 = vsub_f32(v276, v282);
    float32x2_t v289 = vadd_f32(v278, v284);
    float32x2_t v290 = vsub_f32(v278, v284);
    int16x4_t v299 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v286, 15), (int32x2_t){0, 0}));
    int16x4_t v305 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v288, 15), (int32x2_t){0, 0}));
    int16x4_t v311 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v289, 15), (int32x2_t){0, 0}));
    int16x4_t v317 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v290, 15), (int32x2_t){0, 0}));
    int16x4_t v323 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v287, 15), (int32x2_t){0, 0}));
    int16x4_t v329 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v285, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v299), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v305), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v311), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v317), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v323), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v329), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu7(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v174 = -1.1666666666666665e+00F;
    float v179 = 7.9015646852540022e-01F;
    float v184 = 5.5854267289647742e-02F;
    float v189 = 7.3430220123575241e-01F;
    float v194 = -4.4095855184409838e-01F;
    float v201 = -3.4087293062393137e-01F;
    float v208 = 5.3396936033772524e-01F;
    float v215 = -8.7484229096165667e-01F;
    const float32x2_t *v302 = &v5[v0];
    int32_t *v387 = &v6[v2];
    int64_t v33 = v0 * 6;
    int64_t v55 = v10 * 5;
    int64_t v61 = v0 * 4;
    int64_t v75 = v0 * 3;
    int64_t v90 = v10 * 3;
    int64_t v97 = v10 * 2;
    int64_t v103 = v0 * 2;
    int64_t v117 = v0 * 5;
    int64_t v139 = v10 * 4;
    int64_t v140 = v13 * 6;
    float v197 = v4 * v194;
    float v204 = v4 * v201;
    float v211 = v4 * v208;
    float v218 = v4 * v215;
    int64_t v257 = v2 * 2;
    int64_t v265 = v2 * 3;
    int64_t v273 = v2 * 4;
    int64_t v281 = v2 * 5;
    int64_t v289 = v2 * 6;
    const float32x2_t *v359 = &v5[0];
    svint64_t v360 = svindex_s64(0, v1);
    svfloat32_t v363 = svdup_n_f32(v174);
    svfloat32_t v364 = svdup_n_f32(v179);
    svfloat32_t v365 = svdup_n_f32(v184);
    svfloat32_t v366 = svdup_n_f32(v189);
    int32_t *v378 = &v6[0];
    svfloat32_t v51 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v140]));
    int64_t v57 = v55 + v140;
    int64_t v92 = v90 + v140;
    int64_t v99 = v97 + v140;
    int64_t v134 = v10 + v140;
    int64_t v141 = v139 + v140;
    svfloat32_t v304 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v302), v360));
    const float32x2_t *v312 = &v5[v33];
    const float32x2_t *v322 = &v5[v61];
    const float32x2_t *v331 = &v5[v75];
    const float32x2_t *v340 = &v5[v103];
    const float32x2_t *v349 = &v5[v117];
    svfloat32_t v361 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v359), v360));
    svfloat32_t v367 = svdup_n_f32(v197);
    svfloat32_t v368 = svdup_n_f32(v204);
    svfloat32_t v369 = svdup_n_f32(v211);
    svfloat32_t v370 = svdup_n_f32(v218);
    int32_t *v396 = &v6[v257];
    int32_t *v405 = &v6[v265];
    int32_t *v414 = &v6[v273];
    int32_t *v423 = &v6[v281];
    int32_t *v432 = &v6[v289];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v304, v51, 0),
                     v304, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v135 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v134]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v314 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v312), v360));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v322), v360));
    svfloat32_t v333 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v331), v360));
    svfloat32_t v342 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v340), v360));
    svfloat32_t v351 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v349), v360));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v314, v58, 0),
                     v314, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v324, v93, 0),
                     v324, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v333, v100, 0),
                     v333, v100, 90);
    svfloat32_t zero136 = svdup_n_f32(0);
    svfloat32_t v136 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero136, v342, v135, 0),
                     v342, v135, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero143, v351, v142, 0),
                     v351, v142, 90);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v146 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v147 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v149 = svsub_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v150 = svadd_f32_x(svptrue_b32(), v144, v146);
    svfloat32_t v160 = svsub_f32_x(svptrue_b32(), v144, v146);
    svfloat32_t v161 = svsub_f32_x(svptrue_b32(), v146, v148);
    svfloat32_t v162 = svsub_f32_x(svptrue_b32(), v148, v144);
    svfloat32_t v163 = svadd_f32_x(svptrue_b32(), v145, v147);
    svfloat32_t v165 = svsub_f32_x(svptrue_b32(), v145, v147);
    svfloat32_t v166 = svsub_f32_x(svptrue_b32(), v147, v149);
    svfloat32_t v167 = svsub_f32_x(svptrue_b32(), v149, v145);
    svfloat32_t v151 = svadd_f32_x(svptrue_b32(), v150, v148);
    svfloat32_t v164 = svadd_f32_x(svptrue_b32(), v163, v149);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 = svcmla_f32_x(pred_full, zero206, v368, v165, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 = svcmla_f32_x(pred_full, zero213, v369, v166, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 = svcmla_f32_x(pred_full, zero220, v370, v167, 90);
    svfloat32_t v159 = svadd_f32_x(svptrue_b32(), v151, v361);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 = svcmla_f32_x(pred_full, zero199, v367, v164, 90);
    svfloat32_t v221 = svmla_f32_x(pred_full, v159, v151, v363);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v230 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v232 = svsub_f32_x(svptrue_b32(), v199, v213);
    svint16_t v242 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v159, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v222 = svmla_f32_x(pred_full, v221, v160, v364);
    svfloat32_t v224 = svmls_f32_x(pred_full, v221, v160, v364);
    svfloat32_t v226 = svmls_f32_x(pred_full, v221, v161, v365);
    svfloat32_t v229 = svadd_f32_x(svptrue_b32(), v228, v213);
    svfloat32_t v231 = svsub_f32_x(svptrue_b32(), v230, v220);
    svfloat32_t v233 = svadd_f32_x(svptrue_b32(), v232, v220);
    svst1w_u64(pred_full, (unsigned *)(v378), svreinterpret_u64_s16(v242));
    svfloat32_t v223 = svmla_f32_x(pred_full, v222, v161, v365);
    svfloat32_t v225 = svmls_f32_x(pred_full, v224, v162, v366);
    svfloat32_t v227 = svmla_f32_x(pred_full, v226, v162, v366);
    svfloat32_t v234 = svadd_f32_x(svptrue_b32(), v223, v229);
    svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v223, v229);
    svfloat32_t v236 = svadd_f32_x(svptrue_b32(), v225, v231);
    svfloat32_t v237 = svsub_f32_x(svptrue_b32(), v225, v231);
    svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v227, v233);
    svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v227, v233);
    svint16_t v250 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v235, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v258 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v237, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v266 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v238, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v274 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v239, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v282 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v236, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v290 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v234, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v387), svreinterpret_u64_s16(v250));
    svst1w_u64(pred_full, (unsigned *)(v396), svreinterpret_u64_s16(v258));
    svst1w_u64(pred_full, (unsigned *)(v405), svreinterpret_u64_s16(v266));
    svst1w_u64(pred_full, (unsigned *)(v414), svreinterpret_u64_s16(v274));
    svst1w_u64(pred_full, (unsigned *)(v423), svreinterpret_u64_s16(v282));
    svst1w_u64(pred_full, (unsigned *)(v432), svreinterpret_u64_s16(v290));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu8(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v113 = v5[istride];
    float v277 = 1.0000000000000000e+00F;
    float v278 = -1.0000000000000000e+00F;
    float v285 = -7.0710678118654746e-01F;
    float32x2_t v287 = (float32x2_t){v4, v4};
    float v292 = 7.0710678118654757e-01F;
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    float32x2_t v237 = v5[0];
    float32x2_t v279 = (float32x2_t){v277, v278};
    float32x2_t v286 = (float32x2_t){v292, v285};
    float32x2_t v293 = (float32x2_t){v292, v292};
    float32x2_t v20 = v5[istride * 4];
    int64_t v37 = 6 + j * 14;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 6];
    int64_t v86 = 2 + j * 14;
    int64_t v99 = 10 + j * 14;
    float32x2_t v131 = v5[istride * 5];
    float32x2_t v149 = v7[j * 14];
    int64_t v153 = j * 14 + 1;
    int64_t v161 = 8 + j * 14;
    float32x2_t v175 = v5[istride * 3];
    float32x2_t v193 = v5[istride * 7];
    int64_t v210 = 4 + j * 14;
    int64_t v223 = 12 + j * 14;
    float32x2_t v281 = vmul_f32(v287, v279);
    float32x2_t v288 = vmul_f32(v287, v286);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v238 = vadd_f32(v237, v46);
    float32x2_t v239 = vsub_f32(v237, v46);
    float32x2_t v240 = vadd_f32(v95, v108);
    float32x2_t v241 = vsub_f32(v95, v108);
    float32x2_t v242 = vadd_f32(v157, v170);
    float32x2_t v243 = vsub_f32(v157, v170);
    float32x2_t v244 = vadd_f32(v219, v232);
    float32x2_t v245 = vsub_f32(v219, v232);
    float32x2_t v246 = vadd_f32(v238, v240);
    float32x2_t v247 = vsub_f32(v238, v240);
    float32x2_t v248 = vadd_f32(v242, v244);
    float32x2_t v249 = vsub_f32(v242, v244);
    float32x2_t v252 = vadd_f32(v243, v245);
    float32x2_t v253 = vsub_f32(v243, v245);
    float32x2_t v282 = vrev64_f32(v241);
    float32x2_t v250 = vadd_f32(v246, v248);
    float32x2_t v251 = vsub_f32(v246, v248);
    float32x2_t v271 = vrev64_f32(v249);
    float32x2_t v283 = vmul_f32(v282, v281);
    float32x2_t v289 = vrev64_f32(v252);
    float32x2_t v294 = vmul_f32(v253, v293);
    float32x2_t v272 = vmul_f32(v271, v281);
    float32x2_t v290 = vmul_f32(v289, v288);
    float32x2_t v297 = vadd_f32(v239, v294);
    float32x2_t v298 = vsub_f32(v239, v294);
    int16x4_t v307 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v250, 15), (int32x2_t){0, 0}));
    int16x4_t v331 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v251, 15), (int32x2_t){0, 0}));
    float32x2_t v295 = vadd_f32(v247, v272);
    float32x2_t v296 = vsub_f32(v247, v272);
    float32x2_t v299 = vadd_f32(v283, v290);
    float32x2_t v300 = vsub_f32(v283, v290);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v307), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v331), 0);
    float32x2_t v301 = vadd_f32(v297, v299);
    float32x2_t v302 = vsub_f32(v297, v299);
    float32x2_t v303 = vadd_f32(v298, v300);
    float32x2_t v304 = vsub_f32(v298, v300);
    int16x4_t v319 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v296, 15), (int32x2_t){0, 0}));
    int16x4_t v343 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v295, 15), (int32x2_t){0, 0}));
    int16x4_t v313 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v302, 15), (int32x2_t){0, 0}));
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v319), 0);
    int16x4_t v325 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v303, 15), (int32x2_t){0, 0}));
    int16x4_t v337 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v304, 15), (int32x2_t){0, 0}));
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v343), 0);
    int16x4_t v349 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v301, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v313), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v325), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v337), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v349), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu8(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v216 = -1.0000000000000000e+00F;
    float v223 = -7.0710678118654746e-01F;
    float v230 = 7.0710678118654757e-01F;
    const float32x2_t *v341 = &v5[v0];
    int32_t *v407 = &v6[v2];
    int64_t v19 = v0 * 4;
    int64_t v34 = v10 * 3;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 6;
    int64_t v76 = v10 * 5;
    int64_t v96 = v0 * 5;
    int64_t v118 = v10 * 4;
    int64_t v124 = v0 * 3;
    int64_t v138 = v0 * 7;
    int64_t v153 = v10 * 2;
    int64_t v160 = v10 * 6;
    int64_t v161 = v13 * 7;
    float v219 = v4 * v216;
    float v226 = v4 * v223;
    int64_t v261 = v2 * 2;
    int64_t v269 = v2 * 3;
    int64_t v277 = v2 * 4;
    int64_t v285 = v2 * 5;
    int64_t v293 = v2 * 6;
    int64_t v301 = v2 * 7;
    const float32x2_t *v380 = &v5[0];
    svint64_t v381 = svindex_s64(0, v1);
    svfloat32_t v390 = svdup_n_f32(v230);
    int32_t *v398 = &v6[0];
    int64_t v36 = v34 + v161;
    int64_t v71 = v10 + v161;
    int64_t v78 = v76 + v161;
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v161]));
    int64_t v120 = v118 + v161;
    int64_t v155 = v153 + v161;
    int64_t v162 = v160 + v161;
    const float32x2_t *v314 = &v5[v19];
    const float32x2_t *v323 = &v5[v40];
    const float32x2_t *v332 = &v5[v54];
    svfloat32_t v343 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v341), v381));
    const float32x2_t *v351 = &v5[v96];
    const float32x2_t *v361 = &v5[v124];
    const float32x2_t *v370 = &v5[v138];
    svfloat32_t v382 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v380), v381));
    svfloat32_t v388 = svdup_n_f32(v219);
    svfloat32_t v389 = svdup_n_f32(v226);
    int32_t *v416 = &v6[v261];
    int32_t *v425 = &v6[v269];
    int32_t *v434 = &v6[v277];
    int32_t *v443 = &v6[v285];
    int32_t *v452 = &v6[v293];
    int32_t *v461 = &v6[v301];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v343, v114, 0),
                     v343, v114, 90);
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t v316 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v314), v381));
    svfloat32_t v325 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v323), v381));
    svfloat32_t v334 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v332), v381));
    svfloat32_t v353 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v351), v381));
    svfloat32_t v363 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v361), v381));
    svfloat32_t v372 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v370), v381));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v316, v37, 0),
                     v316, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v325, v72, 0),
                     v325, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v334, v79, 0),
                     v334, v79, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v353, v121, 0),
                     v353, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v363, v156, 0),
                     v363, v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v372, v163, 0),
                     v372, v163, 90);
    svfloat32_t v172 = svadd_f32_x(svptrue_b32(), v382, v38);
    svfloat32_t v173 = svsub_f32_x(svptrue_b32(), v382, v38);
    svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v175 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v176 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v178 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v179 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v180 = svadd_f32_x(svptrue_b32(), v172, v174);
    svfloat32_t v181 = svsub_f32_x(svptrue_b32(), v172, v174);
    svfloat32_t v182 = svadd_f32_x(svptrue_b32(), v176, v178);
    svfloat32_t v183 = svsub_f32_x(svptrue_b32(), v176, v178);
    svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v177, v179);
    svfloat32_t v187 = svsub_f32_x(svptrue_b32(), v177, v179);
    svfloat32_t zero221 = svdup_n_f32(0);
    svfloat32_t v221 = svcmla_f32_x(pred_full, zero221, v388, v175, 90);
    svfloat32_t v184 = svadd_f32_x(svptrue_b32(), v180, v182);
    svfloat32_t v185 = svsub_f32_x(svptrue_b32(), v180, v182);
    svfloat32_t zero209 = svdup_n_f32(0);
    svfloat32_t v209 = svcmla_f32_x(pred_full, zero209, v388, v183, 90);
    svfloat32_t zero228 = svdup_n_f32(0);
    svfloat32_t v228 = svcmla_f32_x(pred_full, zero228, v389, v186, 90);
    svfloat32_t v234 = svadd_f32_x(svptrue_b32(), v181, v209);
    svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v181, v209);
    svfloat32_t v236 = svmla_f32_x(pred_full, v173, v187, v390);
    svfloat32_t v237 = svmls_f32_x(pred_full, v173, v187, v390);
    svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v221, v228);
    svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v221, v228);
    svint16_t v246 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v184, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v278 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v185, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v236, v238);
    svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v236, v238);
    svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v237, v239);
    svfloat32_t v243 = svsub_f32_x(svptrue_b32(), v237, v239);
    svint16_t v262 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v235, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v294 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v234, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v398), svreinterpret_u64_s16(v246));
    svst1w_u64(pred_full, (unsigned *)(v434), svreinterpret_u64_s16(v278));
    svint16_t v254 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v241, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v270 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v242, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v286 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v243, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v302 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v240, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v416), svreinterpret_u64_s16(v262));
    svst1w_u64(pred_full, (unsigned *)(v452), svreinterpret_u64_s16(v294));
    svst1w_u64(pred_full, (unsigned *)(v407), svreinterpret_u64_s16(v254));
    svst1w_u64(pred_full, (unsigned *)(v425), svreinterpret_u64_s16(v270));
    svst1w_u64(pred_full, (unsigned *)(v443), svreinterpret_u64_s16(v286));
    svst1w_u64(pred_full, (unsigned *)(v461), svreinterpret_u64_s16(v302));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu9(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v294 = -5.0000000000000000e-01F;
    float v305 = -1.4999999999999998e+00F;
    float v308 = 8.6602540378443871e-01F;
    float v309 = -8.6602540378443871e-01F;
    float v316 = 7.6604444311897801e-01F;
    float v320 = 9.3969262078590832e-01F;
    float v324 = -1.7364817766693039e-01F;
    float v327 = 6.4278760968653925e-01F;
    float v328 = -6.4278760968653925e-01F;
    float v334 = -3.4202014332566888e-01F;
    float v335 = 3.4202014332566888e-01F;
    float v341 = 9.8480775301220802e-01F;
    float v342 = -9.8480775301220802e-01F;
    float32x2_t v344 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v279 = v5[0];
    float32x2_t v295 = (float32x2_t){v294, v294};
    float32x2_t v306 = (float32x2_t){v305, v305};
    float32x2_t v310 = (float32x2_t){v308, v309};
    float32x2_t v317 = (float32x2_t){v316, v316};
    float32x2_t v321 = (float32x2_t){v320, v320};
    float32x2_t v325 = (float32x2_t){v324, v324};
    float32x2_t v329 = (float32x2_t){v327, v328};
    float32x2_t v336 = (float32x2_t){v334, v335};
    float32x2_t v343 = (float32x2_t){v341, v342};
    float32x2_t v38 = v5[istride * 8];
    float32x2_t v56 = v7[j * 16];
    int64_t v60 = j * 16 + 1;
    int64_t v68 = 14 + j * 16;
    float32x2_t v82 = v5[istride * 7];
    float32x2_t v100 = v5[istride * 2];
    int64_t v117 = 12 + j * 16;
    int64_t v130 = 2 + j * 16;
    float32x2_t v144 = v5[istride * 3];
    float32x2_t v162 = v5[istride * 6];
    int64_t v179 = 4 + j * 16;
    int64_t v192 = 10 + j * 16;
    float32x2_t v206 = v5[istride * 4];
    float32x2_t v224 = v5[istride * 5];
    int64_t v241 = 6 + j * 16;
    int64_t v254 = 8 + j * 16;
    float32x2_t v312 = vmul_f32(v344, v310);
    float32x2_t v331 = vmul_f32(v344, v329);
    float32x2_t v338 = vmul_f32(v344, v336);
    float32x2_t v345 = vmul_f32(v344, v343);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v180 = v7[v179];
    float32x2_t v181 = vtrn1_f32(v144, v144);
    float32x2_t v182 = vtrn2_f32(v144, v144);
    int64_t v184 = v179 + 1;
    float32x2_t v193 = v7[v192];
    float32x2_t v194 = vtrn1_f32(v162, v162);
    float32x2_t v195 = vtrn2_f32(v162, v162);
    int64_t v197 = v192 + 1;
    float32x2_t v242 = v7[v241];
    float32x2_t v243 = vtrn1_f32(v206, v206);
    float32x2_t v244 = vtrn2_f32(v206, v206);
    int64_t v246 = v241 + 1;
    float32x2_t v255 = v7[v254];
    float32x2_t v256 = vtrn1_f32(v224, v224);
    float32x2_t v257 = vtrn2_f32(v224, v224);
    int64_t v259 = v254 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v185 = v7[v184];
    float32x2_t v186 = vmul_f32(v181, v180);
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vmul_f32(v194, v193);
    float32x2_t v247 = v7[v246];
    float32x2_t v248 = vmul_f32(v243, v242);
    float32x2_t v260 = v7[v259];
    float32x2_t v261 = vmul_f32(v256, v255);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v188 = vfma_f32(v186, v182, v185);
    float32x2_t v201 = vfma_f32(v199, v195, v198);
    float32x2_t v250 = vfma_f32(v248, v244, v247);
    float32x2_t v263 = vfma_f32(v261, v257, v260);
    float32x2_t v264 = vadd_f32(v64, v77);
    float32x2_t v265 = vsub_f32(v64, v77);
    float32x2_t v266 = vadd_f32(v126, v139);
    float32x2_t v267 = vsub_f32(v126, v139);
    float32x2_t v268 = vadd_f32(v188, v201);
    float32x2_t v269 = vsub_f32(v188, v201);
    float32x2_t v270 = vadd_f32(v250, v263);
    float32x2_t v271 = vsub_f32(v250, v263);
    float32x2_t v272 = vadd_f32(v264, v266);
    float32x2_t v281 = vadd_f32(v265, v267);
    float32x2_t v283 = vsub_f32(v264, v266);
    float32x2_t v284 = vsub_f32(v266, v270);
    float32x2_t v285 = vsub_f32(v270, v264);
    float32x2_t v286 = vsub_f32(v265, v267);
    float32x2_t v287 = vsub_f32(v267, v271);
    float32x2_t v288 = vsub_f32(v271, v265);
    float32x2_t v307 = vmul_f32(v268, v306);
    float32x2_t v313 = vrev64_f32(v269);
    float32x2_t v273 = vadd_f32(v272, v270);
    float32x2_t v282 = vadd_f32(v281, v271);
    float32x2_t v314 = vmul_f32(v313, v312);
    float32x2_t v318 = vmul_f32(v283, v317);
    float32x2_t v322 = vmul_f32(v284, v321);
    float32x2_t v326 = vmul_f32(v285, v325);
    float32x2_t v332 = vrev64_f32(v286);
    float32x2_t v339 = vrev64_f32(v287);
    float32x2_t v346 = vrev64_f32(v288);
    float32x2_t v274 = vadd_f32(v273, v268);
    float32x2_t v296 = vmul_f32(v273, v295);
    float32x2_t v302 = vrev64_f32(v282);
    float32x2_t v333 = vmul_f32(v332, v331);
    float32x2_t v340 = vmul_f32(v339, v338);
    float32x2_t v347 = vmul_f32(v346, v345);
    float32x2_t v280 = vadd_f32(v274, v279);
    float32x2_t v303 = vmul_f32(v302, v312);
    float32x2_t v348 = vadd_f32(v296, v296);
    float32x2_t v361 = vadd_f32(v314, v333);
    float32x2_t v363 = vsub_f32(v314, v340);
    float32x2_t v365 = vsub_f32(v314, v333);
    float32x2_t v349 = vadd_f32(v348, v296);
    float32x2_t v353 = vadd_f32(v280, v307);
    float32x2_t v362 = vadd_f32(v361, v340);
    float32x2_t v364 = vadd_f32(v363, v347);
    float32x2_t v366 = vsub_f32(v365, v347);
    int16x4_t v375 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v280, 15), (int32x2_t){0, 0}));
    float32x2_t v350 = vadd_f32(v280, v349);
    float32x2_t v354 = vadd_f32(v353, v348);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v375), 0);
    float32x2_t v351 = vadd_f32(v350, v303);
    float32x2_t v352 = vsub_f32(v350, v303);
    float32x2_t v355 = vadd_f32(v354, v318);
    float32x2_t v357 = vsub_f32(v354, v322);
    float32x2_t v359 = vsub_f32(v354, v318);
    float32x2_t v356 = vadd_f32(v355, v322);
    float32x2_t v358 = vadd_f32(v357, v326);
    float32x2_t v360 = vsub_f32(v359, v326);
    int16x4_t v393 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v352, 15), (int32x2_t){0, 0}));
    int16x4_t v411 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v351, 15), (int32x2_t){0, 0}));
    float32x2_t v367 = vadd_f32(v356, v362);
    float32x2_t v368 = vsub_f32(v356, v362);
    float32x2_t v369 = vadd_f32(v358, v364);
    float32x2_t v370 = vsub_f32(v358, v364);
    float32x2_t v371 = vadd_f32(v360, v366);
    float32x2_t v372 = vsub_f32(v360, v366);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v393), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v411), 0);
    int16x4_t v381 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v368, 15), (int32x2_t){0, 0}));
    int16x4_t v387 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v369, 15), (int32x2_t){0, 0}));
    int16x4_t v399 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v372, 15), (int32x2_t){0, 0}));
    int16x4_t v405 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v371, 15), (int32x2_t){0, 0}));
    int16x4_t v417 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v370, 15), (int32x2_t){0, 0}));
    int16x4_t v423 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v367, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v381), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v387), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v399), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v405), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v417), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v423), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu9(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v219 = -5.0000000000000000e-01F;
    float v231 = -1.4999999999999998e+00F;
    float v236 = -8.6602540378443871e-01F;
    float v243 = 7.6604444311897801e-01F;
    float v248 = 9.3969262078590832e-01F;
    float v253 = -1.7364817766693039e-01F;
    float v258 = -6.4278760968653925e-01F;
    float v265 = 3.4202014332566888e-01F;
    float v272 = -9.8480775301220802e-01F;
    const float32x2_t *v381 = &v5[v0];
    int32_t *v486 = &v6[v2];
    int64_t v33 = v0 * 8;
    int64_t v55 = v10 * 7;
    int64_t v61 = v0 * 7;
    int64_t v75 = v0 * 2;
    int64_t v90 = v10 * 6;
    int64_t v103 = v0 * 3;
    int64_t v117 = v0 * 6;
    int64_t v132 = v10 * 2;
    int64_t v139 = v10 * 5;
    int64_t v145 = v0 * 4;
    int64_t v159 = v0 * 5;
    int64_t v174 = v10 * 3;
    int64_t v181 = v10 * 4;
    int64_t v182 = v13 * 8;
    float v239 = v4 * v236;
    float v261 = v4 * v258;
    float v268 = v4 * v265;
    float v275 = v4 * v272;
    int64_t v320 = v2 * 2;
    int64_t v328 = v2 * 3;
    int64_t v336 = v2 * 4;
    int64_t v344 = v2 * 5;
    int64_t v352 = v2 * 6;
    int64_t v360 = v2 * 7;
    int64_t v368 = v2 * 8;
    const float32x2_t *v456 = &v5[0];
    svint64_t v457 = svindex_s64(0, v1);
    svfloat32_t v460 = svdup_n_f32(v219);
    svfloat32_t v462 = svdup_n_f32(v231);
    svfloat32_t v464 = svdup_n_f32(v243);
    svfloat32_t v465 = svdup_n_f32(v248);
    svfloat32_t v466 = svdup_n_f32(v253);
    int32_t *v477 = &v6[0];
    svfloat32_t v51 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v182]));
    int64_t v57 = v55 + v182;
    int64_t v92 = v90 + v182;
    int64_t v99 = v10 + v182;
    int64_t v134 = v132 + v182;
    int64_t v141 = v139 + v182;
    int64_t v176 = v174 + v182;
    int64_t v183 = v181 + v182;
    svfloat32_t v383 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v381), v457));
    const float32x2_t *v391 = &v5[v33];
    const float32x2_t *v401 = &v5[v61];
    const float32x2_t *v410 = &v5[v75];
    const float32x2_t *v419 = &v5[v103];
    const float32x2_t *v428 = &v5[v117];
    const float32x2_t *v437 = &v5[v145];
    const float32x2_t *v446 = &v5[v159];
    svfloat32_t v458 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v456), v457));
    svfloat32_t v463 = svdup_n_f32(v239);
    svfloat32_t v467 = svdup_n_f32(v261);
    svfloat32_t v468 = svdup_n_f32(v268);
    svfloat32_t v469 = svdup_n_f32(v275);
    int32_t *v495 = &v6[v320];
    int32_t *v504 = &v6[v328];
    int32_t *v513 = &v6[v336];
    int32_t *v522 = &v6[v344];
    int32_t *v531 = &v6[v352];
    int32_t *v540 = &v6[v360];
    int32_t *v549 = &v6[v368];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v383, v51, 0),
                     v383, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v135 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v134]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v177 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v176]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v393 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v391), v457));
    svfloat32_t v403 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v401), v457));
    svfloat32_t v412 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v410), v457));
    svfloat32_t v421 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v419), v457));
    svfloat32_t v430 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v428), v457));
    svfloat32_t v439 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v437), v457));
    svfloat32_t v448 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v446), v457));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v393, v58, 0),
                     v393, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v403, v93, 0),
                     v403, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v412, v100, 0),
                     v412, v100, 90);
    svfloat32_t zero136 = svdup_n_f32(0);
    svfloat32_t v136 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero136, v421, v135, 0),
                     v421, v135, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero143, v430, v142, 0),
                     v430, v142, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero178, v439, v177, 0),
                     v439, v177, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero185, v448, v184, 0),
                     v448, v184, 90);
    svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v187 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v188 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v189 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v190 = svadd_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v191 = svsub_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v194 = svadd_f32_x(svptrue_b32(), v186, v188);
    svfloat32_t v205 = svadd_f32_x(svptrue_b32(), v187, v189);
    svfloat32_t v207 = svsub_f32_x(svptrue_b32(), v186, v188);
    svfloat32_t v208 = svsub_f32_x(svptrue_b32(), v188, v192);
    svfloat32_t v209 = svsub_f32_x(svptrue_b32(), v192, v186);
    svfloat32_t v210 = svsub_f32_x(svptrue_b32(), v187, v189);
    svfloat32_t v211 = svsub_f32_x(svptrue_b32(), v189, v193);
    svfloat32_t v212 = svsub_f32_x(svptrue_b32(), v193, v187);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 = svcmla_f32_x(pred_full, zero241, v463, v191, 90);
    svfloat32_t v195 = svadd_f32_x(svptrue_b32(), v194, v192);
    svfloat32_t v206 = svadd_f32_x(svptrue_b32(), v205, v193);
    svfloat32_t zero263 = svdup_n_f32(0);
    svfloat32_t v263 = svcmla_f32_x(pred_full, zero263, v467, v210, 90);
    svfloat32_t zero270 = svdup_n_f32(0);
    svfloat32_t v270 = svcmla_f32_x(pred_full, zero270, v468, v211, 90);
    svfloat32_t zero277 = svdup_n_f32(0);
    svfloat32_t v277 = svcmla_f32_x(pred_full, zero277, v469, v212, 90);
    svfloat32_t v196 = svadd_f32_x(svptrue_b32(), v195, v190);
    svfloat32_t v222 = svmul_f32_x(svptrue_b32(), v195, v460);
    svfloat32_t zero229 = svdup_n_f32(0);
    svfloat32_t v229 = svcmla_f32_x(pred_full, zero229, v463, v206, 90);
    svfloat32_t v291 = svadd_f32_x(svptrue_b32(), v241, v263);
    svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v241, v270);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v241, v263);
    svfloat32_t v204 = svadd_f32_x(svptrue_b32(), v196, v458);
    svfloat32_t v278 = svadd_f32_x(svptrue_b32(), v222, v222);
    svfloat32_t v292 = svadd_f32_x(svptrue_b32(), v291, v270);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v293, v277);
    svfloat32_t v296 = svsub_f32_x(svptrue_b32(), v295, v277);
    svfloat32_t v279 = svmla_f32_x(pred_full, v278, v195, v460);
    svfloat32_t v283 = svmla_f32_x(pred_full, v204, v190, v462);
    svint16_t v305 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v204, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v280 = svadd_f32_x(svptrue_b32(), v204, v279);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v283, v278);
    svst1w_u64(pred_full, (unsigned *)(v477), svreinterpret_u64_s16(v305));
    svfloat32_t v281 = svadd_f32_x(svptrue_b32(), v280, v229);
    svfloat32_t v282 = svsub_f32_x(svptrue_b32(), v280, v229);
    svfloat32_t v285 = svmla_f32_x(pred_full, v284, v207, v464);
    svfloat32_t v287 = svmls_f32_x(pred_full, v284, v208, v465);
    svfloat32_t v289 = svmls_f32_x(pred_full, v284, v207, v464);
    svfloat32_t v286 = svmla_f32_x(pred_full, v285, v208, v465);
    svfloat32_t v288 = svmla_f32_x(pred_full, v287, v209, v466);
    svfloat32_t v290 = svmls_f32_x(pred_full, v289, v209, v466);
    svint16_t v329 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v282, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v353 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v281, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v297 = svadd_f32_x(svptrue_b32(), v286, v292);
    svfloat32_t v298 = svsub_f32_x(svptrue_b32(), v286, v292);
    svfloat32_t v299 = svadd_f32_x(svptrue_b32(), v288, v294);
    svfloat32_t v300 = svsub_f32_x(svptrue_b32(), v288, v294);
    svfloat32_t v301 = svadd_f32_x(svptrue_b32(), v290, v296);
    svfloat32_t v302 = svsub_f32_x(svptrue_b32(), v290, v296);
    svst1w_u64(pred_full, (unsigned *)(v504), svreinterpret_u64_s16(v329));
    svst1w_u64(pred_full, (unsigned *)(v531), svreinterpret_u64_s16(v353));
    svint16_t v313 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v298, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v321 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v299, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v337 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v302, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v345 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v301, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v361 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v300, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v369 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v297, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v486), svreinterpret_u64_s16(v313));
    svst1w_u64(pred_full, (unsigned *)(v495), svreinterpret_u64_s16(v321));
    svst1w_u64(pred_full, (unsigned *)(v513), svreinterpret_u64_s16(v337));
    svst1w_u64(pred_full, (unsigned *)(v522), svreinterpret_u64_s16(v345));
    svst1w_u64(pred_full, (unsigned *)(v540), svreinterpret_u64_s16(v361));
    svst1w_u64(pred_full, (unsigned *)(v549), svreinterpret_u64_s16(v369));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu10(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v193 = v5[istride];
    float v373 = -1.2500000000000000e+00F;
    float v377 = 5.5901699437494745e-01F;
    float v380 = 1.5388417685876268e+00F;
    float v381 = -1.5388417685876268e+00F;
    float v387 = 5.8778525229247325e-01F;
    float v388 = -5.8778525229247325e-01F;
    float v394 = 3.6327126400268028e-01F;
    float v395 = -3.6327126400268028e-01F;
    float32x2_t v397 = (float32x2_t){v4, v4};
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    float32x2_t v299 = v5[0];
    float32x2_t v374 = (float32x2_t){v373, v373};
    float32x2_t v378 = (float32x2_t){v377, v377};
    float32x2_t v382 = (float32x2_t){v380, v381};
    float32x2_t v389 = (float32x2_t){v387, v388};
    float32x2_t v396 = (float32x2_t){v394, v395};
    float32x2_t v20 = v5[istride * 5];
    int64_t v37 = 8 + j * 18;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 7];
    int64_t v86 = 2 + j * 18;
    int64_t v99 = 12 + j * 18;
    float32x2_t v113 = v5[istride * 4];
    float32x2_t v131 = v5[istride * 9];
    int64_t v148 = 6 + j * 18;
    int64_t v161 = 16 + j * 18;
    float32x2_t v175 = v5[istride * 6];
    int64_t v210 = 10 + j * 18;
    float32x2_t v224 = v7[j * 18];
    int64_t v228 = j * 18 + 1;
    float32x2_t v237 = v5[istride * 8];
    float32x2_t v255 = v5[istride * 3];
    int64_t v272 = 14 + j * 18;
    int64_t v285 = 4 + j * 18;
    float32x2_t v384 = vmul_f32(v397, v382);
    float32x2_t v391 = vmul_f32(v397, v389);
    float32x2_t v398 = vmul_f32(v397, v396);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v273 = v7[v272];
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    int64_t v277 = v272 + 1;
    float32x2_t v286 = v7[v285];
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    int64_t v290 = v285 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v300 = vadd_f32(v299, v46);
    float32x2_t v301 = vsub_f32(v299, v46);
    float32x2_t v302 = vadd_f32(v95, v108);
    float32x2_t v303 = vsub_f32(v95, v108);
    float32x2_t v304 = vadd_f32(v157, v170);
    float32x2_t v305 = vsub_f32(v157, v170);
    float32x2_t v306 = vadd_f32(v219, v232);
    float32x2_t v307 = vsub_f32(v219, v232);
    float32x2_t v308 = vadd_f32(v281, v294);
    float32x2_t v309 = vsub_f32(v281, v294);
    float32x2_t v310 = vadd_f32(v302, v308);
    float32x2_t v311 = vsub_f32(v302, v308);
    float32x2_t v312 = vadd_f32(v306, v304);
    float32x2_t v313 = vsub_f32(v306, v304);
    float32x2_t v360 = vadd_f32(v303, v309);
    float32x2_t v361 = vsub_f32(v303, v309);
    float32x2_t v362 = vadd_f32(v307, v305);
    float32x2_t v363 = vsub_f32(v307, v305);
    float32x2_t v314 = vadd_f32(v310, v312);
    float32x2_t v315 = vsub_f32(v310, v312);
    float32x2_t v316 = vadd_f32(v311, v313);
    float32x2_t v335 = vrev64_f32(v311);
    float32x2_t v349 = vrev64_f32(v313);
    float32x2_t v364 = vadd_f32(v360, v362);
    float32x2_t v365 = vsub_f32(v360, v362);
    float32x2_t v366 = vadd_f32(v361, v363);
    float32x2_t v385 = vrev64_f32(v361);
    float32x2_t v399 = vrev64_f32(v363);
    float32x2_t v317 = vadd_f32(v314, v300);
    float32x2_t v325 = vmul_f32(v314, v374);
    float32x2_t v329 = vmul_f32(v315, v378);
    float32x2_t v336 = vmul_f32(v335, v384);
    float32x2_t v342 = vrev64_f32(v316);
    float32x2_t v350 = vmul_f32(v349, v398);
    float32x2_t v367 = vadd_f32(v364, v301);
    float32x2_t v375 = vmul_f32(v364, v374);
    float32x2_t v379 = vmul_f32(v365, v378);
    float32x2_t v386 = vmul_f32(v385, v384);
    float32x2_t v392 = vrev64_f32(v366);
    float32x2_t v400 = vmul_f32(v399, v398);
    float32x2_t v343 = vmul_f32(v342, v391);
    float32x2_t v351 = vadd_f32(v317, v325);
    float32x2_t v393 = vmul_f32(v392, v391);
    float32x2_t v401 = vadd_f32(v367, v375);
    int16x4_t v412 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v317, 15), (int32x2_t){0, 0}));
    int16x4_t v418 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v367, 15), (int32x2_t){0, 0}));
    float32x2_t v352 = vadd_f32(v351, v329);
    float32x2_t v353 = vsub_f32(v351, v329);
    float32x2_t v354 = vsub_f32(v336, v343);
    float32x2_t v355 = vadd_f32(v343, v350);
    float32x2_t v402 = vadd_f32(v401, v379);
    float32x2_t v403 = vsub_f32(v401, v379);
    float32x2_t v404 = vsub_f32(v386, v393);
    float32x2_t v405 = vadd_f32(v393, v400);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v412), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v418), 0);
    float32x2_t v356 = vadd_f32(v352, v354);
    float32x2_t v357 = vsub_f32(v352, v354);
    float32x2_t v358 = vadd_f32(v353, v355);
    float32x2_t v359 = vsub_f32(v353, v355);
    float32x2_t v406 = vadd_f32(v402, v404);
    float32x2_t v407 = vsub_f32(v402, v404);
    float32x2_t v408 = vadd_f32(v403, v405);
    float32x2_t v409 = vsub_f32(v403, v405);
    int16x4_t v424 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v357, 15), (int32x2_t){0, 0}));
    int16x4_t v430 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v407, 15), (int32x2_t){0, 0}));
    int16x4_t v436 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v359, 15), (int32x2_t){0, 0}));
    int16x4_t v442 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v409, 15), (int32x2_t){0, 0}));
    int16x4_t v448 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v358, 15), (int32x2_t){0, 0}));
    int16x4_t v454 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v408, 15), (int32x2_t){0, 0}));
    int16x4_t v460 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v356, 15), (int32x2_t){0, 0}));
    int16x4_t v466 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v406, 15), (int32x2_t){0, 0}));
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v424), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v430), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v436), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v442), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v448), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v454), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v460), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v466), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu10(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v291 = -1.2500000000000000e+00F;
    float v296 = 5.5901699437494745e-01F;
    float v301 = -1.5388417685876268e+00F;
    float v308 = -5.8778525229247325e-01F;
    float v315 = -3.6327126400268028e-01F;
    const float32x2_t *v470 = &v5[v0];
    int32_t *v549 = &v6[v2];
    int64_t v19 = v0 * 5;
    int64_t v34 = v10 * 4;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 7;
    int64_t v76 = v10 * 6;
    int64_t v82 = v0 * 4;
    int64_t v96 = v0 * 9;
    int64_t v111 = v10 * 3;
    int64_t v118 = v10 * 8;
    int64_t v124 = v0 * 6;
    int64_t v153 = v10 * 5;
    int64_t v166 = v0 * 8;
    int64_t v180 = v0 * 3;
    int64_t v195 = v10 * 7;
    int64_t v202 = v10 * 2;
    int64_t v203 = v13 * 9;
    float v304 = v4 * v301;
    float v311 = v4 * v308;
    float v318 = v4 * v315;
    int64_t v339 = v2 * 5;
    int64_t v347 = v2 * 6;
    int64_t v363 = v2 * 2;
    int64_t v371 = v2 * 7;
    int64_t v379 = v2 * 8;
    int64_t v387 = v2 * 3;
    int64_t v395 = v2 * 4;
    int64_t v403 = v2 * 9;
    const float32x2_t *v500 = &v5[0];
    svint64_t v501 = svindex_s64(0, v1);
    svfloat32_t v510 = svdup_n_f32(v291);
    svfloat32_t v511 = svdup_n_f32(v296);
    int32_t *v522 = &v6[0];
    int64_t v36 = v34 + v203;
    int64_t v71 = v10 + v203;
    int64_t v78 = v76 + v203;
    int64_t v113 = v111 + v203;
    int64_t v120 = v118 + v203;
    int64_t v155 = v153 + v203;
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v203]));
    int64_t v197 = v195 + v203;
    int64_t v204 = v202 + v203;
    const float32x2_t *v416 = &v5[v19];
    const float32x2_t *v425 = &v5[v40];
    const float32x2_t *v434 = &v5[v54];
    const float32x2_t *v443 = &v5[v82];
    const float32x2_t *v452 = &v5[v96];
    const float32x2_t *v461 = &v5[v124];
    svfloat32_t v472 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v470), v501));
    const float32x2_t *v481 = &v5[v166];
    const float32x2_t *v490 = &v5[v180];
    svfloat32_t v502 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v500), v501));
    svfloat32_t v512 = svdup_n_f32(v304);
    svfloat32_t v513 = svdup_n_f32(v311);
    svfloat32_t v514 = svdup_n_f32(v318);
    int32_t *v531 = &v6[v339];
    int32_t *v540 = &v6[v347];
    int32_t *v558 = &v6[v363];
    int32_t *v567 = &v6[v371];
    int32_t *v576 = &v6[v379];
    int32_t *v585 = &v6[v387];
    int32_t *v594 = &v6[v395];
    int32_t *v603 = &v6[v403];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v472, v163, 0),
                     v472, v163, 90);
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v418 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v416), v501));
    svfloat32_t v427 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v425), v501));
    svfloat32_t v436 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v434), v501));
    svfloat32_t v445 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v443), v501));
    svfloat32_t v454 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v452), v501));
    svfloat32_t v463 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v461), v501));
    svfloat32_t v483 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v481), v501));
    svfloat32_t v492 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v490), v501));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v418, v37, 0),
                     v418, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v427, v72, 0),
                     v427, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v436, v79, 0),
                     v436, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v445, v114, 0),
                     v445, v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v454, v121, 0),
                     v454, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v463, v156, 0),
                     v463, v156, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v483, v198, 0),
                     v483, v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v492, v205, 0),
                     v492, v205, 90);
    svfloat32_t v214 = svadd_f32_x(svptrue_b32(), v502, v38);
    svfloat32_t v215 = svsub_f32_x(svptrue_b32(), v502, v38);
    svfloat32_t v216 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v217 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v218 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v219 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v220 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v221 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v222 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v223 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v224 = svadd_f32_x(svptrue_b32(), v216, v222);
    svfloat32_t v225 = svsub_f32_x(svptrue_b32(), v216, v222);
    svfloat32_t v226 = svadd_f32_x(svptrue_b32(), v220, v218);
    svfloat32_t v227 = svsub_f32_x(svptrue_b32(), v220, v218);
    svfloat32_t v277 = svadd_f32_x(svptrue_b32(), v217, v223);
    svfloat32_t v278 = svsub_f32_x(svptrue_b32(), v217, v223);
    svfloat32_t v279 = svadd_f32_x(svptrue_b32(), v221, v219);
    svfloat32_t v280 = svsub_f32_x(svptrue_b32(), v221, v219);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v224, v226);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v224, v226);
    svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v225, v227);
    svfloat32_t zero253 = svdup_n_f32(0);
    svfloat32_t v253 = svcmla_f32_x(pred_full, zero253, v512, v225, 90);
    svfloat32_t v281 = svadd_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t v282 = svsub_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t v283 = svadd_f32_x(svptrue_b32(), v278, v280);
    svfloat32_t zero306 = svdup_n_f32(0);
    svfloat32_t v306 = svcmla_f32_x(pred_full, zero306, v512, v278, 90);
    svfloat32_t v231 = svadd_f32_x(svptrue_b32(), v228, v214);
    svfloat32_t zero260 = svdup_n_f32(0);
    svfloat32_t v260 = svcmla_f32_x(pred_full, zero260, v513, v230, 90);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v281, v215);
    svfloat32_t zero313 = svdup_n_f32(0);
    svfloat32_t v313 = svcmla_f32_x(pred_full, zero313, v513, v283, 90);
    svfloat32_t v268 = svmla_f32_x(pred_full, v231, v228, v510);
    svfloat32_t v271 = svsub_f32_x(svptrue_b32(), v253, v260);
    svfloat32_t v272 = svcmla_f32_x(pred_full, v260, v514, v227, 90);
    svfloat32_t v321 = svmla_f32_x(pred_full, v284, v281, v510);
    svfloat32_t v324 = svsub_f32_x(svptrue_b32(), v306, v313);
    svfloat32_t v325 = svcmla_f32_x(pred_full, v313, v514, v280, 90);
    svint16_t v332 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v231, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v340 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v284, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v269 = svmla_f32_x(pred_full, v268, v229, v511);
    svfloat32_t v270 = svmls_f32_x(pred_full, v268, v229, v511);
    svfloat32_t v322 = svmla_f32_x(pred_full, v321, v282, v511);
    svfloat32_t v323 = svmls_f32_x(pred_full, v321, v282, v511);
    svst1w_u64(pred_full, (unsigned *)(v522), svreinterpret_u64_s16(v332));
    svst1w_u64(pred_full, (unsigned *)(v531), svreinterpret_u64_s16(v340));
    svfloat32_t v273 = svadd_f32_x(svptrue_b32(), v269, v271);
    svfloat32_t v274 = svsub_f32_x(svptrue_b32(), v269, v271);
    svfloat32_t v275 = svadd_f32_x(svptrue_b32(), v270, v272);
    svfloat32_t v276 = svsub_f32_x(svptrue_b32(), v270, v272);
    svfloat32_t v326 = svadd_f32_x(svptrue_b32(), v322, v324);
    svfloat32_t v327 = svsub_f32_x(svptrue_b32(), v322, v324);
    svfloat32_t v328 = svadd_f32_x(svptrue_b32(), v323, v325);
    svfloat32_t v329 = svsub_f32_x(svptrue_b32(), v323, v325);
    svint16_t v348 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v274, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v356 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v327, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v364 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v276, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v372 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v329, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v380 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v275, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v388 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v328, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v396 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v273, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v404 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v326, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v540), svreinterpret_u64_s16(v348));
    svst1w_u64(pred_full, (unsigned *)(v549), svreinterpret_u64_s16(v356));
    svst1w_u64(pred_full, (unsigned *)(v558), svreinterpret_u64_s16(v364));
    svst1w_u64(pred_full, (unsigned *)(v567), svreinterpret_u64_s16(v372));
    svst1w_u64(pred_full, (unsigned *)(v576), svreinterpret_u64_s16(v380));
    svst1w_u64(pred_full, (unsigned *)(v585), svreinterpret_u64_s16(v388));
    svst1w_u64(pred_full, (unsigned *)(v594), svreinterpret_u64_s16(v396));
    svst1w_u64(pred_full, (unsigned *)(v603), svreinterpret_u64_s16(v404));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu11(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v373 = 1.1000000000000001e+00F;
    float v376 = 3.3166247903554003e-01F;
    float v377 = -3.3166247903554003e-01F;
    float v384 = 5.1541501300188641e-01F;
    float v388 = 9.4125353283118118e-01F;
    float v392 = 1.4143537075597825e+00F;
    float v396 = 8.5949297361449750e-01F;
    float v400 = 4.2314838273285138e-02F;
    float v404 = 3.8639279888589606e-01F;
    float v408 = 5.1254589567200015e-01F;
    float v412 = 1.0702757469471715e+00F;
    float v416 = 5.5486073394528512e-01F;
    float v419 = 1.2412944743900585e+00F;
    float v420 = -1.2412944743900585e+00F;
    float v426 = 2.0897833842005756e-01F;
    float v427 = -2.0897833842005756e-01F;
    float v433 = 3.7415717312460811e-01F;
    float v434 = -3.7415717312460811e-01F;
    float v440 = 4.9929922194110327e-02F;
    float v441 = -4.9929922194110327e-02F;
    float v447 = 6.5815896284539266e-01F;
    float v448 = -6.5815896284539266e-01F;
    float v454 = 6.3306543373877577e-01F;
    float v455 = -6.3306543373877577e-01F;
    float v461 = 1.0822460581641109e+00F;
    float v462 = -1.0822460581641109e+00F;
    float v468 = 8.1720737907134022e-01F;
    float v469 = -8.1720737907134022e-01F;
    float v475 = 4.2408709531871824e-01F;
    float v476 = -4.2408709531871824e-01F;
    float32x2_t v478 = (float32x2_t){v4, v4};
    float32x2_t v201 = vtrn1_f32(v20, v20);
    float32x2_t v202 = vtrn2_f32(v20, v20);
    float32x2_t v346 = v5[0];
    float32x2_t v374 = (float32x2_t){v373, v373};
    float32x2_t v378 = (float32x2_t){v376, v377};
    float32x2_t v385 = (float32x2_t){v384, v384};
    float32x2_t v389 = (float32x2_t){v388, v388};
    float32x2_t v393 = (float32x2_t){v392, v392};
    float32x2_t v397 = (float32x2_t){v396, v396};
    float32x2_t v401 = (float32x2_t){v400, v400};
    float32x2_t v405 = (float32x2_t){v404, v404};
    float32x2_t v409 = (float32x2_t){v408, v408};
    float32x2_t v413 = (float32x2_t){v412, v412};
    float32x2_t v417 = (float32x2_t){v416, v416};
    float32x2_t v421 = (float32x2_t){v419, v420};
    float32x2_t v428 = (float32x2_t){v426, v427};
    float32x2_t v435 = (float32x2_t){v433, v434};
    float32x2_t v442 = (float32x2_t){v440, v441};
    float32x2_t v449 = (float32x2_t){v447, v448};
    float32x2_t v456 = (float32x2_t){v454, v455};
    float32x2_t v463 = (float32x2_t){v461, v462};
    float32x2_t v470 = (float32x2_t){v468, v469};
    float32x2_t v477 = (float32x2_t){v475, v476};
    float32x2_t v38 = v5[istride * 10];
    float32x2_t v56 = v5[istride * 2];
    float32x2_t v74 = v5[istride * 9];
    float32x2_t v92 = v5[istride * 3];
    float32x2_t v110 = v5[istride * 8];
    float32x2_t v128 = v5[istride * 4];
    float32x2_t v146 = v5[istride * 7];
    float32x2_t v164 = v5[istride * 5];
    float32x2_t v182 = v5[istride * 6];
    float32x2_t v200 = v7[j * 20];
    int64_t v204 = j * 20 + 1;
    int64_t v212 = 18 + j * 20;
    int64_t v225 = 2 + j * 20;
    int64_t v238 = 16 + j * 20;
    int64_t v251 = 4 + j * 20;
    int64_t v264 = 14 + j * 20;
    int64_t v277 = 6 + j * 20;
    int64_t v290 = 12 + j * 20;
    int64_t v303 = 8 + j * 20;
    int64_t v316 = 10 + j * 20;
    float32x2_t v380 = vmul_f32(v478, v378);
    float32x2_t v423 = vmul_f32(v478, v421);
    float32x2_t v430 = vmul_f32(v478, v428);
    float32x2_t v437 = vmul_f32(v478, v435);
    float32x2_t v444 = vmul_f32(v478, v442);
    float32x2_t v451 = vmul_f32(v478, v449);
    float32x2_t v458 = vmul_f32(v478, v456);
    float32x2_t v465 = vmul_f32(v478, v463);
    float32x2_t v472 = vmul_f32(v478, v470);
    float32x2_t v479 = vmul_f32(v478, v477);
    float32x2_t v205 = v7[v204];
    float32x2_t v206 = vmul_f32(v201, v200);
    float32x2_t v213 = v7[v212];
    float32x2_t v214 = vtrn1_f32(v38, v38);
    float32x2_t v215 = vtrn2_f32(v38, v38);
    int64_t v217 = v212 + 1;
    float32x2_t v226 = v7[v225];
    float32x2_t v227 = vtrn1_f32(v56, v56);
    float32x2_t v228 = vtrn2_f32(v56, v56);
    int64_t v230 = v225 + 1;
    float32x2_t v239 = v7[v238];
    float32x2_t v240 = vtrn1_f32(v74, v74);
    float32x2_t v241 = vtrn2_f32(v74, v74);
    int64_t v243 = v238 + 1;
    float32x2_t v252 = v7[v251];
    float32x2_t v253 = vtrn1_f32(v92, v92);
    float32x2_t v254 = vtrn2_f32(v92, v92);
    int64_t v256 = v251 + 1;
    float32x2_t v265 = v7[v264];
    float32x2_t v266 = vtrn1_f32(v110, v110);
    float32x2_t v267 = vtrn2_f32(v110, v110);
    int64_t v269 = v264 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vtrn1_f32(v128, v128);
    float32x2_t v280 = vtrn2_f32(v128, v128);
    int64_t v282 = v277 + 1;
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vtrn1_f32(v146, v146);
    float32x2_t v293 = vtrn2_f32(v146, v146);
    int64_t v295 = v290 + 1;
    float32x2_t v304 = v7[v303];
    float32x2_t v305 = vtrn1_f32(v164, v164);
    float32x2_t v306 = vtrn2_f32(v164, v164);
    int64_t v308 = v303 + 1;
    float32x2_t v317 = v7[v316];
    float32x2_t v318 = vtrn1_f32(v182, v182);
    float32x2_t v319 = vtrn2_f32(v182, v182);
    int64_t v321 = v316 + 1;
    float32x2_t v218 = v7[v217];
    float32x2_t v219 = vmul_f32(v214, v213);
    float32x2_t v231 = v7[v230];
    float32x2_t v232 = vmul_f32(v227, v226);
    float32x2_t v244 = v7[v243];
    float32x2_t v245 = vmul_f32(v240, v239);
    float32x2_t v257 = v7[v256];
    float32x2_t v258 = vmul_f32(v253, v252);
    float32x2_t v270 = v7[v269];
    float32x2_t v271 = vmul_f32(v266, v265);
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vmul_f32(v292, v291);
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vmul_f32(v305, v304);
    float32x2_t v322 = v7[v321];
    float32x2_t v323 = vmul_f32(v318, v317);
    float32x2_t v208 = vfma_f32(v206, v202, v205);
    float32x2_t v221 = vfma_f32(v219, v215, v218);
    float32x2_t v234 = vfma_f32(v232, v228, v231);
    float32x2_t v247 = vfma_f32(v245, v241, v244);
    float32x2_t v260 = vfma_f32(v258, v254, v257);
    float32x2_t v273 = vfma_f32(v271, v267, v270);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v299 = vfma_f32(v297, v293, v296);
    float32x2_t v312 = vfma_f32(v310, v306, v309);
    float32x2_t v325 = vfma_f32(v323, v319, v322);
    float32x2_t v326 = vadd_f32(v208, v221);
    float32x2_t v327 = vadd_f32(v234, v247);
    float32x2_t v328 = vadd_f32(v260, v273);
    float32x2_t v329 = vadd_f32(v286, v299);
    float32x2_t v330 = vadd_f32(v312, v325);
    float32x2_t v331 = vsub_f32(v208, v221);
    float32x2_t v332 = vsub_f32(v234, v247);
    float32x2_t v333 = vsub_f32(v260, v273);
    float32x2_t v334 = vsub_f32(v286, v299);
    float32x2_t v335 = vsub_f32(v312, v325);
    float32x2_t v336 = vadd_f32(v326, v327);
    float32x2_t v337 = vadd_f32(v328, v330);
    float32x2_t v339 = vsub_f32(v332, v333);
    float32x2_t v340 = vadd_f32(v331, v335);
    float32x2_t v350 = vsub_f32(v327, v329);
    float32x2_t v351 = vsub_f32(v326, v329);
    float32x2_t v352 = vsub_f32(v327, v326);
    float32x2_t v353 = vsub_f32(v330, v329);
    float32x2_t v354 = vsub_f32(v328, v329);
    float32x2_t v355 = vsub_f32(v330, v328);
    float32x2_t v356 = vsub_f32(v327, v330);
    float32x2_t v357 = vsub_f32(v326, v328);
    float32x2_t v359 = vadd_f32(v332, v334);
    float32x2_t v360 = vsub_f32(v331, v334);
    float32x2_t v361 = vadd_f32(v331, v332);
    float32x2_t v362 = vsub_f32(v334, v335);
    float32x2_t v363 = vsub_f32(v333, v334);
    float32x2_t v364 = vsub_f32(v333, v335);
    float32x2_t v365 = vadd_f32(v332, v335);
    float32x2_t v366 = vsub_f32(v331, v333);
    float32x2_t v338 = vadd_f32(v329, v336);
    float32x2_t v348 = vsub_f32(v339, v340);
    float32x2_t v358 = vsub_f32(v337, v336);
    float32x2_t v367 = vadd_f32(v339, v340);
    float32x2_t v386 = vmul_f32(v350, v385);
    float32x2_t v390 = vmul_f32(v351, v389);
    float32x2_t v394 = vmul_f32(v352, v393);
    float32x2_t v398 = vmul_f32(v353, v397);
    float32x2_t v402 = vmul_f32(v354, v401);
    float32x2_t v406 = vmul_f32(v355, v405);
    float32x2_t v410 = vmul_f32(v356, v409);
    float32x2_t v414 = vmul_f32(v357, v413);
    float32x2_t v424 = vrev64_f32(v359);
    float32x2_t v431 = vrev64_f32(v360);
    float32x2_t v438 = vrev64_f32(v361);
    float32x2_t v445 = vrev64_f32(v362);
    float32x2_t v452 = vrev64_f32(v363);
    float32x2_t v459 = vrev64_f32(v364);
    float32x2_t v466 = vrev64_f32(v365);
    float32x2_t v473 = vrev64_f32(v366);
    float32x2_t v341 = vadd_f32(v338, v337);
    float32x2_t v349 = vsub_f32(v348, v334);
    float32x2_t v418 = vmul_f32(v358, v417);
    float32x2_t v425 = vmul_f32(v424, v423);
    float32x2_t v432 = vmul_f32(v431, v430);
    float32x2_t v439 = vmul_f32(v438, v437);
    float32x2_t v446 = vmul_f32(v445, v444);
    float32x2_t v453 = vmul_f32(v452, v451);
    float32x2_t v460 = vmul_f32(v459, v458);
    float32x2_t v467 = vmul_f32(v466, v465);
    float32x2_t v474 = vmul_f32(v473, v472);
    float32x2_t v480 = vrev64_f32(v367);
    float32x2_t v483 = vadd_f32(v386, v390);
    float32x2_t v484 = vadd_f32(v390, v394);
    float32x2_t v485 = vsub_f32(v386, v394);
    float32x2_t v486 = vadd_f32(v398, v402);
    float32x2_t v487 = vadd_f32(v402, v406);
    float32x2_t v488 = vsub_f32(v398, v406);
    float32x2_t v347 = vadd_f32(v346, v341);
    float32x2_t v375 = vmul_f32(v341, v374);
    float32x2_t v381 = vrev64_f32(v349);
    float32x2_t v481 = vmul_f32(v480, v479);
    float32x2_t v489 = vadd_f32(v414, v418);
    float32x2_t v490 = vadd_f32(v410, v418);
    float32x2_t v491 = vadd_f32(v432, v439);
    float32x2_t v492 = vsub_f32(v425, v439);
    float32x2_t v493 = vadd_f32(v453, v460);
    float32x2_t v494 = vsub_f32(v446, v460);
    float32x2_t v382 = vmul_f32(v381, v380);
    float32x2_t v482 = vsub_f32(v347, v375);
    float32x2_t v495 = vadd_f32(v474, v481);
    float32x2_t v496 = vsub_f32(v467, v481);
    float32x2_t v497 = vadd_f32(v487, v489);
    float32x2_t v515 = vadd_f32(v491, v492);
    int16x4_t v531 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v347, 15), (int32x2_t){0, 0}));
    float32x2_t v498 = vadd_f32(v497, v482);
    float32x2_t v499 = vsub_f32(v482, v484);
    float32x2_t v501 = vadd_f32(v482, v488);
    float32x2_t v503 = vsub_f32(v482, v485);
    float32x2_t v505 = vadd_f32(v482, v483);
    float32x2_t v507 = vadd_f32(v382, v493);
    float32x2_t v509 = vsub_f32(v495, v491);
    float32x2_t v511 = vadd_f32(v382, v496);
    float32x2_t v513 = vsub_f32(v496, v492);
    float32x2_t v516 = vadd_f32(v515, v493);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v531), 0);
    float32x2_t v500 = vsub_f32(v499, v489);
    float32x2_t v502 = vadd_f32(v501, v490);
    float32x2_t v504 = vsub_f32(v503, v490);
    float32x2_t v506 = vsub_f32(v505, v486);
    float32x2_t v508 = vadd_f32(v507, v495);
    float32x2_t v510 = vsub_f32(v509, v382);
    float32x2_t v512 = vadd_f32(v511, v494);
    float32x2_t v514 = vsub_f32(v513, v382);
    float32x2_t v517 = vadd_f32(v516, v494);
    float32x2_t v518 = vsub_f32(v517, v382);
    float32x2_t v520 = vadd_f32(v498, v508);
    float32x2_t v521 = vadd_f32(v500, v510);
    float32x2_t v522 = vsub_f32(v502, v512);
    float32x2_t v523 = vadd_f32(v504, v514);
    float32x2_t v524 = vsub_f32(v504, v514);
    float32x2_t v525 = vadd_f32(v502, v512);
    float32x2_t v526 = vsub_f32(v500, v510);
    float32x2_t v527 = vsub_f32(v498, v508);
    float32x2_t v519 = vadd_f32(v506, v518);
    float32x2_t v528 = vsub_f32(v506, v518);
    int16x4_t v543 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v520, 15), (int32x2_t){0, 0}));
    int16x4_t v549 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v521, 15), (int32x2_t){0, 0}));
    int16x4_t v555 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v522, 15), (int32x2_t){0, 0}));
    int16x4_t v561 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v523, 15), (int32x2_t){0, 0}));
    int16x4_t v567 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v524, 15), (int32x2_t){0, 0}));
    int16x4_t v573 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v525, 15), (int32x2_t){0, 0}));
    int16x4_t v579 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v526, 15), (int32x2_t){0, 0}));
    int16x4_t v585 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v527, 15), (int32x2_t){0, 0}));
    int16x4_t v537 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v519, 15), (int32x2_t){0, 0}));
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v543), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v549), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v555), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v561), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v567), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v573), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v579), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v585), 0);
    int16x4_t v591 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v528, 15), (int32x2_t){0, 0}));
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v537), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v591), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu11(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v278 = 1.1000000000000001e+00F;
    float v283 = -3.3166247903554003e-01F;
    float v290 = 5.1541501300188641e-01F;
    float v295 = 9.4125353283118118e-01F;
    float v300 = 1.4143537075597825e+00F;
    float v305 = 8.5949297361449750e-01F;
    float v310 = 4.2314838273285138e-02F;
    float v315 = 3.8639279888589606e-01F;
    float v320 = 5.1254589567200015e-01F;
    float v325 = 1.0702757469471715e+00F;
    float v330 = 5.5486073394528512e-01F;
    float v335 = -1.2412944743900585e+00F;
    float v342 = -2.0897833842005756e-01F;
    float v349 = -3.7415717312460811e-01F;
    float v356 = -4.9929922194110327e-02F;
    float v363 = -6.5815896284539266e-01F;
    float v370 = -6.3306543373877577e-01F;
    float v377 = -1.0822460581641109e+00F;
    float v384 = -8.1720737907134022e-01F;
    float v391 = -4.2408709531871824e-01F;
    const float32x2_t *v538 = &v5[v0];
    int32_t *v752 = &v6[v2];
    int64_t v33 = v0 * 10;
    int64_t v47 = v0 * 2;
    int64_t v61 = v0 * 9;
    int64_t v75 = v0 * 3;
    int64_t v89 = v0 * 8;
    int64_t v103 = v0 * 4;
    int64_t v117 = v0 * 7;
    int64_t v131 = v0 * 5;
    int64_t v145 = v0 * 6;
    int64_t v167 = v10 * 9;
    int64_t v181 = v10 * 8;
    int64_t v188 = v10 * 2;
    int64_t v195 = v10 * 7;
    int64_t v202 = v10 * 3;
    int64_t v209 = v10 * 6;
    int64_t v216 = v10 * 4;
    int64_t v223 = v10 * 5;
    int64_t v224 = v13 * 10;
    float v286 = v4 * v283;
    float v338 = v4 * v335;
    float v345 = v4 * v342;
    float v352 = v4 * v349;
    float v359 = v4 * v356;
    float v366 = v4 * v363;
    float v373 = v4 * v370;
    float v380 = v4 * v377;
    float v387 = v4 * v384;
    float v394 = v4 * v391;
    int64_t v453 = v2 * 10;
    int64_t v461 = v2 * 9;
    int64_t v469 = v2 * 8;
    int64_t v477 = v2 * 7;
    int64_t v485 = v2 * 6;
    int64_t v493 = v2 * 5;
    int64_t v501 = v2 * 4;
    int64_t v509 = v2 * 3;
    int64_t v517 = v2 * 2;
    const float32x2_t *v631 = &v5[0];
    svint64_t v632 = svindex_s64(0, v1);
    svfloat32_t v635 = svdup_n_f32(v278);
    svfloat32_t v637 = svdup_n_f32(v290);
    svfloat32_t v638 = svdup_n_f32(v295);
    svfloat32_t v639 = svdup_n_f32(v300);
    svfloat32_t v640 = svdup_n_f32(v305);
    svfloat32_t v641 = svdup_n_f32(v310);
    svfloat32_t v642 = svdup_n_f32(v315);
    svfloat32_t v643 = svdup_n_f32(v320);
    svfloat32_t v644 = svdup_n_f32(v325);
    svfloat32_t v645 = svdup_n_f32(v330);
    int32_t *v662 = &v6[0];
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v224]));
    int64_t v169 = v167 + v224;
    int64_t v176 = v10 + v224;
    int64_t v183 = v181 + v224;
    int64_t v190 = v188 + v224;
    int64_t v197 = v195 + v224;
    int64_t v204 = v202 + v224;
    int64_t v211 = v209 + v224;
    int64_t v218 = v216 + v224;
    int64_t v225 = v223 + v224;
    svfloat32_t v540 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v538), v632));
    const float32x2_t *v548 = &v5[v33];
    const float32x2_t *v557 = &v5[v47];
    const float32x2_t *v566 = &v5[v61];
    const float32x2_t *v575 = &v5[v75];
    const float32x2_t *v584 = &v5[v89];
    const float32x2_t *v593 = &v5[v103];
    const float32x2_t *v602 = &v5[v117];
    const float32x2_t *v611 = &v5[v131];
    const float32x2_t *v620 = &v5[v145];
    svfloat32_t v633 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v631), v632));
    svfloat32_t v636 = svdup_n_f32(v286);
    svfloat32_t v646 = svdup_n_f32(v338);
    svfloat32_t v647 = svdup_n_f32(v345);
    svfloat32_t v648 = svdup_n_f32(v352);
    svfloat32_t v649 = svdup_n_f32(v359);
    svfloat32_t v650 = svdup_n_f32(v366);
    svfloat32_t v651 = svdup_n_f32(v373);
    svfloat32_t v652 = svdup_n_f32(v380);
    svfloat32_t v653 = svdup_n_f32(v387);
    svfloat32_t v654 = svdup_n_f32(v394);
    int32_t *v671 = &v6[v453];
    int32_t *v680 = &v6[v461];
    int32_t *v689 = &v6[v469];
    int32_t *v698 = &v6[v477];
    int32_t *v707 = &v6[v485];
    int32_t *v716 = &v6[v493];
    int32_t *v725 = &v6[v501];
    int32_t *v734 = &v6[v509];
    int32_t *v743 = &v6[v517];
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v540, v163, 0),
                     v540, v163, 90);
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t v177 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v176]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v191 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v190]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v219 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v218]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v550 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v548), v632));
    svfloat32_t v559 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v557), v632));
    svfloat32_t v568 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v566), v632));
    svfloat32_t v577 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v575), v632));
    svfloat32_t v586 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v584), v632));
    svfloat32_t v595 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v593), v632));
    svfloat32_t v604 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v602), v632));
    svfloat32_t v613 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v611), v632));
    svfloat32_t v622 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v620), v632));
    svfloat32_t zero171 = svdup_n_f32(0);
    svfloat32_t v171 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero171, v550, v170, 0),
                     v550, v170, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero178, v559, v177, 0),
                     v559, v177, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero185, v568, v184, 0),
                     v568, v184, 90);
    svfloat32_t zero192 = svdup_n_f32(0);
    svfloat32_t v192 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero192, v577, v191, 0),
                     v577, v191, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v586, v198, 0),
                     v586, v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v595, v205, 0),
                     v595, v205, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v604, v212, 0),
                     v604, v212, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero220, v613, v219, 0),
                     v613, v219, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero227, v622, v226, 0),
                     v622, v226, 90);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v164, v171);
    svfloat32_t v229 = svadd_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v192, v199);
    svfloat32_t v231 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v232 = svadd_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v233 = svsub_f32_x(svptrue_b32(), v164, v171);
    svfloat32_t v234 = svsub_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v192, v199);
    svfloat32_t v236 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v237 = svsub_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v228, v229);
    svfloat32_t v239 = svadd_f32_x(svptrue_b32(), v230, v232);
    svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v234, v235);
    svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v233, v237);
    svfloat32_t v254 = svsub_f32_x(svptrue_b32(), v229, v231);
    svfloat32_t v255 = svsub_f32_x(svptrue_b32(), v228, v231);
    svfloat32_t v256 = svsub_f32_x(svptrue_b32(), v229, v228);
    svfloat32_t v257 = svsub_f32_x(svptrue_b32(), v232, v231);
    svfloat32_t v258 = svsub_f32_x(svptrue_b32(), v230, v231);
    svfloat32_t v259 = svsub_f32_x(svptrue_b32(), v232, v230);
    svfloat32_t v260 = svsub_f32_x(svptrue_b32(), v229, v232);
    svfloat32_t v261 = svsub_f32_x(svptrue_b32(), v228, v230);
    svfloat32_t v263 = svadd_f32_x(svptrue_b32(), v234, v236);
    svfloat32_t v264 = svsub_f32_x(svptrue_b32(), v233, v236);
    svfloat32_t v265 = svadd_f32_x(svptrue_b32(), v233, v234);
    svfloat32_t v266 = svsub_f32_x(svptrue_b32(), v236, v237);
    svfloat32_t v267 = svsub_f32_x(svptrue_b32(), v235, v236);
    svfloat32_t v268 = svsub_f32_x(svptrue_b32(), v235, v237);
    svfloat32_t v269 = svadd_f32_x(svptrue_b32(), v234, v237);
    svfloat32_t v270 = svsub_f32_x(svptrue_b32(), v233, v235);
    svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v231, v238);
    svfloat32_t v252 = svsub_f32_x(svptrue_b32(), v241, v242);
    svfloat32_t v262 = svsub_f32_x(svptrue_b32(), v239, v238);
    svfloat32_t v271 = svadd_f32_x(svptrue_b32(), v241, v242);
    svfloat32_t v298 = svmul_f32_x(svptrue_b32(), v255, v638);
    svfloat32_t v303 = svmul_f32_x(svptrue_b32(), v256, v639);
    svfloat32_t v313 = svmul_f32_x(svptrue_b32(), v258, v641);
    svfloat32_t v318 = svmul_f32_x(svptrue_b32(), v259, v642);
    svfloat32_t zero340 = svdup_n_f32(0);
    svfloat32_t v340 = svcmla_f32_x(pred_full, zero340, v646, v263, 90);
    svfloat32_t zero354 = svdup_n_f32(0);
    svfloat32_t v354 = svcmla_f32_x(pred_full, zero354, v648, v265, 90);
    svfloat32_t zero361 = svdup_n_f32(0);
    svfloat32_t v361 = svcmla_f32_x(pred_full, zero361, v649, v266, 90);
    svfloat32_t zero375 = svdup_n_f32(0);
    svfloat32_t v375 = svcmla_f32_x(pred_full, zero375, v651, v268, 90);
    svfloat32_t zero382 = svdup_n_f32(0);
    svfloat32_t v382 = svcmla_f32_x(pred_full, zero382, v652, v269, 90);
    svfloat32_t v243 = svadd_f32_x(svptrue_b32(), v240, v239);
    svfloat32_t v253 = svsub_f32_x(svptrue_b32(), v252, v236);
    svfloat32_t v333 = svmul_f32_x(svptrue_b32(), v262, v645);
    svfloat32_t zero396 = svdup_n_f32(0);
    svfloat32_t v396 = svcmla_f32_x(pred_full, zero396, v654, v271, 90);
    svfloat32_t v398 = svmla_f32_x(pred_full, v298, v254, v637);
    svfloat32_t v399 = svmla_f32_x(pred_full, v303, v255, v638);
    svfloat32_t v400 = svnmls_f32_x(pred_full, v303, v254, v637);
    svfloat32_t v401 = svmla_f32_x(pred_full, v313, v257, v640);
    svfloat32_t v402 = svmla_f32_x(pred_full, v318, v258, v641);
    svfloat32_t v403 = svnmls_f32_x(pred_full, v318, v257, v640);
    svfloat32_t v406 = svcmla_f32_x(pred_full, v354, v647, v264, 90);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v340, v354);
    svfloat32_t v408 = svcmla_f32_x(pred_full, v375, v650, v267, 90);
    svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v361, v375);
    svfloat32_t v251 = svadd_f32_x(svptrue_b32(), v633, v243);
    svfloat32_t zero288 = svdup_n_f32(0);
    svfloat32_t v288 = svcmla_f32_x(pred_full, zero288, v636, v253, 90);
    svfloat32_t v404 = svmla_f32_x(pred_full, v333, v261, v644);
    svfloat32_t v405 = svmla_f32_x(pred_full, v333, v260, v643);
    svfloat32_t v410 = svcmla_f32_x(pred_full, v396, v653, v270, 90);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v382, v396);
    svfloat32_t v430 = svadd_f32_x(svptrue_b32(), v406, v407);
    svfloat32_t v397 = svmls_f32_x(pred_full, v251, v243, v635);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v402, v404);
    svfloat32_t v422 = svadd_f32_x(svptrue_b32(), v288, v408);
    svfloat32_t v424 = svsub_f32_x(svptrue_b32(), v410, v406);
    svfloat32_t v426 = svadd_f32_x(svptrue_b32(), v288, v411);
    svfloat32_t v428 = svsub_f32_x(svptrue_b32(), v411, v407);
    svfloat32_t v431 = svadd_f32_x(svptrue_b32(), v430, v408);
    svint16_t v446 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v251, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v412, v397);
    svfloat32_t v414 = svsub_f32_x(svptrue_b32(), v397, v399);
    svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v397, v403);
    svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v397, v400);
    svfloat32_t v420 = svadd_f32_x(svptrue_b32(), v397, v398);
    svfloat32_t v423 = svadd_f32_x(svptrue_b32(), v422, v410);
    svfloat32_t v425 = svsub_f32_x(svptrue_b32(), v424, v288);
    svfloat32_t v427 = svadd_f32_x(svptrue_b32(), v426, v409);
    svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v428, v288);
    svfloat32_t v432 = svadd_f32_x(svptrue_b32(), v431, v409);
    svst1w_u64(pred_full, (unsigned *)(v662), svreinterpret_u64_s16(v446));
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v414, v404);
    svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v416, v405);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v418, v405);
    svfloat32_t v421 = svsub_f32_x(svptrue_b32(), v420, v401);
    svfloat32_t v433 = svsub_f32_x(svptrue_b32(), v432, v288);
    svfloat32_t v435 = svadd_f32_x(svptrue_b32(), v413, v423);
    svfloat32_t v442 = svsub_f32_x(svptrue_b32(), v413, v423);
    svfloat32_t v434 = svadd_f32_x(svptrue_b32(), v421, v433);
    svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v415, v425);
    svfloat32_t v437 = svsub_f32_x(svptrue_b32(), v417, v427);
    svfloat32_t v438 = svadd_f32_x(svptrue_b32(), v419, v429);
    svfloat32_t v439 = svsub_f32_x(svptrue_b32(), v419, v429);
    svfloat32_t v440 = svadd_f32_x(svptrue_b32(), v417, v427);
    svfloat32_t v441 = svsub_f32_x(svptrue_b32(), v415, v425);
    svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v421, v433);
    svint16_t v462 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v435, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v518 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v442, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v454 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v434, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v470 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v436, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v478 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v437, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v486 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v438, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v494 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v439, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v502 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v440, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v510 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v441, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v526 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v443, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v680), svreinterpret_u64_s16(v462));
    svst1w_u64(pred_full, (unsigned *)(v743), svreinterpret_u64_s16(v518));
    svst1w_u64(pred_full, (unsigned *)(v671), svreinterpret_u64_s16(v454));
    svst1w_u64(pred_full, (unsigned *)(v689), svreinterpret_u64_s16(v470));
    svst1w_u64(pred_full, (unsigned *)(v698), svreinterpret_u64_s16(v478));
    svst1w_u64(pred_full, (unsigned *)(v707), svreinterpret_u64_s16(v486));
    svst1w_u64(pred_full, (unsigned *)(v716), svreinterpret_u64_s16(v494));
    svst1w_u64(pred_full, (unsigned *)(v725), svreinterpret_u64_s16(v502));
    svst1w_u64(pred_full, (unsigned *)(v734), svreinterpret_u64_s16(v510));
    svst1w_u64(pred_full, (unsigned *)(v752), svreinterpret_u64_s16(v526));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu12(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v242 = v5[istride];
    float v353 = 1.0000000000000000e+00F;
    float v354 = -1.0000000000000000e+00F;
    float v380 = -1.4999999999999998e+00F;
    float v381 = 1.4999999999999998e+00F;
    float v409 = 8.6602540378443871e-01F;
    float32x2_t v412 = (float32x2_t){v4, v4};
    float v417 = -8.6602540378443871e-01F;
    float32x2_t v279 = vtrn1_f32(v242, v242);
    float32x2_t v280 = vtrn2_f32(v242, v242);
    float32x2_t v324 = v5[0];
    float32x2_t v355 = (float32x2_t){v353, v354};
    float32x2_t v378 = (float32x2_t){v380, v380};
    float32x2_t v382 = (float32x2_t){v380, v381};
    float32x2_t v411 = (float32x2_t){v409, v417};
    float32x2_t v418 = (float32x2_t){v417, v417};
    float32x2_t v20 = v5[istride * 4];
    float32x2_t v38 = v5[istride * 8];
    int64_t v55 = 6 + j * 22;
    int64_t v68 = 14 + j * 22;
    float32x2_t v82 = v5[istride * 7];
    float32x2_t v100 = v5[istride * 11];
    int64_t v117 = 12 + j * 22;
    int64_t v130 = 20 + j * 22;
    float32x2_t v144 = v5[istride * 3];
    int64_t v148 = 4 + j * 22;
    float32x2_t v162 = v5[istride * 10];
    float32x2_t v180 = v5[istride * 2];
    int64_t v197 = 18 + j * 22;
    int64_t v210 = 2 + j * 22;
    float32x2_t v224 = v5[istride * 6];
    int64_t v228 = 10 + j * 22;
    float32x2_t v260 = v5[istride * 5];
    float32x2_t v278 = v7[j * 22];
    int64_t v282 = j * 22 + 1;
    int64_t v290 = 8 + j * 22;
    float32x2_t v304 = v5[istride * 9];
    int64_t v308 = 16 + j * 22;
    float32x2_t v357 = vmul_f32(v412, v355);
    float32x2_t v384 = vmul_f32(v412, v382);
    float32x2_t v413 = vmul_f32(v412, v411);
    float32x2_t v56 = v7[v55];
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    int64_t v60 = v55 + 1;
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v144, v144);
    float32x2_t v151 = vtrn2_f32(v144, v144);
    int64_t v153 = v148 + 1;
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vtrn1_f32(v162, v162);
    float32x2_t v200 = vtrn2_f32(v162, v162);
    int64_t v202 = v197 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v180, v180);
    float32x2_t v213 = vtrn2_f32(v180, v180);
    int64_t v215 = v210 + 1;
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vtrn1_f32(v224, v224);
    float32x2_t v231 = vtrn2_f32(v224, v224);
    int64_t v233 = v228 + 1;
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vtrn1_f32(v260, v260);
    float32x2_t v293 = vtrn2_f32(v260, v260);
    int64_t v295 = v290 + 1;
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vtrn1_f32(v304, v304);
    float32x2_t v311 = vtrn2_f32(v304, v304);
    int64_t v313 = v308 + 1;
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v203 = v7[v202];
    float32x2_t v204 = vmul_f32(v199, v198);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v234 = v7[v233];
    float32x2_t v235 = vmul_f32(v230, v229);
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vmul_f32(v292, v291);
    float32x2_t v314 = v7[v313];
    float32x2_t v315 = vmul_f32(v310, v309);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v206 = vfma_f32(v204, v200, v203);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v237 = vfma_f32(v235, v231, v234);
    float32x2_t v299 = vfma_f32(v297, v293, v296);
    float32x2_t v317 = vfma_f32(v315, v311, v314);
    float32x2_t v318 = vadd_f32(v64, v77);
    float32x2_t v319 = vsub_f32(v64, v77);
    float32x2_t v326 = vadd_f32(v126, v139);
    float32x2_t v327 = vsub_f32(v126, v139);
    float32x2_t v329 = vadd_f32(v206, v219);
    float32x2_t v330 = vsub_f32(v206, v219);
    float32x2_t v332 = vadd_f32(v286, v299);
    float32x2_t v333 = vsub_f32(v286, v299);
    float32x2_t v325 = vadd_f32(v318, v324);
    float32x2_t v328 = vadd_f32(v326, v157);
    float32x2_t v331 = vadd_f32(v329, v237);
    float32x2_t v334 = vadd_f32(v332, v317);
    float32x2_t v362 = vadd_f32(v318, v329);
    float32x2_t v363 = vsub_f32(v318, v329);
    float32x2_t v364 = vadd_f32(v326, v332);
    float32x2_t v365 = vsub_f32(v326, v332);
    float32x2_t v389 = vadd_f32(v319, v330);
    float32x2_t v390 = vsub_f32(v319, v330);
    float32x2_t v391 = vadd_f32(v327, v333);
    float32x2_t v392 = vsub_f32(v327, v333);
    float32x2_t v335 = vadd_f32(v325, v331);
    float32x2_t v336 = vsub_f32(v325, v331);
    float32x2_t v337 = vadd_f32(v328, v334);
    float32x2_t v338 = vsub_f32(v328, v334);
    float32x2_t v366 = vadd_f32(v362, v364);
    float32x2_t v367 = vsub_f32(v362, v364);
    float32x2_t v379 = vmul_f32(v363, v378);
    float32x2_t v385 = vrev64_f32(v365);
    float32x2_t v393 = vadd_f32(v389, v391);
    float32x2_t v394 = vsub_f32(v389, v391);
    float32x2_t v414 = vrev64_f32(v390);
    float32x2_t v419 = vmul_f32(v392, v418);
    float32x2_t v339 = vadd_f32(v335, v337);
    float32x2_t v340 = vsub_f32(v335, v337);
    float32x2_t v358 = vrev64_f32(v338);
    float32x2_t v371 = vmul_f32(v366, v378);
    float32x2_t v375 = vmul_f32(v367, v378);
    float32x2_t v386 = vmul_f32(v385, v384);
    float32x2_t v400 = vrev64_f32(v393);
    float32x2_t v407 = vrev64_f32(v394);
    float32x2_t v415 = vmul_f32(v414, v413);
    float32x2_t v359 = vmul_f32(v358, v357);
    float32x2_t v387 = vadd_f32(v379, v386);
    float32x2_t v388 = vsub_f32(v379, v386);
    float32x2_t v401 = vmul_f32(v400, v413);
    float32x2_t v408 = vmul_f32(v407, v413);
    float32x2_t v420 = vadd_f32(v415, v419);
    float32x2_t v421 = vsub_f32(v415, v419);
    float32x2_t v422 = vadd_f32(v339, v371);
    int16x4_t v427 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v339, 15), (int32x2_t){0, 0}));
    float32x2_t v464 = vadd_f32(v340, v375);
    int16x4_t v469 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v340, 15), (int32x2_t){0, 0}));
    float32x2_t v360 = vadd_f32(v336, v359);
    float32x2_t v361 = vsub_f32(v336, v359);
    float32x2_t v423 = vadd_f32(v422, v401);
    float32x2_t v424 = vsub_f32(v422, v401);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v427), 0);
    float32x2_t v465 = vadd_f32(v464, v408);
    float32x2_t v466 = vsub_f32(v464, v408);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v469), 0);
    int16x4_t v433 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v424, 15), (int32x2_t){0, 0}));
    int16x4_t v439 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v423, 15), (int32x2_t){0, 0}));
    float32x2_t v443 = vadd_f32(v361, v388);
    int16x4_t v448 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v361, 15), (int32x2_t){0, 0}));
    int16x4_t v475 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v466, 15), (int32x2_t){0, 0}));
    int16x4_t v481 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v465, 15), (int32x2_t){0, 0}));
    float32x2_t v485 = vadd_f32(v360, v387);
    int16x4_t v490 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v360, 15), (int32x2_t){0, 0}));
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v433), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v439), 0);
    float32x2_t v444 = vadd_f32(v443, v421);
    float32x2_t v445 = vsub_f32(v443, v421);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v448), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v475), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v481), 0);
    float32x2_t v486 = vadd_f32(v485, v420);
    float32x2_t v487 = vsub_f32(v485, v420);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v490), 0);
    int16x4_t v454 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v445, 15), (int32x2_t){0, 0}));
    int16x4_t v460 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v444, 15), (int32x2_t){0, 0}));
    int16x4_t v496 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v487, 15), (int32x2_t){0, 0}));
    int16x4_t v502 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v486, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v454), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v460), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v496), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v502), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu12(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v269 = -1.0000000000000000e+00F;
    float v294 = -1.4999999999999998e+00F;
    float v299 = 1.4999999999999998e+00F;
    float v335 = -8.6602540378443871e-01F;
    const float32x2_t *v527 = &v5[v0];
    int32_t *v615 = &v6[v2];
    int64_t v19 = v0 * 4;
    int64_t v33 = v0 * 8;
    int64_t v48 = v10 * 3;
    int64_t v55 = v10 * 7;
    int64_t v61 = v0 * 7;
    int64_t v75 = v0 * 11;
    int64_t v90 = v10 * 6;
    int64_t v97 = v10 * 10;
    int64_t v103 = v0 * 3;
    int64_t v111 = v10 * 2;
    int64_t v117 = v0 * 10;
    int64_t v131 = v0 * 2;
    int64_t v146 = v10 * 9;
    int64_t v159 = v0 * 6;
    int64_t v167 = v10 * 5;
    int64_t v187 = v0 * 5;
    int64_t v209 = v10 * 4;
    int64_t v215 = v0 * 9;
    int64_t v223 = v10 * 8;
    int64_t v224 = v13 * 11;
    float v272 = v4 * v269;
    float v302 = v4 * v299;
    float v331 = v4 * v335;
    int64_t v353 = v2 * 4;
    int64_t v361 = v2 * 8;
    int64_t v372 = v2 * 9;
    int64_t v388 = v2 * 5;
    int64_t v399 = v2 * 6;
    int64_t v407 = v2 * 10;
    int64_t v415 = v2 * 2;
    int64_t v426 = v2 * 3;
    int64_t v434 = v2 * 7;
    int64_t v442 = v2 * 11;
    const float32x2_t *v557 = &v5[0];
    svint64_t v558 = svindex_s64(0, v1);
    svfloat32_t v566 = svdup_n_f32(v294);
    svfloat32_t v571 = svdup_n_f32(v335);
    int32_t *v579 = &v6[0];
    int64_t v50 = v48 + v224;
    int64_t v57 = v55 + v224;
    int64_t v92 = v90 + v224;
    int64_t v99 = v97 + v224;
    int64_t v113 = v111 + v224;
    int64_t v148 = v146 + v224;
    int64_t v155 = v10 + v224;
    int64_t v169 = v167 + v224;
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v224]));
    int64_t v211 = v209 + v224;
    int64_t v225 = v223 + v224;
    const float32x2_t *v455 = &v5[v19];
    const float32x2_t *v464 = &v5[v33];
    const float32x2_t *v473 = &v5[v61];
    const float32x2_t *v482 = &v5[v75];
    const float32x2_t *v491 = &v5[v103];
    const float32x2_t *v500 = &v5[v117];
    const float32x2_t *v509 = &v5[v131];
    const float32x2_t *v518 = &v5[v159];
    svfloat32_t v529 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v527), v558));
    const float32x2_t *v537 = &v5[v187];
    const float32x2_t *v547 = &v5[v215];
    svfloat32_t v559 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v557), v558));
    svfloat32_t v563 = svdup_n_f32(v272);
    svfloat32_t v567 = svdup_n_f32(v302);
    svfloat32_t v570 = svdup_n_f32(v331);
    int32_t *v588 = &v6[v353];
    int32_t *v597 = &v6[v361];
    int32_t *v606 = &v6[v372];
    int32_t *v624 = &v6[v388];
    int32_t *v633 = &v6[v399];
    int32_t *v642 = &v6[v407];
    int32_t *v651 = &v6[v415];
    int32_t *v660 = &v6[v426];
    int32_t *v669 = &v6[v434];
    int32_t *v678 = &v6[v442];
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v50]));
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v149 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v148]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v529, v205, 0),
                     v529, v205, 90);
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v457 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v455), v558));
    svfloat32_t v466 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v464), v558));
    svfloat32_t v475 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v473), v558));
    svfloat32_t v484 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v482), v558));
    svfloat32_t v493 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v491), v558));
    svfloat32_t v502 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v500), v558));
    svfloat32_t v511 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v509), v558));
    svfloat32_t v520 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v518), v558));
    svfloat32_t v539 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v537), v558));
    svfloat32_t v549 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v547), v558));
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v457, v51, 0),
                     v457, v51, 90);
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v466, v58, 0),
                     v466, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v475, v93, 0),
                     v475, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v484, v100, 0),
                     v484, v100, 90);
    svfloat32_t zero150 = svdup_n_f32(0);
    svfloat32_t v150 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero150, v502, v149, 0),
                     v502, v149, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v511, v156, 0),
                     v511, v156, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v539, v212, 0),
                     v539, v212, 90);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v241 = svadd_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v242 = svsub_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v244 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v245 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v237 = svadd_f32_x(svptrue_b32(), v228, v559);
    svfloat32_t v240 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v238, v493, v114, 0),
                     v493, v114, 90);
    svfloat32_t v243 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v241, v520, v170, 0),
                     v520, v170, 90);
    svfloat32_t v246 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v244, v549, v226, 0),
                     v549, v226, 90);
    svfloat32_t v277 = svadd_f32_x(svptrue_b32(), v228, v241);
    svfloat32_t v278 = svsub_f32_x(svptrue_b32(), v228, v241);
    svfloat32_t v279 = svadd_f32_x(svptrue_b32(), v238, v244);
    svfloat32_t v280 = svsub_f32_x(svptrue_b32(), v238, v244);
    svfloat32_t v307 = svadd_f32_x(svptrue_b32(), v229, v242);
    svfloat32_t v308 = svsub_f32_x(svptrue_b32(), v229, v242);
    svfloat32_t v309 = svadd_f32_x(svptrue_b32(), v239, v245);
    svfloat32_t v310 = svsub_f32_x(svptrue_b32(), v239, v245);
    svfloat32_t v247 = svadd_f32_x(svptrue_b32(), v237, v243);
    svfloat32_t v248 = svsub_f32_x(svptrue_b32(), v237, v243);
    svfloat32_t v249 = svadd_f32_x(svptrue_b32(), v240, v246);
    svfloat32_t v250 = svsub_f32_x(svptrue_b32(), v240, v246);
    svfloat32_t v281 = svadd_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t v282 = svsub_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t zero304 = svdup_n_f32(0);
    svfloat32_t v304 = svcmla_f32_x(pred_full, zero304, v567, v280, 90);
    svfloat32_t v311 = svadd_f32_x(svptrue_b32(), v307, v309);
    svfloat32_t v312 = svsub_f32_x(svptrue_b32(), v307, v309);
    svfloat32_t zero333 = svdup_n_f32(0);
    svfloat32_t v333 = svcmla_f32_x(pred_full, zero333, v570, v308, 90);
    svfloat32_t v251 = svadd_f32_x(svptrue_b32(), v247, v249);
    svfloat32_t v252 = svsub_f32_x(svptrue_b32(), v247, v249);
    svfloat32_t zero274 = svdup_n_f32(0);
    svfloat32_t v274 = svcmla_f32_x(pred_full, zero274, v563, v250, 90);
    svfloat32_t v305 = svmla_f32_x(pred_full, v304, v278, v566);
    svfloat32_t v306 = svnmls_f32_x(pred_full, v304, v278, v566);
    svfloat32_t zero319 = svdup_n_f32(0);
    svfloat32_t v319 = svcmla_f32_x(pred_full, zero319, v570, v311, 90);
    svfloat32_t zero326 = svdup_n_f32(0);
    svfloat32_t v326 = svcmla_f32_x(pred_full, zero326, v570, v312, 90);
    svfloat32_t v339 = svmla_f32_x(pred_full, v333, v310, v571);
    svfloat32_t v340 = svmls_f32_x(pred_full, v333, v310, v571);
    svfloat32_t v275 = svadd_f32_x(svptrue_b32(), v248, v274);
    svfloat32_t v276 = svsub_f32_x(svptrue_b32(), v248, v274);
    svfloat32_t v341 = svmla_f32_x(pred_full, v251, v281, v566);
    svint16_t v346 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v251, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v395 = svmla_f32_x(pred_full, v252, v282, v566);
    svint16_t v400 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v252, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v342 = svadd_f32_x(svptrue_b32(), v341, v319);
    svfloat32_t v343 = svsub_f32_x(svptrue_b32(), v341, v319);
    svfloat32_t v368 = svadd_f32_x(svptrue_b32(), v276, v306);
    svint16_t v373 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v276, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v395, v326);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v395, v326);
    svfloat32_t v422 = svadd_f32_x(svptrue_b32(), v275, v305);
    svint16_t v427 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v275, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v579), svreinterpret_u64_s16(v346));
    svst1w_u64(pred_full, (unsigned *)(v633), svreinterpret_u64_s16(v400));
    svint16_t v354 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v343, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v362 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v342, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v369 = svadd_f32_x(svptrue_b32(), v368, v340);
    svfloat32_t v370 = svsub_f32_x(svptrue_b32(), v368, v340);
    svint16_t v408 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v397, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v416 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v396, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v423 = svadd_f32_x(svptrue_b32(), v422, v339);
    svfloat32_t v424 = svsub_f32_x(svptrue_b32(), v422, v339);
    svst1w_u64(pred_full, (unsigned *)(v606), svreinterpret_u64_s16(v373));
    svst1w_u64(pred_full, (unsigned *)(v660), svreinterpret_u64_s16(v427));
    svint16_t v381 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v370, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v389 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v369, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v435 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v424, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v443 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v423, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v588), svreinterpret_u64_s16(v354));
    svst1w_u64(pred_full, (unsigned *)(v597), svreinterpret_u64_s16(v362));
    svst1w_u64(pred_full, (unsigned *)(v642), svreinterpret_u64_s16(v408));
    svst1w_u64(pred_full, (unsigned *)(v651), svreinterpret_u64_s16(v416));
    svst1w_u64(pred_full, (unsigned *)(v615), svreinterpret_u64_s16(v381));
    svst1w_u64(pred_full, (unsigned *)(v624), svreinterpret_u64_s16(v389));
    svst1w_u64(pred_full, (unsigned *)(v669), svreinterpret_u64_s16(v435));
    svst1w_u64(pred_full, (unsigned *)(v678), svreinterpret_u64_s16(v443));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu13(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v441 = 1.0833333333333333e+00F;
    float v445 = -3.0046260628866578e-01F;
    float v448 = 7.4927933062613905e-01F;
    float v449 = -7.4927933062613905e-01F;
    float v455 = 4.0100212832186721e-01F;
    float v456 = -4.0100212832186721e-01F;
    float v462 = 5.7514072947400308e-01F;
    float v463 = -5.7514072947400308e-01F;
    float v470 = 5.2422663952658211e-01F;
    float v474 = 5.1652078062348972e-01F;
    float v478 = 7.7058589030924258e-03F;
    float v482 = 4.2763404682656941e-01F;
    float v486 = 1.5180597207438440e-01F;
    float v490 = 5.7944001890096386e-01F;
    float v493 = 1.1543953381323635e+00F;
    float v494 = -1.1543953381323635e+00F;
    float v500 = 9.0655220171271012e-01F;
    float v501 = -9.0655220171271012e-01F;
    float v507 = 8.1857027294591811e-01F;
    float v508 = -8.1857027294591811e-01F;
    float v514 = 1.1971367726043427e+00F;
    float v515 = -1.1971367726043427e+00F;
    float v521 = 8.6131170741789742e-01F;
    float v522 = -8.6131170741789742e-01F;
    float v528 = 1.1091548438375507e+00F;
    float v529 = -1.1091548438375507e+00F;
    float v535 = 4.2741434471979367e-02F;
    float v536 = -4.2741434471979367e-02F;
    float v542 = -4.5240494294812715e-02F;
    float v543 = 4.5240494294812715e-02F;
    float v549 = 2.9058457089163264e-01F;
    float v550 = -2.9058457089163264e-01F;
    float32x2_t v552 = (float32x2_t){v4, v4};
    float32x2_t v237 = vtrn1_f32(v20, v20);
    float32x2_t v238 = vtrn2_f32(v20, v20);
    float32x2_t v427 = v5[0];
    float32x2_t v442 = (float32x2_t){v441, v441};
    float32x2_t v446 = (float32x2_t){v445, v445};
    float32x2_t v450 = (float32x2_t){v448, v449};
    float32x2_t v457 = (float32x2_t){v455, v456};
    float32x2_t v464 = (float32x2_t){v462, v463};
    float32x2_t v471 = (float32x2_t){v470, v470};
    float32x2_t v475 = (float32x2_t){v474, v474};
    float32x2_t v479 = (float32x2_t){v478, v478};
    float32x2_t v483 = (float32x2_t){v482, v482};
    float32x2_t v487 = (float32x2_t){v486, v486};
    float32x2_t v491 = (float32x2_t){v490, v490};
    float32x2_t v495 = (float32x2_t){v493, v494};
    float32x2_t v502 = (float32x2_t){v500, v501};
    float32x2_t v509 = (float32x2_t){v507, v508};
    float32x2_t v516 = (float32x2_t){v514, v515};
    float32x2_t v523 = (float32x2_t){v521, v522};
    float32x2_t v530 = (float32x2_t){v528, v529};
    float32x2_t v537 = (float32x2_t){v535, v536};
    float32x2_t v544 = (float32x2_t){v542, v543};
    float32x2_t v551 = (float32x2_t){v549, v550};
    float32x2_t v38 = v5[istride * 12];
    float32x2_t v56 = v5[istride * 2];
    float32x2_t v74 = v5[istride * 11];
    float32x2_t v92 = v5[istride * 3];
    float32x2_t v110 = v5[istride * 10];
    float32x2_t v128 = v5[istride * 4];
    float32x2_t v146 = v5[istride * 9];
    float32x2_t v164 = v5[istride * 5];
    float32x2_t v182 = v5[istride * 8];
    float32x2_t v200 = v5[istride * 6];
    float32x2_t v218 = v5[istride * 7];
    float32x2_t v236 = v7[j * 24];
    int64_t v240 = j * 24 + 1;
    int64_t v248 = 22 + j * 24;
    int64_t v261 = 2 + j * 24;
    int64_t v274 = 20 + j * 24;
    int64_t v287 = 4 + j * 24;
    int64_t v300 = 18 + j * 24;
    int64_t v313 = 6 + j * 24;
    int64_t v326 = 16 + j * 24;
    int64_t v339 = 8 + j * 24;
    int64_t v352 = 14 + j * 24;
    int64_t v365 = 10 + j * 24;
    int64_t v378 = 12 + j * 24;
    float32x2_t v452 = vmul_f32(v552, v450);
    float32x2_t v459 = vmul_f32(v552, v457);
    float32x2_t v466 = vmul_f32(v552, v464);
    float32x2_t v497 = vmul_f32(v552, v495);
    float32x2_t v504 = vmul_f32(v552, v502);
    float32x2_t v511 = vmul_f32(v552, v509);
    float32x2_t v518 = vmul_f32(v552, v516);
    float32x2_t v525 = vmul_f32(v552, v523);
    float32x2_t v532 = vmul_f32(v552, v530);
    float32x2_t v539 = vmul_f32(v552, v537);
    float32x2_t v546 = vmul_f32(v552, v544);
    float32x2_t v553 = vmul_f32(v552, v551);
    float32x2_t v241 = v7[v240];
    float32x2_t v242 = vmul_f32(v237, v236);
    float32x2_t v249 = v7[v248];
    float32x2_t v250 = vtrn1_f32(v38, v38);
    float32x2_t v251 = vtrn2_f32(v38, v38);
    int64_t v253 = v248 + 1;
    float32x2_t v262 = v7[v261];
    float32x2_t v263 = vtrn1_f32(v56, v56);
    float32x2_t v264 = vtrn2_f32(v56, v56);
    int64_t v266 = v261 + 1;
    float32x2_t v275 = v7[v274];
    float32x2_t v276 = vtrn1_f32(v74, v74);
    float32x2_t v277 = vtrn2_f32(v74, v74);
    int64_t v279 = v274 + 1;
    float32x2_t v288 = v7[v287];
    float32x2_t v289 = vtrn1_f32(v92, v92);
    float32x2_t v290 = vtrn2_f32(v92, v92);
    int64_t v292 = v287 + 1;
    float32x2_t v301 = v7[v300];
    float32x2_t v302 = vtrn1_f32(v110, v110);
    float32x2_t v303 = vtrn2_f32(v110, v110);
    int64_t v305 = v300 + 1;
    float32x2_t v314 = v7[v313];
    float32x2_t v315 = vtrn1_f32(v128, v128);
    float32x2_t v316 = vtrn2_f32(v128, v128);
    int64_t v318 = v313 + 1;
    float32x2_t v327 = v7[v326];
    float32x2_t v328 = vtrn1_f32(v146, v146);
    float32x2_t v329 = vtrn2_f32(v146, v146);
    int64_t v331 = v326 + 1;
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vtrn1_f32(v164, v164);
    float32x2_t v342 = vtrn2_f32(v164, v164);
    int64_t v344 = v339 + 1;
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vtrn1_f32(v182, v182);
    float32x2_t v355 = vtrn2_f32(v182, v182);
    int64_t v357 = v352 + 1;
    float32x2_t v366 = v7[v365];
    float32x2_t v367 = vtrn1_f32(v200, v200);
    float32x2_t v368 = vtrn2_f32(v200, v200);
    int64_t v370 = v365 + 1;
    float32x2_t v379 = v7[v378];
    float32x2_t v380 = vtrn1_f32(v218, v218);
    float32x2_t v381 = vtrn2_f32(v218, v218);
    int64_t v383 = v378 + 1;
    float32x2_t v254 = v7[v253];
    float32x2_t v255 = vmul_f32(v250, v249);
    float32x2_t v267 = v7[v266];
    float32x2_t v268 = vmul_f32(v263, v262);
    float32x2_t v280 = v7[v279];
    float32x2_t v281 = vmul_f32(v276, v275);
    float32x2_t v293 = v7[v292];
    float32x2_t v294 = vmul_f32(v289, v288);
    float32x2_t v306 = v7[v305];
    float32x2_t v307 = vmul_f32(v302, v301);
    float32x2_t v319 = v7[v318];
    float32x2_t v320 = vmul_f32(v315, v314);
    float32x2_t v332 = v7[v331];
    float32x2_t v333 = vmul_f32(v328, v327);
    float32x2_t v345 = v7[v344];
    float32x2_t v346 = vmul_f32(v341, v340);
    float32x2_t v358 = v7[v357];
    float32x2_t v359 = vmul_f32(v354, v353);
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vmul_f32(v367, v366);
    float32x2_t v384 = v7[v383];
    float32x2_t v385 = vmul_f32(v380, v379);
    float32x2_t v244 = vfma_f32(v242, v238, v241);
    float32x2_t v257 = vfma_f32(v255, v251, v254);
    float32x2_t v270 = vfma_f32(v268, v264, v267);
    float32x2_t v283 = vfma_f32(v281, v277, v280);
    float32x2_t v296 = vfma_f32(v294, v290, v293);
    float32x2_t v309 = vfma_f32(v307, v303, v306);
    float32x2_t v322 = vfma_f32(v320, v316, v319);
    float32x2_t v335 = vfma_f32(v333, v329, v332);
    float32x2_t v348 = vfma_f32(v346, v342, v345);
    float32x2_t v361 = vfma_f32(v359, v355, v358);
    float32x2_t v374 = vfma_f32(v372, v368, v371);
    float32x2_t v387 = vfma_f32(v385, v381, v384);
    float32x2_t v388 = vadd_f32(v244, v257);
    float32x2_t v389 = vadd_f32(v270, v283);
    float32x2_t v390 = vadd_f32(v296, v309);
    float32x2_t v391 = vadd_f32(v322, v335);
    float32x2_t v392 = vadd_f32(v348, v361);
    float32x2_t v393 = vadd_f32(v374, v387);
    float32x2_t v394 = vsub_f32(v244, v257);
    float32x2_t v395 = vsub_f32(v270, v283);
    float32x2_t v396 = vsub_f32(v296, v309);
    float32x2_t v397 = vsub_f32(v322, v335);
    float32x2_t v398 = vsub_f32(v348, v361);
    float32x2_t v399 = vsub_f32(v374, v387);
    float32x2_t v400 = vadd_f32(v389, v392);
    float32x2_t v402 = vadd_f32(v388, v390);
    float32x2_t v405 = vadd_f32(v395, v398);
    float32x2_t v407 = vadd_f32(v394, v396);
    float32x2_t v409 = vsub_f32(v389, v393);
    float32x2_t v410 = vsub_f32(v390, v391);
    float32x2_t v411 = vsub_f32(v388, v391);
    float32x2_t v412 = vsub_f32(v392, v393);
    float32x2_t v417 = vsub_f32(v395, v399);
    float32x2_t v418 = vsub_f32(v394, v396);
    float32x2_t v419 = vsub_f32(v395, v398);
    float32x2_t v420 = vadd_f32(v394, v397);
    float32x2_t v421 = vsub_f32(v398, v399);
    float32x2_t v422 = vadd_f32(v396, v397);
    float32x2_t v401 = vadd_f32(v400, v393);
    float32x2_t v403 = vadd_f32(v402, v391);
    float32x2_t v406 = vadd_f32(v405, v399);
    float32x2_t v408 = vsub_f32(v407, v397);
    float32x2_t v413 = vsub_f32(v409, v410);
    float32x2_t v414 = vsub_f32(v411, v412);
    float32x2_t v415 = vadd_f32(v409, v410);
    float32x2_t v416 = vadd_f32(v411, v412);
    float32x2_t v433 = vadd_f32(v417, v418);
    float32x2_t v434 = vadd_f32(v419, v420);
    float32x2_t v435 = vsub_f32(v421, v422);
    float32x2_t v498 = vrev64_f32(v417);
    float32x2_t v505 = vrev64_f32(v418);
    float32x2_t v519 = vrev64_f32(v419);
    float32x2_t v526 = vrev64_f32(v420);
    float32x2_t v540 = vrev64_f32(v421);
    float32x2_t v547 = vrev64_f32(v422);
    float32x2_t v404 = vadd_f32(v401, v403);
    float32x2_t v429 = vsub_f32(v403, v401);
    float32x2_t v430 = vadd_f32(v406, v408);
    float32x2_t v431 = vadd_f32(v413, v414);
    float32x2_t v432 = vsub_f32(v415, v416);
    float32x2_t v453 = vrev64_f32(v406);
    float32x2_t v460 = vrev64_f32(v408);
    float32x2_t v472 = vmul_f32(v413, v471);
    float32x2_t v476 = vmul_f32(v414, v475);
    float32x2_t v484 = vmul_f32(v415, v483);
    float32x2_t v488 = vmul_f32(v416, v487);
    float32x2_t v499 = vmul_f32(v498, v497);
    float32x2_t v506 = vmul_f32(v505, v504);
    float32x2_t v512 = vrev64_f32(v433);
    float32x2_t v520 = vmul_f32(v519, v518);
    float32x2_t v527 = vmul_f32(v526, v525);
    float32x2_t v533 = vrev64_f32(v434);
    float32x2_t v541 = vmul_f32(v540, v539);
    float32x2_t v548 = vmul_f32(v547, v546);
    float32x2_t v554 = vrev64_f32(v435);
    float32x2_t v428 = vadd_f32(v427, v404);
    float32x2_t v443 = vmul_f32(v404, v442);
    float32x2_t v447 = vmul_f32(v429, v446);
    float32x2_t v454 = vmul_f32(v453, v452);
    float32x2_t v461 = vmul_f32(v460, v459);
    float32x2_t v467 = vrev64_f32(v430);
    float32x2_t v480 = vmul_f32(v431, v479);
    float32x2_t v492 = vmul_f32(v432, v491);
    float32x2_t v513 = vmul_f32(v512, v511);
    float32x2_t v534 = vmul_f32(v533, v532);
    float32x2_t v555 = vmul_f32(v554, v553);
    float32x2_t v557 = vadd_f32(v476, v472);
    float32x2_t v468 = vmul_f32(v467, v466);
    float32x2_t v556 = vsub_f32(v428, v443);
    float32x2_t v558 = vsub_f32(v557, v447);
    float32x2_t v559 = vadd_f32(v476, v480);
    float32x2_t v561 = vsub_f32(v480, v472);
    float32x2_t v569 = vsub_f32(v499, v513);
    float32x2_t v570 = vsub_f32(v506, v513);
    float32x2_t v571 = vsub_f32(v520, v534);
    float32x2_t v572 = vsub_f32(v527, v534);
    float32x2_t v573 = vsub_f32(v541, v555);
    float32x2_t v574 = vadd_f32(v548, v555);
    int16x4_t v609 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v428, 15), (int32x2_t){0, 0}));
    float32x2_t v560 = vadd_f32(v559, v447);
    float32x2_t v562 = vsub_f32(v561, v447);
    float32x2_t v563 = vadd_f32(v556, v484);
    float32x2_t v565 = vsub_f32(v556, v488);
    float32x2_t v567 = vsub_f32(v556, v484);
    float32x2_t v575 = vsub_f32(v454, v468);
    float32x2_t v576 = vsub_f32(v461, v468);
    float32x2_t v587 = vadd_f32(v569, v573);
    float32x2_t v589 = vadd_f32(v571, v573);
    float32x2_t v591 = vsub_f32(v570, v574);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v609), 0);
    float32x2_t v564 = vadd_f32(v563, v488);
    float32x2_t v566 = vsub_f32(v565, v492);
    float32x2_t v568 = vadd_f32(v567, v492);
    float32x2_t v583 = vsub_f32(v576, v569);
    float32x2_t v585 = vsub_f32(v574, v575);
    float32x2_t v588 = vadd_f32(v587, v576);
    float32x2_t v590 = vsub_f32(v589, v576);
    float32x2_t v592 = vsub_f32(v591, v575);
    float32x2_t v593 = vadd_f32(v575, v570);
    float32x2_t v577 = vadd_f32(v558, v564);
    float32x2_t v578 = vadd_f32(v560, v566);
    float32x2_t v579 = vsub_f32(v566, v560);
    float32x2_t v580 = vadd_f32(v562, v568);
    float32x2_t v581 = vsub_f32(v564, v558);
    float32x2_t v582 = vsub_f32(v568, v562);
    float32x2_t v584 = vadd_f32(v583, v571);
    float32x2_t v586 = vsub_f32(v585, v572);
    float32x2_t v594 = vsub_f32(v593, v572);
    float32x2_t v595 = vsub_f32(v577, v584);
    float32x2_t v596 = vadd_f32(v578, v586);
    float32x2_t v597 = vsub_f32(v579, v588);
    float32x2_t v598 = vsub_f32(v580, v590);
    float32x2_t v599 = vadd_f32(v581, v592);
    float32x2_t v600 = vsub_f32(v582, v594);
    float32x2_t v601 = vadd_f32(v582, v594);
    float32x2_t v602 = vsub_f32(v581, v592);
    float32x2_t v603 = vadd_f32(v580, v590);
    float32x2_t v604 = vadd_f32(v579, v588);
    float32x2_t v605 = vsub_f32(v578, v586);
    float32x2_t v606 = vadd_f32(v577, v584);
    int16x4_t v615 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v595, 15), (int32x2_t){0, 0}));
    int16x4_t v621 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v596, 15), (int32x2_t){0, 0}));
    int16x4_t v627 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v597, 15), (int32x2_t){0, 0}));
    int16x4_t v633 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v598, 15), (int32x2_t){0, 0}));
    int16x4_t v639 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v599, 15), (int32x2_t){0, 0}));
    int16x4_t v645 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v600, 15), (int32x2_t){0, 0}));
    int16x4_t v651 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v601, 15), (int32x2_t){0, 0}));
    int16x4_t v657 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v602, 15), (int32x2_t){0, 0}));
    int16x4_t v663 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v603, 15), (int32x2_t){0, 0}));
    int16x4_t v669 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v604, 15), (int32x2_t){0, 0}));
    int16x4_t v675 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v605, 15), (int32x2_t){0, 0}));
    int16x4_t v681 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v606, 15), (int32x2_t){0, 0}));
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v615), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v621), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v627), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v633), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v639), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v645), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v651), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v657), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v663), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v669), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v675), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v681), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu13(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v326 = 1.0833333333333333e+00F;
    float v331 = -3.0046260628866578e-01F;
    float v336 = -7.4927933062613905e-01F;
    float v343 = -4.0100212832186721e-01F;
    float v350 = -5.7514072947400308e-01F;
    float v357 = 5.2422663952658211e-01F;
    float v362 = 5.1652078062348972e-01F;
    float v367 = 7.7058589030924258e-03F;
    float v372 = 4.2763404682656941e-01F;
    float v377 = 1.5180597207438440e-01F;
    float v382 = 5.7944001890096386e-01F;
    float v387 = -1.1543953381323635e+00F;
    float v394 = -9.0655220171271012e-01F;
    float v401 = -8.1857027294591811e-01F;
    float v408 = -1.1971367726043427e+00F;
    float v415 = -8.6131170741789742e-01F;
    float v422 = -1.1091548438375507e+00F;
    float v429 = -4.2741434471979367e-02F;
    float v436 = 4.5240494294812715e-02F;
    float v443 = -2.9058457089163264e-01F;
    const float32x2_t *v610 = &v5[v0];
    int32_t *v860 = &v6[v2];
    int64_t v33 = v0 * 12;
    int64_t v47 = v0 * 2;
    int64_t v61 = v0 * 11;
    int64_t v75 = v0 * 3;
    int64_t v89 = v0 * 10;
    int64_t v103 = v0 * 4;
    int64_t v117 = v0 * 9;
    int64_t v131 = v0 * 5;
    int64_t v145 = v0 * 8;
    int64_t v159 = v0 * 6;
    int64_t v173 = v0 * 7;
    int64_t v195 = v10 * 11;
    int64_t v209 = v10 * 10;
    int64_t v216 = v10 * 2;
    int64_t v223 = v10 * 9;
    int64_t v230 = v10 * 3;
    int64_t v237 = v10 * 8;
    int64_t v244 = v10 * 4;
    int64_t v251 = v10 * 7;
    int64_t v258 = v10 * 5;
    int64_t v265 = v10 * 6;
    int64_t v266 = v13 * 12;
    float v339 = v4 * v336;
    float v346 = v4 * v343;
    float v353 = v4 * v350;
    float v390 = v4 * v387;
    float v397 = v4 * v394;
    float v404 = v4 * v401;
    float v411 = v4 * v408;
    float v418 = v4 * v415;
    float v425 = v4 * v422;
    float v432 = v4 * v429;
    float v439 = v4 * v436;
    float v446 = v4 * v443;
    int64_t v509 = v2 * 12;
    int64_t v517 = v2 * 11;
    int64_t v525 = v2 * 10;
    int64_t v533 = v2 * 9;
    int64_t v541 = v2 * 8;
    int64_t v549 = v2 * 7;
    int64_t v557 = v2 * 6;
    int64_t v565 = v2 * 5;
    int64_t v573 = v2 * 4;
    int64_t v581 = v2 * 3;
    int64_t v589 = v2 * 2;
    const float32x2_t *v721 = &v5[0];
    svint64_t v722 = svindex_s64(0, v1);
    svfloat32_t v725 = svdup_n_f32(v326);
    svfloat32_t v726 = svdup_n_f32(v331);
    svfloat32_t v730 = svdup_n_f32(v357);
    svfloat32_t v731 = svdup_n_f32(v362);
    svfloat32_t v732 = svdup_n_f32(v367);
    svfloat32_t v733 = svdup_n_f32(v372);
    svfloat32_t v734 = svdup_n_f32(v377);
    svfloat32_t v735 = svdup_n_f32(v382);
    int32_t *v752 = &v6[0];
    svfloat32_t v191 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v266]));
    int64_t v197 = v195 + v266;
    int64_t v204 = v10 + v266;
    int64_t v211 = v209 + v266;
    int64_t v218 = v216 + v266;
    int64_t v225 = v223 + v266;
    int64_t v232 = v230 + v266;
    int64_t v239 = v237 + v266;
    int64_t v246 = v244 + v266;
    int64_t v253 = v251 + v266;
    int64_t v260 = v258 + v266;
    int64_t v267 = v265 + v266;
    svfloat32_t v612 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v610), v722));
    const float32x2_t *v620 = &v5[v33];
    const float32x2_t *v629 = &v5[v47];
    const float32x2_t *v638 = &v5[v61];
    const float32x2_t *v647 = &v5[v75];
    const float32x2_t *v656 = &v5[v89];
    const float32x2_t *v665 = &v5[v103];
    const float32x2_t *v674 = &v5[v117];
    const float32x2_t *v683 = &v5[v131];
    const float32x2_t *v692 = &v5[v145];
    const float32x2_t *v701 = &v5[v159];
    const float32x2_t *v710 = &v5[v173];
    svfloat32_t v723 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v721), v722));
    svfloat32_t v727 = svdup_n_f32(v339);
    svfloat32_t v728 = svdup_n_f32(v346);
    svfloat32_t v729 = svdup_n_f32(v353);
    svfloat32_t v736 = svdup_n_f32(v390);
    svfloat32_t v737 = svdup_n_f32(v397);
    svfloat32_t v738 = svdup_n_f32(v404);
    svfloat32_t v739 = svdup_n_f32(v411);
    svfloat32_t v740 = svdup_n_f32(v418);
    svfloat32_t v741 = svdup_n_f32(v425);
    svfloat32_t v742 = svdup_n_f32(v432);
    svfloat32_t v743 = svdup_n_f32(v439);
    svfloat32_t v744 = svdup_n_f32(v446);
    int32_t *v761 = &v6[v509];
    int32_t *v770 = &v6[v517];
    int32_t *v779 = &v6[v525];
    int32_t *v788 = &v6[v533];
    int32_t *v797 = &v6[v541];
    int32_t *v806 = &v6[v549];
    int32_t *v815 = &v6[v557];
    int32_t *v824 = &v6[v565];
    int32_t *v833 = &v6[v573];
    int32_t *v842 = &v6[v581];
    int32_t *v851 = &v6[v589];
    svfloat32_t zero192 = svdup_n_f32(0);
    svfloat32_t v192 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero192, v612, v191, 0),
                     v612, v191, 90);
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v219 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v218]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v233 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v232]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v246]));
    svfloat32_t v254 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v253]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v622 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v620), v722));
    svfloat32_t v631 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v629), v722));
    svfloat32_t v640 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v638), v722));
    svfloat32_t v649 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v647), v722));
    svfloat32_t v658 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v656), v722));
    svfloat32_t v667 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v665), v722));
    svfloat32_t v676 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v674), v722));
    svfloat32_t v685 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v683), v722));
    svfloat32_t v694 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v692), v722));
    svfloat32_t v703 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v701), v722));
    svfloat32_t v712 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v710), v722));
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v622, v198, 0),
                     v622, v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v631, v205, 0),
                     v631, v205, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v640, v212, 0),
                     v640, v212, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero220, v649, v219, 0),
                     v649, v219, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero227, v658, v226, 0),
                     v658, v226, 90);
    svfloat32_t zero234 = svdup_n_f32(0);
    svfloat32_t v234 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero234, v667, v233, 0),
                     v667, v233, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero241, v676, v240, 0),
                     v676, v240, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero248, v685, v247, 0),
                     v685, v247, 90);
    svfloat32_t zero255 = svdup_n_f32(0);
    svfloat32_t v255 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero255, v694, v254, 0),
                     v694, v254, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero262, v703, v261, 0),
                     v703, v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero269, v712, v268, 0),
                     v712, v268, 90);
    svfloat32_t v270 = svadd_f32_x(svptrue_b32(), v192, v199);
    svfloat32_t v271 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v272 = svadd_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v273 = svadd_f32_x(svptrue_b32(), v234, v241);
    svfloat32_t v274 = svadd_f32_x(svptrue_b32(), v248, v255);
    svfloat32_t v275 = svadd_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v276 = svsub_f32_x(svptrue_b32(), v192, v199);
    svfloat32_t v277 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v278 = svsub_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v279 = svsub_f32_x(svptrue_b32(), v234, v241);
    svfloat32_t v280 = svsub_f32_x(svptrue_b32(), v248, v255);
    svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v271, v274);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v270, v272);
    svfloat32_t v287 = svadd_f32_x(svptrue_b32(), v277, v280);
    svfloat32_t v289 = svadd_f32_x(svptrue_b32(), v276, v278);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v271, v275);
    svfloat32_t v292 = svsub_f32_x(svptrue_b32(), v272, v273);
    svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v270, v273);
    svfloat32_t v294 = svsub_f32_x(svptrue_b32(), v274, v275);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v277, v281);
    svfloat32_t v300 = svsub_f32_x(svptrue_b32(), v276, v278);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v277, v280);
    svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v276, v279);
    svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v280, v281);
    svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v278, v279);
    svfloat32_t v283 = svadd_f32_x(svptrue_b32(), v282, v275);
    svfloat32_t v285 = svadd_f32_x(svptrue_b32(), v284, v273);
    svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v287, v281);
    svfloat32_t v290 = svsub_f32_x(svptrue_b32(), v289, v279);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v291, v292);
    svfloat32_t v296 = svsub_f32_x(svptrue_b32(), v293, v294);
    svfloat32_t v297 = svadd_f32_x(svptrue_b32(), v291, v292);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v293, v294);
    svfloat32_t v317 = svadd_f32_x(svptrue_b32(), v299, v300);
    svfloat32_t v318 = svadd_f32_x(svptrue_b32(), v301, v302);
    svfloat32_t v319 = svsub_f32_x(svptrue_b32(), v303, v304);
    svfloat32_t zero392 = svdup_n_f32(0);
    svfloat32_t v392 = svcmla_f32_x(pred_full, zero392, v736, v299, 90);
    svfloat32_t zero399 = svdup_n_f32(0);
    svfloat32_t v399 = svcmla_f32_x(pred_full, zero399, v737, v300, 90);
    svfloat32_t zero413 = svdup_n_f32(0);
    svfloat32_t v413 = svcmla_f32_x(pred_full, zero413, v739, v301, 90);
    svfloat32_t zero420 = svdup_n_f32(0);
    svfloat32_t v420 = svcmla_f32_x(pred_full, zero420, v740, v302, 90);
    svfloat32_t zero434 = svdup_n_f32(0);
    svfloat32_t v434 = svcmla_f32_x(pred_full, zero434, v742, v303, 90);
    svfloat32_t v286 = svadd_f32_x(svptrue_b32(), v283, v285);
    svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v285, v283);
    svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v288, v290);
    svfloat32_t v315 = svadd_f32_x(svptrue_b32(), v295, v296);
    svfloat32_t v316 = svsub_f32_x(svptrue_b32(), v297, v298);
    svfloat32_t zero341 = svdup_n_f32(0);
    svfloat32_t v341 = svcmla_f32_x(pred_full, zero341, v727, v288, 90);
    svfloat32_t zero348 = svdup_n_f32(0);
    svfloat32_t v348 = svcmla_f32_x(pred_full, zero348, v728, v290, 90);
    svfloat32_t v360 = svmul_f32_x(svptrue_b32(), v295, v730);
    svfloat32_t zero406 = svdup_n_f32(0);
    svfloat32_t v406 = svcmla_f32_x(pred_full, zero406, v738, v317, 90);
    svfloat32_t zero427 = svdup_n_f32(0);
    svfloat32_t v427 = svcmla_f32_x(pred_full, zero427, v741, v318, 90);
    svfloat32_t zero448 = svdup_n_f32(0);
    svfloat32_t v448 = svcmla_f32_x(pred_full, zero448, v744, v319, 90);
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v723, v286);
    svfloat32_t zero355 = svdup_n_f32(0);
    svfloat32_t v355 = svcmla_f32_x(pred_full, zero355, v729, v314, 90);
    svfloat32_t v370 = svmul_f32_x(svptrue_b32(), v315, v732);
    svfloat32_t v450 = svmla_f32_x(pred_full, v360, v296, v731);
    svfloat32_t v462 = svsub_f32_x(svptrue_b32(), v392, v406);
    svfloat32_t v463 = svsub_f32_x(svptrue_b32(), v399, v406);
    svfloat32_t v464 = svsub_f32_x(svptrue_b32(), v413, v427);
    svfloat32_t v465 = svsub_f32_x(svptrue_b32(), v420, v427);
    svfloat32_t v466 = svsub_f32_x(svptrue_b32(), v434, v448);
    svfloat32_t v467 = svcmla_f32_x(pred_full, v448, v743, v304, 90);
    svfloat32_t v449 = svmls_f32_x(pred_full, v312, v286, v725);
    svfloat32_t v451 = svmls_f32_x(pred_full, v450, v313, v726);
    svfloat32_t v452 = svmla_f32_x(pred_full, v370, v296, v731);
    svfloat32_t v454 = svnmls_f32_x(pred_full, v360, v315, v732);
    svfloat32_t v468 = svsub_f32_x(svptrue_b32(), v341, v355);
    svfloat32_t v469 = svsub_f32_x(svptrue_b32(), v348, v355);
    svfloat32_t v480 = svadd_f32_x(svptrue_b32(), v462, v466);
    svfloat32_t v482 = svadd_f32_x(svptrue_b32(), v464, v466);
    svfloat32_t v484 = svsub_f32_x(svptrue_b32(), v463, v467);
    svint16_t v502 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v312, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v453 = svmla_f32_x(pred_full, v452, v313, v726);
    svfloat32_t v455 = svmls_f32_x(pred_full, v454, v313, v726);
    svfloat32_t v456 = svmla_f32_x(pred_full, v449, v297, v733);
    svfloat32_t v458 = svmls_f32_x(pred_full, v449, v298, v734);
    svfloat32_t v460 = svmls_f32_x(pred_full, v449, v297, v733);
    svfloat32_t v476 = svsub_f32_x(svptrue_b32(), v469, v462);
    svfloat32_t v478 = svsub_f32_x(svptrue_b32(), v467, v468);
    svfloat32_t v481 = svadd_f32_x(svptrue_b32(), v480, v469);
    svfloat32_t v483 = svsub_f32_x(svptrue_b32(), v482, v469);
    svfloat32_t v485 = svsub_f32_x(svptrue_b32(), v484, v468);
    svfloat32_t v486 = svadd_f32_x(svptrue_b32(), v468, v463);
    svst1w_u64(pred_full, (unsigned *)(v752), svreinterpret_u64_s16(v502));
    svfloat32_t v457 = svmla_f32_x(pred_full, v456, v298, v734);
    svfloat32_t v459 = svmls_f32_x(pred_full, v458, v316, v735);
    svfloat32_t v461 = svmla_f32_x(pred_full, v460, v316, v735);
    svfloat32_t v477 = svadd_f32_x(svptrue_b32(), v476, v464);
    svfloat32_t v479 = svsub_f32_x(svptrue_b32(), v478, v465);
    svfloat32_t v487 = svsub_f32_x(svptrue_b32(), v486, v465);
    svfloat32_t v470 = svadd_f32_x(svptrue_b32(), v451, v457);
    svfloat32_t v471 = svadd_f32_x(svptrue_b32(), v453, v459);
    svfloat32_t v472 = svsub_f32_x(svptrue_b32(), v459, v453);
    svfloat32_t v473 = svadd_f32_x(svptrue_b32(), v455, v461);
    svfloat32_t v474 = svsub_f32_x(svptrue_b32(), v457, v451);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v461, v455);
    svfloat32_t v488 = svsub_f32_x(svptrue_b32(), v470, v477);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v471, v479);
    svfloat32_t v490 = svsub_f32_x(svptrue_b32(), v472, v481);
    svfloat32_t v491 = svsub_f32_x(svptrue_b32(), v473, v483);
    svfloat32_t v492 = svadd_f32_x(svptrue_b32(), v474, v485);
    svfloat32_t v493 = svsub_f32_x(svptrue_b32(), v475, v487);
    svfloat32_t v494 = svadd_f32_x(svptrue_b32(), v475, v487);
    svfloat32_t v495 = svsub_f32_x(svptrue_b32(), v474, v485);
    svfloat32_t v496 = svadd_f32_x(svptrue_b32(), v473, v483);
    svfloat32_t v497 = svadd_f32_x(svptrue_b32(), v472, v481);
    svfloat32_t v498 = svsub_f32_x(svptrue_b32(), v471, v479);
    svfloat32_t v499 = svadd_f32_x(svptrue_b32(), v470, v477);
    svint16_t v510 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v488, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v518 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v489, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v526 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v490, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v534 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v491, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v542 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v492, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v550 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v493, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v558 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v494, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v566 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v495, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v574 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v496, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v582 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v497, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v590 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v498, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v598 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v499, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v761), svreinterpret_u64_s16(v510));
    svst1w_u64(pred_full, (unsigned *)(v770), svreinterpret_u64_s16(v518));
    svst1w_u64(pred_full, (unsigned *)(v779), svreinterpret_u64_s16(v526));
    svst1w_u64(pred_full, (unsigned *)(v788), svreinterpret_u64_s16(v534));
    svst1w_u64(pred_full, (unsigned *)(v797), svreinterpret_u64_s16(v542));
    svst1w_u64(pred_full, (unsigned *)(v806), svreinterpret_u64_s16(v550));
    svst1w_u64(pred_full, (unsigned *)(v815), svreinterpret_u64_s16(v558));
    svst1w_u64(pred_full, (unsigned *)(v824), svreinterpret_u64_s16(v566));
    svst1w_u64(pred_full, (unsigned *)(v833), svreinterpret_u64_s16(v574));
    svst1w_u64(pred_full, (unsigned *)(v842), svreinterpret_u64_s16(v582));
    svst1w_u64(pred_full, (unsigned *)(v851), svreinterpret_u64_s16(v590));
    svst1w_u64(pred_full, (unsigned *)(v860), svreinterpret_u64_s16(v598));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu14(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v255 = v5[istride];
    float v544 = -1.1666666666666665e+00F;
    float v548 = 7.9015646852540022e-01F;
    float v552 = 5.5854267289647742e-02F;
    float v556 = 7.3430220123575241e-01F;
    float v559 = 4.4095855184409838e-01F;
    float v560 = -4.4095855184409838e-01F;
    float v566 = 3.4087293062393137e-01F;
    float v567 = -3.4087293062393137e-01F;
    float v573 = -5.3396936033772524e-01F;
    float v574 = 5.3396936033772524e-01F;
    float v580 = 8.7484229096165667e-01F;
    float v581 = -8.7484229096165667e-01F;
    float32x2_t v583 = (float32x2_t){v4, v4};
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    float32x2_t v423 = v5[0];
    float32x2_t v545 = (float32x2_t){v544, v544};
    float32x2_t v549 = (float32x2_t){v548, v548};
    float32x2_t v553 = (float32x2_t){v552, v552};
    float32x2_t v557 = (float32x2_t){v556, v556};
    float32x2_t v561 = (float32x2_t){v559, v560};
    float32x2_t v568 = (float32x2_t){v566, v567};
    float32x2_t v575 = (float32x2_t){v573, v574};
    float32x2_t v582 = (float32x2_t){v580, v581};
    float32x2_t v20 = v5[istride * 7];
    int64_t v37 = 12 + j * 26;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 9];
    int64_t v86 = 2 + j * 26;
    int64_t v99 = 16 + j * 26;
    float32x2_t v113 = v5[istride * 4];
    float32x2_t v131 = v5[istride * 11];
    int64_t v148 = 6 + j * 26;
    int64_t v161 = 20 + j * 26;
    float32x2_t v175 = v5[istride * 6];
    float32x2_t v193 = v5[istride * 13];
    int64_t v210 = 10 + j * 26;
    int64_t v223 = 24 + j * 26;
    float32x2_t v237 = v5[istride * 8];
    int64_t v272 = 14 + j * 26;
    float32x2_t v286 = v7[j * 26];
    int64_t v290 = j * 26 + 1;
    float32x2_t v299 = v5[istride * 10];
    float32x2_t v317 = v5[istride * 3];
    int64_t v334 = 18 + j * 26;
    int64_t v347 = 4 + j * 26;
    float32x2_t v361 = v5[istride * 12];
    float32x2_t v379 = v5[istride * 5];
    int64_t v396 = 22 + j * 26;
    int64_t v409 = 8 + j * 26;
    float32x2_t v563 = vmul_f32(v583, v561);
    float32x2_t v570 = vmul_f32(v583, v568);
    float32x2_t v577 = vmul_f32(v583, v575);
    float32x2_t v584 = vmul_f32(v583, v582);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v273 = v7[v272];
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    int64_t v277 = v272 + 1;
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v335 = v7[v334];
    float32x2_t v336 = vtrn1_f32(v299, v299);
    float32x2_t v337 = vtrn2_f32(v299, v299);
    int64_t v339 = v334 + 1;
    float32x2_t v348 = v7[v347];
    float32x2_t v349 = vtrn1_f32(v317, v317);
    float32x2_t v350 = vtrn2_f32(v317, v317);
    int64_t v352 = v347 + 1;
    float32x2_t v397 = v7[v396];
    float32x2_t v398 = vtrn1_f32(v361, v361);
    float32x2_t v399 = vtrn2_f32(v361, v361);
    int64_t v401 = v396 + 1;
    float32x2_t v410 = v7[v409];
    float32x2_t v411 = vtrn1_f32(v379, v379);
    float32x2_t v412 = vtrn2_f32(v379, v379);
    int64_t v414 = v409 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vmul_f32(v336, v335);
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vmul_f32(v349, v348);
    float32x2_t v402 = v7[v401];
    float32x2_t v403 = vmul_f32(v398, v397);
    float32x2_t v415 = v7[v414];
    float32x2_t v416 = vmul_f32(v411, v410);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v343 = vfma_f32(v341, v337, v340);
    float32x2_t v356 = vfma_f32(v354, v350, v353);
    float32x2_t v405 = vfma_f32(v403, v399, v402);
    float32x2_t v418 = vfma_f32(v416, v412, v415);
    float32x2_t v424 = vadd_f32(v423, v46);
    float32x2_t v425 = vsub_f32(v423, v46);
    float32x2_t v426 = vadd_f32(v95, v108);
    float32x2_t v427 = vsub_f32(v95, v108);
    float32x2_t v428 = vadd_f32(v157, v170);
    float32x2_t v429 = vsub_f32(v157, v170);
    float32x2_t v430 = vadd_f32(v219, v232);
    float32x2_t v431 = vsub_f32(v219, v232);
    float32x2_t v432 = vadd_f32(v281, v294);
    float32x2_t v433 = vsub_f32(v281, v294);
    float32x2_t v434 = vadd_f32(v343, v356);
    float32x2_t v435 = vsub_f32(v343, v356);
    float32x2_t v436 = vadd_f32(v405, v418);
    float32x2_t v437 = vsub_f32(v405, v418);
    float32x2_t v438 = vadd_f32(v426, v436);
    float32x2_t v439 = vsub_f32(v426, v436);
    float32x2_t v440 = vadd_f32(v432, v430);
    float32x2_t v441 = vsub_f32(v432, v430);
    float32x2_t v442 = vadd_f32(v428, v434);
    float32x2_t v443 = vsub_f32(v428, v434);
    float32x2_t v522 = vadd_f32(v427, v437);
    float32x2_t v523 = vsub_f32(v427, v437);
    float32x2_t v524 = vadd_f32(v433, v431);
    float32x2_t v525 = vsub_f32(v433, v431);
    float32x2_t v526 = vadd_f32(v429, v435);
    float32x2_t v527 = vsub_f32(v429, v435);
    float32x2_t v444 = vadd_f32(v438, v440);
    float32x2_t v447 = vsub_f32(v438, v440);
    float32x2_t v448 = vsub_f32(v440, v442);
    float32x2_t v449 = vsub_f32(v442, v438);
    float32x2_t v450 = vadd_f32(v439, v441);
    float32x2_t v452 = vsub_f32(v439, v441);
    float32x2_t v453 = vsub_f32(v441, v443);
    float32x2_t v454 = vsub_f32(v443, v439);
    float32x2_t v528 = vadd_f32(v522, v524);
    float32x2_t v531 = vsub_f32(v522, v524);
    float32x2_t v532 = vsub_f32(v524, v526);
    float32x2_t v533 = vsub_f32(v526, v522);
    float32x2_t v534 = vadd_f32(v523, v525);
    float32x2_t v536 = vsub_f32(v523, v525);
    float32x2_t v537 = vsub_f32(v525, v527);
    float32x2_t v538 = vsub_f32(v527, v523);
    float32x2_t v445 = vadd_f32(v444, v442);
    float32x2_t v451 = vadd_f32(v450, v443);
    float32x2_t v466 = vmul_f32(v447, v549);
    float32x2_t v470 = vmul_f32(v448, v553);
    float32x2_t v474 = vmul_f32(v449, v557);
    float32x2_t v487 = vrev64_f32(v452);
    float32x2_t v494 = vrev64_f32(v453);
    float32x2_t v501 = vrev64_f32(v454);
    float32x2_t v529 = vadd_f32(v528, v526);
    float32x2_t v535 = vadd_f32(v534, v527);
    float32x2_t v550 = vmul_f32(v531, v549);
    float32x2_t v554 = vmul_f32(v532, v553);
    float32x2_t v558 = vmul_f32(v533, v557);
    float32x2_t v571 = vrev64_f32(v536);
    float32x2_t v578 = vrev64_f32(v537);
    float32x2_t v585 = vrev64_f32(v538);
    float32x2_t v446 = vadd_f32(v445, v424);
    float32x2_t v462 = vmul_f32(v445, v545);
    float32x2_t v480 = vrev64_f32(v451);
    float32x2_t v488 = vmul_f32(v487, v570);
    float32x2_t v495 = vmul_f32(v494, v577);
    float32x2_t v502 = vmul_f32(v501, v584);
    float32x2_t v530 = vadd_f32(v529, v425);
    float32x2_t v546 = vmul_f32(v529, v545);
    float32x2_t v564 = vrev64_f32(v535);
    float32x2_t v572 = vmul_f32(v571, v570);
    float32x2_t v579 = vmul_f32(v578, v577);
    float32x2_t v586 = vmul_f32(v585, v584);
    float32x2_t v481 = vmul_f32(v480, v563);
    float32x2_t v503 = vadd_f32(v446, v462);
    float32x2_t v565 = vmul_f32(v564, v563);
    float32x2_t v587 = vadd_f32(v530, v546);
    int16x4_t v608 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v446, 15), (int32x2_t){0, 0}));
    int16x4_t v614 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v530, 15), (int32x2_t){0, 0}));
    float32x2_t v504 = vadd_f32(v503, v466);
    float32x2_t v506 = vsub_f32(v503, v466);
    float32x2_t v508 = vsub_f32(v503, v470);
    float32x2_t v510 = vadd_f32(v481, v488);
    float32x2_t v512 = vsub_f32(v481, v488);
    float32x2_t v514 = vsub_f32(v481, v495);
    float32x2_t v588 = vadd_f32(v587, v550);
    float32x2_t v590 = vsub_f32(v587, v550);
    float32x2_t v592 = vsub_f32(v587, v554);
    float32x2_t v594 = vadd_f32(v565, v572);
    float32x2_t v596 = vsub_f32(v565, v572);
    float32x2_t v598 = vsub_f32(v565, v579);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v608), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v614), 0);
    float32x2_t v505 = vadd_f32(v504, v470);
    float32x2_t v507 = vsub_f32(v506, v474);
    float32x2_t v509 = vadd_f32(v508, v474);
    float32x2_t v511 = vadd_f32(v510, v495);
    float32x2_t v513 = vsub_f32(v512, v502);
    float32x2_t v515 = vadd_f32(v514, v502);
    float32x2_t v589 = vadd_f32(v588, v554);
    float32x2_t v591 = vsub_f32(v590, v558);
    float32x2_t v593 = vadd_f32(v592, v558);
    float32x2_t v595 = vadd_f32(v594, v579);
    float32x2_t v597 = vsub_f32(v596, v586);
    float32x2_t v599 = vadd_f32(v598, v586);
    float32x2_t v516 = vadd_f32(v505, v511);
    float32x2_t v517 = vsub_f32(v505, v511);
    float32x2_t v518 = vadd_f32(v507, v513);
    float32x2_t v519 = vsub_f32(v507, v513);
    float32x2_t v520 = vadd_f32(v509, v515);
    float32x2_t v521 = vsub_f32(v509, v515);
    float32x2_t v600 = vadd_f32(v589, v595);
    float32x2_t v601 = vsub_f32(v589, v595);
    float32x2_t v602 = vadd_f32(v591, v597);
    float32x2_t v603 = vsub_f32(v591, v597);
    float32x2_t v604 = vadd_f32(v593, v599);
    float32x2_t v605 = vsub_f32(v593, v599);
    int16x4_t v620 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v517, 15), (int32x2_t){0, 0}));
    int16x4_t v626 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v601, 15), (int32x2_t){0, 0}));
    int16x4_t v632 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v519, 15), (int32x2_t){0, 0}));
    int16x4_t v638 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v603, 15), (int32x2_t){0, 0}));
    int16x4_t v644 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v520, 15), (int32x2_t){0, 0}));
    int16x4_t v650 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v604, 15), (int32x2_t){0, 0}));
    int16x4_t v656 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v521, 15), (int32x2_t){0, 0}));
    int16x4_t v662 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v605, 15), (int32x2_t){0, 0}));
    int16x4_t v668 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v518, 15), (int32x2_t){0, 0}));
    int16x4_t v674 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v602, 15), (int32x2_t){0, 0}));
    int16x4_t v680 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v516, 15), (int32x2_t){0, 0}));
    int16x4_t v686 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v600, 15), (int32x2_t){0, 0}));
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v620), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v626), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v632), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v638), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v644), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v650), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v656), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v662), 0);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v668), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v674), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v680), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v686), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu14(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v424 = -1.1666666666666665e+00F;
    float v429 = 7.9015646852540022e-01F;
    float v434 = 5.5854267289647742e-02F;
    float v439 = 7.3430220123575241e-01F;
    float v444 = -4.4095855184409838e-01F;
    float v451 = -3.4087293062393137e-01F;
    float v458 = 5.3396936033772524e-01F;
    float v465 = -8.7484229096165667e-01F;
    const float32x2_t *v680 = &v5[v0];
    int32_t *v783 = &v6[v2];
    int64_t v19 = v0 * 7;
    int64_t v34 = v10 * 6;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 9;
    int64_t v76 = v10 * 8;
    int64_t v82 = v0 * 4;
    int64_t v96 = v0 * 11;
    int64_t v111 = v10 * 3;
    int64_t v118 = v10 * 10;
    int64_t v124 = v0 * 6;
    int64_t v138 = v0 * 13;
    int64_t v153 = v10 * 5;
    int64_t v160 = v10 * 12;
    int64_t v166 = v0 * 8;
    int64_t v195 = v10 * 7;
    int64_t v208 = v0 * 10;
    int64_t v222 = v0 * 3;
    int64_t v237 = v10 * 9;
    int64_t v244 = v10 * 2;
    int64_t v250 = v0 * 12;
    int64_t v264 = v0 * 5;
    int64_t v279 = v10 * 11;
    int64_t v286 = v10 * 4;
    int64_t v287 = v13 * 13;
    float v447 = v4 * v444;
    float v454 = v4 * v451;
    float v461 = v4 * v458;
    float v468 = v4 * v465;
    int64_t v499 = v2 * 7;
    int64_t v507 = v2 * 8;
    int64_t v523 = v2 * 2;
    int64_t v531 = v2 * 9;
    int64_t v539 = v2 * 10;
    int64_t v547 = v2 * 3;
    int64_t v555 = v2 * 4;
    int64_t v563 = v2 * 11;
    int64_t v571 = v2 * 12;
    int64_t v579 = v2 * 5;
    int64_t v587 = v2 * 6;
    int64_t v595 = v2 * 13;
    const float32x2_t *v728 = &v5[0];
    svint64_t v729 = svindex_s64(0, v1);
    svfloat32_t v741 = svdup_n_f32(v424);
    svfloat32_t v742 = svdup_n_f32(v429);
    svfloat32_t v743 = svdup_n_f32(v434);
    svfloat32_t v744 = svdup_n_f32(v439);
    int32_t *v756 = &v6[0];
    int64_t v36 = v34 + v287;
    int64_t v71 = v10 + v287;
    int64_t v78 = v76 + v287;
    int64_t v113 = v111 + v287;
    int64_t v120 = v118 + v287;
    int64_t v155 = v153 + v287;
    int64_t v162 = v160 + v287;
    int64_t v197 = v195 + v287;
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v287]));
    int64_t v239 = v237 + v287;
    int64_t v246 = v244 + v287;
    int64_t v281 = v279 + v287;
    int64_t v288 = v286 + v287;
    const float32x2_t *v608 = &v5[v19];
    const float32x2_t *v617 = &v5[v40];
    const float32x2_t *v626 = &v5[v54];
    const float32x2_t *v635 = &v5[v82];
    const float32x2_t *v644 = &v5[v96];
    const float32x2_t *v653 = &v5[v124];
    const float32x2_t *v662 = &v5[v138];
    const float32x2_t *v671 = &v5[v166];
    svfloat32_t v682 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v680), v729));
    const float32x2_t *v691 = &v5[v208];
    const float32x2_t *v700 = &v5[v222];
    const float32x2_t *v709 = &v5[v250];
    const float32x2_t *v718 = &v5[v264];
    svfloat32_t v730 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v728), v729));
    svfloat32_t v745 = svdup_n_f32(v447);
    svfloat32_t v746 = svdup_n_f32(v454);
    svfloat32_t v747 = svdup_n_f32(v461);
    svfloat32_t v748 = svdup_n_f32(v468);
    int32_t *v765 = &v6[v499];
    int32_t *v774 = &v6[v507];
    int32_t *v792 = &v6[v523];
    int32_t *v801 = &v6[v531];
    int32_t *v810 = &v6[v539];
    int32_t *v819 = &v6[v547];
    int32_t *v828 = &v6[v555];
    int32_t *v837 = &v6[v563];
    int32_t *v846 = &v6[v571];
    int32_t *v855 = &v6[v579];
    int32_t *v864 = &v6[v587];
    int32_t *v873 = &v6[v595];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v682, v205, 0),
                     v682, v205, 90);
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v246]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v288]));
    svfloat32_t v610 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v608), v729));
    svfloat32_t v619 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v617), v729));
    svfloat32_t v628 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v626), v729));
    svfloat32_t v637 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v635), v729));
    svfloat32_t v646 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v644), v729));
    svfloat32_t v655 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v653), v729));
    svfloat32_t v664 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v662), v729));
    svfloat32_t v673 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v671), v729));
    svfloat32_t v693 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v691), v729));
    svfloat32_t v702 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v700), v729));
    svfloat32_t v711 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v709), v729));
    svfloat32_t v720 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v718), v729));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v610, v37, 0),
                     v610, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v619, v72, 0),
                     v619, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v628, v79, 0),
                     v628, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v637, v114, 0),
                     v637, v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v646, v121, 0),
                     v646, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v655, v156, 0),
                     v655, v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v664, v163, 0),
                     v664, v163, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v673, v198, 0),
                     v673, v198, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero241, v693, v240, 0),
                     v693, v240, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero248, v702, v247, 0),
                     v702, v247, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero283, v711, v282, 0),
                     v711, v282, 90);
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero290, v720, v289, 0),
                     v720, v289, 90);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v730, v38);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v730, v38);
    svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v308 = svadd_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v300, v310);
    svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v300, v310);
    svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v306, v304);
    svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v306, v304);
    svfloat32_t v316 = svadd_f32_x(svptrue_b32(), v302, v308);
    svfloat32_t v317 = svsub_f32_x(svptrue_b32(), v302, v308);
    svfloat32_t v401 = svadd_f32_x(svptrue_b32(), v301, v311);
    svfloat32_t v402 = svsub_f32_x(svptrue_b32(), v301, v311);
    svfloat32_t v403 = svadd_f32_x(svptrue_b32(), v307, v305);
    svfloat32_t v404 = svsub_f32_x(svptrue_b32(), v307, v305);
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v303, v309);
    svfloat32_t v406 = svsub_f32_x(svptrue_b32(), v303, v309);
    svfloat32_t v318 = svadd_f32_x(svptrue_b32(), v312, v314);
    svfloat32_t v321 = svsub_f32_x(svptrue_b32(), v312, v314);
    svfloat32_t v322 = svsub_f32_x(svptrue_b32(), v314, v316);
    svfloat32_t v323 = svsub_f32_x(svptrue_b32(), v316, v312);
    svfloat32_t v324 = svadd_f32_x(svptrue_b32(), v313, v315);
    svfloat32_t v326 = svsub_f32_x(svptrue_b32(), v313, v315);
    svfloat32_t v327 = svsub_f32_x(svptrue_b32(), v315, v317);
    svfloat32_t v328 = svsub_f32_x(svptrue_b32(), v317, v313);
    svfloat32_t v407 = svadd_f32_x(svptrue_b32(), v401, v403);
    svfloat32_t v410 = svsub_f32_x(svptrue_b32(), v401, v403);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v403, v405);
    svfloat32_t v412 = svsub_f32_x(svptrue_b32(), v405, v401);
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v402, v404);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v402, v404);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v404, v406);
    svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v406, v402);
    svfloat32_t v319 = svadd_f32_x(svptrue_b32(), v318, v316);
    svfloat32_t v325 = svadd_f32_x(svptrue_b32(), v324, v317);
    svfloat32_t zero367 = svdup_n_f32(0);
    svfloat32_t v367 = svcmla_f32_x(pred_full, zero367, v746, v326, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 = svcmla_f32_x(pred_full, zero374, v747, v327, 90);
    svfloat32_t zero381 = svdup_n_f32(0);
    svfloat32_t v381 = svcmla_f32_x(pred_full, zero381, v748, v328, 90);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v407, v405);
    svfloat32_t v414 = svadd_f32_x(svptrue_b32(), v413, v406);
    svfloat32_t zero456 = svdup_n_f32(0);
    svfloat32_t v456 = svcmla_f32_x(pred_full, zero456, v746, v415, 90);
    svfloat32_t zero463 = svdup_n_f32(0);
    svfloat32_t v463 = svcmla_f32_x(pred_full, zero463, v747, v416, 90);
    svfloat32_t zero470 = svdup_n_f32(0);
    svfloat32_t v470 = svcmla_f32_x(pred_full, zero470, v748, v417, 90);
    svfloat32_t v320 = svadd_f32_x(svptrue_b32(), v319, v298);
    svfloat32_t zero360 = svdup_n_f32(0);
    svfloat32_t v360 = svcmla_f32_x(pred_full, zero360, v745, v325, 90);
    svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v408, v299);
    svfloat32_t zero449 = svdup_n_f32(0);
    svfloat32_t v449 = svcmla_f32_x(pred_full, zero449, v745, v414, 90);
    svfloat32_t v382 = svmla_f32_x(pred_full, v320, v319, v741);
    svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v360, v367);
    svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v360, v367);
    svfloat32_t v393 = svsub_f32_x(svptrue_b32(), v360, v374);
    svfloat32_t v471 = svmla_f32_x(pred_full, v409, v408, v741);
    svfloat32_t v478 = svadd_f32_x(svptrue_b32(), v449, v456);
    svfloat32_t v480 = svsub_f32_x(svptrue_b32(), v449, v456);
    svfloat32_t v482 = svsub_f32_x(svptrue_b32(), v449, v463);
    svint16_t v492 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v320, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v500 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v409, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v383 = svmla_f32_x(pred_full, v382, v321, v742);
    svfloat32_t v385 = svmls_f32_x(pred_full, v382, v321, v742);
    svfloat32_t v387 = svmls_f32_x(pred_full, v382, v322, v743);
    svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v389, v374);
    svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v391, v381);
    svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v393, v381);
    svfloat32_t v472 = svmla_f32_x(pred_full, v471, v410, v742);
    svfloat32_t v474 = svmls_f32_x(pred_full, v471, v410, v742);
    svfloat32_t v476 = svmls_f32_x(pred_full, v471, v411, v743);
    svfloat32_t v479 = svadd_f32_x(svptrue_b32(), v478, v463);
    svfloat32_t v481 = svsub_f32_x(svptrue_b32(), v480, v470);
    svfloat32_t v483 = svadd_f32_x(svptrue_b32(), v482, v470);
    svst1w_u64(pred_full, (unsigned *)(v756), svreinterpret_u64_s16(v492));
    svst1w_u64(pred_full, (unsigned *)(v765), svreinterpret_u64_s16(v500));
    svfloat32_t v384 = svmla_f32_x(pred_full, v383, v322, v743);
    svfloat32_t v386 = svmls_f32_x(pred_full, v385, v323, v744);
    svfloat32_t v388 = svmla_f32_x(pred_full, v387, v323, v744);
    svfloat32_t v473 = svmla_f32_x(pred_full, v472, v411, v743);
    svfloat32_t v475 = svmls_f32_x(pred_full, v474, v412, v744);
    svfloat32_t v477 = svmla_f32_x(pred_full, v476, v412, v744);
    svfloat32_t v395 = svadd_f32_x(svptrue_b32(), v384, v390);
    svfloat32_t v396 = svsub_f32_x(svptrue_b32(), v384, v390);
    svfloat32_t v397 = svadd_f32_x(svptrue_b32(), v386, v392);
    svfloat32_t v398 = svsub_f32_x(svptrue_b32(), v386, v392);
    svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v388, v394);
    svfloat32_t v400 = svsub_f32_x(svptrue_b32(), v388, v394);
    svfloat32_t v484 = svadd_f32_x(svptrue_b32(), v473, v479);
    svfloat32_t v485 = svsub_f32_x(svptrue_b32(), v473, v479);
    svfloat32_t v486 = svadd_f32_x(svptrue_b32(), v475, v481);
    svfloat32_t v487 = svsub_f32_x(svptrue_b32(), v475, v481);
    svfloat32_t v488 = svadd_f32_x(svptrue_b32(), v477, v483);
    svfloat32_t v489 = svsub_f32_x(svptrue_b32(), v477, v483);
    svint16_t v508 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v396, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v516 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v485, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v524 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v398, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v532 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v487, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v540 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v399, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v548 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v488, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v556 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v400, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v564 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v489, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v572 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v397, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v580 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v486, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v588 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v395, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v596 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v484, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v774), svreinterpret_u64_s16(v508));
    svst1w_u64(pred_full, (unsigned *)(v783), svreinterpret_u64_s16(v516));
    svst1w_u64(pred_full, (unsigned *)(v792), svreinterpret_u64_s16(v524));
    svst1w_u64(pred_full, (unsigned *)(v801), svreinterpret_u64_s16(v532));
    svst1w_u64(pred_full, (unsigned *)(v810), svreinterpret_u64_s16(v540));
    svst1w_u64(pred_full, (unsigned *)(v819), svreinterpret_u64_s16(v548));
    svst1w_u64(pred_full, (unsigned *)(v828), svreinterpret_u64_s16(v556));
    svst1w_u64(pred_full, (unsigned *)(v837), svreinterpret_u64_s16(v564));
    svst1w_u64(pred_full, (unsigned *)(v846), svreinterpret_u64_s16(v572));
    svst1w_u64(pred_full, (unsigned *)(v855), svreinterpret_u64_s16(v580));
    svst1w_u64(pred_full, (unsigned *)(v864), svreinterpret_u64_s16(v588));
    svst1w_u64(pred_full, (unsigned *)(v873), svreinterpret_u64_s16(v596));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu15(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v180 = v5[istride];
    float v431 = -1.2500000000000000e+00F;
    float v435 = 5.5901699437494745e-01F;
    float v438 = 1.5388417685876268e+00F;
    float v439 = -1.5388417685876268e+00F;
    float v445 = 5.8778525229247325e-01F;
    float v446 = -5.8778525229247325e-01F;
    float v452 = 3.6327126400268028e-01F;
    float v453 = -3.6327126400268028e-01F;
    float v477 = -1.4999999999999998e+00F;
    float v481 = 1.8749999999999998e+00F;
    float v485 = -8.3852549156242107e-01F;
    float v488 = -2.3082626528814396e+00F;
    float v489 = 2.3082626528814396e+00F;
    float v495 = -8.8167787843870971e-01F;
    float v496 = 8.8167787843870971e-01F;
    float v502 = -5.4490689600402031e-01F;
    float v503 = 5.4490689600402031e-01F;
    float v526 = 8.6602540378443871e-01F;
    float v527 = -8.6602540378443871e-01F;
    float v533 = -1.0825317547305484e+00F;
    float v534 = 1.0825317547305484e+00F;
    float v540 = 4.8412291827592718e-01F;
    float v541 = -4.8412291827592718e-01F;
    float32x2_t v543 = (float32x2_t){v4, v4};
    float v548 = -1.3326760640014592e+00F;
    float v552 = -5.0903696045512736e-01F;
    float v556 = -3.1460214309120460e-01F;
    float32x2_t v212 = vtrn1_f32(v180, v180);
    float32x2_t v213 = vtrn2_f32(v180, v180);
    float32x2_t v404 = v5[0];
    float32x2_t v432 = (float32x2_t){v431, v431};
    float32x2_t v436 = (float32x2_t){v435, v435};
    float32x2_t v440 = (float32x2_t){v438, v439};
    float32x2_t v447 = (float32x2_t){v445, v446};
    float32x2_t v454 = (float32x2_t){v452, v453};
    float32x2_t v478 = (float32x2_t){v477, v477};
    float32x2_t v482 = (float32x2_t){v481, v481};
    float32x2_t v486 = (float32x2_t){v485, v485};
    float32x2_t v490 = (float32x2_t){v488, v489};
    float32x2_t v497 = (float32x2_t){v495, v496};
    float32x2_t v504 = (float32x2_t){v502, v503};
    float32x2_t v528 = (float32x2_t){v526, v527};
    float32x2_t v535 = (float32x2_t){v533, v534};
    float32x2_t v542 = (float32x2_t){v540, v541};
    float32x2_t v549 = (float32x2_t){v548, v548};
    float32x2_t v553 = (float32x2_t){v552, v552};
    float32x2_t v557 = (float32x2_t){v556, v556};
    float32x2_t v20 = v5[istride * 5];
    float32x2_t v38 = v5[istride * 10];
    int64_t v55 = 8 + j * 28;
    int64_t v68 = 18 + j * 28;
    float32x2_t v82 = v5[istride * 8];
    float32x2_t v100 = v5[istride * 13];
    int64_t v117 = 14 + j * 28;
    int64_t v130 = 24 + j * 28;
    float32x2_t v144 = v5[istride * 3];
    int64_t v148 = 4 + j * 28;
    float32x2_t v162 = v5[istride * 11];
    int64_t v197 = 20 + j * 28;
    float32x2_t v211 = v7[j * 28];
    int64_t v215 = j * 28 + 1;
    float32x2_t v224 = v5[istride * 6];
    int64_t v228 = 10 + j * 28;
    float32x2_t v242 = v5[istride * 14];
    float32x2_t v260 = v5[istride * 4];
    int64_t v277 = 26 + j * 28;
    int64_t v290 = 6 + j * 28;
    float32x2_t v304 = v5[istride * 9];
    int64_t v308 = 16 + j * 28;
    float32x2_t v322 = v5[istride * 2];
    float32x2_t v340 = v5[istride * 7];
    int64_t v357 = 2 + j * 28;
    int64_t v370 = 12 + j * 28;
    float32x2_t v384 = v5[istride * 12];
    int64_t v388 = 22 + j * 28;
    float32x2_t v442 = vmul_f32(v543, v440);
    float32x2_t v449 = vmul_f32(v543, v447);
    float32x2_t v456 = vmul_f32(v543, v454);
    float32x2_t v492 = vmul_f32(v543, v490);
    float32x2_t v499 = vmul_f32(v543, v497);
    float32x2_t v506 = vmul_f32(v543, v504);
    float32x2_t v530 = vmul_f32(v543, v528);
    float32x2_t v537 = vmul_f32(v543, v535);
    float32x2_t v544 = vmul_f32(v543, v542);
    float32x2_t v56 = v7[v55];
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    int64_t v60 = v55 + 1;
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v144, v144);
    float32x2_t v151 = vtrn2_f32(v144, v144);
    int64_t v153 = v148 + 1;
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vtrn1_f32(v162, v162);
    float32x2_t v200 = vtrn2_f32(v162, v162);
    int64_t v202 = v197 + 1;
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vtrn1_f32(v224, v224);
    float32x2_t v231 = vtrn2_f32(v224, v224);
    int64_t v233 = v228 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vtrn1_f32(v242, v242);
    float32x2_t v280 = vtrn2_f32(v242, v242);
    int64_t v282 = v277 + 1;
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vtrn1_f32(v260, v260);
    float32x2_t v293 = vtrn2_f32(v260, v260);
    int64_t v295 = v290 + 1;
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vtrn1_f32(v304, v304);
    float32x2_t v311 = vtrn2_f32(v304, v304);
    int64_t v313 = v308 + 1;
    float32x2_t v358 = v7[v357];
    float32x2_t v359 = vtrn1_f32(v322, v322);
    float32x2_t v360 = vtrn2_f32(v322, v322);
    int64_t v362 = v357 + 1;
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vtrn1_f32(v340, v340);
    float32x2_t v373 = vtrn2_f32(v340, v340);
    int64_t v375 = v370 + 1;
    float32x2_t v389 = v7[v388];
    float32x2_t v390 = vtrn1_f32(v384, v384);
    float32x2_t v391 = vtrn2_f32(v384, v384);
    int64_t v393 = v388 + 1;
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v203 = v7[v202];
    float32x2_t v204 = vmul_f32(v199, v198);
    float32x2_t v234 = v7[v233];
    float32x2_t v235 = vmul_f32(v230, v229);
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vmul_f32(v292, v291);
    float32x2_t v314 = v7[v313];
    float32x2_t v315 = vmul_f32(v310, v309);
    float32x2_t v363 = v7[v362];
    float32x2_t v364 = vmul_f32(v359, v358);
    float32x2_t v376 = v7[v375];
    float32x2_t v377 = vmul_f32(v372, v371);
    float32x2_t v394 = v7[v393];
    float32x2_t v395 = vmul_f32(v390, v389);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v206 = vfma_f32(v204, v200, v203);
    float32x2_t v237 = vfma_f32(v235, v231, v234);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v299 = vfma_f32(v297, v293, v296);
    float32x2_t v317 = vfma_f32(v315, v311, v314);
    float32x2_t v366 = vfma_f32(v364, v360, v363);
    float32x2_t v379 = vfma_f32(v377, v373, v376);
    float32x2_t v397 = vfma_f32(v395, v391, v394);
    float32x2_t v398 = vadd_f32(v64, v77);
    float32x2_t v399 = vsub_f32(v64, v77);
    float32x2_t v406 = vadd_f32(v126, v139);
    float32x2_t v407 = vsub_f32(v126, v139);
    float32x2_t v409 = vadd_f32(v206, v219);
    float32x2_t v410 = vsub_f32(v206, v219);
    float32x2_t v412 = vadd_f32(v286, v299);
    float32x2_t v413 = vsub_f32(v286, v299);
    float32x2_t v415 = vadd_f32(v366, v379);
    float32x2_t v416 = vsub_f32(v366, v379);
    float32x2_t v405 = vadd_f32(v398, v404);
    float32x2_t v408 = vadd_f32(v406, v157);
    float32x2_t v411 = vadd_f32(v409, v237);
    float32x2_t v414 = vadd_f32(v412, v317);
    float32x2_t v417 = vadd_f32(v415, v397);
    float32x2_t v468 = vadd_f32(v406, v415);
    float32x2_t v469 = vsub_f32(v406, v415);
    float32x2_t v470 = vadd_f32(v412, v409);
    float32x2_t v471 = vsub_f32(v412, v409);
    float32x2_t v518 = vadd_f32(v407, v416);
    float32x2_t v519 = vsub_f32(v407, v416);
    float32x2_t v520 = vadd_f32(v413, v410);
    float32x2_t v521 = vsub_f32(v413, v410);
    float32x2_t v418 = vadd_f32(v408, v417);
    float32x2_t v419 = vsub_f32(v408, v417);
    float32x2_t v420 = vadd_f32(v414, v411);
    float32x2_t v421 = vsub_f32(v414, v411);
    float32x2_t v472 = vadd_f32(v468, v470);
    float32x2_t v473 = vsub_f32(v468, v470);
    float32x2_t v474 = vadd_f32(v469, v471);
    float32x2_t v493 = vrev64_f32(v469);
    float32x2_t v507 = vrev64_f32(v471);
    float32x2_t v522 = vadd_f32(v518, v520);
    float32x2_t v523 = vsub_f32(v518, v520);
    float32x2_t v524 = vadd_f32(v519, v521);
    float32x2_t v550 = vmul_f32(v519, v549);
    float32x2_t v558 = vmul_f32(v521, v557);
    float32x2_t v422 = vadd_f32(v418, v420);
    float32x2_t v423 = vsub_f32(v418, v420);
    float32x2_t v424 = vadd_f32(v419, v421);
    float32x2_t v443 = vrev64_f32(v419);
    float32x2_t v457 = vrev64_f32(v421);
    float32x2_t v475 = vadd_f32(v472, v398);
    float32x2_t v483 = vmul_f32(v472, v482);
    float32x2_t v487 = vmul_f32(v473, v486);
    float32x2_t v494 = vmul_f32(v493, v492);
    float32x2_t v500 = vrev64_f32(v474);
    float32x2_t v508 = vmul_f32(v507, v506);
    float32x2_t v525 = vadd_f32(v522, v399);
    float32x2_t v538 = vrev64_f32(v522);
    float32x2_t v545 = vrev64_f32(v523);
    float32x2_t v554 = vmul_f32(v524, v553);
    float32x2_t v425 = vadd_f32(v422, v405);
    float32x2_t v433 = vmul_f32(v422, v432);
    float32x2_t v437 = vmul_f32(v423, v436);
    float32x2_t v444 = vmul_f32(v443, v442);
    float32x2_t v450 = vrev64_f32(v424);
    float32x2_t v458 = vmul_f32(v457, v456);
    float32x2_t v479 = vmul_f32(v475, v478);
    float32x2_t v501 = vmul_f32(v500, v499);
    float32x2_t v531 = vrev64_f32(v525);
    float32x2_t v539 = vmul_f32(v538, v537);
    float32x2_t v546 = vmul_f32(v545, v544);
    float32x2_t v562 = vsub_f32(v550, v554);
    float32x2_t v563 = vadd_f32(v554, v558);
    float32x2_t v451 = vmul_f32(v450, v449);
    float32x2_t v459 = vadd_f32(v425, v433);
    float32x2_t v509 = vadd_f32(v479, v483);
    float32x2_t v512 = vsub_f32(v494, v501);
    float32x2_t v513 = vadd_f32(v501, v508);
    float32x2_t v532 = vmul_f32(v531, v530);
    float32x2_t v568 = vadd_f32(v425, v479);
    int16x4_t v573 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v425, 15), (int32x2_t){0, 0}));
    float32x2_t v460 = vadd_f32(v459, v437);
    float32x2_t v461 = vsub_f32(v459, v437);
    float32x2_t v462 = vsub_f32(v444, v451);
    float32x2_t v463 = vadd_f32(v451, v458);
    float32x2_t v510 = vadd_f32(v509, v487);
    float32x2_t v511 = vsub_f32(v509, v487);
    float32x2_t v559 = vadd_f32(v532, v539);
    float32x2_t v569 = vadd_f32(v568, v532);
    float32x2_t v570 = vsub_f32(v568, v532);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v573), 0);
    float32x2_t v464 = vadd_f32(v460, v462);
    float32x2_t v465 = vsub_f32(v460, v462);
    float32x2_t v466 = vadd_f32(v461, v463);
    float32x2_t v467 = vsub_f32(v461, v463);
    float32x2_t v514 = vadd_f32(v510, v512);
    float32x2_t v515 = vsub_f32(v510, v512);
    float32x2_t v516 = vadd_f32(v511, v513);
    float32x2_t v517 = vsub_f32(v511, v513);
    float32x2_t v560 = vadd_f32(v559, v546);
    float32x2_t v561 = vsub_f32(v559, v546);
    int16x4_t v579 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v570, 15), (int32x2_t){0, 0}));
    int16x4_t v585 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v569, 15), (int32x2_t){0, 0}));
    float32x2_t v564 = vadd_f32(v560, v562);
    float32x2_t v565 = vsub_f32(v560, v562);
    float32x2_t v566 = vadd_f32(v561, v563);
    float32x2_t v567 = vsub_f32(v561, v563);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v579), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v585), 0);
    float32x2_t v589 = vadd_f32(v465, v515);
    int16x4_t v594 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v465, 15), (int32x2_t){0, 0}));
    float32x2_t v610 = vadd_f32(v467, v517);
    int16x4_t v615 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v467, 15), (int32x2_t){0, 0}));
    float32x2_t v631 = vadd_f32(v466, v516);
    int16x4_t v636 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v466, 15), (int32x2_t){0, 0}));
    float32x2_t v652 = vadd_f32(v464, v514);
    int16x4_t v657 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v464, 15), (int32x2_t){0, 0}));
    float32x2_t v590 = vadd_f32(v589, v565);
    float32x2_t v591 = vsub_f32(v589, v565);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v594), 0);
    float32x2_t v611 = vadd_f32(v610, v567);
    float32x2_t v612 = vsub_f32(v610, v567);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v615), 0);
    float32x2_t v632 = vadd_f32(v631, v566);
    float32x2_t v633 = vsub_f32(v631, v566);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v636), 0);
    float32x2_t v653 = vadd_f32(v652, v564);
    float32x2_t v654 = vsub_f32(v652, v564);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v657), 0);
    int16x4_t v600 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v591, 15), (int32x2_t){0, 0}));
    int16x4_t v606 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v590, 15), (int32x2_t){0, 0}));
    int16x4_t v621 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v612, 15), (int32x2_t){0, 0}));
    int16x4_t v627 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v611, 15), (int32x2_t){0, 0}));
    int16x4_t v642 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v633, 15), (int32x2_t){0, 0}));
    int16x4_t v648 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v632, 15), (int32x2_t){0, 0}));
    int16x4_t v663 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v654, 15), (int32x2_t){0, 0}));
    int16x4_t v669 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v653, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v600), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v606), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v621), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v627), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v642), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v648), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v663), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v669), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu15(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v320 = -1.2500000000000000e+00F;
    float v325 = 5.5901699437494745e-01F;
    float v330 = -1.5388417685876268e+00F;
    float v337 = -5.8778525229247325e-01F;
    float v344 = -3.6327126400268028e-01F;
    float v368 = -1.4999999999999998e+00F;
    float v373 = 1.8749999999999998e+00F;
    float v378 = -8.3852549156242107e-01F;
    float v383 = 2.3082626528814396e+00F;
    float v390 = 8.8167787843870971e-01F;
    float v397 = 5.4490689600402031e-01F;
    float v421 = -8.6602540378443871e-01F;
    float v428 = 1.0825317547305484e+00F;
    float v435 = -4.8412291827592718e-01F;
    float v442 = -1.3326760640014592e+00F;
    float v447 = -5.0903696045512736e-01F;
    float v452 = -3.1460214309120460e-01F;
    const float32x2_t *v660 = &v5[v0];
    int32_t *v799 = &v6[v2];
    int64_t v19 = v0 * 5;
    int64_t v33 = v0 * 10;
    int64_t v48 = v10 * 4;
    int64_t v55 = v10 * 9;
    int64_t v61 = v0 * 8;
    int64_t v75 = v0 * 13;
    int64_t v90 = v10 * 7;
    int64_t v97 = v10 * 12;
    int64_t v103 = v0 * 3;
    int64_t v111 = v10 * 2;
    int64_t v117 = v0 * 11;
    int64_t v146 = v10 * 10;
    int64_t v159 = v0 * 6;
    int64_t v167 = v10 * 5;
    int64_t v173 = v0 * 14;
    int64_t v187 = v0 * 4;
    int64_t v202 = v10 * 13;
    int64_t v209 = v10 * 3;
    int64_t v215 = v0 * 9;
    int64_t v223 = v10 * 8;
    int64_t v229 = v0 * 2;
    int64_t v243 = v0 * 7;
    int64_t v265 = v10 * 6;
    int64_t v271 = v0 * 12;
    int64_t v279 = v10 * 11;
    int64_t v280 = v13 * 14;
    float v333 = v4 * v330;
    float v340 = v4 * v337;
    float v347 = v4 * v344;
    float v386 = v4 * v383;
    float v393 = v4 * v390;
    float v400 = v4 * v397;
    float v424 = v4 * v421;
    float v431 = v4 * v428;
    float v438 = v4 * v435;
    int64_t v477 = v2 * 10;
    int64_t v485 = v2 * 5;
    int64_t v496 = v2 * 6;
    int64_t v512 = v2 * 11;
    int64_t v523 = v2 * 12;
    int64_t v531 = v2 * 7;
    int64_t v539 = v2 * 2;
    int64_t v550 = v2 * 3;
    int64_t v558 = v2 * 13;
    int64_t v566 = v2 * 8;
    int64_t v577 = v2 * 9;
    int64_t v585 = v2 * 4;
    int64_t v593 = v2 * 14;
    const float32x2_t *v735 = &v5[0];
    svint64_t v736 = svindex_s64(0, v1);
    svfloat32_t v739 = svdup_n_f32(v320);
    svfloat32_t v740 = svdup_n_f32(v325);
    svfloat32_t v744 = svdup_n_f32(v368);
    svfloat32_t v745 = svdup_n_f32(v373);
    svfloat32_t v746 = svdup_n_f32(v378);
    svfloat32_t v753 = svdup_n_f32(v442);
    svfloat32_t v754 = svdup_n_f32(v447);
    svfloat32_t v755 = svdup_n_f32(v452);
    int32_t *v763 = &v6[0];
    int64_t v50 = v48 + v280;
    int64_t v57 = v55 + v280;
    int64_t v92 = v90 + v280;
    int64_t v99 = v97 + v280;
    int64_t v113 = v111 + v280;
    int64_t v148 = v146 + v280;
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v280]));
    int64_t v169 = v167 + v280;
    int64_t v204 = v202 + v280;
    int64_t v211 = v209 + v280;
    int64_t v225 = v223 + v280;
    int64_t v260 = v10 + v280;
    int64_t v267 = v265 + v280;
    int64_t v281 = v279 + v280;
    const float32x2_t *v606 = &v5[v19];
    const float32x2_t *v615 = &v5[v33];
    const float32x2_t *v624 = &v5[v61];
    const float32x2_t *v633 = &v5[v75];
    const float32x2_t *v642 = &v5[v103];
    const float32x2_t *v651 = &v5[v117];
    svfloat32_t v662 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v660), v736));
    const float32x2_t *v671 = &v5[v159];
    const float32x2_t *v680 = &v5[v173];
    const float32x2_t *v689 = &v5[v187];
    const float32x2_t *v698 = &v5[v215];
    const float32x2_t *v707 = &v5[v229];
    const float32x2_t *v716 = &v5[v243];
    const float32x2_t *v725 = &v5[v271];
    svfloat32_t v737 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v735), v736));
    svfloat32_t v741 = svdup_n_f32(v333);
    svfloat32_t v742 = svdup_n_f32(v340);
    svfloat32_t v743 = svdup_n_f32(v347);
    svfloat32_t v747 = svdup_n_f32(v386);
    svfloat32_t v748 = svdup_n_f32(v393);
    svfloat32_t v749 = svdup_n_f32(v400);
    svfloat32_t v750 = svdup_n_f32(v424);
    svfloat32_t v751 = svdup_n_f32(v431);
    svfloat32_t v752 = svdup_n_f32(v438);
    int32_t *v772 = &v6[v477];
    int32_t *v781 = &v6[v485];
    int32_t *v790 = &v6[v496];
    int32_t *v808 = &v6[v512];
    int32_t *v817 = &v6[v523];
    int32_t *v826 = &v6[v531];
    int32_t *v835 = &v6[v539];
    int32_t *v844 = &v6[v550];
    int32_t *v853 = &v6[v558];
    int32_t *v862 = &v6[v566];
    int32_t *v871 = &v6[v577];
    int32_t *v880 = &v6[v585];
    int32_t *v889 = &v6[v593];
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v50]));
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v149 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v148]));
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v662, v156, 0),
                     v662, v156, 90);
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v608 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v606), v736));
    svfloat32_t v617 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v615), v736));
    svfloat32_t v626 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v624), v736));
    svfloat32_t v635 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v633), v736));
    svfloat32_t v644 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v642), v736));
    svfloat32_t v653 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v651), v736));
    svfloat32_t v673 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v671), v736));
    svfloat32_t v682 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v680), v736));
    svfloat32_t v691 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v689), v736));
    svfloat32_t v700 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v698), v736));
    svfloat32_t v709 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v707), v736));
    svfloat32_t v718 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v716), v736));
    svfloat32_t v727 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v725), v736));
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v608, v51, 0),
                     v608, v51, 90);
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v617, v58, 0),
                     v617, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v626, v93, 0),
                     v626, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v635, v100, 0),
                     v635, v100, 90);
    svfloat32_t zero150 = svdup_n_f32(0);
    svfloat32_t v150 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero150, v653, v149, 0),
                     v653, v149, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v682, v205, 0),
                     v682, v205, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v691, v212, 0),
                     v691, v212, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero262, v709, v261, 0),
                     v709, v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero269, v718, v268, 0),
                     v718, v268, 90);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v297 = svadd_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v298 = svsub_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v303 = svadd_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v304 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v293 = svadd_f32_x(svptrue_b32(), v284, v737);
    svfloat32_t v296 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v294, v644, v114, 0),
                     v644, v114, 90);
    svfloat32_t v299 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v297, v673, v170, 0),
                     v673, v170, 90);
    svfloat32_t v302 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v300, v700, v226, 0),
                     v700, v226, 90);
    svfloat32_t v305 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v303, v727, v282, 0),
                     v727, v282, 90);
    svfloat32_t v359 = svadd_f32_x(svptrue_b32(), v294, v303);
    svfloat32_t v360 = svsub_f32_x(svptrue_b32(), v294, v303);
    svfloat32_t v361 = svadd_f32_x(svptrue_b32(), v300, v297);
    svfloat32_t v362 = svsub_f32_x(svptrue_b32(), v300, v297);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v295, v304);
    svfloat32_t v413 = svsub_f32_x(svptrue_b32(), v295, v304);
    svfloat32_t v414 = svadd_f32_x(svptrue_b32(), v301, v298);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v301, v298);
    svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v296, v305);
    svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v296, v305);
    svfloat32_t v308 = svadd_f32_x(svptrue_b32(), v302, v299);
    svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v302, v299);
    svfloat32_t v363 = svadd_f32_x(svptrue_b32(), v359, v361);
    svfloat32_t v364 = svsub_f32_x(svptrue_b32(), v359, v361);
    svfloat32_t v365 = svadd_f32_x(svptrue_b32(), v360, v362);
    svfloat32_t zero388 = svdup_n_f32(0);
    svfloat32_t v388 = svcmla_f32_x(pred_full, zero388, v747, v360, 90);
    svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v412, v414);
    svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v412, v414);
    svfloat32_t v418 = svadd_f32_x(svptrue_b32(), v413, v415);
    svfloat32_t v455 = svmul_f32_x(svptrue_b32(), v415, v755);
    svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v306, v308);
    svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v306, v308);
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v307, v309);
    svfloat32_t zero335 = svdup_n_f32(0);
    svfloat32_t v335 = svcmla_f32_x(pred_full, zero335, v741, v307, 90);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v363, v284);
    svfloat32_t v376 = svmul_f32_x(svptrue_b32(), v363, v745);
    svfloat32_t zero395 = svdup_n_f32(0);
    svfloat32_t v395 = svcmla_f32_x(pred_full, zero395, v748, v365, 90);
    svfloat32_t v419 = svadd_f32_x(svptrue_b32(), v416, v285);
    svfloat32_t zero440 = svdup_n_f32(0);
    svfloat32_t v440 = svcmla_f32_x(pred_full, zero440, v752, v417, 90);
    svfloat32_t v450 = svmul_f32_x(svptrue_b32(), v418, v754);
    svfloat32_t v313 = svadd_f32_x(svptrue_b32(), v310, v293);
    svfloat32_t zero342 = svdup_n_f32(0);
    svfloat32_t v342 = svcmla_f32_x(pred_full, zero342, v742, v312, 90);
    svfloat32_t v406 = svsub_f32_x(svptrue_b32(), v388, v395);
    svfloat32_t v407 = svcmla_f32_x(pred_full, v395, v749, v362, 90);
    svfloat32_t zero426 = svdup_n_f32(0);
    svfloat32_t v426 = svcmla_f32_x(pred_full, zero426, v750, v419, 90);
    svfloat32_t v459 = svnmls_f32_x(pred_full, v450, v413, v753);
    svfloat32_t v460 = svmla_f32_x(pred_full, v455, v418, v754);
    svfloat32_t v350 = svmla_f32_x(pred_full, v313, v310, v739);
    svfloat32_t v353 = svsub_f32_x(svptrue_b32(), v335, v342);
    svfloat32_t v354 = svcmla_f32_x(pred_full, v342, v743, v309, 90);
    svfloat32_t v403 = svmla_f32_x(pred_full, v376, v366, v744);
    svfloat32_t v456 = svcmla_f32_x(pred_full, v426, v751, v416, 90);
    svfloat32_t v465 = svmla_f32_x(pred_full, v313, v366, v744);
    svint16_t v470 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v313, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v351 = svmla_f32_x(pred_full, v350, v311, v740);
    svfloat32_t v352 = svmls_f32_x(pred_full, v350, v311, v740);
    svfloat32_t v404 = svmla_f32_x(pred_full, v403, v364, v746);
    svfloat32_t v405 = svmls_f32_x(pred_full, v403, v364, v746);
    svfloat32_t v457 = svadd_f32_x(svptrue_b32(), v456, v440);
    svfloat32_t v458 = svsub_f32_x(svptrue_b32(), v456, v440);
    svfloat32_t v466 = svadd_f32_x(svptrue_b32(), v465, v426);
    svfloat32_t v467 = svsub_f32_x(svptrue_b32(), v465, v426);
    svst1w_u64(pred_full, (unsigned *)(v763), svreinterpret_u64_s16(v470));
    svfloat32_t v355 = svadd_f32_x(svptrue_b32(), v351, v353);
    svfloat32_t v356 = svsub_f32_x(svptrue_b32(), v351, v353);
    svfloat32_t v357 = svadd_f32_x(svptrue_b32(), v352, v354);
    svfloat32_t v358 = svsub_f32_x(svptrue_b32(), v352, v354);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v404, v406);
    svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v404, v406);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v405, v407);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v405, v407);
    svfloat32_t v461 = svadd_f32_x(svptrue_b32(), v457, v459);
    svfloat32_t v462 = svsub_f32_x(svptrue_b32(), v457, v459);
    svfloat32_t v463 = svadd_f32_x(svptrue_b32(), v458, v460);
    svfloat32_t v464 = svsub_f32_x(svptrue_b32(), v458, v460);
    svint16_t v478 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v467, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v486 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v466, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v492 = svadd_f32_x(svptrue_b32(), v356, v409);
    svint16_t v497 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v356, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v519 = svadd_f32_x(svptrue_b32(), v358, v411);
    svint16_t v524 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v358, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v546 = svadd_f32_x(svptrue_b32(), v357, v410);
    svint16_t v551 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v357, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v573 = svadd_f32_x(svptrue_b32(), v355, v408);
    svint16_t v578 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v355, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v772), svreinterpret_u64_s16(v478));
    svst1w_u64(pred_full, (unsigned *)(v781), svreinterpret_u64_s16(v486));
    svfloat32_t v493 = svadd_f32_x(svptrue_b32(), v492, v462);
    svfloat32_t v494 = svsub_f32_x(svptrue_b32(), v492, v462);
    svfloat32_t v520 = svadd_f32_x(svptrue_b32(), v519, v464);
    svfloat32_t v521 = svsub_f32_x(svptrue_b32(), v519, v464);
    svfloat32_t v547 = svadd_f32_x(svptrue_b32(), v546, v463);
    svfloat32_t v548 = svsub_f32_x(svptrue_b32(), v546, v463);
    svfloat32_t v574 = svadd_f32_x(svptrue_b32(), v573, v461);
    svfloat32_t v575 = svsub_f32_x(svptrue_b32(), v573, v461);
    svst1w_u64(pred_full, (unsigned *)(v790), svreinterpret_u64_s16(v497));
    svst1w_u64(pred_full, (unsigned *)(v817), svreinterpret_u64_s16(v524));
    svst1w_u64(pred_full, (unsigned *)(v844), svreinterpret_u64_s16(v551));
    svst1w_u64(pred_full, (unsigned *)(v871), svreinterpret_u64_s16(v578));
    svint16_t v505 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v494, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v513 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v493, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v532 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v521, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v540 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v520, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v559 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v548, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v567 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v547, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v586 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v575, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v594 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v574, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v799), svreinterpret_u64_s16(v505));
    svst1w_u64(pred_full, (unsigned *)(v808), svreinterpret_u64_s16(v513));
    svst1w_u64(pred_full, (unsigned *)(v826), svreinterpret_u64_s16(v532));
    svst1w_u64(pred_full, (unsigned *)(v835), svreinterpret_u64_s16(v540));
    svst1w_u64(pred_full, (unsigned *)(v853), svreinterpret_u64_s16(v559));
    svst1w_u64(pred_full, (unsigned *)(v862), svreinterpret_u64_s16(v567));
    svst1w_u64(pred_full, (unsigned *)(v880), svreinterpret_u64_s16(v586));
    svst1w_u64(pred_full, (unsigned *)(v889), svreinterpret_u64_s16(v594));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu16(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v237 = v5[istride];
    float v571 = 1.0000000000000000e+00F;
    float v572 = -1.0000000000000000e+00F;
    float v579 = -7.0710678118654746e-01F;
    float v586 = 7.0710678118654757e-01F;
    float v589 = 9.2387953251128674e-01F;
    float v590 = -9.2387953251128674e-01F;
    float v597 = 5.4119610014619690e-01F;
    float v604 = -1.3065629648763766e+00F;
    float32x2_t v606 = (float32x2_t){v4, v4};
    float v611 = 3.8268343236508984e-01F;
    float v615 = 1.3065629648763766e+00F;
    float v619 = -5.4119610014619690e-01F;
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    float32x2_t v485 = v5[0];
    float32x2_t v573 = (float32x2_t){v571, v572};
    float32x2_t v580 = (float32x2_t){v586, v579};
    float32x2_t v587 = (float32x2_t){v586, v586};
    float32x2_t v591 = (float32x2_t){v589, v590};
    float32x2_t v598 = (float32x2_t){v619, v597};
    float32x2_t v605 = (float32x2_t){v615, v604};
    float32x2_t v612 = (float32x2_t){v611, v611};
    float32x2_t v616 = (float32x2_t){v615, v615};
    float32x2_t v620 = (float32x2_t){v619, v619};
    float32x2_t v20 = v5[istride * 8];
    int64_t v37 = 14 + j * 30;
    float32x2_t v51 = v5[istride * 4];
    float32x2_t v69 = v5[istride * 12];
    int64_t v86 = 6 + j * 30;
    int64_t v99 = 22 + j * 30;
    float32x2_t v113 = v5[istride * 2];
    float32x2_t v131 = v5[istride * 10];
    int64_t v148 = 2 + j * 30;
    int64_t v161 = 18 + j * 30;
    float32x2_t v175 = v5[istride * 6];
    float32x2_t v193 = v5[istride * 14];
    int64_t v210 = 10 + j * 30;
    int64_t v223 = 26 + j * 30;
    float32x2_t v255 = v5[istride * 9];
    float32x2_t v273 = v7[j * 30];
    int64_t v277 = j * 30 + 1;
    int64_t v285 = 16 + j * 30;
    float32x2_t v299 = v5[istride * 5];
    float32x2_t v317 = v5[istride * 13];
    int64_t v334 = 8 + j * 30;
    int64_t v347 = 24 + j * 30;
    float32x2_t v361 = v5[istride * 3];
    float32x2_t v379 = v5[istride * 11];
    int64_t v396 = 4 + j * 30;
    int64_t v409 = 20 + j * 30;
    float32x2_t v423 = v5[istride * 7];
    float32x2_t v441 = v5[istride * 15];
    int64_t v458 = 12 + j * 30;
    int64_t v471 = 28 + j * 30;
    float32x2_t v575 = vmul_f32(v606, v573);
    float32x2_t v582 = vmul_f32(v606, v580);
    float32x2_t v593 = vmul_f32(v606, v591);
    float32x2_t v600 = vmul_f32(v606, v598);
    float32x2_t v607 = vmul_f32(v606, v605);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v286 = v7[v285];
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    int64_t v290 = v285 + 1;
    float32x2_t v335 = v7[v334];
    float32x2_t v336 = vtrn1_f32(v299, v299);
    float32x2_t v337 = vtrn2_f32(v299, v299);
    int64_t v339 = v334 + 1;
    float32x2_t v348 = v7[v347];
    float32x2_t v349 = vtrn1_f32(v317, v317);
    float32x2_t v350 = vtrn2_f32(v317, v317);
    int64_t v352 = v347 + 1;
    float32x2_t v397 = v7[v396];
    float32x2_t v398 = vtrn1_f32(v361, v361);
    float32x2_t v399 = vtrn2_f32(v361, v361);
    int64_t v401 = v396 + 1;
    float32x2_t v410 = v7[v409];
    float32x2_t v411 = vtrn1_f32(v379, v379);
    float32x2_t v412 = vtrn2_f32(v379, v379);
    int64_t v414 = v409 + 1;
    float32x2_t v459 = v7[v458];
    float32x2_t v460 = vtrn1_f32(v423, v423);
    float32x2_t v461 = vtrn2_f32(v423, v423);
    int64_t v463 = v458 + 1;
    float32x2_t v472 = v7[v471];
    float32x2_t v473 = vtrn1_f32(v441, v441);
    float32x2_t v474 = vtrn2_f32(v441, v441);
    int64_t v476 = v471 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vmul_f32(v336, v335);
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vmul_f32(v349, v348);
    float32x2_t v402 = v7[v401];
    float32x2_t v403 = vmul_f32(v398, v397);
    float32x2_t v415 = v7[v414];
    float32x2_t v416 = vmul_f32(v411, v410);
    float32x2_t v464 = v7[v463];
    float32x2_t v465 = vmul_f32(v460, v459);
    float32x2_t v477 = v7[v476];
    float32x2_t v478 = vmul_f32(v473, v472);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v343 = vfma_f32(v341, v337, v340);
    float32x2_t v356 = vfma_f32(v354, v350, v353);
    float32x2_t v405 = vfma_f32(v403, v399, v402);
    float32x2_t v418 = vfma_f32(v416, v412, v415);
    float32x2_t v467 = vfma_f32(v465, v461, v464);
    float32x2_t v480 = vfma_f32(v478, v474, v477);
    float32x2_t v486 = vadd_f32(v485, v46);
    float32x2_t v487 = vsub_f32(v485, v46);
    float32x2_t v488 = vadd_f32(v95, v108);
    float32x2_t v489 = vsub_f32(v95, v108);
    float32x2_t v490 = vadd_f32(v157, v170);
    float32x2_t v491 = vsub_f32(v157, v170);
    float32x2_t v492 = vadd_f32(v219, v232);
    float32x2_t v493 = vsub_f32(v219, v232);
    float32x2_t v494 = vadd_f32(v281, v294);
    float32x2_t v495 = vsub_f32(v281, v294);
    float32x2_t v496 = vadd_f32(v343, v356);
    float32x2_t v497 = vsub_f32(v343, v356);
    float32x2_t v498 = vadd_f32(v405, v418);
    float32x2_t v499 = vsub_f32(v405, v418);
    float32x2_t v500 = vadd_f32(v467, v480);
    float32x2_t v501 = vsub_f32(v467, v480);
    float32x2_t v502 = vadd_f32(v486, v488);
    float32x2_t v503 = vsub_f32(v486, v488);
    float32x2_t v504 = vadd_f32(v490, v492);
    float32x2_t v505 = vsub_f32(v490, v492);
    float32x2_t v506 = vadd_f32(v494, v496);
    float32x2_t v507 = vsub_f32(v494, v496);
    float32x2_t v508 = vadd_f32(v498, v500);
    float32x2_t v509 = vsub_f32(v498, v500);
    float32x2_t v518 = vadd_f32(v491, v493);
    float32x2_t v519 = vsub_f32(v491, v493);
    float32x2_t v520 = vadd_f32(v495, v501);
    float32x2_t v521 = vsub_f32(v495, v501);
    float32x2_t v522 = vadd_f32(v497, v499);
    float32x2_t v523 = vsub_f32(v497, v499);
    float32x2_t v576 = vrev64_f32(v489);
    float32x2_t v510 = vadd_f32(v502, v504);
    float32x2_t v511 = vsub_f32(v502, v504);
    float32x2_t v512 = vadd_f32(v506, v508);
    float32x2_t v513 = vsub_f32(v506, v508);
    float32x2_t v516 = vadd_f32(v507, v509);
    float32x2_t v517 = vsub_f32(v507, v509);
    float32x2_t v524 = vadd_f32(v520, v522);
    float32x2_t v525 = vadd_f32(v521, v523);
    float32x2_t v554 = vrev64_f32(v505);
    float32x2_t v577 = vmul_f32(v576, v575);
    float32x2_t v583 = vrev64_f32(v518);
    float32x2_t v588 = vmul_f32(v519, v587);
    float32x2_t v601 = vrev64_f32(v520);
    float32x2_t v608 = vrev64_f32(v522);
    float32x2_t v617 = vmul_f32(v521, v616);
    float32x2_t v621 = vmul_f32(v523, v620);
    float32x2_t v514 = vadd_f32(v510, v512);
    float32x2_t v515 = vsub_f32(v510, v512);
    float32x2_t v543 = vrev64_f32(v513);
    float32x2_t v555 = vmul_f32(v554, v575);
    float32x2_t v561 = vrev64_f32(v516);
    float32x2_t v566 = vmul_f32(v517, v587);
    float32x2_t v584 = vmul_f32(v583, v582);
    float32x2_t v594 = vrev64_f32(v524);
    float32x2_t v602 = vmul_f32(v601, v600);
    float32x2_t v609 = vmul_f32(v608, v607);
    float32x2_t v613 = vmul_f32(v525, v612);
    float32x2_t v632 = vadd_f32(v487, v588);
    float32x2_t v633 = vsub_f32(v487, v588);
    float32x2_t v544 = vmul_f32(v543, v575);
    float32x2_t v562 = vmul_f32(v561, v582);
    float32x2_t v595 = vmul_f32(v594, v593);
    float32x2_t v624 = vadd_f32(v503, v566);
    float32x2_t v626 = vsub_f32(v503, v566);
    float32x2_t v634 = vadd_f32(v577, v584);
    float32x2_t v635 = vsub_f32(v577, v584);
    float32x2_t v638 = vsub_f32(v617, v613);
    float32x2_t v639 = vsub_f32(v621, v613);
    float32x2_t v640 = vsub_f32(v613, v617);
    float32x2_t v641 = vsub_f32(v613, v621);
    int16x4_t v668 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v514, 15), (int32x2_t){0, 0}));
    int16x4_t v716 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v515, 15), (int32x2_t){0, 0}));
    float32x2_t v622 = vadd_f32(v511, v544);
    float32x2_t v623 = vsub_f32(v511, v544);
    float32x2_t v625 = vadd_f32(v555, v562);
    float32x2_t v627 = vsub_f32(v562, v555);
    float32x2_t v636 = vadd_f32(v595, v602);
    float32x2_t v637 = vsub_f32(v595, v609);
    float32x2_t v642 = vadd_f32(v632, v638);
    float32x2_t v643 = vsub_f32(v632, v638);
    float32x2_t v644 = vadd_f32(v632, v640);
    float32x2_t v645 = vsub_f32(v632, v640);
    float32x2_t v646 = vadd_f32(v633, v635);
    float32x2_t v647 = vsub_f32(v633, v635);
    float32x2_t v648 = vadd_f32(v633, v641);
    float32x2_t v649 = vsub_f32(v633, v641);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v668), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v716), 0);
    float32x2_t v628 = vadd_f32(v624, v625);
    float32x2_t v629 = vadd_f32(v626, v627);
    float32x2_t v630 = vsub_f32(v626, v627);
    float32x2_t v631 = vsub_f32(v624, v625);
    float32x2_t v652 = vadd_f32(v636, v634);
    float32x2_t v653 = vsub_f32(v636, v634);
    float32x2_t v654 = vadd_f32(v637, v639);
    float32x2_t v655 = vsub_f32(v637, v639);
    float32x2_t v656 = vadd_f32(v637, v635);
    float32x2_t v657 = vsub_f32(v637, v635);
    int16x4_t v692 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v623, 15), (int32x2_t){0, 0}));
    int16x4_t v740 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v622, 15), (int32x2_t){0, 0}));
    float32x2_t v658 = vadd_f32(v642, v652);
    float32x2_t v659 = vadd_f32(v643, v653);
    float32x2_t v660 = vsub_f32(v644, v653);
    float32x2_t v661 = vsub_f32(v645, v652);
    float32x2_t v662 = vadd_f32(v646, v654);
    float32x2_t v663 = vadd_f32(v647, v655);
    float32x2_t v664 = vsub_f32(v648, v657);
    float32x2_t v665 = vsub_f32(v649, v656);
    int16x4_t v680 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v631, 15), (int32x2_t){0, 0}));
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v692), 0);
    int16x4_t v704 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v630, 15), (int32x2_t){0, 0}));
    int16x4_t v728 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v629, 15), (int32x2_t){0, 0}));
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v740), 0);
    int16x4_t v752 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v628, 15), (int32x2_t){0, 0}));
    int16x4_t v674 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v661, 15), (int32x2_t){0, 0}));
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v680), 0);
    int16x4_t v686 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v664, 15), (int32x2_t){0, 0}));
    int16x4_t v698 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v665, 15), (int32x2_t){0, 0}));
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v704), 0);
    int16x4_t v710 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v660, 15), (int32x2_t){0, 0}));
    int16x4_t v722 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v659, 15), (int32x2_t){0, 0}));
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v728), 0);
    int16x4_t v734 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v662, 15), (int32x2_t){0, 0}));
    int16x4_t v746 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v663, 15), (int32x2_t){0, 0}));
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v752), 0);
    int16x4_t v758 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v658, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v674), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v686), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v698), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v710), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v722), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v734), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v746), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v758), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu16(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v432 = -1.0000000000000000e+00F;
    float v439 = -7.0710678118654746e-01F;
    float v446 = 7.0710678118654757e-01F;
    float v451 = -9.2387953251128674e-01F;
    float v458 = 5.4119610014619690e-01F;
    float v465 = -1.3065629648763766e+00F;
    float v472 = 3.8268343236508984e-01F;
    float v477 = 1.3065629648763766e+00F;
    float v482 = -5.4119610014619690e-01F;
    const float32x2_t *v727 = &v5[v0];
    int32_t *v839 = &v6[v2];
    int64_t v19 = v0 * 8;
    int64_t v34 = v10 * 7;
    int64_t v40 = v0 * 4;
    int64_t v54 = v0 * 12;
    int64_t v69 = v10 * 3;
    int64_t v76 = v10 * 11;
    int64_t v82 = v0 * 2;
    int64_t v96 = v0 * 10;
    int64_t v118 = v10 * 9;
    int64_t v124 = v0 * 6;
    int64_t v138 = v0 * 14;
    int64_t v153 = v10 * 5;
    int64_t v160 = v10 * 13;
    int64_t v180 = v0 * 9;
    int64_t v202 = v10 * 8;
    int64_t v208 = v0 * 5;
    int64_t v222 = v0 * 13;
    int64_t v237 = v10 * 4;
    int64_t v244 = v10 * 12;
    int64_t v250 = v0 * 3;
    int64_t v264 = v0 * 11;
    int64_t v279 = v10 * 2;
    int64_t v286 = v10 * 10;
    int64_t v292 = v0 * 7;
    int64_t v306 = v0 * 15;
    int64_t v321 = v10 * 6;
    int64_t v328 = v10 * 14;
    int64_t v329 = v13 * 15;
    float v435 = v4 * v432;
    float v442 = v4 * v439;
    float v454 = v4 * v451;
    float v461 = v4 * v458;
    float v468 = v4 * v465;
    int64_t v547 = v2 * 2;
    int64_t v555 = v2 * 3;
    int64_t v563 = v2 * 4;
    int64_t v571 = v2 * 5;
    int64_t v579 = v2 * 6;
    int64_t v587 = v2 * 7;
    int64_t v595 = v2 * 8;
    int64_t v603 = v2 * 9;
    int64_t v611 = v2 * 10;
    int64_t v619 = v2 * 11;
    int64_t v627 = v2 * 12;
    int64_t v635 = v2 * 13;
    int64_t v643 = v2 * 14;
    int64_t v651 = v2 * 15;
    const float32x2_t *v802 = &v5[0];
    svint64_t v803 = svindex_s64(0, v1);
    svfloat32_t v816 = svdup_n_f32(v446);
    svfloat32_t v820 = svdup_n_f32(v472);
    svfloat32_t v821 = svdup_n_f32(v477);
    svfloat32_t v822 = svdup_n_f32(v482);
    int32_t *v830 = &v6[0];
    int64_t v36 = v34 + v329;
    int64_t v71 = v69 + v329;
    int64_t v78 = v76 + v329;
    int64_t v113 = v10 + v329;
    int64_t v120 = v118 + v329;
    int64_t v155 = v153 + v329;
    int64_t v162 = v160 + v329;
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v329]));
    int64_t v204 = v202 + v329;
    int64_t v239 = v237 + v329;
    int64_t v246 = v244 + v329;
    int64_t v281 = v279 + v329;
    int64_t v288 = v286 + v329;
    int64_t v323 = v321 + v329;
    int64_t v330 = v328 + v329;
    const float32x2_t *v664 = &v5[v19];
    const float32x2_t *v673 = &v5[v40];
    const float32x2_t *v682 = &v5[v54];
    const float32x2_t *v691 = &v5[v82];
    const float32x2_t *v700 = &v5[v96];
    const float32x2_t *v709 = &v5[v124];
    const float32x2_t *v718 = &v5[v138];
    svfloat32_t v729 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v727), v803));
    const float32x2_t *v737 = &v5[v180];
    const float32x2_t *v747 = &v5[v208];
    const float32x2_t *v756 = &v5[v222];
    const float32x2_t *v765 = &v5[v250];
    const float32x2_t *v774 = &v5[v264];
    const float32x2_t *v783 = &v5[v292];
    const float32x2_t *v792 = &v5[v306];
    svfloat32_t v804 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v802), v803));
    svfloat32_t v814 = svdup_n_f32(v435);
    svfloat32_t v815 = svdup_n_f32(v442);
    svfloat32_t v817 = svdup_n_f32(v454);
    svfloat32_t v818 = svdup_n_f32(v461);
    svfloat32_t v819 = svdup_n_f32(v468);
    int32_t *v848 = &v6[v547];
    int32_t *v857 = &v6[v555];
    int32_t *v866 = &v6[v563];
    int32_t *v875 = &v6[v571];
    int32_t *v884 = &v6[v579];
    int32_t *v893 = &v6[v587];
    int32_t *v902 = &v6[v595];
    int32_t *v911 = &v6[v603];
    int32_t *v920 = &v6[v611];
    int32_t *v929 = &v6[v619];
    int32_t *v938 = &v6[v627];
    int32_t *v947 = &v6[v635];
    int32_t *v956 = &v6[v643];
    int32_t *v965 = &v6[v651];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v729, v198, 0),
                     v729, v198, 90);
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v246]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v288]));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v331 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v330]));
    svfloat32_t v666 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v664), v803));
    svfloat32_t v675 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v673), v803));
    svfloat32_t v684 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v682), v803));
    svfloat32_t v693 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v691), v803));
    svfloat32_t v702 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v700), v803));
    svfloat32_t v711 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v709), v803));
    svfloat32_t v720 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v718), v803));
    svfloat32_t v739 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v737), v803));
    svfloat32_t v749 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v747), v803));
    svfloat32_t v758 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v756), v803));
    svfloat32_t v767 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v765), v803));
    svfloat32_t v776 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v774), v803));
    svfloat32_t v785 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v783), v803));
    svfloat32_t v794 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v792), v803));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v666, v37, 0),
                     v666, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v675, v72, 0),
                     v675, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v684, v79, 0),
                     v684, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v693, v114, 0),
                     v693, v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v702, v121, 0),
                     v702, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v711, v156, 0),
                     v711, v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v720, v163, 0),
                     v720, v163, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v739, v205, 0),
                     v739, v205, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero241, v749, v240, 0),
                     v749, v240, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero248, v758, v247, 0),
                     v758, v247, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero283, v767, v282, 0),
                     v767, v282, 90);
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero290, v776, v289, 0),
                     v776, v289, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero325, v785, v324, 0),
                     v785, v324, 90);
    svfloat32_t zero332 = svdup_n_f32(0);
    svfloat32_t v332 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero332, v794, v331, 0),
                     v794, v331, 90);
    svfloat32_t v340 = svadd_f32_x(svptrue_b32(), v804, v38);
    svfloat32_t v341 = svsub_f32_x(svptrue_b32(), v804, v38);
    svfloat32_t v342 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v343 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v344 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v345 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v346 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v347 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v348 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v349 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v350 = svadd_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v351 = svsub_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v352 = svadd_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v353 = svsub_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v354 = svadd_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v355 = svsub_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v356 = svadd_f32_x(svptrue_b32(), v340, v342);
    svfloat32_t v357 = svsub_f32_x(svptrue_b32(), v340, v342);
    svfloat32_t v358 = svadd_f32_x(svptrue_b32(), v344, v346);
    svfloat32_t v359 = svsub_f32_x(svptrue_b32(), v344, v346);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v348, v350);
    svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v348, v350);
    svfloat32_t v362 = svadd_f32_x(svptrue_b32(), v352, v354);
    svfloat32_t v363 = svsub_f32_x(svptrue_b32(), v352, v354);
    svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v345, v347);
    svfloat32_t v373 = svsub_f32_x(svptrue_b32(), v345, v347);
    svfloat32_t v374 = svadd_f32_x(svptrue_b32(), v349, v355);
    svfloat32_t v375 = svsub_f32_x(svptrue_b32(), v349, v355);
    svfloat32_t v376 = svadd_f32_x(svptrue_b32(), v351, v353);
    svfloat32_t v377 = svsub_f32_x(svptrue_b32(), v351, v353);
    svfloat32_t zero437 = svdup_n_f32(0);
    svfloat32_t v437 = svcmla_f32_x(pred_full, zero437, v814, v343, 90);
    svfloat32_t v364 = svadd_f32_x(svptrue_b32(), v356, v358);
    svfloat32_t v365 = svsub_f32_x(svptrue_b32(), v356, v358);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v360, v362);
    svfloat32_t v367 = svsub_f32_x(svptrue_b32(), v360, v362);
    svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v361, v363);
    svfloat32_t v371 = svsub_f32_x(svptrue_b32(), v361, v363);
    svfloat32_t v378 = svadd_f32_x(svptrue_b32(), v374, v376);
    svfloat32_t v379 = svadd_f32_x(svptrue_b32(), v375, v377);
    svfloat32_t zero413 = svdup_n_f32(0);
    svfloat32_t v413 = svcmla_f32_x(pred_full, zero413, v814, v359, 90);
    svfloat32_t zero444 = svdup_n_f32(0);
    svfloat32_t v444 = svcmla_f32_x(pred_full, zero444, v815, v372, 90);
    svfloat32_t zero470 = svdup_n_f32(0);
    svfloat32_t v470 = svcmla_f32_x(pred_full, zero470, v819, v376, 90);
    svfloat32_t v480 = svmul_f32_x(svptrue_b32(), v375, v821);
    svfloat32_t v485 = svmul_f32_x(svptrue_b32(), v377, v822);
    svfloat32_t v368 = svadd_f32_x(svptrue_b32(), v364, v366);
    svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v364, v366);
    svfloat32_t zero401 = svdup_n_f32(0);
    svfloat32_t v401 = svcmla_f32_x(pred_full, zero401, v814, v367, 90);
    svfloat32_t zero420 = svdup_n_f32(0);
    svfloat32_t v420 = svcmla_f32_x(pred_full, zero420, v815, v370, 90);
    svfloat32_t zero456 = svdup_n_f32(0);
    svfloat32_t v456 = svcmla_f32_x(pred_full, zero456, v817, v378, 90);
    svfloat32_t v475 = svmul_f32_x(svptrue_b32(), v379, v820);
    svfloat32_t v496 = svmla_f32_x(pred_full, v341, v373, v816);
    svfloat32_t v497 = svmls_f32_x(pred_full, v341, v373, v816);
    svfloat32_t v498 = svadd_f32_x(svptrue_b32(), v437, v444);
    svfloat32_t v499 = svsub_f32_x(svptrue_b32(), v437, v444);
    svfloat32_t v486 = svadd_f32_x(svptrue_b32(), v365, v401);
    svfloat32_t v487 = svsub_f32_x(svptrue_b32(), v365, v401);
    svfloat32_t v488 = svmla_f32_x(pred_full, v357, v371, v816);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v413, v420);
    svfloat32_t v490 = svmls_f32_x(pred_full, v357, v371, v816);
    svfloat32_t v491 = svsub_f32_x(svptrue_b32(), v420, v413);
    svfloat32_t v500 = svcmla_f32_x(pred_full, v456, v818, v374, 90);
    svfloat32_t v501 = svsub_f32_x(svptrue_b32(), v456, v470);
    svfloat32_t v502 = svnmls_f32_x(pred_full, v475, v375, v821);
    svfloat32_t v503 = svnmls_f32_x(pred_full, v475, v377, v822);
    svfloat32_t v504 = svnmls_f32_x(pred_full, v480, v379, v820);
    svfloat32_t v505 = svnmls_f32_x(pred_full, v485, v379, v820);
    svfloat32_t v510 = svadd_f32_x(svptrue_b32(), v497, v499);
    svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v497, v499);
    svint16_t v532 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v368, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v596 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v369, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v492 = svadd_f32_x(svptrue_b32(), v488, v489);
    svfloat32_t v493 = svadd_f32_x(svptrue_b32(), v490, v491);
    svfloat32_t v494 = svsub_f32_x(svptrue_b32(), v490, v491);
    svfloat32_t v495 = svsub_f32_x(svptrue_b32(), v488, v489);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v496, v502);
    svfloat32_t v507 = svsub_f32_x(svptrue_b32(), v496, v502);
    svfloat32_t v508 = svadd_f32_x(svptrue_b32(), v496, v504);
    svfloat32_t v509 = svsub_f32_x(svptrue_b32(), v496, v504);
    svfloat32_t v512 = svadd_f32_x(svptrue_b32(), v497, v505);
    svfloat32_t v513 = svsub_f32_x(svptrue_b32(), v497, v505);
    svfloat32_t v516 = svadd_f32_x(svptrue_b32(), v500, v498);
    svfloat32_t v517 = svsub_f32_x(svptrue_b32(), v500, v498);
    svfloat32_t v518 = svadd_f32_x(svptrue_b32(), v501, v503);
    svfloat32_t v519 = svsub_f32_x(svptrue_b32(), v501, v503);
    svfloat32_t v520 = svadd_f32_x(svptrue_b32(), v501, v499);
    svfloat32_t v521 = svsub_f32_x(svptrue_b32(), v501, v499);
    svint16_t v564 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v487, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v628 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v486, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v830), svreinterpret_u64_s16(v532));
    svst1w_u64(pred_full, (unsigned *)(v902), svreinterpret_u64_s16(v596));
    svfloat32_t v522 = svadd_f32_x(svptrue_b32(), v506, v516);
    svfloat32_t v523 = svadd_f32_x(svptrue_b32(), v507, v517);
    svfloat32_t v524 = svsub_f32_x(svptrue_b32(), v508, v517);
    svfloat32_t v525 = svsub_f32_x(svptrue_b32(), v509, v516);
    svfloat32_t v526 = svadd_f32_x(svptrue_b32(), v510, v518);
    svfloat32_t v527 = svadd_f32_x(svptrue_b32(), v511, v519);
    svfloat32_t v528 = svsub_f32_x(svptrue_b32(), v512, v521);
    svfloat32_t v529 = svsub_f32_x(svptrue_b32(), v513, v520);
    svint16_t v548 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v495, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v580 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v494, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v612 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v493, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v644 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v492, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v866), svreinterpret_u64_s16(v564));
    svst1w_u64(pred_full, (unsigned *)(v938), svreinterpret_u64_s16(v628));
    svint16_t v540 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v525, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v556 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v528, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v572 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v529, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v588 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v524, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v604 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v523, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v620 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v526, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v636 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v527, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v652 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v522, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v848), svreinterpret_u64_s16(v548));
    svst1w_u64(pred_full, (unsigned *)(v884), svreinterpret_u64_s16(v580));
    svst1w_u64(pred_full, (unsigned *)(v920), svreinterpret_u64_s16(v612));
    svst1w_u64(pred_full, (unsigned *)(v956), svreinterpret_u64_s16(v644));
    svst1w_u64(pred_full, (unsigned *)(v839), svreinterpret_u64_s16(v540));
    svst1w_u64(pred_full, (unsigned *)(v857), svreinterpret_u64_s16(v556));
    svst1w_u64(pred_full, (unsigned *)(v875), svreinterpret_u64_s16(v572));
    svst1w_u64(pred_full, (unsigned *)(v893), svreinterpret_u64_s16(v588));
    svst1w_u64(pred_full, (unsigned *)(v911), svreinterpret_u64_s16(v604));
    svst1w_u64(pred_full, (unsigned *)(v929), svreinterpret_u64_s16(v620));
    svst1w_u64(pred_full, (unsigned *)(v947), svreinterpret_u64_s16(v636));
    svst1w_u64(pred_full, (unsigned *)(v965), svreinterpret_u64_s16(v652));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu17(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v589 = -4.2602849117736000e-02F;
    float v593 = 2.0497965023262180e-01F;
    float v597 = 1.0451835201736759e+00F;
    float v601 = 1.7645848660222969e+00F;
    float v605 = -7.2340797728605655e-01F;
    float v609 = -8.9055591620606403e-02F;
    float v613 = -1.0625000000000000e+00F;
    float v617 = 2.5769410160110379e-01F;
    float v621 = 7.7980260789483757e-01F;
    float v625 = 5.4389318464570580e-01F;
    float v629 = 4.2010193497052700e-01F;
    float v633 = 1.2810929434228073e+00F;
    float v637 = 4.4088907348175338e-01F;
    float v641 = 3.1717619283272508e-01F;
    float v644 = -9.0138318648016680e-01F;
    float v645 = 9.0138318648016680e-01F;
    float v651 = -4.3248756360072310e-01F;
    float v652 = 4.3248756360072310e-01F;
    float v658 = 6.6693537504044498e-01F;
    float v659 = -6.6693537504044498e-01F;
    float v665 = -6.0389004312516970e-01F;
    float v666 = 6.0389004312516970e-01F;
    float v672 = -3.6924873198582547e-01F;
    float v673 = 3.6924873198582547e-01F;
    float v679 = 4.8656938755549761e-01F;
    float v680 = -4.8656938755549761e-01F;
    float v686 = 2.3813712136760609e-01F;
    float v687 = -2.3813712136760609e-01F;
    float v693 = -1.5573820617422458e+00F;
    float v694 = 1.5573820617422458e+00F;
    float v700 = 6.5962247018731990e-01F;
    float v701 = -6.5962247018731990e-01F;
    float v707 = -1.4316961569866241e-01F;
    float v708 = 1.4316961569866241e-01F;
    float v714 = 2.3903469959860771e-01F;
    float v715 = -2.3903469959860771e-01F;
    float v721 = -4.7932541949972603e-02F;
    float v722 = 4.7932541949972603e-02F;
    float v728 = -2.3188014856550065e+00F;
    float v729 = 2.3188014856550065e+00F;
    float v735 = 7.8914568419206255e-01F;
    float v736 = -7.8914568419206255e-01F;
    float v742 = 3.8484572871179505e+00F;
    float v743 = -3.8484572871179505e+00F;
    float v749 = -1.3003804568801376e+00F;
    float v750 = 1.3003804568801376e+00F;
    float v756 = 4.0814769046889037e+00F;
    float v757 = -4.0814769046889037e+00F;
    float v763 = -1.4807159909286283e+00F;
    float v764 = 1.4807159909286283e+00F;
    float v770 = -1.3332470363551400e-02F;
    float v771 = 1.3332470363551400e-02F;
    float v777 = -3.7139778690557629e-01F;
    float v778 = 3.7139778690557629e-01F;
    float v784 = 1.9236512863456379e-01F;
    float v785 = -1.9236512863456379e-01F;
    float32x2_t v787 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v582 = v5[0];
    float32x2_t v590 = (float32x2_t){v589, v589};
    float32x2_t v594 = (float32x2_t){v593, v593};
    float32x2_t v598 = (float32x2_t){v597, v597};
    float32x2_t v602 = (float32x2_t){v601, v601};
    float32x2_t v606 = (float32x2_t){v605, v605};
    float32x2_t v610 = (float32x2_t){v609, v609};
    float32x2_t v614 = (float32x2_t){v613, v613};
    float32x2_t v618 = (float32x2_t){v617, v617};
    float32x2_t v622 = (float32x2_t){v621, v621};
    float32x2_t v626 = (float32x2_t){v625, v625};
    float32x2_t v630 = (float32x2_t){v629, v629};
    float32x2_t v634 = (float32x2_t){v633, v633};
    float32x2_t v638 = (float32x2_t){v637, v637};
    float32x2_t v642 = (float32x2_t){v641, v641};
    float32x2_t v646 = (float32x2_t){v644, v645};
    float32x2_t v653 = (float32x2_t){v651, v652};
    float32x2_t v660 = (float32x2_t){v658, v659};
    float32x2_t v667 = (float32x2_t){v665, v666};
    float32x2_t v674 = (float32x2_t){v672, v673};
    float32x2_t v681 = (float32x2_t){v679, v680};
    float32x2_t v688 = (float32x2_t){v686, v687};
    float32x2_t v695 = (float32x2_t){v693, v694};
    float32x2_t v702 = (float32x2_t){v700, v701};
    float32x2_t v709 = (float32x2_t){v707, v708};
    float32x2_t v716 = (float32x2_t){v714, v715};
    float32x2_t v723 = (float32x2_t){v721, v722};
    float32x2_t v730 = (float32x2_t){v728, v729};
    float32x2_t v737 = (float32x2_t){v735, v736};
    float32x2_t v744 = (float32x2_t){v742, v743};
    float32x2_t v751 = (float32x2_t){v749, v750};
    float32x2_t v758 = (float32x2_t){v756, v757};
    float32x2_t v765 = (float32x2_t){v763, v764};
    float32x2_t v772 = (float32x2_t){v770, v771};
    float32x2_t v779 = (float32x2_t){v777, v778};
    float32x2_t v786 = (float32x2_t){v784, v785};
    float32x2_t v38 = v5[istride * 16];
    float32x2_t v56 = v7[j * 32];
    int64_t v60 = j * 32 + 1;
    int64_t v68 = 30 + j * 32;
    float32x2_t v82 = v5[istride * 3];
    float32x2_t v100 = v5[istride * 14];
    int64_t v117 = 4 + j * 32;
    int64_t v130 = 26 + j * 32;
    float32x2_t v144 = v5[istride * 9];
    float32x2_t v162 = v5[istride * 8];
    int64_t v179 = 16 + j * 32;
    int64_t v192 = 14 + j * 32;
    float32x2_t v206 = v5[istride * 10];
    float32x2_t v224 = v5[istride * 7];
    int64_t v241 = 18 + j * 32;
    int64_t v254 = 12 + j * 32;
    float32x2_t v268 = v5[istride * 13];
    float32x2_t v286 = v5[istride * 4];
    int64_t v303 = 24 + j * 32;
    int64_t v316 = 6 + j * 32;
    float32x2_t v330 = v5[istride * 5];
    float32x2_t v348 = v5[istride * 12];
    int64_t v365 = 8 + j * 32;
    int64_t v378 = 22 + j * 32;
    float32x2_t v392 = v5[istride * 15];
    float32x2_t v410 = v5[istride * 2];
    int64_t v427 = 28 + j * 32;
    int64_t v440 = 2 + j * 32;
    float32x2_t v454 = v5[istride * 11];
    float32x2_t v472 = v5[istride * 6];
    int64_t v489 = 20 + j * 32;
    int64_t v502 = 10 + j * 32;
    float32x2_t v648 = vmul_f32(v787, v646);
    float32x2_t v655 = vmul_f32(v787, v653);
    float32x2_t v662 = vmul_f32(v787, v660);
    float32x2_t v669 = vmul_f32(v787, v667);
    float32x2_t v676 = vmul_f32(v787, v674);
    float32x2_t v683 = vmul_f32(v787, v681);
    float32x2_t v690 = vmul_f32(v787, v688);
    float32x2_t v697 = vmul_f32(v787, v695);
    float32x2_t v704 = vmul_f32(v787, v702);
    float32x2_t v711 = vmul_f32(v787, v709);
    float32x2_t v718 = vmul_f32(v787, v716);
    float32x2_t v725 = vmul_f32(v787, v723);
    float32x2_t v732 = vmul_f32(v787, v730);
    float32x2_t v739 = vmul_f32(v787, v737);
    float32x2_t v746 = vmul_f32(v787, v744);
    float32x2_t v753 = vmul_f32(v787, v751);
    float32x2_t v760 = vmul_f32(v787, v758);
    float32x2_t v767 = vmul_f32(v787, v765);
    float32x2_t v774 = vmul_f32(v787, v772);
    float32x2_t v781 = vmul_f32(v787, v779);
    float32x2_t v788 = vmul_f32(v787, v786);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v180 = v7[v179];
    float32x2_t v181 = vtrn1_f32(v144, v144);
    float32x2_t v182 = vtrn2_f32(v144, v144);
    int64_t v184 = v179 + 1;
    float32x2_t v193 = v7[v192];
    float32x2_t v194 = vtrn1_f32(v162, v162);
    float32x2_t v195 = vtrn2_f32(v162, v162);
    int64_t v197 = v192 + 1;
    float32x2_t v242 = v7[v241];
    float32x2_t v243 = vtrn1_f32(v206, v206);
    float32x2_t v244 = vtrn2_f32(v206, v206);
    int64_t v246 = v241 + 1;
    float32x2_t v255 = v7[v254];
    float32x2_t v256 = vtrn1_f32(v224, v224);
    float32x2_t v257 = vtrn2_f32(v224, v224);
    int64_t v259 = v254 + 1;
    float32x2_t v304 = v7[v303];
    float32x2_t v305 = vtrn1_f32(v268, v268);
    float32x2_t v306 = vtrn2_f32(v268, v268);
    int64_t v308 = v303 + 1;
    float32x2_t v317 = v7[v316];
    float32x2_t v318 = vtrn1_f32(v286, v286);
    float32x2_t v319 = vtrn2_f32(v286, v286);
    int64_t v321 = v316 + 1;
    float32x2_t v366 = v7[v365];
    float32x2_t v367 = vtrn1_f32(v330, v330);
    float32x2_t v368 = vtrn2_f32(v330, v330);
    int64_t v370 = v365 + 1;
    float32x2_t v379 = v7[v378];
    float32x2_t v380 = vtrn1_f32(v348, v348);
    float32x2_t v381 = vtrn2_f32(v348, v348);
    int64_t v383 = v378 + 1;
    float32x2_t v428 = v7[v427];
    float32x2_t v429 = vtrn1_f32(v392, v392);
    float32x2_t v430 = vtrn2_f32(v392, v392);
    int64_t v432 = v427 + 1;
    float32x2_t v441 = v7[v440];
    float32x2_t v442 = vtrn1_f32(v410, v410);
    float32x2_t v443 = vtrn2_f32(v410, v410);
    int64_t v445 = v440 + 1;
    float32x2_t v490 = v7[v489];
    float32x2_t v491 = vtrn1_f32(v454, v454);
    float32x2_t v492 = vtrn2_f32(v454, v454);
    int64_t v494 = v489 + 1;
    float32x2_t v503 = v7[v502];
    float32x2_t v504 = vtrn1_f32(v472, v472);
    float32x2_t v505 = vtrn2_f32(v472, v472);
    int64_t v507 = v502 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v185 = v7[v184];
    float32x2_t v186 = vmul_f32(v181, v180);
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vmul_f32(v194, v193);
    float32x2_t v247 = v7[v246];
    float32x2_t v248 = vmul_f32(v243, v242);
    float32x2_t v260 = v7[v259];
    float32x2_t v261 = vmul_f32(v256, v255);
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vmul_f32(v305, v304);
    float32x2_t v322 = v7[v321];
    float32x2_t v323 = vmul_f32(v318, v317);
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vmul_f32(v367, v366);
    float32x2_t v384 = v7[v383];
    float32x2_t v385 = vmul_f32(v380, v379);
    float32x2_t v433 = v7[v432];
    float32x2_t v434 = vmul_f32(v429, v428);
    float32x2_t v446 = v7[v445];
    float32x2_t v447 = vmul_f32(v442, v441);
    float32x2_t v495 = v7[v494];
    float32x2_t v496 = vmul_f32(v491, v490);
    float32x2_t v508 = v7[v507];
    float32x2_t v509 = vmul_f32(v504, v503);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v188 = vfma_f32(v186, v182, v185);
    float32x2_t v201 = vfma_f32(v199, v195, v198);
    float32x2_t v250 = vfma_f32(v248, v244, v247);
    float32x2_t v263 = vfma_f32(v261, v257, v260);
    float32x2_t v312 = vfma_f32(v310, v306, v309);
    float32x2_t v325 = vfma_f32(v323, v319, v322);
    float32x2_t v374 = vfma_f32(v372, v368, v371);
    float32x2_t v387 = vfma_f32(v385, v381, v384);
    float32x2_t v436 = vfma_f32(v434, v430, v433);
    float32x2_t v449 = vfma_f32(v447, v443, v446);
    float32x2_t v498 = vfma_f32(v496, v492, v495);
    float32x2_t v511 = vfma_f32(v509, v505, v508);
    float32x2_t v512 = vadd_f32(v64, v77);
    float32x2_t v513 = vsub_f32(v64, v77);
    float32x2_t v514 = vadd_f32(v126, v139);
    float32x2_t v515 = vsub_f32(v126, v139);
    float32x2_t v516 = vadd_f32(v188, v201);
    float32x2_t v517 = vsub_f32(v188, v201);
    float32x2_t v518 = vadd_f32(v250, v263);
    float32x2_t v519 = vsub_f32(v250, v263);
    float32x2_t v520 = vadd_f32(v312, v325);
    float32x2_t v521 = vsub_f32(v312, v325);
    float32x2_t v522 = vadd_f32(v374, v387);
    float32x2_t v523 = vsub_f32(v374, v387);
    float32x2_t v524 = vadd_f32(v436, v449);
    float32x2_t v525 = vsub_f32(v436, v449);
    float32x2_t v526 = vadd_f32(v498, v511);
    float32x2_t v527 = vsub_f32(v498, v511);
    float32x2_t v528 = vadd_f32(v512, v520);
    float32x2_t v529 = vadd_f32(v514, v522);
    float32x2_t v530 = vadd_f32(v516, v524);
    float32x2_t v531 = vadd_f32(v518, v526);
    float32x2_t v534 = vsub_f32(v512, v520);
    float32x2_t v535 = vsub_f32(v514, v522);
    float32x2_t v536 = vsub_f32(v516, v524);
    float32x2_t v537 = vsub_f32(v518, v526);
    float32x2_t v548 = vadd_f32(v513, v517);
    float32x2_t v549 = vadd_f32(v515, v519);
    float32x2_t v550 = vsub_f32(v513, v517);
    float32x2_t v551 = vsub_f32(v527, v523);
    float32x2_t v552 = vadd_f32(v521, v525);
    float32x2_t v553 = vadd_f32(v523, v527);
    float32x2_t v554 = vsub_f32(v521, v525);
    float32x2_t v555 = vsub_f32(v515, v519);
    float32x2_t v568 = vadd_f32(v513, v521);
    float32x2_t v569 = vadd_f32(v519, v527);
    float32x2_t v740 = vrev64_f32(v513);
    float32x2_t v747 = vrev64_f32(v521);
    float32x2_t v761 = vrev64_f32(v519);
    float32x2_t v768 = vrev64_f32(v527);
    float32x2_t v532 = vadd_f32(v528, v530);
    float32x2_t v533 = vadd_f32(v529, v531);
    float32x2_t v538 = vsub_f32(v528, v530);
    float32x2_t v539 = vsub_f32(v529, v531);
    float32x2_t v542 = vadd_f32(v535, v537);
    float32x2_t v543 = vadd_f32(v534, v536);
    float32x2_t v545 = vsub_f32(v536, v537);
    float32x2_t v546 = vsub_f32(v534, v535);
    float32x2_t v556 = vadd_f32(v548, v549);
    float32x2_t v557 = vadd_f32(v552, v553);
    float32x2_t v559 = vsub_f32(v548, v549);
    float32x2_t v560 = vsub_f32(v552, v553);
    float32x2_t v562 = vadd_f32(v550, v551);
    float32x2_t v563 = vadd_f32(v554, v555);
    float32x2_t v565 = vsub_f32(v550, v551);
    float32x2_t v566 = vsub_f32(v554, v555);
    float32x2_t v591 = vmul_f32(v534, v590);
    float32x2_t v595 = vmul_f32(v535, v594);
    float32x2_t v599 = vmul_f32(v536, v598);
    float32x2_t v603 = vmul_f32(v537, v602);
    float32x2_t v733 = vrev64_f32(v568);
    float32x2_t v741 = vmul_f32(v740, v739);
    float32x2_t v748 = vmul_f32(v747, v746);
    float32x2_t v754 = vrev64_f32(v569);
    float32x2_t v762 = vmul_f32(v761, v760);
    float32x2_t v769 = vmul_f32(v768, v767);
    float32x2_t v540 = vadd_f32(v532, v533);
    float32x2_t v541 = vsub_f32(v532, v533);
    float32x2_t v544 = vsub_f32(v543, v542);
    float32x2_t v547 = vadd_f32(v538, v539);
    float32x2_t v558 = vadd_f32(v556, v557);
    float32x2_t v561 = vadd_f32(v559, v560);
    float32x2_t v564 = vadd_f32(v562, v563);
    float32x2_t v567 = vadd_f32(v565, v566);
    float32x2_t v570 = vsub_f32(v563, v557);
    float32x2_t v573 = vsub_f32(v556, v562);
    float32x2_t v607 = vmul_f32(v538, v606);
    float32x2_t v611 = vmul_f32(v539, v610);
    float32x2_t v623 = vmul_f32(v542, v622);
    float32x2_t v627 = vmul_f32(v543, v626);
    float32x2_t v635 = vmul_f32(v545, v634);
    float32x2_t v639 = vmul_f32(v546, v638);
    float32x2_t v649 = vrev64_f32(v556);
    float32x2_t v656 = vrev64_f32(v557);
    float32x2_t v670 = vrev64_f32(v559);
    float32x2_t v677 = vrev64_f32(v560);
    float32x2_t v691 = vrev64_f32(v562);
    float32x2_t v698 = vrev64_f32(v563);
    float32x2_t v712 = vrev64_f32(v565);
    float32x2_t v719 = vrev64_f32(v566);
    float32x2_t v734 = vmul_f32(v733, v732);
    float32x2_t v755 = vmul_f32(v754, v753);
    float32x2_t v571 = vadd_f32(v570, v513);
    float32x2_t v574 = vadd_f32(v573, v519);
    float32x2_t v583 = vadd_f32(v582, v540);
    float32x2_t v615 = vmul_f32(v540, v614);
    float32x2_t v619 = vmul_f32(v541, v618);
    float32x2_t v631 = vmul_f32(v544, v630);
    float32x2_t v643 = vmul_f32(v547, v642);
    float32x2_t v650 = vmul_f32(v649, v648);
    float32x2_t v657 = vmul_f32(v656, v655);
    float32x2_t v663 = vrev64_f32(v558);
    float32x2_t v671 = vmul_f32(v670, v669);
    float32x2_t v678 = vmul_f32(v677, v676);
    float32x2_t v684 = vrev64_f32(v561);
    float32x2_t v692 = vmul_f32(v691, v690);
    float32x2_t v699 = vmul_f32(v698, v697);
    float32x2_t v705 = vrev64_f32(v564);
    float32x2_t v713 = vmul_f32(v712, v711);
    float32x2_t v720 = vmul_f32(v719, v718);
    float32x2_t v726 = vrev64_f32(v567);
    float32x2_t v793 = vadd_f32(v603, v635);
    float32x2_t v794 = vsub_f32(v635, v599);
    float32x2_t v795 = vadd_f32(v595, v639);
    float32x2_t v796 = vsub_f32(v591, v639);
    float32x2_t v572 = vsub_f32(v571, v569);
    float32x2_t v575 = vadd_f32(v574, v521);
    float32x2_t v664 = vmul_f32(v663, v662);
    float32x2_t v685 = vmul_f32(v684, v683);
    float32x2_t v706 = vmul_f32(v705, v704);
    float32x2_t v727 = vmul_f32(v726, v725);
    float32x2_t v791 = vadd_f32(v623, v631);
    float32x2_t v792 = vsub_f32(v627, v631);
    float32x2_t v797 = vsub_f32(v643, v611);
    float32x2_t v798 = vadd_f32(v643, v607);
    float32x2_t v799 = vadd_f32(v615, v583);
    int16x4_t v867 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v583, 15), (int32x2_t){0, 0}));
    float32x2_t v576 = vsub_f32(v575, v527);
    float32x2_t v775 = vrev64_f32(v572);
    float32x2_t v800 = vadd_f32(v619, v799);
    float32x2_t v801 = vsub_f32(v799, v619);
    float32x2_t v802 = vsub_f32(v791, v793);
    float32x2_t v804 = vadd_f32(v792, v794);
    float32x2_t v806 = vadd_f32(v791, v795);
    float32x2_t v808 = vadd_f32(v792, v796);
    float32x2_t v818 = vadd_f32(v650, v664);
    float32x2_t v819 = vadd_f32(v657, v664);
    float32x2_t v820 = vadd_f32(v671, v685);
    float32x2_t v821 = vadd_f32(v678, v685);
    float32x2_t v822 = vadd_f32(v692, v706);
    float32x2_t v823 = vadd_f32(v699, v706);
    float32x2_t v824 = vadd_f32(v713, v727);
    float32x2_t v825 = vadd_f32(v720, v727);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v867), 0);
    float32x2_t v577 = vadd_f32(v572, v576);
    float32x2_t v776 = vmul_f32(v775, v774);
    float32x2_t v782 = vrev64_f32(v576);
    float32x2_t v803 = vadd_f32(v797, v800);
    float32x2_t v805 = vadd_f32(v798, v801);
    float32x2_t v807 = vsub_f32(v800, v797);
    float32x2_t v809 = vsub_f32(v801, v798);
    float32x2_t v829 = vadd_f32(v818, v820);
    float32x2_t v830 = vsub_f32(v818, v820);
    float32x2_t v831 = vadd_f32(v819, v821);
    float32x2_t v832 = vsub_f32(v819, v821);
    float32x2_t v833 = vadd_f32(v822, v824);
    float32x2_t v834 = vsub_f32(v824, v822);
    float32x2_t v835 = vadd_f32(v823, v825);
    float32x2_t v836 = vsub_f32(v825, v823);
    float32x2_t v783 = vmul_f32(v782, v781);
    float32x2_t v789 = vrev64_f32(v577);
    float32x2_t v810 = vadd_f32(v802, v803);
    float32x2_t v811 = vadd_f32(v804, v805);
    float32x2_t v812 = vadd_f32(v806, v807);
    float32x2_t v813 = vadd_f32(v808, v809);
    float32x2_t v814 = vsub_f32(v803, v802);
    float32x2_t v815 = vsub_f32(v805, v804);
    float32x2_t v816 = vsub_f32(v807, v806);
    float32x2_t v817 = vsub_f32(v809, v808);
    float32x2_t v846 = vadd_f32(v831, v835);
    float32x2_t v848 = vadd_f32(v830, v836);
    float32x2_t v850 = vsub_f32(v829, v833);
    float32x2_t v852 = vsub_f32(v836, v830);
    float32x2_t v854 = vadd_f32(v829, v833);
    float32x2_t v857 = vsub_f32(v834, v832);
    float32x2_t v860 = vsub_f32(v835, v831);
    float32x2_t v863 = vadd_f32(v832, v834);
    float32x2_t v790 = vmul_f32(v789, v788);
    float32x2_t v837 = vsub_f32(v776, v783);
    float32x2_t v826 = vadd_f32(v790, v783);
    float32x2_t v839 = vadd_f32(v837, v837);
    float32x2_t v864 = vsub_f32(v863, v837);
    float32x2_t v827 = vadd_f32(v734, v826);
    float32x2_t v840 = vsub_f32(v755, v839);
    float32x2_t v843 = vadd_f32(v826, v826);
    float32x2_t v861 = vadd_f32(v860, v839);
    float32x2_t v899 = vadd_f32(v817, v864);
    float32x2_t v906 = vsub_f32(v817, v864);
    float32x2_t v828 = vadd_f32(v827, v741);
    float32x2_t v838 = vadd_f32(v827, v748);
    float32x2_t v841 = vadd_f32(v840, v762);
    float32x2_t v842 = vadd_f32(v840, v769);
    float32x2_t v844 = vadd_f32(v843, v843);
    float32x2_t v845 = vadd_f32(v837, v843);
    float32x2_t v851 = vadd_f32(v850, v843);
    float32x2_t v862 = vadd_f32(v861, v843);
    int16x4_t v902 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v899, 15), (int32x2_t){0, 0}));
    int16x4_t v909 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v906, 15), (int32x2_t){0, 0}));
    float32x2_t v847 = vadd_f32(v846, v838);
    float32x2_t v849 = vadd_f32(v848, v841);
    float32x2_t v853 = vsub_f32(v852, v845);
    float32x2_t v855 = vadd_f32(v854, v828);
    float32x2_t v858 = vsub_f32(v857, v842);
    float32x2_t v885 = vadd_f32(v812, v851);
    float32x2_t v892 = vsub_f32(v812, v851);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v902), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v909), 0);
    float32x2_t v969 = vadd_f32(v816, v862);
    float32x2_t v976 = vsub_f32(v816, v862);
    float32x2_t v856 = vadd_f32(v855, v837);
    float32x2_t v859 = vadd_f32(v858, v844);
    float32x2_t v871 = vadd_f32(v810, v847);
    float32x2_t v878 = vsub_f32(v810, v847);
    int16x4_t v888 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v885, 15), (int32x2_t){0, 0}));
    int16x4_t v895 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v892, 15), (int32x2_t){0, 0}));
    float32x2_t v927 = vadd_f32(v813, v853);
    float32x2_t v934 = vsub_f32(v813, v853);
    float32x2_t v941 = vadd_f32(v811, v849);
    float32x2_t v948 = vsub_f32(v811, v849);
    int16x4_t v972 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v969, 15), (int32x2_t){0, 0}));
    int16x4_t v979 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v976, 15), (int32x2_t){0, 0}));
    int16x4_t v874 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v871, 15), (int32x2_t){0, 0}));
    int16x4_t v881 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v878, 15), (int32x2_t){0, 0}));
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v888), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v895), 0);
    float32x2_t v913 = vadd_f32(v814, v856);
    float32x2_t v920 = vsub_f32(v814, v856);
    int16x4_t v930 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v927, 15), (int32x2_t){0, 0}));
    int16x4_t v937 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v934, 15), (int32x2_t){0, 0}));
    int16x4_t v944 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v941, 15), (int32x2_t){0, 0}));
    int16x4_t v951 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v948, 15), (int32x2_t){0, 0}));
    float32x2_t v955 = vadd_f32(v815, v859);
    float32x2_t v962 = vsub_f32(v815, v859);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v972), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v979), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v874), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v881), 0);
    int16x4_t v916 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v913, 15), (int32x2_t){0, 0}));
    int16x4_t v923 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v920, 15), (int32x2_t){0, 0}));
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v930), 0);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v937), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v944), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v951), 0);
    int16x4_t v958 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v955, 15), (int32x2_t){0, 0}));
    int16x4_t v965 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v962, 15), (int32x2_t){0, 0}));
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v916), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v923), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v958), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v965), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu17(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v434 = -4.2602849117736000e-02F;
    float v439 = 2.0497965023262180e-01F;
    float v444 = 1.0451835201736759e+00F;
    float v449 = 1.7645848660222969e+00F;
    float v454 = -7.2340797728605655e-01F;
    float v459 = -8.9055591620606403e-02F;
    float v464 = -1.0625000000000000e+00F;
    float v469 = 2.5769410160110379e-01F;
    float v474 = 7.7980260789483757e-01F;
    float v479 = 5.4389318464570580e-01F;
    float v484 = 4.2010193497052700e-01F;
    float v489 = 1.2810929434228073e+00F;
    float v494 = 4.4088907348175338e-01F;
    float v499 = 3.1717619283272508e-01F;
    float v504 = 9.0138318648016680e-01F;
    float v511 = 4.3248756360072310e-01F;
    float v518 = -6.6693537504044498e-01F;
    float v525 = 6.0389004312516970e-01F;
    float v532 = 3.6924873198582547e-01F;
    float v539 = -4.8656938755549761e-01F;
    float v546 = -2.3813712136760609e-01F;
    float v553 = 1.5573820617422458e+00F;
    float v560 = -6.5962247018731990e-01F;
    float v567 = 1.4316961569866241e-01F;
    float v574 = -2.3903469959860771e-01F;
    float v581 = 4.7932541949972603e-02F;
    float v588 = 2.3188014856550065e+00F;
    float v595 = -7.8914568419206255e-01F;
    float v602 = -3.8484572871179505e+00F;
    float v609 = 1.3003804568801376e+00F;
    float v616 = -4.0814769046889037e+00F;
    float v623 = 1.4807159909286283e+00F;
    float v630 = 1.3332470363551400e-02F;
    float v637 = 3.7139778690557629e-01F;
    float v644 = -1.9236512863456379e-01F;
    const float32x2_t *v882 = &v5[v0];
    int32_t *v1084 = &v6[v2];
    int64_t v33 = v0 * 16;
    int64_t v55 = v10 * 15;
    int64_t v61 = v0 * 3;
    int64_t v75 = v0 * 14;
    int64_t v90 = v10 * 2;
    int64_t v97 = v10 * 13;
    int64_t v103 = v0 * 9;
    int64_t v117 = v0 * 8;
    int64_t v132 = v10 * 8;
    int64_t v139 = v10 * 7;
    int64_t v145 = v0 * 10;
    int64_t v159 = v0 * 7;
    int64_t v174 = v10 * 9;
    int64_t v181 = v10 * 6;
    int64_t v187 = v0 * 13;
    int64_t v201 = v0 * 4;
    int64_t v216 = v10 * 12;
    int64_t v223 = v10 * 3;
    int64_t v229 = v0 * 5;
    int64_t v243 = v0 * 12;
    int64_t v258 = v10 * 4;
    int64_t v265 = v10 * 11;
    int64_t v271 = v0 * 15;
    int64_t v285 = v0 * 2;
    int64_t v300 = v10 * 14;
    int64_t v313 = v0 * 11;
    int64_t v327 = v0 * 6;
    int64_t v342 = v10 * 10;
    int64_t v349 = v10 * 5;
    int64_t v350 = v13 * 16;
    float v507 = v4 * v504;
    float v514 = v4 * v511;
    float v521 = v4 * v518;
    float v528 = v4 * v525;
    float v535 = v4 * v532;
    float v542 = v4 * v539;
    float v549 = v4 * v546;
    float v556 = v4 * v553;
    float v563 = v4 * v560;
    float v570 = v4 * v567;
    float v577 = v4 * v574;
    float v584 = v4 * v581;
    float v591 = v4 * v588;
    float v598 = v4 * v595;
    float v605 = v4 * v602;
    float v612 = v4 * v609;
    float v619 = v4 * v616;
    float v626 = v4 * v623;
    float v633 = v4 * v630;
    float v640 = v4 * v637;
    float v647 = v4 * v644;
    int64_t v743 = v2 * 16;
    int64_t v752 = v2 * 2;
    int64_t v761 = v2 * 15;
    int64_t v770 = v2 * 3;
    int64_t v779 = v2 * 14;
    int64_t v788 = v2 * 4;
    int64_t v797 = v2 * 13;
    int64_t v806 = v2 * 5;
    int64_t v815 = v2 * 12;
    int64_t v824 = v2 * 6;
    int64_t v833 = v2 * 11;
    int64_t v842 = v2 * 7;
    int64_t v851 = v2 * 10;
    int64_t v860 = v2 * 8;
    int64_t v869 = v2 * 9;
    const float32x2_t *v1029 = &v5[0];
    svint64_t v1030 = svindex_s64(0, v1);
    svfloat32_t v1033 = svdup_n_f32(v434);
    svfloat32_t v1034 = svdup_n_f32(v439);
    svfloat32_t v1035 = svdup_n_f32(v444);
    svfloat32_t v1036 = svdup_n_f32(v449);
    svfloat32_t v1037 = svdup_n_f32(v454);
    svfloat32_t v1038 = svdup_n_f32(v459);
    svfloat32_t v1039 = svdup_n_f32(v464);
    svfloat32_t v1040 = svdup_n_f32(v469);
    svfloat32_t v1041 = svdup_n_f32(v474);
    svfloat32_t v1042 = svdup_n_f32(v479);
    svfloat32_t v1043 = svdup_n_f32(v484);
    svfloat32_t v1044 = svdup_n_f32(v489);
    svfloat32_t v1045 = svdup_n_f32(v494);
    svfloat32_t v1046 = svdup_n_f32(v499);
    int32_t *v1075 = &v6[0];
    svfloat32_t v51 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v350]));
    int64_t v57 = v55 + v350;
    int64_t v92 = v90 + v350;
    int64_t v99 = v97 + v350;
    int64_t v134 = v132 + v350;
    int64_t v141 = v139 + v350;
    int64_t v176 = v174 + v350;
    int64_t v183 = v181 + v350;
    int64_t v218 = v216 + v350;
    int64_t v225 = v223 + v350;
    int64_t v260 = v258 + v350;
    int64_t v267 = v265 + v350;
    int64_t v302 = v300 + v350;
    int64_t v309 = v10 + v350;
    int64_t v344 = v342 + v350;
    int64_t v351 = v349 + v350;
    svfloat32_t v884 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v882), v1030));
    const float32x2_t *v892 = &v5[v33];
    const float32x2_t *v902 = &v5[v61];
    const float32x2_t *v911 = &v5[v75];
    const float32x2_t *v920 = &v5[v103];
    const float32x2_t *v929 = &v5[v117];
    const float32x2_t *v938 = &v5[v145];
    const float32x2_t *v947 = &v5[v159];
    const float32x2_t *v956 = &v5[v187];
    const float32x2_t *v965 = &v5[v201];
    const float32x2_t *v974 = &v5[v229];
    const float32x2_t *v983 = &v5[v243];
    const float32x2_t *v992 = &v5[v271];
    const float32x2_t *v1001 = &v5[v285];
    const float32x2_t *v1010 = &v5[v313];
    const float32x2_t *v1019 = &v5[v327];
    svfloat32_t v1031 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1029), v1030));
    svfloat32_t v1047 = svdup_n_f32(v507);
    svfloat32_t v1048 = svdup_n_f32(v514);
    svfloat32_t v1049 = svdup_n_f32(v521);
    svfloat32_t v1050 = svdup_n_f32(v528);
    svfloat32_t v1051 = svdup_n_f32(v535);
    svfloat32_t v1052 = svdup_n_f32(v542);
    svfloat32_t v1053 = svdup_n_f32(v549);
    svfloat32_t v1054 = svdup_n_f32(v556);
    svfloat32_t v1055 = svdup_n_f32(v563);
    svfloat32_t v1056 = svdup_n_f32(v570);
    svfloat32_t v1057 = svdup_n_f32(v577);
    svfloat32_t v1058 = svdup_n_f32(v584);
    svfloat32_t v1059 = svdup_n_f32(v591);
    svfloat32_t v1060 = svdup_n_f32(v598);
    svfloat32_t v1061 = svdup_n_f32(v605);
    svfloat32_t v1062 = svdup_n_f32(v612);
    svfloat32_t v1063 = svdup_n_f32(v619);
    svfloat32_t v1064 = svdup_n_f32(v626);
    svfloat32_t v1065 = svdup_n_f32(v633);
    svfloat32_t v1066 = svdup_n_f32(v640);
    svfloat32_t v1067 = svdup_n_f32(v647);
    int32_t *v1093 = &v6[v743];
    int32_t *v1102 = &v6[v752];
    int32_t *v1111 = &v6[v761];
    int32_t *v1120 = &v6[v770];
    int32_t *v1129 = &v6[v779];
    int32_t *v1138 = &v6[v788];
    int32_t *v1147 = &v6[v797];
    int32_t *v1156 = &v6[v806];
    int32_t *v1165 = &v6[v815];
    int32_t *v1174 = &v6[v824];
    int32_t *v1183 = &v6[v833];
    int32_t *v1192 = &v6[v842];
    int32_t *v1201 = &v6[v851];
    int32_t *v1210 = &v6[v860];
    int32_t *v1219 = &v6[v869];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v884, v51, 0),
                     v884, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v135 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v134]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v177 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v176]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v219 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v218]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v303 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v302]));
    svfloat32_t v310 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v309]));
    svfloat32_t v345 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v344]));
    svfloat32_t v352 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v351]));
    svfloat32_t v894 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v892), v1030));
    svfloat32_t v904 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v902), v1030));
    svfloat32_t v913 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v911), v1030));
    svfloat32_t v922 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v920), v1030));
    svfloat32_t v931 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v929), v1030));
    svfloat32_t v940 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v938), v1030));
    svfloat32_t v949 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v947), v1030));
    svfloat32_t v958 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v956), v1030));
    svfloat32_t v967 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v965), v1030));
    svfloat32_t v976 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v974), v1030));
    svfloat32_t v985 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v983), v1030));
    svfloat32_t v994 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v992), v1030));
    svfloat32_t v1003 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1001), v1030));
    svfloat32_t v1012 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1010), v1030));
    svfloat32_t v1021 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1019), v1030));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v894, v58, 0),
                     v894, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v904, v93, 0),
                     v904, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v913, v100, 0),
                     v913, v100, 90);
    svfloat32_t zero136 = svdup_n_f32(0);
    svfloat32_t v136 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero136, v922, v135, 0),
                     v922, v135, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero143, v931, v142, 0),
                     v931, v142, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero178, v940, v177, 0),
                     v940, v177, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero185, v949, v184, 0),
                     v949, v184, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero220, v958, v219, 0),
                     v958, v219, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero227, v967, v226, 0),
                     v967, v226, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero262, v976, v261, 0),
                     v976, v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero269, v985, v268, 0),
                     v985, v268, 90);
    svfloat32_t zero304 = svdup_n_f32(0);
    svfloat32_t v304 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero304, v994, v303, 0),
                     v994, v303, 90);
    svfloat32_t zero311 = svdup_n_f32(0);
    svfloat32_t v311 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero311, v1003, v310, 0), v1003,
        v310, 90);
    svfloat32_t zero346 = svdup_n_f32(0);
    svfloat32_t v346 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero346, v1012, v345, 0), v1012,
        v345, 90);
    svfloat32_t zero353 = svdup_n_f32(0);
    svfloat32_t v353 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero353, v1021, v352, 0), v1021,
        v352, 90);
    svfloat32_t v354 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v355 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v356 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v357 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v358 = svadd_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v359 = svsub_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v362 = svadd_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v363 = svsub_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v364 = svadd_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v365 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v304, v311);
    svfloat32_t v367 = svsub_f32_x(svptrue_b32(), v304, v311);
    svfloat32_t v368 = svadd_f32_x(svptrue_b32(), v346, v353);
    svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v346, v353);
    svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v354, v362);
    svfloat32_t v371 = svadd_f32_x(svptrue_b32(), v356, v364);
    svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v358, v366);
    svfloat32_t v373 = svadd_f32_x(svptrue_b32(), v360, v368);
    svfloat32_t v376 = svsub_f32_x(svptrue_b32(), v354, v362);
    svfloat32_t v377 = svsub_f32_x(svptrue_b32(), v356, v364);
    svfloat32_t v378 = svsub_f32_x(svptrue_b32(), v358, v366);
    svfloat32_t v379 = svsub_f32_x(svptrue_b32(), v360, v368);
    svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v355, v359);
    svfloat32_t v391 = svadd_f32_x(svptrue_b32(), v357, v361);
    svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v355, v359);
    svfloat32_t v393 = svsub_f32_x(svptrue_b32(), v369, v365);
    svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v363, v367);
    svfloat32_t v395 = svadd_f32_x(svptrue_b32(), v365, v369);
    svfloat32_t v396 = svsub_f32_x(svptrue_b32(), v363, v367);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v357, v361);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v355, v363);
    svfloat32_t v411 = svadd_f32_x(svptrue_b32(), v361, v369);
    svfloat32_t v374 = svadd_f32_x(svptrue_b32(), v370, v372);
    svfloat32_t v375 = svadd_f32_x(svptrue_b32(), v371, v373);
    svfloat32_t v380 = svsub_f32_x(svptrue_b32(), v370, v372);
    svfloat32_t v381 = svsub_f32_x(svptrue_b32(), v371, v373);
    svfloat32_t v384 = svadd_f32_x(svptrue_b32(), v377, v379);
    svfloat32_t v385 = svadd_f32_x(svptrue_b32(), v376, v378);
    svfloat32_t v387 = svsub_f32_x(svptrue_b32(), v378, v379);
    svfloat32_t v388 = svsub_f32_x(svptrue_b32(), v376, v377);
    svfloat32_t v398 = svadd_f32_x(svptrue_b32(), v390, v391);
    svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v394, v395);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v390, v391);
    svfloat32_t v402 = svsub_f32_x(svptrue_b32(), v394, v395);
    svfloat32_t v404 = svadd_f32_x(svptrue_b32(), v392, v393);
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v396, v397);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v392, v393);
    svfloat32_t v408 = svsub_f32_x(svptrue_b32(), v396, v397);
    svfloat32_t v447 = svmul_f32_x(svptrue_b32(), v378, v1035);
    svfloat32_t zero614 = svdup_n_f32(0);
    svfloat32_t v614 = svcmla_f32_x(pred_full, zero614, v1062, v411, 90);
    svfloat32_t v382 = svadd_f32_x(svptrue_b32(), v374, v375);
    svfloat32_t v383 = svsub_f32_x(svptrue_b32(), v374, v375);
    svfloat32_t v386 = svsub_f32_x(svptrue_b32(), v385, v384);
    svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v380, v381);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v398, v399);
    svfloat32_t v403 = svadd_f32_x(svptrue_b32(), v401, v402);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v404, v405);
    svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v407, v408);
    svfloat32_t v412 = svsub_f32_x(svptrue_b32(), v405, v399);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v398, v404);
    svfloat32_t v457 = svmul_f32_x(svptrue_b32(), v380, v1037);
    svfloat32_t v462 = svmul_f32_x(svptrue_b32(), v381, v1038);
    svfloat32_t v492 = svmul_f32_x(svptrue_b32(), v387, v1044);
    svfloat32_t v497 = svmul_f32_x(svptrue_b32(), v388, v1045);
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v412, v355);
    svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v415, v361);
    svfloat32_t v427 = svadd_f32_x(svptrue_b32(), v1031, v382);
    svfloat32_t v487 = svmul_f32_x(svptrue_b32(), v386, v1043);
    svfloat32_t zero523 = svdup_n_f32(0);
    svfloat32_t v523 = svcmla_f32_x(pred_full, zero523, v1049, v400, 90);
    svfloat32_t zero544 = svdup_n_f32(0);
    svfloat32_t v544 = svcmla_f32_x(pred_full, zero544, v1052, v403, 90);
    svfloat32_t zero565 = svdup_n_f32(0);
    svfloat32_t v565 = svcmla_f32_x(pred_full, zero565, v1055, v406, 90);
    svfloat32_t zero586 = svdup_n_f32(0);
    svfloat32_t v586 = svcmla_f32_x(pred_full, zero586, v1058, v409, 90);
    svfloat32_t v652 = svmla_f32_x(pred_full, v492, v379, v1036);
    svfloat32_t v653 = svnmls_f32_x(pred_full, v447, v387, v1044);
    svfloat32_t v654 = svmla_f32_x(pred_full, v497, v377, v1034);
    svfloat32_t v655 = svnmls_f32_x(pred_full, v497, v376, v1033);
    svfloat32_t v414 = svsub_f32_x(svptrue_b32(), v413, v411);
    svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v416, v363);
    svfloat32_t v650 = svmla_f32_x(pred_full, v487, v384, v1041);
    svfloat32_t v651 = svnmls_f32_x(pred_full, v487, v385, v1042);
    svfloat32_t v656 = svnmls_f32_x(pred_full, v462, v389, v1046);
    svfloat32_t v657 = svmla_f32_x(pred_full, v457, v389, v1046);
    svfloat32_t v658 = svmla_f32_x(pred_full, v427, v382, v1039);
    svfloat32_t v677 = svcmla_f32_x(pred_full, v523, v1047, v398, 90);
    svfloat32_t v678 = svcmla_f32_x(pred_full, v523, v1048, v399, 90);
    svfloat32_t v679 = svcmla_f32_x(pred_full, v544, v1050, v401, 90);
    svfloat32_t v680 = svcmla_f32_x(pred_full, v544, v1051, v402, 90);
    svfloat32_t v681 = svcmla_f32_x(pred_full, v565, v1053, v404, 90);
    svfloat32_t v682 = svcmla_f32_x(pred_full, v565, v1054, v405, 90);
    svfloat32_t v683 = svcmla_f32_x(pred_full, v586, v1056, v407, 90);
    svfloat32_t v684 = svcmla_f32_x(pred_full, v586, v1057, v408, 90);
    svint16_t v726 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v427, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v417, v369);
    svfloat32_t zero635 = svdup_n_f32(0);
    svfloat32_t v635 = svcmla_f32_x(pred_full, zero635, v1065, v414, 90);
    svfloat32_t v659 = svmla_f32_x(pred_full, v658, v383, v1040);
    svfloat32_t v660 = svmls_f32_x(pred_full, v658, v383, v1040);
    svfloat32_t v661 = svsub_f32_x(svptrue_b32(), v650, v652);
    svfloat32_t v663 = svadd_f32_x(svptrue_b32(), v651, v653);
    svfloat32_t v665 = svadd_f32_x(svptrue_b32(), v650, v654);
    svfloat32_t v667 = svadd_f32_x(svptrue_b32(), v651, v655);
    svfloat32_t v688 = svadd_f32_x(svptrue_b32(), v677, v679);
    svfloat32_t v689 = svsub_f32_x(svptrue_b32(), v677, v679);
    svfloat32_t v690 = svadd_f32_x(svptrue_b32(), v678, v680);
    svfloat32_t v691 = svsub_f32_x(svptrue_b32(), v678, v680);
    svfloat32_t v692 = svadd_f32_x(svptrue_b32(), v681, v683);
    svfloat32_t v693 = svsub_f32_x(svptrue_b32(), v683, v681);
    svfloat32_t v694 = svadd_f32_x(svptrue_b32(), v682, v684);
    svfloat32_t v695 = svsub_f32_x(svptrue_b32(), v684, v682);
    svst1w_u64(pred_full, (unsigned *)(v1075), svreinterpret_u64_s16(v726));
    svfloat32_t v419 = svadd_f32_x(svptrue_b32(), v414, v418);
    svfloat32_t zero642 = svdup_n_f32(0);
    svfloat32_t v642 = svcmla_f32_x(pred_full, zero642, v1066, v418, 90);
    svfloat32_t v662 = svadd_f32_x(svptrue_b32(), v656, v659);
    svfloat32_t v664 = svadd_f32_x(svptrue_b32(), v657, v660);
    svfloat32_t v666 = svsub_f32_x(svptrue_b32(), v659, v656);
    svfloat32_t v668 = svsub_f32_x(svptrue_b32(), v660, v657);
    svfloat32_t v705 = svadd_f32_x(svptrue_b32(), v690, v694);
    svfloat32_t v707 = svadd_f32_x(svptrue_b32(), v689, v695);
    svfloat32_t v709 = svsub_f32_x(svptrue_b32(), v688, v692);
    svfloat32_t v711 = svsub_f32_x(svptrue_b32(), v695, v689);
    svfloat32_t v713 = svadd_f32_x(svptrue_b32(), v688, v692);
    svfloat32_t v716 = svsub_f32_x(svptrue_b32(), v693, v691);
    svfloat32_t v719 = svsub_f32_x(svptrue_b32(), v694, v690);
    svfloat32_t v722 = svadd_f32_x(svptrue_b32(), v691, v693);
    svfloat32_t v669 = svadd_f32_x(svptrue_b32(), v661, v662);
    svfloat32_t v670 = svadd_f32_x(svptrue_b32(), v663, v664);
    svfloat32_t v671 = svadd_f32_x(svptrue_b32(), v665, v666);
    svfloat32_t v672 = svadd_f32_x(svptrue_b32(), v667, v668);
    svfloat32_t v673 = svsub_f32_x(svptrue_b32(), v662, v661);
    svfloat32_t v674 = svsub_f32_x(svptrue_b32(), v664, v663);
    svfloat32_t v675 = svsub_f32_x(svptrue_b32(), v666, v665);
    svfloat32_t v676 = svsub_f32_x(svptrue_b32(), v668, v667);
    svfloat32_t v696 = svsub_f32_x(svptrue_b32(), v635, v642);
    svfloat32_t v685 = svcmla_f32_x(pred_full, v642, v1067, v419, 90);
    svfloat32_t v698 = svadd_f32_x(svptrue_b32(), v696, v696);
    svfloat32_t v723 = svsub_f32_x(svptrue_b32(), v722, v696);
    svfloat32_t v686 = svcmla_f32_x(pred_full, v685, v1059, v410, 90);
    svfloat32_t v699 = svsub_f32_x(svptrue_b32(), v614, v698);
    svfloat32_t v702 = svadd_f32_x(svptrue_b32(), v685, v685);
    svfloat32_t v720 = svadd_f32_x(svptrue_b32(), v719, v698);
    svfloat32_t v768 = svadd_f32_x(svptrue_b32(), v676, v723);
    svfloat32_t v777 = svsub_f32_x(svptrue_b32(), v676, v723);
    svfloat32_t v687 = svcmla_f32_x(pred_full, v686, v1060, v355, 90);
    svfloat32_t v697 = svcmla_f32_x(pred_full, v686, v1061, v363, 90);
    svfloat32_t v700 = svcmla_f32_x(pred_full, v699, v1063, v361, 90);
    svfloat32_t v701 = svcmla_f32_x(pred_full, v699, v1064, v369, 90);
    svfloat32_t v703 = svadd_f32_x(svptrue_b32(), v702, v702);
    svfloat32_t v704 = svadd_f32_x(svptrue_b32(), v696, v702);
    svfloat32_t v710 = svadd_f32_x(svptrue_b32(), v709, v702);
    svfloat32_t v721 = svadd_f32_x(svptrue_b32(), v720, v702);
    svint16_t v771 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v768, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v780 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v777, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v706 = svadd_f32_x(svptrue_b32(), v705, v697);
    svfloat32_t v708 = svadd_f32_x(svptrue_b32(), v707, v700);
    svfloat32_t v712 = svsub_f32_x(svptrue_b32(), v711, v704);
    svfloat32_t v714 = svadd_f32_x(svptrue_b32(), v713, v687);
    svfloat32_t v717 = svsub_f32_x(svptrue_b32(), v716, v701);
    svfloat32_t v750 = svadd_f32_x(svptrue_b32(), v671, v710);
    svfloat32_t v759 = svsub_f32_x(svptrue_b32(), v671, v710);
    svfloat32_t v858 = svadd_f32_x(svptrue_b32(), v675, v721);
    svfloat32_t v867 = svsub_f32_x(svptrue_b32(), v675, v721);
    svst1w_u64(pred_full, (unsigned *)(v1120), svreinterpret_u64_s16(v771));
    svst1w_u64(pred_full, (unsigned *)(v1129), svreinterpret_u64_s16(v780));
    svfloat32_t v715 = svadd_f32_x(svptrue_b32(), v714, v696);
    svfloat32_t v718 = svadd_f32_x(svptrue_b32(), v717, v703);
    svfloat32_t v732 = svadd_f32_x(svptrue_b32(), v669, v706);
    svfloat32_t v741 = svsub_f32_x(svptrue_b32(), v669, v706);
    svint16_t v753 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v750, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v762 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v759, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v804 = svadd_f32_x(svptrue_b32(), v672, v712);
    svfloat32_t v813 = svsub_f32_x(svptrue_b32(), v672, v712);
    svfloat32_t v822 = svadd_f32_x(svptrue_b32(), v670, v708);
    svfloat32_t v831 = svsub_f32_x(svptrue_b32(), v670, v708);
    svint16_t v861 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v858, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v870 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v867, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v735 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v732, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v744 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v741, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v786 = svadd_f32_x(svptrue_b32(), v673, v715);
    svfloat32_t v795 = svsub_f32_x(svptrue_b32(), v673, v715);
    svint16_t v807 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v804, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v816 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v813, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v825 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v822, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v834 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v831, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v840 = svadd_f32_x(svptrue_b32(), v674, v718);
    svfloat32_t v849 = svsub_f32_x(svptrue_b32(), v674, v718);
    svst1w_u64(pred_full, (unsigned *)(v1102), svreinterpret_u64_s16(v753));
    svst1w_u64(pred_full, (unsigned *)(v1111), svreinterpret_u64_s16(v762));
    svst1w_u64(pred_full, (unsigned *)(v1210), svreinterpret_u64_s16(v861));
    svst1w_u64(pred_full, (unsigned *)(v1219), svreinterpret_u64_s16(v870));
    svint16_t v789 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v786, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v798 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v795, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v843 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v840, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v852 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v849, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1084), svreinterpret_u64_s16(v735));
    svst1w_u64(pred_full, (unsigned *)(v1093), svreinterpret_u64_s16(v744));
    svst1w_u64(pred_full, (unsigned *)(v1156), svreinterpret_u64_s16(v807));
    svst1w_u64(pred_full, (unsigned *)(v1165), svreinterpret_u64_s16(v816));
    svst1w_u64(pred_full, (unsigned *)(v1174), svreinterpret_u64_s16(v825));
    svst1w_u64(pred_full, (unsigned *)(v1183), svreinterpret_u64_s16(v834));
    svst1w_u64(pred_full, (unsigned *)(v1138), svreinterpret_u64_s16(v789));
    svst1w_u64(pred_full, (unsigned *)(v1147), svreinterpret_u64_s16(v798));
    svst1w_u64(pred_full, (unsigned *)(v1192), svreinterpret_u64_s16(v843));
    svst1w_u64(pred_full, (unsigned *)(v1201), svreinterpret_u64_s16(v852));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu18(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v317 = v5[istride];
    float v695 = -5.0000000000000000e-01F;
    float v706 = -1.4999999999999998e+00F;
    float v709 = 8.6602540378443871e-01F;
    float v710 = -8.6602540378443871e-01F;
    float v717 = 7.6604444311897801e-01F;
    float v721 = 9.3969262078590832e-01F;
    float v725 = -1.7364817766693039e-01F;
    float v728 = 6.4278760968653925e-01F;
    float v729 = -6.4278760968653925e-01F;
    float v735 = -3.4202014332566888e-01F;
    float v736 = 3.4202014332566888e-01F;
    float v742 = 9.8480775301220802e-01F;
    float v743 = -9.8480775301220802e-01F;
    float32x2_t v745 = (float32x2_t){v4, v4};
    float32x2_t v349 = vtrn1_f32(v317, v317);
    float32x2_t v350 = vtrn2_f32(v317, v317);
    float32x2_t v547 = v5[0];
    float32x2_t v696 = (float32x2_t){v695, v695};
    float32x2_t v707 = (float32x2_t){v706, v706};
    float32x2_t v711 = (float32x2_t){v709, v710};
    float32x2_t v718 = (float32x2_t){v717, v717};
    float32x2_t v722 = (float32x2_t){v721, v721};
    float32x2_t v726 = (float32x2_t){v725, v725};
    float32x2_t v730 = (float32x2_t){v728, v729};
    float32x2_t v737 = (float32x2_t){v735, v736};
    float32x2_t v744 = (float32x2_t){v742, v743};
    float32x2_t v20 = v5[istride * 9];
    int64_t v37 = 16 + j * 34;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 11];
    int64_t v86 = 2 + j * 34;
    int64_t v99 = 20 + j * 34;
    float32x2_t v113 = v5[istride * 4];
    float32x2_t v131 = v5[istride * 13];
    int64_t v148 = 6 + j * 34;
    int64_t v161 = 24 + j * 34;
    float32x2_t v175 = v5[istride * 6];
    float32x2_t v193 = v5[istride * 15];
    int64_t v210 = 10 + j * 34;
    int64_t v223 = 28 + j * 34;
    float32x2_t v237 = v5[istride * 8];
    float32x2_t v255 = v5[istride * 17];
    int64_t v272 = 14 + j * 34;
    int64_t v285 = 32 + j * 34;
    float32x2_t v299 = v5[istride * 10];
    int64_t v334 = 18 + j * 34;
    float32x2_t v348 = v7[j * 34];
    int64_t v352 = j * 34 + 1;
    float32x2_t v361 = v5[istride * 12];
    float32x2_t v379 = v5[istride * 3];
    int64_t v396 = 22 + j * 34;
    int64_t v409 = 4 + j * 34;
    float32x2_t v423 = v5[istride * 14];
    float32x2_t v441 = v5[istride * 5];
    int64_t v458 = 26 + j * 34;
    int64_t v471 = 8 + j * 34;
    float32x2_t v485 = v5[istride * 16];
    float32x2_t v503 = v5[istride * 7];
    int64_t v520 = 30 + j * 34;
    int64_t v533 = 12 + j * 34;
    float32x2_t v713 = vmul_f32(v745, v711);
    float32x2_t v732 = vmul_f32(v745, v730);
    float32x2_t v739 = vmul_f32(v745, v737);
    float32x2_t v746 = vmul_f32(v745, v744);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v273 = v7[v272];
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    int64_t v277 = v272 + 1;
    float32x2_t v286 = v7[v285];
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    int64_t v290 = v285 + 1;
    float32x2_t v335 = v7[v334];
    float32x2_t v336 = vtrn1_f32(v299, v299);
    float32x2_t v337 = vtrn2_f32(v299, v299);
    int64_t v339 = v334 + 1;
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vmul_f32(v349, v348);
    float32x2_t v397 = v7[v396];
    float32x2_t v398 = vtrn1_f32(v361, v361);
    float32x2_t v399 = vtrn2_f32(v361, v361);
    int64_t v401 = v396 + 1;
    float32x2_t v410 = v7[v409];
    float32x2_t v411 = vtrn1_f32(v379, v379);
    float32x2_t v412 = vtrn2_f32(v379, v379);
    int64_t v414 = v409 + 1;
    float32x2_t v459 = v7[v458];
    float32x2_t v460 = vtrn1_f32(v423, v423);
    float32x2_t v461 = vtrn2_f32(v423, v423);
    int64_t v463 = v458 + 1;
    float32x2_t v472 = v7[v471];
    float32x2_t v473 = vtrn1_f32(v441, v441);
    float32x2_t v474 = vtrn2_f32(v441, v441);
    int64_t v476 = v471 + 1;
    float32x2_t v521 = v7[v520];
    float32x2_t v522 = vtrn1_f32(v485, v485);
    float32x2_t v523 = vtrn2_f32(v485, v485);
    int64_t v525 = v520 + 1;
    float32x2_t v534 = v7[v533];
    float32x2_t v535 = vtrn1_f32(v503, v503);
    float32x2_t v536 = vtrn2_f32(v503, v503);
    int64_t v538 = v533 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vmul_f32(v336, v335);
    float32x2_t v402 = v7[v401];
    float32x2_t v403 = vmul_f32(v398, v397);
    float32x2_t v415 = v7[v414];
    float32x2_t v416 = vmul_f32(v411, v410);
    float32x2_t v464 = v7[v463];
    float32x2_t v465 = vmul_f32(v460, v459);
    float32x2_t v477 = v7[v476];
    float32x2_t v478 = vmul_f32(v473, v472);
    float32x2_t v526 = v7[v525];
    float32x2_t v527 = vmul_f32(v522, v521);
    float32x2_t v539 = v7[v538];
    float32x2_t v540 = vmul_f32(v535, v534);
    float32x2_t v356 = vfma_f32(v354, v350, v353);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v343 = vfma_f32(v341, v337, v340);
    float32x2_t v405 = vfma_f32(v403, v399, v402);
    float32x2_t v418 = vfma_f32(v416, v412, v415);
    float32x2_t v467 = vfma_f32(v465, v461, v464);
    float32x2_t v480 = vfma_f32(v478, v474, v477);
    float32x2_t v529 = vfma_f32(v527, v523, v526);
    float32x2_t v542 = vfma_f32(v540, v536, v539);
    float32x2_t v548 = vadd_f32(v547, v46);
    float32x2_t v549 = vsub_f32(v547, v46);
    float32x2_t v550 = vadd_f32(v95, v108);
    float32x2_t v551 = vsub_f32(v95, v108);
    float32x2_t v552 = vadd_f32(v157, v170);
    float32x2_t v553 = vsub_f32(v157, v170);
    float32x2_t v554 = vadd_f32(v219, v232);
    float32x2_t v555 = vsub_f32(v219, v232);
    float32x2_t v556 = vadd_f32(v281, v294);
    float32x2_t v557 = vsub_f32(v281, v294);
    float32x2_t v558 = vadd_f32(v343, v356);
    float32x2_t v559 = vsub_f32(v343, v356);
    float32x2_t v560 = vadd_f32(v405, v418);
    float32x2_t v561 = vsub_f32(v405, v418);
    float32x2_t v562 = vadd_f32(v467, v480);
    float32x2_t v563 = vsub_f32(v467, v480);
    float32x2_t v564 = vadd_f32(v529, v542);
    float32x2_t v565 = vsub_f32(v529, v542);
    float32x2_t v566 = vadd_f32(v550, v564);
    float32x2_t v567 = vsub_f32(v550, v564);
    float32x2_t v568 = vadd_f32(v562, v552);
    float32x2_t v569 = vsub_f32(v562, v552);
    float32x2_t v570 = vadd_f32(v554, v560);
    float32x2_t v571 = vsub_f32(v554, v560);
    float32x2_t v572 = vadd_f32(v556, v558);
    float32x2_t v573 = vsub_f32(v556, v558);
    float32x2_t v670 = vadd_f32(v551, v565);
    float32x2_t v671 = vsub_f32(v551, v565);
    float32x2_t v672 = vadd_f32(v563, v553);
    float32x2_t v673 = vsub_f32(v563, v553);
    float32x2_t v674 = vadd_f32(v555, v561);
    float32x2_t v675 = vsub_f32(v555, v561);
    float32x2_t v676 = vadd_f32(v557, v559);
    float32x2_t v677 = vsub_f32(v557, v559);
    float32x2_t v574 = vadd_f32(v566, v568);
    float32x2_t v578 = vadd_f32(v567, v569);
    float32x2_t v580 = vsub_f32(v566, v568);
    float32x2_t v581 = vsub_f32(v568, v572);
    float32x2_t v582 = vsub_f32(v572, v566);
    float32x2_t v583 = vsub_f32(v567, v569);
    float32x2_t v584 = vsub_f32(v569, v573);
    float32x2_t v585 = vsub_f32(v573, v567);
    float32x2_t v604 = vmul_f32(v570, v707);
    float32x2_t v610 = vrev64_f32(v571);
    float32x2_t v678 = vadd_f32(v670, v672);
    float32x2_t v682 = vadd_f32(v671, v673);
    float32x2_t v684 = vsub_f32(v670, v672);
    float32x2_t v685 = vsub_f32(v672, v676);
    float32x2_t v686 = vsub_f32(v676, v670);
    float32x2_t v687 = vsub_f32(v671, v673);
    float32x2_t v688 = vsub_f32(v673, v677);
    float32x2_t v689 = vsub_f32(v677, v671);
    float32x2_t v708 = vmul_f32(v674, v707);
    float32x2_t v714 = vrev64_f32(v675);
    float32x2_t v575 = vadd_f32(v574, v572);
    float32x2_t v579 = vadd_f32(v578, v573);
    float32x2_t v611 = vmul_f32(v610, v713);
    float32x2_t v615 = vmul_f32(v580, v718);
    float32x2_t v619 = vmul_f32(v581, v722);
    float32x2_t v623 = vmul_f32(v582, v726);
    float32x2_t v629 = vrev64_f32(v583);
    float32x2_t v636 = vrev64_f32(v584);
    float32x2_t v643 = vrev64_f32(v585);
    float32x2_t v679 = vadd_f32(v678, v676);
    float32x2_t v683 = vadd_f32(v682, v677);
    float32x2_t v715 = vmul_f32(v714, v713);
    float32x2_t v719 = vmul_f32(v684, v718);
    float32x2_t v723 = vmul_f32(v685, v722);
    float32x2_t v727 = vmul_f32(v686, v726);
    float32x2_t v733 = vrev64_f32(v687);
    float32x2_t v740 = vrev64_f32(v688);
    float32x2_t v747 = vrev64_f32(v689);
    float32x2_t v576 = vadd_f32(v575, v570);
    float32x2_t v593 = vmul_f32(v575, v696);
    float32x2_t v599 = vrev64_f32(v579);
    float32x2_t v630 = vmul_f32(v629, v732);
    float32x2_t v637 = vmul_f32(v636, v739);
    float32x2_t v644 = vmul_f32(v643, v746);
    float32x2_t v680 = vadd_f32(v679, v674);
    float32x2_t v697 = vmul_f32(v679, v696);
    float32x2_t v703 = vrev64_f32(v683);
    float32x2_t v734 = vmul_f32(v733, v732);
    float32x2_t v741 = vmul_f32(v740, v739);
    float32x2_t v748 = vmul_f32(v747, v746);
    float32x2_t v577 = vadd_f32(v576, v548);
    float32x2_t v600 = vmul_f32(v599, v713);
    float32x2_t v645 = vadd_f32(v593, v593);
    float32x2_t v658 = vadd_f32(v611, v630);
    float32x2_t v660 = vsub_f32(v611, v637);
    float32x2_t v662 = vsub_f32(v611, v630);
    float32x2_t v681 = vadd_f32(v680, v549);
    float32x2_t v704 = vmul_f32(v703, v713);
    float32x2_t v749 = vadd_f32(v697, v697);
    float32x2_t v762 = vadd_f32(v715, v734);
    float32x2_t v764 = vsub_f32(v715, v741);
    float32x2_t v766 = vsub_f32(v715, v734);
    float32x2_t v646 = vadd_f32(v645, v593);
    float32x2_t v650 = vadd_f32(v577, v604);
    float32x2_t v659 = vadd_f32(v658, v637);
    float32x2_t v661 = vadd_f32(v660, v644);
    float32x2_t v663 = vsub_f32(v662, v644);
    float32x2_t v750 = vadd_f32(v749, v697);
    float32x2_t v754 = vadd_f32(v681, v708);
    float32x2_t v763 = vadd_f32(v762, v741);
    float32x2_t v765 = vadd_f32(v764, v748);
    float32x2_t v767 = vsub_f32(v766, v748);
    int16x4_t v776 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v577, 15), (int32x2_t){0, 0}));
    int16x4_t v782 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v681, 15), (int32x2_t){0, 0}));
    float32x2_t v647 = vadd_f32(v577, v646);
    float32x2_t v651 = vadd_f32(v650, v645);
    float32x2_t v751 = vadd_f32(v681, v750);
    float32x2_t v755 = vadd_f32(v754, v749);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v776), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v782), 0);
    float32x2_t v648 = vadd_f32(v647, v600);
    float32x2_t v649 = vsub_f32(v647, v600);
    float32x2_t v652 = vadd_f32(v651, v615);
    float32x2_t v654 = vsub_f32(v651, v619);
    float32x2_t v656 = vsub_f32(v651, v615);
    float32x2_t v752 = vadd_f32(v751, v704);
    float32x2_t v753 = vsub_f32(v751, v704);
    float32x2_t v756 = vadd_f32(v755, v719);
    float32x2_t v758 = vsub_f32(v755, v723);
    float32x2_t v760 = vsub_f32(v755, v719);
    float32x2_t v653 = vadd_f32(v652, v619);
    float32x2_t v655 = vadd_f32(v654, v623);
    float32x2_t v657 = vsub_f32(v656, v623);
    float32x2_t v757 = vadd_f32(v756, v723);
    float32x2_t v759 = vadd_f32(v758, v727);
    float32x2_t v761 = vsub_f32(v760, v727);
    int16x4_t v812 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v649, 15), (int32x2_t){0, 0}));
    int16x4_t v818 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v753, 15), (int32x2_t){0, 0}));
    int16x4_t v848 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v648, 15), (int32x2_t){0, 0}));
    int16x4_t v854 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v752, 15), (int32x2_t){0, 0}));
    float32x2_t v664 = vadd_f32(v653, v659);
    float32x2_t v665 = vsub_f32(v653, v659);
    float32x2_t v666 = vadd_f32(v655, v661);
    float32x2_t v667 = vsub_f32(v655, v661);
    float32x2_t v668 = vadd_f32(v657, v663);
    float32x2_t v669 = vsub_f32(v657, v663);
    float32x2_t v768 = vadd_f32(v757, v763);
    float32x2_t v769 = vsub_f32(v757, v763);
    float32x2_t v770 = vadd_f32(v759, v765);
    float32x2_t v771 = vsub_f32(v759, v765);
    float32x2_t v772 = vadd_f32(v761, v767);
    float32x2_t v773 = vsub_f32(v761, v767);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v812), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v818), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v848), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v854), 0);
    int16x4_t v788 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v665, 15), (int32x2_t){0, 0}));
    int16x4_t v794 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v769, 15), (int32x2_t){0, 0}));
    int16x4_t v800 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v666, 15), (int32x2_t){0, 0}));
    int16x4_t v806 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v770, 15), (int32x2_t){0, 0}));
    int16x4_t v824 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v669, 15), (int32x2_t){0, 0}));
    int16x4_t v830 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v773, 15), (int32x2_t){0, 0}));
    int16x4_t v836 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v668, 15), (int32x2_t){0, 0}));
    int16x4_t v842 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v772, 15), (int32x2_t){0, 0}));
    int16x4_t v860 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v667, 15), (int32x2_t){0, 0}));
    int16x4_t v866 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v771, 15), (int32x2_t){0, 0}));
    int16x4_t v872 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v664, 15), (int32x2_t){0, 0}));
    int16x4_t v878 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v768, 15), (int32x2_t){0, 0}));
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v788), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v794), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v800), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v806), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v824), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v830), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v836), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v842), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v860), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v866), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v872), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v878), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu18(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v536 = -5.0000000000000000e-01F;
    float v548 = -1.4999999999999998e+00F;
    float v553 = -8.6602540378443871e-01F;
    float v560 = 7.6604444311897801e-01F;
    float v565 = 9.3969262078590832e-01F;
    float v570 = -1.7364817766693039e-01F;
    float v575 = -6.4278760968653925e-01F;
    float v582 = 3.4202014332566888e-01F;
    float v589 = -9.8480775301220802e-01F;
    const float32x2_t *v860 = &v5[v0];
    int32_t *v985 = &v6[v2];
    int64_t v19 = v0 * 9;
    int64_t v34 = v10 * 8;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 11;
    int64_t v76 = v10 * 10;
    int64_t v82 = v0 * 4;
    int64_t v96 = v0 * 13;
    int64_t v111 = v10 * 3;
    int64_t v118 = v10 * 12;
    int64_t v124 = v0 * 6;
    int64_t v138 = v0 * 15;
    int64_t v153 = v10 * 5;
    int64_t v160 = v10 * 14;
    int64_t v166 = v0 * 8;
    int64_t v180 = v0 * 17;
    int64_t v195 = v10 * 7;
    int64_t v202 = v10 * 16;
    int64_t v208 = v0 * 10;
    int64_t v237 = v10 * 9;
    int64_t v250 = v0 * 12;
    int64_t v264 = v0 * 3;
    int64_t v279 = v10 * 11;
    int64_t v286 = v10 * 2;
    int64_t v292 = v0 * 14;
    int64_t v306 = v0 * 5;
    int64_t v321 = v10 * 13;
    int64_t v328 = v10 * 4;
    int64_t v334 = v0 * 16;
    int64_t v348 = v0 * 7;
    int64_t v363 = v10 * 15;
    int64_t v370 = v10 * 6;
    int64_t v371 = v13 * 17;
    float v556 = v4 * v553;
    float v578 = v4 * v575;
    float v585 = v4 * v582;
    float v592 = v4 * v589;
    int64_t v629 = v2 * 9;
    int64_t v637 = v2 * 10;
    int64_t v653 = v2 * 2;
    int64_t v661 = v2 * 11;
    int64_t v669 = v2 * 12;
    int64_t v677 = v2 * 3;
    int64_t v685 = v2 * 4;
    int64_t v693 = v2 * 13;
    int64_t v701 = v2 * 14;
    int64_t v709 = v2 * 5;
    int64_t v717 = v2 * 6;
    int64_t v725 = v2 * 15;
    int64_t v733 = v2 * 16;
    int64_t v741 = v2 * 7;
    int64_t v749 = v2 * 8;
    int64_t v757 = v2 * 17;
    const float32x2_t *v926 = &v5[0];
    svint64_t v927 = svindex_s64(0, v1);
    svfloat32_t v941 = svdup_n_f32(v536);
    svfloat32_t v943 = svdup_n_f32(v548);
    svfloat32_t v945 = svdup_n_f32(v560);
    svfloat32_t v946 = svdup_n_f32(v565);
    svfloat32_t v947 = svdup_n_f32(v570);
    int32_t *v958 = &v6[0];
    int64_t v36 = v34 + v371;
    int64_t v71 = v10 + v371;
    int64_t v78 = v76 + v371;
    int64_t v113 = v111 + v371;
    int64_t v120 = v118 + v371;
    int64_t v155 = v153 + v371;
    int64_t v162 = v160 + v371;
    int64_t v197 = v195 + v371;
    int64_t v204 = v202 + v371;
    int64_t v239 = v237 + v371;
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v371]));
    int64_t v281 = v279 + v371;
    int64_t v288 = v286 + v371;
    int64_t v323 = v321 + v371;
    int64_t v330 = v328 + v371;
    int64_t v365 = v363 + v371;
    int64_t v372 = v370 + v371;
    const float32x2_t *v770 = &v5[v19];
    const float32x2_t *v779 = &v5[v40];
    const float32x2_t *v788 = &v5[v54];
    const float32x2_t *v797 = &v5[v82];
    const float32x2_t *v806 = &v5[v96];
    const float32x2_t *v815 = &v5[v124];
    const float32x2_t *v824 = &v5[v138];
    const float32x2_t *v833 = &v5[v166];
    const float32x2_t *v842 = &v5[v180];
    const float32x2_t *v851 = &v5[v208];
    svfloat32_t v862 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v860), v927));
    const float32x2_t *v871 = &v5[v250];
    const float32x2_t *v880 = &v5[v264];
    const float32x2_t *v889 = &v5[v292];
    const float32x2_t *v898 = &v5[v306];
    const float32x2_t *v907 = &v5[v334];
    const float32x2_t *v916 = &v5[v348];
    svfloat32_t v928 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v926), v927));
    svfloat32_t v944 = svdup_n_f32(v556);
    svfloat32_t v948 = svdup_n_f32(v578);
    svfloat32_t v949 = svdup_n_f32(v585);
    svfloat32_t v950 = svdup_n_f32(v592);
    int32_t *v967 = &v6[v629];
    int32_t *v976 = &v6[v637];
    int32_t *v994 = &v6[v653];
    int32_t *v1003 = &v6[v661];
    int32_t *v1012 = &v6[v669];
    int32_t *v1021 = &v6[v677];
    int32_t *v1030 = &v6[v685];
    int32_t *v1039 = &v6[v693];
    int32_t *v1048 = &v6[v701];
    int32_t *v1057 = &v6[v709];
    int32_t *v1066 = &v6[v717];
    int32_t *v1075 = &v6[v725];
    int32_t *v1084 = &v6[v733];
    int32_t *v1093 = &v6[v741];
    int32_t *v1102 = &v6[v749];
    int32_t *v1111 = &v6[v757];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero248, v862, v247, 0),
                     v862, v247, 90);
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v288]));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v331 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v330]));
    svfloat32_t v366 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v365]));
    svfloat32_t v373 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v372]));
    svfloat32_t v772 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v770), v927));
    svfloat32_t v781 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v779), v927));
    svfloat32_t v790 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v788), v927));
    svfloat32_t v799 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v797), v927));
    svfloat32_t v808 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v806), v927));
    svfloat32_t v817 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v815), v927));
    svfloat32_t v826 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v824), v927));
    svfloat32_t v835 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v833), v927));
    svfloat32_t v844 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v842), v927));
    svfloat32_t v853 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v851), v927));
    svfloat32_t v873 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v871), v927));
    svfloat32_t v882 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v880), v927));
    svfloat32_t v891 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v889), v927));
    svfloat32_t v900 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v898), v927));
    svfloat32_t v909 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v907), v927));
    svfloat32_t v918 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v916), v927));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v772, v37, 0),
                     v772, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v781, v72, 0),
                     v781, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v790, v79, 0),
                     v790, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v799, v114, 0),
                     v799, v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v808, v121, 0),
                     v808, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v817, v156, 0),
                     v817, v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v826, v163, 0),
                     v826, v163, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v835, v198, 0),
                     v835, v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v844, v205, 0),
                     v844, v205, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero241, v853, v240, 0),
                     v853, v240, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero283, v873, v282, 0),
                     v873, v282, 90);
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero290, v882, v289, 0),
                     v882, v289, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero325, v891, v324, 0),
                     v891, v324, 90);
    svfloat32_t zero332 = svdup_n_f32(0);
    svfloat32_t v332 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero332, v900, v331, 0),
                     v900, v331, 90);
    svfloat32_t zero367 = svdup_n_f32(0);
    svfloat32_t v367 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero367, v909, v366, 0),
                     v909, v366, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero374, v918, v373, 0),
                     v918, v373, 90);
    svfloat32_t v382 = svadd_f32_x(svptrue_b32(), v928, v38);
    svfloat32_t v383 = svsub_f32_x(svptrue_b32(), v928, v38);
    svfloat32_t v384 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v385 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v386 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v387 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v388 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v389 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v392 = svadd_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v393 = svsub_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v395 = svsub_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v398 = svadd_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v399 = svsub_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v384, v398);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v384, v398);
    svfloat32_t v402 = svadd_f32_x(svptrue_b32(), v396, v386);
    svfloat32_t v403 = svsub_f32_x(svptrue_b32(), v396, v386);
    svfloat32_t v404 = svadd_f32_x(svptrue_b32(), v388, v394);
    svfloat32_t v405 = svsub_f32_x(svptrue_b32(), v388, v394);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v390, v392);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v390, v392);
    svfloat32_t v510 = svadd_f32_x(svptrue_b32(), v385, v399);
    svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v385, v399);
    svfloat32_t v512 = svadd_f32_x(svptrue_b32(), v397, v387);
    svfloat32_t v513 = svsub_f32_x(svptrue_b32(), v397, v387);
    svfloat32_t v514 = svadd_f32_x(svptrue_b32(), v389, v395);
    svfloat32_t v515 = svsub_f32_x(svptrue_b32(), v389, v395);
    svfloat32_t v516 = svadd_f32_x(svptrue_b32(), v391, v393);
    svfloat32_t v517 = svsub_f32_x(svptrue_b32(), v391, v393);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v400, v402);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v401, v403);
    svfloat32_t v414 = svsub_f32_x(svptrue_b32(), v400, v402);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v402, v406);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v406, v400);
    svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v401, v403);
    svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v403, v407);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v407, v401);
    svfloat32_t zero448 = svdup_n_f32(0);
    svfloat32_t v448 = svcmla_f32_x(pred_full, zero448, v944, v405, 90);
    svfloat32_t v518 = svadd_f32_x(svptrue_b32(), v510, v512);
    svfloat32_t v522 = svadd_f32_x(svptrue_b32(), v511, v513);
    svfloat32_t v524 = svsub_f32_x(svptrue_b32(), v510, v512);
    svfloat32_t v525 = svsub_f32_x(svptrue_b32(), v512, v516);
    svfloat32_t v526 = svsub_f32_x(svptrue_b32(), v516, v510);
    svfloat32_t v527 = svsub_f32_x(svptrue_b32(), v511, v513);
    svfloat32_t v528 = svsub_f32_x(svptrue_b32(), v513, v517);
    svfloat32_t v529 = svsub_f32_x(svptrue_b32(), v517, v511);
    svfloat32_t zero558 = svdup_n_f32(0);
    svfloat32_t v558 = svcmla_f32_x(pred_full, zero558, v944, v515, 90);
    svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v408, v406);
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v412, v407);
    svfloat32_t zero470 = svdup_n_f32(0);
    svfloat32_t v470 = svcmla_f32_x(pred_full, zero470, v948, v417, 90);
    svfloat32_t zero477 = svdup_n_f32(0);
    svfloat32_t v477 = svcmla_f32_x(pred_full, zero477, v949, v418, 90);
    svfloat32_t zero484 = svdup_n_f32(0);
    svfloat32_t v484 = svcmla_f32_x(pred_full, zero484, v950, v419, 90);
    svfloat32_t v519 = svadd_f32_x(svptrue_b32(), v518, v516);
    svfloat32_t v523 = svadd_f32_x(svptrue_b32(), v522, v517);
    svfloat32_t zero580 = svdup_n_f32(0);
    svfloat32_t v580 = svcmla_f32_x(pred_full, zero580, v948, v527, 90);
    svfloat32_t zero587 = svdup_n_f32(0);
    svfloat32_t v587 = svcmla_f32_x(pred_full, zero587, v949, v528, 90);
    svfloat32_t zero594 = svdup_n_f32(0);
    svfloat32_t v594 = svcmla_f32_x(pred_full, zero594, v950, v529, 90);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v409, v404);
    svfloat32_t v429 = svmul_f32_x(svptrue_b32(), v409, v941);
    svfloat32_t zero436 = svdup_n_f32(0);
    svfloat32_t v436 = svcmla_f32_x(pred_full, zero436, v944, v413, 90);
    svfloat32_t v498 = svadd_f32_x(svptrue_b32(), v448, v470);
    svfloat32_t v500 = svsub_f32_x(svptrue_b32(), v448, v477);
    svfloat32_t v502 = svsub_f32_x(svptrue_b32(), v448, v470);
    svfloat32_t v520 = svadd_f32_x(svptrue_b32(), v519, v514);
    svfloat32_t v539 = svmul_f32_x(svptrue_b32(), v519, v941);
    svfloat32_t zero546 = svdup_n_f32(0);
    svfloat32_t v546 = svcmla_f32_x(pred_full, zero546, v944, v523, 90);
    svfloat32_t v608 = svadd_f32_x(svptrue_b32(), v558, v580);
    svfloat32_t v610 = svsub_f32_x(svptrue_b32(), v558, v587);
    svfloat32_t v612 = svsub_f32_x(svptrue_b32(), v558, v580);
    svfloat32_t v411 = svadd_f32_x(svptrue_b32(), v410, v382);
    svfloat32_t v485 = svadd_f32_x(svptrue_b32(), v429, v429);
    svfloat32_t v499 = svadd_f32_x(svptrue_b32(), v498, v477);
    svfloat32_t v501 = svadd_f32_x(svptrue_b32(), v500, v484);
    svfloat32_t v503 = svsub_f32_x(svptrue_b32(), v502, v484);
    svfloat32_t v521 = svadd_f32_x(svptrue_b32(), v520, v383);
    svfloat32_t v595 = svadd_f32_x(svptrue_b32(), v539, v539);
    svfloat32_t v609 = svadd_f32_x(svptrue_b32(), v608, v587);
    svfloat32_t v611 = svadd_f32_x(svptrue_b32(), v610, v594);
    svfloat32_t v613 = svsub_f32_x(svptrue_b32(), v612, v594);
    svfloat32_t v486 = svmla_f32_x(pred_full, v485, v409, v941);
    svfloat32_t v490 = svmla_f32_x(pred_full, v411, v404, v943);
    svfloat32_t v596 = svmla_f32_x(pred_full, v595, v519, v941);
    svfloat32_t v600 = svmla_f32_x(pred_full, v521, v514, v943);
    svint16_t v622 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v411, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v630 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v521, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v487 = svadd_f32_x(svptrue_b32(), v411, v486);
    svfloat32_t v491 = svadd_f32_x(svptrue_b32(), v490, v485);
    svfloat32_t v597 = svadd_f32_x(svptrue_b32(), v521, v596);
    svfloat32_t v601 = svadd_f32_x(svptrue_b32(), v600, v595);
    svst1w_u64(pred_full, (unsigned *)(v958), svreinterpret_u64_s16(v622));
    svst1w_u64(pred_full, (unsigned *)(v967), svreinterpret_u64_s16(v630));
    svfloat32_t v488 = svadd_f32_x(svptrue_b32(), v487, v436);
    svfloat32_t v489 = svsub_f32_x(svptrue_b32(), v487, v436);
    svfloat32_t v492 = svmla_f32_x(pred_full, v491, v414, v945);
    svfloat32_t v494 = svmls_f32_x(pred_full, v491, v415, v946);
    svfloat32_t v496 = svmls_f32_x(pred_full, v491, v414, v945);
    svfloat32_t v598 = svadd_f32_x(svptrue_b32(), v597, v546);
    svfloat32_t v599 = svsub_f32_x(svptrue_b32(), v597, v546);
    svfloat32_t v602 = svmla_f32_x(pred_full, v601, v524, v945);
    svfloat32_t v604 = svmls_f32_x(pred_full, v601, v525, v946);
    svfloat32_t v606 = svmls_f32_x(pred_full, v601, v524, v945);
    svfloat32_t v493 = svmla_f32_x(pred_full, v492, v415, v946);
    svfloat32_t v495 = svmla_f32_x(pred_full, v494, v416, v947);
    svfloat32_t v497 = svmls_f32_x(pred_full, v496, v416, v947);
    svfloat32_t v603 = svmla_f32_x(pred_full, v602, v525, v946);
    svfloat32_t v605 = svmla_f32_x(pred_full, v604, v526, v947);
    svfloat32_t v607 = svmls_f32_x(pred_full, v606, v526, v947);
    svint16_t v670 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v489, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v678 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v599, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v718 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v488, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v726 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v598, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v504 = svadd_f32_x(svptrue_b32(), v493, v499);
    svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v493, v499);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v495, v501);
    svfloat32_t v507 = svsub_f32_x(svptrue_b32(), v495, v501);
    svfloat32_t v508 = svadd_f32_x(svptrue_b32(), v497, v503);
    svfloat32_t v509 = svsub_f32_x(svptrue_b32(), v497, v503);
    svfloat32_t v614 = svadd_f32_x(svptrue_b32(), v603, v609);
    svfloat32_t v615 = svsub_f32_x(svptrue_b32(), v603, v609);
    svfloat32_t v616 = svadd_f32_x(svptrue_b32(), v605, v611);
    svfloat32_t v617 = svsub_f32_x(svptrue_b32(), v605, v611);
    svfloat32_t v618 = svadd_f32_x(svptrue_b32(), v607, v613);
    svfloat32_t v619 = svsub_f32_x(svptrue_b32(), v607, v613);
    svst1w_u64(pred_full, (unsigned *)(v1012), svreinterpret_u64_s16(v670));
    svst1w_u64(pred_full, (unsigned *)(v1021), svreinterpret_u64_s16(v678));
    svst1w_u64(pred_full, (unsigned *)(v1066), svreinterpret_u64_s16(v718));
    svst1w_u64(pred_full, (unsigned *)(v1075), svreinterpret_u64_s16(v726));
    svint16_t v638 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v505, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v646 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v615, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v654 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v506, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v662 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v616, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v686 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v509, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v694 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v619, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v702 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v508, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v710 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v618, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v734 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v507, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v742 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v617, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v750 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v504, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v758 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v614, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v976), svreinterpret_u64_s16(v638));
    svst1w_u64(pred_full, (unsigned *)(v985), svreinterpret_u64_s16(v646));
    svst1w_u64(pred_full, (unsigned *)(v994), svreinterpret_u64_s16(v654));
    svst1w_u64(pred_full, (unsigned *)(v1003), svreinterpret_u64_s16(v662));
    svst1w_u64(pred_full, (unsigned *)(v1030), svreinterpret_u64_s16(v686));
    svst1w_u64(pred_full, (unsigned *)(v1039), svreinterpret_u64_s16(v694));
    svst1w_u64(pred_full, (unsigned *)(v1048), svreinterpret_u64_s16(v702));
    svst1w_u64(pred_full, (unsigned *)(v1057), svreinterpret_u64_s16(v710));
    svst1w_u64(pred_full, (unsigned *)(v1084), svreinterpret_u64_s16(v734));
    svst1w_u64(pred_full, (unsigned *)(v1093), svreinterpret_u64_s16(v742));
    svst1w_u64(pred_full, (unsigned *)(v1102), svreinterpret_u64_s16(v750));
    svst1w_u64(pred_full, (unsigned *)(v1111), svreinterpret_u64_s16(v758));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu19(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v667 = -1.0555555555555556e+00F;
    float v671 = 1.7752228513927079e-01F;
    float v675 = -1.2820077502191529e-01F;
    float v679 = 4.9321510117355499e-02F;
    float v683 = 5.7611011491005903e-01F;
    float v687 = -7.4996449655536279e-01F;
    float v691 = -1.7385438164530381e-01F;
    float v695 = -2.1729997561977314e+00F;
    float v699 = -1.7021211726914738e+00F;
    float v703 = 4.7087858350625778e-01F;
    float v707 = -2.0239400846888440e+00F;
    float v711 = 1.0551641201664090e-01F;
    float v715 = 2.1294564967054850e+00F;
    float v719 = -7.5087543897371167e-01F;
    float v723 = 1.4812817695157160e-01F;
    float v727 = 8.9900361592528333e-01F;
    float v731 = -6.2148246772602778e-01F;
    float v735 = -7.9869352098712687e-01F;
    float v739 = -4.7339199623771833e-01F;
    float v742 = -2.4216105241892630e-01F;
    float v743 = 2.4216105241892630e-01F;
    float v749 = -5.9368607967505101e-02F;
    float v750 = 5.9368607967505101e-02F;
    float v756 = 1.2578688255176201e-02F;
    float v757 = -1.2578688255176201e-02F;
    float v763 = -4.6789919712328903e-02F;
    float v764 = 4.6789919712328903e-02F;
    float v770 = -9.3750121913782358e-01F;
    float v771 = 9.3750121913782358e-01F;
    float v777 = -5.0111537043352902e-02F;
    float v778 = 5.0111537043352902e-02F;
    float v784 = -9.8761275618117661e-01F;
    float v785 = 9.8761275618117661e-01F;
    float v791 = -1.1745786501205959e+00F;
    float v792 = 1.1745786501205959e+00F;
    float v798 = 1.1114482296234993e+00F;
    float v799 = -1.1114482296234993e+00F;
    float v805 = 2.2860268797440955e+00F;
    float v806 = -2.2860268797440955e+00F;
    float v812 = 2.6420523257930939e-01F;
    float v813 = -2.6420523257930939e-01F;
    float v819 = 2.1981792779352136e+00F;
    float v820 = -2.1981792779352136e+00F;
    float v826 = 1.9339740453559042e+00F;
    float v827 = -1.9339740453559042e+00F;
    float v833 = -7.4825847091254893e-01F;
    float v834 = 7.4825847091254893e-01F;
    float v840 = -4.7820835642768872e-01F;
    float v841 = 4.7820835642768872e-01F;
    float v847 = 2.7005011448486022e-01F;
    float v848 = -2.7005011448486022e-01F;
    float v854 = -3.4642356159542270e-01F;
    float v855 = 3.4642356159542270e-01F;
    float v861 = -8.3485429360688279e-01F;
    float v862 = 8.3485429360688279e-01F;
    float v868 = -3.9375928506743518e-01F;
    float v869 = 3.9375928506743518e-01F;
    float32x2_t v871 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v612 = v5[0];
    float32x2_t v668 = (float32x2_t){v667, v667};
    float32x2_t v672 = (float32x2_t){v671, v671};
    float32x2_t v676 = (float32x2_t){v675, v675};
    float32x2_t v680 = (float32x2_t){v679, v679};
    float32x2_t v684 = (float32x2_t){v683, v683};
    float32x2_t v688 = (float32x2_t){v687, v687};
    float32x2_t v692 = (float32x2_t){v691, v691};
    float32x2_t v696 = (float32x2_t){v695, v695};
    float32x2_t v700 = (float32x2_t){v699, v699};
    float32x2_t v704 = (float32x2_t){v703, v703};
    float32x2_t v708 = (float32x2_t){v707, v707};
    float32x2_t v712 = (float32x2_t){v711, v711};
    float32x2_t v716 = (float32x2_t){v715, v715};
    float32x2_t v720 = (float32x2_t){v719, v719};
    float32x2_t v724 = (float32x2_t){v723, v723};
    float32x2_t v728 = (float32x2_t){v727, v727};
    float32x2_t v732 = (float32x2_t){v731, v731};
    float32x2_t v736 = (float32x2_t){v735, v735};
    float32x2_t v740 = (float32x2_t){v739, v739};
    float32x2_t v744 = (float32x2_t){v742, v743};
    float32x2_t v751 = (float32x2_t){v749, v750};
    float32x2_t v758 = (float32x2_t){v756, v757};
    float32x2_t v765 = (float32x2_t){v763, v764};
    float32x2_t v772 = (float32x2_t){v770, v771};
    float32x2_t v779 = (float32x2_t){v777, v778};
    float32x2_t v786 = (float32x2_t){v784, v785};
    float32x2_t v793 = (float32x2_t){v791, v792};
    float32x2_t v800 = (float32x2_t){v798, v799};
    float32x2_t v807 = (float32x2_t){v805, v806};
    float32x2_t v814 = (float32x2_t){v812, v813};
    float32x2_t v821 = (float32x2_t){v819, v820};
    float32x2_t v828 = (float32x2_t){v826, v827};
    float32x2_t v835 = (float32x2_t){v833, v834};
    float32x2_t v842 = (float32x2_t){v840, v841};
    float32x2_t v849 = (float32x2_t){v847, v848};
    float32x2_t v856 = (float32x2_t){v854, v855};
    float32x2_t v863 = (float32x2_t){v861, v862};
    float32x2_t v870 = (float32x2_t){v868, v869};
    float32x2_t v38 = v5[istride * 18];
    float32x2_t v56 = v7[j * 36];
    int64_t v60 = j * 36 + 1;
    int64_t v68 = 34 + j * 36;
    float32x2_t v82 = v5[istride * 2];
    float32x2_t v100 = v5[istride * 17];
    int64_t v117 = 32 + j * 36;
    int64_t v130 = 2 + j * 36;
    float32x2_t v144 = v5[istride * 4];
    float32x2_t v162 = v5[istride * 15];
    int64_t v179 = 6 + j * 36;
    int64_t v192 = 28 + j * 36;
    float32x2_t v206 = v5[istride * 8];
    float32x2_t v224 = v5[istride * 11];
    int64_t v241 = 20 + j * 36;
    int64_t v254 = 14 + j * 36;
    float32x2_t v268 = v5[istride * 16];
    float32x2_t v286 = v5[istride * 3];
    int64_t v303 = 30 + j * 36;
    int64_t v316 = 4 + j * 36;
    float32x2_t v330 = v5[istride * 13];
    float32x2_t v348 = v5[istride * 6];
    int64_t v365 = 10 + j * 36;
    int64_t v378 = 24 + j * 36;
    float32x2_t v392 = v5[istride * 7];
    float32x2_t v410 = v5[istride * 12];
    int64_t v427 = 12 + j * 36;
    int64_t v440 = 22 + j * 36;
    float32x2_t v454 = v5[istride * 14];
    float32x2_t v472 = v5[istride * 5];
    int64_t v489 = 8 + j * 36;
    int64_t v502 = 26 + j * 36;
    float32x2_t v516 = v5[istride * 9];
    float32x2_t v534 = v5[istride * 10];
    int64_t v551 = 16 + j * 36;
    int64_t v564 = 18 + j * 36;
    float32x2_t v746 = vmul_f32(v871, v744);
    float32x2_t v753 = vmul_f32(v871, v751);
    float32x2_t v760 = vmul_f32(v871, v758);
    float32x2_t v767 = vmul_f32(v871, v765);
    float32x2_t v774 = vmul_f32(v871, v772);
    float32x2_t v781 = vmul_f32(v871, v779);
    float32x2_t v788 = vmul_f32(v871, v786);
    float32x2_t v795 = vmul_f32(v871, v793);
    float32x2_t v802 = vmul_f32(v871, v800);
    float32x2_t v809 = vmul_f32(v871, v807);
    float32x2_t v816 = vmul_f32(v871, v814);
    float32x2_t v823 = vmul_f32(v871, v821);
    float32x2_t v830 = vmul_f32(v871, v828);
    float32x2_t v837 = vmul_f32(v871, v835);
    float32x2_t v844 = vmul_f32(v871, v842);
    float32x2_t v851 = vmul_f32(v871, v849);
    float32x2_t v858 = vmul_f32(v871, v856);
    float32x2_t v865 = vmul_f32(v871, v863);
    float32x2_t v872 = vmul_f32(v871, v870);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v100, v100);
    float32x2_t v120 = vtrn2_f32(v100, v100);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v82, v82);
    float32x2_t v133 = vtrn2_f32(v82, v82);
    int64_t v135 = v130 + 1;
    float32x2_t v180 = v7[v179];
    float32x2_t v181 = vtrn1_f32(v144, v144);
    float32x2_t v182 = vtrn2_f32(v144, v144);
    int64_t v184 = v179 + 1;
    float32x2_t v193 = v7[v192];
    float32x2_t v194 = vtrn1_f32(v162, v162);
    float32x2_t v195 = vtrn2_f32(v162, v162);
    int64_t v197 = v192 + 1;
    float32x2_t v242 = v7[v241];
    float32x2_t v243 = vtrn1_f32(v224, v224);
    float32x2_t v244 = vtrn2_f32(v224, v224);
    int64_t v246 = v241 + 1;
    float32x2_t v255 = v7[v254];
    float32x2_t v256 = vtrn1_f32(v206, v206);
    float32x2_t v257 = vtrn2_f32(v206, v206);
    int64_t v259 = v254 + 1;
    float32x2_t v304 = v7[v303];
    float32x2_t v305 = vtrn1_f32(v268, v268);
    float32x2_t v306 = vtrn2_f32(v268, v268);
    int64_t v308 = v303 + 1;
    float32x2_t v317 = v7[v316];
    float32x2_t v318 = vtrn1_f32(v286, v286);
    float32x2_t v319 = vtrn2_f32(v286, v286);
    int64_t v321 = v316 + 1;
    float32x2_t v366 = v7[v365];
    float32x2_t v367 = vtrn1_f32(v348, v348);
    float32x2_t v368 = vtrn2_f32(v348, v348);
    int64_t v370 = v365 + 1;
    float32x2_t v379 = v7[v378];
    float32x2_t v380 = vtrn1_f32(v330, v330);
    float32x2_t v381 = vtrn2_f32(v330, v330);
    int64_t v383 = v378 + 1;
    float32x2_t v428 = v7[v427];
    float32x2_t v429 = vtrn1_f32(v392, v392);
    float32x2_t v430 = vtrn2_f32(v392, v392);
    int64_t v432 = v427 + 1;
    float32x2_t v441 = v7[v440];
    float32x2_t v442 = vtrn1_f32(v410, v410);
    float32x2_t v443 = vtrn2_f32(v410, v410);
    int64_t v445 = v440 + 1;
    float32x2_t v490 = v7[v489];
    float32x2_t v491 = vtrn1_f32(v472, v472);
    float32x2_t v492 = vtrn2_f32(v472, v472);
    int64_t v494 = v489 + 1;
    float32x2_t v503 = v7[v502];
    float32x2_t v504 = vtrn1_f32(v454, v454);
    float32x2_t v505 = vtrn2_f32(v454, v454);
    int64_t v507 = v502 + 1;
    float32x2_t v552 = v7[v551];
    float32x2_t v553 = vtrn1_f32(v516, v516);
    float32x2_t v554 = vtrn2_f32(v516, v516);
    int64_t v556 = v551 + 1;
    float32x2_t v565 = v7[v564];
    float32x2_t v566 = vtrn1_f32(v534, v534);
    float32x2_t v567 = vtrn2_f32(v534, v534);
    int64_t v569 = v564 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v185 = v7[v184];
    float32x2_t v186 = vmul_f32(v181, v180);
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vmul_f32(v194, v193);
    float32x2_t v247 = v7[v246];
    float32x2_t v248 = vmul_f32(v243, v242);
    float32x2_t v260 = v7[v259];
    float32x2_t v261 = vmul_f32(v256, v255);
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vmul_f32(v305, v304);
    float32x2_t v322 = v7[v321];
    float32x2_t v323 = vmul_f32(v318, v317);
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vmul_f32(v367, v366);
    float32x2_t v384 = v7[v383];
    float32x2_t v385 = vmul_f32(v380, v379);
    float32x2_t v433 = v7[v432];
    float32x2_t v434 = vmul_f32(v429, v428);
    float32x2_t v446 = v7[v445];
    float32x2_t v447 = vmul_f32(v442, v441);
    float32x2_t v495 = v7[v494];
    float32x2_t v496 = vmul_f32(v491, v490);
    float32x2_t v508 = v7[v507];
    float32x2_t v509 = vmul_f32(v504, v503);
    float32x2_t v557 = v7[v556];
    float32x2_t v558 = vmul_f32(v553, v552);
    float32x2_t v570 = v7[v569];
    float32x2_t v571 = vmul_f32(v566, v565);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v188 = vfma_f32(v186, v182, v185);
    float32x2_t v201 = vfma_f32(v199, v195, v198);
    float32x2_t v250 = vfma_f32(v248, v244, v247);
    float32x2_t v263 = vfma_f32(v261, v257, v260);
    float32x2_t v312 = vfma_f32(v310, v306, v309);
    float32x2_t v325 = vfma_f32(v323, v319, v322);
    float32x2_t v374 = vfma_f32(v372, v368, v371);
    float32x2_t v387 = vfma_f32(v385, v381, v384);
    float32x2_t v436 = vfma_f32(v434, v430, v433);
    float32x2_t v449 = vfma_f32(v447, v443, v446);
    float32x2_t v498 = vfma_f32(v496, v492, v495);
    float32x2_t v511 = vfma_f32(v509, v505, v508);
    float32x2_t v560 = vfma_f32(v558, v554, v557);
    float32x2_t v573 = vfma_f32(v571, v567, v570);
    float32x2_t v574 = vadd_f32(v64, v77);
    float32x2_t v575 = vsub_f32(v64, v77);
    float32x2_t v576 = vadd_f32(v139, v126);
    float32x2_t v577 = vsub_f32(v126, v139);
    float32x2_t v578 = vadd_f32(v188, v201);
    float32x2_t v579 = vsub_f32(v188, v201);
    float32x2_t v580 = vadd_f32(v263, v250);
    float32x2_t v581 = vsub_f32(v250, v263);
    float32x2_t v582 = vadd_f32(v312, v325);
    float32x2_t v583 = vsub_f32(v312, v325);
    float32x2_t v584 = vadd_f32(v387, v374);
    float32x2_t v585 = vsub_f32(v374, v387);
    float32x2_t v586 = vadd_f32(v436, v449);
    float32x2_t v587 = vsub_f32(v436, v449);
    float32x2_t v588 = vadd_f32(v511, v498);
    float32x2_t v589 = vsub_f32(v498, v511);
    float32x2_t v590 = vadd_f32(v560, v573);
    float32x2_t v591 = vsub_f32(v560, v573);
    float32x2_t v592 = vsub_f32(v574, v586);
    float32x2_t v593 = vsub_f32(v576, v588);
    float32x2_t v594 = vsub_f32(v578, v590);
    float32x2_t v595 = vsub_f32(v580, v586);
    float32x2_t v596 = vsub_f32(v582, v588);
    float32x2_t v597 = vsub_f32(v584, v590);
    float32x2_t v598 = vadd_f32(v574, v580);
    float32x2_t v600 = vadd_f32(v576, v582);
    float32x2_t v602 = vadd_f32(v578, v584);
    float32x2_t v630 = vsub_f32(v575, v587);
    float32x2_t v631 = vsub_f32(v577, v589);
    float32x2_t v632 = vsub_f32(v579, v591);
    float32x2_t v633 = vsub_f32(v581, v587);
    float32x2_t v634 = vsub_f32(v583, v589);
    float32x2_t v635 = vsub_f32(v585, v591);
    float32x2_t v636 = vadd_f32(v575, v581);
    float32x2_t v638 = vadd_f32(v577, v583);
    float32x2_t v640 = vadd_f32(v579, v585);
    float32x2_t v599 = vadd_f32(v598, v586);
    float32x2_t v601 = vadd_f32(v600, v588);
    float32x2_t v603 = vadd_f32(v602, v590);
    float32x2_t v604 = vadd_f32(v592, v594);
    float32x2_t v605 = vadd_f32(v595, v597);
    float32x2_t v620 = vsub_f32(v592, v595);
    float32x2_t v621 = vsub_f32(v594, v597);
    float32x2_t v637 = vadd_f32(v636, v587);
    float32x2_t v639 = vadd_f32(v638, v589);
    float32x2_t v641 = vadd_f32(v640, v591);
    float32x2_t v642 = vadd_f32(v630, v632);
    float32x2_t v643 = vadd_f32(v633, v635);
    float32x2_t v652 = vsub_f32(v630, v633);
    float32x2_t v653 = vsub_f32(v632, v635);
    float32x2_t v697 = vmul_f32(v595, v696);
    float32x2_t v709 = vmul_f32(v597, v708);
    float32x2_t v717 = vmul_f32(v594, v716);
    float32x2_t v796 = vrev64_f32(v633);
    float32x2_t v810 = vrev64_f32(v630);
    float32x2_t v817 = vrev64_f32(v635);
    float32x2_t v831 = vrev64_f32(v632);
    float32x2_t v606 = vadd_f32(v599, v601);
    float32x2_t v614 = vadd_f32(v605, v596);
    float32x2_t v615 = vadd_f32(v604, v593);
    float32x2_t v617 = vsub_f32(v605, v596);
    float32x2_t v618 = vsub_f32(v604, v593);
    float32x2_t v622 = vsub_f32(v592, v621);
    float32x2_t v624 = vadd_f32(v620, v597);
    float32x2_t v627 = vsub_f32(v599, v603);
    float32x2_t v628 = vsub_f32(v601, v603);
    float32x2_t v644 = vadd_f32(v637, v639);
    float32x2_t v646 = vadd_f32(v643, v634);
    float32x2_t v647 = vadd_f32(v642, v631);
    float32x2_t v649 = vsub_f32(v643, v634);
    float32x2_t v650 = vsub_f32(v642, v631);
    float32x2_t v654 = vsub_f32(v630, v653);
    float32x2_t v656 = vadd_f32(v652, v635);
    float32x2_t v659 = vsub_f32(v637, v641);
    float32x2_t v660 = vsub_f32(v639, v641);
    float32x2_t v701 = vmul_f32(v620, v700);
    float32x2_t v713 = vmul_f32(v621, v712);
    float32x2_t v797 = vmul_f32(v796, v795);
    float32x2_t v803 = vrev64_f32(v652);
    float32x2_t v818 = vmul_f32(v817, v816);
    float32x2_t v824 = vrev64_f32(v653);
    float32x2_t v832 = vmul_f32(v831, v830);
    float32x2_t v607 = vadd_f32(v606, v603);
    float32x2_t v616 = vsub_f32(v615, v614);
    float32x2_t v619 = vsub_f32(v618, v617);
    float32x2_t v623 = vsub_f32(v622, v596);
    float32x2_t v625 = vsub_f32(v624, v593);
    float32x2_t v629 = vadd_f32(v627, v628);
    float32x2_t v645 = vadd_f32(v644, v641);
    float32x2_t v648 = vsub_f32(v647, v646);
    float32x2_t v651 = vsub_f32(v650, v649);
    float32x2_t v655 = vsub_f32(v654, v634);
    float32x2_t v657 = vsub_f32(v656, v631);
    float32x2_t v661 = vadd_f32(v659, v660);
    float32x2_t v673 = vmul_f32(v614, v672);
    float32x2_t v677 = vmul_f32(v615, v676);
    float32x2_t v685 = vmul_f32(v617, v684);
    float32x2_t v689 = vmul_f32(v618, v688);
    float32x2_t v733 = vmul_f32(v627, v732);
    float32x2_t v737 = vmul_f32(v628, v736);
    float32x2_t v754 = vrev64_f32(v646);
    float32x2_t v761 = vrev64_f32(v647);
    float32x2_t v775 = vrev64_f32(v649);
    float32x2_t v782 = vrev64_f32(v650);
    float32x2_t v804 = vmul_f32(v803, v802);
    float32x2_t v825 = vmul_f32(v824, v823);
    float32x2_t v859 = vrev64_f32(v659);
    float32x2_t v866 = vrev64_f32(v660);
    float32x2_t v613 = vadd_f32(v612, v607);
    float32x2_t v626 = vsub_f32(v623, v625);
    float32x2_t v658 = vsub_f32(v655, v657);
    float32x2_t v669 = vmul_f32(v607, v668);
    float32x2_t v681 = vmul_f32(v616, v680);
    float32x2_t v693 = vmul_f32(v619, v692);
    float32x2_t v721 = vmul_f32(v623, v720);
    float32x2_t v725 = vmul_f32(v625, v724);
    float32x2_t v741 = vmul_f32(v629, v740);
    float32x2_t v747 = vrev64_f32(v645);
    float32x2_t v755 = vmul_f32(v754, v753);
    float32x2_t v762 = vmul_f32(v761, v760);
    float32x2_t v768 = vrev64_f32(v648);
    float32x2_t v776 = vmul_f32(v775, v774);
    float32x2_t v783 = vmul_f32(v782, v781);
    float32x2_t v789 = vrev64_f32(v651);
    float32x2_t v838 = vrev64_f32(v655);
    float32x2_t v845 = vrev64_f32(v657);
    float32x2_t v860 = vmul_f32(v859, v858);
    float32x2_t v867 = vmul_f32(v866, v865);
    float32x2_t v873 = vrev64_f32(v661);
    float32x2_t v875 = vadd_f32(v673, v677);
    float32x2_t v876 = vadd_f32(v685, v689);
    float32x2_t v729 = vmul_f32(v626, v728);
    float32x2_t v748 = vmul_f32(v747, v746);
    float32x2_t v769 = vmul_f32(v768, v767);
    float32x2_t v790 = vmul_f32(v789, v788);
    float32x2_t v839 = vmul_f32(v838, v837);
    float32x2_t v846 = vmul_f32(v845, v844);
    float32x2_t v852 = vrev64_f32(v658);
    float32x2_t v874 = vmul_f32(v873, v872);
    float32x2_t v878 = vadd_f32(v875, v876);
    float32x2_t v879 = vadd_f32(v673, v681);
    float32x2_t v880 = vadd_f32(v685, v693);
    float32x2_t v897 = vsub_f32(v875, v876);
    float32x2_t v899 = vsub_f32(v733, v741);
    float32x2_t v900 = vsub_f32(v737, v741);
    float32x2_t v901 = vadd_f32(v669, v613);
    float32x2_t v906 = vadd_f32(v755, v762);
    float32x2_t v907 = vadd_f32(v776, v783);
    int16x4_t v962 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v613, 15), (int32x2_t){0, 0}));
    float32x2_t v853 = vmul_f32(v852, v851);
    float32x2_t v877 = vadd_f32(v725, v729);
    float32x2_t v881 = vadd_f32(v721, v729);
    float32x2_t v882 = vsub_f32(v697, v878);
    float32x2_t v883 = vadd_f32(v879, v880);
    float32x2_t v889 = vsub_f32(v879, v880);
    float32x2_t v894 = vadd_f32(v878, v717);
    float32x2_t v902 = vadd_f32(v901, v899);
    float32x2_t v903 = vsub_f32(v901, v899);
    float32x2_t v905 = vadd_f32(v901, v900);
    float32x2_t v909 = vadd_f32(v906, v907);
    float32x2_t v910 = vadd_f32(v755, v769);
    float32x2_t v911 = vadd_f32(v776, v790);
    float32x2_t v928 = vsub_f32(v906, v907);
    float32x2_t v930 = vsub_f32(v860, v874);
    float32x2_t v931 = vsub_f32(v867, v874);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v962), 0);
    float32x2_t v884 = vsub_f32(v709, v881);
    float32x2_t v885 = vadd_f32(v701, v877);
    float32x2_t v887 = vadd_f32(v883, v713);
    float32x2_t v890 = vadd_f32(v889, v877);
    float32x2_t v891 = vadd_f32(v882, v883);
    float32x2_t v898 = vadd_f32(v897, v881);
    float32x2_t v904 = vsub_f32(v903, v900);
    float32x2_t v908 = vadd_f32(v846, v853);
    float32x2_t v912 = vadd_f32(v839, v853);
    float32x2_t v913 = vsub_f32(v797, v909);
    float32x2_t v914 = vadd_f32(v910, v911);
    float32x2_t v920 = vsub_f32(v910, v911);
    float32x2_t v925 = vadd_f32(v909, v832);
    float32x2_t v932 = vadd_f32(v748, v930);
    float32x2_t v933 = vsub_f32(v748, v930);
    float32x2_t v935 = vadd_f32(v748, v931);
    float32x2_t v886 = vadd_f32(v885, v882);
    float32x2_t v888 = vadd_f32(v887, v884);
    float32x2_t v892 = vfma_f32(v891, v592, v704);
    float32x2_t v895 = vadd_f32(v894, v884);
    float32x2_t v915 = vsub_f32(v818, v912);
    float32x2_t v916 = vadd_f32(v804, v908);
    float32x2_t v918 = vadd_f32(v914, v825);
    float32x2_t v921 = vadd_f32(v920, v908);
    float32x2_t v922 = vadd_f32(v913, v914);
    float32x2_t v929 = vadd_f32(v928, v912);
    float32x2_t v934 = vsub_f32(v933, v931);
    float32x2_t v940 = vsub_f32(v898, v890);
    float32x2_t v944 = vsub_f32(v905, v898);
    float32x2_t v947 = vadd_f32(v890, v905);
    float32x2_t v893 = vadd_f32(v892, v881);
    float32x2_t v896 = vadd_f32(v895, v877);
    float32x2_t v917 = vadd_f32(v916, v913);
    float32x2_t v919 = vadd_f32(v918, v915);
    float32x2_t v923 = vfma_f32(v922, v810, v809);
    float32x2_t v926 = vadd_f32(v925, v915);
    float32x2_t v941 = vadd_f32(v940, v905);
    float32x2_t v945 = vadd_f32(v886, v902);
    float32x2_t v946 = vadd_f32(v888, v904);
    float32x2_t v952 = vsub_f32(v929, v921);
    float32x2_t v956 = vsub_f32(v929, v935);
    float32x2_t v959 = vadd_f32(v921, v935);
    float32x2_t v924 = vadd_f32(v923, v912);
    float32x2_t v927 = vadd_f32(v926, v908);
    float32x2_t v936 = vsub_f32(v893, v886);
    float32x2_t v938 = vsub_f32(v896, v888);
    float32x2_t v942 = vsub_f32(v902, v893);
    float32x2_t v943 = vsub_f32(v904, v896);
    float32x2_t v953 = vadd_f32(v952, v935);
    float32x2_t v957 = vadd_f32(v917, v932);
    float32x2_t v958 = vadd_f32(v919, v934);
    float32x2_t v980 = vsub_f32(v947, v959);
    float32x2_t v987 = vadd_f32(v947, v959);
    float32x2_t v994 = vadd_f32(v944, v956);
    float32x2_t v1001 = vsub_f32(v944, v956);
    float32x2_t v937 = vadd_f32(v936, v902);
    float32x2_t v939 = vadd_f32(v938, v904);
    float32x2_t v948 = vsub_f32(v924, v917);
    float32x2_t v950 = vsub_f32(v927, v919);
    float32x2_t v954 = vsub_f32(v932, v924);
    float32x2_t v955 = vsub_f32(v934, v927);
    int16x4_t v983 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v980, 15), (int32x2_t){0, 0}));
    int16x4_t v990 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v987, 15), (int32x2_t){0, 0}));
    int16x4_t v997 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v994, 15), (int32x2_t){0, 0}));
    int16x4_t v1004 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1001, 15), (int32x2_t){0, 0}));
    float32x2_t v1008 = vadd_f32(v946, v958);
    float32x2_t v1015 = vsub_f32(v946, v958);
    float32x2_t v1022 = vadd_f32(v941, v953);
    float32x2_t v1029 = vsub_f32(v941, v953);
    float32x2_t v1064 = vsub_f32(v945, v957);
    float32x2_t v1071 = vadd_f32(v945, v957);
    float32x2_t v949 = vadd_f32(v948, v932);
    float32x2_t v951 = vadd_f32(v950, v934);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v983), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v990), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v997), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v1004), 0);
    int16x4_t v1011 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1008, 15), (int32x2_t){0, 0}));
    int16x4_t v1018 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1015, 15), (int32x2_t){0, 0}));
    int16x4_t v1025 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1022, 15), (int32x2_t){0, 0}));
    int16x4_t v1032 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1029, 15), (int32x2_t){0, 0}));
    float32x2_t v1036 = vadd_f32(v943, v955);
    float32x2_t v1043 = vsub_f32(v943, v955);
    float32x2_t v1050 = vadd_f32(v942, v954);
    float32x2_t v1057 = vsub_f32(v942, v954);
    int16x4_t v1067 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1064, 15), (int32x2_t){0, 0}));
    int16x4_t v1074 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1071, 15), (int32x2_t){0, 0}));
    float32x2_t v966 = vadd_f32(v937, v949);
    float32x2_t v973 = vsub_f32(v937, v949);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v1011), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v1018), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v1025), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v1032), 0);
    int16x4_t v1039 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1036, 15), (int32x2_t){0, 0}));
    int16x4_t v1046 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1043, 15), (int32x2_t){0, 0}));
    int16x4_t v1053 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1050, 15), (int32x2_t){0, 0}));
    int16x4_t v1060 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1057, 15), (int32x2_t){0, 0}));
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v1067), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v1074), 0);
    float32x2_t v1078 = vadd_f32(v939, v951);
    float32x2_t v1085 = vsub_f32(v939, v951);
    int16x4_t v969 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v966, 15), (int32x2_t){0, 0}));
    int16x4_t v976 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v973, 15), (int32x2_t){0, 0}));
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v1039), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v1046), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v1053), 0);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v1060), 0);
    int16x4_t v1081 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1078, 15), (int32x2_t){0, 0}));
    int16x4_t v1088 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1085, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v969), 0);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v976), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v1081), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v1088), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu19(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v492 = -1.0555555555555556e+00F;
    float v497 = 1.7752228513927079e-01F;
    float v502 = -1.2820077502191529e-01F;
    float v507 = 4.9321510117355499e-02F;
    float v512 = 5.7611011491005903e-01F;
    float v517 = -7.4996449655536279e-01F;
    float v522 = -1.7385438164530381e-01F;
    float v527 = -2.1729997561977314e+00F;
    float v532 = -1.7021211726914738e+00F;
    float v537 = 4.7087858350625778e-01F;
    float v542 = -2.0239400846888440e+00F;
    float v547 = 1.0551641201664090e-01F;
    float v552 = 2.1294564967054850e+00F;
    float v557 = -7.5087543897371167e-01F;
    float v562 = 1.4812817695157160e-01F;
    float v567 = 8.9900361592528333e-01F;
    float v572 = -6.2148246772602778e-01F;
    float v577 = -7.9869352098712687e-01F;
    float v582 = -4.7339199623771833e-01F;
    float v587 = 2.4216105241892630e-01F;
    float v594 = 5.9368607967505101e-02F;
    float v601 = -1.2578688255176201e-02F;
    float v608 = 4.6789919712328903e-02F;
    float v615 = 9.3750121913782358e-01F;
    float v622 = 5.0111537043352902e-02F;
    float v629 = 9.8761275618117661e-01F;
    float v636 = 1.1745786501205959e+00F;
    float v643 = -1.1114482296234993e+00F;
    float v650 = -2.2860268797440955e+00F;
    float v657 = -2.6420523257930939e-01F;
    float v664 = -2.1981792779352136e+00F;
    float v671 = -1.9339740453559042e+00F;
    float v678 = 7.4825847091254893e-01F;
    float v685 = 4.7820835642768872e-01F;
    float v692 = -2.7005011448486022e-01F;
    float v699 = 3.4642356159542270e-01F;
    float v706 = 8.3485429360688279e-01F;
    float v713 = 3.9375928506743518e-01F;
    const float32x2_t *v980 = &v5[v0];
    int32_t *v1203 = &v6[v2];
    int64_t v33 = v0 * 18;
    int64_t v55 = v10 * 17;
    int64_t v61 = v0 * 2;
    int64_t v75 = v0 * 17;
    int64_t v90 = v10 * 16;
    int64_t v103 = v0 * 4;
    int64_t v117 = v0 * 15;
    int64_t v132 = v10 * 3;
    int64_t v139 = v10 * 14;
    int64_t v145 = v0 * 8;
    int64_t v159 = v0 * 11;
    int64_t v174 = v10 * 10;
    int64_t v181 = v10 * 7;
    int64_t v187 = v0 * 16;
    int64_t v201 = v0 * 3;
    int64_t v216 = v10 * 15;
    int64_t v223 = v10 * 2;
    int64_t v229 = v0 * 13;
    int64_t v243 = v0 * 6;
    int64_t v258 = v10 * 5;
    int64_t v265 = v10 * 12;
    int64_t v271 = v0 * 7;
    int64_t v285 = v0 * 12;
    int64_t v300 = v10 * 6;
    int64_t v307 = v10 * 11;
    int64_t v313 = v0 * 14;
    int64_t v327 = v0 * 5;
    int64_t v342 = v10 * 4;
    int64_t v349 = v10 * 13;
    int64_t v355 = v0 * 9;
    int64_t v369 = v0 * 10;
    int64_t v384 = v10 * 8;
    int64_t v391 = v10 * 9;
    int64_t v392 = v13 * 18;
    float v590 = v4 * v587;
    float v597 = v4 * v594;
    float v604 = v4 * v601;
    float v611 = v4 * v608;
    float v618 = v4 * v615;
    float v625 = v4 * v622;
    float v632 = v4 * v629;
    float v639 = v4 * v636;
    float v646 = v4 * v643;
    float v653 = v4 * v650;
    float v660 = v4 * v657;
    float v667 = v4 * v664;
    float v674 = v4 * v671;
    float v681 = v4 * v678;
    float v688 = v4 * v685;
    float v695 = v4 * v692;
    float v702 = v4 * v699;
    float v709 = v4 * v706;
    float v716 = v4 * v713;
    int64_t v823 = v2 * 18;
    int64_t v832 = v2 * 2;
    int64_t v841 = v2 * 17;
    int64_t v850 = v2 * 3;
    int64_t v859 = v2 * 16;
    int64_t v868 = v2 * 4;
    int64_t v877 = v2 * 15;
    int64_t v886 = v2 * 5;
    int64_t v895 = v2 * 14;
    int64_t v904 = v2 * 6;
    int64_t v913 = v2 * 13;
    int64_t v922 = v2 * 7;
    int64_t v931 = v2 * 12;
    int64_t v940 = v2 * 8;
    int64_t v949 = v2 * 11;
    int64_t v958 = v2 * 9;
    int64_t v967 = v2 * 10;
    const float32x2_t *v1145 = &v5[0];
    svint64_t v1146 = svindex_s64(0, v1);
    svfloat32_t v1149 = svdup_n_f32(v492);
    svfloat32_t v1150 = svdup_n_f32(v497);
    svfloat32_t v1151 = svdup_n_f32(v502);
    svfloat32_t v1152 = svdup_n_f32(v507);
    svfloat32_t v1153 = svdup_n_f32(v512);
    svfloat32_t v1154 = svdup_n_f32(v517);
    svfloat32_t v1155 = svdup_n_f32(v522);
    svfloat32_t v1156 = svdup_n_f32(v527);
    svfloat32_t v1157 = svdup_n_f32(v532);
    svfloat32_t v1158 = svdup_n_f32(v537);
    svfloat32_t v1159 = svdup_n_f32(v542);
    svfloat32_t v1160 = svdup_n_f32(v547);
    svfloat32_t v1161 = svdup_n_f32(v552);
    svfloat32_t v1162 = svdup_n_f32(v557);
    svfloat32_t v1163 = svdup_n_f32(v562);
    svfloat32_t v1164 = svdup_n_f32(v567);
    svfloat32_t v1165 = svdup_n_f32(v572);
    svfloat32_t v1166 = svdup_n_f32(v577);
    svfloat32_t v1167 = svdup_n_f32(v582);
    int32_t *v1194 = &v6[0];
    svfloat32_t v51 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v392]));
    int64_t v57 = v55 + v392;
    int64_t v92 = v90 + v392;
    int64_t v99 = v10 + v392;
    int64_t v134 = v132 + v392;
    int64_t v141 = v139 + v392;
    int64_t v176 = v174 + v392;
    int64_t v183 = v181 + v392;
    int64_t v218 = v216 + v392;
    int64_t v225 = v223 + v392;
    int64_t v260 = v258 + v392;
    int64_t v267 = v265 + v392;
    int64_t v302 = v300 + v392;
    int64_t v309 = v307 + v392;
    int64_t v344 = v342 + v392;
    int64_t v351 = v349 + v392;
    int64_t v386 = v384 + v392;
    int64_t v393 = v391 + v392;
    svfloat32_t v982 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v980), v1146));
    const float32x2_t *v990 = &v5[v33];
    const float32x2_t *v1000 = &v5[v61];
    const float32x2_t *v1009 = &v5[v75];
    const float32x2_t *v1018 = &v5[v103];
    const float32x2_t *v1027 = &v5[v117];
    const float32x2_t *v1036 = &v5[v145];
    const float32x2_t *v1045 = &v5[v159];
    const float32x2_t *v1054 = &v5[v187];
    const float32x2_t *v1063 = &v5[v201];
    const float32x2_t *v1072 = &v5[v229];
    const float32x2_t *v1081 = &v5[v243];
    const float32x2_t *v1090 = &v5[v271];
    const float32x2_t *v1099 = &v5[v285];
    const float32x2_t *v1108 = &v5[v313];
    const float32x2_t *v1117 = &v5[v327];
    const float32x2_t *v1126 = &v5[v355];
    const float32x2_t *v1135 = &v5[v369];
    svfloat32_t v1147 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1145), v1146));
    svfloat32_t v1168 = svdup_n_f32(v590);
    svfloat32_t v1169 = svdup_n_f32(v597);
    svfloat32_t v1170 = svdup_n_f32(v604);
    svfloat32_t v1171 = svdup_n_f32(v611);
    svfloat32_t v1172 = svdup_n_f32(v618);
    svfloat32_t v1173 = svdup_n_f32(v625);
    svfloat32_t v1174 = svdup_n_f32(v632);
    svfloat32_t v1175 = svdup_n_f32(v639);
    svfloat32_t v1176 = svdup_n_f32(v646);
    svfloat32_t v1177 = svdup_n_f32(v653);
    svfloat32_t v1178 = svdup_n_f32(v660);
    svfloat32_t v1179 = svdup_n_f32(v667);
    svfloat32_t v1180 = svdup_n_f32(v674);
    svfloat32_t v1181 = svdup_n_f32(v681);
    svfloat32_t v1182 = svdup_n_f32(v688);
    svfloat32_t v1183 = svdup_n_f32(v695);
    svfloat32_t v1184 = svdup_n_f32(v702);
    svfloat32_t v1185 = svdup_n_f32(v709);
    svfloat32_t v1186 = svdup_n_f32(v716);
    int32_t *v1212 = &v6[v823];
    int32_t *v1221 = &v6[v832];
    int32_t *v1230 = &v6[v841];
    int32_t *v1239 = &v6[v850];
    int32_t *v1248 = &v6[v859];
    int32_t *v1257 = &v6[v868];
    int32_t *v1266 = &v6[v877];
    int32_t *v1275 = &v6[v886];
    int32_t *v1284 = &v6[v895];
    int32_t *v1293 = &v6[v904];
    int32_t *v1302 = &v6[v913];
    int32_t *v1311 = &v6[v922];
    int32_t *v1320 = &v6[v931];
    int32_t *v1329 = &v6[v940];
    int32_t *v1338 = &v6[v949];
    int32_t *v1347 = &v6[v958];
    int32_t *v1356 = &v6[v967];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v982, v51, 0),
                     v982, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v135 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v134]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v177 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v176]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v219 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v218]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v303 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v302]));
    svfloat32_t v310 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v309]));
    svfloat32_t v345 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v344]));
    svfloat32_t v352 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v351]));
    svfloat32_t v387 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v386]));
    svfloat32_t v394 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v393]));
    svfloat32_t v992 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v990), v1146));
    svfloat32_t v1002 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1000), v1146));
    svfloat32_t v1011 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1009), v1146));
    svfloat32_t v1020 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1018), v1146));
    svfloat32_t v1029 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1027), v1146));
    svfloat32_t v1038 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1036), v1146));
    svfloat32_t v1047 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1045), v1146));
    svfloat32_t v1056 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1054), v1146));
    svfloat32_t v1065 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1063), v1146));
    svfloat32_t v1074 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1072), v1146));
    svfloat32_t v1083 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1081), v1146));
    svfloat32_t v1092 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1090), v1146));
    svfloat32_t v1101 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1099), v1146));
    svfloat32_t v1110 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1108), v1146));
    svfloat32_t v1119 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1117), v1146));
    svfloat32_t v1128 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1126), v1146));
    svfloat32_t v1137 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1135), v1146));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v992, v58, 0),
                     v992, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v1011, v93, 0),
                     v1011, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero101, v1002, v100, 0), v1002,
        v100, 90);
    svfloat32_t zero136 = svdup_n_f32(0);
    svfloat32_t v136 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero136, v1020, v135, 0), v1020,
        v135, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero143, v1029, v142, 0), v1029,
        v142, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero178, v1047, v177, 0), v1047,
        v177, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero185, v1038, v184, 0), v1038,
        v184, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero220, v1056, v219, 0), v1056,
        v219, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero227, v1065, v226, 0), v1065,
        v226, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero262, v1083, v261, 0), v1083,
        v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero269, v1074, v268, 0), v1074,
        v268, 90);
    svfloat32_t zero304 = svdup_n_f32(0);
    svfloat32_t v304 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero304, v1092, v303, 0), v1092,
        v303, 90);
    svfloat32_t zero311 = svdup_n_f32(0);
    svfloat32_t v311 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero311, v1101, v310, 0), v1101,
        v310, 90);
    svfloat32_t zero346 = svdup_n_f32(0);
    svfloat32_t v346 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero346, v1119, v345, 0), v1119,
        v345, 90);
    svfloat32_t zero353 = svdup_n_f32(0);
    svfloat32_t v353 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero353, v1110, v352, 0), v1110,
        v352, 90);
    svfloat32_t zero388 = svdup_n_f32(0);
    svfloat32_t v388 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero388, v1128, v387, 0), v1128,
        v387, 90);
    svfloat32_t zero395 = svdup_n_f32(0);
    svfloat32_t v395 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero395, v1137, v394, 0), v1137,
        v394, 90);
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v398 = svadd_f32_x(svptrue_b32(), v101, v94);
    svfloat32_t v399 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v402 = svadd_f32_x(svptrue_b32(), v185, v178);
    svfloat32_t v403 = svsub_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v404 = svadd_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v405 = svsub_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v269, v262);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v304, v311);
    svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v304, v311);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v353, v346);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v346, v353);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v388, v395);
    svfloat32_t v413 = svsub_f32_x(svptrue_b32(), v388, v395);
    svfloat32_t v414 = svsub_f32_x(svptrue_b32(), v396, v408);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v398, v410);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v400, v412);
    svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v402, v408);
    svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v404, v410);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v406, v412);
    svfloat32_t v420 = svadd_f32_x(svptrue_b32(), v396, v402);
    svfloat32_t v422 = svadd_f32_x(svptrue_b32(), v398, v404);
    svfloat32_t v424 = svadd_f32_x(svptrue_b32(), v400, v406);
    svfloat32_t v454 = svsub_f32_x(svptrue_b32(), v397, v409);
    svfloat32_t v455 = svsub_f32_x(svptrue_b32(), v399, v411);
    svfloat32_t v456 = svsub_f32_x(svptrue_b32(), v401, v413);
    svfloat32_t v457 = svsub_f32_x(svptrue_b32(), v403, v409);
    svfloat32_t v458 = svsub_f32_x(svptrue_b32(), v405, v411);
    svfloat32_t v459 = svsub_f32_x(svptrue_b32(), v407, v413);
    svfloat32_t v460 = svadd_f32_x(svptrue_b32(), v397, v403);
    svfloat32_t v462 = svadd_f32_x(svptrue_b32(), v399, v405);
    svfloat32_t v464 = svadd_f32_x(svptrue_b32(), v401, v407);
    svfloat32_t v421 = svadd_f32_x(svptrue_b32(), v420, v408);
    svfloat32_t v423 = svadd_f32_x(svptrue_b32(), v422, v410);
    svfloat32_t v425 = svadd_f32_x(svptrue_b32(), v424, v412);
    svfloat32_t v426 = svadd_f32_x(svptrue_b32(), v414, v416);
    svfloat32_t v427 = svadd_f32_x(svptrue_b32(), v417, v419);
    svfloat32_t v444 = svsub_f32_x(svptrue_b32(), v414, v417);
    svfloat32_t v445 = svsub_f32_x(svptrue_b32(), v416, v419);
    svfloat32_t v461 = svadd_f32_x(svptrue_b32(), v460, v409);
    svfloat32_t v463 = svadd_f32_x(svptrue_b32(), v462, v411);
    svfloat32_t v465 = svadd_f32_x(svptrue_b32(), v464, v413);
    svfloat32_t v466 = svadd_f32_x(svptrue_b32(), v454, v456);
    svfloat32_t v467 = svadd_f32_x(svptrue_b32(), v457, v459);
    svfloat32_t v476 = svsub_f32_x(svptrue_b32(), v454, v457);
    svfloat32_t v477 = svsub_f32_x(svptrue_b32(), v456, v459);
    svfloat32_t zero641 = svdup_n_f32(0);
    svfloat32_t v641 = svcmla_f32_x(pred_full, zero641, v1175, v457, 90);
    svfloat32_t zero662 = svdup_n_f32(0);
    svfloat32_t v662 = svcmla_f32_x(pred_full, zero662, v1178, v459, 90);
    svfloat32_t v428 = svadd_f32_x(svptrue_b32(), v421, v423);
    svfloat32_t v438 = svadd_f32_x(svptrue_b32(), v427, v418);
    svfloat32_t v439 = svadd_f32_x(svptrue_b32(), v426, v415);
    svfloat32_t v441 = svsub_f32_x(svptrue_b32(), v427, v418);
    svfloat32_t v442 = svsub_f32_x(svptrue_b32(), v426, v415);
    svfloat32_t v446 = svsub_f32_x(svptrue_b32(), v414, v445);
    svfloat32_t v448 = svadd_f32_x(svptrue_b32(), v444, v419);
    svfloat32_t v451 = svsub_f32_x(svptrue_b32(), v421, v425);
    svfloat32_t v452 = svsub_f32_x(svptrue_b32(), v423, v425);
    svfloat32_t v468 = svadd_f32_x(svptrue_b32(), v461, v463);
    svfloat32_t v470 = svadd_f32_x(svptrue_b32(), v467, v458);
    svfloat32_t v471 = svadd_f32_x(svptrue_b32(), v466, v455);
    svfloat32_t v473 = svsub_f32_x(svptrue_b32(), v467, v458);
    svfloat32_t v474 = svsub_f32_x(svptrue_b32(), v466, v455);
    svfloat32_t v478 = svsub_f32_x(svptrue_b32(), v454, v477);
    svfloat32_t v480 = svadd_f32_x(svptrue_b32(), v476, v459);
    svfloat32_t v483 = svsub_f32_x(svptrue_b32(), v461, v465);
    svfloat32_t v484 = svsub_f32_x(svptrue_b32(), v463, v465);
    svfloat32_t v429 = svadd_f32_x(svptrue_b32(), v428, v425);
    svfloat32_t v440 = svsub_f32_x(svptrue_b32(), v439, v438);
    svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v442, v441);
    svfloat32_t v447 = svsub_f32_x(svptrue_b32(), v446, v418);
    svfloat32_t v449 = svsub_f32_x(svptrue_b32(), v448, v415);
    svfloat32_t v453 = svadd_f32_x(svptrue_b32(), v451, v452);
    svfloat32_t v469 = svadd_f32_x(svptrue_b32(), v468, v465);
    svfloat32_t v472 = svsub_f32_x(svptrue_b32(), v471, v470);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v474, v473);
    svfloat32_t v479 = svsub_f32_x(svptrue_b32(), v478, v458);
    svfloat32_t v481 = svsub_f32_x(svptrue_b32(), v480, v455);
    svfloat32_t v485 = svadd_f32_x(svptrue_b32(), v483, v484);
    svfloat32_t v505 = svmul_f32_x(svptrue_b32(), v439, v1151);
    svfloat32_t v520 = svmul_f32_x(svptrue_b32(), v442, v1154);
    svfloat32_t zero599 = svdup_n_f32(0);
    svfloat32_t v599 = svcmla_f32_x(pred_full, zero599, v1169, v470, 90);
    svfloat32_t zero620 = svdup_n_f32(0);
    svfloat32_t v620 = svcmla_f32_x(pred_full, zero620, v1172, v473, 90);
    svfloat32_t zero704 = svdup_n_f32(0);
    svfloat32_t v704 = svcmla_f32_x(pred_full, zero704, v1184, v483, 90);
    svfloat32_t zero711 = svdup_n_f32(0);
    svfloat32_t v711 = svcmla_f32_x(pred_full, zero711, v1185, v484, 90);
    svfloat32_t v437 = svadd_f32_x(svptrue_b32(), v1147, v429);
    svfloat32_t v450 = svsub_f32_x(svptrue_b32(), v447, v449);
    svfloat32_t v482 = svsub_f32_x(svptrue_b32(), v479, v481);
    svfloat32_t v510 = svmul_f32_x(svptrue_b32(), v440, v1152);
    svfloat32_t v525 = svmul_f32_x(svptrue_b32(), v443, v1155);
    svfloat32_t v585 = svmul_f32_x(svptrue_b32(), v453, v1167);
    svfloat32_t zero592 = svdup_n_f32(0);
    svfloat32_t v592 = svcmla_f32_x(pred_full, zero592, v1168, v469, 90);
    svfloat32_t zero718 = svdup_n_f32(0);
    svfloat32_t v718 = svcmla_f32_x(pred_full, zero718, v1186, v485, 90);
    svfloat32_t v719 = svmla_f32_x(pred_full, v505, v438, v1150);
    svfloat32_t v720 = svmla_f32_x(pred_full, v520, v441, v1153);
    svfloat32_t v750 = svcmla_f32_x(pred_full, v599, v1170, v471, 90);
    svfloat32_t v751 = svcmla_f32_x(pred_full, v620, v1173, v474, 90);
    svfloat32_t v570 = svmul_f32_x(svptrue_b32(), v450, v1164);
    svfloat32_t zero697 = svdup_n_f32(0);
    svfloat32_t v697 = svcmla_f32_x(pred_full, zero697, v1183, v482, 90);
    svfloat32_t v722 = svadd_f32_x(svptrue_b32(), v719, v720);
    svfloat32_t v723 = svmla_f32_x(pred_full, v510, v438, v1150);
    svfloat32_t v724 = svmla_f32_x(pred_full, v525, v441, v1153);
    svfloat32_t v741 = svsub_f32_x(svptrue_b32(), v719, v720);
    svfloat32_t v743 = svnmls_f32_x(pred_full, v585, v451, v1165);
    svfloat32_t v744 = svnmls_f32_x(pred_full, v585, v452, v1166);
    svfloat32_t v745 = svmla_f32_x(pred_full, v437, v429, v1149);
    svfloat32_t v753 = svadd_f32_x(svptrue_b32(), v750, v751);
    svfloat32_t v754 = svcmla_f32_x(pred_full, v599, v1171, v472, 90);
    svfloat32_t v755 = svcmla_f32_x(pred_full, v620, v1174, v475, 90);
    svfloat32_t v772 = svsub_f32_x(svptrue_b32(), v750, v751);
    svfloat32_t v774 = svsub_f32_x(svptrue_b32(), v704, v718);
    svfloat32_t v775 = svsub_f32_x(svptrue_b32(), v711, v718);
    svint16_t v806 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v437, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v721 = svmla_f32_x(pred_full, v570, v449, v1163);
    svfloat32_t v725 = svmla_f32_x(pred_full, v570, v447, v1162);
    svfloat32_t v726 = svnmls_f32_x(pred_full, v722, v417, v1156);
    svfloat32_t v727 = svadd_f32_x(svptrue_b32(), v723, v724);
    svfloat32_t v733 = svsub_f32_x(svptrue_b32(), v723, v724);
    svfloat32_t v738 = svmla_f32_x(pred_full, v722, v416, v1161);
    svfloat32_t v746 = svadd_f32_x(svptrue_b32(), v745, v743);
    svfloat32_t v747 = svsub_f32_x(svptrue_b32(), v745, v743);
    svfloat32_t v749 = svadd_f32_x(svptrue_b32(), v745, v744);
    svfloat32_t v752 = svcmla_f32_x(pred_full, v697, v1182, v481, 90);
    svfloat32_t v756 = svcmla_f32_x(pred_full, v697, v1181, v479, 90);
    svfloat32_t v757 = svsub_f32_x(svptrue_b32(), v641, v753);
    svfloat32_t v758 = svadd_f32_x(svptrue_b32(), v754, v755);
    svfloat32_t v764 = svsub_f32_x(svptrue_b32(), v754, v755);
    svfloat32_t v769 = svcmla_f32_x(pred_full, v753, v1180, v456, 90);
    svfloat32_t v776 = svadd_f32_x(svptrue_b32(), v592, v774);
    svfloat32_t v777 = svsub_f32_x(svptrue_b32(), v592, v774);
    svfloat32_t v779 = svadd_f32_x(svptrue_b32(), v592, v775);
    svst1w_u64(pred_full, (unsigned *)(v1194), svreinterpret_u64_s16(v806));
    svfloat32_t v728 = svnmls_f32_x(pred_full, v725, v419, v1159);
    svfloat32_t v729 = svmla_f32_x(pred_full, v721, v444, v1157);
    svfloat32_t v731 = svmla_f32_x(pred_full, v727, v445, v1160);
    svfloat32_t v734 = svadd_f32_x(svptrue_b32(), v733, v721);
    svfloat32_t v735 = svadd_f32_x(svptrue_b32(), v726, v727);
    svfloat32_t v742 = svadd_f32_x(svptrue_b32(), v741, v725);
    svfloat32_t v748 = svsub_f32_x(svptrue_b32(), v747, v744);
    svfloat32_t v759 = svsub_f32_x(svptrue_b32(), v662, v756);
    svfloat32_t v760 = svcmla_f32_x(pred_full, v752, v1176, v476, 90);
    svfloat32_t v762 = svcmla_f32_x(pred_full, v758, v1179, v477, 90);
    svfloat32_t v765 = svadd_f32_x(svptrue_b32(), v764, v752);
    svfloat32_t v766 = svadd_f32_x(svptrue_b32(), v757, v758);
    svfloat32_t v773 = svadd_f32_x(svptrue_b32(), v772, v756);
    svfloat32_t v778 = svsub_f32_x(svptrue_b32(), v777, v775);
    svfloat32_t v730 = svadd_f32_x(svptrue_b32(), v729, v726);
    svfloat32_t v732 = svadd_f32_x(svptrue_b32(), v731, v728);
    svfloat32_t v736 = svmla_f32_x(pred_full, v735, v414, v1158);
    svfloat32_t v739 = svadd_f32_x(svptrue_b32(), v738, v728);
    svfloat32_t v761 = svadd_f32_x(svptrue_b32(), v760, v757);
    svfloat32_t v763 = svadd_f32_x(svptrue_b32(), v762, v759);
    svfloat32_t v767 = svcmla_f32_x(pred_full, v766, v1177, v454, 90);
    svfloat32_t v770 = svadd_f32_x(svptrue_b32(), v769, v759);
    svfloat32_t v784 = svsub_f32_x(svptrue_b32(), v742, v734);
    svfloat32_t v788 = svsub_f32_x(svptrue_b32(), v749, v742);
    svfloat32_t v791 = svadd_f32_x(svptrue_b32(), v734, v749);
    svfloat32_t v796 = svsub_f32_x(svptrue_b32(), v773, v765);
    svfloat32_t v800 = svsub_f32_x(svptrue_b32(), v773, v779);
    svfloat32_t v803 = svadd_f32_x(svptrue_b32(), v765, v779);
    svfloat32_t v737 = svadd_f32_x(svptrue_b32(), v736, v725);
    svfloat32_t v740 = svadd_f32_x(svptrue_b32(), v739, v721);
    svfloat32_t v768 = svadd_f32_x(svptrue_b32(), v767, v756);
    svfloat32_t v771 = svadd_f32_x(svptrue_b32(), v770, v752);
    svfloat32_t v785 = svadd_f32_x(svptrue_b32(), v784, v749);
    svfloat32_t v789 = svadd_f32_x(svptrue_b32(), v730, v746);
    svfloat32_t v790 = svadd_f32_x(svptrue_b32(), v732, v748);
    svfloat32_t v797 = svadd_f32_x(svptrue_b32(), v796, v779);
    svfloat32_t v801 = svadd_f32_x(svptrue_b32(), v761, v776);
    svfloat32_t v802 = svadd_f32_x(svptrue_b32(), v763, v778);
    svfloat32_t v830 = svsub_f32_x(svptrue_b32(), v791, v803);
    svfloat32_t v839 = svadd_f32_x(svptrue_b32(), v791, v803);
    svfloat32_t v848 = svadd_f32_x(svptrue_b32(), v788, v800);
    svfloat32_t v857 = svsub_f32_x(svptrue_b32(), v788, v800);
    svfloat32_t v780 = svsub_f32_x(svptrue_b32(), v737, v730);
    svfloat32_t v782 = svsub_f32_x(svptrue_b32(), v740, v732);
    svfloat32_t v786 = svsub_f32_x(svptrue_b32(), v746, v737);
    svfloat32_t v787 = svsub_f32_x(svptrue_b32(), v748, v740);
    svfloat32_t v792 = svsub_f32_x(svptrue_b32(), v768, v761);
    svfloat32_t v794 = svsub_f32_x(svptrue_b32(), v771, v763);
    svfloat32_t v798 = svsub_f32_x(svptrue_b32(), v776, v768);
    svfloat32_t v799 = svsub_f32_x(svptrue_b32(), v778, v771);
    svint16_t v833 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v830, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v842 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v839, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v851 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v848, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v860 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v857, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v866 = svadd_f32_x(svptrue_b32(), v790, v802);
    svfloat32_t v875 = svsub_f32_x(svptrue_b32(), v790, v802);
    svfloat32_t v884 = svadd_f32_x(svptrue_b32(), v785, v797);
    svfloat32_t v893 = svsub_f32_x(svptrue_b32(), v785, v797);
    svfloat32_t v938 = svsub_f32_x(svptrue_b32(), v789, v801);
    svfloat32_t v947 = svadd_f32_x(svptrue_b32(), v789, v801);
    svfloat32_t v781 = svadd_f32_x(svptrue_b32(), v780, v746);
    svfloat32_t v783 = svadd_f32_x(svptrue_b32(), v782, v748);
    svfloat32_t v793 = svadd_f32_x(svptrue_b32(), v792, v776);
    svfloat32_t v795 = svadd_f32_x(svptrue_b32(), v794, v778);
    svint16_t v869 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v866, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v878 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v875, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v887 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v884, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v896 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v893, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v902 = svadd_f32_x(svptrue_b32(), v787, v799);
    svfloat32_t v911 = svsub_f32_x(svptrue_b32(), v787, v799);
    svfloat32_t v920 = svadd_f32_x(svptrue_b32(), v786, v798);
    svfloat32_t v929 = svsub_f32_x(svptrue_b32(), v786, v798);
    svint16_t v941 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v938, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v950 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v947, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1221), svreinterpret_u64_s16(v833));
    svst1w_u64(pred_full, (unsigned *)(v1230), svreinterpret_u64_s16(v842));
    svst1w_u64(pred_full, (unsigned *)(v1239), svreinterpret_u64_s16(v851));
    svst1w_u64(pred_full, (unsigned *)(v1248), svreinterpret_u64_s16(v860));
    svfloat32_t v812 = svadd_f32_x(svptrue_b32(), v781, v793);
    svfloat32_t v821 = svsub_f32_x(svptrue_b32(), v781, v793);
    svint16_t v905 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v902, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v914 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v911, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v923 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v920, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v932 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v929, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v956 = svadd_f32_x(svptrue_b32(), v783, v795);
    svfloat32_t v965 = svsub_f32_x(svptrue_b32(), v783, v795);
    svst1w_u64(pred_full, (unsigned *)(v1257), svreinterpret_u64_s16(v869));
    svst1w_u64(pred_full, (unsigned *)(v1266), svreinterpret_u64_s16(v878));
    svst1w_u64(pred_full, (unsigned *)(v1275), svreinterpret_u64_s16(v887));
    svst1w_u64(pred_full, (unsigned *)(v1284), svreinterpret_u64_s16(v896));
    svst1w_u64(pred_full, (unsigned *)(v1329), svreinterpret_u64_s16(v941));
    svst1w_u64(pred_full, (unsigned *)(v1338), svreinterpret_u64_s16(v950));
    svint16_t v815 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v812, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v824 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v821, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v959 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v956, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v968 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v965, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1293), svreinterpret_u64_s16(v905));
    svst1w_u64(pred_full, (unsigned *)(v1302), svreinterpret_u64_s16(v914));
    svst1w_u64(pred_full, (unsigned *)(v1311), svreinterpret_u64_s16(v923));
    svst1w_u64(pred_full, (unsigned *)(v1320), svreinterpret_u64_s16(v932));
    svst1w_u64(pred_full, (unsigned *)(v1203), svreinterpret_u64_s16(v815));
    svst1w_u64(pred_full, (unsigned *)(v1212), svreinterpret_u64_s16(v824));
    svst1w_u64(pred_full, (unsigned *)(v1347), svreinterpret_u64_s16(v959));
    svst1w_u64(pred_full, (unsigned *)(v1356), svreinterpret_u64_s16(v968));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu20(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v547 = v5[istride];
    float v760 = 1.5388417685876268e+00F;
    float v767 = 5.8778525229247325e-01F;
    float v774 = 3.6327126400268028e-01F;
    float v798 = 1.0000000000000000e+00F;
    float v799 = -1.0000000000000000e+00F;
    float v805 = -1.2500000000000000e+00F;
    float v806 = 1.2500000000000000e+00F;
    float v812 = 5.5901699437494745e-01F;
    float v813 = -5.5901699437494745e-01F;
    float32x2_t v815 = (float32x2_t){v4, v4};
    float v820 = -1.5388417685876268e+00F;
    float v824 = -5.8778525229247325e-01F;
    float v828 = -3.6327126400268028e-01F;
    float32x2_t v584 = vtrn1_f32(v547, v547);
    float32x2_t v585 = vtrn2_f32(v547, v547);
    float32x2_t v609 = v5[0];
    float32x2_t v754 = (float32x2_t){v805, v805};
    float32x2_t v758 = (float32x2_t){v812, v812};
    float32x2_t v762 = (float32x2_t){v760, v820};
    float32x2_t v769 = (float32x2_t){v767, v824};
    float32x2_t v776 = (float32x2_t){v774, v828};
    float32x2_t v800 = (float32x2_t){v798, v799};
    float32x2_t v807 = (float32x2_t){v805, v806};
    float32x2_t v814 = (float32x2_t){v812, v813};
    float32x2_t v821 = (float32x2_t){v820, v820};
    float32x2_t v825 = (float32x2_t){v824, v824};
    float32x2_t v829 = (float32x2_t){v828, v828};
    float32x2_t v20 = v5[istride * 10];
    int64_t v37 = 18 + j * 38;
    float32x2_t v51 = v5[istride * 5];
    float32x2_t v69 = v5[istride * 15];
    int64_t v86 = 8 + j * 38;
    int64_t v99 = 28 + j * 38;
    float32x2_t v113 = v5[istride * 4];
    float32x2_t v131 = v5[istride * 14];
    int64_t v148 = 6 + j * 38;
    int64_t v161 = 26 + j * 38;
    float32x2_t v175 = v5[istride * 9];
    float32x2_t v193 = v5[istride * 19];
    int64_t v210 = 16 + j * 38;
    int64_t v223 = 36 + j * 38;
    float32x2_t v237 = v5[istride * 8];
    float32x2_t v255 = v5[istride * 18];
    int64_t v272 = 14 + j * 38;
    int64_t v285 = 34 + j * 38;
    float32x2_t v299 = v5[istride * 13];
    float32x2_t v317 = v5[istride * 3];
    int64_t v334 = 24 + j * 38;
    int64_t v347 = 4 + j * 38;
    float32x2_t v361 = v5[istride * 12];
    float32x2_t v379 = v5[istride * 2];
    int64_t v396 = 22 + j * 38;
    int64_t v409 = 2 + j * 38;
    float32x2_t v423 = v5[istride * 17];
    float32x2_t v441 = v5[istride * 7];
    int64_t v458 = 32 + j * 38;
    int64_t v471 = 12 + j * 38;
    float32x2_t v485 = v5[istride * 16];
    float32x2_t v503 = v5[istride * 6];
    int64_t v520 = 30 + j * 38;
    int64_t v533 = 10 + j * 38;
    float32x2_t v565 = v5[istride * 11];
    float32x2_t v583 = v7[j * 38];
    int64_t v587 = j * 38 + 1;
    int64_t v595 = 20 + j * 38;
    float32x2_t v764 = vmul_f32(v815, v762);
    float32x2_t v771 = vmul_f32(v815, v769);
    float32x2_t v778 = vmul_f32(v815, v776);
    float32x2_t v802 = vmul_f32(v815, v800);
    float32x2_t v809 = vmul_f32(v815, v807);
    float32x2_t v816 = vmul_f32(v815, v814);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v273 = v7[v272];
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    int64_t v277 = v272 + 1;
    float32x2_t v286 = v7[v285];
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    int64_t v290 = v285 + 1;
    float32x2_t v335 = v7[v334];
    float32x2_t v336 = vtrn1_f32(v299, v299);
    float32x2_t v337 = vtrn2_f32(v299, v299);
    int64_t v339 = v334 + 1;
    float32x2_t v348 = v7[v347];
    float32x2_t v349 = vtrn1_f32(v317, v317);
    float32x2_t v350 = vtrn2_f32(v317, v317);
    int64_t v352 = v347 + 1;
    float32x2_t v397 = v7[v396];
    float32x2_t v398 = vtrn1_f32(v361, v361);
    float32x2_t v399 = vtrn2_f32(v361, v361);
    int64_t v401 = v396 + 1;
    float32x2_t v410 = v7[v409];
    float32x2_t v411 = vtrn1_f32(v379, v379);
    float32x2_t v412 = vtrn2_f32(v379, v379);
    int64_t v414 = v409 + 1;
    float32x2_t v459 = v7[v458];
    float32x2_t v460 = vtrn1_f32(v423, v423);
    float32x2_t v461 = vtrn2_f32(v423, v423);
    int64_t v463 = v458 + 1;
    float32x2_t v472 = v7[v471];
    float32x2_t v473 = vtrn1_f32(v441, v441);
    float32x2_t v474 = vtrn2_f32(v441, v441);
    int64_t v476 = v471 + 1;
    float32x2_t v521 = v7[v520];
    float32x2_t v522 = vtrn1_f32(v485, v485);
    float32x2_t v523 = vtrn2_f32(v485, v485);
    int64_t v525 = v520 + 1;
    float32x2_t v534 = v7[v533];
    float32x2_t v535 = vtrn1_f32(v503, v503);
    float32x2_t v536 = vtrn2_f32(v503, v503);
    int64_t v538 = v533 + 1;
    float32x2_t v588 = v7[v587];
    float32x2_t v589 = vmul_f32(v584, v583);
    float32x2_t v596 = v7[v595];
    float32x2_t v597 = vtrn1_f32(v565, v565);
    float32x2_t v598 = vtrn2_f32(v565, v565);
    int64_t v600 = v595 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vmul_f32(v336, v335);
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vmul_f32(v349, v348);
    float32x2_t v402 = v7[v401];
    float32x2_t v403 = vmul_f32(v398, v397);
    float32x2_t v415 = v7[v414];
    float32x2_t v416 = vmul_f32(v411, v410);
    float32x2_t v464 = v7[v463];
    float32x2_t v465 = vmul_f32(v460, v459);
    float32x2_t v477 = v7[v476];
    float32x2_t v478 = vmul_f32(v473, v472);
    float32x2_t v526 = v7[v525];
    float32x2_t v527 = vmul_f32(v522, v521);
    float32x2_t v539 = v7[v538];
    float32x2_t v540 = vmul_f32(v535, v534);
    float32x2_t v601 = v7[v600];
    float32x2_t v602 = vmul_f32(v597, v596);
    float32x2_t v591 = vfma_f32(v589, v585, v588);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v343 = vfma_f32(v341, v337, v340);
    float32x2_t v356 = vfma_f32(v354, v350, v353);
    float32x2_t v405 = vfma_f32(v403, v399, v402);
    float32x2_t v418 = vfma_f32(v416, v412, v415);
    float32x2_t v467 = vfma_f32(v465, v461, v464);
    float32x2_t v480 = vfma_f32(v478, v474, v477);
    float32x2_t v529 = vfma_f32(v527, v523, v526);
    float32x2_t v542 = vfma_f32(v540, v536, v539);
    float32x2_t v604 = vfma_f32(v602, v598, v601);
    float32x2_t v610 = vadd_f32(v609, v46);
    float32x2_t v611 = vsub_f32(v609, v46);
    float32x2_t v612 = vadd_f32(v95, v108);
    float32x2_t v613 = vsub_f32(v95, v108);
    float32x2_t v616 = vadd_f32(v157, v170);
    float32x2_t v617 = vsub_f32(v157, v170);
    float32x2_t v618 = vadd_f32(v219, v232);
    float32x2_t v619 = vsub_f32(v219, v232);
    float32x2_t v622 = vadd_f32(v281, v294);
    float32x2_t v623 = vsub_f32(v281, v294);
    float32x2_t v624 = vadd_f32(v343, v356);
    float32x2_t v625 = vsub_f32(v343, v356);
    float32x2_t v628 = vadd_f32(v405, v418);
    float32x2_t v629 = vsub_f32(v405, v418);
    float32x2_t v630 = vadd_f32(v467, v480);
    float32x2_t v631 = vsub_f32(v467, v480);
    float32x2_t v634 = vadd_f32(v529, v542);
    float32x2_t v635 = vsub_f32(v529, v542);
    float32x2_t v636 = vadd_f32(v591, v604);
    float32x2_t v637 = vsub_f32(v591, v604);
    float32x2_t v614 = vadd_f32(v610, v612);
    float32x2_t v615 = vsub_f32(v610, v612);
    float32x2_t v620 = vadd_f32(v616, v618);
    float32x2_t v621 = vsub_f32(v616, v618);
    float32x2_t v626 = vadd_f32(v622, v624);
    float32x2_t v627 = vsub_f32(v622, v624);
    float32x2_t v632 = vadd_f32(v628, v630);
    float32x2_t v633 = vsub_f32(v628, v630);
    float32x2_t v638 = vadd_f32(v634, v636);
    float32x2_t v639 = vsub_f32(v634, v636);
    float32x2_t v740 = vadd_f32(v617, v635);
    float32x2_t v741 = vsub_f32(v617, v635);
    float32x2_t v742 = vadd_f32(v629, v623);
    float32x2_t v743 = vsub_f32(v629, v623);
    float32x2_t v790 = vadd_f32(v619, v637);
    float32x2_t v791 = vsub_f32(v619, v637);
    float32x2_t v792 = vadd_f32(v631, v625);
    float32x2_t v793 = vsub_f32(v631, v625);
    float32x2_t v640 = vadd_f32(v620, v638);
    float32x2_t v641 = vsub_f32(v620, v638);
    float32x2_t v642 = vadd_f32(v632, v626);
    float32x2_t v643 = vsub_f32(v632, v626);
    float32x2_t v690 = vadd_f32(v621, v639);
    float32x2_t v691 = vsub_f32(v621, v639);
    float32x2_t v692 = vadd_f32(v633, v627);
    float32x2_t v693 = vsub_f32(v633, v627);
    float32x2_t v744 = vadd_f32(v740, v742);
    float32x2_t v745 = vsub_f32(v740, v742);
    float32x2_t v746 = vadd_f32(v741, v743);
    float32x2_t v765 = vrev64_f32(v741);
    float32x2_t v779 = vrev64_f32(v743);
    float32x2_t v794 = vadd_f32(v790, v792);
    float32x2_t v795 = vsub_f32(v790, v792);
    float32x2_t v796 = vadd_f32(v791, v793);
    float32x2_t v822 = vmul_f32(v791, v821);
    float32x2_t v830 = vmul_f32(v793, v829);
    float32x2_t v644 = vadd_f32(v640, v642);
    float32x2_t v645 = vsub_f32(v640, v642);
    float32x2_t v646 = vadd_f32(v641, v643);
    float32x2_t v665 = vrev64_f32(v641);
    float32x2_t v679 = vrev64_f32(v643);
    float32x2_t v694 = vadd_f32(v690, v692);
    float32x2_t v695 = vsub_f32(v690, v692);
    float32x2_t v696 = vadd_f32(v691, v693);
    float32x2_t v715 = vrev64_f32(v691);
    float32x2_t v729 = vrev64_f32(v693);
    float32x2_t v747 = vadd_f32(v744, v611);
    float32x2_t v755 = vmul_f32(v744, v754);
    float32x2_t v759 = vmul_f32(v745, v758);
    float32x2_t v766 = vmul_f32(v765, v764);
    float32x2_t v772 = vrev64_f32(v746);
    float32x2_t v780 = vmul_f32(v779, v778);
    float32x2_t v797 = vadd_f32(v794, v613);
    float32x2_t v810 = vrev64_f32(v794);
    float32x2_t v817 = vrev64_f32(v795);
    float32x2_t v826 = vmul_f32(v796, v825);
    float32x2_t v647 = vadd_f32(v644, v614);
    float32x2_t v655 = vmul_f32(v644, v754);
    float32x2_t v659 = vmul_f32(v645, v758);
    float32x2_t v666 = vmul_f32(v665, v764);
    float32x2_t v672 = vrev64_f32(v646);
    float32x2_t v680 = vmul_f32(v679, v778);
    float32x2_t v697 = vadd_f32(v694, v615);
    float32x2_t v705 = vmul_f32(v694, v754);
    float32x2_t v709 = vmul_f32(v695, v758);
    float32x2_t v716 = vmul_f32(v715, v764);
    float32x2_t v722 = vrev64_f32(v696);
    float32x2_t v730 = vmul_f32(v729, v778);
    float32x2_t v773 = vmul_f32(v772, v771);
    float32x2_t v781 = vadd_f32(v747, v755);
    float32x2_t v803 = vrev64_f32(v797);
    float32x2_t v811 = vmul_f32(v810, v809);
    float32x2_t v818 = vmul_f32(v817, v816);
    float32x2_t v834 = vsub_f32(v822, v826);
    float32x2_t v835 = vadd_f32(v826, v830);
    float32x2_t v673 = vmul_f32(v672, v771);
    float32x2_t v681 = vadd_f32(v647, v655);
    float32x2_t v723 = vmul_f32(v722, v771);
    float32x2_t v731 = vadd_f32(v697, v705);
    float32x2_t v782 = vadd_f32(v781, v759);
    float32x2_t v783 = vsub_f32(v781, v759);
    float32x2_t v784 = vsub_f32(v766, v773);
    float32x2_t v785 = vadd_f32(v773, v780);
    float32x2_t v804 = vmul_f32(v803, v802);
    int16x4_t v844 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v647, 15), (int32x2_t){0, 0}));
    int16x4_t v856 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v697, 15), (int32x2_t){0, 0}));
    float32x2_t v682 = vadd_f32(v681, v659);
    float32x2_t v683 = vsub_f32(v681, v659);
    float32x2_t v684 = vsub_f32(v666, v673);
    float32x2_t v685 = vadd_f32(v673, v680);
    float32x2_t v732 = vadd_f32(v731, v709);
    float32x2_t v733 = vsub_f32(v731, v709);
    float32x2_t v734 = vsub_f32(v716, v723);
    float32x2_t v735 = vadd_f32(v723, v730);
    float32x2_t v786 = vadd_f32(v782, v784);
    float32x2_t v787 = vsub_f32(v782, v784);
    float32x2_t v788 = vadd_f32(v783, v785);
    float32x2_t v789 = vsub_f32(v783, v785);
    float32x2_t v831 = vadd_f32(v804, v811);
    float32x2_t v840 = vadd_f32(v747, v804);
    float32x2_t v841 = vsub_f32(v747, v804);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v844), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v856), 0);
    float32x2_t v686 = vadd_f32(v682, v684);
    float32x2_t v687 = vsub_f32(v682, v684);
    float32x2_t v688 = vadd_f32(v683, v685);
    float32x2_t v689 = vsub_f32(v683, v685);
    float32x2_t v736 = vadd_f32(v732, v734);
    float32x2_t v737 = vsub_f32(v732, v734);
    float32x2_t v738 = vadd_f32(v733, v735);
    float32x2_t v739 = vsub_f32(v733, v735);
    float32x2_t v832 = vadd_f32(v831, v818);
    float32x2_t v833 = vsub_f32(v831, v818);
    int16x4_t v850 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v841, 15), (int32x2_t){0, 0}));
    int16x4_t v862 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v840, 15), (int32x2_t){0, 0}));
    float32x2_t v836 = vadd_f32(v832, v834);
    float32x2_t v837 = vsub_f32(v832, v834);
    float32x2_t v838 = vadd_f32(v833, v835);
    float32x2_t v839 = vsub_f32(v833, v835);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v850), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v862), 0);
    int16x4_t v870 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v687, 15), (int32x2_t){0, 0}));
    int16x4_t v882 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v737, 15), (int32x2_t){0, 0}));
    int16x4_t v896 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v689, 15), (int32x2_t){0, 0}));
    int16x4_t v908 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v739, 15), (int32x2_t){0, 0}));
    int16x4_t v922 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v688, 15), (int32x2_t){0, 0}));
    int16x4_t v934 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v738, 15), (int32x2_t){0, 0}));
    int16x4_t v948 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v686, 15), (int32x2_t){0, 0}));
    int16x4_t v960 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v736, 15), (int32x2_t){0, 0}));
    float32x2_t v866 = vadd_f32(v787, v837);
    float32x2_t v867 = vsub_f32(v787, v837);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v870), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v882), 0);
    float32x2_t v892 = vadd_f32(v789, v839);
    float32x2_t v893 = vsub_f32(v789, v839);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v896), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v908), 0);
    float32x2_t v918 = vadd_f32(v788, v838);
    float32x2_t v919 = vsub_f32(v788, v838);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v922), 0);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v934), 0);
    float32x2_t v944 = vadd_f32(v786, v836);
    float32x2_t v945 = vsub_f32(v786, v836);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v948), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v960), 0);
    int16x4_t v876 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v867, 15), (int32x2_t){0, 0}));
    int16x4_t v888 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v866, 15), (int32x2_t){0, 0}));
    int16x4_t v902 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v893, 15), (int32x2_t){0, 0}));
    int16x4_t v914 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v892, 15), (int32x2_t){0, 0}));
    int16x4_t v928 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v919, 15), (int32x2_t){0, 0}));
    int16x4_t v940 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v918, 15), (int32x2_t){0, 0}));
    int16x4_t v954 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v945, 15), (int32x2_t){0, 0}));
    int16x4_t v966 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v944, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v876), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v888), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v902), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v914), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v928), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v940), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v954), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v966), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu20(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v574 = -1.2500000000000000e+00F;
    float v579 = 5.5901699437494745e-01F;
    float v622 = -1.0000000000000000e+00F;
    float v629 = 1.2500000000000000e+00F;
    float v636 = -5.5901699437494745e-01F;
    float v643 = -1.5388417685876268e+00F;
    float v648 = -5.8778525229247325e-01F;
    float v653 = -3.6327126400268028e-01F;
    const float32x2_t *v995 = &v5[v0];
    int32_t *v1095 = &v6[v2];
    int64_t v19 = v0 * 10;
    int64_t v34 = v10 * 9;
    int64_t v40 = v0 * 5;
    int64_t v54 = v0 * 15;
    int64_t v69 = v10 * 4;
    int64_t v76 = v10 * 14;
    int64_t v82 = v0 * 4;
    int64_t v96 = v0 * 14;
    int64_t v111 = v10 * 3;
    int64_t v118 = v10 * 13;
    int64_t v124 = v0 * 9;
    int64_t v138 = v0 * 19;
    int64_t v153 = v10 * 8;
    int64_t v160 = v10 * 18;
    int64_t v166 = v0 * 8;
    int64_t v180 = v0 * 18;
    int64_t v195 = v10 * 7;
    int64_t v202 = v10 * 17;
    int64_t v208 = v0 * 13;
    int64_t v222 = v0 * 3;
    int64_t v237 = v10 * 12;
    int64_t v244 = v10 * 2;
    int64_t v250 = v0 * 12;
    int64_t v264 = v0 * 2;
    int64_t v279 = v10 * 11;
    int64_t v292 = v0 * 17;
    int64_t v306 = v0 * 7;
    int64_t v321 = v10 * 16;
    int64_t v328 = v10 * 6;
    int64_t v334 = v0 * 16;
    int64_t v348 = v0 * 6;
    int64_t v363 = v10 * 15;
    int64_t v370 = v10 * 5;
    int64_t v390 = v0 * 11;
    int64_t v412 = v10 * 10;
    int64_t v413 = v13 * 19;
    float v587 = v4 * v643;
    float v594 = v4 * v648;
    float v601 = v4 * v653;
    float v625 = v4 * v622;
    float v632 = v4 * v629;
    float v639 = v4 * v636;
    int64_t v677 = v2 * 5;
    int64_t v685 = v2 * 10;
    int64_t v693 = v2 * 15;
    int64_t v703 = v2 * 16;
    int64_t v719 = v2 * 6;
    int64_t v727 = v2 * 11;
    int64_t v737 = v2 * 12;
    int64_t v745 = v2 * 17;
    int64_t v753 = v2 * 2;
    int64_t v761 = v2 * 7;
    int64_t v771 = v2 * 8;
    int64_t v779 = v2 * 13;
    int64_t v787 = v2 * 18;
    int64_t v795 = v2 * 3;
    int64_t v805 = v2 * 4;
    int64_t v813 = v2 * 9;
    int64_t v821 = v2 * 14;
    int64_t v829 = v2 * 19;
    const float32x2_t *v1016 = &v5[0];
    svint64_t v1017 = svindex_s64(0, v1);
    svfloat32_t v1032 = svdup_n_f32(v574);
    svfloat32_t v1033 = svdup_n_f32(v579);
    svfloat32_t v1040 = svdup_n_f32(v643);
    svfloat32_t v1041 = svdup_n_f32(v648);
    svfloat32_t v1042 = svdup_n_f32(v653);
    int32_t *v1050 = &v6[0];
    int64_t v36 = v34 + v413;
    int64_t v71 = v69 + v413;
    int64_t v78 = v76 + v413;
    int64_t v113 = v111 + v413;
    int64_t v120 = v118 + v413;
    int64_t v155 = v153 + v413;
    int64_t v162 = v160 + v413;
    int64_t v197 = v195 + v413;
    int64_t v204 = v202 + v413;
    int64_t v239 = v237 + v413;
    int64_t v246 = v244 + v413;
    int64_t v281 = v279 + v413;
    int64_t v288 = v10 + v413;
    int64_t v323 = v321 + v413;
    int64_t v330 = v328 + v413;
    int64_t v365 = v363 + v413;
    int64_t v372 = v370 + v413;
    svfloat32_t v408 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v413]));
    int64_t v414 = v412 + v413;
    const float32x2_t *v842 = &v5[v19];
    const float32x2_t *v851 = &v5[v40];
    const float32x2_t *v860 = &v5[v54];
    const float32x2_t *v869 = &v5[v82];
    const float32x2_t *v878 = &v5[v96];
    const float32x2_t *v887 = &v5[v124];
    const float32x2_t *v896 = &v5[v138];
    const float32x2_t *v905 = &v5[v166];
    const float32x2_t *v914 = &v5[v180];
    const float32x2_t *v923 = &v5[v208];
    const float32x2_t *v932 = &v5[v222];
    const float32x2_t *v941 = &v5[v250];
    const float32x2_t *v950 = &v5[v264];
    const float32x2_t *v959 = &v5[v292];
    const float32x2_t *v968 = &v5[v306];
    const float32x2_t *v977 = &v5[v334];
    const float32x2_t *v986 = &v5[v348];
    svfloat32_t v997 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v995), v1017));
    const float32x2_t *v1005 = &v5[v390];
    svfloat32_t v1018 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1016), v1017));
    svfloat32_t v1034 = svdup_n_f32(v587);
    svfloat32_t v1035 = svdup_n_f32(v594);
    svfloat32_t v1036 = svdup_n_f32(v601);
    svfloat32_t v1037 = svdup_n_f32(v625);
    svfloat32_t v1038 = svdup_n_f32(v632);
    svfloat32_t v1039 = svdup_n_f32(v639);
    int32_t *v1059 = &v6[v677];
    int32_t *v1068 = &v6[v685];
    int32_t *v1077 = &v6[v693];
    int32_t *v1086 = &v6[v703];
    int32_t *v1104 = &v6[v719];
    int32_t *v1113 = &v6[v727];
    int32_t *v1122 = &v6[v737];
    int32_t *v1131 = &v6[v745];
    int32_t *v1140 = &v6[v753];
    int32_t *v1149 = &v6[v761];
    int32_t *v1158 = &v6[v771];
    int32_t *v1167 = &v6[v779];
    int32_t *v1176 = &v6[v787];
    int32_t *v1185 = &v6[v795];
    int32_t *v1194 = &v6[v805];
    int32_t *v1203 = &v6[v813];
    int32_t *v1212 = &v6[v821];
    int32_t *v1221 = &v6[v829];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v246]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v288]));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v331 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v330]));
    svfloat32_t v366 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v365]));
    svfloat32_t v373 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v372]));
    svfloat32_t zero409 = svdup_n_f32(0);
    svfloat32_t v409 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero409, v997, v408, 0),
                     v997, v408, 90);
    svfloat32_t v415 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v414]));
    svfloat32_t v844 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v842), v1017));
    svfloat32_t v853 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v851), v1017));
    svfloat32_t v862 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v860), v1017));
    svfloat32_t v871 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v869), v1017));
    svfloat32_t v880 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v878), v1017));
    svfloat32_t v889 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v887), v1017));
    svfloat32_t v898 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v896), v1017));
    svfloat32_t v907 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v905), v1017));
    svfloat32_t v916 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v914), v1017));
    svfloat32_t v925 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v923), v1017));
    svfloat32_t v934 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v932), v1017));
    svfloat32_t v943 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v941), v1017));
    svfloat32_t v952 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v950), v1017));
    svfloat32_t v961 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v959), v1017));
    svfloat32_t v970 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v968), v1017));
    svfloat32_t v979 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v977), v1017));
    svfloat32_t v988 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v986), v1017));
    svfloat32_t v1007 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1005), v1017));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v844, v37, 0),
                     v844, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v853, v72, 0),
                     v853, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v862, v79, 0),
                     v862, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v871, v114, 0),
                     v871, v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v880, v121, 0),
                     v880, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v889, v156, 0),
                     v889, v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v898, v163, 0),
                     v898, v163, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v907, v198, 0),
                     v907, v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v916, v205, 0),
                     v916, v205, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero241, v925, v240, 0),
                     v925, v240, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero248, v934, v247, 0),
                     v934, v247, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero283, v943, v282, 0),
                     v943, v282, 90);
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero290, v952, v289, 0),
                     v952, v289, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero325, v961, v324, 0),
                     v961, v324, 90);
    svfloat32_t zero332 = svdup_n_f32(0);
    svfloat32_t v332 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero332, v970, v331, 0),
                     v970, v331, 90);
    svfloat32_t zero367 = svdup_n_f32(0);
    svfloat32_t v367 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero367, v979, v366, 0),
                     v979, v366, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero374, v988, v373, 0),
                     v988, v373, 90);
    svfloat32_t zero416 = svdup_n_f32(0);
    svfloat32_t v416 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero416, v1007, v415, 0), v1007,
        v415, 90);
    svfloat32_t v424 = svadd_f32_x(svptrue_b32(), v1018, v38);
    svfloat32_t v425 = svsub_f32_x(svptrue_b32(), v1018, v38);
    svfloat32_t v426 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v430 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v431 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v432 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v433 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v437 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v438 = svadd_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v439 = svsub_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v442 = svadd_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v444 = svadd_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v445 = svsub_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v448 = svadd_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v449 = svsub_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v450 = svadd_f32_x(svptrue_b32(), v409, v416);
    svfloat32_t v451 = svsub_f32_x(svptrue_b32(), v409, v416);
    svfloat32_t v428 = svadd_f32_x(svptrue_b32(), v424, v426);
    svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v424, v426);
    svfloat32_t v434 = svadd_f32_x(svptrue_b32(), v430, v432);
    svfloat32_t v435 = svsub_f32_x(svptrue_b32(), v430, v432);
    svfloat32_t v440 = svadd_f32_x(svptrue_b32(), v436, v438);
    svfloat32_t v441 = svsub_f32_x(svptrue_b32(), v436, v438);
    svfloat32_t v446 = svadd_f32_x(svptrue_b32(), v442, v444);
    svfloat32_t v447 = svsub_f32_x(svptrue_b32(), v442, v444);
    svfloat32_t v452 = svadd_f32_x(svptrue_b32(), v448, v450);
    svfloat32_t v453 = svsub_f32_x(svptrue_b32(), v448, v450);
    svfloat32_t v560 = svadd_f32_x(svptrue_b32(), v431, v449);
    svfloat32_t v561 = svsub_f32_x(svptrue_b32(), v431, v449);
    svfloat32_t v562 = svadd_f32_x(svptrue_b32(), v443, v437);
    svfloat32_t v563 = svsub_f32_x(svptrue_b32(), v443, v437);
    svfloat32_t v613 = svadd_f32_x(svptrue_b32(), v433, v451);
    svfloat32_t v614 = svsub_f32_x(svptrue_b32(), v433, v451);
    svfloat32_t v615 = svadd_f32_x(svptrue_b32(), v445, v439);
    svfloat32_t v616 = svsub_f32_x(svptrue_b32(), v445, v439);
    svfloat32_t v454 = svadd_f32_x(svptrue_b32(), v434, v452);
    svfloat32_t v455 = svsub_f32_x(svptrue_b32(), v434, v452);
    svfloat32_t v456 = svadd_f32_x(svptrue_b32(), v446, v440);
    svfloat32_t v457 = svsub_f32_x(svptrue_b32(), v446, v440);
    svfloat32_t v507 = svadd_f32_x(svptrue_b32(), v435, v453);
    svfloat32_t v508 = svsub_f32_x(svptrue_b32(), v435, v453);
    svfloat32_t v509 = svadd_f32_x(svptrue_b32(), v447, v441);
    svfloat32_t v510 = svsub_f32_x(svptrue_b32(), v447, v441);
    svfloat32_t v564 = svadd_f32_x(svptrue_b32(), v560, v562);
    svfloat32_t v565 = svsub_f32_x(svptrue_b32(), v560, v562);
    svfloat32_t v566 = svadd_f32_x(svptrue_b32(), v561, v563);
    svfloat32_t zero589 = svdup_n_f32(0);
    svfloat32_t v589 = svcmla_f32_x(pred_full, zero589, v1034, v561, 90);
    svfloat32_t v617 = svadd_f32_x(svptrue_b32(), v613, v615);
    svfloat32_t v618 = svsub_f32_x(svptrue_b32(), v613, v615);
    svfloat32_t v619 = svadd_f32_x(svptrue_b32(), v614, v616);
    svfloat32_t v656 = svmul_f32_x(svptrue_b32(), v616, v1042);
    svfloat32_t v458 = svadd_f32_x(svptrue_b32(), v454, v456);
    svfloat32_t v459 = svsub_f32_x(svptrue_b32(), v454, v456);
    svfloat32_t v460 = svadd_f32_x(svptrue_b32(), v455, v457);
    svfloat32_t zero483 = svdup_n_f32(0);
    svfloat32_t v483 = svcmla_f32_x(pred_full, zero483, v1034, v455, 90);
    svfloat32_t v511 = svadd_f32_x(svptrue_b32(), v507, v509);
    svfloat32_t v512 = svsub_f32_x(svptrue_b32(), v507, v509);
    svfloat32_t v513 = svadd_f32_x(svptrue_b32(), v508, v510);
    svfloat32_t zero536 = svdup_n_f32(0);
    svfloat32_t v536 = svcmla_f32_x(pred_full, zero536, v1034, v508, 90);
    svfloat32_t v567 = svadd_f32_x(svptrue_b32(), v564, v425);
    svfloat32_t zero596 = svdup_n_f32(0);
    svfloat32_t v596 = svcmla_f32_x(pred_full, zero596, v1035, v566, 90);
    svfloat32_t v620 = svadd_f32_x(svptrue_b32(), v617, v427);
    svfloat32_t zero641 = svdup_n_f32(0);
    svfloat32_t v641 = svcmla_f32_x(pred_full, zero641, v1039, v618, 90);
    svfloat32_t v651 = svmul_f32_x(svptrue_b32(), v619, v1041);
    svfloat32_t v461 = svadd_f32_x(svptrue_b32(), v458, v428);
    svfloat32_t zero490 = svdup_n_f32(0);
    svfloat32_t v490 = svcmla_f32_x(pred_full, zero490, v1035, v460, 90);
    svfloat32_t v514 = svadd_f32_x(svptrue_b32(), v511, v429);
    svfloat32_t zero543 = svdup_n_f32(0);
    svfloat32_t v543 = svcmla_f32_x(pred_full, zero543, v1035, v513, 90);
    svfloat32_t v604 = svmla_f32_x(pred_full, v567, v564, v1032);
    svfloat32_t v607 = svsub_f32_x(svptrue_b32(), v589, v596);
    svfloat32_t v608 = svcmla_f32_x(pred_full, v596, v1036, v563, 90);
    svfloat32_t zero627 = svdup_n_f32(0);
    svfloat32_t v627 = svcmla_f32_x(pred_full, zero627, v1037, v620, 90);
    svfloat32_t v660 = svnmls_f32_x(pred_full, v651, v614, v1040);
    svfloat32_t v661 = svmla_f32_x(pred_full, v656, v619, v1041);
    svfloat32_t v498 = svmla_f32_x(pred_full, v461, v458, v1032);
    svfloat32_t v501 = svsub_f32_x(svptrue_b32(), v483, v490);
    svfloat32_t v502 = svcmla_f32_x(pred_full, v490, v1036, v457, 90);
    svfloat32_t v551 = svmla_f32_x(pred_full, v514, v511, v1032);
    svfloat32_t v554 = svsub_f32_x(svptrue_b32(), v536, v543);
    svfloat32_t v555 = svcmla_f32_x(pred_full, v543, v1036, v510, 90);
    svfloat32_t v605 = svmla_f32_x(pred_full, v604, v565, v1033);
    svfloat32_t v606 = svmls_f32_x(pred_full, v604, v565, v1033);
    svfloat32_t v657 = svcmla_f32_x(pred_full, v627, v1038, v617, 90);
    svfloat32_t v666 = svadd_f32_x(svptrue_b32(), v567, v627);
    svfloat32_t v667 = svsub_f32_x(svptrue_b32(), v567, v627);
    svint16_t v670 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v461, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v686 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v514, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v499 = svmla_f32_x(pred_full, v498, v459, v1033);
    svfloat32_t v500 = svmls_f32_x(pred_full, v498, v459, v1033);
    svfloat32_t v552 = svmla_f32_x(pred_full, v551, v512, v1033);
    svfloat32_t v553 = svmls_f32_x(pred_full, v551, v512, v1033);
    svfloat32_t v609 = svadd_f32_x(svptrue_b32(), v605, v607);
    svfloat32_t v610 = svsub_f32_x(svptrue_b32(), v605, v607);
    svfloat32_t v611 = svadd_f32_x(svptrue_b32(), v606, v608);
    svfloat32_t v612 = svsub_f32_x(svptrue_b32(), v606, v608);
    svfloat32_t v658 = svadd_f32_x(svptrue_b32(), v657, v641);
    svfloat32_t v659 = svsub_f32_x(svptrue_b32(), v657, v641);
    svint16_t v678 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v667, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v694 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v666, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1050), svreinterpret_u64_s16(v670));
    svst1w_u64(pred_full, (unsigned *)(v1068), svreinterpret_u64_s16(v686));
    svfloat32_t v503 = svadd_f32_x(svptrue_b32(), v499, v501);
    svfloat32_t v504 = svsub_f32_x(svptrue_b32(), v499, v501);
    svfloat32_t v505 = svadd_f32_x(svptrue_b32(), v500, v502);
    svfloat32_t v506 = svsub_f32_x(svptrue_b32(), v500, v502);
    svfloat32_t v556 = svadd_f32_x(svptrue_b32(), v552, v554);
    svfloat32_t v557 = svsub_f32_x(svptrue_b32(), v552, v554);
    svfloat32_t v558 = svadd_f32_x(svptrue_b32(), v553, v555);
    svfloat32_t v559 = svsub_f32_x(svptrue_b32(), v553, v555);
    svfloat32_t v662 = svadd_f32_x(svptrue_b32(), v658, v660);
    svfloat32_t v663 = svsub_f32_x(svptrue_b32(), v658, v660);
    svfloat32_t v664 = svadd_f32_x(svptrue_b32(), v659, v661);
    svfloat32_t v665 = svsub_f32_x(svptrue_b32(), v659, v661);
    svst1w_u64(pred_full, (unsigned *)(v1059), svreinterpret_u64_s16(v678));
    svst1w_u64(pred_full, (unsigned *)(v1077), svreinterpret_u64_s16(v694));
    svfloat32_t v700 = svadd_f32_x(svptrue_b32(), v610, v663);
    svfloat32_t v701 = svsub_f32_x(svptrue_b32(), v610, v663);
    svint16_t v704 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v504, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v720 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v557, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v734 = svadd_f32_x(svptrue_b32(), v612, v665);
    svfloat32_t v735 = svsub_f32_x(svptrue_b32(), v612, v665);
    svint16_t v738 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v506, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v754 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v559, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v768 = svadd_f32_x(svptrue_b32(), v611, v664);
    svfloat32_t v769 = svsub_f32_x(svptrue_b32(), v611, v664);
    svint16_t v772 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v505, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v788 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v558, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v802 = svadd_f32_x(svptrue_b32(), v609, v662);
    svfloat32_t v803 = svsub_f32_x(svptrue_b32(), v609, v662);
    svint16_t v806 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v503, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v822 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v556, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v712 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v701, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v728 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v700, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v746 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v735, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v762 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v734, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v780 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v769, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v796 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v768, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v814 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v803, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v830 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v802, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1086), svreinterpret_u64_s16(v704));
    svst1w_u64(pred_full, (unsigned *)(v1104), svreinterpret_u64_s16(v720));
    svst1w_u64(pred_full, (unsigned *)(v1122), svreinterpret_u64_s16(v738));
    svst1w_u64(pred_full, (unsigned *)(v1140), svreinterpret_u64_s16(v754));
    svst1w_u64(pred_full, (unsigned *)(v1158), svreinterpret_u64_s16(v772));
    svst1w_u64(pred_full, (unsigned *)(v1176), svreinterpret_u64_s16(v788));
    svst1w_u64(pred_full, (unsigned *)(v1194), svreinterpret_u64_s16(v806));
    svst1w_u64(pred_full, (unsigned *)(v1212), svreinterpret_u64_s16(v822));
    svst1w_u64(pred_full, (unsigned *)(v1095), svreinterpret_u64_s16(v712));
    svst1w_u64(pred_full, (unsigned *)(v1113), svreinterpret_u64_s16(v728));
    svst1w_u64(pred_full, (unsigned *)(v1131), svreinterpret_u64_s16(v746));
    svst1w_u64(pred_full, (unsigned *)(v1149), svreinterpret_u64_s16(v762));
    svst1w_u64(pred_full, (unsigned *)(v1167), svreinterpret_u64_s16(v780));
    svst1w_u64(pred_full, (unsigned *)(v1185), svreinterpret_u64_s16(v796));
    svst1w_u64(pred_full, (unsigned *)(v1203), svreinterpret_u64_s16(v814));
    svst1w_u64(pred_full, (unsigned *)(v1221), svreinterpret_u64_s16(v830));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu21(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v402 = v5[istride];
    float v606 = -1.1666666666666665e+00F;
    float v610 = 7.9015646852540022e-01F;
    float v614 = 5.5854267289647742e-02F;
    float v618 = 7.3430220123575241e-01F;
    float v621 = 4.4095855184409838e-01F;
    float v622 = -4.4095855184409838e-01F;
    float v628 = 3.4087293062393137e-01F;
    float v629 = -3.4087293062393137e-01F;
    float v635 = -5.3396936033772524e-01F;
    float v636 = 5.3396936033772524e-01F;
    float v642 = 8.7484229096165667e-01F;
    float v643 = -8.7484229096165667e-01F;
    float v686 = -1.4999999999999998e+00F;
    float v690 = 1.7499999999999996e+00F;
    float v694 = -1.1852347027881001e+00F;
    float v698 = -8.3781400934471603e-02F;
    float v702 = -1.1014533018536286e+00F;
    float v705 = -6.6143782776614746e-01F;
    float v706 = 6.6143782776614746e-01F;
    float v712 = -5.1130939593589697e-01F;
    float v713 = 5.1130939593589697e-01F;
    float v719 = 8.0095404050658769e-01F;
    float v720 = -8.0095404050658769e-01F;
    float v726 = -1.3122634364424848e+00F;
    float v727 = 1.3122634364424848e+00F;
    float v769 = 8.6602540378443871e-01F;
    float v770 = -8.6602540378443871e-01F;
    float v776 = -1.0103629710818451e+00F;
    float v777 = 1.0103629710818451e+00F;
    float v783 = 6.8429557470759583e-01F;
    float v784 = -6.8429557470759583e-01F;
    float v790 = 4.8371214382601155e-02F;
    float v791 = -4.8371214382601155e-02F;
    float v797 = 6.3592436032499466e-01F;
    float v798 = -6.3592436032499466e-01F;
    float32x2_t v800 = (float32x2_t){v4, v4};
    float v805 = -3.8188130791298663e-01F;
    float v809 = -2.9520461738277515e-01F;
    float v813 = 4.6243103089499693e-01F;
    float v817 = -7.5763564827777208e-01F;
    float32x2_t v439 = vtrn1_f32(v402, v402);
    float32x2_t v440 = vtrn2_f32(v402, v402);
    float32x2_t v564 = v5[0];
    float32x2_t v607 = (float32x2_t){v606, v606};
    float32x2_t v611 = (float32x2_t){v610, v610};
    float32x2_t v615 = (float32x2_t){v614, v614};
    float32x2_t v619 = (float32x2_t){v618, v618};
    float32x2_t v623 = (float32x2_t){v621, v622};
    float32x2_t v630 = (float32x2_t){v628, v629};
    float32x2_t v637 = (float32x2_t){v635, v636};
    float32x2_t v644 = (float32x2_t){v642, v643};
    float32x2_t v687 = (float32x2_t){v686, v686};
    float32x2_t v691 = (float32x2_t){v690, v690};
    float32x2_t v695 = (float32x2_t){v694, v694};
    float32x2_t v699 = (float32x2_t){v698, v698};
    float32x2_t v703 = (float32x2_t){v702, v702};
    float32x2_t v707 = (float32x2_t){v705, v706};
    float32x2_t v714 = (float32x2_t){v712, v713};
    float32x2_t v721 = (float32x2_t){v719, v720};
    float32x2_t v728 = (float32x2_t){v726, v727};
    float32x2_t v771 = (float32x2_t){v769, v770};
    float32x2_t v778 = (float32x2_t){v776, v777};
    float32x2_t v785 = (float32x2_t){v783, v784};
    float32x2_t v792 = (float32x2_t){v790, v791};
    float32x2_t v799 = (float32x2_t){v797, v798};
    float32x2_t v806 = (float32x2_t){v805, v805};
    float32x2_t v810 = (float32x2_t){v809, v809};
    float32x2_t v814 = (float32x2_t){v813, v813};
    float32x2_t v818 = (float32x2_t){v817, v817};
    float32x2_t v20 = v5[istride * 7];
    float32x2_t v38 = v5[istride * 14];
    int64_t v55 = 12 + j * 40;
    int64_t v68 = 26 + j * 40;
    float32x2_t v82 = v5[istride * 10];
    float32x2_t v100 = v5[istride * 17];
    int64_t v117 = 18 + j * 40;
    int64_t v130 = 32 + j * 40;
    float32x2_t v144 = v5[istride * 3];
    int64_t v148 = 4 + j * 40;
    float32x2_t v162 = v5[istride * 13];
    float32x2_t v180 = v5[istride * 20];
    int64_t v197 = 24 + j * 40;
    int64_t v210 = 38 + j * 40;
    float32x2_t v224 = v5[istride * 6];
    int64_t v228 = 10 + j * 40;
    float32x2_t v242 = v5[istride * 16];
    float32x2_t v260 = v5[istride * 2];
    int64_t v277 = 30 + j * 40;
    int64_t v290 = 2 + j * 40;
    float32x2_t v304 = v5[istride * 9];
    int64_t v308 = 16 + j * 40;
    float32x2_t v322 = v5[istride * 19];
    float32x2_t v340 = v5[istride * 5];
    int64_t v357 = 36 + j * 40;
    int64_t v370 = 8 + j * 40;
    float32x2_t v384 = v5[istride * 12];
    int64_t v388 = 22 + j * 40;
    float32x2_t v420 = v5[istride * 8];
    float32x2_t v438 = v7[j * 40];
    int64_t v442 = j * 40 + 1;
    int64_t v450 = 14 + j * 40;
    float32x2_t v464 = v5[istride * 15];
    int64_t v468 = 28 + j * 40;
    float32x2_t v482 = v5[istride * 4];
    float32x2_t v500 = v5[istride * 11];
    int64_t v517 = 6 + j * 40;
    int64_t v530 = 20 + j * 40;
    float32x2_t v544 = v5[istride * 18];
    int64_t v548 = 34 + j * 40;
    float32x2_t v625 = vmul_f32(v800, v623);
    float32x2_t v632 = vmul_f32(v800, v630);
    float32x2_t v639 = vmul_f32(v800, v637);
    float32x2_t v646 = vmul_f32(v800, v644);
    float32x2_t v709 = vmul_f32(v800, v707);
    float32x2_t v716 = vmul_f32(v800, v714);
    float32x2_t v723 = vmul_f32(v800, v721);
    float32x2_t v730 = vmul_f32(v800, v728);
    float32x2_t v773 = vmul_f32(v800, v771);
    float32x2_t v780 = vmul_f32(v800, v778);
    float32x2_t v787 = vmul_f32(v800, v785);
    float32x2_t v794 = vmul_f32(v800, v792);
    float32x2_t v801 = vmul_f32(v800, v799);
    float32x2_t v56 = v7[v55];
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    int64_t v60 = v55 + 1;
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v144, v144);
    float32x2_t v151 = vtrn2_f32(v144, v144);
    int64_t v153 = v148 + 1;
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vtrn1_f32(v162, v162);
    float32x2_t v200 = vtrn2_f32(v162, v162);
    int64_t v202 = v197 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v180, v180);
    float32x2_t v213 = vtrn2_f32(v180, v180);
    int64_t v215 = v210 + 1;
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vtrn1_f32(v224, v224);
    float32x2_t v231 = vtrn2_f32(v224, v224);
    int64_t v233 = v228 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vtrn1_f32(v242, v242);
    float32x2_t v280 = vtrn2_f32(v242, v242);
    int64_t v282 = v277 + 1;
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vtrn1_f32(v260, v260);
    float32x2_t v293 = vtrn2_f32(v260, v260);
    int64_t v295 = v290 + 1;
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vtrn1_f32(v304, v304);
    float32x2_t v311 = vtrn2_f32(v304, v304);
    int64_t v313 = v308 + 1;
    float32x2_t v358 = v7[v357];
    float32x2_t v359 = vtrn1_f32(v322, v322);
    float32x2_t v360 = vtrn2_f32(v322, v322);
    int64_t v362 = v357 + 1;
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vtrn1_f32(v340, v340);
    float32x2_t v373 = vtrn2_f32(v340, v340);
    int64_t v375 = v370 + 1;
    float32x2_t v389 = v7[v388];
    float32x2_t v390 = vtrn1_f32(v384, v384);
    float32x2_t v391 = vtrn2_f32(v384, v384);
    int64_t v393 = v388 + 1;
    float32x2_t v443 = v7[v442];
    float32x2_t v444 = vmul_f32(v439, v438);
    float32x2_t v451 = v7[v450];
    float32x2_t v452 = vtrn1_f32(v420, v420);
    float32x2_t v453 = vtrn2_f32(v420, v420);
    int64_t v455 = v450 + 1;
    float32x2_t v469 = v7[v468];
    float32x2_t v470 = vtrn1_f32(v464, v464);
    float32x2_t v471 = vtrn2_f32(v464, v464);
    int64_t v473 = v468 + 1;
    float32x2_t v518 = v7[v517];
    float32x2_t v519 = vtrn1_f32(v482, v482);
    float32x2_t v520 = vtrn2_f32(v482, v482);
    int64_t v522 = v517 + 1;
    float32x2_t v531 = v7[v530];
    float32x2_t v532 = vtrn1_f32(v500, v500);
    float32x2_t v533 = vtrn2_f32(v500, v500);
    int64_t v535 = v530 + 1;
    float32x2_t v549 = v7[v548];
    float32x2_t v550 = vtrn1_f32(v544, v544);
    float32x2_t v551 = vtrn2_f32(v544, v544);
    int64_t v553 = v548 + 1;
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v203 = v7[v202];
    float32x2_t v204 = vmul_f32(v199, v198);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v234 = v7[v233];
    float32x2_t v235 = vmul_f32(v230, v229);
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vmul_f32(v292, v291);
    float32x2_t v314 = v7[v313];
    float32x2_t v315 = vmul_f32(v310, v309);
    float32x2_t v363 = v7[v362];
    float32x2_t v364 = vmul_f32(v359, v358);
    float32x2_t v376 = v7[v375];
    float32x2_t v377 = vmul_f32(v372, v371);
    float32x2_t v394 = v7[v393];
    float32x2_t v395 = vmul_f32(v390, v389);
    float32x2_t v456 = v7[v455];
    float32x2_t v457 = vmul_f32(v452, v451);
    float32x2_t v474 = v7[v473];
    float32x2_t v475 = vmul_f32(v470, v469);
    float32x2_t v523 = v7[v522];
    float32x2_t v524 = vmul_f32(v519, v518);
    float32x2_t v536 = v7[v535];
    float32x2_t v537 = vmul_f32(v532, v531);
    float32x2_t v554 = v7[v553];
    float32x2_t v555 = vmul_f32(v550, v549);
    float32x2_t v446 = vfma_f32(v444, v440, v443);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v206 = vfma_f32(v204, v200, v203);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v237 = vfma_f32(v235, v231, v234);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v299 = vfma_f32(v297, v293, v296);
    float32x2_t v317 = vfma_f32(v315, v311, v314);
    float32x2_t v366 = vfma_f32(v364, v360, v363);
    float32x2_t v379 = vfma_f32(v377, v373, v376);
    float32x2_t v397 = vfma_f32(v395, v391, v394);
    float32x2_t v459 = vfma_f32(v457, v453, v456);
    float32x2_t v477 = vfma_f32(v475, v471, v474);
    float32x2_t v526 = vfma_f32(v524, v520, v523);
    float32x2_t v539 = vfma_f32(v537, v533, v536);
    float32x2_t v557 = vfma_f32(v555, v551, v554);
    float32x2_t v558 = vadd_f32(v64, v77);
    float32x2_t v559 = vsub_f32(v64, v77);
    float32x2_t v566 = vadd_f32(v126, v139);
    float32x2_t v567 = vsub_f32(v126, v139);
    float32x2_t v569 = vadd_f32(v206, v219);
    float32x2_t v570 = vsub_f32(v206, v219);
    float32x2_t v572 = vadd_f32(v286, v299);
    float32x2_t v573 = vsub_f32(v286, v299);
    float32x2_t v575 = vadd_f32(v366, v379);
    float32x2_t v576 = vsub_f32(v366, v379);
    float32x2_t v578 = vadd_f32(v446, v459);
    float32x2_t v579 = vsub_f32(v446, v459);
    float32x2_t v581 = vadd_f32(v526, v539);
    float32x2_t v582 = vsub_f32(v526, v539);
    float32x2_t v565 = vadd_f32(v558, v564);
    float32x2_t v568 = vadd_f32(v566, v157);
    float32x2_t v571 = vadd_f32(v569, v237);
    float32x2_t v574 = vadd_f32(v572, v317);
    float32x2_t v577 = vadd_f32(v575, v397);
    float32x2_t v580 = vadd_f32(v578, v477);
    float32x2_t v583 = vadd_f32(v581, v557);
    float32x2_t v668 = vadd_f32(v566, v581);
    float32x2_t v669 = vsub_f32(v566, v581);
    float32x2_t v670 = vadd_f32(v575, v572);
    float32x2_t v671 = vsub_f32(v575, v572);
    float32x2_t v672 = vadd_f32(v569, v578);
    float32x2_t v673 = vsub_f32(v569, v578);
    float32x2_t v752 = vadd_f32(v567, v582);
    float32x2_t v753 = vsub_f32(v567, v582);
    float32x2_t v754 = vadd_f32(v576, v573);
    float32x2_t v755 = vsub_f32(v576, v573);
    float32x2_t v756 = vadd_f32(v570, v579);
    float32x2_t v757 = vsub_f32(v570, v579);
    float32x2_t v584 = vadd_f32(v568, v583);
    float32x2_t v585 = vsub_f32(v568, v583);
    float32x2_t v586 = vadd_f32(v577, v574);
    float32x2_t v587 = vsub_f32(v577, v574);
    float32x2_t v588 = vadd_f32(v571, v580);
    float32x2_t v589 = vsub_f32(v571, v580);
    float32x2_t v674 = vadd_f32(v668, v670);
    float32x2_t v677 = vsub_f32(v668, v670);
    float32x2_t v678 = vsub_f32(v670, v672);
    float32x2_t v679 = vsub_f32(v672, v668);
    float32x2_t v680 = vadd_f32(v669, v671);
    float32x2_t v682 = vsub_f32(v669, v671);
    float32x2_t v683 = vsub_f32(v671, v673);
    float32x2_t v684 = vsub_f32(v673, v669);
    float32x2_t v758 = vadd_f32(v752, v754);
    float32x2_t v761 = vsub_f32(v752, v754);
    float32x2_t v762 = vsub_f32(v754, v756);
    float32x2_t v763 = vsub_f32(v756, v752);
    float32x2_t v764 = vadd_f32(v753, v755);
    float32x2_t v766 = vsub_f32(v753, v755);
    float32x2_t v767 = vsub_f32(v755, v757);
    float32x2_t v768 = vsub_f32(v757, v753);
    float32x2_t v590 = vadd_f32(v584, v586);
    float32x2_t v593 = vsub_f32(v584, v586);
    float32x2_t v594 = vsub_f32(v586, v588);
    float32x2_t v595 = vsub_f32(v588, v584);
    float32x2_t v596 = vadd_f32(v585, v587);
    float32x2_t v598 = vsub_f32(v585, v587);
    float32x2_t v599 = vsub_f32(v587, v589);
    float32x2_t v600 = vsub_f32(v589, v585);
    float32x2_t v675 = vadd_f32(v674, v672);
    float32x2_t v681 = vadd_f32(v680, v673);
    float32x2_t v696 = vmul_f32(v677, v695);
    float32x2_t v700 = vmul_f32(v678, v699);
    float32x2_t v704 = vmul_f32(v679, v703);
    float32x2_t v717 = vrev64_f32(v682);
    float32x2_t v724 = vrev64_f32(v683);
    float32x2_t v731 = vrev64_f32(v684);
    float32x2_t v759 = vadd_f32(v758, v756);
    float32x2_t v765 = vadd_f32(v764, v757);
    float32x2_t v788 = vrev64_f32(v761);
    float32x2_t v795 = vrev64_f32(v762);
    float32x2_t v802 = vrev64_f32(v763);
    float32x2_t v811 = vmul_f32(v766, v810);
    float32x2_t v815 = vmul_f32(v767, v814);
    float32x2_t v819 = vmul_f32(v768, v818);
    float32x2_t v591 = vadd_f32(v590, v588);
    float32x2_t v597 = vadd_f32(v596, v589);
    float32x2_t v612 = vmul_f32(v593, v611);
    float32x2_t v616 = vmul_f32(v594, v615);
    float32x2_t v620 = vmul_f32(v595, v619);
    float32x2_t v633 = vrev64_f32(v598);
    float32x2_t v640 = vrev64_f32(v599);
    float32x2_t v647 = vrev64_f32(v600);
    float32x2_t v676 = vadd_f32(v675, v558);
    float32x2_t v692 = vmul_f32(v675, v691);
    float32x2_t v710 = vrev64_f32(v681);
    float32x2_t v718 = vmul_f32(v717, v716);
    float32x2_t v725 = vmul_f32(v724, v723);
    float32x2_t v732 = vmul_f32(v731, v730);
    float32x2_t v760 = vadd_f32(v759, v559);
    float32x2_t v781 = vrev64_f32(v759);
    float32x2_t v789 = vmul_f32(v788, v787);
    float32x2_t v796 = vmul_f32(v795, v794);
    float32x2_t v803 = vmul_f32(v802, v801);
    float32x2_t v807 = vmul_f32(v765, v806);
    float32x2_t v592 = vadd_f32(v591, v565);
    float32x2_t v608 = vmul_f32(v591, v607);
    float32x2_t v626 = vrev64_f32(v597);
    float32x2_t v634 = vmul_f32(v633, v632);
    float32x2_t v641 = vmul_f32(v640, v639);
    float32x2_t v648 = vmul_f32(v647, v646);
    float32x2_t v688 = vmul_f32(v676, v687);
    float32x2_t v711 = vmul_f32(v710, v709);
    float32x2_t v774 = vrev64_f32(v760);
    float32x2_t v782 = vmul_f32(v781, v780);
    float32x2_t v827 = vadd_f32(v807, v811);
    float32x2_t v829 = vsub_f32(v807, v811);
    float32x2_t v831 = vsub_f32(v807, v815);
    float32x2_t v627 = vmul_f32(v626, v625);
    float32x2_t v649 = vadd_f32(v592, v608);
    float32x2_t v733 = vadd_f32(v688, v692);
    float32x2_t v740 = vadd_f32(v711, v718);
    float32x2_t v742 = vsub_f32(v711, v718);
    float32x2_t v744 = vsub_f32(v711, v725);
    float32x2_t v775 = vmul_f32(v774, v773);
    float32x2_t v828 = vadd_f32(v827, v815);
    float32x2_t v830 = vsub_f32(v829, v819);
    float32x2_t v832 = vadd_f32(v831, v819);
    float32x2_t v839 = vadd_f32(v592, v688);
    int16x4_t v844 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v592, 15), (int32x2_t){0, 0}));
    float32x2_t v650 = vadd_f32(v649, v612);
    float32x2_t v652 = vsub_f32(v649, v612);
    float32x2_t v654 = vsub_f32(v649, v616);
    float32x2_t v656 = vadd_f32(v627, v634);
    float32x2_t v658 = vsub_f32(v627, v634);
    float32x2_t v660 = vsub_f32(v627, v641);
    float32x2_t v734 = vadd_f32(v733, v696);
    float32x2_t v736 = vsub_f32(v733, v696);
    float32x2_t v738 = vsub_f32(v733, v700);
    float32x2_t v741 = vadd_f32(v740, v725);
    float32x2_t v743 = vsub_f32(v742, v732);
    float32x2_t v745 = vadd_f32(v744, v732);
    float32x2_t v820 = vadd_f32(v775, v782);
    float32x2_t v840 = vadd_f32(v839, v775);
    float32x2_t v841 = vsub_f32(v839, v775);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v844), 0);
    float32x2_t v651 = vadd_f32(v650, v616);
    float32x2_t v653 = vsub_f32(v652, v620);
    float32x2_t v655 = vadd_f32(v654, v620);
    float32x2_t v657 = vadd_f32(v656, v641);
    float32x2_t v659 = vsub_f32(v658, v648);
    float32x2_t v661 = vadd_f32(v660, v648);
    float32x2_t v735 = vadd_f32(v734, v700);
    float32x2_t v737 = vsub_f32(v736, v704);
    float32x2_t v739 = vadd_f32(v738, v704);
    float32x2_t v821 = vadd_f32(v820, v789);
    float32x2_t v823 = vsub_f32(v820, v789);
    float32x2_t v825 = vsub_f32(v820, v796);
    int16x4_t v850 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v841, 15), (int32x2_t){0, 0}));
    int16x4_t v856 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v840, 15), (int32x2_t){0, 0}));
    float32x2_t v662 = vadd_f32(v651, v657);
    float32x2_t v663 = vsub_f32(v651, v657);
    float32x2_t v664 = vadd_f32(v653, v659);
    float32x2_t v665 = vsub_f32(v653, v659);
    float32x2_t v666 = vadd_f32(v655, v661);
    float32x2_t v667 = vsub_f32(v655, v661);
    float32x2_t v746 = vadd_f32(v735, v741);
    float32x2_t v747 = vsub_f32(v735, v741);
    float32x2_t v748 = vadd_f32(v737, v743);
    float32x2_t v749 = vsub_f32(v737, v743);
    float32x2_t v750 = vadd_f32(v739, v745);
    float32x2_t v751 = vsub_f32(v739, v745);
    float32x2_t v822 = vadd_f32(v821, v796);
    float32x2_t v824 = vsub_f32(v823, v803);
    float32x2_t v826 = vadd_f32(v825, v803);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v850), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v856), 0);
    float32x2_t v833 = vadd_f32(v822, v828);
    float32x2_t v834 = vsub_f32(v822, v828);
    float32x2_t v835 = vadd_f32(v824, v830);
    float32x2_t v836 = vsub_f32(v824, v830);
    float32x2_t v837 = vadd_f32(v826, v832);
    float32x2_t v838 = vsub_f32(v826, v832);
    float32x2_t v860 = vadd_f32(v663, v747);
    int16x4_t v865 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v663, 15), (int32x2_t){0, 0}));
    float32x2_t v881 = vadd_f32(v665, v749);
    int16x4_t v886 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v665, 15), (int32x2_t){0, 0}));
    float32x2_t v902 = vadd_f32(v666, v750);
    int16x4_t v907 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v666, 15), (int32x2_t){0, 0}));
    float32x2_t v923 = vadd_f32(v667, v751);
    int16x4_t v928 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v667, 15), (int32x2_t){0, 0}));
    float32x2_t v944 = vadd_f32(v664, v748);
    int16x4_t v949 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v664, 15), (int32x2_t){0, 0}));
    float32x2_t v965 = vadd_f32(v662, v746);
    int16x4_t v970 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v662, 15), (int32x2_t){0, 0}));
    float32x2_t v861 = vadd_f32(v860, v834);
    float32x2_t v862 = vsub_f32(v860, v834);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v865), 0);
    float32x2_t v882 = vadd_f32(v881, v836);
    float32x2_t v883 = vsub_f32(v881, v836);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v886), 0);
    float32x2_t v903 = vadd_f32(v902, v837);
    float32x2_t v904 = vsub_f32(v902, v837);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v907), 0);
    float32x2_t v924 = vadd_f32(v923, v838);
    float32x2_t v925 = vsub_f32(v923, v838);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v928), 0);
    float32x2_t v945 = vadd_f32(v944, v835);
    float32x2_t v946 = vsub_f32(v944, v835);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v949), 0);
    float32x2_t v966 = vadd_f32(v965, v833);
    float32x2_t v967 = vsub_f32(v965, v833);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v970), 0);
    int16x4_t v871 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v862, 15), (int32x2_t){0, 0}));
    int16x4_t v877 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v861, 15), (int32x2_t){0, 0}));
    int16x4_t v892 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v883, 15), (int32x2_t){0, 0}));
    int16x4_t v898 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v882, 15), (int32x2_t){0, 0}));
    int16x4_t v913 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v904, 15), (int32x2_t){0, 0}));
    int16x4_t v919 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v903, 15), (int32x2_t){0, 0}));
    int16x4_t v934 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v925, 15), (int32x2_t){0, 0}));
    int16x4_t v940 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v924, 15), (int32x2_t){0, 0}));
    int16x4_t v955 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v946, 15), (int32x2_t){0, 0}));
    int16x4_t v961 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v945, 15), (int32x2_t){0, 0}));
    int16x4_t v976 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v967, 15), (int32x2_t){0, 0}));
    int16x4_t v982 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v966, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v871), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v877), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v892), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v898), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v913), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v919), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v934), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v940), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v955), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v961), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v976), 0);
    v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v982), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu21(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v447 = -1.1666666666666665e+00F;
    float v452 = 7.9015646852540022e-01F;
    float v457 = 5.5854267289647742e-02F;
    float v462 = 7.3430220123575241e-01F;
    float v467 = -4.4095855184409838e-01F;
    float v474 = -3.4087293062393137e-01F;
    float v481 = 5.3396936033772524e-01F;
    float v488 = -8.7484229096165667e-01F;
    float v531 = -1.4999999999999998e+00F;
    float v536 = 1.7499999999999996e+00F;
    float v541 = -1.1852347027881001e+00F;
    float v546 = -8.3781400934471603e-02F;
    float v551 = -1.1014533018536286e+00F;
    float v556 = 6.6143782776614746e-01F;
    float v563 = 5.1130939593589697e-01F;
    float v570 = -8.0095404050658769e-01F;
    float v577 = 1.3122634364424848e+00F;
    float v620 = -8.6602540378443871e-01F;
    float v627 = 1.0103629710818451e+00F;
    float v634 = -6.8429557470759583e-01F;
    float v641 = -4.8371214382601155e-02F;
    float v648 = -6.3592436032499466e-01F;
    float v655 = -3.8188130791298663e-01F;
    float v660 = -2.9520461738277515e-01F;
    float v665 = 4.6243103089499693e-01F;
    float v670 = -7.5763564827777208e-01F;
    const float32x2_t *v1014 = &v5[v0];
    int32_t *v1144 = &v6[v2];
    int64_t v19 = v0 * 7;
    int64_t v33 = v0 * 14;
    int64_t v48 = v10 * 6;
    int64_t v55 = v10 * 13;
    int64_t v61 = v0 * 10;
    int64_t v75 = v0 * 17;
    int64_t v90 = v10 * 9;
    int64_t v97 = v10 * 16;
    int64_t v103 = v0 * 3;
    int64_t v111 = v10 * 2;
    int64_t v117 = v0 * 13;
    int64_t v131 = v0 * 20;
    int64_t v146 = v10 * 12;
    int64_t v153 = v10 * 19;
    int64_t v159 = v0 * 6;
    int64_t v167 = v10 * 5;
    int64_t v173 = v0 * 16;
    int64_t v187 = v0 * 2;
    int64_t v202 = v10 * 15;
    int64_t v215 = v0 * 9;
    int64_t v223 = v10 * 8;
    int64_t v229 = v0 * 19;
    int64_t v243 = v0 * 5;
    int64_t v258 = v10 * 18;
    int64_t v265 = v10 * 4;
    int64_t v271 = v0 * 12;
    int64_t v279 = v10 * 11;
    int64_t v299 = v0 * 8;
    int64_t v321 = v10 * 7;
    int64_t v327 = v0 * 15;
    int64_t v335 = v10 * 14;
    int64_t v341 = v0 * 4;
    int64_t v355 = v0 * 11;
    int64_t v370 = v10 * 3;
    int64_t v377 = v10 * 10;
    int64_t v383 = v0 * 18;
    int64_t v391 = v10 * 17;
    int64_t v392 = v13 * 20;
    float v470 = v4 * v467;
    float v477 = v4 * v474;
    float v484 = v4 * v481;
    float v491 = v4 * v488;
    float v559 = v4 * v556;
    float v566 = v4 * v563;
    float v573 = v4 * v570;
    float v580 = v4 * v577;
    float v623 = v4 * v620;
    float v630 = v4 * v627;
    float v637 = v4 * v634;
    float v644 = v4 * v641;
    float v651 = v4 * v648;
    int64_t v705 = v2 * 7;
    int64_t v713 = v2 * 14;
    int64_t v724 = v2 * 15;
    int64_t v740 = v2 * 8;
    int64_t v751 = v2 * 9;
    int64_t v759 = v2 * 16;
    int64_t v767 = v2 * 2;
    int64_t v778 = v2 * 3;
    int64_t v786 = v2 * 10;
    int64_t v794 = v2 * 17;
    int64_t v805 = v2 * 18;
    int64_t v813 = v2 * 4;
    int64_t v821 = v2 * 11;
    int64_t v832 = v2 * 12;
    int64_t v840 = v2 * 19;
    int64_t v848 = v2 * 5;
    int64_t v859 = v2 * 6;
    int64_t v867 = v2 * 13;
    int64_t v875 = v2 * 20;
    const float32x2_t *v1071 = &v5[0];
    svint64_t v1072 = svindex_s64(0, v1);
    svfloat32_t v1075 = svdup_n_f32(v447);
    svfloat32_t v1076 = svdup_n_f32(v452);
    svfloat32_t v1077 = svdup_n_f32(v457);
    svfloat32_t v1078 = svdup_n_f32(v462);
    svfloat32_t v1083 = svdup_n_f32(v531);
    svfloat32_t v1084 = svdup_n_f32(v536);
    svfloat32_t v1085 = svdup_n_f32(v541);
    svfloat32_t v1086 = svdup_n_f32(v546);
    svfloat32_t v1087 = svdup_n_f32(v551);
    svfloat32_t v1097 = svdup_n_f32(v655);
    svfloat32_t v1098 = svdup_n_f32(v660);
    svfloat32_t v1099 = svdup_n_f32(v665);
    svfloat32_t v1100 = svdup_n_f32(v670);
    int32_t *v1108 = &v6[0];
    int64_t v50 = v48 + v392;
    int64_t v57 = v55 + v392;
    int64_t v92 = v90 + v392;
    int64_t v99 = v97 + v392;
    int64_t v113 = v111 + v392;
    int64_t v148 = v146 + v392;
    int64_t v155 = v153 + v392;
    int64_t v169 = v167 + v392;
    int64_t v204 = v202 + v392;
    int64_t v211 = v10 + v392;
    int64_t v225 = v223 + v392;
    int64_t v260 = v258 + v392;
    int64_t v267 = v265 + v392;
    int64_t v281 = v279 + v392;
    svfloat32_t v317 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v392]));
    int64_t v323 = v321 + v392;
    int64_t v337 = v335 + v392;
    int64_t v372 = v370 + v392;
    int64_t v379 = v377 + v392;
    int64_t v393 = v391 + v392;
    const float32x2_t *v888 = &v5[v19];
    const float32x2_t *v897 = &v5[v33];
    const float32x2_t *v906 = &v5[v61];
    const float32x2_t *v915 = &v5[v75];
    const float32x2_t *v924 = &v5[v103];
    const float32x2_t *v933 = &v5[v117];
    const float32x2_t *v942 = &v5[v131];
    const float32x2_t *v951 = &v5[v159];
    const float32x2_t *v960 = &v5[v173];
    const float32x2_t *v969 = &v5[v187];
    const float32x2_t *v978 = &v5[v215];
    const float32x2_t *v987 = &v5[v229];
    const float32x2_t *v996 = &v5[v243];
    const float32x2_t *v1005 = &v5[v271];
    svfloat32_t v1016 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1014), v1072));
    const float32x2_t *v1024 = &v5[v299];
    const float32x2_t *v1034 = &v5[v327];
    const float32x2_t *v1043 = &v5[v341];
    const float32x2_t *v1052 = &v5[v355];
    const float32x2_t *v1061 = &v5[v383];
    svfloat32_t v1073 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1071), v1072));
    svfloat32_t v1079 = svdup_n_f32(v470);
    svfloat32_t v1080 = svdup_n_f32(v477);
    svfloat32_t v1081 = svdup_n_f32(v484);
    svfloat32_t v1082 = svdup_n_f32(v491);
    svfloat32_t v1088 = svdup_n_f32(v559);
    svfloat32_t v1089 = svdup_n_f32(v566);
    svfloat32_t v1090 = svdup_n_f32(v573);
    svfloat32_t v1091 = svdup_n_f32(v580);
    svfloat32_t v1092 = svdup_n_f32(v623);
    svfloat32_t v1093 = svdup_n_f32(v630);
    svfloat32_t v1094 = svdup_n_f32(v637);
    svfloat32_t v1095 = svdup_n_f32(v644);
    svfloat32_t v1096 = svdup_n_f32(v651);
    int32_t *v1117 = &v6[v705];
    int32_t *v1126 = &v6[v713];
    int32_t *v1135 = &v6[v724];
    int32_t *v1153 = &v6[v740];
    int32_t *v1162 = &v6[v751];
    int32_t *v1171 = &v6[v759];
    int32_t *v1180 = &v6[v767];
    int32_t *v1189 = &v6[v778];
    int32_t *v1198 = &v6[v786];
    int32_t *v1207 = &v6[v794];
    int32_t *v1216 = &v6[v805];
    int32_t *v1225 = &v6[v813];
    int32_t *v1234 = &v6[v821];
    int32_t *v1243 = &v6[v832];
    int32_t *v1252 = &v6[v840];
    int32_t *v1261 = &v6[v848];
    int32_t *v1270 = &v6[v859];
    int32_t *v1279 = &v6[v867];
    int32_t *v1288 = &v6[v875];
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v50]));
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v149 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v148]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t zero318 = svdup_n_f32(0);
    svfloat32_t v318 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero318, v1016, v317, 0), v1016,
        v317, 90);
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v338 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v337]));
    svfloat32_t v373 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v372]));
    svfloat32_t v380 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v379]));
    svfloat32_t v394 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v393]));
    svfloat32_t v890 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v888), v1072));
    svfloat32_t v899 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v897), v1072));
    svfloat32_t v908 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v906), v1072));
    svfloat32_t v917 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v915), v1072));
    svfloat32_t v926 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v924), v1072));
    svfloat32_t v935 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v933), v1072));
    svfloat32_t v944 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v942), v1072));
    svfloat32_t v953 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v951), v1072));
    svfloat32_t v962 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v960), v1072));
    svfloat32_t v971 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v969), v1072));
    svfloat32_t v980 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v978), v1072));
    svfloat32_t v989 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v987), v1072));
    svfloat32_t v998 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v996), v1072));
    svfloat32_t v1007 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1005), v1072));
    svfloat32_t v1026 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1024), v1072));
    svfloat32_t v1036 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1034), v1072));
    svfloat32_t v1045 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1043), v1072));
    svfloat32_t v1054 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1052), v1072));
    svfloat32_t v1063 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1061), v1072));
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v890, v51, 0),
                     v890, v51, 90);
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v899, v58, 0),
                     v899, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v908, v93, 0),
                     v908, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v917, v100, 0),
                     v917, v100, 90);
    svfloat32_t zero150 = svdup_n_f32(0);
    svfloat32_t v150 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero150, v935, v149, 0),
                     v935, v149, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v944, v156, 0),
                     v944, v156, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v962, v205, 0),
                     v962, v205, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v971, v212, 0),
                     v971, v212, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero262, v989, v261, 0),
                     v989, v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero269, v998, v268, 0),
                     v998, v268, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero325, v1026, v324, 0), v1026,
        v324, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero374, v1045, v373, 0), v1045,
        v373, 90);
    svfloat32_t zero381 = svdup_n_f32(0);
    svfloat32_t v381 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero381, v1054, v380, 0), v1054,
        v380, 90);
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v410 = svsub_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v413 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v415 = svadd_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v418 = svadd_f32_x(svptrue_b32(), v318, v325);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v318, v325);
    svfloat32_t v421 = svadd_f32_x(svptrue_b32(), v374, v381);
    svfloat32_t v422 = svsub_f32_x(svptrue_b32(), v374, v381);
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v396, v1073);
    svfloat32_t v408 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v406, v926, v114, 0),
                     v926, v114, 90);
    svfloat32_t v411 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v409, v953, v170, 0),
                     v953, v170, 90);
    svfloat32_t v414 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v412, v980, v226, 0),
                     v980, v226, 90);
    svfloat32_t v417 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v415, v1007, v282, 0),
                     v1007, v282, 90);
    svfloat32_t v420 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v418, v1036, v338, 0),
                     v1036, v338, 90);
    svfloat32_t v423 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v421, v1063, v394, 0),
                     v1063, v394, 90);
    svfloat32_t v513 = svadd_f32_x(svptrue_b32(), v406, v421);
    svfloat32_t v514 = svsub_f32_x(svptrue_b32(), v406, v421);
    svfloat32_t v515 = svadd_f32_x(svptrue_b32(), v415, v412);
    svfloat32_t v516 = svsub_f32_x(svptrue_b32(), v415, v412);
    svfloat32_t v517 = svadd_f32_x(svptrue_b32(), v409, v418);
    svfloat32_t v518 = svsub_f32_x(svptrue_b32(), v409, v418);
    svfloat32_t v602 = svadd_f32_x(svptrue_b32(), v407, v422);
    svfloat32_t v603 = svsub_f32_x(svptrue_b32(), v407, v422);
    svfloat32_t v604 = svadd_f32_x(svptrue_b32(), v416, v413);
    svfloat32_t v605 = svsub_f32_x(svptrue_b32(), v416, v413);
    svfloat32_t v606 = svadd_f32_x(svptrue_b32(), v410, v419);
    svfloat32_t v607 = svsub_f32_x(svptrue_b32(), v410, v419);
    svfloat32_t v424 = svadd_f32_x(svptrue_b32(), v408, v423);
    svfloat32_t v425 = svsub_f32_x(svptrue_b32(), v408, v423);
    svfloat32_t v426 = svadd_f32_x(svptrue_b32(), v417, v414);
    svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v417, v414);
    svfloat32_t v428 = svadd_f32_x(svptrue_b32(), v411, v420);
    svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v411, v420);
    svfloat32_t v519 = svadd_f32_x(svptrue_b32(), v513, v515);
    svfloat32_t v522 = svsub_f32_x(svptrue_b32(), v513, v515);
    svfloat32_t v523 = svsub_f32_x(svptrue_b32(), v515, v517);
    svfloat32_t v524 = svsub_f32_x(svptrue_b32(), v517, v513);
    svfloat32_t v525 = svadd_f32_x(svptrue_b32(), v514, v516);
    svfloat32_t v527 = svsub_f32_x(svptrue_b32(), v514, v516);
    svfloat32_t v528 = svsub_f32_x(svptrue_b32(), v516, v518);
    svfloat32_t v529 = svsub_f32_x(svptrue_b32(), v518, v514);
    svfloat32_t v608 = svadd_f32_x(svptrue_b32(), v602, v604);
    svfloat32_t v611 = svsub_f32_x(svptrue_b32(), v602, v604);
    svfloat32_t v612 = svsub_f32_x(svptrue_b32(), v604, v606);
    svfloat32_t v613 = svsub_f32_x(svptrue_b32(), v606, v602);
    svfloat32_t v614 = svadd_f32_x(svptrue_b32(), v603, v605);
    svfloat32_t v616 = svsub_f32_x(svptrue_b32(), v603, v605);
    svfloat32_t v617 = svsub_f32_x(svptrue_b32(), v605, v607);
    svfloat32_t v618 = svsub_f32_x(svptrue_b32(), v607, v603);
    svfloat32_t v430 = svadd_f32_x(svptrue_b32(), v424, v426);
    svfloat32_t v433 = svsub_f32_x(svptrue_b32(), v424, v426);
    svfloat32_t v434 = svsub_f32_x(svptrue_b32(), v426, v428);
    svfloat32_t v435 = svsub_f32_x(svptrue_b32(), v428, v424);
    svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v425, v427);
    svfloat32_t v438 = svsub_f32_x(svptrue_b32(), v425, v427);
    svfloat32_t v439 = svsub_f32_x(svptrue_b32(), v427, v429);
    svfloat32_t v440 = svsub_f32_x(svptrue_b32(), v429, v425);
    svfloat32_t v520 = svadd_f32_x(svptrue_b32(), v519, v517);
    svfloat32_t v526 = svadd_f32_x(svptrue_b32(), v525, v518);
    svfloat32_t zero568 = svdup_n_f32(0);
    svfloat32_t v568 = svcmla_f32_x(pred_full, zero568, v1089, v527, 90);
    svfloat32_t zero575 = svdup_n_f32(0);
    svfloat32_t v575 = svcmla_f32_x(pred_full, zero575, v1090, v528, 90);
    svfloat32_t zero582 = svdup_n_f32(0);
    svfloat32_t v582 = svcmla_f32_x(pred_full, zero582, v1091, v529, 90);
    svfloat32_t v609 = svadd_f32_x(svptrue_b32(), v608, v606);
    svfloat32_t v615 = svadd_f32_x(svptrue_b32(), v614, v607);
    svfloat32_t zero639 = svdup_n_f32(0);
    svfloat32_t v639 = svcmla_f32_x(pred_full, zero639, v1094, v611, 90);
    svfloat32_t zero646 = svdup_n_f32(0);
    svfloat32_t v646 = svcmla_f32_x(pred_full, zero646, v1095, v612, 90);
    svfloat32_t zero653 = svdup_n_f32(0);
    svfloat32_t v653 = svcmla_f32_x(pred_full, zero653, v1096, v613, 90);
    svfloat32_t v663 = svmul_f32_x(svptrue_b32(), v616, v1098);
    svfloat32_t v668 = svmul_f32_x(svptrue_b32(), v617, v1099);
    svfloat32_t v431 = svadd_f32_x(svptrue_b32(), v430, v428);
    svfloat32_t v437 = svadd_f32_x(svptrue_b32(), v436, v429);
    svfloat32_t zero479 = svdup_n_f32(0);
    svfloat32_t v479 = svcmla_f32_x(pred_full, zero479, v1080, v438, 90);
    svfloat32_t zero486 = svdup_n_f32(0);
    svfloat32_t v486 = svcmla_f32_x(pred_full, zero486, v1081, v439, 90);
    svfloat32_t zero493 = svdup_n_f32(0);
    svfloat32_t v493 = svcmla_f32_x(pred_full, zero493, v1082, v440, 90);
    svfloat32_t v521 = svadd_f32_x(svptrue_b32(), v520, v396);
    svfloat32_t v539 = svmul_f32_x(svptrue_b32(), v520, v1084);
    svfloat32_t zero561 = svdup_n_f32(0);
    svfloat32_t v561 = svcmla_f32_x(pred_full, zero561, v1088, v526, 90);
    svfloat32_t v610 = svadd_f32_x(svptrue_b32(), v609, v397);
    svfloat32_t v432 = svadd_f32_x(svptrue_b32(), v431, v405);
    svfloat32_t zero472 = svdup_n_f32(0);
    svfloat32_t v472 = svcmla_f32_x(pred_full, zero472, v1079, v437, 90);
    svfloat32_t v590 = svadd_f32_x(svptrue_b32(), v561, v568);
    svfloat32_t v592 = svsub_f32_x(svptrue_b32(), v561, v568);
    svfloat32_t v594 = svsub_f32_x(svptrue_b32(), v561, v575);
    svfloat32_t zero625 = svdup_n_f32(0);
    svfloat32_t v625 = svcmla_f32_x(pred_full, zero625, v1092, v610, 90);
    svfloat32_t v681 = svmla_f32_x(pred_full, v663, v615, v1097);
    svfloat32_t v683 = svnmls_f32_x(pred_full, v663, v615, v1097);
    svfloat32_t v685 = svnmls_f32_x(pred_full, v668, v615, v1097);
    svfloat32_t v494 = svmla_f32_x(pred_full, v432, v431, v1075);
    svfloat32_t v501 = svadd_f32_x(svptrue_b32(), v472, v479);
    svfloat32_t v503 = svsub_f32_x(svptrue_b32(), v472, v479);
    svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v472, v486);
    svfloat32_t v583 = svmla_f32_x(pred_full, v539, v521, v1083);
    svfloat32_t v591 = svadd_f32_x(svptrue_b32(), v590, v575);
    svfloat32_t v593 = svsub_f32_x(svptrue_b32(), v592, v582);
    svfloat32_t v595 = svadd_f32_x(svptrue_b32(), v594, v582);
    svfloat32_t v674 = svcmla_f32_x(pred_full, v625, v1093, v609, 90);
    svfloat32_t v682 = svmla_f32_x(pred_full, v681, v617, v1099);
    svfloat32_t v684 = svmls_f32_x(pred_full, v683, v618, v1100);
    svfloat32_t v686 = svmla_f32_x(pred_full, v685, v618, v1100);
    svfloat32_t v693 = svmla_f32_x(pred_full, v432, v521, v1083);
    svint16_t v698 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v432, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v495 = svmla_f32_x(pred_full, v494, v433, v1076);
    svfloat32_t v497 = svmls_f32_x(pred_full, v494, v433, v1076);
    svfloat32_t v499 = svmls_f32_x(pred_full, v494, v434, v1077);
    svfloat32_t v502 = svadd_f32_x(svptrue_b32(), v501, v486);
    svfloat32_t v504 = svsub_f32_x(svptrue_b32(), v503, v493);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v505, v493);
    svfloat32_t v584 = svmla_f32_x(pred_full, v583, v522, v1085);
    svfloat32_t v586 = svmls_f32_x(pred_full, v583, v522, v1085);
    svfloat32_t v588 = svmls_f32_x(pred_full, v583, v523, v1086);
    svfloat32_t v675 = svadd_f32_x(svptrue_b32(), v674, v639);
    svfloat32_t v677 = svsub_f32_x(svptrue_b32(), v674, v639);
    svfloat32_t v679 = svsub_f32_x(svptrue_b32(), v674, v646);
    svfloat32_t v694 = svadd_f32_x(svptrue_b32(), v693, v625);
    svfloat32_t v695 = svsub_f32_x(svptrue_b32(), v693, v625);
    svst1w_u64(pred_full, (unsigned *)(v1108), svreinterpret_u64_s16(v698));
    svfloat32_t v496 = svmla_f32_x(pred_full, v495, v434, v1077);
    svfloat32_t v498 = svmls_f32_x(pred_full, v497, v435, v1078);
    svfloat32_t v500 = svmla_f32_x(pred_full, v499, v435, v1078);
    svfloat32_t v585 = svmla_f32_x(pred_full, v584, v523, v1086);
    svfloat32_t v587 = svmls_f32_x(pred_full, v586, v524, v1087);
    svfloat32_t v589 = svmla_f32_x(pred_full, v588, v524, v1087);
    svfloat32_t v676 = svadd_f32_x(svptrue_b32(), v675, v646);
    svfloat32_t v678 = svsub_f32_x(svptrue_b32(), v677, v653);
    svfloat32_t v680 = svadd_f32_x(svptrue_b32(), v679, v653);
    svint16_t v706 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v695, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v714 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v694, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v507 = svadd_f32_x(svptrue_b32(), v496, v502);
    svfloat32_t v508 = svsub_f32_x(svptrue_b32(), v496, v502);
    svfloat32_t v509 = svadd_f32_x(svptrue_b32(), v498, v504);
    svfloat32_t v510 = svsub_f32_x(svptrue_b32(), v498, v504);
    svfloat32_t v511 = svadd_f32_x(svptrue_b32(), v500, v506);
    svfloat32_t v512 = svsub_f32_x(svptrue_b32(), v500, v506);
    svfloat32_t v596 = svadd_f32_x(svptrue_b32(), v585, v591);
    svfloat32_t v597 = svsub_f32_x(svptrue_b32(), v585, v591);
    svfloat32_t v598 = svadd_f32_x(svptrue_b32(), v587, v593);
    svfloat32_t v599 = svsub_f32_x(svptrue_b32(), v587, v593);
    svfloat32_t v600 = svadd_f32_x(svptrue_b32(), v589, v595);
    svfloat32_t v601 = svsub_f32_x(svptrue_b32(), v589, v595);
    svfloat32_t v687 = svadd_f32_x(svptrue_b32(), v676, v682);
    svfloat32_t v688 = svsub_f32_x(svptrue_b32(), v676, v682);
    svfloat32_t v689 = svadd_f32_x(svptrue_b32(), v678, v684);
    svfloat32_t v690 = svsub_f32_x(svptrue_b32(), v678, v684);
    svfloat32_t v691 = svadd_f32_x(svptrue_b32(), v680, v686);
    svfloat32_t v692 = svsub_f32_x(svptrue_b32(), v680, v686);
    svst1w_u64(pred_full, (unsigned *)(v1117), svreinterpret_u64_s16(v706));
    svst1w_u64(pred_full, (unsigned *)(v1126), svreinterpret_u64_s16(v714));
    svfloat32_t v720 = svadd_f32_x(svptrue_b32(), v508, v597);
    svint16_t v725 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v508, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v747 = svadd_f32_x(svptrue_b32(), v510, v599);
    svint16_t v752 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v510, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v774 = svadd_f32_x(svptrue_b32(), v511, v600);
    svint16_t v779 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v511, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v801 = svadd_f32_x(svptrue_b32(), v512, v601);
    svint16_t v806 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v512, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v828 = svadd_f32_x(svptrue_b32(), v509, v598);
    svint16_t v833 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v509, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v855 = svadd_f32_x(svptrue_b32(), v507, v596);
    svint16_t v860 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v507, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v721 = svadd_f32_x(svptrue_b32(), v720, v688);
    svfloat32_t v722 = svsub_f32_x(svptrue_b32(), v720, v688);
    svfloat32_t v748 = svadd_f32_x(svptrue_b32(), v747, v690);
    svfloat32_t v749 = svsub_f32_x(svptrue_b32(), v747, v690);
    svfloat32_t v775 = svadd_f32_x(svptrue_b32(), v774, v691);
    svfloat32_t v776 = svsub_f32_x(svptrue_b32(), v774, v691);
    svfloat32_t v802 = svadd_f32_x(svptrue_b32(), v801, v692);
    svfloat32_t v803 = svsub_f32_x(svptrue_b32(), v801, v692);
    svfloat32_t v829 = svadd_f32_x(svptrue_b32(), v828, v689);
    svfloat32_t v830 = svsub_f32_x(svptrue_b32(), v828, v689);
    svfloat32_t v856 = svadd_f32_x(svptrue_b32(), v855, v687);
    svfloat32_t v857 = svsub_f32_x(svptrue_b32(), v855, v687);
    svst1w_u64(pred_full, (unsigned *)(v1135), svreinterpret_u64_s16(v725));
    svst1w_u64(pred_full, (unsigned *)(v1162), svreinterpret_u64_s16(v752));
    svst1w_u64(pred_full, (unsigned *)(v1189), svreinterpret_u64_s16(v779));
    svst1w_u64(pred_full, (unsigned *)(v1216), svreinterpret_u64_s16(v806));
    svst1w_u64(pred_full, (unsigned *)(v1243), svreinterpret_u64_s16(v833));
    svst1w_u64(pred_full, (unsigned *)(v1270), svreinterpret_u64_s16(v860));
    svint16_t v733 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v722, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v741 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v721, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v760 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v749, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v768 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v748, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v787 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v776, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v795 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v775, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v814 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v803, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v822 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v802, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v841 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v830, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v849 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v829, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v868 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v857, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v876 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v856, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1144), svreinterpret_u64_s16(v733));
    svst1w_u64(pred_full, (unsigned *)(v1153), svreinterpret_u64_s16(v741));
    svst1w_u64(pred_full, (unsigned *)(v1171), svreinterpret_u64_s16(v760));
    svst1w_u64(pred_full, (unsigned *)(v1180), svreinterpret_u64_s16(v768));
    svst1w_u64(pred_full, (unsigned *)(v1198), svreinterpret_u64_s16(v787));
    svst1w_u64(pred_full, (unsigned *)(v1207), svreinterpret_u64_s16(v795));
    svst1w_u64(pred_full, (unsigned *)(v1225), svreinterpret_u64_s16(v814));
    svst1w_u64(pred_full, (unsigned *)(v1234), svreinterpret_u64_s16(v822));
    svst1w_u64(pred_full, (unsigned *)(v1252), svreinterpret_u64_s16(v841));
    svst1w_u64(pred_full, (unsigned *)(v1261), svreinterpret_u64_s16(v849));
    svst1w_u64(pred_full, (unsigned *)(v1279), svreinterpret_u64_s16(v868));
    svst1w_u64(pred_full, (unsigned *)(v1288), svreinterpret_u64_s16(v876));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu22(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v379 = v5[istride];
    float v934 = 1.1000000000000001e+00F;
    float v937 = 3.3166247903554003e-01F;
    float v938 = -3.3166247903554003e-01F;
    float v945 = 5.1541501300188641e-01F;
    float v949 = 9.4125353283118118e-01F;
    float v953 = 1.4143537075597825e+00F;
    float v957 = 8.5949297361449750e-01F;
    float v961 = 4.2314838273285138e-02F;
    float v965 = 3.8639279888589606e-01F;
    float v969 = 5.1254589567200015e-01F;
    float v973 = 1.0702757469471715e+00F;
    float v977 = 5.5486073394528512e-01F;
    float v980 = 1.2412944743900585e+00F;
    float v981 = -1.2412944743900585e+00F;
    float v987 = 2.0897833842005756e-01F;
    float v988 = -2.0897833842005756e-01F;
    float v994 = 3.7415717312460811e-01F;
    float v995 = -3.7415717312460811e-01F;
    float v1001 = 4.9929922194110327e-02F;
    float v1002 = -4.9929922194110327e-02F;
    float v1008 = 6.5815896284539266e-01F;
    float v1009 = -6.5815896284539266e-01F;
    float v1015 = 6.3306543373877577e-01F;
    float v1016 = -6.3306543373877577e-01F;
    float v1022 = 1.0822460581641109e+00F;
    float v1023 = -1.0822460581641109e+00F;
    float v1029 = 8.1720737907134022e-01F;
    float v1030 = -8.1720737907134022e-01F;
    float v1036 = 4.2408709531871824e-01F;
    float v1037 = -4.2408709531871824e-01F;
    float32x2_t v1039 = (float32x2_t){v4, v4};
    float32x2_t v411 = vtrn1_f32(v379, v379);
    float32x2_t v412 = vtrn2_f32(v379, v379);
    float32x2_t v671 = v5[0];
    float32x2_t v935 = (float32x2_t){v934, v934};
    float32x2_t v939 = (float32x2_t){v937, v938};
    float32x2_t v946 = (float32x2_t){v945, v945};
    float32x2_t v950 = (float32x2_t){v949, v949};
    float32x2_t v954 = (float32x2_t){v953, v953};
    float32x2_t v958 = (float32x2_t){v957, v957};
    float32x2_t v962 = (float32x2_t){v961, v961};
    float32x2_t v966 = (float32x2_t){v965, v965};
    float32x2_t v970 = (float32x2_t){v969, v969};
    float32x2_t v974 = (float32x2_t){v973, v973};
    float32x2_t v978 = (float32x2_t){v977, v977};
    float32x2_t v982 = (float32x2_t){v980, v981};
    float32x2_t v989 = (float32x2_t){v987, v988};
    float32x2_t v996 = (float32x2_t){v994, v995};
    float32x2_t v1003 = (float32x2_t){v1001, v1002};
    float32x2_t v1010 = (float32x2_t){v1008, v1009};
    float32x2_t v1017 = (float32x2_t){v1015, v1016};
    float32x2_t v1024 = (float32x2_t){v1022, v1023};
    float32x2_t v1031 = (float32x2_t){v1029, v1030};
    float32x2_t v1038 = (float32x2_t){v1036, v1037};
    float32x2_t v20 = v5[istride * 11];
    int64_t v37 = 20 + j * 42;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 13];
    int64_t v86 = 2 + j * 42;
    int64_t v99 = 24 + j * 42;
    float32x2_t v113 = v5[istride * 4];
    float32x2_t v131 = v5[istride * 15];
    int64_t v148 = 6 + j * 42;
    int64_t v161 = 28 + j * 42;
    float32x2_t v175 = v5[istride * 6];
    float32x2_t v193 = v5[istride * 17];
    int64_t v210 = 10 + j * 42;
    int64_t v223 = 32 + j * 42;
    float32x2_t v237 = v5[istride * 8];
    float32x2_t v255 = v5[istride * 19];
    int64_t v272 = 14 + j * 42;
    int64_t v285 = 36 + j * 42;
    float32x2_t v299 = v5[istride * 10];
    float32x2_t v317 = v5[istride * 21];
    int64_t v334 = 18 + j * 42;
    int64_t v347 = 40 + j * 42;
    float32x2_t v361 = v5[istride * 12];
    int64_t v396 = 22 + j * 42;
    float32x2_t v410 = v7[j * 42];
    int64_t v414 = j * 42 + 1;
    float32x2_t v423 = v5[istride * 14];
    float32x2_t v441 = v5[istride * 3];
    int64_t v458 = 26 + j * 42;
    int64_t v471 = 4 + j * 42;
    float32x2_t v485 = v5[istride * 16];
    float32x2_t v503 = v5[istride * 5];
    int64_t v520 = 30 + j * 42;
    int64_t v533 = 8 + j * 42;
    float32x2_t v547 = v5[istride * 18];
    float32x2_t v565 = v5[istride * 7];
    int64_t v582 = 34 + j * 42;
    int64_t v595 = 12 + j * 42;
    float32x2_t v609 = v5[istride * 20];
    float32x2_t v627 = v5[istride * 9];
    int64_t v644 = 38 + j * 42;
    int64_t v657 = 16 + j * 42;
    float32x2_t v941 = vmul_f32(v1039, v939);
    float32x2_t v984 = vmul_f32(v1039, v982);
    float32x2_t v991 = vmul_f32(v1039, v989);
    float32x2_t v998 = vmul_f32(v1039, v996);
    float32x2_t v1005 = vmul_f32(v1039, v1003);
    float32x2_t v1012 = vmul_f32(v1039, v1010);
    float32x2_t v1019 = vmul_f32(v1039, v1017);
    float32x2_t v1026 = vmul_f32(v1039, v1024);
    float32x2_t v1033 = vmul_f32(v1039, v1031);
    float32x2_t v1040 = vmul_f32(v1039, v1038);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v273 = v7[v272];
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    int64_t v277 = v272 + 1;
    float32x2_t v286 = v7[v285];
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    int64_t v290 = v285 + 1;
    float32x2_t v335 = v7[v334];
    float32x2_t v336 = vtrn1_f32(v299, v299);
    float32x2_t v337 = vtrn2_f32(v299, v299);
    int64_t v339 = v334 + 1;
    float32x2_t v348 = v7[v347];
    float32x2_t v349 = vtrn1_f32(v317, v317);
    float32x2_t v350 = vtrn2_f32(v317, v317);
    int64_t v352 = v347 + 1;
    float32x2_t v397 = v7[v396];
    float32x2_t v398 = vtrn1_f32(v361, v361);
    float32x2_t v399 = vtrn2_f32(v361, v361);
    int64_t v401 = v396 + 1;
    float32x2_t v415 = v7[v414];
    float32x2_t v416 = vmul_f32(v411, v410);
    float32x2_t v459 = v7[v458];
    float32x2_t v460 = vtrn1_f32(v423, v423);
    float32x2_t v461 = vtrn2_f32(v423, v423);
    int64_t v463 = v458 + 1;
    float32x2_t v472 = v7[v471];
    float32x2_t v473 = vtrn1_f32(v441, v441);
    float32x2_t v474 = vtrn2_f32(v441, v441);
    int64_t v476 = v471 + 1;
    float32x2_t v521 = v7[v520];
    float32x2_t v522 = vtrn1_f32(v485, v485);
    float32x2_t v523 = vtrn2_f32(v485, v485);
    int64_t v525 = v520 + 1;
    float32x2_t v534 = v7[v533];
    float32x2_t v535 = vtrn1_f32(v503, v503);
    float32x2_t v536 = vtrn2_f32(v503, v503);
    int64_t v538 = v533 + 1;
    float32x2_t v583 = v7[v582];
    float32x2_t v584 = vtrn1_f32(v547, v547);
    float32x2_t v585 = vtrn2_f32(v547, v547);
    int64_t v587 = v582 + 1;
    float32x2_t v596 = v7[v595];
    float32x2_t v597 = vtrn1_f32(v565, v565);
    float32x2_t v598 = vtrn2_f32(v565, v565);
    int64_t v600 = v595 + 1;
    float32x2_t v645 = v7[v644];
    float32x2_t v646 = vtrn1_f32(v609, v609);
    float32x2_t v647 = vtrn2_f32(v609, v609);
    int64_t v649 = v644 + 1;
    float32x2_t v658 = v7[v657];
    float32x2_t v659 = vtrn1_f32(v627, v627);
    float32x2_t v660 = vtrn2_f32(v627, v627);
    int64_t v662 = v657 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vmul_f32(v336, v335);
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vmul_f32(v349, v348);
    float32x2_t v402 = v7[v401];
    float32x2_t v403 = vmul_f32(v398, v397);
    float32x2_t v464 = v7[v463];
    float32x2_t v465 = vmul_f32(v460, v459);
    float32x2_t v477 = v7[v476];
    float32x2_t v478 = vmul_f32(v473, v472);
    float32x2_t v526 = v7[v525];
    float32x2_t v527 = vmul_f32(v522, v521);
    float32x2_t v539 = v7[v538];
    float32x2_t v540 = vmul_f32(v535, v534);
    float32x2_t v588 = v7[v587];
    float32x2_t v589 = vmul_f32(v584, v583);
    float32x2_t v601 = v7[v600];
    float32x2_t v602 = vmul_f32(v597, v596);
    float32x2_t v650 = v7[v649];
    float32x2_t v651 = vmul_f32(v646, v645);
    float32x2_t v663 = v7[v662];
    float32x2_t v664 = vmul_f32(v659, v658);
    float32x2_t v418 = vfma_f32(v416, v412, v415);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v343 = vfma_f32(v341, v337, v340);
    float32x2_t v356 = vfma_f32(v354, v350, v353);
    float32x2_t v405 = vfma_f32(v403, v399, v402);
    float32x2_t v467 = vfma_f32(v465, v461, v464);
    float32x2_t v480 = vfma_f32(v478, v474, v477);
    float32x2_t v529 = vfma_f32(v527, v523, v526);
    float32x2_t v542 = vfma_f32(v540, v536, v539);
    float32x2_t v591 = vfma_f32(v589, v585, v588);
    float32x2_t v604 = vfma_f32(v602, v598, v601);
    float32x2_t v653 = vfma_f32(v651, v647, v650);
    float32x2_t v666 = vfma_f32(v664, v660, v663);
    float32x2_t v672 = vadd_f32(v671, v46);
    float32x2_t v673 = vsub_f32(v671, v46);
    float32x2_t v674 = vadd_f32(v95, v108);
    float32x2_t v675 = vsub_f32(v95, v108);
    float32x2_t v676 = vadd_f32(v157, v170);
    float32x2_t v677 = vsub_f32(v157, v170);
    float32x2_t v678 = vadd_f32(v219, v232);
    float32x2_t v679 = vsub_f32(v219, v232);
    float32x2_t v680 = vadd_f32(v281, v294);
    float32x2_t v681 = vsub_f32(v281, v294);
    float32x2_t v682 = vadd_f32(v343, v356);
    float32x2_t v683 = vsub_f32(v343, v356);
    float32x2_t v684 = vadd_f32(v405, v418);
    float32x2_t v685 = vsub_f32(v405, v418);
    float32x2_t v686 = vadd_f32(v467, v480);
    float32x2_t v687 = vsub_f32(v467, v480);
    float32x2_t v688 = vadd_f32(v529, v542);
    float32x2_t v689 = vsub_f32(v529, v542);
    float32x2_t v690 = vadd_f32(v591, v604);
    float32x2_t v691 = vsub_f32(v591, v604);
    float32x2_t v692 = vadd_f32(v653, v666);
    float32x2_t v693 = vsub_f32(v653, v666);
    float32x2_t v694 = vadd_f32(v674, v692);
    float32x2_t v695 = vadd_f32(v676, v690);
    float32x2_t v696 = vadd_f32(v678, v688);
    float32x2_t v697 = vadd_f32(v680, v686);
    float32x2_t v698 = vadd_f32(v682, v684);
    float32x2_t v699 = vsub_f32(v674, v692);
    float32x2_t v700 = vsub_f32(v676, v690);
    float32x2_t v701 = vsub_f32(v678, v688);
    float32x2_t v702 = vsub_f32(v680, v686);
    float32x2_t v703 = vsub_f32(v682, v684);
    float32x2_t v892 = vadd_f32(v675, v693);
    float32x2_t v893 = vadd_f32(v677, v691);
    float32x2_t v894 = vadd_f32(v679, v689);
    float32x2_t v895 = vadd_f32(v681, v687);
    float32x2_t v896 = vadd_f32(v683, v685);
    float32x2_t v897 = vsub_f32(v675, v693);
    float32x2_t v898 = vsub_f32(v677, v691);
    float32x2_t v899 = vsub_f32(v679, v689);
    float32x2_t v900 = vsub_f32(v681, v687);
    float32x2_t v901 = vsub_f32(v683, v685);
    float32x2_t v704 = vadd_f32(v694, v695);
    float32x2_t v705 = vadd_f32(v696, v698);
    float32x2_t v707 = vsub_f32(v700, v701);
    float32x2_t v708 = vadd_f32(v699, v703);
    float32x2_t v713 = vsub_f32(v695, v697);
    float32x2_t v714 = vsub_f32(v694, v697);
    float32x2_t v715 = vsub_f32(v695, v694);
    float32x2_t v716 = vsub_f32(v698, v697);
    float32x2_t v717 = vsub_f32(v696, v697);
    float32x2_t v718 = vsub_f32(v698, v696);
    float32x2_t v719 = vsub_f32(v695, v698);
    float32x2_t v720 = vsub_f32(v694, v696);
    float32x2_t v722 = vadd_f32(v700, v702);
    float32x2_t v723 = vsub_f32(v699, v702);
    float32x2_t v724 = vadd_f32(v699, v700);
    float32x2_t v725 = vsub_f32(v702, v703);
    float32x2_t v726 = vsub_f32(v701, v702);
    float32x2_t v727 = vsub_f32(v701, v703);
    float32x2_t v728 = vadd_f32(v700, v703);
    float32x2_t v729 = vsub_f32(v699, v701);
    float32x2_t v902 = vadd_f32(v892, v893);
    float32x2_t v903 = vadd_f32(v894, v896);
    float32x2_t v905 = vsub_f32(v898, v899);
    float32x2_t v906 = vadd_f32(v897, v901);
    float32x2_t v911 = vsub_f32(v893, v895);
    float32x2_t v912 = vsub_f32(v892, v895);
    float32x2_t v913 = vsub_f32(v893, v892);
    float32x2_t v914 = vsub_f32(v896, v895);
    float32x2_t v915 = vsub_f32(v894, v895);
    float32x2_t v916 = vsub_f32(v896, v894);
    float32x2_t v917 = vsub_f32(v893, v896);
    float32x2_t v918 = vsub_f32(v892, v894);
    float32x2_t v920 = vadd_f32(v898, v900);
    float32x2_t v921 = vsub_f32(v897, v900);
    float32x2_t v922 = vadd_f32(v897, v898);
    float32x2_t v923 = vsub_f32(v900, v901);
    float32x2_t v924 = vsub_f32(v899, v900);
    float32x2_t v925 = vsub_f32(v899, v901);
    float32x2_t v926 = vadd_f32(v898, v901);
    float32x2_t v927 = vsub_f32(v897, v899);
    float32x2_t v706 = vadd_f32(v697, v704);
    float32x2_t v711 = vsub_f32(v707, v708);
    float32x2_t v721 = vsub_f32(v705, v704);
    float32x2_t v730 = vadd_f32(v707, v708);
    float32x2_t v749 = vmul_f32(v713, v946);
    float32x2_t v753 = vmul_f32(v714, v950);
    float32x2_t v757 = vmul_f32(v715, v954);
    float32x2_t v761 = vmul_f32(v716, v958);
    float32x2_t v765 = vmul_f32(v717, v962);
    float32x2_t v769 = vmul_f32(v718, v966);
    float32x2_t v773 = vmul_f32(v719, v970);
    float32x2_t v777 = vmul_f32(v720, v974);
    float32x2_t v787 = vrev64_f32(v722);
    float32x2_t v794 = vrev64_f32(v723);
    float32x2_t v801 = vrev64_f32(v724);
    float32x2_t v808 = vrev64_f32(v725);
    float32x2_t v815 = vrev64_f32(v726);
    float32x2_t v822 = vrev64_f32(v727);
    float32x2_t v829 = vrev64_f32(v728);
    float32x2_t v836 = vrev64_f32(v729);
    float32x2_t v904 = vadd_f32(v895, v902);
    float32x2_t v909 = vsub_f32(v905, v906);
    float32x2_t v919 = vsub_f32(v903, v902);
    float32x2_t v928 = vadd_f32(v905, v906);
    float32x2_t v947 = vmul_f32(v911, v946);
    float32x2_t v951 = vmul_f32(v912, v950);
    float32x2_t v955 = vmul_f32(v913, v954);
    float32x2_t v959 = vmul_f32(v914, v958);
    float32x2_t v963 = vmul_f32(v915, v962);
    float32x2_t v967 = vmul_f32(v916, v966);
    float32x2_t v971 = vmul_f32(v917, v970);
    float32x2_t v975 = vmul_f32(v918, v974);
    float32x2_t v985 = vrev64_f32(v920);
    float32x2_t v992 = vrev64_f32(v921);
    float32x2_t v999 = vrev64_f32(v922);
    float32x2_t v1006 = vrev64_f32(v923);
    float32x2_t v1013 = vrev64_f32(v924);
    float32x2_t v1020 = vrev64_f32(v925);
    float32x2_t v1027 = vrev64_f32(v926);
    float32x2_t v1034 = vrev64_f32(v927);
    float32x2_t v709 = vadd_f32(v706, v705);
    float32x2_t v712 = vsub_f32(v711, v702);
    float32x2_t v781 = vmul_f32(v721, v978);
    float32x2_t v788 = vmul_f32(v787, v984);
    float32x2_t v795 = vmul_f32(v794, v991);
    float32x2_t v802 = vmul_f32(v801, v998);
    float32x2_t v809 = vmul_f32(v808, v1005);
    float32x2_t v816 = vmul_f32(v815, v1012);
    float32x2_t v823 = vmul_f32(v822, v1019);
    float32x2_t v830 = vmul_f32(v829, v1026);
    float32x2_t v837 = vmul_f32(v836, v1033);
    float32x2_t v843 = vrev64_f32(v730);
    float32x2_t v846 = vadd_f32(v749, v753);
    float32x2_t v847 = vadd_f32(v753, v757);
    float32x2_t v848 = vsub_f32(v749, v757);
    float32x2_t v849 = vadd_f32(v761, v765);
    float32x2_t v850 = vadd_f32(v765, v769);
    float32x2_t v851 = vsub_f32(v761, v769);
    float32x2_t v907 = vadd_f32(v904, v903);
    float32x2_t v910 = vsub_f32(v909, v900);
    float32x2_t v979 = vmul_f32(v919, v978);
    float32x2_t v986 = vmul_f32(v985, v984);
    float32x2_t v993 = vmul_f32(v992, v991);
    float32x2_t v1000 = vmul_f32(v999, v998);
    float32x2_t v1007 = vmul_f32(v1006, v1005);
    float32x2_t v1014 = vmul_f32(v1013, v1012);
    float32x2_t v1021 = vmul_f32(v1020, v1019);
    float32x2_t v1028 = vmul_f32(v1027, v1026);
    float32x2_t v1035 = vmul_f32(v1034, v1033);
    float32x2_t v1041 = vrev64_f32(v928);
    float32x2_t v1044 = vadd_f32(v947, v951);
    float32x2_t v1045 = vadd_f32(v951, v955);
    float32x2_t v1046 = vsub_f32(v947, v955);
    float32x2_t v1047 = vadd_f32(v959, v963);
    float32x2_t v1048 = vadd_f32(v963, v967);
    float32x2_t v1049 = vsub_f32(v959, v967);
    float32x2_t v710 = vadd_f32(v672, v709);
    float32x2_t v738 = vmul_f32(v709, v935);
    float32x2_t v744 = vrev64_f32(v712);
    float32x2_t v844 = vmul_f32(v843, v1040);
    float32x2_t v852 = vadd_f32(v777, v781);
    float32x2_t v853 = vadd_f32(v773, v781);
    float32x2_t v854 = vadd_f32(v795, v802);
    float32x2_t v855 = vsub_f32(v788, v802);
    float32x2_t v856 = vadd_f32(v816, v823);
    float32x2_t v857 = vsub_f32(v809, v823);
    float32x2_t v908 = vadd_f32(v673, v907);
    float32x2_t v936 = vmul_f32(v907, v935);
    float32x2_t v942 = vrev64_f32(v910);
    float32x2_t v1042 = vmul_f32(v1041, v1040);
    float32x2_t v1050 = vadd_f32(v975, v979);
    float32x2_t v1051 = vadd_f32(v971, v979);
    float32x2_t v1052 = vadd_f32(v993, v1000);
    float32x2_t v1053 = vsub_f32(v986, v1000);
    float32x2_t v1054 = vadd_f32(v1014, v1021);
    float32x2_t v1055 = vsub_f32(v1007, v1021);
    float32x2_t v745 = vmul_f32(v744, v941);
    float32x2_t v845 = vsub_f32(v710, v738);
    float32x2_t v858 = vadd_f32(v837, v844);
    float32x2_t v859 = vsub_f32(v830, v844);
    float32x2_t v860 = vadd_f32(v850, v852);
    float32x2_t v878 = vadd_f32(v854, v855);
    float32x2_t v943 = vmul_f32(v942, v941);
    float32x2_t v1043 = vsub_f32(v908, v936);
    float32x2_t v1056 = vadd_f32(v1035, v1042);
    float32x2_t v1057 = vsub_f32(v1028, v1042);
    float32x2_t v1058 = vadd_f32(v1048, v1050);
    float32x2_t v1076 = vadd_f32(v1052, v1053);
    int16x4_t v1092 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v710, 15), (int32x2_t){0, 0}));
    int16x4_t v1098 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v908, 15), (int32x2_t){0, 0}));
    float32x2_t v861 = vadd_f32(v860, v845);
    float32x2_t v862 = vsub_f32(v845, v847);
    float32x2_t v864 = vadd_f32(v845, v851);
    float32x2_t v866 = vsub_f32(v845, v848);
    float32x2_t v868 = vadd_f32(v845, v846);
    float32x2_t v870 = vadd_f32(v745, v856);
    float32x2_t v872 = vsub_f32(v858, v854);
    float32x2_t v874 = vadd_f32(v745, v859);
    float32x2_t v876 = vsub_f32(v859, v855);
    float32x2_t v879 = vadd_f32(v878, v856);
    float32x2_t v1059 = vadd_f32(v1058, v1043);
    float32x2_t v1060 = vsub_f32(v1043, v1045);
    float32x2_t v1062 = vadd_f32(v1043, v1049);
    float32x2_t v1064 = vsub_f32(v1043, v1046);
    float32x2_t v1066 = vadd_f32(v1043, v1044);
    float32x2_t v1068 = vadd_f32(v943, v1054);
    float32x2_t v1070 = vsub_f32(v1056, v1052);
    float32x2_t v1072 = vadd_f32(v943, v1057);
    float32x2_t v1074 = vsub_f32(v1057, v1053);
    float32x2_t v1077 = vadd_f32(v1076, v1054);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v1092), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v1098), 0);
    float32x2_t v863 = vsub_f32(v862, v852);
    float32x2_t v865 = vadd_f32(v864, v853);
    float32x2_t v867 = vsub_f32(v866, v853);
    float32x2_t v869 = vsub_f32(v868, v849);
    float32x2_t v871 = vadd_f32(v870, v858);
    float32x2_t v873 = vsub_f32(v872, v745);
    float32x2_t v875 = vadd_f32(v874, v857);
    float32x2_t v877 = vsub_f32(v876, v745);
    float32x2_t v880 = vadd_f32(v879, v857);
    float32x2_t v1061 = vsub_f32(v1060, v1050);
    float32x2_t v1063 = vadd_f32(v1062, v1051);
    float32x2_t v1065 = vsub_f32(v1064, v1051);
    float32x2_t v1067 = vsub_f32(v1066, v1047);
    float32x2_t v1069 = vadd_f32(v1068, v1056);
    float32x2_t v1071 = vsub_f32(v1070, v943);
    float32x2_t v1073 = vadd_f32(v1072, v1055);
    float32x2_t v1075 = vsub_f32(v1074, v943);
    float32x2_t v1078 = vadd_f32(v1077, v1055);
    float32x2_t v881 = vsub_f32(v880, v745);
    float32x2_t v883 = vadd_f32(v861, v871);
    float32x2_t v884 = vadd_f32(v863, v873);
    float32x2_t v885 = vsub_f32(v865, v875);
    float32x2_t v886 = vadd_f32(v867, v877);
    float32x2_t v887 = vsub_f32(v867, v877);
    float32x2_t v888 = vadd_f32(v865, v875);
    float32x2_t v889 = vsub_f32(v863, v873);
    float32x2_t v890 = vsub_f32(v861, v871);
    float32x2_t v1079 = vsub_f32(v1078, v943);
    float32x2_t v1081 = vadd_f32(v1059, v1069);
    float32x2_t v1082 = vadd_f32(v1061, v1071);
    float32x2_t v1083 = vsub_f32(v1063, v1073);
    float32x2_t v1084 = vadd_f32(v1065, v1075);
    float32x2_t v1085 = vsub_f32(v1065, v1075);
    float32x2_t v1086 = vadd_f32(v1063, v1073);
    float32x2_t v1087 = vsub_f32(v1061, v1071);
    float32x2_t v1088 = vsub_f32(v1059, v1069);
    float32x2_t v882 = vadd_f32(v869, v881);
    float32x2_t v891 = vsub_f32(v869, v881);
    float32x2_t v1080 = vadd_f32(v1067, v1079);
    float32x2_t v1089 = vsub_f32(v1067, v1079);
    int16x4_t v1116 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v890, 15), (int32x2_t){0, 0}));
    int16x4_t v1122 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1088, 15), (int32x2_t){0, 0}));
    int16x4_t v1128 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v889, 15), (int32x2_t){0, 0}));
    int16x4_t v1134 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1087, 15), (int32x2_t){0, 0}));
    int16x4_t v1140 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v888, 15), (int32x2_t){0, 0}));
    int16x4_t v1146 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1086, 15), (int32x2_t){0, 0}));
    int16x4_t v1152 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v887, 15), (int32x2_t){0, 0}));
    int16x4_t v1158 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1085, 15), (int32x2_t){0, 0}));
    int16x4_t v1164 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v886, 15), (int32x2_t){0, 0}));
    int16x4_t v1170 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1084, 15), (int32x2_t){0, 0}));
    int16x4_t v1176 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v885, 15), (int32x2_t){0, 0}));
    int16x4_t v1182 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1083, 15), (int32x2_t){0, 0}));
    int16x4_t v1188 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v884, 15), (int32x2_t){0, 0}));
    int16x4_t v1194 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1082, 15), (int32x2_t){0, 0}));
    int16x4_t v1200 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v883, 15), (int32x2_t){0, 0}));
    int16x4_t v1206 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1081, 15), (int32x2_t){0, 0}));
    int16x4_t v1104 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v891, 15), (int32x2_t){0, 0}));
    int16x4_t v1110 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1089, 15), (int32x2_t){0, 0}));
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v1116), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v1122), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v1128), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v1134), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v1140), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v1146), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v1152), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v1158), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v1164), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v1170), 0);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v1176), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v1182), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v1188), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v1194), 0);
    v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v1200), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v1206), 0);
    int16x4_t v1212 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v882, 15), (int32x2_t){0, 0}));
    int16x4_t v1218 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1080, 15), (int32x2_t){0, 0}));
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v1104), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v1110), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v1212), 0);
    v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v1218), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu22(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v740 = 1.1000000000000001e+00F;
    float v745 = -3.3166247903554003e-01F;
    float v752 = 5.1541501300188641e-01F;
    float v757 = 9.4125353283118118e-01F;
    float v762 = 1.4143537075597825e+00F;
    float v767 = 8.5949297361449750e-01F;
    float v772 = 4.2314838273285138e-02F;
    float v777 = 3.8639279888589606e-01F;
    float v782 = 5.1254589567200015e-01F;
    float v787 = 1.0702757469471715e+00F;
    float v792 = 5.5486073394528512e-01F;
    float v797 = -1.2412944743900585e+00F;
    float v804 = -2.0897833842005756e-01F;
    float v811 = -3.7415717312460811e-01F;
    float v818 = -4.9929922194110327e-02F;
    float v825 = -6.5815896284539266e-01F;
    float v832 = -6.3306543373877577e-01F;
    float v839 = -1.0822460581641109e+00F;
    float v846 = -8.1720737907134022e-01F;
    float v853 = -4.2408709531871824e-01F;
    const float32x2_t *v1196 = &v5[v0];
    int32_t *v1359 = &v6[v2];
    int64_t v19 = v0 * 11;
    int64_t v34 = v10 * 10;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 13;
    int64_t v76 = v10 * 12;
    int64_t v82 = v0 * 4;
    int64_t v96 = v0 * 15;
    int64_t v111 = v10 * 3;
    int64_t v118 = v10 * 14;
    int64_t v124 = v0 * 6;
    int64_t v138 = v0 * 17;
    int64_t v153 = v10 * 5;
    int64_t v160 = v10 * 16;
    int64_t v166 = v0 * 8;
    int64_t v180 = v0 * 19;
    int64_t v195 = v10 * 7;
    int64_t v202 = v10 * 18;
    int64_t v208 = v0 * 10;
    int64_t v222 = v0 * 21;
    int64_t v237 = v10 * 9;
    int64_t v244 = v10 * 20;
    int64_t v250 = v0 * 12;
    int64_t v279 = v10 * 11;
    int64_t v292 = v0 * 14;
    int64_t v306 = v0 * 3;
    int64_t v321 = v10 * 13;
    int64_t v328 = v10 * 2;
    int64_t v334 = v0 * 16;
    int64_t v348 = v0 * 5;
    int64_t v363 = v10 * 15;
    int64_t v370 = v10 * 4;
    int64_t v376 = v0 * 18;
    int64_t v390 = v0 * 7;
    int64_t v405 = v10 * 17;
    int64_t v412 = v10 * 6;
    int64_t v418 = v0 * 20;
    int64_t v432 = v0 * 9;
    int64_t v447 = v10 * 19;
    int64_t v454 = v10 * 8;
    int64_t v455 = v13 * 21;
    float v748 = v4 * v745;
    float v800 = v4 * v797;
    float v807 = v4 * v804;
    float v814 = v4 * v811;
    float v821 = v4 * v818;
    float v828 = v4 * v825;
    float v835 = v4 * v832;
    float v842 = v4 * v839;
    float v849 = v4 * v846;
    float v856 = v4 * v853;
    int64_t v915 = v2 * 11;
    int64_t v923 = v2 * 12;
    int64_t v939 = v2 * 2;
    int64_t v947 = v2 * 13;
    int64_t v955 = v2 * 14;
    int64_t v963 = v2 * 3;
    int64_t v971 = v2 * 4;
    int64_t v979 = v2 * 15;
    int64_t v987 = v2 * 16;
    int64_t v995 = v2 * 5;
    int64_t v1003 = v2 * 6;
    int64_t v1011 = v2 * 17;
    int64_t v1019 = v2 * 18;
    int64_t v1027 = v2 * 7;
    int64_t v1035 = v2 * 8;
    int64_t v1043 = v2 * 19;
    int64_t v1051 = v2 * 20;
    int64_t v1059 = v2 * 9;
    int64_t v1067 = v2 * 10;
    int64_t v1075 = v2 * 21;
    const float32x2_t *v1280 = &v5[0];
    svint64_t v1281 = svindex_s64(0, v1);
    svfloat32_t v1305 = svdup_n_f32(v740);
    svfloat32_t v1307 = svdup_n_f32(v752);
    svfloat32_t v1308 = svdup_n_f32(v757);
    svfloat32_t v1309 = svdup_n_f32(v762);
    svfloat32_t v1310 = svdup_n_f32(v767);
    svfloat32_t v1311 = svdup_n_f32(v772);
    svfloat32_t v1312 = svdup_n_f32(v777);
    svfloat32_t v1313 = svdup_n_f32(v782);
    svfloat32_t v1314 = svdup_n_f32(v787);
    svfloat32_t v1315 = svdup_n_f32(v792);
    int32_t *v1332 = &v6[0];
    int64_t v36 = v34 + v455;
    int64_t v71 = v10 + v455;
    int64_t v78 = v76 + v455;
    int64_t v113 = v111 + v455;
    int64_t v120 = v118 + v455;
    int64_t v155 = v153 + v455;
    int64_t v162 = v160 + v455;
    int64_t v197 = v195 + v455;
    int64_t v204 = v202 + v455;
    int64_t v239 = v237 + v455;
    int64_t v246 = v244 + v455;
    int64_t v281 = v279 + v455;
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v455]));
    int64_t v323 = v321 + v455;
    int64_t v330 = v328 + v455;
    int64_t v365 = v363 + v455;
    int64_t v372 = v370 + v455;
    int64_t v407 = v405 + v455;
    int64_t v414 = v412 + v455;
    int64_t v449 = v447 + v455;
    int64_t v456 = v454 + v455;
    const float32x2_t *v1088 = &v5[v19];
    const float32x2_t *v1097 = &v5[v40];
    const float32x2_t *v1106 = &v5[v54];
    const float32x2_t *v1115 = &v5[v82];
    const float32x2_t *v1124 = &v5[v96];
    const float32x2_t *v1133 = &v5[v124];
    const float32x2_t *v1142 = &v5[v138];
    const float32x2_t *v1151 = &v5[v166];
    const float32x2_t *v1160 = &v5[v180];
    const float32x2_t *v1169 = &v5[v208];
    const float32x2_t *v1178 = &v5[v222];
    const float32x2_t *v1187 = &v5[v250];
    svfloat32_t v1198 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1196), v1281));
    const float32x2_t *v1207 = &v5[v292];
    const float32x2_t *v1216 = &v5[v306];
    const float32x2_t *v1225 = &v5[v334];
    const float32x2_t *v1234 = &v5[v348];
    const float32x2_t *v1243 = &v5[v376];
    const float32x2_t *v1252 = &v5[v390];
    const float32x2_t *v1261 = &v5[v418];
    const float32x2_t *v1270 = &v5[v432];
    svfloat32_t v1282 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1280), v1281));
    svfloat32_t v1306 = svdup_n_f32(v748);
    svfloat32_t v1316 = svdup_n_f32(v800);
    svfloat32_t v1317 = svdup_n_f32(v807);
    svfloat32_t v1318 = svdup_n_f32(v814);
    svfloat32_t v1319 = svdup_n_f32(v821);
    svfloat32_t v1320 = svdup_n_f32(v828);
    svfloat32_t v1321 = svdup_n_f32(v835);
    svfloat32_t v1322 = svdup_n_f32(v842);
    svfloat32_t v1323 = svdup_n_f32(v849);
    svfloat32_t v1324 = svdup_n_f32(v856);
    int32_t *v1341 = &v6[v915];
    int32_t *v1350 = &v6[v923];
    int32_t *v1368 = &v6[v939];
    int32_t *v1377 = &v6[v947];
    int32_t *v1386 = &v6[v955];
    int32_t *v1395 = &v6[v963];
    int32_t *v1404 = &v6[v971];
    int32_t *v1413 = &v6[v979];
    int32_t *v1422 = &v6[v987];
    int32_t *v1431 = &v6[v995];
    int32_t *v1440 = &v6[v1003];
    int32_t *v1449 = &v6[v1011];
    int32_t *v1458 = &v6[v1019];
    int32_t *v1467 = &v6[v1027];
    int32_t *v1476 = &v6[v1035];
    int32_t *v1485 = &v6[v1043];
    int32_t *v1494 = &v6[v1051];
    int32_t *v1503 = &v6[v1059];
    int32_t *v1512 = &v6[v1067];
    int32_t *v1521 = &v6[v1075];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v246]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero290, v1198, v289, 0), v1198,
        v289, 90);
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v331 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v330]));
    svfloat32_t v366 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v365]));
    svfloat32_t v373 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v372]));
    svfloat32_t v408 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v407]));
    svfloat32_t v415 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v414]));
    svfloat32_t v450 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v449]));
    svfloat32_t v457 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v456]));
    svfloat32_t v1090 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1088), v1281));
    svfloat32_t v1099 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1097), v1281));
    svfloat32_t v1108 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1106), v1281));
    svfloat32_t v1117 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1115), v1281));
    svfloat32_t v1126 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1124), v1281));
    svfloat32_t v1135 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1133), v1281));
    svfloat32_t v1144 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1142), v1281));
    svfloat32_t v1153 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1151), v1281));
    svfloat32_t v1162 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1160), v1281));
    svfloat32_t v1171 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1169), v1281));
    svfloat32_t v1180 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1178), v1281));
    svfloat32_t v1189 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1187), v1281));
    svfloat32_t v1209 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1207), v1281));
    svfloat32_t v1218 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1216), v1281));
    svfloat32_t v1227 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1225), v1281));
    svfloat32_t v1236 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1234), v1281));
    svfloat32_t v1245 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1243), v1281));
    svfloat32_t v1254 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1252), v1281));
    svfloat32_t v1263 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1261), v1281));
    svfloat32_t v1272 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1270), v1281));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v1090, v37, 0),
                     v1090, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v1099, v72, 0),
                     v1099, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v1108, v79, 0),
                     v1108, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero115, v1117, v114, 0), v1117,
        v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero122, v1126, v121, 0), v1126,
        v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero157, v1135, v156, 0), v1135,
        v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero164, v1144, v163, 0), v1144,
        v163, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero199, v1153, v198, 0), v1153,
        v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero206, v1162, v205, 0), v1162,
        v205, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero241, v1171, v240, 0), v1171,
        v240, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero248, v1180, v247, 0), v1180,
        v247, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero283, v1189, v282, 0), v1189,
        v282, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero325, v1209, v324, 0), v1209,
        v324, 90);
    svfloat32_t zero332 = svdup_n_f32(0);
    svfloat32_t v332 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero332, v1218, v331, 0), v1218,
        v331, 90);
    svfloat32_t zero367 = svdup_n_f32(0);
    svfloat32_t v367 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero367, v1227, v366, 0), v1227,
        v366, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero374, v1236, v373, 0), v1236,
        v373, 90);
    svfloat32_t zero409 = svdup_n_f32(0);
    svfloat32_t v409 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero409, v1245, v408, 0), v1245,
        v408, 90);
    svfloat32_t zero416 = svdup_n_f32(0);
    svfloat32_t v416 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero416, v1254, v415, 0), v1254,
        v415, 90);
    svfloat32_t zero451 = svdup_n_f32(0);
    svfloat32_t v451 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero451, v1263, v450, 0), v1263,
        v450, 90);
    svfloat32_t zero458 = svdup_n_f32(0);
    svfloat32_t v458 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero458, v1272, v457, 0), v1272,
        v457, 90);
    svfloat32_t v466 = svadd_f32_x(svptrue_b32(), v1282, v38);
    svfloat32_t v467 = svsub_f32_x(svptrue_b32(), v1282, v38);
    svfloat32_t v468 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v469 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v470 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v471 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v472 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v473 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v474 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v476 = svadd_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v477 = svsub_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v478 = svadd_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v479 = svsub_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v480 = svadd_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v481 = svsub_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v482 = svadd_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v483 = svsub_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v484 = svadd_f32_x(svptrue_b32(), v409, v416);
    svfloat32_t v485 = svsub_f32_x(svptrue_b32(), v409, v416);
    svfloat32_t v486 = svadd_f32_x(svptrue_b32(), v451, v458);
    svfloat32_t v487 = svsub_f32_x(svptrue_b32(), v451, v458);
    svfloat32_t v488 = svadd_f32_x(svptrue_b32(), v468, v486);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v470, v484);
    svfloat32_t v490 = svadd_f32_x(svptrue_b32(), v472, v482);
    svfloat32_t v491 = svadd_f32_x(svptrue_b32(), v474, v480);
    svfloat32_t v492 = svadd_f32_x(svptrue_b32(), v476, v478);
    svfloat32_t v493 = svsub_f32_x(svptrue_b32(), v468, v486);
    svfloat32_t v494 = svsub_f32_x(svptrue_b32(), v470, v484);
    svfloat32_t v495 = svsub_f32_x(svptrue_b32(), v472, v482);
    svfloat32_t v496 = svsub_f32_x(svptrue_b32(), v474, v480);
    svfloat32_t v497 = svsub_f32_x(svptrue_b32(), v476, v478);
    svfloat32_t v697 = svadd_f32_x(svptrue_b32(), v469, v487);
    svfloat32_t v698 = svadd_f32_x(svptrue_b32(), v471, v485);
    svfloat32_t v699 = svadd_f32_x(svptrue_b32(), v473, v483);
    svfloat32_t v700 = svadd_f32_x(svptrue_b32(), v475, v481);
    svfloat32_t v701 = svadd_f32_x(svptrue_b32(), v477, v479);
    svfloat32_t v702 = svsub_f32_x(svptrue_b32(), v469, v487);
    svfloat32_t v703 = svsub_f32_x(svptrue_b32(), v471, v485);
    svfloat32_t v704 = svsub_f32_x(svptrue_b32(), v473, v483);
    svfloat32_t v705 = svsub_f32_x(svptrue_b32(), v475, v481);
    svfloat32_t v706 = svsub_f32_x(svptrue_b32(), v477, v479);
    svfloat32_t v498 = svadd_f32_x(svptrue_b32(), v488, v489);
    svfloat32_t v499 = svadd_f32_x(svptrue_b32(), v490, v492);
    svfloat32_t v501 = svsub_f32_x(svptrue_b32(), v494, v495);
    svfloat32_t v502 = svadd_f32_x(svptrue_b32(), v493, v497);
    svfloat32_t v507 = svsub_f32_x(svptrue_b32(), v489, v491);
    svfloat32_t v508 = svsub_f32_x(svptrue_b32(), v488, v491);
    svfloat32_t v509 = svsub_f32_x(svptrue_b32(), v489, v488);
    svfloat32_t v510 = svsub_f32_x(svptrue_b32(), v492, v491);
    svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v490, v491);
    svfloat32_t v512 = svsub_f32_x(svptrue_b32(), v492, v490);
    svfloat32_t v513 = svsub_f32_x(svptrue_b32(), v489, v492);
    svfloat32_t v514 = svsub_f32_x(svptrue_b32(), v488, v490);
    svfloat32_t v516 = svadd_f32_x(svptrue_b32(), v494, v496);
    svfloat32_t v517 = svsub_f32_x(svptrue_b32(), v493, v496);
    svfloat32_t v518 = svadd_f32_x(svptrue_b32(), v493, v494);
    svfloat32_t v519 = svsub_f32_x(svptrue_b32(), v496, v497);
    svfloat32_t v520 = svsub_f32_x(svptrue_b32(), v495, v496);
    svfloat32_t v521 = svsub_f32_x(svptrue_b32(), v495, v497);
    svfloat32_t v522 = svadd_f32_x(svptrue_b32(), v494, v497);
    svfloat32_t v523 = svsub_f32_x(svptrue_b32(), v493, v495);
    svfloat32_t v707 = svadd_f32_x(svptrue_b32(), v697, v698);
    svfloat32_t v708 = svadd_f32_x(svptrue_b32(), v699, v701);
    svfloat32_t v710 = svsub_f32_x(svptrue_b32(), v703, v704);
    svfloat32_t v711 = svadd_f32_x(svptrue_b32(), v702, v706);
    svfloat32_t v716 = svsub_f32_x(svptrue_b32(), v698, v700);
    svfloat32_t v717 = svsub_f32_x(svptrue_b32(), v697, v700);
    svfloat32_t v718 = svsub_f32_x(svptrue_b32(), v698, v697);
    svfloat32_t v719 = svsub_f32_x(svptrue_b32(), v701, v700);
    svfloat32_t v720 = svsub_f32_x(svptrue_b32(), v699, v700);
    svfloat32_t v721 = svsub_f32_x(svptrue_b32(), v701, v699);
    svfloat32_t v722 = svsub_f32_x(svptrue_b32(), v698, v701);
    svfloat32_t v723 = svsub_f32_x(svptrue_b32(), v697, v699);
    svfloat32_t v725 = svadd_f32_x(svptrue_b32(), v703, v705);
    svfloat32_t v726 = svsub_f32_x(svptrue_b32(), v702, v705);
    svfloat32_t v727 = svadd_f32_x(svptrue_b32(), v702, v703);
    svfloat32_t v728 = svsub_f32_x(svptrue_b32(), v705, v706);
    svfloat32_t v729 = svsub_f32_x(svptrue_b32(), v704, v705);
    svfloat32_t v730 = svsub_f32_x(svptrue_b32(), v704, v706);
    svfloat32_t v731 = svadd_f32_x(svptrue_b32(), v703, v706);
    svfloat32_t v732 = svsub_f32_x(svptrue_b32(), v702, v704);
    svfloat32_t v500 = svadd_f32_x(svptrue_b32(), v491, v498);
    svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v501, v502);
    svfloat32_t v515 = svsub_f32_x(svptrue_b32(), v499, v498);
    svfloat32_t v524 = svadd_f32_x(svptrue_b32(), v501, v502);
    svfloat32_t v551 = svmul_f32_x(svptrue_b32(), v508, v1308);
    svfloat32_t v556 = svmul_f32_x(svptrue_b32(), v509, v1309);
    svfloat32_t v566 = svmul_f32_x(svptrue_b32(), v511, v1311);
    svfloat32_t v571 = svmul_f32_x(svptrue_b32(), v512, v1312);
    svfloat32_t zero593 = svdup_n_f32(0);
    svfloat32_t v593 = svcmla_f32_x(pred_full, zero593, v1316, v516, 90);
    svfloat32_t zero607 = svdup_n_f32(0);
    svfloat32_t v607 = svcmla_f32_x(pred_full, zero607, v1318, v518, 90);
    svfloat32_t zero614 = svdup_n_f32(0);
    svfloat32_t v614 = svcmla_f32_x(pred_full, zero614, v1319, v519, 90);
    svfloat32_t zero628 = svdup_n_f32(0);
    svfloat32_t v628 = svcmla_f32_x(pred_full, zero628, v1321, v521, 90);
    svfloat32_t zero635 = svdup_n_f32(0);
    svfloat32_t v635 = svcmla_f32_x(pred_full, zero635, v1322, v522, 90);
    svfloat32_t v709 = svadd_f32_x(svptrue_b32(), v700, v707);
    svfloat32_t v714 = svsub_f32_x(svptrue_b32(), v710, v711);
    svfloat32_t v724 = svsub_f32_x(svptrue_b32(), v708, v707);
    svfloat32_t v733 = svadd_f32_x(svptrue_b32(), v710, v711);
    svfloat32_t v760 = svmul_f32_x(svptrue_b32(), v717, v1308);
    svfloat32_t v765 = svmul_f32_x(svptrue_b32(), v718, v1309);
    svfloat32_t v775 = svmul_f32_x(svptrue_b32(), v720, v1311);
    svfloat32_t v780 = svmul_f32_x(svptrue_b32(), v721, v1312);
    svfloat32_t zero802 = svdup_n_f32(0);
    svfloat32_t v802 = svcmla_f32_x(pred_full, zero802, v1316, v725, 90);
    svfloat32_t zero816 = svdup_n_f32(0);
    svfloat32_t v816 = svcmla_f32_x(pred_full, zero816, v1318, v727, 90);
    svfloat32_t zero823 = svdup_n_f32(0);
    svfloat32_t v823 = svcmla_f32_x(pred_full, zero823, v1319, v728, 90);
    svfloat32_t zero837 = svdup_n_f32(0);
    svfloat32_t v837 = svcmla_f32_x(pred_full, zero837, v1321, v730, 90);
    svfloat32_t zero844 = svdup_n_f32(0);
    svfloat32_t v844 = svcmla_f32_x(pred_full, zero844, v1322, v731, 90);
    svfloat32_t v503 = svadd_f32_x(svptrue_b32(), v500, v499);
    svfloat32_t v506 = svsub_f32_x(svptrue_b32(), v505, v496);
    svfloat32_t v586 = svmul_f32_x(svptrue_b32(), v515, v1315);
    svfloat32_t zero649 = svdup_n_f32(0);
    svfloat32_t v649 = svcmla_f32_x(pred_full, zero649, v1324, v524, 90);
    svfloat32_t v651 = svmla_f32_x(pred_full, v551, v507, v1307);
    svfloat32_t v652 = svmla_f32_x(pred_full, v556, v508, v1308);
    svfloat32_t v653 = svnmls_f32_x(pred_full, v556, v507, v1307);
    svfloat32_t v654 = svmla_f32_x(pred_full, v566, v510, v1310);
    svfloat32_t v655 = svmla_f32_x(pred_full, v571, v511, v1311);
    svfloat32_t v656 = svnmls_f32_x(pred_full, v571, v510, v1310);
    svfloat32_t v659 = svcmla_f32_x(pred_full, v607, v1317, v517, 90);
    svfloat32_t v660 = svsub_f32_x(svptrue_b32(), v593, v607);
    svfloat32_t v661 = svcmla_f32_x(pred_full, v628, v1320, v520, 90);
    svfloat32_t v662 = svsub_f32_x(svptrue_b32(), v614, v628);
    svfloat32_t v712 = svadd_f32_x(svptrue_b32(), v709, v708);
    svfloat32_t v715 = svsub_f32_x(svptrue_b32(), v714, v705);
    svfloat32_t v795 = svmul_f32_x(svptrue_b32(), v724, v1315);
    svfloat32_t zero858 = svdup_n_f32(0);
    svfloat32_t v858 = svcmla_f32_x(pred_full, zero858, v1324, v733, 90);
    svfloat32_t v860 = svmla_f32_x(pred_full, v760, v716, v1307);
    svfloat32_t v861 = svmla_f32_x(pred_full, v765, v717, v1308);
    svfloat32_t v862 = svnmls_f32_x(pred_full, v765, v716, v1307);
    svfloat32_t v863 = svmla_f32_x(pred_full, v775, v719, v1310);
    svfloat32_t v864 = svmla_f32_x(pred_full, v780, v720, v1311);
    svfloat32_t v865 = svnmls_f32_x(pred_full, v780, v719, v1310);
    svfloat32_t v868 = svcmla_f32_x(pred_full, v816, v1317, v726, 90);
    svfloat32_t v869 = svsub_f32_x(svptrue_b32(), v802, v816);
    svfloat32_t v870 = svcmla_f32_x(pred_full, v837, v1320, v729, 90);
    svfloat32_t v871 = svsub_f32_x(svptrue_b32(), v823, v837);
    svfloat32_t v504 = svadd_f32_x(svptrue_b32(), v466, v503);
    svfloat32_t zero541 = svdup_n_f32(0);
    svfloat32_t v541 = svcmla_f32_x(pred_full, zero541, v1306, v506, 90);
    svfloat32_t v657 = svmla_f32_x(pred_full, v586, v514, v1314);
    svfloat32_t v658 = svmla_f32_x(pred_full, v586, v513, v1313);
    svfloat32_t v663 = svcmla_f32_x(pred_full, v649, v1323, v523, 90);
    svfloat32_t v664 = svsub_f32_x(svptrue_b32(), v635, v649);
    svfloat32_t v683 = svadd_f32_x(svptrue_b32(), v659, v660);
    svfloat32_t v713 = svadd_f32_x(svptrue_b32(), v467, v712);
    svfloat32_t zero750 = svdup_n_f32(0);
    svfloat32_t v750 = svcmla_f32_x(pred_full, zero750, v1306, v715, 90);
    svfloat32_t v866 = svmla_f32_x(pred_full, v795, v723, v1314);
    svfloat32_t v867 = svmla_f32_x(pred_full, v795, v722, v1313);
    svfloat32_t v872 = svcmla_f32_x(pred_full, v858, v1323, v732, 90);
    svfloat32_t v873 = svsub_f32_x(svptrue_b32(), v844, v858);
    svfloat32_t v892 = svadd_f32_x(svptrue_b32(), v868, v869);
    svfloat32_t v650 = svmls_f32_x(pred_full, v504, v503, v1305);
    svfloat32_t v665 = svadd_f32_x(svptrue_b32(), v655, v657);
    svfloat32_t v675 = svadd_f32_x(svptrue_b32(), v541, v661);
    svfloat32_t v677 = svsub_f32_x(svptrue_b32(), v663, v659);
    svfloat32_t v679 = svadd_f32_x(svptrue_b32(), v541, v664);
    svfloat32_t v681 = svsub_f32_x(svptrue_b32(), v664, v660);
    svfloat32_t v684 = svadd_f32_x(svptrue_b32(), v683, v661);
    svfloat32_t v859 = svmls_f32_x(pred_full, v713, v712, v1305);
    svfloat32_t v874 = svadd_f32_x(svptrue_b32(), v864, v866);
    svfloat32_t v884 = svadd_f32_x(svptrue_b32(), v750, v870);
    svfloat32_t v886 = svsub_f32_x(svptrue_b32(), v872, v868);
    svfloat32_t v888 = svadd_f32_x(svptrue_b32(), v750, v873);
    svfloat32_t v890 = svsub_f32_x(svptrue_b32(), v873, v869);
    svfloat32_t v893 = svadd_f32_x(svptrue_b32(), v892, v870);
    svint16_t v908 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v504, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v916 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v713, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v666 = svadd_f32_x(svptrue_b32(), v665, v650);
    svfloat32_t v667 = svsub_f32_x(svptrue_b32(), v650, v652);
    svfloat32_t v669 = svadd_f32_x(svptrue_b32(), v650, v656);
    svfloat32_t v671 = svsub_f32_x(svptrue_b32(), v650, v653);
    svfloat32_t v673 = svadd_f32_x(svptrue_b32(), v650, v651);
    svfloat32_t v676 = svadd_f32_x(svptrue_b32(), v675, v663);
    svfloat32_t v678 = svsub_f32_x(svptrue_b32(), v677, v541);
    svfloat32_t v680 = svadd_f32_x(svptrue_b32(), v679, v662);
    svfloat32_t v682 = svsub_f32_x(svptrue_b32(), v681, v541);
    svfloat32_t v685 = svadd_f32_x(svptrue_b32(), v684, v662);
    svfloat32_t v875 = svadd_f32_x(svptrue_b32(), v874, v859);
    svfloat32_t v876 = svsub_f32_x(svptrue_b32(), v859, v861);
    svfloat32_t v878 = svadd_f32_x(svptrue_b32(), v859, v865);
    svfloat32_t v880 = svsub_f32_x(svptrue_b32(), v859, v862);
    svfloat32_t v882 = svadd_f32_x(svptrue_b32(), v859, v860);
    svfloat32_t v885 = svadd_f32_x(svptrue_b32(), v884, v872);
    svfloat32_t v887 = svsub_f32_x(svptrue_b32(), v886, v750);
    svfloat32_t v889 = svadd_f32_x(svptrue_b32(), v888, v871);
    svfloat32_t v891 = svsub_f32_x(svptrue_b32(), v890, v750);
    svfloat32_t v894 = svadd_f32_x(svptrue_b32(), v893, v871);
    svst1w_u64(pred_full, (unsigned *)(v1332), svreinterpret_u64_s16(v908));
    svst1w_u64(pred_full, (unsigned *)(v1341), svreinterpret_u64_s16(v916));
    svfloat32_t v668 = svsub_f32_x(svptrue_b32(), v667, v657);
    svfloat32_t v670 = svadd_f32_x(svptrue_b32(), v669, v658);
    svfloat32_t v672 = svsub_f32_x(svptrue_b32(), v671, v658);
    svfloat32_t v674 = svsub_f32_x(svptrue_b32(), v673, v654);
    svfloat32_t v686 = svsub_f32_x(svptrue_b32(), v685, v541);
    svfloat32_t v688 = svadd_f32_x(svptrue_b32(), v666, v676);
    svfloat32_t v695 = svsub_f32_x(svptrue_b32(), v666, v676);
    svfloat32_t v877 = svsub_f32_x(svptrue_b32(), v876, v866);
    svfloat32_t v879 = svadd_f32_x(svptrue_b32(), v878, v867);
    svfloat32_t v881 = svsub_f32_x(svptrue_b32(), v880, v867);
    svfloat32_t v883 = svsub_f32_x(svptrue_b32(), v882, v863);
    svfloat32_t v895 = svsub_f32_x(svptrue_b32(), v894, v750);
    svfloat32_t v897 = svadd_f32_x(svptrue_b32(), v875, v885);
    svfloat32_t v904 = svsub_f32_x(svptrue_b32(), v875, v885);
    svfloat32_t v687 = svadd_f32_x(svptrue_b32(), v674, v686);
    svfloat32_t v689 = svadd_f32_x(svptrue_b32(), v668, v678);
    svfloat32_t v690 = svsub_f32_x(svptrue_b32(), v670, v680);
    svfloat32_t v691 = svadd_f32_x(svptrue_b32(), v672, v682);
    svfloat32_t v692 = svsub_f32_x(svptrue_b32(), v672, v682);
    svfloat32_t v693 = svadd_f32_x(svptrue_b32(), v670, v680);
    svfloat32_t v694 = svsub_f32_x(svptrue_b32(), v668, v678);
    svfloat32_t v696 = svsub_f32_x(svptrue_b32(), v674, v686);
    svfloat32_t v896 = svadd_f32_x(svptrue_b32(), v883, v895);
    svfloat32_t v898 = svadd_f32_x(svptrue_b32(), v877, v887);
    svfloat32_t v899 = svsub_f32_x(svptrue_b32(), v879, v889);
    svfloat32_t v900 = svadd_f32_x(svptrue_b32(), v881, v891);
    svfloat32_t v901 = svsub_f32_x(svptrue_b32(), v881, v891);
    svfloat32_t v902 = svadd_f32_x(svptrue_b32(), v879, v889);
    svfloat32_t v903 = svsub_f32_x(svptrue_b32(), v877, v887);
    svfloat32_t v905 = svsub_f32_x(svptrue_b32(), v883, v895);
    svint16_t v940 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v695, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v948 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v904, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1052 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v688, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1060 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v897, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v924 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v696, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v932 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v905, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v956 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v694, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v964 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v903, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v972 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v693, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v980 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v902, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v988 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v692, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v996 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v901, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1004 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v691, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1012 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v900, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1020 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v690, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1028 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v899, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1036 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v689, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1044 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v898, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1068 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v687, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1076 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v896, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1368), svreinterpret_u64_s16(v940));
    svst1w_u64(pred_full, (unsigned *)(v1377), svreinterpret_u64_s16(v948));
    svst1w_u64(pred_full, (unsigned *)(v1494), svreinterpret_u64_s16(v1052));
    svst1w_u64(pred_full, (unsigned *)(v1503), svreinterpret_u64_s16(v1060));
    svst1w_u64(pred_full, (unsigned *)(v1350), svreinterpret_u64_s16(v924));
    svst1w_u64(pred_full, (unsigned *)(v1359), svreinterpret_u64_s16(v932));
    svst1w_u64(pred_full, (unsigned *)(v1386), svreinterpret_u64_s16(v956));
    svst1w_u64(pred_full, (unsigned *)(v1395), svreinterpret_u64_s16(v964));
    svst1w_u64(pred_full, (unsigned *)(v1404), svreinterpret_u64_s16(v972));
    svst1w_u64(pred_full, (unsigned *)(v1413), svreinterpret_u64_s16(v980));
    svst1w_u64(pred_full, (unsigned *)(v1422), svreinterpret_u64_s16(v988));
    svst1w_u64(pred_full, (unsigned *)(v1431), svreinterpret_u64_s16(v996));
    svst1w_u64(pred_full, (unsigned *)(v1440), svreinterpret_u64_s16(v1004));
    svst1w_u64(pred_full, (unsigned *)(v1449), svreinterpret_u64_s16(v1012));
    svst1w_u64(pred_full, (unsigned *)(v1458), svreinterpret_u64_s16(v1020));
    svst1w_u64(pred_full, (unsigned *)(v1467), svreinterpret_u64_s16(v1028));
    svst1w_u64(pred_full, (unsigned *)(v1476), svreinterpret_u64_s16(v1036));
    svst1w_u64(pred_full, (unsigned *)(v1485), svreinterpret_u64_s16(v1044));
    svst1w_u64(pred_full, (unsigned *)(v1512), svreinterpret_u64_s16(v1068));
    svst1w_u64(pred_full, (unsigned *)(v1521), svreinterpret_u64_s16(v1076));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu24(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v260 = v5[istride];
    float v706 = 1.0000000000000000e+00F;
    float v707 = -1.0000000000000000e+00F;
    float v714 = -7.0710678118654746e-01F;
    float v721 = 7.0710678118654757e-01F;
    float v773 = -1.4999999999999998e+00F;
    float v774 = 1.4999999999999998e+00F;
    float v781 = 1.0606601717798210e+00F;
    float v788 = -1.0606601717798212e+00F;
    float v842 = 8.6602540378443871e-01F;
    float v850 = -8.6602540378443871e-01F;
    float v857 = 6.1237243569579458e-01F;
    float v858 = -6.1237243569579458e-01F;
    float32x2_t v860 = (float32x2_t){v4, v4};
    float32x2_t v292 = vtrn1_f32(v260, v260);
    float32x2_t v293 = vtrn2_f32(v260, v260);
    float32x2_t v644 = v5[0];
    float32x2_t v708 = (float32x2_t){v706, v707};
    float32x2_t v715 = (float32x2_t){v721, v714};
    float32x2_t v722 = (float32x2_t){v721, v721};
    float32x2_t v771 = (float32x2_t){v773, v773};
    float32x2_t v775 = (float32x2_t){v773, v774};
    float32x2_t v782 = (float32x2_t){v788, v781};
    float32x2_t v789 = (float32x2_t){v788, v788};
    float32x2_t v844 = (float32x2_t){v842, v850};
    float32x2_t v851 = (float32x2_t){v850, v850};
    float32x2_t v855 = (float32x2_t){v858, v858};
    float32x2_t v859 = (float32x2_t){v857, v858};
    float32x2_t v20 = v5[istride * 8];
    float32x2_t v38 = v5[istride * 16];
    int64_t v55 = 14 + j * 46;
    int64_t v68 = 30 + j * 46;
    float32x2_t v82 = v5[istride * 11];
    float32x2_t v100 = v5[istride * 19];
    int64_t v117 = 20 + j * 46;
    int64_t v130 = 36 + j * 46;
    float32x2_t v144 = v5[istride * 3];
    int64_t v148 = 4 + j * 46;
    float32x2_t v162 = v5[istride * 14];
    float32x2_t v180 = v5[istride * 22];
    int64_t v197 = 26 + j * 46;
    int64_t v210 = 42 + j * 46;
    float32x2_t v224 = v5[istride * 6];
    int64_t v228 = 10 + j * 46;
    float32x2_t v242 = v5[istride * 17];
    int64_t v277 = 32 + j * 46;
    float32x2_t v291 = v7[j * 46];
    int64_t v295 = j * 46 + 1;
    float32x2_t v304 = v5[istride * 9];
    int64_t v308 = 16 + j * 46;
    float32x2_t v322 = v5[istride * 20];
    float32x2_t v340 = v5[istride * 4];
    int64_t v357 = 38 + j * 46;
    int64_t v370 = 6 + j * 46;
    float32x2_t v384 = v5[istride * 12];
    int64_t v388 = 22 + j * 46;
    float32x2_t v402 = v5[istride * 23];
    float32x2_t v420 = v5[istride * 7];
    int64_t v437 = 44 + j * 46;
    int64_t v450 = 12 + j * 46;
    float32x2_t v464 = v5[istride * 15];
    int64_t v468 = 28 + j * 46;
    float32x2_t v482 = v5[istride * 2];
    float32x2_t v500 = v5[istride * 10];
    int64_t v517 = 2 + j * 46;
    int64_t v530 = 18 + j * 46;
    float32x2_t v544 = v5[istride * 18];
    int64_t v548 = 34 + j * 46;
    float32x2_t v562 = v5[istride * 5];
    float32x2_t v580 = v5[istride * 13];
    int64_t v597 = 8 + j * 46;
    int64_t v610 = 24 + j * 46;
    float32x2_t v624 = v5[istride * 21];
    int64_t v628 = 40 + j * 46;
    float32x2_t v710 = vmul_f32(v860, v708);
    float32x2_t v717 = vmul_f32(v860, v715);
    float32x2_t v777 = vmul_f32(v860, v775);
    float32x2_t v784 = vmul_f32(v860, v782);
    float32x2_t v846 = vmul_f32(v860, v844);
    float32x2_t v861 = vmul_f32(v860, v859);
    float32x2_t v56 = v7[v55];
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    int64_t v60 = v55 + 1;
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v144, v144);
    float32x2_t v151 = vtrn2_f32(v144, v144);
    int64_t v153 = v148 + 1;
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vtrn1_f32(v162, v162);
    float32x2_t v200 = vtrn2_f32(v162, v162);
    int64_t v202 = v197 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v180, v180);
    float32x2_t v213 = vtrn2_f32(v180, v180);
    int64_t v215 = v210 + 1;
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vtrn1_f32(v224, v224);
    float32x2_t v231 = vtrn2_f32(v224, v224);
    int64_t v233 = v228 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vtrn1_f32(v242, v242);
    float32x2_t v280 = vtrn2_f32(v242, v242);
    int64_t v282 = v277 + 1;
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vmul_f32(v292, v291);
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vtrn1_f32(v304, v304);
    float32x2_t v311 = vtrn2_f32(v304, v304);
    int64_t v313 = v308 + 1;
    float32x2_t v358 = v7[v357];
    float32x2_t v359 = vtrn1_f32(v322, v322);
    float32x2_t v360 = vtrn2_f32(v322, v322);
    int64_t v362 = v357 + 1;
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vtrn1_f32(v340, v340);
    float32x2_t v373 = vtrn2_f32(v340, v340);
    int64_t v375 = v370 + 1;
    float32x2_t v389 = v7[v388];
    float32x2_t v390 = vtrn1_f32(v384, v384);
    float32x2_t v391 = vtrn2_f32(v384, v384);
    int64_t v393 = v388 + 1;
    float32x2_t v438 = v7[v437];
    float32x2_t v439 = vtrn1_f32(v402, v402);
    float32x2_t v440 = vtrn2_f32(v402, v402);
    int64_t v442 = v437 + 1;
    float32x2_t v451 = v7[v450];
    float32x2_t v452 = vtrn1_f32(v420, v420);
    float32x2_t v453 = vtrn2_f32(v420, v420);
    int64_t v455 = v450 + 1;
    float32x2_t v469 = v7[v468];
    float32x2_t v470 = vtrn1_f32(v464, v464);
    float32x2_t v471 = vtrn2_f32(v464, v464);
    int64_t v473 = v468 + 1;
    float32x2_t v518 = v7[v517];
    float32x2_t v519 = vtrn1_f32(v482, v482);
    float32x2_t v520 = vtrn2_f32(v482, v482);
    int64_t v522 = v517 + 1;
    float32x2_t v531 = v7[v530];
    float32x2_t v532 = vtrn1_f32(v500, v500);
    float32x2_t v533 = vtrn2_f32(v500, v500);
    int64_t v535 = v530 + 1;
    float32x2_t v549 = v7[v548];
    float32x2_t v550 = vtrn1_f32(v544, v544);
    float32x2_t v551 = vtrn2_f32(v544, v544);
    int64_t v553 = v548 + 1;
    float32x2_t v598 = v7[v597];
    float32x2_t v599 = vtrn1_f32(v562, v562);
    float32x2_t v600 = vtrn2_f32(v562, v562);
    int64_t v602 = v597 + 1;
    float32x2_t v611 = v7[v610];
    float32x2_t v612 = vtrn1_f32(v580, v580);
    float32x2_t v613 = vtrn2_f32(v580, v580);
    int64_t v615 = v610 + 1;
    float32x2_t v629 = v7[v628];
    float32x2_t v630 = vtrn1_f32(v624, v624);
    float32x2_t v631 = vtrn2_f32(v624, v624);
    int64_t v633 = v628 + 1;
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v203 = v7[v202];
    float32x2_t v204 = vmul_f32(v199, v198);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v234 = v7[v233];
    float32x2_t v235 = vmul_f32(v230, v229);
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v314 = v7[v313];
    float32x2_t v315 = vmul_f32(v310, v309);
    float32x2_t v363 = v7[v362];
    float32x2_t v364 = vmul_f32(v359, v358);
    float32x2_t v376 = v7[v375];
    float32x2_t v377 = vmul_f32(v372, v371);
    float32x2_t v394 = v7[v393];
    float32x2_t v395 = vmul_f32(v390, v389);
    float32x2_t v443 = v7[v442];
    float32x2_t v444 = vmul_f32(v439, v438);
    float32x2_t v456 = v7[v455];
    float32x2_t v457 = vmul_f32(v452, v451);
    float32x2_t v474 = v7[v473];
    float32x2_t v475 = vmul_f32(v470, v469);
    float32x2_t v523 = v7[v522];
    float32x2_t v524 = vmul_f32(v519, v518);
    float32x2_t v536 = v7[v535];
    float32x2_t v537 = vmul_f32(v532, v531);
    float32x2_t v554 = v7[v553];
    float32x2_t v555 = vmul_f32(v550, v549);
    float32x2_t v603 = v7[v602];
    float32x2_t v604 = vmul_f32(v599, v598);
    float32x2_t v616 = v7[v615];
    float32x2_t v617 = vmul_f32(v612, v611);
    float32x2_t v634 = v7[v633];
    float32x2_t v635 = vmul_f32(v630, v629);
    float32x2_t v299 = vfma_f32(v297, v293, v296);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v206 = vfma_f32(v204, v200, v203);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v237 = vfma_f32(v235, v231, v234);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v317 = vfma_f32(v315, v311, v314);
    float32x2_t v366 = vfma_f32(v364, v360, v363);
    float32x2_t v379 = vfma_f32(v377, v373, v376);
    float32x2_t v397 = vfma_f32(v395, v391, v394);
    float32x2_t v446 = vfma_f32(v444, v440, v443);
    float32x2_t v459 = vfma_f32(v457, v453, v456);
    float32x2_t v477 = vfma_f32(v475, v471, v474);
    float32x2_t v526 = vfma_f32(v524, v520, v523);
    float32x2_t v539 = vfma_f32(v537, v533, v536);
    float32x2_t v557 = vfma_f32(v555, v551, v554);
    float32x2_t v606 = vfma_f32(v604, v600, v603);
    float32x2_t v619 = vfma_f32(v617, v613, v616);
    float32x2_t v637 = vfma_f32(v635, v631, v634);
    float32x2_t v638 = vadd_f32(v64, v77);
    float32x2_t v639 = vsub_f32(v64, v77);
    float32x2_t v646 = vadd_f32(v126, v139);
    float32x2_t v647 = vsub_f32(v126, v139);
    float32x2_t v649 = vadd_f32(v206, v219);
    float32x2_t v650 = vsub_f32(v206, v219);
    float32x2_t v652 = vadd_f32(v286, v299);
    float32x2_t v653 = vsub_f32(v286, v299);
    float32x2_t v655 = vadd_f32(v366, v379);
    float32x2_t v656 = vsub_f32(v366, v379);
    float32x2_t v658 = vadd_f32(v446, v459);
    float32x2_t v659 = vsub_f32(v446, v459);
    float32x2_t v661 = vadd_f32(v526, v539);
    float32x2_t v662 = vsub_f32(v526, v539);
    float32x2_t v664 = vadd_f32(v606, v619);
    float32x2_t v665 = vsub_f32(v606, v619);
    float32x2_t v645 = vadd_f32(v638, v644);
    float32x2_t v648 = vadd_f32(v646, v157);
    float32x2_t v651 = vadd_f32(v649, v237);
    float32x2_t v654 = vadd_f32(v652, v317);
    float32x2_t v657 = vadd_f32(v655, v397);
    float32x2_t v660 = vadd_f32(v658, v477);
    float32x2_t v663 = vadd_f32(v661, v557);
    float32x2_t v666 = vadd_f32(v664, v637);
    float32x2_t v734 = vadd_f32(v638, v655);
    float32x2_t v735 = vsub_f32(v638, v655);
    float32x2_t v736 = vadd_f32(v649, v661);
    float32x2_t v737 = vsub_f32(v649, v661);
    float32x2_t v738 = vadd_f32(v646, v658);
    float32x2_t v739 = vsub_f32(v646, v658);
    float32x2_t v740 = vadd_f32(v652, v664);
    float32x2_t v741 = vsub_f32(v652, v664);
    float32x2_t v801 = vadd_f32(v639, v656);
    float32x2_t v802 = vsub_f32(v639, v656);
    float32x2_t v803 = vadd_f32(v650, v662);
    float32x2_t v804 = vsub_f32(v650, v662);
    float32x2_t v805 = vadd_f32(v647, v659);
    float32x2_t v806 = vsub_f32(v647, v659);
    float32x2_t v807 = vadd_f32(v653, v665);
    float32x2_t v808 = vsub_f32(v653, v665);
    float32x2_t v667 = vadd_f32(v645, v657);
    float32x2_t v668 = vsub_f32(v645, v657);
    float32x2_t v669 = vadd_f32(v651, v663);
    float32x2_t v670 = vsub_f32(v651, v663);
    float32x2_t v671 = vadd_f32(v648, v660);
    float32x2_t v672 = vsub_f32(v648, v660);
    float32x2_t v673 = vadd_f32(v654, v666);
    float32x2_t v674 = vsub_f32(v654, v666);
    float32x2_t v742 = vadd_f32(v734, v736);
    float32x2_t v743 = vsub_f32(v734, v736);
    float32x2_t v744 = vadd_f32(v738, v740);
    float32x2_t v745 = vsub_f32(v738, v740);
    float32x2_t v748 = vadd_f32(v739, v741);
    float32x2_t v749 = vsub_f32(v739, v741);
    float32x2_t v772 = vmul_f32(v735, v771);
    float32x2_t v778 = vrev64_f32(v737);
    float32x2_t v809 = vadd_f32(v801, v803);
    float32x2_t v810 = vsub_f32(v801, v803);
    float32x2_t v811 = vadd_f32(v805, v807);
    float32x2_t v812 = vsub_f32(v805, v807);
    float32x2_t v815 = vadd_f32(v806, v808);
    float32x2_t v816 = vsub_f32(v806, v808);
    float32x2_t v847 = vrev64_f32(v802);
    float32x2_t v852 = vmul_f32(v804, v851);
    float32x2_t v675 = vadd_f32(v667, v669);
    float32x2_t v676 = vsub_f32(v667, v669);
    float32x2_t v677 = vadd_f32(v671, v673);
    float32x2_t v678 = vsub_f32(v671, v673);
    float32x2_t v681 = vadd_f32(v672, v674);
    float32x2_t v682 = vsub_f32(v672, v674);
    float32x2_t v711 = vrev64_f32(v670);
    float32x2_t v746 = vadd_f32(v742, v744);
    float32x2_t v747 = vsub_f32(v742, v744);
    float32x2_t v761 = vmul_f32(v743, v771);
    float32x2_t v767 = vrev64_f32(v745);
    float32x2_t v779 = vmul_f32(v778, v777);
    float32x2_t v785 = vrev64_f32(v748);
    float32x2_t v790 = vmul_f32(v749, v789);
    float32x2_t v813 = vadd_f32(v809, v811);
    float32x2_t v814 = vsub_f32(v809, v811);
    float32x2_t v836 = vrev64_f32(v810);
    float32x2_t v841 = vmul_f32(v812, v851);
    float32x2_t v848 = vmul_f32(v847, v846);
    float32x2_t v856 = vmul_f32(v815, v855);
    float32x2_t v862 = vrev64_f32(v816);
    float32x2_t v679 = vadd_f32(v675, v677);
    float32x2_t v680 = vsub_f32(v675, v677);
    float32x2_t v700 = vrev64_f32(v678);
    float32x2_t v712 = vmul_f32(v711, v710);
    float32x2_t v718 = vrev64_f32(v681);
    float32x2_t v723 = vmul_f32(v682, v722);
    float32x2_t v753 = vmul_f32(v746, v771);
    float32x2_t v757 = vmul_f32(v747, v771);
    float32x2_t v768 = vmul_f32(v767, v777);
    float32x2_t v786 = vmul_f32(v785, v784);
    float32x2_t v793 = vadd_f32(v772, v790);
    float32x2_t v794 = vsub_f32(v772, v790);
    float32x2_t v822 = vrev64_f32(v813);
    float32x2_t v829 = vrev64_f32(v814);
    float32x2_t v837 = vmul_f32(v836, v846);
    float32x2_t v863 = vmul_f32(v862, v861);
    float32x2_t v868 = vadd_f32(v852, v856);
    float32x2_t v869 = vsub_f32(v852, v856);
    float32x2_t v701 = vmul_f32(v700, v710);
    float32x2_t v719 = vmul_f32(v718, v717);
    float32x2_t v726 = vadd_f32(v668, v723);
    float32x2_t v727 = vsub_f32(v668, v723);
    float32x2_t v791 = vadd_f32(v761, v768);
    float32x2_t v792 = vsub_f32(v761, v768);
    float32x2_t v795 = vadd_f32(v779, v786);
    float32x2_t v796 = vsub_f32(v779, v786);
    float32x2_t v823 = vmul_f32(v822, v846);
    float32x2_t v830 = vmul_f32(v829, v846);
    float32x2_t v864 = vadd_f32(v837, v841);
    float32x2_t v865 = vsub_f32(v837, v841);
    float32x2_t v866 = vadd_f32(v848, v863);
    float32x2_t v867 = vsub_f32(v848, v863);
    float32x2_t v874 = vadd_f32(v679, v753);
    int16x4_t v879 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v679, 15), (int32x2_t){0, 0}));
    float32x2_t v958 = vadd_f32(v680, v757);
    int16x4_t v963 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v680, 15), (int32x2_t){0, 0}));
    float32x2_t v724 = vadd_f32(v676, v701);
    float32x2_t v725 = vsub_f32(v676, v701);
    float32x2_t v728 = vadd_f32(v712, v719);
    float32x2_t v729 = vsub_f32(v712, v719);
    float32x2_t v797 = vadd_f32(v793, v795);
    float32x2_t v798 = vsub_f32(v793, v795);
    float32x2_t v799 = vadd_f32(v794, v796);
    float32x2_t v800 = vsub_f32(v794, v796);
    float32x2_t v870 = vadd_f32(v866, v868);
    float32x2_t v871 = vsub_f32(v866, v868);
    float32x2_t v872 = vadd_f32(v867, v869);
    float32x2_t v873 = vsub_f32(v867, v869);
    float32x2_t v875 = vadd_f32(v874, v823);
    float32x2_t v876 = vsub_f32(v874, v823);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v879), 0);
    float32x2_t v959 = vadd_f32(v958, v830);
    float32x2_t v960 = vsub_f32(v958, v830);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v963), 0);
    float32x2_t v730 = vadd_f32(v726, v728);
    float32x2_t v731 = vsub_f32(v726, v728);
    float32x2_t v732 = vadd_f32(v727, v729);
    float32x2_t v733 = vsub_f32(v727, v729);
    int16x4_t v885 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v876, 15), (int32x2_t){0, 0}));
    int16x4_t v891 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v875, 15), (int32x2_t){0, 0}));
    float32x2_t v916 = vadd_f32(v725, v792);
    int16x4_t v921 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v725, 15), (int32x2_t){0, 0}));
    int16x4_t v969 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v960, 15), (int32x2_t){0, 0}));
    int16x4_t v975 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v959, 15), (int32x2_t){0, 0}));
    float32x2_t v1000 = vadd_f32(v724, v791);
    int16x4_t v1005 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v724, 15), (int32x2_t){0, 0}));
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v885), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v891), 0);
    float32x2_t v895 = vadd_f32(v731, v798);
    int16x4_t v900 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v731, 15), (int32x2_t){0, 0}));
    float32x2_t v917 = vadd_f32(v916, v865);
    float32x2_t v918 = vsub_f32(v916, v865);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v921), 0);
    float32x2_t v937 = vadd_f32(v732, v799);
    int16x4_t v942 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v732, 15), (int32x2_t){0, 0}));
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v969), 0);
    v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v975), 0);
    float32x2_t v979 = vadd_f32(v733, v800);
    int16x4_t v984 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v733, 15), (int32x2_t){0, 0}));
    float32x2_t v1001 = vadd_f32(v1000, v864);
    float32x2_t v1002 = vsub_f32(v1000, v864);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v1005), 0);
    float32x2_t v1021 = vadd_f32(v730, v797);
    int16x4_t v1026 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v730, 15), (int32x2_t){0, 0}));
    float32x2_t v896 = vadd_f32(v895, v871);
    float32x2_t v897 = vsub_f32(v895, v871);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v900), 0);
    int16x4_t v927 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v918, 15), (int32x2_t){0, 0}));
    int16x4_t v933 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v917, 15), (int32x2_t){0, 0}));
    float32x2_t v938 = vadd_f32(v937, v872);
    float32x2_t v939 = vsub_f32(v937, v872);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v942), 0);
    float32x2_t v980 = vadd_f32(v979, v873);
    float32x2_t v981 = vsub_f32(v979, v873);
    v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v984), 0);
    int16x4_t v1011 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1002, 15), (int32x2_t){0, 0}));
    int16x4_t v1017 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1001, 15), (int32x2_t){0, 0}));
    float32x2_t v1022 = vadd_f32(v1021, v870);
    float32x2_t v1023 = vsub_f32(v1021, v870);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v1026), 0);
    int16x4_t v906 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v897, 15), (int32x2_t){0, 0}));
    int16x4_t v912 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v896, 15), (int32x2_t){0, 0}));
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v927), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v933), 0);
    int16x4_t v948 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v939, 15), (int32x2_t){0, 0}));
    int16x4_t v954 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v938, 15), (int32x2_t){0, 0}));
    int16x4_t v990 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v981, 15), (int32x2_t){0, 0}));
    int16x4_t v996 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v980, 15), (int32x2_t){0, 0}));
    v6[ostride * 22] = vget_lane_s32(vreinterpret_s32_s16(v1011), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v1017), 0);
    int16x4_t v1032 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1023, 15), (int32x2_t){0, 0}));
    int16x4_t v1038 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1022, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v906), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v912), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v948), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v954), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v990), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v996), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v1032), 0);
    v6[ostride * 23] = vget_lane_s32(vreinterpret_s32_s16(v1038), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu24(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v527 = -1.0000000000000000e+00F;
    float v534 = -7.0710678118654746e-01F;
    float v541 = 7.0710678118654757e-01F;
    float v594 = -1.4999999999999998e+00F;
    float v599 = 1.4999999999999998e+00F;
    float v606 = 1.0606601717798210e+00F;
    float v613 = -1.0606601717798212e+00F;
    float v677 = -8.6602540378443871e-01F;
    float v687 = -6.1237243569579458e-01F;
    const float32x2_t *v1006 = &v5[v0];
    int32_t *v1205 = &v6[v2];
    int64_t v19 = v0 * 8;
    int64_t v33 = v0 * 16;
    int64_t v48 = v10 * 7;
    int64_t v55 = v10 * 15;
    int64_t v61 = v0 * 11;
    int64_t v75 = v0 * 19;
    int64_t v90 = v10 * 10;
    int64_t v97 = v10 * 18;
    int64_t v103 = v0 * 3;
    int64_t v111 = v10 * 2;
    int64_t v117 = v0 * 14;
    int64_t v131 = v0 * 22;
    int64_t v146 = v10 * 13;
    int64_t v153 = v10 * 21;
    int64_t v159 = v0 * 6;
    int64_t v167 = v10 * 5;
    int64_t v173 = v0 * 17;
    int64_t v202 = v10 * 16;
    int64_t v215 = v0 * 9;
    int64_t v223 = v10 * 8;
    int64_t v229 = v0 * 20;
    int64_t v243 = v0 * 4;
    int64_t v258 = v10 * 19;
    int64_t v265 = v10 * 3;
    int64_t v271 = v0 * 12;
    int64_t v279 = v10 * 11;
    int64_t v285 = v0 * 23;
    int64_t v299 = v0 * 7;
    int64_t v314 = v10 * 22;
    int64_t v321 = v10 * 6;
    int64_t v327 = v0 * 15;
    int64_t v335 = v10 * 14;
    int64_t v341 = v0 * 2;
    int64_t v355 = v0 * 10;
    int64_t v377 = v10 * 9;
    int64_t v383 = v0 * 18;
    int64_t v391 = v10 * 17;
    int64_t v397 = v0 * 5;
    int64_t v411 = v0 * 13;
    int64_t v426 = v10 * 4;
    int64_t v433 = v10 * 12;
    int64_t v439 = v0 * 21;
    int64_t v447 = v10 * 20;
    int64_t v448 = v13 * 23;
    float v530 = v4 * v527;
    float v537 = v4 * v534;
    float v602 = v4 * v599;
    float v609 = v4 * v606;
    float v673 = v4 * v677;
    float v690 = v4 * v687;
    int64_t v715 = v2 * 16;
    int64_t v723 = v2 * 8;
    int64_t v734 = v2 * 9;
    int64_t v750 = v2 * 17;
    int64_t v761 = v2 * 18;
    int64_t v769 = v2 * 10;
    int64_t v777 = v2 * 2;
    int64_t v788 = v2 * 3;
    int64_t v796 = v2 * 19;
    int64_t v804 = v2 * 11;
    int64_t v815 = v2 * 12;
    int64_t v823 = v2 * 4;
    int64_t v831 = v2 * 20;
    int64_t v842 = v2 * 21;
    int64_t v850 = v2 * 13;
    int64_t v858 = v2 * 5;
    int64_t v869 = v2 * 6;
    int64_t v877 = v2 * 22;
    int64_t v885 = v2 * 14;
    int64_t v896 = v2 * 15;
    int64_t v904 = v2 * 7;
    int64_t v912 = v2 * 23;
    const float32x2_t *v1135 = &v5[0];
    svint64_t v1136 = svindex_s64(0, v1);
    svfloat32_t v1145 = svdup_n_f32(v541);
    svfloat32_t v1150 = svdup_n_f32(v594);
    svfloat32_t v1153 = svdup_n_f32(v613);
    svfloat32_t v1159 = svdup_n_f32(v677);
    svfloat32_t v1160 = svdup_n_f32(v687);
    int32_t *v1169 = &v6[0];
    int64_t v50 = v48 + v448;
    int64_t v57 = v55 + v448;
    int64_t v92 = v90 + v448;
    int64_t v99 = v97 + v448;
    int64_t v113 = v111 + v448;
    int64_t v148 = v146 + v448;
    int64_t v155 = v153 + v448;
    int64_t v169 = v167 + v448;
    int64_t v204 = v202 + v448;
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v448]));
    int64_t v225 = v223 + v448;
    int64_t v260 = v258 + v448;
    int64_t v267 = v265 + v448;
    int64_t v281 = v279 + v448;
    int64_t v316 = v314 + v448;
    int64_t v323 = v321 + v448;
    int64_t v337 = v335 + v448;
    int64_t v372 = v10 + v448;
    int64_t v379 = v377 + v448;
    int64_t v393 = v391 + v448;
    int64_t v428 = v426 + v448;
    int64_t v435 = v433 + v448;
    int64_t v449 = v447 + v448;
    const float32x2_t *v925 = &v5[v19];
    const float32x2_t *v934 = &v5[v33];
    const float32x2_t *v943 = &v5[v61];
    const float32x2_t *v952 = &v5[v75];
    const float32x2_t *v961 = &v5[v103];
    const float32x2_t *v970 = &v5[v117];
    const float32x2_t *v979 = &v5[v131];
    const float32x2_t *v988 = &v5[v159];
    const float32x2_t *v997 = &v5[v173];
    svfloat32_t v1008 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1006), v1136));
    const float32x2_t *v1017 = &v5[v215];
    const float32x2_t *v1026 = &v5[v229];
    const float32x2_t *v1035 = &v5[v243];
    const float32x2_t *v1044 = &v5[v271];
    const float32x2_t *v1053 = &v5[v285];
    const float32x2_t *v1062 = &v5[v299];
    const float32x2_t *v1071 = &v5[v327];
    const float32x2_t *v1080 = &v5[v341];
    const float32x2_t *v1089 = &v5[v355];
    const float32x2_t *v1098 = &v5[v383];
    const float32x2_t *v1107 = &v5[v397];
    const float32x2_t *v1116 = &v5[v411];
    const float32x2_t *v1125 = &v5[v439];
    svfloat32_t v1137 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1135), v1136));
    svfloat32_t v1143 = svdup_n_f32(v530);
    svfloat32_t v1144 = svdup_n_f32(v537);
    svfloat32_t v1151 = svdup_n_f32(v602);
    svfloat32_t v1152 = svdup_n_f32(v609);
    svfloat32_t v1158 = svdup_n_f32(v673);
    svfloat32_t v1161 = svdup_n_f32(v690);
    int32_t *v1178 = &v6[v715];
    int32_t *v1187 = &v6[v723];
    int32_t *v1196 = &v6[v734];
    int32_t *v1214 = &v6[v750];
    int32_t *v1223 = &v6[v761];
    int32_t *v1232 = &v6[v769];
    int32_t *v1241 = &v6[v777];
    int32_t *v1250 = &v6[v788];
    int32_t *v1259 = &v6[v796];
    int32_t *v1268 = &v6[v804];
    int32_t *v1277 = &v6[v815];
    int32_t *v1286 = &v6[v823];
    int32_t *v1295 = &v6[v831];
    int32_t *v1304 = &v6[v842];
    int32_t *v1313 = &v6[v850];
    int32_t *v1322 = &v6[v858];
    int32_t *v1331 = &v6[v869];
    int32_t *v1340 = &v6[v877];
    int32_t *v1349 = &v6[v885];
    int32_t *v1358 = &v6[v896];
    int32_t *v1367 = &v6[v904];
    int32_t *v1376 = &v6[v912];
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v50]));
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v149 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v148]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero213, v1008, v212, 0), v1008,
        v212, 90);
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v317 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v316]));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v338 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v337]));
    svfloat32_t v373 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v372]));
    svfloat32_t v380 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v379]));
    svfloat32_t v394 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v393]));
    svfloat32_t v429 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v428]));
    svfloat32_t v436 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v435]));
    svfloat32_t v450 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v449]));
    svfloat32_t v927 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v925), v1136));
    svfloat32_t v936 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v934), v1136));
    svfloat32_t v945 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v943), v1136));
    svfloat32_t v954 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v952), v1136));
    svfloat32_t v963 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v961), v1136));
    svfloat32_t v972 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v970), v1136));
    svfloat32_t v981 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v979), v1136));
    svfloat32_t v990 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v988), v1136));
    svfloat32_t v999 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v997), v1136));
    svfloat32_t v1019 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1017), v1136));
    svfloat32_t v1028 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1026), v1136));
    svfloat32_t v1037 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1035), v1136));
    svfloat32_t v1046 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1044), v1136));
    svfloat32_t v1055 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1053), v1136));
    svfloat32_t v1064 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1062), v1136));
    svfloat32_t v1073 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1071), v1136));
    svfloat32_t v1082 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1080), v1136));
    svfloat32_t v1091 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1089), v1136));
    svfloat32_t v1100 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1098), v1136));
    svfloat32_t v1109 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1107), v1136));
    svfloat32_t v1118 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1116), v1136));
    svfloat32_t v1127 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1125), v1136));
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v927, v51, 0),
                     v927, v51, 90);
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v936, v58, 0),
                     v936, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v945, v93, 0),
                     v945, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v954, v100, 0),
                     v954, v100, 90);
    svfloat32_t zero150 = svdup_n_f32(0);
    svfloat32_t v150 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero150, v972, v149, 0),
                     v972, v149, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v981, v156, 0),
                     v981, v156, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v999, v205, 0),
                     v999, v205, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero262, v1028, v261, 0), v1028,
        v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero269, v1037, v268, 0), v1037,
        v268, 90);
    svfloat32_t zero318 = svdup_n_f32(0);
    svfloat32_t v318 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero318, v1055, v317, 0), v1055,
        v317, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero325, v1064, v324, 0), v1064,
        v324, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero374, v1082, v373, 0), v1082,
        v373, 90);
    svfloat32_t zero381 = svdup_n_f32(0);
    svfloat32_t v381 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero381, v1091, v380, 0), v1091,
        v380, 90);
    svfloat32_t zero430 = svdup_n_f32(0);
    svfloat32_t v430 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero430, v1109, v429, 0), v1109,
        v429, 90);
    svfloat32_t zero437 = svdup_n_f32(0);
    svfloat32_t v437 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero437, v1118, v436, 0), v1118,
        v436, 90);
    svfloat32_t v452 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v453 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v462 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v463 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v465 = svadd_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v466 = svsub_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v468 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v469 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v471 = svadd_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v472 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v474 = svadd_f32_x(svptrue_b32(), v318, v325);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v318, v325);
    svfloat32_t v477 = svadd_f32_x(svptrue_b32(), v374, v381);
    svfloat32_t v478 = svsub_f32_x(svptrue_b32(), v374, v381);
    svfloat32_t v480 = svadd_f32_x(svptrue_b32(), v430, v437);
    svfloat32_t v481 = svsub_f32_x(svptrue_b32(), v430, v437);
    svfloat32_t v461 = svadd_f32_x(svptrue_b32(), v452, v1137);
    svfloat32_t v464 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v462, v963, v114, 0),
                     v963, v114, 90);
    svfloat32_t v467 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v465, v990, v170, 0),
                     v990, v170, 90);
    svfloat32_t v470 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v468, v1019, v226, 0),
                     v1019, v226, 90);
    svfloat32_t v473 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v471, v1046, v282, 0),
                     v1046, v282, 90);
    svfloat32_t v476 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v474, v1073, v338, 0),
                     v1073, v338, 90);
    svfloat32_t v479 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v477, v1100, v394, 0),
                     v1100, v394, 90);
    svfloat32_t v482 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v480, v1127, v450, 0),
                     v1127, v450, 90);
    svfloat32_t v555 = svadd_f32_x(svptrue_b32(), v452, v471);
    svfloat32_t v556 = svsub_f32_x(svptrue_b32(), v452, v471);
    svfloat32_t v557 = svadd_f32_x(svptrue_b32(), v465, v477);
    svfloat32_t v558 = svsub_f32_x(svptrue_b32(), v465, v477);
    svfloat32_t v559 = svadd_f32_x(svptrue_b32(), v462, v474);
    svfloat32_t v560 = svsub_f32_x(svptrue_b32(), v462, v474);
    svfloat32_t v561 = svadd_f32_x(svptrue_b32(), v468, v480);
    svfloat32_t v562 = svsub_f32_x(svptrue_b32(), v468, v480);
    svfloat32_t v627 = svadd_f32_x(svptrue_b32(), v453, v472);
    svfloat32_t v628 = svsub_f32_x(svptrue_b32(), v453, v472);
    svfloat32_t v629 = svadd_f32_x(svptrue_b32(), v466, v478);
    svfloat32_t v630 = svsub_f32_x(svptrue_b32(), v466, v478);
    svfloat32_t v631 = svadd_f32_x(svptrue_b32(), v463, v475);
    svfloat32_t v632 = svsub_f32_x(svptrue_b32(), v463, v475);
    svfloat32_t v633 = svadd_f32_x(svptrue_b32(), v469, v481);
    svfloat32_t v634 = svsub_f32_x(svptrue_b32(), v469, v481);
    svfloat32_t v483 = svadd_f32_x(svptrue_b32(), v461, v473);
    svfloat32_t v484 = svsub_f32_x(svptrue_b32(), v461, v473);
    svfloat32_t v485 = svadd_f32_x(svptrue_b32(), v467, v479);
    svfloat32_t v486 = svsub_f32_x(svptrue_b32(), v467, v479);
    svfloat32_t v487 = svadd_f32_x(svptrue_b32(), v464, v476);
    svfloat32_t v488 = svsub_f32_x(svptrue_b32(), v464, v476);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v470, v482);
    svfloat32_t v490 = svsub_f32_x(svptrue_b32(), v470, v482);
    svfloat32_t v563 = svadd_f32_x(svptrue_b32(), v555, v557);
    svfloat32_t v564 = svsub_f32_x(svptrue_b32(), v555, v557);
    svfloat32_t v565 = svadd_f32_x(svptrue_b32(), v559, v561);
    svfloat32_t v566 = svsub_f32_x(svptrue_b32(), v559, v561);
    svfloat32_t v569 = svadd_f32_x(svptrue_b32(), v560, v562);
    svfloat32_t v570 = svsub_f32_x(svptrue_b32(), v560, v562);
    svfloat32_t zero604 = svdup_n_f32(0);
    svfloat32_t v604 = svcmla_f32_x(pred_full, zero604, v1151, v558, 90);
    svfloat32_t v635 = svadd_f32_x(svptrue_b32(), v627, v629);
    svfloat32_t v636 = svsub_f32_x(svptrue_b32(), v627, v629);
    svfloat32_t v637 = svadd_f32_x(svptrue_b32(), v631, v633);
    svfloat32_t v638 = svsub_f32_x(svptrue_b32(), v631, v633);
    svfloat32_t v641 = svadd_f32_x(svptrue_b32(), v632, v634);
    svfloat32_t v642 = svsub_f32_x(svptrue_b32(), v632, v634);
    svfloat32_t zero675 = svdup_n_f32(0);
    svfloat32_t v675 = svcmla_f32_x(pred_full, zero675, v1158, v628, 90);
    svfloat32_t v491 = svadd_f32_x(svptrue_b32(), v483, v485);
    svfloat32_t v492 = svsub_f32_x(svptrue_b32(), v483, v485);
    svfloat32_t v493 = svadd_f32_x(svptrue_b32(), v487, v489);
    svfloat32_t v494 = svsub_f32_x(svptrue_b32(), v487, v489);
    svfloat32_t v497 = svadd_f32_x(svptrue_b32(), v488, v490);
    svfloat32_t v498 = svsub_f32_x(svptrue_b32(), v488, v490);
    svfloat32_t zero532 = svdup_n_f32(0);
    svfloat32_t v532 = svcmla_f32_x(pred_full, zero532, v1143, v486, 90);
    svfloat32_t v567 = svadd_f32_x(svptrue_b32(), v563, v565);
    svfloat32_t v568 = svsub_f32_x(svptrue_b32(), v563, v565);
    svfloat32_t zero592 = svdup_n_f32(0);
    svfloat32_t v592 = svcmla_f32_x(pred_full, zero592, v1151, v566, 90);
    svfloat32_t zero611 = svdup_n_f32(0);
    svfloat32_t v611 = svcmla_f32_x(pred_full, zero611, v1152, v569, 90);
    svfloat32_t v616 = svmul_f32_x(svptrue_b32(), v570, v1153);
    svfloat32_t v639 = svadd_f32_x(svptrue_b32(), v635, v637);
    svfloat32_t v640 = svsub_f32_x(svptrue_b32(), v635, v637);
    svfloat32_t zero663 = svdup_n_f32(0);
    svfloat32_t v663 = svcmla_f32_x(pred_full, zero663, v1158, v636, 90);
    svfloat32_t v685 = svmul_f32_x(svptrue_b32(), v641, v1160);
    svfloat32_t zero692 = svdup_n_f32(0);
    svfloat32_t v692 = svcmla_f32_x(pred_full, zero692, v1161, v642, 90);
    svfloat32_t v495 = svadd_f32_x(svptrue_b32(), v491, v493);
    svfloat32_t v496 = svsub_f32_x(svptrue_b32(), v491, v493);
    svfloat32_t zero520 = svdup_n_f32(0);
    svfloat32_t v520 = svcmla_f32_x(pred_full, zero520, v1143, v494, 90);
    svfloat32_t zero539 = svdup_n_f32(0);
    svfloat32_t v539 = svcmla_f32_x(pred_full, zero539, v1144, v497, 90);
    svfloat32_t v617 = svmla_f32_x(pred_full, v592, v564, v1150);
    svfloat32_t v618 = svnmls_f32_x(pred_full, v592, v564, v1150);
    svfloat32_t v619 = svmla_f32_x(pred_full, v616, v556, v1150);
    svfloat32_t v620 = svnmls_f32_x(pred_full, v616, v556, v1150);
    svfloat32_t v621 = svadd_f32_x(svptrue_b32(), v604, v611);
    svfloat32_t v622 = svsub_f32_x(svptrue_b32(), v604, v611);
    svfloat32_t zero649 = svdup_n_f32(0);
    svfloat32_t v649 = svcmla_f32_x(pred_full, zero649, v1158, v639, 90);
    svfloat32_t zero656 = svdup_n_f32(0);
    svfloat32_t v656 = svcmla_f32_x(pred_full, zero656, v1158, v640, 90);
    svfloat32_t v693 = svmla_f32_x(pred_full, v663, v638, v1159);
    svfloat32_t v694 = svmls_f32_x(pred_full, v663, v638, v1159);
    svfloat32_t v695 = svadd_f32_x(svptrue_b32(), v675, v692);
    svfloat32_t v696 = svsub_f32_x(svptrue_b32(), v675, v692);
    svfloat32_t v697 = svmla_f32_x(pred_full, v685, v630, v1159);
    svfloat32_t v698 = svnmls_f32_x(pred_full, v685, v630, v1159);
    svfloat32_t v545 = svadd_f32_x(svptrue_b32(), v492, v520);
    svfloat32_t v546 = svsub_f32_x(svptrue_b32(), v492, v520);
    svfloat32_t v547 = svmla_f32_x(pred_full, v484, v498, v1145);
    svfloat32_t v548 = svmls_f32_x(pred_full, v484, v498, v1145);
    svfloat32_t v549 = svadd_f32_x(svptrue_b32(), v532, v539);
    svfloat32_t v550 = svsub_f32_x(svptrue_b32(), v532, v539);
    svfloat32_t v623 = svadd_f32_x(svptrue_b32(), v619, v621);
    svfloat32_t v624 = svsub_f32_x(svptrue_b32(), v619, v621);
    svfloat32_t v625 = svadd_f32_x(svptrue_b32(), v620, v622);
    svfloat32_t v626 = svsub_f32_x(svptrue_b32(), v620, v622);
    svfloat32_t v699 = svadd_f32_x(svptrue_b32(), v695, v697);
    svfloat32_t v700 = svsub_f32_x(svptrue_b32(), v695, v697);
    svfloat32_t v701 = svadd_f32_x(svptrue_b32(), v696, v698);
    svfloat32_t v702 = svsub_f32_x(svptrue_b32(), v696, v698);
    svfloat32_t v703 = svmla_f32_x(pred_full, v495, v567, v1150);
    svint16_t v708 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v495, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v811 = svmla_f32_x(pred_full, v496, v568, v1150);
    svint16_t v816 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v496, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v551 = svadd_f32_x(svptrue_b32(), v547, v549);
    svfloat32_t v552 = svsub_f32_x(svptrue_b32(), v547, v549);
    svfloat32_t v553 = svadd_f32_x(svptrue_b32(), v548, v550);
    svfloat32_t v554 = svsub_f32_x(svptrue_b32(), v548, v550);
    svfloat32_t v704 = svadd_f32_x(svptrue_b32(), v703, v649);
    svfloat32_t v705 = svsub_f32_x(svptrue_b32(), v703, v649);
    svfloat32_t v757 = svadd_f32_x(svptrue_b32(), v546, v618);
    svint16_t v762 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v546, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v812 = svadd_f32_x(svptrue_b32(), v811, v656);
    svfloat32_t v813 = svsub_f32_x(svptrue_b32(), v811, v656);
    svfloat32_t v865 = svadd_f32_x(svptrue_b32(), v545, v617);
    svint16_t v870 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v545, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1169), svreinterpret_u64_s16(v708));
    svst1w_u64(pred_full, (unsigned *)(v1277), svreinterpret_u64_s16(v816));
    svint16_t v716 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v705, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v724 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v704, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v730 = svadd_f32_x(svptrue_b32(), v552, v624);
    svint16_t v735 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v552, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v758 = svadd_f32_x(svptrue_b32(), v757, v694);
    svfloat32_t v759 = svsub_f32_x(svptrue_b32(), v757, v694);
    svfloat32_t v784 = svadd_f32_x(svptrue_b32(), v553, v625);
    svint16_t v789 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v553, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v824 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v813, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v832 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v812, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v838 = svadd_f32_x(svptrue_b32(), v554, v626);
    svint16_t v843 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v554, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v866 = svadd_f32_x(svptrue_b32(), v865, v693);
    svfloat32_t v867 = svsub_f32_x(svptrue_b32(), v865, v693);
    svfloat32_t v892 = svadd_f32_x(svptrue_b32(), v551, v623);
    svint16_t v897 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v551, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1223), svreinterpret_u64_s16(v762));
    svst1w_u64(pred_full, (unsigned *)(v1331), svreinterpret_u64_s16(v870));
    svfloat32_t v731 = svadd_f32_x(svptrue_b32(), v730, v700);
    svfloat32_t v732 = svsub_f32_x(svptrue_b32(), v730, v700);
    svint16_t v770 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v759, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v778 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v758, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v785 = svadd_f32_x(svptrue_b32(), v784, v701);
    svfloat32_t v786 = svsub_f32_x(svptrue_b32(), v784, v701);
    svfloat32_t v839 = svadd_f32_x(svptrue_b32(), v838, v702);
    svfloat32_t v840 = svsub_f32_x(svptrue_b32(), v838, v702);
    svint16_t v878 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v867, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v886 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v866, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v893 = svadd_f32_x(svptrue_b32(), v892, v699);
    svfloat32_t v894 = svsub_f32_x(svptrue_b32(), v892, v699);
    svst1w_u64(pred_full, (unsigned *)(v1178), svreinterpret_u64_s16(v716));
    svst1w_u64(pred_full, (unsigned *)(v1187), svreinterpret_u64_s16(v724));
    svst1w_u64(pred_full, (unsigned *)(v1196), svreinterpret_u64_s16(v735));
    svst1w_u64(pred_full, (unsigned *)(v1250), svreinterpret_u64_s16(v789));
    svst1w_u64(pred_full, (unsigned *)(v1286), svreinterpret_u64_s16(v824));
    svst1w_u64(pred_full, (unsigned *)(v1295), svreinterpret_u64_s16(v832));
    svst1w_u64(pred_full, (unsigned *)(v1304), svreinterpret_u64_s16(v843));
    svst1w_u64(pred_full, (unsigned *)(v1358), svreinterpret_u64_s16(v897));
    svint16_t v743 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v732, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v751 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v731, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v797 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v786, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v805 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v785, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v851 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v840, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v859 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v839, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v905 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v894, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v913 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v893, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1232), svreinterpret_u64_s16(v770));
    svst1w_u64(pred_full, (unsigned *)(v1241), svreinterpret_u64_s16(v778));
    svst1w_u64(pred_full, (unsigned *)(v1340), svreinterpret_u64_s16(v878));
    svst1w_u64(pred_full, (unsigned *)(v1349), svreinterpret_u64_s16(v886));
    svst1w_u64(pred_full, (unsigned *)(v1205), svreinterpret_u64_s16(v743));
    svst1w_u64(pred_full, (unsigned *)(v1214), svreinterpret_u64_s16(v751));
    svst1w_u64(pred_full, (unsigned *)(v1259), svreinterpret_u64_s16(v797));
    svst1w_u64(pred_full, (unsigned *)(v1268), svreinterpret_u64_s16(v805));
    svst1w_u64(pred_full, (unsigned *)(v1313), svreinterpret_u64_s16(v851));
    svst1w_u64(pred_full, (unsigned *)(v1322), svreinterpret_u64_s16(v859));
    svst1w_u64(pred_full, (unsigned *)(v1367), svreinterpret_u64_s16(v905));
    svst1w_u64(pred_full, (unsigned *)(v1376), svreinterpret_u64_s16(v913));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu25(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v92 = v5[istride];
    float v1168 = 9.6858316112863108e-01F;
    float v1171 = -2.4868988716485479e-01F;
    float v1172 = 2.4868988716485479e-01F;
    float v1312 = 8.7630668004386358e-01F;
    float v1315 = -4.8175367410171532e-01F;
    float v1316 = 4.8175367410171532e-01F;
    float v1456 = 7.2896862742141155e-01F;
    float v1459 = -6.8454710592868862e-01F;
    float v1460 = 6.8454710592868862e-01F;
    float v1468 = 6.2790519529313527e-02F;
    float v1471 = -9.9802672842827156e-01F;
    float v1472 = 9.9802672842827156e-01F;
    float v1600 = 5.3582679497899655e-01F;
    float v1603 = -8.4432792550201508e-01F;
    float v1604 = 8.4432792550201508e-01F;
    float v1612 = -4.2577929156507272e-01F;
    float v1615 = -9.0482705246601947e-01F;
    float v1616 = 9.0482705246601947e-01F;
    float v1624 = -6.3742398974868952e-01F;
    float v1627 = 7.7051324277578936e-01F;
    float v1628 = -7.7051324277578936e-01F;
    float v1642 = -9.9211470131447776e-01F;
    float v1645 = -1.2533323356430454e-01F;
    float v1646 = 1.2533323356430454e-01F;
    float v1662 = 2.5000000000000000e-01F;
    float v1672 = 5.5901699437494745e-01F;
    float v1682 = 6.1803398874989490e-01F;
    float v1707 = 9.5105651629515353e-01F;
    float v1708 = -9.5105651629515353e-01F;
    float32x2_t v1710 = (float32x2_t){v4, v4};
    float v1733 = 2.0000000000000000e+00F;
    float32x2_t v98 = vtrn1_f32(v92, v92);
    float32x2_t v99 = vtrn2_f32(v92, v92);
    float32x2_t v452 = v5[0];
    float32x2_t v1169 = (float32x2_t){v1168, v1168};
    float32x2_t v1173 = (float32x2_t){v1171, v1172};
    float32x2_t v1313 = (float32x2_t){v1312, v1312};
    float32x2_t v1317 = (float32x2_t){v1315, v1316};
    float32x2_t v1457 = (float32x2_t){v1456, v1456};
    float32x2_t v1461 = (float32x2_t){v1459, v1460};
    float32x2_t v1469 = (float32x2_t){v1468, v1468};
    float32x2_t v1473 = (float32x2_t){v1471, v1472};
    float32x2_t v1503 = (float32x2_t){v1628, v1627};
    float32x2_t v1601 = (float32x2_t){v1600, v1600};
    float32x2_t v1605 = (float32x2_t){v1603, v1604};
    float32x2_t v1613 = (float32x2_t){v1612, v1612};
    float32x2_t v1617 = (float32x2_t){v1615, v1616};
    float32x2_t v1625 = (float32x2_t){v1624, v1624};
    float32x2_t v1629 = (float32x2_t){v1627, v1628};
    float32x2_t v1643 = (float32x2_t){v1642, v1642};
    float32x2_t v1647 = (float32x2_t){v1645, v1646};
    float32x2_t v1663 = (float32x2_t){v1662, v1662};
    float32x2_t v1673 = (float32x2_t){v1672, v1672};
    float32x2_t v1683 = (float32x2_t){v1682, v1682};
    float32x2_t v1709 = (float32x2_t){v1707, v1708};
    float32x2_t v1734 = (float32x2_t){v1733, v1733};
    float32x2_t v20 = v5[istride * 5];
    int64_t v24 = 8 + j * 48;
    float32x2_t v38 = v5[istride * 10];
    int64_t v42 = 18 + j * 48;
    float32x2_t v56 = v5[istride * 15];
    int64_t v60 = 28 + j * 48;
    float32x2_t v74 = v5[istride * 20];
    int64_t v78 = 38 + j * 48;
    float32x2_t v97 = v7[j * 48];
    int64_t v101 = j * 48 + 1;
    float32x2_t v110 = v5[istride * 6];
    int64_t v114 = 10 + j * 48;
    float32x2_t v128 = v5[istride * 11];
    int64_t v132 = 20 + j * 48;
    float32x2_t v146 = v5[istride * 16];
    int64_t v150 = 30 + j * 48;
    float32x2_t v164 = v5[istride * 21];
    int64_t v168 = 40 + j * 48;
    float32x2_t v182 = v5[istride * 2];
    int64_t v186 = 2 + j * 48;
    float32x2_t v200 = v5[istride * 7];
    int64_t v204 = 12 + j * 48;
    float32x2_t v218 = v5[istride * 12];
    int64_t v222 = 22 + j * 48;
    float32x2_t v236 = v5[istride * 17];
    int64_t v240 = 32 + j * 48;
    float32x2_t v254 = v5[istride * 22];
    int64_t v258 = 42 + j * 48;
    float32x2_t v272 = v5[istride * 3];
    int64_t v276 = 4 + j * 48;
    float32x2_t v290 = v5[istride * 8];
    int64_t v294 = 14 + j * 48;
    float32x2_t v308 = v5[istride * 13];
    int64_t v312 = 24 + j * 48;
    float32x2_t v326 = v5[istride * 18];
    int64_t v330 = 34 + j * 48;
    float32x2_t v344 = v5[istride * 23];
    int64_t v348 = 44 + j * 48;
    float32x2_t v362 = v5[istride * 4];
    int64_t v366 = 6 + j * 48;
    float32x2_t v380 = v5[istride * 9];
    int64_t v384 = 16 + j * 48;
    float32x2_t v398 = v5[istride * 14];
    int64_t v402 = 26 + j * 48;
    float32x2_t v416 = v5[istride * 19];
    int64_t v420 = 36 + j * 48;
    float32x2_t v434 = v5[istride * 24];
    int64_t v438 = 46 + j * 48;
    float32x2_t v1175 = vmul_f32(v1710, v1173);
    float32x2_t v1319 = vmul_f32(v1710, v1317);
    float32x2_t v1463 = vmul_f32(v1710, v1461);
    float32x2_t v1475 = vmul_f32(v1710, v1473);
    float32x2_t v1505 = vmul_f32(v1710, v1503);
    float32x2_t v1607 = vmul_f32(v1710, v1605);
    float32x2_t v1619 = vmul_f32(v1710, v1617);
    float32x2_t v1631 = vmul_f32(v1710, v1629);
    float32x2_t v1649 = vmul_f32(v1710, v1647);
    float32x2_t v1711 = vmul_f32(v1710, v1709);
    float32x2_t v25 = v7[v24];
    float32x2_t v26 = vtrn1_f32(v20, v20);
    float32x2_t v27 = vtrn2_f32(v20, v20);
    int64_t v29 = v24 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vtrn1_f32(v38, v38);
    float32x2_t v45 = vtrn2_f32(v38, v38);
    int64_t v47 = v42 + 1;
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vtrn1_f32(v56, v56);
    float32x2_t v63 = vtrn2_f32(v56, v56);
    int64_t v65 = v60 + 1;
    float32x2_t v79 = v7[v78];
    float32x2_t v80 = vtrn1_f32(v74, v74);
    float32x2_t v81 = vtrn2_f32(v74, v74);
    int64_t v83 = v78 + 1;
    float32x2_t v102 = v7[v101];
    float32x2_t v103 = vmul_f32(v98, v97);
    float32x2_t v115 = v7[v114];
    float32x2_t v116 = vtrn1_f32(v110, v110);
    float32x2_t v117 = vtrn2_f32(v110, v110);
    int64_t v119 = v114 + 1;
    float32x2_t v133 = v7[v132];
    float32x2_t v134 = vtrn1_f32(v128, v128);
    float32x2_t v135 = vtrn2_f32(v128, v128);
    int64_t v137 = v132 + 1;
    float32x2_t v151 = v7[v150];
    float32x2_t v152 = vtrn1_f32(v146, v146);
    float32x2_t v153 = vtrn2_f32(v146, v146);
    int64_t v155 = v150 + 1;
    float32x2_t v169 = v7[v168];
    float32x2_t v170 = vtrn1_f32(v164, v164);
    float32x2_t v171 = vtrn2_f32(v164, v164);
    int64_t v173 = v168 + 1;
    float32x2_t v187 = v7[v186];
    float32x2_t v188 = vtrn1_f32(v182, v182);
    float32x2_t v189 = vtrn2_f32(v182, v182);
    int64_t v191 = v186 + 1;
    float32x2_t v205 = v7[v204];
    float32x2_t v206 = vtrn1_f32(v200, v200);
    float32x2_t v207 = vtrn2_f32(v200, v200);
    int64_t v209 = v204 + 1;
    float32x2_t v223 = v7[v222];
    float32x2_t v224 = vtrn1_f32(v218, v218);
    float32x2_t v225 = vtrn2_f32(v218, v218);
    int64_t v227 = v222 + 1;
    float32x2_t v241 = v7[v240];
    float32x2_t v242 = vtrn1_f32(v236, v236);
    float32x2_t v243 = vtrn2_f32(v236, v236);
    int64_t v245 = v240 + 1;
    float32x2_t v259 = v7[v258];
    float32x2_t v260 = vtrn1_f32(v254, v254);
    float32x2_t v261 = vtrn2_f32(v254, v254);
    int64_t v263 = v258 + 1;
    float32x2_t v277 = v7[v276];
    float32x2_t v278 = vtrn1_f32(v272, v272);
    float32x2_t v279 = vtrn2_f32(v272, v272);
    int64_t v281 = v276 + 1;
    float32x2_t v295 = v7[v294];
    float32x2_t v296 = vtrn1_f32(v290, v290);
    float32x2_t v297 = vtrn2_f32(v290, v290);
    int64_t v299 = v294 + 1;
    float32x2_t v313 = v7[v312];
    float32x2_t v314 = vtrn1_f32(v308, v308);
    float32x2_t v315 = vtrn2_f32(v308, v308);
    int64_t v317 = v312 + 1;
    float32x2_t v331 = v7[v330];
    float32x2_t v332 = vtrn1_f32(v326, v326);
    float32x2_t v333 = vtrn2_f32(v326, v326);
    int64_t v335 = v330 + 1;
    float32x2_t v349 = v7[v348];
    float32x2_t v350 = vtrn1_f32(v344, v344);
    float32x2_t v351 = vtrn2_f32(v344, v344);
    int64_t v353 = v348 + 1;
    float32x2_t v367 = v7[v366];
    float32x2_t v368 = vtrn1_f32(v362, v362);
    float32x2_t v369 = vtrn2_f32(v362, v362);
    int64_t v371 = v366 + 1;
    float32x2_t v385 = v7[v384];
    float32x2_t v386 = vtrn1_f32(v380, v380);
    float32x2_t v387 = vtrn2_f32(v380, v380);
    int64_t v389 = v384 + 1;
    float32x2_t v403 = v7[v402];
    float32x2_t v404 = vtrn1_f32(v398, v398);
    float32x2_t v405 = vtrn2_f32(v398, v398);
    int64_t v407 = v402 + 1;
    float32x2_t v421 = v7[v420];
    float32x2_t v422 = vtrn1_f32(v416, v416);
    float32x2_t v423 = vtrn2_f32(v416, v416);
    int64_t v425 = v420 + 1;
    float32x2_t v439 = v7[v438];
    float32x2_t v440 = vtrn1_f32(v434, v434);
    float32x2_t v441 = vtrn2_f32(v434, v434);
    int64_t v443 = v438 + 1;
    float32x2_t v30 = v7[v29];
    float32x2_t v31 = vmul_f32(v26, v25);
    float32x2_t v48 = v7[v47];
    float32x2_t v49 = vmul_f32(v44, v43);
    float32x2_t v66 = v7[v65];
    float32x2_t v67 = vmul_f32(v62, v61);
    float32x2_t v84 = v7[v83];
    float32x2_t v85 = vmul_f32(v80, v79);
    float32x2_t v120 = v7[v119];
    float32x2_t v121 = vmul_f32(v116, v115);
    float32x2_t v138 = v7[v137];
    float32x2_t v139 = vmul_f32(v134, v133);
    float32x2_t v156 = v7[v155];
    float32x2_t v157 = vmul_f32(v152, v151);
    float32x2_t v174 = v7[v173];
    float32x2_t v175 = vmul_f32(v170, v169);
    float32x2_t v192 = v7[v191];
    float32x2_t v193 = vmul_f32(v188, v187);
    float32x2_t v210 = v7[v209];
    float32x2_t v211 = vmul_f32(v206, v205);
    float32x2_t v228 = v7[v227];
    float32x2_t v229 = vmul_f32(v224, v223);
    float32x2_t v246 = v7[v245];
    float32x2_t v247 = vmul_f32(v242, v241);
    float32x2_t v264 = v7[v263];
    float32x2_t v265 = vmul_f32(v260, v259);
    float32x2_t v282 = v7[v281];
    float32x2_t v283 = vmul_f32(v278, v277);
    float32x2_t v300 = v7[v299];
    float32x2_t v301 = vmul_f32(v296, v295);
    float32x2_t v318 = v7[v317];
    float32x2_t v319 = vmul_f32(v314, v313);
    float32x2_t v336 = v7[v335];
    float32x2_t v337 = vmul_f32(v332, v331);
    float32x2_t v354 = v7[v353];
    float32x2_t v355 = vmul_f32(v350, v349);
    float32x2_t v372 = v7[v371];
    float32x2_t v373 = vmul_f32(v368, v367);
    float32x2_t v390 = v7[v389];
    float32x2_t v391 = vmul_f32(v386, v385);
    float32x2_t v408 = v7[v407];
    float32x2_t v409 = vmul_f32(v404, v403);
    float32x2_t v426 = v7[v425];
    float32x2_t v427 = vmul_f32(v422, v421);
    float32x2_t v444 = v7[v443];
    float32x2_t v445 = vmul_f32(v440, v439);
    float32x2_t v105 = vfma_f32(v103, v99, v102);
    float32x2_t v33 = vfma_f32(v31, v27, v30);
    float32x2_t v51 = vfma_f32(v49, v45, v48);
    float32x2_t v69 = vfma_f32(v67, v63, v66);
    float32x2_t v87 = vfma_f32(v85, v81, v84);
    float32x2_t v123 = vfma_f32(v121, v117, v120);
    float32x2_t v141 = vfma_f32(v139, v135, v138);
    float32x2_t v159 = vfma_f32(v157, v153, v156);
    float32x2_t v177 = vfma_f32(v175, v171, v174);
    float32x2_t v195 = vfma_f32(v193, v189, v192);
    float32x2_t v213 = vfma_f32(v211, v207, v210);
    float32x2_t v231 = vfma_f32(v229, v225, v228);
    float32x2_t v249 = vfma_f32(v247, v243, v246);
    float32x2_t v267 = vfma_f32(v265, v261, v264);
    float32x2_t v285 = vfma_f32(v283, v279, v282);
    float32x2_t v303 = vfma_f32(v301, v297, v300);
    float32x2_t v321 = vfma_f32(v319, v315, v318);
    float32x2_t v339 = vfma_f32(v337, v333, v336);
    float32x2_t v357 = vfma_f32(v355, v351, v354);
    float32x2_t v375 = vfma_f32(v373, v369, v372);
    float32x2_t v393 = vfma_f32(v391, v387, v390);
    float32x2_t v411 = vfma_f32(v409, v405, v408);
    float32x2_t v429 = vfma_f32(v427, v423, v426);
    float32x2_t v447 = vfma_f32(v445, v441, v444);
    float32x2_t v489 = vsub_f32(v33, v87);
    float32x2_t v493 = vmul_f32(v33, v1734);
    float32x2_t v507 = vsub_f32(v51, v69);
    float32x2_t v511 = vmul_f32(v51, v1734);
    float32x2_t v603 = vsub_f32(v123, v177);
    float32x2_t v607 = vmul_f32(v123, v1734);
    float32x2_t v621 = vsub_f32(v141, v159);
    float32x2_t v625 = vmul_f32(v141, v1734);
    float32x2_t v717 = vsub_f32(v213, v267);
    float32x2_t v721 = vmul_f32(v213, v1734);
    float32x2_t v735 = vsub_f32(v231, v249);
    float32x2_t v739 = vmul_f32(v231, v1734);
    float32x2_t v831 = vsub_f32(v303, v357);
    float32x2_t v835 = vmul_f32(v303, v1734);
    float32x2_t v849 = vsub_f32(v321, v339);
    float32x2_t v853 = vmul_f32(v321, v1734);
    float32x2_t v945 = vsub_f32(v393, v447);
    float32x2_t v949 = vmul_f32(v393, v1734);
    float32x2_t v963 = vsub_f32(v411, v429);
    float32x2_t v967 = vmul_f32(v411, v1734);
    float32x2_t v494 = vsub_f32(v493, v489);
    float32x2_t v512 = vsub_f32(v511, v507);
    float32x2_t v523 = vmul_f32(v507, v1683);
    float32x2_t v538 = vmul_f32(v489, v1683);
    float32x2_t v608 = vsub_f32(v607, v603);
    float32x2_t v626 = vsub_f32(v625, v621);
    float32x2_t v637 = vmul_f32(v621, v1683);
    float32x2_t v652 = vmul_f32(v603, v1683);
    float32x2_t v722 = vsub_f32(v721, v717);
    float32x2_t v740 = vsub_f32(v739, v735);
    float32x2_t v751 = vmul_f32(v735, v1683);
    float32x2_t v766 = vmul_f32(v717, v1683);
    float32x2_t v836 = vsub_f32(v835, v831);
    float32x2_t v854 = vsub_f32(v853, v849);
    float32x2_t v865 = vmul_f32(v849, v1683);
    float32x2_t v880 = vmul_f32(v831, v1683);
    float32x2_t v950 = vsub_f32(v949, v945);
    float32x2_t v968 = vsub_f32(v967, v963);
    float32x2_t v979 = vmul_f32(v963, v1683);
    float32x2_t v994 = vmul_f32(v945, v1683);
    float32x2_t v513 = vadd_f32(v494, v512);
    float32x2_t v514 = vsub_f32(v494, v512);
    float32x2_t v524 = vadd_f32(v489, v523);
    float32x2_t v539 = vsub_f32(v538, v507);
    float32x2_t v627 = vadd_f32(v608, v626);
    float32x2_t v628 = vsub_f32(v608, v626);
    float32x2_t v638 = vadd_f32(v603, v637);
    float32x2_t v653 = vsub_f32(v652, v621);
    float32x2_t v741 = vadd_f32(v722, v740);
    float32x2_t v742 = vsub_f32(v722, v740);
    float32x2_t v752 = vadd_f32(v717, v751);
    float32x2_t v767 = vsub_f32(v766, v735);
    float32x2_t v855 = vadd_f32(v836, v854);
    float32x2_t v856 = vsub_f32(v836, v854);
    float32x2_t v866 = vadd_f32(v831, v865);
    float32x2_t v881 = vsub_f32(v880, v849);
    float32x2_t v969 = vadd_f32(v950, v968);
    float32x2_t v970 = vsub_f32(v950, v968);
    float32x2_t v980 = vadd_f32(v945, v979);
    float32x2_t v995 = vsub_f32(v994, v963);
    float32x2_t v518 = vmul_f32(v513, v1663);
    float32x2_t v528 = vmul_f32(v514, v1673);
    float32x2_t v540 = vadd_f32(v452, v513);
    float32x2_t v546 = vrev64_f32(v524);
    float32x2_t v554 = vrev64_f32(v539);
    float32x2_t v632 = vmul_f32(v627, v1663);
    float32x2_t v642 = vmul_f32(v628, v1673);
    float32x2_t v654 = vadd_f32(v105, v627);
    float32x2_t v660 = vrev64_f32(v638);
    float32x2_t v668 = vrev64_f32(v653);
    float32x2_t v746 = vmul_f32(v741, v1663);
    float32x2_t v756 = vmul_f32(v742, v1673);
    float32x2_t v768 = vadd_f32(v195, v741);
    float32x2_t v774 = vrev64_f32(v752);
    float32x2_t v782 = vrev64_f32(v767);
    float32x2_t v860 = vmul_f32(v855, v1663);
    float32x2_t v870 = vmul_f32(v856, v1673);
    float32x2_t v882 = vadd_f32(v285, v855);
    float32x2_t v888 = vrev64_f32(v866);
    float32x2_t v896 = vrev64_f32(v881);
    float32x2_t v974 = vmul_f32(v969, v1663);
    float32x2_t v984 = vmul_f32(v970, v1673);
    float32x2_t v996 = vadd_f32(v375, v969);
    float32x2_t v1002 = vrev64_f32(v980);
    float32x2_t v1010 = vrev64_f32(v995);
    float32x2_t v519 = vsub_f32(v452, v518);
    float32x2_t v547 = vmul_f32(v546, v1711);
    float32x2_t v555 = vmul_f32(v554, v1711);
    float32x2_t v633 = vsub_f32(v105, v632);
    float32x2_t v661 = vmul_f32(v660, v1711);
    float32x2_t v669 = vmul_f32(v668, v1711);
    float32x2_t v747 = vsub_f32(v195, v746);
    float32x2_t v775 = vmul_f32(v774, v1711);
    float32x2_t v783 = vmul_f32(v782, v1711);
    float32x2_t v861 = vsub_f32(v285, v860);
    float32x2_t v889 = vmul_f32(v888, v1711);
    float32x2_t v897 = vmul_f32(v896, v1711);
    float32x2_t v975 = vsub_f32(v375, v974);
    float32x2_t v1003 = vmul_f32(v1002, v1711);
    float32x2_t v1011 = vmul_f32(v1010, v1711);
    float32x2_t v1059 = vsub_f32(v654, v996);
    float32x2_t v1063 = vmul_f32(v654, v1734);
    float32x2_t v1077 = vsub_f32(v768, v882);
    float32x2_t v1081 = vmul_f32(v768, v1734);
    float32x2_t v529 = vsub_f32(v519, v528);
    float32x2_t v533 = vmul_f32(v519, v1734);
    float32x2_t v643 = vsub_f32(v633, v642);
    float32x2_t v647 = vmul_f32(v633, v1734);
    float32x2_t v757 = vsub_f32(v747, v756);
    float32x2_t v761 = vmul_f32(v747, v1734);
    float32x2_t v871 = vsub_f32(v861, v870);
    float32x2_t v875 = vmul_f32(v861, v1734);
    float32x2_t v985 = vsub_f32(v975, v984);
    float32x2_t v989 = vmul_f32(v975, v1734);
    float32x2_t v1064 = vsub_f32(v1063, v1059);
    float32x2_t v1082 = vsub_f32(v1081, v1077);
    float32x2_t v1093 = vmul_f32(v1077, v1683);
    float32x2_t v1108 = vmul_f32(v1059, v1683);
    float32x2_t v534 = vsub_f32(v533, v529);
    float32x2_t v556 = vsub_f32(v529, v555);
    float32x2_t v560 = vmul_f32(v529, v1734);
    float32x2_t v648 = vsub_f32(v647, v643);
    float32x2_t v670 = vsub_f32(v643, v669);
    float32x2_t v674 = vmul_f32(v643, v1734);
    float32x2_t v762 = vsub_f32(v761, v757);
    float32x2_t v784 = vsub_f32(v757, v783);
    float32x2_t v788 = vmul_f32(v757, v1734);
    float32x2_t v876 = vsub_f32(v875, v871);
    float32x2_t v898 = vsub_f32(v871, v897);
    float32x2_t v902 = vmul_f32(v871, v1734);
    float32x2_t v990 = vsub_f32(v989, v985);
    float32x2_t v1012 = vsub_f32(v985, v1011);
    float32x2_t v1016 = vmul_f32(v985, v1734);
    float32x2_t v1083 = vadd_f32(v1064, v1082);
    float32x2_t v1084 = vsub_f32(v1064, v1082);
    float32x2_t v1094 = vadd_f32(v1059, v1093);
    float32x2_t v1109 = vsub_f32(v1108, v1077);
    float32x2_t v548 = vsub_f32(v534, v547);
    float32x2_t v561 = vsub_f32(v560, v556);
    float32x2_t v565 = vmul_f32(v534, v1734);
    float32x2_t v662 = vsub_f32(v648, v661);
    float32x2_t v675 = vsub_f32(v674, v670);
    float32x2_t v679 = vmul_f32(v648, v1734);
    float32x2_t v776 = vsub_f32(v762, v775);
    float32x2_t v789 = vsub_f32(v788, v784);
    float32x2_t v793 = vmul_f32(v762, v1734);
    float32x2_t v890 = vsub_f32(v876, v889);
    float32x2_t v903 = vsub_f32(v902, v898);
    float32x2_t v907 = vmul_f32(v876, v1734);
    float32x2_t v1004 = vsub_f32(v990, v1003);
    float32x2_t v1017 = vsub_f32(v1016, v1012);
    float32x2_t v1021 = vmul_f32(v990, v1734);
    float32x2_t v1088 = vmul_f32(v1083, v1663);
    float32x2_t v1098 = vmul_f32(v1084, v1673);
    float32x2_t v1110 = vadd_f32(v540, v1083);
    float32x2_t v1122 = vrev64_f32(v1094);
    float32x2_t v1136 = vrev64_f32(v1109);
    float32x2_t v1320 = vrev64_f32(v670);
    float32x2_t v1332 = vrev64_f32(v784);
    float32x2_t v1344 = vrev64_f32(v1012);
    float32x2_t v1362 = vrev64_f32(v898);
    float32x2_t v566 = vsub_f32(v565, v548);
    float32x2_t v680 = vsub_f32(v679, v662);
    float32x2_t v794 = vsub_f32(v793, v776);
    float32x2_t v908 = vsub_f32(v907, v890);
    float32x2_t v1022 = vsub_f32(v1021, v1004);
    float32x2_t v1089 = vsub_f32(v540, v1088);
    int16x4_t v1113 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1110, 15), (int32x2_t){0, 0}));
    float32x2_t v1123 = vmul_f32(v1122, v1711);
    float32x2_t v1137 = vmul_f32(v1136, v1711);
    float32x2_t v1176 = vrev64_f32(v662);
    float32x2_t v1188 = vrev64_f32(v776);
    float32x2_t v1200 = vrev64_f32(v1004);
    float32x2_t v1218 = vrev64_f32(v890);
    float32x2_t v1321 = vmul_f32(v1320, v1319);
    float32x2_t v1333 = vmul_f32(v1332, v1607);
    float32x2_t v1345 = vmul_f32(v1344, v1619);
    float32x2_t v1363 = vmul_f32(v1362, v1475);
    float32x2_t v1464 = vrev64_f32(v675);
    float32x2_t v1476 = vrev64_f32(v789);
    float32x2_t v1488 = vrev64_f32(v1017);
    float32x2_t v1506 = vrev64_f32(v903);
    float32x2_t v1099 = vsub_f32(v1089, v1098);
    float32x2_t v1103 = vmul_f32(v1089, v1734);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v1113), 0);
    float32x2_t v1177 = vmul_f32(v1176, v1175);
    float32x2_t v1189 = vmul_f32(v1188, v1319);
    float32x2_t v1201 = vmul_f32(v1200, v1607);
    float32x2_t v1219 = vmul_f32(v1218, v1463);
    float32x2_t v1322 = vfma_f32(v1321, v670, v1313);
    float32x2_t v1334 = vfma_f32(v1333, v784, v1601);
    float32x2_t v1346 = vfma_f32(v1345, v1012, v1613);
    float32x2_t v1364 = vfma_f32(v1363, v898, v1469);
    float32x2_t v1465 = vmul_f32(v1464, v1463);
    float32x2_t v1477 = vmul_f32(v1476, v1475);
    float32x2_t v1489 = vmul_f32(v1488, v1649);
    float32x2_t v1507 = vmul_f32(v1506, v1505);
    float32x2_t v1608 = vrev64_f32(v680);
    float32x2_t v1620 = vrev64_f32(v794);
    float32x2_t v1632 = vrev64_f32(v1022);
    float32x2_t v1650 = vrev64_f32(v908);
    float32x2_t v1104 = vsub_f32(v1103, v1099);
    float32x2_t v1138 = vsub_f32(v1099, v1137);
    float32x2_t v1148 = vmul_f32(v1099, v1734);
    float32x2_t v1178 = vfma_f32(v1177, v662, v1169);
    float32x2_t v1190 = vfma_f32(v1189, v776, v1313);
    float32x2_t v1202 = vfma_f32(v1201, v1004, v1601);
    float32x2_t v1220 = vfma_f32(v1219, v890, v1457);
    float32x2_t v1347 = vsub_f32(v1322, v1346);
    float32x2_t v1351 = vmul_f32(v1322, v1734);
    float32x2_t v1365 = vsub_f32(v1334, v1364);
    float32x2_t v1369 = vmul_f32(v1334, v1734);
    float32x2_t v1466 = vfma_f32(v1465, v675, v1457);
    float32x2_t v1478 = vfma_f32(v1477, v789, v1469);
    float32x2_t v1490 = vfma_f32(v1489, v1017, v1643);
    float32x2_t v1508 = vfma_f32(v1507, v903, v1625);
    float32x2_t v1609 = vmul_f32(v1608, v1607);
    float32x2_t v1621 = vmul_f32(v1620, v1619);
    float32x2_t v1633 = vmul_f32(v1632, v1631);
    float32x2_t v1651 = vmul_f32(v1650, v1649);
    float32x2_t v1124 = vsub_f32(v1104, v1123);
    int16x4_t v1141 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1138, 15), (int32x2_t){0, 0}));
    float32x2_t v1149 = vsub_f32(v1148, v1138);
    float32x2_t v1159 = vmul_f32(v1104, v1734);
    float32x2_t v1203 = vsub_f32(v1178, v1202);
    float32x2_t v1207 = vmul_f32(v1178, v1734);
    float32x2_t v1221 = vsub_f32(v1190, v1220);
    float32x2_t v1225 = vmul_f32(v1190, v1734);
    float32x2_t v1352 = vsub_f32(v1351, v1347);
    float32x2_t v1370 = vsub_f32(v1369, v1365);
    float32x2_t v1381 = vmul_f32(v1365, v1683);
    float32x2_t v1396 = vmul_f32(v1347, v1683);
    float32x2_t v1491 = vsub_f32(v1466, v1490);
    float32x2_t v1495 = vmul_f32(v1466, v1734);
    float32x2_t v1509 = vsub_f32(v1478, v1508);
    float32x2_t v1513 = vmul_f32(v1478, v1734);
    float32x2_t v1610 = vfma_f32(v1609, v680, v1601);
    float32x2_t v1622 = vfma_f32(v1621, v794, v1613);
    float32x2_t v1634 = vfma_f32(v1633, v1022, v1625);
    float32x2_t v1652 = vfma_f32(v1651, v908, v1643);
    int16x4_t v1127 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1124, 15), (int32x2_t){0, 0}));
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v1141), 0);
    int16x4_t v1152 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1149, 15), (int32x2_t){0, 0}));
    float32x2_t v1160 = vsub_f32(v1159, v1124);
    float32x2_t v1208 = vsub_f32(v1207, v1203);
    float32x2_t v1226 = vsub_f32(v1225, v1221);
    float32x2_t v1237 = vmul_f32(v1221, v1683);
    float32x2_t v1252 = vmul_f32(v1203, v1683);
    float32x2_t v1371 = vadd_f32(v1352, v1370);
    float32x2_t v1372 = vsub_f32(v1352, v1370);
    float32x2_t v1382 = vadd_f32(v1347, v1381);
    float32x2_t v1397 = vsub_f32(v1396, v1365);
    float32x2_t v1496 = vsub_f32(v1495, v1491);
    float32x2_t v1514 = vsub_f32(v1513, v1509);
    float32x2_t v1525 = vmul_f32(v1509, v1683);
    float32x2_t v1540 = vmul_f32(v1491, v1683);
    float32x2_t v1635 = vsub_f32(v1610, v1634);
    float32x2_t v1639 = vmul_f32(v1610, v1734);
    float32x2_t v1653 = vsub_f32(v1622, v1652);
    float32x2_t v1657 = vmul_f32(v1622, v1734);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v1127), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v1152), 0);
    int16x4_t v1163 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1160, 15), (int32x2_t){0, 0}));
    float32x2_t v1227 = vadd_f32(v1208, v1226);
    float32x2_t v1228 = vsub_f32(v1208, v1226);
    float32x2_t v1238 = vadd_f32(v1203, v1237);
    float32x2_t v1253 = vsub_f32(v1252, v1221);
    float32x2_t v1376 = vmul_f32(v1371, v1663);
    float32x2_t v1386 = vmul_f32(v1372, v1673);
    float32x2_t v1398 = vadd_f32(v556, v1371);
    float32x2_t v1410 = vrev64_f32(v1382);
    float32x2_t v1424 = vrev64_f32(v1397);
    float32x2_t v1515 = vadd_f32(v1496, v1514);
    float32x2_t v1516 = vsub_f32(v1496, v1514);
    float32x2_t v1526 = vadd_f32(v1491, v1525);
    float32x2_t v1541 = vsub_f32(v1540, v1509);
    float32x2_t v1640 = vsub_f32(v1639, v1635);
    float32x2_t v1658 = vsub_f32(v1657, v1653);
    float32x2_t v1669 = vmul_f32(v1653, v1683);
    float32x2_t v1684 = vmul_f32(v1635, v1683);
    v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v1163), 0);
    float32x2_t v1232 = vmul_f32(v1227, v1663);
    float32x2_t v1242 = vmul_f32(v1228, v1673);
    float32x2_t v1254 = vadd_f32(v548, v1227);
    float32x2_t v1266 = vrev64_f32(v1238);
    float32x2_t v1280 = vrev64_f32(v1253);
    float32x2_t v1377 = vsub_f32(v556, v1376);
    int16x4_t v1401 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1398, 15), (int32x2_t){0, 0}));
    float32x2_t v1411 = vmul_f32(v1410, v1711);
    float32x2_t v1425 = vmul_f32(v1424, v1711);
    float32x2_t v1520 = vmul_f32(v1515, v1663);
    float32x2_t v1530 = vmul_f32(v1516, v1673);
    float32x2_t v1542 = vadd_f32(v561, v1515);
    float32x2_t v1554 = vrev64_f32(v1526);
    float32x2_t v1568 = vrev64_f32(v1541);
    float32x2_t v1659 = vadd_f32(v1640, v1658);
    float32x2_t v1660 = vsub_f32(v1640, v1658);
    float32x2_t v1670 = vadd_f32(v1635, v1669);
    float32x2_t v1685 = vsub_f32(v1684, v1653);
    float32x2_t v1233 = vsub_f32(v548, v1232);
    int16x4_t v1257 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1254, 15), (int32x2_t){0, 0}));
    float32x2_t v1267 = vmul_f32(v1266, v1711);
    float32x2_t v1281 = vmul_f32(v1280, v1711);
    float32x2_t v1387 = vsub_f32(v1377, v1386);
    float32x2_t v1391 = vmul_f32(v1377, v1734);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v1401), 0);
    float32x2_t v1521 = vsub_f32(v561, v1520);
    int16x4_t v1545 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1542, 15), (int32x2_t){0, 0}));
    float32x2_t v1555 = vmul_f32(v1554, v1711);
    float32x2_t v1569 = vmul_f32(v1568, v1711);
    float32x2_t v1664 = vmul_f32(v1659, v1663);
    float32x2_t v1674 = vmul_f32(v1660, v1673);
    float32x2_t v1686 = vadd_f32(v566, v1659);
    float32x2_t v1698 = vrev64_f32(v1670);
    float32x2_t v1712 = vrev64_f32(v1685);
    float32x2_t v1243 = vsub_f32(v1233, v1242);
    float32x2_t v1247 = vmul_f32(v1233, v1734);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v1257), 0);
    float32x2_t v1392 = vsub_f32(v1391, v1387);
    float32x2_t v1426 = vsub_f32(v1387, v1425);
    float32x2_t v1436 = vmul_f32(v1387, v1734);
    float32x2_t v1531 = vsub_f32(v1521, v1530);
    float32x2_t v1535 = vmul_f32(v1521, v1734);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v1545), 0);
    float32x2_t v1665 = vsub_f32(v566, v1664);
    int16x4_t v1689 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1686, 15), (int32x2_t){0, 0}));
    float32x2_t v1699 = vmul_f32(v1698, v1711);
    float32x2_t v1713 = vmul_f32(v1712, v1711);
    float32x2_t v1248 = vsub_f32(v1247, v1243);
    float32x2_t v1282 = vsub_f32(v1243, v1281);
    float32x2_t v1292 = vmul_f32(v1243, v1734);
    float32x2_t v1412 = vsub_f32(v1392, v1411);
    int16x4_t v1429 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1426, 15), (int32x2_t){0, 0}));
    float32x2_t v1437 = vsub_f32(v1436, v1426);
    float32x2_t v1447 = vmul_f32(v1392, v1734);
    float32x2_t v1536 = vsub_f32(v1535, v1531);
    float32x2_t v1570 = vsub_f32(v1531, v1569);
    float32x2_t v1580 = vmul_f32(v1531, v1734);
    float32x2_t v1675 = vsub_f32(v1665, v1674);
    float32x2_t v1679 = vmul_f32(v1665, v1734);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v1689), 0);
    float32x2_t v1268 = vsub_f32(v1248, v1267);
    int16x4_t v1285 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1282, 15), (int32x2_t){0, 0}));
    float32x2_t v1293 = vsub_f32(v1292, v1282);
    float32x2_t v1303 = vmul_f32(v1248, v1734);
    int16x4_t v1415 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1412, 15), (int32x2_t){0, 0}));
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v1429), 0);
    int16x4_t v1440 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1437, 15), (int32x2_t){0, 0}));
    float32x2_t v1448 = vsub_f32(v1447, v1412);
    float32x2_t v1556 = vsub_f32(v1536, v1555);
    int16x4_t v1573 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1570, 15), (int32x2_t){0, 0}));
    float32x2_t v1581 = vsub_f32(v1580, v1570);
    float32x2_t v1591 = vmul_f32(v1536, v1734);
    float32x2_t v1680 = vsub_f32(v1679, v1675);
    float32x2_t v1714 = vsub_f32(v1675, v1713);
    float32x2_t v1724 = vmul_f32(v1675, v1734);
    int16x4_t v1271 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1268, 15), (int32x2_t){0, 0}));
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v1285), 0);
    int16x4_t v1296 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1293, 15), (int32x2_t){0, 0}));
    float32x2_t v1304 = vsub_f32(v1303, v1268);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v1415), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v1440), 0);
    int16x4_t v1451 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1448, 15), (int32x2_t){0, 0}));
    int16x4_t v1559 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1556, 15), (int32x2_t){0, 0}));
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v1573), 0);
    int16x4_t v1584 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1581, 15), (int32x2_t){0, 0}));
    float32x2_t v1592 = vsub_f32(v1591, v1556);
    float32x2_t v1700 = vsub_f32(v1680, v1699);
    int16x4_t v1717 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1714, 15), (int32x2_t){0, 0}));
    float32x2_t v1725 = vsub_f32(v1724, v1714);
    float32x2_t v1735 = vmul_f32(v1680, v1734);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v1271), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v1296), 0);
    int16x4_t v1307 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1304, 15), (int32x2_t){0, 0}));
    v6[ostride * 22] = vget_lane_s32(vreinterpret_s32_s16(v1451), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v1559), 0);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v1584), 0);
    int16x4_t v1595 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1592, 15), (int32x2_t){0, 0}));
    int16x4_t v1703 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1700, 15), (int32x2_t){0, 0}));
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v1717), 0);
    int16x4_t v1728 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1725, 15), (int32x2_t){0, 0}));
    float32x2_t v1736 = vsub_f32(v1735, v1700);
    v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v1307), 0);
    v6[ostride * 23] = vget_lane_s32(vreinterpret_s32_s16(v1595), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v1703), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v1728), 0);
    int16x4_t v1739 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1736, 15), (int32x2_t){0, 0}));
    v6[ostride * 24] = vget_lane_s32(vreinterpret_s32_s16(v1739), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu25(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v1164 = 9.6858316112863108e-01F;
    float v1169 = 2.4868988716485479e-01F;
    float v1331 = 8.7630668004386358e-01F;
    float v1336 = 4.8175367410171532e-01F;
    float v1498 = 7.2896862742141155e-01F;
    float v1503 = 6.8454710592868862e-01F;
    float v1511 = 6.2790519529313527e-02F;
    float v1516 = 9.9802672842827156e-01F;
    float v1549 = 7.7051324277578925e-01F;
    float v1665 = 5.3582679497899655e-01F;
    float v1670 = 8.4432792550201508e-01F;
    float v1678 = -4.2577929156507272e-01F;
    float v1683 = 9.0482705246601947e-01F;
    float v1691 = -6.3742398974868952e-01F;
    float v1696 = -7.7051324277578936e-01F;
    float v1711 = -9.9211470131447776e-01F;
    float v1716 = 1.2533323356430454e-01F;
    float v1733 = 2.5000000000000000e-01F;
    float v1745 = 5.5901699437494745e-01F;
    float v1757 = 6.1803398874989490e-01F;
    float v1788 = -9.5105651629515353e-01F;
    float v1818 = 2.0000000000000000e+00F;
    const float32x2_t *v1873 = &v5[v0];
    int32_t *v2239 = &v6[v2];
    int64_t v19 = v0 * 5;
    int64_t v27 = v10 * 4;
    int64_t v33 = v0 * 10;
    int64_t v41 = v10 * 9;
    int64_t v47 = v0 * 15;
    int64_t v55 = v10 * 14;
    int64_t v61 = v0 * 20;
    int64_t v69 = v10 * 19;
    int64_t v89 = v0 * 6;
    int64_t v97 = v10 * 5;
    int64_t v103 = v0 * 11;
    int64_t v111 = v10 * 10;
    int64_t v117 = v0 * 16;
    int64_t v125 = v10 * 15;
    int64_t v131 = v0 * 21;
    int64_t v139 = v10 * 20;
    int64_t v145 = v0 * 2;
    int64_t v159 = v0 * 7;
    int64_t v167 = v10 * 6;
    int64_t v173 = v0 * 12;
    int64_t v181 = v10 * 11;
    int64_t v187 = v0 * 17;
    int64_t v195 = v10 * 16;
    int64_t v201 = v0 * 22;
    int64_t v209 = v10 * 21;
    int64_t v215 = v0 * 3;
    int64_t v223 = v10 * 2;
    int64_t v229 = v0 * 8;
    int64_t v237 = v10 * 7;
    int64_t v243 = v0 * 13;
    int64_t v251 = v10 * 12;
    int64_t v257 = v0 * 18;
    int64_t v265 = v10 * 17;
    int64_t v271 = v0 * 23;
    int64_t v279 = v10 * 22;
    int64_t v285 = v0 * 4;
    int64_t v293 = v10 * 3;
    int64_t v299 = v0 * 9;
    int64_t v307 = v10 * 8;
    int64_t v313 = v0 * 14;
    int64_t v321 = v10 * 13;
    int64_t v327 = v0 * 19;
    int64_t v335 = v10 * 18;
    int64_t v341 = v0 * 24;
    int64_t v349 = v10 * 23;
    int64_t v350 = v13 * 24;
    int64_t v1112 = v2 * 5;
    int64_t v1128 = v2 * 10;
    int64_t v1142 = v2 * 15;
    int64_t v1156 = v2 * 20;
    float v1172 = v4 * v1169;
    int64_t v1279 = v2 * 6;
    int64_t v1295 = v2 * 11;
    int64_t v1309 = v2 * 16;
    int64_t v1323 = v2 * 21;
    float v1339 = v4 * v1336;
    int64_t v1430 = v2 * 2;
    int64_t v1446 = v2 * 7;
    int64_t v1462 = v2 * 12;
    int64_t v1476 = v2 * 17;
    int64_t v1490 = v2 * 22;
    float v1506 = v4 * v1503;
    float v1519 = v4 * v1516;
    float v1552 = v4 * v1549;
    int64_t v1597 = v2 * 3;
    int64_t v1613 = v2 * 8;
    int64_t v1629 = v2 * 13;
    int64_t v1643 = v2 * 18;
    int64_t v1657 = v2 * 23;
    float v1673 = v4 * v1670;
    float v1686 = v4 * v1683;
    float v1699 = v4 * v1696;
    float v1719 = v4 * v1716;
    int64_t v1764 = v2 * 4;
    int64_t v1780 = v2 * 9;
    float v1791 = v4 * v1788;
    int64_t v1796 = v2 * 14;
    int64_t v1810 = v2 * 19;
    int64_t v1824 = v2 * 24;
    const float32x2_t *v2055 = &v5[0];
    svint64_t v2056 = svindex_s64(0, v1);
    svfloat32_t v2161 = svdup_n_f32(0);
    int32_t *v2175 = &v6[0];
    svfloat32_t v2218 = svdup_n_f32(v1164);
    svfloat32_t v2282 = svdup_n_f32(v1331);
    svfloat32_t v2346 = svdup_n_f32(v1498);
    svfloat32_t v2348 = svdup_n_f32(v1511);
    svfloat32_t v2410 = svdup_n_f32(v1665);
    svfloat32_t v2412 = svdup_n_f32(v1678);
    svfloat32_t v2414 = svdup_n_f32(v1691);
    svfloat32_t v2417 = svdup_n_f32(v1711);
    svfloat32_t v2420 = svdup_n_f32(v1733);
    svfloat32_t v2422 = svdup_n_f32(v1745);
    svfloat32_t v2424 = svdup_n_f32(v1757);
    svfloat32_t v2464 = svdup_n_f32(v1818);
    int64_t v29 = v27 + v350;
    int64_t v43 = v41 + v350;
    int64_t v57 = v55 + v350;
    int64_t v71 = v69 + v350;
    svfloat32_t v86 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v350]));
    int64_t v99 = v97 + v350;
    int64_t v113 = v111 + v350;
    int64_t v127 = v125 + v350;
    int64_t v141 = v139 + v350;
    int64_t v155 = v10 + v350;
    int64_t v169 = v167 + v350;
    int64_t v183 = v181 + v350;
    int64_t v197 = v195 + v350;
    int64_t v211 = v209 + v350;
    int64_t v225 = v223 + v350;
    int64_t v239 = v237 + v350;
    int64_t v253 = v251 + v350;
    int64_t v267 = v265 + v350;
    int64_t v281 = v279 + v350;
    int64_t v295 = v293 + v350;
    int64_t v309 = v307 + v350;
    int64_t v323 = v321 + v350;
    int64_t v337 = v335 + v350;
    int64_t v351 = v349 + v350;
    const float32x2_t *v1837 = &v5[v19];
    const float32x2_t *v1846 = &v5[v33];
    const float32x2_t *v1855 = &v5[v47];
    const float32x2_t *v1864 = &v5[v61];
    svfloat32_t v1875 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1873), v2056));
    const float32x2_t *v1883 = &v5[v89];
    const float32x2_t *v1892 = &v5[v103];
    const float32x2_t *v1901 = &v5[v117];
    const float32x2_t *v1910 = &v5[v131];
    const float32x2_t *v1919 = &v5[v145];
    const float32x2_t *v1928 = &v5[v159];
    const float32x2_t *v1937 = &v5[v173];
    const float32x2_t *v1946 = &v5[v187];
    const float32x2_t *v1955 = &v5[v201];
    const float32x2_t *v1964 = &v5[v215];
    const float32x2_t *v1973 = &v5[v229];
    const float32x2_t *v1982 = &v5[v243];
    const float32x2_t *v1991 = &v5[v257];
    const float32x2_t *v2000 = &v5[v271];
    const float32x2_t *v2009 = &v5[v285];
    const float32x2_t *v2018 = &v5[v299];
    const float32x2_t *v2027 = &v5[v313];
    const float32x2_t *v2036 = &v5[v327];
    const float32x2_t *v2045 = &v5[v341];
    svfloat32_t v2057 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2055), v2056));
    int32_t *v2185 = &v6[v1112];
    int32_t *v2195 = &v6[v1128];
    int32_t *v2205 = &v6[v1142];
    int32_t *v2215 = &v6[v1156];
    svfloat32_t v2219 = svdup_n_f32(v1172);
    int32_t *v2249 = &v6[v1279];
    int32_t *v2259 = &v6[v1295];
    int32_t *v2269 = &v6[v1309];
    int32_t *v2279 = &v6[v1323];
    svfloat32_t v2283 = svdup_n_f32(v1339);
    int32_t *v2303 = &v6[v1430];
    int32_t *v2313 = &v6[v1446];
    int32_t *v2323 = &v6[v1462];
    int32_t *v2333 = &v6[v1476];
    int32_t *v2343 = &v6[v1490];
    svfloat32_t v2347 = svdup_n_f32(v1506);
    svfloat32_t v2349 = svdup_n_f32(v1519);
    svfloat32_t v2354 = svdup_n_f32(v1552);
    int32_t *v2367 = &v6[v1597];
    int32_t *v2377 = &v6[v1613];
    int32_t *v2387 = &v6[v1629];
    int32_t *v2397 = &v6[v1643];
    int32_t *v2407 = &v6[v1657];
    svfloat32_t v2411 = svdup_n_f32(v1673);
    svfloat32_t v2413 = svdup_n_f32(v1686);
    svfloat32_t v2415 = svdup_n_f32(v1699);
    svfloat32_t v2418 = svdup_n_f32(v1719);
    int32_t *v2431 = &v6[v1764];
    int32_t *v2441 = &v6[v1780];
    svfloat32_t v2444 = svdup_n_f32(v1791);
    int32_t *v2451 = &v6[v1796];
    int32_t *v2461 = &v6[v1810];
    int32_t *v2471 = &v6[v1824];
    svfloat32_t v30 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v29]));
    svfloat32_t v44 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v43]));
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t zero87 = svdup_n_f32(0);
    svfloat32_t v87 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero87, v1875, v86, 0),
                     v1875, v86, 90);
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v128 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v127]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v254 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v253]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v296 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v295]));
    svfloat32_t v310 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v309]));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v338 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v337]));
    svfloat32_t v352 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v351]));
    svfloat32_t v1839 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1837), v2056));
    svfloat32_t v1848 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1846), v2056));
    svfloat32_t v1857 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1855), v2056));
    svfloat32_t v1866 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1864), v2056));
    svfloat32_t v1885 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1883), v2056));
    svfloat32_t v1894 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1892), v2056));
    svfloat32_t v1903 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1901), v2056));
    svfloat32_t v1912 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1910), v2056));
    svfloat32_t v1921 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1919), v2056));
    svfloat32_t v1930 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1928), v2056));
    svfloat32_t v1939 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1937), v2056));
    svfloat32_t v1948 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1946), v2056));
    svfloat32_t v1957 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1955), v2056));
    svfloat32_t v1966 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1964), v2056));
    svfloat32_t v1975 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1973), v2056));
    svfloat32_t v1984 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1982), v2056));
    svfloat32_t v1993 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1991), v2056));
    svfloat32_t v2002 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2000), v2056));
    svfloat32_t v2011 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2009), v2056));
    svfloat32_t v2020 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2018), v2056));
    svfloat32_t v2029 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2027), v2056));
    svfloat32_t v2038 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2036), v2056));
    svfloat32_t v2047 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2045), v2056));
    svfloat32_t zero31 = svdup_n_f32(0);
    svfloat32_t v31 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero31, v1839, v30, 0),
                     v1839, v30, 90);
    svfloat32_t zero45 = svdup_n_f32(0);
    svfloat32_t v45 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero45, v1848, v44, 0),
                     v1848, v44, 90);
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v1857, v58, 0),
                     v1857, v58, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v1866, v72, 0),
                     v1866, v72, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero101, v1885, v100, 0), v1885,
        v100, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero115, v1894, v114, 0), v1894,
        v114, 90);
    svfloat32_t zero129 = svdup_n_f32(0);
    svfloat32_t v129 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero129, v1903, v128, 0), v1903,
        v128, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero143, v1912, v142, 0), v1912,
        v142, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero157, v1921, v156, 0), v1921,
        v156, 90);
    svfloat32_t zero171 = svdup_n_f32(0);
    svfloat32_t v171 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero171, v1930, v170, 0), v1930,
        v170, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero185, v1939, v184, 0), v1939,
        v184, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero199, v1948, v198, 0), v1948,
        v198, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero213, v1957, v212, 0), v1957,
        v212, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero227, v1966, v226, 0), v1966,
        v226, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero241, v1975, v240, 0), v1975,
        v240, 90);
    svfloat32_t zero255 = svdup_n_f32(0);
    svfloat32_t v255 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero255, v1984, v254, 0), v1984,
        v254, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero269, v1993, v268, 0), v1993,
        v268, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero283, v2002, v282, 0), v2002,
        v282, 90);
    svfloat32_t zero297 = svdup_n_f32(0);
    svfloat32_t v297 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero297, v2011, v296, 0), v2011,
        v296, 90);
    svfloat32_t zero311 = svdup_n_f32(0);
    svfloat32_t v311 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero311, v2020, v310, 0), v2020,
        v310, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero325, v2029, v324, 0), v2029,
        v324, 90);
    svfloat32_t zero339 = svdup_n_f32(0);
    svfloat32_t v339 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero339, v2038, v338, 0), v2038,
        v338, 90);
    svfloat32_t zero353 = svdup_n_f32(0);
    svfloat32_t v353 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero353, v2047, v352, 0), v2047,
        v352, 90);
    svfloat32_t v373 = svcmla_f32_x(pred_full, v31, v2161, v31, 90);
    svfloat32_t v386 = svcmla_f32_x(pred_full, v45, v2161, v45, 90);
    svfloat32_t v399 = svcmla_f32_x(pred_full, v73, v2161, v73, 90);
    svfloat32_t v419 = svcmla_f32_x(pred_full, v59, v2161, v59, 90);
    svfloat32_t v500 = svcmla_f32_x(pred_full, v101, v2161, v101, 90);
    svfloat32_t v513 = svcmla_f32_x(pred_full, v115, v2161, v115, 90);
    svfloat32_t v526 = svcmla_f32_x(pred_full, v143, v2161, v143, 90);
    svfloat32_t v546 = svcmla_f32_x(pred_full, v129, v2161, v129, 90);
    svfloat32_t v627 = svcmla_f32_x(pred_full, v171, v2161, v171, 90);
    svfloat32_t v640 = svcmla_f32_x(pred_full, v185, v2161, v185, 90);
    svfloat32_t v653 = svcmla_f32_x(pred_full, v213, v2161, v213, 90);
    svfloat32_t v673 = svcmla_f32_x(pred_full, v199, v2161, v199, 90);
    svfloat32_t v754 = svcmla_f32_x(pred_full, v241, v2161, v241, 90);
    svfloat32_t v767 = svcmla_f32_x(pred_full, v255, v2161, v255, 90);
    svfloat32_t v780 = svcmla_f32_x(pred_full, v283, v2161, v283, 90);
    svfloat32_t v800 = svcmla_f32_x(pred_full, v269, v2161, v269, 90);
    svfloat32_t v881 = svcmla_f32_x(pred_full, v311, v2161, v311, 90);
    svfloat32_t v894 = svcmla_f32_x(pred_full, v325, v2161, v325, 90);
    svfloat32_t v907 = svcmla_f32_x(pred_full, v353, v2161, v353, 90);
    svfloat32_t v927 = svcmla_f32_x(pred_full, v339, v2161, v339, 90);
    svfloat32_t v400 = svsub_f32_x(svptrue_b32(), v373, v399);
    svfloat32_t v420 = svsub_f32_x(svptrue_b32(), v386, v419);
    svfloat32_t v527 = svsub_f32_x(svptrue_b32(), v500, v526);
    svfloat32_t v547 = svsub_f32_x(svptrue_b32(), v513, v546);
    svfloat32_t v654 = svsub_f32_x(svptrue_b32(), v627, v653);
    svfloat32_t v674 = svsub_f32_x(svptrue_b32(), v640, v673);
    svfloat32_t v781 = svsub_f32_x(svptrue_b32(), v754, v780);
    svfloat32_t v801 = svsub_f32_x(svptrue_b32(), v767, v800);
    svfloat32_t v908 = svsub_f32_x(svptrue_b32(), v881, v907);
    svfloat32_t v928 = svsub_f32_x(svptrue_b32(), v894, v927);
    svfloat32_t v406 = svnmls_f32_x(pred_full, v400, v373, v2464);
    svfloat32_t v426 = svnmls_f32_x(pred_full, v420, v386, v2464);
    svfloat32_t v533 = svnmls_f32_x(pred_full, v527, v500, v2464);
    svfloat32_t v553 = svnmls_f32_x(pred_full, v547, v513, v2464);
    svfloat32_t v660 = svnmls_f32_x(pred_full, v654, v627, v2464);
    svfloat32_t v680 = svnmls_f32_x(pred_full, v674, v640, v2464);
    svfloat32_t v787 = svnmls_f32_x(pred_full, v781, v754, v2464);
    svfloat32_t v807 = svnmls_f32_x(pred_full, v801, v767, v2464);
    svfloat32_t v914 = svnmls_f32_x(pred_full, v908, v881, v2464);
    svfloat32_t v934 = svnmls_f32_x(pred_full, v928, v894, v2464);
    svfloat32_t v427 = svadd_f32_x(svptrue_b32(), v406, v426);
    svfloat32_t v428 = svsub_f32_x(svptrue_b32(), v406, v426);
    svfloat32_t v440 = svmla_f32_x(pred_full, v400, v420, v2424);
    svfloat32_t v458 = svnmls_f32_x(pred_full, v420, v400, v2424);
    svfloat32_t v554 = svadd_f32_x(svptrue_b32(), v533, v553);
    svfloat32_t v555 = svsub_f32_x(svptrue_b32(), v533, v553);
    svfloat32_t v567 = svmla_f32_x(pred_full, v527, v547, v2424);
    svfloat32_t v585 = svnmls_f32_x(pred_full, v547, v527, v2424);
    svfloat32_t v681 = svadd_f32_x(svptrue_b32(), v660, v680);
    svfloat32_t v682 = svsub_f32_x(svptrue_b32(), v660, v680);
    svfloat32_t v694 = svmla_f32_x(pred_full, v654, v674, v2424);
    svfloat32_t v712 = svnmls_f32_x(pred_full, v674, v654, v2424);
    svfloat32_t v808 = svadd_f32_x(svptrue_b32(), v787, v807);
    svfloat32_t v809 = svsub_f32_x(svptrue_b32(), v787, v807);
    svfloat32_t v821 = svmla_f32_x(pred_full, v781, v801, v2424);
    svfloat32_t v839 = svnmls_f32_x(pred_full, v801, v781, v2424);
    svfloat32_t v935 = svadd_f32_x(svptrue_b32(), v914, v934);
    svfloat32_t v936 = svsub_f32_x(svptrue_b32(), v914, v934);
    svfloat32_t v948 = svmla_f32_x(pred_full, v908, v928, v2424);
    svfloat32_t v966 = svnmls_f32_x(pred_full, v928, v908, v2424);
    svfloat32_t v459 = svadd_f32_x(svptrue_b32(), v2057, v427);
    svfloat32_t zero466 = svdup_n_f32(0);
    svfloat32_t v466 = svcmla_f32_x(pred_full, zero466, v2444, v440, 90);
    svfloat32_t zero474 = svdup_n_f32(0);
    svfloat32_t v474 = svcmla_f32_x(pred_full, zero474, v2444, v458, 90);
    svfloat32_t v586 = svadd_f32_x(svptrue_b32(), v87, v554);
    svfloat32_t zero593 = svdup_n_f32(0);
    svfloat32_t v593 = svcmla_f32_x(pred_full, zero593, v2444, v567, 90);
    svfloat32_t zero601 = svdup_n_f32(0);
    svfloat32_t v601 = svcmla_f32_x(pred_full, zero601, v2444, v585, 90);
    svfloat32_t v713 = svadd_f32_x(svptrue_b32(), v157, v681);
    svfloat32_t zero720 = svdup_n_f32(0);
    svfloat32_t v720 = svcmla_f32_x(pred_full, zero720, v2444, v694, 90);
    svfloat32_t zero728 = svdup_n_f32(0);
    svfloat32_t v728 = svcmla_f32_x(pred_full, zero728, v2444, v712, 90);
    svfloat32_t v840 = svadd_f32_x(svptrue_b32(), v227, v808);
    svfloat32_t zero847 = svdup_n_f32(0);
    svfloat32_t v847 = svcmla_f32_x(pred_full, zero847, v2444, v821, 90);
    svfloat32_t zero855 = svdup_n_f32(0);
    svfloat32_t v855 = svcmla_f32_x(pred_full, zero855, v2444, v839, 90);
    svfloat32_t v967 = svadd_f32_x(svptrue_b32(), v297, v935);
    svfloat32_t zero974 = svdup_n_f32(0);
    svfloat32_t v974 = svcmla_f32_x(pred_full, zero974, v2444, v948, 90);
    svfloat32_t zero982 = svdup_n_f32(0);
    svfloat32_t v982 = svcmla_f32_x(pred_full, zero982, v2444, v966, 90);
    svfloat32_t v434 = svmls_f32_x(pred_full, v2057, v427, v2420);
    svfloat32_t v561 = svmls_f32_x(pred_full, v87, v554, v2420);
    svfloat32_t v688 = svmls_f32_x(pred_full, v157, v681, v2420);
    svfloat32_t v815 = svmls_f32_x(pred_full, v227, v808, v2420);
    svfloat32_t v942 = svmls_f32_x(pred_full, v297, v935, v2420);
    svfloat32_t v446 = svmls_f32_x(pred_full, v434, v428, v2422);
    svfloat32_t v573 = svmls_f32_x(pred_full, v561, v555, v2422);
    svfloat32_t v700 = svmls_f32_x(pred_full, v688, v682, v2422);
    svfloat32_t v827 = svmls_f32_x(pred_full, v815, v809, v2422);
    svfloat32_t v954 = svmls_f32_x(pred_full, v942, v936, v2422);
    svfloat32_t v1008 = svcmla_f32_x(pred_full, v586, v2161, v586, 90);
    svfloat32_t v1021 = svcmla_f32_x(pred_full, v713, v2161, v713, 90);
    svfloat32_t v1034 = svcmla_f32_x(pred_full, v967, v2161, v967, 90);
    svfloat32_t v1054 = svcmla_f32_x(pred_full, v840, v2161, v840, 90);
    svfloat32_t v452 = svnmls_f32_x(pred_full, v446, v434, v2464);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v446, v474);
    svfloat32_t v579 = svnmls_f32_x(pred_full, v573, v561, v2464);
    svfloat32_t v602 = svsub_f32_x(svptrue_b32(), v573, v601);
    svfloat32_t v706 = svnmls_f32_x(pred_full, v700, v688, v2464);
    svfloat32_t v729 = svsub_f32_x(svptrue_b32(), v700, v728);
    svfloat32_t v833 = svnmls_f32_x(pred_full, v827, v815, v2464);
    svfloat32_t v856 = svsub_f32_x(svptrue_b32(), v827, v855);
    svfloat32_t v960 = svnmls_f32_x(pred_full, v954, v942, v2464);
    svfloat32_t v983 = svsub_f32_x(svptrue_b32(), v954, v982);
    svfloat32_t v1035 = svsub_f32_x(svptrue_b32(), v1008, v1034);
    svfloat32_t v1055 = svsub_f32_x(svptrue_b32(), v1021, v1054);
    svfloat32_t v467 = svsub_f32_x(svptrue_b32(), v452, v466);
    svfloat32_t v481 = svnmls_f32_x(pred_full, v475, v446, v2464);
    svfloat32_t v594 = svsub_f32_x(svptrue_b32(), v579, v593);
    svfloat32_t v608 = svnmls_f32_x(pred_full, v602, v573, v2464);
    svfloat32_t v721 = svsub_f32_x(svptrue_b32(), v706, v720);
    svfloat32_t v735 = svnmls_f32_x(pred_full, v729, v700, v2464);
    svfloat32_t v848 = svsub_f32_x(svptrue_b32(), v833, v847);
    svfloat32_t v862 = svnmls_f32_x(pred_full, v856, v827, v2464);
    svfloat32_t v975 = svsub_f32_x(svptrue_b32(), v960, v974);
    svfloat32_t v989 = svnmls_f32_x(pred_full, v983, v954, v2464);
    svfloat32_t v1041 = svnmls_f32_x(pred_full, v1035, v1008, v2464);
    svfloat32_t v1061 = svnmls_f32_x(pred_full, v1055, v1021, v2464);
    svfloat32_t v1334 = svmul_f32_x(svptrue_b32(), v602, v2282);
    svfloat32_t v1347 = svmul_f32_x(svptrue_b32(), v729, v2410);
    svfloat32_t v1360 = svmul_f32_x(svptrue_b32(), v983, v2412);
    svfloat32_t v1380 = svmul_f32_x(svptrue_b32(), v856, v2348);
    svfloat32_t v487 = svnmls_f32_x(pred_full, v467, v452, v2464);
    svfloat32_t v614 = svnmls_f32_x(pred_full, v594, v579, v2464);
    svfloat32_t v741 = svnmls_f32_x(pred_full, v721, v706, v2464);
    svfloat32_t v868 = svnmls_f32_x(pred_full, v848, v833, v2464);
    svfloat32_t v995 = svnmls_f32_x(pred_full, v975, v960, v2464);
    svfloat32_t v1062 = svadd_f32_x(svptrue_b32(), v1041, v1061);
    svfloat32_t v1063 = svsub_f32_x(svptrue_b32(), v1041, v1061);
    svfloat32_t v1075 = svmla_f32_x(pred_full, v1035, v1055, v2424);
    svfloat32_t v1093 = svnmls_f32_x(pred_full, v1055, v1035, v2424);
    svfloat32_t v1167 = svmul_f32_x(svptrue_b32(), v594, v2218);
    svfloat32_t v1180 = svmul_f32_x(svptrue_b32(), v721, v2282);
    svfloat32_t v1193 = svmul_f32_x(svptrue_b32(), v975, v2410);
    svfloat32_t v1213 = svmul_f32_x(svptrue_b32(), v848, v2346);
    svfloat32_t v1342 = svcmla_f32_x(pred_full, v1334, v2283, v602, 90);
    svfloat32_t v1355 = svcmla_f32_x(pred_full, v1347, v2411, v729, 90);
    svfloat32_t v1368 = svcmla_f32_x(pred_full, v1360, v2413, v983, 90);
    svfloat32_t v1388 = svcmla_f32_x(pred_full, v1380, v2349, v856, 90);
    svfloat32_t v1501 = svmul_f32_x(svptrue_b32(), v608, v2346);
    svfloat32_t v1514 = svmul_f32_x(svptrue_b32(), v735, v2348);
    svfloat32_t v1527 = svmul_f32_x(svptrue_b32(), v989, v2417);
    svfloat32_t v1547 = svmul_f32_x(svptrue_b32(), v862, v2414);
    svfloat32_t v1094 = svadd_f32_x(svptrue_b32(), v459, v1062);
    svfloat32_t zero1109 = svdup_n_f32(0);
    svfloat32_t v1109 = svcmla_f32_x(pred_full, zero1109, v2444, v1075, 90);
    svfloat32_t zero1125 = svdup_n_f32(0);
    svfloat32_t v1125 = svcmla_f32_x(pred_full, zero1125, v2444, v1093, 90);
    svfloat32_t v1175 = svcmla_f32_x(pred_full, v1167, v2219, v594, 90);
    svfloat32_t v1188 = svcmla_f32_x(pred_full, v1180, v2283, v721, 90);
    svfloat32_t v1201 = svcmla_f32_x(pred_full, v1193, v2411, v975, 90);
    svfloat32_t v1221 = svcmla_f32_x(pred_full, v1213, v2347, v848, 90);
    svfloat32_t v1369 = svsub_f32_x(svptrue_b32(), v1342, v1368);
    svfloat32_t v1389 = svsub_f32_x(svptrue_b32(), v1355, v1388);
    svfloat32_t v1509 = svcmla_f32_x(pred_full, v1501, v2347, v608, 90);
    svfloat32_t v1522 = svcmla_f32_x(pred_full, v1514, v2349, v735, 90);
    svfloat32_t v1535 = svcmla_f32_x(pred_full, v1527, v2418, v989, 90);
    svfloat32_t v1555 = svcmla_f32_x(pred_full, v1547, v2354, v862, 90);
    svfloat32_t v1668 = svmul_f32_x(svptrue_b32(), v614, v2410);
    svfloat32_t v1681 = svmul_f32_x(svptrue_b32(), v741, v2412);
    svfloat32_t v1694 = svmul_f32_x(svptrue_b32(), v995, v2414);
    svfloat32_t v1714 = svmul_f32_x(svptrue_b32(), v868, v2417);
    svfloat32_t v1069 = svmls_f32_x(pred_full, v459, v1062, v2420);
    svint16_t v1097 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1094, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1202 = svsub_f32_x(svptrue_b32(), v1175, v1201);
    svfloat32_t v1222 = svsub_f32_x(svptrue_b32(), v1188, v1221);
    svfloat32_t v1375 = svnmls_f32_x(pred_full, v1369, v1342, v2464);
    svfloat32_t v1395 = svnmls_f32_x(pred_full, v1389, v1355, v2464);
    svfloat32_t v1536 = svsub_f32_x(svptrue_b32(), v1509, v1535);
    svfloat32_t v1556 = svsub_f32_x(svptrue_b32(), v1522, v1555);
    svfloat32_t v1676 = svcmla_f32_x(pred_full, v1668, v2411, v614, 90);
    svfloat32_t v1689 = svcmla_f32_x(pred_full, v1681, v2413, v741, 90);
    svfloat32_t v1702 = svcmla_f32_x(pred_full, v1694, v2415, v995, 90);
    svfloat32_t v1722 = svcmla_f32_x(pred_full, v1714, v2418, v868, 90);
    svfloat32_t v1081 = svmls_f32_x(pred_full, v1069, v1063, v2422);
    svfloat32_t v1208 = svnmls_f32_x(pred_full, v1202, v1175, v2464);
    svfloat32_t v1228 = svnmls_f32_x(pred_full, v1222, v1188, v2464);
    svfloat32_t v1396 = svadd_f32_x(svptrue_b32(), v1375, v1395);
    svfloat32_t v1397 = svsub_f32_x(svptrue_b32(), v1375, v1395);
    svfloat32_t v1409 = svmla_f32_x(pred_full, v1369, v1389, v2424);
    svfloat32_t v1427 = svnmls_f32_x(pred_full, v1389, v1369, v2424);
    svfloat32_t v1542 = svnmls_f32_x(pred_full, v1536, v1509, v2464);
    svfloat32_t v1562 = svnmls_f32_x(pred_full, v1556, v1522, v2464);
    svfloat32_t v1703 = svsub_f32_x(svptrue_b32(), v1676, v1702);
    svfloat32_t v1723 = svsub_f32_x(svptrue_b32(), v1689, v1722);
    svst1w_u64(pred_full, (unsigned *)(v2175), svreinterpret_u64_s16(v1097));
    svfloat32_t v1087 = svnmls_f32_x(pred_full, v1081, v1069, v2464);
    svfloat32_t v1126 = svsub_f32_x(svptrue_b32(), v1081, v1125);
    svfloat32_t v1229 = svadd_f32_x(svptrue_b32(), v1208, v1228);
    svfloat32_t v1230 = svsub_f32_x(svptrue_b32(), v1208, v1228);
    svfloat32_t v1242 = svmla_f32_x(pred_full, v1202, v1222, v2424);
    svfloat32_t v1260 = svnmls_f32_x(pred_full, v1222, v1202, v2424);
    svfloat32_t v1428 = svadd_f32_x(svptrue_b32(), v475, v1396);
    svfloat32_t zero1443 = svdup_n_f32(0);
    svfloat32_t v1443 = svcmla_f32_x(pred_full, zero1443, v2444, v1409, 90);
    svfloat32_t zero1459 = svdup_n_f32(0);
    svfloat32_t v1459 = svcmla_f32_x(pred_full, zero1459, v2444, v1427, 90);
    svfloat32_t v1563 = svadd_f32_x(svptrue_b32(), v1542, v1562);
    svfloat32_t v1564 = svsub_f32_x(svptrue_b32(), v1542, v1562);
    svfloat32_t v1576 = svmla_f32_x(pred_full, v1536, v1556, v2424);
    svfloat32_t v1594 = svnmls_f32_x(pred_full, v1556, v1536, v2424);
    svfloat32_t v1709 = svnmls_f32_x(pred_full, v1703, v1676, v2464);
    svfloat32_t v1729 = svnmls_f32_x(pred_full, v1723, v1689, v2464);
    svfloat32_t v1110 = svsub_f32_x(svptrue_b32(), v1087, v1109);
    svint16_t v1129 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1126, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1140 = svnmls_f32_x(pred_full, v1126, v1081, v2464);
    svfloat32_t v1261 = svadd_f32_x(svptrue_b32(), v467, v1229);
    svfloat32_t zero1276 = svdup_n_f32(0);
    svfloat32_t v1276 = svcmla_f32_x(pred_full, zero1276, v2444, v1242, 90);
    svfloat32_t zero1292 = svdup_n_f32(0);
    svfloat32_t v1292 = svcmla_f32_x(pred_full, zero1292, v2444, v1260, 90);
    svfloat32_t v1403 = svmls_f32_x(pred_full, v475, v1396, v2420);
    svint16_t v1431 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1428, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1595 = svadd_f32_x(svptrue_b32(), v481, v1563);
    svfloat32_t zero1610 = svdup_n_f32(0);
    svfloat32_t v1610 = svcmla_f32_x(pred_full, zero1610, v2444, v1576, 90);
    svfloat32_t zero1626 = svdup_n_f32(0);
    svfloat32_t v1626 = svcmla_f32_x(pred_full, zero1626, v2444, v1594, 90);
    svfloat32_t v1730 = svadd_f32_x(svptrue_b32(), v1709, v1729);
    svfloat32_t v1731 = svsub_f32_x(svptrue_b32(), v1709, v1729);
    svfloat32_t v1743 = svmla_f32_x(pred_full, v1703, v1723, v2424);
    svfloat32_t v1761 = svnmls_f32_x(pred_full, v1723, v1703, v2424);
    svint16_t v1113 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1110, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1143 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1140, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1154 = svnmls_f32_x(pred_full, v1110, v1087, v2464);
    svfloat32_t v1236 = svmls_f32_x(pred_full, v467, v1229, v2420);
    svint16_t v1264 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1261, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1415 = svmls_f32_x(pred_full, v1403, v1397, v2422);
    svfloat32_t v1570 = svmls_f32_x(pred_full, v481, v1563, v2420);
    svint16_t v1598 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1595, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1762 = svadd_f32_x(svptrue_b32(), v487, v1730);
    svfloat32_t zero1777 = svdup_n_f32(0);
    svfloat32_t v1777 = svcmla_f32_x(pred_full, zero1777, v2444, v1743, 90);
    svfloat32_t zero1793 = svdup_n_f32(0);
    svfloat32_t v1793 = svcmla_f32_x(pred_full, zero1793, v2444, v1761, 90);
    svst1w_u64(pred_full, (unsigned *)(v2195), svreinterpret_u64_s16(v1129));
    svst1w_u64(pred_full, (unsigned *)(v2303), svreinterpret_u64_s16(v1431));
    svint16_t v1157 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1154, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1248 = svmls_f32_x(pred_full, v1236, v1230, v2422);
    svfloat32_t v1421 = svnmls_f32_x(pred_full, v1415, v1403, v2464);
    svfloat32_t v1460 = svsub_f32_x(svptrue_b32(), v1415, v1459);
    svfloat32_t v1582 = svmls_f32_x(pred_full, v1570, v1564, v2422);
    svfloat32_t v1737 = svmls_f32_x(pred_full, v487, v1730, v2420);
    svint16_t v1765 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1762, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v2185), svreinterpret_u64_s16(v1113));
    svst1w_u64(pred_full, (unsigned *)(v2205), svreinterpret_u64_s16(v1143));
    svst1w_u64(pred_full, (unsigned *)(v2239), svreinterpret_u64_s16(v1264));
    svst1w_u64(pred_full, (unsigned *)(v2367), svreinterpret_u64_s16(v1598));
    svfloat32_t v1254 = svnmls_f32_x(pred_full, v1248, v1236, v2464);
    svfloat32_t v1293 = svsub_f32_x(svptrue_b32(), v1248, v1292);
    svfloat32_t v1444 = svsub_f32_x(svptrue_b32(), v1421, v1443);
    svint16_t v1463 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1460, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1474 = svnmls_f32_x(pred_full, v1460, v1415, v2464);
    svfloat32_t v1588 = svnmls_f32_x(pred_full, v1582, v1570, v2464);
    svfloat32_t v1627 = svsub_f32_x(svptrue_b32(), v1582, v1626);
    svfloat32_t v1749 = svmls_f32_x(pred_full, v1737, v1731, v2422);
    svst1w_u64(pred_full, (unsigned *)(v2215), svreinterpret_u64_s16(v1157));
    svst1w_u64(pred_full, (unsigned *)(v2431), svreinterpret_u64_s16(v1765));
    svfloat32_t v1277 = svsub_f32_x(svptrue_b32(), v1254, v1276);
    svint16_t v1296 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1293, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1307 = svnmls_f32_x(pred_full, v1293, v1248, v2464);
    svint16_t v1447 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1444, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1477 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1474, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1488 = svnmls_f32_x(pred_full, v1444, v1421, v2464);
    svfloat32_t v1611 = svsub_f32_x(svptrue_b32(), v1588, v1610);
    svint16_t v1630 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1627, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1641 = svnmls_f32_x(pred_full, v1627, v1582, v2464);
    svfloat32_t v1755 = svnmls_f32_x(pred_full, v1749, v1737, v2464);
    svfloat32_t v1794 = svsub_f32_x(svptrue_b32(), v1749, v1793);
    svst1w_u64(pred_full, (unsigned *)(v2323), svreinterpret_u64_s16(v1463));
    svint16_t v1280 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1277, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1310 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1307, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1321 = svnmls_f32_x(pred_full, v1277, v1254, v2464);
    svint16_t v1491 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1488, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1614 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1611, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1644 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1641, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1655 = svnmls_f32_x(pred_full, v1611, v1588, v2464);
    svfloat32_t v1778 = svsub_f32_x(svptrue_b32(), v1755, v1777);
    svint16_t v1797 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1794, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1808 = svnmls_f32_x(pred_full, v1794, v1749, v2464);
    svst1w_u64(pred_full, (unsigned *)(v2259), svreinterpret_u64_s16(v1296));
    svst1w_u64(pred_full, (unsigned *)(v2313), svreinterpret_u64_s16(v1447));
    svst1w_u64(pred_full, (unsigned *)(v2333), svreinterpret_u64_s16(v1477));
    svst1w_u64(pred_full, (unsigned *)(v2387), svreinterpret_u64_s16(v1630));
    svint16_t v1324 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1321, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1658 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1655, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1781 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1778, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1811 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1808, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1822 = svnmls_f32_x(pred_full, v1778, v1755, v2464);
    svst1w_u64(pred_full, (unsigned *)(v2249), svreinterpret_u64_s16(v1280));
    svst1w_u64(pred_full, (unsigned *)(v2269), svreinterpret_u64_s16(v1310));
    svst1w_u64(pred_full, (unsigned *)(v2343), svreinterpret_u64_s16(v1491));
    svst1w_u64(pred_full, (unsigned *)(v2377), svreinterpret_u64_s16(v1614));
    svst1w_u64(pred_full, (unsigned *)(v2397), svreinterpret_u64_s16(v1644));
    svst1w_u64(pred_full, (unsigned *)(v2451), svreinterpret_u64_s16(v1797));
    svint16_t v1825 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1822, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v2279), svreinterpret_u64_s16(v1324));
    svst1w_u64(pred_full, (unsigned *)(v2407), svreinterpret_u64_s16(v1658));
    svst1w_u64(pred_full, (unsigned *)(v2441), svreinterpret_u64_s16(v1781));
    svst1w_u64(pred_full, (unsigned *)(v2461), svreinterpret_u64_s16(v1811));
    svst1w_u64(pred_full, (unsigned *)(v2471), svreinterpret_u64_s16(v1825));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu32(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v407 = v5[istride];
    float v1434 = 7.0710678118654757e-01F;
    float v1445 = -7.0710678118654746e-01F;
    float v1495 = 5.5557023301960229e-01F;
    float v1509 = -1.9509032201612861e-01F;
    float v1560 = 9.2387953251128674e-01F;
    float v1567 = -9.2387953251128685e-01F;
    float v1570 = 3.8268343236508967e-01F;
    float v1571 = -3.8268343236508967e-01F;
    float v1617 = 1.9509032201612833e-01F;
    float v1620 = -9.8078528040323043e-01F;
    float v1621 = 9.8078528040323043e-01F;
    float v1628 = -5.5557023301960218e-01F;
    float v1631 = 8.3146961230254524e-01F;
    float v1632 = -8.3146961230254524e-01F;
    float v1642 = -1.0000000000000000e+00F;
    float v1643 = 1.0000000000000000e+00F;
    float32x2_t v1645 = (float32x2_t){v4, v4};
    float32x2_t v444 = vtrn1_f32(v407, v407);
    float32x2_t v445 = vtrn2_f32(v407, v407);
    float32x2_t v851 = v5[0];
    float32x2_t v1252 = (float32x2_t){v1621, v1621};
    float32x2_t v1313 = (float32x2_t){v1560, v1560};
    float32x2_t v1317 = (float32x2_t){v1571, v1570};
    float32x2_t v1374 = (float32x2_t){v1631, v1631};
    float32x2_t v1378 = (float32x2_t){v1628, v1495};
    float32x2_t v1385 = (float32x2_t){v1509, v1509};
    float32x2_t v1435 = (float32x2_t){v1434, v1434};
    float32x2_t v1446 = (float32x2_t){v1445, v1445};
    float32x2_t v1450 = (float32x2_t){v1643, v1642};
    float32x2_t v1496 = (float32x2_t){v1495, v1495};
    float32x2_t v1500 = (float32x2_t){v1632, v1631};
    float32x2_t v1507 = (float32x2_t){v1620, v1620};
    float32x2_t v1511 = (float32x2_t){v1509, v1617};
    float32x2_t v1557 = (float32x2_t){v1570, v1570};
    float32x2_t v1561 = (float32x2_t){v1567, v1560};
    float32x2_t v1568 = (float32x2_t){v1567, v1567};
    float32x2_t v1572 = (float32x2_t){v1570, v1571};
    float32x2_t v1618 = (float32x2_t){v1617, v1617};
    float32x2_t v1622 = (float32x2_t){v1620, v1621};
    float32x2_t v1629 = (float32x2_t){v1628, v1628};
    float32x2_t v1633 = (float32x2_t){v1631, v1632};
    float32x2_t v1644 = (float32x2_t){v1642, v1643};
    float32x2_t v20 = v5[istride * 16];
    int64_t v37 = 30 + j * 62;
    float32x2_t v51 = v5[istride * 8];
    int64_t v55 = 14 + j * 62;
    float32x2_t v69 = v5[istride * 24];
    int64_t v73 = 46 + j * 62;
    float32x2_t v87 = v5[istride * 4];
    float32x2_t v105 = v5[istride * 20];
    int64_t v122 = 6 + j * 62;
    int64_t v135 = 38 + j * 62;
    float32x2_t v149 = v5[istride * 12];
    float32x2_t v167 = v5[istride * 28];
    int64_t v184 = 22 + j * 62;
    int64_t v197 = 54 + j * 62;
    float32x2_t v211 = v5[istride * 2];
    float32x2_t v229 = v5[istride * 18];
    int64_t v246 = 2 + j * 62;
    int64_t v259 = 34 + j * 62;
    float32x2_t v273 = v5[istride * 10];
    int64_t v277 = 18 + j * 62;
    float32x2_t v291 = v5[istride * 26];
    int64_t v295 = 50 + j * 62;
    float32x2_t v309 = v5[istride * 6];
    float32x2_t v327 = v5[istride * 22];
    int64_t v344 = 10 + j * 62;
    int64_t v357 = 42 + j * 62;
    float32x2_t v371 = v5[istride * 14];
    int64_t v375 = 26 + j * 62;
    float32x2_t v389 = v5[istride * 30];
    int64_t v393 = 58 + j * 62;
    float32x2_t v425 = v5[istride * 17];
    float32x2_t v443 = v7[j * 62];
    int64_t v447 = j * 62 + 1;
    int64_t v455 = 32 + j * 62;
    float32x2_t v469 = v5[istride * 9];
    int64_t v473 = 16 + j * 62;
    float32x2_t v487 = v5[istride * 25];
    int64_t v491 = 48 + j * 62;
    float32x2_t v505 = v5[istride * 5];
    float32x2_t v523 = v5[istride * 21];
    int64_t v540 = 8 + j * 62;
    int64_t v553 = 40 + j * 62;
    float32x2_t v567 = v5[istride * 13];
    float32x2_t v585 = v5[istride * 29];
    int64_t v602 = 24 + j * 62;
    int64_t v615 = 56 + j * 62;
    float32x2_t v629 = v5[istride * 3];
    float32x2_t v647 = v5[istride * 19];
    int64_t v664 = 4 + j * 62;
    int64_t v677 = 36 + j * 62;
    float32x2_t v691 = v5[istride * 11];
    int64_t v695 = 20 + j * 62;
    float32x2_t v709 = v5[istride * 27];
    int64_t v713 = 52 + j * 62;
    float32x2_t v727 = v5[istride * 7];
    float32x2_t v745 = v5[istride * 23];
    int64_t v762 = 12 + j * 62;
    int64_t v775 = 44 + j * 62;
    float32x2_t v789 = v5[istride * 15];
    float32x2_t v807 = v5[istride * 31];
    int64_t v824 = 28 + j * 62;
    int64_t v837 = 60 + j * 62;
    float32x2_t v1319 = vmul_f32(v1645, v1317);
    float32x2_t v1380 = vmul_f32(v1645, v1378);
    float32x2_t v1452 = vmul_f32(v1645, v1450);
    float32x2_t v1502 = vmul_f32(v1645, v1500);
    float32x2_t v1513 = vmul_f32(v1645, v1511);
    float32x2_t v1563 = vmul_f32(v1645, v1561);
    float32x2_t v1574 = vmul_f32(v1645, v1572);
    float32x2_t v1624 = vmul_f32(v1645, v1622);
    float32x2_t v1635 = vmul_f32(v1645, v1633);
    float32x2_t v1646 = vmul_f32(v1645, v1644);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v56 = v7[v55];
    float32x2_t v57 = vtrn1_f32(v51, v51);
    float32x2_t v58 = vtrn2_f32(v51, v51);
    int64_t v60 = v55 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vtrn1_f32(v69, v69);
    float32x2_t v76 = vtrn2_f32(v69, v69);
    int64_t v78 = v73 + 1;
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vtrn1_f32(v87, v87);
    float32x2_t v125 = vtrn2_f32(v87, v87);
    int64_t v127 = v122 + 1;
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vtrn1_f32(v105, v105);
    float32x2_t v138 = vtrn2_f32(v105, v105);
    int64_t v140 = v135 + 1;
    float32x2_t v185 = v7[v184];
    float32x2_t v186 = vtrn1_f32(v149, v149);
    float32x2_t v187 = vtrn2_f32(v149, v149);
    int64_t v189 = v184 + 1;
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vtrn1_f32(v167, v167);
    float32x2_t v200 = vtrn2_f32(v167, v167);
    int64_t v202 = v197 + 1;
    float32x2_t v247 = v7[v246];
    float32x2_t v248 = vtrn1_f32(v211, v211);
    float32x2_t v249 = vtrn2_f32(v211, v211);
    int64_t v251 = v246 + 1;
    float32x2_t v260 = v7[v259];
    float32x2_t v261 = vtrn1_f32(v229, v229);
    float32x2_t v262 = vtrn2_f32(v229, v229);
    int64_t v264 = v259 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vtrn1_f32(v273, v273);
    float32x2_t v280 = vtrn2_f32(v273, v273);
    int64_t v282 = v277 + 1;
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vtrn1_f32(v291, v291);
    float32x2_t v298 = vtrn2_f32(v291, v291);
    int64_t v300 = v295 + 1;
    float32x2_t v345 = v7[v344];
    float32x2_t v346 = vtrn1_f32(v309, v309);
    float32x2_t v347 = vtrn2_f32(v309, v309);
    int64_t v349 = v344 + 1;
    float32x2_t v358 = v7[v357];
    float32x2_t v359 = vtrn1_f32(v327, v327);
    float32x2_t v360 = vtrn2_f32(v327, v327);
    int64_t v362 = v357 + 1;
    float32x2_t v376 = v7[v375];
    float32x2_t v377 = vtrn1_f32(v371, v371);
    float32x2_t v378 = vtrn2_f32(v371, v371);
    int64_t v380 = v375 + 1;
    float32x2_t v394 = v7[v393];
    float32x2_t v395 = vtrn1_f32(v389, v389);
    float32x2_t v396 = vtrn2_f32(v389, v389);
    int64_t v398 = v393 + 1;
    float32x2_t v448 = v7[v447];
    float32x2_t v449 = vmul_f32(v444, v443);
    float32x2_t v456 = v7[v455];
    float32x2_t v457 = vtrn1_f32(v425, v425);
    float32x2_t v458 = vtrn2_f32(v425, v425);
    int64_t v460 = v455 + 1;
    float32x2_t v474 = v7[v473];
    float32x2_t v475 = vtrn1_f32(v469, v469);
    float32x2_t v476 = vtrn2_f32(v469, v469);
    int64_t v478 = v473 + 1;
    float32x2_t v492 = v7[v491];
    float32x2_t v493 = vtrn1_f32(v487, v487);
    float32x2_t v494 = vtrn2_f32(v487, v487);
    int64_t v496 = v491 + 1;
    float32x2_t v541 = v7[v540];
    float32x2_t v542 = vtrn1_f32(v505, v505);
    float32x2_t v543 = vtrn2_f32(v505, v505);
    int64_t v545 = v540 + 1;
    float32x2_t v554 = v7[v553];
    float32x2_t v555 = vtrn1_f32(v523, v523);
    float32x2_t v556 = vtrn2_f32(v523, v523);
    int64_t v558 = v553 + 1;
    float32x2_t v603 = v7[v602];
    float32x2_t v604 = vtrn1_f32(v567, v567);
    float32x2_t v605 = vtrn2_f32(v567, v567);
    int64_t v607 = v602 + 1;
    float32x2_t v616 = v7[v615];
    float32x2_t v617 = vtrn1_f32(v585, v585);
    float32x2_t v618 = vtrn2_f32(v585, v585);
    int64_t v620 = v615 + 1;
    float32x2_t v665 = v7[v664];
    float32x2_t v666 = vtrn1_f32(v629, v629);
    float32x2_t v667 = vtrn2_f32(v629, v629);
    int64_t v669 = v664 + 1;
    float32x2_t v678 = v7[v677];
    float32x2_t v679 = vtrn1_f32(v647, v647);
    float32x2_t v680 = vtrn2_f32(v647, v647);
    int64_t v682 = v677 + 1;
    float32x2_t v696 = v7[v695];
    float32x2_t v697 = vtrn1_f32(v691, v691);
    float32x2_t v698 = vtrn2_f32(v691, v691);
    int64_t v700 = v695 + 1;
    float32x2_t v714 = v7[v713];
    float32x2_t v715 = vtrn1_f32(v709, v709);
    float32x2_t v716 = vtrn2_f32(v709, v709);
    int64_t v718 = v713 + 1;
    float32x2_t v763 = v7[v762];
    float32x2_t v764 = vtrn1_f32(v727, v727);
    float32x2_t v765 = vtrn2_f32(v727, v727);
    int64_t v767 = v762 + 1;
    float32x2_t v776 = v7[v775];
    float32x2_t v777 = vtrn1_f32(v745, v745);
    float32x2_t v778 = vtrn2_f32(v745, v745);
    int64_t v780 = v775 + 1;
    float32x2_t v825 = v7[v824];
    float32x2_t v826 = vtrn1_f32(v789, v789);
    float32x2_t v827 = vtrn2_f32(v789, v789);
    int64_t v829 = v824 + 1;
    float32x2_t v838 = v7[v837];
    float32x2_t v839 = vtrn1_f32(v807, v807);
    float32x2_t v840 = vtrn2_f32(v807, v807);
    int64_t v842 = v837 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v79 = v7[v78];
    float32x2_t v80 = vmul_f32(v75, v74);
    float32x2_t v128 = v7[v127];
    float32x2_t v129 = vmul_f32(v124, v123);
    float32x2_t v141 = v7[v140];
    float32x2_t v142 = vmul_f32(v137, v136);
    float32x2_t v190 = v7[v189];
    float32x2_t v191 = vmul_f32(v186, v185);
    float32x2_t v203 = v7[v202];
    float32x2_t v204 = vmul_f32(v199, v198);
    float32x2_t v252 = v7[v251];
    float32x2_t v253 = vmul_f32(v248, v247);
    float32x2_t v265 = v7[v264];
    float32x2_t v266 = vmul_f32(v261, v260);
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v301 = v7[v300];
    float32x2_t v302 = vmul_f32(v297, v296);
    float32x2_t v350 = v7[v349];
    float32x2_t v351 = vmul_f32(v346, v345);
    float32x2_t v363 = v7[v362];
    float32x2_t v364 = vmul_f32(v359, v358);
    float32x2_t v381 = v7[v380];
    float32x2_t v382 = vmul_f32(v377, v376);
    float32x2_t v399 = v7[v398];
    float32x2_t v400 = vmul_f32(v395, v394);
    float32x2_t v461 = v7[v460];
    float32x2_t v462 = vmul_f32(v457, v456);
    float32x2_t v479 = v7[v478];
    float32x2_t v480 = vmul_f32(v475, v474);
    float32x2_t v497 = v7[v496];
    float32x2_t v498 = vmul_f32(v493, v492);
    float32x2_t v546 = v7[v545];
    float32x2_t v547 = vmul_f32(v542, v541);
    float32x2_t v559 = v7[v558];
    float32x2_t v560 = vmul_f32(v555, v554);
    float32x2_t v608 = v7[v607];
    float32x2_t v609 = vmul_f32(v604, v603);
    float32x2_t v621 = v7[v620];
    float32x2_t v622 = vmul_f32(v617, v616);
    float32x2_t v670 = v7[v669];
    float32x2_t v671 = vmul_f32(v666, v665);
    float32x2_t v683 = v7[v682];
    float32x2_t v684 = vmul_f32(v679, v678);
    float32x2_t v701 = v7[v700];
    float32x2_t v702 = vmul_f32(v697, v696);
    float32x2_t v719 = v7[v718];
    float32x2_t v720 = vmul_f32(v715, v714);
    float32x2_t v768 = v7[v767];
    float32x2_t v769 = vmul_f32(v764, v763);
    float32x2_t v781 = v7[v780];
    float32x2_t v782 = vmul_f32(v777, v776);
    float32x2_t v830 = v7[v829];
    float32x2_t v831 = vmul_f32(v826, v825);
    float32x2_t v843 = v7[v842];
    float32x2_t v844 = vmul_f32(v839, v838);
    float32x2_t v451 = vfma_f32(v449, v445, v448);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v82 = vfma_f32(v80, v76, v79);
    float32x2_t v131 = vfma_f32(v129, v125, v128);
    float32x2_t v144 = vfma_f32(v142, v138, v141);
    float32x2_t v193 = vfma_f32(v191, v187, v190);
    float32x2_t v206 = vfma_f32(v204, v200, v203);
    float32x2_t v255 = vfma_f32(v253, v249, v252);
    float32x2_t v268 = vfma_f32(v266, v262, v265);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v304 = vfma_f32(v302, v298, v301);
    float32x2_t v353 = vfma_f32(v351, v347, v350);
    float32x2_t v366 = vfma_f32(v364, v360, v363);
    float32x2_t v384 = vfma_f32(v382, v378, v381);
    float32x2_t v402 = vfma_f32(v400, v396, v399);
    float32x2_t v464 = vfma_f32(v462, v458, v461);
    float32x2_t v482 = vfma_f32(v480, v476, v479);
    float32x2_t v500 = vfma_f32(v498, v494, v497);
    float32x2_t v549 = vfma_f32(v547, v543, v546);
    float32x2_t v562 = vfma_f32(v560, v556, v559);
    float32x2_t v611 = vfma_f32(v609, v605, v608);
    float32x2_t v624 = vfma_f32(v622, v618, v621);
    float32x2_t v673 = vfma_f32(v671, v667, v670);
    float32x2_t v686 = vfma_f32(v684, v680, v683);
    float32x2_t v704 = vfma_f32(v702, v698, v701);
    float32x2_t v722 = vfma_f32(v720, v716, v719);
    float32x2_t v771 = vfma_f32(v769, v765, v768);
    float32x2_t v784 = vfma_f32(v782, v778, v781);
    float32x2_t v833 = vfma_f32(v831, v827, v830);
    float32x2_t v846 = vfma_f32(v844, v840, v843);
    float32x2_t v852 = vadd_f32(v851, v46);
    float32x2_t v853 = vsub_f32(v851, v46);
    float32x2_t v854 = vadd_f32(v64, v82);
    float32x2_t v855 = vsub_f32(v64, v82);
    float32x2_t v867 = vadd_f32(v131, v144);
    float32x2_t v868 = vsub_f32(v131, v144);
    float32x2_t v869 = vadd_f32(v193, v206);
    float32x2_t v870 = vsub_f32(v193, v206);
    float32x2_t v921 = vadd_f32(v255, v268);
    float32x2_t v922 = vsub_f32(v255, v268);
    float32x2_t v923 = vadd_f32(v286, v304);
    float32x2_t v924 = vsub_f32(v286, v304);
    float32x2_t v936 = vadd_f32(v353, v366);
    float32x2_t v937 = vsub_f32(v353, v366);
    float32x2_t v938 = vadd_f32(v384, v402);
    float32x2_t v939 = vsub_f32(v384, v402);
    float32x2_t v1075 = vadd_f32(v451, v464);
    float32x2_t v1076 = vsub_f32(v451, v464);
    float32x2_t v1077 = vadd_f32(v482, v500);
    float32x2_t v1078 = vsub_f32(v482, v500);
    float32x2_t v1090 = vadd_f32(v549, v562);
    float32x2_t v1091 = vsub_f32(v549, v562);
    float32x2_t v1092 = vadd_f32(v611, v624);
    float32x2_t v1093 = vsub_f32(v611, v624);
    float32x2_t v1144 = vadd_f32(v673, v686);
    float32x2_t v1145 = vsub_f32(v673, v686);
    float32x2_t v1146 = vadd_f32(v704, v722);
    float32x2_t v1147 = vsub_f32(v704, v722);
    float32x2_t v1159 = vadd_f32(v771, v784);
    float32x2_t v1160 = vsub_f32(v771, v784);
    float32x2_t v1161 = vadd_f32(v833, v846);
    float32x2_t v1162 = vsub_f32(v833, v846);
    float32x2_t v861 = vrev64_f32(v855);
    float32x2_t v863 = vadd_f32(v852, v854);
    float32x2_t v864 = vsub_f32(v852, v854);
    float32x2_t v871 = vadd_f32(v867, v869);
    float32x2_t v872 = vsub_f32(v867, v869);
    float32x2_t v887 = vmul_f32(v868, v1435);
    float32x2_t v898 = vmul_f32(v870, v1446);
    float32x2_t v930 = vrev64_f32(v924);
    float32x2_t v932 = vadd_f32(v921, v923);
    float32x2_t v933 = vsub_f32(v921, v923);
    float32x2_t v945 = vrev64_f32(v939);
    float32x2_t v947 = vadd_f32(v936, v938);
    float32x2_t v948 = vsub_f32(v936, v938);
    float32x2_t v1084 = vrev64_f32(v1078);
    float32x2_t v1086 = vadd_f32(v1075, v1077);
    float32x2_t v1087 = vsub_f32(v1075, v1077);
    float32x2_t v1094 = vadd_f32(v1090, v1092);
    float32x2_t v1095 = vsub_f32(v1090, v1092);
    float32x2_t v1110 = vmul_f32(v1091, v1435);
    float32x2_t v1121 = vmul_f32(v1093, v1446);
    float32x2_t v1153 = vrev64_f32(v1147);
    float32x2_t v1155 = vadd_f32(v1144, v1146);
    float32x2_t v1156 = vsub_f32(v1144, v1146);
    float32x2_t v1163 = vadd_f32(v1159, v1161);
    float32x2_t v1164 = vsub_f32(v1159, v1161);
    float32x2_t v1179 = vmul_f32(v1160, v1435);
    float32x2_t v1190 = vmul_f32(v1162, v1446);
    float32x2_t v862 = vmul_f32(v861, v1452);
    float32x2_t v878 = vrev64_f32(v872);
    float32x2_t v880 = vadd_f32(v863, v871);
    float32x2_t v881 = vsub_f32(v863, v871);
    float32x2_t v893 = vrev64_f32(v887);
    float32x2_t v904 = vrev64_f32(v898);
    float32x2_t v931 = vmul_f32(v930, v1452);
    float32x2_t v946 = vmul_f32(v945, v1452);
    float32x2_t v951 = vadd_f32(v932, v947);
    float32x2_t v952 = vsub_f32(v932, v947);
    float32x2_t v1004 = vmul_f32(v933, v1435);
    float32x2_t v1015 = vmul_f32(v948, v1446);
    float32x2_t v1085 = vmul_f32(v1084, v1452);
    float32x2_t v1101 = vrev64_f32(v1095);
    float32x2_t v1103 = vadd_f32(v1086, v1094);
    float32x2_t v1104 = vsub_f32(v1086, v1094);
    float32x2_t v1116 = vrev64_f32(v1110);
    float32x2_t v1127 = vrev64_f32(v1121);
    float32x2_t v1154 = vmul_f32(v1153, v1452);
    float32x2_t v1170 = vrev64_f32(v1164);
    float32x2_t v1172 = vadd_f32(v1155, v1163);
    float32x2_t v1173 = vsub_f32(v1155, v1163);
    float32x2_t v1185 = vrev64_f32(v1179);
    float32x2_t v1196 = vrev64_f32(v1190);
    float32x2_t v865 = vsub_f32(v853, v862);
    float32x2_t v866 = vadd_f32(v853, v862);
    float32x2_t v879 = vmul_f32(v878, v1452);
    float32x2_t v894 = vmul_f32(v893, v1646);
    float32x2_t v905 = vmul_f32(v904, v1452);
    float32x2_t v934 = vsub_f32(v922, v931);
    float32x2_t v935 = vadd_f32(v922, v931);
    float32x2_t v949 = vsub_f32(v937, v946);
    float32x2_t v950 = vadd_f32(v937, v946);
    float32x2_t v958 = vrev64_f32(v952);
    float32x2_t v960 = vadd_f32(v880, v951);
    float32x2_t v961 = vsub_f32(v880, v951);
    float32x2_t v1010 = vrev64_f32(v1004);
    float32x2_t v1021 = vrev64_f32(v1015);
    float32x2_t v1088 = vsub_f32(v1076, v1085);
    float32x2_t v1089 = vadd_f32(v1076, v1085);
    float32x2_t v1102 = vmul_f32(v1101, v1452);
    float32x2_t v1117 = vmul_f32(v1116, v1646);
    float32x2_t v1128 = vmul_f32(v1127, v1452);
    float32x2_t v1157 = vsub_f32(v1145, v1154);
    float32x2_t v1158 = vadd_f32(v1145, v1154);
    float32x2_t v1171 = vmul_f32(v1170, v1452);
    float32x2_t v1186 = vmul_f32(v1185, v1646);
    float32x2_t v1197 = vmul_f32(v1196, v1452);
    float32x2_t v1213 = vadd_f32(v1103, v1172);
    float32x2_t v1214 = vsub_f32(v1103, v1172);
    float32x2_t v1436 = vmul_f32(v1104, v1435);
    float32x2_t v1447 = vmul_f32(v1173, v1446);
    float32x2_t v882 = vsub_f32(v864, v879);
    float32x2_t v883 = vadd_f32(v864, v879);
    float32x2_t v906 = vadd_f32(v887, v894);
    float32x2_t v907 = vadd_f32(v898, v905);
    float32x2_t v959 = vmul_f32(v958, v1452);
    float32x2_t v967 = vmul_f32(v934, v1313);
    float32x2_t v973 = vrev64_f32(v934);
    float32x2_t v978 = vmul_f32(v949, v1557);
    float32x2_t v984 = vrev64_f32(v949);
    float32x2_t v1011 = vmul_f32(v1010, v1646);
    float32x2_t v1022 = vmul_f32(v1021, v1452);
    float32x2_t v1041 = vmul_f32(v935, v1557);
    float32x2_t v1047 = vrev64_f32(v935);
    float32x2_t v1052 = vmul_f32(v950, v1568);
    float32x2_t v1058 = vrev64_f32(v950);
    float32x2_t v1105 = vsub_f32(v1087, v1102);
    float32x2_t v1106 = vadd_f32(v1087, v1102);
    float32x2_t v1129 = vadd_f32(v1110, v1117);
    float32x2_t v1130 = vadd_f32(v1121, v1128);
    float32x2_t v1174 = vsub_f32(v1156, v1171);
    float32x2_t v1175 = vadd_f32(v1156, v1171);
    float32x2_t v1198 = vadd_f32(v1179, v1186);
    float32x2_t v1199 = vadd_f32(v1190, v1197);
    float32x2_t v1220 = vrev64_f32(v1214);
    float32x2_t v1222 = vadd_f32(v960, v1213);
    float32x2_t v1223 = vsub_f32(v960, v1213);
    float32x2_t v1442 = vrev64_f32(v1436);
    float32x2_t v1453 = vrev64_f32(v1447);
    float32x2_t v908 = vadd_f32(v906, v907);
    float32x2_t v909 = vsub_f32(v907, v906);
    float32x2_t v962 = vsub_f32(v881, v959);
    float32x2_t v963 = vadd_f32(v881, v959);
    float32x2_t v1023 = vadd_f32(v1004, v1011);
    float32x2_t v1024 = vadd_f32(v1015, v1022);
    float32x2_t v1131 = vadd_f32(v1129, v1130);
    float32x2_t v1132 = vsub_f32(v1130, v1129);
    float32x2_t v1200 = vadd_f32(v1198, v1199);
    float32x2_t v1201 = vsub_f32(v1199, v1198);
    float32x2_t v1221 = vmul_f32(v1220, v1452);
    int16x4_t v1228 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1222, 15), (int32x2_t){0, 0}));
    int16x4_t v1240 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1223, 15), (int32x2_t){0, 0}));
    float32x2_t v1314 = vmul_f32(v1105, v1313);
    float32x2_t v1320 = vrev64_f32(v1105);
    float32x2_t v1325 = vmul_f32(v1174, v1557);
    float32x2_t v1331 = vrev64_f32(v1174);
    float32x2_t v1443 = vmul_f32(v1442, v1646);
    float32x2_t v1454 = vmul_f32(v1453, v1452);
    float32x2_t v1558 = vmul_f32(v1106, v1557);
    float32x2_t v1564 = vrev64_f32(v1106);
    float32x2_t v1569 = vmul_f32(v1175, v1568);
    float32x2_t v1575 = vrev64_f32(v1175);
    float32x2_t v915 = vrev64_f32(v909);
    float32x2_t v917 = vadd_f32(v865, v908);
    float32x2_t v918 = vsub_f32(v865, v908);
    float32x2_t v986 = vfma_f32(v967, v973, v1319);
    float32x2_t v987 = vfma_f32(v978, v984, v1563);
    float32x2_t v1025 = vadd_f32(v1023, v1024);
    float32x2_t v1026 = vsub_f32(v1024, v1023);
    float32x2_t v1060 = vfma_f32(v1041, v1047, v1563);
    float32x2_t v1061 = vfma_f32(v1052, v1058, v1574);
    float32x2_t v1138 = vrev64_f32(v1132);
    float32x2_t v1140 = vadd_f32(v1088, v1131);
    float32x2_t v1141 = vsub_f32(v1088, v1131);
    float32x2_t v1207 = vrev64_f32(v1201);
    float32x2_t v1209 = vadd_f32(v1157, v1200);
    float32x2_t v1210 = vsub_f32(v1157, v1200);
    float32x2_t v1224 = vsub_f32(v961, v1221);
    float32x2_t v1225 = vadd_f32(v961, v1221);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v1228), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v1240), 0);
    float32x2_t v1455 = vadd_f32(v1436, v1443);
    float32x2_t v1456 = vadd_f32(v1447, v1454);
    float32x2_t v916 = vmul_f32(v915, v1646);
    float32x2_t v988 = vadd_f32(v986, v987);
    float32x2_t v989 = vsub_f32(v987, v986);
    float32x2_t v1032 = vrev64_f32(v1026);
    float32x2_t v1034 = vadd_f32(v882, v1025);
    float32x2_t v1035 = vsub_f32(v882, v1025);
    float32x2_t v1062 = vadd_f32(v1060, v1061);
    float32x2_t v1063 = vsub_f32(v1061, v1060);
    float32x2_t v1139 = vmul_f32(v1138, v1646);
    float32x2_t v1208 = vmul_f32(v1207, v1646);
    int16x4_t v1234 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1224, 15), (int32x2_t){0, 0}));
    int16x4_t v1246 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1225, 15), (int32x2_t){0, 0}));
    float32x2_t v1253 = vmul_f32(v1140, v1252);
    float32x2_t v1259 = vrev64_f32(v1140);
    float32x2_t v1264 = vmul_f32(v1209, v1374);
    float32x2_t v1270 = vrev64_f32(v1209);
    float32x2_t v1333 = vfma_f32(v1314, v1320, v1319);
    float32x2_t v1334 = vfma_f32(v1325, v1331, v1563);
    float32x2_t v1457 = vadd_f32(v1455, v1456);
    float32x2_t v1458 = vsub_f32(v1456, v1455);
    float32x2_t v1497 = vmul_f32(v1141, v1496);
    float32x2_t v1503 = vrev64_f32(v1141);
    float32x2_t v1508 = vmul_f32(v1210, v1507);
    float32x2_t v1514 = vrev64_f32(v1210);
    float32x2_t v1577 = vfma_f32(v1558, v1564, v1563);
    float32x2_t v1578 = vfma_f32(v1569, v1575, v1574);
    float32x2_t v919 = vsub_f32(v866, v916);
    float32x2_t v920 = vadd_f32(v866, v916);
    float32x2_t v995 = vrev64_f32(v989);
    float32x2_t v997 = vadd_f32(v917, v988);
    float32x2_t v998 = vsub_f32(v917, v988);
    float32x2_t v1033 = vmul_f32(v1032, v1646);
    float32x2_t v1069 = vrev64_f32(v1063);
    float32x2_t v1142 = vsub_f32(v1089, v1139);
    float32x2_t v1143 = vadd_f32(v1089, v1139);
    float32x2_t v1211 = vsub_f32(v1158, v1208);
    float32x2_t v1212 = vadd_f32(v1158, v1208);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v1234), 0);
    v6[ostride * 24] = vget_lane_s32(vreinterpret_s32_s16(v1246), 0);
    float32x2_t v1335 = vadd_f32(v1333, v1334);
    float32x2_t v1336 = vsub_f32(v1334, v1333);
    float32x2_t v1464 = vrev64_f32(v1458);
    float32x2_t v1466 = vadd_f32(v962, v1457);
    float32x2_t v1467 = vsub_f32(v962, v1457);
    float32x2_t v1579 = vadd_f32(v1577, v1578);
    float32x2_t v1580 = vsub_f32(v1578, v1577);
    float32x2_t v996 = vmul_f32(v995, v1646);
    float32x2_t v1036 = vsub_f32(v883, v1033);
    float32x2_t v1037 = vadd_f32(v883, v1033);
    float32x2_t v1070 = vmul_f32(v1069, v1646);
    float32x2_t v1071 = vadd_f32(v919, v1062);
    float32x2_t v1072 = vsub_f32(v919, v1062);
    float32x2_t v1272 = vfma_f32(v1253, v1259, v1513);
    float32x2_t v1273 = vfma_f32(v1264, v1270, v1380);
    float32x2_t v1342 = vrev64_f32(v1336);
    float32x2_t v1344 = vadd_f32(v1034, v1335);
    float32x2_t v1345 = vsub_f32(v1034, v1335);
    float32x2_t v1375 = vmul_f32(v1142, v1374);
    float32x2_t v1381 = vrev64_f32(v1142);
    float32x2_t v1386 = vmul_f32(v1211, v1385);
    float32x2_t v1392 = vrev64_f32(v1211);
    float32x2_t v1465 = vmul_f32(v1464, v1646);
    int16x4_t v1472 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1466, 15), (int32x2_t){0, 0}));
    int16x4_t v1484 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1467, 15), (int32x2_t){0, 0}));
    float32x2_t v1516 = vfma_f32(v1497, v1503, v1502);
    float32x2_t v1517 = vfma_f32(v1508, v1514, v1513);
    float32x2_t v1586 = vrev64_f32(v1580);
    float32x2_t v1619 = vmul_f32(v1143, v1618);
    float32x2_t v1625 = vrev64_f32(v1143);
    float32x2_t v1630 = vmul_f32(v1212, v1629);
    float32x2_t v1636 = vrev64_f32(v1212);
    float32x2_t v999 = vsub_f32(v918, v996);
    float32x2_t v1000 = vadd_f32(v918, v996);
    float32x2_t v1073 = vsub_f32(v920, v1070);
    float32x2_t v1074 = vadd_f32(v920, v1070);
    float32x2_t v1274 = vadd_f32(v1272, v1273);
    float32x2_t v1275 = vsub_f32(v1273, v1272);
    float32x2_t v1343 = vmul_f32(v1342, v1646);
    int16x4_t v1350 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1344, 15), (int32x2_t){0, 0}));
    int16x4_t v1362 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1345, 15), (int32x2_t){0, 0}));
    float32x2_t v1468 = vsub_f32(v963, v1465);
    float32x2_t v1469 = vadd_f32(v963, v1465);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v1472), 0);
    v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v1484), 0);
    float32x2_t v1518 = vadd_f32(v1516, v1517);
    float32x2_t v1519 = vsub_f32(v1517, v1516);
    float32x2_t v1587 = vmul_f32(v1586, v1646);
    float32x2_t v1588 = vadd_f32(v1036, v1579);
    float32x2_t v1589 = vsub_f32(v1036, v1579);
    float32x2_t v1281 = vrev64_f32(v1275);
    float32x2_t v1283 = vadd_f32(v997, v1274);
    float32x2_t v1284 = vsub_f32(v997, v1274);
    float32x2_t v1346 = vsub_f32(v1035, v1343);
    float32x2_t v1347 = vadd_f32(v1035, v1343);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v1350), 0);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v1362), 0);
    float32x2_t v1394 = vfma_f32(v1375, v1381, v1380);
    float32x2_t v1395 = vfma_f32(v1386, v1392, v1624);
    int16x4_t v1478 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1468, 15), (int32x2_t){0, 0}));
    int16x4_t v1490 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1469, 15), (int32x2_t){0, 0}));
    float32x2_t v1525 = vrev64_f32(v1519);
    float32x2_t v1527 = vadd_f32(v999, v1518);
    float32x2_t v1528 = vsub_f32(v999, v1518);
    float32x2_t v1590 = vsub_f32(v1037, v1587);
    float32x2_t v1591 = vadd_f32(v1037, v1587);
    int16x4_t v1594 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1588, 15), (int32x2_t){0, 0}));
    int16x4_t v1606 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1589, 15), (int32x2_t){0, 0}));
    float32x2_t v1638 = vfma_f32(v1619, v1625, v1624);
    float32x2_t v1639 = vfma_f32(v1630, v1636, v1635);
    float32x2_t v1282 = vmul_f32(v1281, v1646);
    int16x4_t v1289 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1283, 15), (int32x2_t){0, 0}));
    int16x4_t v1301 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1284, 15), (int32x2_t){0, 0}));
    int16x4_t v1356 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1346, 15), (int32x2_t){0, 0}));
    int16x4_t v1368 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1347, 15), (int32x2_t){0, 0}));
    float32x2_t v1396 = vadd_f32(v1394, v1395);
    float32x2_t v1397 = vsub_f32(v1395, v1394);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v1478), 0);
    v6[ostride * 28] = vget_lane_s32(vreinterpret_s32_s16(v1490), 0);
    float32x2_t v1526 = vmul_f32(v1525, v1646);
    int16x4_t v1533 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1527, 15), (int32x2_t){0, 0}));
    int16x4_t v1545 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1528, 15), (int32x2_t){0, 0}));
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v1594), 0);
    int16x4_t v1600 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1590, 15), (int32x2_t){0, 0}));
    v6[ostride * 22] = vget_lane_s32(vreinterpret_s32_s16(v1606), 0);
    int16x4_t v1612 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1591, 15), (int32x2_t){0, 0}));
    float32x2_t v1640 = vadd_f32(v1638, v1639);
    float32x2_t v1641 = vsub_f32(v1639, v1638);
    float32x2_t v1285 = vsub_f32(v998, v1282);
    float32x2_t v1286 = vadd_f32(v998, v1282);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v1289), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v1301), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v1356), 0);
    v6[ostride * 26] = vget_lane_s32(vreinterpret_s32_s16(v1368), 0);
    float32x2_t v1403 = vrev64_f32(v1397);
    float32x2_t v1405 = vadd_f32(v1071, v1396);
    float32x2_t v1406 = vsub_f32(v1071, v1396);
    float32x2_t v1529 = vsub_f32(v1000, v1526);
    float32x2_t v1530 = vadd_f32(v1000, v1526);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v1533), 0);
    v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v1545), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v1600), 0);
    v6[ostride * 30] = vget_lane_s32(vreinterpret_s32_s16(v1612), 0);
    float32x2_t v1647 = vrev64_f32(v1641);
    float32x2_t v1649 = vadd_f32(v1073, v1640);
    float32x2_t v1650 = vsub_f32(v1073, v1640);
    int16x4_t v1295 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1285, 15), (int32x2_t){0, 0}));
    int16x4_t v1307 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1286, 15), (int32x2_t){0, 0}));
    float32x2_t v1404 = vmul_f32(v1403, v1646);
    int16x4_t v1411 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1405, 15), (int32x2_t){0, 0}));
    int16x4_t v1423 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1406, 15), (int32x2_t){0, 0}));
    int16x4_t v1539 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1529, 15), (int32x2_t){0, 0}));
    int16x4_t v1551 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1530, 15), (int32x2_t){0, 0}));
    float32x2_t v1648 = vmul_f32(v1647, v1646);
    int16x4_t v1655 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1649, 15), (int32x2_t){0, 0}));
    int16x4_t v1667 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1650, 15), (int32x2_t){0, 0}));
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v1295), 0);
    v6[ostride * 25] = vget_lane_s32(vreinterpret_s32_s16(v1307), 0);
    float32x2_t v1407 = vsub_f32(v1072, v1404);
    float32x2_t v1408 = vadd_f32(v1072, v1404);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v1411), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v1423), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v1539), 0);
    v6[ostride * 29] = vget_lane_s32(vreinterpret_s32_s16(v1551), 0);
    float32x2_t v1651 = vsub_f32(v1074, v1648);
    float32x2_t v1652 = vadd_f32(v1074, v1648);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v1655), 0);
    v6[ostride * 23] = vget_lane_s32(vreinterpret_s32_s16(v1667), 0);
    int16x4_t v1417 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1407, 15), (int32x2_t){0, 0}));
    int16x4_t v1429 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1408, 15), (int32x2_t){0, 0}));
    int16x4_t v1661 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1651, 15), (int32x2_t){0, 0}));
    int16x4_t v1673 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1652, 15), (int32x2_t){0, 0}));
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v1417), 0);
    v6[ostride * 27] = vget_lane_s32(vreinterpret_s32_s16(v1429), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v1661), 0);
    v6[ostride * 31] = vget_lane_s32(vreinterpret_s32_s16(v1673), 0);
    v5 += 1 * idist;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ab_t_gu32(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v1179 = -1.9509032201612819e-01F;
    float v1238 = 7.0710678118654757e-01F;
    float v1250 = -7.0710678118654746e-01F;
    float v1255 = -1.0000000000000000e+00F;
    float v1309 = 5.5557023301960229e-01F;
    float v1314 = 8.3146961230254524e-01F;
    float v1321 = -9.8078528040323043e-01F;
    float v1380 = 3.8268343236508984e-01F;
    float v1385 = 9.2387953251128674e-01F;
    float v1392 = -9.2387953251128685e-01F;
    float v1397 = -3.8268343236508967e-01F;
    float v1451 = 1.9509032201612833e-01F;
    float v1456 = 9.8078528040323043e-01F;
    float v1463 = -5.5557023301960218e-01F;
    float v1468 = -8.3146961230254524e-01F;
    const float32x2_t *v1662 = &v5[v0];
    int32_t *v1900 = &v6[v2];
    int64_t v19 = v0 * 16;
    int64_t v34 = v10 * 15;
    int64_t v40 = v0 * 8;
    int64_t v48 = v10 * 7;
    int64_t v54 = v0 * 24;
    int64_t v62 = v10 * 23;
    int64_t v68 = v0 * 4;
    int64_t v82 = v0 * 20;
    int64_t v97 = v10 * 3;
    int64_t v104 = v10 * 19;
    int64_t v110 = v0 * 12;
    int64_t v124 = v0 * 28;
    int64_t v139 = v10 * 11;
    int64_t v146 = v10 * 27;
    int64_t v152 = v0 * 2;
    int64_t v166 = v0 * 18;
    int64_t v188 = v10 * 17;
    int64_t v194 = v0 * 10;
    int64_t v202 = v10 * 9;
    int64_t v208 = v0 * 26;
    int64_t v216 = v10 * 25;
    int64_t v222 = v0 * 6;
    int64_t v236 = v0 * 22;
    int64_t v251 = v10 * 5;
    int64_t v258 = v10 * 21;
    int64_t v264 = v0 * 14;
    int64_t v272 = v10 * 13;
    int64_t v278 = v0 * 30;
    int64_t v286 = v10 * 29;
    int64_t v306 = v0 * 17;
    int64_t v328 = v10 * 16;
    int64_t v334 = v0 * 9;
    int64_t v342 = v10 * 8;
    int64_t v348 = v0 * 25;
    int64_t v356 = v10 * 24;
    int64_t v362 = v0 * 5;
    int64_t v376 = v0 * 21;
    int64_t v391 = v10 * 4;
    int64_t v398 = v10 * 20;
    int64_t v404 = v0 * 13;
    int64_t v418 = v0 * 29;
    int64_t v433 = v10 * 12;
    int64_t v440 = v10 * 28;
    int64_t v446 = v0 * 3;
    int64_t v460 = v0 * 19;
    int64_t v475 = v10 * 2;
    int64_t v482 = v10 * 18;
    int64_t v488 = v0 * 11;
    int64_t v496 = v10 * 10;
    int64_t v502 = v0 * 27;
    int64_t v510 = v10 * 26;
    int64_t v516 = v0 * 7;
    int64_t v530 = v0 * 23;
    int64_t v545 = v10 * 6;
    int64_t v552 = v10 * 22;
    int64_t v558 = v0 * 15;
    int64_t v572 = v0 * 31;
    int64_t v587 = v10 * 14;
    int64_t v594 = v10 * 30;
    int64_t v595 = v13 * 31;
    int64_t v1001 = v2 * 8;
    int64_t v1009 = v2 * 16;
    int64_t v1017 = v2 * 24;
    int64_t v1072 = v2 * 9;
    int64_t v1080 = v2 * 17;
    int64_t v1088 = v2 * 25;
    float v1104 = v4 * v1380;
    int64_t v1135 = v2 * 2;
    int64_t v1143 = v2 * 10;
    int64_t v1151 = v2 * 18;
    int64_t v1159 = v2 * 26;
    float v1175 = v4 * v1309;
    int64_t v1206 = v2 * 3;
    int64_t v1214 = v2 * 11;
    int64_t v1222 = v2 * 19;
    int64_t v1230 = v2 * 27;
    float v1258 = v4 * v1255;
    int64_t v1277 = v2 * 4;
    int64_t v1285 = v2 * 12;
    int64_t v1293 = v2 * 20;
    int64_t v1301 = v2 * 28;
    float v1317 = v4 * v1314;
    float v1329 = v4 * v1451;
    int64_t v1348 = v2 * 5;
    int64_t v1356 = v2 * 13;
    int64_t v1364 = v2 * 21;
    int64_t v1372 = v2 * 29;
    float v1388 = v4 * v1385;
    float v1400 = v4 * v1397;
    int64_t v1419 = v2 * 6;
    int64_t v1427 = v2 * 14;
    int64_t v1435 = v2 * 22;
    int64_t v1443 = v2 * 30;
    float v1459 = v4 * v1456;
    float v1471 = v4 * v1468;
    int64_t v1490 = v2 * 7;
    int64_t v1498 = v2 * 15;
    int64_t v1506 = v2 * 23;
    int64_t v1514 = v2 * 31;
    const float32x2_t *v1809 = &v5[0];
    svint64_t v1810 = svindex_s64(0, v1);
    int32_t *v1859 = &v6[0];
    svfloat32_t v1889 = svdup_n_f32(v1456);
    svfloat32_t v1930 = svdup_n_f32(v1385);
    svfloat32_t v1971 = svdup_n_f32(v1314);
    svfloat32_t v1973 = svdup_n_f32(v1179);
    svfloat32_t v2012 = svdup_n_f32(v1238);
    svfloat32_t v2014 = svdup_n_f32(v1250);
    svfloat32_t v2053 = svdup_n_f32(v1309);
    svfloat32_t v2055 = svdup_n_f32(v1321);
    svfloat32_t v2094 = svdup_n_f32(v1380);
    svfloat32_t v2096 = svdup_n_f32(v1392);
    svfloat32_t v2135 = svdup_n_f32(v1451);
    svfloat32_t v2137 = svdup_n_f32(v1463);
    svfloat32_t v2139 = svdup_n_f32(v4);
    int64_t v36 = v34 + v595;
    int64_t v50 = v48 + v595;
    int64_t v64 = v62 + v595;
    int64_t v99 = v97 + v595;
    int64_t v106 = v104 + v595;
    int64_t v141 = v139 + v595;
    int64_t v148 = v146 + v595;
    int64_t v183 = v10 + v595;
    int64_t v190 = v188 + v595;
    int64_t v204 = v202 + v595;
    int64_t v218 = v216 + v595;
    int64_t v253 = v251 + v595;
    int64_t v260 = v258 + v595;
    int64_t v274 = v272 + v595;
    int64_t v288 = v286 + v595;
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v595]));
    int64_t v330 = v328 + v595;
    int64_t v344 = v342 + v595;
    int64_t v358 = v356 + v595;
    int64_t v393 = v391 + v595;
    int64_t v400 = v398 + v595;
    int64_t v435 = v433 + v595;
    int64_t v442 = v440 + v595;
    int64_t v477 = v475 + v595;
    int64_t v484 = v482 + v595;
    int64_t v498 = v496 + v595;
    int64_t v512 = v510 + v595;
    int64_t v547 = v545 + v595;
    int64_t v554 = v552 + v595;
    int64_t v589 = v587 + v595;
    int64_t v596 = v594 + v595;
    const float32x2_t *v1527 = &v5[v19];
    const float32x2_t *v1536 = &v5[v40];
    const float32x2_t *v1545 = &v5[v54];
    const float32x2_t *v1554 = &v5[v68];
    const float32x2_t *v1563 = &v5[v82];
    const float32x2_t *v1572 = &v5[v110];
    const float32x2_t *v1581 = &v5[v124];
    const float32x2_t *v1590 = &v5[v152];
    const float32x2_t *v1599 = &v5[v166];
    const float32x2_t *v1608 = &v5[v194];
    const float32x2_t *v1617 = &v5[v208];
    const float32x2_t *v1626 = &v5[v222];
    const float32x2_t *v1635 = &v5[v236];
    const float32x2_t *v1644 = &v5[v264];
    const float32x2_t *v1653 = &v5[v278];
    svfloat32_t v1664 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1662), v1810));
    const float32x2_t *v1672 = &v5[v306];
    const float32x2_t *v1682 = &v5[v334];
    const float32x2_t *v1691 = &v5[v348];
    const float32x2_t *v1700 = &v5[v362];
    const float32x2_t *v1709 = &v5[v376];
    const float32x2_t *v1718 = &v5[v404];
    const float32x2_t *v1727 = &v5[v418];
    const float32x2_t *v1736 = &v5[v446];
    const float32x2_t *v1745 = &v5[v460];
    const float32x2_t *v1754 = &v5[v488];
    const float32x2_t *v1763 = &v5[v502];
    const float32x2_t *v1772 = &v5[v516];
    const float32x2_t *v1781 = &v5[v530];
    const float32x2_t *v1790 = &v5[v558];
    const float32x2_t *v1799 = &v5[v572];
    svfloat32_t v1811 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1809), v1810));
    int32_t *v1868 = &v6[v1001];
    int32_t *v1877 = &v6[v1009];
    int32_t *v1886 = &v6[v1017];
    int32_t *v1909 = &v6[v1072];
    int32_t *v1918 = &v6[v1080];
    int32_t *v1927 = &v6[v1088];
    svfloat32_t v1931 = svdup_n_f32(v1104);
    int32_t *v1941 = &v6[v1135];
    int32_t *v1950 = &v6[v1143];
    int32_t *v1959 = &v6[v1151];
    int32_t *v1968 = &v6[v1159];
    svfloat32_t v1972 = svdup_n_f32(v1175);
    int32_t *v1982 = &v6[v1206];
    int32_t *v1991 = &v6[v1214];
    int32_t *v2000 = &v6[v1222];
    int32_t *v2009 = &v6[v1230];
    svfloat32_t v2015 = svdup_n_f32(v1258);
    int32_t *v2023 = &v6[v1277];
    int32_t *v2032 = &v6[v1285];
    int32_t *v2041 = &v6[v1293];
    int32_t *v2050 = &v6[v1301];
    svfloat32_t v2054 = svdup_n_f32(v1317);
    svfloat32_t v2056 = svdup_n_f32(v1329);
    int32_t *v2064 = &v6[v1348];
    int32_t *v2073 = &v6[v1356];
    int32_t *v2082 = &v6[v1364];
    int32_t *v2091 = &v6[v1372];
    svfloat32_t v2095 = svdup_n_f32(v1388);
    svfloat32_t v2097 = svdup_n_f32(v1400);
    int32_t *v2105 = &v6[v1419];
    int32_t *v2114 = &v6[v1427];
    int32_t *v2123 = &v6[v1435];
    int32_t *v2132 = &v6[v1443];
    svfloat32_t v2136 = svdup_n_f32(v1459);
    svfloat32_t v2138 = svdup_n_f32(v1471);
    int32_t *v2146 = &v6[v1490];
    int32_t *v2155 = &v6[v1498];
    int32_t *v2164 = &v6[v1506];
    int32_t *v2173 = &v6[v1514];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v50]));
    svfloat32_t v65 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v64]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v107 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v106]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v149 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v148]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v191 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v190]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v219 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v218]));
    svfloat32_t v254 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v253]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v275 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v274]));
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v288]));
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero325, v1664, v324, 0), v1664,
        v324, 90);
    svfloat32_t v331 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v330]));
    svfloat32_t v345 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v344]));
    svfloat32_t v359 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v358]));
    svfloat32_t v394 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v393]));
    svfloat32_t v401 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v400]));
    svfloat32_t v436 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v435]));
    svfloat32_t v443 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v442]));
    svfloat32_t v478 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v477]));
    svfloat32_t v485 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v484]));
    svfloat32_t v499 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v498]));
    svfloat32_t v513 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v512]));
    svfloat32_t v548 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v547]));
    svfloat32_t v555 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v554]));
    svfloat32_t v590 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v589]));
    svfloat32_t v597 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v596]));
    svfloat32_t v1529 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1527), v1810));
    svfloat32_t v1538 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1536), v1810));
    svfloat32_t v1547 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1545), v1810));
    svfloat32_t v1556 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1554), v1810));
    svfloat32_t v1565 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1563), v1810));
    svfloat32_t v1574 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1572), v1810));
    svfloat32_t v1583 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1581), v1810));
    svfloat32_t v1592 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1590), v1810));
    svfloat32_t v1601 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1599), v1810));
    svfloat32_t v1610 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1608), v1810));
    svfloat32_t v1619 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1617), v1810));
    svfloat32_t v1628 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1626), v1810));
    svfloat32_t v1637 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1635), v1810));
    svfloat32_t v1646 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1644), v1810));
    svfloat32_t v1655 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1653), v1810));
    svfloat32_t v1674 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1672), v1810));
    svfloat32_t v1684 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1682), v1810));
    svfloat32_t v1693 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1691), v1810));
    svfloat32_t v1702 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1700), v1810));
    svfloat32_t v1711 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1709), v1810));
    svfloat32_t v1720 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1718), v1810));
    svfloat32_t v1729 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1727), v1810));
    svfloat32_t v1738 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1736), v1810));
    svfloat32_t v1747 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1745), v1810));
    svfloat32_t v1756 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1754), v1810));
    svfloat32_t v1765 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1763), v1810));
    svfloat32_t v1774 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1772), v1810));
    svfloat32_t v1783 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1781), v1810));
    svfloat32_t v1792 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1790), v1810));
    svfloat32_t v1801 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1799), v1810));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v1529, v37, 0),
                     v1529, v37, 90);
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v1538, v51, 0),
                     v1538, v51, 90);
    svfloat32_t zero66 = svdup_n_f32(0);
    svfloat32_t v66 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero66, v1547, v65, 0),
                     v1547, v65, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero101, v1556, v100, 0), v1556,
        v100, 90);
    svfloat32_t zero108 = svdup_n_f32(0);
    svfloat32_t v108 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero108, v1565, v107, 0), v1565,
        v107, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero143, v1574, v142, 0), v1574,
        v142, 90);
    svfloat32_t zero150 = svdup_n_f32(0);
    svfloat32_t v150 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero150, v1583, v149, 0), v1583,
        v149, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero185, v1592, v184, 0), v1592,
        v184, 90);
    svfloat32_t zero192 = svdup_n_f32(0);
    svfloat32_t v192 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero192, v1601, v191, 0), v1601,
        v191, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero206, v1610, v205, 0), v1610,
        v205, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero220, v1619, v219, 0), v1619,
        v219, 90);
    svfloat32_t zero255 = svdup_n_f32(0);
    svfloat32_t v255 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero255, v1628, v254, 0), v1628,
        v254, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero262, v1637, v261, 0), v1637,
        v261, 90);
    svfloat32_t zero276 = svdup_n_f32(0);
    svfloat32_t v276 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero276, v1646, v275, 0), v1646,
        v275, 90);
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero290, v1655, v289, 0), v1655,
        v289, 90);
    svfloat32_t zero332 = svdup_n_f32(0);
    svfloat32_t v332 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero332, v1674, v331, 0), v1674,
        v331, 90);
    svfloat32_t zero346 = svdup_n_f32(0);
    svfloat32_t v346 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero346, v1684, v345, 0), v1684,
        v345, 90);
    svfloat32_t zero360 = svdup_n_f32(0);
    svfloat32_t v360 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero360, v1693, v359, 0), v1693,
        v359, 90);
    svfloat32_t zero395 = svdup_n_f32(0);
    svfloat32_t v395 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero395, v1702, v394, 0), v1702,
        v394, 90);
    svfloat32_t zero402 = svdup_n_f32(0);
    svfloat32_t v402 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero402, v1711, v401, 0), v1711,
        v401, 90);
    svfloat32_t zero437 = svdup_n_f32(0);
    svfloat32_t v437 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero437, v1720, v436, 0), v1720,
        v436, 90);
    svfloat32_t zero444 = svdup_n_f32(0);
    svfloat32_t v444 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero444, v1729, v443, 0), v1729,
        v443, 90);
    svfloat32_t zero479 = svdup_n_f32(0);
    svfloat32_t v479 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero479, v1738, v478, 0), v1738,
        v478, 90);
    svfloat32_t zero486 = svdup_n_f32(0);
    svfloat32_t v486 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero486, v1747, v485, 0), v1747,
        v485, 90);
    svfloat32_t zero500 = svdup_n_f32(0);
    svfloat32_t v500 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero500, v1756, v499, 0), v1756,
        v499, 90);
    svfloat32_t zero514 = svdup_n_f32(0);
    svfloat32_t v514 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero514, v1765, v513, 0), v1765,
        v513, 90);
    svfloat32_t zero549 = svdup_n_f32(0);
    svfloat32_t v549 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero549, v1774, v548, 0), v1774,
        v548, 90);
    svfloat32_t zero556 = svdup_n_f32(0);
    svfloat32_t v556 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero556, v1783, v555, 0), v1783,
        v555, 90);
    svfloat32_t zero591 = svdup_n_f32(0);
    svfloat32_t v591 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero591, v1792, v590, 0), v1792,
        v590, 90);
    svfloat32_t zero598 = svdup_n_f32(0);
    svfloat32_t v598 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero598, v1801, v597, 0), v1801,
        v597, 90);
    svfloat32_t v606 = svadd_f32_x(svptrue_b32(), v1811, v38);
    svfloat32_t v607 = svsub_f32_x(svptrue_b32(), v1811, v38);
    svfloat32_t v608 = svadd_f32_x(svptrue_b32(), v52, v66);
    svfloat32_t v609 = svsub_f32_x(svptrue_b32(), v52, v66);
    svfloat32_t v621 = svadd_f32_x(svptrue_b32(), v101, v108);
    svfloat32_t v622 = svsub_f32_x(svptrue_b32(), v101, v108);
    svfloat32_t v623 = svadd_f32_x(svptrue_b32(), v143, v150);
    svfloat32_t v624 = svsub_f32_x(svptrue_b32(), v143, v150);
    svfloat32_t v677 = svadd_f32_x(svptrue_b32(), v185, v192);
    svfloat32_t v678 = svsub_f32_x(svptrue_b32(), v185, v192);
    svfloat32_t v679 = svadd_f32_x(svptrue_b32(), v206, v220);
    svfloat32_t v680 = svsub_f32_x(svptrue_b32(), v206, v220);
    svfloat32_t v692 = svadd_f32_x(svptrue_b32(), v255, v262);
    svfloat32_t v693 = svsub_f32_x(svptrue_b32(), v255, v262);
    svfloat32_t v694 = svadd_f32_x(svptrue_b32(), v276, v290);
    svfloat32_t v695 = svsub_f32_x(svptrue_b32(), v276, v290);
    svfloat32_t v837 = svadd_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v838 = svsub_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v839 = svadd_f32_x(svptrue_b32(), v346, v360);
    svfloat32_t v840 = svsub_f32_x(svptrue_b32(), v346, v360);
    svfloat32_t v852 = svadd_f32_x(svptrue_b32(), v395, v402);
    svfloat32_t v853 = svsub_f32_x(svptrue_b32(), v395, v402);
    svfloat32_t v854 = svadd_f32_x(svptrue_b32(), v437, v444);
    svfloat32_t v855 = svsub_f32_x(svptrue_b32(), v437, v444);
    svfloat32_t v908 = svadd_f32_x(svptrue_b32(), v479, v486);
    svfloat32_t v909 = svsub_f32_x(svptrue_b32(), v479, v486);
    svfloat32_t v910 = svadd_f32_x(svptrue_b32(), v500, v514);
    svfloat32_t v911 = svsub_f32_x(svptrue_b32(), v500, v514);
    svfloat32_t v923 = svadd_f32_x(svptrue_b32(), v549, v556);
    svfloat32_t v924 = svsub_f32_x(svptrue_b32(), v549, v556);
    svfloat32_t v925 = svadd_f32_x(svptrue_b32(), v591, v598);
    svfloat32_t v926 = svsub_f32_x(svptrue_b32(), v591, v598);
    svfloat32_t zero616 = svdup_n_f32(0);
    svfloat32_t v616 = svcmla_f32_x(pred_full, zero616, v2015, v609, 90);
    svfloat32_t v617 = svadd_f32_x(svptrue_b32(), v606, v608);
    svfloat32_t v618 = svsub_f32_x(svptrue_b32(), v606, v608);
    svfloat32_t v625 = svadd_f32_x(svptrue_b32(), v621, v623);
    svfloat32_t v626 = svsub_f32_x(svptrue_b32(), v621, v623);
    svfloat32_t v642 = svmul_f32_x(svptrue_b32(), v622, v2012);
    svfloat32_t v654 = svmul_f32_x(svptrue_b32(), v624, v2014);
    svfloat32_t zero687 = svdup_n_f32(0);
    svfloat32_t v687 = svcmla_f32_x(pred_full, zero687, v2015, v680, 90);
    svfloat32_t v688 = svadd_f32_x(svptrue_b32(), v677, v679);
    svfloat32_t v689 = svsub_f32_x(svptrue_b32(), v677, v679);
    svfloat32_t zero702 = svdup_n_f32(0);
    svfloat32_t v702 = svcmla_f32_x(pred_full, zero702, v2015, v695, 90);
    svfloat32_t v703 = svadd_f32_x(svptrue_b32(), v692, v694);
    svfloat32_t v704 = svsub_f32_x(svptrue_b32(), v692, v694);
    svfloat32_t zero847 = svdup_n_f32(0);
    svfloat32_t v847 = svcmla_f32_x(pred_full, zero847, v2015, v840, 90);
    svfloat32_t v848 = svadd_f32_x(svptrue_b32(), v837, v839);
    svfloat32_t v849 = svsub_f32_x(svptrue_b32(), v837, v839);
    svfloat32_t v856 = svadd_f32_x(svptrue_b32(), v852, v854);
    svfloat32_t v857 = svsub_f32_x(svptrue_b32(), v852, v854);
    svfloat32_t v873 = svmul_f32_x(svptrue_b32(), v853, v2012);
    svfloat32_t v885 = svmul_f32_x(svptrue_b32(), v855, v2014);
    svfloat32_t zero918 = svdup_n_f32(0);
    svfloat32_t v918 = svcmla_f32_x(pred_full, zero918, v2015, v911, 90);
    svfloat32_t v919 = svadd_f32_x(svptrue_b32(), v908, v910);
    svfloat32_t v920 = svsub_f32_x(svptrue_b32(), v908, v910);
    svfloat32_t v927 = svadd_f32_x(svptrue_b32(), v923, v925);
    svfloat32_t v928 = svsub_f32_x(svptrue_b32(), v923, v925);
    svfloat32_t v944 = svmul_f32_x(svptrue_b32(), v924, v2012);
    svfloat32_t v956 = svmul_f32_x(svptrue_b32(), v926, v2014);
    svfloat32_t v619 = svsub_f32_x(svptrue_b32(), v607, v616);
    svfloat32_t v620 = svadd_f32_x(svptrue_b32(), v607, v616);
    svfloat32_t zero633 = svdup_n_f32(0);
    svfloat32_t v633 = svcmla_f32_x(pred_full, zero633, v2015, v626, 90);
    svfloat32_t v634 = svadd_f32_x(svptrue_b32(), v617, v625);
    svfloat32_t v635 = svsub_f32_x(svptrue_b32(), v617, v625);
    svfloat32_t v690 = svsub_f32_x(svptrue_b32(), v678, v687);
    svfloat32_t v691 = svadd_f32_x(svptrue_b32(), v678, v687);
    svfloat32_t v705 = svsub_f32_x(svptrue_b32(), v693, v702);
    svfloat32_t v706 = svadd_f32_x(svptrue_b32(), v693, v702);
    svfloat32_t v707 = svadd_f32_x(svptrue_b32(), v688, v703);
    svfloat32_t v708 = svsub_f32_x(svptrue_b32(), v688, v703);
    svfloat32_t v763 = svmul_f32_x(svptrue_b32(), v689, v2012);
    svfloat32_t v775 = svmul_f32_x(svptrue_b32(), v704, v2014);
    svfloat32_t v850 = svsub_f32_x(svptrue_b32(), v838, v847);
    svfloat32_t v851 = svadd_f32_x(svptrue_b32(), v838, v847);
    svfloat32_t zero864 = svdup_n_f32(0);
    svfloat32_t v864 = svcmla_f32_x(pred_full, zero864, v2015, v857, 90);
    svfloat32_t v865 = svadd_f32_x(svptrue_b32(), v848, v856);
    svfloat32_t v866 = svsub_f32_x(svptrue_b32(), v848, v856);
    svfloat32_t v921 = svsub_f32_x(svptrue_b32(), v909, v918);
    svfloat32_t v922 = svadd_f32_x(svptrue_b32(), v909, v918);
    svfloat32_t zero935 = svdup_n_f32(0);
    svfloat32_t v935 = svcmla_f32_x(pred_full, zero935, v2015, v928, 90);
    svfloat32_t v936 = svadd_f32_x(svptrue_b32(), v919, v927);
    svfloat32_t v937 = svsub_f32_x(svptrue_b32(), v919, v927);
    svfloat32_t v636 = svsub_f32_x(svptrue_b32(), v618, v633);
    svfloat32_t v637 = svadd_f32_x(svptrue_b32(), v618, v633);
    svfloat32_t v662 = svcmla_f32_x(pred_full, v642, v2139, v642, 90);
    svfloat32_t v663 = svcmla_f32_x(pred_full, v654, v2015, v654, 90);
    svfloat32_t zero715 = svdup_n_f32(0);
    svfloat32_t v715 = svcmla_f32_x(pred_full, zero715, v2015, v708, 90);
    svfloat32_t v716 = svadd_f32_x(svptrue_b32(), v634, v707);
    svfloat32_t v717 = svsub_f32_x(svptrue_b32(), v634, v707);
    svfloat32_t v724 = svmul_f32_x(svptrue_b32(), v690, v1930);
    svfloat32_t v736 = svmul_f32_x(svptrue_b32(), v705, v2094);
    svfloat32_t v802 = svmul_f32_x(svptrue_b32(), v691, v2094);
    svfloat32_t v814 = svmul_f32_x(svptrue_b32(), v706, v2096);
    svfloat32_t v867 = svsub_f32_x(svptrue_b32(), v849, v864);
    svfloat32_t v868 = svadd_f32_x(svptrue_b32(), v849, v864);
    svfloat32_t v893 = svcmla_f32_x(pred_full, v873, v2139, v873, 90);
    svfloat32_t v894 = svcmla_f32_x(pred_full, v885, v2015, v885, 90);
    svfloat32_t v938 = svsub_f32_x(svptrue_b32(), v920, v935);
    svfloat32_t v939 = svadd_f32_x(svptrue_b32(), v920, v935);
    svfloat32_t v964 = svcmla_f32_x(pred_full, v944, v2139, v944, 90);
    svfloat32_t v965 = svcmla_f32_x(pred_full, v956, v2015, v956, 90);
    svfloat32_t v979 = svadd_f32_x(svptrue_b32(), v865, v936);
    svfloat32_t v980 = svsub_f32_x(svptrue_b32(), v865, v936);
    svfloat32_t v1241 = svmul_f32_x(svptrue_b32(), v866, v2012);
    svfloat32_t v1253 = svmul_f32_x(svptrue_b32(), v937, v2014);
    svfloat32_t v664 = svadd_f32_x(svptrue_b32(), v662, v663);
    svfloat32_t v665 = svsub_f32_x(svptrue_b32(), v663, v662);
    svfloat32_t v718 = svsub_f32_x(svptrue_b32(), v635, v715);
    svfloat32_t v719 = svadd_f32_x(svptrue_b32(), v635, v715);
    svfloat32_t v744 = svcmla_f32_x(pred_full, v724, v1931, v690, 90);
    svfloat32_t v745 = svcmla_f32_x(pred_full, v736, v2095, v705, 90);
    svfloat32_t v783 = svcmla_f32_x(pred_full, v763, v2139, v763, 90);
    svfloat32_t v784 = svcmla_f32_x(pred_full, v775, v2015, v775, 90);
    svfloat32_t v822 = svcmla_f32_x(pred_full, v802, v2095, v691, 90);
    svfloat32_t v823 = svcmla_f32_x(pred_full, v814, v2097, v706, 90);
    svfloat32_t v895 = svadd_f32_x(svptrue_b32(), v893, v894);
    svfloat32_t v896 = svsub_f32_x(svptrue_b32(), v894, v893);
    svfloat32_t v966 = svadd_f32_x(svptrue_b32(), v964, v965);
    svfloat32_t v967 = svsub_f32_x(svptrue_b32(), v965, v964);
    svfloat32_t zero987 = svdup_n_f32(0);
    svfloat32_t v987 = svcmla_f32_x(pred_full, zero987, v2015, v980, 90);
    svfloat32_t v988 = svadd_f32_x(svptrue_b32(), v716, v979);
    svfloat32_t v989 = svsub_f32_x(svptrue_b32(), v716, v979);
    svfloat32_t v1099 = svmul_f32_x(svptrue_b32(), v867, v1930);
    svfloat32_t v1111 = svmul_f32_x(svptrue_b32(), v938, v2094);
    svfloat32_t v1383 = svmul_f32_x(svptrue_b32(), v868, v2094);
    svfloat32_t v1395 = svmul_f32_x(svptrue_b32(), v939, v2096);
    svfloat32_t zero672 = svdup_n_f32(0);
    svfloat32_t v672 = svcmla_f32_x(pred_full, zero672, v2139, v665, 90);
    svfloat32_t v673 = svadd_f32_x(svptrue_b32(), v619, v664);
    svfloat32_t v674 = svsub_f32_x(svptrue_b32(), v619, v664);
    svfloat32_t v746 = svadd_f32_x(svptrue_b32(), v744, v745);
    svfloat32_t v747 = svsub_f32_x(svptrue_b32(), v745, v744);
    svfloat32_t v785 = svadd_f32_x(svptrue_b32(), v783, v784);
    svfloat32_t v786 = svsub_f32_x(svptrue_b32(), v784, v783);
    svfloat32_t v824 = svadd_f32_x(svptrue_b32(), v822, v823);
    svfloat32_t v825 = svsub_f32_x(svptrue_b32(), v823, v822);
    svfloat32_t zero903 = svdup_n_f32(0);
    svfloat32_t v903 = svcmla_f32_x(pred_full, zero903, v2139, v896, 90);
    svfloat32_t v904 = svadd_f32_x(svptrue_b32(), v850, v895);
    svfloat32_t v905 = svsub_f32_x(svptrue_b32(), v850, v895);
    svfloat32_t zero974 = svdup_n_f32(0);
    svfloat32_t v974 = svcmla_f32_x(pred_full, zero974, v2139, v967, 90);
    svfloat32_t v975 = svadd_f32_x(svptrue_b32(), v921, v966);
    svfloat32_t v976 = svsub_f32_x(svptrue_b32(), v921, v966);
    svfloat32_t v990 = svsub_f32_x(svptrue_b32(), v717, v987);
    svfloat32_t v991 = svadd_f32_x(svptrue_b32(), v717, v987);
    svint16_t v994 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v988, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1010 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v989, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v1119 = svcmla_f32_x(pred_full, v1099, v1931, v867, 90);
    svfloat32_t v1120 = svcmla_f32_x(pred_full, v1111, v2095, v938, 90);
    svfloat32_t v1261 = svcmla_f32_x(pred_full, v1241, v2139, v1241, 90);
    svfloat32_t v1262 = svcmla_f32_x(pred_full, v1253, v2015, v1253, 90);
    svfloat32_t v1403 = svcmla_f32_x(pred_full, v1383, v2095, v868, 90);
    svfloat32_t v1404 = svcmla_f32_x(pred_full, v1395, v2097, v939, 90);
    svfloat32_t v675 = svsub_f32_x(svptrue_b32(), v620, v672);
    svfloat32_t v676 = svadd_f32_x(svptrue_b32(), v620, v672);
    svfloat32_t zero754 = svdup_n_f32(0);
    svfloat32_t v754 = svcmla_f32_x(pred_full, zero754, v2139, v747, 90);
    svfloat32_t v755 = svadd_f32_x(svptrue_b32(), v673, v746);
    svfloat32_t v756 = svsub_f32_x(svptrue_b32(), v673, v746);
    svfloat32_t zero793 = svdup_n_f32(0);
    svfloat32_t v793 = svcmla_f32_x(pred_full, zero793, v2139, v786, 90);
    svfloat32_t v794 = svadd_f32_x(svptrue_b32(), v636, v785);
    svfloat32_t v795 = svsub_f32_x(svptrue_b32(), v636, v785);
    svfloat32_t zero832 = svdup_n_f32(0);
    svfloat32_t v832 = svcmla_f32_x(pred_full, zero832, v2139, v825, 90);
    svfloat32_t v906 = svsub_f32_x(svptrue_b32(), v851, v903);
    svfloat32_t v907 = svadd_f32_x(svptrue_b32(), v851, v903);
    svfloat32_t v977 = svsub_f32_x(svptrue_b32(), v922, v974);
    svfloat32_t v978 = svadd_f32_x(svptrue_b32(), v922, v974);
    svint16_t v1002 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v990, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1018 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v991, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v1028 = svmul_f32_x(svptrue_b32(), v904, v1889);
    svfloat32_t v1040 = svmul_f32_x(svptrue_b32(), v975, v1971);
    svfloat32_t v1121 = svadd_f32_x(svptrue_b32(), v1119, v1120);
    svfloat32_t v1122 = svsub_f32_x(svptrue_b32(), v1120, v1119);
    svfloat32_t v1263 = svadd_f32_x(svptrue_b32(), v1261, v1262);
    svfloat32_t v1264 = svsub_f32_x(svptrue_b32(), v1262, v1261);
    svfloat32_t v1312 = svmul_f32_x(svptrue_b32(), v905, v2053);
    svfloat32_t v1324 = svmul_f32_x(svptrue_b32(), v976, v2055);
    svfloat32_t v1405 = svadd_f32_x(svptrue_b32(), v1403, v1404);
    svfloat32_t v1406 = svsub_f32_x(svptrue_b32(), v1404, v1403);
    svst1w_u64(pred_full, (unsigned *)(v1859), svreinterpret_u64_s16(v994));
    svst1w_u64(pred_full, (unsigned *)(v1877), svreinterpret_u64_s16(v1010));
    svfloat32_t v757 = svsub_f32_x(svptrue_b32(), v674, v754);
    svfloat32_t v758 = svadd_f32_x(svptrue_b32(), v674, v754);
    svfloat32_t v796 = svsub_f32_x(svptrue_b32(), v637, v793);
    svfloat32_t v797 = svadd_f32_x(svptrue_b32(), v637, v793);
    svfloat32_t v833 = svadd_f32_x(svptrue_b32(), v675, v824);
    svfloat32_t v834 = svsub_f32_x(svptrue_b32(), v675, v824);
    svfloat32_t v835 = svsub_f32_x(svptrue_b32(), v676, v832);
    svfloat32_t v836 = svadd_f32_x(svptrue_b32(), v676, v832);
    svfloat32_t v1048 = svcmla_f32_x(pred_full, v1028, v2056, v904, 90);
    svfloat32_t v1049 = svcmla_f32_x(pred_full, v1040, v1972, v975, 90);
    svfloat32_t zero1129 = svdup_n_f32(0);
    svfloat32_t v1129 = svcmla_f32_x(pred_full, zero1129, v2139, v1122, 90);
    svfloat32_t v1130 = svadd_f32_x(svptrue_b32(), v794, v1121);
    svfloat32_t v1131 = svsub_f32_x(svptrue_b32(), v794, v1121);
    svfloat32_t v1170 = svmul_f32_x(svptrue_b32(), v906, v1971);
    svfloat32_t v1182 = svmul_f32_x(svptrue_b32(), v977, v1973);
    svfloat32_t zero1271 = svdup_n_f32(0);
    svfloat32_t v1271 = svcmla_f32_x(pred_full, zero1271, v2139, v1264, 90);
    svfloat32_t v1272 = svadd_f32_x(svptrue_b32(), v718, v1263);
    svfloat32_t v1273 = svsub_f32_x(svptrue_b32(), v718, v1263);
    svfloat32_t v1332 = svcmla_f32_x(pred_full, v1312, v2054, v905, 90);
    svfloat32_t v1333 = svcmla_f32_x(pred_full, v1324, v2056, v976, 90);
    svfloat32_t zero1413 = svdup_n_f32(0);
    svfloat32_t v1413 = svcmla_f32_x(pred_full, zero1413, v2139, v1406, 90);
    svfloat32_t v1454 = svmul_f32_x(svptrue_b32(), v907, v2135);
    svfloat32_t v1466 = svmul_f32_x(svptrue_b32(), v978, v2137);
    svst1w_u64(pred_full, (unsigned *)(v1868), svreinterpret_u64_s16(v1002));
    svst1w_u64(pred_full, (unsigned *)(v1886), svreinterpret_u64_s16(v1018));
    svfloat32_t v1050 = svadd_f32_x(svptrue_b32(), v1048, v1049);
    svfloat32_t v1051 = svsub_f32_x(svptrue_b32(), v1049, v1048);
    svfloat32_t v1132 = svsub_f32_x(svptrue_b32(), v795, v1129);
    svfloat32_t v1133 = svadd_f32_x(svptrue_b32(), v795, v1129);
    svint16_t v1136 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1130, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1152 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1131, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1190 = svcmla_f32_x(pred_full, v1170, v1972, v906, 90);
    svfloat32_t v1191 = svcmla_f32_x(pred_full, v1182, v2136, v977, 90);
    svfloat32_t v1274 = svsub_f32_x(svptrue_b32(), v719, v1271);
    svfloat32_t v1275 = svadd_f32_x(svptrue_b32(), v719, v1271);
    svint16_t v1278 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1272, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1294 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1273, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1334 = svadd_f32_x(svptrue_b32(), v1332, v1333);
    svfloat32_t v1335 = svsub_f32_x(svptrue_b32(), v1333, v1332);
    svfloat32_t v1414 = svadd_f32_x(svptrue_b32(), v796, v1405);
    svfloat32_t v1415 = svsub_f32_x(svptrue_b32(), v796, v1405);
    svfloat32_t v1416 = svsub_f32_x(svptrue_b32(), v797, v1413);
    svfloat32_t v1417 = svadd_f32_x(svptrue_b32(), v797, v1413);
    svfloat32_t v1474 = svcmla_f32_x(pred_full, v1454, v2136, v907, 90);
    svfloat32_t v1475 = svcmla_f32_x(pred_full, v1466, v2138, v978, 90);
    svfloat32_t zero1058 = svdup_n_f32(0);
    svfloat32_t v1058 = svcmla_f32_x(pred_full, zero1058, v2139, v1051, 90);
    svfloat32_t v1059 = svadd_f32_x(svptrue_b32(), v755, v1050);
    svfloat32_t v1060 = svsub_f32_x(svptrue_b32(), v755, v1050);
    svint16_t v1144 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1132, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1160 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1133, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1192 = svadd_f32_x(svptrue_b32(), v1190, v1191);
    svfloat32_t v1193 = svsub_f32_x(svptrue_b32(), v1191, v1190);
    svint16_t v1286 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1274, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1302 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1275, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t zero1342 = svdup_n_f32(0);
    svfloat32_t v1342 = svcmla_f32_x(pred_full, zero1342, v2139, v1335, 90);
    svfloat32_t v1343 = svadd_f32_x(svptrue_b32(), v757, v1334);
    svfloat32_t v1344 = svsub_f32_x(svptrue_b32(), v757, v1334);
    svint16_t v1420 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1414, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1428 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1416, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1436 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1415, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1444 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1417, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1476 = svadd_f32_x(svptrue_b32(), v1474, v1475);
    svfloat32_t v1477 = svsub_f32_x(svptrue_b32(), v1475, v1474);
    svst1w_u64(pred_full, (unsigned *)(v1941), svreinterpret_u64_s16(v1136));
    svst1w_u64(pred_full, (unsigned *)(v1959), svreinterpret_u64_s16(v1152));
    svst1w_u64(pred_full, (unsigned *)(v2023), svreinterpret_u64_s16(v1278));
    svst1w_u64(pred_full, (unsigned *)(v2041), svreinterpret_u64_s16(v1294));
    svfloat32_t v1061 = svsub_f32_x(svptrue_b32(), v756, v1058);
    svfloat32_t v1062 = svadd_f32_x(svptrue_b32(), v756, v1058);
    svint16_t v1065 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1059, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1081 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1060, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t zero1200 = svdup_n_f32(0);
    svfloat32_t v1200 = svcmla_f32_x(pred_full, zero1200, v2139, v1193, 90);
    svfloat32_t v1201 = svadd_f32_x(svptrue_b32(), v833, v1192);
    svfloat32_t v1202 = svsub_f32_x(svptrue_b32(), v833, v1192);
    svfloat32_t v1345 = svsub_f32_x(svptrue_b32(), v758, v1342);
    svfloat32_t v1346 = svadd_f32_x(svptrue_b32(), v758, v1342);
    svint16_t v1349 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1343, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1365 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1344, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t zero1484 = svdup_n_f32(0);
    svfloat32_t v1484 = svcmla_f32_x(pred_full, zero1484, v2139, v1477, 90);
    svfloat32_t v1485 = svadd_f32_x(svptrue_b32(), v835, v1476);
    svfloat32_t v1486 = svsub_f32_x(svptrue_b32(), v835, v1476);
    svst1w_u64(pred_full, (unsigned *)(v1950), svreinterpret_u64_s16(v1144));
    svst1w_u64(pred_full, (unsigned *)(v1968), svreinterpret_u64_s16(v1160));
    svst1w_u64(pred_full, (unsigned *)(v2032), svreinterpret_u64_s16(v1286));
    svst1w_u64(pred_full, (unsigned *)(v2050), svreinterpret_u64_s16(v1302));
    svst1w_u64(pred_full, (unsigned *)(v2105), svreinterpret_u64_s16(v1420));
    svst1w_u64(pred_full, (unsigned *)(v2114), svreinterpret_u64_s16(v1428));
    svst1w_u64(pred_full, (unsigned *)(v2123), svreinterpret_u64_s16(v1436));
    svst1w_u64(pred_full, (unsigned *)(v2132), svreinterpret_u64_s16(v1444));
    svint16_t v1073 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1061, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1089 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1062, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1203 = svsub_f32_x(svptrue_b32(), v834, v1200);
    svfloat32_t v1204 = svadd_f32_x(svptrue_b32(), v834, v1200);
    svint16_t v1207 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1201, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1223 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1202, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1357 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1345, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1373 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1346, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1487 = svsub_f32_x(svptrue_b32(), v836, v1484);
    svfloat32_t v1488 = svadd_f32_x(svptrue_b32(), v836, v1484);
    svint16_t v1491 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1485, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1507 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1486, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1900), svreinterpret_u64_s16(v1065));
    svst1w_u64(pred_full, (unsigned *)(v1918), svreinterpret_u64_s16(v1081));
    svst1w_u64(pred_full, (unsigned *)(v2064), svreinterpret_u64_s16(v1349));
    svst1w_u64(pred_full, (unsigned *)(v2082), svreinterpret_u64_s16(v1365));
    svint16_t v1215 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1203, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1231 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1204, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1499 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1487, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1515 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1488, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1909), svreinterpret_u64_s16(v1073));
    svst1w_u64(pred_full, (unsigned *)(v1927), svreinterpret_u64_s16(v1089));
    svst1w_u64(pred_full, (unsigned *)(v1982), svreinterpret_u64_s16(v1207));
    svst1w_u64(pred_full, (unsigned *)(v2000), svreinterpret_u64_s16(v1223));
    svst1w_u64(pred_full, (unsigned *)(v2073), svreinterpret_u64_s16(v1357));
    svst1w_u64(pred_full, (unsigned *)(v2091), svreinterpret_u64_s16(v1373));
    svst1w_u64(pred_full, (unsigned *)(v2146), svreinterpret_u64_s16(v1491));
    svst1w_u64(pred_full, (unsigned *)(v2164), svreinterpret_u64_s16(v1507));
    svst1w_u64(pred_full, (unsigned *)(v1991), svreinterpret_u64_s16(v1215));
    svst1w_u64(pred_full, (unsigned *)(v2009), svreinterpret_u64_s16(v1231));
    svst1w_u64(pred_full, (unsigned *)(v2155), svreinterpret_u64_s16(v1499));
    svst1w_u64(pred_full, (unsigned *)(v2173), svreinterpret_u64_s16(v1515));
    v5 += v11;
    v6 += v12;
  }
}
#endif
