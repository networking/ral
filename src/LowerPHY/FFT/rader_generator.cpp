/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "rader_generator.hpp"

#include <algorithm>
#include <cstdint>

int armral::fft::find_group_generator(int n) {
  // just go through in order until we find one
  uint8_t seen[n];
  for (int cand = 2; cand < n; ++cand) {
    std::fill(seen, &seen[n], 0);
    bool success = true;
    for (int x = 1, i = 1; i < n; ++i, x = (x * cand) % n) {
      if (seen[x] != 0U) {
        success = false;
        break;
      }
      seen[x] = 1;
    }
    if (success) {
      return cand;
    }
  }
  return 0;
}

int armral::fft::find_inverse_mod_n(int gen, int n) {
  int s = 0;
  int r = n;
  int old_s = 1;
  int old_r = gen;

  while (r > 0) {
    int quotient = old_r / r;
    int new_r = old_r - quotient * r;
    int new_s = old_s - quotient * s;
    old_r = r;
    old_s = s;
    r = new_r;
    s = new_s;
  }

  // This is just mod, but we need a correct value for negative old_s
  return (old_s % n + n) % n;
}
