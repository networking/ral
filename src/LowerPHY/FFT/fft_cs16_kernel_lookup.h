/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "fft_cf32_cf32_cs16_ab_t_gu.h"
#include "fft_cf32_cf32_cs16_ac_n_uu.h"
#include "fft_cs16_cf32_cf32_ac_n_uu.h"
#include "fft_cs16_cf32_cs16_ac_n_uun.h"

#ifdef __cplusplus
extern "C" {
#endif

cs16_cf32_cs16_ac_n_uun_fft_t *
lookup_ac_uun_base_kernel_cs16_cs16(int n, armral_fft_direction_t dir);

cs16_cf32_cf32_ac_n_uu_fft_t *
lookup_ac_uu_base_kernel_cs16_cf32(int n, armral_fft_direction_t dir);

cf32_cf32_cs16_ab_t_gu_fft_t *
lookup_ab_twiddle_gu_base_kernel_cf32_cs16(int n, armral_fft_direction_t dir);

cf32_cf32_cs16_ac_n_uu_fft_t *
lookup_ac_uu_base_kernel_cf32_cs16(int n, armral_fft_direction_t dir);

#ifdef __cplusplus
} // extern "C"
#endif