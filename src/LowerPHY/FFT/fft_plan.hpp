/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "fft_level.hpp"

namespace armral::fft {
/**
 * Creates a plan for solving FFTs. Depending on the data type, the
 * plan will execute different functions.
 * @tparam      Tx              Input data type
 * @tparam      Ty              Output data type
 * @tparam      Tw              Working data type
 * @param [out] p               Pointer to populate with the created FFT plan.
 * @param [in]  n               The overall size of the FFT to perform.
 * @param [in]  dir             The direction of the FFT (forwards or
 *                              backwards).
 * @param [in]  allow_raders    Allow use of Rader's algorithm.
 * @param [in]  use_all_kernels Allow use of all available kernels. Default is
 *                              false.
 * @returns ARMRAL_SUCCESS if a plan is successfully created.
 */
template<typename Tx, typename Ty, typename Tw>
armral_status create_plan(armral_fft_plan_t **p, int n,
                          armral_fft_direction_t dir, bool allow_raders,
                          bool use_all_kernels = false, bool want_uun = true);

/**
 * Common code for destroying a plan. For the time being, the plan is identical
 * for 32-bit float and 16-bit Q15 kernels. Memory assigned in the call to
 * armral_fft_create_plan() is unassigned in this function.
 * @param [inout] p  The plan to unassign memory from. On output, \c *p is
 *                   a NULL pointer.
 */
armral_status destroy_plan(armral_fft_plan_t **p);

// Forward declaration of lev_base_t because of circular dependence
// plan has levels
// levels has rader
// rader has plans
struct lev_base_t;
} // namespace armral::fft

/**
 * Structure encapsulating a series of steps to solve an FFT of a
 * particular length. This must be built ahead of execution since the
 * process of building the plan is potentially time consuming (in
 * particular, constructing twiddle factors).
 */
struct armral_fft_plan_t {
  static constexpr int max_levels = 5;
  /// The problem size being solved.
  int n;
  /// The direction of the problem being solved.
  armral_fft_direction_t dir;
  /// The number of composite factors involved in the solve.
  int num_levels;
  /// Information required to solve the FFT at each level
  armral::fft::lev_base_t *levels[max_levels];
};
