/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "fft_execute.hpp"
#include "fft_plan.hpp"

namespace {

template<typename Tx, typename Ty, typename Tw>
inline void execute_single_level(const armral_fft_plan_t *p, const Tx *x, Ty *y,
                                 int istride, int ostride, int howmany,
                                 int idist, int odist) {
  auto *lev = static_cast<armral::fft::lev_t<Tx, Ty, Tw> *>(p->levels[0]);
  assert(lev->how_many == 1);
  assert(lev->n2 == 1);
  if (lev->r) {
    armral::fft::execute_rader<Tx, Ty, Tw>(lev->r, x, y, istride, ostride,
                                           nullptr, howmany, idist, odist);
  } else if (lev->bs) {
    armral::fft::execute_bluestein<Tx, Ty, Tw>(lev->bs, x, y, istride, ostride,
                                               nullptr, howmany, idist, odist);
  } else {
    assert(lev->kernel);
    lev->kernel(x, y, istride, ostride, howmany, p->dir);
  }
}

template<typename Tx, typename Ty, typename Tw>
inline void execute_dit(const armral::fft::lev_base_t *lev, const Tx *x, Ty *y,
                        int istride, int ostride) {
  // the non-twiddle case does not need to permute the array, hence
  // the strides are identical.
  auto level = static_cast<const armral::fft::lev_t<Tx, Ty, Tw> *>(lev);
  int n2_ostride = level->how_many * ostride;
  int n2_istride = level->how_many * istride;
  int n1_ostride = level->how_many * level->n2 * ostride;
  int n1_istride = level->how_many * level->n2 * istride;
  assert(level->kernel || level->r || level->bs);
  if (level->kernel) {
    assert(n2_ostride == level->how_many);
    if (n2_istride == level->how_many) {
      level->kernel(x, y, n1_istride, n1_ostride, level->n2 * level->how_many,
                    lev->dir);
    } else {
      if constexpr (std::is_same_v<Tx, Ty> && std::is_same_v<Ty, Tw>) {
        assert(level->ac_gu_kernel);
        // TODO_KB: I think that this is right, in terms of the idist =
        // n2_istride. May need to tweak this a little
        level->ac_gu_kernel(x, y, n1_istride, n1_ostride,
                            level->n2 * level->how_many, istride, lev->dir);
      } else {
        // We should not be able to get into this branch, as we expect input,
        // output and working type to be the same
        assert(false);
      }
    }
  } else {
    // Rader's or Bluestein's
    for (int hm = 0; hm != level->how_many; ++hm) {
      const Tx *x_ptr = &x[hm];
      Ty *y_ptr = &y[hm];
      if (level->r) {
        armral::fft::execute_rader<Tx, Ty, Tw>(
            level->r, x_ptr, y_ptr, n1_istride, n1_ostride, nullptr, level->n2,
            n2_istride, n2_ostride);
      } else {
        armral::fft::execute_bluestein<Tx, Ty, Tw>(
            level->bs, x_ptr, y_ptr, n1_istride, n1_ostride, nullptr, level->n2,
            n2_istride, n2_ostride);
      }
    }
  }
}

template<typename Tx, typename Ty, typename Tw>
inline void execute_dit_ab_twid(const armral::fft::lev_base_t *lev, const Tx *x,
                                Ty *y, int istride, int ostride) {
  // twiddle cases need to additionally do a permutation, hence why
  // the stride pairs are different.
  auto level = static_cast<const armral::fft::lev_t<Tx, Ty, Tw> *>(lev);
  int n2_ostride = level->how_many * ostride;
  int n2_istride = level->how_many * level->n1 * istride;
  int n1_ostride = level->how_many * level->n2 * ostride;
  int n1_istride = level->how_many * istride;
  assert(level->kernel || level->r || level->bs);
  if (level->kernel) {
    assert(level->ab_twid_gu_kernel);
    for (int hm = 0; hm != level->how_many; ++hm) {
      const Tx *x_ptr = &x[hm];
      Ty *y_ptr = &y[hm];
      level->kernel(x_ptr, y_ptr, n1_istride, n1_ostride, 1, lev->dir);
      if (n2_ostride == 1) {
        level->ab_twid_gu_kernel(x_ptr + n2_istride, y_ptr + n2_ostride,
                                 n1_istride, n1_ostride, level->twids,
                                 level->n2 - 1, n2_istride, lev->dir);
      } else {
        level->ab_twid_gs_kernel(
            x_ptr + n2_istride, y_ptr + n2_ostride, n1_istride, n1_ostride,
            level->twids, level->n2 - 1, n2_istride, n2_ostride, lev->dir);
      }
    }
  } else {
    for (int hm = 0; hm < level->how_many; ++hm) {
      const Tx *x_ptr = &x[hm];
      Ty *y_ptr = &y[hm];
      if (level->r) {
        armral::fft::execute_rader<Tx, Ty, Tw>(
            level->r, x_ptr, y_ptr, n1_istride, n1_ostride, level->twids,
            level->n2, n2_istride, n2_ostride);
      } else {
        armral::fft::execute_bluestein<Tx, Ty, Tw>(
            level->bs, x_ptr, y_ptr, n1_istride, n1_ostride, level->twids,
            level->n2, n2_istride, n2_ostride);
      }
    }
  }
}

template<typename Tx, typename Ty, typename Tw>
inline void execute_dit_ac_twid(const armral::fft::lev_base_t *lev, const Tx *x,
                                Ty *y, int istride, int ostride) {
  // twiddle cases need to additionally do a permutation, hence why
  // the stride pairs are different.
  auto level = static_cast<const armral::fft::lev_t<Tx, Ty, Tw> *>(lev);
  int n2_ostride = level->how_many * ostride;
  int n2_istride = level->how_many * level->n1 * istride;
  int n1_ostride = level->how_many * level->n2 * ostride;
  int n1_istride = level->how_many * istride;
  assert(level->kernel || level->r || level->bs);
  if (level->kernel) {
    assert(level->ac_twid_kernel);
    level->kernel(x, y, n1_istride, n1_ostride, level->how_many, lev->dir);
    for (int i = 1; i < level->n2; ++i) {
      const Tx *x_ptr = &x[i * n2_istride];
      Ty *y_ptr = &y[i * n2_ostride];
#ifdef ARMRAL_ARCH_SVE
      bool have_premul_twiddles = false;
#else
      bool have_premul_twiddles = true;
#endif
      int ofs_mul = have_premul_twiddles ? 2 : 1;
      auto *twids = &level->twids[(i - 1) * ofs_mul * (level->n1 - 1)];
      level->ac_twid_kernel(x_ptr, y_ptr, n1_istride, n1_ostride, twids,
                            level->how_many, lev->dir);
    }
  } else {
    for (int hm = 0; hm < level->how_many; ++hm) {
      const Tx *x_ptr = &x[hm];
      Ty *y_ptr = &y[hm];
      if (level->r) {
        armral::fft::execute_rader<Tx, Ty, Tw>(
            level->r, x_ptr, y_ptr, n1_istride, n1_ostride, level->twids,
            level->n2, n2_istride, n2_ostride);
      } else {
        armral::fft::execute_bluestein<Tx, Ty, Tw>(
            level->bs, x_ptr, y_ptr, n1_istride, n1_ostride, level->twids,
            level->n2, n2_istride, n2_ostride);
      }
    }
  }
}

} // anonymous namespace

namespace armral::fft {

template<typename Tx, typename Ty, typename Tw>
armral_status execute(const armral_fft_plan_t *p, const Tx *x, Ty *y,
                      int istride, int ostride, int howmany) {
  static_assert(sizeof(Tw) >= sizeof(Tx) && sizeof(Tw) >= sizeof(Ty));
  if (p == nullptr) {
    assert(false && "Plan is invalid");
    return ARMRAL_ARGUMENT_ERROR;
  }
  int num_levels = p->num_levels;
  const auto *levs = p->levels;

  // shortcut if we only have one level, no need to use temporary buffers etc.
  // We assume that howmany are unit stride apart in both the input and output
  if (num_levels == 1) {
    execute_single_level<Tx, Ty, Tw>(p, x, y, istride, ostride, howmany, 1, 1);
    return ARMRAL_SUCCESS;
  }

  // allocate temporary buffers to store the intermediate data.
  Tw tmp1[p->n];
  Tw tmp2[p->n];

  // actually do the computation!
  for (int h = 0; h < howmany; ++h) {
    execute_dit<Tx, Tw, Tw>(levs[0], &x[h], tmp1, istride, 1); // x -> tmp
    for (int i = 1; i < num_levels - 1; ++i) {
      Tw *t1 = i % 2 == 0 ? tmp2 : tmp1;
      Tw *t2 = i % 2 == 0 ? tmp1 : tmp2;
      // tmp(one) -> tmp(other)
      execute_dit_ac_twid<Tw, Tw, Tw>(levs[i], t1, t2, 1, 1);
    }
    auto *t1 = num_levels % 2 == 0 ? tmp1 : tmp2;
    // tmp -> y
    execute_dit_ab_twid<Tw, Ty, Tw>(levs[num_levels - 1], t1, &y[h], 1,
                                    ostride);
  }
  return ARMRAL_SUCCESS;
}

template armral_status
execute<armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t>(
    const armral_fft_plan_t *p, const armral_cmplx_f32_t *x,
    armral_cmplx_f32_t *y, int istride, int ostride, int howmany);

template armral_status
execute<armral_cmplx_int16_t, armral_cmplx_int16_t, armral_cmplx_f32_t>(
    const armral_fft_plan_t *p, const armral_cmplx_int16_t *x,
    armral_cmplx_int16_t *y, int istride, int ostride, int howmany);

} // namespace armral::fft
