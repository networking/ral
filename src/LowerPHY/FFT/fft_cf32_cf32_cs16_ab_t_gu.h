/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "fft_helper.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void(cf32_cf32_cs16_ab_t_gu_fft_t)(const armral_cmplx_f32_t *x,
                                           armral_cmplx_int16_t *y, int istride,
                                           int ostride,
                                           const armral_cmplx_f32_t *w,
                                           int howmany, int idist, float dir);

cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu2;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu3;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu4;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu5;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu6;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu7;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu8;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu9;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu10;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu11;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu12;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu13;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu14;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu15;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu16;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu17;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu18;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu19;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu20;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu21;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu22;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu24;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu25;
cf32_cf32_cs16_ab_t_gu_fft_t armral_fft_cf32_cf32_cs16_ab_t_gu32;

#ifdef __cplusplus
} // extern "C"
#endif