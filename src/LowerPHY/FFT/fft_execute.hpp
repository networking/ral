/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"

namespace armral::fft {

template<typename Tx, typename Ty, typename Tw>
armral_status execute(const armral_fft_plan_t *p, const Tx *x, Ty *y,
                      int istride, int ostride, int howmany);

} // namespace armral::fft
