/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "fft_helper.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void(cf32_cf32_cf32_ac_n_gu_fft_t)(const armral_cmplx_f32_t *x,
                                           armral_cmplx_f32_t *y, int istride,
                                           int ostride, int howmany, int idist,
                                           float dir);

cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu13;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu14;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu16;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu17;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu18;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu19;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu20;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu21;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu22;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu24;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu25;
cf32_cf32_cf32_ac_n_gu_fft_t armral_fft_cf32_cf32_cf32_ac_n_gu32;

#ifdef __cplusplus
} // extern "C"
#endif