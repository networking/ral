/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"

#include <algorithm>
#include <limits>
#include <type_traits>

namespace armral::fft {

template<typename T>
struct is_armral_cmplx : std::false_type {};

template<typename T>
constexpr bool is_armral_cmplx_v = is_armral_cmplx<T>::value;

template<>
struct is_armral_cmplx<armral_cmplx_int16_t> : std::true_type {};

template<>
struct is_armral_cmplx<armral_cmplx_f32_t> : std::true_type {};

template<typename T>
struct real_type;

template<>
struct real_type<armral_cmplx_f32_t> {
  using type = float;
};

template<>
struct real_type<armral_cmplx_int16_t> {
  using type = int16_t;
};

template<typename T>
using real_t = typename real_type<T>::type;

template<typename Tx, typename Ty>
inline Tx cast(Ty x) {
  return x;
}

inline int16_t clamp_to_int16(float x) {
  return std::clamp(x, (float)std::numeric_limits<int16_t>::min(),
                    (float)std::numeric_limits<int16_t>::max());
}

template<>
inline armral_cmplx_f32_t cast<armral_cmplx_f32_t>(armral_cmplx_int16_t x) {
  return {(float)x.re / (1 << 15), (float)x.im / (1 << 15)};
}

template<>
inline armral_cmplx_int16_t cast<armral_cmplx_int16_t>(armral_cmplx_f32_t x) {
  return {clamp_to_int16(x.re * (1 << 15)), clamp_to_int16(x.im * (1 << 15))};
}

/**
 * Signature of an FFT function which does not use twiddle factors.
 *
 * No idist/odist parameters since we assume a unit stride between each
 * "howmany".
 */
template<typename Tx, typename Ty, typename Tw>
using fft_ac_uu_func_t = void (*)(const Tx *x, Ty *y, int istride, int ostride,
                                  int howmany, real_t<Tw> dir);

/**
 * Signature of an FFT function which does not use twiddle factors.
 *
 * No odist parameter since we assume a unit stride between each
 * "howmany" in the output.
 */
template<typename Tx, typename Ty, typename Tw>
using fft_ac_gu_func_t = void (*)(const Tx *x, Ty *y, int istride, int ostride,
                                  int howmany, int idist, real_t<Tw> dir);

/**
 * Signature of an FFT function which use a set of twiddle factors per
 * iteration of "howmany".
 *
 * No odist parameter since we assume a unit stride between each "howmany".
 */
template<typename Tx, typename Ty, typename Tw>
using fft_ab_twid_gu_func_t = void (*)(const Tx *x, Ty *y, int istride,
                                       int ostride, const Tw *w, int howmany,
                                       int idist, real_t<Tw> dir);

template<typename Tx, typename Ty, typename Tw>
using fft_ab_twid_gs_func_t = void (*)(const Tx *x, Ty *y, int istride,
                                       int ostride, const Tw *w, int howmany,
                                       int idist, int odist, real_t<Tw> dir);

/**
 * Signature of an FFT function which use a single set of twiddle factors for
 * all "howmany" iterations.
 *
 * No idist/odist parameters since we assume a unit stride between each
 * "howmany".
 */
template<typename Tx, typename Ty, typename Tw>
using fft_ac_twid_func_t = void (*)(const Tx *x, Ty *y, int istride,
                                    int ostride, const Tw *w, int howmany,
                                    real_t<Tw> dir);

} // namespace armral::fft
