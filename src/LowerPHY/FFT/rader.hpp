/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "fft_plan.hpp"

#include <optional>

#ifdef ARMRAL_SEMIHOSTING
#define M_PI 3.14159265358979323846
#endif

namespace armral::fft {

/// Class to support using Rader's algorithm for prime n
template<typename Tx, typename Ty, typename Tw>
struct rader {
  /// The size of the FFT to solve with this object. It is assumed that this is
  /// prime
  int n;

  /// Generator for the group of primitive root modulo n (see
  /// https://en.wikipedia.org/wiki/Primitive_root_modulo_n)
  int g;
  /// Alternative generator for the group, which gives the values in reverse
  /// order to g (except for the first value, which is always 1)
  int g_inv;

  /// Plan for the forward part of a sub-fft
  armral_fft_plan_t *pf;
  /// Plan for the backward part of a sub-fft
  armral_fft_plan_t *pb;

  const Tw *b;

  /// Permutations for forward transform
  const int *gmul_fw_perm;
  /// Permutations for the backward transform
  const int *gmul_bw_perm;
  /// Permutations for the forward transform (in reverse order)
  const int *ginvmul_fw_perm;
  /// Permutations for the backward transform (in reverse order)
  const int *ginvmul_bw_perm;

  rader()
    : n(0), g(0), g_inv(0), pf(nullptr), pb(nullptr), b(nullptr),
      gmul_fw_perm(nullptr), gmul_bw_perm(nullptr), ginvmul_fw_perm(nullptr),
      ginvmul_bw_perm(nullptr) {}

  rader(const rader &) = delete;

  rader(rader &&other)
    : n(other.n), g(other.g), g_inv(other.g_inv), pf(other.pf), pb(other.pb),
      b(other.b), gmul_fw_perm(other.gmul_fw_perm),
      gmul_bw_perm(other.gmul_bw_perm), ginvmul_fw_perm(other.ginvmul_fw_perm),
      ginvmul_bw_perm(other.ginvmul_bw_perm) {
    other.pf = nullptr;
    other.pb = nullptr;
    other.b = nullptr;
    other.gmul_fw_perm = nullptr;
    other.gmul_bw_perm = nullptr;
    other.ginvmul_fw_perm = nullptr;
    other.ginvmul_bw_perm = nullptr;
  }

  rader &operator=(const rader &) = delete;
  rader &operator=(rader &&) = delete;

  rader(int n_in, int g_in, int g_inv_in, armral_fft_plan_t *pf_in,
        armral_fft_plan_t *pb_in, const Tw *b_in, const int *gmul_fw_in,
        const int *gmul_bw_in, const int *ginvmul_fw_in,
        const int *ginvmul_bw_in)
    : n(n_in), g(g_in), g_inv(g_inv_in), pf(pf_in), pb(pb_in), b(b_in),
      gmul_fw_perm(gmul_fw_in), gmul_bw_perm(gmul_bw_in),
      ginvmul_fw_perm(ginvmul_fw_in), ginvmul_bw_perm(ginvmul_bw_in) {}

  ~rader();

  operator bool() const {
    return n != 0;
  }
};

template<typename Tx, typename Ty, typename Tw>
std::optional<rader<Tx, Ty, Tw>> make_rader(int n, armral_fft_direction_t dir,
                                            int n_whole);

template<typename Tx, typename Ty, typename Tw>
void execute_rader(const rader<Tx, Ty, Tw> &r, const Tx *x, Ty *y, int istride,
                   int ostride, const Tw *w, int howmany, int idist, int odist);

} // namespace armral::fft
