/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "fft_cf32_cf32_cs16_ac_n_uu.h"

#include <arm_neon.h>
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu2(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v25 = v5[istride];
    float32x2_t v20 = v5[0];
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    int16x4_t v38 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v26, 15), (int32x2_t){0, 0}));
    int16x4_t v44 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v27, 15), (int32x2_t){0, 0}));
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v38), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v44), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu2(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    const float32x2_t *v76 = &v5[v0];
    int32_t *v97 = &v6[v2];
    const float32x2_t *v67 = &v5[0];
    int32_t *v88 = &v6[0];
    svfloat32_t v103 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v76)[0]));
    svfloat32_t v101 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v67)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v101, v103);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v101, v103);
    svint16_t v46 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v32, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v54 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v33, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v88), svreinterpret_u64_s16(v46));
    svst1w_u64(pred_full, (unsigned *)(v97), svreinterpret_u64_s16(v54));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu3(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v39 = -1.4999999999999998e+00F;
    float v42 = 8.6602540378443871e-01F;
    float v43 = -8.6602540378443871e-01F;
    float32x2_t v45 = (float32x2_t){v4, v4};
    float32x2_t v32 = v5[0];
    float32x2_t v40 = (float32x2_t){v39, v39};
    float32x2_t v44 = (float32x2_t){v42, v43};
    float32x2_t v25 = v5[istride * 2];
    float32x2_t v46 = vmul_f32(v45, v44);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v33 = vadd_f32(v26, v32);
    float32x2_t v41 = vmul_f32(v26, v40);
    float32x2_t v47 = vrev64_f32(v27);
    float32x2_t v48 = vmul_f32(v47, v46);
    float32x2_t v49 = vadd_f32(v33, v41);
    int16x4_t v54 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v33, 15), (int32x2_t){0, 0}));
    float32x2_t v50 = vadd_f32(v49, v48);
    float32x2_t v51 = vsub_f32(v49, v48);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v54), 0);
    int16x4_t v60 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v51, 15), (int32x2_t){0, 0}));
    int16x4_t v66 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v50, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v60), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v66), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu3(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v48 = -1.4999999999999998e+00F;
    float v53 = -8.6602540378443871e-01F;
    const float32x2_t *v92 = &v5[v0];
    int32_t *v133 = &v6[v2];
    int64_t v26 = v0 * 2;
    float v56 = v4 * v53;
    int64_t v79 = v2 * 2;
    const float32x2_t *v111 = &v5[0];
    svfloat32_t v115 = svdup_n_f32(v48);
    int32_t *v124 = &v6[0];
    svfloat32_t v146 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v92)[0]));
    const float32x2_t *v101 = &v5[v26];
    svfloat32_t v116 = svdup_n_f32(v56);
    int32_t *v142 = &v6[v79];
    svfloat32_t v150 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v111)[0]));
    svfloat32_t v148 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v101)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v146, v148);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v146, v148);
    svfloat32_t v41 = svadd_f32_x(svptrue_b32(), v32, v150);
    svfloat32_t zero58 = svdup_n_f32(0);
    svfloat32_t v58 = svcmla_f32_x(pred_full, zero58, v116, v33, 90);
    svfloat32_t v59 = svmla_f32_x(pred_full, v41, v32, v115);
    svint16_t v64 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v41, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v60 = svadd_f32_x(svptrue_b32(), v59, v58);
    svfloat32_t v61 = svsub_f32_x(svptrue_b32(), v59, v58);
    svst1w_u64(pred_full, (unsigned *)(v124), svreinterpret_u64_s16(v64));
    svint16_t v72 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v61, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v80 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v60, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v133), svreinterpret_u64_s16(v72));
    svst1w_u64(pred_full, (unsigned *)(v142), svreinterpret_u64_s16(v80));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu4(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v32 = v5[istride];
    float v54 = 1.0000000000000000e+00F;
    float v55 = -1.0000000000000000e+00F;
    float32x2_t v57 = (float32x2_t){v4, v4};
    float32x2_t v20 = v5[0];
    float32x2_t v56 = (float32x2_t){v54, v55};
    float32x2_t v25 = v5[istride * 2];
    float32x2_t v37 = v5[istride * 3];
    float32x2_t v58 = vmul_f32(v57, v56);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v40 = vadd_f32(v26, v38);
    float32x2_t v41 = vsub_f32(v26, v38);
    float32x2_t v59 = vrev64_f32(v39);
    float32x2_t v60 = vmul_f32(v59, v58);
    int16x4_t v65 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v40, 15), (int32x2_t){0, 0}));
    int16x4_t v77 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v41, 15), (int32x2_t){0, 0}));
    float32x2_t v61 = vadd_f32(v27, v60);
    float32x2_t v62 = vsub_f32(v27, v60);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v65), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v77), 0);
    int16x4_t v71 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v62, 15), (int32x2_t){0, 0}));
    int16x4_t v83 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v61, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v71), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v83), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu4(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v68 = -1.0000000000000000e+00F;
    const float32x2_t *v133 = &v5[v0];
    int32_t *v165 = &v6[v2];
    int64_t v26 = v0 * 2;
    int64_t v42 = v0 * 3;
    float v71 = v4 * v68;
    int64_t v93 = v2 * 2;
    int64_t v101 = v2 * 3;
    const float32x2_t *v115 = &v5[0];
    int32_t *v156 = &v6[0];
    svfloat32_t v191 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v133)[0]));
    const float32x2_t *v124 = &v5[v26];
    const float32x2_t *v142 = &v5[v42];
    svfloat32_t v148 = svdup_n_f32(v71);
    int32_t *v174 = &v6[v93];
    int32_t *v183 = &v6[v101];
    svfloat32_t v187 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v115)[0]));
    svfloat32_t v189 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v124)[0]));
    svfloat32_t v193 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v142)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v187, v189);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v187, v189);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v191, v193);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v191, v193);
    svfloat32_t v50 = svadd_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v51 = svsub_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 = svcmla_f32_x(pred_full, zero73, v148, v49, 90);
    svfloat32_t v74 = svadd_f32_x(svptrue_b32(), v33, v73);
    svfloat32_t v75 = svsub_f32_x(svptrue_b32(), v33, v73);
    svint16_t v78 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v50, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v94 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v51, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v86 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v75, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v102 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v74, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v156), svreinterpret_u64_s16(v78));
    svst1w_u64(pred_full, (unsigned *)(v174), svreinterpret_u64_s16(v94));
    svst1w_u64(pred_full, (unsigned *)(v165), svreinterpret_u64_s16(v86));
    svst1w_u64(pred_full, (unsigned *)(v183), svreinterpret_u64_s16(v102));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu5(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v54 = -1.2500000000000000e+00F;
    float v58 = 5.5901699437494745e-01F;
    float v61 = 1.5388417685876268e+00F;
    float v62 = -1.5388417685876268e+00F;
    float v68 = 5.8778525229247325e-01F;
    float v69 = -5.8778525229247325e-01F;
    float v75 = 3.6327126400268028e-01F;
    float v76 = -3.6327126400268028e-01F;
    float32x2_t v78 = (float32x2_t){v4, v4};
    float32x2_t v47 = v5[0];
    float32x2_t v55 = (float32x2_t){v54, v54};
    float32x2_t v59 = (float32x2_t){v58, v58};
    float32x2_t v63 = (float32x2_t){v61, v62};
    float32x2_t v70 = (float32x2_t){v68, v69};
    float32x2_t v77 = (float32x2_t){v75, v76};
    float32x2_t v25 = v5[istride * 4];
    float32x2_t v32 = v5[istride * 3];
    float32x2_t v37 = v5[istride * 2];
    float32x2_t v65 = vmul_f32(v78, v63);
    float32x2_t v72 = vmul_f32(v78, v70);
    float32x2_t v79 = vmul_f32(v78, v77);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v40 = vadd_f32(v26, v38);
    float32x2_t v41 = vsub_f32(v26, v38);
    float32x2_t v42 = vadd_f32(v27, v39);
    float32x2_t v66 = vrev64_f32(v27);
    float32x2_t v80 = vrev64_f32(v39);
    float32x2_t v48 = vadd_f32(v40, v47);
    float32x2_t v56 = vmul_f32(v40, v55);
    float32x2_t v60 = vmul_f32(v41, v59);
    float32x2_t v67 = vmul_f32(v66, v65);
    float32x2_t v73 = vrev64_f32(v42);
    float32x2_t v81 = vmul_f32(v80, v79);
    float32x2_t v74 = vmul_f32(v73, v72);
    float32x2_t v82 = vadd_f32(v48, v56);
    int16x4_t v93 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v48, 15), (int32x2_t){0, 0}));
    float32x2_t v83 = vadd_f32(v82, v60);
    float32x2_t v84 = vsub_f32(v82, v60);
    float32x2_t v85 = vsub_f32(v67, v74);
    float32x2_t v86 = vadd_f32(v74, v81);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v93), 0);
    float32x2_t v87 = vadd_f32(v83, v85);
    float32x2_t v88 = vsub_f32(v83, v85);
    float32x2_t v89 = vadd_f32(v84, v86);
    float32x2_t v90 = vsub_f32(v84, v86);
    int16x4_t v99 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v88, 15), (int32x2_t){0, 0}));
    int16x4_t v105 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v90, 15), (int32x2_t){0, 0}));
    int16x4_t v111 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v89, 15), (int32x2_t){0, 0}));
    int16x4_t v117 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v87, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v99), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v105), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v111), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v117), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu5(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v67 = -1.2500000000000000e+00F;
    float v72 = 5.5901699437494745e-01F;
    float v77 = -1.5388417685876268e+00F;
    float v84 = -5.8778525229247325e-01F;
    float v91 = -3.6327126400268028e-01F;
    const float32x2_t *v152 = &v5[v0];
    int32_t *v214 = &v6[v2];
    int64_t v26 = v0 * 4;
    int64_t v35 = v0 * 3;
    int64_t v42 = v0 * 2;
    float v80 = v4 * v77;
    float v87 = v4 * v84;
    float v94 = v4 * v91;
    int64_t v123 = v2 * 2;
    int64_t v131 = v2 * 3;
    int64_t v139 = v2 * 4;
    const float32x2_t *v189 = &v5[0];
    svfloat32_t v193 = svdup_n_f32(v67);
    svfloat32_t v194 = svdup_n_f32(v72);
    int32_t *v205 = &v6[0];
    svfloat32_t v245 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v152)[0]));
    const float32x2_t *v161 = &v5[v26];
    const float32x2_t *v170 = &v5[v35];
    const float32x2_t *v179 = &v5[v42];
    svfloat32_t v195 = svdup_n_f32(v80);
    svfloat32_t v196 = svdup_n_f32(v87);
    svfloat32_t v197 = svdup_n_f32(v94);
    int32_t *v223 = &v6[v123];
    int32_t *v232 = &v6[v131];
    int32_t *v241 = &v6[v139];
    svfloat32_t v253 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v189)[0]));
    svfloat32_t v247 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v161)[0]));
    svfloat32_t v249 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v170)[0]));
    svfloat32_t v251 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v179)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v245, v247);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v245, v247);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v249, v251);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v249, v251);
    svfloat32_t v50 = svadd_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v51 = svsub_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v52 = svadd_f32_x(svptrue_b32(), v33, v49);
    svfloat32_t zero82 = svdup_n_f32(0);
    svfloat32_t v82 = svcmla_f32_x(pred_full, zero82, v195, v33, 90);
    svfloat32_t v60 = svadd_f32_x(svptrue_b32(), v50, v253);
    svfloat32_t zero89 = svdup_n_f32(0);
    svfloat32_t v89 = svcmla_f32_x(pred_full, zero89, v196, v52, 90);
    svfloat32_t v97 = svmla_f32_x(pred_full, v60, v50, v193);
    svfloat32_t v100 = svsub_f32_x(svptrue_b32(), v82, v89);
    svfloat32_t v101 = svcmla_f32_x(pred_full, v89, v197, v49, 90);
    svint16_t v108 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v60, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v98 = svmla_f32_x(pred_full, v97, v51, v194);
    svfloat32_t v99 = svmls_f32_x(pred_full, v97, v51, v194);
    svst1w_u64(pred_full, (unsigned *)(v205), svreinterpret_u64_s16(v108));
    svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v98, v100);
    svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v98, v100);
    svfloat32_t v104 = svadd_f32_x(svptrue_b32(), v99, v101);
    svfloat32_t v105 = svsub_f32_x(svptrue_b32(), v99, v101);
    svint16_t v116 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v103, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v124 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v105, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v132 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v104, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v140 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v102, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v214), svreinterpret_u64_s16(v116));
    svst1w_u64(pred_full, (unsigned *)(v223), svreinterpret_u64_s16(v124));
    svst1w_u64(pred_full, (unsigned *)(v232), svreinterpret_u64_s16(v132));
    svst1w_u64(pred_full, (unsigned *)(v241), svreinterpret_u64_s16(v140));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu6(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v49 = v5[istride];
    float v81 = -1.4999999999999998e+00F;
    float v84 = 8.6602540378443871e-01F;
    float v85 = -8.6602540378443871e-01F;
    float32x2_t v87 = (float32x2_t){v4, v4};
    float32x2_t v20 = v5[0];
    float32x2_t v82 = (float32x2_t){v81, v81};
    float32x2_t v86 = (float32x2_t){v84, v85};
    float32x2_t v25 = v5[istride * 3];
    float32x2_t v32 = v5[istride * 2];
    float32x2_t v37 = v5[istride * 5];
    float32x2_t v44 = v5[istride * 4];
    float32x2_t v88 = vmul_f32(v87, v86);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v52 = vadd_f32(v38, v50);
    float32x2_t v53 = vsub_f32(v38, v50);
    float32x2_t v73 = vadd_f32(v39, v51);
    float32x2_t v74 = vsub_f32(v39, v51);
    float32x2_t v54 = vadd_f32(v52, v26);
    float32x2_t v62 = vmul_f32(v52, v82);
    float32x2_t v68 = vrev64_f32(v53);
    float32x2_t v75 = vadd_f32(v73, v27);
    float32x2_t v83 = vmul_f32(v73, v82);
    float32x2_t v89 = vrev64_f32(v74);
    float32x2_t v69 = vmul_f32(v68, v88);
    float32x2_t v70 = vadd_f32(v54, v62);
    float32x2_t v90 = vmul_f32(v89, v88);
    float32x2_t v91 = vadd_f32(v75, v83);
    int16x4_t v96 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v54, 15), (int32x2_t){0, 0}));
    int16x4_t v102 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v75, 15), (int32x2_t){0, 0}));
    float32x2_t v71 = vadd_f32(v70, v69);
    float32x2_t v72 = vsub_f32(v70, v69);
    float32x2_t v92 = vadd_f32(v91, v90);
    float32x2_t v93 = vsub_f32(v91, v90);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v96), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v102), 0);
    int16x4_t v108 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v72, 15), (int32x2_t){0, 0}));
    int16x4_t v114 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v93, 15), (int32x2_t){0, 0}));
    int16x4_t v120 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v71, 15), (int32x2_t){0, 0}));
    int16x4_t v126 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v92, 15), (int32x2_t){0, 0}));
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v108), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v114), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v120), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v126), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu6(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v98 = -1.4999999999999998e+00F;
    float v103 = -8.6602540378443871e-01F;
    const float32x2_t *v212 = &v5[v0];
    int32_t *v255 = &v6[v2];
    int64_t v26 = v0 * 3;
    int64_t v35 = v0 * 2;
    int64_t v42 = v0 * 5;
    int64_t v51 = v0 * 4;
    float v106 = v4 * v103;
    int64_t v121 = v2 * 3;
    int64_t v129 = v2 * 4;
    int64_t v145 = v2 * 2;
    int64_t v153 = v2 * 5;
    const float32x2_t *v167 = &v5[0];
    svfloat32_t v219 = svdup_n_f32(v98);
    int32_t *v228 = &v6[0];
    svfloat32_t v287 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v212)[0]));
    const float32x2_t *v176 = &v5[v26];
    const float32x2_t *v185 = &v5[v35];
    const float32x2_t *v194 = &v5[v42];
    const float32x2_t *v203 = &v5[v51];
    svfloat32_t v220 = svdup_n_f32(v106);
    int32_t *v237 = &v6[v121];
    int32_t *v246 = &v6[v129];
    int32_t *v264 = &v6[v145];
    int32_t *v273 = &v6[v153];
    svfloat32_t v277 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v167)[0]));
    svfloat32_t v279 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v176)[0]));
    svfloat32_t v281 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v185)[0]));
    svfloat32_t v283 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v194)[0]));
    svfloat32_t v285 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v203)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v281, v283);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v281, v283);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v285, v287);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v285, v287);
    svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v48, v64);
    svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v48, v64);
    svfloat32_t v89 = svadd_f32_x(svptrue_b32(), v49, v65);
    svfloat32_t v90 = svsub_f32_x(svptrue_b32(), v49, v65);
    svfloat32_t v68 = svadd_f32_x(svptrue_b32(), v66, v32);
    svfloat32_t zero85 = svdup_n_f32(0);
    svfloat32_t v85 = svcmla_f32_x(pred_full, zero85, v220, v67, 90);
    svfloat32_t v91 = svadd_f32_x(svptrue_b32(), v89, v33);
    svfloat32_t zero108 = svdup_n_f32(0);
    svfloat32_t v108 = svcmla_f32_x(pred_full, zero108, v220, v90, 90);
    svfloat32_t v86 = svmla_f32_x(pred_full, v68, v66, v219);
    svfloat32_t v109 = svmla_f32_x(pred_full, v91, v89, v219);
    svint16_t v114 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v68, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v122 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v91, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v87 = svadd_f32_x(svptrue_b32(), v86, v85);
    svfloat32_t v88 = svsub_f32_x(svptrue_b32(), v86, v85);
    svfloat32_t v110 = svadd_f32_x(svptrue_b32(), v109, v108);
    svfloat32_t v111 = svsub_f32_x(svptrue_b32(), v109, v108);
    svst1w_u64(pred_full, (unsigned *)(v228), svreinterpret_u64_s16(v114));
    svst1w_u64(pred_full, (unsigned *)(v237), svreinterpret_u64_s16(v122));
    svint16_t v130 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v88, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v138 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v111, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v146 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v87, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v154 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v110, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v246), svreinterpret_u64_s16(v130));
    svst1w_u64(pred_full, (unsigned *)(v255), svreinterpret_u64_s16(v138));
    svst1w_u64(pred_full, (unsigned *)(v264), svreinterpret_u64_s16(v146));
    svst1w_u64(pred_full, (unsigned *)(v273), svreinterpret_u64_s16(v154));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu7(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v73 = -1.1666666666666665e+00F;
    float v77 = 7.9015646852540022e-01F;
    float v81 = 5.5854267289647742e-02F;
    float v85 = 7.3430220123575241e-01F;
    float v88 = 4.4095855184409838e-01F;
    float v89 = -4.4095855184409838e-01F;
    float v95 = 3.4087293062393137e-01F;
    float v96 = -3.4087293062393137e-01F;
    float v102 = -5.3396936033772524e-01F;
    float v103 = 5.3396936033772524e-01F;
    float v109 = 8.7484229096165667e-01F;
    float v110 = -8.7484229096165667e-01F;
    float32x2_t v112 = (float32x2_t){v4, v4};
    float32x2_t v58 = v5[0];
    float32x2_t v74 = (float32x2_t){v73, v73};
    float32x2_t v78 = (float32x2_t){v77, v77};
    float32x2_t v82 = (float32x2_t){v81, v81};
    float32x2_t v86 = (float32x2_t){v85, v85};
    float32x2_t v90 = (float32x2_t){v88, v89};
    float32x2_t v97 = (float32x2_t){v95, v96};
    float32x2_t v104 = (float32x2_t){v102, v103};
    float32x2_t v111 = (float32x2_t){v109, v110};
    float32x2_t v25 = v5[istride * 6];
    float32x2_t v32 = v5[istride * 4];
    float32x2_t v37 = v5[istride * 3];
    float32x2_t v44 = v5[istride * 2];
    float32x2_t v49 = v5[istride * 5];
    float32x2_t v92 = vmul_f32(v112, v90);
    float32x2_t v99 = vmul_f32(v112, v97);
    float32x2_t v106 = vmul_f32(v112, v104);
    float32x2_t v113 = vmul_f32(v112, v111);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v52 = vadd_f32(v26, v38);
    float32x2_t v60 = vsub_f32(v26, v38);
    float32x2_t v61 = vsub_f32(v38, v50);
    float32x2_t v62 = vsub_f32(v50, v26);
    float32x2_t v63 = vadd_f32(v27, v39);
    float32x2_t v65 = vsub_f32(v27, v39);
    float32x2_t v66 = vsub_f32(v39, v51);
    float32x2_t v67 = vsub_f32(v51, v27);
    float32x2_t v53 = vadd_f32(v52, v50);
    float32x2_t v64 = vadd_f32(v63, v51);
    float32x2_t v79 = vmul_f32(v60, v78);
    float32x2_t v83 = vmul_f32(v61, v82);
    float32x2_t v87 = vmul_f32(v62, v86);
    float32x2_t v100 = vrev64_f32(v65);
    float32x2_t v107 = vrev64_f32(v66);
    float32x2_t v114 = vrev64_f32(v67);
    float32x2_t v59 = vadd_f32(v53, v58);
    float32x2_t v75 = vmul_f32(v53, v74);
    float32x2_t v93 = vrev64_f32(v64);
    float32x2_t v101 = vmul_f32(v100, v99);
    float32x2_t v108 = vmul_f32(v107, v106);
    float32x2_t v115 = vmul_f32(v114, v113);
    float32x2_t v94 = vmul_f32(v93, v92);
    float32x2_t v116 = vadd_f32(v59, v75);
    int16x4_t v137 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v59, 15), (int32x2_t){0, 0}));
    float32x2_t v117 = vadd_f32(v116, v79);
    float32x2_t v119 = vsub_f32(v116, v79);
    float32x2_t v121 = vsub_f32(v116, v83);
    float32x2_t v123 = vadd_f32(v94, v101);
    float32x2_t v125 = vsub_f32(v94, v101);
    float32x2_t v127 = vsub_f32(v94, v108);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v137), 0);
    float32x2_t v118 = vadd_f32(v117, v83);
    float32x2_t v120 = vsub_f32(v119, v87);
    float32x2_t v122 = vadd_f32(v121, v87);
    float32x2_t v124 = vadd_f32(v123, v108);
    float32x2_t v126 = vsub_f32(v125, v115);
    float32x2_t v128 = vadd_f32(v127, v115);
    float32x2_t v129 = vadd_f32(v118, v124);
    float32x2_t v130 = vsub_f32(v118, v124);
    float32x2_t v131 = vadd_f32(v120, v126);
    float32x2_t v132 = vsub_f32(v120, v126);
    float32x2_t v133 = vadd_f32(v122, v128);
    float32x2_t v134 = vsub_f32(v122, v128);
    int16x4_t v143 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v130, 15), (int32x2_t){0, 0}));
    int16x4_t v149 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v132, 15), (int32x2_t){0, 0}));
    int16x4_t v155 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v133, 15), (int32x2_t){0, 0}));
    int16x4_t v161 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v134, 15), (int32x2_t){0, 0}));
    int16x4_t v167 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v131, 15), (int32x2_t){0, 0}));
    int16x4_t v173 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v129, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v143), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v149), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v155), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v161), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v167), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v173), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu7(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v90 = -1.1666666666666665e+00F;
    float v95 = 7.9015646852540022e-01F;
    float v100 = 5.5854267289647742e-02F;
    float v105 = 7.3430220123575241e-01F;
    float v110 = -4.4095855184409838e-01F;
    float v117 = -3.4087293062393137e-01F;
    float v124 = 5.3396936033772524e-01F;
    float v131 = -8.7484229096165667e-01F;
    const float32x2_t *v218 = &v5[v0];
    int32_t *v301 = &v6[v2];
    int64_t v26 = v0 * 6;
    int64_t v35 = v0 * 4;
    int64_t v42 = v0 * 3;
    int64_t v51 = v0 * 2;
    int64_t v58 = v0 * 5;
    float v113 = v4 * v110;
    float v120 = v4 * v117;
    float v127 = v4 * v124;
    float v134 = v4 * v131;
    int64_t v173 = v2 * 2;
    int64_t v181 = v2 * 3;
    int64_t v189 = v2 * 4;
    int64_t v197 = v2 * 5;
    int64_t v205 = v2 * 6;
    const float32x2_t *v273 = &v5[0];
    svfloat32_t v277 = svdup_n_f32(v90);
    svfloat32_t v278 = svdup_n_f32(v95);
    svfloat32_t v279 = svdup_n_f32(v100);
    svfloat32_t v280 = svdup_n_f32(v105);
    int32_t *v292 = &v6[0];
    svfloat32_t v350 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v218)[0]));
    const float32x2_t *v227 = &v5[v26];
    const float32x2_t *v236 = &v5[v35];
    const float32x2_t *v245 = &v5[v42];
    const float32x2_t *v254 = &v5[v51];
    const float32x2_t *v263 = &v5[v58];
    svfloat32_t v281 = svdup_n_f32(v113);
    svfloat32_t v282 = svdup_n_f32(v120);
    svfloat32_t v283 = svdup_n_f32(v127);
    svfloat32_t v284 = svdup_n_f32(v134);
    int32_t *v310 = &v6[v173];
    int32_t *v319 = &v6[v181];
    int32_t *v328 = &v6[v189];
    int32_t *v337 = &v6[v197];
    int32_t *v346 = &v6[v205];
    svfloat32_t v362 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v273)[0]));
    svfloat32_t v352 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v227)[0]));
    svfloat32_t v354 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v236)[0]));
    svfloat32_t v356 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v245)[0]));
    svfloat32_t v358 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v254)[0]));
    svfloat32_t v360 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v263)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v350, v352);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v350, v352);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v354, v356);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v354, v356);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v358, v360);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v358, v360);
    svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v76 = svsub_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v77 = svsub_f32_x(svptrue_b32(), v48, v64);
    svfloat32_t v78 = svsub_f32_x(svptrue_b32(), v64, v32);
    svfloat32_t v79 = svadd_f32_x(svptrue_b32(), v33, v49);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v33, v49);
    svfloat32_t v82 = svsub_f32_x(svptrue_b32(), v49, v65);
    svfloat32_t v83 = svsub_f32_x(svptrue_b32(), v65, v33);
    svfloat32_t v67 = svadd_f32_x(svptrue_b32(), v66, v64);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v79, v65);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 = svcmla_f32_x(pred_full, zero122, v282, v81, 90);
    svfloat32_t zero129 = svdup_n_f32(0);
    svfloat32_t v129 = svcmla_f32_x(pred_full, zero129, v283, v82, 90);
    svfloat32_t zero136 = svdup_n_f32(0);
    svfloat32_t v136 = svcmla_f32_x(pred_full, zero136, v284, v83, 90);
    svfloat32_t v75 = svadd_f32_x(svptrue_b32(), v67, v362);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 = svcmla_f32_x(pred_full, zero115, v281, v80, 90);
    svfloat32_t v137 = svmla_f32_x(pred_full, v75, v67, v277);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v146 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v148 = svsub_f32_x(svptrue_b32(), v115, v129);
    svint16_t v158 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v75, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v138 = svmla_f32_x(pred_full, v137, v76, v278);
    svfloat32_t v140 = svmls_f32_x(pred_full, v137, v76, v278);
    svfloat32_t v142 = svmls_f32_x(pred_full, v137, v77, v279);
    svfloat32_t v145 = svadd_f32_x(svptrue_b32(), v144, v129);
    svfloat32_t v147 = svsub_f32_x(svptrue_b32(), v146, v136);
    svfloat32_t v149 = svadd_f32_x(svptrue_b32(), v148, v136);
    svst1w_u64(pred_full, (unsigned *)(v292), svreinterpret_u64_s16(v158));
    svfloat32_t v139 = svmla_f32_x(pred_full, v138, v77, v279);
    svfloat32_t v141 = svmls_f32_x(pred_full, v140, v78, v280);
    svfloat32_t v143 = svmla_f32_x(pred_full, v142, v78, v280);
    svfloat32_t v150 = svadd_f32_x(svptrue_b32(), v139, v145);
    svfloat32_t v151 = svsub_f32_x(svptrue_b32(), v139, v145);
    svfloat32_t v152 = svadd_f32_x(svptrue_b32(), v141, v147);
    svfloat32_t v153 = svsub_f32_x(svptrue_b32(), v141, v147);
    svfloat32_t v154 = svadd_f32_x(svptrue_b32(), v143, v149);
    svfloat32_t v155 = svsub_f32_x(svptrue_b32(), v143, v149);
    svint16_t v166 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v151, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v174 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v153, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v182 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v154, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v190 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v155, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v198 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v152, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v206 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v150, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v301), svreinterpret_u64_s16(v166));
    svst1w_u64(pred_full, (unsigned *)(v310), svreinterpret_u64_s16(v174));
    svst1w_u64(pred_full, (unsigned *)(v319), svreinterpret_u64_s16(v182));
    svst1w_u64(pred_full, (unsigned *)(v328), svreinterpret_u64_s16(v190));
    svst1w_u64(pred_full, (unsigned *)(v337), svreinterpret_u64_s16(v198));
    svst1w_u64(pred_full, (unsigned *)(v346), svreinterpret_u64_s16(v206));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu8(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v44 = v5[istride];
    float v95 = 1.0000000000000000e+00F;
    float v96 = -1.0000000000000000e+00F;
    float v103 = -7.0710678118654746e-01F;
    float32x2_t v105 = (float32x2_t){v4, v4};
    float v110 = 7.0710678118654757e-01F;
    float32x2_t v20 = v5[0];
    float32x2_t v97 = (float32x2_t){v95, v96};
    float32x2_t v104 = (float32x2_t){v110, v103};
    float32x2_t v111 = (float32x2_t){v110, v110};
    float32x2_t v25 = v5[istride * 4];
    float32x2_t v32 = v5[istride * 2];
    float32x2_t v37 = v5[istride * 6];
    float32x2_t v49 = v5[istride * 5];
    float32x2_t v56 = v5[istride * 3];
    float32x2_t v61 = v5[istride * 7];
    float32x2_t v99 = vmul_f32(v105, v97);
    float32x2_t v106 = vmul_f32(v105, v104);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v64 = vadd_f32(v26, v38);
    float32x2_t v65 = vsub_f32(v26, v38);
    float32x2_t v66 = vadd_f32(v50, v62);
    float32x2_t v67 = vsub_f32(v50, v62);
    float32x2_t v70 = vadd_f32(v51, v63);
    float32x2_t v71 = vsub_f32(v51, v63);
    float32x2_t v100 = vrev64_f32(v39);
    float32x2_t v68 = vadd_f32(v64, v66);
    float32x2_t v69 = vsub_f32(v64, v66);
    float32x2_t v89 = vrev64_f32(v67);
    float32x2_t v101 = vmul_f32(v100, v99);
    float32x2_t v107 = vrev64_f32(v70);
    float32x2_t v112 = vmul_f32(v71, v111);
    float32x2_t v90 = vmul_f32(v89, v99);
    float32x2_t v108 = vmul_f32(v107, v106);
    float32x2_t v115 = vadd_f32(v27, v112);
    float32x2_t v116 = vsub_f32(v27, v112);
    int16x4_t v125 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v68, 15), (int32x2_t){0, 0}));
    int16x4_t v149 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v69, 15), (int32x2_t){0, 0}));
    float32x2_t v113 = vadd_f32(v65, v90);
    float32x2_t v114 = vsub_f32(v65, v90);
    float32x2_t v117 = vadd_f32(v101, v108);
    float32x2_t v118 = vsub_f32(v101, v108);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v125), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v149), 0);
    float32x2_t v119 = vadd_f32(v115, v117);
    float32x2_t v120 = vsub_f32(v115, v117);
    float32x2_t v121 = vadd_f32(v116, v118);
    float32x2_t v122 = vsub_f32(v116, v118);
    int16x4_t v137 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v114, 15), (int32x2_t){0, 0}));
    int16x4_t v161 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v113, 15), (int32x2_t){0, 0}));
    int16x4_t v131 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v120, 15), (int32x2_t){0, 0}));
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v137), 0);
    int16x4_t v143 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v121, 15), (int32x2_t){0, 0}));
    int16x4_t v155 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v122, 15), (int32x2_t){0, 0}));
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v161), 0);
    int16x4_t v167 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v119, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v131), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v143), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v155), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v167), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu8(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v118 = -1.0000000000000000e+00F;
    float v125 = -7.0710678118654746e-01F;
    float v132 = 7.0710678118654757e-01F;
    const float32x2_t *v253 = &v5[v0];
    int32_t *v307 = &v6[v2];
    int64_t v26 = v0 * 4;
    int64_t v35 = v0 * 2;
    int64_t v42 = v0 * 6;
    int64_t v58 = v0 * 5;
    int64_t v67 = v0 * 3;
    int64_t v74 = v0 * 7;
    float v121 = v4 * v118;
    float v128 = v4 * v125;
    int64_t v163 = v2 * 2;
    int64_t v171 = v2 * 3;
    int64_t v179 = v2 * 4;
    int64_t v187 = v2 * 5;
    int64_t v195 = v2 * 6;
    int64_t v203 = v2 * 7;
    const float32x2_t *v217 = &v5[0];
    svfloat32_t v290 = svdup_n_f32(v132);
    int32_t *v298 = &v6[0];
    svfloat32_t v373 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v253)[0]));
    const float32x2_t *v226 = &v5[v26];
    const float32x2_t *v235 = &v5[v35];
    const float32x2_t *v244 = &v5[v42];
    const float32x2_t *v262 = &v5[v58];
    const float32x2_t *v271 = &v5[v67];
    const float32x2_t *v280 = &v5[v74];
    svfloat32_t v288 = svdup_n_f32(v121);
    svfloat32_t v289 = svdup_n_f32(v128);
    int32_t *v316 = &v6[v163];
    int32_t *v325 = &v6[v171];
    int32_t *v334 = &v6[v179];
    int32_t *v343 = &v6[v187];
    int32_t *v352 = &v6[v195];
    int32_t *v361 = &v6[v203];
    svfloat32_t v365 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v217)[0]));
    svfloat32_t v367 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v226)[0]));
    svfloat32_t v369 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v235)[0]));
    svfloat32_t v371 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v244)[0]));
    svfloat32_t v375 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v262)[0]));
    svfloat32_t v377 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v271)[0]));
    svfloat32_t v379 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v280)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v365, v367);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v365, v367);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v369, v371);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v369, v371);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v373, v375);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v373, v375);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v377, v379);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v377, v379);
    svfloat32_t v82 = svadd_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v83 = svsub_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v64, v80);
    svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v64, v80);
    svfloat32_t v88 = svadd_f32_x(svptrue_b32(), v65, v81);
    svfloat32_t v89 = svsub_f32_x(svptrue_b32(), v65, v81);
    svfloat32_t zero123 = svdup_n_f32(0);
    svfloat32_t v123 = svcmla_f32_x(pred_full, zero123, v288, v49, 90);
    svfloat32_t v86 = svadd_f32_x(svptrue_b32(), v82, v84);
    svfloat32_t v87 = svsub_f32_x(svptrue_b32(), v82, v84);
    svfloat32_t zero111 = svdup_n_f32(0);
    svfloat32_t v111 = svcmla_f32_x(pred_full, zero111, v288, v85, 90);
    svfloat32_t zero130 = svdup_n_f32(0);
    svfloat32_t v130 = svcmla_f32_x(pred_full, zero130, v289, v88, 90);
    svfloat32_t v136 = svadd_f32_x(svptrue_b32(), v83, v111);
    svfloat32_t v137 = svsub_f32_x(svptrue_b32(), v83, v111);
    svfloat32_t v138 = svmla_f32_x(pred_full, v33, v89, v290);
    svfloat32_t v139 = svmls_f32_x(pred_full, v33, v89, v290);
    svfloat32_t v140 = svadd_f32_x(svptrue_b32(), v123, v130);
    svfloat32_t v141 = svsub_f32_x(svptrue_b32(), v123, v130);
    svint16_t v148 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v86, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v180 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v87, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v142 = svadd_f32_x(svptrue_b32(), v138, v140);
    svfloat32_t v143 = svsub_f32_x(svptrue_b32(), v138, v140);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v139, v141);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v139, v141);
    svint16_t v164 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v137, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v196 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v136, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v298), svreinterpret_u64_s16(v148));
    svst1w_u64(pred_full, (unsigned *)(v334), svreinterpret_u64_s16(v180));
    svint16_t v156 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v143, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v172 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v144, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v188 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v145, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v204 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v142, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v316), svreinterpret_u64_s16(v164));
    svst1w_u64(pred_full, (unsigned *)(v352), svreinterpret_u64_s16(v196));
    svst1w_u64(pred_full, (unsigned *)(v307), svreinterpret_u64_s16(v156));
    svst1w_u64(pred_full, (unsigned *)(v325), svreinterpret_u64_s16(v172));
    svst1w_u64(pred_full, (unsigned *)(v343), svreinterpret_u64_s16(v188));
    svst1w_u64(pred_full, (unsigned *)(v361), svreinterpret_u64_s16(v204));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu9(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v86 = -5.0000000000000000e-01F;
    float v97 = -1.4999999999999998e+00F;
    float v100 = 8.6602540378443871e-01F;
    float v101 = -8.6602540378443871e-01F;
    float v108 = 7.6604444311897801e-01F;
    float v112 = 9.3969262078590832e-01F;
    float v116 = -1.7364817766693039e-01F;
    float v119 = 6.4278760968653925e-01F;
    float v120 = -6.4278760968653925e-01F;
    float v126 = -3.4202014332566888e-01F;
    float v127 = 3.4202014332566888e-01F;
    float v133 = 9.8480775301220802e-01F;
    float v134 = -9.8480775301220802e-01F;
    float32x2_t v136 = (float32x2_t){v4, v4};
    float32x2_t v71 = v5[0];
    float32x2_t v87 = (float32x2_t){v86, v86};
    float32x2_t v98 = (float32x2_t){v97, v97};
    float32x2_t v102 = (float32x2_t){v100, v101};
    float32x2_t v109 = (float32x2_t){v108, v108};
    float32x2_t v113 = (float32x2_t){v112, v112};
    float32x2_t v117 = (float32x2_t){v116, v116};
    float32x2_t v121 = (float32x2_t){v119, v120};
    float32x2_t v128 = (float32x2_t){v126, v127};
    float32x2_t v135 = (float32x2_t){v133, v134};
    float32x2_t v25 = v5[istride * 8];
    float32x2_t v32 = v5[istride * 7];
    float32x2_t v37 = v5[istride * 2];
    float32x2_t v44 = v5[istride * 3];
    float32x2_t v49 = v5[istride * 6];
    float32x2_t v56 = v5[istride * 4];
    float32x2_t v61 = v5[istride * 5];
    float32x2_t v104 = vmul_f32(v136, v102);
    float32x2_t v123 = vmul_f32(v136, v121);
    float32x2_t v130 = vmul_f32(v136, v128);
    float32x2_t v137 = vmul_f32(v136, v135);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v64 = vadd_f32(v26, v38);
    float32x2_t v73 = vadd_f32(v27, v39);
    float32x2_t v75 = vsub_f32(v26, v38);
    float32x2_t v76 = vsub_f32(v38, v62);
    float32x2_t v77 = vsub_f32(v62, v26);
    float32x2_t v78 = vsub_f32(v27, v39);
    float32x2_t v79 = vsub_f32(v39, v63);
    float32x2_t v80 = vsub_f32(v63, v27);
    float32x2_t v99 = vmul_f32(v50, v98);
    float32x2_t v105 = vrev64_f32(v51);
    float32x2_t v65 = vadd_f32(v64, v62);
    float32x2_t v74 = vadd_f32(v73, v63);
    float32x2_t v106 = vmul_f32(v105, v104);
    float32x2_t v110 = vmul_f32(v75, v109);
    float32x2_t v114 = vmul_f32(v76, v113);
    float32x2_t v118 = vmul_f32(v77, v117);
    float32x2_t v124 = vrev64_f32(v78);
    float32x2_t v131 = vrev64_f32(v79);
    float32x2_t v138 = vrev64_f32(v80);
    float32x2_t v66 = vadd_f32(v65, v50);
    float32x2_t v88 = vmul_f32(v65, v87);
    float32x2_t v94 = vrev64_f32(v74);
    float32x2_t v125 = vmul_f32(v124, v123);
    float32x2_t v132 = vmul_f32(v131, v130);
    float32x2_t v139 = vmul_f32(v138, v137);
    float32x2_t v72 = vadd_f32(v66, v71);
    float32x2_t v95 = vmul_f32(v94, v104);
    float32x2_t v140 = vadd_f32(v88, v88);
    float32x2_t v153 = vadd_f32(v106, v125);
    float32x2_t v155 = vsub_f32(v106, v132);
    float32x2_t v157 = vsub_f32(v106, v125);
    float32x2_t v141 = vadd_f32(v140, v88);
    float32x2_t v145 = vadd_f32(v72, v99);
    float32x2_t v154 = vadd_f32(v153, v132);
    float32x2_t v156 = vadd_f32(v155, v139);
    float32x2_t v158 = vsub_f32(v157, v139);
    int16x4_t v167 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v72, 15), (int32x2_t){0, 0}));
    float32x2_t v142 = vadd_f32(v72, v141);
    float32x2_t v146 = vadd_f32(v145, v140);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v167), 0);
    float32x2_t v143 = vadd_f32(v142, v95);
    float32x2_t v144 = vsub_f32(v142, v95);
    float32x2_t v147 = vadd_f32(v146, v110);
    float32x2_t v149 = vsub_f32(v146, v114);
    float32x2_t v151 = vsub_f32(v146, v110);
    float32x2_t v148 = vadd_f32(v147, v114);
    float32x2_t v150 = vadd_f32(v149, v118);
    float32x2_t v152 = vsub_f32(v151, v118);
    int16x4_t v185 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v144, 15), (int32x2_t){0, 0}));
    int16x4_t v203 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v143, 15), (int32x2_t){0, 0}));
    float32x2_t v159 = vadd_f32(v148, v154);
    float32x2_t v160 = vsub_f32(v148, v154);
    float32x2_t v161 = vadd_f32(v150, v156);
    float32x2_t v162 = vsub_f32(v150, v156);
    float32x2_t v163 = vadd_f32(v152, v158);
    float32x2_t v164 = vsub_f32(v152, v158);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v185), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v203), 0);
    int16x4_t v173 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v160, 15), (int32x2_t){0, 0}));
    int16x4_t v179 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v161, 15), (int32x2_t){0, 0}));
    int16x4_t v191 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v164, 15), (int32x2_t){0, 0}));
    int16x4_t v197 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v163, 15), (int32x2_t){0, 0}));
    int16x4_t v209 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v162, 15), (int32x2_t){0, 0}));
    int16x4_t v215 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v159, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v173), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v179), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v191), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v197), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v209), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v215), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu9(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_int16_t *restrict y,
                                        int istride, int ostride, int howmany,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v107 = -5.0000000000000000e-01F;
    float v119 = -1.4999999999999998e+00F;
    float v124 = -8.6602540378443871e-01F;
    float v131 = 7.6604444311897801e-01F;
    float v136 = 9.3969262078590832e-01F;
    float v141 = -1.7364817766693039e-01F;
    float v146 = -6.4278760968653925e-01F;
    float v153 = 3.4202014332566888e-01F;
    float v160 = -9.8480775301220802e-01F;
    const float32x2_t *v269 = &v5[v0];
    int32_t *v372 = &v6[v2];
    int64_t v26 = v0 * 8;
    int64_t v35 = v0 * 7;
    int64_t v42 = v0 * 2;
    int64_t v51 = v0 * 3;
    int64_t v58 = v0 * 6;
    int64_t v67 = v0 * 4;
    int64_t v74 = v0 * 5;
    float v127 = v4 * v124;
    float v149 = v4 * v146;
    float v156 = v4 * v153;
    float v163 = v4 * v160;
    int64_t v208 = v2 * 2;
    int64_t v216 = v2 * 3;
    int64_t v224 = v2 * 4;
    int64_t v232 = v2 * 5;
    int64_t v240 = v2 * 6;
    int64_t v248 = v2 * 7;
    int64_t v256 = v2 * 8;
    const float32x2_t *v342 = &v5[0];
    svfloat32_t v346 = svdup_n_f32(v107);
    svfloat32_t v348 = svdup_n_f32(v119);
    svfloat32_t v350 = svdup_n_f32(v131);
    svfloat32_t v351 = svdup_n_f32(v136);
    svfloat32_t v352 = svdup_n_f32(v141);
    int32_t *v363 = &v6[0];
    svfloat32_t v439 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v269)[0]));
    const float32x2_t *v278 = &v5[v26];
    const float32x2_t *v287 = &v5[v35];
    const float32x2_t *v296 = &v5[v42];
    const float32x2_t *v305 = &v5[v51];
    const float32x2_t *v314 = &v5[v58];
    const float32x2_t *v323 = &v5[v67];
    const float32x2_t *v332 = &v5[v74];
    svfloat32_t v349 = svdup_n_f32(v127);
    svfloat32_t v353 = svdup_n_f32(v149);
    svfloat32_t v354 = svdup_n_f32(v156);
    svfloat32_t v355 = svdup_n_f32(v163);
    int32_t *v381 = &v6[v208];
    int32_t *v390 = &v6[v216];
    int32_t *v399 = &v6[v224];
    int32_t *v408 = &v6[v232];
    int32_t *v417 = &v6[v240];
    int32_t *v426 = &v6[v248];
    int32_t *v435 = &v6[v256];
    svfloat32_t v455 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v342)[0]));
    svfloat32_t v441 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v278)[0]));
    svfloat32_t v443 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v287)[0]));
    svfloat32_t v445 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v296)[0]));
    svfloat32_t v447 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v305)[0]));
    svfloat32_t v449 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v314)[0]));
    svfloat32_t v451 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v323)[0]));
    svfloat32_t v453 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v332)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v439, v441);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v439, v441);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v443, v445);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v443, v445);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v447, v449);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v447, v449);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v451, v453);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v451, v453);
    svfloat32_t v82 = svadd_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v93 = svadd_f32_x(svptrue_b32(), v33, v49);
    svfloat32_t v95 = svsub_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v96 = svsub_f32_x(svptrue_b32(), v48, v80);
    svfloat32_t v97 = svsub_f32_x(svptrue_b32(), v80, v32);
    svfloat32_t v98 = svsub_f32_x(svptrue_b32(), v33, v49);
    svfloat32_t v99 = svsub_f32_x(svptrue_b32(), v49, v81);
    svfloat32_t v100 = svsub_f32_x(svptrue_b32(), v81, v33);
    svfloat32_t zero129 = svdup_n_f32(0);
    svfloat32_t v129 = svcmla_f32_x(pred_full, zero129, v349, v65, 90);
    svfloat32_t v83 = svadd_f32_x(svptrue_b32(), v82, v80);
    svfloat32_t v94 = svadd_f32_x(svptrue_b32(), v93, v81);
    svfloat32_t zero151 = svdup_n_f32(0);
    svfloat32_t v151 = svcmla_f32_x(pred_full, zero151, v353, v98, 90);
    svfloat32_t zero158 = svdup_n_f32(0);
    svfloat32_t v158 = svcmla_f32_x(pred_full, zero158, v354, v99, 90);
    svfloat32_t zero165 = svdup_n_f32(0);
    svfloat32_t v165 = svcmla_f32_x(pred_full, zero165, v355, v100, 90);
    svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v83, v64);
    svfloat32_t v110 = svmul_f32_x(svptrue_b32(), v83, v346);
    svfloat32_t zero117 = svdup_n_f32(0);
    svfloat32_t v117 = svcmla_f32_x(pred_full, zero117, v349, v94, 90);
    svfloat32_t v179 = svadd_f32_x(svptrue_b32(), v129, v151);
    svfloat32_t v181 = svsub_f32_x(svptrue_b32(), v129, v158);
    svfloat32_t v183 = svsub_f32_x(svptrue_b32(), v129, v151);
    svfloat32_t v92 = svadd_f32_x(svptrue_b32(), v84, v455);
    svfloat32_t v166 = svadd_f32_x(svptrue_b32(), v110, v110);
    svfloat32_t v180 = svadd_f32_x(svptrue_b32(), v179, v158);
    svfloat32_t v182 = svadd_f32_x(svptrue_b32(), v181, v165);
    svfloat32_t v184 = svsub_f32_x(svptrue_b32(), v183, v165);
    svfloat32_t v167 = svmla_f32_x(pred_full, v166, v83, v346);
    svfloat32_t v171 = svmla_f32_x(pred_full, v92, v64, v348);
    svint16_t v193 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v92, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v168 = svadd_f32_x(svptrue_b32(), v92, v167);
    svfloat32_t v172 = svadd_f32_x(svptrue_b32(), v171, v166);
    svst1w_u64(pred_full, (unsigned *)(v363), svreinterpret_u64_s16(v193));
    svfloat32_t v169 = svadd_f32_x(svptrue_b32(), v168, v117);
    svfloat32_t v170 = svsub_f32_x(svptrue_b32(), v168, v117);
    svfloat32_t v173 = svmla_f32_x(pred_full, v172, v95, v350);
    svfloat32_t v175 = svmls_f32_x(pred_full, v172, v96, v351);
    svfloat32_t v177 = svmls_f32_x(pred_full, v172, v95, v350);
    svfloat32_t v174 = svmla_f32_x(pred_full, v173, v96, v351);
    svfloat32_t v176 = svmla_f32_x(pred_full, v175, v97, v352);
    svfloat32_t v178 = svmls_f32_x(pred_full, v177, v97, v352);
    svint16_t v217 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v170, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v241 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v169, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v185 = svadd_f32_x(svptrue_b32(), v174, v180);
    svfloat32_t v186 = svsub_f32_x(svptrue_b32(), v174, v180);
    svfloat32_t v187 = svadd_f32_x(svptrue_b32(), v176, v182);
    svfloat32_t v188 = svsub_f32_x(svptrue_b32(), v176, v182);
    svfloat32_t v189 = svadd_f32_x(svptrue_b32(), v178, v184);
    svfloat32_t v190 = svsub_f32_x(svptrue_b32(), v178, v184);
    svst1w_u64(pred_full, (unsigned *)(v390), svreinterpret_u64_s16(v217));
    svst1w_u64(pred_full, (unsigned *)(v417), svreinterpret_u64_s16(v241));
    svint16_t v201 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v186, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v209 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v187, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v225 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v190, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v233 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v189, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v249 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v188, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v257 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v185, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v372), svreinterpret_u64_s16(v201));
    svst1w_u64(pred_full, (unsigned *)(v381), svreinterpret_u64_s16(v209));
    svst1w_u64(pred_full, (unsigned *)(v399), svreinterpret_u64_s16(v225));
    svst1w_u64(pred_full, (unsigned *)(v408), svreinterpret_u64_s16(v233));
    svst1w_u64(pred_full, (unsigned *)(v426), svreinterpret_u64_s16(v249));
    svst1w_u64(pred_full, (unsigned *)(v435), svreinterpret_u64_s16(v257));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu10(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v61 = v5[istride];
    float v139 = -1.2500000000000000e+00F;
    float v143 = 5.5901699437494745e-01F;
    float v146 = 1.5388417685876268e+00F;
    float v147 = -1.5388417685876268e+00F;
    float v153 = 5.8778525229247325e-01F;
    float v154 = -5.8778525229247325e-01F;
    float v160 = 3.6327126400268028e-01F;
    float v161 = -3.6327126400268028e-01F;
    float32x2_t v163 = (float32x2_t){v4, v4};
    float32x2_t v20 = v5[0];
    float32x2_t v140 = (float32x2_t){v139, v139};
    float32x2_t v144 = (float32x2_t){v143, v143};
    float32x2_t v148 = (float32x2_t){v146, v147};
    float32x2_t v155 = (float32x2_t){v153, v154};
    float32x2_t v162 = (float32x2_t){v160, v161};
    float32x2_t v25 = v5[istride * 5];
    float32x2_t v32 = v5[istride * 2];
    float32x2_t v37 = v5[istride * 7];
    float32x2_t v44 = v5[istride * 4];
    float32x2_t v49 = v5[istride * 9];
    float32x2_t v56 = v5[istride * 6];
    float32x2_t v68 = v5[istride * 8];
    float32x2_t v73 = v5[istride * 3];
    float32x2_t v150 = vmul_f32(v163, v148);
    float32x2_t v157 = vmul_f32(v163, v155);
    float32x2_t v164 = vmul_f32(v163, v162);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v74 = vadd_f32(v68, v73);
    float32x2_t v75 = vsub_f32(v68, v73);
    float32x2_t v76 = vadd_f32(v38, v74);
    float32x2_t v77 = vsub_f32(v38, v74);
    float32x2_t v78 = vadd_f32(v62, v50);
    float32x2_t v79 = vsub_f32(v62, v50);
    float32x2_t v126 = vadd_f32(v39, v75);
    float32x2_t v127 = vsub_f32(v39, v75);
    float32x2_t v128 = vadd_f32(v63, v51);
    float32x2_t v129 = vsub_f32(v63, v51);
    float32x2_t v80 = vadd_f32(v76, v78);
    float32x2_t v81 = vsub_f32(v76, v78);
    float32x2_t v82 = vadd_f32(v77, v79);
    float32x2_t v101 = vrev64_f32(v77);
    float32x2_t v115 = vrev64_f32(v79);
    float32x2_t v130 = vadd_f32(v126, v128);
    float32x2_t v131 = vsub_f32(v126, v128);
    float32x2_t v132 = vadd_f32(v127, v129);
    float32x2_t v151 = vrev64_f32(v127);
    float32x2_t v165 = vrev64_f32(v129);
    float32x2_t v83 = vadd_f32(v80, v26);
    float32x2_t v91 = vmul_f32(v80, v140);
    float32x2_t v95 = vmul_f32(v81, v144);
    float32x2_t v102 = vmul_f32(v101, v150);
    float32x2_t v108 = vrev64_f32(v82);
    float32x2_t v116 = vmul_f32(v115, v164);
    float32x2_t v133 = vadd_f32(v130, v27);
    float32x2_t v141 = vmul_f32(v130, v140);
    float32x2_t v145 = vmul_f32(v131, v144);
    float32x2_t v152 = vmul_f32(v151, v150);
    float32x2_t v158 = vrev64_f32(v132);
    float32x2_t v166 = vmul_f32(v165, v164);
    float32x2_t v109 = vmul_f32(v108, v157);
    float32x2_t v117 = vadd_f32(v83, v91);
    float32x2_t v159 = vmul_f32(v158, v157);
    float32x2_t v167 = vadd_f32(v133, v141);
    int16x4_t v178 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v83, 15), (int32x2_t){0, 0}));
    int16x4_t v184 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v133, 15), (int32x2_t){0, 0}));
    float32x2_t v118 = vadd_f32(v117, v95);
    float32x2_t v119 = vsub_f32(v117, v95);
    float32x2_t v120 = vsub_f32(v102, v109);
    float32x2_t v121 = vadd_f32(v109, v116);
    float32x2_t v168 = vadd_f32(v167, v145);
    float32x2_t v169 = vsub_f32(v167, v145);
    float32x2_t v170 = vsub_f32(v152, v159);
    float32x2_t v171 = vadd_f32(v159, v166);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v178), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v184), 0);
    float32x2_t v122 = vadd_f32(v118, v120);
    float32x2_t v123 = vsub_f32(v118, v120);
    float32x2_t v124 = vadd_f32(v119, v121);
    float32x2_t v125 = vsub_f32(v119, v121);
    float32x2_t v172 = vadd_f32(v168, v170);
    float32x2_t v173 = vsub_f32(v168, v170);
    float32x2_t v174 = vadd_f32(v169, v171);
    float32x2_t v175 = vsub_f32(v169, v171);
    int16x4_t v190 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v123, 15), (int32x2_t){0, 0}));
    int16x4_t v196 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v173, 15), (int32x2_t){0, 0}));
    int16x4_t v202 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v125, 15), (int32x2_t){0, 0}));
    int16x4_t v208 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v175, 15), (int32x2_t){0, 0}));
    int16x4_t v214 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v124, 15), (int32x2_t){0, 0}));
    int16x4_t v220 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v174, 15), (int32x2_t){0, 0}));
    int16x4_t v226 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v122, 15), (int32x2_t){0, 0}));
    int16x4_t v232 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v172, 15), (int32x2_t){0, 0}));
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v190), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v196), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v202), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v208), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v214), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v220), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v226), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v232), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu10(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v165 = -1.2500000000000000e+00F;
    float v170 = 5.5901699437494745e-01F;
    float v175 = -1.5388417685876268e+00F;
    float v182 = -5.8778525229247325e-01F;
    float v189 = -3.6327126400268028e-01F;
    const float32x2_t *v354 = &v5[v0];
    int32_t *v421 = &v6[v2];
    int64_t v26 = v0 * 5;
    int64_t v35 = v0 * 2;
    int64_t v42 = v0 * 7;
    int64_t v51 = v0 * 4;
    int64_t v58 = v0 * 9;
    int64_t v67 = v0 * 6;
    int64_t v83 = v0 * 8;
    int64_t v90 = v0 * 3;
    float v178 = v4 * v175;
    float v185 = v4 * v182;
    float v192 = v4 * v189;
    int64_t v213 = v2 * 5;
    int64_t v221 = v2 * 6;
    int64_t v237 = v2 * 2;
    int64_t v245 = v2 * 7;
    int64_t v253 = v2 * 8;
    int64_t v261 = v2 * 3;
    int64_t v269 = v2 * 4;
    int64_t v277 = v2 * 9;
    const float32x2_t *v291 = &v5[0];
    svfloat32_t v382 = svdup_n_f32(v165);
    svfloat32_t v383 = svdup_n_f32(v170);
    int32_t *v394 = &v6[0];
    svfloat32_t v493 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v354)[0]));
    const float32x2_t *v300 = &v5[v26];
    const float32x2_t *v309 = &v5[v35];
    const float32x2_t *v318 = &v5[v42];
    const float32x2_t *v327 = &v5[v51];
    const float32x2_t *v336 = &v5[v58];
    const float32x2_t *v345 = &v5[v67];
    const float32x2_t *v363 = &v5[v83];
    const float32x2_t *v372 = &v5[v90];
    svfloat32_t v384 = svdup_n_f32(v178);
    svfloat32_t v385 = svdup_n_f32(v185);
    svfloat32_t v386 = svdup_n_f32(v192);
    int32_t *v403 = &v6[v213];
    int32_t *v412 = &v6[v221];
    int32_t *v430 = &v6[v237];
    int32_t *v439 = &v6[v245];
    int32_t *v448 = &v6[v253];
    int32_t *v457 = &v6[v261];
    int32_t *v466 = &v6[v269];
    int32_t *v475 = &v6[v277];
    svfloat32_t v479 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v291)[0]));
    svfloat32_t v481 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v300)[0]));
    svfloat32_t v483 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v309)[0]));
    svfloat32_t v485 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v318)[0]));
    svfloat32_t v487 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v327)[0]));
    svfloat32_t v489 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v336)[0]));
    svfloat32_t v491 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v345)[0]));
    svfloat32_t v495 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v363)[0]));
    svfloat32_t v497 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v372)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v479, v481);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v479, v481);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v483, v485);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v483, v485);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v487, v489);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v487, v489);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v491, v493);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v491, v493);
    svfloat32_t v96 = svadd_f32_x(svptrue_b32(), v495, v497);
    svfloat32_t v97 = svsub_f32_x(svptrue_b32(), v495, v497);
    svfloat32_t v98 = svadd_f32_x(svptrue_b32(), v48, v96);
    svfloat32_t v99 = svsub_f32_x(svptrue_b32(), v48, v96);
    svfloat32_t v100 = svadd_f32_x(svptrue_b32(), v80, v64);
    svfloat32_t v101 = svsub_f32_x(svptrue_b32(), v80, v64);
    svfloat32_t v151 = svadd_f32_x(svptrue_b32(), v49, v97);
    svfloat32_t v152 = svsub_f32_x(svptrue_b32(), v49, v97);
    svfloat32_t v153 = svadd_f32_x(svptrue_b32(), v81, v65);
    svfloat32_t v154 = svsub_f32_x(svptrue_b32(), v81, v65);
    svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v98, v100);
    svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v98, v100);
    svfloat32_t v104 = svadd_f32_x(svptrue_b32(), v99, v101);
    svfloat32_t zero127 = svdup_n_f32(0);
    svfloat32_t v127 = svcmla_f32_x(pred_full, zero127, v384, v99, 90);
    svfloat32_t v155 = svadd_f32_x(svptrue_b32(), v151, v153);
    svfloat32_t v156 = svsub_f32_x(svptrue_b32(), v151, v153);
    svfloat32_t v157 = svadd_f32_x(svptrue_b32(), v152, v154);
    svfloat32_t zero180 = svdup_n_f32(0);
    svfloat32_t v180 = svcmla_f32_x(pred_full, zero180, v384, v152, 90);
    svfloat32_t v105 = svadd_f32_x(svptrue_b32(), v102, v32);
    svfloat32_t zero134 = svdup_n_f32(0);
    svfloat32_t v134 = svcmla_f32_x(pred_full, zero134, v385, v104, 90);
    svfloat32_t v158 = svadd_f32_x(svptrue_b32(), v155, v33);
    svfloat32_t zero187 = svdup_n_f32(0);
    svfloat32_t v187 = svcmla_f32_x(pred_full, zero187, v385, v157, 90);
    svfloat32_t v142 = svmla_f32_x(pred_full, v105, v102, v382);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v127, v134);
    svfloat32_t v146 = svcmla_f32_x(pred_full, v134, v386, v101, 90);
    svfloat32_t v195 = svmla_f32_x(pred_full, v158, v155, v382);
    svfloat32_t v198 = svsub_f32_x(svptrue_b32(), v180, v187);
    svfloat32_t v199 = svcmla_f32_x(pred_full, v187, v386, v154, 90);
    svint16_t v206 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v105, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v214 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v158, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v143 = svmla_f32_x(pred_full, v142, v103, v383);
    svfloat32_t v144 = svmls_f32_x(pred_full, v142, v103, v383);
    svfloat32_t v196 = svmla_f32_x(pred_full, v195, v156, v383);
    svfloat32_t v197 = svmls_f32_x(pred_full, v195, v156, v383);
    svst1w_u64(pred_full, (unsigned *)(v394), svreinterpret_u64_s16(v206));
    svst1w_u64(pred_full, (unsigned *)(v403), svreinterpret_u64_s16(v214));
    svfloat32_t v147 = svadd_f32_x(svptrue_b32(), v143, v145);
    svfloat32_t v148 = svsub_f32_x(svptrue_b32(), v143, v145);
    svfloat32_t v149 = svadd_f32_x(svptrue_b32(), v144, v146);
    svfloat32_t v150 = svsub_f32_x(svptrue_b32(), v144, v146);
    svfloat32_t v200 = svadd_f32_x(svptrue_b32(), v196, v198);
    svfloat32_t v201 = svsub_f32_x(svptrue_b32(), v196, v198);
    svfloat32_t v202 = svadd_f32_x(svptrue_b32(), v197, v199);
    svfloat32_t v203 = svsub_f32_x(svptrue_b32(), v197, v199);
    svint16_t v222 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v148, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v230 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v201, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v238 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v150, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v246 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v203, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v254 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v149, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v262 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v202, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v270 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v147, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v278 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v200, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v412), svreinterpret_u64_s16(v222));
    svst1w_u64(pred_full, (unsigned *)(v421), svreinterpret_u64_s16(v230));
    svst1w_u64(pred_full, (unsigned *)(v430), svreinterpret_u64_s16(v238));
    svst1w_u64(pred_full, (unsigned *)(v439), svreinterpret_u64_s16(v246));
    svst1w_u64(pred_full, (unsigned *)(v448), svreinterpret_u64_s16(v254));
    svst1w_u64(pred_full, (unsigned *)(v457), svreinterpret_u64_s16(v262));
    svst1w_u64(pred_full, (unsigned *)(v466), svreinterpret_u64_s16(v270));
    svst1w_u64(pred_full, (unsigned *)(v475), svreinterpret_u64_s16(v278));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu11(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v113 = 1.1000000000000001e+00F;
    float v116 = 3.3166247903554003e-01F;
    float v117 = -3.3166247903554003e-01F;
    float v124 = 5.1541501300188641e-01F;
    float v128 = 9.4125353283118118e-01F;
    float v132 = 1.4143537075597825e+00F;
    float v136 = 8.5949297361449750e-01F;
    float v140 = 4.2314838273285138e-02F;
    float v144 = 3.8639279888589606e-01F;
    float v148 = 5.1254589567200015e-01F;
    float v152 = 1.0702757469471715e+00F;
    float v156 = 5.5486073394528512e-01F;
    float v159 = 1.2412944743900585e+00F;
    float v160 = -1.2412944743900585e+00F;
    float v166 = 2.0897833842005756e-01F;
    float v167 = -2.0897833842005756e-01F;
    float v173 = 3.7415717312460811e-01F;
    float v174 = -3.7415717312460811e-01F;
    float v180 = 4.9929922194110327e-02F;
    float v181 = -4.9929922194110327e-02F;
    float v187 = 6.5815896284539266e-01F;
    float v188 = -6.5815896284539266e-01F;
    float v194 = 6.3306543373877577e-01F;
    float v195 = -6.3306543373877577e-01F;
    float v201 = 1.0822460581641109e+00F;
    float v202 = -1.0822460581641109e+00F;
    float v208 = 8.1720737907134022e-01F;
    float v209 = -8.1720737907134022e-01F;
    float v215 = 4.2408709531871824e-01F;
    float v216 = -4.2408709531871824e-01F;
    float32x2_t v218 = (float32x2_t){v4, v4};
    float32x2_t v86 = v5[0];
    float32x2_t v114 = (float32x2_t){v113, v113};
    float32x2_t v118 = (float32x2_t){v116, v117};
    float32x2_t v125 = (float32x2_t){v124, v124};
    float32x2_t v129 = (float32x2_t){v128, v128};
    float32x2_t v133 = (float32x2_t){v132, v132};
    float32x2_t v137 = (float32x2_t){v136, v136};
    float32x2_t v141 = (float32x2_t){v140, v140};
    float32x2_t v145 = (float32x2_t){v144, v144};
    float32x2_t v149 = (float32x2_t){v148, v148};
    float32x2_t v153 = (float32x2_t){v152, v152};
    float32x2_t v157 = (float32x2_t){v156, v156};
    float32x2_t v161 = (float32x2_t){v159, v160};
    float32x2_t v168 = (float32x2_t){v166, v167};
    float32x2_t v175 = (float32x2_t){v173, v174};
    float32x2_t v182 = (float32x2_t){v180, v181};
    float32x2_t v189 = (float32x2_t){v187, v188};
    float32x2_t v196 = (float32x2_t){v194, v195};
    float32x2_t v203 = (float32x2_t){v201, v202};
    float32x2_t v210 = (float32x2_t){v208, v209};
    float32x2_t v217 = (float32x2_t){v215, v216};
    float32x2_t v25 = v5[istride * 10];
    float32x2_t v31 = v5[istride * 2];
    float32x2_t v36 = v5[istride * 9];
    float32x2_t v42 = v5[istride * 3];
    float32x2_t v47 = v5[istride * 8];
    float32x2_t v53 = v5[istride * 4];
    float32x2_t v58 = v5[istride * 7];
    float32x2_t v64 = v5[istride * 5];
    float32x2_t v69 = v5[istride * 6];
    float32x2_t v120 = vmul_f32(v218, v118);
    float32x2_t v163 = vmul_f32(v218, v161);
    float32x2_t v170 = vmul_f32(v218, v168);
    float32x2_t v177 = vmul_f32(v218, v175);
    float32x2_t v184 = vmul_f32(v218, v182);
    float32x2_t v191 = vmul_f32(v218, v189);
    float32x2_t v198 = vmul_f32(v218, v196);
    float32x2_t v205 = vmul_f32(v218, v203);
    float32x2_t v212 = vmul_f32(v218, v210);
    float32x2_t v219 = vmul_f32(v218, v217);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v37 = vadd_f32(v31, v36);
    float32x2_t v48 = vadd_f32(v42, v47);
    float32x2_t v59 = vadd_f32(v53, v58);
    float32x2_t v70 = vadd_f32(v64, v69);
    float32x2_t v71 = vsub_f32(v20, v25);
    float32x2_t v72 = vsub_f32(v31, v36);
    float32x2_t v73 = vsub_f32(v42, v47);
    float32x2_t v74 = vsub_f32(v53, v58);
    float32x2_t v75 = vsub_f32(v64, v69);
    float32x2_t v76 = vadd_f32(v26, v37);
    float32x2_t v77 = vadd_f32(v48, v70);
    float32x2_t v79 = vsub_f32(v72, v73);
    float32x2_t v80 = vadd_f32(v71, v75);
    float32x2_t v90 = vsub_f32(v37, v59);
    float32x2_t v91 = vsub_f32(v26, v59);
    float32x2_t v92 = vsub_f32(v37, v26);
    float32x2_t v93 = vsub_f32(v70, v59);
    float32x2_t v94 = vsub_f32(v48, v59);
    float32x2_t v95 = vsub_f32(v70, v48);
    float32x2_t v96 = vsub_f32(v37, v70);
    float32x2_t v97 = vsub_f32(v26, v48);
    float32x2_t v99 = vadd_f32(v72, v74);
    float32x2_t v100 = vsub_f32(v71, v74);
    float32x2_t v101 = vadd_f32(v71, v72);
    float32x2_t v102 = vsub_f32(v74, v75);
    float32x2_t v103 = vsub_f32(v73, v74);
    float32x2_t v104 = vsub_f32(v73, v75);
    float32x2_t v105 = vadd_f32(v72, v75);
    float32x2_t v106 = vsub_f32(v71, v73);
    float32x2_t v78 = vadd_f32(v59, v76);
    float32x2_t v88 = vsub_f32(v79, v80);
    float32x2_t v98 = vsub_f32(v77, v76);
    float32x2_t v107 = vadd_f32(v79, v80);
    float32x2_t v126 = vmul_f32(v90, v125);
    float32x2_t v130 = vmul_f32(v91, v129);
    float32x2_t v134 = vmul_f32(v92, v133);
    float32x2_t v138 = vmul_f32(v93, v137);
    float32x2_t v142 = vmul_f32(v94, v141);
    float32x2_t v146 = vmul_f32(v95, v145);
    float32x2_t v150 = vmul_f32(v96, v149);
    float32x2_t v154 = vmul_f32(v97, v153);
    float32x2_t v164 = vrev64_f32(v99);
    float32x2_t v171 = vrev64_f32(v100);
    float32x2_t v178 = vrev64_f32(v101);
    float32x2_t v185 = vrev64_f32(v102);
    float32x2_t v192 = vrev64_f32(v103);
    float32x2_t v199 = vrev64_f32(v104);
    float32x2_t v206 = vrev64_f32(v105);
    float32x2_t v213 = vrev64_f32(v106);
    float32x2_t v81 = vadd_f32(v78, v77);
    float32x2_t v89 = vsub_f32(v88, v74);
    float32x2_t v158 = vmul_f32(v98, v157);
    float32x2_t v165 = vmul_f32(v164, v163);
    float32x2_t v172 = vmul_f32(v171, v170);
    float32x2_t v179 = vmul_f32(v178, v177);
    float32x2_t v186 = vmul_f32(v185, v184);
    float32x2_t v193 = vmul_f32(v192, v191);
    float32x2_t v200 = vmul_f32(v199, v198);
    float32x2_t v207 = vmul_f32(v206, v205);
    float32x2_t v214 = vmul_f32(v213, v212);
    float32x2_t v220 = vrev64_f32(v107);
    float32x2_t v223 = vadd_f32(v126, v130);
    float32x2_t v224 = vadd_f32(v130, v134);
    float32x2_t v225 = vsub_f32(v126, v134);
    float32x2_t v226 = vadd_f32(v138, v142);
    float32x2_t v227 = vadd_f32(v142, v146);
    float32x2_t v228 = vsub_f32(v138, v146);
    float32x2_t v87 = vadd_f32(v86, v81);
    float32x2_t v115 = vmul_f32(v81, v114);
    float32x2_t v121 = vrev64_f32(v89);
    float32x2_t v221 = vmul_f32(v220, v219);
    float32x2_t v229 = vadd_f32(v154, v158);
    float32x2_t v230 = vadd_f32(v150, v158);
    float32x2_t v231 = vadd_f32(v172, v179);
    float32x2_t v232 = vsub_f32(v165, v179);
    float32x2_t v233 = vadd_f32(v193, v200);
    float32x2_t v234 = vsub_f32(v186, v200);
    float32x2_t v122 = vmul_f32(v121, v120);
    float32x2_t v222 = vsub_f32(v87, v115);
    float32x2_t v235 = vadd_f32(v214, v221);
    float32x2_t v236 = vsub_f32(v207, v221);
    float32x2_t v237 = vadd_f32(v227, v229);
    float32x2_t v255 = vadd_f32(v231, v232);
    int16x4_t v271 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v87, 15), (int32x2_t){0, 0}));
    float32x2_t v238 = vadd_f32(v237, v222);
    float32x2_t v239 = vsub_f32(v222, v224);
    float32x2_t v241 = vadd_f32(v222, v228);
    float32x2_t v243 = vsub_f32(v222, v225);
    float32x2_t v245 = vadd_f32(v222, v223);
    float32x2_t v247 = vadd_f32(v122, v233);
    float32x2_t v249 = vsub_f32(v235, v231);
    float32x2_t v251 = vadd_f32(v122, v236);
    float32x2_t v253 = vsub_f32(v236, v232);
    float32x2_t v256 = vadd_f32(v255, v233);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v271), 0);
    float32x2_t v240 = vsub_f32(v239, v229);
    float32x2_t v242 = vadd_f32(v241, v230);
    float32x2_t v244 = vsub_f32(v243, v230);
    float32x2_t v246 = vsub_f32(v245, v226);
    float32x2_t v248 = vadd_f32(v247, v235);
    float32x2_t v250 = vsub_f32(v249, v122);
    float32x2_t v252 = vadd_f32(v251, v234);
    float32x2_t v254 = vsub_f32(v253, v122);
    float32x2_t v257 = vadd_f32(v256, v234);
    float32x2_t v258 = vsub_f32(v257, v122);
    float32x2_t v260 = vadd_f32(v238, v248);
    float32x2_t v261 = vadd_f32(v240, v250);
    float32x2_t v262 = vsub_f32(v242, v252);
    float32x2_t v263 = vadd_f32(v244, v254);
    float32x2_t v264 = vsub_f32(v244, v254);
    float32x2_t v265 = vadd_f32(v242, v252);
    float32x2_t v266 = vsub_f32(v240, v250);
    float32x2_t v267 = vsub_f32(v238, v248);
    float32x2_t v259 = vadd_f32(v246, v258);
    float32x2_t v268 = vsub_f32(v246, v258);
    int16x4_t v283 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v260, 15), (int32x2_t){0, 0}));
    int16x4_t v289 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v261, 15), (int32x2_t){0, 0}));
    int16x4_t v295 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v262, 15), (int32x2_t){0, 0}));
    int16x4_t v301 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v263, 15), (int32x2_t){0, 0}));
    int16x4_t v307 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v264, 15), (int32x2_t){0, 0}));
    int16x4_t v313 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v265, 15), (int32x2_t){0, 0}));
    int16x4_t v319 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v266, 15), (int32x2_t){0, 0}));
    int16x4_t v325 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v267, 15), (int32x2_t){0, 0}));
    int16x4_t v277 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v259, 15), (int32x2_t){0, 0}));
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v283), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v289), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v295), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v301), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v307), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v313), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v319), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v325), 0);
    int16x4_t v331 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v268, 15), (int32x2_t){0, 0}));
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v277), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v331), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu11(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v138 = 1.1000000000000001e+00F;
    float v143 = -3.3166247903554003e-01F;
    float v150 = 5.1541501300188641e-01F;
    float v155 = 9.4125353283118118e-01F;
    float v160 = 1.4143537075597825e+00F;
    float v165 = 8.5949297361449750e-01F;
    float v170 = 4.2314838273285138e-02F;
    float v175 = 3.8639279888589606e-01F;
    float v180 = 5.1254589567200015e-01F;
    float v185 = 1.0702757469471715e+00F;
    float v190 = 5.5486073394528512e-01F;
    float v195 = -1.2412944743900585e+00F;
    float v202 = -2.0897833842005756e-01F;
    float v209 = -3.7415717312460811e-01F;
    float v216 = -4.9929922194110327e-02F;
    float v223 = -6.5815896284539266e-01F;
    float v230 = -6.3306543373877577e-01F;
    float v237 = -1.0822460581641109e+00F;
    float v244 = -8.1720737907134022e-01F;
    float v251 = -4.2408709531871824e-01F;
    const float32x2_t *v398 = &v5[v0];
    int32_t *v610 = &v6[v2];
    int64_t v26 = v0 * 10;
    int64_t v34 = v0 * 2;
    int64_t v41 = v0 * 9;
    int64_t v49 = v0 * 3;
    int64_t v56 = v0 * 8;
    int64_t v64 = v0 * 4;
    int64_t v71 = v0 * 7;
    int64_t v79 = v0 * 5;
    int64_t v86 = v0 * 6;
    float v146 = v4 * v143;
    float v198 = v4 * v195;
    float v205 = v4 * v202;
    float v212 = v4 * v209;
    float v219 = v4 * v216;
    float v226 = v4 * v223;
    float v233 = v4 * v230;
    float v240 = v4 * v237;
    float v247 = v4 * v244;
    float v254 = v4 * v251;
    int64_t v313 = v2 * 10;
    int64_t v321 = v2 * 9;
    int64_t v329 = v2 * 8;
    int64_t v337 = v2 * 7;
    int64_t v345 = v2 * 6;
    int64_t v353 = v2 * 5;
    int64_t v361 = v2 * 4;
    int64_t v369 = v2 * 3;
    int64_t v377 = v2 * 2;
    const float32x2_t *v489 = &v5[0];
    svfloat32_t v493 = svdup_n_f32(v138);
    svfloat32_t v495 = svdup_n_f32(v150);
    svfloat32_t v496 = svdup_n_f32(v155);
    svfloat32_t v497 = svdup_n_f32(v160);
    svfloat32_t v498 = svdup_n_f32(v165);
    svfloat32_t v499 = svdup_n_f32(v170);
    svfloat32_t v500 = svdup_n_f32(v175);
    svfloat32_t v501 = svdup_n_f32(v180);
    svfloat32_t v502 = svdup_n_f32(v185);
    svfloat32_t v503 = svdup_n_f32(v190);
    int32_t *v520 = &v6[0];
    svfloat32_t v614 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v398)[0]));
    const float32x2_t *v407 = &v5[v26];
    const float32x2_t *v416 = &v5[v34];
    const float32x2_t *v425 = &v5[v41];
    const float32x2_t *v434 = &v5[v49];
    const float32x2_t *v443 = &v5[v56];
    const float32x2_t *v452 = &v5[v64];
    const float32x2_t *v461 = &v5[v71];
    const float32x2_t *v470 = &v5[v79];
    const float32x2_t *v479 = &v5[v86];
    svfloat32_t v494 = svdup_n_f32(v146);
    svfloat32_t v504 = svdup_n_f32(v198);
    svfloat32_t v505 = svdup_n_f32(v205);
    svfloat32_t v506 = svdup_n_f32(v212);
    svfloat32_t v507 = svdup_n_f32(v219);
    svfloat32_t v508 = svdup_n_f32(v226);
    svfloat32_t v509 = svdup_n_f32(v233);
    svfloat32_t v510 = svdup_n_f32(v240);
    svfloat32_t v511 = svdup_n_f32(v247);
    svfloat32_t v512 = svdup_n_f32(v254);
    int32_t *v529 = &v6[v313];
    int32_t *v538 = &v6[v321];
    int32_t *v547 = &v6[v329];
    int32_t *v556 = &v6[v337];
    int32_t *v565 = &v6[v345];
    int32_t *v574 = &v6[v353];
    int32_t *v583 = &v6[v361];
    int32_t *v592 = &v6[v369];
    int32_t *v601 = &v6[v377];
    svfloat32_t v634 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v489)[0]));
    svfloat32_t v616 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v407)[0]));
    svfloat32_t v618 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v416)[0]));
    svfloat32_t v620 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v425)[0]));
    svfloat32_t v622 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v434)[0]));
    svfloat32_t v624 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v443)[0]));
    svfloat32_t v626 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v452)[0]));
    svfloat32_t v628 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v461)[0]));
    svfloat32_t v630 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v470)[0]));
    svfloat32_t v632 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v479)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v614, v616);
    svfloat32_t v47 = svadd_f32_x(svptrue_b32(), v618, v620);
    svfloat32_t v62 = svadd_f32_x(svptrue_b32(), v622, v624);
    svfloat32_t v77 = svadd_f32_x(svptrue_b32(), v626, v628);
    svfloat32_t v92 = svadd_f32_x(svptrue_b32(), v630, v632);
    svfloat32_t v93 = svsub_f32_x(svptrue_b32(), v614, v616);
    svfloat32_t v94 = svsub_f32_x(svptrue_b32(), v618, v620);
    svfloat32_t v95 = svsub_f32_x(svptrue_b32(), v622, v624);
    svfloat32_t v96 = svsub_f32_x(svptrue_b32(), v626, v628);
    svfloat32_t v97 = svsub_f32_x(svptrue_b32(), v630, v632);
    svfloat32_t v98 = svadd_f32_x(svptrue_b32(), v32, v47);
    svfloat32_t v99 = svadd_f32_x(svptrue_b32(), v62, v92);
    svfloat32_t v101 = svsub_f32_x(svptrue_b32(), v94, v95);
    svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v93, v97);
    svfloat32_t v114 = svsub_f32_x(svptrue_b32(), v47, v77);
    svfloat32_t v115 = svsub_f32_x(svptrue_b32(), v32, v77);
    svfloat32_t v116 = svsub_f32_x(svptrue_b32(), v47, v32);
    svfloat32_t v117 = svsub_f32_x(svptrue_b32(), v92, v77);
    svfloat32_t v118 = svsub_f32_x(svptrue_b32(), v62, v77);
    svfloat32_t v119 = svsub_f32_x(svptrue_b32(), v92, v62);
    svfloat32_t v120 = svsub_f32_x(svptrue_b32(), v47, v92);
    svfloat32_t v121 = svsub_f32_x(svptrue_b32(), v32, v62);
    svfloat32_t v123 = svadd_f32_x(svptrue_b32(), v94, v96);
    svfloat32_t v124 = svsub_f32_x(svptrue_b32(), v93, v96);
    svfloat32_t v125 = svadd_f32_x(svptrue_b32(), v93, v94);
    svfloat32_t v126 = svsub_f32_x(svptrue_b32(), v96, v97);
    svfloat32_t v127 = svsub_f32_x(svptrue_b32(), v95, v96);
    svfloat32_t v128 = svsub_f32_x(svptrue_b32(), v95, v97);
    svfloat32_t v129 = svadd_f32_x(svptrue_b32(), v94, v97);
    svfloat32_t v130 = svsub_f32_x(svptrue_b32(), v93, v95);
    svfloat32_t v100 = svadd_f32_x(svptrue_b32(), v77, v98);
    svfloat32_t v112 = svsub_f32_x(svptrue_b32(), v101, v102);
    svfloat32_t v122 = svsub_f32_x(svptrue_b32(), v99, v98);
    svfloat32_t v131 = svadd_f32_x(svptrue_b32(), v101, v102);
    svfloat32_t v158 = svmul_f32_x(svptrue_b32(), v115, v496);
    svfloat32_t v163 = svmul_f32_x(svptrue_b32(), v116, v497);
    svfloat32_t v173 = svmul_f32_x(svptrue_b32(), v118, v499);
    svfloat32_t v178 = svmul_f32_x(svptrue_b32(), v119, v500);
    svfloat32_t zero200 = svdup_n_f32(0);
    svfloat32_t v200 = svcmla_f32_x(pred_full, zero200, v504, v123, 90);
    svfloat32_t zero214 = svdup_n_f32(0);
    svfloat32_t v214 = svcmla_f32_x(pred_full, zero214, v506, v125, 90);
    svfloat32_t zero221 = svdup_n_f32(0);
    svfloat32_t v221 = svcmla_f32_x(pred_full, zero221, v507, v126, 90);
    svfloat32_t zero235 = svdup_n_f32(0);
    svfloat32_t v235 = svcmla_f32_x(pred_full, zero235, v509, v128, 90);
    svfloat32_t zero242 = svdup_n_f32(0);
    svfloat32_t v242 = svcmla_f32_x(pred_full, zero242, v510, v129, 90);
    svfloat32_t v103 = svadd_f32_x(svptrue_b32(), v100, v99);
    svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v112, v96);
    svfloat32_t v193 = svmul_f32_x(svptrue_b32(), v122, v503);
    svfloat32_t zero256 = svdup_n_f32(0);
    svfloat32_t v256 = svcmla_f32_x(pred_full, zero256, v512, v131, 90);
    svfloat32_t v258 = svmla_f32_x(pred_full, v158, v114, v495);
    svfloat32_t v259 = svmla_f32_x(pred_full, v163, v115, v496);
    svfloat32_t v260 = svnmls_f32_x(pred_full, v163, v114, v495);
    svfloat32_t v261 = svmla_f32_x(pred_full, v173, v117, v498);
    svfloat32_t v262 = svmla_f32_x(pred_full, v178, v118, v499);
    svfloat32_t v263 = svnmls_f32_x(pred_full, v178, v117, v498);
    svfloat32_t v266 = svcmla_f32_x(pred_full, v214, v505, v124, 90);
    svfloat32_t v267 = svsub_f32_x(svptrue_b32(), v200, v214);
    svfloat32_t v268 = svcmla_f32_x(pred_full, v235, v508, v127, 90);
    svfloat32_t v269 = svsub_f32_x(svptrue_b32(), v221, v235);
    svfloat32_t v111 = svadd_f32_x(svptrue_b32(), v634, v103);
    svfloat32_t zero148 = svdup_n_f32(0);
    svfloat32_t v148 = svcmla_f32_x(pred_full, zero148, v494, v113, 90);
    svfloat32_t v264 = svmla_f32_x(pred_full, v193, v121, v502);
    svfloat32_t v265 = svmla_f32_x(pred_full, v193, v120, v501);
    svfloat32_t v270 = svcmla_f32_x(pred_full, v256, v511, v130, 90);
    svfloat32_t v271 = svsub_f32_x(svptrue_b32(), v242, v256);
    svfloat32_t v290 = svadd_f32_x(svptrue_b32(), v266, v267);
    svfloat32_t v257 = svmls_f32_x(pred_full, v111, v103, v493);
    svfloat32_t v272 = svadd_f32_x(svptrue_b32(), v262, v264);
    svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v148, v268);
    svfloat32_t v284 = svsub_f32_x(svptrue_b32(), v270, v266);
    svfloat32_t v286 = svadd_f32_x(svptrue_b32(), v148, v271);
    svfloat32_t v288 = svsub_f32_x(svptrue_b32(), v271, v267);
    svfloat32_t v291 = svadd_f32_x(svptrue_b32(), v290, v268);
    svint16_t v306 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v111, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v273 = svadd_f32_x(svptrue_b32(), v272, v257);
    svfloat32_t v274 = svsub_f32_x(svptrue_b32(), v257, v259);
    svfloat32_t v276 = svadd_f32_x(svptrue_b32(), v257, v263);
    svfloat32_t v278 = svsub_f32_x(svptrue_b32(), v257, v260);
    svfloat32_t v280 = svadd_f32_x(svptrue_b32(), v257, v258);
    svfloat32_t v283 = svadd_f32_x(svptrue_b32(), v282, v270);
    svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v284, v148);
    svfloat32_t v287 = svadd_f32_x(svptrue_b32(), v286, v269);
    svfloat32_t v289 = svsub_f32_x(svptrue_b32(), v288, v148);
    svfloat32_t v292 = svadd_f32_x(svptrue_b32(), v291, v269);
    svst1w_u64(pred_full, (unsigned *)(v520), svreinterpret_u64_s16(v306));
    svfloat32_t v275 = svsub_f32_x(svptrue_b32(), v274, v264);
    svfloat32_t v277 = svadd_f32_x(svptrue_b32(), v276, v265);
    svfloat32_t v279 = svsub_f32_x(svptrue_b32(), v278, v265);
    svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v280, v261);
    svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v292, v148);
    svfloat32_t v295 = svadd_f32_x(svptrue_b32(), v273, v283);
    svfloat32_t v302 = svsub_f32_x(svptrue_b32(), v273, v283);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v281, v293);
    svfloat32_t v296 = svadd_f32_x(svptrue_b32(), v275, v285);
    svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v277, v287);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v279, v289);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v279, v289);
    svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v277, v287);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v275, v285);
    svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v281, v293);
    svint16_t v322 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v295, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v378 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v302, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v314 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v294, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v330 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v296, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v338 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v297, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v346 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v298, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v354 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v299, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v362 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v300, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v370 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v301, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v386 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v303, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v538), svreinterpret_u64_s16(v322));
    svst1w_u64(pred_full, (unsigned *)(v601), svreinterpret_u64_s16(v378));
    svst1w_u64(pred_full, (unsigned *)(v529), svreinterpret_u64_s16(v314));
    svst1w_u64(pred_full, (unsigned *)(v547), svreinterpret_u64_s16(v330));
    svst1w_u64(pred_full, (unsigned *)(v556), svreinterpret_u64_s16(v338));
    svst1w_u64(pred_full, (unsigned *)(v565), svreinterpret_u64_s16(v346));
    svst1w_u64(pred_full, (unsigned *)(v574), svreinterpret_u64_s16(v354));
    svst1w_u64(pred_full, (unsigned *)(v583), svreinterpret_u64_s16(v362));
    svst1w_u64(pred_full, (unsigned *)(v592), svreinterpret_u64_s16(v370));
    svst1w_u64(pred_full, (unsigned *)(v610), svreinterpret_u64_s16(v386));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu12(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v74 = v5[istride];
    float v106 = 1.0000000000000000e+00F;
    float v107 = -1.0000000000000000e+00F;
    float v133 = -1.4999999999999998e+00F;
    float v134 = 1.4999999999999998e+00F;
    float v162 = 8.6602540378443871e-01F;
    float32x2_t v165 = (float32x2_t){v4, v4};
    float v170 = -8.6602540378443871e-01F;
    float32x2_t v32 = v5[0];
    float32x2_t v108 = (float32x2_t){v106, v107};
    float32x2_t v131 = (float32x2_t){v133, v133};
    float32x2_t v135 = (float32x2_t){v133, v134};
    float32x2_t v164 = (float32x2_t){v162, v170};
    float32x2_t v171 = (float32x2_t){v170, v170};
    float32x2_t v20 = v5[istride * 4];
    float32x2_t v25 = v5[istride * 8];
    float32x2_t v38 = v5[istride * 7];
    float32x2_t v43 = v5[istride * 11];
    float32x2_t v50 = v5[istride * 3];
    float32x2_t v56 = v5[istride * 10];
    float32x2_t v61 = v5[istride * 2];
    float32x2_t v68 = v5[istride * 6];
    float32x2_t v79 = v5[istride * 5];
    float32x2_t v86 = v5[istride * 9];
    float32x2_t v110 = vmul_f32(v165, v108);
    float32x2_t v137 = vmul_f32(v165, v135);
    float32x2_t v166 = vmul_f32(v165, v164);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v44 = vadd_f32(v38, v43);
    float32x2_t v45 = vsub_f32(v38, v43);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v80 = vadd_f32(v74, v79);
    float32x2_t v81 = vsub_f32(v74, v79);
    float32x2_t v33 = vadd_f32(v26, v32);
    float32x2_t v51 = vadd_f32(v44, v50);
    float32x2_t v69 = vadd_f32(v62, v68);
    float32x2_t v87 = vadd_f32(v80, v86);
    float32x2_t v115 = vadd_f32(v26, v62);
    float32x2_t v116 = vsub_f32(v26, v62);
    float32x2_t v117 = vadd_f32(v44, v80);
    float32x2_t v118 = vsub_f32(v44, v80);
    float32x2_t v142 = vadd_f32(v27, v63);
    float32x2_t v143 = vsub_f32(v27, v63);
    float32x2_t v144 = vadd_f32(v45, v81);
    float32x2_t v145 = vsub_f32(v45, v81);
    float32x2_t v88 = vadd_f32(v33, v69);
    float32x2_t v89 = vsub_f32(v33, v69);
    float32x2_t v90 = vadd_f32(v51, v87);
    float32x2_t v91 = vsub_f32(v51, v87);
    float32x2_t v119 = vadd_f32(v115, v117);
    float32x2_t v120 = vsub_f32(v115, v117);
    float32x2_t v132 = vmul_f32(v116, v131);
    float32x2_t v138 = vrev64_f32(v118);
    float32x2_t v146 = vadd_f32(v142, v144);
    float32x2_t v147 = vsub_f32(v142, v144);
    float32x2_t v167 = vrev64_f32(v143);
    float32x2_t v172 = vmul_f32(v145, v171);
    float32x2_t v92 = vadd_f32(v88, v90);
    float32x2_t v93 = vsub_f32(v88, v90);
    float32x2_t v111 = vrev64_f32(v91);
    float32x2_t v124 = vmul_f32(v119, v131);
    float32x2_t v128 = vmul_f32(v120, v131);
    float32x2_t v139 = vmul_f32(v138, v137);
    float32x2_t v153 = vrev64_f32(v146);
    float32x2_t v160 = vrev64_f32(v147);
    float32x2_t v168 = vmul_f32(v167, v166);
    float32x2_t v112 = vmul_f32(v111, v110);
    float32x2_t v140 = vadd_f32(v132, v139);
    float32x2_t v141 = vsub_f32(v132, v139);
    float32x2_t v154 = vmul_f32(v153, v166);
    float32x2_t v161 = vmul_f32(v160, v166);
    float32x2_t v173 = vadd_f32(v168, v172);
    float32x2_t v174 = vsub_f32(v168, v172);
    float32x2_t v175 = vadd_f32(v92, v124);
    int16x4_t v180 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v92, 15), (int32x2_t){0, 0}));
    float32x2_t v217 = vadd_f32(v93, v128);
    int16x4_t v222 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v93, 15), (int32x2_t){0, 0}));
    float32x2_t v113 = vadd_f32(v89, v112);
    float32x2_t v114 = vsub_f32(v89, v112);
    float32x2_t v176 = vadd_f32(v175, v154);
    float32x2_t v177 = vsub_f32(v175, v154);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v180), 0);
    float32x2_t v218 = vadd_f32(v217, v161);
    float32x2_t v219 = vsub_f32(v217, v161);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v222), 0);
    int16x4_t v186 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v177, 15), (int32x2_t){0, 0}));
    int16x4_t v192 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v176, 15), (int32x2_t){0, 0}));
    float32x2_t v196 = vadd_f32(v114, v141);
    int16x4_t v201 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v114, 15), (int32x2_t){0, 0}));
    int16x4_t v228 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v219, 15), (int32x2_t){0, 0}));
    int16x4_t v234 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v218, 15), (int32x2_t){0, 0}));
    float32x2_t v238 = vadd_f32(v113, v140);
    int16x4_t v243 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v113, 15), (int32x2_t){0, 0}));
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v186), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v192), 0);
    float32x2_t v197 = vadd_f32(v196, v174);
    float32x2_t v198 = vsub_f32(v196, v174);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v201), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v228), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v234), 0);
    float32x2_t v239 = vadd_f32(v238, v173);
    float32x2_t v240 = vsub_f32(v238, v173);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v243), 0);
    int16x4_t v207 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v198, 15), (int32x2_t){0, 0}));
    int16x4_t v213 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v197, 15), (int32x2_t){0, 0}));
    int16x4_t v249 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v240, 15), (int32x2_t){0, 0}));
    int16x4_t v255 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v239, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v207), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v213), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v249), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v255), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu12(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v136 = -1.0000000000000000e+00F;
    float v161 = -1.4999999999999998e+00F;
    float v166 = 1.4999999999999998e+00F;
    float v202 = -8.6602540378443871e-01F;
    const float32x2_t *v404 = &v5[v0];
    int32_t *v480 = &v6[v2];
    int64_t v19 = v0 * 4;
    int64_t v26 = v0 * 8;
    int64_t v43 = v0 * 7;
    int64_t v50 = v0 * 11;
    int64_t v59 = v0 * 3;
    int64_t v67 = v0 * 10;
    int64_t v74 = v0 * 2;
    int64_t v83 = v0 * 6;
    int64_t v98 = v0 * 5;
    int64_t v107 = v0 * 9;
    float v139 = v4 * v136;
    float v169 = v4 * v166;
    float v198 = v4 * v202;
    int64_t v220 = v2 * 4;
    int64_t v228 = v2 * 8;
    int64_t v239 = v2 * 9;
    int64_t v255 = v2 * 5;
    int64_t v266 = v2 * 6;
    int64_t v274 = v2 * 10;
    int64_t v282 = v2 * 2;
    int64_t v293 = v2 * 3;
    int64_t v301 = v2 * 7;
    int64_t v309 = v2 * 11;
    const float32x2_t *v341 = &v5[0];
    svfloat32_t v431 = svdup_n_f32(v161);
    svfloat32_t v436 = svdup_n_f32(v202);
    int32_t *v444 = &v6[0];
    svfloat32_t v565 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v404)[0]));
    const float32x2_t *v322 = &v5[v19];
    const float32x2_t *v331 = &v5[v26];
    const float32x2_t *v350 = &v5[v43];
    const float32x2_t *v359 = &v5[v50];
    const float32x2_t *v368 = &v5[v59];
    const float32x2_t *v377 = &v5[v67];
    const float32x2_t *v386 = &v5[v74];
    const float32x2_t *v395 = &v5[v83];
    const float32x2_t *v413 = &v5[v98];
    const float32x2_t *v422 = &v5[v107];
    svfloat32_t v428 = svdup_n_f32(v139);
    svfloat32_t v432 = svdup_n_f32(v169);
    svfloat32_t v435 = svdup_n_f32(v198);
    int32_t *v453 = &v6[v220];
    int32_t *v462 = &v6[v228];
    int32_t *v471 = &v6[v239];
    int32_t *v489 = &v6[v255];
    int32_t *v498 = &v6[v266];
    int32_t *v507 = &v6[v274];
    int32_t *v516 = &v6[v282];
    int32_t *v525 = &v6[v293];
    int32_t *v534 = &v6[v301];
    int32_t *v543 = &v6[v309];
    svfloat32_t v551 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v341)[0]));
    svfloat32_t v547 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v322)[0]));
    svfloat32_t v549 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v331)[0]));
    svfloat32_t v553 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v350)[0]));
    svfloat32_t v555 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v359)[0]));
    svfloat32_t v557 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v368)[0]));
    svfloat32_t v559 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v377)[0]));
    svfloat32_t v561 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v386)[0]));
    svfloat32_t v563 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v395)[0]));
    svfloat32_t v567 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v413)[0]));
    svfloat32_t v569 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v422)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v547, v549);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v547, v549);
    svfloat32_t v56 = svadd_f32_x(svptrue_b32(), v553, v555);
    svfloat32_t v57 = svsub_f32_x(svptrue_b32(), v553, v555);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v559, v561);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v559, v561);
    svfloat32_t v104 = svadd_f32_x(svptrue_b32(), v565, v567);
    svfloat32_t v105 = svsub_f32_x(svptrue_b32(), v565, v567);
    svfloat32_t v41 = svadd_f32_x(svptrue_b32(), v32, v551);
    svfloat32_t v65 = svadd_f32_x(svptrue_b32(), v56, v557);
    svfloat32_t v89 = svadd_f32_x(svptrue_b32(), v80, v563);
    svfloat32_t v113 = svadd_f32_x(svptrue_b32(), v104, v569);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v32, v80);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v32, v80);
    svfloat32_t v146 = svadd_f32_x(svptrue_b32(), v56, v104);
    svfloat32_t v147 = svsub_f32_x(svptrue_b32(), v56, v104);
    svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v33, v81);
    svfloat32_t v175 = svsub_f32_x(svptrue_b32(), v33, v81);
    svfloat32_t v176 = svadd_f32_x(svptrue_b32(), v57, v105);
    svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v57, v105);
    svfloat32_t v114 = svadd_f32_x(svptrue_b32(), v41, v89);
    svfloat32_t v115 = svsub_f32_x(svptrue_b32(), v41, v89);
    svfloat32_t v116 = svadd_f32_x(svptrue_b32(), v65, v113);
    svfloat32_t v117 = svsub_f32_x(svptrue_b32(), v65, v113);
    svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v144, v146);
    svfloat32_t v149 = svsub_f32_x(svptrue_b32(), v144, v146);
    svfloat32_t zero171 = svdup_n_f32(0);
    svfloat32_t v171 = svcmla_f32_x(pred_full, zero171, v432, v147, 90);
    svfloat32_t v178 = svadd_f32_x(svptrue_b32(), v174, v176);
    svfloat32_t v179 = svsub_f32_x(svptrue_b32(), v174, v176);
    svfloat32_t zero200 = svdup_n_f32(0);
    svfloat32_t v200 = svcmla_f32_x(pred_full, zero200, v435, v175, 90);
    svfloat32_t v118 = svadd_f32_x(svptrue_b32(), v114, v116);
    svfloat32_t v119 = svsub_f32_x(svptrue_b32(), v114, v116);
    svfloat32_t zero141 = svdup_n_f32(0);
    svfloat32_t v141 = svcmla_f32_x(pred_full, zero141, v428, v117, 90);
    svfloat32_t v172 = svmla_f32_x(pred_full, v171, v145, v431);
    svfloat32_t v173 = svnmls_f32_x(pred_full, v171, v145, v431);
    svfloat32_t zero186 = svdup_n_f32(0);
    svfloat32_t v186 = svcmla_f32_x(pred_full, zero186, v435, v178, 90);
    svfloat32_t zero193 = svdup_n_f32(0);
    svfloat32_t v193 = svcmla_f32_x(pred_full, zero193, v435, v179, 90);
    svfloat32_t v206 = svmla_f32_x(pred_full, v200, v177, v436);
    svfloat32_t v207 = svmls_f32_x(pred_full, v200, v177, v436);
    svfloat32_t v142 = svadd_f32_x(svptrue_b32(), v115, v141);
    svfloat32_t v143 = svsub_f32_x(svptrue_b32(), v115, v141);
    svfloat32_t v208 = svmla_f32_x(pred_full, v118, v148, v431);
    svint16_t v213 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v118, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v262 = svmla_f32_x(pred_full, v119, v149, v431);
    svint16_t v267 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v119, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v209 = svadd_f32_x(svptrue_b32(), v208, v186);
    svfloat32_t v210 = svsub_f32_x(svptrue_b32(), v208, v186);
    svfloat32_t v235 = svadd_f32_x(svptrue_b32(), v143, v173);
    svint16_t v240 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v143, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v263 = svadd_f32_x(svptrue_b32(), v262, v193);
    svfloat32_t v264 = svsub_f32_x(svptrue_b32(), v262, v193);
    svfloat32_t v289 = svadd_f32_x(svptrue_b32(), v142, v172);
    svint16_t v294 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v142, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v444), svreinterpret_u64_s16(v213));
    svst1w_u64(pred_full, (unsigned *)(v498), svreinterpret_u64_s16(v267));
    svint16_t v221 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v210, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v229 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v209, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v236 = svadd_f32_x(svptrue_b32(), v235, v207);
    svfloat32_t v237 = svsub_f32_x(svptrue_b32(), v235, v207);
    svint16_t v275 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v264, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v283 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v263, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v290 = svadd_f32_x(svptrue_b32(), v289, v206);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v289, v206);
    svst1w_u64(pred_full, (unsigned *)(v471), svreinterpret_u64_s16(v240));
    svst1w_u64(pred_full, (unsigned *)(v525), svreinterpret_u64_s16(v294));
    svint16_t v248 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v237, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v256 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v236, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v302 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v291, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v310 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v290, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v453), svreinterpret_u64_s16(v221));
    svst1w_u64(pred_full, (unsigned *)(v462), svreinterpret_u64_s16(v229));
    svst1w_u64(pred_full, (unsigned *)(v507), svreinterpret_u64_s16(v275));
    svst1w_u64(pred_full, (unsigned *)(v516), svreinterpret_u64_s16(v283));
    svst1w_u64(pred_full, (unsigned *)(v480), svreinterpret_u64_s16(v248));
    svst1w_u64(pred_full, (unsigned *)(v489), svreinterpret_u64_s16(v256));
    svst1w_u64(pred_full, (unsigned *)(v534), svreinterpret_u64_s16(v302));
    svst1w_u64(pred_full, (unsigned *)(v543), svreinterpret_u64_s16(v310));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu13(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v129 = 1.0833333333333333e+00F;
    float v133 = -3.0046260628866578e-01F;
    float v136 = 7.4927933062613905e-01F;
    float v137 = -7.4927933062613905e-01F;
    float v143 = 4.0100212832186721e-01F;
    float v144 = -4.0100212832186721e-01F;
    float v150 = 5.7514072947400308e-01F;
    float v151 = -5.7514072947400308e-01F;
    float v158 = 5.2422663952658211e-01F;
    float v162 = 5.1652078062348972e-01F;
    float v166 = 7.7058589030924258e-03F;
    float v170 = 4.2763404682656941e-01F;
    float v174 = 1.5180597207438440e-01F;
    float v178 = 5.7944001890096386e-01F;
    float v181 = 1.1543953381323635e+00F;
    float v182 = -1.1543953381323635e+00F;
    float v188 = 9.0655220171271012e-01F;
    float v189 = -9.0655220171271012e-01F;
    float v195 = 8.1857027294591811e-01F;
    float v196 = -8.1857027294591811e-01F;
    float v202 = 1.1971367726043427e+00F;
    float v203 = -1.1971367726043427e+00F;
    float v209 = 8.6131170741789742e-01F;
    float v210 = -8.6131170741789742e-01F;
    float v216 = 1.1091548438375507e+00F;
    float v217 = -1.1091548438375507e+00F;
    float v223 = 4.2741434471979367e-02F;
    float v224 = -4.2741434471979367e-02F;
    float v230 = -4.5240494294812715e-02F;
    float v231 = 4.5240494294812715e-02F;
    float v237 = 2.9058457089163264e-01F;
    float v238 = -2.9058457089163264e-01F;
    float32x2_t v240 = (float32x2_t){v4, v4};
    float32x2_t v115 = v5[0];
    float32x2_t v130 = (float32x2_t){v129, v129};
    float32x2_t v134 = (float32x2_t){v133, v133};
    float32x2_t v138 = (float32x2_t){v136, v137};
    float32x2_t v145 = (float32x2_t){v143, v144};
    float32x2_t v152 = (float32x2_t){v150, v151};
    float32x2_t v159 = (float32x2_t){v158, v158};
    float32x2_t v163 = (float32x2_t){v162, v162};
    float32x2_t v167 = (float32x2_t){v166, v166};
    float32x2_t v171 = (float32x2_t){v170, v170};
    float32x2_t v175 = (float32x2_t){v174, v174};
    float32x2_t v179 = (float32x2_t){v178, v178};
    float32x2_t v183 = (float32x2_t){v181, v182};
    float32x2_t v190 = (float32x2_t){v188, v189};
    float32x2_t v197 = (float32x2_t){v195, v196};
    float32x2_t v204 = (float32x2_t){v202, v203};
    float32x2_t v211 = (float32x2_t){v209, v210};
    float32x2_t v218 = (float32x2_t){v216, v217};
    float32x2_t v225 = (float32x2_t){v223, v224};
    float32x2_t v232 = (float32x2_t){v230, v231};
    float32x2_t v239 = (float32x2_t){v237, v238};
    float32x2_t v25 = v5[istride * 12];
    float32x2_t v31 = v5[istride * 2];
    float32x2_t v36 = v5[istride * 11];
    float32x2_t v42 = v5[istride * 3];
    float32x2_t v47 = v5[istride * 10];
    float32x2_t v53 = v5[istride * 4];
    float32x2_t v58 = v5[istride * 9];
    float32x2_t v64 = v5[istride * 5];
    float32x2_t v69 = v5[istride * 8];
    float32x2_t v75 = v5[istride * 6];
    float32x2_t v80 = v5[istride * 7];
    float32x2_t v140 = vmul_f32(v240, v138);
    float32x2_t v147 = vmul_f32(v240, v145);
    float32x2_t v154 = vmul_f32(v240, v152);
    float32x2_t v185 = vmul_f32(v240, v183);
    float32x2_t v192 = vmul_f32(v240, v190);
    float32x2_t v199 = vmul_f32(v240, v197);
    float32x2_t v206 = vmul_f32(v240, v204);
    float32x2_t v213 = vmul_f32(v240, v211);
    float32x2_t v220 = vmul_f32(v240, v218);
    float32x2_t v227 = vmul_f32(v240, v225);
    float32x2_t v234 = vmul_f32(v240, v232);
    float32x2_t v241 = vmul_f32(v240, v239);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v37 = vadd_f32(v31, v36);
    float32x2_t v48 = vadd_f32(v42, v47);
    float32x2_t v59 = vadd_f32(v53, v58);
    float32x2_t v70 = vadd_f32(v64, v69);
    float32x2_t v81 = vadd_f32(v75, v80);
    float32x2_t v82 = vsub_f32(v20, v25);
    float32x2_t v83 = vsub_f32(v31, v36);
    float32x2_t v84 = vsub_f32(v42, v47);
    float32x2_t v85 = vsub_f32(v53, v58);
    float32x2_t v86 = vsub_f32(v64, v69);
    float32x2_t v87 = vsub_f32(v75, v80);
    float32x2_t v88 = vadd_f32(v37, v70);
    float32x2_t v90 = vadd_f32(v26, v48);
    float32x2_t v93 = vadd_f32(v83, v86);
    float32x2_t v95 = vadd_f32(v82, v84);
    float32x2_t v97 = vsub_f32(v37, v81);
    float32x2_t v98 = vsub_f32(v48, v59);
    float32x2_t v99 = vsub_f32(v26, v59);
    float32x2_t v100 = vsub_f32(v70, v81);
    float32x2_t v105 = vsub_f32(v83, v87);
    float32x2_t v106 = vsub_f32(v82, v84);
    float32x2_t v107 = vsub_f32(v83, v86);
    float32x2_t v108 = vadd_f32(v82, v85);
    float32x2_t v109 = vsub_f32(v86, v87);
    float32x2_t v110 = vadd_f32(v84, v85);
    float32x2_t v89 = vadd_f32(v88, v81);
    float32x2_t v91 = vadd_f32(v90, v59);
    float32x2_t v94 = vadd_f32(v93, v87);
    float32x2_t v96 = vsub_f32(v95, v85);
    float32x2_t v101 = vsub_f32(v97, v98);
    float32x2_t v102 = vsub_f32(v99, v100);
    float32x2_t v103 = vadd_f32(v97, v98);
    float32x2_t v104 = vadd_f32(v99, v100);
    float32x2_t v121 = vadd_f32(v105, v106);
    float32x2_t v122 = vadd_f32(v107, v108);
    float32x2_t v123 = vsub_f32(v109, v110);
    float32x2_t v186 = vrev64_f32(v105);
    float32x2_t v193 = vrev64_f32(v106);
    float32x2_t v207 = vrev64_f32(v107);
    float32x2_t v214 = vrev64_f32(v108);
    float32x2_t v228 = vrev64_f32(v109);
    float32x2_t v235 = vrev64_f32(v110);
    float32x2_t v92 = vadd_f32(v89, v91);
    float32x2_t v117 = vsub_f32(v91, v89);
    float32x2_t v118 = vadd_f32(v94, v96);
    float32x2_t v119 = vadd_f32(v101, v102);
    float32x2_t v120 = vsub_f32(v103, v104);
    float32x2_t v141 = vrev64_f32(v94);
    float32x2_t v148 = vrev64_f32(v96);
    float32x2_t v160 = vmul_f32(v101, v159);
    float32x2_t v164 = vmul_f32(v102, v163);
    float32x2_t v172 = vmul_f32(v103, v171);
    float32x2_t v176 = vmul_f32(v104, v175);
    float32x2_t v187 = vmul_f32(v186, v185);
    float32x2_t v194 = vmul_f32(v193, v192);
    float32x2_t v200 = vrev64_f32(v121);
    float32x2_t v208 = vmul_f32(v207, v206);
    float32x2_t v215 = vmul_f32(v214, v213);
    float32x2_t v221 = vrev64_f32(v122);
    float32x2_t v229 = vmul_f32(v228, v227);
    float32x2_t v236 = vmul_f32(v235, v234);
    float32x2_t v242 = vrev64_f32(v123);
    float32x2_t v116 = vadd_f32(v115, v92);
    float32x2_t v131 = vmul_f32(v92, v130);
    float32x2_t v135 = vmul_f32(v117, v134);
    float32x2_t v142 = vmul_f32(v141, v140);
    float32x2_t v149 = vmul_f32(v148, v147);
    float32x2_t v155 = vrev64_f32(v118);
    float32x2_t v168 = vmul_f32(v119, v167);
    float32x2_t v180 = vmul_f32(v120, v179);
    float32x2_t v201 = vmul_f32(v200, v199);
    float32x2_t v222 = vmul_f32(v221, v220);
    float32x2_t v243 = vmul_f32(v242, v241);
    float32x2_t v245 = vadd_f32(v164, v160);
    float32x2_t v156 = vmul_f32(v155, v154);
    float32x2_t v244 = vsub_f32(v116, v131);
    float32x2_t v246 = vsub_f32(v245, v135);
    float32x2_t v247 = vadd_f32(v164, v168);
    float32x2_t v249 = vsub_f32(v168, v160);
    float32x2_t v257 = vsub_f32(v187, v201);
    float32x2_t v258 = vsub_f32(v194, v201);
    float32x2_t v259 = vsub_f32(v208, v222);
    float32x2_t v260 = vsub_f32(v215, v222);
    float32x2_t v261 = vsub_f32(v229, v243);
    float32x2_t v262 = vadd_f32(v236, v243);
    int16x4_t v297 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v116, 15), (int32x2_t){0, 0}));
    float32x2_t v248 = vadd_f32(v247, v135);
    float32x2_t v250 = vsub_f32(v249, v135);
    float32x2_t v251 = vadd_f32(v244, v172);
    float32x2_t v253 = vsub_f32(v244, v176);
    float32x2_t v255 = vsub_f32(v244, v172);
    float32x2_t v263 = vsub_f32(v142, v156);
    float32x2_t v264 = vsub_f32(v149, v156);
    float32x2_t v275 = vadd_f32(v257, v261);
    float32x2_t v277 = vadd_f32(v259, v261);
    float32x2_t v279 = vsub_f32(v258, v262);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v297), 0);
    float32x2_t v252 = vadd_f32(v251, v176);
    float32x2_t v254 = vsub_f32(v253, v180);
    float32x2_t v256 = vadd_f32(v255, v180);
    float32x2_t v271 = vsub_f32(v264, v257);
    float32x2_t v273 = vsub_f32(v262, v263);
    float32x2_t v276 = vadd_f32(v275, v264);
    float32x2_t v278 = vsub_f32(v277, v264);
    float32x2_t v280 = vsub_f32(v279, v263);
    float32x2_t v281 = vadd_f32(v263, v258);
    float32x2_t v265 = vadd_f32(v246, v252);
    float32x2_t v266 = vadd_f32(v248, v254);
    float32x2_t v267 = vsub_f32(v254, v248);
    float32x2_t v268 = vadd_f32(v250, v256);
    float32x2_t v269 = vsub_f32(v252, v246);
    float32x2_t v270 = vsub_f32(v256, v250);
    float32x2_t v272 = vadd_f32(v271, v259);
    float32x2_t v274 = vsub_f32(v273, v260);
    float32x2_t v282 = vsub_f32(v281, v260);
    float32x2_t v283 = vsub_f32(v265, v272);
    float32x2_t v284 = vadd_f32(v266, v274);
    float32x2_t v285 = vsub_f32(v267, v276);
    float32x2_t v286 = vsub_f32(v268, v278);
    float32x2_t v287 = vadd_f32(v269, v280);
    float32x2_t v288 = vsub_f32(v270, v282);
    float32x2_t v289 = vadd_f32(v270, v282);
    float32x2_t v290 = vsub_f32(v269, v280);
    float32x2_t v291 = vadd_f32(v268, v278);
    float32x2_t v292 = vadd_f32(v267, v276);
    float32x2_t v293 = vsub_f32(v266, v274);
    float32x2_t v294 = vadd_f32(v265, v272);
    int16x4_t v303 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v283, 15), (int32x2_t){0, 0}));
    int16x4_t v309 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v284, 15), (int32x2_t){0, 0}));
    int16x4_t v315 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v285, 15), (int32x2_t){0, 0}));
    int16x4_t v321 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v286, 15), (int32x2_t){0, 0}));
    int16x4_t v327 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v287, 15), (int32x2_t){0, 0}));
    int16x4_t v333 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v288, 15), (int32x2_t){0, 0}));
    int16x4_t v339 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v289, 15), (int32x2_t){0, 0}));
    int16x4_t v345 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v290, 15), (int32x2_t){0, 0}));
    int16x4_t v351 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v291, 15), (int32x2_t){0, 0}));
    int16x4_t v357 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v292, 15), (int32x2_t){0, 0}));
    int16x4_t v363 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v293, 15), (int32x2_t){0, 0}));
    int16x4_t v369 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v294, 15), (int32x2_t){0, 0}));
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v303), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v309), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v315), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v321), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v327), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v333), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v339), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v345), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v351), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v357), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v363), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v369), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu13(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v158 = 1.0833333333333333e+00F;
    float v163 = -3.0046260628866578e-01F;
    float v168 = -7.4927933062613905e-01F;
    float v175 = -4.0100212832186721e-01F;
    float v182 = -5.7514072947400308e-01F;
    float v189 = 5.2422663952658211e-01F;
    float v194 = 5.1652078062348972e-01F;
    float v199 = 7.7058589030924258e-03F;
    float v204 = 4.2763404682656941e-01F;
    float v209 = 1.5180597207438440e-01F;
    float v214 = 5.7944001890096386e-01F;
    float v219 = -1.1543953381323635e+00F;
    float v226 = -9.0655220171271012e-01F;
    float v233 = -8.1857027294591811e-01F;
    float v240 = -1.1971367726043427e+00F;
    float v247 = -8.6131170741789742e-01F;
    float v254 = -1.1091548438375507e+00F;
    float v261 = -4.2741434471979367e-02F;
    float v268 = 4.5240494294812715e-02F;
    float v275 = -2.9058457089163264e-01F;
    const float32x2_t *v442 = &v5[v0];
    int32_t *v690 = &v6[v2];
    int64_t v26 = v0 * 12;
    int64_t v34 = v0 * 2;
    int64_t v41 = v0 * 11;
    int64_t v49 = v0 * 3;
    int64_t v56 = v0 * 10;
    int64_t v64 = v0 * 4;
    int64_t v71 = v0 * 9;
    int64_t v79 = v0 * 5;
    int64_t v86 = v0 * 8;
    int64_t v94 = v0 * 6;
    int64_t v101 = v0 * 7;
    float v171 = v4 * v168;
    float v178 = v4 * v175;
    float v185 = v4 * v182;
    float v222 = v4 * v219;
    float v229 = v4 * v226;
    float v236 = v4 * v233;
    float v243 = v4 * v240;
    float v250 = v4 * v247;
    float v257 = v4 * v254;
    float v264 = v4 * v261;
    float v271 = v4 * v268;
    float v278 = v4 * v275;
    int64_t v341 = v2 * 12;
    int64_t v349 = v2 * 11;
    int64_t v357 = v2 * 10;
    int64_t v365 = v2 * 9;
    int64_t v373 = v2 * 8;
    int64_t v381 = v2 * 7;
    int64_t v389 = v2 * 6;
    int64_t v397 = v2 * 5;
    int64_t v405 = v2 * 4;
    int64_t v413 = v2 * 3;
    int64_t v421 = v2 * 2;
    const float32x2_t *v551 = &v5[0];
    svfloat32_t v555 = svdup_n_f32(v158);
    svfloat32_t v556 = svdup_n_f32(v163);
    svfloat32_t v560 = svdup_n_f32(v189);
    svfloat32_t v561 = svdup_n_f32(v194);
    svfloat32_t v562 = svdup_n_f32(v199);
    svfloat32_t v563 = svdup_n_f32(v204);
    svfloat32_t v564 = svdup_n_f32(v209);
    svfloat32_t v565 = svdup_n_f32(v214);
    int32_t *v582 = &v6[0];
    svfloat32_t v694 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v442)[0]));
    const float32x2_t *v451 = &v5[v26];
    const float32x2_t *v460 = &v5[v34];
    const float32x2_t *v469 = &v5[v41];
    const float32x2_t *v478 = &v5[v49];
    const float32x2_t *v487 = &v5[v56];
    const float32x2_t *v496 = &v5[v64];
    const float32x2_t *v505 = &v5[v71];
    const float32x2_t *v514 = &v5[v79];
    const float32x2_t *v523 = &v5[v86];
    const float32x2_t *v532 = &v5[v94];
    const float32x2_t *v541 = &v5[v101];
    svfloat32_t v557 = svdup_n_f32(v171);
    svfloat32_t v558 = svdup_n_f32(v178);
    svfloat32_t v559 = svdup_n_f32(v185);
    svfloat32_t v566 = svdup_n_f32(v222);
    svfloat32_t v567 = svdup_n_f32(v229);
    svfloat32_t v568 = svdup_n_f32(v236);
    svfloat32_t v569 = svdup_n_f32(v243);
    svfloat32_t v570 = svdup_n_f32(v250);
    svfloat32_t v571 = svdup_n_f32(v257);
    svfloat32_t v572 = svdup_n_f32(v264);
    svfloat32_t v573 = svdup_n_f32(v271);
    svfloat32_t v574 = svdup_n_f32(v278);
    int32_t *v591 = &v6[v341];
    int32_t *v600 = &v6[v349];
    int32_t *v609 = &v6[v357];
    int32_t *v618 = &v6[v365];
    int32_t *v627 = &v6[v373];
    int32_t *v636 = &v6[v381];
    int32_t *v645 = &v6[v389];
    int32_t *v654 = &v6[v397];
    int32_t *v663 = &v6[v405];
    int32_t *v672 = &v6[v413];
    int32_t *v681 = &v6[v421];
    svfloat32_t v718 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v551)[0]));
    svfloat32_t v696 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v451)[0]));
    svfloat32_t v698 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v460)[0]));
    svfloat32_t v700 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v469)[0]));
    svfloat32_t v702 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v478)[0]));
    svfloat32_t v704 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v487)[0]));
    svfloat32_t v706 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v496)[0]));
    svfloat32_t v708 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v505)[0]));
    svfloat32_t v710 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v514)[0]));
    svfloat32_t v712 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v523)[0]));
    svfloat32_t v714 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v532)[0]));
    svfloat32_t v716 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v541)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v694, v696);
    svfloat32_t v47 = svadd_f32_x(svptrue_b32(), v698, v700);
    svfloat32_t v62 = svadd_f32_x(svptrue_b32(), v702, v704);
    svfloat32_t v77 = svadd_f32_x(svptrue_b32(), v706, v708);
    svfloat32_t v92 = svadd_f32_x(svptrue_b32(), v710, v712);
    svfloat32_t v107 = svadd_f32_x(svptrue_b32(), v714, v716);
    svfloat32_t v108 = svsub_f32_x(svptrue_b32(), v694, v696);
    svfloat32_t v109 = svsub_f32_x(svptrue_b32(), v698, v700);
    svfloat32_t v110 = svsub_f32_x(svptrue_b32(), v702, v704);
    svfloat32_t v111 = svsub_f32_x(svptrue_b32(), v706, v708);
    svfloat32_t v112 = svsub_f32_x(svptrue_b32(), v710, v712);
    svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v714, v716);
    svfloat32_t v114 = svadd_f32_x(svptrue_b32(), v47, v92);
    svfloat32_t v116 = svadd_f32_x(svptrue_b32(), v32, v62);
    svfloat32_t v119 = svadd_f32_x(svptrue_b32(), v109, v112);
    svfloat32_t v121 = svadd_f32_x(svptrue_b32(), v108, v110);
    svfloat32_t v123 = svsub_f32_x(svptrue_b32(), v47, v107);
    svfloat32_t v124 = svsub_f32_x(svptrue_b32(), v62, v77);
    svfloat32_t v125 = svsub_f32_x(svptrue_b32(), v32, v77);
    svfloat32_t v126 = svsub_f32_x(svptrue_b32(), v92, v107);
    svfloat32_t v131 = svsub_f32_x(svptrue_b32(), v109, v113);
    svfloat32_t v132 = svsub_f32_x(svptrue_b32(), v108, v110);
    svfloat32_t v133 = svsub_f32_x(svptrue_b32(), v109, v112);
    svfloat32_t v134 = svadd_f32_x(svptrue_b32(), v108, v111);
    svfloat32_t v135 = svsub_f32_x(svptrue_b32(), v112, v113);
    svfloat32_t v136 = svadd_f32_x(svptrue_b32(), v110, v111);
    svfloat32_t v115 = svadd_f32_x(svptrue_b32(), v114, v107);
    svfloat32_t v117 = svadd_f32_x(svptrue_b32(), v116, v77);
    svfloat32_t v120 = svadd_f32_x(svptrue_b32(), v119, v113);
    svfloat32_t v122 = svsub_f32_x(svptrue_b32(), v121, v111);
    svfloat32_t v127 = svsub_f32_x(svptrue_b32(), v123, v124);
    svfloat32_t v128 = svsub_f32_x(svptrue_b32(), v125, v126);
    svfloat32_t v129 = svadd_f32_x(svptrue_b32(), v123, v124);
    svfloat32_t v130 = svadd_f32_x(svptrue_b32(), v125, v126);
    svfloat32_t v149 = svadd_f32_x(svptrue_b32(), v131, v132);
    svfloat32_t v150 = svadd_f32_x(svptrue_b32(), v133, v134);
    svfloat32_t v151 = svsub_f32_x(svptrue_b32(), v135, v136);
    svfloat32_t zero224 = svdup_n_f32(0);
    svfloat32_t v224 = svcmla_f32_x(pred_full, zero224, v566, v131, 90);
    svfloat32_t zero231 = svdup_n_f32(0);
    svfloat32_t v231 = svcmla_f32_x(pred_full, zero231, v567, v132, 90);
    svfloat32_t zero245 = svdup_n_f32(0);
    svfloat32_t v245 = svcmla_f32_x(pred_full, zero245, v569, v133, 90);
    svfloat32_t zero252 = svdup_n_f32(0);
    svfloat32_t v252 = svcmla_f32_x(pred_full, zero252, v570, v134, 90);
    svfloat32_t zero266 = svdup_n_f32(0);
    svfloat32_t v266 = svcmla_f32_x(pred_full, zero266, v572, v135, 90);
    svfloat32_t v118 = svadd_f32_x(svptrue_b32(), v115, v117);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v117, v115);
    svfloat32_t v146 = svadd_f32_x(svptrue_b32(), v120, v122);
    svfloat32_t v147 = svadd_f32_x(svptrue_b32(), v127, v128);
    svfloat32_t v148 = svsub_f32_x(svptrue_b32(), v129, v130);
    svfloat32_t zero173 = svdup_n_f32(0);
    svfloat32_t v173 = svcmla_f32_x(pred_full, zero173, v557, v120, 90);
    svfloat32_t zero180 = svdup_n_f32(0);
    svfloat32_t v180 = svcmla_f32_x(pred_full, zero180, v558, v122, 90);
    svfloat32_t v192 = svmul_f32_x(svptrue_b32(), v127, v560);
    svfloat32_t zero238 = svdup_n_f32(0);
    svfloat32_t v238 = svcmla_f32_x(pred_full, zero238, v568, v149, 90);
    svfloat32_t zero259 = svdup_n_f32(0);
    svfloat32_t v259 = svcmla_f32_x(pred_full, zero259, v571, v150, 90);
    svfloat32_t zero280 = svdup_n_f32(0);
    svfloat32_t v280 = svcmla_f32_x(pred_full, zero280, v574, v151, 90);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v718, v118);
    svfloat32_t zero187 = svdup_n_f32(0);
    svfloat32_t v187 = svcmla_f32_x(pred_full, zero187, v559, v146, 90);
    svfloat32_t v202 = svmul_f32_x(svptrue_b32(), v147, v562);
    svfloat32_t v282 = svmla_f32_x(pred_full, v192, v128, v561);
    svfloat32_t v294 = svsub_f32_x(svptrue_b32(), v224, v238);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v231, v238);
    svfloat32_t v296 = svsub_f32_x(svptrue_b32(), v245, v259);
    svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v252, v259);
    svfloat32_t v298 = svsub_f32_x(svptrue_b32(), v266, v280);
    svfloat32_t v299 = svcmla_f32_x(pred_full, v280, v573, v136, 90);
    svfloat32_t v281 = svmls_f32_x(pred_full, v144, v118, v555);
    svfloat32_t v283 = svmls_f32_x(pred_full, v282, v145, v556);
    svfloat32_t v284 = svmla_f32_x(pred_full, v202, v128, v561);
    svfloat32_t v286 = svnmls_f32_x(pred_full, v192, v147, v562);
    svfloat32_t v300 = svsub_f32_x(svptrue_b32(), v173, v187);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v180, v187);
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v294, v298);
    svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v296, v298);
    svfloat32_t v316 = svsub_f32_x(svptrue_b32(), v295, v299);
    svint16_t v334 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v144, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v285 = svmla_f32_x(pred_full, v284, v145, v556);
    svfloat32_t v287 = svmls_f32_x(pred_full, v286, v145, v556);
    svfloat32_t v288 = svmla_f32_x(pred_full, v281, v129, v563);
    svfloat32_t v290 = svmls_f32_x(pred_full, v281, v130, v564);
    svfloat32_t v292 = svmls_f32_x(pred_full, v281, v129, v563);
    svfloat32_t v308 = svsub_f32_x(svptrue_b32(), v301, v294);
    svfloat32_t v310 = svsub_f32_x(svptrue_b32(), v299, v300);
    svfloat32_t v313 = svadd_f32_x(svptrue_b32(), v312, v301);
    svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v314, v301);
    svfloat32_t v317 = svsub_f32_x(svptrue_b32(), v316, v300);
    svfloat32_t v318 = svadd_f32_x(svptrue_b32(), v300, v295);
    svst1w_u64(pred_full, (unsigned *)(v582), svreinterpret_u64_s16(v334));
    svfloat32_t v289 = svmla_f32_x(pred_full, v288, v130, v564);
    svfloat32_t v291 = svmls_f32_x(pred_full, v290, v148, v565);
    svfloat32_t v293 = svmla_f32_x(pred_full, v292, v148, v565);
    svfloat32_t v309 = svadd_f32_x(svptrue_b32(), v308, v296);
    svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v310, v297);
    svfloat32_t v319 = svsub_f32_x(svptrue_b32(), v318, v297);
    svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v283, v289);
    svfloat32_t v303 = svadd_f32_x(svptrue_b32(), v285, v291);
    svfloat32_t v304 = svsub_f32_x(svptrue_b32(), v291, v285);
    svfloat32_t v305 = svadd_f32_x(svptrue_b32(), v287, v293);
    svfloat32_t v306 = svsub_f32_x(svptrue_b32(), v289, v283);
    svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v293, v287);
    svfloat32_t v320 = svsub_f32_x(svptrue_b32(), v302, v309);
    svfloat32_t v321 = svadd_f32_x(svptrue_b32(), v303, v311);
    svfloat32_t v322 = svsub_f32_x(svptrue_b32(), v304, v313);
    svfloat32_t v323 = svsub_f32_x(svptrue_b32(), v305, v315);
    svfloat32_t v324 = svadd_f32_x(svptrue_b32(), v306, v317);
    svfloat32_t v325 = svsub_f32_x(svptrue_b32(), v307, v319);
    svfloat32_t v326 = svadd_f32_x(svptrue_b32(), v307, v319);
    svfloat32_t v327 = svsub_f32_x(svptrue_b32(), v306, v317);
    svfloat32_t v328 = svadd_f32_x(svptrue_b32(), v305, v315);
    svfloat32_t v329 = svadd_f32_x(svptrue_b32(), v304, v313);
    svfloat32_t v330 = svsub_f32_x(svptrue_b32(), v303, v311);
    svfloat32_t v331 = svadd_f32_x(svptrue_b32(), v302, v309);
    svint16_t v342 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v320, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v350 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v321, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v358 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v322, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v366 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v323, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v374 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v324, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v382 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v325, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v390 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v326, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v398 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v327, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v406 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v328, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v414 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v329, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v422 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v330, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v430 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v331, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v591), svreinterpret_u64_s16(v342));
    svst1w_u64(pred_full, (unsigned *)(v600), svreinterpret_u64_s16(v350));
    svst1w_u64(pred_full, (unsigned *)(v609), svreinterpret_u64_s16(v358));
    svst1w_u64(pred_full, (unsigned *)(v618), svreinterpret_u64_s16(v366));
    svst1w_u64(pred_full, (unsigned *)(v627), svreinterpret_u64_s16(v374));
    svst1w_u64(pred_full, (unsigned *)(v636), svreinterpret_u64_s16(v382));
    svst1w_u64(pred_full, (unsigned *)(v645), svreinterpret_u64_s16(v390));
    svst1w_u64(pred_full, (unsigned *)(v654), svreinterpret_u64_s16(v398));
    svst1w_u64(pred_full, (unsigned *)(v663), svreinterpret_u64_s16(v406));
    svst1w_u64(pred_full, (unsigned *)(v672), svreinterpret_u64_s16(v414));
    svst1w_u64(pred_full, (unsigned *)(v681), svreinterpret_u64_s16(v422));
    svst1w_u64(pred_full, (unsigned *)(v690), svreinterpret_u64_s16(v430));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu14(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v73 = v5[istride];
    float v206 = -1.1666666666666665e+00F;
    float v210 = 7.9015646852540022e-01F;
    float v214 = 5.5854267289647742e-02F;
    float v218 = 7.3430220123575241e-01F;
    float v221 = 4.4095855184409838e-01F;
    float v222 = -4.4095855184409838e-01F;
    float v228 = 3.4087293062393137e-01F;
    float v229 = -3.4087293062393137e-01F;
    float v235 = -5.3396936033772524e-01F;
    float v236 = 5.3396936033772524e-01F;
    float v242 = 8.7484229096165667e-01F;
    float v243 = -8.7484229096165667e-01F;
    float32x2_t v245 = (float32x2_t){v4, v4};
    float32x2_t v20 = v5[0];
    float32x2_t v207 = (float32x2_t){v206, v206};
    float32x2_t v211 = (float32x2_t){v210, v210};
    float32x2_t v215 = (float32x2_t){v214, v214};
    float32x2_t v219 = (float32x2_t){v218, v218};
    float32x2_t v223 = (float32x2_t){v221, v222};
    float32x2_t v230 = (float32x2_t){v228, v229};
    float32x2_t v237 = (float32x2_t){v235, v236};
    float32x2_t v244 = (float32x2_t){v242, v243};
    float32x2_t v25 = v5[istride * 7];
    float32x2_t v32 = v5[istride * 2];
    float32x2_t v37 = v5[istride * 9];
    float32x2_t v44 = v5[istride * 4];
    float32x2_t v49 = v5[istride * 11];
    float32x2_t v56 = v5[istride * 6];
    float32x2_t v61 = v5[istride * 13];
    float32x2_t v68 = v5[istride * 8];
    float32x2_t v80 = v5[istride * 10];
    float32x2_t v85 = v5[istride * 3];
    float32x2_t v92 = v5[istride * 12];
    float32x2_t v97 = v5[istride * 5];
    float32x2_t v225 = vmul_f32(v245, v223);
    float32x2_t v232 = vmul_f32(v245, v230);
    float32x2_t v239 = vmul_f32(v245, v237);
    float32x2_t v246 = vmul_f32(v245, v244);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v74 = vadd_f32(v68, v73);
    float32x2_t v75 = vsub_f32(v68, v73);
    float32x2_t v86 = vadd_f32(v80, v85);
    float32x2_t v87 = vsub_f32(v80, v85);
    float32x2_t v98 = vadd_f32(v92, v97);
    float32x2_t v99 = vsub_f32(v92, v97);
    float32x2_t v100 = vadd_f32(v38, v98);
    float32x2_t v101 = vsub_f32(v38, v98);
    float32x2_t v102 = vadd_f32(v74, v62);
    float32x2_t v103 = vsub_f32(v74, v62);
    float32x2_t v104 = vadd_f32(v50, v86);
    float32x2_t v105 = vsub_f32(v50, v86);
    float32x2_t v184 = vadd_f32(v39, v99);
    float32x2_t v185 = vsub_f32(v39, v99);
    float32x2_t v186 = vadd_f32(v75, v63);
    float32x2_t v187 = vsub_f32(v75, v63);
    float32x2_t v188 = vadd_f32(v51, v87);
    float32x2_t v189 = vsub_f32(v51, v87);
    float32x2_t v106 = vadd_f32(v100, v102);
    float32x2_t v109 = vsub_f32(v100, v102);
    float32x2_t v110 = vsub_f32(v102, v104);
    float32x2_t v111 = vsub_f32(v104, v100);
    float32x2_t v112 = vadd_f32(v101, v103);
    float32x2_t v114 = vsub_f32(v101, v103);
    float32x2_t v115 = vsub_f32(v103, v105);
    float32x2_t v116 = vsub_f32(v105, v101);
    float32x2_t v190 = vadd_f32(v184, v186);
    float32x2_t v193 = vsub_f32(v184, v186);
    float32x2_t v194 = vsub_f32(v186, v188);
    float32x2_t v195 = vsub_f32(v188, v184);
    float32x2_t v196 = vadd_f32(v185, v187);
    float32x2_t v198 = vsub_f32(v185, v187);
    float32x2_t v199 = vsub_f32(v187, v189);
    float32x2_t v200 = vsub_f32(v189, v185);
    float32x2_t v107 = vadd_f32(v106, v104);
    float32x2_t v113 = vadd_f32(v112, v105);
    float32x2_t v128 = vmul_f32(v109, v211);
    float32x2_t v132 = vmul_f32(v110, v215);
    float32x2_t v136 = vmul_f32(v111, v219);
    float32x2_t v149 = vrev64_f32(v114);
    float32x2_t v156 = vrev64_f32(v115);
    float32x2_t v163 = vrev64_f32(v116);
    float32x2_t v191 = vadd_f32(v190, v188);
    float32x2_t v197 = vadd_f32(v196, v189);
    float32x2_t v212 = vmul_f32(v193, v211);
    float32x2_t v216 = vmul_f32(v194, v215);
    float32x2_t v220 = vmul_f32(v195, v219);
    float32x2_t v233 = vrev64_f32(v198);
    float32x2_t v240 = vrev64_f32(v199);
    float32x2_t v247 = vrev64_f32(v200);
    float32x2_t v108 = vadd_f32(v107, v26);
    float32x2_t v124 = vmul_f32(v107, v207);
    float32x2_t v142 = vrev64_f32(v113);
    float32x2_t v150 = vmul_f32(v149, v232);
    float32x2_t v157 = vmul_f32(v156, v239);
    float32x2_t v164 = vmul_f32(v163, v246);
    float32x2_t v192 = vadd_f32(v191, v27);
    float32x2_t v208 = vmul_f32(v191, v207);
    float32x2_t v226 = vrev64_f32(v197);
    float32x2_t v234 = vmul_f32(v233, v232);
    float32x2_t v241 = vmul_f32(v240, v239);
    float32x2_t v248 = vmul_f32(v247, v246);
    float32x2_t v143 = vmul_f32(v142, v225);
    float32x2_t v165 = vadd_f32(v108, v124);
    float32x2_t v227 = vmul_f32(v226, v225);
    float32x2_t v249 = vadd_f32(v192, v208);
    int16x4_t v270 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v108, 15), (int32x2_t){0, 0}));
    int16x4_t v276 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v192, 15), (int32x2_t){0, 0}));
    float32x2_t v166 = vadd_f32(v165, v128);
    float32x2_t v168 = vsub_f32(v165, v128);
    float32x2_t v170 = vsub_f32(v165, v132);
    float32x2_t v172 = vadd_f32(v143, v150);
    float32x2_t v174 = vsub_f32(v143, v150);
    float32x2_t v176 = vsub_f32(v143, v157);
    float32x2_t v250 = vadd_f32(v249, v212);
    float32x2_t v252 = vsub_f32(v249, v212);
    float32x2_t v254 = vsub_f32(v249, v216);
    float32x2_t v256 = vadd_f32(v227, v234);
    float32x2_t v258 = vsub_f32(v227, v234);
    float32x2_t v260 = vsub_f32(v227, v241);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v270), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v276), 0);
    float32x2_t v167 = vadd_f32(v166, v132);
    float32x2_t v169 = vsub_f32(v168, v136);
    float32x2_t v171 = vadd_f32(v170, v136);
    float32x2_t v173 = vadd_f32(v172, v157);
    float32x2_t v175 = vsub_f32(v174, v164);
    float32x2_t v177 = vadd_f32(v176, v164);
    float32x2_t v251 = vadd_f32(v250, v216);
    float32x2_t v253 = vsub_f32(v252, v220);
    float32x2_t v255 = vadd_f32(v254, v220);
    float32x2_t v257 = vadd_f32(v256, v241);
    float32x2_t v259 = vsub_f32(v258, v248);
    float32x2_t v261 = vadd_f32(v260, v248);
    float32x2_t v178 = vadd_f32(v167, v173);
    float32x2_t v179 = vsub_f32(v167, v173);
    float32x2_t v180 = vadd_f32(v169, v175);
    float32x2_t v181 = vsub_f32(v169, v175);
    float32x2_t v182 = vadd_f32(v171, v177);
    float32x2_t v183 = vsub_f32(v171, v177);
    float32x2_t v262 = vadd_f32(v251, v257);
    float32x2_t v263 = vsub_f32(v251, v257);
    float32x2_t v264 = vadd_f32(v253, v259);
    float32x2_t v265 = vsub_f32(v253, v259);
    float32x2_t v266 = vadd_f32(v255, v261);
    float32x2_t v267 = vsub_f32(v255, v261);
    int16x4_t v282 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v179, 15), (int32x2_t){0, 0}));
    int16x4_t v288 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v263, 15), (int32x2_t){0, 0}));
    int16x4_t v294 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v181, 15), (int32x2_t){0, 0}));
    int16x4_t v300 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v265, 15), (int32x2_t){0, 0}));
    int16x4_t v306 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v182, 15), (int32x2_t){0, 0}));
    int16x4_t v312 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v266, 15), (int32x2_t){0, 0}));
    int16x4_t v318 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v183, 15), (int32x2_t){0, 0}));
    int16x4_t v324 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v267, 15), (int32x2_t){0, 0}));
    int16x4_t v330 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v180, 15), (int32x2_t){0, 0}));
    int16x4_t v336 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v264, 15), (int32x2_t){0, 0}));
    int16x4_t v342 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v178, 15), (int32x2_t){0, 0}));
    int16x4_t v348 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v262, 15), (int32x2_t){0, 0}));
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v282), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v288), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v294), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v300), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v306), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v312), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v318), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v324), 0);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v330), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v336), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v342), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v348), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu14(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v242 = -1.1666666666666665e+00F;
    float v247 = 7.9015646852540022e-01F;
    float v252 = 5.5854267289647742e-02F;
    float v257 = 7.3430220123575241e-01F;
    float v262 = -4.4095855184409838e-01F;
    float v269 = -3.4087293062393137e-01F;
    float v276 = 5.3396936033772524e-01F;
    float v283 = -8.7484229096165667e-01F;
    const float32x2_t *v508 = &v5[v0];
    int32_t *v599 = &v6[v2];
    int64_t v26 = v0 * 7;
    int64_t v35 = v0 * 2;
    int64_t v42 = v0 * 9;
    int64_t v51 = v0 * 4;
    int64_t v58 = v0 * 11;
    int64_t v67 = v0 * 6;
    int64_t v74 = v0 * 13;
    int64_t v83 = v0 * 8;
    int64_t v99 = v0 * 10;
    int64_t v106 = v0 * 3;
    int64_t v115 = v0 * 12;
    int64_t v122 = v0 * 5;
    float v265 = v4 * v262;
    float v272 = v4 * v269;
    float v279 = v4 * v276;
    float v286 = v4 * v283;
    int64_t v317 = v2 * 7;
    int64_t v325 = v2 * 8;
    int64_t v341 = v2 * 2;
    int64_t v349 = v2 * 9;
    int64_t v357 = v2 * 10;
    int64_t v365 = v2 * 3;
    int64_t v373 = v2 * 4;
    int64_t v381 = v2 * 11;
    int64_t v389 = v2 * 12;
    int64_t v397 = v2 * 5;
    int64_t v405 = v2 * 6;
    int64_t v413 = v2 * 13;
    const float32x2_t *v427 = &v5[0];
    svfloat32_t v557 = svdup_n_f32(v242);
    svfloat32_t v558 = svdup_n_f32(v247);
    svfloat32_t v559 = svdup_n_f32(v252);
    svfloat32_t v560 = svdup_n_f32(v257);
    int32_t *v572 = &v6[0];
    svfloat32_t v711 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v508)[0]));
    const float32x2_t *v436 = &v5[v26];
    const float32x2_t *v445 = &v5[v35];
    const float32x2_t *v454 = &v5[v42];
    const float32x2_t *v463 = &v5[v51];
    const float32x2_t *v472 = &v5[v58];
    const float32x2_t *v481 = &v5[v67];
    const float32x2_t *v490 = &v5[v74];
    const float32x2_t *v499 = &v5[v83];
    const float32x2_t *v517 = &v5[v99];
    const float32x2_t *v526 = &v5[v106];
    const float32x2_t *v535 = &v5[v115];
    const float32x2_t *v544 = &v5[v122];
    svfloat32_t v561 = svdup_n_f32(v265);
    svfloat32_t v562 = svdup_n_f32(v272);
    svfloat32_t v563 = svdup_n_f32(v279);
    svfloat32_t v564 = svdup_n_f32(v286);
    int32_t *v581 = &v6[v317];
    int32_t *v590 = &v6[v325];
    int32_t *v608 = &v6[v341];
    int32_t *v617 = &v6[v349];
    int32_t *v626 = &v6[v357];
    int32_t *v635 = &v6[v365];
    int32_t *v644 = &v6[v373];
    int32_t *v653 = &v6[v381];
    int32_t *v662 = &v6[v389];
    int32_t *v671 = &v6[v397];
    int32_t *v680 = &v6[v405];
    int32_t *v689 = &v6[v413];
    svfloat32_t v693 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v427)[0]));
    svfloat32_t v695 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v436)[0]));
    svfloat32_t v697 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v445)[0]));
    svfloat32_t v699 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v454)[0]));
    svfloat32_t v701 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v463)[0]));
    svfloat32_t v703 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v472)[0]));
    svfloat32_t v705 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v481)[0]));
    svfloat32_t v707 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v490)[0]));
    svfloat32_t v709 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v499)[0]));
    svfloat32_t v713 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v517)[0]));
    svfloat32_t v715 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v526)[0]));
    svfloat32_t v717 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v535)[0]));
    svfloat32_t v719 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v544)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v693, v695);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v693, v695);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v697, v699);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v697, v699);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v701, v703);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v701, v703);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v705, v707);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v705, v707);
    svfloat32_t v96 = svadd_f32_x(svptrue_b32(), v709, v711);
    svfloat32_t v97 = svsub_f32_x(svptrue_b32(), v709, v711);
    svfloat32_t v112 = svadd_f32_x(svptrue_b32(), v713, v715);
    svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v713, v715);
    svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v717, v719);
    svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v717, v719);
    svfloat32_t v130 = svadd_f32_x(svptrue_b32(), v48, v128);
    svfloat32_t v131 = svsub_f32_x(svptrue_b32(), v48, v128);
    svfloat32_t v132 = svadd_f32_x(svptrue_b32(), v96, v80);
    svfloat32_t v133 = svsub_f32_x(svptrue_b32(), v96, v80);
    svfloat32_t v134 = svadd_f32_x(svptrue_b32(), v64, v112);
    svfloat32_t v135 = svsub_f32_x(svptrue_b32(), v64, v112);
    svfloat32_t v219 = svadd_f32_x(svptrue_b32(), v49, v129);
    svfloat32_t v220 = svsub_f32_x(svptrue_b32(), v49, v129);
    svfloat32_t v221 = svadd_f32_x(svptrue_b32(), v97, v81);
    svfloat32_t v222 = svsub_f32_x(svptrue_b32(), v97, v81);
    svfloat32_t v223 = svadd_f32_x(svptrue_b32(), v65, v113);
    svfloat32_t v224 = svsub_f32_x(svptrue_b32(), v65, v113);
    svfloat32_t v136 = svadd_f32_x(svptrue_b32(), v130, v132);
    svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v130, v132);
    svfloat32_t v140 = svsub_f32_x(svptrue_b32(), v132, v134);
    svfloat32_t v141 = svsub_f32_x(svptrue_b32(), v134, v130);
    svfloat32_t v142 = svadd_f32_x(svptrue_b32(), v131, v133);
    svfloat32_t v144 = svsub_f32_x(svptrue_b32(), v131, v133);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v133, v135);
    svfloat32_t v146 = svsub_f32_x(svptrue_b32(), v135, v131);
    svfloat32_t v225 = svadd_f32_x(svptrue_b32(), v219, v221);
    svfloat32_t v228 = svsub_f32_x(svptrue_b32(), v219, v221);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v221, v223);
    svfloat32_t v230 = svsub_f32_x(svptrue_b32(), v223, v219);
    svfloat32_t v231 = svadd_f32_x(svptrue_b32(), v220, v222);
    svfloat32_t v233 = svsub_f32_x(svptrue_b32(), v220, v222);
    svfloat32_t v234 = svsub_f32_x(svptrue_b32(), v222, v224);
    svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v224, v220);
    svfloat32_t v137 = svadd_f32_x(svptrue_b32(), v136, v134);
    svfloat32_t v143 = svadd_f32_x(svptrue_b32(), v142, v135);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 = svcmla_f32_x(pred_full, zero185, v562, v144, 90);
    svfloat32_t zero192 = svdup_n_f32(0);
    svfloat32_t v192 = svcmla_f32_x(pred_full, zero192, v563, v145, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 = svcmla_f32_x(pred_full, zero199, v564, v146, 90);
    svfloat32_t v226 = svadd_f32_x(svptrue_b32(), v225, v223);
    svfloat32_t v232 = svadd_f32_x(svptrue_b32(), v231, v224);
    svfloat32_t zero274 = svdup_n_f32(0);
    svfloat32_t v274 = svcmla_f32_x(pred_full, zero274, v562, v233, 90);
    svfloat32_t zero281 = svdup_n_f32(0);
    svfloat32_t v281 = svcmla_f32_x(pred_full, zero281, v563, v234, 90);
    svfloat32_t zero288 = svdup_n_f32(0);
    svfloat32_t v288 = svcmla_f32_x(pred_full, zero288, v564, v235, 90);
    svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v137, v32);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 = svcmla_f32_x(pred_full, zero178, v561, v143, 90);
    svfloat32_t v227 = svadd_f32_x(svptrue_b32(), v226, v33);
    svfloat32_t zero267 = svdup_n_f32(0);
    svfloat32_t v267 = svcmla_f32_x(pred_full, zero267, v561, v232, 90);
    svfloat32_t v200 = svmla_f32_x(pred_full, v138, v137, v557);
    svfloat32_t v207 = svadd_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v209 = svsub_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v211 = svsub_f32_x(svptrue_b32(), v178, v192);
    svfloat32_t v289 = svmla_f32_x(pred_full, v227, v226, v557);
    svfloat32_t v296 = svadd_f32_x(svptrue_b32(), v267, v274);
    svfloat32_t v298 = svsub_f32_x(svptrue_b32(), v267, v274);
    svfloat32_t v300 = svsub_f32_x(svptrue_b32(), v267, v281);
    svint16_t v310 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v138, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v318 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v227, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v201 = svmla_f32_x(pred_full, v200, v139, v558);
    svfloat32_t v203 = svmls_f32_x(pred_full, v200, v139, v558);
    svfloat32_t v205 = svmls_f32_x(pred_full, v200, v140, v559);
    svfloat32_t v208 = svadd_f32_x(svptrue_b32(), v207, v192);
    svfloat32_t v210 = svsub_f32_x(svptrue_b32(), v209, v199);
    svfloat32_t v212 = svadd_f32_x(svptrue_b32(), v211, v199);
    svfloat32_t v290 = svmla_f32_x(pred_full, v289, v228, v558);
    svfloat32_t v292 = svmls_f32_x(pred_full, v289, v228, v558);
    svfloat32_t v294 = svmls_f32_x(pred_full, v289, v229, v559);
    svfloat32_t v297 = svadd_f32_x(svptrue_b32(), v296, v281);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v298, v288);
    svfloat32_t v301 = svadd_f32_x(svptrue_b32(), v300, v288);
    svst1w_u64(pred_full, (unsigned *)(v572), svreinterpret_u64_s16(v310));
    svst1w_u64(pred_full, (unsigned *)(v581), svreinterpret_u64_s16(v318));
    svfloat32_t v202 = svmla_f32_x(pred_full, v201, v140, v559);
    svfloat32_t v204 = svmls_f32_x(pred_full, v203, v141, v560);
    svfloat32_t v206 = svmla_f32_x(pred_full, v205, v141, v560);
    svfloat32_t v291 = svmla_f32_x(pred_full, v290, v229, v559);
    svfloat32_t v293 = svmls_f32_x(pred_full, v292, v230, v560);
    svfloat32_t v295 = svmla_f32_x(pred_full, v294, v230, v560);
    svfloat32_t v213 = svadd_f32_x(svptrue_b32(), v202, v208);
    svfloat32_t v214 = svsub_f32_x(svptrue_b32(), v202, v208);
    svfloat32_t v215 = svadd_f32_x(svptrue_b32(), v204, v210);
    svfloat32_t v216 = svsub_f32_x(svptrue_b32(), v204, v210);
    svfloat32_t v217 = svadd_f32_x(svptrue_b32(), v206, v212);
    svfloat32_t v218 = svsub_f32_x(svptrue_b32(), v206, v212);
    svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v291, v297);
    svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v291, v297);
    svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v293, v299);
    svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v293, v299);
    svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v295, v301);
    svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v295, v301);
    svint16_t v326 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v214, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v334 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v303, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v342 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v216, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v350 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v305, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v358 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v217, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v366 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v306, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v374 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v218, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v382 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v307, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v390 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v215, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v398 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v304, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v406 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v213, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v414 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v302, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v590), svreinterpret_u64_s16(v326));
    svst1w_u64(pred_full, (unsigned *)(v599), svreinterpret_u64_s16(v334));
    svst1w_u64(pred_full, (unsigned *)(v608), svreinterpret_u64_s16(v342));
    svst1w_u64(pred_full, (unsigned *)(v617), svreinterpret_u64_s16(v350));
    svst1w_u64(pred_full, (unsigned *)(v626), svreinterpret_u64_s16(v358));
    svst1w_u64(pred_full, (unsigned *)(v635), svreinterpret_u64_s16(v366));
    svst1w_u64(pred_full, (unsigned *)(v644), svreinterpret_u64_s16(v374));
    svst1w_u64(pred_full, (unsigned *)(v653), svreinterpret_u64_s16(v382));
    svst1w_u64(pred_full, (unsigned *)(v662), svreinterpret_u64_s16(v390));
    svst1w_u64(pred_full, (unsigned *)(v671), svreinterpret_u64_s16(v398));
    svst1w_u64(pred_full, (unsigned *)(v680), svreinterpret_u64_s16(v406));
    svst1w_u64(pred_full, (unsigned *)(v689), svreinterpret_u64_s16(v414));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu15(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v61 = v5[istride];
    float v119 = -1.2500000000000000e+00F;
    float v123 = 5.5901699437494745e-01F;
    float v126 = 1.5388417685876268e+00F;
    float v127 = -1.5388417685876268e+00F;
    float v133 = 5.8778525229247325e-01F;
    float v134 = -5.8778525229247325e-01F;
    float v140 = 3.6327126400268028e-01F;
    float v141 = -3.6327126400268028e-01F;
    float v165 = -1.4999999999999998e+00F;
    float v169 = 1.8749999999999998e+00F;
    float v173 = -8.3852549156242107e-01F;
    float v176 = -2.3082626528814396e+00F;
    float v177 = 2.3082626528814396e+00F;
    float v183 = -8.8167787843870971e-01F;
    float v184 = 8.8167787843870971e-01F;
    float v190 = -5.4490689600402031e-01F;
    float v191 = 5.4490689600402031e-01F;
    float v214 = 8.6602540378443871e-01F;
    float v215 = -8.6602540378443871e-01F;
    float v221 = -1.0825317547305484e+00F;
    float v222 = 1.0825317547305484e+00F;
    float v228 = 4.8412291827592718e-01F;
    float v229 = -4.8412291827592718e-01F;
    float32x2_t v231 = (float32x2_t){v4, v4};
    float v236 = -1.3326760640014592e+00F;
    float v240 = -5.0903696045512736e-01F;
    float v244 = -3.1460214309120460e-01F;
    float32x2_t v32 = v5[0];
    float32x2_t v120 = (float32x2_t){v119, v119};
    float32x2_t v124 = (float32x2_t){v123, v123};
    float32x2_t v128 = (float32x2_t){v126, v127};
    float32x2_t v135 = (float32x2_t){v133, v134};
    float32x2_t v142 = (float32x2_t){v140, v141};
    float32x2_t v166 = (float32x2_t){v165, v165};
    float32x2_t v170 = (float32x2_t){v169, v169};
    float32x2_t v174 = (float32x2_t){v173, v173};
    float32x2_t v178 = (float32x2_t){v176, v177};
    float32x2_t v185 = (float32x2_t){v183, v184};
    float32x2_t v192 = (float32x2_t){v190, v191};
    float32x2_t v216 = (float32x2_t){v214, v215};
    float32x2_t v223 = (float32x2_t){v221, v222};
    float32x2_t v230 = (float32x2_t){v228, v229};
    float32x2_t v237 = (float32x2_t){v236, v236};
    float32x2_t v241 = (float32x2_t){v240, v240};
    float32x2_t v245 = (float32x2_t){v244, v244};
    float32x2_t v20 = v5[istride * 5];
    float32x2_t v25 = v5[istride * 10];
    float32x2_t v38 = v5[istride * 8];
    float32x2_t v43 = v5[istride * 13];
    float32x2_t v50 = v5[istride * 3];
    float32x2_t v56 = v5[istride * 11];
    float32x2_t v68 = v5[istride * 6];
    float32x2_t v74 = v5[istride * 14];
    float32x2_t v79 = v5[istride * 4];
    float32x2_t v86 = v5[istride * 9];
    float32x2_t v92 = v5[istride * 2];
    float32x2_t v97 = v5[istride * 7];
    float32x2_t v104 = v5[istride * 12];
    float32x2_t v130 = vmul_f32(v231, v128);
    float32x2_t v137 = vmul_f32(v231, v135);
    float32x2_t v144 = vmul_f32(v231, v142);
    float32x2_t v180 = vmul_f32(v231, v178);
    float32x2_t v187 = vmul_f32(v231, v185);
    float32x2_t v194 = vmul_f32(v231, v192);
    float32x2_t v218 = vmul_f32(v231, v216);
    float32x2_t v225 = vmul_f32(v231, v223);
    float32x2_t v232 = vmul_f32(v231, v230);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v44 = vadd_f32(v38, v43);
    float32x2_t v45 = vsub_f32(v38, v43);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v80 = vadd_f32(v74, v79);
    float32x2_t v81 = vsub_f32(v74, v79);
    float32x2_t v98 = vadd_f32(v92, v97);
    float32x2_t v99 = vsub_f32(v92, v97);
    float32x2_t v33 = vadd_f32(v26, v32);
    float32x2_t v51 = vadd_f32(v44, v50);
    float32x2_t v69 = vadd_f32(v62, v68);
    float32x2_t v87 = vadd_f32(v80, v86);
    float32x2_t v105 = vadd_f32(v98, v104);
    float32x2_t v156 = vadd_f32(v44, v98);
    float32x2_t v157 = vsub_f32(v44, v98);
    float32x2_t v158 = vadd_f32(v80, v62);
    float32x2_t v159 = vsub_f32(v80, v62);
    float32x2_t v206 = vadd_f32(v45, v99);
    float32x2_t v207 = vsub_f32(v45, v99);
    float32x2_t v208 = vadd_f32(v81, v63);
    float32x2_t v209 = vsub_f32(v81, v63);
    float32x2_t v106 = vadd_f32(v51, v105);
    float32x2_t v107 = vsub_f32(v51, v105);
    float32x2_t v108 = vadd_f32(v87, v69);
    float32x2_t v109 = vsub_f32(v87, v69);
    float32x2_t v160 = vadd_f32(v156, v158);
    float32x2_t v161 = vsub_f32(v156, v158);
    float32x2_t v162 = vadd_f32(v157, v159);
    float32x2_t v181 = vrev64_f32(v157);
    float32x2_t v195 = vrev64_f32(v159);
    float32x2_t v210 = vadd_f32(v206, v208);
    float32x2_t v211 = vsub_f32(v206, v208);
    float32x2_t v212 = vadd_f32(v207, v209);
    float32x2_t v238 = vmul_f32(v207, v237);
    float32x2_t v246 = vmul_f32(v209, v245);
    float32x2_t v110 = vadd_f32(v106, v108);
    float32x2_t v111 = vsub_f32(v106, v108);
    float32x2_t v112 = vadd_f32(v107, v109);
    float32x2_t v131 = vrev64_f32(v107);
    float32x2_t v145 = vrev64_f32(v109);
    float32x2_t v163 = vadd_f32(v160, v26);
    float32x2_t v171 = vmul_f32(v160, v170);
    float32x2_t v175 = vmul_f32(v161, v174);
    float32x2_t v182 = vmul_f32(v181, v180);
    float32x2_t v188 = vrev64_f32(v162);
    float32x2_t v196 = vmul_f32(v195, v194);
    float32x2_t v213 = vadd_f32(v210, v27);
    float32x2_t v226 = vrev64_f32(v210);
    float32x2_t v233 = vrev64_f32(v211);
    float32x2_t v242 = vmul_f32(v212, v241);
    float32x2_t v113 = vadd_f32(v110, v33);
    float32x2_t v121 = vmul_f32(v110, v120);
    float32x2_t v125 = vmul_f32(v111, v124);
    float32x2_t v132 = vmul_f32(v131, v130);
    float32x2_t v138 = vrev64_f32(v112);
    float32x2_t v146 = vmul_f32(v145, v144);
    float32x2_t v167 = vmul_f32(v163, v166);
    float32x2_t v189 = vmul_f32(v188, v187);
    float32x2_t v219 = vrev64_f32(v213);
    float32x2_t v227 = vmul_f32(v226, v225);
    float32x2_t v234 = vmul_f32(v233, v232);
    float32x2_t v250 = vsub_f32(v238, v242);
    float32x2_t v251 = vadd_f32(v242, v246);
    float32x2_t v139 = vmul_f32(v138, v137);
    float32x2_t v147 = vadd_f32(v113, v121);
    float32x2_t v197 = vadd_f32(v167, v171);
    float32x2_t v200 = vsub_f32(v182, v189);
    float32x2_t v201 = vadd_f32(v189, v196);
    float32x2_t v220 = vmul_f32(v219, v218);
    float32x2_t v256 = vadd_f32(v113, v167);
    int16x4_t v261 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v113, 15), (int32x2_t){0, 0}));
    float32x2_t v148 = vadd_f32(v147, v125);
    float32x2_t v149 = vsub_f32(v147, v125);
    float32x2_t v150 = vsub_f32(v132, v139);
    float32x2_t v151 = vadd_f32(v139, v146);
    float32x2_t v198 = vadd_f32(v197, v175);
    float32x2_t v199 = vsub_f32(v197, v175);
    float32x2_t v247 = vadd_f32(v220, v227);
    float32x2_t v257 = vadd_f32(v256, v220);
    float32x2_t v258 = vsub_f32(v256, v220);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v261), 0);
    float32x2_t v152 = vadd_f32(v148, v150);
    float32x2_t v153 = vsub_f32(v148, v150);
    float32x2_t v154 = vadd_f32(v149, v151);
    float32x2_t v155 = vsub_f32(v149, v151);
    float32x2_t v202 = vadd_f32(v198, v200);
    float32x2_t v203 = vsub_f32(v198, v200);
    float32x2_t v204 = vadd_f32(v199, v201);
    float32x2_t v205 = vsub_f32(v199, v201);
    float32x2_t v248 = vadd_f32(v247, v234);
    float32x2_t v249 = vsub_f32(v247, v234);
    int16x4_t v267 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v258, 15), (int32x2_t){0, 0}));
    int16x4_t v273 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v257, 15), (int32x2_t){0, 0}));
    float32x2_t v252 = vadd_f32(v248, v250);
    float32x2_t v253 = vsub_f32(v248, v250);
    float32x2_t v254 = vadd_f32(v249, v251);
    float32x2_t v255 = vsub_f32(v249, v251);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v267), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v273), 0);
    float32x2_t v277 = vadd_f32(v153, v203);
    int16x4_t v282 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v153, 15), (int32x2_t){0, 0}));
    float32x2_t v298 = vadd_f32(v155, v205);
    int16x4_t v303 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v155, 15), (int32x2_t){0, 0}));
    float32x2_t v319 = vadd_f32(v154, v204);
    int16x4_t v324 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v154, 15), (int32x2_t){0, 0}));
    float32x2_t v340 = vadd_f32(v152, v202);
    int16x4_t v345 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v152, 15), (int32x2_t){0, 0}));
    float32x2_t v278 = vadd_f32(v277, v253);
    float32x2_t v279 = vsub_f32(v277, v253);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v282), 0);
    float32x2_t v299 = vadd_f32(v298, v255);
    float32x2_t v300 = vsub_f32(v298, v255);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v303), 0);
    float32x2_t v320 = vadd_f32(v319, v254);
    float32x2_t v321 = vsub_f32(v319, v254);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v324), 0);
    float32x2_t v341 = vadd_f32(v340, v252);
    float32x2_t v342 = vsub_f32(v340, v252);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v345), 0);
    int16x4_t v288 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v279, 15), (int32x2_t){0, 0}));
    int16x4_t v294 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v278, 15), (int32x2_t){0, 0}));
    int16x4_t v309 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v300, 15), (int32x2_t){0, 0}));
    int16x4_t v315 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v299, 15), (int32x2_t){0, 0}));
    int16x4_t v330 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v321, 15), (int32x2_t){0, 0}));
    int16x4_t v336 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v320, 15), (int32x2_t){0, 0}));
    int16x4_t v351 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v342, 15), (int32x2_t){0, 0}));
    int16x4_t v357 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v341, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v288), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v294), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v309), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v315), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v330), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v336), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v351), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v357), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu15(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v152 = -1.2500000000000000e+00F;
    float v157 = 5.5901699437494745e-01F;
    float v162 = -1.5388417685876268e+00F;
    float v169 = -5.8778525229247325e-01F;
    float v176 = -3.6327126400268028e-01F;
    float v200 = -1.4999999999999998e+00F;
    float v205 = 1.8749999999999998e+00F;
    float v210 = -8.3852549156242107e-01F;
    float v215 = 2.3082626528814396e+00F;
    float v222 = 8.8167787843870971e-01F;
    float v229 = 5.4490689600402031e-01F;
    float v253 = -8.6602540378443871e-01F;
    float v260 = 1.0825317547305484e+00F;
    float v267 = -4.8412291827592718e-01F;
    float v274 = -1.3326760640014592e+00F;
    float v279 = -5.0903696045512736e-01F;
    float v284 = -3.1460214309120460e-01F;
    const float32x2_t *v502 = &v5[v0];
    int32_t *v629 = &v6[v2];
    int64_t v19 = v0 * 5;
    int64_t v26 = v0 * 10;
    int64_t v43 = v0 * 8;
    int64_t v50 = v0 * 13;
    int64_t v59 = v0 * 3;
    int64_t v67 = v0 * 11;
    int64_t v83 = v0 * 6;
    int64_t v91 = v0 * 14;
    int64_t v98 = v0 * 4;
    int64_t v107 = v0 * 9;
    int64_t v115 = v0 * 2;
    int64_t v122 = v0 * 7;
    int64_t v131 = v0 * 12;
    float v165 = v4 * v162;
    float v172 = v4 * v169;
    float v179 = v4 * v176;
    float v218 = v4 * v215;
    float v225 = v4 * v222;
    float v232 = v4 * v229;
    float v256 = v4 * v253;
    float v263 = v4 * v260;
    float v270 = v4 * v267;
    int64_t v309 = v2 * 10;
    int64_t v317 = v2 * 5;
    int64_t v328 = v2 * 6;
    int64_t v344 = v2 * 11;
    int64_t v355 = v2 * 12;
    int64_t v363 = v2 * 7;
    int64_t v371 = v2 * 2;
    int64_t v382 = v2 * 3;
    int64_t v390 = v2 * 13;
    int64_t v398 = v2 * 8;
    int64_t v409 = v2 * 9;
    int64_t v417 = v2 * 4;
    int64_t v425 = v2 * 14;
    const float32x2_t *v457 = &v5[0];
    svfloat32_t v569 = svdup_n_f32(v152);
    svfloat32_t v570 = svdup_n_f32(v157);
    svfloat32_t v574 = svdup_n_f32(v200);
    svfloat32_t v575 = svdup_n_f32(v205);
    svfloat32_t v576 = svdup_n_f32(v210);
    svfloat32_t v583 = svdup_n_f32(v274);
    svfloat32_t v584 = svdup_n_f32(v279);
    svfloat32_t v585 = svdup_n_f32(v284);
    int32_t *v593 = &v6[0];
    svfloat32_t v737 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v502)[0]));
    const float32x2_t *v438 = &v5[v19];
    const float32x2_t *v447 = &v5[v26];
    const float32x2_t *v466 = &v5[v43];
    const float32x2_t *v475 = &v5[v50];
    const float32x2_t *v484 = &v5[v59];
    const float32x2_t *v493 = &v5[v67];
    const float32x2_t *v511 = &v5[v83];
    const float32x2_t *v520 = &v5[v91];
    const float32x2_t *v529 = &v5[v98];
    const float32x2_t *v538 = &v5[v107];
    const float32x2_t *v547 = &v5[v115];
    const float32x2_t *v556 = &v5[v122];
    const float32x2_t *v565 = &v5[v131];
    svfloat32_t v571 = svdup_n_f32(v165);
    svfloat32_t v572 = svdup_n_f32(v172);
    svfloat32_t v573 = svdup_n_f32(v179);
    svfloat32_t v577 = svdup_n_f32(v218);
    svfloat32_t v578 = svdup_n_f32(v225);
    svfloat32_t v579 = svdup_n_f32(v232);
    svfloat32_t v580 = svdup_n_f32(v256);
    svfloat32_t v581 = svdup_n_f32(v263);
    svfloat32_t v582 = svdup_n_f32(v270);
    int32_t *v602 = &v6[v309];
    int32_t *v611 = &v6[v317];
    int32_t *v620 = &v6[v328];
    int32_t *v638 = &v6[v344];
    int32_t *v647 = &v6[v355];
    int32_t *v656 = &v6[v363];
    int32_t *v665 = &v6[v371];
    int32_t *v674 = &v6[v382];
    int32_t *v683 = &v6[v390];
    int32_t *v692 = &v6[v398];
    int32_t *v701 = &v6[v409];
    int32_t *v710 = &v6[v417];
    int32_t *v719 = &v6[v425];
    svfloat32_t v727 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v457)[0]));
    svfloat32_t v723 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v438)[0]));
    svfloat32_t v725 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v447)[0]));
    svfloat32_t v729 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v466)[0]));
    svfloat32_t v731 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v475)[0]));
    svfloat32_t v733 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v484)[0]));
    svfloat32_t v735 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v493)[0]));
    svfloat32_t v739 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v511)[0]));
    svfloat32_t v741 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v520)[0]));
    svfloat32_t v743 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v529)[0]));
    svfloat32_t v745 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v538)[0]));
    svfloat32_t v747 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v547)[0]));
    svfloat32_t v749 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v556)[0]));
    svfloat32_t v751 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v565)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v723, v725);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v723, v725);
    svfloat32_t v56 = svadd_f32_x(svptrue_b32(), v729, v731);
    svfloat32_t v57 = svsub_f32_x(svptrue_b32(), v729, v731);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v735, v737);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v735, v737);
    svfloat32_t v104 = svadd_f32_x(svptrue_b32(), v741, v743);
    svfloat32_t v105 = svsub_f32_x(svptrue_b32(), v741, v743);
    svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v747, v749);
    svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v747, v749);
    svfloat32_t v41 = svadd_f32_x(svptrue_b32(), v32, v727);
    svfloat32_t v65 = svadd_f32_x(svptrue_b32(), v56, v733);
    svfloat32_t v89 = svadd_f32_x(svptrue_b32(), v80, v739);
    svfloat32_t v113 = svadd_f32_x(svptrue_b32(), v104, v745);
    svfloat32_t v137 = svadd_f32_x(svptrue_b32(), v128, v751);
    svfloat32_t v191 = svadd_f32_x(svptrue_b32(), v56, v128);
    svfloat32_t v192 = svsub_f32_x(svptrue_b32(), v56, v128);
    svfloat32_t v193 = svadd_f32_x(svptrue_b32(), v104, v80);
    svfloat32_t v194 = svsub_f32_x(svptrue_b32(), v104, v80);
    svfloat32_t v244 = svadd_f32_x(svptrue_b32(), v57, v129);
    svfloat32_t v245 = svsub_f32_x(svptrue_b32(), v57, v129);
    svfloat32_t v246 = svadd_f32_x(svptrue_b32(), v105, v81);
    svfloat32_t v247 = svsub_f32_x(svptrue_b32(), v105, v81);
    svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v65, v137);
    svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v65, v137);
    svfloat32_t v140 = svadd_f32_x(svptrue_b32(), v113, v89);
    svfloat32_t v141 = svsub_f32_x(svptrue_b32(), v113, v89);
    svfloat32_t v195 = svadd_f32_x(svptrue_b32(), v191, v193);
    svfloat32_t v196 = svsub_f32_x(svptrue_b32(), v191, v193);
    svfloat32_t v197 = svadd_f32_x(svptrue_b32(), v192, v194);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 = svcmla_f32_x(pred_full, zero220, v577, v192, 90);
    svfloat32_t v248 = svadd_f32_x(svptrue_b32(), v244, v246);
    svfloat32_t v249 = svsub_f32_x(svptrue_b32(), v244, v246);
    svfloat32_t v250 = svadd_f32_x(svptrue_b32(), v245, v247);
    svfloat32_t v287 = svmul_f32_x(svptrue_b32(), v247, v585);
    svfloat32_t v142 = svadd_f32_x(svptrue_b32(), v138, v140);
    svfloat32_t v143 = svsub_f32_x(svptrue_b32(), v138, v140);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v139, v141);
    svfloat32_t zero167 = svdup_n_f32(0);
    svfloat32_t v167 = svcmla_f32_x(pred_full, zero167, v571, v139, 90);
    svfloat32_t v198 = svadd_f32_x(svptrue_b32(), v195, v32);
    svfloat32_t v208 = svmul_f32_x(svptrue_b32(), v195, v575);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 = svcmla_f32_x(pred_full, zero227, v578, v197, 90);
    svfloat32_t v251 = svadd_f32_x(svptrue_b32(), v248, v33);
    svfloat32_t zero272 = svdup_n_f32(0);
    svfloat32_t v272 = svcmla_f32_x(pred_full, zero272, v582, v249, 90);
    svfloat32_t v282 = svmul_f32_x(svptrue_b32(), v250, v584);
    svfloat32_t v145 = svadd_f32_x(svptrue_b32(), v142, v41);
    svfloat32_t zero174 = svdup_n_f32(0);
    svfloat32_t v174 = svcmla_f32_x(pred_full, zero174, v572, v144, 90);
    svfloat32_t v238 = svsub_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v239 = svcmla_f32_x(pred_full, v227, v579, v194, 90);
    svfloat32_t zero258 = svdup_n_f32(0);
    svfloat32_t v258 = svcmla_f32_x(pred_full, zero258, v580, v251, 90);
    svfloat32_t v291 = svnmls_f32_x(pred_full, v282, v245, v583);
    svfloat32_t v292 = svmla_f32_x(pred_full, v287, v250, v584);
    svfloat32_t v182 = svmla_f32_x(pred_full, v145, v142, v569);
    svfloat32_t v185 = svsub_f32_x(svptrue_b32(), v167, v174);
    svfloat32_t v186 = svcmla_f32_x(pred_full, v174, v573, v141, 90);
    svfloat32_t v235 = svmla_f32_x(pred_full, v208, v198, v574);
    svfloat32_t v288 = svcmla_f32_x(pred_full, v258, v581, v248, 90);
    svfloat32_t v297 = svmla_f32_x(pred_full, v145, v198, v574);
    svint16_t v302 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v145, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v183 = svmla_f32_x(pred_full, v182, v143, v570);
    svfloat32_t v184 = svmls_f32_x(pred_full, v182, v143, v570);
    svfloat32_t v236 = svmla_f32_x(pred_full, v235, v196, v576);
    svfloat32_t v237 = svmls_f32_x(pred_full, v235, v196, v576);
    svfloat32_t v289 = svadd_f32_x(svptrue_b32(), v288, v272);
    svfloat32_t v290 = svsub_f32_x(svptrue_b32(), v288, v272);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v297, v258);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v297, v258);
    svst1w_u64(pred_full, (unsigned *)(v593), svreinterpret_u64_s16(v302));
    svfloat32_t v187 = svadd_f32_x(svptrue_b32(), v183, v185);
    svfloat32_t v188 = svsub_f32_x(svptrue_b32(), v183, v185);
    svfloat32_t v189 = svadd_f32_x(svptrue_b32(), v184, v186);
    svfloat32_t v190 = svsub_f32_x(svptrue_b32(), v184, v186);
    svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v236, v238);
    svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v236, v238);
    svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v237, v239);
    svfloat32_t v243 = svsub_f32_x(svptrue_b32(), v237, v239);
    svfloat32_t v293 = svadd_f32_x(svptrue_b32(), v289, v291);
    svfloat32_t v294 = svsub_f32_x(svptrue_b32(), v289, v291);
    svfloat32_t v295 = svadd_f32_x(svptrue_b32(), v290, v292);
    svfloat32_t v296 = svsub_f32_x(svptrue_b32(), v290, v292);
    svint16_t v310 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v299, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v318 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v298, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v324 = svadd_f32_x(svptrue_b32(), v188, v241);
    svint16_t v329 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v188, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v351 = svadd_f32_x(svptrue_b32(), v190, v243);
    svint16_t v356 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v190, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v378 = svadd_f32_x(svptrue_b32(), v189, v242);
    svint16_t v383 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v189, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v187, v240);
    svint16_t v410 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v187, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v602), svreinterpret_u64_s16(v310));
    svst1w_u64(pred_full, (unsigned *)(v611), svreinterpret_u64_s16(v318));
    svfloat32_t v325 = svadd_f32_x(svptrue_b32(), v324, v294);
    svfloat32_t v326 = svsub_f32_x(svptrue_b32(), v324, v294);
    svfloat32_t v352 = svadd_f32_x(svptrue_b32(), v351, v296);
    svfloat32_t v353 = svsub_f32_x(svptrue_b32(), v351, v296);
    svfloat32_t v379 = svadd_f32_x(svptrue_b32(), v378, v295);
    svfloat32_t v380 = svsub_f32_x(svptrue_b32(), v378, v295);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v405, v293);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v405, v293);
    svst1w_u64(pred_full, (unsigned *)(v620), svreinterpret_u64_s16(v329));
    svst1w_u64(pred_full, (unsigned *)(v647), svreinterpret_u64_s16(v356));
    svst1w_u64(pred_full, (unsigned *)(v674), svreinterpret_u64_s16(v383));
    svst1w_u64(pred_full, (unsigned *)(v701), svreinterpret_u64_s16(v410));
    svint16_t v337 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v326, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v345 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v325, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v364 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v353, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v372 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v352, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v391 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v380, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v399 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v379, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v418 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v407, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v426 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v406, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v629), svreinterpret_u64_s16(v337));
    svst1w_u64(pred_full, (unsigned *)(v638), svreinterpret_u64_s16(v345));
    svst1w_u64(pred_full, (unsigned *)(v656), svreinterpret_u64_s16(v364));
    svst1w_u64(pred_full, (unsigned *)(v665), svreinterpret_u64_s16(v372));
    svst1w_u64(pred_full, (unsigned *)(v683), svreinterpret_u64_s16(v391));
    svst1w_u64(pred_full, (unsigned *)(v692), svreinterpret_u64_s16(v399));
    svst1w_u64(pred_full, (unsigned *)(v710), svreinterpret_u64_s16(v418));
    svst1w_u64(pred_full, (unsigned *)(v719), svreinterpret_u64_s16(v426));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu16(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v68 = v5[istride];
    float v181 = 1.0000000000000000e+00F;
    float v182 = -1.0000000000000000e+00F;
    float v189 = -7.0710678118654746e-01F;
    float v196 = 7.0710678118654757e-01F;
    float v199 = 9.2387953251128674e-01F;
    float v200 = -9.2387953251128674e-01F;
    float v207 = 5.4119610014619690e-01F;
    float v214 = -1.3065629648763766e+00F;
    float32x2_t v216 = (float32x2_t){v4, v4};
    float v221 = 3.8268343236508984e-01F;
    float v225 = 1.3065629648763766e+00F;
    float v229 = -5.4119610014619690e-01F;
    float32x2_t v20 = v5[0];
    float32x2_t v183 = (float32x2_t){v181, v182};
    float32x2_t v190 = (float32x2_t){v196, v189};
    float32x2_t v197 = (float32x2_t){v196, v196};
    float32x2_t v201 = (float32x2_t){v199, v200};
    float32x2_t v208 = (float32x2_t){v229, v207};
    float32x2_t v215 = (float32x2_t){v225, v214};
    float32x2_t v222 = (float32x2_t){v221, v221};
    float32x2_t v226 = (float32x2_t){v225, v225};
    float32x2_t v230 = (float32x2_t){v229, v229};
    float32x2_t v25 = v5[istride * 8];
    float32x2_t v32 = v5[istride * 4];
    float32x2_t v37 = v5[istride * 12];
    float32x2_t v44 = v5[istride * 2];
    float32x2_t v49 = v5[istride * 10];
    float32x2_t v56 = v5[istride * 6];
    float32x2_t v61 = v5[istride * 14];
    float32x2_t v73 = v5[istride * 9];
    float32x2_t v80 = v5[istride * 5];
    float32x2_t v85 = v5[istride * 13];
    float32x2_t v92 = v5[istride * 3];
    float32x2_t v97 = v5[istride * 11];
    float32x2_t v104 = v5[istride * 7];
    float32x2_t v109 = v5[istride * 15];
    float32x2_t v185 = vmul_f32(v216, v183);
    float32x2_t v192 = vmul_f32(v216, v190);
    float32x2_t v203 = vmul_f32(v216, v201);
    float32x2_t v210 = vmul_f32(v216, v208);
    float32x2_t v217 = vmul_f32(v216, v215);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v74 = vadd_f32(v68, v73);
    float32x2_t v75 = vsub_f32(v68, v73);
    float32x2_t v86 = vadd_f32(v80, v85);
    float32x2_t v87 = vsub_f32(v80, v85);
    float32x2_t v98 = vadd_f32(v92, v97);
    float32x2_t v99 = vsub_f32(v92, v97);
    float32x2_t v110 = vadd_f32(v104, v109);
    float32x2_t v111 = vsub_f32(v104, v109);
    float32x2_t v112 = vadd_f32(v26, v38);
    float32x2_t v113 = vsub_f32(v26, v38);
    float32x2_t v114 = vadd_f32(v50, v62);
    float32x2_t v115 = vsub_f32(v50, v62);
    float32x2_t v116 = vadd_f32(v74, v86);
    float32x2_t v117 = vsub_f32(v74, v86);
    float32x2_t v118 = vadd_f32(v98, v110);
    float32x2_t v119 = vsub_f32(v98, v110);
    float32x2_t v128 = vadd_f32(v51, v63);
    float32x2_t v129 = vsub_f32(v51, v63);
    float32x2_t v130 = vadd_f32(v75, v111);
    float32x2_t v131 = vsub_f32(v75, v111);
    float32x2_t v132 = vadd_f32(v87, v99);
    float32x2_t v133 = vsub_f32(v87, v99);
    float32x2_t v186 = vrev64_f32(v39);
    float32x2_t v120 = vadd_f32(v112, v114);
    float32x2_t v121 = vsub_f32(v112, v114);
    float32x2_t v122 = vadd_f32(v116, v118);
    float32x2_t v123 = vsub_f32(v116, v118);
    float32x2_t v126 = vadd_f32(v117, v119);
    float32x2_t v127 = vsub_f32(v117, v119);
    float32x2_t v134 = vadd_f32(v130, v132);
    float32x2_t v135 = vadd_f32(v131, v133);
    float32x2_t v164 = vrev64_f32(v115);
    float32x2_t v187 = vmul_f32(v186, v185);
    float32x2_t v193 = vrev64_f32(v128);
    float32x2_t v198 = vmul_f32(v129, v197);
    float32x2_t v211 = vrev64_f32(v130);
    float32x2_t v218 = vrev64_f32(v132);
    float32x2_t v227 = vmul_f32(v131, v226);
    float32x2_t v231 = vmul_f32(v133, v230);
    float32x2_t v124 = vadd_f32(v120, v122);
    float32x2_t v125 = vsub_f32(v120, v122);
    float32x2_t v153 = vrev64_f32(v123);
    float32x2_t v165 = vmul_f32(v164, v185);
    float32x2_t v171 = vrev64_f32(v126);
    float32x2_t v176 = vmul_f32(v127, v197);
    float32x2_t v194 = vmul_f32(v193, v192);
    float32x2_t v204 = vrev64_f32(v134);
    float32x2_t v212 = vmul_f32(v211, v210);
    float32x2_t v219 = vmul_f32(v218, v217);
    float32x2_t v223 = vmul_f32(v135, v222);
    float32x2_t v242 = vadd_f32(v27, v198);
    float32x2_t v243 = vsub_f32(v27, v198);
    float32x2_t v154 = vmul_f32(v153, v185);
    float32x2_t v172 = vmul_f32(v171, v192);
    float32x2_t v205 = vmul_f32(v204, v203);
    float32x2_t v234 = vadd_f32(v113, v176);
    float32x2_t v236 = vsub_f32(v113, v176);
    float32x2_t v244 = vadd_f32(v187, v194);
    float32x2_t v245 = vsub_f32(v187, v194);
    float32x2_t v248 = vsub_f32(v227, v223);
    float32x2_t v249 = vsub_f32(v231, v223);
    float32x2_t v250 = vsub_f32(v223, v227);
    float32x2_t v251 = vsub_f32(v223, v231);
    int16x4_t v278 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v124, 15), (int32x2_t){0, 0}));
    int16x4_t v326 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v125, 15), (int32x2_t){0, 0}));
    float32x2_t v232 = vadd_f32(v121, v154);
    float32x2_t v233 = vsub_f32(v121, v154);
    float32x2_t v235 = vadd_f32(v165, v172);
    float32x2_t v237 = vsub_f32(v172, v165);
    float32x2_t v246 = vadd_f32(v205, v212);
    float32x2_t v247 = vsub_f32(v205, v219);
    float32x2_t v252 = vadd_f32(v242, v248);
    float32x2_t v253 = vsub_f32(v242, v248);
    float32x2_t v254 = vadd_f32(v242, v250);
    float32x2_t v255 = vsub_f32(v242, v250);
    float32x2_t v256 = vadd_f32(v243, v245);
    float32x2_t v257 = vsub_f32(v243, v245);
    float32x2_t v258 = vadd_f32(v243, v251);
    float32x2_t v259 = vsub_f32(v243, v251);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v278), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v326), 0);
    float32x2_t v238 = vadd_f32(v234, v235);
    float32x2_t v239 = vadd_f32(v236, v237);
    float32x2_t v240 = vsub_f32(v236, v237);
    float32x2_t v241 = vsub_f32(v234, v235);
    float32x2_t v262 = vadd_f32(v246, v244);
    float32x2_t v263 = vsub_f32(v246, v244);
    float32x2_t v264 = vadd_f32(v247, v249);
    float32x2_t v265 = vsub_f32(v247, v249);
    float32x2_t v266 = vadd_f32(v247, v245);
    float32x2_t v267 = vsub_f32(v247, v245);
    int16x4_t v302 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v233, 15), (int32x2_t){0, 0}));
    int16x4_t v350 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v232, 15), (int32x2_t){0, 0}));
    float32x2_t v268 = vadd_f32(v252, v262);
    float32x2_t v269 = vadd_f32(v253, v263);
    float32x2_t v270 = vsub_f32(v254, v263);
    float32x2_t v271 = vsub_f32(v255, v262);
    float32x2_t v272 = vadd_f32(v256, v264);
    float32x2_t v273 = vadd_f32(v257, v265);
    float32x2_t v274 = vsub_f32(v258, v267);
    float32x2_t v275 = vsub_f32(v259, v266);
    int16x4_t v290 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v241, 15), (int32x2_t){0, 0}));
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v302), 0);
    int16x4_t v314 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v240, 15), (int32x2_t){0, 0}));
    int16x4_t v338 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v239, 15), (int32x2_t){0, 0}));
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v350), 0);
    int16x4_t v362 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v238, 15), (int32x2_t){0, 0}));
    int16x4_t v284 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v271, 15), (int32x2_t){0, 0}));
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v290), 0);
    int16x4_t v296 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v274, 15), (int32x2_t){0, 0}));
    int16x4_t v308 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v275, 15), (int32x2_t){0, 0}));
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v314), 0);
    int16x4_t v320 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v270, 15), (int32x2_t){0, 0}));
    int16x4_t v332 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v269, 15), (int32x2_t){0, 0}));
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v338), 0);
    int16x4_t v344 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v272, 15), (int32x2_t){0, 0}));
    int16x4_t v356 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v273, 15), (int32x2_t){0, 0}));
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v362), 0);
    int16x4_t v368 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v268, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v284), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v296), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v308), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v320), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v332), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v344), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v356), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v368), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu16(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v222 = -1.0000000000000000e+00F;
    float v229 = -7.0710678118654746e-01F;
    float v236 = 7.0710678118654757e-01F;
    float v241 = -9.2387953251128674e-01F;
    float v248 = 5.4119610014619690e-01F;
    float v255 = -1.3065629648763766e+00F;
    float v262 = 3.8268343236508984e-01F;
    float v267 = 1.3065629648763766e+00F;
    float v272 = -5.4119610014619690e-01F;
    const float32x2_t *v527 = &v5[v0];
    int32_t *v627 = &v6[v2];
    int64_t v26 = v0 * 8;
    int64_t v35 = v0 * 4;
    int64_t v42 = v0 * 12;
    int64_t v51 = v0 * 2;
    int64_t v58 = v0 * 10;
    int64_t v67 = v0 * 6;
    int64_t v74 = v0 * 14;
    int64_t v90 = v0 * 9;
    int64_t v99 = v0 * 5;
    int64_t v106 = v0 * 13;
    int64_t v115 = v0 * 3;
    int64_t v122 = v0 * 11;
    int64_t v131 = v0 * 7;
    int64_t v138 = v0 * 15;
    float v225 = v4 * v222;
    float v232 = v4 * v229;
    float v244 = v4 * v241;
    float v251 = v4 * v248;
    float v258 = v4 * v255;
    int64_t v337 = v2 * 2;
    int64_t v345 = v2 * 3;
    int64_t v353 = v2 * 4;
    int64_t v361 = v2 * 5;
    int64_t v369 = v2 * 6;
    int64_t v377 = v2 * 7;
    int64_t v385 = v2 * 8;
    int64_t v393 = v2 * 9;
    int64_t v401 = v2 * 10;
    int64_t v409 = v2 * 11;
    int64_t v417 = v2 * 12;
    int64_t v425 = v2 * 13;
    int64_t v433 = v2 * 14;
    int64_t v441 = v2 * 15;
    const float32x2_t *v455 = &v5[0];
    svfloat32_t v604 = svdup_n_f32(v236);
    svfloat32_t v608 = svdup_n_f32(v262);
    svfloat32_t v609 = svdup_n_f32(v267);
    svfloat32_t v610 = svdup_n_f32(v272);
    int32_t *v618 = &v6[0];
    svfloat32_t v773 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v527)[0]));
    const float32x2_t *v464 = &v5[v26];
    const float32x2_t *v473 = &v5[v35];
    const float32x2_t *v482 = &v5[v42];
    const float32x2_t *v491 = &v5[v51];
    const float32x2_t *v500 = &v5[v58];
    const float32x2_t *v509 = &v5[v67];
    const float32x2_t *v518 = &v5[v74];
    const float32x2_t *v536 = &v5[v90];
    const float32x2_t *v545 = &v5[v99];
    const float32x2_t *v554 = &v5[v106];
    const float32x2_t *v563 = &v5[v115];
    const float32x2_t *v572 = &v5[v122];
    const float32x2_t *v581 = &v5[v131];
    const float32x2_t *v590 = &v5[v138];
    svfloat32_t v602 = svdup_n_f32(v225);
    svfloat32_t v603 = svdup_n_f32(v232);
    svfloat32_t v605 = svdup_n_f32(v244);
    svfloat32_t v606 = svdup_n_f32(v251);
    svfloat32_t v607 = svdup_n_f32(v258);
    int32_t *v636 = &v6[v337];
    int32_t *v645 = &v6[v345];
    int32_t *v654 = &v6[v353];
    int32_t *v663 = &v6[v361];
    int32_t *v672 = &v6[v369];
    int32_t *v681 = &v6[v377];
    int32_t *v690 = &v6[v385];
    int32_t *v699 = &v6[v393];
    int32_t *v708 = &v6[v401];
    int32_t *v717 = &v6[v409];
    int32_t *v726 = &v6[v417];
    int32_t *v735 = &v6[v425];
    int32_t *v744 = &v6[v433];
    int32_t *v753 = &v6[v441];
    svfloat32_t v757 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v455)[0]));
    svfloat32_t v759 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v464)[0]));
    svfloat32_t v761 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v473)[0]));
    svfloat32_t v763 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v482)[0]));
    svfloat32_t v765 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v491)[0]));
    svfloat32_t v767 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v500)[0]));
    svfloat32_t v769 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v509)[0]));
    svfloat32_t v771 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v518)[0]));
    svfloat32_t v775 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v536)[0]));
    svfloat32_t v777 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v545)[0]));
    svfloat32_t v779 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v554)[0]));
    svfloat32_t v781 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v563)[0]));
    svfloat32_t v783 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v572)[0]));
    svfloat32_t v785 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v581)[0]));
    svfloat32_t v787 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v590)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v757, v759);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v757, v759);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v761, v763);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v761, v763);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v765, v767);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v765, v767);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v769, v771);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v769, v771);
    svfloat32_t v96 = svadd_f32_x(svptrue_b32(), v773, v775);
    svfloat32_t v97 = svsub_f32_x(svptrue_b32(), v773, v775);
    svfloat32_t v112 = svadd_f32_x(svptrue_b32(), v777, v779);
    svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v777, v779);
    svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v781, v783);
    svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v781, v783);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v785, v787);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v785, v787);
    svfloat32_t v146 = svadd_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v147 = svsub_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v64, v80);
    svfloat32_t v149 = svsub_f32_x(svptrue_b32(), v64, v80);
    svfloat32_t v150 = svadd_f32_x(svptrue_b32(), v96, v112);
    svfloat32_t v151 = svsub_f32_x(svptrue_b32(), v96, v112);
    svfloat32_t v152 = svadd_f32_x(svptrue_b32(), v128, v144);
    svfloat32_t v153 = svsub_f32_x(svptrue_b32(), v128, v144);
    svfloat32_t v162 = svadd_f32_x(svptrue_b32(), v65, v81);
    svfloat32_t v163 = svsub_f32_x(svptrue_b32(), v65, v81);
    svfloat32_t v164 = svadd_f32_x(svptrue_b32(), v97, v145);
    svfloat32_t v165 = svsub_f32_x(svptrue_b32(), v97, v145);
    svfloat32_t v166 = svadd_f32_x(svptrue_b32(), v113, v129);
    svfloat32_t v167 = svsub_f32_x(svptrue_b32(), v113, v129);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 = svcmla_f32_x(pred_full, zero227, v602, v49, 90);
    svfloat32_t v154 = svadd_f32_x(svptrue_b32(), v146, v148);
    svfloat32_t v155 = svsub_f32_x(svptrue_b32(), v146, v148);
    svfloat32_t v156 = svadd_f32_x(svptrue_b32(), v150, v152);
    svfloat32_t v157 = svsub_f32_x(svptrue_b32(), v150, v152);
    svfloat32_t v160 = svadd_f32_x(svptrue_b32(), v151, v153);
    svfloat32_t v161 = svsub_f32_x(svptrue_b32(), v151, v153);
    svfloat32_t v168 = svadd_f32_x(svptrue_b32(), v164, v166);
    svfloat32_t v169 = svadd_f32_x(svptrue_b32(), v165, v167);
    svfloat32_t zero203 = svdup_n_f32(0);
    svfloat32_t v203 = svcmla_f32_x(pred_full, zero203, v602, v149, 90);
    svfloat32_t zero234 = svdup_n_f32(0);
    svfloat32_t v234 = svcmla_f32_x(pred_full, zero234, v603, v162, 90);
    svfloat32_t zero260 = svdup_n_f32(0);
    svfloat32_t v260 = svcmla_f32_x(pred_full, zero260, v607, v166, 90);
    svfloat32_t v270 = svmul_f32_x(svptrue_b32(), v165, v609);
    svfloat32_t v275 = svmul_f32_x(svptrue_b32(), v167, v610);
    svfloat32_t v158 = svadd_f32_x(svptrue_b32(), v154, v156);
    svfloat32_t v159 = svsub_f32_x(svptrue_b32(), v154, v156);
    svfloat32_t zero191 = svdup_n_f32(0);
    svfloat32_t v191 = svcmla_f32_x(pred_full, zero191, v602, v157, 90);
    svfloat32_t zero210 = svdup_n_f32(0);
    svfloat32_t v210 = svcmla_f32_x(pred_full, zero210, v603, v160, 90);
    svfloat32_t zero246 = svdup_n_f32(0);
    svfloat32_t v246 = svcmla_f32_x(pred_full, zero246, v605, v168, 90);
    svfloat32_t v265 = svmul_f32_x(svptrue_b32(), v169, v608);
    svfloat32_t v286 = svmla_f32_x(pred_full, v33, v163, v604);
    svfloat32_t v287 = svmls_f32_x(pred_full, v33, v163, v604);
    svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v227, v234);
    svfloat32_t v289 = svsub_f32_x(svptrue_b32(), v227, v234);
    svfloat32_t v276 = svadd_f32_x(svptrue_b32(), v155, v191);
    svfloat32_t v277 = svsub_f32_x(svptrue_b32(), v155, v191);
    svfloat32_t v278 = svmla_f32_x(pred_full, v147, v161, v604);
    svfloat32_t v279 = svadd_f32_x(svptrue_b32(), v203, v210);
    svfloat32_t v280 = svmls_f32_x(pred_full, v147, v161, v604);
    svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v210, v203);
    svfloat32_t v290 = svcmla_f32_x(pred_full, v246, v606, v164, 90);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v246, v260);
    svfloat32_t v292 = svnmls_f32_x(pred_full, v265, v165, v609);
    svfloat32_t v293 = svnmls_f32_x(pred_full, v265, v167, v610);
    svfloat32_t v294 = svnmls_f32_x(pred_full, v270, v169, v608);
    svfloat32_t v295 = svnmls_f32_x(pred_full, v275, v169, v608);
    svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v287, v289);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v287, v289);
    svint16_t v322 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v158, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v386 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v159, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v278, v279);
    svfloat32_t v283 = svadd_f32_x(svptrue_b32(), v280, v281);
    svfloat32_t v284 = svsub_f32_x(svptrue_b32(), v280, v281);
    svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v278, v279);
    svfloat32_t v296 = svadd_f32_x(svptrue_b32(), v286, v292);
    svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v286, v292);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v286, v294);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v286, v294);
    svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v287, v295);
    svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v287, v295);
    svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v290, v288);
    svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v290, v288);
    svfloat32_t v308 = svadd_f32_x(svptrue_b32(), v291, v293);
    svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v291, v293);
    svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v291, v289);
    svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v291, v289);
    svint16_t v354 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v277, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v418 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v276, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v618), svreinterpret_u64_s16(v322));
    svst1w_u64(pred_full, (unsigned *)(v690), svreinterpret_u64_s16(v386));
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v296, v306);
    svfloat32_t v313 = svadd_f32_x(svptrue_b32(), v297, v307);
    svfloat32_t v314 = svsub_f32_x(svptrue_b32(), v298, v307);
    svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v299, v306);
    svfloat32_t v316 = svadd_f32_x(svptrue_b32(), v300, v308);
    svfloat32_t v317 = svadd_f32_x(svptrue_b32(), v301, v309);
    svfloat32_t v318 = svsub_f32_x(svptrue_b32(), v302, v311);
    svfloat32_t v319 = svsub_f32_x(svptrue_b32(), v303, v310);
    svint16_t v338 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v285, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v370 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v284, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v402 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v283, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v434 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v282, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v654), svreinterpret_u64_s16(v354));
    svst1w_u64(pred_full, (unsigned *)(v726), svreinterpret_u64_s16(v418));
    svint16_t v330 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v315, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v346 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v318, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v362 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v319, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v378 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v314, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v394 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v313, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v410 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v316, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v426 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v317, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v442 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v312, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v636), svreinterpret_u64_s16(v338));
    svst1w_u64(pred_full, (unsigned *)(v672), svreinterpret_u64_s16(v370));
    svst1w_u64(pred_full, (unsigned *)(v708), svreinterpret_u64_s16(v402));
    svst1w_u64(pred_full, (unsigned *)(v744), svreinterpret_u64_s16(v434));
    svst1w_u64(pred_full, (unsigned *)(v627), svreinterpret_u64_s16(v330));
    svst1w_u64(pred_full, (unsigned *)(v645), svreinterpret_u64_s16(v346));
    svst1w_u64(pred_full, (unsigned *)(v663), svreinterpret_u64_s16(v362));
    svst1w_u64(pred_full, (unsigned *)(v681), svreinterpret_u64_s16(v378));
    svst1w_u64(pred_full, (unsigned *)(v699), svreinterpret_u64_s16(v394));
    svst1w_u64(pred_full, (unsigned *)(v717), svreinterpret_u64_s16(v410));
    svst1w_u64(pred_full, (unsigned *)(v735), svreinterpret_u64_s16(v426));
    svst1w_u64(pred_full, (unsigned *)(v753), svreinterpret_u64_s16(v442));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu17(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v173 = -4.2602849117736000e-02F;
    float v177 = 2.0497965023262180e-01F;
    float v181 = 1.0451835201736759e+00F;
    float v185 = 1.7645848660222969e+00F;
    float v189 = -7.2340797728605655e-01F;
    float v193 = -8.9055591620606403e-02F;
    float v197 = -1.0625000000000000e+00F;
    float v201 = 2.5769410160110379e-01F;
    float v205 = 7.7980260789483757e-01F;
    float v209 = 5.4389318464570580e-01F;
    float v213 = 4.2010193497052700e-01F;
    float v217 = 1.2810929434228073e+00F;
    float v221 = 4.4088907348175338e-01F;
    float v225 = 3.1717619283272508e-01F;
    float v228 = -9.0138318648016680e-01F;
    float v229 = 9.0138318648016680e-01F;
    float v235 = -4.3248756360072310e-01F;
    float v236 = 4.3248756360072310e-01F;
    float v242 = 6.6693537504044498e-01F;
    float v243 = -6.6693537504044498e-01F;
    float v249 = -6.0389004312516970e-01F;
    float v250 = 6.0389004312516970e-01F;
    float v256 = -3.6924873198582547e-01F;
    float v257 = 3.6924873198582547e-01F;
    float v263 = 4.8656938755549761e-01F;
    float v264 = -4.8656938755549761e-01F;
    float v270 = 2.3813712136760609e-01F;
    float v271 = -2.3813712136760609e-01F;
    float v277 = -1.5573820617422458e+00F;
    float v278 = 1.5573820617422458e+00F;
    float v284 = 6.5962247018731990e-01F;
    float v285 = -6.5962247018731990e-01F;
    float v291 = -1.4316961569866241e-01F;
    float v292 = 1.4316961569866241e-01F;
    float v298 = 2.3903469959860771e-01F;
    float v299 = -2.3903469959860771e-01F;
    float v305 = -4.7932541949972603e-02F;
    float v306 = 4.7932541949972603e-02F;
    float v312 = -2.3188014856550065e+00F;
    float v313 = 2.3188014856550065e+00F;
    float v319 = 7.8914568419206255e-01F;
    float v320 = -7.8914568419206255e-01F;
    float v326 = 3.8484572871179505e+00F;
    float v327 = -3.8484572871179505e+00F;
    float v333 = -1.3003804568801376e+00F;
    float v334 = 1.3003804568801376e+00F;
    float v340 = 4.0814769046889037e+00F;
    float v341 = -4.0814769046889037e+00F;
    float v347 = -1.4807159909286283e+00F;
    float v348 = 1.4807159909286283e+00F;
    float v354 = -1.3332470363551400e-02F;
    float v355 = 1.3332470363551400e-02F;
    float v361 = -3.7139778690557629e-01F;
    float v362 = 3.7139778690557629e-01F;
    float v368 = 1.9236512863456379e-01F;
    float v369 = -1.9236512863456379e-01F;
    float32x2_t v371 = (float32x2_t){v4, v4};
    float32x2_t v166 = v5[0];
    float32x2_t v174 = (float32x2_t){v173, v173};
    float32x2_t v178 = (float32x2_t){v177, v177};
    float32x2_t v182 = (float32x2_t){v181, v181};
    float32x2_t v186 = (float32x2_t){v185, v185};
    float32x2_t v190 = (float32x2_t){v189, v189};
    float32x2_t v194 = (float32x2_t){v193, v193};
    float32x2_t v198 = (float32x2_t){v197, v197};
    float32x2_t v202 = (float32x2_t){v201, v201};
    float32x2_t v206 = (float32x2_t){v205, v205};
    float32x2_t v210 = (float32x2_t){v209, v209};
    float32x2_t v214 = (float32x2_t){v213, v213};
    float32x2_t v218 = (float32x2_t){v217, v217};
    float32x2_t v222 = (float32x2_t){v221, v221};
    float32x2_t v226 = (float32x2_t){v225, v225};
    float32x2_t v230 = (float32x2_t){v228, v229};
    float32x2_t v237 = (float32x2_t){v235, v236};
    float32x2_t v244 = (float32x2_t){v242, v243};
    float32x2_t v251 = (float32x2_t){v249, v250};
    float32x2_t v258 = (float32x2_t){v256, v257};
    float32x2_t v265 = (float32x2_t){v263, v264};
    float32x2_t v272 = (float32x2_t){v270, v271};
    float32x2_t v279 = (float32x2_t){v277, v278};
    float32x2_t v286 = (float32x2_t){v284, v285};
    float32x2_t v293 = (float32x2_t){v291, v292};
    float32x2_t v300 = (float32x2_t){v298, v299};
    float32x2_t v307 = (float32x2_t){v305, v306};
    float32x2_t v314 = (float32x2_t){v312, v313};
    float32x2_t v321 = (float32x2_t){v319, v320};
    float32x2_t v328 = (float32x2_t){v326, v327};
    float32x2_t v335 = (float32x2_t){v333, v334};
    float32x2_t v342 = (float32x2_t){v340, v341};
    float32x2_t v349 = (float32x2_t){v347, v348};
    float32x2_t v356 = (float32x2_t){v354, v355};
    float32x2_t v363 = (float32x2_t){v361, v362};
    float32x2_t v370 = (float32x2_t){v368, v369};
    float32x2_t v25 = v5[istride * 16];
    float32x2_t v32 = v5[istride * 3];
    float32x2_t v37 = v5[istride * 14];
    float32x2_t v44 = v5[istride * 9];
    float32x2_t v49 = v5[istride * 8];
    float32x2_t v56 = v5[istride * 10];
    float32x2_t v61 = v5[istride * 7];
    float32x2_t v68 = v5[istride * 13];
    float32x2_t v73 = v5[istride * 4];
    float32x2_t v80 = v5[istride * 5];
    float32x2_t v85 = v5[istride * 12];
    float32x2_t v92 = v5[istride * 15];
    float32x2_t v97 = v5[istride * 2];
    float32x2_t v104 = v5[istride * 11];
    float32x2_t v109 = v5[istride * 6];
    float32x2_t v232 = vmul_f32(v371, v230);
    float32x2_t v239 = vmul_f32(v371, v237);
    float32x2_t v246 = vmul_f32(v371, v244);
    float32x2_t v253 = vmul_f32(v371, v251);
    float32x2_t v260 = vmul_f32(v371, v258);
    float32x2_t v267 = vmul_f32(v371, v265);
    float32x2_t v274 = vmul_f32(v371, v272);
    float32x2_t v281 = vmul_f32(v371, v279);
    float32x2_t v288 = vmul_f32(v371, v286);
    float32x2_t v295 = vmul_f32(v371, v293);
    float32x2_t v302 = vmul_f32(v371, v300);
    float32x2_t v309 = vmul_f32(v371, v307);
    float32x2_t v316 = vmul_f32(v371, v314);
    float32x2_t v323 = vmul_f32(v371, v321);
    float32x2_t v330 = vmul_f32(v371, v328);
    float32x2_t v337 = vmul_f32(v371, v335);
    float32x2_t v344 = vmul_f32(v371, v342);
    float32x2_t v351 = vmul_f32(v371, v349);
    float32x2_t v358 = vmul_f32(v371, v356);
    float32x2_t v365 = vmul_f32(v371, v363);
    float32x2_t v372 = vmul_f32(v371, v370);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v74 = vadd_f32(v68, v73);
    float32x2_t v75 = vsub_f32(v68, v73);
    float32x2_t v86 = vadd_f32(v80, v85);
    float32x2_t v87 = vsub_f32(v80, v85);
    float32x2_t v98 = vadd_f32(v92, v97);
    float32x2_t v99 = vsub_f32(v92, v97);
    float32x2_t v110 = vadd_f32(v104, v109);
    float32x2_t v111 = vsub_f32(v104, v109);
    float32x2_t v112 = vadd_f32(v26, v74);
    float32x2_t v113 = vadd_f32(v38, v86);
    float32x2_t v114 = vadd_f32(v50, v98);
    float32x2_t v115 = vadd_f32(v62, v110);
    float32x2_t v118 = vsub_f32(v26, v74);
    float32x2_t v119 = vsub_f32(v38, v86);
    float32x2_t v120 = vsub_f32(v50, v98);
    float32x2_t v121 = vsub_f32(v62, v110);
    float32x2_t v132 = vadd_f32(v27, v51);
    float32x2_t v133 = vadd_f32(v39, v63);
    float32x2_t v134 = vsub_f32(v27, v51);
    float32x2_t v135 = vsub_f32(v111, v87);
    float32x2_t v136 = vadd_f32(v75, v99);
    float32x2_t v137 = vadd_f32(v87, v111);
    float32x2_t v138 = vsub_f32(v75, v99);
    float32x2_t v139 = vsub_f32(v39, v63);
    float32x2_t v152 = vadd_f32(v27, v75);
    float32x2_t v153 = vadd_f32(v63, v111);
    float32x2_t v324 = vrev64_f32(v27);
    float32x2_t v331 = vrev64_f32(v75);
    float32x2_t v345 = vrev64_f32(v63);
    float32x2_t v352 = vrev64_f32(v111);
    float32x2_t v116 = vadd_f32(v112, v114);
    float32x2_t v117 = vadd_f32(v113, v115);
    float32x2_t v122 = vsub_f32(v112, v114);
    float32x2_t v123 = vsub_f32(v113, v115);
    float32x2_t v126 = vadd_f32(v119, v121);
    float32x2_t v127 = vadd_f32(v118, v120);
    float32x2_t v129 = vsub_f32(v120, v121);
    float32x2_t v130 = vsub_f32(v118, v119);
    float32x2_t v140 = vadd_f32(v132, v133);
    float32x2_t v141 = vadd_f32(v136, v137);
    float32x2_t v143 = vsub_f32(v132, v133);
    float32x2_t v144 = vsub_f32(v136, v137);
    float32x2_t v146 = vadd_f32(v134, v135);
    float32x2_t v147 = vadd_f32(v138, v139);
    float32x2_t v149 = vsub_f32(v134, v135);
    float32x2_t v150 = vsub_f32(v138, v139);
    float32x2_t v175 = vmul_f32(v118, v174);
    float32x2_t v179 = vmul_f32(v119, v178);
    float32x2_t v183 = vmul_f32(v120, v182);
    float32x2_t v187 = vmul_f32(v121, v186);
    float32x2_t v317 = vrev64_f32(v152);
    float32x2_t v325 = vmul_f32(v324, v323);
    float32x2_t v332 = vmul_f32(v331, v330);
    float32x2_t v338 = vrev64_f32(v153);
    float32x2_t v346 = vmul_f32(v345, v344);
    float32x2_t v353 = vmul_f32(v352, v351);
    float32x2_t v124 = vadd_f32(v116, v117);
    float32x2_t v125 = vsub_f32(v116, v117);
    float32x2_t v128 = vsub_f32(v127, v126);
    float32x2_t v131 = vadd_f32(v122, v123);
    float32x2_t v142 = vadd_f32(v140, v141);
    float32x2_t v145 = vadd_f32(v143, v144);
    float32x2_t v148 = vadd_f32(v146, v147);
    float32x2_t v151 = vadd_f32(v149, v150);
    float32x2_t v154 = vsub_f32(v147, v141);
    float32x2_t v157 = vsub_f32(v140, v146);
    float32x2_t v191 = vmul_f32(v122, v190);
    float32x2_t v195 = vmul_f32(v123, v194);
    float32x2_t v207 = vmul_f32(v126, v206);
    float32x2_t v211 = vmul_f32(v127, v210);
    float32x2_t v219 = vmul_f32(v129, v218);
    float32x2_t v223 = vmul_f32(v130, v222);
    float32x2_t v233 = vrev64_f32(v140);
    float32x2_t v240 = vrev64_f32(v141);
    float32x2_t v254 = vrev64_f32(v143);
    float32x2_t v261 = vrev64_f32(v144);
    float32x2_t v275 = vrev64_f32(v146);
    float32x2_t v282 = vrev64_f32(v147);
    float32x2_t v296 = vrev64_f32(v149);
    float32x2_t v303 = vrev64_f32(v150);
    float32x2_t v318 = vmul_f32(v317, v316);
    float32x2_t v339 = vmul_f32(v338, v337);
    float32x2_t v155 = vadd_f32(v154, v27);
    float32x2_t v158 = vadd_f32(v157, v63);
    float32x2_t v167 = vadd_f32(v166, v124);
    float32x2_t v199 = vmul_f32(v124, v198);
    float32x2_t v203 = vmul_f32(v125, v202);
    float32x2_t v215 = vmul_f32(v128, v214);
    float32x2_t v227 = vmul_f32(v131, v226);
    float32x2_t v234 = vmul_f32(v233, v232);
    float32x2_t v241 = vmul_f32(v240, v239);
    float32x2_t v247 = vrev64_f32(v142);
    float32x2_t v255 = vmul_f32(v254, v253);
    float32x2_t v262 = vmul_f32(v261, v260);
    float32x2_t v268 = vrev64_f32(v145);
    float32x2_t v276 = vmul_f32(v275, v274);
    float32x2_t v283 = vmul_f32(v282, v281);
    float32x2_t v289 = vrev64_f32(v148);
    float32x2_t v297 = vmul_f32(v296, v295);
    float32x2_t v304 = vmul_f32(v303, v302);
    float32x2_t v310 = vrev64_f32(v151);
    float32x2_t v377 = vadd_f32(v187, v219);
    float32x2_t v378 = vsub_f32(v219, v183);
    float32x2_t v379 = vadd_f32(v179, v223);
    float32x2_t v380 = vsub_f32(v175, v223);
    float32x2_t v156 = vsub_f32(v155, v153);
    float32x2_t v159 = vadd_f32(v158, v75);
    float32x2_t v248 = vmul_f32(v247, v246);
    float32x2_t v269 = vmul_f32(v268, v267);
    float32x2_t v290 = vmul_f32(v289, v288);
    float32x2_t v311 = vmul_f32(v310, v309);
    float32x2_t v375 = vadd_f32(v207, v215);
    float32x2_t v376 = vsub_f32(v211, v215);
    float32x2_t v381 = vsub_f32(v227, v195);
    float32x2_t v382 = vadd_f32(v227, v191);
    float32x2_t v383 = vadd_f32(v199, v167);
    int16x4_t v451 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v167, 15), (int32x2_t){0, 0}));
    float32x2_t v160 = vsub_f32(v159, v111);
    float32x2_t v359 = vrev64_f32(v156);
    float32x2_t v384 = vadd_f32(v203, v383);
    float32x2_t v385 = vsub_f32(v383, v203);
    float32x2_t v386 = vsub_f32(v375, v377);
    float32x2_t v388 = vadd_f32(v376, v378);
    float32x2_t v390 = vadd_f32(v375, v379);
    float32x2_t v392 = vadd_f32(v376, v380);
    float32x2_t v402 = vadd_f32(v234, v248);
    float32x2_t v403 = vadd_f32(v241, v248);
    float32x2_t v404 = vadd_f32(v255, v269);
    float32x2_t v405 = vadd_f32(v262, v269);
    float32x2_t v406 = vadd_f32(v276, v290);
    float32x2_t v407 = vadd_f32(v283, v290);
    float32x2_t v408 = vadd_f32(v297, v311);
    float32x2_t v409 = vadd_f32(v304, v311);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v451), 0);
    float32x2_t v161 = vadd_f32(v156, v160);
    float32x2_t v360 = vmul_f32(v359, v358);
    float32x2_t v366 = vrev64_f32(v160);
    float32x2_t v387 = vadd_f32(v381, v384);
    float32x2_t v389 = vadd_f32(v382, v385);
    float32x2_t v391 = vsub_f32(v384, v381);
    float32x2_t v393 = vsub_f32(v385, v382);
    float32x2_t v413 = vadd_f32(v402, v404);
    float32x2_t v414 = vsub_f32(v402, v404);
    float32x2_t v415 = vadd_f32(v403, v405);
    float32x2_t v416 = vsub_f32(v403, v405);
    float32x2_t v417 = vadd_f32(v406, v408);
    float32x2_t v418 = vsub_f32(v408, v406);
    float32x2_t v419 = vadd_f32(v407, v409);
    float32x2_t v420 = vsub_f32(v409, v407);
    float32x2_t v367 = vmul_f32(v366, v365);
    float32x2_t v373 = vrev64_f32(v161);
    float32x2_t v394 = vadd_f32(v386, v387);
    float32x2_t v395 = vadd_f32(v388, v389);
    float32x2_t v396 = vadd_f32(v390, v391);
    float32x2_t v397 = vadd_f32(v392, v393);
    float32x2_t v398 = vsub_f32(v387, v386);
    float32x2_t v399 = vsub_f32(v389, v388);
    float32x2_t v400 = vsub_f32(v391, v390);
    float32x2_t v401 = vsub_f32(v393, v392);
    float32x2_t v430 = vadd_f32(v415, v419);
    float32x2_t v432 = vadd_f32(v414, v420);
    float32x2_t v434 = vsub_f32(v413, v417);
    float32x2_t v436 = vsub_f32(v420, v414);
    float32x2_t v438 = vadd_f32(v413, v417);
    float32x2_t v441 = vsub_f32(v418, v416);
    float32x2_t v444 = vsub_f32(v419, v415);
    float32x2_t v447 = vadd_f32(v416, v418);
    float32x2_t v374 = vmul_f32(v373, v372);
    float32x2_t v421 = vsub_f32(v360, v367);
    float32x2_t v410 = vadd_f32(v374, v367);
    float32x2_t v423 = vadd_f32(v421, v421);
    float32x2_t v448 = vsub_f32(v447, v421);
    float32x2_t v411 = vadd_f32(v318, v410);
    float32x2_t v424 = vsub_f32(v339, v423);
    float32x2_t v427 = vadd_f32(v410, v410);
    float32x2_t v445 = vadd_f32(v444, v423);
    float32x2_t v483 = vadd_f32(v401, v448);
    float32x2_t v490 = vsub_f32(v401, v448);
    float32x2_t v412 = vadd_f32(v411, v325);
    float32x2_t v422 = vadd_f32(v411, v332);
    float32x2_t v425 = vadd_f32(v424, v346);
    float32x2_t v426 = vadd_f32(v424, v353);
    float32x2_t v428 = vadd_f32(v427, v427);
    float32x2_t v429 = vadd_f32(v421, v427);
    float32x2_t v435 = vadd_f32(v434, v427);
    float32x2_t v446 = vadd_f32(v445, v427);
    int16x4_t v486 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v483, 15), (int32x2_t){0, 0}));
    int16x4_t v493 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v490, 15), (int32x2_t){0, 0}));
    float32x2_t v431 = vadd_f32(v430, v422);
    float32x2_t v433 = vadd_f32(v432, v425);
    float32x2_t v437 = vsub_f32(v436, v429);
    float32x2_t v439 = vadd_f32(v438, v412);
    float32x2_t v442 = vsub_f32(v441, v426);
    float32x2_t v469 = vadd_f32(v396, v435);
    float32x2_t v476 = vsub_f32(v396, v435);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v486), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v493), 0);
    float32x2_t v553 = vadd_f32(v400, v446);
    float32x2_t v560 = vsub_f32(v400, v446);
    float32x2_t v440 = vadd_f32(v439, v421);
    float32x2_t v443 = vadd_f32(v442, v428);
    float32x2_t v455 = vadd_f32(v394, v431);
    float32x2_t v462 = vsub_f32(v394, v431);
    int16x4_t v472 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v469, 15), (int32x2_t){0, 0}));
    int16x4_t v479 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v476, 15), (int32x2_t){0, 0}));
    float32x2_t v511 = vadd_f32(v397, v437);
    float32x2_t v518 = vsub_f32(v397, v437);
    float32x2_t v525 = vadd_f32(v395, v433);
    float32x2_t v532 = vsub_f32(v395, v433);
    int16x4_t v556 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v553, 15), (int32x2_t){0, 0}));
    int16x4_t v563 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v560, 15), (int32x2_t){0, 0}));
    int16x4_t v458 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v455, 15), (int32x2_t){0, 0}));
    int16x4_t v465 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v462, 15), (int32x2_t){0, 0}));
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v472), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v479), 0);
    float32x2_t v497 = vadd_f32(v398, v440);
    float32x2_t v504 = vsub_f32(v398, v440);
    int16x4_t v514 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v511, 15), (int32x2_t){0, 0}));
    int16x4_t v521 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v518, 15), (int32x2_t){0, 0}));
    int16x4_t v528 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v525, 15), (int32x2_t){0, 0}));
    int16x4_t v535 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v532, 15), (int32x2_t){0, 0}));
    float32x2_t v539 = vadd_f32(v399, v443);
    float32x2_t v546 = vsub_f32(v399, v443);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v556), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v563), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v458), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v465), 0);
    int16x4_t v500 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v497, 15), (int32x2_t){0, 0}));
    int16x4_t v507 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v504, 15), (int32x2_t){0, 0}));
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v514), 0);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v521), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v528), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v535), 0);
    int16x4_t v542 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v539, 15), (int32x2_t){0, 0}));
    int16x4_t v549 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v546, 15), (int32x2_t){0, 0}));
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v500), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v507), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v542), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v549), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu17(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v210 = -4.2602849117736000e-02F;
    float v215 = 2.0497965023262180e-01F;
    float v220 = 1.0451835201736759e+00F;
    float v225 = 1.7645848660222969e+00F;
    float v230 = -7.2340797728605655e-01F;
    float v235 = -8.9055591620606403e-02F;
    float v240 = -1.0625000000000000e+00F;
    float v245 = 2.5769410160110379e-01F;
    float v250 = 7.7980260789483757e-01F;
    float v255 = 5.4389318464570580e-01F;
    float v260 = 4.2010193497052700e-01F;
    float v265 = 1.2810929434228073e+00F;
    float v270 = 4.4088907348175338e-01F;
    float v275 = 3.1717619283272508e-01F;
    float v280 = 9.0138318648016680e-01F;
    float v287 = 4.3248756360072310e-01F;
    float v294 = -6.6693537504044498e-01F;
    float v301 = 6.0389004312516970e-01F;
    float v308 = 3.6924873198582547e-01F;
    float v315 = -4.8656938755549761e-01F;
    float v322 = -2.3813712136760609e-01F;
    float v329 = 1.5573820617422458e+00F;
    float v336 = -6.5962247018731990e-01F;
    float v343 = 1.4316961569866241e-01F;
    float v350 = -2.3903469959860771e-01F;
    float v357 = 4.7932541949972603e-02F;
    float v364 = 2.3188014856550065e+00F;
    float v371 = -7.8914568419206255e-01F;
    float v378 = -3.8484572871179505e+00F;
    float v385 = 1.3003804568801376e+00F;
    float v392 = -4.0814769046889037e+00F;
    float v399 = 1.4807159909286283e+00F;
    float v406 = 1.3332470363551400e-02F;
    float v413 = 3.7139778690557629e-01F;
    float v420 = -1.9236512863456379e-01F;
    const float32x2_t *v658 = &v5[v0];
    int32_t *v858 = &v6[v2];
    int64_t v26 = v0 * 16;
    int64_t v35 = v0 * 3;
    int64_t v42 = v0 * 14;
    int64_t v51 = v0 * 9;
    int64_t v58 = v0 * 8;
    int64_t v67 = v0 * 10;
    int64_t v74 = v0 * 7;
    int64_t v83 = v0 * 13;
    int64_t v90 = v0 * 4;
    int64_t v99 = v0 * 5;
    int64_t v106 = v0 * 12;
    int64_t v115 = v0 * 15;
    int64_t v122 = v0 * 2;
    int64_t v131 = v0 * 11;
    int64_t v138 = v0 * 6;
    float v283 = v4 * v280;
    float v290 = v4 * v287;
    float v297 = v4 * v294;
    float v304 = v4 * v301;
    float v311 = v4 * v308;
    float v318 = v4 * v315;
    float v325 = v4 * v322;
    float v332 = v4 * v329;
    float v339 = v4 * v336;
    float v346 = v4 * v343;
    float v353 = v4 * v350;
    float v360 = v4 * v357;
    float v367 = v4 * v364;
    float v374 = v4 * v371;
    float v381 = v4 * v378;
    float v388 = v4 * v385;
    float v395 = v4 * v392;
    float v402 = v4 * v399;
    float v409 = v4 * v406;
    float v416 = v4 * v413;
    float v423 = v4 * v420;
    int64_t v519 = v2 * 16;
    int64_t v528 = v2 * 2;
    int64_t v537 = v2 * 15;
    int64_t v546 = v2 * 3;
    int64_t v555 = v2 * 14;
    int64_t v564 = v2 * 4;
    int64_t v573 = v2 * 13;
    int64_t v582 = v2 * 5;
    int64_t v591 = v2 * 12;
    int64_t v600 = v2 * 6;
    int64_t v609 = v2 * 11;
    int64_t v618 = v2 * 7;
    int64_t v627 = v2 * 10;
    int64_t v636 = v2 * 8;
    int64_t v645 = v2 * 9;
    const float32x2_t *v803 = &v5[0];
    svfloat32_t v807 = svdup_n_f32(v210);
    svfloat32_t v808 = svdup_n_f32(v215);
    svfloat32_t v809 = svdup_n_f32(v220);
    svfloat32_t v810 = svdup_n_f32(v225);
    svfloat32_t v811 = svdup_n_f32(v230);
    svfloat32_t v812 = svdup_n_f32(v235);
    svfloat32_t v813 = svdup_n_f32(v240);
    svfloat32_t v814 = svdup_n_f32(v245);
    svfloat32_t v815 = svdup_n_f32(v250);
    svfloat32_t v816 = svdup_n_f32(v255);
    svfloat32_t v817 = svdup_n_f32(v260);
    svfloat32_t v818 = svdup_n_f32(v265);
    svfloat32_t v819 = svdup_n_f32(v270);
    svfloat32_t v820 = svdup_n_f32(v275);
    int32_t *v849 = &v6[0];
    svfloat32_t v997 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v658)[0]));
    const float32x2_t *v667 = &v5[v26];
    const float32x2_t *v676 = &v5[v35];
    const float32x2_t *v685 = &v5[v42];
    const float32x2_t *v694 = &v5[v51];
    const float32x2_t *v703 = &v5[v58];
    const float32x2_t *v712 = &v5[v67];
    const float32x2_t *v721 = &v5[v74];
    const float32x2_t *v730 = &v5[v83];
    const float32x2_t *v739 = &v5[v90];
    const float32x2_t *v748 = &v5[v99];
    const float32x2_t *v757 = &v5[v106];
    const float32x2_t *v766 = &v5[v115];
    const float32x2_t *v775 = &v5[v122];
    const float32x2_t *v784 = &v5[v131];
    const float32x2_t *v793 = &v5[v138];
    svfloat32_t v821 = svdup_n_f32(v283);
    svfloat32_t v822 = svdup_n_f32(v290);
    svfloat32_t v823 = svdup_n_f32(v297);
    svfloat32_t v824 = svdup_n_f32(v304);
    svfloat32_t v825 = svdup_n_f32(v311);
    svfloat32_t v826 = svdup_n_f32(v318);
    svfloat32_t v827 = svdup_n_f32(v325);
    svfloat32_t v828 = svdup_n_f32(v332);
    svfloat32_t v829 = svdup_n_f32(v339);
    svfloat32_t v830 = svdup_n_f32(v346);
    svfloat32_t v831 = svdup_n_f32(v353);
    svfloat32_t v832 = svdup_n_f32(v360);
    svfloat32_t v833 = svdup_n_f32(v367);
    svfloat32_t v834 = svdup_n_f32(v374);
    svfloat32_t v835 = svdup_n_f32(v381);
    svfloat32_t v836 = svdup_n_f32(v388);
    svfloat32_t v837 = svdup_n_f32(v395);
    svfloat32_t v838 = svdup_n_f32(v402);
    svfloat32_t v839 = svdup_n_f32(v409);
    svfloat32_t v840 = svdup_n_f32(v416);
    svfloat32_t v841 = svdup_n_f32(v423);
    int32_t *v867 = &v6[v519];
    int32_t *v876 = &v6[v528];
    int32_t *v885 = &v6[v537];
    int32_t *v894 = &v6[v546];
    int32_t *v903 = &v6[v555];
    int32_t *v912 = &v6[v564];
    int32_t *v921 = &v6[v573];
    int32_t *v930 = &v6[v582];
    int32_t *v939 = &v6[v591];
    int32_t *v948 = &v6[v600];
    int32_t *v957 = &v6[v609];
    int32_t *v966 = &v6[v618];
    int32_t *v975 = &v6[v627];
    int32_t *v984 = &v6[v636];
    int32_t *v993 = &v6[v645];
    svfloat32_t v1029 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v803)[0]));
    svfloat32_t v999 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v667)[0]));
    svfloat32_t v1001 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v676)[0]));
    svfloat32_t v1003 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v685)[0]));
    svfloat32_t v1005 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v694)[0]));
    svfloat32_t v1007 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v703)[0]));
    svfloat32_t v1009 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v712)[0]));
    svfloat32_t v1011 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v721)[0]));
    svfloat32_t v1013 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v730)[0]));
    svfloat32_t v1015 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v739)[0]));
    svfloat32_t v1017 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v748)[0]));
    svfloat32_t v1019 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v757)[0]));
    svfloat32_t v1021 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v766)[0]));
    svfloat32_t v1023 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v775)[0]));
    svfloat32_t v1025 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v784)[0]));
    svfloat32_t v1027 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v793)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v997, v999);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v997, v999);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v1001, v1003);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v1001, v1003);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v1005, v1007);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v1005, v1007);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v1009, v1011);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v1009, v1011);
    svfloat32_t v96 = svadd_f32_x(svptrue_b32(), v1013, v1015);
    svfloat32_t v97 = svsub_f32_x(svptrue_b32(), v1013, v1015);
    svfloat32_t v112 = svadd_f32_x(svptrue_b32(), v1017, v1019);
    svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v1017, v1019);
    svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v1021, v1023);
    svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v1021, v1023);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v1025, v1027);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v1025, v1027);
    svfloat32_t v146 = svadd_f32_x(svptrue_b32(), v32, v96);
    svfloat32_t v147 = svadd_f32_x(svptrue_b32(), v48, v112);
    svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v64, v128);
    svfloat32_t v149 = svadd_f32_x(svptrue_b32(), v80, v144);
    svfloat32_t v152 = svsub_f32_x(svptrue_b32(), v32, v96);
    svfloat32_t v153 = svsub_f32_x(svptrue_b32(), v48, v112);
    svfloat32_t v154 = svsub_f32_x(svptrue_b32(), v64, v128);
    svfloat32_t v155 = svsub_f32_x(svptrue_b32(), v80, v144);
    svfloat32_t v166 = svadd_f32_x(svptrue_b32(), v33, v65);
    svfloat32_t v167 = svadd_f32_x(svptrue_b32(), v49, v81);
    svfloat32_t v168 = svsub_f32_x(svptrue_b32(), v33, v65);
    svfloat32_t v169 = svsub_f32_x(svptrue_b32(), v145, v113);
    svfloat32_t v170 = svadd_f32_x(svptrue_b32(), v97, v129);
    svfloat32_t v171 = svadd_f32_x(svptrue_b32(), v113, v145);
    svfloat32_t v172 = svsub_f32_x(svptrue_b32(), v97, v129);
    svfloat32_t v173 = svsub_f32_x(svptrue_b32(), v49, v81);
    svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v33, v97);
    svfloat32_t v187 = svadd_f32_x(svptrue_b32(), v81, v145);
    svfloat32_t v150 = svadd_f32_x(svptrue_b32(), v146, v148);
    svfloat32_t v151 = svadd_f32_x(svptrue_b32(), v147, v149);
    svfloat32_t v156 = svsub_f32_x(svptrue_b32(), v146, v148);
    svfloat32_t v157 = svsub_f32_x(svptrue_b32(), v147, v149);
    svfloat32_t v160 = svadd_f32_x(svptrue_b32(), v153, v155);
    svfloat32_t v161 = svadd_f32_x(svptrue_b32(), v152, v154);
    svfloat32_t v163 = svsub_f32_x(svptrue_b32(), v154, v155);
    svfloat32_t v164 = svsub_f32_x(svptrue_b32(), v152, v153);
    svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v166, v167);
    svfloat32_t v175 = svadd_f32_x(svptrue_b32(), v170, v171);
    svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v166, v167);
    svfloat32_t v178 = svsub_f32_x(svptrue_b32(), v170, v171);
    svfloat32_t v180 = svadd_f32_x(svptrue_b32(), v168, v169);
    svfloat32_t v181 = svadd_f32_x(svptrue_b32(), v172, v173);
    svfloat32_t v183 = svsub_f32_x(svptrue_b32(), v168, v169);
    svfloat32_t v184 = svsub_f32_x(svptrue_b32(), v172, v173);
    svfloat32_t v223 = svmul_f32_x(svptrue_b32(), v154, v809);
    svfloat32_t zero390 = svdup_n_f32(0);
    svfloat32_t v390 = svcmla_f32_x(pred_full, zero390, v836, v187, 90);
    svfloat32_t v158 = svadd_f32_x(svptrue_b32(), v150, v151);
    svfloat32_t v159 = svsub_f32_x(svptrue_b32(), v150, v151);
    svfloat32_t v162 = svsub_f32_x(svptrue_b32(), v161, v160);
    svfloat32_t v165 = svadd_f32_x(svptrue_b32(), v156, v157);
    svfloat32_t v176 = svadd_f32_x(svptrue_b32(), v174, v175);
    svfloat32_t v179 = svadd_f32_x(svptrue_b32(), v177, v178);
    svfloat32_t v182 = svadd_f32_x(svptrue_b32(), v180, v181);
    svfloat32_t v185 = svadd_f32_x(svptrue_b32(), v183, v184);
    svfloat32_t v188 = svsub_f32_x(svptrue_b32(), v181, v175);
    svfloat32_t v191 = svsub_f32_x(svptrue_b32(), v174, v180);
    svfloat32_t v233 = svmul_f32_x(svptrue_b32(), v156, v811);
    svfloat32_t v238 = svmul_f32_x(svptrue_b32(), v157, v812);
    svfloat32_t v268 = svmul_f32_x(svptrue_b32(), v163, v818);
    svfloat32_t v273 = svmul_f32_x(svptrue_b32(), v164, v819);
    svfloat32_t v189 = svadd_f32_x(svptrue_b32(), v188, v33);
    svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v191, v81);
    svfloat32_t v203 = svadd_f32_x(svptrue_b32(), v1029, v158);
    svfloat32_t v263 = svmul_f32_x(svptrue_b32(), v162, v817);
    svfloat32_t zero299 = svdup_n_f32(0);
    svfloat32_t v299 = svcmla_f32_x(pred_full, zero299, v823, v176, 90);
    svfloat32_t zero320 = svdup_n_f32(0);
    svfloat32_t v320 = svcmla_f32_x(pred_full, zero320, v826, v179, 90);
    svfloat32_t zero341 = svdup_n_f32(0);
    svfloat32_t v341 = svcmla_f32_x(pred_full, zero341, v829, v182, 90);
    svfloat32_t zero362 = svdup_n_f32(0);
    svfloat32_t v362 = svcmla_f32_x(pred_full, zero362, v832, v185, 90);
    svfloat32_t v428 = svmla_f32_x(pred_full, v268, v155, v810);
    svfloat32_t v429 = svnmls_f32_x(pred_full, v223, v163, v818);
    svfloat32_t v430 = svmla_f32_x(pred_full, v273, v153, v808);
    svfloat32_t v431 = svnmls_f32_x(pred_full, v273, v152, v807);
    svfloat32_t v190 = svsub_f32_x(svptrue_b32(), v189, v187);
    svfloat32_t v193 = svadd_f32_x(svptrue_b32(), v192, v97);
    svfloat32_t v426 = svmla_f32_x(pred_full, v263, v160, v815);
    svfloat32_t v427 = svnmls_f32_x(pred_full, v263, v161, v816);
    svfloat32_t v432 = svnmls_f32_x(pred_full, v238, v165, v820);
    svfloat32_t v433 = svmla_f32_x(pred_full, v233, v165, v820);
    svfloat32_t v434 = svmla_f32_x(pred_full, v203, v158, v813);
    svfloat32_t v453 = svcmla_f32_x(pred_full, v299, v821, v174, 90);
    svfloat32_t v454 = svcmla_f32_x(pred_full, v299, v822, v175, 90);
    svfloat32_t v455 = svcmla_f32_x(pred_full, v320, v824, v177, 90);
    svfloat32_t v456 = svcmla_f32_x(pred_full, v320, v825, v178, 90);
    svfloat32_t v457 = svcmla_f32_x(pred_full, v341, v827, v180, 90);
    svfloat32_t v458 = svcmla_f32_x(pred_full, v341, v828, v181, 90);
    svfloat32_t v459 = svcmla_f32_x(pred_full, v362, v830, v183, 90);
    svfloat32_t v460 = svcmla_f32_x(pred_full, v362, v831, v184, 90);
    svint16_t v502 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v203, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v194 = svsub_f32_x(svptrue_b32(), v193, v145);
    svfloat32_t zero411 = svdup_n_f32(0);
    svfloat32_t v411 = svcmla_f32_x(pred_full, zero411, v839, v190, 90);
    svfloat32_t v435 = svmla_f32_x(pred_full, v434, v159, v814);
    svfloat32_t v436 = svmls_f32_x(pred_full, v434, v159, v814);
    svfloat32_t v437 = svsub_f32_x(svptrue_b32(), v426, v428);
    svfloat32_t v439 = svadd_f32_x(svptrue_b32(), v427, v429);
    svfloat32_t v441 = svadd_f32_x(svptrue_b32(), v426, v430);
    svfloat32_t v443 = svadd_f32_x(svptrue_b32(), v427, v431);
    svfloat32_t v464 = svadd_f32_x(svptrue_b32(), v453, v455);
    svfloat32_t v465 = svsub_f32_x(svptrue_b32(), v453, v455);
    svfloat32_t v466 = svadd_f32_x(svptrue_b32(), v454, v456);
    svfloat32_t v467 = svsub_f32_x(svptrue_b32(), v454, v456);
    svfloat32_t v468 = svadd_f32_x(svptrue_b32(), v457, v459);
    svfloat32_t v469 = svsub_f32_x(svptrue_b32(), v459, v457);
    svfloat32_t v470 = svadd_f32_x(svptrue_b32(), v458, v460);
    svfloat32_t v471 = svsub_f32_x(svptrue_b32(), v460, v458);
    svst1w_u64(pred_full, (unsigned *)(v849), svreinterpret_u64_s16(v502));
    svfloat32_t v195 = svadd_f32_x(svptrue_b32(), v190, v194);
    svfloat32_t zero418 = svdup_n_f32(0);
    svfloat32_t v418 = svcmla_f32_x(pred_full, zero418, v840, v194, 90);
    svfloat32_t v438 = svadd_f32_x(svptrue_b32(), v432, v435);
    svfloat32_t v440 = svadd_f32_x(svptrue_b32(), v433, v436);
    svfloat32_t v442 = svsub_f32_x(svptrue_b32(), v435, v432);
    svfloat32_t v444 = svsub_f32_x(svptrue_b32(), v436, v433);
    svfloat32_t v481 = svadd_f32_x(svptrue_b32(), v466, v470);
    svfloat32_t v483 = svadd_f32_x(svptrue_b32(), v465, v471);
    svfloat32_t v485 = svsub_f32_x(svptrue_b32(), v464, v468);
    svfloat32_t v487 = svsub_f32_x(svptrue_b32(), v471, v465);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v464, v468);
    svfloat32_t v492 = svsub_f32_x(svptrue_b32(), v469, v467);
    svfloat32_t v495 = svsub_f32_x(svptrue_b32(), v470, v466);
    svfloat32_t v498 = svadd_f32_x(svptrue_b32(), v467, v469);
    svfloat32_t v445 = svadd_f32_x(svptrue_b32(), v437, v438);
    svfloat32_t v446 = svadd_f32_x(svptrue_b32(), v439, v440);
    svfloat32_t v447 = svadd_f32_x(svptrue_b32(), v441, v442);
    svfloat32_t v448 = svadd_f32_x(svptrue_b32(), v443, v444);
    svfloat32_t v449 = svsub_f32_x(svptrue_b32(), v438, v437);
    svfloat32_t v450 = svsub_f32_x(svptrue_b32(), v440, v439);
    svfloat32_t v451 = svsub_f32_x(svptrue_b32(), v442, v441);
    svfloat32_t v452 = svsub_f32_x(svptrue_b32(), v444, v443);
    svfloat32_t v472 = svsub_f32_x(svptrue_b32(), v411, v418);
    svfloat32_t v461 = svcmla_f32_x(pred_full, v418, v841, v195, 90);
    svfloat32_t v474 = svadd_f32_x(svptrue_b32(), v472, v472);
    svfloat32_t v499 = svsub_f32_x(svptrue_b32(), v498, v472);
    svfloat32_t v462 = svcmla_f32_x(pred_full, v461, v833, v186, 90);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v390, v474);
    svfloat32_t v478 = svadd_f32_x(svptrue_b32(), v461, v461);
    svfloat32_t v496 = svadd_f32_x(svptrue_b32(), v495, v474);
    svfloat32_t v544 = svadd_f32_x(svptrue_b32(), v452, v499);
    svfloat32_t v553 = svsub_f32_x(svptrue_b32(), v452, v499);
    svfloat32_t v463 = svcmla_f32_x(pred_full, v462, v834, v33, 90);
    svfloat32_t v473 = svcmla_f32_x(pred_full, v462, v835, v97, 90);
    svfloat32_t v476 = svcmla_f32_x(pred_full, v475, v837, v81, 90);
    svfloat32_t v477 = svcmla_f32_x(pred_full, v475, v838, v145, 90);
    svfloat32_t v479 = svadd_f32_x(svptrue_b32(), v478, v478);
    svfloat32_t v480 = svadd_f32_x(svptrue_b32(), v472, v478);
    svfloat32_t v486 = svadd_f32_x(svptrue_b32(), v485, v478);
    svfloat32_t v497 = svadd_f32_x(svptrue_b32(), v496, v478);
    svint16_t v547 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v544, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v556 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v553, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v482 = svadd_f32_x(svptrue_b32(), v481, v473);
    svfloat32_t v484 = svadd_f32_x(svptrue_b32(), v483, v476);
    svfloat32_t v488 = svsub_f32_x(svptrue_b32(), v487, v480);
    svfloat32_t v490 = svadd_f32_x(svptrue_b32(), v489, v463);
    svfloat32_t v493 = svsub_f32_x(svptrue_b32(), v492, v477);
    svfloat32_t v526 = svadd_f32_x(svptrue_b32(), v447, v486);
    svfloat32_t v535 = svsub_f32_x(svptrue_b32(), v447, v486);
    svfloat32_t v634 = svadd_f32_x(svptrue_b32(), v451, v497);
    svfloat32_t v643 = svsub_f32_x(svptrue_b32(), v451, v497);
    svst1w_u64(pred_full, (unsigned *)(v894), svreinterpret_u64_s16(v547));
    svst1w_u64(pred_full, (unsigned *)(v903), svreinterpret_u64_s16(v556));
    svfloat32_t v491 = svadd_f32_x(svptrue_b32(), v490, v472);
    svfloat32_t v494 = svadd_f32_x(svptrue_b32(), v493, v479);
    svfloat32_t v508 = svadd_f32_x(svptrue_b32(), v445, v482);
    svfloat32_t v517 = svsub_f32_x(svptrue_b32(), v445, v482);
    svint16_t v529 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v526, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v538 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v535, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v580 = svadd_f32_x(svptrue_b32(), v448, v488);
    svfloat32_t v589 = svsub_f32_x(svptrue_b32(), v448, v488);
    svfloat32_t v598 = svadd_f32_x(svptrue_b32(), v446, v484);
    svfloat32_t v607 = svsub_f32_x(svptrue_b32(), v446, v484);
    svint16_t v637 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v634, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v646 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v643, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v511 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v508, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v520 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v517, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v562 = svadd_f32_x(svptrue_b32(), v449, v491);
    svfloat32_t v571 = svsub_f32_x(svptrue_b32(), v449, v491);
    svint16_t v583 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v580, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v592 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v589, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v601 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v598, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v610 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v607, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v616 = svadd_f32_x(svptrue_b32(), v450, v494);
    svfloat32_t v625 = svsub_f32_x(svptrue_b32(), v450, v494);
    svst1w_u64(pred_full, (unsigned *)(v876), svreinterpret_u64_s16(v529));
    svst1w_u64(pred_full, (unsigned *)(v885), svreinterpret_u64_s16(v538));
    svst1w_u64(pred_full, (unsigned *)(v984), svreinterpret_u64_s16(v637));
    svst1w_u64(pred_full, (unsigned *)(v993), svreinterpret_u64_s16(v646));
    svint16_t v565 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v562, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v574 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v571, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v619 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v616, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v628 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v625, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v858), svreinterpret_u64_s16(v511));
    svst1w_u64(pred_full, (unsigned *)(v867), svreinterpret_u64_s16(v520));
    svst1w_u64(pred_full, (unsigned *)(v930), svreinterpret_u64_s16(v583));
    svst1w_u64(pred_full, (unsigned *)(v939), svreinterpret_u64_s16(v592));
    svst1w_u64(pred_full, (unsigned *)(v948), svreinterpret_u64_s16(v601));
    svst1w_u64(pred_full, (unsigned *)(v957), svreinterpret_u64_s16(v610));
    svst1w_u64(pred_full, (unsigned *)(v912), svreinterpret_u64_s16(v565));
    svst1w_u64(pred_full, (unsigned *)(v921), svreinterpret_u64_s16(v574));
    svst1w_u64(pred_full, (unsigned *)(v966), svreinterpret_u64_s16(v619));
    svst1w_u64(pred_full, (unsigned *)(v975), svreinterpret_u64_s16(v628));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu18(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v85 = v5[istride];
    float v253 = -5.0000000000000000e-01F;
    float v264 = -1.4999999999999998e+00F;
    float v267 = 8.6602540378443871e-01F;
    float v268 = -8.6602540378443871e-01F;
    float v275 = 7.6604444311897801e-01F;
    float v279 = 9.3969262078590832e-01F;
    float v283 = -1.7364817766693039e-01F;
    float v286 = 6.4278760968653925e-01F;
    float v287 = -6.4278760968653925e-01F;
    float v293 = -3.4202014332566888e-01F;
    float v294 = 3.4202014332566888e-01F;
    float v300 = 9.8480775301220802e-01F;
    float v301 = -9.8480775301220802e-01F;
    float32x2_t v303 = (float32x2_t){v4, v4};
    float32x2_t v20 = v5[0];
    float32x2_t v254 = (float32x2_t){v253, v253};
    float32x2_t v265 = (float32x2_t){v264, v264};
    float32x2_t v269 = (float32x2_t){v267, v268};
    float32x2_t v276 = (float32x2_t){v275, v275};
    float32x2_t v280 = (float32x2_t){v279, v279};
    float32x2_t v284 = (float32x2_t){v283, v283};
    float32x2_t v288 = (float32x2_t){v286, v287};
    float32x2_t v295 = (float32x2_t){v293, v294};
    float32x2_t v302 = (float32x2_t){v300, v301};
    float32x2_t v25 = v5[istride * 9];
    float32x2_t v32 = v5[istride * 2];
    float32x2_t v37 = v5[istride * 11];
    float32x2_t v44 = v5[istride * 4];
    float32x2_t v49 = v5[istride * 13];
    float32x2_t v56 = v5[istride * 6];
    float32x2_t v61 = v5[istride * 15];
    float32x2_t v68 = v5[istride * 8];
    float32x2_t v73 = v5[istride * 17];
    float32x2_t v80 = v5[istride * 10];
    float32x2_t v92 = v5[istride * 12];
    float32x2_t v97 = v5[istride * 3];
    float32x2_t v104 = v5[istride * 14];
    float32x2_t v109 = v5[istride * 5];
    float32x2_t v116 = v5[istride * 16];
    float32x2_t v121 = v5[istride * 7];
    float32x2_t v271 = vmul_f32(v303, v269);
    float32x2_t v290 = vmul_f32(v303, v288);
    float32x2_t v297 = vmul_f32(v303, v295);
    float32x2_t v304 = vmul_f32(v303, v302);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v74 = vadd_f32(v68, v73);
    float32x2_t v75 = vsub_f32(v68, v73);
    float32x2_t v86 = vadd_f32(v80, v85);
    float32x2_t v87 = vsub_f32(v80, v85);
    float32x2_t v98 = vadd_f32(v92, v97);
    float32x2_t v99 = vsub_f32(v92, v97);
    float32x2_t v110 = vadd_f32(v104, v109);
    float32x2_t v111 = vsub_f32(v104, v109);
    float32x2_t v122 = vadd_f32(v116, v121);
    float32x2_t v123 = vsub_f32(v116, v121);
    float32x2_t v124 = vadd_f32(v38, v122);
    float32x2_t v125 = vsub_f32(v38, v122);
    float32x2_t v126 = vadd_f32(v110, v50);
    float32x2_t v127 = vsub_f32(v110, v50);
    float32x2_t v128 = vadd_f32(v62, v98);
    float32x2_t v129 = vsub_f32(v62, v98);
    float32x2_t v130 = vadd_f32(v74, v86);
    float32x2_t v131 = vsub_f32(v74, v86);
    float32x2_t v228 = vadd_f32(v39, v123);
    float32x2_t v229 = vsub_f32(v39, v123);
    float32x2_t v230 = vadd_f32(v111, v51);
    float32x2_t v231 = vsub_f32(v111, v51);
    float32x2_t v232 = vadd_f32(v63, v99);
    float32x2_t v233 = vsub_f32(v63, v99);
    float32x2_t v234 = vadd_f32(v75, v87);
    float32x2_t v235 = vsub_f32(v75, v87);
    float32x2_t v132 = vadd_f32(v124, v126);
    float32x2_t v136 = vadd_f32(v125, v127);
    float32x2_t v138 = vsub_f32(v124, v126);
    float32x2_t v139 = vsub_f32(v126, v130);
    float32x2_t v140 = vsub_f32(v130, v124);
    float32x2_t v141 = vsub_f32(v125, v127);
    float32x2_t v142 = vsub_f32(v127, v131);
    float32x2_t v143 = vsub_f32(v131, v125);
    float32x2_t v162 = vmul_f32(v128, v265);
    float32x2_t v168 = vrev64_f32(v129);
    float32x2_t v236 = vadd_f32(v228, v230);
    float32x2_t v240 = vadd_f32(v229, v231);
    float32x2_t v242 = vsub_f32(v228, v230);
    float32x2_t v243 = vsub_f32(v230, v234);
    float32x2_t v244 = vsub_f32(v234, v228);
    float32x2_t v245 = vsub_f32(v229, v231);
    float32x2_t v246 = vsub_f32(v231, v235);
    float32x2_t v247 = vsub_f32(v235, v229);
    float32x2_t v266 = vmul_f32(v232, v265);
    float32x2_t v272 = vrev64_f32(v233);
    float32x2_t v133 = vadd_f32(v132, v130);
    float32x2_t v137 = vadd_f32(v136, v131);
    float32x2_t v169 = vmul_f32(v168, v271);
    float32x2_t v173 = vmul_f32(v138, v276);
    float32x2_t v177 = vmul_f32(v139, v280);
    float32x2_t v181 = vmul_f32(v140, v284);
    float32x2_t v187 = vrev64_f32(v141);
    float32x2_t v194 = vrev64_f32(v142);
    float32x2_t v201 = vrev64_f32(v143);
    float32x2_t v237 = vadd_f32(v236, v234);
    float32x2_t v241 = vadd_f32(v240, v235);
    float32x2_t v273 = vmul_f32(v272, v271);
    float32x2_t v277 = vmul_f32(v242, v276);
    float32x2_t v281 = vmul_f32(v243, v280);
    float32x2_t v285 = vmul_f32(v244, v284);
    float32x2_t v291 = vrev64_f32(v245);
    float32x2_t v298 = vrev64_f32(v246);
    float32x2_t v305 = vrev64_f32(v247);
    float32x2_t v134 = vadd_f32(v133, v128);
    float32x2_t v151 = vmul_f32(v133, v254);
    float32x2_t v157 = vrev64_f32(v137);
    float32x2_t v188 = vmul_f32(v187, v290);
    float32x2_t v195 = vmul_f32(v194, v297);
    float32x2_t v202 = vmul_f32(v201, v304);
    float32x2_t v238 = vadd_f32(v237, v232);
    float32x2_t v255 = vmul_f32(v237, v254);
    float32x2_t v261 = vrev64_f32(v241);
    float32x2_t v292 = vmul_f32(v291, v290);
    float32x2_t v299 = vmul_f32(v298, v297);
    float32x2_t v306 = vmul_f32(v305, v304);
    float32x2_t v135 = vadd_f32(v134, v26);
    float32x2_t v158 = vmul_f32(v157, v271);
    float32x2_t v203 = vadd_f32(v151, v151);
    float32x2_t v216 = vadd_f32(v169, v188);
    float32x2_t v218 = vsub_f32(v169, v195);
    float32x2_t v220 = vsub_f32(v169, v188);
    float32x2_t v239 = vadd_f32(v238, v27);
    float32x2_t v262 = vmul_f32(v261, v271);
    float32x2_t v307 = vadd_f32(v255, v255);
    float32x2_t v320 = vadd_f32(v273, v292);
    float32x2_t v322 = vsub_f32(v273, v299);
    float32x2_t v324 = vsub_f32(v273, v292);
    float32x2_t v204 = vadd_f32(v203, v151);
    float32x2_t v208 = vadd_f32(v135, v162);
    float32x2_t v217 = vadd_f32(v216, v195);
    float32x2_t v219 = vadd_f32(v218, v202);
    float32x2_t v221 = vsub_f32(v220, v202);
    float32x2_t v308 = vadd_f32(v307, v255);
    float32x2_t v312 = vadd_f32(v239, v266);
    float32x2_t v321 = vadd_f32(v320, v299);
    float32x2_t v323 = vadd_f32(v322, v306);
    float32x2_t v325 = vsub_f32(v324, v306);
    int16x4_t v334 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v135, 15), (int32x2_t){0, 0}));
    int16x4_t v340 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v239, 15), (int32x2_t){0, 0}));
    float32x2_t v205 = vadd_f32(v135, v204);
    float32x2_t v209 = vadd_f32(v208, v203);
    float32x2_t v309 = vadd_f32(v239, v308);
    float32x2_t v313 = vadd_f32(v312, v307);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v334), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v340), 0);
    float32x2_t v206 = vadd_f32(v205, v158);
    float32x2_t v207 = vsub_f32(v205, v158);
    float32x2_t v210 = vadd_f32(v209, v173);
    float32x2_t v212 = vsub_f32(v209, v177);
    float32x2_t v214 = vsub_f32(v209, v173);
    float32x2_t v310 = vadd_f32(v309, v262);
    float32x2_t v311 = vsub_f32(v309, v262);
    float32x2_t v314 = vadd_f32(v313, v277);
    float32x2_t v316 = vsub_f32(v313, v281);
    float32x2_t v318 = vsub_f32(v313, v277);
    float32x2_t v211 = vadd_f32(v210, v177);
    float32x2_t v213 = vadd_f32(v212, v181);
    float32x2_t v215 = vsub_f32(v214, v181);
    float32x2_t v315 = vadd_f32(v314, v281);
    float32x2_t v317 = vadd_f32(v316, v285);
    float32x2_t v319 = vsub_f32(v318, v285);
    int16x4_t v370 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v207, 15), (int32x2_t){0, 0}));
    int16x4_t v376 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v311, 15), (int32x2_t){0, 0}));
    int16x4_t v406 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v206, 15), (int32x2_t){0, 0}));
    int16x4_t v412 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v310, 15), (int32x2_t){0, 0}));
    float32x2_t v222 = vadd_f32(v211, v217);
    float32x2_t v223 = vsub_f32(v211, v217);
    float32x2_t v224 = vadd_f32(v213, v219);
    float32x2_t v225 = vsub_f32(v213, v219);
    float32x2_t v226 = vadd_f32(v215, v221);
    float32x2_t v227 = vsub_f32(v215, v221);
    float32x2_t v326 = vadd_f32(v315, v321);
    float32x2_t v327 = vsub_f32(v315, v321);
    float32x2_t v328 = vadd_f32(v317, v323);
    float32x2_t v329 = vsub_f32(v317, v323);
    float32x2_t v330 = vadd_f32(v319, v325);
    float32x2_t v331 = vsub_f32(v319, v325);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v370), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v376), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v406), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v412), 0);
    int16x4_t v346 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v223, 15), (int32x2_t){0, 0}));
    int16x4_t v352 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v327, 15), (int32x2_t){0, 0}));
    int16x4_t v358 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v224, 15), (int32x2_t){0, 0}));
    int16x4_t v364 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v328, 15), (int32x2_t){0, 0}));
    int16x4_t v382 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v227, 15), (int32x2_t){0, 0}));
    int16x4_t v388 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v331, 15), (int32x2_t){0, 0}));
    int16x4_t v394 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v226, 15), (int32x2_t){0, 0}));
    int16x4_t v400 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v330, 15), (int32x2_t){0, 0}));
    int16x4_t v418 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v225, 15), (int32x2_t){0, 0}));
    int16x4_t v424 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v329, 15), (int32x2_t){0, 0}));
    int16x4_t v430 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v222, 15), (int32x2_t){0, 0}));
    int16x4_t v436 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v326, 15), (int32x2_t){0, 0}));
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v346), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v352), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v358), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v364), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v382), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v388), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v394), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v400), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v418), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v424), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v430), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v436), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu18(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v298 = -5.0000000000000000e-01F;
    float v310 = -1.4999999999999998e+00F;
    float v315 = -8.6602540378443871e-01F;
    float v322 = 7.6604444311897801e-01F;
    float v327 = 9.3969262078590832e-01F;
    float v332 = -1.7364817766693039e-01F;
    float v337 = -6.4278760968653925e-01F;
    float v344 = 3.4202014332566888e-01F;
    float v351 = -9.8480775301220802e-01F;
    const float32x2_t *v632 = &v5[v0];
    int32_t *v745 = &v6[v2];
    int64_t v26 = v0 * 9;
    int64_t v35 = v0 * 2;
    int64_t v42 = v0 * 11;
    int64_t v51 = v0 * 4;
    int64_t v58 = v0 * 13;
    int64_t v67 = v0 * 6;
    int64_t v74 = v0 * 15;
    int64_t v83 = v0 * 8;
    int64_t v90 = v0 * 17;
    int64_t v99 = v0 * 10;
    int64_t v115 = v0 * 12;
    int64_t v122 = v0 * 3;
    int64_t v131 = v0 * 14;
    int64_t v138 = v0 * 5;
    int64_t v147 = v0 * 16;
    int64_t v154 = v0 * 7;
    float v318 = v4 * v315;
    float v340 = v4 * v337;
    float v347 = v4 * v344;
    float v354 = v4 * v351;
    int64_t v391 = v2 * 9;
    int64_t v399 = v2 * 10;
    int64_t v415 = v2 * 2;
    int64_t v423 = v2 * 11;
    int64_t v431 = v2 * 12;
    int64_t v439 = v2 * 3;
    int64_t v447 = v2 * 4;
    int64_t v455 = v2 * 13;
    int64_t v463 = v2 * 14;
    int64_t v471 = v2 * 5;
    int64_t v479 = v2 * 6;
    int64_t v487 = v2 * 15;
    int64_t v495 = v2 * 16;
    int64_t v503 = v2 * 7;
    int64_t v511 = v2 * 8;
    int64_t v519 = v2 * 17;
    const float32x2_t *v533 = &v5[0];
    svfloat32_t v701 = svdup_n_f32(v298);
    svfloat32_t v703 = svdup_n_f32(v310);
    svfloat32_t v705 = svdup_n_f32(v322);
    svfloat32_t v706 = svdup_n_f32(v327);
    svfloat32_t v707 = svdup_n_f32(v332);
    int32_t *v718 = &v6[0];
    svfloat32_t v897 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v632)[0]));
    const float32x2_t *v542 = &v5[v26];
    const float32x2_t *v551 = &v5[v35];
    const float32x2_t *v560 = &v5[v42];
    const float32x2_t *v569 = &v5[v51];
    const float32x2_t *v578 = &v5[v58];
    const float32x2_t *v587 = &v5[v67];
    const float32x2_t *v596 = &v5[v74];
    const float32x2_t *v605 = &v5[v83];
    const float32x2_t *v614 = &v5[v90];
    const float32x2_t *v623 = &v5[v99];
    const float32x2_t *v641 = &v5[v115];
    const float32x2_t *v650 = &v5[v122];
    const float32x2_t *v659 = &v5[v131];
    const float32x2_t *v668 = &v5[v138];
    const float32x2_t *v677 = &v5[v147];
    const float32x2_t *v686 = &v5[v154];
    svfloat32_t v704 = svdup_n_f32(v318);
    svfloat32_t v708 = svdup_n_f32(v340);
    svfloat32_t v709 = svdup_n_f32(v347);
    svfloat32_t v710 = svdup_n_f32(v354);
    int32_t *v727 = &v6[v391];
    int32_t *v736 = &v6[v399];
    int32_t *v754 = &v6[v415];
    int32_t *v763 = &v6[v423];
    int32_t *v772 = &v6[v431];
    int32_t *v781 = &v6[v439];
    int32_t *v790 = &v6[v447];
    int32_t *v799 = &v6[v455];
    int32_t *v808 = &v6[v463];
    int32_t *v817 = &v6[v471];
    int32_t *v826 = &v6[v479];
    int32_t *v835 = &v6[v487];
    int32_t *v844 = &v6[v495];
    int32_t *v853 = &v6[v503];
    int32_t *v862 = &v6[v511];
    int32_t *v871 = &v6[v519];
    svfloat32_t v875 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v533)[0]));
    svfloat32_t v877 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v542)[0]));
    svfloat32_t v879 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v551)[0]));
    svfloat32_t v881 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v560)[0]));
    svfloat32_t v883 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v569)[0]));
    svfloat32_t v885 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v578)[0]));
    svfloat32_t v887 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v587)[0]));
    svfloat32_t v889 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v596)[0]));
    svfloat32_t v891 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v605)[0]));
    svfloat32_t v893 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v614)[0]));
    svfloat32_t v895 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v623)[0]));
    svfloat32_t v899 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v641)[0]));
    svfloat32_t v901 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v650)[0]));
    svfloat32_t v903 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v659)[0]));
    svfloat32_t v905 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v668)[0]));
    svfloat32_t v907 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v677)[0]));
    svfloat32_t v909 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v686)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v875, v877);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v875, v877);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v879, v881);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v879, v881);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v883, v885);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v883, v885);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v887, v889);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v887, v889);
    svfloat32_t v96 = svadd_f32_x(svptrue_b32(), v891, v893);
    svfloat32_t v97 = svsub_f32_x(svptrue_b32(), v891, v893);
    svfloat32_t v112 = svadd_f32_x(svptrue_b32(), v895, v897);
    svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v895, v897);
    svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v899, v901);
    svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v899, v901);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v903, v905);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v903, v905);
    svfloat32_t v160 = svadd_f32_x(svptrue_b32(), v907, v909);
    svfloat32_t v161 = svsub_f32_x(svptrue_b32(), v907, v909);
    svfloat32_t v162 = svadd_f32_x(svptrue_b32(), v48, v160);
    svfloat32_t v163 = svsub_f32_x(svptrue_b32(), v48, v160);
    svfloat32_t v164 = svadd_f32_x(svptrue_b32(), v144, v64);
    svfloat32_t v165 = svsub_f32_x(svptrue_b32(), v144, v64);
    svfloat32_t v166 = svadd_f32_x(svptrue_b32(), v80, v128);
    svfloat32_t v167 = svsub_f32_x(svptrue_b32(), v80, v128);
    svfloat32_t v168 = svadd_f32_x(svptrue_b32(), v96, v112);
    svfloat32_t v169 = svsub_f32_x(svptrue_b32(), v96, v112);
    svfloat32_t v272 = svadd_f32_x(svptrue_b32(), v49, v161);
    svfloat32_t v273 = svsub_f32_x(svptrue_b32(), v49, v161);
    svfloat32_t v274 = svadd_f32_x(svptrue_b32(), v145, v65);
    svfloat32_t v275 = svsub_f32_x(svptrue_b32(), v145, v65);
    svfloat32_t v276 = svadd_f32_x(svptrue_b32(), v81, v129);
    svfloat32_t v277 = svsub_f32_x(svptrue_b32(), v81, v129);
    svfloat32_t v278 = svadd_f32_x(svptrue_b32(), v97, v113);
    svfloat32_t v279 = svsub_f32_x(svptrue_b32(), v97, v113);
    svfloat32_t v170 = svadd_f32_x(svptrue_b32(), v162, v164);
    svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v163, v165);
    svfloat32_t v176 = svsub_f32_x(svptrue_b32(), v162, v164);
    svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v164, v168);
    svfloat32_t v178 = svsub_f32_x(svptrue_b32(), v168, v162);
    svfloat32_t v179 = svsub_f32_x(svptrue_b32(), v163, v165);
    svfloat32_t v180 = svsub_f32_x(svptrue_b32(), v165, v169);
    svfloat32_t v181 = svsub_f32_x(svptrue_b32(), v169, v163);
    svfloat32_t zero210 = svdup_n_f32(0);
    svfloat32_t v210 = svcmla_f32_x(pred_full, zero210, v704, v167, 90);
    svfloat32_t v280 = svadd_f32_x(svptrue_b32(), v272, v274);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v273, v275);
    svfloat32_t v286 = svsub_f32_x(svptrue_b32(), v272, v274);
    svfloat32_t v287 = svsub_f32_x(svptrue_b32(), v274, v278);
    svfloat32_t v288 = svsub_f32_x(svptrue_b32(), v278, v272);
    svfloat32_t v289 = svsub_f32_x(svptrue_b32(), v273, v275);
    svfloat32_t v290 = svsub_f32_x(svptrue_b32(), v275, v279);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v279, v273);
    svfloat32_t zero320 = svdup_n_f32(0);
    svfloat32_t v320 = svcmla_f32_x(pred_full, zero320, v704, v277, 90);
    svfloat32_t v171 = svadd_f32_x(svptrue_b32(), v170, v168);
    svfloat32_t v175 = svadd_f32_x(svptrue_b32(), v174, v169);
    svfloat32_t zero232 = svdup_n_f32(0);
    svfloat32_t v232 = svcmla_f32_x(pred_full, zero232, v708, v179, 90);
    svfloat32_t zero239 = svdup_n_f32(0);
    svfloat32_t v239 = svcmla_f32_x(pred_full, zero239, v709, v180, 90);
    svfloat32_t zero246 = svdup_n_f32(0);
    svfloat32_t v246 = svcmla_f32_x(pred_full, zero246, v710, v181, 90);
    svfloat32_t v281 = svadd_f32_x(svptrue_b32(), v280, v278);
    svfloat32_t v285 = svadd_f32_x(svptrue_b32(), v284, v279);
    svfloat32_t zero342 = svdup_n_f32(0);
    svfloat32_t v342 = svcmla_f32_x(pred_full, zero342, v708, v289, 90);
    svfloat32_t zero349 = svdup_n_f32(0);
    svfloat32_t v349 = svcmla_f32_x(pred_full, zero349, v709, v290, 90);
    svfloat32_t zero356 = svdup_n_f32(0);
    svfloat32_t v356 = svcmla_f32_x(pred_full, zero356, v710, v291, 90);
    svfloat32_t v172 = svadd_f32_x(svptrue_b32(), v171, v166);
    svfloat32_t v191 = svmul_f32_x(svptrue_b32(), v171, v701);
    svfloat32_t zero198 = svdup_n_f32(0);
    svfloat32_t v198 = svcmla_f32_x(pred_full, zero198, v704, v175, 90);
    svfloat32_t v260 = svadd_f32_x(svptrue_b32(), v210, v232);
    svfloat32_t v262 = svsub_f32_x(svptrue_b32(), v210, v239);
    svfloat32_t v264 = svsub_f32_x(svptrue_b32(), v210, v232);
    svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v281, v276);
    svfloat32_t v301 = svmul_f32_x(svptrue_b32(), v281, v701);
    svfloat32_t zero308 = svdup_n_f32(0);
    svfloat32_t v308 = svcmla_f32_x(pred_full, zero308, v704, v285, 90);
    svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v320, v342);
    svfloat32_t v372 = svsub_f32_x(svptrue_b32(), v320, v349);
    svfloat32_t v374 = svsub_f32_x(svptrue_b32(), v320, v342);
    svfloat32_t v173 = svadd_f32_x(svptrue_b32(), v172, v32);
    svfloat32_t v247 = svadd_f32_x(svptrue_b32(), v191, v191);
    svfloat32_t v261 = svadd_f32_x(svptrue_b32(), v260, v239);
    svfloat32_t v263 = svadd_f32_x(svptrue_b32(), v262, v246);
    svfloat32_t v265 = svsub_f32_x(svptrue_b32(), v264, v246);
    svfloat32_t v283 = svadd_f32_x(svptrue_b32(), v282, v33);
    svfloat32_t v357 = svadd_f32_x(svptrue_b32(), v301, v301);
    svfloat32_t v371 = svadd_f32_x(svptrue_b32(), v370, v349);
    svfloat32_t v373 = svadd_f32_x(svptrue_b32(), v372, v356);
    svfloat32_t v375 = svsub_f32_x(svptrue_b32(), v374, v356);
    svfloat32_t v248 = svmla_f32_x(pred_full, v247, v171, v701);
    svfloat32_t v252 = svmla_f32_x(pred_full, v173, v166, v703);
    svfloat32_t v358 = svmla_f32_x(pred_full, v357, v281, v701);
    svfloat32_t v362 = svmla_f32_x(pred_full, v283, v276, v703);
    svint16_t v384 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v173, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v392 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v283, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v249 = svadd_f32_x(svptrue_b32(), v173, v248);
    svfloat32_t v253 = svadd_f32_x(svptrue_b32(), v252, v247);
    svfloat32_t v359 = svadd_f32_x(svptrue_b32(), v283, v358);
    svfloat32_t v363 = svadd_f32_x(svptrue_b32(), v362, v357);
    svst1w_u64(pred_full, (unsigned *)(v718), svreinterpret_u64_s16(v384));
    svst1w_u64(pred_full, (unsigned *)(v727), svreinterpret_u64_s16(v392));
    svfloat32_t v250 = svadd_f32_x(svptrue_b32(), v249, v198);
    svfloat32_t v251 = svsub_f32_x(svptrue_b32(), v249, v198);
    svfloat32_t v254 = svmla_f32_x(pred_full, v253, v176, v705);
    svfloat32_t v256 = svmls_f32_x(pred_full, v253, v177, v706);
    svfloat32_t v258 = svmls_f32_x(pred_full, v253, v176, v705);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v359, v308);
    svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v359, v308);
    svfloat32_t v364 = svmla_f32_x(pred_full, v363, v286, v705);
    svfloat32_t v366 = svmls_f32_x(pred_full, v363, v287, v706);
    svfloat32_t v368 = svmls_f32_x(pred_full, v363, v286, v705);
    svfloat32_t v255 = svmla_f32_x(pred_full, v254, v177, v706);
    svfloat32_t v257 = svmla_f32_x(pred_full, v256, v178, v707);
    svfloat32_t v259 = svmls_f32_x(pred_full, v258, v178, v707);
    svfloat32_t v365 = svmla_f32_x(pred_full, v364, v287, v706);
    svfloat32_t v367 = svmla_f32_x(pred_full, v366, v288, v707);
    svfloat32_t v369 = svmls_f32_x(pred_full, v368, v288, v707);
    svint16_t v432 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v251, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v440 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v361, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v480 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v250, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v488 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v360, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v266 = svadd_f32_x(svptrue_b32(), v255, v261);
    svfloat32_t v267 = svsub_f32_x(svptrue_b32(), v255, v261);
    svfloat32_t v268 = svadd_f32_x(svptrue_b32(), v257, v263);
    svfloat32_t v269 = svsub_f32_x(svptrue_b32(), v257, v263);
    svfloat32_t v270 = svadd_f32_x(svptrue_b32(), v259, v265);
    svfloat32_t v271 = svsub_f32_x(svptrue_b32(), v259, v265);
    svfloat32_t v376 = svadd_f32_x(svptrue_b32(), v365, v371);
    svfloat32_t v377 = svsub_f32_x(svptrue_b32(), v365, v371);
    svfloat32_t v378 = svadd_f32_x(svptrue_b32(), v367, v373);
    svfloat32_t v379 = svsub_f32_x(svptrue_b32(), v367, v373);
    svfloat32_t v380 = svadd_f32_x(svptrue_b32(), v369, v375);
    svfloat32_t v381 = svsub_f32_x(svptrue_b32(), v369, v375);
    svst1w_u64(pred_full, (unsigned *)(v772), svreinterpret_u64_s16(v432));
    svst1w_u64(pred_full, (unsigned *)(v781), svreinterpret_u64_s16(v440));
    svst1w_u64(pred_full, (unsigned *)(v826), svreinterpret_u64_s16(v480));
    svst1w_u64(pred_full, (unsigned *)(v835), svreinterpret_u64_s16(v488));
    svint16_t v400 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v267, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v408 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v377, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v416 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v268, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v424 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v378, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v448 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v271, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v456 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v381, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v464 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v270, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v472 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v380, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v496 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v269, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v504 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v379, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v512 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v266, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v520 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v376, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v736), svreinterpret_u64_s16(v400));
    svst1w_u64(pred_full, (unsigned *)(v745), svreinterpret_u64_s16(v408));
    svst1w_u64(pred_full, (unsigned *)(v754), svreinterpret_u64_s16(v416));
    svst1w_u64(pred_full, (unsigned *)(v763), svreinterpret_u64_s16(v424));
    svst1w_u64(pred_full, (unsigned *)(v790), svreinterpret_u64_s16(v448));
    svst1w_u64(pred_full, (unsigned *)(v799), svreinterpret_u64_s16(v456));
    svst1w_u64(pred_full, (unsigned *)(v808), svreinterpret_u64_s16(v464));
    svst1w_u64(pred_full, (unsigned *)(v817), svreinterpret_u64_s16(v472));
    svst1w_u64(pred_full, (unsigned *)(v844), svreinterpret_u64_s16(v496));
    svst1w_u64(pred_full, (unsigned *)(v853), svreinterpret_u64_s16(v504));
    svst1w_u64(pred_full, (unsigned *)(v862), svreinterpret_u64_s16(v512));
    svst1w_u64(pred_full, (unsigned *)(v871), svreinterpret_u64_s16(v520));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu19(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v199 = -1.0555555555555556e+00F;
    float v203 = 1.7752228513927079e-01F;
    float v207 = -1.2820077502191529e-01F;
    float v211 = 4.9321510117355499e-02F;
    float v215 = 5.7611011491005903e-01F;
    float v219 = -7.4996449655536279e-01F;
    float v223 = -1.7385438164530381e-01F;
    float v227 = -2.1729997561977314e+00F;
    float v231 = -1.7021211726914738e+00F;
    float v235 = 4.7087858350625778e-01F;
    float v239 = -2.0239400846888440e+00F;
    float v243 = 1.0551641201664090e-01F;
    float v247 = 2.1294564967054850e+00F;
    float v251 = -7.5087543897371167e-01F;
    float v255 = 1.4812817695157160e-01F;
    float v259 = 8.9900361592528333e-01F;
    float v263 = -6.2148246772602778e-01F;
    float v267 = -7.9869352098712687e-01F;
    float v271 = -4.7339199623771833e-01F;
    float v274 = -2.4216105241892630e-01F;
    float v275 = 2.4216105241892630e-01F;
    float v281 = -5.9368607967505101e-02F;
    float v282 = 5.9368607967505101e-02F;
    float v288 = 1.2578688255176201e-02F;
    float v289 = -1.2578688255176201e-02F;
    float v295 = -4.6789919712328903e-02F;
    float v296 = 4.6789919712328903e-02F;
    float v302 = -9.3750121913782358e-01F;
    float v303 = 9.3750121913782358e-01F;
    float v309 = -5.0111537043352902e-02F;
    float v310 = 5.0111537043352902e-02F;
    float v316 = -9.8761275618117661e-01F;
    float v317 = 9.8761275618117661e-01F;
    float v323 = -1.1745786501205959e+00F;
    float v324 = 1.1745786501205959e+00F;
    float v330 = 1.1114482296234993e+00F;
    float v331 = -1.1114482296234993e+00F;
    float v337 = 2.2860268797440955e+00F;
    float v338 = -2.2860268797440955e+00F;
    float v344 = 2.6420523257930939e-01F;
    float v345 = -2.6420523257930939e-01F;
    float v351 = 2.1981792779352136e+00F;
    float v352 = -2.1981792779352136e+00F;
    float v358 = 1.9339740453559042e+00F;
    float v359 = -1.9339740453559042e+00F;
    float v365 = -7.4825847091254893e-01F;
    float v366 = 7.4825847091254893e-01F;
    float v372 = -4.7820835642768872e-01F;
    float v373 = 4.7820835642768872e-01F;
    float v379 = 2.7005011448486022e-01F;
    float v380 = -2.7005011448486022e-01F;
    float v386 = -3.4642356159542270e-01F;
    float v387 = 3.4642356159542270e-01F;
    float v393 = -8.3485429360688279e-01F;
    float v394 = 8.3485429360688279e-01F;
    float v400 = -3.9375928506743518e-01F;
    float v401 = 3.9375928506743518e-01F;
    float32x2_t v403 = (float32x2_t){v4, v4};
    float32x2_t v144 = v5[0];
    float32x2_t v200 = (float32x2_t){v199, v199};
    float32x2_t v204 = (float32x2_t){v203, v203};
    float32x2_t v208 = (float32x2_t){v207, v207};
    float32x2_t v212 = (float32x2_t){v211, v211};
    float32x2_t v216 = (float32x2_t){v215, v215};
    float32x2_t v220 = (float32x2_t){v219, v219};
    float32x2_t v224 = (float32x2_t){v223, v223};
    float32x2_t v228 = (float32x2_t){v227, v227};
    float32x2_t v232 = (float32x2_t){v231, v231};
    float32x2_t v236 = (float32x2_t){v235, v235};
    float32x2_t v240 = (float32x2_t){v239, v239};
    float32x2_t v244 = (float32x2_t){v243, v243};
    float32x2_t v248 = (float32x2_t){v247, v247};
    float32x2_t v252 = (float32x2_t){v251, v251};
    float32x2_t v256 = (float32x2_t){v255, v255};
    float32x2_t v260 = (float32x2_t){v259, v259};
    float32x2_t v264 = (float32x2_t){v263, v263};
    float32x2_t v268 = (float32x2_t){v267, v267};
    float32x2_t v272 = (float32x2_t){v271, v271};
    float32x2_t v276 = (float32x2_t){v274, v275};
    float32x2_t v283 = (float32x2_t){v281, v282};
    float32x2_t v290 = (float32x2_t){v288, v289};
    float32x2_t v297 = (float32x2_t){v295, v296};
    float32x2_t v304 = (float32x2_t){v302, v303};
    float32x2_t v311 = (float32x2_t){v309, v310};
    float32x2_t v318 = (float32x2_t){v316, v317};
    float32x2_t v325 = (float32x2_t){v323, v324};
    float32x2_t v332 = (float32x2_t){v330, v331};
    float32x2_t v339 = (float32x2_t){v337, v338};
    float32x2_t v346 = (float32x2_t){v344, v345};
    float32x2_t v353 = (float32x2_t){v351, v352};
    float32x2_t v360 = (float32x2_t){v358, v359};
    float32x2_t v367 = (float32x2_t){v365, v366};
    float32x2_t v374 = (float32x2_t){v372, v373};
    float32x2_t v381 = (float32x2_t){v379, v380};
    float32x2_t v388 = (float32x2_t){v386, v387};
    float32x2_t v395 = (float32x2_t){v393, v394};
    float32x2_t v402 = (float32x2_t){v400, v401};
    float32x2_t v25 = v5[istride * 18];
    float32x2_t v32 = v5[istride * 2];
    float32x2_t v37 = v5[istride * 17];
    float32x2_t v44 = v5[istride * 4];
    float32x2_t v49 = v5[istride * 15];
    float32x2_t v56 = v5[istride * 8];
    float32x2_t v61 = v5[istride * 11];
    float32x2_t v68 = v5[istride * 16];
    float32x2_t v73 = v5[istride * 3];
    float32x2_t v80 = v5[istride * 13];
    float32x2_t v85 = v5[istride * 6];
    float32x2_t v92 = v5[istride * 7];
    float32x2_t v97 = v5[istride * 12];
    float32x2_t v104 = v5[istride * 14];
    float32x2_t v109 = v5[istride * 5];
    float32x2_t v116 = v5[istride * 9];
    float32x2_t v121 = v5[istride * 10];
    float32x2_t v278 = vmul_f32(v403, v276);
    float32x2_t v285 = vmul_f32(v403, v283);
    float32x2_t v292 = vmul_f32(v403, v290);
    float32x2_t v299 = vmul_f32(v403, v297);
    float32x2_t v306 = vmul_f32(v403, v304);
    float32x2_t v313 = vmul_f32(v403, v311);
    float32x2_t v320 = vmul_f32(v403, v318);
    float32x2_t v327 = vmul_f32(v403, v325);
    float32x2_t v334 = vmul_f32(v403, v332);
    float32x2_t v341 = vmul_f32(v403, v339);
    float32x2_t v348 = vmul_f32(v403, v346);
    float32x2_t v355 = vmul_f32(v403, v353);
    float32x2_t v362 = vmul_f32(v403, v360);
    float32x2_t v369 = vmul_f32(v403, v367);
    float32x2_t v376 = vmul_f32(v403, v374);
    float32x2_t v383 = vmul_f32(v403, v381);
    float32x2_t v390 = vmul_f32(v403, v388);
    float32x2_t v397 = vmul_f32(v403, v395);
    float32x2_t v404 = vmul_f32(v403, v402);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v37, v32);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v61, v56);
    float32x2_t v74 = vadd_f32(v68, v73);
    float32x2_t v75 = vsub_f32(v68, v73);
    float32x2_t v86 = vadd_f32(v80, v85);
    float32x2_t v87 = vsub_f32(v85, v80);
    float32x2_t v98 = vadd_f32(v92, v97);
    float32x2_t v99 = vsub_f32(v92, v97);
    float32x2_t v110 = vadd_f32(v104, v109);
    float32x2_t v111 = vsub_f32(v109, v104);
    float32x2_t v122 = vadd_f32(v116, v121);
    float32x2_t v123 = vsub_f32(v116, v121);
    float32x2_t v124 = vsub_f32(v26, v98);
    float32x2_t v125 = vsub_f32(v38, v110);
    float32x2_t v126 = vsub_f32(v50, v122);
    float32x2_t v127 = vsub_f32(v62, v98);
    float32x2_t v128 = vsub_f32(v74, v110);
    float32x2_t v129 = vsub_f32(v86, v122);
    float32x2_t v130 = vadd_f32(v26, v62);
    float32x2_t v132 = vadd_f32(v38, v74);
    float32x2_t v134 = vadd_f32(v50, v86);
    float32x2_t v162 = vsub_f32(v27, v99);
    float32x2_t v163 = vsub_f32(v39, v111);
    float32x2_t v164 = vsub_f32(v51, v123);
    float32x2_t v165 = vsub_f32(v63, v99);
    float32x2_t v166 = vsub_f32(v75, v111);
    float32x2_t v167 = vsub_f32(v87, v123);
    float32x2_t v168 = vadd_f32(v27, v63);
    float32x2_t v170 = vadd_f32(v39, v75);
    float32x2_t v172 = vadd_f32(v51, v87);
    float32x2_t v131 = vadd_f32(v130, v98);
    float32x2_t v133 = vadd_f32(v132, v110);
    float32x2_t v135 = vadd_f32(v134, v122);
    float32x2_t v136 = vadd_f32(v124, v126);
    float32x2_t v137 = vadd_f32(v127, v129);
    float32x2_t v152 = vsub_f32(v124, v127);
    float32x2_t v153 = vsub_f32(v126, v129);
    float32x2_t v169 = vadd_f32(v168, v99);
    float32x2_t v171 = vadd_f32(v170, v111);
    float32x2_t v173 = vadd_f32(v172, v123);
    float32x2_t v174 = vadd_f32(v162, v164);
    float32x2_t v175 = vadd_f32(v165, v167);
    float32x2_t v184 = vsub_f32(v162, v165);
    float32x2_t v185 = vsub_f32(v164, v167);
    float32x2_t v229 = vmul_f32(v127, v228);
    float32x2_t v241 = vmul_f32(v129, v240);
    float32x2_t v249 = vmul_f32(v126, v248);
    float32x2_t v328 = vrev64_f32(v165);
    float32x2_t v342 = vrev64_f32(v162);
    float32x2_t v349 = vrev64_f32(v167);
    float32x2_t v363 = vrev64_f32(v164);
    float32x2_t v138 = vadd_f32(v131, v133);
    float32x2_t v146 = vadd_f32(v137, v128);
    float32x2_t v147 = vadd_f32(v136, v125);
    float32x2_t v149 = vsub_f32(v137, v128);
    float32x2_t v150 = vsub_f32(v136, v125);
    float32x2_t v154 = vsub_f32(v124, v153);
    float32x2_t v156 = vadd_f32(v152, v129);
    float32x2_t v159 = vsub_f32(v131, v135);
    float32x2_t v160 = vsub_f32(v133, v135);
    float32x2_t v176 = vadd_f32(v169, v171);
    float32x2_t v178 = vadd_f32(v175, v166);
    float32x2_t v179 = vadd_f32(v174, v163);
    float32x2_t v181 = vsub_f32(v175, v166);
    float32x2_t v182 = vsub_f32(v174, v163);
    float32x2_t v186 = vsub_f32(v162, v185);
    float32x2_t v188 = vadd_f32(v184, v167);
    float32x2_t v191 = vsub_f32(v169, v173);
    float32x2_t v192 = vsub_f32(v171, v173);
    float32x2_t v233 = vmul_f32(v152, v232);
    float32x2_t v245 = vmul_f32(v153, v244);
    float32x2_t v329 = vmul_f32(v328, v327);
    float32x2_t v335 = vrev64_f32(v184);
    float32x2_t v350 = vmul_f32(v349, v348);
    float32x2_t v356 = vrev64_f32(v185);
    float32x2_t v364 = vmul_f32(v363, v362);
    float32x2_t v139 = vadd_f32(v138, v135);
    float32x2_t v148 = vsub_f32(v147, v146);
    float32x2_t v151 = vsub_f32(v150, v149);
    float32x2_t v155 = vsub_f32(v154, v128);
    float32x2_t v157 = vsub_f32(v156, v125);
    float32x2_t v161 = vadd_f32(v159, v160);
    float32x2_t v177 = vadd_f32(v176, v173);
    float32x2_t v180 = vsub_f32(v179, v178);
    float32x2_t v183 = vsub_f32(v182, v181);
    float32x2_t v187 = vsub_f32(v186, v166);
    float32x2_t v189 = vsub_f32(v188, v163);
    float32x2_t v193 = vadd_f32(v191, v192);
    float32x2_t v205 = vmul_f32(v146, v204);
    float32x2_t v209 = vmul_f32(v147, v208);
    float32x2_t v217 = vmul_f32(v149, v216);
    float32x2_t v221 = vmul_f32(v150, v220);
    float32x2_t v265 = vmul_f32(v159, v264);
    float32x2_t v269 = vmul_f32(v160, v268);
    float32x2_t v286 = vrev64_f32(v178);
    float32x2_t v293 = vrev64_f32(v179);
    float32x2_t v307 = vrev64_f32(v181);
    float32x2_t v314 = vrev64_f32(v182);
    float32x2_t v336 = vmul_f32(v335, v334);
    float32x2_t v357 = vmul_f32(v356, v355);
    float32x2_t v391 = vrev64_f32(v191);
    float32x2_t v398 = vrev64_f32(v192);
    float32x2_t v145 = vadd_f32(v144, v139);
    float32x2_t v158 = vsub_f32(v155, v157);
    float32x2_t v190 = vsub_f32(v187, v189);
    float32x2_t v201 = vmul_f32(v139, v200);
    float32x2_t v213 = vmul_f32(v148, v212);
    float32x2_t v225 = vmul_f32(v151, v224);
    float32x2_t v253 = vmul_f32(v155, v252);
    float32x2_t v257 = vmul_f32(v157, v256);
    float32x2_t v273 = vmul_f32(v161, v272);
    float32x2_t v279 = vrev64_f32(v177);
    float32x2_t v287 = vmul_f32(v286, v285);
    float32x2_t v294 = vmul_f32(v293, v292);
    float32x2_t v300 = vrev64_f32(v180);
    float32x2_t v308 = vmul_f32(v307, v306);
    float32x2_t v315 = vmul_f32(v314, v313);
    float32x2_t v321 = vrev64_f32(v183);
    float32x2_t v370 = vrev64_f32(v187);
    float32x2_t v377 = vrev64_f32(v189);
    float32x2_t v392 = vmul_f32(v391, v390);
    float32x2_t v399 = vmul_f32(v398, v397);
    float32x2_t v405 = vrev64_f32(v193);
    float32x2_t v407 = vadd_f32(v205, v209);
    float32x2_t v408 = vadd_f32(v217, v221);
    float32x2_t v261 = vmul_f32(v158, v260);
    float32x2_t v280 = vmul_f32(v279, v278);
    float32x2_t v301 = vmul_f32(v300, v299);
    float32x2_t v322 = vmul_f32(v321, v320);
    float32x2_t v371 = vmul_f32(v370, v369);
    float32x2_t v378 = vmul_f32(v377, v376);
    float32x2_t v384 = vrev64_f32(v190);
    float32x2_t v406 = vmul_f32(v405, v404);
    float32x2_t v410 = vadd_f32(v407, v408);
    float32x2_t v411 = vadd_f32(v205, v213);
    float32x2_t v412 = vadd_f32(v217, v225);
    float32x2_t v429 = vsub_f32(v407, v408);
    float32x2_t v431 = vsub_f32(v265, v273);
    float32x2_t v432 = vsub_f32(v269, v273);
    float32x2_t v433 = vadd_f32(v201, v145);
    float32x2_t v438 = vadd_f32(v287, v294);
    float32x2_t v439 = vadd_f32(v308, v315);
    int16x4_t v494 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v145, 15), (int32x2_t){0, 0}));
    float32x2_t v385 = vmul_f32(v384, v383);
    float32x2_t v409 = vadd_f32(v257, v261);
    float32x2_t v413 = vadd_f32(v253, v261);
    float32x2_t v414 = vsub_f32(v229, v410);
    float32x2_t v415 = vadd_f32(v411, v412);
    float32x2_t v421 = vsub_f32(v411, v412);
    float32x2_t v426 = vadd_f32(v410, v249);
    float32x2_t v434 = vadd_f32(v433, v431);
    float32x2_t v435 = vsub_f32(v433, v431);
    float32x2_t v437 = vadd_f32(v433, v432);
    float32x2_t v441 = vadd_f32(v438, v439);
    float32x2_t v442 = vadd_f32(v287, v301);
    float32x2_t v443 = vadd_f32(v308, v322);
    float32x2_t v460 = vsub_f32(v438, v439);
    float32x2_t v462 = vsub_f32(v392, v406);
    float32x2_t v463 = vsub_f32(v399, v406);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v494), 0);
    float32x2_t v416 = vsub_f32(v241, v413);
    float32x2_t v417 = vadd_f32(v233, v409);
    float32x2_t v419 = vadd_f32(v415, v245);
    float32x2_t v422 = vadd_f32(v421, v409);
    float32x2_t v423 = vadd_f32(v414, v415);
    float32x2_t v430 = vadd_f32(v429, v413);
    float32x2_t v436 = vsub_f32(v435, v432);
    float32x2_t v440 = vadd_f32(v378, v385);
    float32x2_t v444 = vadd_f32(v371, v385);
    float32x2_t v445 = vsub_f32(v329, v441);
    float32x2_t v446 = vadd_f32(v442, v443);
    float32x2_t v452 = vsub_f32(v442, v443);
    float32x2_t v457 = vadd_f32(v441, v364);
    float32x2_t v464 = vadd_f32(v280, v462);
    float32x2_t v465 = vsub_f32(v280, v462);
    float32x2_t v467 = vadd_f32(v280, v463);
    float32x2_t v418 = vadd_f32(v417, v414);
    float32x2_t v420 = vadd_f32(v419, v416);
    float32x2_t v424 = vfma_f32(v423, v124, v236);
    float32x2_t v427 = vadd_f32(v426, v416);
    float32x2_t v447 = vsub_f32(v350, v444);
    float32x2_t v448 = vadd_f32(v336, v440);
    float32x2_t v450 = vadd_f32(v446, v357);
    float32x2_t v453 = vadd_f32(v452, v440);
    float32x2_t v454 = vadd_f32(v445, v446);
    float32x2_t v461 = vadd_f32(v460, v444);
    float32x2_t v466 = vsub_f32(v465, v463);
    float32x2_t v472 = vsub_f32(v430, v422);
    float32x2_t v476 = vsub_f32(v437, v430);
    float32x2_t v479 = vadd_f32(v422, v437);
    float32x2_t v425 = vadd_f32(v424, v413);
    float32x2_t v428 = vadd_f32(v427, v409);
    float32x2_t v449 = vadd_f32(v448, v445);
    float32x2_t v451 = vadd_f32(v450, v447);
    float32x2_t v455 = vfma_f32(v454, v342, v341);
    float32x2_t v458 = vadd_f32(v457, v447);
    float32x2_t v473 = vadd_f32(v472, v437);
    float32x2_t v477 = vadd_f32(v418, v434);
    float32x2_t v478 = vadd_f32(v420, v436);
    float32x2_t v484 = vsub_f32(v461, v453);
    float32x2_t v488 = vsub_f32(v461, v467);
    float32x2_t v491 = vadd_f32(v453, v467);
    float32x2_t v456 = vadd_f32(v455, v444);
    float32x2_t v459 = vadd_f32(v458, v440);
    float32x2_t v468 = vsub_f32(v425, v418);
    float32x2_t v470 = vsub_f32(v428, v420);
    float32x2_t v474 = vsub_f32(v434, v425);
    float32x2_t v475 = vsub_f32(v436, v428);
    float32x2_t v485 = vadd_f32(v484, v467);
    float32x2_t v489 = vadd_f32(v449, v464);
    float32x2_t v490 = vadd_f32(v451, v466);
    float32x2_t v512 = vsub_f32(v479, v491);
    float32x2_t v519 = vadd_f32(v479, v491);
    float32x2_t v526 = vadd_f32(v476, v488);
    float32x2_t v533 = vsub_f32(v476, v488);
    float32x2_t v469 = vadd_f32(v468, v434);
    float32x2_t v471 = vadd_f32(v470, v436);
    float32x2_t v480 = vsub_f32(v456, v449);
    float32x2_t v482 = vsub_f32(v459, v451);
    float32x2_t v486 = vsub_f32(v464, v456);
    float32x2_t v487 = vsub_f32(v466, v459);
    int16x4_t v515 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v512, 15), (int32x2_t){0, 0}));
    int16x4_t v522 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v519, 15), (int32x2_t){0, 0}));
    int16x4_t v529 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v526, 15), (int32x2_t){0, 0}));
    int16x4_t v536 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v533, 15), (int32x2_t){0, 0}));
    float32x2_t v540 = vadd_f32(v478, v490);
    float32x2_t v547 = vsub_f32(v478, v490);
    float32x2_t v554 = vadd_f32(v473, v485);
    float32x2_t v561 = vsub_f32(v473, v485);
    float32x2_t v596 = vsub_f32(v477, v489);
    float32x2_t v603 = vadd_f32(v477, v489);
    float32x2_t v481 = vadd_f32(v480, v464);
    float32x2_t v483 = vadd_f32(v482, v466);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v515), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v522), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v529), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v536), 0);
    int16x4_t v543 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v540, 15), (int32x2_t){0, 0}));
    int16x4_t v550 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v547, 15), (int32x2_t){0, 0}));
    int16x4_t v557 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v554, 15), (int32x2_t){0, 0}));
    int16x4_t v564 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v561, 15), (int32x2_t){0, 0}));
    float32x2_t v568 = vadd_f32(v475, v487);
    float32x2_t v575 = vsub_f32(v475, v487);
    float32x2_t v582 = vadd_f32(v474, v486);
    float32x2_t v589 = vsub_f32(v474, v486);
    int16x4_t v599 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v596, 15), (int32x2_t){0, 0}));
    int16x4_t v606 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v603, 15), (int32x2_t){0, 0}));
    float32x2_t v498 = vadd_f32(v469, v481);
    float32x2_t v505 = vsub_f32(v469, v481);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v543), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v550), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v557), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v564), 0);
    int16x4_t v571 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v568, 15), (int32x2_t){0, 0}));
    int16x4_t v578 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v575, 15), (int32x2_t){0, 0}));
    int16x4_t v585 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v582, 15), (int32x2_t){0, 0}));
    int16x4_t v592 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v589, 15), (int32x2_t){0, 0}));
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v599), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v606), 0);
    float32x2_t v610 = vadd_f32(v471, v483);
    float32x2_t v617 = vsub_f32(v471, v483);
    int16x4_t v501 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v498, 15), (int32x2_t){0, 0}));
    int16x4_t v508 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v505, 15), (int32x2_t){0, 0}));
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v571), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v578), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v585), 0);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v592), 0);
    int16x4_t v613 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v610, 15), (int32x2_t){0, 0}));
    int16x4_t v620 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v617, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v501), 0);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v508), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v613), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v620), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu19(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v240 = -1.0555555555555556e+00F;
    float v245 = 1.7752228513927079e-01F;
    float v250 = -1.2820077502191529e-01F;
    float v255 = 4.9321510117355499e-02F;
    float v260 = 5.7611011491005903e-01F;
    float v265 = -7.4996449655536279e-01F;
    float v270 = -1.7385438164530381e-01F;
    float v275 = -2.1729997561977314e+00F;
    float v280 = -1.7021211726914738e+00F;
    float v285 = 4.7087858350625778e-01F;
    float v290 = -2.0239400846888440e+00F;
    float v295 = 1.0551641201664090e-01F;
    float v300 = 2.1294564967054850e+00F;
    float v305 = -7.5087543897371167e-01F;
    float v310 = 1.4812817695157160e-01F;
    float v315 = 8.9900361592528333e-01F;
    float v320 = -6.2148246772602778e-01F;
    float v325 = -7.9869352098712687e-01F;
    float v330 = -4.7339199623771833e-01F;
    float v335 = 2.4216105241892630e-01F;
    float v342 = 5.9368607967505101e-02F;
    float v349 = -1.2578688255176201e-02F;
    float v356 = 4.6789919712328903e-02F;
    float v363 = 9.3750121913782358e-01F;
    float v370 = 5.0111537043352902e-02F;
    float v377 = 9.8761275618117661e-01F;
    float v384 = 1.1745786501205959e+00F;
    float v391 = -1.1114482296234993e+00F;
    float v398 = -2.2860268797440955e+00F;
    float v405 = -2.6420523257930939e-01F;
    float v412 = -2.1981792779352136e+00F;
    float v419 = -1.9339740453559042e+00F;
    float v426 = 7.4825847091254893e-01F;
    float v433 = 4.7820835642768872e-01F;
    float v440 = -2.7005011448486022e-01F;
    float v447 = 3.4642356159542270e-01F;
    float v454 = 8.3485429360688279e-01F;
    float v461 = 3.9375928506743518e-01F;
    const float32x2_t *v728 = &v5[v0];
    int32_t *v949 = &v6[v2];
    int64_t v26 = v0 * 18;
    int64_t v35 = v0 * 2;
    int64_t v42 = v0 * 17;
    int64_t v51 = v0 * 4;
    int64_t v58 = v0 * 15;
    int64_t v67 = v0 * 8;
    int64_t v74 = v0 * 11;
    int64_t v83 = v0 * 16;
    int64_t v90 = v0 * 3;
    int64_t v99 = v0 * 13;
    int64_t v106 = v0 * 6;
    int64_t v115 = v0 * 7;
    int64_t v122 = v0 * 12;
    int64_t v131 = v0 * 14;
    int64_t v138 = v0 * 5;
    int64_t v147 = v0 * 9;
    int64_t v154 = v0 * 10;
    float v338 = v4 * v335;
    float v345 = v4 * v342;
    float v352 = v4 * v349;
    float v359 = v4 * v356;
    float v366 = v4 * v363;
    float v373 = v4 * v370;
    float v380 = v4 * v377;
    float v387 = v4 * v384;
    float v394 = v4 * v391;
    float v401 = v4 * v398;
    float v408 = v4 * v405;
    float v415 = v4 * v412;
    float v422 = v4 * v419;
    float v429 = v4 * v426;
    float v436 = v4 * v433;
    float v443 = v4 * v440;
    float v450 = v4 * v447;
    float v457 = v4 * v454;
    float v464 = v4 * v461;
    int64_t v571 = v2 * 18;
    int64_t v580 = v2 * 2;
    int64_t v589 = v2 * 17;
    int64_t v598 = v2 * 3;
    int64_t v607 = v2 * 16;
    int64_t v616 = v2 * 4;
    int64_t v625 = v2 * 15;
    int64_t v634 = v2 * 5;
    int64_t v643 = v2 * 14;
    int64_t v652 = v2 * 6;
    int64_t v661 = v2 * 13;
    int64_t v670 = v2 * 7;
    int64_t v679 = v2 * 12;
    int64_t v688 = v2 * 8;
    int64_t v697 = v2 * 11;
    int64_t v706 = v2 * 9;
    int64_t v715 = v2 * 10;
    const float32x2_t *v891 = &v5[0];
    svfloat32_t v895 = svdup_n_f32(v240);
    svfloat32_t v896 = svdup_n_f32(v245);
    svfloat32_t v897 = svdup_n_f32(v250);
    svfloat32_t v898 = svdup_n_f32(v255);
    svfloat32_t v899 = svdup_n_f32(v260);
    svfloat32_t v900 = svdup_n_f32(v265);
    svfloat32_t v901 = svdup_n_f32(v270);
    svfloat32_t v902 = svdup_n_f32(v275);
    svfloat32_t v903 = svdup_n_f32(v280);
    svfloat32_t v904 = svdup_n_f32(v285);
    svfloat32_t v905 = svdup_n_f32(v290);
    svfloat32_t v906 = svdup_n_f32(v295);
    svfloat32_t v907 = svdup_n_f32(v300);
    svfloat32_t v908 = svdup_n_f32(v305);
    svfloat32_t v909 = svdup_n_f32(v310);
    svfloat32_t v910 = svdup_n_f32(v315);
    svfloat32_t v911 = svdup_n_f32(v320);
    svfloat32_t v912 = svdup_n_f32(v325);
    svfloat32_t v913 = svdup_n_f32(v330);
    int32_t *v940 = &v6[0];
    svfloat32_t v1106 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v728)[0]));
    const float32x2_t *v737 = &v5[v26];
    const float32x2_t *v746 = &v5[v35];
    const float32x2_t *v755 = &v5[v42];
    const float32x2_t *v764 = &v5[v51];
    const float32x2_t *v773 = &v5[v58];
    const float32x2_t *v782 = &v5[v67];
    const float32x2_t *v791 = &v5[v74];
    const float32x2_t *v800 = &v5[v83];
    const float32x2_t *v809 = &v5[v90];
    const float32x2_t *v818 = &v5[v99];
    const float32x2_t *v827 = &v5[v106];
    const float32x2_t *v836 = &v5[v115];
    const float32x2_t *v845 = &v5[v122];
    const float32x2_t *v854 = &v5[v131];
    const float32x2_t *v863 = &v5[v138];
    const float32x2_t *v872 = &v5[v147];
    const float32x2_t *v881 = &v5[v154];
    svfloat32_t v914 = svdup_n_f32(v338);
    svfloat32_t v915 = svdup_n_f32(v345);
    svfloat32_t v916 = svdup_n_f32(v352);
    svfloat32_t v917 = svdup_n_f32(v359);
    svfloat32_t v918 = svdup_n_f32(v366);
    svfloat32_t v919 = svdup_n_f32(v373);
    svfloat32_t v920 = svdup_n_f32(v380);
    svfloat32_t v921 = svdup_n_f32(v387);
    svfloat32_t v922 = svdup_n_f32(v394);
    svfloat32_t v923 = svdup_n_f32(v401);
    svfloat32_t v924 = svdup_n_f32(v408);
    svfloat32_t v925 = svdup_n_f32(v415);
    svfloat32_t v926 = svdup_n_f32(v422);
    svfloat32_t v927 = svdup_n_f32(v429);
    svfloat32_t v928 = svdup_n_f32(v436);
    svfloat32_t v929 = svdup_n_f32(v443);
    svfloat32_t v930 = svdup_n_f32(v450);
    svfloat32_t v931 = svdup_n_f32(v457);
    svfloat32_t v932 = svdup_n_f32(v464);
    int32_t *v958 = &v6[v571];
    int32_t *v967 = &v6[v580];
    int32_t *v976 = &v6[v589];
    int32_t *v985 = &v6[v598];
    int32_t *v994 = &v6[v607];
    int32_t *v1003 = &v6[v616];
    int32_t *v1012 = &v6[v625];
    int32_t *v1021 = &v6[v634];
    int32_t *v1030 = &v6[v643];
    int32_t *v1039 = &v6[v652];
    int32_t *v1048 = &v6[v661];
    int32_t *v1057 = &v6[v670];
    int32_t *v1066 = &v6[v679];
    int32_t *v1075 = &v6[v688];
    int32_t *v1084 = &v6[v697];
    int32_t *v1093 = &v6[v706];
    int32_t *v1102 = &v6[v715];
    svfloat32_t v1142 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v891)[0]));
    svfloat32_t v1108 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v737)[0]));
    svfloat32_t v1110 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v746)[0]));
    svfloat32_t v1112 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v755)[0]));
    svfloat32_t v1114 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v764)[0]));
    svfloat32_t v1116 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v773)[0]));
    svfloat32_t v1118 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v782)[0]));
    svfloat32_t v1120 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v791)[0]));
    svfloat32_t v1122 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v800)[0]));
    svfloat32_t v1124 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v809)[0]));
    svfloat32_t v1126 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v818)[0]));
    svfloat32_t v1128 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v827)[0]));
    svfloat32_t v1130 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v836)[0]));
    svfloat32_t v1132 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v845)[0]));
    svfloat32_t v1134 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v854)[0]));
    svfloat32_t v1136 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v863)[0]));
    svfloat32_t v1138 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v872)[0]));
    svfloat32_t v1140 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v881)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v1106, v1108);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v1106, v1108);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v1110, v1112);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v1112, v1110);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v1114, v1116);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v1114, v1116);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v1118, v1120);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v1120, v1118);
    svfloat32_t v96 = svadd_f32_x(svptrue_b32(), v1122, v1124);
    svfloat32_t v97 = svsub_f32_x(svptrue_b32(), v1122, v1124);
    svfloat32_t v112 = svadd_f32_x(svptrue_b32(), v1126, v1128);
    svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v1128, v1126);
    svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v1130, v1132);
    svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v1130, v1132);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v1134, v1136);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v1136, v1134);
    svfloat32_t v160 = svadd_f32_x(svptrue_b32(), v1138, v1140);
    svfloat32_t v161 = svsub_f32_x(svptrue_b32(), v1138, v1140);
    svfloat32_t v162 = svsub_f32_x(svptrue_b32(), v32, v128);
    svfloat32_t v163 = svsub_f32_x(svptrue_b32(), v48, v144);
    svfloat32_t v164 = svsub_f32_x(svptrue_b32(), v64, v160);
    svfloat32_t v165 = svsub_f32_x(svptrue_b32(), v80, v128);
    svfloat32_t v166 = svsub_f32_x(svptrue_b32(), v96, v144);
    svfloat32_t v167 = svsub_f32_x(svptrue_b32(), v112, v160);
    svfloat32_t v168 = svadd_f32_x(svptrue_b32(), v32, v80);
    svfloat32_t v170 = svadd_f32_x(svptrue_b32(), v48, v96);
    svfloat32_t v172 = svadd_f32_x(svptrue_b32(), v64, v112);
    svfloat32_t v202 = svsub_f32_x(svptrue_b32(), v33, v129);
    svfloat32_t v203 = svsub_f32_x(svptrue_b32(), v49, v145);
    svfloat32_t v204 = svsub_f32_x(svptrue_b32(), v65, v161);
    svfloat32_t v205 = svsub_f32_x(svptrue_b32(), v81, v129);
    svfloat32_t v206 = svsub_f32_x(svptrue_b32(), v97, v145);
    svfloat32_t v207 = svsub_f32_x(svptrue_b32(), v113, v161);
    svfloat32_t v208 = svadd_f32_x(svptrue_b32(), v33, v81);
    svfloat32_t v210 = svadd_f32_x(svptrue_b32(), v49, v97);
    svfloat32_t v212 = svadd_f32_x(svptrue_b32(), v65, v113);
    svfloat32_t v169 = svadd_f32_x(svptrue_b32(), v168, v128);
    svfloat32_t v171 = svadd_f32_x(svptrue_b32(), v170, v144);
    svfloat32_t v173 = svadd_f32_x(svptrue_b32(), v172, v160);
    svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v162, v164);
    svfloat32_t v175 = svadd_f32_x(svptrue_b32(), v165, v167);
    svfloat32_t v192 = svsub_f32_x(svptrue_b32(), v162, v165);
    svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v164, v167);
    svfloat32_t v209 = svadd_f32_x(svptrue_b32(), v208, v129);
    svfloat32_t v211 = svadd_f32_x(svptrue_b32(), v210, v145);
    svfloat32_t v213 = svadd_f32_x(svptrue_b32(), v212, v161);
    svfloat32_t v214 = svadd_f32_x(svptrue_b32(), v202, v204);
    svfloat32_t v215 = svadd_f32_x(svptrue_b32(), v205, v207);
    svfloat32_t v224 = svsub_f32_x(svptrue_b32(), v202, v205);
    svfloat32_t v225 = svsub_f32_x(svptrue_b32(), v204, v207);
    svfloat32_t zero389 = svdup_n_f32(0);
    svfloat32_t v389 = svcmla_f32_x(pred_full, zero389, v921, v205, 90);
    svfloat32_t zero410 = svdup_n_f32(0);
    svfloat32_t v410 = svcmla_f32_x(pred_full, zero410, v924, v207, 90);
    svfloat32_t v176 = svadd_f32_x(svptrue_b32(), v169, v171);
    svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v175, v166);
    svfloat32_t v187 = svadd_f32_x(svptrue_b32(), v174, v163);
    svfloat32_t v189 = svsub_f32_x(svptrue_b32(), v175, v166);
    svfloat32_t v190 = svsub_f32_x(svptrue_b32(), v174, v163);
    svfloat32_t v194 = svsub_f32_x(svptrue_b32(), v162, v193);
    svfloat32_t v196 = svadd_f32_x(svptrue_b32(), v192, v167);
    svfloat32_t v199 = svsub_f32_x(svptrue_b32(), v169, v173);
    svfloat32_t v200 = svsub_f32_x(svptrue_b32(), v171, v173);
    svfloat32_t v216 = svadd_f32_x(svptrue_b32(), v209, v211);
    svfloat32_t v218 = svadd_f32_x(svptrue_b32(), v215, v206);
    svfloat32_t v219 = svadd_f32_x(svptrue_b32(), v214, v203);
    svfloat32_t v221 = svsub_f32_x(svptrue_b32(), v215, v206);
    svfloat32_t v222 = svsub_f32_x(svptrue_b32(), v214, v203);
    svfloat32_t v226 = svsub_f32_x(svptrue_b32(), v202, v225);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v224, v207);
    svfloat32_t v231 = svsub_f32_x(svptrue_b32(), v209, v213);
    svfloat32_t v232 = svsub_f32_x(svptrue_b32(), v211, v213);
    svfloat32_t v177 = svadd_f32_x(svptrue_b32(), v176, v173);
    svfloat32_t v188 = svsub_f32_x(svptrue_b32(), v187, v186);
    svfloat32_t v191 = svsub_f32_x(svptrue_b32(), v190, v189);
    svfloat32_t v195 = svsub_f32_x(svptrue_b32(), v194, v166);
    svfloat32_t v197 = svsub_f32_x(svptrue_b32(), v196, v163);
    svfloat32_t v201 = svadd_f32_x(svptrue_b32(), v199, v200);
    svfloat32_t v217 = svadd_f32_x(svptrue_b32(), v216, v213);
    svfloat32_t v220 = svsub_f32_x(svptrue_b32(), v219, v218);
    svfloat32_t v223 = svsub_f32_x(svptrue_b32(), v222, v221);
    svfloat32_t v227 = svsub_f32_x(svptrue_b32(), v226, v206);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v228, v203);
    svfloat32_t v233 = svadd_f32_x(svptrue_b32(), v231, v232);
    svfloat32_t v253 = svmul_f32_x(svptrue_b32(), v187, v897);
    svfloat32_t v268 = svmul_f32_x(svptrue_b32(), v190, v900);
    svfloat32_t zero347 = svdup_n_f32(0);
    svfloat32_t v347 = svcmla_f32_x(pred_full, zero347, v915, v218, 90);
    svfloat32_t zero368 = svdup_n_f32(0);
    svfloat32_t v368 = svcmla_f32_x(pred_full, zero368, v918, v221, 90);
    svfloat32_t zero452 = svdup_n_f32(0);
    svfloat32_t v452 = svcmla_f32_x(pred_full, zero452, v930, v231, 90);
    svfloat32_t zero459 = svdup_n_f32(0);
    svfloat32_t v459 = svcmla_f32_x(pred_full, zero459, v931, v232, 90);
    svfloat32_t v185 = svadd_f32_x(svptrue_b32(), v1142, v177);
    svfloat32_t v198 = svsub_f32_x(svptrue_b32(), v195, v197);
    svfloat32_t v230 = svsub_f32_x(svptrue_b32(), v227, v229);
    svfloat32_t v258 = svmul_f32_x(svptrue_b32(), v188, v898);
    svfloat32_t v273 = svmul_f32_x(svptrue_b32(), v191, v901);
    svfloat32_t v333 = svmul_f32_x(svptrue_b32(), v201, v913);
    svfloat32_t zero340 = svdup_n_f32(0);
    svfloat32_t v340 = svcmla_f32_x(pred_full, zero340, v914, v217, 90);
    svfloat32_t zero466 = svdup_n_f32(0);
    svfloat32_t v466 = svcmla_f32_x(pred_full, zero466, v932, v233, 90);
    svfloat32_t v467 = svmla_f32_x(pred_full, v253, v186, v896);
    svfloat32_t v468 = svmla_f32_x(pred_full, v268, v189, v899);
    svfloat32_t v498 = svcmla_f32_x(pred_full, v347, v916, v219, 90);
    svfloat32_t v499 = svcmla_f32_x(pred_full, v368, v919, v222, 90);
    svfloat32_t v318 = svmul_f32_x(svptrue_b32(), v198, v910);
    svfloat32_t zero445 = svdup_n_f32(0);
    svfloat32_t v445 = svcmla_f32_x(pred_full, zero445, v929, v230, 90);
    svfloat32_t v470 = svadd_f32_x(svptrue_b32(), v467, v468);
    svfloat32_t v471 = svmla_f32_x(pred_full, v258, v186, v896);
    svfloat32_t v472 = svmla_f32_x(pred_full, v273, v189, v899);
    svfloat32_t v489 = svsub_f32_x(svptrue_b32(), v467, v468);
    svfloat32_t v491 = svnmls_f32_x(pred_full, v333, v199, v911);
    svfloat32_t v492 = svnmls_f32_x(pred_full, v333, v200, v912);
    svfloat32_t v493 = svmla_f32_x(pred_full, v185, v177, v895);
    svfloat32_t v501 = svadd_f32_x(svptrue_b32(), v498, v499);
    svfloat32_t v502 = svcmla_f32_x(pred_full, v347, v917, v220, 90);
    svfloat32_t v503 = svcmla_f32_x(pred_full, v368, v920, v223, 90);
    svfloat32_t v520 = svsub_f32_x(svptrue_b32(), v498, v499);
    svfloat32_t v522 = svsub_f32_x(svptrue_b32(), v452, v466);
    svfloat32_t v523 = svsub_f32_x(svptrue_b32(), v459, v466);
    svint16_t v554 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v185, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v469 = svmla_f32_x(pred_full, v318, v197, v909);
    svfloat32_t v473 = svmla_f32_x(pred_full, v318, v195, v908);
    svfloat32_t v474 = svnmls_f32_x(pred_full, v470, v165, v902);
    svfloat32_t v475 = svadd_f32_x(svptrue_b32(), v471, v472);
    svfloat32_t v481 = svsub_f32_x(svptrue_b32(), v471, v472);
    svfloat32_t v486 = svmla_f32_x(pred_full, v470, v164, v907);
    svfloat32_t v494 = svadd_f32_x(svptrue_b32(), v493, v491);
    svfloat32_t v495 = svsub_f32_x(svptrue_b32(), v493, v491);
    svfloat32_t v497 = svadd_f32_x(svptrue_b32(), v493, v492);
    svfloat32_t v500 = svcmla_f32_x(pred_full, v445, v928, v229, 90);
    svfloat32_t v504 = svcmla_f32_x(pred_full, v445, v927, v227, 90);
    svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v389, v501);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v502, v503);
    svfloat32_t v512 = svsub_f32_x(svptrue_b32(), v502, v503);
    svfloat32_t v517 = svcmla_f32_x(pred_full, v501, v926, v204, 90);
    svfloat32_t v524 = svadd_f32_x(svptrue_b32(), v340, v522);
    svfloat32_t v525 = svsub_f32_x(svptrue_b32(), v340, v522);
    svfloat32_t v527 = svadd_f32_x(svptrue_b32(), v340, v523);
    svst1w_u64(pred_full, (unsigned *)(v940), svreinterpret_u64_s16(v554));
    svfloat32_t v476 = svnmls_f32_x(pred_full, v473, v167, v905);
    svfloat32_t v477 = svmla_f32_x(pred_full, v469, v192, v903);
    svfloat32_t v479 = svmla_f32_x(pred_full, v475, v193, v906);
    svfloat32_t v482 = svadd_f32_x(svptrue_b32(), v481, v469);
    svfloat32_t v483 = svadd_f32_x(svptrue_b32(), v474, v475);
    svfloat32_t v490 = svadd_f32_x(svptrue_b32(), v489, v473);
    svfloat32_t v496 = svsub_f32_x(svptrue_b32(), v495, v492);
    svfloat32_t v507 = svsub_f32_x(svptrue_b32(), v410, v504);
    svfloat32_t v508 = svcmla_f32_x(pred_full, v500, v922, v224, 90);
    svfloat32_t v510 = svcmla_f32_x(pred_full, v506, v925, v225, 90);
    svfloat32_t v513 = svadd_f32_x(svptrue_b32(), v512, v500);
    svfloat32_t v514 = svadd_f32_x(svptrue_b32(), v505, v506);
    svfloat32_t v521 = svadd_f32_x(svptrue_b32(), v520, v504);
    svfloat32_t v526 = svsub_f32_x(svptrue_b32(), v525, v523);
    svfloat32_t v478 = svadd_f32_x(svptrue_b32(), v477, v474);
    svfloat32_t v480 = svadd_f32_x(svptrue_b32(), v479, v476);
    svfloat32_t v484 = svmla_f32_x(pred_full, v483, v162, v904);
    svfloat32_t v487 = svadd_f32_x(svptrue_b32(), v486, v476);
    svfloat32_t v509 = svadd_f32_x(svptrue_b32(), v508, v505);
    svfloat32_t v511 = svadd_f32_x(svptrue_b32(), v510, v507);
    svfloat32_t v515 = svcmla_f32_x(pred_full, v514, v923, v202, 90);
    svfloat32_t v518 = svadd_f32_x(svptrue_b32(), v517, v507);
    svfloat32_t v532 = svsub_f32_x(svptrue_b32(), v490, v482);
    svfloat32_t v536 = svsub_f32_x(svptrue_b32(), v497, v490);
    svfloat32_t v539 = svadd_f32_x(svptrue_b32(), v482, v497);
    svfloat32_t v544 = svsub_f32_x(svptrue_b32(), v521, v513);
    svfloat32_t v548 = svsub_f32_x(svptrue_b32(), v521, v527);
    svfloat32_t v551 = svadd_f32_x(svptrue_b32(), v513, v527);
    svfloat32_t v485 = svadd_f32_x(svptrue_b32(), v484, v473);
    svfloat32_t v488 = svadd_f32_x(svptrue_b32(), v487, v469);
    svfloat32_t v516 = svadd_f32_x(svptrue_b32(), v515, v504);
    svfloat32_t v519 = svadd_f32_x(svptrue_b32(), v518, v500);
    svfloat32_t v533 = svadd_f32_x(svptrue_b32(), v532, v497);
    svfloat32_t v537 = svadd_f32_x(svptrue_b32(), v478, v494);
    svfloat32_t v538 = svadd_f32_x(svptrue_b32(), v480, v496);
    svfloat32_t v545 = svadd_f32_x(svptrue_b32(), v544, v527);
    svfloat32_t v549 = svadd_f32_x(svptrue_b32(), v509, v524);
    svfloat32_t v550 = svadd_f32_x(svptrue_b32(), v511, v526);
    svfloat32_t v578 = svsub_f32_x(svptrue_b32(), v539, v551);
    svfloat32_t v587 = svadd_f32_x(svptrue_b32(), v539, v551);
    svfloat32_t v596 = svadd_f32_x(svptrue_b32(), v536, v548);
    svfloat32_t v605 = svsub_f32_x(svptrue_b32(), v536, v548);
    svfloat32_t v528 = svsub_f32_x(svptrue_b32(), v485, v478);
    svfloat32_t v530 = svsub_f32_x(svptrue_b32(), v488, v480);
    svfloat32_t v534 = svsub_f32_x(svptrue_b32(), v494, v485);
    svfloat32_t v535 = svsub_f32_x(svptrue_b32(), v496, v488);
    svfloat32_t v540 = svsub_f32_x(svptrue_b32(), v516, v509);
    svfloat32_t v542 = svsub_f32_x(svptrue_b32(), v519, v511);
    svfloat32_t v546 = svsub_f32_x(svptrue_b32(), v524, v516);
    svfloat32_t v547 = svsub_f32_x(svptrue_b32(), v526, v519);
    svint16_t v581 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v578, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v590 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v587, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v599 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v596, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v608 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v605, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v614 = svadd_f32_x(svptrue_b32(), v538, v550);
    svfloat32_t v623 = svsub_f32_x(svptrue_b32(), v538, v550);
    svfloat32_t v632 = svadd_f32_x(svptrue_b32(), v533, v545);
    svfloat32_t v641 = svsub_f32_x(svptrue_b32(), v533, v545);
    svfloat32_t v686 = svsub_f32_x(svptrue_b32(), v537, v549);
    svfloat32_t v695 = svadd_f32_x(svptrue_b32(), v537, v549);
    svfloat32_t v529 = svadd_f32_x(svptrue_b32(), v528, v494);
    svfloat32_t v531 = svadd_f32_x(svptrue_b32(), v530, v496);
    svfloat32_t v541 = svadd_f32_x(svptrue_b32(), v540, v524);
    svfloat32_t v543 = svadd_f32_x(svptrue_b32(), v542, v526);
    svint16_t v617 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v614, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v626 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v623, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v635 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v632, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v644 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v641, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v650 = svadd_f32_x(svptrue_b32(), v535, v547);
    svfloat32_t v659 = svsub_f32_x(svptrue_b32(), v535, v547);
    svfloat32_t v668 = svadd_f32_x(svptrue_b32(), v534, v546);
    svfloat32_t v677 = svsub_f32_x(svptrue_b32(), v534, v546);
    svint16_t v689 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v686, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v698 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v695, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v967), svreinterpret_u64_s16(v581));
    svst1w_u64(pred_full, (unsigned *)(v976), svreinterpret_u64_s16(v590));
    svst1w_u64(pred_full, (unsigned *)(v985), svreinterpret_u64_s16(v599));
    svst1w_u64(pred_full, (unsigned *)(v994), svreinterpret_u64_s16(v608));
    svfloat32_t v560 = svadd_f32_x(svptrue_b32(), v529, v541);
    svfloat32_t v569 = svsub_f32_x(svptrue_b32(), v529, v541);
    svint16_t v653 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v650, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v662 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v659, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v671 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v668, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v680 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v677, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v704 = svadd_f32_x(svptrue_b32(), v531, v543);
    svfloat32_t v713 = svsub_f32_x(svptrue_b32(), v531, v543);
    svst1w_u64(pred_full, (unsigned *)(v1003), svreinterpret_u64_s16(v617));
    svst1w_u64(pred_full, (unsigned *)(v1012), svreinterpret_u64_s16(v626));
    svst1w_u64(pred_full, (unsigned *)(v1021), svreinterpret_u64_s16(v635));
    svst1w_u64(pred_full, (unsigned *)(v1030), svreinterpret_u64_s16(v644));
    svst1w_u64(pred_full, (unsigned *)(v1075), svreinterpret_u64_s16(v689));
    svst1w_u64(pred_full, (unsigned *)(v1084), svreinterpret_u64_s16(v698));
    svint16_t v563 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v560, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v572 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v569, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v707 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v704, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v716 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v713, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1039), svreinterpret_u64_s16(v653));
    svst1w_u64(pred_full, (unsigned *)(v1048), svreinterpret_u64_s16(v662));
    svst1w_u64(pred_full, (unsigned *)(v1057), svreinterpret_u64_s16(v671));
    svst1w_u64(pred_full, (unsigned *)(v1066), svreinterpret_u64_s16(v680));
    svst1w_u64(pred_full, (unsigned *)(v949), svreinterpret_u64_s16(v563));
    svst1w_u64(pred_full, (unsigned *)(v958), svreinterpret_u64_s16(v572));
    svst1w_u64(pred_full, (unsigned *)(v1093), svreinterpret_u64_s16(v707));
    svst1w_u64(pred_full, (unsigned *)(v1102), svreinterpret_u64_s16(v716));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu20(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v136 = v5[istride];
    float v266 = 1.5388417685876268e+00F;
    float v273 = 5.8778525229247325e-01F;
    float v280 = 3.6327126400268028e-01F;
    float v304 = 1.0000000000000000e+00F;
    float v305 = -1.0000000000000000e+00F;
    float v311 = -1.2500000000000000e+00F;
    float v312 = 1.2500000000000000e+00F;
    float v318 = 5.5901699437494745e-01F;
    float v319 = -5.5901699437494745e-01F;
    float32x2_t v321 = (float32x2_t){v4, v4};
    float v326 = -1.5388417685876268e+00F;
    float v330 = -5.8778525229247325e-01F;
    float v334 = -3.6327126400268028e-01F;
    float32x2_t v20 = v5[0];
    float32x2_t v260 = (float32x2_t){v311, v311};
    float32x2_t v264 = (float32x2_t){v318, v318};
    float32x2_t v268 = (float32x2_t){v266, v326};
    float32x2_t v275 = (float32x2_t){v273, v330};
    float32x2_t v282 = (float32x2_t){v280, v334};
    float32x2_t v306 = (float32x2_t){v304, v305};
    float32x2_t v313 = (float32x2_t){v311, v312};
    float32x2_t v320 = (float32x2_t){v318, v319};
    float32x2_t v327 = (float32x2_t){v326, v326};
    float32x2_t v331 = (float32x2_t){v330, v330};
    float32x2_t v335 = (float32x2_t){v334, v334};
    float32x2_t v25 = v5[istride * 10];
    float32x2_t v32 = v5[istride * 5];
    float32x2_t v37 = v5[istride * 15];
    float32x2_t v46 = v5[istride * 4];
    float32x2_t v51 = v5[istride * 14];
    float32x2_t v58 = v5[istride * 9];
    float32x2_t v63 = v5[istride * 19];
    float32x2_t v72 = v5[istride * 8];
    float32x2_t v77 = v5[istride * 18];
    float32x2_t v84 = v5[istride * 13];
    float32x2_t v89 = v5[istride * 3];
    float32x2_t v98 = v5[istride * 12];
    float32x2_t v103 = v5[istride * 2];
    float32x2_t v110 = v5[istride * 17];
    float32x2_t v115 = v5[istride * 7];
    float32x2_t v124 = v5[istride * 16];
    float32x2_t v129 = v5[istride * 6];
    float32x2_t v141 = v5[istride * 11];
    float32x2_t v270 = vmul_f32(v321, v268);
    float32x2_t v277 = vmul_f32(v321, v275);
    float32x2_t v284 = vmul_f32(v321, v282);
    float32x2_t v308 = vmul_f32(v321, v306);
    float32x2_t v315 = vmul_f32(v321, v313);
    float32x2_t v322 = vmul_f32(v321, v320);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v52 = vadd_f32(v46, v51);
    float32x2_t v53 = vsub_f32(v46, v51);
    float32x2_t v64 = vadd_f32(v58, v63);
    float32x2_t v65 = vsub_f32(v58, v63);
    float32x2_t v78 = vadd_f32(v72, v77);
    float32x2_t v79 = vsub_f32(v72, v77);
    float32x2_t v90 = vadd_f32(v84, v89);
    float32x2_t v91 = vsub_f32(v84, v89);
    float32x2_t v104 = vadd_f32(v98, v103);
    float32x2_t v105 = vsub_f32(v98, v103);
    float32x2_t v116 = vadd_f32(v110, v115);
    float32x2_t v117 = vsub_f32(v110, v115);
    float32x2_t v130 = vadd_f32(v124, v129);
    float32x2_t v131 = vsub_f32(v124, v129);
    float32x2_t v142 = vadd_f32(v136, v141);
    float32x2_t v143 = vsub_f32(v136, v141);
    float32x2_t v40 = vadd_f32(v26, v38);
    float32x2_t v41 = vsub_f32(v26, v38);
    float32x2_t v66 = vadd_f32(v52, v64);
    float32x2_t v67 = vsub_f32(v52, v64);
    float32x2_t v92 = vadd_f32(v78, v90);
    float32x2_t v93 = vsub_f32(v78, v90);
    float32x2_t v118 = vadd_f32(v104, v116);
    float32x2_t v119 = vsub_f32(v104, v116);
    float32x2_t v144 = vadd_f32(v130, v142);
    float32x2_t v145 = vsub_f32(v130, v142);
    float32x2_t v246 = vadd_f32(v53, v131);
    float32x2_t v247 = vsub_f32(v53, v131);
    float32x2_t v248 = vadd_f32(v105, v79);
    float32x2_t v249 = vsub_f32(v105, v79);
    float32x2_t v296 = vadd_f32(v65, v143);
    float32x2_t v297 = vsub_f32(v65, v143);
    float32x2_t v298 = vadd_f32(v117, v91);
    float32x2_t v299 = vsub_f32(v117, v91);
    float32x2_t v146 = vadd_f32(v66, v144);
    float32x2_t v147 = vsub_f32(v66, v144);
    float32x2_t v148 = vadd_f32(v118, v92);
    float32x2_t v149 = vsub_f32(v118, v92);
    float32x2_t v196 = vadd_f32(v67, v145);
    float32x2_t v197 = vsub_f32(v67, v145);
    float32x2_t v198 = vadd_f32(v119, v93);
    float32x2_t v199 = vsub_f32(v119, v93);
    float32x2_t v250 = vadd_f32(v246, v248);
    float32x2_t v251 = vsub_f32(v246, v248);
    float32x2_t v252 = vadd_f32(v247, v249);
    float32x2_t v271 = vrev64_f32(v247);
    float32x2_t v285 = vrev64_f32(v249);
    float32x2_t v300 = vadd_f32(v296, v298);
    float32x2_t v301 = vsub_f32(v296, v298);
    float32x2_t v302 = vadd_f32(v297, v299);
    float32x2_t v328 = vmul_f32(v297, v327);
    float32x2_t v336 = vmul_f32(v299, v335);
    float32x2_t v150 = vadd_f32(v146, v148);
    float32x2_t v151 = vsub_f32(v146, v148);
    float32x2_t v152 = vadd_f32(v147, v149);
    float32x2_t v171 = vrev64_f32(v147);
    float32x2_t v185 = vrev64_f32(v149);
    float32x2_t v200 = vadd_f32(v196, v198);
    float32x2_t v201 = vsub_f32(v196, v198);
    float32x2_t v202 = vadd_f32(v197, v199);
    float32x2_t v221 = vrev64_f32(v197);
    float32x2_t v235 = vrev64_f32(v199);
    float32x2_t v253 = vadd_f32(v250, v27);
    float32x2_t v261 = vmul_f32(v250, v260);
    float32x2_t v265 = vmul_f32(v251, v264);
    float32x2_t v272 = vmul_f32(v271, v270);
    float32x2_t v278 = vrev64_f32(v252);
    float32x2_t v286 = vmul_f32(v285, v284);
    float32x2_t v303 = vadd_f32(v300, v39);
    float32x2_t v316 = vrev64_f32(v300);
    float32x2_t v323 = vrev64_f32(v301);
    float32x2_t v332 = vmul_f32(v302, v331);
    float32x2_t v153 = vadd_f32(v150, v40);
    float32x2_t v161 = vmul_f32(v150, v260);
    float32x2_t v165 = vmul_f32(v151, v264);
    float32x2_t v172 = vmul_f32(v171, v270);
    float32x2_t v178 = vrev64_f32(v152);
    float32x2_t v186 = vmul_f32(v185, v284);
    float32x2_t v203 = vadd_f32(v200, v41);
    float32x2_t v211 = vmul_f32(v200, v260);
    float32x2_t v215 = vmul_f32(v201, v264);
    float32x2_t v222 = vmul_f32(v221, v270);
    float32x2_t v228 = vrev64_f32(v202);
    float32x2_t v236 = vmul_f32(v235, v284);
    float32x2_t v279 = vmul_f32(v278, v277);
    float32x2_t v287 = vadd_f32(v253, v261);
    float32x2_t v309 = vrev64_f32(v303);
    float32x2_t v317 = vmul_f32(v316, v315);
    float32x2_t v324 = vmul_f32(v323, v322);
    float32x2_t v340 = vsub_f32(v328, v332);
    float32x2_t v341 = vadd_f32(v332, v336);
    float32x2_t v179 = vmul_f32(v178, v277);
    float32x2_t v187 = vadd_f32(v153, v161);
    float32x2_t v229 = vmul_f32(v228, v277);
    float32x2_t v237 = vadd_f32(v203, v211);
    float32x2_t v288 = vadd_f32(v287, v265);
    float32x2_t v289 = vsub_f32(v287, v265);
    float32x2_t v290 = vsub_f32(v272, v279);
    float32x2_t v291 = vadd_f32(v279, v286);
    float32x2_t v310 = vmul_f32(v309, v308);
    int16x4_t v350 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v153, 15), (int32x2_t){0, 0}));
    int16x4_t v362 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v203, 15), (int32x2_t){0, 0}));
    float32x2_t v188 = vadd_f32(v187, v165);
    float32x2_t v189 = vsub_f32(v187, v165);
    float32x2_t v190 = vsub_f32(v172, v179);
    float32x2_t v191 = vadd_f32(v179, v186);
    float32x2_t v238 = vadd_f32(v237, v215);
    float32x2_t v239 = vsub_f32(v237, v215);
    float32x2_t v240 = vsub_f32(v222, v229);
    float32x2_t v241 = vadd_f32(v229, v236);
    float32x2_t v292 = vadd_f32(v288, v290);
    float32x2_t v293 = vsub_f32(v288, v290);
    float32x2_t v294 = vadd_f32(v289, v291);
    float32x2_t v295 = vsub_f32(v289, v291);
    float32x2_t v337 = vadd_f32(v310, v317);
    float32x2_t v346 = vadd_f32(v253, v310);
    float32x2_t v347 = vsub_f32(v253, v310);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v350), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v362), 0);
    float32x2_t v192 = vadd_f32(v188, v190);
    float32x2_t v193 = vsub_f32(v188, v190);
    float32x2_t v194 = vadd_f32(v189, v191);
    float32x2_t v195 = vsub_f32(v189, v191);
    float32x2_t v242 = vadd_f32(v238, v240);
    float32x2_t v243 = vsub_f32(v238, v240);
    float32x2_t v244 = vadd_f32(v239, v241);
    float32x2_t v245 = vsub_f32(v239, v241);
    float32x2_t v338 = vadd_f32(v337, v324);
    float32x2_t v339 = vsub_f32(v337, v324);
    int16x4_t v356 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v347, 15), (int32x2_t){0, 0}));
    int16x4_t v368 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v346, 15), (int32x2_t){0, 0}));
    float32x2_t v342 = vadd_f32(v338, v340);
    float32x2_t v343 = vsub_f32(v338, v340);
    float32x2_t v344 = vadd_f32(v339, v341);
    float32x2_t v345 = vsub_f32(v339, v341);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v356), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v368), 0);
    int16x4_t v376 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v193, 15), (int32x2_t){0, 0}));
    int16x4_t v388 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v243, 15), (int32x2_t){0, 0}));
    int16x4_t v402 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v195, 15), (int32x2_t){0, 0}));
    int16x4_t v414 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v245, 15), (int32x2_t){0, 0}));
    int16x4_t v428 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v194, 15), (int32x2_t){0, 0}));
    int16x4_t v440 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v244, 15), (int32x2_t){0, 0}));
    int16x4_t v454 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v192, 15), (int32x2_t){0, 0}));
    int16x4_t v466 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v242, 15), (int32x2_t){0, 0}));
    float32x2_t v372 = vadd_f32(v293, v343);
    float32x2_t v373 = vsub_f32(v293, v343);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v376), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v388), 0);
    float32x2_t v398 = vadd_f32(v295, v345);
    float32x2_t v399 = vsub_f32(v295, v345);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v402), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v414), 0);
    float32x2_t v424 = vadd_f32(v294, v344);
    float32x2_t v425 = vsub_f32(v294, v344);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v428), 0);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v440), 0);
    float32x2_t v450 = vadd_f32(v292, v342);
    float32x2_t v451 = vsub_f32(v292, v342);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v454), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v466), 0);
    int16x4_t v382 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v373, 15), (int32x2_t){0, 0}));
    int16x4_t v394 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v372, 15), (int32x2_t){0, 0}));
    int16x4_t v408 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v399, 15), (int32x2_t){0, 0}));
    int16x4_t v420 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v398, 15), (int32x2_t){0, 0}));
    int16x4_t v434 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v425, 15), (int32x2_t){0, 0}));
    int16x4_t v446 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v424, 15), (int32x2_t){0, 0}));
    int16x4_t v460 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v451, 15), (int32x2_t){0, 0}));
    int16x4_t v472 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v450, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v382), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v394), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v408), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v420), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v434), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v446), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v460), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v472), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu20(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v308 = -1.2500000000000000e+00F;
    float v313 = 5.5901699437494745e-01F;
    float v356 = -1.0000000000000000e+00F;
    float v363 = 1.2500000000000000e+00F;
    float v370 = -5.5901699437494745e-01F;
    float v377 = -1.5388417685876268e+00F;
    float v382 = -5.8778525229247325e-01F;
    float v387 = -3.6327126400268028e-01F;
    const float32x2_t *v739 = &v5[v0];
    int32_t *v827 = &v6[v2];
    int64_t v26 = v0 * 10;
    int64_t v35 = v0 * 5;
    int64_t v42 = v0 * 15;
    int64_t v53 = v0 * 4;
    int64_t v60 = v0 * 14;
    int64_t v69 = v0 * 9;
    int64_t v76 = v0 * 19;
    int64_t v87 = v0 * 8;
    int64_t v94 = v0 * 18;
    int64_t v103 = v0 * 13;
    int64_t v110 = v0 * 3;
    int64_t v121 = v0 * 12;
    int64_t v128 = v0 * 2;
    int64_t v137 = v0 * 17;
    int64_t v144 = v0 * 7;
    int64_t v155 = v0 * 16;
    int64_t v162 = v0 * 6;
    int64_t v178 = v0 * 11;
    float v321 = v4 * v377;
    float v328 = v4 * v382;
    float v335 = v4 * v387;
    float v359 = v4 * v356;
    float v366 = v4 * v363;
    float v373 = v4 * v370;
    int64_t v411 = v2 * 5;
    int64_t v419 = v2 * 10;
    int64_t v427 = v2 * 15;
    int64_t v437 = v2 * 16;
    int64_t v453 = v2 * 6;
    int64_t v461 = v2 * 11;
    int64_t v471 = v2 * 12;
    int64_t v479 = v2 * 17;
    int64_t v487 = v2 * 2;
    int64_t v495 = v2 * 7;
    int64_t v505 = v2 * 8;
    int64_t v513 = v2 * 13;
    int64_t v521 = v2 * 18;
    int64_t v529 = v2 * 3;
    int64_t v539 = v2 * 4;
    int64_t v547 = v2 * 9;
    int64_t v555 = v2 * 14;
    int64_t v563 = v2 * 19;
    const float32x2_t *v577 = &v5[0];
    svfloat32_t v764 = svdup_n_f32(v308);
    svfloat32_t v765 = svdup_n_f32(v313);
    svfloat32_t v772 = svdup_n_f32(v377);
    svfloat32_t v773 = svdup_n_f32(v382);
    svfloat32_t v774 = svdup_n_f32(v387);
    int32_t *v782 = &v6[0];
    svfloat32_t v993 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v739)[0]));
    const float32x2_t *v586 = &v5[v26];
    const float32x2_t *v595 = &v5[v35];
    const float32x2_t *v604 = &v5[v42];
    const float32x2_t *v613 = &v5[v53];
    const float32x2_t *v622 = &v5[v60];
    const float32x2_t *v631 = &v5[v69];
    const float32x2_t *v640 = &v5[v76];
    const float32x2_t *v649 = &v5[v87];
    const float32x2_t *v658 = &v5[v94];
    const float32x2_t *v667 = &v5[v103];
    const float32x2_t *v676 = &v5[v110];
    const float32x2_t *v685 = &v5[v121];
    const float32x2_t *v694 = &v5[v128];
    const float32x2_t *v703 = &v5[v137];
    const float32x2_t *v712 = &v5[v144];
    const float32x2_t *v721 = &v5[v155];
    const float32x2_t *v730 = &v5[v162];
    const float32x2_t *v748 = &v5[v178];
    svfloat32_t v766 = svdup_n_f32(v321);
    svfloat32_t v767 = svdup_n_f32(v328);
    svfloat32_t v768 = svdup_n_f32(v335);
    svfloat32_t v769 = svdup_n_f32(v359);
    svfloat32_t v770 = svdup_n_f32(v366);
    svfloat32_t v771 = svdup_n_f32(v373);
    int32_t *v791 = &v6[v411];
    int32_t *v800 = &v6[v419];
    int32_t *v809 = &v6[v427];
    int32_t *v818 = &v6[v437];
    int32_t *v836 = &v6[v453];
    int32_t *v845 = &v6[v461];
    int32_t *v854 = &v6[v471];
    int32_t *v863 = &v6[v479];
    int32_t *v872 = &v6[v487];
    int32_t *v881 = &v6[v495];
    int32_t *v890 = &v6[v505];
    int32_t *v899 = &v6[v513];
    int32_t *v908 = &v6[v521];
    int32_t *v917 = &v6[v529];
    int32_t *v926 = &v6[v539];
    int32_t *v935 = &v6[v547];
    int32_t *v944 = &v6[v555];
    int32_t *v953 = &v6[v563];
    svfloat32_t v957 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v577)[0]));
    svfloat32_t v959 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v586)[0]));
    svfloat32_t v961 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v595)[0]));
    svfloat32_t v963 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v604)[0]));
    svfloat32_t v965 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v613)[0]));
    svfloat32_t v967 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v622)[0]));
    svfloat32_t v969 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v631)[0]));
    svfloat32_t v971 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v640)[0]));
    svfloat32_t v973 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v649)[0]));
    svfloat32_t v975 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v658)[0]));
    svfloat32_t v977 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v667)[0]));
    svfloat32_t v979 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v676)[0]));
    svfloat32_t v981 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v685)[0]));
    svfloat32_t v983 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v694)[0]));
    svfloat32_t v985 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v703)[0]));
    svfloat32_t v987 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v712)[0]));
    svfloat32_t v989 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v721)[0]));
    svfloat32_t v991 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v730)[0]));
    svfloat32_t v995 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v748)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v957, v959);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v957, v959);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v961, v963);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v961, v963);
    svfloat32_t v66 = svadd_f32_x(svptrue_b32(), v965, v967);
    svfloat32_t v67 = svsub_f32_x(svptrue_b32(), v965, v967);
    svfloat32_t v82 = svadd_f32_x(svptrue_b32(), v969, v971);
    svfloat32_t v83 = svsub_f32_x(svptrue_b32(), v969, v971);
    svfloat32_t v100 = svadd_f32_x(svptrue_b32(), v973, v975);
    svfloat32_t v101 = svsub_f32_x(svptrue_b32(), v973, v975);
    svfloat32_t v116 = svadd_f32_x(svptrue_b32(), v977, v979);
    svfloat32_t v117 = svsub_f32_x(svptrue_b32(), v977, v979);
    svfloat32_t v134 = svadd_f32_x(svptrue_b32(), v981, v983);
    svfloat32_t v135 = svsub_f32_x(svptrue_b32(), v981, v983);
    svfloat32_t v150 = svadd_f32_x(svptrue_b32(), v985, v987);
    svfloat32_t v151 = svsub_f32_x(svptrue_b32(), v985, v987);
    svfloat32_t v168 = svadd_f32_x(svptrue_b32(), v989, v991);
    svfloat32_t v169 = svsub_f32_x(svptrue_b32(), v989, v991);
    svfloat32_t v184 = svadd_f32_x(svptrue_b32(), v993, v995);
    svfloat32_t v185 = svsub_f32_x(svptrue_b32(), v993, v995);
    svfloat32_t v50 = svadd_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v51 = svsub_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v84 = svadd_f32_x(svptrue_b32(), v66, v82);
    svfloat32_t v85 = svsub_f32_x(svptrue_b32(), v66, v82);
    svfloat32_t v118 = svadd_f32_x(svptrue_b32(), v100, v116);
    svfloat32_t v119 = svsub_f32_x(svptrue_b32(), v100, v116);
    svfloat32_t v152 = svadd_f32_x(svptrue_b32(), v134, v150);
    svfloat32_t v153 = svsub_f32_x(svptrue_b32(), v134, v150);
    svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v168, v184);
    svfloat32_t v187 = svsub_f32_x(svptrue_b32(), v168, v184);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v67, v169);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v67, v169);
    svfloat32_t v296 = svadd_f32_x(svptrue_b32(), v135, v101);
    svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v135, v101);
    svfloat32_t v347 = svadd_f32_x(svptrue_b32(), v83, v185);
    svfloat32_t v348 = svsub_f32_x(svptrue_b32(), v83, v185);
    svfloat32_t v349 = svadd_f32_x(svptrue_b32(), v151, v117);
    svfloat32_t v350 = svsub_f32_x(svptrue_b32(), v151, v117);
    svfloat32_t v188 = svadd_f32_x(svptrue_b32(), v84, v186);
    svfloat32_t v189 = svsub_f32_x(svptrue_b32(), v84, v186);
    svfloat32_t v190 = svadd_f32_x(svptrue_b32(), v152, v118);
    svfloat32_t v191 = svsub_f32_x(svptrue_b32(), v152, v118);
    svfloat32_t v241 = svadd_f32_x(svptrue_b32(), v85, v187);
    svfloat32_t v242 = svsub_f32_x(svptrue_b32(), v85, v187);
    svfloat32_t v243 = svadd_f32_x(svptrue_b32(), v153, v119);
    svfloat32_t v244 = svsub_f32_x(svptrue_b32(), v153, v119);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v294, v296);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v294, v296);
    svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v295, v297);
    svfloat32_t zero323 = svdup_n_f32(0);
    svfloat32_t v323 = svcmla_f32_x(pred_full, zero323, v766, v295, 90);
    svfloat32_t v351 = svadd_f32_x(svptrue_b32(), v347, v349);
    svfloat32_t v352 = svsub_f32_x(svptrue_b32(), v347, v349);
    svfloat32_t v353 = svadd_f32_x(svptrue_b32(), v348, v350);
    svfloat32_t v390 = svmul_f32_x(svptrue_b32(), v350, v774);
    svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v188, v190);
    svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v188, v190);
    svfloat32_t v194 = svadd_f32_x(svptrue_b32(), v189, v191);
    svfloat32_t zero217 = svdup_n_f32(0);
    svfloat32_t v217 = svcmla_f32_x(pred_full, zero217, v766, v189, 90);
    svfloat32_t v245 = svadd_f32_x(svptrue_b32(), v241, v243);
    svfloat32_t v246 = svsub_f32_x(svptrue_b32(), v241, v243);
    svfloat32_t v247 = svadd_f32_x(svptrue_b32(), v242, v244);
    svfloat32_t zero270 = svdup_n_f32(0);
    svfloat32_t v270 = svcmla_f32_x(pred_full, zero270, v766, v242, 90);
    svfloat32_t v301 = svadd_f32_x(svptrue_b32(), v298, v33);
    svfloat32_t zero330 = svdup_n_f32(0);
    svfloat32_t v330 = svcmla_f32_x(pred_full, zero330, v767, v300, 90);
    svfloat32_t v354 = svadd_f32_x(svptrue_b32(), v351, v49);
    svfloat32_t zero375 = svdup_n_f32(0);
    svfloat32_t v375 = svcmla_f32_x(pred_full, zero375, v771, v352, 90);
    svfloat32_t v385 = svmul_f32_x(svptrue_b32(), v353, v773);
    svfloat32_t v195 = svadd_f32_x(svptrue_b32(), v192, v50);
    svfloat32_t zero224 = svdup_n_f32(0);
    svfloat32_t v224 = svcmla_f32_x(pred_full, zero224, v767, v194, 90);
    svfloat32_t v248 = svadd_f32_x(svptrue_b32(), v245, v51);
    svfloat32_t zero277 = svdup_n_f32(0);
    svfloat32_t v277 = svcmla_f32_x(pred_full, zero277, v767, v247, 90);
    svfloat32_t v338 = svmla_f32_x(pred_full, v301, v298, v764);
    svfloat32_t v341 = svsub_f32_x(svptrue_b32(), v323, v330);
    svfloat32_t v342 = svcmla_f32_x(pred_full, v330, v768, v297, 90);
    svfloat32_t zero361 = svdup_n_f32(0);
    svfloat32_t v361 = svcmla_f32_x(pred_full, zero361, v769, v354, 90);
    svfloat32_t v394 = svnmls_f32_x(pred_full, v385, v348, v772);
    svfloat32_t v395 = svmla_f32_x(pred_full, v390, v353, v773);
    svfloat32_t v232 = svmla_f32_x(pred_full, v195, v192, v764);
    svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v217, v224);
    svfloat32_t v236 = svcmla_f32_x(pred_full, v224, v768, v191, 90);
    svfloat32_t v285 = svmla_f32_x(pred_full, v248, v245, v764);
    svfloat32_t v288 = svsub_f32_x(svptrue_b32(), v270, v277);
    svfloat32_t v289 = svcmla_f32_x(pred_full, v277, v768, v244, 90);
    svfloat32_t v339 = svmla_f32_x(pred_full, v338, v299, v765);
    svfloat32_t v340 = svmls_f32_x(pred_full, v338, v299, v765);
    svfloat32_t v391 = svcmla_f32_x(pred_full, v361, v770, v351, 90);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v301, v361);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v301, v361);
    svint16_t v404 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v195, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v420 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v248, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v233 = svmla_f32_x(pred_full, v232, v193, v765);
    svfloat32_t v234 = svmls_f32_x(pred_full, v232, v193, v765);
    svfloat32_t v286 = svmla_f32_x(pred_full, v285, v246, v765);
    svfloat32_t v287 = svmls_f32_x(pred_full, v285, v246, v765);
    svfloat32_t v343 = svadd_f32_x(svptrue_b32(), v339, v341);
    svfloat32_t v344 = svsub_f32_x(svptrue_b32(), v339, v341);
    svfloat32_t v345 = svadd_f32_x(svptrue_b32(), v340, v342);
    svfloat32_t v346 = svsub_f32_x(svptrue_b32(), v340, v342);
    svfloat32_t v392 = svadd_f32_x(svptrue_b32(), v391, v375);
    svfloat32_t v393 = svsub_f32_x(svptrue_b32(), v391, v375);
    svint16_t v412 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v401, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v428 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v400, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v782), svreinterpret_u64_s16(v404));
    svst1w_u64(pred_full, (unsigned *)(v800), svreinterpret_u64_s16(v420));
    svfloat32_t v237 = svadd_f32_x(svptrue_b32(), v233, v235);
    svfloat32_t v238 = svsub_f32_x(svptrue_b32(), v233, v235);
    svfloat32_t v239 = svadd_f32_x(svptrue_b32(), v234, v236);
    svfloat32_t v240 = svsub_f32_x(svptrue_b32(), v234, v236);
    svfloat32_t v290 = svadd_f32_x(svptrue_b32(), v286, v288);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v286, v288);
    svfloat32_t v292 = svadd_f32_x(svptrue_b32(), v287, v289);
    svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v287, v289);
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v392, v394);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v392, v394);
    svfloat32_t v398 = svadd_f32_x(svptrue_b32(), v393, v395);
    svfloat32_t v399 = svsub_f32_x(svptrue_b32(), v393, v395);
    svst1w_u64(pred_full, (unsigned *)(v791), svreinterpret_u64_s16(v412));
    svst1w_u64(pred_full, (unsigned *)(v809), svreinterpret_u64_s16(v428));
    svfloat32_t v434 = svadd_f32_x(svptrue_b32(), v344, v397);
    svfloat32_t v435 = svsub_f32_x(svptrue_b32(), v344, v397);
    svint16_t v438 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v238, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v454 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v291, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v468 = svadd_f32_x(svptrue_b32(), v346, v399);
    svfloat32_t v469 = svsub_f32_x(svptrue_b32(), v346, v399);
    svint16_t v472 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v240, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v488 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v293, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v502 = svadd_f32_x(svptrue_b32(), v345, v398);
    svfloat32_t v503 = svsub_f32_x(svptrue_b32(), v345, v398);
    svint16_t v506 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v239, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v522 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v292, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v536 = svadd_f32_x(svptrue_b32(), v343, v396);
    svfloat32_t v537 = svsub_f32_x(svptrue_b32(), v343, v396);
    svint16_t v540 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v237, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v556 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v290, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v446 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v435, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v462 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v434, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v480 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v469, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v496 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v468, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v514 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v503, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v530 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v502, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v548 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v537, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v564 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v536, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v818), svreinterpret_u64_s16(v438));
    svst1w_u64(pred_full, (unsigned *)(v836), svreinterpret_u64_s16(v454));
    svst1w_u64(pred_full, (unsigned *)(v854), svreinterpret_u64_s16(v472));
    svst1w_u64(pred_full, (unsigned *)(v872), svreinterpret_u64_s16(v488));
    svst1w_u64(pred_full, (unsigned *)(v890), svreinterpret_u64_s16(v506));
    svst1w_u64(pred_full, (unsigned *)(v908), svreinterpret_u64_s16(v522));
    svst1w_u64(pred_full, (unsigned *)(v926), svreinterpret_u64_s16(v540));
    svst1w_u64(pred_full, (unsigned *)(v944), svreinterpret_u64_s16(v556));
    svst1w_u64(pred_full, (unsigned *)(v827), svreinterpret_u64_s16(v446));
    svst1w_u64(pred_full, (unsigned *)(v845), svreinterpret_u64_s16(v462));
    svst1w_u64(pred_full, (unsigned *)(v863), svreinterpret_u64_s16(v480));
    svst1w_u64(pred_full, (unsigned *)(v881), svreinterpret_u64_s16(v496));
    svst1w_u64(pred_full, (unsigned *)(v899), svreinterpret_u64_s16(v514));
    svst1w_u64(pred_full, (unsigned *)(v917), svreinterpret_u64_s16(v530));
    svst1w_u64(pred_full, (unsigned *)(v935), svreinterpret_u64_s16(v548));
    svst1w_u64(pred_full, (unsigned *)(v953), svreinterpret_u64_s16(v564));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu21(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v110 = v5[istride];
    float v164 = -1.1666666666666665e+00F;
    float v168 = 7.9015646852540022e-01F;
    float v172 = 5.5854267289647742e-02F;
    float v176 = 7.3430220123575241e-01F;
    float v179 = 4.4095855184409838e-01F;
    float v180 = -4.4095855184409838e-01F;
    float v186 = 3.4087293062393137e-01F;
    float v187 = -3.4087293062393137e-01F;
    float v193 = -5.3396936033772524e-01F;
    float v194 = 5.3396936033772524e-01F;
    float v200 = 8.7484229096165667e-01F;
    float v201 = -8.7484229096165667e-01F;
    float v244 = -1.4999999999999998e+00F;
    float v248 = 1.7499999999999996e+00F;
    float v252 = -1.1852347027881001e+00F;
    float v256 = -8.3781400934471603e-02F;
    float v260 = -1.1014533018536286e+00F;
    float v263 = -6.6143782776614746e-01F;
    float v264 = 6.6143782776614746e-01F;
    float v270 = -5.1130939593589697e-01F;
    float v271 = 5.1130939593589697e-01F;
    float v277 = 8.0095404050658769e-01F;
    float v278 = -8.0095404050658769e-01F;
    float v284 = -1.3122634364424848e+00F;
    float v285 = 1.3122634364424848e+00F;
    float v327 = 8.6602540378443871e-01F;
    float v328 = -8.6602540378443871e-01F;
    float v334 = -1.0103629710818451e+00F;
    float v335 = 1.0103629710818451e+00F;
    float v341 = 6.8429557470759583e-01F;
    float v342 = -6.8429557470759583e-01F;
    float v348 = 4.8371214382601155e-02F;
    float v349 = -4.8371214382601155e-02F;
    float v355 = 6.3592436032499466e-01F;
    float v356 = -6.3592436032499466e-01F;
    float32x2_t v358 = (float32x2_t){v4, v4};
    float v363 = -3.8188130791298663e-01F;
    float v367 = -2.9520461738277515e-01F;
    float v371 = 4.6243103089499693e-01F;
    float v375 = -7.5763564827777208e-01F;
    float32x2_t v32 = v5[0];
    float32x2_t v165 = (float32x2_t){v164, v164};
    float32x2_t v169 = (float32x2_t){v168, v168};
    float32x2_t v173 = (float32x2_t){v172, v172};
    float32x2_t v177 = (float32x2_t){v176, v176};
    float32x2_t v181 = (float32x2_t){v179, v180};
    float32x2_t v188 = (float32x2_t){v186, v187};
    float32x2_t v195 = (float32x2_t){v193, v194};
    float32x2_t v202 = (float32x2_t){v200, v201};
    float32x2_t v245 = (float32x2_t){v244, v244};
    float32x2_t v249 = (float32x2_t){v248, v248};
    float32x2_t v253 = (float32x2_t){v252, v252};
    float32x2_t v257 = (float32x2_t){v256, v256};
    float32x2_t v261 = (float32x2_t){v260, v260};
    float32x2_t v265 = (float32x2_t){v263, v264};
    float32x2_t v272 = (float32x2_t){v270, v271};
    float32x2_t v279 = (float32x2_t){v277, v278};
    float32x2_t v286 = (float32x2_t){v284, v285};
    float32x2_t v329 = (float32x2_t){v327, v328};
    float32x2_t v336 = (float32x2_t){v334, v335};
    float32x2_t v343 = (float32x2_t){v341, v342};
    float32x2_t v350 = (float32x2_t){v348, v349};
    float32x2_t v357 = (float32x2_t){v355, v356};
    float32x2_t v364 = (float32x2_t){v363, v363};
    float32x2_t v368 = (float32x2_t){v367, v367};
    float32x2_t v372 = (float32x2_t){v371, v371};
    float32x2_t v376 = (float32x2_t){v375, v375};
    float32x2_t v20 = v5[istride * 7];
    float32x2_t v25 = v5[istride * 14];
    float32x2_t v38 = v5[istride * 10];
    float32x2_t v43 = v5[istride * 17];
    float32x2_t v50 = v5[istride * 3];
    float32x2_t v56 = v5[istride * 13];
    float32x2_t v61 = v5[istride * 20];
    float32x2_t v68 = v5[istride * 6];
    float32x2_t v74 = v5[istride * 16];
    float32x2_t v79 = v5[istride * 2];
    float32x2_t v86 = v5[istride * 9];
    float32x2_t v92 = v5[istride * 19];
    float32x2_t v97 = v5[istride * 5];
    float32x2_t v104 = v5[istride * 12];
    float32x2_t v115 = v5[istride * 8];
    float32x2_t v122 = v5[istride * 15];
    float32x2_t v128 = v5[istride * 4];
    float32x2_t v133 = v5[istride * 11];
    float32x2_t v140 = v5[istride * 18];
    float32x2_t v183 = vmul_f32(v358, v181);
    float32x2_t v190 = vmul_f32(v358, v188);
    float32x2_t v197 = vmul_f32(v358, v195);
    float32x2_t v204 = vmul_f32(v358, v202);
    float32x2_t v267 = vmul_f32(v358, v265);
    float32x2_t v274 = vmul_f32(v358, v272);
    float32x2_t v281 = vmul_f32(v358, v279);
    float32x2_t v288 = vmul_f32(v358, v286);
    float32x2_t v331 = vmul_f32(v358, v329);
    float32x2_t v338 = vmul_f32(v358, v336);
    float32x2_t v345 = vmul_f32(v358, v343);
    float32x2_t v352 = vmul_f32(v358, v350);
    float32x2_t v359 = vmul_f32(v358, v357);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v44 = vadd_f32(v38, v43);
    float32x2_t v45 = vsub_f32(v38, v43);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v80 = vadd_f32(v74, v79);
    float32x2_t v81 = vsub_f32(v74, v79);
    float32x2_t v98 = vadd_f32(v92, v97);
    float32x2_t v99 = vsub_f32(v92, v97);
    float32x2_t v116 = vadd_f32(v110, v115);
    float32x2_t v117 = vsub_f32(v110, v115);
    float32x2_t v134 = vadd_f32(v128, v133);
    float32x2_t v135 = vsub_f32(v128, v133);
    float32x2_t v33 = vadd_f32(v26, v32);
    float32x2_t v51 = vadd_f32(v44, v50);
    float32x2_t v69 = vadd_f32(v62, v68);
    float32x2_t v87 = vadd_f32(v80, v86);
    float32x2_t v105 = vadd_f32(v98, v104);
    float32x2_t v123 = vadd_f32(v116, v122);
    float32x2_t v141 = vadd_f32(v134, v140);
    float32x2_t v226 = vadd_f32(v44, v134);
    float32x2_t v227 = vsub_f32(v44, v134);
    float32x2_t v228 = vadd_f32(v98, v80);
    float32x2_t v229 = vsub_f32(v98, v80);
    float32x2_t v230 = vadd_f32(v62, v116);
    float32x2_t v231 = vsub_f32(v62, v116);
    float32x2_t v310 = vadd_f32(v45, v135);
    float32x2_t v311 = vsub_f32(v45, v135);
    float32x2_t v312 = vadd_f32(v99, v81);
    float32x2_t v313 = vsub_f32(v99, v81);
    float32x2_t v314 = vadd_f32(v63, v117);
    float32x2_t v315 = vsub_f32(v63, v117);
    float32x2_t v142 = vadd_f32(v51, v141);
    float32x2_t v143 = vsub_f32(v51, v141);
    float32x2_t v144 = vadd_f32(v105, v87);
    float32x2_t v145 = vsub_f32(v105, v87);
    float32x2_t v146 = vadd_f32(v69, v123);
    float32x2_t v147 = vsub_f32(v69, v123);
    float32x2_t v232 = vadd_f32(v226, v228);
    float32x2_t v235 = vsub_f32(v226, v228);
    float32x2_t v236 = vsub_f32(v228, v230);
    float32x2_t v237 = vsub_f32(v230, v226);
    float32x2_t v238 = vadd_f32(v227, v229);
    float32x2_t v240 = vsub_f32(v227, v229);
    float32x2_t v241 = vsub_f32(v229, v231);
    float32x2_t v242 = vsub_f32(v231, v227);
    float32x2_t v316 = vadd_f32(v310, v312);
    float32x2_t v319 = vsub_f32(v310, v312);
    float32x2_t v320 = vsub_f32(v312, v314);
    float32x2_t v321 = vsub_f32(v314, v310);
    float32x2_t v322 = vadd_f32(v311, v313);
    float32x2_t v324 = vsub_f32(v311, v313);
    float32x2_t v325 = vsub_f32(v313, v315);
    float32x2_t v326 = vsub_f32(v315, v311);
    float32x2_t v148 = vadd_f32(v142, v144);
    float32x2_t v151 = vsub_f32(v142, v144);
    float32x2_t v152 = vsub_f32(v144, v146);
    float32x2_t v153 = vsub_f32(v146, v142);
    float32x2_t v154 = vadd_f32(v143, v145);
    float32x2_t v156 = vsub_f32(v143, v145);
    float32x2_t v157 = vsub_f32(v145, v147);
    float32x2_t v158 = vsub_f32(v147, v143);
    float32x2_t v233 = vadd_f32(v232, v230);
    float32x2_t v239 = vadd_f32(v238, v231);
    float32x2_t v254 = vmul_f32(v235, v253);
    float32x2_t v258 = vmul_f32(v236, v257);
    float32x2_t v262 = vmul_f32(v237, v261);
    float32x2_t v275 = vrev64_f32(v240);
    float32x2_t v282 = vrev64_f32(v241);
    float32x2_t v289 = vrev64_f32(v242);
    float32x2_t v317 = vadd_f32(v316, v314);
    float32x2_t v323 = vadd_f32(v322, v315);
    float32x2_t v346 = vrev64_f32(v319);
    float32x2_t v353 = vrev64_f32(v320);
    float32x2_t v360 = vrev64_f32(v321);
    float32x2_t v369 = vmul_f32(v324, v368);
    float32x2_t v373 = vmul_f32(v325, v372);
    float32x2_t v377 = vmul_f32(v326, v376);
    float32x2_t v149 = vadd_f32(v148, v146);
    float32x2_t v155 = vadd_f32(v154, v147);
    float32x2_t v170 = vmul_f32(v151, v169);
    float32x2_t v174 = vmul_f32(v152, v173);
    float32x2_t v178 = vmul_f32(v153, v177);
    float32x2_t v191 = vrev64_f32(v156);
    float32x2_t v198 = vrev64_f32(v157);
    float32x2_t v205 = vrev64_f32(v158);
    float32x2_t v234 = vadd_f32(v233, v26);
    float32x2_t v250 = vmul_f32(v233, v249);
    float32x2_t v268 = vrev64_f32(v239);
    float32x2_t v276 = vmul_f32(v275, v274);
    float32x2_t v283 = vmul_f32(v282, v281);
    float32x2_t v290 = vmul_f32(v289, v288);
    float32x2_t v318 = vadd_f32(v317, v27);
    float32x2_t v339 = vrev64_f32(v317);
    float32x2_t v347 = vmul_f32(v346, v345);
    float32x2_t v354 = vmul_f32(v353, v352);
    float32x2_t v361 = vmul_f32(v360, v359);
    float32x2_t v365 = vmul_f32(v323, v364);
    float32x2_t v150 = vadd_f32(v149, v33);
    float32x2_t v166 = vmul_f32(v149, v165);
    float32x2_t v184 = vrev64_f32(v155);
    float32x2_t v192 = vmul_f32(v191, v190);
    float32x2_t v199 = vmul_f32(v198, v197);
    float32x2_t v206 = vmul_f32(v205, v204);
    float32x2_t v246 = vmul_f32(v234, v245);
    float32x2_t v269 = vmul_f32(v268, v267);
    float32x2_t v332 = vrev64_f32(v318);
    float32x2_t v340 = vmul_f32(v339, v338);
    float32x2_t v385 = vadd_f32(v365, v369);
    float32x2_t v387 = vsub_f32(v365, v369);
    float32x2_t v389 = vsub_f32(v365, v373);
    float32x2_t v185 = vmul_f32(v184, v183);
    float32x2_t v207 = vadd_f32(v150, v166);
    float32x2_t v291 = vadd_f32(v246, v250);
    float32x2_t v298 = vadd_f32(v269, v276);
    float32x2_t v300 = vsub_f32(v269, v276);
    float32x2_t v302 = vsub_f32(v269, v283);
    float32x2_t v333 = vmul_f32(v332, v331);
    float32x2_t v386 = vadd_f32(v385, v373);
    float32x2_t v388 = vsub_f32(v387, v377);
    float32x2_t v390 = vadd_f32(v389, v377);
    float32x2_t v397 = vadd_f32(v150, v246);
    int16x4_t v402 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v150, 15), (int32x2_t){0, 0}));
    float32x2_t v208 = vadd_f32(v207, v170);
    float32x2_t v210 = vsub_f32(v207, v170);
    float32x2_t v212 = vsub_f32(v207, v174);
    float32x2_t v214 = vadd_f32(v185, v192);
    float32x2_t v216 = vsub_f32(v185, v192);
    float32x2_t v218 = vsub_f32(v185, v199);
    float32x2_t v292 = vadd_f32(v291, v254);
    float32x2_t v294 = vsub_f32(v291, v254);
    float32x2_t v296 = vsub_f32(v291, v258);
    float32x2_t v299 = vadd_f32(v298, v283);
    float32x2_t v301 = vsub_f32(v300, v290);
    float32x2_t v303 = vadd_f32(v302, v290);
    float32x2_t v378 = vadd_f32(v333, v340);
    float32x2_t v398 = vadd_f32(v397, v333);
    float32x2_t v399 = vsub_f32(v397, v333);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v402), 0);
    float32x2_t v209 = vadd_f32(v208, v174);
    float32x2_t v211 = vsub_f32(v210, v178);
    float32x2_t v213 = vadd_f32(v212, v178);
    float32x2_t v215 = vadd_f32(v214, v199);
    float32x2_t v217 = vsub_f32(v216, v206);
    float32x2_t v219 = vadd_f32(v218, v206);
    float32x2_t v293 = vadd_f32(v292, v258);
    float32x2_t v295 = vsub_f32(v294, v262);
    float32x2_t v297 = vadd_f32(v296, v262);
    float32x2_t v379 = vadd_f32(v378, v347);
    float32x2_t v381 = vsub_f32(v378, v347);
    float32x2_t v383 = vsub_f32(v378, v354);
    int16x4_t v408 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v399, 15), (int32x2_t){0, 0}));
    int16x4_t v414 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v398, 15), (int32x2_t){0, 0}));
    float32x2_t v220 = vadd_f32(v209, v215);
    float32x2_t v221 = vsub_f32(v209, v215);
    float32x2_t v222 = vadd_f32(v211, v217);
    float32x2_t v223 = vsub_f32(v211, v217);
    float32x2_t v224 = vadd_f32(v213, v219);
    float32x2_t v225 = vsub_f32(v213, v219);
    float32x2_t v304 = vadd_f32(v293, v299);
    float32x2_t v305 = vsub_f32(v293, v299);
    float32x2_t v306 = vadd_f32(v295, v301);
    float32x2_t v307 = vsub_f32(v295, v301);
    float32x2_t v308 = vadd_f32(v297, v303);
    float32x2_t v309 = vsub_f32(v297, v303);
    float32x2_t v380 = vadd_f32(v379, v354);
    float32x2_t v382 = vsub_f32(v381, v361);
    float32x2_t v384 = vadd_f32(v383, v361);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v408), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v414), 0);
    float32x2_t v391 = vadd_f32(v380, v386);
    float32x2_t v392 = vsub_f32(v380, v386);
    float32x2_t v393 = vadd_f32(v382, v388);
    float32x2_t v394 = vsub_f32(v382, v388);
    float32x2_t v395 = vadd_f32(v384, v390);
    float32x2_t v396 = vsub_f32(v384, v390);
    float32x2_t v418 = vadd_f32(v221, v305);
    int16x4_t v423 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v221, 15), (int32x2_t){0, 0}));
    float32x2_t v439 = vadd_f32(v223, v307);
    int16x4_t v444 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v223, 15), (int32x2_t){0, 0}));
    float32x2_t v460 = vadd_f32(v224, v308);
    int16x4_t v465 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v224, 15), (int32x2_t){0, 0}));
    float32x2_t v481 = vadd_f32(v225, v309);
    int16x4_t v486 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v225, 15), (int32x2_t){0, 0}));
    float32x2_t v502 = vadd_f32(v222, v306);
    int16x4_t v507 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v222, 15), (int32x2_t){0, 0}));
    float32x2_t v523 = vadd_f32(v220, v304);
    int16x4_t v528 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v220, 15), (int32x2_t){0, 0}));
    float32x2_t v419 = vadd_f32(v418, v392);
    float32x2_t v420 = vsub_f32(v418, v392);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v423), 0);
    float32x2_t v440 = vadd_f32(v439, v394);
    float32x2_t v441 = vsub_f32(v439, v394);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v444), 0);
    float32x2_t v461 = vadd_f32(v460, v395);
    float32x2_t v462 = vsub_f32(v460, v395);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v465), 0);
    float32x2_t v482 = vadd_f32(v481, v396);
    float32x2_t v483 = vsub_f32(v481, v396);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v486), 0);
    float32x2_t v503 = vadd_f32(v502, v393);
    float32x2_t v504 = vsub_f32(v502, v393);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v507), 0);
    float32x2_t v524 = vadd_f32(v523, v391);
    float32x2_t v525 = vsub_f32(v523, v391);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v528), 0);
    int16x4_t v429 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v420, 15), (int32x2_t){0, 0}));
    int16x4_t v435 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v419, 15), (int32x2_t){0, 0}));
    int16x4_t v450 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v441, 15), (int32x2_t){0, 0}));
    int16x4_t v456 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v440, 15), (int32x2_t){0, 0}));
    int16x4_t v471 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v462, 15), (int32x2_t){0, 0}));
    int16x4_t v477 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v461, 15), (int32x2_t){0, 0}));
    int16x4_t v492 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v483, 15), (int32x2_t){0, 0}));
    int16x4_t v498 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v482, 15), (int32x2_t){0, 0}));
    int16x4_t v513 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v504, 15), (int32x2_t){0, 0}));
    int16x4_t v519 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v503, 15), (int32x2_t){0, 0}));
    int16x4_t v534 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v525, 15), (int32x2_t){0, 0}));
    int16x4_t v540 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v524, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v429), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v435), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v450), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v456), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v471), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v477), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v492), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v498), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v513), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v519), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v534), 0);
    v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v540), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu21(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v209 = -1.1666666666666665e+00F;
    float v214 = 7.9015646852540022e-01F;
    float v219 = 5.5854267289647742e-02F;
    float v224 = 7.3430220123575241e-01F;
    float v229 = -4.4095855184409838e-01F;
    float v236 = -3.4087293062393137e-01F;
    float v243 = 5.3396936033772524e-01F;
    float v250 = -8.7484229096165667e-01F;
    float v293 = -1.4999999999999998e+00F;
    float v298 = 1.7499999999999996e+00F;
    float v303 = -1.1852347027881001e+00F;
    float v308 = -8.3781400934471603e-02F;
    float v313 = -1.1014533018536286e+00F;
    float v318 = 6.6143782776614746e-01F;
    float v325 = 5.1130939593589697e-01F;
    float v332 = -8.0095404050658769e-01F;
    float v339 = 1.3122634364424848e+00F;
    float v382 = -8.6602540378443871e-01F;
    float v389 = 1.0103629710818451e+00F;
    float v396 = -6.8429557470759583e-01F;
    float v403 = -4.8371214382601155e-02F;
    float v410 = -6.3592436032499466e-01F;
    float v417 = -3.8188130791298663e-01F;
    float v422 = -2.9520461738277515e-01F;
    float v427 = 4.6243103089499693e-01F;
    float v432 = -7.5763564827777208e-01F;
    const float32x2_t *v786 = &v5[v0];
    int32_t *v904 = &v6[v2];
    int64_t v19 = v0 * 7;
    int64_t v26 = v0 * 14;
    int64_t v43 = v0 * 10;
    int64_t v50 = v0 * 17;
    int64_t v59 = v0 * 3;
    int64_t v67 = v0 * 13;
    int64_t v74 = v0 * 20;
    int64_t v83 = v0 * 6;
    int64_t v91 = v0 * 16;
    int64_t v98 = v0 * 2;
    int64_t v107 = v0 * 9;
    int64_t v115 = v0 * 19;
    int64_t v122 = v0 * 5;
    int64_t v131 = v0 * 12;
    int64_t v146 = v0 * 8;
    int64_t v155 = v0 * 15;
    int64_t v163 = v0 * 4;
    int64_t v170 = v0 * 11;
    int64_t v179 = v0 * 18;
    float v232 = v4 * v229;
    float v239 = v4 * v236;
    float v246 = v4 * v243;
    float v253 = v4 * v250;
    float v321 = v4 * v318;
    float v328 = v4 * v325;
    float v335 = v4 * v332;
    float v342 = v4 * v339;
    float v385 = v4 * v382;
    float v392 = v4 * v389;
    float v399 = v4 * v396;
    float v406 = v4 * v403;
    float v413 = v4 * v410;
    int64_t v467 = v2 * 7;
    int64_t v475 = v2 * 14;
    int64_t v486 = v2 * 15;
    int64_t v502 = v2 * 8;
    int64_t v513 = v2 * 9;
    int64_t v521 = v2 * 16;
    int64_t v529 = v2 * 2;
    int64_t v540 = v2 * 3;
    int64_t v548 = v2 * 10;
    int64_t v556 = v2 * 17;
    int64_t v567 = v2 * 18;
    int64_t v575 = v2 * 4;
    int64_t v583 = v2 * 11;
    int64_t v594 = v2 * 12;
    int64_t v602 = v2 * 19;
    int64_t v610 = v2 * 5;
    int64_t v621 = v2 * 6;
    int64_t v629 = v2 * 13;
    int64_t v637 = v2 * 20;
    const float32x2_t *v669 = &v5[0];
    svfloat32_t v835 = svdup_n_f32(v209);
    svfloat32_t v836 = svdup_n_f32(v214);
    svfloat32_t v837 = svdup_n_f32(v219);
    svfloat32_t v838 = svdup_n_f32(v224);
    svfloat32_t v843 = svdup_n_f32(v293);
    svfloat32_t v844 = svdup_n_f32(v298);
    svfloat32_t v845 = svdup_n_f32(v303);
    svfloat32_t v846 = svdup_n_f32(v308);
    svfloat32_t v847 = svdup_n_f32(v313);
    svfloat32_t v857 = svdup_n_f32(v417);
    svfloat32_t v858 = svdup_n_f32(v422);
    svfloat32_t v859 = svdup_n_f32(v427);
    svfloat32_t v860 = svdup_n_f32(v432);
    int32_t *v868 = &v6[0];
    svfloat32_t v1082 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v786)[0]));
    const float32x2_t *v650 = &v5[v19];
    const float32x2_t *v659 = &v5[v26];
    const float32x2_t *v678 = &v5[v43];
    const float32x2_t *v687 = &v5[v50];
    const float32x2_t *v696 = &v5[v59];
    const float32x2_t *v705 = &v5[v67];
    const float32x2_t *v714 = &v5[v74];
    const float32x2_t *v723 = &v5[v83];
    const float32x2_t *v732 = &v5[v91];
    const float32x2_t *v741 = &v5[v98];
    const float32x2_t *v750 = &v5[v107];
    const float32x2_t *v759 = &v5[v115];
    const float32x2_t *v768 = &v5[v122];
    const float32x2_t *v777 = &v5[v131];
    const float32x2_t *v795 = &v5[v146];
    const float32x2_t *v804 = &v5[v155];
    const float32x2_t *v813 = &v5[v163];
    const float32x2_t *v822 = &v5[v170];
    const float32x2_t *v831 = &v5[v179];
    svfloat32_t v839 = svdup_n_f32(v232);
    svfloat32_t v840 = svdup_n_f32(v239);
    svfloat32_t v841 = svdup_n_f32(v246);
    svfloat32_t v842 = svdup_n_f32(v253);
    svfloat32_t v848 = svdup_n_f32(v321);
    svfloat32_t v849 = svdup_n_f32(v328);
    svfloat32_t v850 = svdup_n_f32(v335);
    svfloat32_t v851 = svdup_n_f32(v342);
    svfloat32_t v852 = svdup_n_f32(v385);
    svfloat32_t v853 = svdup_n_f32(v392);
    svfloat32_t v854 = svdup_n_f32(v399);
    svfloat32_t v855 = svdup_n_f32(v406);
    svfloat32_t v856 = svdup_n_f32(v413);
    int32_t *v877 = &v6[v467];
    int32_t *v886 = &v6[v475];
    int32_t *v895 = &v6[v486];
    int32_t *v913 = &v6[v502];
    int32_t *v922 = &v6[v513];
    int32_t *v931 = &v6[v521];
    int32_t *v940 = &v6[v529];
    int32_t *v949 = &v6[v540];
    int32_t *v958 = &v6[v548];
    int32_t *v967 = &v6[v556];
    int32_t *v976 = &v6[v567];
    int32_t *v985 = &v6[v575];
    int32_t *v994 = &v6[v583];
    int32_t *v1003 = &v6[v594];
    int32_t *v1012 = &v6[v602];
    int32_t *v1021 = &v6[v610];
    int32_t *v1030 = &v6[v621];
    int32_t *v1039 = &v6[v629];
    int32_t *v1048 = &v6[v637];
    svfloat32_t v1056 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v669)[0]));
    svfloat32_t v1052 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v650)[0]));
    svfloat32_t v1054 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v659)[0]));
    svfloat32_t v1058 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v678)[0]));
    svfloat32_t v1060 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v687)[0]));
    svfloat32_t v1062 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v696)[0]));
    svfloat32_t v1064 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v705)[0]));
    svfloat32_t v1066 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v714)[0]));
    svfloat32_t v1068 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v723)[0]));
    svfloat32_t v1070 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v732)[0]));
    svfloat32_t v1072 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v741)[0]));
    svfloat32_t v1074 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v750)[0]));
    svfloat32_t v1076 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v759)[0]));
    svfloat32_t v1078 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v768)[0]));
    svfloat32_t v1080 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v777)[0]));
    svfloat32_t v1084 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v795)[0]));
    svfloat32_t v1086 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v804)[0]));
    svfloat32_t v1088 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v813)[0]));
    svfloat32_t v1090 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v822)[0]));
    svfloat32_t v1092 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v831)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v1052, v1054);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v1052, v1054);
    svfloat32_t v56 = svadd_f32_x(svptrue_b32(), v1058, v1060);
    svfloat32_t v57 = svsub_f32_x(svptrue_b32(), v1058, v1060);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v1064, v1066);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v1064, v1066);
    svfloat32_t v104 = svadd_f32_x(svptrue_b32(), v1070, v1072);
    svfloat32_t v105 = svsub_f32_x(svptrue_b32(), v1070, v1072);
    svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v1076, v1078);
    svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v1076, v1078);
    svfloat32_t v152 = svadd_f32_x(svptrue_b32(), v1082, v1084);
    svfloat32_t v153 = svsub_f32_x(svptrue_b32(), v1082, v1084);
    svfloat32_t v176 = svadd_f32_x(svptrue_b32(), v1088, v1090);
    svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v1088, v1090);
    svfloat32_t v41 = svadd_f32_x(svptrue_b32(), v32, v1056);
    svfloat32_t v65 = svadd_f32_x(svptrue_b32(), v56, v1062);
    svfloat32_t v89 = svadd_f32_x(svptrue_b32(), v80, v1068);
    svfloat32_t v113 = svadd_f32_x(svptrue_b32(), v104, v1074);
    svfloat32_t v137 = svadd_f32_x(svptrue_b32(), v128, v1080);
    svfloat32_t v161 = svadd_f32_x(svptrue_b32(), v152, v1086);
    svfloat32_t v185 = svadd_f32_x(svptrue_b32(), v176, v1092);
    svfloat32_t v275 = svadd_f32_x(svptrue_b32(), v56, v176);
    svfloat32_t v276 = svsub_f32_x(svptrue_b32(), v56, v176);
    svfloat32_t v277 = svadd_f32_x(svptrue_b32(), v128, v104);
    svfloat32_t v278 = svsub_f32_x(svptrue_b32(), v128, v104);
    svfloat32_t v279 = svadd_f32_x(svptrue_b32(), v80, v152);
    svfloat32_t v280 = svsub_f32_x(svptrue_b32(), v80, v152);
    svfloat32_t v364 = svadd_f32_x(svptrue_b32(), v57, v177);
    svfloat32_t v365 = svsub_f32_x(svptrue_b32(), v57, v177);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v129, v105);
    svfloat32_t v367 = svsub_f32_x(svptrue_b32(), v129, v105);
    svfloat32_t v368 = svadd_f32_x(svptrue_b32(), v81, v153);
    svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v81, v153);
    svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v65, v185);
    svfloat32_t v187 = svsub_f32_x(svptrue_b32(), v65, v185);
    svfloat32_t v188 = svadd_f32_x(svptrue_b32(), v137, v113);
    svfloat32_t v189 = svsub_f32_x(svptrue_b32(), v137, v113);
    svfloat32_t v190 = svadd_f32_x(svptrue_b32(), v89, v161);
    svfloat32_t v191 = svsub_f32_x(svptrue_b32(), v89, v161);
    svfloat32_t v281 = svadd_f32_x(svptrue_b32(), v275, v277);
    svfloat32_t v284 = svsub_f32_x(svptrue_b32(), v275, v277);
    svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t v286 = svsub_f32_x(svptrue_b32(), v279, v275);
    svfloat32_t v287 = svadd_f32_x(svptrue_b32(), v276, v278);
    svfloat32_t v289 = svsub_f32_x(svptrue_b32(), v276, v278);
    svfloat32_t v290 = svsub_f32_x(svptrue_b32(), v278, v280);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v280, v276);
    svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v364, v366);
    svfloat32_t v373 = svsub_f32_x(svptrue_b32(), v364, v366);
    svfloat32_t v374 = svsub_f32_x(svptrue_b32(), v366, v368);
    svfloat32_t v375 = svsub_f32_x(svptrue_b32(), v368, v364);
    svfloat32_t v376 = svadd_f32_x(svptrue_b32(), v365, v367);
    svfloat32_t v378 = svsub_f32_x(svptrue_b32(), v365, v367);
    svfloat32_t v379 = svsub_f32_x(svptrue_b32(), v367, v369);
    svfloat32_t v380 = svsub_f32_x(svptrue_b32(), v369, v365);
    svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v186, v188);
    svfloat32_t v195 = svsub_f32_x(svptrue_b32(), v186, v188);
    svfloat32_t v196 = svsub_f32_x(svptrue_b32(), v188, v190);
    svfloat32_t v197 = svsub_f32_x(svptrue_b32(), v190, v186);
    svfloat32_t v198 = svadd_f32_x(svptrue_b32(), v187, v189);
    svfloat32_t v200 = svsub_f32_x(svptrue_b32(), v187, v189);
    svfloat32_t v201 = svsub_f32_x(svptrue_b32(), v189, v191);
    svfloat32_t v202 = svsub_f32_x(svptrue_b32(), v191, v187);
    svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v281, v279);
    svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v287, v280);
    svfloat32_t zero330 = svdup_n_f32(0);
    svfloat32_t v330 = svcmla_f32_x(pred_full, zero330, v849, v289, 90);
    svfloat32_t zero337 = svdup_n_f32(0);
    svfloat32_t v337 = svcmla_f32_x(pred_full, zero337, v850, v290, 90);
    svfloat32_t zero344 = svdup_n_f32(0);
    svfloat32_t v344 = svcmla_f32_x(pred_full, zero344, v851, v291, 90);
    svfloat32_t v371 = svadd_f32_x(svptrue_b32(), v370, v368);
    svfloat32_t v377 = svadd_f32_x(svptrue_b32(), v376, v369);
    svfloat32_t zero401 = svdup_n_f32(0);
    svfloat32_t v401 = svcmla_f32_x(pred_full, zero401, v854, v373, 90);
    svfloat32_t zero408 = svdup_n_f32(0);
    svfloat32_t v408 = svcmla_f32_x(pred_full, zero408, v855, v374, 90);
    svfloat32_t zero415 = svdup_n_f32(0);
    svfloat32_t v415 = svcmla_f32_x(pred_full, zero415, v856, v375, 90);
    svfloat32_t v425 = svmul_f32_x(svptrue_b32(), v378, v858);
    svfloat32_t v430 = svmul_f32_x(svptrue_b32(), v379, v859);
    svfloat32_t v193 = svadd_f32_x(svptrue_b32(), v192, v190);
    svfloat32_t v199 = svadd_f32_x(svptrue_b32(), v198, v191);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 = svcmla_f32_x(pred_full, zero241, v840, v200, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 = svcmla_f32_x(pred_full, zero248, v841, v201, 90);
    svfloat32_t zero255 = svdup_n_f32(0);
    svfloat32_t v255 = svcmla_f32_x(pred_full, zero255, v842, v202, 90);
    svfloat32_t v283 = svadd_f32_x(svptrue_b32(), v282, v32);
    svfloat32_t v301 = svmul_f32_x(svptrue_b32(), v282, v844);
    svfloat32_t zero323 = svdup_n_f32(0);
    svfloat32_t v323 = svcmla_f32_x(pred_full, zero323, v848, v288, 90);
    svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v371, v33);
    svfloat32_t v194 = svadd_f32_x(svptrue_b32(), v193, v41);
    svfloat32_t zero234 = svdup_n_f32(0);
    svfloat32_t v234 = svcmla_f32_x(pred_full, zero234, v839, v199, 90);
    svfloat32_t v352 = svadd_f32_x(svptrue_b32(), v323, v330);
    svfloat32_t v354 = svsub_f32_x(svptrue_b32(), v323, v330);
    svfloat32_t v356 = svsub_f32_x(svptrue_b32(), v323, v337);
    svfloat32_t zero387 = svdup_n_f32(0);
    svfloat32_t v387 = svcmla_f32_x(pred_full, zero387, v852, v372, 90);
    svfloat32_t v443 = svmla_f32_x(pred_full, v425, v377, v857);
    svfloat32_t v445 = svnmls_f32_x(pred_full, v425, v377, v857);
    svfloat32_t v447 = svnmls_f32_x(pred_full, v430, v377, v857);
    svfloat32_t v256 = svmla_f32_x(pred_full, v194, v193, v835);
    svfloat32_t v263 = svadd_f32_x(svptrue_b32(), v234, v241);
    svfloat32_t v265 = svsub_f32_x(svptrue_b32(), v234, v241);
    svfloat32_t v267 = svsub_f32_x(svptrue_b32(), v234, v248);
    svfloat32_t v345 = svmla_f32_x(pred_full, v301, v283, v843);
    svfloat32_t v353 = svadd_f32_x(svptrue_b32(), v352, v337);
    svfloat32_t v355 = svsub_f32_x(svptrue_b32(), v354, v344);
    svfloat32_t v357 = svadd_f32_x(svptrue_b32(), v356, v344);
    svfloat32_t v436 = svcmla_f32_x(pred_full, v387, v853, v371, 90);
    svfloat32_t v444 = svmla_f32_x(pred_full, v443, v379, v859);
    svfloat32_t v446 = svmls_f32_x(pred_full, v445, v380, v860);
    svfloat32_t v448 = svmla_f32_x(pred_full, v447, v380, v860);
    svfloat32_t v455 = svmla_f32_x(pred_full, v194, v283, v843);
    svint16_t v460 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v194, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v257 = svmla_f32_x(pred_full, v256, v195, v836);
    svfloat32_t v259 = svmls_f32_x(pred_full, v256, v195, v836);
    svfloat32_t v261 = svmls_f32_x(pred_full, v256, v196, v837);
    svfloat32_t v264 = svadd_f32_x(svptrue_b32(), v263, v248);
    svfloat32_t v266 = svsub_f32_x(svptrue_b32(), v265, v255);
    svfloat32_t v268 = svadd_f32_x(svptrue_b32(), v267, v255);
    svfloat32_t v346 = svmla_f32_x(pred_full, v345, v284, v845);
    svfloat32_t v348 = svmls_f32_x(pred_full, v345, v284, v845);
    svfloat32_t v350 = svmls_f32_x(pred_full, v345, v285, v846);
    svfloat32_t v437 = svadd_f32_x(svptrue_b32(), v436, v401);
    svfloat32_t v439 = svsub_f32_x(svptrue_b32(), v436, v401);
    svfloat32_t v441 = svsub_f32_x(svptrue_b32(), v436, v408);
    svfloat32_t v456 = svadd_f32_x(svptrue_b32(), v455, v387);
    svfloat32_t v457 = svsub_f32_x(svptrue_b32(), v455, v387);
    svst1w_u64(pred_full, (unsigned *)(v868), svreinterpret_u64_s16(v460));
    svfloat32_t v258 = svmla_f32_x(pred_full, v257, v196, v837);
    svfloat32_t v260 = svmls_f32_x(pred_full, v259, v197, v838);
    svfloat32_t v262 = svmla_f32_x(pred_full, v261, v197, v838);
    svfloat32_t v347 = svmla_f32_x(pred_full, v346, v285, v846);
    svfloat32_t v349 = svmls_f32_x(pred_full, v348, v286, v847);
    svfloat32_t v351 = svmla_f32_x(pred_full, v350, v286, v847);
    svfloat32_t v438 = svadd_f32_x(svptrue_b32(), v437, v408);
    svfloat32_t v440 = svsub_f32_x(svptrue_b32(), v439, v415);
    svfloat32_t v442 = svadd_f32_x(svptrue_b32(), v441, v415);
    svint16_t v468 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v457, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v476 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v456, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v269 = svadd_f32_x(svptrue_b32(), v258, v264);
    svfloat32_t v270 = svsub_f32_x(svptrue_b32(), v258, v264);
    svfloat32_t v271 = svadd_f32_x(svptrue_b32(), v260, v266);
    svfloat32_t v272 = svsub_f32_x(svptrue_b32(), v260, v266);
    svfloat32_t v273 = svadd_f32_x(svptrue_b32(), v262, v268);
    svfloat32_t v274 = svsub_f32_x(svptrue_b32(), v262, v268);
    svfloat32_t v358 = svadd_f32_x(svptrue_b32(), v347, v353);
    svfloat32_t v359 = svsub_f32_x(svptrue_b32(), v347, v353);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v349, v355);
    svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v349, v355);
    svfloat32_t v362 = svadd_f32_x(svptrue_b32(), v351, v357);
    svfloat32_t v363 = svsub_f32_x(svptrue_b32(), v351, v357);
    svfloat32_t v449 = svadd_f32_x(svptrue_b32(), v438, v444);
    svfloat32_t v450 = svsub_f32_x(svptrue_b32(), v438, v444);
    svfloat32_t v451 = svadd_f32_x(svptrue_b32(), v440, v446);
    svfloat32_t v452 = svsub_f32_x(svptrue_b32(), v440, v446);
    svfloat32_t v453 = svadd_f32_x(svptrue_b32(), v442, v448);
    svfloat32_t v454 = svsub_f32_x(svptrue_b32(), v442, v448);
    svst1w_u64(pred_full, (unsigned *)(v877), svreinterpret_u64_s16(v468));
    svst1w_u64(pred_full, (unsigned *)(v886), svreinterpret_u64_s16(v476));
    svfloat32_t v482 = svadd_f32_x(svptrue_b32(), v270, v359);
    svint16_t v487 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v270, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v509 = svadd_f32_x(svptrue_b32(), v272, v361);
    svint16_t v514 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v272, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v536 = svadd_f32_x(svptrue_b32(), v273, v362);
    svint16_t v541 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v273, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v563 = svadd_f32_x(svptrue_b32(), v274, v363);
    svint16_t v568 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v274, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v590 = svadd_f32_x(svptrue_b32(), v271, v360);
    svint16_t v595 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v271, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v617 = svadd_f32_x(svptrue_b32(), v269, v358);
    svint16_t v622 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v269, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v483 = svadd_f32_x(svptrue_b32(), v482, v450);
    svfloat32_t v484 = svsub_f32_x(svptrue_b32(), v482, v450);
    svfloat32_t v510 = svadd_f32_x(svptrue_b32(), v509, v452);
    svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v509, v452);
    svfloat32_t v537 = svadd_f32_x(svptrue_b32(), v536, v453);
    svfloat32_t v538 = svsub_f32_x(svptrue_b32(), v536, v453);
    svfloat32_t v564 = svadd_f32_x(svptrue_b32(), v563, v454);
    svfloat32_t v565 = svsub_f32_x(svptrue_b32(), v563, v454);
    svfloat32_t v591 = svadd_f32_x(svptrue_b32(), v590, v451);
    svfloat32_t v592 = svsub_f32_x(svptrue_b32(), v590, v451);
    svfloat32_t v618 = svadd_f32_x(svptrue_b32(), v617, v449);
    svfloat32_t v619 = svsub_f32_x(svptrue_b32(), v617, v449);
    svst1w_u64(pred_full, (unsigned *)(v895), svreinterpret_u64_s16(v487));
    svst1w_u64(pred_full, (unsigned *)(v922), svreinterpret_u64_s16(v514));
    svst1w_u64(pred_full, (unsigned *)(v949), svreinterpret_u64_s16(v541));
    svst1w_u64(pred_full, (unsigned *)(v976), svreinterpret_u64_s16(v568));
    svst1w_u64(pred_full, (unsigned *)(v1003), svreinterpret_u64_s16(v595));
    svst1w_u64(pred_full, (unsigned *)(v1030), svreinterpret_u64_s16(v622));
    svint16_t v495 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v484, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v503 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v483, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v522 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v511, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v530 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v510, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v549 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v538, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v557 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v537, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v576 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v565, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v584 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v564, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v603 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v592, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v611 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v591, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v630 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v619, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v638 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v618, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v904), svreinterpret_u64_s16(v495));
    svst1w_u64(pred_full, (unsigned *)(v913), svreinterpret_u64_s16(v503));
    svst1w_u64(pred_full, (unsigned *)(v931), svreinterpret_u64_s16(v522));
    svst1w_u64(pred_full, (unsigned *)(v940), svreinterpret_u64_s16(v530));
    svst1w_u64(pred_full, (unsigned *)(v958), svreinterpret_u64_s16(v549));
    svst1w_u64(pred_full, (unsigned *)(v967), svreinterpret_u64_s16(v557));
    svst1w_u64(pred_full, (unsigned *)(v985), svreinterpret_u64_s16(v576));
    svst1w_u64(pred_full, (unsigned *)(v994), svreinterpret_u64_s16(v584));
    svst1w_u64(pred_full, (unsigned *)(v1012), svreinterpret_u64_s16(v603));
    svst1w_u64(pred_full, (unsigned *)(v1021), svreinterpret_u64_s16(v611));
    svst1w_u64(pred_full, (unsigned *)(v1039), svreinterpret_u64_s16(v630));
    svst1w_u64(pred_full, (unsigned *)(v1048), svreinterpret_u64_s16(v638));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu22(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v97 = v5[istride];
    float v388 = 1.1000000000000001e+00F;
    float v391 = 3.3166247903554003e-01F;
    float v392 = -3.3166247903554003e-01F;
    float v399 = 5.1541501300188641e-01F;
    float v403 = 9.4125353283118118e-01F;
    float v407 = 1.4143537075597825e+00F;
    float v411 = 8.5949297361449750e-01F;
    float v415 = 4.2314838273285138e-02F;
    float v419 = 3.8639279888589606e-01F;
    float v423 = 5.1254589567200015e-01F;
    float v427 = 1.0702757469471715e+00F;
    float v431 = 5.5486073394528512e-01F;
    float v434 = 1.2412944743900585e+00F;
    float v435 = -1.2412944743900585e+00F;
    float v441 = 2.0897833842005756e-01F;
    float v442 = -2.0897833842005756e-01F;
    float v448 = 3.7415717312460811e-01F;
    float v449 = -3.7415717312460811e-01F;
    float v455 = 4.9929922194110327e-02F;
    float v456 = -4.9929922194110327e-02F;
    float v462 = 6.5815896284539266e-01F;
    float v463 = -6.5815896284539266e-01F;
    float v469 = 6.3306543373877577e-01F;
    float v470 = -6.3306543373877577e-01F;
    float v476 = 1.0822460581641109e+00F;
    float v477 = -1.0822460581641109e+00F;
    float v483 = 8.1720737907134022e-01F;
    float v484 = -8.1720737907134022e-01F;
    float v490 = 4.2408709531871824e-01F;
    float v491 = -4.2408709531871824e-01F;
    float32x2_t v493 = (float32x2_t){v4, v4};
    float32x2_t v20 = v5[0];
    float32x2_t v389 = (float32x2_t){v388, v388};
    float32x2_t v393 = (float32x2_t){v391, v392};
    float32x2_t v400 = (float32x2_t){v399, v399};
    float32x2_t v404 = (float32x2_t){v403, v403};
    float32x2_t v408 = (float32x2_t){v407, v407};
    float32x2_t v412 = (float32x2_t){v411, v411};
    float32x2_t v416 = (float32x2_t){v415, v415};
    float32x2_t v420 = (float32x2_t){v419, v419};
    float32x2_t v424 = (float32x2_t){v423, v423};
    float32x2_t v428 = (float32x2_t){v427, v427};
    float32x2_t v432 = (float32x2_t){v431, v431};
    float32x2_t v436 = (float32x2_t){v434, v435};
    float32x2_t v443 = (float32x2_t){v441, v442};
    float32x2_t v450 = (float32x2_t){v448, v449};
    float32x2_t v457 = (float32x2_t){v455, v456};
    float32x2_t v464 = (float32x2_t){v462, v463};
    float32x2_t v471 = (float32x2_t){v469, v470};
    float32x2_t v478 = (float32x2_t){v476, v477};
    float32x2_t v485 = (float32x2_t){v483, v484};
    float32x2_t v492 = (float32x2_t){v490, v491};
    float32x2_t v25 = v5[istride * 11];
    float32x2_t v32 = v5[istride * 2];
    float32x2_t v37 = v5[istride * 13];
    float32x2_t v44 = v5[istride * 4];
    float32x2_t v49 = v5[istride * 15];
    float32x2_t v56 = v5[istride * 6];
    float32x2_t v61 = v5[istride * 17];
    float32x2_t v68 = v5[istride * 8];
    float32x2_t v73 = v5[istride * 19];
    float32x2_t v80 = v5[istride * 10];
    float32x2_t v85 = v5[istride * 21];
    float32x2_t v92 = v5[istride * 12];
    float32x2_t v104 = v5[istride * 14];
    float32x2_t v109 = v5[istride * 3];
    float32x2_t v116 = v5[istride * 16];
    float32x2_t v121 = v5[istride * 5];
    float32x2_t v128 = v5[istride * 18];
    float32x2_t v133 = v5[istride * 7];
    float32x2_t v140 = v5[istride * 20];
    float32x2_t v145 = v5[istride * 9];
    float32x2_t v395 = vmul_f32(v493, v393);
    float32x2_t v438 = vmul_f32(v493, v436);
    float32x2_t v445 = vmul_f32(v493, v443);
    float32x2_t v452 = vmul_f32(v493, v450);
    float32x2_t v459 = vmul_f32(v493, v457);
    float32x2_t v466 = vmul_f32(v493, v464);
    float32x2_t v473 = vmul_f32(v493, v471);
    float32x2_t v480 = vmul_f32(v493, v478);
    float32x2_t v487 = vmul_f32(v493, v485);
    float32x2_t v494 = vmul_f32(v493, v492);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v50 = vadd_f32(v44, v49);
    float32x2_t v51 = vsub_f32(v44, v49);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v74 = vadd_f32(v68, v73);
    float32x2_t v75 = vsub_f32(v68, v73);
    float32x2_t v86 = vadd_f32(v80, v85);
    float32x2_t v87 = vsub_f32(v80, v85);
    float32x2_t v98 = vadd_f32(v92, v97);
    float32x2_t v99 = vsub_f32(v92, v97);
    float32x2_t v110 = vadd_f32(v104, v109);
    float32x2_t v111 = vsub_f32(v104, v109);
    float32x2_t v122 = vadd_f32(v116, v121);
    float32x2_t v123 = vsub_f32(v116, v121);
    float32x2_t v134 = vadd_f32(v128, v133);
    float32x2_t v135 = vsub_f32(v128, v133);
    float32x2_t v146 = vadd_f32(v140, v145);
    float32x2_t v147 = vsub_f32(v140, v145);
    float32x2_t v148 = vadd_f32(v38, v146);
    float32x2_t v149 = vadd_f32(v50, v134);
    float32x2_t v150 = vadd_f32(v62, v122);
    float32x2_t v151 = vadd_f32(v74, v110);
    float32x2_t v152 = vadd_f32(v86, v98);
    float32x2_t v153 = vsub_f32(v38, v146);
    float32x2_t v154 = vsub_f32(v50, v134);
    float32x2_t v155 = vsub_f32(v62, v122);
    float32x2_t v156 = vsub_f32(v74, v110);
    float32x2_t v157 = vsub_f32(v86, v98);
    float32x2_t v346 = vadd_f32(v39, v147);
    float32x2_t v347 = vadd_f32(v51, v135);
    float32x2_t v348 = vadd_f32(v63, v123);
    float32x2_t v349 = vadd_f32(v75, v111);
    float32x2_t v350 = vadd_f32(v87, v99);
    float32x2_t v351 = vsub_f32(v39, v147);
    float32x2_t v352 = vsub_f32(v51, v135);
    float32x2_t v353 = vsub_f32(v63, v123);
    float32x2_t v354 = vsub_f32(v75, v111);
    float32x2_t v355 = vsub_f32(v87, v99);
    float32x2_t v158 = vadd_f32(v148, v149);
    float32x2_t v159 = vadd_f32(v150, v152);
    float32x2_t v161 = vsub_f32(v154, v155);
    float32x2_t v162 = vadd_f32(v153, v157);
    float32x2_t v167 = vsub_f32(v149, v151);
    float32x2_t v168 = vsub_f32(v148, v151);
    float32x2_t v169 = vsub_f32(v149, v148);
    float32x2_t v170 = vsub_f32(v152, v151);
    float32x2_t v171 = vsub_f32(v150, v151);
    float32x2_t v172 = vsub_f32(v152, v150);
    float32x2_t v173 = vsub_f32(v149, v152);
    float32x2_t v174 = vsub_f32(v148, v150);
    float32x2_t v176 = vadd_f32(v154, v156);
    float32x2_t v177 = vsub_f32(v153, v156);
    float32x2_t v178 = vadd_f32(v153, v154);
    float32x2_t v179 = vsub_f32(v156, v157);
    float32x2_t v180 = vsub_f32(v155, v156);
    float32x2_t v181 = vsub_f32(v155, v157);
    float32x2_t v182 = vadd_f32(v154, v157);
    float32x2_t v183 = vsub_f32(v153, v155);
    float32x2_t v356 = vadd_f32(v346, v347);
    float32x2_t v357 = vadd_f32(v348, v350);
    float32x2_t v359 = vsub_f32(v352, v353);
    float32x2_t v360 = vadd_f32(v351, v355);
    float32x2_t v365 = vsub_f32(v347, v349);
    float32x2_t v366 = vsub_f32(v346, v349);
    float32x2_t v367 = vsub_f32(v347, v346);
    float32x2_t v368 = vsub_f32(v350, v349);
    float32x2_t v369 = vsub_f32(v348, v349);
    float32x2_t v370 = vsub_f32(v350, v348);
    float32x2_t v371 = vsub_f32(v347, v350);
    float32x2_t v372 = vsub_f32(v346, v348);
    float32x2_t v374 = vadd_f32(v352, v354);
    float32x2_t v375 = vsub_f32(v351, v354);
    float32x2_t v376 = vadd_f32(v351, v352);
    float32x2_t v377 = vsub_f32(v354, v355);
    float32x2_t v378 = vsub_f32(v353, v354);
    float32x2_t v379 = vsub_f32(v353, v355);
    float32x2_t v380 = vadd_f32(v352, v355);
    float32x2_t v381 = vsub_f32(v351, v353);
    float32x2_t v160 = vadd_f32(v151, v158);
    float32x2_t v165 = vsub_f32(v161, v162);
    float32x2_t v175 = vsub_f32(v159, v158);
    float32x2_t v184 = vadd_f32(v161, v162);
    float32x2_t v203 = vmul_f32(v167, v400);
    float32x2_t v207 = vmul_f32(v168, v404);
    float32x2_t v211 = vmul_f32(v169, v408);
    float32x2_t v215 = vmul_f32(v170, v412);
    float32x2_t v219 = vmul_f32(v171, v416);
    float32x2_t v223 = vmul_f32(v172, v420);
    float32x2_t v227 = vmul_f32(v173, v424);
    float32x2_t v231 = vmul_f32(v174, v428);
    float32x2_t v241 = vrev64_f32(v176);
    float32x2_t v248 = vrev64_f32(v177);
    float32x2_t v255 = vrev64_f32(v178);
    float32x2_t v262 = vrev64_f32(v179);
    float32x2_t v269 = vrev64_f32(v180);
    float32x2_t v276 = vrev64_f32(v181);
    float32x2_t v283 = vrev64_f32(v182);
    float32x2_t v290 = vrev64_f32(v183);
    float32x2_t v358 = vadd_f32(v349, v356);
    float32x2_t v363 = vsub_f32(v359, v360);
    float32x2_t v373 = vsub_f32(v357, v356);
    float32x2_t v382 = vadd_f32(v359, v360);
    float32x2_t v401 = vmul_f32(v365, v400);
    float32x2_t v405 = vmul_f32(v366, v404);
    float32x2_t v409 = vmul_f32(v367, v408);
    float32x2_t v413 = vmul_f32(v368, v412);
    float32x2_t v417 = vmul_f32(v369, v416);
    float32x2_t v421 = vmul_f32(v370, v420);
    float32x2_t v425 = vmul_f32(v371, v424);
    float32x2_t v429 = vmul_f32(v372, v428);
    float32x2_t v439 = vrev64_f32(v374);
    float32x2_t v446 = vrev64_f32(v375);
    float32x2_t v453 = vrev64_f32(v376);
    float32x2_t v460 = vrev64_f32(v377);
    float32x2_t v467 = vrev64_f32(v378);
    float32x2_t v474 = vrev64_f32(v379);
    float32x2_t v481 = vrev64_f32(v380);
    float32x2_t v488 = vrev64_f32(v381);
    float32x2_t v163 = vadd_f32(v160, v159);
    float32x2_t v166 = vsub_f32(v165, v156);
    float32x2_t v235 = vmul_f32(v175, v432);
    float32x2_t v242 = vmul_f32(v241, v438);
    float32x2_t v249 = vmul_f32(v248, v445);
    float32x2_t v256 = vmul_f32(v255, v452);
    float32x2_t v263 = vmul_f32(v262, v459);
    float32x2_t v270 = vmul_f32(v269, v466);
    float32x2_t v277 = vmul_f32(v276, v473);
    float32x2_t v284 = vmul_f32(v283, v480);
    float32x2_t v291 = vmul_f32(v290, v487);
    float32x2_t v297 = vrev64_f32(v184);
    float32x2_t v300 = vadd_f32(v203, v207);
    float32x2_t v301 = vadd_f32(v207, v211);
    float32x2_t v302 = vsub_f32(v203, v211);
    float32x2_t v303 = vadd_f32(v215, v219);
    float32x2_t v304 = vadd_f32(v219, v223);
    float32x2_t v305 = vsub_f32(v215, v223);
    float32x2_t v361 = vadd_f32(v358, v357);
    float32x2_t v364 = vsub_f32(v363, v354);
    float32x2_t v433 = vmul_f32(v373, v432);
    float32x2_t v440 = vmul_f32(v439, v438);
    float32x2_t v447 = vmul_f32(v446, v445);
    float32x2_t v454 = vmul_f32(v453, v452);
    float32x2_t v461 = vmul_f32(v460, v459);
    float32x2_t v468 = vmul_f32(v467, v466);
    float32x2_t v475 = vmul_f32(v474, v473);
    float32x2_t v482 = vmul_f32(v481, v480);
    float32x2_t v489 = vmul_f32(v488, v487);
    float32x2_t v495 = vrev64_f32(v382);
    float32x2_t v498 = vadd_f32(v401, v405);
    float32x2_t v499 = vadd_f32(v405, v409);
    float32x2_t v500 = vsub_f32(v401, v409);
    float32x2_t v501 = vadd_f32(v413, v417);
    float32x2_t v502 = vadd_f32(v417, v421);
    float32x2_t v503 = vsub_f32(v413, v421);
    float32x2_t v164 = vadd_f32(v26, v163);
    float32x2_t v192 = vmul_f32(v163, v389);
    float32x2_t v198 = vrev64_f32(v166);
    float32x2_t v298 = vmul_f32(v297, v494);
    float32x2_t v306 = vadd_f32(v231, v235);
    float32x2_t v307 = vadd_f32(v227, v235);
    float32x2_t v308 = vadd_f32(v249, v256);
    float32x2_t v309 = vsub_f32(v242, v256);
    float32x2_t v310 = vadd_f32(v270, v277);
    float32x2_t v311 = vsub_f32(v263, v277);
    float32x2_t v362 = vadd_f32(v27, v361);
    float32x2_t v390 = vmul_f32(v361, v389);
    float32x2_t v396 = vrev64_f32(v364);
    float32x2_t v496 = vmul_f32(v495, v494);
    float32x2_t v504 = vadd_f32(v429, v433);
    float32x2_t v505 = vadd_f32(v425, v433);
    float32x2_t v506 = vadd_f32(v447, v454);
    float32x2_t v507 = vsub_f32(v440, v454);
    float32x2_t v508 = vadd_f32(v468, v475);
    float32x2_t v509 = vsub_f32(v461, v475);
    float32x2_t v199 = vmul_f32(v198, v395);
    float32x2_t v299 = vsub_f32(v164, v192);
    float32x2_t v312 = vadd_f32(v291, v298);
    float32x2_t v313 = vsub_f32(v284, v298);
    float32x2_t v314 = vadd_f32(v304, v306);
    float32x2_t v332 = vadd_f32(v308, v309);
    float32x2_t v397 = vmul_f32(v396, v395);
    float32x2_t v497 = vsub_f32(v362, v390);
    float32x2_t v510 = vadd_f32(v489, v496);
    float32x2_t v511 = vsub_f32(v482, v496);
    float32x2_t v512 = vadd_f32(v502, v504);
    float32x2_t v530 = vadd_f32(v506, v507);
    int16x4_t v546 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v164, 15), (int32x2_t){0, 0}));
    int16x4_t v552 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v362, 15), (int32x2_t){0, 0}));
    float32x2_t v315 = vadd_f32(v314, v299);
    float32x2_t v316 = vsub_f32(v299, v301);
    float32x2_t v318 = vadd_f32(v299, v305);
    float32x2_t v320 = vsub_f32(v299, v302);
    float32x2_t v322 = vadd_f32(v299, v300);
    float32x2_t v324 = vadd_f32(v199, v310);
    float32x2_t v326 = vsub_f32(v312, v308);
    float32x2_t v328 = vadd_f32(v199, v313);
    float32x2_t v330 = vsub_f32(v313, v309);
    float32x2_t v333 = vadd_f32(v332, v310);
    float32x2_t v513 = vadd_f32(v512, v497);
    float32x2_t v514 = vsub_f32(v497, v499);
    float32x2_t v516 = vadd_f32(v497, v503);
    float32x2_t v518 = vsub_f32(v497, v500);
    float32x2_t v520 = vadd_f32(v497, v498);
    float32x2_t v522 = vadd_f32(v397, v508);
    float32x2_t v524 = vsub_f32(v510, v506);
    float32x2_t v526 = vadd_f32(v397, v511);
    float32x2_t v528 = vsub_f32(v511, v507);
    float32x2_t v531 = vadd_f32(v530, v508);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v546), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v552), 0);
    float32x2_t v317 = vsub_f32(v316, v306);
    float32x2_t v319 = vadd_f32(v318, v307);
    float32x2_t v321 = vsub_f32(v320, v307);
    float32x2_t v323 = vsub_f32(v322, v303);
    float32x2_t v325 = vadd_f32(v324, v312);
    float32x2_t v327 = vsub_f32(v326, v199);
    float32x2_t v329 = vadd_f32(v328, v311);
    float32x2_t v331 = vsub_f32(v330, v199);
    float32x2_t v334 = vadd_f32(v333, v311);
    float32x2_t v515 = vsub_f32(v514, v504);
    float32x2_t v517 = vadd_f32(v516, v505);
    float32x2_t v519 = vsub_f32(v518, v505);
    float32x2_t v521 = vsub_f32(v520, v501);
    float32x2_t v523 = vadd_f32(v522, v510);
    float32x2_t v525 = vsub_f32(v524, v397);
    float32x2_t v527 = vadd_f32(v526, v509);
    float32x2_t v529 = vsub_f32(v528, v397);
    float32x2_t v532 = vadd_f32(v531, v509);
    float32x2_t v335 = vsub_f32(v334, v199);
    float32x2_t v337 = vadd_f32(v315, v325);
    float32x2_t v338 = vadd_f32(v317, v327);
    float32x2_t v339 = vsub_f32(v319, v329);
    float32x2_t v340 = vadd_f32(v321, v331);
    float32x2_t v341 = vsub_f32(v321, v331);
    float32x2_t v342 = vadd_f32(v319, v329);
    float32x2_t v343 = vsub_f32(v317, v327);
    float32x2_t v344 = vsub_f32(v315, v325);
    float32x2_t v533 = vsub_f32(v532, v397);
    float32x2_t v535 = vadd_f32(v513, v523);
    float32x2_t v536 = vadd_f32(v515, v525);
    float32x2_t v537 = vsub_f32(v517, v527);
    float32x2_t v538 = vadd_f32(v519, v529);
    float32x2_t v539 = vsub_f32(v519, v529);
    float32x2_t v540 = vadd_f32(v517, v527);
    float32x2_t v541 = vsub_f32(v515, v525);
    float32x2_t v542 = vsub_f32(v513, v523);
    float32x2_t v336 = vadd_f32(v323, v335);
    float32x2_t v345 = vsub_f32(v323, v335);
    float32x2_t v534 = vadd_f32(v521, v533);
    float32x2_t v543 = vsub_f32(v521, v533);
    int16x4_t v570 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v344, 15), (int32x2_t){0, 0}));
    int16x4_t v576 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v542, 15), (int32x2_t){0, 0}));
    int16x4_t v582 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v343, 15), (int32x2_t){0, 0}));
    int16x4_t v588 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v541, 15), (int32x2_t){0, 0}));
    int16x4_t v594 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v342, 15), (int32x2_t){0, 0}));
    int16x4_t v600 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v540, 15), (int32x2_t){0, 0}));
    int16x4_t v606 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v341, 15), (int32x2_t){0, 0}));
    int16x4_t v612 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v539, 15), (int32x2_t){0, 0}));
    int16x4_t v618 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v340, 15), (int32x2_t){0, 0}));
    int16x4_t v624 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v538, 15), (int32x2_t){0, 0}));
    int16x4_t v630 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v339, 15), (int32x2_t){0, 0}));
    int16x4_t v636 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v537, 15), (int32x2_t){0, 0}));
    int16x4_t v642 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v338, 15), (int32x2_t){0, 0}));
    int16x4_t v648 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v536, 15), (int32x2_t){0, 0}));
    int16x4_t v654 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v337, 15), (int32x2_t){0, 0}));
    int16x4_t v660 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v535, 15), (int32x2_t){0, 0}));
    int16x4_t v558 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v345, 15), (int32x2_t){0, 0}));
    int16x4_t v564 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v543, 15), (int32x2_t){0, 0}));
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v570), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v576), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v582), 0);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v588), 0);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v594), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v600), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v606), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v612), 0);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v618), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v624), 0);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v630), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v636), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v642), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v648), 0);
    v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v654), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v660), 0);
    int16x4_t v666 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v336, 15), (int32x2_t){0, 0}));
    int16x4_t v672 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v534, 15), (int32x2_t){0, 0}));
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v558), 0);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v564), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v666), 0);
    v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v672), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu22(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v446 = 1.1000000000000001e+00F;
    float v451 = -3.3166247903554003e-01F;
    float v458 = 5.1541501300188641e-01F;
    float v463 = 9.4125353283118118e-01F;
    float v468 = 1.4143537075597825e+00F;
    float v473 = 8.5949297361449750e-01F;
    float v478 = 4.2314838273285138e-02F;
    float v483 = 3.8639279888589606e-01F;
    float v488 = 5.1254589567200015e-01F;
    float v493 = 1.0702757469471715e+00F;
    float v498 = 5.5486073394528512e-01F;
    float v503 = -1.2412944743900585e+00F;
    float v510 = -2.0897833842005756e-01F;
    float v517 = -3.7415717312460811e-01F;
    float v524 = -4.9929922194110327e-02F;
    float v531 = -6.5815896284539266e-01F;
    float v538 = -6.3306543373877577e-01F;
    float v545 = -1.0822460581641109e+00F;
    float v552 = -8.1720737907134022e-01F;
    float v559 = -4.2408709531871824e-01F;
    const float32x2_t *v912 = &v5[v0];
    int32_t *v1063 = &v6[v2];
    int64_t v26 = v0 * 11;
    int64_t v35 = v0 * 2;
    int64_t v42 = v0 * 13;
    int64_t v51 = v0 * 4;
    int64_t v58 = v0 * 15;
    int64_t v67 = v0 * 6;
    int64_t v74 = v0 * 17;
    int64_t v83 = v0 * 8;
    int64_t v90 = v0 * 19;
    int64_t v99 = v0 * 10;
    int64_t v106 = v0 * 21;
    int64_t v115 = v0 * 12;
    int64_t v131 = v0 * 14;
    int64_t v138 = v0 * 3;
    int64_t v147 = v0 * 16;
    int64_t v154 = v0 * 5;
    int64_t v163 = v0 * 18;
    int64_t v170 = v0 * 7;
    int64_t v179 = v0 * 20;
    int64_t v186 = v0 * 9;
    float v454 = v4 * v451;
    float v506 = v4 * v503;
    float v513 = v4 * v510;
    float v520 = v4 * v517;
    float v527 = v4 * v524;
    float v534 = v4 * v531;
    float v541 = v4 * v538;
    float v548 = v4 * v545;
    float v555 = v4 * v552;
    float v562 = v4 * v559;
    int64_t v621 = v2 * 11;
    int64_t v629 = v2 * 12;
    int64_t v645 = v2 * 2;
    int64_t v653 = v2 * 13;
    int64_t v661 = v2 * 14;
    int64_t v669 = v2 * 3;
    int64_t v677 = v2 * 4;
    int64_t v685 = v2 * 15;
    int64_t v693 = v2 * 16;
    int64_t v701 = v2 * 5;
    int64_t v709 = v2 * 6;
    int64_t v717 = v2 * 17;
    int64_t v725 = v2 * 18;
    int64_t v733 = v2 * 7;
    int64_t v741 = v2 * 8;
    int64_t v749 = v2 * 19;
    int64_t v757 = v2 * 20;
    int64_t v765 = v2 * 9;
    int64_t v773 = v2 * 10;
    int64_t v781 = v2 * 21;
    const float32x2_t *v795 = &v5[0];
    svfloat32_t v1009 = svdup_n_f32(v446);
    svfloat32_t v1011 = svdup_n_f32(v458);
    svfloat32_t v1012 = svdup_n_f32(v463);
    svfloat32_t v1013 = svdup_n_f32(v468);
    svfloat32_t v1014 = svdup_n_f32(v473);
    svfloat32_t v1015 = svdup_n_f32(v478);
    svfloat32_t v1016 = svdup_n_f32(v483);
    svfloat32_t v1017 = svdup_n_f32(v488);
    svfloat32_t v1018 = svdup_n_f32(v493);
    svfloat32_t v1019 = svdup_n_f32(v498);
    int32_t *v1036 = &v6[0];
    svfloat32_t v1255 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v912)[0]));
    const float32x2_t *v804 = &v5[v26];
    const float32x2_t *v813 = &v5[v35];
    const float32x2_t *v822 = &v5[v42];
    const float32x2_t *v831 = &v5[v51];
    const float32x2_t *v840 = &v5[v58];
    const float32x2_t *v849 = &v5[v67];
    const float32x2_t *v858 = &v5[v74];
    const float32x2_t *v867 = &v5[v83];
    const float32x2_t *v876 = &v5[v90];
    const float32x2_t *v885 = &v5[v99];
    const float32x2_t *v894 = &v5[v106];
    const float32x2_t *v903 = &v5[v115];
    const float32x2_t *v921 = &v5[v131];
    const float32x2_t *v930 = &v5[v138];
    const float32x2_t *v939 = &v5[v147];
    const float32x2_t *v948 = &v5[v154];
    const float32x2_t *v957 = &v5[v163];
    const float32x2_t *v966 = &v5[v170];
    const float32x2_t *v975 = &v5[v179];
    const float32x2_t *v984 = &v5[v186];
    svfloat32_t v1010 = svdup_n_f32(v454);
    svfloat32_t v1020 = svdup_n_f32(v506);
    svfloat32_t v1021 = svdup_n_f32(v513);
    svfloat32_t v1022 = svdup_n_f32(v520);
    svfloat32_t v1023 = svdup_n_f32(v527);
    svfloat32_t v1024 = svdup_n_f32(v534);
    svfloat32_t v1025 = svdup_n_f32(v541);
    svfloat32_t v1026 = svdup_n_f32(v548);
    svfloat32_t v1027 = svdup_n_f32(v555);
    svfloat32_t v1028 = svdup_n_f32(v562);
    int32_t *v1045 = &v6[v621];
    int32_t *v1054 = &v6[v629];
    int32_t *v1072 = &v6[v645];
    int32_t *v1081 = &v6[v653];
    int32_t *v1090 = &v6[v661];
    int32_t *v1099 = &v6[v669];
    int32_t *v1108 = &v6[v677];
    int32_t *v1117 = &v6[v685];
    int32_t *v1126 = &v6[v693];
    int32_t *v1135 = &v6[v701];
    int32_t *v1144 = &v6[v709];
    int32_t *v1153 = &v6[v717];
    int32_t *v1162 = &v6[v725];
    int32_t *v1171 = &v6[v733];
    int32_t *v1180 = &v6[v741];
    int32_t *v1189 = &v6[v749];
    int32_t *v1198 = &v6[v757];
    int32_t *v1207 = &v6[v765];
    int32_t *v1216 = &v6[v773];
    int32_t *v1225 = &v6[v781];
    svfloat32_t v1229 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v795)[0]));
    svfloat32_t v1231 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v804)[0]));
    svfloat32_t v1233 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v813)[0]));
    svfloat32_t v1235 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v822)[0]));
    svfloat32_t v1237 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v831)[0]));
    svfloat32_t v1239 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v840)[0]));
    svfloat32_t v1241 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v849)[0]));
    svfloat32_t v1243 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v858)[0]));
    svfloat32_t v1245 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v867)[0]));
    svfloat32_t v1247 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v876)[0]));
    svfloat32_t v1249 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v885)[0]));
    svfloat32_t v1251 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v894)[0]));
    svfloat32_t v1253 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v903)[0]));
    svfloat32_t v1257 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v921)[0]));
    svfloat32_t v1259 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v930)[0]));
    svfloat32_t v1261 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v939)[0]));
    svfloat32_t v1263 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v948)[0]));
    svfloat32_t v1265 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v957)[0]));
    svfloat32_t v1267 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v966)[0]));
    svfloat32_t v1269 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v975)[0]));
    svfloat32_t v1271 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v984)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v1229, v1231);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v1229, v1231);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v1233, v1235);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v1233, v1235);
    svfloat32_t v64 = svadd_f32_x(svptrue_b32(), v1237, v1239);
    svfloat32_t v65 = svsub_f32_x(svptrue_b32(), v1237, v1239);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v1241, v1243);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v1241, v1243);
    svfloat32_t v96 = svadd_f32_x(svptrue_b32(), v1245, v1247);
    svfloat32_t v97 = svsub_f32_x(svptrue_b32(), v1245, v1247);
    svfloat32_t v112 = svadd_f32_x(svptrue_b32(), v1249, v1251);
    svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v1249, v1251);
    svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v1253, v1255);
    svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v1253, v1255);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v1257, v1259);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v1257, v1259);
    svfloat32_t v160 = svadd_f32_x(svptrue_b32(), v1261, v1263);
    svfloat32_t v161 = svsub_f32_x(svptrue_b32(), v1261, v1263);
    svfloat32_t v176 = svadd_f32_x(svptrue_b32(), v1265, v1267);
    svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v1265, v1267);
    svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v1269, v1271);
    svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v1269, v1271);
    svfloat32_t v194 = svadd_f32_x(svptrue_b32(), v48, v192);
    svfloat32_t v195 = svadd_f32_x(svptrue_b32(), v64, v176);
    svfloat32_t v196 = svadd_f32_x(svptrue_b32(), v80, v160);
    svfloat32_t v197 = svadd_f32_x(svptrue_b32(), v96, v144);
    svfloat32_t v198 = svadd_f32_x(svptrue_b32(), v112, v128);
    svfloat32_t v199 = svsub_f32_x(svptrue_b32(), v48, v192);
    svfloat32_t v200 = svsub_f32_x(svptrue_b32(), v64, v176);
    svfloat32_t v201 = svsub_f32_x(svptrue_b32(), v80, v160);
    svfloat32_t v202 = svsub_f32_x(svptrue_b32(), v96, v144);
    svfloat32_t v203 = svsub_f32_x(svptrue_b32(), v112, v128);
    svfloat32_t v403 = svadd_f32_x(svptrue_b32(), v49, v193);
    svfloat32_t v404 = svadd_f32_x(svptrue_b32(), v65, v177);
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v81, v161);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v97, v145);
    svfloat32_t v407 = svadd_f32_x(svptrue_b32(), v113, v129);
    svfloat32_t v408 = svsub_f32_x(svptrue_b32(), v49, v193);
    svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v65, v177);
    svfloat32_t v410 = svsub_f32_x(svptrue_b32(), v81, v161);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v97, v145);
    svfloat32_t v412 = svsub_f32_x(svptrue_b32(), v113, v129);
    svfloat32_t v204 = svadd_f32_x(svptrue_b32(), v194, v195);
    svfloat32_t v205 = svadd_f32_x(svptrue_b32(), v196, v198);
    svfloat32_t v207 = svsub_f32_x(svptrue_b32(), v200, v201);
    svfloat32_t v208 = svadd_f32_x(svptrue_b32(), v199, v203);
    svfloat32_t v213 = svsub_f32_x(svptrue_b32(), v195, v197);
    svfloat32_t v214 = svsub_f32_x(svptrue_b32(), v194, v197);
    svfloat32_t v215 = svsub_f32_x(svptrue_b32(), v195, v194);
    svfloat32_t v216 = svsub_f32_x(svptrue_b32(), v198, v197);
    svfloat32_t v217 = svsub_f32_x(svptrue_b32(), v196, v197);
    svfloat32_t v218 = svsub_f32_x(svptrue_b32(), v198, v196);
    svfloat32_t v219 = svsub_f32_x(svptrue_b32(), v195, v198);
    svfloat32_t v220 = svsub_f32_x(svptrue_b32(), v194, v196);
    svfloat32_t v222 = svadd_f32_x(svptrue_b32(), v200, v202);
    svfloat32_t v223 = svsub_f32_x(svptrue_b32(), v199, v202);
    svfloat32_t v224 = svadd_f32_x(svptrue_b32(), v199, v200);
    svfloat32_t v225 = svsub_f32_x(svptrue_b32(), v202, v203);
    svfloat32_t v226 = svsub_f32_x(svptrue_b32(), v201, v202);
    svfloat32_t v227 = svsub_f32_x(svptrue_b32(), v201, v203);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v200, v203);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v199, v201);
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v403, v404);
    svfloat32_t v414 = svadd_f32_x(svptrue_b32(), v405, v407);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v409, v410);
    svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v408, v412);
    svfloat32_t v422 = svsub_f32_x(svptrue_b32(), v404, v406);
    svfloat32_t v423 = svsub_f32_x(svptrue_b32(), v403, v406);
    svfloat32_t v424 = svsub_f32_x(svptrue_b32(), v404, v403);
    svfloat32_t v425 = svsub_f32_x(svptrue_b32(), v407, v406);
    svfloat32_t v426 = svsub_f32_x(svptrue_b32(), v405, v406);
    svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v407, v405);
    svfloat32_t v428 = svsub_f32_x(svptrue_b32(), v404, v407);
    svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v403, v405);
    svfloat32_t v431 = svadd_f32_x(svptrue_b32(), v409, v411);
    svfloat32_t v432 = svsub_f32_x(svptrue_b32(), v408, v411);
    svfloat32_t v433 = svadd_f32_x(svptrue_b32(), v408, v409);
    svfloat32_t v434 = svsub_f32_x(svptrue_b32(), v411, v412);
    svfloat32_t v435 = svsub_f32_x(svptrue_b32(), v410, v411);
    svfloat32_t v436 = svsub_f32_x(svptrue_b32(), v410, v412);
    svfloat32_t v437 = svadd_f32_x(svptrue_b32(), v409, v412);
    svfloat32_t v438 = svsub_f32_x(svptrue_b32(), v408, v410);
    svfloat32_t v206 = svadd_f32_x(svptrue_b32(), v197, v204);
    svfloat32_t v211 = svsub_f32_x(svptrue_b32(), v207, v208);
    svfloat32_t v221 = svsub_f32_x(svptrue_b32(), v205, v204);
    svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v207, v208);
    svfloat32_t v257 = svmul_f32_x(svptrue_b32(), v214, v1012);
    svfloat32_t v262 = svmul_f32_x(svptrue_b32(), v215, v1013);
    svfloat32_t v272 = svmul_f32_x(svptrue_b32(), v217, v1015);
    svfloat32_t v277 = svmul_f32_x(svptrue_b32(), v218, v1016);
    svfloat32_t zero299 = svdup_n_f32(0);
    svfloat32_t v299 = svcmla_f32_x(pred_full, zero299, v1020, v222, 90);
    svfloat32_t zero313 = svdup_n_f32(0);
    svfloat32_t v313 = svcmla_f32_x(pred_full, zero313, v1022, v224, 90);
    svfloat32_t zero320 = svdup_n_f32(0);
    svfloat32_t v320 = svcmla_f32_x(pred_full, zero320, v1023, v225, 90);
    svfloat32_t zero334 = svdup_n_f32(0);
    svfloat32_t v334 = svcmla_f32_x(pred_full, zero334, v1025, v227, 90);
    svfloat32_t zero341 = svdup_n_f32(0);
    svfloat32_t v341 = svcmla_f32_x(pred_full, zero341, v1026, v228, 90);
    svfloat32_t v415 = svadd_f32_x(svptrue_b32(), v406, v413);
    svfloat32_t v420 = svsub_f32_x(svptrue_b32(), v416, v417);
    svfloat32_t v430 = svsub_f32_x(svptrue_b32(), v414, v413);
    svfloat32_t v439 = svadd_f32_x(svptrue_b32(), v416, v417);
    svfloat32_t v466 = svmul_f32_x(svptrue_b32(), v423, v1012);
    svfloat32_t v471 = svmul_f32_x(svptrue_b32(), v424, v1013);
    svfloat32_t v481 = svmul_f32_x(svptrue_b32(), v426, v1015);
    svfloat32_t v486 = svmul_f32_x(svptrue_b32(), v427, v1016);
    svfloat32_t zero508 = svdup_n_f32(0);
    svfloat32_t v508 = svcmla_f32_x(pred_full, zero508, v1020, v431, 90);
    svfloat32_t zero522 = svdup_n_f32(0);
    svfloat32_t v522 = svcmla_f32_x(pred_full, zero522, v1022, v433, 90);
    svfloat32_t zero529 = svdup_n_f32(0);
    svfloat32_t v529 = svcmla_f32_x(pred_full, zero529, v1023, v434, 90);
    svfloat32_t zero543 = svdup_n_f32(0);
    svfloat32_t v543 = svcmla_f32_x(pred_full, zero543, v1025, v436, 90);
    svfloat32_t zero550 = svdup_n_f32(0);
    svfloat32_t v550 = svcmla_f32_x(pred_full, zero550, v1026, v437, 90);
    svfloat32_t v209 = svadd_f32_x(svptrue_b32(), v206, v205);
    svfloat32_t v212 = svsub_f32_x(svptrue_b32(), v211, v202);
    svfloat32_t v292 = svmul_f32_x(svptrue_b32(), v221, v1019);
    svfloat32_t zero355 = svdup_n_f32(0);
    svfloat32_t v355 = svcmla_f32_x(pred_full, zero355, v1028, v230, 90);
    svfloat32_t v357 = svmla_f32_x(pred_full, v257, v213, v1011);
    svfloat32_t v358 = svmla_f32_x(pred_full, v262, v214, v1012);
    svfloat32_t v359 = svnmls_f32_x(pred_full, v262, v213, v1011);
    svfloat32_t v360 = svmla_f32_x(pred_full, v272, v216, v1014);
    svfloat32_t v361 = svmla_f32_x(pred_full, v277, v217, v1015);
    svfloat32_t v362 = svnmls_f32_x(pred_full, v277, v216, v1014);
    svfloat32_t v365 = svcmla_f32_x(pred_full, v313, v1021, v223, 90);
    svfloat32_t v366 = svsub_f32_x(svptrue_b32(), v299, v313);
    svfloat32_t v367 = svcmla_f32_x(pred_full, v334, v1024, v226, 90);
    svfloat32_t v368 = svsub_f32_x(svptrue_b32(), v320, v334);
    svfloat32_t v418 = svadd_f32_x(svptrue_b32(), v415, v414);
    svfloat32_t v421 = svsub_f32_x(svptrue_b32(), v420, v411);
    svfloat32_t v501 = svmul_f32_x(svptrue_b32(), v430, v1019);
    svfloat32_t zero564 = svdup_n_f32(0);
    svfloat32_t v564 = svcmla_f32_x(pred_full, zero564, v1028, v439, 90);
    svfloat32_t v566 = svmla_f32_x(pred_full, v466, v422, v1011);
    svfloat32_t v567 = svmla_f32_x(pred_full, v471, v423, v1012);
    svfloat32_t v568 = svnmls_f32_x(pred_full, v471, v422, v1011);
    svfloat32_t v569 = svmla_f32_x(pred_full, v481, v425, v1014);
    svfloat32_t v570 = svmla_f32_x(pred_full, v486, v426, v1015);
    svfloat32_t v571 = svnmls_f32_x(pred_full, v486, v425, v1014);
    svfloat32_t v574 = svcmla_f32_x(pred_full, v522, v1021, v432, 90);
    svfloat32_t v575 = svsub_f32_x(svptrue_b32(), v508, v522);
    svfloat32_t v576 = svcmla_f32_x(pred_full, v543, v1024, v435, 90);
    svfloat32_t v577 = svsub_f32_x(svptrue_b32(), v529, v543);
    svfloat32_t v210 = svadd_f32_x(svptrue_b32(), v32, v209);
    svfloat32_t zero247 = svdup_n_f32(0);
    svfloat32_t v247 = svcmla_f32_x(pred_full, zero247, v1010, v212, 90);
    svfloat32_t v363 = svmla_f32_x(pred_full, v292, v220, v1018);
    svfloat32_t v364 = svmla_f32_x(pred_full, v292, v219, v1017);
    svfloat32_t v369 = svcmla_f32_x(pred_full, v355, v1027, v229, 90);
    svfloat32_t v370 = svsub_f32_x(svptrue_b32(), v341, v355);
    svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v365, v366);
    svfloat32_t v419 = svadd_f32_x(svptrue_b32(), v33, v418);
    svfloat32_t zero456 = svdup_n_f32(0);
    svfloat32_t v456 = svcmla_f32_x(pred_full, zero456, v1010, v421, 90);
    svfloat32_t v572 = svmla_f32_x(pred_full, v501, v429, v1018);
    svfloat32_t v573 = svmla_f32_x(pred_full, v501, v428, v1017);
    svfloat32_t v578 = svcmla_f32_x(pred_full, v564, v1027, v438, 90);
    svfloat32_t v579 = svsub_f32_x(svptrue_b32(), v550, v564);
    svfloat32_t v598 = svadd_f32_x(svptrue_b32(), v574, v575);
    svfloat32_t v356 = svmls_f32_x(pred_full, v210, v209, v1009);
    svfloat32_t v371 = svadd_f32_x(svptrue_b32(), v361, v363);
    svfloat32_t v381 = svadd_f32_x(svptrue_b32(), v247, v367);
    svfloat32_t v383 = svsub_f32_x(svptrue_b32(), v369, v365);
    svfloat32_t v385 = svadd_f32_x(svptrue_b32(), v247, v370);
    svfloat32_t v387 = svsub_f32_x(svptrue_b32(), v370, v366);
    svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v389, v367);
    svfloat32_t v565 = svmls_f32_x(pred_full, v419, v418, v1009);
    svfloat32_t v580 = svadd_f32_x(svptrue_b32(), v570, v572);
    svfloat32_t v590 = svadd_f32_x(svptrue_b32(), v456, v576);
    svfloat32_t v592 = svsub_f32_x(svptrue_b32(), v578, v574);
    svfloat32_t v594 = svadd_f32_x(svptrue_b32(), v456, v579);
    svfloat32_t v596 = svsub_f32_x(svptrue_b32(), v579, v575);
    svfloat32_t v599 = svadd_f32_x(svptrue_b32(), v598, v576);
    svint16_t v614 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v210, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v622 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v419, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v371, v356);
    svfloat32_t v373 = svsub_f32_x(svptrue_b32(), v356, v358);
    svfloat32_t v375 = svadd_f32_x(svptrue_b32(), v356, v362);
    svfloat32_t v377 = svsub_f32_x(svptrue_b32(), v356, v359);
    svfloat32_t v379 = svadd_f32_x(svptrue_b32(), v356, v357);
    svfloat32_t v382 = svadd_f32_x(svptrue_b32(), v381, v369);
    svfloat32_t v384 = svsub_f32_x(svptrue_b32(), v383, v247);
    svfloat32_t v386 = svadd_f32_x(svptrue_b32(), v385, v368);
    svfloat32_t v388 = svsub_f32_x(svptrue_b32(), v387, v247);
    svfloat32_t v391 = svadd_f32_x(svptrue_b32(), v390, v368);
    svfloat32_t v581 = svadd_f32_x(svptrue_b32(), v580, v565);
    svfloat32_t v582 = svsub_f32_x(svptrue_b32(), v565, v567);
    svfloat32_t v584 = svadd_f32_x(svptrue_b32(), v565, v571);
    svfloat32_t v586 = svsub_f32_x(svptrue_b32(), v565, v568);
    svfloat32_t v588 = svadd_f32_x(svptrue_b32(), v565, v566);
    svfloat32_t v591 = svadd_f32_x(svptrue_b32(), v590, v578);
    svfloat32_t v593 = svsub_f32_x(svptrue_b32(), v592, v456);
    svfloat32_t v595 = svadd_f32_x(svptrue_b32(), v594, v577);
    svfloat32_t v597 = svsub_f32_x(svptrue_b32(), v596, v456);
    svfloat32_t v600 = svadd_f32_x(svptrue_b32(), v599, v577);
    svst1w_u64(pred_full, (unsigned *)(v1036), svreinterpret_u64_s16(v614));
    svst1w_u64(pred_full, (unsigned *)(v1045), svreinterpret_u64_s16(v622));
    svfloat32_t v374 = svsub_f32_x(svptrue_b32(), v373, v363);
    svfloat32_t v376 = svadd_f32_x(svptrue_b32(), v375, v364);
    svfloat32_t v378 = svsub_f32_x(svptrue_b32(), v377, v364);
    svfloat32_t v380 = svsub_f32_x(svptrue_b32(), v379, v360);
    svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v391, v247);
    svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v372, v382);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v372, v382);
    svfloat32_t v583 = svsub_f32_x(svptrue_b32(), v582, v572);
    svfloat32_t v585 = svadd_f32_x(svptrue_b32(), v584, v573);
    svfloat32_t v587 = svsub_f32_x(svptrue_b32(), v586, v573);
    svfloat32_t v589 = svsub_f32_x(svptrue_b32(), v588, v569);
    svfloat32_t v601 = svsub_f32_x(svptrue_b32(), v600, v456);
    svfloat32_t v603 = svadd_f32_x(svptrue_b32(), v581, v591);
    svfloat32_t v610 = svsub_f32_x(svptrue_b32(), v581, v591);
    svfloat32_t v393 = svadd_f32_x(svptrue_b32(), v380, v392);
    svfloat32_t v395 = svadd_f32_x(svptrue_b32(), v374, v384);
    svfloat32_t v396 = svsub_f32_x(svptrue_b32(), v376, v386);
    svfloat32_t v397 = svadd_f32_x(svptrue_b32(), v378, v388);
    svfloat32_t v398 = svsub_f32_x(svptrue_b32(), v378, v388);
    svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v376, v386);
    svfloat32_t v400 = svsub_f32_x(svptrue_b32(), v374, v384);
    svfloat32_t v402 = svsub_f32_x(svptrue_b32(), v380, v392);
    svfloat32_t v602 = svadd_f32_x(svptrue_b32(), v589, v601);
    svfloat32_t v604 = svadd_f32_x(svptrue_b32(), v583, v593);
    svfloat32_t v605 = svsub_f32_x(svptrue_b32(), v585, v595);
    svfloat32_t v606 = svadd_f32_x(svptrue_b32(), v587, v597);
    svfloat32_t v607 = svsub_f32_x(svptrue_b32(), v587, v597);
    svfloat32_t v608 = svadd_f32_x(svptrue_b32(), v585, v595);
    svfloat32_t v609 = svsub_f32_x(svptrue_b32(), v583, v593);
    svfloat32_t v611 = svsub_f32_x(svptrue_b32(), v589, v601);
    svint16_t v646 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v401, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v654 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v610, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v758 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v394, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v766 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v603, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v630 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v402, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v638 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v611, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v662 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v400, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v670 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v609, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v678 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v399, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v686 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v608, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v694 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v398, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v702 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v607, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v710 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v397, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v718 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v606, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v726 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v396, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v734 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v605, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v742 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v395, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v750 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v604, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v774 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v393, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v782 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v602, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1072), svreinterpret_u64_s16(v646));
    svst1w_u64(pred_full, (unsigned *)(v1081), svreinterpret_u64_s16(v654));
    svst1w_u64(pred_full, (unsigned *)(v1198), svreinterpret_u64_s16(v758));
    svst1w_u64(pred_full, (unsigned *)(v1207), svreinterpret_u64_s16(v766));
    svst1w_u64(pred_full, (unsigned *)(v1054), svreinterpret_u64_s16(v630));
    svst1w_u64(pred_full, (unsigned *)(v1063), svreinterpret_u64_s16(v638));
    svst1w_u64(pred_full, (unsigned *)(v1090), svreinterpret_u64_s16(v662));
    svst1w_u64(pred_full, (unsigned *)(v1099), svreinterpret_u64_s16(v670));
    svst1w_u64(pred_full, (unsigned *)(v1108), svreinterpret_u64_s16(v678));
    svst1w_u64(pred_full, (unsigned *)(v1117), svreinterpret_u64_s16(v686));
    svst1w_u64(pred_full, (unsigned *)(v1126), svreinterpret_u64_s16(v694));
    svst1w_u64(pred_full, (unsigned *)(v1135), svreinterpret_u64_s16(v702));
    svst1w_u64(pred_full, (unsigned *)(v1144), svreinterpret_u64_s16(v710));
    svst1w_u64(pred_full, (unsigned *)(v1153), svreinterpret_u64_s16(v718));
    svst1w_u64(pred_full, (unsigned *)(v1162), svreinterpret_u64_s16(v726));
    svst1w_u64(pred_full, (unsigned *)(v1171), svreinterpret_u64_s16(v734));
    svst1w_u64(pred_full, (unsigned *)(v1180), svreinterpret_u64_s16(v742));
    svst1w_u64(pred_full, (unsigned *)(v1189), svreinterpret_u64_s16(v750));
    svst1w_u64(pred_full, (unsigned *)(v1216), svreinterpret_u64_s16(v774));
    svst1w_u64(pred_full, (unsigned *)(v1225), svreinterpret_u64_s16(v782));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu24(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v79 = v5[istride];
    float v199 = 1.0000000000000000e+00F;
    float v200 = -1.0000000000000000e+00F;
    float v207 = -7.0710678118654746e-01F;
    float v214 = 7.0710678118654757e-01F;
    float v266 = -1.4999999999999998e+00F;
    float v267 = 1.4999999999999998e+00F;
    float v274 = 1.0606601717798210e+00F;
    float v281 = -1.0606601717798212e+00F;
    float v335 = 8.6602540378443871e-01F;
    float v343 = -8.6602540378443871e-01F;
    float v350 = 6.1237243569579458e-01F;
    float v351 = -6.1237243569579458e-01F;
    float32x2_t v353 = (float32x2_t){v4, v4};
    float32x2_t v32 = v5[0];
    float32x2_t v201 = (float32x2_t){v199, v200};
    float32x2_t v208 = (float32x2_t){v214, v207};
    float32x2_t v215 = (float32x2_t){v214, v214};
    float32x2_t v264 = (float32x2_t){v266, v266};
    float32x2_t v268 = (float32x2_t){v266, v267};
    float32x2_t v275 = (float32x2_t){v281, v274};
    float32x2_t v282 = (float32x2_t){v281, v281};
    float32x2_t v337 = (float32x2_t){v335, v343};
    float32x2_t v344 = (float32x2_t){v343, v343};
    float32x2_t v348 = (float32x2_t){v351, v351};
    float32x2_t v352 = (float32x2_t){v350, v351};
    float32x2_t v20 = v5[istride * 8];
    float32x2_t v25 = v5[istride * 16];
    float32x2_t v38 = v5[istride * 11];
    float32x2_t v43 = v5[istride * 19];
    float32x2_t v50 = v5[istride * 3];
    float32x2_t v56 = v5[istride * 14];
    float32x2_t v61 = v5[istride * 22];
    float32x2_t v68 = v5[istride * 6];
    float32x2_t v74 = v5[istride * 17];
    float32x2_t v86 = v5[istride * 9];
    float32x2_t v92 = v5[istride * 20];
    float32x2_t v97 = v5[istride * 4];
    float32x2_t v104 = v5[istride * 12];
    float32x2_t v110 = v5[istride * 23];
    float32x2_t v115 = v5[istride * 7];
    float32x2_t v122 = v5[istride * 15];
    float32x2_t v128 = v5[istride * 2];
    float32x2_t v133 = v5[istride * 10];
    float32x2_t v140 = v5[istride * 18];
    float32x2_t v146 = v5[istride * 5];
    float32x2_t v151 = v5[istride * 13];
    float32x2_t v158 = v5[istride * 21];
    float32x2_t v203 = vmul_f32(v353, v201);
    float32x2_t v210 = vmul_f32(v353, v208);
    float32x2_t v270 = vmul_f32(v353, v268);
    float32x2_t v277 = vmul_f32(v353, v275);
    float32x2_t v339 = vmul_f32(v353, v337);
    float32x2_t v354 = vmul_f32(v353, v352);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v44 = vadd_f32(v38, v43);
    float32x2_t v45 = vsub_f32(v38, v43);
    float32x2_t v62 = vadd_f32(v56, v61);
    float32x2_t v63 = vsub_f32(v56, v61);
    float32x2_t v80 = vadd_f32(v74, v79);
    float32x2_t v81 = vsub_f32(v74, v79);
    float32x2_t v98 = vadd_f32(v92, v97);
    float32x2_t v99 = vsub_f32(v92, v97);
    float32x2_t v116 = vadd_f32(v110, v115);
    float32x2_t v117 = vsub_f32(v110, v115);
    float32x2_t v134 = vadd_f32(v128, v133);
    float32x2_t v135 = vsub_f32(v128, v133);
    float32x2_t v152 = vadd_f32(v146, v151);
    float32x2_t v153 = vsub_f32(v146, v151);
    float32x2_t v33 = vadd_f32(v26, v32);
    float32x2_t v51 = vadd_f32(v44, v50);
    float32x2_t v69 = vadd_f32(v62, v68);
    float32x2_t v87 = vadd_f32(v80, v86);
    float32x2_t v105 = vadd_f32(v98, v104);
    float32x2_t v123 = vadd_f32(v116, v122);
    float32x2_t v141 = vadd_f32(v134, v140);
    float32x2_t v159 = vadd_f32(v152, v158);
    float32x2_t v227 = vadd_f32(v26, v98);
    float32x2_t v228 = vsub_f32(v26, v98);
    float32x2_t v229 = vadd_f32(v62, v134);
    float32x2_t v230 = vsub_f32(v62, v134);
    float32x2_t v231 = vadd_f32(v44, v116);
    float32x2_t v232 = vsub_f32(v44, v116);
    float32x2_t v233 = vadd_f32(v80, v152);
    float32x2_t v234 = vsub_f32(v80, v152);
    float32x2_t v294 = vadd_f32(v27, v99);
    float32x2_t v295 = vsub_f32(v27, v99);
    float32x2_t v296 = vadd_f32(v63, v135);
    float32x2_t v297 = vsub_f32(v63, v135);
    float32x2_t v298 = vadd_f32(v45, v117);
    float32x2_t v299 = vsub_f32(v45, v117);
    float32x2_t v300 = vadd_f32(v81, v153);
    float32x2_t v301 = vsub_f32(v81, v153);
    float32x2_t v160 = vadd_f32(v33, v105);
    float32x2_t v161 = vsub_f32(v33, v105);
    float32x2_t v162 = vadd_f32(v69, v141);
    float32x2_t v163 = vsub_f32(v69, v141);
    float32x2_t v164 = vadd_f32(v51, v123);
    float32x2_t v165 = vsub_f32(v51, v123);
    float32x2_t v166 = vadd_f32(v87, v159);
    float32x2_t v167 = vsub_f32(v87, v159);
    float32x2_t v235 = vadd_f32(v227, v229);
    float32x2_t v236 = vsub_f32(v227, v229);
    float32x2_t v237 = vadd_f32(v231, v233);
    float32x2_t v238 = vsub_f32(v231, v233);
    float32x2_t v241 = vadd_f32(v232, v234);
    float32x2_t v242 = vsub_f32(v232, v234);
    float32x2_t v265 = vmul_f32(v228, v264);
    float32x2_t v271 = vrev64_f32(v230);
    float32x2_t v302 = vadd_f32(v294, v296);
    float32x2_t v303 = vsub_f32(v294, v296);
    float32x2_t v304 = vadd_f32(v298, v300);
    float32x2_t v305 = vsub_f32(v298, v300);
    float32x2_t v308 = vadd_f32(v299, v301);
    float32x2_t v309 = vsub_f32(v299, v301);
    float32x2_t v340 = vrev64_f32(v295);
    float32x2_t v345 = vmul_f32(v297, v344);
    float32x2_t v168 = vadd_f32(v160, v162);
    float32x2_t v169 = vsub_f32(v160, v162);
    float32x2_t v170 = vadd_f32(v164, v166);
    float32x2_t v171 = vsub_f32(v164, v166);
    float32x2_t v174 = vadd_f32(v165, v167);
    float32x2_t v175 = vsub_f32(v165, v167);
    float32x2_t v204 = vrev64_f32(v163);
    float32x2_t v239 = vadd_f32(v235, v237);
    float32x2_t v240 = vsub_f32(v235, v237);
    float32x2_t v254 = vmul_f32(v236, v264);
    float32x2_t v260 = vrev64_f32(v238);
    float32x2_t v272 = vmul_f32(v271, v270);
    float32x2_t v278 = vrev64_f32(v241);
    float32x2_t v283 = vmul_f32(v242, v282);
    float32x2_t v306 = vadd_f32(v302, v304);
    float32x2_t v307 = vsub_f32(v302, v304);
    float32x2_t v329 = vrev64_f32(v303);
    float32x2_t v334 = vmul_f32(v305, v344);
    float32x2_t v341 = vmul_f32(v340, v339);
    float32x2_t v349 = vmul_f32(v308, v348);
    float32x2_t v355 = vrev64_f32(v309);
    float32x2_t v172 = vadd_f32(v168, v170);
    float32x2_t v173 = vsub_f32(v168, v170);
    float32x2_t v193 = vrev64_f32(v171);
    float32x2_t v205 = vmul_f32(v204, v203);
    float32x2_t v211 = vrev64_f32(v174);
    float32x2_t v216 = vmul_f32(v175, v215);
    float32x2_t v246 = vmul_f32(v239, v264);
    float32x2_t v250 = vmul_f32(v240, v264);
    float32x2_t v261 = vmul_f32(v260, v270);
    float32x2_t v279 = vmul_f32(v278, v277);
    float32x2_t v286 = vadd_f32(v265, v283);
    float32x2_t v287 = vsub_f32(v265, v283);
    float32x2_t v315 = vrev64_f32(v306);
    float32x2_t v322 = vrev64_f32(v307);
    float32x2_t v330 = vmul_f32(v329, v339);
    float32x2_t v356 = vmul_f32(v355, v354);
    float32x2_t v361 = vadd_f32(v345, v349);
    float32x2_t v362 = vsub_f32(v345, v349);
    float32x2_t v194 = vmul_f32(v193, v203);
    float32x2_t v212 = vmul_f32(v211, v210);
    float32x2_t v219 = vadd_f32(v161, v216);
    float32x2_t v220 = vsub_f32(v161, v216);
    float32x2_t v284 = vadd_f32(v254, v261);
    float32x2_t v285 = vsub_f32(v254, v261);
    float32x2_t v288 = vadd_f32(v272, v279);
    float32x2_t v289 = vsub_f32(v272, v279);
    float32x2_t v316 = vmul_f32(v315, v339);
    float32x2_t v323 = vmul_f32(v322, v339);
    float32x2_t v357 = vadd_f32(v330, v334);
    float32x2_t v358 = vsub_f32(v330, v334);
    float32x2_t v359 = vadd_f32(v341, v356);
    float32x2_t v360 = vsub_f32(v341, v356);
    float32x2_t v367 = vadd_f32(v172, v246);
    int16x4_t v372 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v172, 15), (int32x2_t){0, 0}));
    float32x2_t v451 = vadd_f32(v173, v250);
    int16x4_t v456 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v173, 15), (int32x2_t){0, 0}));
    float32x2_t v217 = vadd_f32(v169, v194);
    float32x2_t v218 = vsub_f32(v169, v194);
    float32x2_t v221 = vadd_f32(v205, v212);
    float32x2_t v222 = vsub_f32(v205, v212);
    float32x2_t v290 = vadd_f32(v286, v288);
    float32x2_t v291 = vsub_f32(v286, v288);
    float32x2_t v292 = vadd_f32(v287, v289);
    float32x2_t v293 = vsub_f32(v287, v289);
    float32x2_t v363 = vadd_f32(v359, v361);
    float32x2_t v364 = vsub_f32(v359, v361);
    float32x2_t v365 = vadd_f32(v360, v362);
    float32x2_t v366 = vsub_f32(v360, v362);
    float32x2_t v368 = vadd_f32(v367, v316);
    float32x2_t v369 = vsub_f32(v367, v316);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v372), 0);
    float32x2_t v452 = vadd_f32(v451, v323);
    float32x2_t v453 = vsub_f32(v451, v323);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v456), 0);
    float32x2_t v223 = vadd_f32(v219, v221);
    float32x2_t v224 = vsub_f32(v219, v221);
    float32x2_t v225 = vadd_f32(v220, v222);
    float32x2_t v226 = vsub_f32(v220, v222);
    int16x4_t v378 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v369, 15), (int32x2_t){0, 0}));
    int16x4_t v384 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v368, 15), (int32x2_t){0, 0}));
    float32x2_t v409 = vadd_f32(v218, v285);
    int16x4_t v414 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v218, 15), (int32x2_t){0, 0}));
    int16x4_t v462 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v453, 15), (int32x2_t){0, 0}));
    int16x4_t v468 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v452, 15), (int32x2_t){0, 0}));
    float32x2_t v493 = vadd_f32(v217, v284);
    int16x4_t v498 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v217, 15), (int32x2_t){0, 0}));
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v378), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v384), 0);
    float32x2_t v388 = vadd_f32(v224, v291);
    int16x4_t v393 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v224, 15), (int32x2_t){0, 0}));
    float32x2_t v410 = vadd_f32(v409, v358);
    float32x2_t v411 = vsub_f32(v409, v358);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v414), 0);
    float32x2_t v430 = vadd_f32(v225, v292);
    int16x4_t v435 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v225, 15), (int32x2_t){0, 0}));
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v462), 0);
    v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v468), 0);
    float32x2_t v472 = vadd_f32(v226, v293);
    int16x4_t v477 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v226, 15), (int32x2_t){0, 0}));
    float32x2_t v494 = vadd_f32(v493, v357);
    float32x2_t v495 = vsub_f32(v493, v357);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v498), 0);
    float32x2_t v514 = vadd_f32(v223, v290);
    int16x4_t v519 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v223, 15), (int32x2_t){0, 0}));
    float32x2_t v389 = vadd_f32(v388, v364);
    float32x2_t v390 = vsub_f32(v388, v364);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v393), 0);
    int16x4_t v420 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v411, 15), (int32x2_t){0, 0}));
    int16x4_t v426 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v410, 15), (int32x2_t){0, 0}));
    float32x2_t v431 = vadd_f32(v430, v365);
    float32x2_t v432 = vsub_f32(v430, v365);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v435), 0);
    float32x2_t v473 = vadd_f32(v472, v366);
    float32x2_t v474 = vsub_f32(v472, v366);
    v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v477), 0);
    int16x4_t v504 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v495, 15), (int32x2_t){0, 0}));
    int16x4_t v510 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v494, 15), (int32x2_t){0, 0}));
    float32x2_t v515 = vadd_f32(v514, v363);
    float32x2_t v516 = vsub_f32(v514, v363);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v519), 0);
    int16x4_t v399 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v390, 15), (int32x2_t){0, 0}));
    int16x4_t v405 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v389, 15), (int32x2_t){0, 0}));
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v420), 0);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v426), 0);
    int16x4_t v441 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v432, 15), (int32x2_t){0, 0}));
    int16x4_t v447 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v431, 15), (int32x2_t){0, 0}));
    int16x4_t v483 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v474, 15), (int32x2_t){0, 0}));
    int16x4_t v489 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v473, 15), (int32x2_t){0, 0}));
    v6[ostride * 22] = vget_lane_s32(vreinterpret_s32_s16(v504), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v510), 0);
    int16x4_t v525 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v516, 15), (int32x2_t){0, 0}));
    int16x4_t v531 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v515, 15), (int32x2_t){0, 0}));
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v399), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v405), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v441), 0);
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v447), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v483), 0);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v489), 0);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v525), 0);
    v6[ostride * 23] = vget_lane_s32(vreinterpret_s32_s16(v531), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu24(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v254 = -1.0000000000000000e+00F;
    float v261 = -7.0710678118654746e-01F;
    float v268 = 7.0710678118654757e-01F;
    float v321 = -1.4999999999999998e+00F;
    float v326 = 1.4999999999999998e+00F;
    float v333 = 1.0606601717798210e+00F;
    float v340 = -1.0606601717798212e+00F;
    float v404 = -8.6602540378443871e-01F;
    float v414 = -6.1237243569579458e-01F;
    const float32x2_t *v743 = &v5[v0];
    int32_t *v930 = &v6[v2];
    int64_t v19 = v0 * 8;
    int64_t v26 = v0 * 16;
    int64_t v43 = v0 * 11;
    int64_t v50 = v0 * 19;
    int64_t v59 = v0 * 3;
    int64_t v67 = v0 * 14;
    int64_t v74 = v0 * 22;
    int64_t v83 = v0 * 6;
    int64_t v91 = v0 * 17;
    int64_t v107 = v0 * 9;
    int64_t v115 = v0 * 20;
    int64_t v122 = v0 * 4;
    int64_t v131 = v0 * 12;
    int64_t v139 = v0 * 23;
    int64_t v146 = v0 * 7;
    int64_t v155 = v0 * 15;
    int64_t v163 = v0 * 2;
    int64_t v170 = v0 * 10;
    int64_t v179 = v0 * 18;
    int64_t v187 = v0 * 5;
    int64_t v194 = v0 * 13;
    int64_t v203 = v0 * 21;
    float v257 = v4 * v254;
    float v264 = v4 * v261;
    float v329 = v4 * v326;
    float v336 = v4 * v333;
    float v400 = v4 * v404;
    float v417 = v4 * v414;
    int64_t v442 = v2 * 16;
    int64_t v450 = v2 * 8;
    int64_t v461 = v2 * 9;
    int64_t v477 = v2 * 17;
    int64_t v488 = v2 * 18;
    int64_t v496 = v2 * 10;
    int64_t v504 = v2 * 2;
    int64_t v515 = v2 * 3;
    int64_t v523 = v2 * 19;
    int64_t v531 = v2 * 11;
    int64_t v542 = v2 * 12;
    int64_t v550 = v2 * 4;
    int64_t v558 = v2 * 20;
    int64_t v569 = v2 * 21;
    int64_t v577 = v2 * 13;
    int64_t v585 = v2 * 5;
    int64_t v596 = v2 * 6;
    int64_t v604 = v2 * 22;
    int64_t v612 = v2 * 14;
    int64_t v623 = v2 * 15;
    int64_t v631 = v2 * 7;
    int64_t v639 = v2 * 23;
    const float32x2_t *v671 = &v5[0];
    svfloat32_t v870 = svdup_n_f32(v268);
    svfloat32_t v875 = svdup_n_f32(v321);
    svfloat32_t v878 = svdup_n_f32(v340);
    svfloat32_t v884 = svdup_n_f32(v404);
    svfloat32_t v885 = svdup_n_f32(v414);
    int32_t *v894 = &v6[0];
    svfloat32_t v1125 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v743)[0]));
    const float32x2_t *v652 = &v5[v19];
    const float32x2_t *v661 = &v5[v26];
    const float32x2_t *v680 = &v5[v43];
    const float32x2_t *v689 = &v5[v50];
    const float32x2_t *v698 = &v5[v59];
    const float32x2_t *v707 = &v5[v67];
    const float32x2_t *v716 = &v5[v74];
    const float32x2_t *v725 = &v5[v83];
    const float32x2_t *v734 = &v5[v91];
    const float32x2_t *v752 = &v5[v107];
    const float32x2_t *v761 = &v5[v115];
    const float32x2_t *v770 = &v5[v122];
    const float32x2_t *v779 = &v5[v131];
    const float32x2_t *v788 = &v5[v139];
    const float32x2_t *v797 = &v5[v146];
    const float32x2_t *v806 = &v5[v155];
    const float32x2_t *v815 = &v5[v163];
    const float32x2_t *v824 = &v5[v170];
    const float32x2_t *v833 = &v5[v179];
    const float32x2_t *v842 = &v5[v187];
    const float32x2_t *v851 = &v5[v194];
    const float32x2_t *v860 = &v5[v203];
    svfloat32_t v868 = svdup_n_f32(v257);
    svfloat32_t v869 = svdup_n_f32(v264);
    svfloat32_t v876 = svdup_n_f32(v329);
    svfloat32_t v877 = svdup_n_f32(v336);
    svfloat32_t v883 = svdup_n_f32(v400);
    svfloat32_t v886 = svdup_n_f32(v417);
    int32_t *v903 = &v6[v442];
    int32_t *v912 = &v6[v450];
    int32_t *v921 = &v6[v461];
    int32_t *v939 = &v6[v477];
    int32_t *v948 = &v6[v488];
    int32_t *v957 = &v6[v496];
    int32_t *v966 = &v6[v504];
    int32_t *v975 = &v6[v515];
    int32_t *v984 = &v6[v523];
    int32_t *v993 = &v6[v531];
    int32_t *v1002 = &v6[v542];
    int32_t *v1011 = &v6[v550];
    int32_t *v1020 = &v6[v558];
    int32_t *v1029 = &v6[v569];
    int32_t *v1038 = &v6[v577];
    int32_t *v1047 = &v6[v585];
    int32_t *v1056 = &v6[v596];
    int32_t *v1065 = &v6[v604];
    int32_t *v1074 = &v6[v612];
    int32_t *v1083 = &v6[v623];
    int32_t *v1092 = &v6[v631];
    int32_t *v1101 = &v6[v639];
    svfloat32_t v1109 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v671)[0]));
    svfloat32_t v1105 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v652)[0]));
    svfloat32_t v1107 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v661)[0]));
    svfloat32_t v1111 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v680)[0]));
    svfloat32_t v1113 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v689)[0]));
    svfloat32_t v1115 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v698)[0]));
    svfloat32_t v1117 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v707)[0]));
    svfloat32_t v1119 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v716)[0]));
    svfloat32_t v1121 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v725)[0]));
    svfloat32_t v1123 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v734)[0]));
    svfloat32_t v1127 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v752)[0]));
    svfloat32_t v1129 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v761)[0]));
    svfloat32_t v1131 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v770)[0]));
    svfloat32_t v1133 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v779)[0]));
    svfloat32_t v1135 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v788)[0]));
    svfloat32_t v1137 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v797)[0]));
    svfloat32_t v1139 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v806)[0]));
    svfloat32_t v1141 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v815)[0]));
    svfloat32_t v1143 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v824)[0]));
    svfloat32_t v1145 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v833)[0]));
    svfloat32_t v1147 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v842)[0]));
    svfloat32_t v1149 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v851)[0]));
    svfloat32_t v1151 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v860)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v1105, v1107);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v1105, v1107);
    svfloat32_t v56 = svadd_f32_x(svptrue_b32(), v1111, v1113);
    svfloat32_t v57 = svsub_f32_x(svptrue_b32(), v1111, v1113);
    svfloat32_t v80 = svadd_f32_x(svptrue_b32(), v1117, v1119);
    svfloat32_t v81 = svsub_f32_x(svptrue_b32(), v1117, v1119);
    svfloat32_t v104 = svadd_f32_x(svptrue_b32(), v1123, v1125);
    svfloat32_t v105 = svsub_f32_x(svptrue_b32(), v1123, v1125);
    svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v1129, v1131);
    svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v1129, v1131);
    svfloat32_t v152 = svadd_f32_x(svptrue_b32(), v1135, v1137);
    svfloat32_t v153 = svsub_f32_x(svptrue_b32(), v1135, v1137);
    svfloat32_t v176 = svadd_f32_x(svptrue_b32(), v1141, v1143);
    svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v1141, v1143);
    svfloat32_t v200 = svadd_f32_x(svptrue_b32(), v1147, v1149);
    svfloat32_t v201 = svsub_f32_x(svptrue_b32(), v1147, v1149);
    svfloat32_t v41 = svadd_f32_x(svptrue_b32(), v32, v1109);
    svfloat32_t v65 = svadd_f32_x(svptrue_b32(), v56, v1115);
    svfloat32_t v89 = svadd_f32_x(svptrue_b32(), v80, v1121);
    svfloat32_t v113 = svadd_f32_x(svptrue_b32(), v104, v1127);
    svfloat32_t v137 = svadd_f32_x(svptrue_b32(), v128, v1133);
    svfloat32_t v161 = svadd_f32_x(svptrue_b32(), v152, v1139);
    svfloat32_t v185 = svadd_f32_x(svptrue_b32(), v176, v1145);
    svfloat32_t v209 = svadd_f32_x(svptrue_b32(), v200, v1151);
    svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v32, v128);
    svfloat32_t v283 = svsub_f32_x(svptrue_b32(), v32, v128);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v80, v176);
    svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v80, v176);
    svfloat32_t v286 = svadd_f32_x(svptrue_b32(), v56, v152);
    svfloat32_t v287 = svsub_f32_x(svptrue_b32(), v56, v152);
    svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v104, v200);
    svfloat32_t v289 = svsub_f32_x(svptrue_b32(), v104, v200);
    svfloat32_t v354 = svadd_f32_x(svptrue_b32(), v33, v129);
    svfloat32_t v355 = svsub_f32_x(svptrue_b32(), v33, v129);
    svfloat32_t v356 = svadd_f32_x(svptrue_b32(), v81, v177);
    svfloat32_t v357 = svsub_f32_x(svptrue_b32(), v81, v177);
    svfloat32_t v358 = svadd_f32_x(svptrue_b32(), v57, v153);
    svfloat32_t v359 = svsub_f32_x(svptrue_b32(), v57, v153);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v105, v201);
    svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v105, v201);
    svfloat32_t v210 = svadd_f32_x(svptrue_b32(), v41, v137);
    svfloat32_t v211 = svsub_f32_x(svptrue_b32(), v41, v137);
    svfloat32_t v212 = svadd_f32_x(svptrue_b32(), v89, v185);
    svfloat32_t v213 = svsub_f32_x(svptrue_b32(), v89, v185);
    svfloat32_t v214 = svadd_f32_x(svptrue_b32(), v65, v161);
    svfloat32_t v215 = svsub_f32_x(svptrue_b32(), v65, v161);
    svfloat32_t v216 = svadd_f32_x(svptrue_b32(), v113, v209);
    svfloat32_t v217 = svsub_f32_x(svptrue_b32(), v113, v209);
    svfloat32_t v290 = svadd_f32_x(svptrue_b32(), v282, v284);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v282, v284);
    svfloat32_t v292 = svadd_f32_x(svptrue_b32(), v286, v288);
    svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v286, v288);
    svfloat32_t v296 = svadd_f32_x(svptrue_b32(), v287, v289);
    svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v287, v289);
    svfloat32_t zero331 = svdup_n_f32(0);
    svfloat32_t v331 = svcmla_f32_x(pred_full, zero331, v876, v285, 90);
    svfloat32_t v362 = svadd_f32_x(svptrue_b32(), v354, v356);
    svfloat32_t v363 = svsub_f32_x(svptrue_b32(), v354, v356);
    svfloat32_t v364 = svadd_f32_x(svptrue_b32(), v358, v360);
    svfloat32_t v365 = svsub_f32_x(svptrue_b32(), v358, v360);
    svfloat32_t v368 = svadd_f32_x(svptrue_b32(), v359, v361);
    svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v359, v361);
    svfloat32_t zero402 = svdup_n_f32(0);
    svfloat32_t v402 = svcmla_f32_x(pred_full, zero402, v883, v355, 90);
    svfloat32_t v218 = svadd_f32_x(svptrue_b32(), v210, v212);
    svfloat32_t v219 = svsub_f32_x(svptrue_b32(), v210, v212);
    svfloat32_t v220 = svadd_f32_x(svptrue_b32(), v214, v216);
    svfloat32_t v221 = svsub_f32_x(svptrue_b32(), v214, v216);
    svfloat32_t v224 = svadd_f32_x(svptrue_b32(), v215, v217);
    svfloat32_t v225 = svsub_f32_x(svptrue_b32(), v215, v217);
    svfloat32_t zero259 = svdup_n_f32(0);
    svfloat32_t v259 = svcmla_f32_x(pred_full, zero259, v868, v213, 90);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v290, v292);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v290, v292);
    svfloat32_t zero319 = svdup_n_f32(0);
    svfloat32_t v319 = svcmla_f32_x(pred_full, zero319, v876, v293, 90);
    svfloat32_t zero338 = svdup_n_f32(0);
    svfloat32_t v338 = svcmla_f32_x(pred_full, zero338, v877, v296, 90);
    svfloat32_t v343 = svmul_f32_x(svptrue_b32(), v297, v878);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v362, v364);
    svfloat32_t v367 = svsub_f32_x(svptrue_b32(), v362, v364);
    svfloat32_t zero390 = svdup_n_f32(0);
    svfloat32_t v390 = svcmla_f32_x(pred_full, zero390, v883, v363, 90);
    svfloat32_t v412 = svmul_f32_x(svptrue_b32(), v368, v885);
    svfloat32_t zero419 = svdup_n_f32(0);
    svfloat32_t v419 = svcmla_f32_x(pred_full, zero419, v886, v369, 90);
    svfloat32_t v222 = svadd_f32_x(svptrue_b32(), v218, v220);
    svfloat32_t v223 = svsub_f32_x(svptrue_b32(), v218, v220);
    svfloat32_t zero247 = svdup_n_f32(0);
    svfloat32_t v247 = svcmla_f32_x(pred_full, zero247, v868, v221, 90);
    svfloat32_t zero266 = svdup_n_f32(0);
    svfloat32_t v266 = svcmla_f32_x(pred_full, zero266, v869, v224, 90);
    svfloat32_t v344 = svmla_f32_x(pred_full, v319, v291, v875);
    svfloat32_t v345 = svnmls_f32_x(pred_full, v319, v291, v875);
    svfloat32_t v346 = svmla_f32_x(pred_full, v343, v283, v875);
    svfloat32_t v347 = svnmls_f32_x(pred_full, v343, v283, v875);
    svfloat32_t v348 = svadd_f32_x(svptrue_b32(), v331, v338);
    svfloat32_t v349 = svsub_f32_x(svptrue_b32(), v331, v338);
    svfloat32_t zero376 = svdup_n_f32(0);
    svfloat32_t v376 = svcmla_f32_x(pred_full, zero376, v883, v366, 90);
    svfloat32_t zero383 = svdup_n_f32(0);
    svfloat32_t v383 = svcmla_f32_x(pred_full, zero383, v883, v367, 90);
    svfloat32_t v420 = svmla_f32_x(pred_full, v390, v365, v884);
    svfloat32_t v421 = svmls_f32_x(pred_full, v390, v365, v884);
    svfloat32_t v422 = svadd_f32_x(svptrue_b32(), v402, v419);
    svfloat32_t v423 = svsub_f32_x(svptrue_b32(), v402, v419);
    svfloat32_t v424 = svmla_f32_x(pred_full, v412, v357, v884);
    svfloat32_t v425 = svnmls_f32_x(pred_full, v412, v357, v884);
    svfloat32_t v272 = svadd_f32_x(svptrue_b32(), v219, v247);
    svfloat32_t v273 = svsub_f32_x(svptrue_b32(), v219, v247);
    svfloat32_t v274 = svmla_f32_x(pred_full, v211, v225, v870);
    svfloat32_t v275 = svmls_f32_x(pred_full, v211, v225, v870);
    svfloat32_t v276 = svadd_f32_x(svptrue_b32(), v259, v266);
    svfloat32_t v277 = svsub_f32_x(svptrue_b32(), v259, v266);
    svfloat32_t v350 = svadd_f32_x(svptrue_b32(), v346, v348);
    svfloat32_t v351 = svsub_f32_x(svptrue_b32(), v346, v348);
    svfloat32_t v352 = svadd_f32_x(svptrue_b32(), v347, v349);
    svfloat32_t v353 = svsub_f32_x(svptrue_b32(), v347, v349);
    svfloat32_t v426 = svadd_f32_x(svptrue_b32(), v422, v424);
    svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v422, v424);
    svfloat32_t v428 = svadd_f32_x(svptrue_b32(), v423, v425);
    svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v423, v425);
    svfloat32_t v430 = svmla_f32_x(pred_full, v222, v294, v875);
    svint16_t v435 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v222, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v538 = svmla_f32_x(pred_full, v223, v295, v875);
    svint16_t v543 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v223, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v278 = svadd_f32_x(svptrue_b32(), v274, v276);
    svfloat32_t v279 = svsub_f32_x(svptrue_b32(), v274, v276);
    svfloat32_t v280 = svadd_f32_x(svptrue_b32(), v275, v277);
    svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v275, v277);
    svfloat32_t v431 = svadd_f32_x(svptrue_b32(), v430, v376);
    svfloat32_t v432 = svsub_f32_x(svptrue_b32(), v430, v376);
    svfloat32_t v484 = svadd_f32_x(svptrue_b32(), v273, v345);
    svint16_t v489 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v273, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v539 = svadd_f32_x(svptrue_b32(), v538, v383);
    svfloat32_t v540 = svsub_f32_x(svptrue_b32(), v538, v383);
    svfloat32_t v592 = svadd_f32_x(svptrue_b32(), v272, v344);
    svint16_t v597 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v272, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v894), svreinterpret_u64_s16(v435));
    svst1w_u64(pred_full, (unsigned *)(v1002), svreinterpret_u64_s16(v543));
    svint16_t v443 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v432, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v451 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v431, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v457 = svadd_f32_x(svptrue_b32(), v279, v351);
    svint16_t v462 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v279, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v485 = svadd_f32_x(svptrue_b32(), v484, v421);
    svfloat32_t v486 = svsub_f32_x(svptrue_b32(), v484, v421);
    svfloat32_t v511 = svadd_f32_x(svptrue_b32(), v280, v352);
    svint16_t v516 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v280, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v551 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v540, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v559 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v539, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v565 = svadd_f32_x(svptrue_b32(), v281, v353);
    svint16_t v570 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v281, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v593 = svadd_f32_x(svptrue_b32(), v592, v420);
    svfloat32_t v594 = svsub_f32_x(svptrue_b32(), v592, v420);
    svfloat32_t v619 = svadd_f32_x(svptrue_b32(), v278, v350);
    svint16_t v624 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v278, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v948), svreinterpret_u64_s16(v489));
    svst1w_u64(pred_full, (unsigned *)(v1056), svreinterpret_u64_s16(v597));
    svfloat32_t v458 = svadd_f32_x(svptrue_b32(), v457, v427);
    svfloat32_t v459 = svsub_f32_x(svptrue_b32(), v457, v427);
    svint16_t v497 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v486, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v505 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v485, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v512 = svadd_f32_x(svptrue_b32(), v511, v428);
    svfloat32_t v513 = svsub_f32_x(svptrue_b32(), v511, v428);
    svfloat32_t v566 = svadd_f32_x(svptrue_b32(), v565, v429);
    svfloat32_t v567 = svsub_f32_x(svptrue_b32(), v565, v429);
    svint16_t v605 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v594, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v613 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v593, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v620 = svadd_f32_x(svptrue_b32(), v619, v426);
    svfloat32_t v621 = svsub_f32_x(svptrue_b32(), v619, v426);
    svst1w_u64(pred_full, (unsigned *)(v903), svreinterpret_u64_s16(v443));
    svst1w_u64(pred_full, (unsigned *)(v912), svreinterpret_u64_s16(v451));
    svst1w_u64(pred_full, (unsigned *)(v921), svreinterpret_u64_s16(v462));
    svst1w_u64(pred_full, (unsigned *)(v975), svreinterpret_u64_s16(v516));
    svst1w_u64(pred_full, (unsigned *)(v1011), svreinterpret_u64_s16(v551));
    svst1w_u64(pred_full, (unsigned *)(v1020), svreinterpret_u64_s16(v559));
    svst1w_u64(pred_full, (unsigned *)(v1029), svreinterpret_u64_s16(v570));
    svst1w_u64(pred_full, (unsigned *)(v1083), svreinterpret_u64_s16(v624));
    svint16_t v470 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v459, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v478 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v458, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v524 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v513, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v532 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v512, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v578 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v567, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v586 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v566, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v632 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v621, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v640 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v620, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v957), svreinterpret_u64_s16(v497));
    svst1w_u64(pred_full, (unsigned *)(v966), svreinterpret_u64_s16(v505));
    svst1w_u64(pred_full, (unsigned *)(v1065), svreinterpret_u64_s16(v605));
    svst1w_u64(pred_full, (unsigned *)(v1074), svreinterpret_u64_s16(v613));
    svst1w_u64(pred_full, (unsigned *)(v930), svreinterpret_u64_s16(v470));
    svst1w_u64(pred_full, (unsigned *)(v939), svreinterpret_u64_s16(v478));
    svst1w_u64(pred_full, (unsigned *)(v984), svreinterpret_u64_s16(v524));
    svst1w_u64(pred_full, (unsigned *)(v993), svreinterpret_u64_s16(v532));
    svst1w_u64(pred_full, (unsigned *)(v1038), svreinterpret_u64_s16(v578));
    svst1w_u64(pred_full, (unsigned *)(v1047), svreinterpret_u64_s16(v586));
    svst1w_u64(pred_full, (unsigned *)(v1092), svreinterpret_u64_s16(v632));
    svst1w_u64(pred_full, (unsigned *)(v1101), svreinterpret_u64_s16(v640));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu25(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v159 = v5[istride];
    float v856 = 9.6858316112863108e-01F;
    float v859 = -2.4868988716485479e-01F;
    float v860 = 2.4868988716485479e-01F;
    float v1000 = 8.7630668004386358e-01F;
    float v1003 = -4.8175367410171532e-01F;
    float v1004 = 4.8175367410171532e-01F;
    float v1144 = 7.2896862742141155e-01F;
    float v1147 = -6.8454710592868862e-01F;
    float v1148 = 6.8454710592868862e-01F;
    float v1156 = 6.2790519529313527e-02F;
    float v1159 = -9.9802672842827156e-01F;
    float v1160 = 9.9802672842827156e-01F;
    float v1288 = 5.3582679497899655e-01F;
    float v1291 = -8.4432792550201508e-01F;
    float v1292 = 8.4432792550201508e-01F;
    float v1300 = -4.2577929156507272e-01F;
    float v1303 = -9.0482705246601947e-01F;
    float v1304 = 9.0482705246601947e-01F;
    float v1312 = -6.3742398974868952e-01F;
    float v1315 = 7.7051324277578936e-01F;
    float v1316 = -7.7051324277578936e-01F;
    float v1330 = -9.9211470131447776e-01F;
    float v1333 = -1.2533323356430454e-01F;
    float v1334 = 1.2533323356430454e-01F;
    float v1350 = 2.5000000000000000e-01F;
    float v1360 = 5.5901699437494745e-01F;
    float v1370 = 6.1803398874989490e-01F;
    float v1395 = 9.5105651629515353e-01F;
    float v1396 = -9.5105651629515353e-01F;
    float32x2_t v1398 = (float32x2_t){v4, v4};
    float v1421 = 2.0000000000000000e+00F;
    float32x2_t v20 = v5[0];
    float32x2_t v857 = (float32x2_t){v856, v856};
    float32x2_t v861 = (float32x2_t){v859, v860};
    float32x2_t v1001 = (float32x2_t){v1000, v1000};
    float32x2_t v1005 = (float32x2_t){v1003, v1004};
    float32x2_t v1145 = (float32x2_t){v1144, v1144};
    float32x2_t v1149 = (float32x2_t){v1147, v1148};
    float32x2_t v1157 = (float32x2_t){v1156, v1156};
    float32x2_t v1161 = (float32x2_t){v1159, v1160};
    float32x2_t v1191 = (float32x2_t){v1316, v1315};
    float32x2_t v1289 = (float32x2_t){v1288, v1288};
    float32x2_t v1293 = (float32x2_t){v1291, v1292};
    float32x2_t v1301 = (float32x2_t){v1300, v1300};
    float32x2_t v1305 = (float32x2_t){v1303, v1304};
    float32x2_t v1313 = (float32x2_t){v1312, v1312};
    float32x2_t v1317 = (float32x2_t){v1315, v1316};
    float32x2_t v1331 = (float32x2_t){v1330, v1330};
    float32x2_t v1335 = (float32x2_t){v1333, v1334};
    float32x2_t v1351 = (float32x2_t){v1350, v1350};
    float32x2_t v1361 = (float32x2_t){v1360, v1360};
    float32x2_t v1371 = (float32x2_t){v1370, v1370};
    float32x2_t v1397 = (float32x2_t){v1395, v1396};
    float32x2_t v1422 = (float32x2_t){v1421, v1421};
    float32x2_t v25 = v5[istride * 5];
    float32x2_t v30 = v5[istride * 10];
    float32x2_t v35 = v5[istride * 15];
    float32x2_t v40 = v5[istride * 20];
    float32x2_t v164 = v5[istride * 6];
    float32x2_t v169 = v5[istride * 11];
    float32x2_t v174 = v5[istride * 16];
    float32x2_t v179 = v5[istride * 21];
    float32x2_t v298 = v5[istride * 2];
    float32x2_t v303 = v5[istride * 7];
    float32x2_t v308 = v5[istride * 12];
    float32x2_t v313 = v5[istride * 17];
    float32x2_t v318 = v5[istride * 22];
    float32x2_t v437 = v5[istride * 3];
    float32x2_t v442 = v5[istride * 8];
    float32x2_t v447 = v5[istride * 13];
    float32x2_t v452 = v5[istride * 18];
    float32x2_t v457 = v5[istride * 23];
    float32x2_t v576 = v5[istride * 4];
    float32x2_t v581 = v5[istride * 9];
    float32x2_t v586 = v5[istride * 14];
    float32x2_t v591 = v5[istride * 19];
    float32x2_t v596 = v5[istride * 24];
    float32x2_t v863 = vmul_f32(v1398, v861);
    float32x2_t v1007 = vmul_f32(v1398, v1005);
    float32x2_t v1151 = vmul_f32(v1398, v1149);
    float32x2_t v1163 = vmul_f32(v1398, v1161);
    float32x2_t v1193 = vmul_f32(v1398, v1191);
    float32x2_t v1295 = vmul_f32(v1398, v1293);
    float32x2_t v1307 = vmul_f32(v1398, v1305);
    float32x2_t v1319 = vmul_f32(v1398, v1317);
    float32x2_t v1337 = vmul_f32(v1398, v1335);
    float32x2_t v1399 = vmul_f32(v1398, v1397);
    float32x2_t v77 = vsub_f32(v25, v40);
    float32x2_t v81 = vmul_f32(v25, v1422);
    float32x2_t v95 = vsub_f32(v30, v35);
    float32x2_t v99 = vmul_f32(v30, v1422);
    float32x2_t v216 = vsub_f32(v164, v179);
    float32x2_t v220 = vmul_f32(v164, v1422);
    float32x2_t v234 = vsub_f32(v169, v174);
    float32x2_t v238 = vmul_f32(v169, v1422);
    float32x2_t v355 = vsub_f32(v303, v318);
    float32x2_t v359 = vmul_f32(v303, v1422);
    float32x2_t v373 = vsub_f32(v308, v313);
    float32x2_t v377 = vmul_f32(v308, v1422);
    float32x2_t v494 = vsub_f32(v442, v457);
    float32x2_t v498 = vmul_f32(v442, v1422);
    float32x2_t v512 = vsub_f32(v447, v452);
    float32x2_t v516 = vmul_f32(v447, v1422);
    float32x2_t v633 = vsub_f32(v581, v596);
    float32x2_t v637 = vmul_f32(v581, v1422);
    float32x2_t v651 = vsub_f32(v586, v591);
    float32x2_t v655 = vmul_f32(v586, v1422);
    float32x2_t v82 = vsub_f32(v81, v77);
    float32x2_t v100 = vsub_f32(v99, v95);
    float32x2_t v111 = vmul_f32(v95, v1371);
    float32x2_t v126 = vmul_f32(v77, v1371);
    float32x2_t v221 = vsub_f32(v220, v216);
    float32x2_t v239 = vsub_f32(v238, v234);
    float32x2_t v250 = vmul_f32(v234, v1371);
    float32x2_t v265 = vmul_f32(v216, v1371);
    float32x2_t v360 = vsub_f32(v359, v355);
    float32x2_t v378 = vsub_f32(v377, v373);
    float32x2_t v389 = vmul_f32(v373, v1371);
    float32x2_t v404 = vmul_f32(v355, v1371);
    float32x2_t v499 = vsub_f32(v498, v494);
    float32x2_t v517 = vsub_f32(v516, v512);
    float32x2_t v528 = vmul_f32(v512, v1371);
    float32x2_t v543 = vmul_f32(v494, v1371);
    float32x2_t v638 = vsub_f32(v637, v633);
    float32x2_t v656 = vsub_f32(v655, v651);
    float32x2_t v667 = vmul_f32(v651, v1371);
    float32x2_t v682 = vmul_f32(v633, v1371);
    float32x2_t v101 = vadd_f32(v82, v100);
    float32x2_t v102 = vsub_f32(v82, v100);
    float32x2_t v112 = vadd_f32(v77, v111);
    float32x2_t v127 = vsub_f32(v126, v95);
    float32x2_t v240 = vadd_f32(v221, v239);
    float32x2_t v241 = vsub_f32(v221, v239);
    float32x2_t v251 = vadd_f32(v216, v250);
    float32x2_t v266 = vsub_f32(v265, v234);
    float32x2_t v379 = vadd_f32(v360, v378);
    float32x2_t v380 = vsub_f32(v360, v378);
    float32x2_t v390 = vadd_f32(v355, v389);
    float32x2_t v405 = vsub_f32(v404, v373);
    float32x2_t v518 = vadd_f32(v499, v517);
    float32x2_t v519 = vsub_f32(v499, v517);
    float32x2_t v529 = vadd_f32(v494, v528);
    float32x2_t v544 = vsub_f32(v543, v512);
    float32x2_t v657 = vadd_f32(v638, v656);
    float32x2_t v658 = vsub_f32(v638, v656);
    float32x2_t v668 = vadd_f32(v633, v667);
    float32x2_t v683 = vsub_f32(v682, v651);
    float32x2_t v106 = vmul_f32(v101, v1351);
    float32x2_t v116 = vmul_f32(v102, v1361);
    float32x2_t v128 = vadd_f32(v20, v101);
    float32x2_t v134 = vrev64_f32(v112);
    float32x2_t v142 = vrev64_f32(v127);
    float32x2_t v245 = vmul_f32(v240, v1351);
    float32x2_t v255 = vmul_f32(v241, v1361);
    float32x2_t v267 = vadd_f32(v159, v240);
    float32x2_t v273 = vrev64_f32(v251);
    float32x2_t v281 = vrev64_f32(v266);
    float32x2_t v384 = vmul_f32(v379, v1351);
    float32x2_t v394 = vmul_f32(v380, v1361);
    float32x2_t v406 = vadd_f32(v298, v379);
    float32x2_t v412 = vrev64_f32(v390);
    float32x2_t v420 = vrev64_f32(v405);
    float32x2_t v523 = vmul_f32(v518, v1351);
    float32x2_t v533 = vmul_f32(v519, v1361);
    float32x2_t v545 = vadd_f32(v437, v518);
    float32x2_t v551 = vrev64_f32(v529);
    float32x2_t v559 = vrev64_f32(v544);
    float32x2_t v662 = vmul_f32(v657, v1351);
    float32x2_t v672 = vmul_f32(v658, v1361);
    float32x2_t v684 = vadd_f32(v576, v657);
    float32x2_t v690 = vrev64_f32(v668);
    float32x2_t v698 = vrev64_f32(v683);
    float32x2_t v107 = vsub_f32(v20, v106);
    float32x2_t v135 = vmul_f32(v134, v1399);
    float32x2_t v143 = vmul_f32(v142, v1399);
    float32x2_t v246 = vsub_f32(v159, v245);
    float32x2_t v274 = vmul_f32(v273, v1399);
    float32x2_t v282 = vmul_f32(v281, v1399);
    float32x2_t v385 = vsub_f32(v298, v384);
    float32x2_t v413 = vmul_f32(v412, v1399);
    float32x2_t v421 = vmul_f32(v420, v1399);
    float32x2_t v524 = vsub_f32(v437, v523);
    float32x2_t v552 = vmul_f32(v551, v1399);
    float32x2_t v560 = vmul_f32(v559, v1399);
    float32x2_t v663 = vsub_f32(v576, v662);
    float32x2_t v691 = vmul_f32(v690, v1399);
    float32x2_t v699 = vmul_f32(v698, v1399);
    float32x2_t v747 = vsub_f32(v267, v684);
    float32x2_t v751 = vmul_f32(v267, v1422);
    float32x2_t v765 = vsub_f32(v406, v545);
    float32x2_t v769 = vmul_f32(v406, v1422);
    float32x2_t v117 = vsub_f32(v107, v116);
    float32x2_t v121 = vmul_f32(v107, v1422);
    float32x2_t v256 = vsub_f32(v246, v255);
    float32x2_t v260 = vmul_f32(v246, v1422);
    float32x2_t v395 = vsub_f32(v385, v394);
    float32x2_t v399 = vmul_f32(v385, v1422);
    float32x2_t v534 = vsub_f32(v524, v533);
    float32x2_t v538 = vmul_f32(v524, v1422);
    float32x2_t v673 = vsub_f32(v663, v672);
    float32x2_t v677 = vmul_f32(v663, v1422);
    float32x2_t v752 = vsub_f32(v751, v747);
    float32x2_t v770 = vsub_f32(v769, v765);
    float32x2_t v781 = vmul_f32(v765, v1371);
    float32x2_t v796 = vmul_f32(v747, v1371);
    float32x2_t v122 = vsub_f32(v121, v117);
    float32x2_t v144 = vsub_f32(v117, v143);
    float32x2_t v148 = vmul_f32(v117, v1422);
    float32x2_t v261 = vsub_f32(v260, v256);
    float32x2_t v283 = vsub_f32(v256, v282);
    float32x2_t v287 = vmul_f32(v256, v1422);
    float32x2_t v400 = vsub_f32(v399, v395);
    float32x2_t v422 = vsub_f32(v395, v421);
    float32x2_t v426 = vmul_f32(v395, v1422);
    float32x2_t v539 = vsub_f32(v538, v534);
    float32x2_t v561 = vsub_f32(v534, v560);
    float32x2_t v565 = vmul_f32(v534, v1422);
    float32x2_t v678 = vsub_f32(v677, v673);
    float32x2_t v700 = vsub_f32(v673, v699);
    float32x2_t v704 = vmul_f32(v673, v1422);
    float32x2_t v771 = vadd_f32(v752, v770);
    float32x2_t v772 = vsub_f32(v752, v770);
    float32x2_t v782 = vadd_f32(v747, v781);
    float32x2_t v797 = vsub_f32(v796, v765);
    float32x2_t v136 = vsub_f32(v122, v135);
    float32x2_t v149 = vsub_f32(v148, v144);
    float32x2_t v153 = vmul_f32(v122, v1422);
    float32x2_t v275 = vsub_f32(v261, v274);
    float32x2_t v288 = vsub_f32(v287, v283);
    float32x2_t v292 = vmul_f32(v261, v1422);
    float32x2_t v414 = vsub_f32(v400, v413);
    float32x2_t v427 = vsub_f32(v426, v422);
    float32x2_t v431 = vmul_f32(v400, v1422);
    float32x2_t v553 = vsub_f32(v539, v552);
    float32x2_t v566 = vsub_f32(v565, v561);
    float32x2_t v570 = vmul_f32(v539, v1422);
    float32x2_t v692 = vsub_f32(v678, v691);
    float32x2_t v705 = vsub_f32(v704, v700);
    float32x2_t v709 = vmul_f32(v678, v1422);
    float32x2_t v776 = vmul_f32(v771, v1351);
    float32x2_t v786 = vmul_f32(v772, v1361);
    float32x2_t v798 = vadd_f32(v128, v771);
    float32x2_t v810 = vrev64_f32(v782);
    float32x2_t v824 = vrev64_f32(v797);
    float32x2_t v1008 = vrev64_f32(v283);
    float32x2_t v1020 = vrev64_f32(v422);
    float32x2_t v1032 = vrev64_f32(v700);
    float32x2_t v1050 = vrev64_f32(v561);
    float32x2_t v154 = vsub_f32(v153, v136);
    float32x2_t v293 = vsub_f32(v292, v275);
    float32x2_t v432 = vsub_f32(v431, v414);
    float32x2_t v571 = vsub_f32(v570, v553);
    float32x2_t v710 = vsub_f32(v709, v692);
    float32x2_t v777 = vsub_f32(v128, v776);
    int16x4_t v801 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v798, 15), (int32x2_t){0, 0}));
    float32x2_t v811 = vmul_f32(v810, v1399);
    float32x2_t v825 = vmul_f32(v824, v1399);
    float32x2_t v864 = vrev64_f32(v275);
    float32x2_t v876 = vrev64_f32(v414);
    float32x2_t v888 = vrev64_f32(v692);
    float32x2_t v906 = vrev64_f32(v553);
    float32x2_t v1009 = vmul_f32(v1008, v1007);
    float32x2_t v1021 = vmul_f32(v1020, v1295);
    float32x2_t v1033 = vmul_f32(v1032, v1307);
    float32x2_t v1051 = vmul_f32(v1050, v1163);
    float32x2_t v1152 = vrev64_f32(v288);
    float32x2_t v1164 = vrev64_f32(v427);
    float32x2_t v1176 = vrev64_f32(v705);
    float32x2_t v1194 = vrev64_f32(v566);
    float32x2_t v787 = vsub_f32(v777, v786);
    float32x2_t v791 = vmul_f32(v777, v1422);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v801), 0);
    float32x2_t v865 = vmul_f32(v864, v863);
    float32x2_t v877 = vmul_f32(v876, v1007);
    float32x2_t v889 = vmul_f32(v888, v1295);
    float32x2_t v907 = vmul_f32(v906, v1151);
    float32x2_t v1010 = vfma_f32(v1009, v283, v1001);
    float32x2_t v1022 = vfma_f32(v1021, v422, v1289);
    float32x2_t v1034 = vfma_f32(v1033, v700, v1301);
    float32x2_t v1052 = vfma_f32(v1051, v561, v1157);
    float32x2_t v1153 = vmul_f32(v1152, v1151);
    float32x2_t v1165 = vmul_f32(v1164, v1163);
    float32x2_t v1177 = vmul_f32(v1176, v1337);
    float32x2_t v1195 = vmul_f32(v1194, v1193);
    float32x2_t v1296 = vrev64_f32(v293);
    float32x2_t v1308 = vrev64_f32(v432);
    float32x2_t v1320 = vrev64_f32(v710);
    float32x2_t v1338 = vrev64_f32(v571);
    float32x2_t v792 = vsub_f32(v791, v787);
    float32x2_t v826 = vsub_f32(v787, v825);
    float32x2_t v836 = vmul_f32(v787, v1422);
    float32x2_t v866 = vfma_f32(v865, v275, v857);
    float32x2_t v878 = vfma_f32(v877, v414, v1001);
    float32x2_t v890 = vfma_f32(v889, v692, v1289);
    float32x2_t v908 = vfma_f32(v907, v553, v1145);
    float32x2_t v1035 = vsub_f32(v1010, v1034);
    float32x2_t v1039 = vmul_f32(v1010, v1422);
    float32x2_t v1053 = vsub_f32(v1022, v1052);
    float32x2_t v1057 = vmul_f32(v1022, v1422);
    float32x2_t v1154 = vfma_f32(v1153, v288, v1145);
    float32x2_t v1166 = vfma_f32(v1165, v427, v1157);
    float32x2_t v1178 = vfma_f32(v1177, v705, v1331);
    float32x2_t v1196 = vfma_f32(v1195, v566, v1313);
    float32x2_t v1297 = vmul_f32(v1296, v1295);
    float32x2_t v1309 = vmul_f32(v1308, v1307);
    float32x2_t v1321 = vmul_f32(v1320, v1319);
    float32x2_t v1339 = vmul_f32(v1338, v1337);
    float32x2_t v812 = vsub_f32(v792, v811);
    int16x4_t v829 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v826, 15), (int32x2_t){0, 0}));
    float32x2_t v837 = vsub_f32(v836, v826);
    float32x2_t v847 = vmul_f32(v792, v1422);
    float32x2_t v891 = vsub_f32(v866, v890);
    float32x2_t v895 = vmul_f32(v866, v1422);
    float32x2_t v909 = vsub_f32(v878, v908);
    float32x2_t v913 = vmul_f32(v878, v1422);
    float32x2_t v1040 = vsub_f32(v1039, v1035);
    float32x2_t v1058 = vsub_f32(v1057, v1053);
    float32x2_t v1069 = vmul_f32(v1053, v1371);
    float32x2_t v1084 = vmul_f32(v1035, v1371);
    float32x2_t v1179 = vsub_f32(v1154, v1178);
    float32x2_t v1183 = vmul_f32(v1154, v1422);
    float32x2_t v1197 = vsub_f32(v1166, v1196);
    float32x2_t v1201 = vmul_f32(v1166, v1422);
    float32x2_t v1298 = vfma_f32(v1297, v293, v1289);
    float32x2_t v1310 = vfma_f32(v1309, v432, v1301);
    float32x2_t v1322 = vfma_f32(v1321, v710, v1313);
    float32x2_t v1340 = vfma_f32(v1339, v571, v1331);
    int16x4_t v815 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v812, 15), (int32x2_t){0, 0}));
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v829), 0);
    int16x4_t v840 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v837, 15), (int32x2_t){0, 0}));
    float32x2_t v848 = vsub_f32(v847, v812);
    float32x2_t v896 = vsub_f32(v895, v891);
    float32x2_t v914 = vsub_f32(v913, v909);
    float32x2_t v925 = vmul_f32(v909, v1371);
    float32x2_t v940 = vmul_f32(v891, v1371);
    float32x2_t v1059 = vadd_f32(v1040, v1058);
    float32x2_t v1060 = vsub_f32(v1040, v1058);
    float32x2_t v1070 = vadd_f32(v1035, v1069);
    float32x2_t v1085 = vsub_f32(v1084, v1053);
    float32x2_t v1184 = vsub_f32(v1183, v1179);
    float32x2_t v1202 = vsub_f32(v1201, v1197);
    float32x2_t v1213 = vmul_f32(v1197, v1371);
    float32x2_t v1228 = vmul_f32(v1179, v1371);
    float32x2_t v1323 = vsub_f32(v1298, v1322);
    float32x2_t v1327 = vmul_f32(v1298, v1422);
    float32x2_t v1341 = vsub_f32(v1310, v1340);
    float32x2_t v1345 = vmul_f32(v1310, v1422);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v815), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v840), 0);
    int16x4_t v851 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v848, 15), (int32x2_t){0, 0}));
    float32x2_t v915 = vadd_f32(v896, v914);
    float32x2_t v916 = vsub_f32(v896, v914);
    float32x2_t v926 = vadd_f32(v891, v925);
    float32x2_t v941 = vsub_f32(v940, v909);
    float32x2_t v1064 = vmul_f32(v1059, v1351);
    float32x2_t v1074 = vmul_f32(v1060, v1361);
    float32x2_t v1086 = vadd_f32(v144, v1059);
    float32x2_t v1098 = vrev64_f32(v1070);
    float32x2_t v1112 = vrev64_f32(v1085);
    float32x2_t v1203 = vadd_f32(v1184, v1202);
    float32x2_t v1204 = vsub_f32(v1184, v1202);
    float32x2_t v1214 = vadd_f32(v1179, v1213);
    float32x2_t v1229 = vsub_f32(v1228, v1197);
    float32x2_t v1328 = vsub_f32(v1327, v1323);
    float32x2_t v1346 = vsub_f32(v1345, v1341);
    float32x2_t v1357 = vmul_f32(v1341, v1371);
    float32x2_t v1372 = vmul_f32(v1323, v1371);
    v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v851), 0);
    float32x2_t v920 = vmul_f32(v915, v1351);
    float32x2_t v930 = vmul_f32(v916, v1361);
    float32x2_t v942 = vadd_f32(v136, v915);
    float32x2_t v954 = vrev64_f32(v926);
    float32x2_t v968 = vrev64_f32(v941);
    float32x2_t v1065 = vsub_f32(v144, v1064);
    int16x4_t v1089 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1086, 15), (int32x2_t){0, 0}));
    float32x2_t v1099 = vmul_f32(v1098, v1399);
    float32x2_t v1113 = vmul_f32(v1112, v1399);
    float32x2_t v1208 = vmul_f32(v1203, v1351);
    float32x2_t v1218 = vmul_f32(v1204, v1361);
    float32x2_t v1230 = vadd_f32(v149, v1203);
    float32x2_t v1242 = vrev64_f32(v1214);
    float32x2_t v1256 = vrev64_f32(v1229);
    float32x2_t v1347 = vadd_f32(v1328, v1346);
    float32x2_t v1348 = vsub_f32(v1328, v1346);
    float32x2_t v1358 = vadd_f32(v1323, v1357);
    float32x2_t v1373 = vsub_f32(v1372, v1341);
    float32x2_t v921 = vsub_f32(v136, v920);
    int16x4_t v945 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v942, 15), (int32x2_t){0, 0}));
    float32x2_t v955 = vmul_f32(v954, v1399);
    float32x2_t v969 = vmul_f32(v968, v1399);
    float32x2_t v1075 = vsub_f32(v1065, v1074);
    float32x2_t v1079 = vmul_f32(v1065, v1422);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v1089), 0);
    float32x2_t v1209 = vsub_f32(v149, v1208);
    int16x4_t v1233 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1230, 15), (int32x2_t){0, 0}));
    float32x2_t v1243 = vmul_f32(v1242, v1399);
    float32x2_t v1257 = vmul_f32(v1256, v1399);
    float32x2_t v1352 = vmul_f32(v1347, v1351);
    float32x2_t v1362 = vmul_f32(v1348, v1361);
    float32x2_t v1374 = vadd_f32(v154, v1347);
    float32x2_t v1386 = vrev64_f32(v1358);
    float32x2_t v1400 = vrev64_f32(v1373);
    float32x2_t v931 = vsub_f32(v921, v930);
    float32x2_t v935 = vmul_f32(v921, v1422);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v945), 0);
    float32x2_t v1080 = vsub_f32(v1079, v1075);
    float32x2_t v1114 = vsub_f32(v1075, v1113);
    float32x2_t v1124 = vmul_f32(v1075, v1422);
    float32x2_t v1219 = vsub_f32(v1209, v1218);
    float32x2_t v1223 = vmul_f32(v1209, v1422);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v1233), 0);
    float32x2_t v1353 = vsub_f32(v154, v1352);
    int16x4_t v1377 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1374, 15), (int32x2_t){0, 0}));
    float32x2_t v1387 = vmul_f32(v1386, v1399);
    float32x2_t v1401 = vmul_f32(v1400, v1399);
    float32x2_t v936 = vsub_f32(v935, v931);
    float32x2_t v970 = vsub_f32(v931, v969);
    float32x2_t v980 = vmul_f32(v931, v1422);
    float32x2_t v1100 = vsub_f32(v1080, v1099);
    int16x4_t v1117 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1114, 15), (int32x2_t){0, 0}));
    float32x2_t v1125 = vsub_f32(v1124, v1114);
    float32x2_t v1135 = vmul_f32(v1080, v1422);
    float32x2_t v1224 = vsub_f32(v1223, v1219);
    float32x2_t v1258 = vsub_f32(v1219, v1257);
    float32x2_t v1268 = vmul_f32(v1219, v1422);
    float32x2_t v1363 = vsub_f32(v1353, v1362);
    float32x2_t v1367 = vmul_f32(v1353, v1422);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v1377), 0);
    float32x2_t v956 = vsub_f32(v936, v955);
    int16x4_t v973 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v970, 15), (int32x2_t){0, 0}));
    float32x2_t v981 = vsub_f32(v980, v970);
    float32x2_t v991 = vmul_f32(v936, v1422);
    int16x4_t v1103 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1100, 15), (int32x2_t){0, 0}));
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v1117), 0);
    int16x4_t v1128 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1125, 15), (int32x2_t){0, 0}));
    float32x2_t v1136 = vsub_f32(v1135, v1100);
    float32x2_t v1244 = vsub_f32(v1224, v1243);
    int16x4_t v1261 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1258, 15), (int32x2_t){0, 0}));
    float32x2_t v1269 = vsub_f32(v1268, v1258);
    float32x2_t v1279 = vmul_f32(v1224, v1422);
    float32x2_t v1368 = vsub_f32(v1367, v1363);
    float32x2_t v1402 = vsub_f32(v1363, v1401);
    float32x2_t v1412 = vmul_f32(v1363, v1422);
    int16x4_t v959 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v956, 15), (int32x2_t){0, 0}));
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v973), 0);
    int16x4_t v984 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v981, 15), (int32x2_t){0, 0}));
    float32x2_t v992 = vsub_f32(v991, v956);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v1103), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v1128), 0);
    int16x4_t v1139 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1136, 15), (int32x2_t){0, 0}));
    int16x4_t v1247 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1244, 15), (int32x2_t){0, 0}));
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v1261), 0);
    int16x4_t v1272 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1269, 15), (int32x2_t){0, 0}));
    float32x2_t v1280 = vsub_f32(v1279, v1244);
    float32x2_t v1388 = vsub_f32(v1368, v1387);
    int16x4_t v1405 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1402, 15), (int32x2_t){0, 0}));
    float32x2_t v1413 = vsub_f32(v1412, v1402);
    float32x2_t v1423 = vmul_f32(v1368, v1422);
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v959), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v984), 0);
    int16x4_t v995 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v992, 15), (int32x2_t){0, 0}));
    v6[ostride * 22] = vget_lane_s32(vreinterpret_s32_s16(v1139), 0);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v1247), 0);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v1272), 0);
    int16x4_t v1283 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1280, 15), (int32x2_t){0, 0}));
    int16x4_t v1391 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1388, 15), (int32x2_t){0, 0}));
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v1405), 0);
    int16x4_t v1416 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1413, 15), (int32x2_t){0, 0}));
    float32x2_t v1424 = vsub_f32(v1423, v1388);
    v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v995), 0);
    v6[ostride * 23] = vget_lane_s32(vreinterpret_s32_s16(v1283), 0);
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v1391), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v1416), 0);
    int16x4_t v1427 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v1424, 15), (int32x2_t){0, 0}));
    v6[ostride * 24] = vget_lane_s32(vreinterpret_s32_s16(v1427), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu25(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v996 = 9.6858316112863108e-01F;
    float v1001 = 2.4868988716485479e-01F;
    float v1163 = 8.7630668004386358e-01F;
    float v1168 = 4.8175367410171532e-01F;
    float v1330 = 7.2896862742141155e-01F;
    float v1335 = 6.8454710592868862e-01F;
    float v1343 = 6.2790519529313527e-02F;
    float v1348 = 9.9802672842827156e-01F;
    float v1381 = 7.7051324277578925e-01F;
    float v1497 = 5.3582679497899655e-01F;
    float v1502 = 8.4432792550201508e-01F;
    float v1510 = -4.2577929156507272e-01F;
    float v1515 = 9.0482705246601947e-01F;
    float v1523 = -6.3742398974868952e-01F;
    float v1528 = -7.7051324277578936e-01F;
    float v1543 = -9.9211470131447776e-01F;
    float v1548 = 1.2533323356430454e-01F;
    float v1565 = 2.5000000000000000e-01F;
    float v1577 = 5.5901699437494745e-01F;
    float v1589 = 6.1803398874989490e-01F;
    float v1620 = -9.5105651629515353e-01F;
    float v1650 = 2.0000000000000000e+00F;
    const float32x2_t *v1734 = &v5[v0];
    int32_t *v2070 = &v6[v2];
    int64_t v26 = v0 * 5;
    int64_t v33 = v0 * 10;
    int64_t v40 = v0 * 15;
    int64_t v47 = v0 * 20;
    int64_t v188 = v0 * 6;
    int64_t v195 = v0 * 11;
    int64_t v202 = v0 * 16;
    int64_t v209 = v0 * 21;
    int64_t v343 = v0 * 2;
    int64_t v350 = v0 * 7;
    int64_t v357 = v0 * 12;
    int64_t v364 = v0 * 17;
    int64_t v371 = v0 * 22;
    int64_t v505 = v0 * 3;
    int64_t v512 = v0 * 8;
    int64_t v519 = v0 * 13;
    int64_t v526 = v0 * 18;
    int64_t v533 = v0 * 23;
    int64_t v667 = v0 * 4;
    int64_t v674 = v0 * 9;
    int64_t v681 = v0 * 14;
    int64_t v688 = v0 * 19;
    int64_t v695 = v0 * 24;
    int64_t v944 = v2 * 5;
    int64_t v960 = v2 * 10;
    int64_t v974 = v2 * 15;
    int64_t v988 = v2 * 20;
    float v1004 = v4 * v1001;
    int64_t v1111 = v2 * 6;
    int64_t v1127 = v2 * 11;
    int64_t v1141 = v2 * 16;
    int64_t v1155 = v2 * 21;
    float v1171 = v4 * v1168;
    int64_t v1262 = v2 * 2;
    int64_t v1278 = v2 * 7;
    int64_t v1294 = v2 * 12;
    int64_t v1308 = v2 * 17;
    int64_t v1322 = v2 * 22;
    float v1338 = v4 * v1335;
    float v1351 = v4 * v1348;
    float v1384 = v4 * v1381;
    int64_t v1429 = v2 * 3;
    int64_t v1445 = v2 * 8;
    int64_t v1461 = v2 * 13;
    int64_t v1475 = v2 * 18;
    int64_t v1489 = v2 * 23;
    float v1505 = v4 * v1502;
    float v1518 = v4 * v1515;
    float v1531 = v4 * v1528;
    float v1551 = v4 * v1548;
    int64_t v1596 = v2 * 4;
    int64_t v1612 = v2 * 9;
    float v1623 = v4 * v1620;
    int64_t v1628 = v2 * 14;
    int64_t v1642 = v2 * 19;
    int64_t v1656 = v2 * 24;
    const float32x2_t *v1670 = &v5[0];
    svfloat32_t v1992 = svdup_n_f32(0);
    int32_t *v2006 = &v6[0];
    svfloat32_t v2049 = svdup_n_f32(v996);
    svfloat32_t v2113 = svdup_n_f32(v1163);
    svfloat32_t v2177 = svdup_n_f32(v1330);
    svfloat32_t v2179 = svdup_n_f32(v1343);
    svfloat32_t v2241 = svdup_n_f32(v1497);
    svfloat32_t v2243 = svdup_n_f32(v1510);
    svfloat32_t v2245 = svdup_n_f32(v1523);
    svfloat32_t v2248 = svdup_n_f32(v1543);
    svfloat32_t v2251 = svdup_n_f32(v1565);
    svfloat32_t v2253 = svdup_n_f32(v1577);
    svfloat32_t v2255 = svdup_n_f32(v1589);
    svfloat32_t v2295 = svdup_n_f32(v1650);
    svfloat32_t v2340 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1734)[0]));
    const float32x2_t *v1679 = &v5[v26];
    const float32x2_t *v1688 = &v5[v33];
    const float32x2_t *v1697 = &v5[v40];
    const float32x2_t *v1706 = &v5[v47];
    const float32x2_t *v1743 = &v5[v188];
    const float32x2_t *v1752 = &v5[v195];
    const float32x2_t *v1761 = &v5[v202];
    const float32x2_t *v1770 = &v5[v209];
    const float32x2_t *v1798 = &v5[v343];
    const float32x2_t *v1807 = &v5[v350];
    const float32x2_t *v1816 = &v5[v357];
    const float32x2_t *v1825 = &v5[v364];
    const float32x2_t *v1834 = &v5[v371];
    const float32x2_t *v1862 = &v5[v505];
    const float32x2_t *v1871 = &v5[v512];
    const float32x2_t *v1880 = &v5[v519];
    const float32x2_t *v1889 = &v5[v526];
    const float32x2_t *v1898 = &v5[v533];
    const float32x2_t *v1926 = &v5[v667];
    const float32x2_t *v1935 = &v5[v674];
    const float32x2_t *v1944 = &v5[v681];
    const float32x2_t *v1953 = &v5[v688];
    const float32x2_t *v1962 = &v5[v695];
    int32_t *v2016 = &v6[v944];
    int32_t *v2026 = &v6[v960];
    int32_t *v2036 = &v6[v974];
    int32_t *v2046 = &v6[v988];
    svfloat32_t v2050 = svdup_n_f32(v1004);
    int32_t *v2080 = &v6[v1111];
    int32_t *v2090 = &v6[v1127];
    int32_t *v2100 = &v6[v1141];
    int32_t *v2110 = &v6[v1155];
    svfloat32_t v2114 = svdup_n_f32(v1171);
    int32_t *v2134 = &v6[v1262];
    int32_t *v2144 = &v6[v1278];
    int32_t *v2154 = &v6[v1294];
    int32_t *v2164 = &v6[v1308];
    int32_t *v2174 = &v6[v1322];
    svfloat32_t v2178 = svdup_n_f32(v1338);
    svfloat32_t v2180 = svdup_n_f32(v1351);
    svfloat32_t v2185 = svdup_n_f32(v1384);
    int32_t *v2198 = &v6[v1429];
    int32_t *v2208 = &v6[v1445];
    int32_t *v2218 = &v6[v1461];
    int32_t *v2228 = &v6[v1475];
    int32_t *v2238 = &v6[v1489];
    svfloat32_t v2242 = svdup_n_f32(v1505);
    svfloat32_t v2244 = svdup_n_f32(v1518);
    svfloat32_t v2246 = svdup_n_f32(v1531);
    svfloat32_t v2249 = svdup_n_f32(v1551);
    int32_t *v2262 = &v6[v1596];
    int32_t *v2272 = &v6[v1612];
    svfloat32_t v2275 = svdup_n_f32(v1623);
    int32_t *v2282 = &v6[v1628];
    int32_t *v2292 = &v6[v1642];
    int32_t *v2302 = &v6[v1656];
    svfloat32_t v2330 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1670)[0]));
    svfloat32_t v2332 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1679)[0]));
    svfloat32_t v2334 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1688)[0]));
    svfloat32_t v2336 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1697)[0]));
    svfloat32_t v2338 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1706)[0]));
    svfloat32_t v2342 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1743)[0]));
    svfloat32_t v2344 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1752)[0]));
    svfloat32_t v2346 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1761)[0]));
    svfloat32_t v2348 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1770)[0]));
    svfloat32_t v2350 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1798)[0]));
    svfloat32_t v2352 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1807)[0]));
    svfloat32_t v2354 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1816)[0]));
    svfloat32_t v2356 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1825)[0]));
    svfloat32_t v2358 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1834)[0]));
    svfloat32_t v2360 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1862)[0]));
    svfloat32_t v2362 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1871)[0]));
    svfloat32_t v2364 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1880)[0]));
    svfloat32_t v2366 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1889)[0]));
    svfloat32_t v2368 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1898)[0]));
    svfloat32_t v2370 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1926)[0]));
    svfloat32_t v2372 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1935)[0]));
    svfloat32_t v2374 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1944)[0]));
    svfloat32_t v2376 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1953)[0]));
    svfloat32_t v2378 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1962)[0]));
    svfloat32_t v65 = svcmla_f32_x(pred_full, v2332, v1992, v2332, 90);
    svfloat32_t v78 = svcmla_f32_x(pred_full, v2334, v1992, v2334, 90);
    svfloat32_t v91 = svcmla_f32_x(pred_full, v2338, v1992, v2338, 90);
    svfloat32_t v111 = svcmla_f32_x(pred_full, v2336, v1992, v2336, 90);
    svfloat32_t v227 = svcmla_f32_x(pred_full, v2342, v1992, v2342, 90);
    svfloat32_t v240 = svcmla_f32_x(pred_full, v2344, v1992, v2344, 90);
    svfloat32_t v253 = svcmla_f32_x(pred_full, v2348, v1992, v2348, 90);
    svfloat32_t v273 = svcmla_f32_x(pred_full, v2346, v1992, v2346, 90);
    svfloat32_t v389 = svcmla_f32_x(pred_full, v2352, v1992, v2352, 90);
    svfloat32_t v402 = svcmla_f32_x(pred_full, v2354, v1992, v2354, 90);
    svfloat32_t v415 = svcmla_f32_x(pred_full, v2358, v1992, v2358, 90);
    svfloat32_t v435 = svcmla_f32_x(pred_full, v2356, v1992, v2356, 90);
    svfloat32_t v551 = svcmla_f32_x(pred_full, v2362, v1992, v2362, 90);
    svfloat32_t v564 = svcmla_f32_x(pred_full, v2364, v1992, v2364, 90);
    svfloat32_t v577 = svcmla_f32_x(pred_full, v2368, v1992, v2368, 90);
    svfloat32_t v597 = svcmla_f32_x(pred_full, v2366, v1992, v2366, 90);
    svfloat32_t v713 = svcmla_f32_x(pred_full, v2372, v1992, v2372, 90);
    svfloat32_t v726 = svcmla_f32_x(pred_full, v2374, v1992, v2374, 90);
    svfloat32_t v739 = svcmla_f32_x(pred_full, v2378, v1992, v2378, 90);
    svfloat32_t v759 = svcmla_f32_x(pred_full, v2376, v1992, v2376, 90);
    svfloat32_t v92 = svsub_f32_x(svptrue_b32(), v65, v91);
    svfloat32_t v112 = svsub_f32_x(svptrue_b32(), v78, v111);
    svfloat32_t v254 = svsub_f32_x(svptrue_b32(), v227, v253);
    svfloat32_t v274 = svsub_f32_x(svptrue_b32(), v240, v273);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v389, v415);
    svfloat32_t v436 = svsub_f32_x(svptrue_b32(), v402, v435);
    svfloat32_t v578 = svsub_f32_x(svptrue_b32(), v551, v577);
    svfloat32_t v598 = svsub_f32_x(svptrue_b32(), v564, v597);
    svfloat32_t v740 = svsub_f32_x(svptrue_b32(), v713, v739);
    svfloat32_t v760 = svsub_f32_x(svptrue_b32(), v726, v759);
    svfloat32_t v98 = svnmls_f32_x(pred_full, v92, v65, v2295);
    svfloat32_t v118 = svnmls_f32_x(pred_full, v112, v78, v2295);
    svfloat32_t v260 = svnmls_f32_x(pred_full, v254, v227, v2295);
    svfloat32_t v280 = svnmls_f32_x(pred_full, v274, v240, v2295);
    svfloat32_t v422 = svnmls_f32_x(pred_full, v416, v389, v2295);
    svfloat32_t v442 = svnmls_f32_x(pred_full, v436, v402, v2295);
    svfloat32_t v584 = svnmls_f32_x(pred_full, v578, v551, v2295);
    svfloat32_t v604 = svnmls_f32_x(pred_full, v598, v564, v2295);
    svfloat32_t v746 = svnmls_f32_x(pred_full, v740, v713, v2295);
    svfloat32_t v766 = svnmls_f32_x(pred_full, v760, v726, v2295);
    svfloat32_t v119 = svadd_f32_x(svptrue_b32(), v98, v118);
    svfloat32_t v120 = svsub_f32_x(svptrue_b32(), v98, v118);
    svfloat32_t v132 = svmla_f32_x(pred_full, v92, v112, v2255);
    svfloat32_t v150 = svnmls_f32_x(pred_full, v112, v92, v2255);
    svfloat32_t v281 = svadd_f32_x(svptrue_b32(), v260, v280);
    svfloat32_t v282 = svsub_f32_x(svptrue_b32(), v260, v280);
    svfloat32_t v294 = svmla_f32_x(pred_full, v254, v274, v2255);
    svfloat32_t v312 = svnmls_f32_x(pred_full, v274, v254, v2255);
    svfloat32_t v443 = svadd_f32_x(svptrue_b32(), v422, v442);
    svfloat32_t v444 = svsub_f32_x(svptrue_b32(), v422, v442);
    svfloat32_t v456 = svmla_f32_x(pred_full, v416, v436, v2255);
    svfloat32_t v474 = svnmls_f32_x(pred_full, v436, v416, v2255);
    svfloat32_t v605 = svadd_f32_x(svptrue_b32(), v584, v604);
    svfloat32_t v606 = svsub_f32_x(svptrue_b32(), v584, v604);
    svfloat32_t v618 = svmla_f32_x(pred_full, v578, v598, v2255);
    svfloat32_t v636 = svnmls_f32_x(pred_full, v598, v578, v2255);
    svfloat32_t v767 = svadd_f32_x(svptrue_b32(), v746, v766);
    svfloat32_t v768 = svsub_f32_x(svptrue_b32(), v746, v766);
    svfloat32_t v780 = svmla_f32_x(pred_full, v740, v760, v2255);
    svfloat32_t v798 = svnmls_f32_x(pred_full, v760, v740, v2255);
    svfloat32_t v151 = svadd_f32_x(svptrue_b32(), v2330, v119);
    svfloat32_t zero158 = svdup_n_f32(0);
    svfloat32_t v158 = svcmla_f32_x(pred_full, zero158, v2275, v132, 90);
    svfloat32_t zero166 = svdup_n_f32(0);
    svfloat32_t v166 = svcmla_f32_x(pred_full, zero166, v2275, v150, 90);
    svfloat32_t v313 = svadd_f32_x(svptrue_b32(), v2340, v281);
    svfloat32_t zero320 = svdup_n_f32(0);
    svfloat32_t v320 = svcmla_f32_x(pred_full, zero320, v2275, v294, 90);
    svfloat32_t zero328 = svdup_n_f32(0);
    svfloat32_t v328 = svcmla_f32_x(pred_full, zero328, v2275, v312, 90);
    svfloat32_t v475 = svadd_f32_x(svptrue_b32(), v2350, v443);
    svfloat32_t zero482 = svdup_n_f32(0);
    svfloat32_t v482 = svcmla_f32_x(pred_full, zero482, v2275, v456, 90);
    svfloat32_t zero490 = svdup_n_f32(0);
    svfloat32_t v490 = svcmla_f32_x(pred_full, zero490, v2275, v474, 90);
    svfloat32_t v637 = svadd_f32_x(svptrue_b32(), v2360, v605);
    svfloat32_t zero644 = svdup_n_f32(0);
    svfloat32_t v644 = svcmla_f32_x(pred_full, zero644, v2275, v618, 90);
    svfloat32_t zero652 = svdup_n_f32(0);
    svfloat32_t v652 = svcmla_f32_x(pred_full, zero652, v2275, v636, 90);
    svfloat32_t v799 = svadd_f32_x(svptrue_b32(), v2370, v767);
    svfloat32_t zero806 = svdup_n_f32(0);
    svfloat32_t v806 = svcmla_f32_x(pred_full, zero806, v2275, v780, 90);
    svfloat32_t zero814 = svdup_n_f32(0);
    svfloat32_t v814 = svcmla_f32_x(pred_full, zero814, v2275, v798, 90);
    svfloat32_t v126 = svmls_f32_x(pred_full, v2330, v119, v2251);
    svfloat32_t v288 = svmls_f32_x(pred_full, v2340, v281, v2251);
    svfloat32_t v450 = svmls_f32_x(pred_full, v2350, v443, v2251);
    svfloat32_t v612 = svmls_f32_x(pred_full, v2360, v605, v2251);
    svfloat32_t v774 = svmls_f32_x(pred_full, v2370, v767, v2251);
    svfloat32_t v138 = svmls_f32_x(pred_full, v126, v120, v2253);
    svfloat32_t v300 = svmls_f32_x(pred_full, v288, v282, v2253);
    svfloat32_t v462 = svmls_f32_x(pred_full, v450, v444, v2253);
    svfloat32_t v624 = svmls_f32_x(pred_full, v612, v606, v2253);
    svfloat32_t v786 = svmls_f32_x(pred_full, v774, v768, v2253);
    svfloat32_t v840 = svcmla_f32_x(pred_full, v313, v1992, v313, 90);
    svfloat32_t v853 = svcmla_f32_x(pred_full, v475, v1992, v475, 90);
    svfloat32_t v866 = svcmla_f32_x(pred_full, v799, v1992, v799, 90);
    svfloat32_t v886 = svcmla_f32_x(pred_full, v637, v1992, v637, 90);
    svfloat32_t v144 = svnmls_f32_x(pred_full, v138, v126, v2295);
    svfloat32_t v167 = svsub_f32_x(svptrue_b32(), v138, v166);
    svfloat32_t v306 = svnmls_f32_x(pred_full, v300, v288, v2295);
    svfloat32_t v329 = svsub_f32_x(svptrue_b32(), v300, v328);
    svfloat32_t v468 = svnmls_f32_x(pred_full, v462, v450, v2295);
    svfloat32_t v491 = svsub_f32_x(svptrue_b32(), v462, v490);
    svfloat32_t v630 = svnmls_f32_x(pred_full, v624, v612, v2295);
    svfloat32_t v653 = svsub_f32_x(svptrue_b32(), v624, v652);
    svfloat32_t v792 = svnmls_f32_x(pred_full, v786, v774, v2295);
    svfloat32_t v815 = svsub_f32_x(svptrue_b32(), v786, v814);
    svfloat32_t v867 = svsub_f32_x(svptrue_b32(), v840, v866);
    svfloat32_t v887 = svsub_f32_x(svptrue_b32(), v853, v886);
    svfloat32_t v159 = svsub_f32_x(svptrue_b32(), v144, v158);
    svfloat32_t v173 = svnmls_f32_x(pred_full, v167, v138, v2295);
    svfloat32_t v321 = svsub_f32_x(svptrue_b32(), v306, v320);
    svfloat32_t v335 = svnmls_f32_x(pred_full, v329, v300, v2295);
    svfloat32_t v483 = svsub_f32_x(svptrue_b32(), v468, v482);
    svfloat32_t v497 = svnmls_f32_x(pred_full, v491, v462, v2295);
    svfloat32_t v645 = svsub_f32_x(svptrue_b32(), v630, v644);
    svfloat32_t v659 = svnmls_f32_x(pred_full, v653, v624, v2295);
    svfloat32_t v807 = svsub_f32_x(svptrue_b32(), v792, v806);
    svfloat32_t v821 = svnmls_f32_x(pred_full, v815, v786, v2295);
    svfloat32_t v873 = svnmls_f32_x(pred_full, v867, v840, v2295);
    svfloat32_t v893 = svnmls_f32_x(pred_full, v887, v853, v2295);
    svfloat32_t v1166 = svmul_f32_x(svptrue_b32(), v329, v2113);
    svfloat32_t v1179 = svmul_f32_x(svptrue_b32(), v491, v2241);
    svfloat32_t v1192 = svmul_f32_x(svptrue_b32(), v815, v2243);
    svfloat32_t v1212 = svmul_f32_x(svptrue_b32(), v653, v2179);
    svfloat32_t v179 = svnmls_f32_x(pred_full, v159, v144, v2295);
    svfloat32_t v341 = svnmls_f32_x(pred_full, v321, v306, v2295);
    svfloat32_t v503 = svnmls_f32_x(pred_full, v483, v468, v2295);
    svfloat32_t v665 = svnmls_f32_x(pred_full, v645, v630, v2295);
    svfloat32_t v827 = svnmls_f32_x(pred_full, v807, v792, v2295);
    svfloat32_t v894 = svadd_f32_x(svptrue_b32(), v873, v893);
    svfloat32_t v895 = svsub_f32_x(svptrue_b32(), v873, v893);
    svfloat32_t v907 = svmla_f32_x(pred_full, v867, v887, v2255);
    svfloat32_t v925 = svnmls_f32_x(pred_full, v887, v867, v2255);
    svfloat32_t v999 = svmul_f32_x(svptrue_b32(), v321, v2049);
    svfloat32_t v1012 = svmul_f32_x(svptrue_b32(), v483, v2113);
    svfloat32_t v1025 = svmul_f32_x(svptrue_b32(), v807, v2241);
    svfloat32_t v1045 = svmul_f32_x(svptrue_b32(), v645, v2177);
    svfloat32_t v1174 = svcmla_f32_x(pred_full, v1166, v2114, v329, 90);
    svfloat32_t v1187 = svcmla_f32_x(pred_full, v1179, v2242, v491, 90);
    svfloat32_t v1200 = svcmla_f32_x(pred_full, v1192, v2244, v815, 90);
    svfloat32_t v1220 = svcmla_f32_x(pred_full, v1212, v2180, v653, 90);
    svfloat32_t v1333 = svmul_f32_x(svptrue_b32(), v335, v2177);
    svfloat32_t v1346 = svmul_f32_x(svptrue_b32(), v497, v2179);
    svfloat32_t v1359 = svmul_f32_x(svptrue_b32(), v821, v2248);
    svfloat32_t v1379 = svmul_f32_x(svptrue_b32(), v659, v2245);
    svfloat32_t v926 = svadd_f32_x(svptrue_b32(), v151, v894);
    svfloat32_t zero941 = svdup_n_f32(0);
    svfloat32_t v941 = svcmla_f32_x(pred_full, zero941, v2275, v907, 90);
    svfloat32_t zero957 = svdup_n_f32(0);
    svfloat32_t v957 = svcmla_f32_x(pred_full, zero957, v2275, v925, 90);
    svfloat32_t v1007 = svcmla_f32_x(pred_full, v999, v2050, v321, 90);
    svfloat32_t v1020 = svcmla_f32_x(pred_full, v1012, v2114, v483, 90);
    svfloat32_t v1033 = svcmla_f32_x(pred_full, v1025, v2242, v807, 90);
    svfloat32_t v1053 = svcmla_f32_x(pred_full, v1045, v2178, v645, 90);
    svfloat32_t v1201 = svsub_f32_x(svptrue_b32(), v1174, v1200);
    svfloat32_t v1221 = svsub_f32_x(svptrue_b32(), v1187, v1220);
    svfloat32_t v1341 = svcmla_f32_x(pred_full, v1333, v2178, v335, 90);
    svfloat32_t v1354 = svcmla_f32_x(pred_full, v1346, v2180, v497, 90);
    svfloat32_t v1367 = svcmla_f32_x(pred_full, v1359, v2249, v821, 90);
    svfloat32_t v1387 = svcmla_f32_x(pred_full, v1379, v2185, v659, 90);
    svfloat32_t v1500 = svmul_f32_x(svptrue_b32(), v341, v2241);
    svfloat32_t v1513 = svmul_f32_x(svptrue_b32(), v503, v2243);
    svfloat32_t v1526 = svmul_f32_x(svptrue_b32(), v827, v2245);
    svfloat32_t v1546 = svmul_f32_x(svptrue_b32(), v665, v2248);
    svfloat32_t v901 = svmls_f32_x(pred_full, v151, v894, v2251);
    svint16_t v929 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v926, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v1034 = svsub_f32_x(svptrue_b32(), v1007, v1033);
    svfloat32_t v1054 = svsub_f32_x(svptrue_b32(), v1020, v1053);
    svfloat32_t v1207 = svnmls_f32_x(pred_full, v1201, v1174, v2295);
    svfloat32_t v1227 = svnmls_f32_x(pred_full, v1221, v1187, v2295);
    svfloat32_t v1368 = svsub_f32_x(svptrue_b32(), v1341, v1367);
    svfloat32_t v1388 = svsub_f32_x(svptrue_b32(), v1354, v1387);
    svfloat32_t v1508 = svcmla_f32_x(pred_full, v1500, v2242, v341, 90);
    svfloat32_t v1521 = svcmla_f32_x(pred_full, v1513, v2244, v503, 90);
    svfloat32_t v1534 = svcmla_f32_x(pred_full, v1526, v2246, v827, 90);
    svfloat32_t v1554 = svcmla_f32_x(pred_full, v1546, v2249, v665, 90);
    svfloat32_t v913 = svmls_f32_x(pred_full, v901, v895, v2253);
    svfloat32_t v1040 = svnmls_f32_x(pred_full, v1034, v1007, v2295);
    svfloat32_t v1060 = svnmls_f32_x(pred_full, v1054, v1020, v2295);
    svfloat32_t v1228 = svadd_f32_x(svptrue_b32(), v1207, v1227);
    svfloat32_t v1229 = svsub_f32_x(svptrue_b32(), v1207, v1227);
    svfloat32_t v1241 = svmla_f32_x(pred_full, v1201, v1221, v2255);
    svfloat32_t v1259 = svnmls_f32_x(pred_full, v1221, v1201, v2255);
    svfloat32_t v1374 = svnmls_f32_x(pred_full, v1368, v1341, v2295);
    svfloat32_t v1394 = svnmls_f32_x(pred_full, v1388, v1354, v2295);
    svfloat32_t v1535 = svsub_f32_x(svptrue_b32(), v1508, v1534);
    svfloat32_t v1555 = svsub_f32_x(svptrue_b32(), v1521, v1554);
    svst1w_u64(pred_full, (unsigned *)(v2006), svreinterpret_u64_s16(v929));
    svfloat32_t v919 = svnmls_f32_x(pred_full, v913, v901, v2295);
    svfloat32_t v958 = svsub_f32_x(svptrue_b32(), v913, v957);
    svfloat32_t v1061 = svadd_f32_x(svptrue_b32(), v1040, v1060);
    svfloat32_t v1062 = svsub_f32_x(svptrue_b32(), v1040, v1060);
    svfloat32_t v1074 = svmla_f32_x(pred_full, v1034, v1054, v2255);
    svfloat32_t v1092 = svnmls_f32_x(pred_full, v1054, v1034, v2255);
    svfloat32_t v1260 = svadd_f32_x(svptrue_b32(), v167, v1228);
    svfloat32_t zero1275 = svdup_n_f32(0);
    svfloat32_t v1275 = svcmla_f32_x(pred_full, zero1275, v2275, v1241, 90);
    svfloat32_t zero1291 = svdup_n_f32(0);
    svfloat32_t v1291 = svcmla_f32_x(pred_full, zero1291, v2275, v1259, 90);
    svfloat32_t v1395 = svadd_f32_x(svptrue_b32(), v1374, v1394);
    svfloat32_t v1396 = svsub_f32_x(svptrue_b32(), v1374, v1394);
    svfloat32_t v1408 = svmla_f32_x(pred_full, v1368, v1388, v2255);
    svfloat32_t v1426 = svnmls_f32_x(pred_full, v1388, v1368, v2255);
    svfloat32_t v1541 = svnmls_f32_x(pred_full, v1535, v1508, v2295);
    svfloat32_t v1561 = svnmls_f32_x(pred_full, v1555, v1521, v2295);
    svfloat32_t v942 = svsub_f32_x(svptrue_b32(), v919, v941);
    svint16_t v961 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v958, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v972 = svnmls_f32_x(pred_full, v958, v913, v2295);
    svfloat32_t v1093 = svadd_f32_x(svptrue_b32(), v159, v1061);
    svfloat32_t zero1108 = svdup_n_f32(0);
    svfloat32_t v1108 = svcmla_f32_x(pred_full, zero1108, v2275, v1074, 90);
    svfloat32_t zero1124 = svdup_n_f32(0);
    svfloat32_t v1124 = svcmla_f32_x(pred_full, zero1124, v2275, v1092, 90);
    svfloat32_t v1235 = svmls_f32_x(pred_full, v167, v1228, v2251);
    svint16_t v1263 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1260, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1427 = svadd_f32_x(svptrue_b32(), v173, v1395);
    svfloat32_t zero1442 = svdup_n_f32(0);
    svfloat32_t v1442 = svcmla_f32_x(pred_full, zero1442, v2275, v1408, 90);
    svfloat32_t zero1458 = svdup_n_f32(0);
    svfloat32_t v1458 = svcmla_f32_x(pred_full, zero1458, v2275, v1426, 90);
    svfloat32_t v1562 = svadd_f32_x(svptrue_b32(), v1541, v1561);
    svfloat32_t v1563 = svsub_f32_x(svptrue_b32(), v1541, v1561);
    svfloat32_t v1575 = svmla_f32_x(pred_full, v1535, v1555, v2255);
    svfloat32_t v1593 = svnmls_f32_x(pred_full, v1555, v1535, v2255);
    svint16_t v945 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v942, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v975 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v972, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v986 = svnmls_f32_x(pred_full, v942, v919, v2295);
    svfloat32_t v1068 = svmls_f32_x(pred_full, v159, v1061, v2251);
    svint16_t v1096 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1093, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1247 = svmls_f32_x(pred_full, v1235, v1229, v2253);
    svfloat32_t v1402 = svmls_f32_x(pred_full, v173, v1395, v2251);
    svint16_t v1430 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1427, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1594 = svadd_f32_x(svptrue_b32(), v179, v1562);
    svfloat32_t zero1609 = svdup_n_f32(0);
    svfloat32_t v1609 = svcmla_f32_x(pred_full, zero1609, v2275, v1575, 90);
    svfloat32_t zero1625 = svdup_n_f32(0);
    svfloat32_t v1625 = svcmla_f32_x(pred_full, zero1625, v2275, v1593, 90);
    svst1w_u64(pred_full, (unsigned *)(v2026), svreinterpret_u64_s16(v961));
    svst1w_u64(pred_full, (unsigned *)(v2134), svreinterpret_u64_s16(v1263));
    svint16_t v989 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v986, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v1080 = svmls_f32_x(pred_full, v1068, v1062, v2253);
    svfloat32_t v1253 = svnmls_f32_x(pred_full, v1247, v1235, v2295);
    svfloat32_t v1292 = svsub_f32_x(svptrue_b32(), v1247, v1291);
    svfloat32_t v1414 = svmls_f32_x(pred_full, v1402, v1396, v2253);
    svfloat32_t v1569 = svmls_f32_x(pred_full, v179, v1562, v2251);
    svint16_t v1597 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1594, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v2016), svreinterpret_u64_s16(v945));
    svst1w_u64(pred_full, (unsigned *)(v2036), svreinterpret_u64_s16(v975));
    svst1w_u64(pred_full, (unsigned *)(v2070), svreinterpret_u64_s16(v1096));
    svst1w_u64(pred_full, (unsigned *)(v2198), svreinterpret_u64_s16(v1430));
    svfloat32_t v1086 = svnmls_f32_x(pred_full, v1080, v1068, v2295);
    svfloat32_t v1125 = svsub_f32_x(svptrue_b32(), v1080, v1124);
    svfloat32_t v1276 = svsub_f32_x(svptrue_b32(), v1253, v1275);
    svint16_t v1295 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1292, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1306 = svnmls_f32_x(pred_full, v1292, v1247, v2295);
    svfloat32_t v1420 = svnmls_f32_x(pred_full, v1414, v1402, v2295);
    svfloat32_t v1459 = svsub_f32_x(svptrue_b32(), v1414, v1458);
    svfloat32_t v1581 = svmls_f32_x(pred_full, v1569, v1563, v2253);
    svst1w_u64(pred_full, (unsigned *)(v2046), svreinterpret_u64_s16(v989));
    svst1w_u64(pred_full, (unsigned *)(v2262), svreinterpret_u64_s16(v1597));
    svfloat32_t v1109 = svsub_f32_x(svptrue_b32(), v1086, v1108);
    svint16_t v1128 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1125, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1139 = svnmls_f32_x(pred_full, v1125, v1080, v2295);
    svint16_t v1279 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1276, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1309 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1306, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1320 = svnmls_f32_x(pred_full, v1276, v1253, v2295);
    svfloat32_t v1443 = svsub_f32_x(svptrue_b32(), v1420, v1442);
    svint16_t v1462 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1459, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1473 = svnmls_f32_x(pred_full, v1459, v1414, v2295);
    svfloat32_t v1587 = svnmls_f32_x(pred_full, v1581, v1569, v2295);
    svfloat32_t v1626 = svsub_f32_x(svptrue_b32(), v1581, v1625);
    svst1w_u64(pred_full, (unsigned *)(v2154), svreinterpret_u64_s16(v1295));
    svint16_t v1112 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1109, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1142 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1139, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1153 = svnmls_f32_x(pred_full, v1109, v1086, v2295);
    svint16_t v1323 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1320, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1446 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1443, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1476 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1473, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1487 = svnmls_f32_x(pred_full, v1443, v1420, v2295);
    svfloat32_t v1610 = svsub_f32_x(svptrue_b32(), v1587, v1609);
    svint16_t v1629 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1626, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1640 = svnmls_f32_x(pred_full, v1626, v1581, v2295);
    svst1w_u64(pred_full, (unsigned *)(v2090), svreinterpret_u64_s16(v1128));
    svst1w_u64(pred_full, (unsigned *)(v2144), svreinterpret_u64_s16(v1279));
    svst1w_u64(pred_full, (unsigned *)(v2164), svreinterpret_u64_s16(v1309));
    svst1w_u64(pred_full, (unsigned *)(v2218), svreinterpret_u64_s16(v1462));
    svint16_t v1156 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1153, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1490 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1487, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1613 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1610, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1643 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1640, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1654 = svnmls_f32_x(pred_full, v1610, v1587, v2295);
    svst1w_u64(pred_full, (unsigned *)(v2080), svreinterpret_u64_s16(v1112));
    svst1w_u64(pred_full, (unsigned *)(v2100), svreinterpret_u64_s16(v1142));
    svst1w_u64(pred_full, (unsigned *)(v2174), svreinterpret_u64_s16(v1323));
    svst1w_u64(pred_full, (unsigned *)(v2208), svreinterpret_u64_s16(v1446));
    svst1w_u64(pred_full, (unsigned *)(v2228), svreinterpret_u64_s16(v1476));
    svst1w_u64(pred_full, (unsigned *)(v2282), svreinterpret_u64_s16(v1629));
    svint16_t v1657 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1654, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v2110), svreinterpret_u64_s16(v1156));
    svst1w_u64(pred_full, (unsigned *)(v2238), svreinterpret_u64_s16(v1490));
    svst1w_u64(pred_full, (unsigned *)(v2272), svreinterpret_u64_s16(v1613));
    svst1w_u64(pred_full, (unsigned *)(v2292), svreinterpret_u64_s16(v1643));
    svst1w_u64(pred_full, (unsigned *)(v2302), svreinterpret_u64_s16(v1657));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu32(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v323 = v5[istride];
    float v758 = 7.0710678118654757e-01F;
    float v769 = -7.0710678118654746e-01F;
    float v819 = 5.5557023301960229e-01F;
    float v833 = -1.9509032201612861e-01F;
    float v884 = 9.2387953251128674e-01F;
    float v891 = -9.2387953251128685e-01F;
    float v894 = 3.8268343236508967e-01F;
    float v895 = -3.8268343236508967e-01F;
    float v941 = 1.9509032201612833e-01F;
    float v944 = -9.8078528040323043e-01F;
    float v945 = 9.8078528040323043e-01F;
    float v952 = -5.5557023301960218e-01F;
    float v955 = 8.3146961230254524e-01F;
    float v956 = -8.3146961230254524e-01F;
    float v966 = -1.0000000000000000e+00F;
    float v967 = 1.0000000000000000e+00F;
    float32x2_t v969 = (float32x2_t){v4, v4};
    float32x2_t v20 = v5[0];
    float32x2_t v576 = (float32x2_t){v945, v945};
    float32x2_t v637 = (float32x2_t){v884, v884};
    float32x2_t v641 = (float32x2_t){v895, v894};
    float32x2_t v698 = (float32x2_t){v955, v955};
    float32x2_t v702 = (float32x2_t){v952, v819};
    float32x2_t v709 = (float32x2_t){v833, v833};
    float32x2_t v759 = (float32x2_t){v758, v758};
    float32x2_t v770 = (float32x2_t){v769, v769};
    float32x2_t v774 = (float32x2_t){v967, v966};
    float32x2_t v820 = (float32x2_t){v819, v819};
    float32x2_t v824 = (float32x2_t){v956, v955};
    float32x2_t v831 = (float32x2_t){v944, v944};
    float32x2_t v835 = (float32x2_t){v833, v941};
    float32x2_t v881 = (float32x2_t){v894, v894};
    float32x2_t v885 = (float32x2_t){v891, v884};
    float32x2_t v892 = (float32x2_t){v891, v891};
    float32x2_t v896 = (float32x2_t){v894, v895};
    float32x2_t v942 = (float32x2_t){v941, v941};
    float32x2_t v946 = (float32x2_t){v944, v945};
    float32x2_t v953 = (float32x2_t){v952, v952};
    float32x2_t v957 = (float32x2_t){v955, v956};
    float32x2_t v968 = (float32x2_t){v966, v967};
    float32x2_t v25 = v5[istride * 16];
    float32x2_t v32 = v5[istride * 8];
    float32x2_t v37 = v5[istride * 24];
    float32x2_t v55 = v5[istride * 4];
    float32x2_t v60 = v5[istride * 20];
    float32x2_t v67 = v5[istride * 12];
    float32x2_t v72 = v5[istride * 28];
    float32x2_t v129 = v5[istride * 2];
    float32x2_t v134 = v5[istride * 18];
    float32x2_t v141 = v5[istride * 10];
    float32x2_t v146 = v5[istride * 26];
    float32x2_t v164 = v5[istride * 6];
    float32x2_t v169 = v5[istride * 22];
    float32x2_t v176 = v5[istride * 14];
    float32x2_t v181 = v5[istride * 30];
    float32x2_t v328 = v5[istride * 17];
    float32x2_t v335 = v5[istride * 9];
    float32x2_t v340 = v5[istride * 25];
    float32x2_t v358 = v5[istride * 5];
    float32x2_t v363 = v5[istride * 21];
    float32x2_t v370 = v5[istride * 13];
    float32x2_t v375 = v5[istride * 29];
    float32x2_t v432 = v5[istride * 3];
    float32x2_t v437 = v5[istride * 19];
    float32x2_t v444 = v5[istride * 11];
    float32x2_t v449 = v5[istride * 27];
    float32x2_t v467 = v5[istride * 7];
    float32x2_t v472 = v5[istride * 23];
    float32x2_t v479 = v5[istride * 15];
    float32x2_t v484 = v5[istride * 31];
    float32x2_t v643 = vmul_f32(v969, v641);
    float32x2_t v704 = vmul_f32(v969, v702);
    float32x2_t v776 = vmul_f32(v969, v774);
    float32x2_t v826 = vmul_f32(v969, v824);
    float32x2_t v837 = vmul_f32(v969, v835);
    float32x2_t v887 = vmul_f32(v969, v885);
    float32x2_t v898 = vmul_f32(v969, v896);
    float32x2_t v948 = vmul_f32(v969, v946);
    float32x2_t v959 = vmul_f32(v969, v957);
    float32x2_t v970 = vmul_f32(v969, v968);
    float32x2_t v26 = vadd_f32(v20, v25);
    float32x2_t v27 = vsub_f32(v20, v25);
    float32x2_t v38 = vadd_f32(v32, v37);
    float32x2_t v39 = vsub_f32(v32, v37);
    float32x2_t v61 = vadd_f32(v55, v60);
    float32x2_t v62 = vsub_f32(v55, v60);
    float32x2_t v73 = vadd_f32(v67, v72);
    float32x2_t v74 = vsub_f32(v67, v72);
    float32x2_t v135 = vadd_f32(v129, v134);
    float32x2_t v136 = vsub_f32(v129, v134);
    float32x2_t v147 = vadd_f32(v141, v146);
    float32x2_t v148 = vsub_f32(v141, v146);
    float32x2_t v170 = vadd_f32(v164, v169);
    float32x2_t v171 = vsub_f32(v164, v169);
    float32x2_t v182 = vadd_f32(v176, v181);
    float32x2_t v183 = vsub_f32(v176, v181);
    float32x2_t v329 = vadd_f32(v323, v328);
    float32x2_t v330 = vsub_f32(v323, v328);
    float32x2_t v341 = vadd_f32(v335, v340);
    float32x2_t v342 = vsub_f32(v335, v340);
    float32x2_t v364 = vadd_f32(v358, v363);
    float32x2_t v365 = vsub_f32(v358, v363);
    float32x2_t v376 = vadd_f32(v370, v375);
    float32x2_t v377 = vsub_f32(v370, v375);
    float32x2_t v438 = vadd_f32(v432, v437);
    float32x2_t v439 = vsub_f32(v432, v437);
    float32x2_t v450 = vadd_f32(v444, v449);
    float32x2_t v451 = vsub_f32(v444, v449);
    float32x2_t v473 = vadd_f32(v467, v472);
    float32x2_t v474 = vsub_f32(v467, v472);
    float32x2_t v485 = vadd_f32(v479, v484);
    float32x2_t v486 = vsub_f32(v479, v484);
    float32x2_t v45 = vrev64_f32(v39);
    float32x2_t v47 = vadd_f32(v26, v38);
    float32x2_t v48 = vsub_f32(v26, v38);
    float32x2_t v75 = vadd_f32(v61, v73);
    float32x2_t v76 = vsub_f32(v61, v73);
    float32x2_t v91 = vmul_f32(v62, v759);
    float32x2_t v102 = vmul_f32(v74, v770);
    float32x2_t v154 = vrev64_f32(v148);
    float32x2_t v156 = vadd_f32(v135, v147);
    float32x2_t v157 = vsub_f32(v135, v147);
    float32x2_t v189 = vrev64_f32(v183);
    float32x2_t v191 = vadd_f32(v170, v182);
    float32x2_t v192 = vsub_f32(v170, v182);
    float32x2_t v348 = vrev64_f32(v342);
    float32x2_t v350 = vadd_f32(v329, v341);
    float32x2_t v351 = vsub_f32(v329, v341);
    float32x2_t v378 = vadd_f32(v364, v376);
    float32x2_t v379 = vsub_f32(v364, v376);
    float32x2_t v394 = vmul_f32(v365, v759);
    float32x2_t v405 = vmul_f32(v377, v770);
    float32x2_t v457 = vrev64_f32(v451);
    float32x2_t v459 = vadd_f32(v438, v450);
    float32x2_t v460 = vsub_f32(v438, v450);
    float32x2_t v487 = vadd_f32(v473, v485);
    float32x2_t v488 = vsub_f32(v473, v485);
    float32x2_t v503 = vmul_f32(v474, v759);
    float32x2_t v514 = vmul_f32(v486, v770);
    float32x2_t v46 = vmul_f32(v45, v776);
    float32x2_t v82 = vrev64_f32(v76);
    float32x2_t v84 = vadd_f32(v47, v75);
    float32x2_t v85 = vsub_f32(v47, v75);
    float32x2_t v97 = vrev64_f32(v91);
    float32x2_t v108 = vrev64_f32(v102);
    float32x2_t v155 = vmul_f32(v154, v776);
    float32x2_t v190 = vmul_f32(v189, v776);
    float32x2_t v195 = vadd_f32(v156, v191);
    float32x2_t v196 = vsub_f32(v156, v191);
    float32x2_t v248 = vmul_f32(v157, v759);
    float32x2_t v259 = vmul_f32(v192, v770);
    float32x2_t v349 = vmul_f32(v348, v776);
    float32x2_t v385 = vrev64_f32(v379);
    float32x2_t v387 = vadd_f32(v350, v378);
    float32x2_t v388 = vsub_f32(v350, v378);
    float32x2_t v400 = vrev64_f32(v394);
    float32x2_t v411 = vrev64_f32(v405);
    float32x2_t v458 = vmul_f32(v457, v776);
    float32x2_t v494 = vrev64_f32(v488);
    float32x2_t v496 = vadd_f32(v459, v487);
    float32x2_t v497 = vsub_f32(v459, v487);
    float32x2_t v509 = vrev64_f32(v503);
    float32x2_t v520 = vrev64_f32(v514);
    float32x2_t v49 = vsub_f32(v27, v46);
    float32x2_t v50 = vadd_f32(v27, v46);
    float32x2_t v83 = vmul_f32(v82, v776);
    float32x2_t v98 = vmul_f32(v97, v970);
    float32x2_t v109 = vmul_f32(v108, v776);
    float32x2_t v158 = vsub_f32(v136, v155);
    float32x2_t v159 = vadd_f32(v136, v155);
    float32x2_t v193 = vsub_f32(v171, v190);
    float32x2_t v194 = vadd_f32(v171, v190);
    float32x2_t v202 = vrev64_f32(v196);
    float32x2_t v204 = vadd_f32(v84, v195);
    float32x2_t v205 = vsub_f32(v84, v195);
    float32x2_t v254 = vrev64_f32(v248);
    float32x2_t v265 = vrev64_f32(v259);
    float32x2_t v352 = vsub_f32(v330, v349);
    float32x2_t v353 = vadd_f32(v330, v349);
    float32x2_t v386 = vmul_f32(v385, v776);
    float32x2_t v401 = vmul_f32(v400, v970);
    float32x2_t v412 = vmul_f32(v411, v776);
    float32x2_t v461 = vsub_f32(v439, v458);
    float32x2_t v462 = vadd_f32(v439, v458);
    float32x2_t v495 = vmul_f32(v494, v776);
    float32x2_t v510 = vmul_f32(v509, v970);
    float32x2_t v521 = vmul_f32(v520, v776);
    float32x2_t v537 = vadd_f32(v387, v496);
    float32x2_t v538 = vsub_f32(v387, v496);
    float32x2_t v760 = vmul_f32(v388, v759);
    float32x2_t v771 = vmul_f32(v497, v770);
    float32x2_t v86 = vsub_f32(v48, v83);
    float32x2_t v87 = vadd_f32(v48, v83);
    float32x2_t v110 = vadd_f32(v91, v98);
    float32x2_t v111 = vadd_f32(v102, v109);
    float32x2_t v203 = vmul_f32(v202, v776);
    float32x2_t v211 = vmul_f32(v158, v637);
    float32x2_t v217 = vrev64_f32(v158);
    float32x2_t v222 = vmul_f32(v193, v881);
    float32x2_t v228 = vrev64_f32(v193);
    float32x2_t v255 = vmul_f32(v254, v970);
    float32x2_t v266 = vmul_f32(v265, v776);
    float32x2_t v285 = vmul_f32(v159, v881);
    float32x2_t v291 = vrev64_f32(v159);
    float32x2_t v296 = vmul_f32(v194, v892);
    float32x2_t v302 = vrev64_f32(v194);
    float32x2_t v389 = vsub_f32(v351, v386);
    float32x2_t v390 = vadd_f32(v351, v386);
    float32x2_t v413 = vadd_f32(v394, v401);
    float32x2_t v414 = vadd_f32(v405, v412);
    float32x2_t v498 = vsub_f32(v460, v495);
    float32x2_t v499 = vadd_f32(v460, v495);
    float32x2_t v522 = vadd_f32(v503, v510);
    float32x2_t v523 = vadd_f32(v514, v521);
    float32x2_t v544 = vrev64_f32(v538);
    float32x2_t v546 = vadd_f32(v204, v537);
    float32x2_t v547 = vsub_f32(v204, v537);
    float32x2_t v766 = vrev64_f32(v760);
    float32x2_t v777 = vrev64_f32(v771);
    float32x2_t v112 = vadd_f32(v110, v111);
    float32x2_t v113 = vsub_f32(v111, v110);
    float32x2_t v206 = vsub_f32(v85, v203);
    float32x2_t v207 = vadd_f32(v85, v203);
    float32x2_t v267 = vadd_f32(v248, v255);
    float32x2_t v268 = vadd_f32(v259, v266);
    float32x2_t v415 = vadd_f32(v413, v414);
    float32x2_t v416 = vsub_f32(v414, v413);
    float32x2_t v524 = vadd_f32(v522, v523);
    float32x2_t v525 = vsub_f32(v523, v522);
    float32x2_t v545 = vmul_f32(v544, v776);
    int16x4_t v552 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v546, 15), (int32x2_t){0, 0}));
    int16x4_t v564 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v547, 15), (int32x2_t){0, 0}));
    float32x2_t v638 = vmul_f32(v389, v637);
    float32x2_t v644 = vrev64_f32(v389);
    float32x2_t v649 = vmul_f32(v498, v881);
    float32x2_t v655 = vrev64_f32(v498);
    float32x2_t v767 = vmul_f32(v766, v970);
    float32x2_t v778 = vmul_f32(v777, v776);
    float32x2_t v882 = vmul_f32(v390, v881);
    float32x2_t v888 = vrev64_f32(v390);
    float32x2_t v893 = vmul_f32(v499, v892);
    float32x2_t v899 = vrev64_f32(v499);
    float32x2_t v119 = vrev64_f32(v113);
    float32x2_t v121 = vadd_f32(v49, v112);
    float32x2_t v122 = vsub_f32(v49, v112);
    float32x2_t v230 = vfma_f32(v211, v217, v643);
    float32x2_t v231 = vfma_f32(v222, v228, v887);
    float32x2_t v269 = vadd_f32(v267, v268);
    float32x2_t v270 = vsub_f32(v268, v267);
    float32x2_t v304 = vfma_f32(v285, v291, v887);
    float32x2_t v305 = vfma_f32(v296, v302, v898);
    float32x2_t v422 = vrev64_f32(v416);
    float32x2_t v424 = vadd_f32(v352, v415);
    float32x2_t v425 = vsub_f32(v352, v415);
    float32x2_t v531 = vrev64_f32(v525);
    float32x2_t v533 = vadd_f32(v461, v524);
    float32x2_t v534 = vsub_f32(v461, v524);
    float32x2_t v548 = vsub_f32(v205, v545);
    float32x2_t v549 = vadd_f32(v205, v545);
    v6[0] = vget_lane_s32(vreinterpret_s32_s16(v552), 0);
    v6[ostride * 16] = vget_lane_s32(vreinterpret_s32_s16(v564), 0);
    float32x2_t v779 = vadd_f32(v760, v767);
    float32x2_t v780 = vadd_f32(v771, v778);
    float32x2_t v120 = vmul_f32(v119, v970);
    float32x2_t v232 = vadd_f32(v230, v231);
    float32x2_t v233 = vsub_f32(v231, v230);
    float32x2_t v276 = vrev64_f32(v270);
    float32x2_t v278 = vadd_f32(v86, v269);
    float32x2_t v279 = vsub_f32(v86, v269);
    float32x2_t v306 = vadd_f32(v304, v305);
    float32x2_t v307 = vsub_f32(v305, v304);
    float32x2_t v423 = vmul_f32(v422, v970);
    float32x2_t v532 = vmul_f32(v531, v970);
    int16x4_t v558 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v548, 15), (int32x2_t){0, 0}));
    int16x4_t v570 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v549, 15), (int32x2_t){0, 0}));
    float32x2_t v577 = vmul_f32(v424, v576);
    float32x2_t v583 = vrev64_f32(v424);
    float32x2_t v588 = vmul_f32(v533, v698);
    float32x2_t v594 = vrev64_f32(v533);
    float32x2_t v657 = vfma_f32(v638, v644, v643);
    float32x2_t v658 = vfma_f32(v649, v655, v887);
    float32x2_t v781 = vadd_f32(v779, v780);
    float32x2_t v782 = vsub_f32(v780, v779);
    float32x2_t v821 = vmul_f32(v425, v820);
    float32x2_t v827 = vrev64_f32(v425);
    float32x2_t v832 = vmul_f32(v534, v831);
    float32x2_t v838 = vrev64_f32(v534);
    float32x2_t v901 = vfma_f32(v882, v888, v887);
    float32x2_t v902 = vfma_f32(v893, v899, v898);
    float32x2_t v123 = vsub_f32(v50, v120);
    float32x2_t v124 = vadd_f32(v50, v120);
    float32x2_t v239 = vrev64_f32(v233);
    float32x2_t v241 = vadd_f32(v121, v232);
    float32x2_t v242 = vsub_f32(v121, v232);
    float32x2_t v277 = vmul_f32(v276, v970);
    float32x2_t v313 = vrev64_f32(v307);
    float32x2_t v426 = vsub_f32(v353, v423);
    float32x2_t v427 = vadd_f32(v353, v423);
    float32x2_t v535 = vsub_f32(v462, v532);
    float32x2_t v536 = vadd_f32(v462, v532);
    v6[ostride * 8] = vget_lane_s32(vreinterpret_s32_s16(v558), 0);
    v6[ostride * 24] = vget_lane_s32(vreinterpret_s32_s16(v570), 0);
    float32x2_t v659 = vadd_f32(v657, v658);
    float32x2_t v660 = vsub_f32(v658, v657);
    float32x2_t v788 = vrev64_f32(v782);
    float32x2_t v790 = vadd_f32(v206, v781);
    float32x2_t v791 = vsub_f32(v206, v781);
    float32x2_t v903 = vadd_f32(v901, v902);
    float32x2_t v904 = vsub_f32(v902, v901);
    float32x2_t v240 = vmul_f32(v239, v970);
    float32x2_t v280 = vsub_f32(v87, v277);
    float32x2_t v281 = vadd_f32(v87, v277);
    float32x2_t v314 = vmul_f32(v313, v970);
    float32x2_t v315 = vadd_f32(v123, v306);
    float32x2_t v316 = vsub_f32(v123, v306);
    float32x2_t v596 = vfma_f32(v577, v583, v837);
    float32x2_t v597 = vfma_f32(v588, v594, v704);
    float32x2_t v666 = vrev64_f32(v660);
    float32x2_t v668 = vadd_f32(v278, v659);
    float32x2_t v669 = vsub_f32(v278, v659);
    float32x2_t v699 = vmul_f32(v426, v698);
    float32x2_t v705 = vrev64_f32(v426);
    float32x2_t v710 = vmul_f32(v535, v709);
    float32x2_t v716 = vrev64_f32(v535);
    float32x2_t v789 = vmul_f32(v788, v970);
    int16x4_t v796 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v790, 15), (int32x2_t){0, 0}));
    int16x4_t v808 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v791, 15), (int32x2_t){0, 0}));
    float32x2_t v840 = vfma_f32(v821, v827, v826);
    float32x2_t v841 = vfma_f32(v832, v838, v837);
    float32x2_t v910 = vrev64_f32(v904);
    float32x2_t v943 = vmul_f32(v427, v942);
    float32x2_t v949 = vrev64_f32(v427);
    float32x2_t v954 = vmul_f32(v536, v953);
    float32x2_t v960 = vrev64_f32(v536);
    float32x2_t v243 = vsub_f32(v122, v240);
    float32x2_t v244 = vadd_f32(v122, v240);
    float32x2_t v317 = vsub_f32(v124, v314);
    float32x2_t v318 = vadd_f32(v124, v314);
    float32x2_t v598 = vadd_f32(v596, v597);
    float32x2_t v599 = vsub_f32(v597, v596);
    float32x2_t v667 = vmul_f32(v666, v970);
    int16x4_t v674 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v668, 15), (int32x2_t){0, 0}));
    int16x4_t v686 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v669, 15), (int32x2_t){0, 0}));
    float32x2_t v792 = vsub_f32(v207, v789);
    float32x2_t v793 = vadd_f32(v207, v789);
    v6[ostride * 4] = vget_lane_s32(vreinterpret_s32_s16(v796), 0);
    v6[ostride * 20] = vget_lane_s32(vreinterpret_s32_s16(v808), 0);
    float32x2_t v842 = vadd_f32(v840, v841);
    float32x2_t v843 = vsub_f32(v841, v840);
    float32x2_t v911 = vmul_f32(v910, v970);
    float32x2_t v912 = vadd_f32(v280, v903);
    float32x2_t v913 = vsub_f32(v280, v903);
    float32x2_t v605 = vrev64_f32(v599);
    float32x2_t v607 = vadd_f32(v241, v598);
    float32x2_t v608 = vsub_f32(v241, v598);
    float32x2_t v670 = vsub_f32(v279, v667);
    float32x2_t v671 = vadd_f32(v279, v667);
    v6[ostride * 2] = vget_lane_s32(vreinterpret_s32_s16(v674), 0);
    v6[ostride * 18] = vget_lane_s32(vreinterpret_s32_s16(v686), 0);
    float32x2_t v718 = vfma_f32(v699, v705, v704);
    float32x2_t v719 = vfma_f32(v710, v716, v948);
    int16x4_t v802 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v792, 15), (int32x2_t){0, 0}));
    int16x4_t v814 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v793, 15), (int32x2_t){0, 0}));
    float32x2_t v849 = vrev64_f32(v843);
    float32x2_t v851 = vadd_f32(v243, v842);
    float32x2_t v852 = vsub_f32(v243, v842);
    float32x2_t v914 = vsub_f32(v281, v911);
    float32x2_t v915 = vadd_f32(v281, v911);
    int16x4_t v918 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v912, 15), (int32x2_t){0, 0}));
    int16x4_t v930 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v913, 15), (int32x2_t){0, 0}));
    float32x2_t v962 = vfma_f32(v943, v949, v948);
    float32x2_t v963 = vfma_f32(v954, v960, v959);
    float32x2_t v606 = vmul_f32(v605, v970);
    int16x4_t v613 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v607, 15), (int32x2_t){0, 0}));
    int16x4_t v625 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v608, 15), (int32x2_t){0, 0}));
    int16x4_t v680 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v670, 15), (int32x2_t){0, 0}));
    int16x4_t v692 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v671, 15), (int32x2_t){0, 0}));
    float32x2_t v720 = vadd_f32(v718, v719);
    float32x2_t v721 = vsub_f32(v719, v718);
    v6[ostride * 12] = vget_lane_s32(vreinterpret_s32_s16(v802), 0);
    v6[ostride * 28] = vget_lane_s32(vreinterpret_s32_s16(v814), 0);
    float32x2_t v850 = vmul_f32(v849, v970);
    int16x4_t v857 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v851, 15), (int32x2_t){0, 0}));
    int16x4_t v869 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v852, 15), (int32x2_t){0, 0}));
    v6[ostride * 6] = vget_lane_s32(vreinterpret_s32_s16(v918), 0);
    int16x4_t v924 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v914, 15), (int32x2_t){0, 0}));
    v6[ostride * 22] = vget_lane_s32(vreinterpret_s32_s16(v930), 0);
    int16x4_t v936 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v915, 15), (int32x2_t){0, 0}));
    float32x2_t v964 = vadd_f32(v962, v963);
    float32x2_t v965 = vsub_f32(v963, v962);
    float32x2_t v609 = vsub_f32(v242, v606);
    float32x2_t v610 = vadd_f32(v242, v606);
    v6[ostride] = vget_lane_s32(vreinterpret_s32_s16(v613), 0);
    v6[ostride * 17] = vget_lane_s32(vreinterpret_s32_s16(v625), 0);
    v6[ostride * 10] = vget_lane_s32(vreinterpret_s32_s16(v680), 0);
    v6[ostride * 26] = vget_lane_s32(vreinterpret_s32_s16(v692), 0);
    float32x2_t v727 = vrev64_f32(v721);
    float32x2_t v729 = vadd_f32(v315, v720);
    float32x2_t v730 = vsub_f32(v315, v720);
    float32x2_t v853 = vsub_f32(v244, v850);
    float32x2_t v854 = vadd_f32(v244, v850);
    v6[ostride * 5] = vget_lane_s32(vreinterpret_s32_s16(v857), 0);
    v6[ostride * 21] = vget_lane_s32(vreinterpret_s32_s16(v869), 0);
    v6[ostride * 14] = vget_lane_s32(vreinterpret_s32_s16(v924), 0);
    v6[ostride * 30] = vget_lane_s32(vreinterpret_s32_s16(v936), 0);
    float32x2_t v971 = vrev64_f32(v965);
    float32x2_t v973 = vadd_f32(v317, v964);
    float32x2_t v974 = vsub_f32(v317, v964);
    int16x4_t v619 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v609, 15), (int32x2_t){0, 0}));
    int16x4_t v631 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v610, 15), (int32x2_t){0, 0}));
    float32x2_t v728 = vmul_f32(v727, v970);
    int16x4_t v735 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v729, 15), (int32x2_t){0, 0}));
    int16x4_t v747 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v730, 15), (int32x2_t){0, 0}));
    int16x4_t v863 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v853, 15), (int32x2_t){0, 0}));
    int16x4_t v875 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v854, 15), (int32x2_t){0, 0}));
    float32x2_t v972 = vmul_f32(v971, v970);
    int16x4_t v979 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v973, 15), (int32x2_t){0, 0}));
    int16x4_t v991 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v974, 15), (int32x2_t){0, 0}));
    v6[ostride * 9] = vget_lane_s32(vreinterpret_s32_s16(v619), 0);
    v6[ostride * 25] = vget_lane_s32(vreinterpret_s32_s16(v631), 0);
    float32x2_t v731 = vsub_f32(v316, v728);
    float32x2_t v732 = vadd_f32(v316, v728);
    v6[ostride * 3] = vget_lane_s32(vreinterpret_s32_s16(v735), 0);
    v6[ostride * 19] = vget_lane_s32(vreinterpret_s32_s16(v747), 0);
    v6[ostride * 13] = vget_lane_s32(vreinterpret_s32_s16(v863), 0);
    v6[ostride * 29] = vget_lane_s32(vreinterpret_s32_s16(v875), 0);
    float32x2_t v975 = vsub_f32(v318, v972);
    float32x2_t v976 = vadd_f32(v318, v972);
    v6[ostride * 7] = vget_lane_s32(vreinterpret_s32_s16(v979), 0);
    v6[ostride * 23] = vget_lane_s32(vreinterpret_s32_s16(v991), 0);
    int16x4_t v741 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v731, 15), (int32x2_t){0, 0}));
    int16x4_t v753 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v732, 15), (int32x2_t){0, 0}));
    int16x4_t v985 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v975, 15), (int32x2_t){0, 0}));
    int16x4_t v997 =
        vqmovn_s32(vcombine_s32(vcvt_n_s32_f32(v976, 15), (int32x2_t){0, 0}));
    v6[ostride * 11] = vget_lane_s32(vreinterpret_s32_s16(v741), 0);
    v6[ostride * 27] = vget_lane_s32(vreinterpret_s32_s16(v753), 0);
    v6[ostride * 15] = vget_lane_s32(vreinterpret_s32_s16(v985), 0);
    v6[ostride * 31] = vget_lane_s32(vreinterpret_s32_s16(v997), 0);
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cs16_ac_n_uu32(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_int16_t *restrict y,
                                         int istride, int ostride, int howmany,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  int32_t *v6 = (int32_t *)y;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v815 = -1.9509032201612819e-01F;
    float v874 = 7.0710678118654757e-01F;
    float v886 = -7.0710678118654746e-01F;
    float v891 = -1.0000000000000000e+00F;
    float v945 = 5.5557023301960229e-01F;
    float v950 = 8.3146961230254524e-01F;
    float v957 = -9.8078528040323043e-01F;
    float v1016 = 3.8268343236508984e-01F;
    float v1021 = 9.2387953251128674e-01F;
    float v1028 = -9.2387953251128685e-01F;
    float v1033 = -3.8268343236508967e-01F;
    float v1087 = 1.9509032201612833e-01F;
    float v1092 = 9.8078528040323043e-01F;
    float v1099 = -5.5557023301960218e-01F;
    float v1104 = -8.3146961230254524e-01F;
    const float32x2_t *v1333 = &v5[v0];
    int32_t *v1534 = &v6[v2];
    int64_t v26 = v0 * 16;
    int64_t v35 = v0 * 8;
    int64_t v42 = v0 * 24;
    int64_t v62 = v0 * 4;
    int64_t v69 = v0 * 20;
    int64_t v78 = v0 * 12;
    int64_t v85 = v0 * 28;
    int64_t v146 = v0 * 2;
    int64_t v153 = v0 * 18;
    int64_t v162 = v0 * 10;
    int64_t v169 = v0 * 26;
    int64_t v189 = v0 * 6;
    int64_t v196 = v0 * 22;
    int64_t v205 = v0 * 14;
    int64_t v212 = v0 * 30;
    int64_t v369 = v0 * 17;
    int64_t v378 = v0 * 9;
    int64_t v385 = v0 * 25;
    int64_t v405 = v0 * 5;
    int64_t v412 = v0 * 21;
    int64_t v421 = v0 * 13;
    int64_t v428 = v0 * 29;
    int64_t v489 = v0 * 3;
    int64_t v496 = v0 * 19;
    int64_t v505 = v0 * 11;
    int64_t v512 = v0 * 27;
    int64_t v532 = v0 * 7;
    int64_t v539 = v0 * 23;
    int64_t v548 = v0 * 15;
    int64_t v555 = v0 * 31;
    int64_t v637 = v2 * 8;
    int64_t v645 = v2 * 16;
    int64_t v653 = v2 * 24;
    int64_t v708 = v2 * 9;
    int64_t v716 = v2 * 17;
    int64_t v724 = v2 * 25;
    float v740 = v4 * v1016;
    int64_t v771 = v2 * 2;
    int64_t v779 = v2 * 10;
    int64_t v787 = v2 * 18;
    int64_t v795 = v2 * 26;
    float v811 = v4 * v945;
    int64_t v842 = v2 * 3;
    int64_t v850 = v2 * 11;
    int64_t v858 = v2 * 19;
    int64_t v866 = v2 * 27;
    float v894 = v4 * v891;
    int64_t v913 = v2 * 4;
    int64_t v921 = v2 * 12;
    int64_t v929 = v2 * 20;
    int64_t v937 = v2 * 28;
    float v953 = v4 * v950;
    float v965 = v4 * v1087;
    int64_t v984 = v2 * 5;
    int64_t v992 = v2 * 13;
    int64_t v1000 = v2 * 21;
    int64_t v1008 = v2 * 29;
    float v1024 = v4 * v1021;
    float v1036 = v4 * v1033;
    int64_t v1055 = v2 * 6;
    int64_t v1063 = v2 * 14;
    int64_t v1071 = v2 * 22;
    int64_t v1079 = v2 * 30;
    float v1095 = v4 * v1092;
    float v1107 = v4 * v1104;
    int64_t v1126 = v2 * 7;
    int64_t v1134 = v2 * 15;
    int64_t v1142 = v2 * 23;
    int64_t v1150 = v2 * 31;
    const float32x2_t *v1164 = &v5[0];
    int32_t *v1493 = &v6[0];
    svfloat32_t v1523 = svdup_n_f32(v1092);
    svfloat32_t v1564 = svdup_n_f32(v1021);
    svfloat32_t v1605 = svdup_n_f32(v950);
    svfloat32_t v1607 = svdup_n_f32(v815);
    svfloat32_t v1646 = svdup_n_f32(v874);
    svfloat32_t v1648 = svdup_n_f32(v886);
    svfloat32_t v1687 = svdup_n_f32(v945);
    svfloat32_t v1689 = svdup_n_f32(v957);
    svfloat32_t v1728 = svdup_n_f32(v1016);
    svfloat32_t v1730 = svdup_n_f32(v1028);
    svfloat32_t v1769 = svdup_n_f32(v1087);
    svfloat32_t v1771 = svdup_n_f32(v1099);
    svfloat32_t v1773 = svdup_n_f32(v4);
    svfloat32_t v1843 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1333)[0]));
    const float32x2_t *v1173 = &v5[v26];
    const float32x2_t *v1182 = &v5[v35];
    const float32x2_t *v1191 = &v5[v42];
    const float32x2_t *v1201 = &v5[v62];
    const float32x2_t *v1210 = &v5[v69];
    const float32x2_t *v1219 = &v5[v78];
    const float32x2_t *v1228 = &v5[v85];
    const float32x2_t *v1243 = &v5[v146];
    const float32x2_t *v1252 = &v5[v153];
    const float32x2_t *v1261 = &v5[v162];
    const float32x2_t *v1270 = &v5[v169];
    const float32x2_t *v1280 = &v5[v189];
    const float32x2_t *v1289 = &v5[v196];
    const float32x2_t *v1298 = &v5[v205];
    const float32x2_t *v1307 = &v5[v212];
    const float32x2_t *v1342 = &v5[v369];
    const float32x2_t *v1351 = &v5[v378];
    const float32x2_t *v1360 = &v5[v385];
    const float32x2_t *v1370 = &v5[v405];
    const float32x2_t *v1379 = &v5[v412];
    const float32x2_t *v1388 = &v5[v421];
    const float32x2_t *v1397 = &v5[v428];
    const float32x2_t *v1412 = &v5[v489];
    const float32x2_t *v1421 = &v5[v496];
    const float32x2_t *v1430 = &v5[v505];
    const float32x2_t *v1439 = &v5[v512];
    const float32x2_t *v1449 = &v5[v532];
    const float32x2_t *v1458 = &v5[v539];
    const float32x2_t *v1467 = &v5[v548];
    const float32x2_t *v1476 = &v5[v555];
    int32_t *v1502 = &v6[v637];
    int32_t *v1511 = &v6[v645];
    int32_t *v1520 = &v6[v653];
    int32_t *v1543 = &v6[v708];
    int32_t *v1552 = &v6[v716];
    int32_t *v1561 = &v6[v724];
    svfloat32_t v1565 = svdup_n_f32(v740);
    int32_t *v1575 = &v6[v771];
    int32_t *v1584 = &v6[v779];
    int32_t *v1593 = &v6[v787];
    int32_t *v1602 = &v6[v795];
    svfloat32_t v1606 = svdup_n_f32(v811);
    int32_t *v1616 = &v6[v842];
    int32_t *v1625 = &v6[v850];
    int32_t *v1634 = &v6[v858];
    int32_t *v1643 = &v6[v866];
    svfloat32_t v1649 = svdup_n_f32(v894);
    int32_t *v1657 = &v6[v913];
    int32_t *v1666 = &v6[v921];
    int32_t *v1675 = &v6[v929];
    int32_t *v1684 = &v6[v937];
    svfloat32_t v1688 = svdup_n_f32(v953);
    svfloat32_t v1690 = svdup_n_f32(v965);
    int32_t *v1698 = &v6[v984];
    int32_t *v1707 = &v6[v992];
    int32_t *v1716 = &v6[v1000];
    int32_t *v1725 = &v6[v1008];
    svfloat32_t v1729 = svdup_n_f32(v1024);
    svfloat32_t v1731 = svdup_n_f32(v1036);
    int32_t *v1739 = &v6[v1055];
    int32_t *v1748 = &v6[v1063];
    int32_t *v1757 = &v6[v1071];
    int32_t *v1766 = &v6[v1079];
    svfloat32_t v1770 = svdup_n_f32(v1095);
    svfloat32_t v1772 = svdup_n_f32(v1107);
    int32_t *v1780 = &v6[v1126];
    int32_t *v1789 = &v6[v1134];
    int32_t *v1798 = &v6[v1142];
    int32_t *v1807 = &v6[v1150];
    svfloat32_t v1811 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1164)[0]));
    svfloat32_t v1813 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1173)[0]));
    svfloat32_t v1815 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1182)[0]));
    svfloat32_t v1817 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1191)[0]));
    svfloat32_t v1819 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1201)[0]));
    svfloat32_t v1821 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1210)[0]));
    svfloat32_t v1823 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1219)[0]));
    svfloat32_t v1825 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1228)[0]));
    svfloat32_t v1827 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1243)[0]));
    svfloat32_t v1829 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1252)[0]));
    svfloat32_t v1831 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1261)[0]));
    svfloat32_t v1833 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1270)[0]));
    svfloat32_t v1835 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1280)[0]));
    svfloat32_t v1837 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1289)[0]));
    svfloat32_t v1839 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1298)[0]));
    svfloat32_t v1841 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1307)[0]));
    svfloat32_t v1845 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1342)[0]));
    svfloat32_t v1847 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1351)[0]));
    svfloat32_t v1849 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1360)[0]));
    svfloat32_t v1851 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1370)[0]));
    svfloat32_t v1853 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1379)[0]));
    svfloat32_t v1855 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1388)[0]));
    svfloat32_t v1857 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1397)[0]));
    svfloat32_t v1859 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1412)[0]));
    svfloat32_t v1861 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1421)[0]));
    svfloat32_t v1863 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1430)[0]));
    svfloat32_t v1865 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1439)[0]));
    svfloat32_t v1867 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1449)[0]));
    svfloat32_t v1869 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1458)[0]));
    svfloat32_t v1871 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1467)[0]));
    svfloat32_t v1873 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1476)[0]));
    svfloat32_t v32 = svadd_f32_x(svptrue_b32(), v1811, v1813);
    svfloat32_t v33 = svsub_f32_x(svptrue_b32(), v1811, v1813);
    svfloat32_t v48 = svadd_f32_x(svptrue_b32(), v1815, v1817);
    svfloat32_t v49 = svsub_f32_x(svptrue_b32(), v1815, v1817);
    svfloat32_t v75 = svadd_f32_x(svptrue_b32(), v1819, v1821);
    svfloat32_t v76 = svsub_f32_x(svptrue_b32(), v1819, v1821);
    svfloat32_t v91 = svadd_f32_x(svptrue_b32(), v1823, v1825);
    svfloat32_t v92 = svsub_f32_x(svptrue_b32(), v1823, v1825);
    svfloat32_t v159 = svadd_f32_x(svptrue_b32(), v1827, v1829);
    svfloat32_t v160 = svsub_f32_x(svptrue_b32(), v1827, v1829);
    svfloat32_t v175 = svadd_f32_x(svptrue_b32(), v1831, v1833);
    svfloat32_t v176 = svsub_f32_x(svptrue_b32(), v1831, v1833);
    svfloat32_t v202 = svadd_f32_x(svptrue_b32(), v1835, v1837);
    svfloat32_t v203 = svsub_f32_x(svptrue_b32(), v1835, v1837);
    svfloat32_t v218 = svadd_f32_x(svptrue_b32(), v1839, v1841);
    svfloat32_t v219 = svsub_f32_x(svptrue_b32(), v1839, v1841);
    svfloat32_t v375 = svadd_f32_x(svptrue_b32(), v1843, v1845);
    svfloat32_t v376 = svsub_f32_x(svptrue_b32(), v1843, v1845);
    svfloat32_t v391 = svadd_f32_x(svptrue_b32(), v1847, v1849);
    svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v1847, v1849);
    svfloat32_t v418 = svadd_f32_x(svptrue_b32(), v1851, v1853);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v1851, v1853);
    svfloat32_t v434 = svadd_f32_x(svptrue_b32(), v1855, v1857);
    svfloat32_t v435 = svsub_f32_x(svptrue_b32(), v1855, v1857);
    svfloat32_t v502 = svadd_f32_x(svptrue_b32(), v1859, v1861);
    svfloat32_t v503 = svsub_f32_x(svptrue_b32(), v1859, v1861);
    svfloat32_t v518 = svadd_f32_x(svptrue_b32(), v1863, v1865);
    svfloat32_t v519 = svsub_f32_x(svptrue_b32(), v1863, v1865);
    svfloat32_t v545 = svadd_f32_x(svptrue_b32(), v1867, v1869);
    svfloat32_t v546 = svsub_f32_x(svptrue_b32(), v1867, v1869);
    svfloat32_t v561 = svadd_f32_x(svptrue_b32(), v1871, v1873);
    svfloat32_t v562 = svsub_f32_x(svptrue_b32(), v1871, v1873);
    svfloat32_t zero56 = svdup_n_f32(0);
    svfloat32_t v56 = svcmla_f32_x(pred_full, zero56, v1649, v49, 90);
    svfloat32_t v57 = svadd_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v58 = svsub_f32_x(svptrue_b32(), v32, v48);
    svfloat32_t v93 = svadd_f32_x(svptrue_b32(), v75, v91);
    svfloat32_t v94 = svsub_f32_x(svptrue_b32(), v75, v91);
    svfloat32_t v110 = svmul_f32_x(svptrue_b32(), v76, v1646);
    svfloat32_t v122 = svmul_f32_x(svptrue_b32(), v92, v1648);
    svfloat32_t zero183 = svdup_n_f32(0);
    svfloat32_t v183 = svcmla_f32_x(pred_full, zero183, v1649, v176, 90);
    svfloat32_t v184 = svadd_f32_x(svptrue_b32(), v159, v175);
    svfloat32_t v185 = svsub_f32_x(svptrue_b32(), v159, v175);
    svfloat32_t zero226 = svdup_n_f32(0);
    svfloat32_t v226 = svcmla_f32_x(pred_full, zero226, v1649, v219, 90);
    svfloat32_t v227 = svadd_f32_x(svptrue_b32(), v202, v218);
    svfloat32_t v228 = svsub_f32_x(svptrue_b32(), v202, v218);
    svfloat32_t zero399 = svdup_n_f32(0);
    svfloat32_t v399 = svcmla_f32_x(pred_full, zero399, v1649, v392, 90);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v375, v391);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v375, v391);
    svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v418, v434);
    svfloat32_t v437 = svsub_f32_x(svptrue_b32(), v418, v434);
    svfloat32_t v453 = svmul_f32_x(svptrue_b32(), v419, v1646);
    svfloat32_t v465 = svmul_f32_x(svptrue_b32(), v435, v1648);
    svfloat32_t zero526 = svdup_n_f32(0);
    svfloat32_t v526 = svcmla_f32_x(pred_full, zero526, v1649, v519, 90);
    svfloat32_t v527 = svadd_f32_x(svptrue_b32(), v502, v518);
    svfloat32_t v528 = svsub_f32_x(svptrue_b32(), v502, v518);
    svfloat32_t v563 = svadd_f32_x(svptrue_b32(), v545, v561);
    svfloat32_t v564 = svsub_f32_x(svptrue_b32(), v545, v561);
    svfloat32_t v580 = svmul_f32_x(svptrue_b32(), v546, v1646);
    svfloat32_t v592 = svmul_f32_x(svptrue_b32(), v562, v1648);
    svfloat32_t v59 = svsub_f32_x(svptrue_b32(), v33, v56);
    svfloat32_t v60 = svadd_f32_x(svptrue_b32(), v33, v56);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 = svcmla_f32_x(pred_full, zero101, v1649, v94, 90);
    svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v57, v93);
    svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v57, v93);
    svfloat32_t v186 = svsub_f32_x(svptrue_b32(), v160, v183);
    svfloat32_t v187 = svadd_f32_x(svptrue_b32(), v160, v183);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v203, v226);
    svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v203, v226);
    svfloat32_t v231 = svadd_f32_x(svptrue_b32(), v184, v227);
    svfloat32_t v232 = svsub_f32_x(svptrue_b32(), v184, v227);
    svfloat32_t v287 = svmul_f32_x(svptrue_b32(), v185, v1646);
    svfloat32_t v299 = svmul_f32_x(svptrue_b32(), v228, v1648);
    svfloat32_t v402 = svsub_f32_x(svptrue_b32(), v376, v399);
    svfloat32_t v403 = svadd_f32_x(svptrue_b32(), v376, v399);
    svfloat32_t zero444 = svdup_n_f32(0);
    svfloat32_t v444 = svcmla_f32_x(pred_full, zero444, v1649, v437, 90);
    svfloat32_t v445 = svadd_f32_x(svptrue_b32(), v400, v436);
    svfloat32_t v446 = svsub_f32_x(svptrue_b32(), v400, v436);
    svfloat32_t v529 = svsub_f32_x(svptrue_b32(), v503, v526);
    svfloat32_t v530 = svadd_f32_x(svptrue_b32(), v503, v526);
    svfloat32_t zero571 = svdup_n_f32(0);
    svfloat32_t v571 = svcmla_f32_x(pred_full, zero571, v1649, v564, 90);
    svfloat32_t v572 = svadd_f32_x(svptrue_b32(), v527, v563);
    svfloat32_t v573 = svsub_f32_x(svptrue_b32(), v527, v563);
    svfloat32_t v104 = svsub_f32_x(svptrue_b32(), v58, v101);
    svfloat32_t v105 = svadd_f32_x(svptrue_b32(), v58, v101);
    svfloat32_t v130 = svcmla_f32_x(pred_full, v110, v1773, v110, 90);
    svfloat32_t v131 = svcmla_f32_x(pred_full, v122, v1649, v122, 90);
    svfloat32_t zero239 = svdup_n_f32(0);
    svfloat32_t v239 = svcmla_f32_x(pred_full, zero239, v1649, v232, 90);
    svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v102, v231);
    svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v102, v231);
    svfloat32_t v248 = svmul_f32_x(svptrue_b32(), v186, v1564);
    svfloat32_t v260 = svmul_f32_x(svptrue_b32(), v229, v1728);
    svfloat32_t v326 = svmul_f32_x(svptrue_b32(), v187, v1728);
    svfloat32_t v338 = svmul_f32_x(svptrue_b32(), v230, v1730);
    svfloat32_t v447 = svsub_f32_x(svptrue_b32(), v401, v444);
    svfloat32_t v448 = svadd_f32_x(svptrue_b32(), v401, v444);
    svfloat32_t v473 = svcmla_f32_x(pred_full, v453, v1773, v453, 90);
    svfloat32_t v474 = svcmla_f32_x(pred_full, v465, v1649, v465, 90);
    svfloat32_t v574 = svsub_f32_x(svptrue_b32(), v528, v571);
    svfloat32_t v575 = svadd_f32_x(svptrue_b32(), v528, v571);
    svfloat32_t v600 = svcmla_f32_x(pred_full, v580, v1773, v580, 90);
    svfloat32_t v601 = svcmla_f32_x(pred_full, v592, v1649, v592, 90);
    svfloat32_t v615 = svadd_f32_x(svptrue_b32(), v445, v572);
    svfloat32_t v616 = svsub_f32_x(svptrue_b32(), v445, v572);
    svfloat32_t v877 = svmul_f32_x(svptrue_b32(), v446, v1646);
    svfloat32_t v889 = svmul_f32_x(svptrue_b32(), v573, v1648);
    svfloat32_t v132 = svadd_f32_x(svptrue_b32(), v130, v131);
    svfloat32_t v133 = svsub_f32_x(svptrue_b32(), v131, v130);
    svfloat32_t v242 = svsub_f32_x(svptrue_b32(), v103, v239);
    svfloat32_t v243 = svadd_f32_x(svptrue_b32(), v103, v239);
    svfloat32_t v268 = svcmla_f32_x(pred_full, v248, v1565, v186, 90);
    svfloat32_t v269 = svcmla_f32_x(pred_full, v260, v1729, v229, 90);
    svfloat32_t v307 = svcmla_f32_x(pred_full, v287, v1773, v287, 90);
    svfloat32_t v308 = svcmla_f32_x(pred_full, v299, v1649, v299, 90);
    svfloat32_t v346 = svcmla_f32_x(pred_full, v326, v1729, v187, 90);
    svfloat32_t v347 = svcmla_f32_x(pred_full, v338, v1731, v230, 90);
    svfloat32_t v475 = svadd_f32_x(svptrue_b32(), v473, v474);
    svfloat32_t v476 = svsub_f32_x(svptrue_b32(), v474, v473);
    svfloat32_t v602 = svadd_f32_x(svptrue_b32(), v600, v601);
    svfloat32_t v603 = svsub_f32_x(svptrue_b32(), v601, v600);
    svfloat32_t zero623 = svdup_n_f32(0);
    svfloat32_t v623 = svcmla_f32_x(pred_full, zero623, v1649, v616, 90);
    svfloat32_t v624 = svadd_f32_x(svptrue_b32(), v240, v615);
    svfloat32_t v625 = svsub_f32_x(svptrue_b32(), v240, v615);
    svfloat32_t v735 = svmul_f32_x(svptrue_b32(), v447, v1564);
    svfloat32_t v747 = svmul_f32_x(svptrue_b32(), v574, v1728);
    svfloat32_t v1019 = svmul_f32_x(svptrue_b32(), v448, v1728);
    svfloat32_t v1031 = svmul_f32_x(svptrue_b32(), v575, v1730);
    svfloat32_t zero140 = svdup_n_f32(0);
    svfloat32_t v140 = svcmla_f32_x(pred_full, zero140, v1773, v133, 90);
    svfloat32_t v141 = svadd_f32_x(svptrue_b32(), v59, v132);
    svfloat32_t v142 = svsub_f32_x(svptrue_b32(), v59, v132);
    svfloat32_t v270 = svadd_f32_x(svptrue_b32(), v268, v269);
    svfloat32_t v271 = svsub_f32_x(svptrue_b32(), v269, v268);
    svfloat32_t v309 = svadd_f32_x(svptrue_b32(), v307, v308);
    svfloat32_t v310 = svsub_f32_x(svptrue_b32(), v308, v307);
    svfloat32_t v348 = svadd_f32_x(svptrue_b32(), v346, v347);
    svfloat32_t v349 = svsub_f32_x(svptrue_b32(), v347, v346);
    svfloat32_t zero483 = svdup_n_f32(0);
    svfloat32_t v483 = svcmla_f32_x(pred_full, zero483, v1773, v476, 90);
    svfloat32_t v484 = svadd_f32_x(svptrue_b32(), v402, v475);
    svfloat32_t v485 = svsub_f32_x(svptrue_b32(), v402, v475);
    svfloat32_t zero610 = svdup_n_f32(0);
    svfloat32_t v610 = svcmla_f32_x(pred_full, zero610, v1773, v603, 90);
    svfloat32_t v611 = svadd_f32_x(svptrue_b32(), v529, v602);
    svfloat32_t v612 = svsub_f32_x(svptrue_b32(), v529, v602);
    svfloat32_t v626 = svsub_f32_x(svptrue_b32(), v241, v623);
    svfloat32_t v627 = svadd_f32_x(svptrue_b32(), v241, v623);
    svint16_t v630 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v624, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v646 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v625, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v755 = svcmla_f32_x(pred_full, v735, v1565, v447, 90);
    svfloat32_t v756 = svcmla_f32_x(pred_full, v747, v1729, v574, 90);
    svfloat32_t v897 = svcmla_f32_x(pred_full, v877, v1773, v877, 90);
    svfloat32_t v898 = svcmla_f32_x(pred_full, v889, v1649, v889, 90);
    svfloat32_t v1039 = svcmla_f32_x(pred_full, v1019, v1729, v448, 90);
    svfloat32_t v1040 = svcmla_f32_x(pred_full, v1031, v1731, v575, 90);
    svfloat32_t v143 = svsub_f32_x(svptrue_b32(), v60, v140);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v60, v140);
    svfloat32_t zero278 = svdup_n_f32(0);
    svfloat32_t v278 = svcmla_f32_x(pred_full, zero278, v1773, v271, 90);
    svfloat32_t v279 = svadd_f32_x(svptrue_b32(), v141, v270);
    svfloat32_t v280 = svsub_f32_x(svptrue_b32(), v141, v270);
    svfloat32_t zero317 = svdup_n_f32(0);
    svfloat32_t v317 = svcmla_f32_x(pred_full, zero317, v1773, v310, 90);
    svfloat32_t v318 = svadd_f32_x(svptrue_b32(), v104, v309);
    svfloat32_t v319 = svsub_f32_x(svptrue_b32(), v104, v309);
    svfloat32_t zero356 = svdup_n_f32(0);
    svfloat32_t v356 = svcmla_f32_x(pred_full, zero356, v1773, v349, 90);
    svfloat32_t v486 = svsub_f32_x(svptrue_b32(), v403, v483);
    svfloat32_t v487 = svadd_f32_x(svptrue_b32(), v403, v483);
    svfloat32_t v613 = svsub_f32_x(svptrue_b32(), v530, v610);
    svfloat32_t v614 = svadd_f32_x(svptrue_b32(), v530, v610);
    svint16_t v638 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v626, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v654 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v627, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v664 = svmul_f32_x(svptrue_b32(), v484, v1523);
    svfloat32_t v676 = svmul_f32_x(svptrue_b32(), v611, v1605);
    svfloat32_t v757 = svadd_f32_x(svptrue_b32(), v755, v756);
    svfloat32_t v758 = svsub_f32_x(svptrue_b32(), v756, v755);
    svfloat32_t v899 = svadd_f32_x(svptrue_b32(), v897, v898);
    svfloat32_t v900 = svsub_f32_x(svptrue_b32(), v898, v897);
    svfloat32_t v948 = svmul_f32_x(svptrue_b32(), v485, v1687);
    svfloat32_t v960 = svmul_f32_x(svptrue_b32(), v612, v1689);
    svfloat32_t v1041 = svadd_f32_x(svptrue_b32(), v1039, v1040);
    svfloat32_t v1042 = svsub_f32_x(svptrue_b32(), v1040, v1039);
    svst1w_u64(pred_full, (unsigned *)(v1493), svreinterpret_u64_s16(v630));
    svst1w_u64(pred_full, (unsigned *)(v1511), svreinterpret_u64_s16(v646));
    svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v142, v278);
    svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v142, v278);
    svfloat32_t v320 = svsub_f32_x(svptrue_b32(), v105, v317);
    svfloat32_t v321 = svadd_f32_x(svptrue_b32(), v105, v317);
    svfloat32_t v357 = svadd_f32_x(svptrue_b32(), v143, v348);
    svfloat32_t v358 = svsub_f32_x(svptrue_b32(), v143, v348);
    svfloat32_t v359 = svsub_f32_x(svptrue_b32(), v144, v356);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v144, v356);
    svfloat32_t v684 = svcmla_f32_x(pred_full, v664, v1690, v484, 90);
    svfloat32_t v685 = svcmla_f32_x(pred_full, v676, v1606, v611, 90);
    svfloat32_t zero765 = svdup_n_f32(0);
    svfloat32_t v765 = svcmla_f32_x(pred_full, zero765, v1773, v758, 90);
    svfloat32_t v766 = svadd_f32_x(svptrue_b32(), v318, v757);
    svfloat32_t v767 = svsub_f32_x(svptrue_b32(), v318, v757);
    svfloat32_t v806 = svmul_f32_x(svptrue_b32(), v486, v1605);
    svfloat32_t v818 = svmul_f32_x(svptrue_b32(), v613, v1607);
    svfloat32_t zero907 = svdup_n_f32(0);
    svfloat32_t v907 = svcmla_f32_x(pred_full, zero907, v1773, v900, 90);
    svfloat32_t v908 = svadd_f32_x(svptrue_b32(), v242, v899);
    svfloat32_t v909 = svsub_f32_x(svptrue_b32(), v242, v899);
    svfloat32_t v968 = svcmla_f32_x(pred_full, v948, v1688, v485, 90);
    svfloat32_t v969 = svcmla_f32_x(pred_full, v960, v1690, v612, 90);
    svfloat32_t zero1049 = svdup_n_f32(0);
    svfloat32_t v1049 = svcmla_f32_x(pred_full, zero1049, v1773, v1042, 90);
    svfloat32_t v1090 = svmul_f32_x(svptrue_b32(), v487, v1769);
    svfloat32_t v1102 = svmul_f32_x(svptrue_b32(), v614, v1771);
    svst1w_u64(pred_full, (unsigned *)(v1502), svreinterpret_u64_s16(v638));
    svst1w_u64(pred_full, (unsigned *)(v1520), svreinterpret_u64_s16(v654));
    svfloat32_t v686 = svadd_f32_x(svptrue_b32(), v684, v685);
    svfloat32_t v687 = svsub_f32_x(svptrue_b32(), v685, v684);
    svfloat32_t v768 = svsub_f32_x(svptrue_b32(), v319, v765);
    svfloat32_t v769 = svadd_f32_x(svptrue_b32(), v319, v765);
    svint16_t v772 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v766, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v788 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v767, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v826 = svcmla_f32_x(pred_full, v806, v1606, v486, 90);
    svfloat32_t v827 = svcmla_f32_x(pred_full, v818, v1770, v613, 90);
    svfloat32_t v910 = svsub_f32_x(svptrue_b32(), v243, v907);
    svfloat32_t v911 = svadd_f32_x(svptrue_b32(), v243, v907);
    svint16_t v914 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v908, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v930 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v909, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v970 = svadd_f32_x(svptrue_b32(), v968, v969);
    svfloat32_t v971 = svsub_f32_x(svptrue_b32(), v969, v968);
    svfloat32_t v1050 = svadd_f32_x(svptrue_b32(), v320, v1041);
    svfloat32_t v1051 = svsub_f32_x(svptrue_b32(), v320, v1041);
    svfloat32_t v1052 = svsub_f32_x(svptrue_b32(), v321, v1049);
    svfloat32_t v1053 = svadd_f32_x(svptrue_b32(), v321, v1049);
    svfloat32_t v1110 = svcmla_f32_x(pred_full, v1090, v1770, v487, 90);
    svfloat32_t v1111 = svcmla_f32_x(pred_full, v1102, v1772, v614, 90);
    svfloat32_t zero694 = svdup_n_f32(0);
    svfloat32_t v694 = svcmla_f32_x(pred_full, zero694, v1773, v687, 90);
    svfloat32_t v695 = svadd_f32_x(svptrue_b32(), v279, v686);
    svfloat32_t v696 = svsub_f32_x(svptrue_b32(), v279, v686);
    svint16_t v780 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v768, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v796 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v769, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v828 = svadd_f32_x(svptrue_b32(), v826, v827);
    svfloat32_t v829 = svsub_f32_x(svptrue_b32(), v827, v826);
    svint16_t v922 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v910, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v938 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v911, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t zero978 = svdup_n_f32(0);
    svfloat32_t v978 = svcmla_f32_x(pred_full, zero978, v1773, v971, 90);
    svfloat32_t v979 = svadd_f32_x(svptrue_b32(), v281, v970);
    svfloat32_t v980 = svsub_f32_x(svptrue_b32(), v281, v970);
    svint16_t v1056 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1050, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1064 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1052, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1072 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1051, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1080 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1053, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svfloat32_t v1112 = svadd_f32_x(svptrue_b32(), v1110, v1111);
    svfloat32_t v1113 = svsub_f32_x(svptrue_b32(), v1111, v1110);
    svst1w_u64(pred_full, (unsigned *)(v1575), svreinterpret_u64_s16(v772));
    svst1w_u64(pred_full, (unsigned *)(v1593), svreinterpret_u64_s16(v788));
    svst1w_u64(pred_full, (unsigned *)(v1657), svreinterpret_u64_s16(v914));
    svst1w_u64(pred_full, (unsigned *)(v1675), svreinterpret_u64_s16(v930));
    svfloat32_t v697 = svsub_f32_x(svptrue_b32(), v280, v694);
    svfloat32_t v698 = svadd_f32_x(svptrue_b32(), v280, v694);
    svint16_t v701 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v695, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v717 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v696, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t zero836 = svdup_n_f32(0);
    svfloat32_t v836 = svcmla_f32_x(pred_full, zero836, v1773, v829, 90);
    svfloat32_t v837 = svadd_f32_x(svptrue_b32(), v357, v828);
    svfloat32_t v838 = svsub_f32_x(svptrue_b32(), v357, v828);
    svfloat32_t v981 = svsub_f32_x(svptrue_b32(), v282, v978);
    svfloat32_t v982 = svadd_f32_x(svptrue_b32(), v282, v978);
    svint16_t v985 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v979, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1001 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v980, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t zero1120 = svdup_n_f32(0);
    svfloat32_t v1120 = svcmla_f32_x(pred_full, zero1120, v1773, v1113, 90);
    svfloat32_t v1121 = svadd_f32_x(svptrue_b32(), v359, v1112);
    svfloat32_t v1122 = svsub_f32_x(svptrue_b32(), v359, v1112);
    svst1w_u64(pred_full, (unsigned *)(v1584), svreinterpret_u64_s16(v780));
    svst1w_u64(pred_full, (unsigned *)(v1602), svreinterpret_u64_s16(v796));
    svst1w_u64(pred_full, (unsigned *)(v1666), svreinterpret_u64_s16(v922));
    svst1w_u64(pred_full, (unsigned *)(v1684), svreinterpret_u64_s16(v938));
    svst1w_u64(pred_full, (unsigned *)(v1739), svreinterpret_u64_s16(v1056));
    svst1w_u64(pred_full, (unsigned *)(v1748), svreinterpret_u64_s16(v1064));
    svst1w_u64(pred_full, (unsigned *)(v1757), svreinterpret_u64_s16(v1072));
    svst1w_u64(pred_full, (unsigned *)(v1766), svreinterpret_u64_s16(v1080));
    svint16_t v709 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v697, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v725 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v698, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v839 = svsub_f32_x(svptrue_b32(), v358, v836);
    svfloat32_t v840 = svadd_f32_x(svptrue_b32(), v358, v836);
    svint16_t v843 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v837, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v859 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v838, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v993 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v981, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1009 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v982, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svfloat32_t v1123 = svsub_f32_x(svptrue_b32(), v360, v1120);
    svfloat32_t v1124 = svadd_f32_x(svptrue_b32(), v360, v1120);
    svint16_t v1127 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1121, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1143 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1122, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1534), svreinterpret_u64_s16(v701));
    svst1w_u64(pred_full, (unsigned *)(v1552), svreinterpret_u64_s16(v717));
    svst1w_u64(pred_full, (unsigned *)(v1698), svreinterpret_u64_s16(v985));
    svst1w_u64(pred_full, (unsigned *)(v1716), svreinterpret_u64_s16(v1001));
    svint16_t v851 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v839, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v867 = svtbl_s16(
        svreinterpret_s16_s32(svcvt_s32_f32_x(
            pred_full, svmul_n_f32_x(pred_full, v840, (float)(1ULL << 31ULL)))),
        svreinterpret_u16_u64(
            svindex_u64(0xffffffff00030001ULL, 0x0000000000040004ULL)));
    svint16_t v1135 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1123, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svint16_t v1151 =
        svtbl_s16(svreinterpret_s16_s32(svcvt_s32_f32_x(
                      pred_full,
                      svmul_n_f32_x(pred_full, v1124, (float)(1ULL << 31ULL)))),
                  svreinterpret_u16_u64(svindex_u64(0xffffffff00030001ULL,
                                                    0x0000000000040004ULL)));
    svst1w_u64(pred_full, (unsigned *)(v1543), svreinterpret_u64_s16(v709));
    svst1w_u64(pred_full, (unsigned *)(v1561), svreinterpret_u64_s16(v725));
    svst1w_u64(pred_full, (unsigned *)(v1616), svreinterpret_u64_s16(v843));
    svst1w_u64(pred_full, (unsigned *)(v1634), svreinterpret_u64_s16(v859));
    svst1w_u64(pred_full, (unsigned *)(v1707), svreinterpret_u64_s16(v993));
    svst1w_u64(pred_full, (unsigned *)(v1725), svreinterpret_u64_s16(v1009));
    svst1w_u64(pred_full, (unsigned *)(v1780), svreinterpret_u64_s16(v1127));
    svst1w_u64(pred_full, (unsigned *)(v1798), svreinterpret_u64_s16(v1143));
    svst1w_u64(pred_full, (unsigned *)(v1625), svreinterpret_u64_s16(v851));
    svst1w_u64(pred_full, (unsigned *)(v1643), svreinterpret_u64_s16(v867));
    svst1w_u64(pred_full, (unsigned *)(v1789), svreinterpret_u64_s16(v1135));
    svst1w_u64(pred_full, (unsigned *)(v1807), svreinterpret_u64_s16(v1151));
    v5 += v11;
    v6 += v12;
  }
}
#endif
