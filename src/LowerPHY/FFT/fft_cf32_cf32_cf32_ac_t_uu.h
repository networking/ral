/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "fft_helper.h"

#ifdef __cplusplus
extern "C" {
#endif

typedef void(cf32_cf32_cf32_ac_t_uu_fft_t)(const armral_cmplx_f32_t *x,
                                           armral_cmplx_f32_t *y, int istride,
                                           int ostride,
                                           const armral_cmplx_f32_t *w,
                                           int howmany, float dir);

cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu7;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu9;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu11;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu13;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu14;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu15;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu16;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu17;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu18;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu19;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu20;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu21;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu22;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu24;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu25;
cf32_cf32_cf32_ac_t_uu_fft_t armral_fft_cf32_cf32_cf32_ac_t_uu32;

#ifdef __cplusplus
} // extern "C"
#endif