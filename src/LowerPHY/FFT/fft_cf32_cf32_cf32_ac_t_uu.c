/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "fft_cf32_cf32_cf32_ac_t_uu.h"

#include <arm_neon.h>
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu7(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v356 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v237 = -1.1666666666666665e+00F;
    float v242 = 7.9015646852540022e-01F;
    float v247 = 5.5854267289647742e-02F;
    float v252 = 7.3430220123575241e-01F;
    float v256 = 4.4095855184409838e-01F;
    float v257 = -4.4095855184409838e-01F;
    float v264 = 3.4087293062393137e-01F;
    float v265 = -3.4087293062393137e-01F;
    float v272 = -5.3396936033772524e-01F;
    float v273 = 5.3396936033772524e-01F;
    float v280 = 8.7484229096165667e-01F;
    float v281 = -8.7484229096165667e-01F;
    float32x2_t v283 = (float32x2_t){v4, v4};
    const float32x2_t *v648 = &v5[istride];
    float32x2_t *v732 = &v6[ostride];
    float32x2_t v238 = (float32x2_t){v237, v237};
    float32x2_t v243 = (float32x2_t){v242, v242};
    float32x2_t v248 = (float32x2_t){v247, v247};
    float32x2_t v253 = (float32x2_t){v252, v252};
    float32x2_t v258 = (float32x2_t){v256, v257};
    float32x2_t v266 = (float32x2_t){v264, v265};
    float32x2_t v274 = (float32x2_t){v272, v273};
    float32x2_t v282 = (float32x2_t){v280, v281};
    const float32x2_t *v713 = &v5[0];
    float32x2_t *v723 = &v6[0];
    float32x4_t v781 = vld1q_f32((const float32_t *)v648);
    float32x4_t v61 = vtrn1q_f32(v781, v781);
    float32x4_t v62 = vtrn2q_f32(v781, v781);
    float32x4_t v66 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v68 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v78 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v80 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v128 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v130 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v140 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v142 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v190 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v192 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v202 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v204 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v239 = vcombine_f32(v238, v238);
    float32x4_t v244 = vcombine_f32(v243, v243);
    float32x4_t v249 = vcombine_f32(v248, v248);
    float32x4_t v254 = vcombine_f32(v253, v253);
    float32x2_t v260 = vmul_f32(v283, v258);
    float32x2_t v268 = vmul_f32(v283, v266);
    float32x2_t v276 = vmul_f32(v283, v274);
    float32x2_t v284 = vmul_f32(v283, v282);
    const float32x2_t *v657 = &v5[istride * 6];
    const float32x2_t *v668 = &v5[istride * 4];
    const float32x2_t *v678 = &v5[istride * 3];
    const float32x2_t *v690 = &v5[istride * 2];
    const float32x2_t *v700 = &v5[istride * 5];
    float32x2_t *v741 = &v6[ostride * 2];
    float32x2_t *v750 = &v6[ostride * 3];
    float32x2_t *v759 = &v6[ostride * 4];
    float32x2_t *v768 = &v6[ostride * 5];
    float32x2_t *v777 = &v6[ostride * 6];
    float32x4_t v793 = vld1q_f32((const float32_t *)v713);
    float32x4_t v67 = vmulq_f32(v61, v66);
    float32x4_t v262 = vcombine_f32(v260, v260);
    float32x4_t v270 = vcombine_f32(v268, v268);
    float32x4_t v278 = vcombine_f32(v276, v276);
    float32x4_t v286 = vcombine_f32(v284, v284);
    float32x4_t v783 = vld1q_f32((const float32_t *)v657);
    float32x4_t v785 = vld1q_f32((const float32_t *)v668);
    float32x4_t v787 = vld1q_f32((const float32_t *)v678);
    float32x4_t v789 = vld1q_f32((const float32_t *)v690);
    float32x4_t v791 = vld1q_f32((const float32_t *)v700);
    float32x4_t v70 = vfmaq_f32(v67, v62, v68);
    float32x4_t v73 = vtrn1q_f32(v783, v783);
    float32x4_t v74 = vtrn2q_f32(v783, v783);
    float32x4_t v123 = vtrn1q_f32(v785, v785);
    float32x4_t v124 = vtrn2q_f32(v785, v785);
    float32x4_t v135 = vtrn1q_f32(v787, v787);
    float32x4_t v136 = vtrn2q_f32(v787, v787);
    float32x4_t v185 = vtrn1q_f32(v789, v789);
    float32x4_t v186 = vtrn2q_f32(v789, v789);
    float32x4_t v197 = vtrn1q_f32(v791, v791);
    float32x4_t v198 = vtrn2q_f32(v791, v791);
    float32x4_t v79 = vmulq_f32(v73, v78);
    float32x4_t v129 = vmulq_f32(v123, v128);
    float32x4_t v141 = vmulq_f32(v135, v140);
    float32x4_t v191 = vmulq_f32(v185, v190);
    float32x4_t v203 = vmulq_f32(v197, v202);
    float32x4_t v82 = vfmaq_f32(v79, v74, v80);
    float32x4_t v132 = vfmaq_f32(v129, v124, v130);
    float32x4_t v144 = vfmaq_f32(v141, v136, v142);
    float32x4_t v194 = vfmaq_f32(v191, v186, v192);
    float32x4_t v206 = vfmaq_f32(v203, v198, v204);
    float32x4_t v207 = vaddq_f32(v70, v82);
    float32x4_t v208 = vsubq_f32(v70, v82);
    float32x4_t v209 = vaddq_f32(v132, v144);
    float32x4_t v210 = vsubq_f32(v132, v144);
    float32x4_t v211 = vaddq_f32(v194, v206);
    float32x4_t v212 = vsubq_f32(v194, v206);
    float32x4_t v213 = vaddq_f32(v207, v209);
    float32x4_t v223 = vsubq_f32(v207, v209);
    float32x4_t v224 = vsubq_f32(v209, v211);
    float32x4_t v225 = vsubq_f32(v211, v207);
    float32x4_t v226 = vaddq_f32(v208, v210);
    float32x4_t v228 = vsubq_f32(v208, v210);
    float32x4_t v229 = vsubq_f32(v210, v212);
    float32x4_t v230 = vsubq_f32(v212, v208);
    float32x4_t v214 = vaddq_f32(v213, v211);
    float32x4_t v227 = vaddq_f32(v226, v212);
    float32x4_t v245 = vmulq_f32(v223, v244);
    float32x4_t v250 = vmulq_f32(v224, v249);
    float32x4_t v255 = vmulq_f32(v225, v254);
    float32x4_t v269 = vrev64q_f32(v228);
    float32x4_t v277 = vrev64q_f32(v229);
    float32x4_t v285 = vrev64q_f32(v230);
    float32x4_t v222 = vaddq_f32(v214, v793);
    float32x4_t v240 = vmulq_f32(v214, v239);
    float32x4_t v261 = vrev64q_f32(v227);
    float32x4_t v271 = vmulq_f32(v269, v270);
    float32x4_t v279 = vmulq_f32(v277, v278);
    float32x4_t v287 = vmulq_f32(v285, v286);
    float32x4_t v263 = vmulq_f32(v261, v262);
    float32x4_t v288 = vaddq_f32(v222, v240);
    vst1q_f32((float32_t *)v723, v222);
    float32x4_t v289 = vaddq_f32(v288, v245);
    float32x4_t v291 = vsubq_f32(v288, v245);
    float32x4_t v293 = vsubq_f32(v288, v250);
    float32x4_t v295 = vaddq_f32(v263, v271);
    float32x4_t v297 = vsubq_f32(v263, v271);
    float32x4_t v299 = vsubq_f32(v263, v279);
    float32x4_t v290 = vaddq_f32(v289, v250);
    float32x4_t v292 = vsubq_f32(v291, v255);
    float32x4_t v294 = vaddq_f32(v293, v255);
    float32x4_t v296 = vaddq_f32(v295, v279);
    float32x4_t v298 = vsubq_f32(v297, v287);
    float32x4_t v300 = vaddq_f32(v299, v287);
    float32x4_t v301 = vaddq_f32(v290, v296);
    float32x4_t v302 = vsubq_f32(v290, v296);
    float32x4_t v303 = vaddq_f32(v292, v298);
    float32x4_t v304 = vsubq_f32(v292, v298);
    float32x4_t v305 = vaddq_f32(v294, v300);
    float32x4_t v306 = vsubq_f32(v294, v300);
    vst1q_f32((float32_t *)v732, v302);
    vst1q_f32((float32_t *)v741, v304);
    vst1q_f32((float32_t *)v750, v305);
    vst1q_f32((float32_t *)v759, v306);
    vst1q_f32((float32_t *)v768, v303);
    vst1q_f32((float32_t *)v777, v301);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v356 * 2; j < howmany; j += 1) {
    float32x2_t v368 = v5[istride];
    float v541 = -1.1666666666666665e+00F;
    float v545 = 7.9015646852540022e-01F;
    float v549 = 5.5854267289647742e-02F;
    float v553 = 7.3430220123575241e-01F;
    float v556 = 4.4095855184409838e-01F;
    float v557 = -4.4095855184409838e-01F;
    float v563 = 3.4087293062393137e-01F;
    float v564 = -3.4087293062393137e-01F;
    float v570 = -5.3396936033772524e-01F;
    float v571 = 5.3396936033772524e-01F;
    float v577 = 8.7484229096165667e-01F;
    float v578 = -8.7484229096165667e-01F;
    float32x2_t v580 = (float32x2_t){v4, v4};
    float32x2_t v395 = v7[0];
    float32x2_t v396 = vtrn1_f32(v368, v368);
    float32x2_t v397 = vtrn2_f32(v368, v368);
    float32x2_t v400 = v7[1];
    float32x2_t v405 = v7[10];
    float32x2_t v410 = v7[11];
    float32x2_t v445 = v7[6];
    float32x2_t v450 = v7[7];
    float32x2_t v455 = v7[4];
    float32x2_t v460 = v7[5];
    float32x2_t v495 = v7[2];
    float32x2_t v500 = v7[3];
    float32x2_t v505 = v7[8];
    float32x2_t v510 = v7[9];
    float32x2_t v526 = v5[0];
    float32x2_t v542 = (float32x2_t){v541, v541};
    float32x2_t v546 = (float32x2_t){v545, v545};
    float32x2_t v550 = (float32x2_t){v549, v549};
    float32x2_t v554 = (float32x2_t){v553, v553};
    float32x2_t v558 = (float32x2_t){v556, v557};
    float32x2_t v565 = (float32x2_t){v563, v564};
    float32x2_t v572 = (float32x2_t){v570, v571};
    float32x2_t v579 = (float32x2_t){v577, v578};
    float32x2_t v383 = v5[istride * 6];
    float32x2_t v401 = vmul_f32(v396, v395);
    float32x2_t v418 = v5[istride * 4];
    float32x2_t v433 = v5[istride * 3];
    float32x2_t v468 = v5[istride * 2];
    float32x2_t v483 = v5[istride * 5];
    float32x2_t v560 = vmul_f32(v580, v558);
    float32x2_t v567 = vmul_f32(v580, v565);
    float32x2_t v574 = vmul_f32(v580, v572);
    float32x2_t v581 = vmul_f32(v580, v579);
    float32x2_t v403 = vfma_f32(v401, v397, v400);
    float32x2_t v406 = vtrn1_f32(v383, v383);
    float32x2_t v407 = vtrn2_f32(v383, v383);
    float32x2_t v446 = vtrn1_f32(v418, v418);
    float32x2_t v447 = vtrn2_f32(v418, v418);
    float32x2_t v456 = vtrn1_f32(v433, v433);
    float32x2_t v457 = vtrn2_f32(v433, v433);
    float32x2_t v496 = vtrn1_f32(v468, v468);
    float32x2_t v497 = vtrn2_f32(v468, v468);
    float32x2_t v506 = vtrn1_f32(v483, v483);
    float32x2_t v507 = vtrn2_f32(v483, v483);
    float32x2_t v411 = vmul_f32(v406, v405);
    float32x2_t v451 = vmul_f32(v446, v445);
    float32x2_t v461 = vmul_f32(v456, v455);
    float32x2_t v501 = vmul_f32(v496, v495);
    float32x2_t v511 = vmul_f32(v506, v505);
    float32x2_t v413 = vfma_f32(v411, v407, v410);
    float32x2_t v453 = vfma_f32(v451, v447, v450);
    float32x2_t v463 = vfma_f32(v461, v457, v460);
    float32x2_t v503 = vfma_f32(v501, v497, v500);
    float32x2_t v513 = vfma_f32(v511, v507, v510);
    float32x2_t v514 = vadd_f32(v403, v413);
    float32x2_t v515 = vsub_f32(v403, v413);
    float32x2_t v516 = vadd_f32(v453, v463);
    float32x2_t v517 = vsub_f32(v453, v463);
    float32x2_t v518 = vadd_f32(v503, v513);
    float32x2_t v519 = vsub_f32(v503, v513);
    float32x2_t v520 = vadd_f32(v514, v516);
    float32x2_t v528 = vsub_f32(v514, v516);
    float32x2_t v529 = vsub_f32(v516, v518);
    float32x2_t v530 = vsub_f32(v518, v514);
    float32x2_t v531 = vadd_f32(v515, v517);
    float32x2_t v533 = vsub_f32(v515, v517);
    float32x2_t v534 = vsub_f32(v517, v519);
    float32x2_t v535 = vsub_f32(v519, v515);
    float32x2_t v521 = vadd_f32(v520, v518);
    float32x2_t v532 = vadd_f32(v531, v519);
    float32x2_t v547 = vmul_f32(v528, v546);
    float32x2_t v551 = vmul_f32(v529, v550);
    float32x2_t v555 = vmul_f32(v530, v554);
    float32x2_t v568 = vrev64_f32(v533);
    float32x2_t v575 = vrev64_f32(v534);
    float32x2_t v582 = vrev64_f32(v535);
    float32x2_t v527 = vadd_f32(v521, v526);
    float32x2_t v543 = vmul_f32(v521, v542);
    float32x2_t v561 = vrev64_f32(v532);
    float32x2_t v569 = vmul_f32(v568, v567);
    float32x2_t v576 = vmul_f32(v575, v574);
    float32x2_t v583 = vmul_f32(v582, v581);
    float32x2_t v562 = vmul_f32(v561, v560);
    float32x2_t v584 = vadd_f32(v527, v543);
    v6[0] = v527;
    float32x2_t v585 = vadd_f32(v584, v547);
    float32x2_t v587 = vsub_f32(v584, v547);
    float32x2_t v589 = vsub_f32(v584, v551);
    float32x2_t v591 = vadd_f32(v562, v569);
    float32x2_t v593 = vsub_f32(v562, v569);
    float32x2_t v595 = vsub_f32(v562, v576);
    float32x2_t v586 = vadd_f32(v585, v551);
    float32x2_t v588 = vsub_f32(v587, v555);
    float32x2_t v590 = vadd_f32(v589, v555);
    float32x2_t v592 = vadd_f32(v591, v576);
    float32x2_t v594 = vsub_f32(v593, v583);
    float32x2_t v596 = vadd_f32(v595, v583);
    float32x2_t v597 = vadd_f32(v586, v592);
    float32x2_t v598 = vsub_f32(v586, v592);
    float32x2_t v599 = vadd_f32(v588, v594);
    float32x2_t v600 = vsub_f32(v588, v594);
    float32x2_t v601 = vadd_f32(v590, v596);
    float32x2_t v602 = vsub_f32(v590, v596);
    v6[ostride] = v598;
    v6[ostride * 2] = v600;
    v6[ostride * 3] = v601;
    v6[ostride * 4] = v602;
    v6[ostride * 5] = v599;
    v6[ostride * 6] = v597;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu7(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v138 = -1.1666666666666665e+00F;
    float v143 = 7.9015646852540022e-01F;
    float v148 = 5.5854267289647742e-02F;
    float v153 = 7.3430220123575241e-01F;
    float v158 = -4.4095855184409838e-01F;
    float v165 = -3.4087293062393137e-01F;
    float v172 = 5.3396936033772524e-01F;
    float v179 = -8.7484229096165667e-01F;
    const float32x2_t *v259 = &v5[v0];
    float32x2_t *v342 = &v6[v2];
    int64_t v30 = v0 * 6;
    int64_t v49 = v0 * 4;
    int64_t v60 = v0 * 3;
    int64_t v79 = v0 * 2;
    int64_t v90 = v0 * 5;
    float v161 = v4 * v158;
    float v168 = v4 * v165;
    float v175 = v4 * v172;
    float v182 = v4 * v179;
    int64_t v219 = v2 * 2;
    int64_t v226 = v2 * 3;
    int64_t v233 = v2 * 4;
    int64_t v240 = v2 * 5;
    int64_t v247 = v2 * 6;
    const float32x2_t *v314 = &v5[0];
    svfloat32_t v318 = svdup_n_f32(v138);
    svfloat32_t v319 = svdup_n_f32(v143);
    svfloat32_t v320 = svdup_n_f32(v148);
    svfloat32_t v321 = svdup_n_f32(v153);
    float32x2_t *v333 = &v6[0];
    svfloat32_t v391 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v259)[0]));
    svfloat32_t v42 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v46 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v72 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v76 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v102 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v106 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    const float32x2_t *v268 = &v5[v30];
    const float32x2_t *v277 = &v5[v49];
    const float32x2_t *v286 = &v5[v60];
    const float32x2_t *v295 = &v5[v79];
    const float32x2_t *v304 = &v5[v90];
    svfloat32_t v322 = svdup_n_f32(v161);
    svfloat32_t v323 = svdup_n_f32(v168);
    svfloat32_t v324 = svdup_n_f32(v175);
    svfloat32_t v325 = svdup_n_f32(v182);
    float32x2_t *v351 = &v6[v219];
    float32x2_t *v360 = &v6[v226];
    float32x2_t *v369 = &v6[v233];
    float32x2_t *v378 = &v6[v240];
    float32x2_t *v387 = &v6[v247];
    svfloat32_t v403 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v314)[0]));
    svfloat32_t zero43 = svdup_n_f32(0);
    svfloat32_t v43 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero43, v391, v42, 0),
                     v391, v42, 90);
    svfloat32_t v393 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v268)[0]));
    svfloat32_t v395 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v277)[0]));
    svfloat32_t v397 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v286)[0]));
    svfloat32_t v399 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v295)[0]));
    svfloat32_t v401 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v304)[0]));
    svfloat32_t zero47 = svdup_n_f32(0);
    svfloat32_t v47 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero47, v393, v46, 0),
                     v393, v46, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v395, v72, 0),
                     v395, v72, 90);
    svfloat32_t zero77 = svdup_n_f32(0);
    svfloat32_t v77 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero77, v397, v76, 0),
                     v397, v76, 90);
    svfloat32_t zero103 = svdup_n_f32(0);
    svfloat32_t v103 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero103, v399, v102, 0),
                     v399, v102, 90);
    svfloat32_t zero107 = svdup_n_f32(0);
    svfloat32_t v107 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero107, v401, v106, 0),
                     v401, v106, 90);
    svfloat32_t v108 = svadd_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v109 = svsub_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v110 = svadd_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v111 = svsub_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v112 = svadd_f32_x(svptrue_b32(), v103, v107);
    svfloat32_t v113 = svsub_f32_x(svptrue_b32(), v103, v107);
    svfloat32_t v114 = svadd_f32_x(svptrue_b32(), v108, v110);
    svfloat32_t v124 = svsub_f32_x(svptrue_b32(), v108, v110);
    svfloat32_t v125 = svsub_f32_x(svptrue_b32(), v110, v112);
    svfloat32_t v126 = svsub_f32_x(svptrue_b32(), v112, v108);
    svfloat32_t v127 = svadd_f32_x(svptrue_b32(), v109, v111);
    svfloat32_t v129 = svsub_f32_x(svptrue_b32(), v109, v111);
    svfloat32_t v130 = svsub_f32_x(svptrue_b32(), v111, v113);
    svfloat32_t v131 = svsub_f32_x(svptrue_b32(), v113, v109);
    svfloat32_t v115 = svadd_f32_x(svptrue_b32(), v114, v112);
    svfloat32_t v128 = svadd_f32_x(svptrue_b32(), v127, v113);
    svfloat32_t zero170 = svdup_n_f32(0);
    svfloat32_t v170 = svcmla_f32_x(pred_full, zero170, v323, v129, 90);
    svfloat32_t zero177 = svdup_n_f32(0);
    svfloat32_t v177 = svcmla_f32_x(pred_full, zero177, v324, v130, 90);
    svfloat32_t zero184 = svdup_n_f32(0);
    svfloat32_t v184 = svcmla_f32_x(pred_full, zero184, v325, v131, 90);
    svfloat32_t v123 = svadd_f32_x(svptrue_b32(), v115, v403);
    svfloat32_t zero163 = svdup_n_f32(0);
    svfloat32_t v163 = svcmla_f32_x(pred_full, zero163, v322, v128, 90);
    svfloat32_t v185 = svmla_f32_x(pred_full, v123, v115, v318);
    svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v163, v170);
    svfloat32_t v194 = svsub_f32_x(svptrue_b32(), v163, v170);
    svfloat32_t v196 = svsub_f32_x(svptrue_b32(), v163, v177);
    svst1_f64(pred_full, (double *)(v333), svreinterpret_f64_f32(v123));
    svfloat32_t v186 = svmla_f32_x(pred_full, v185, v124, v319);
    svfloat32_t v188 = svmls_f32_x(pred_full, v185, v124, v319);
    svfloat32_t v190 = svmls_f32_x(pred_full, v185, v125, v320);
    svfloat32_t v193 = svadd_f32_x(svptrue_b32(), v192, v177);
    svfloat32_t v195 = svsub_f32_x(svptrue_b32(), v194, v184);
    svfloat32_t v197 = svadd_f32_x(svptrue_b32(), v196, v184);
    svfloat32_t v187 = svmla_f32_x(pred_full, v186, v125, v320);
    svfloat32_t v189 = svmls_f32_x(pred_full, v188, v126, v321);
    svfloat32_t v191 = svmla_f32_x(pred_full, v190, v126, v321);
    svfloat32_t v198 = svadd_f32_x(svptrue_b32(), v187, v193);
    svfloat32_t v199 = svsub_f32_x(svptrue_b32(), v187, v193);
    svfloat32_t v200 = svadd_f32_x(svptrue_b32(), v189, v195);
    svfloat32_t v201 = svsub_f32_x(svptrue_b32(), v189, v195);
    svfloat32_t v202 = svadd_f32_x(svptrue_b32(), v191, v197);
    svfloat32_t v203 = svsub_f32_x(svptrue_b32(), v191, v197);
    svst1_f64(pred_full, (double *)(v342), svreinterpret_f64_f32(v199));
    svst1_f64(pred_full, (double *)(v351), svreinterpret_f64_f32(v201));
    svst1_f64(pred_full, (double *)(v360), svreinterpret_f64_f32(v202));
    svst1_f64(pred_full, (double *)(v369), svreinterpret_f64_f32(v203));
    svst1_f64(pred_full, (double *)(v378), svreinterpret_f64_f32(v200));
    svst1_f64(pred_full, (double *)(v387), svreinterpret_f64_f32(v198));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu9(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v454 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v302 = -5.0000000000000000e-01F;
    float v315 = -1.4999999999999998e+00F;
    float v319 = 8.6602540378443871e-01F;
    float v320 = -8.6602540378443871e-01F;
    float v328 = 7.6604444311897801e-01F;
    float v333 = 9.3969262078590832e-01F;
    float v338 = -1.7364817766693039e-01F;
    float v342 = 6.4278760968653925e-01F;
    float v343 = -6.4278760968653925e-01F;
    float v350 = -3.4202014332566888e-01F;
    float v351 = 3.4202014332566888e-01F;
    float v358 = 9.8480775301220802e-01F;
    float v359 = -9.8480775301220802e-01F;
    float32x2_t v361 = (float32x2_t){v4, v4};
    const float32x2_t *v826 = &v5[istride];
    float32x2_t *v932 = &v6[ostride];
    float32x2_t v303 = (float32x2_t){v302, v302};
    float32x2_t v316 = (float32x2_t){v315, v315};
    float32x2_t v321 = (float32x2_t){v319, v320};
    float32x2_t v329 = (float32x2_t){v328, v328};
    float32x2_t v334 = (float32x2_t){v333, v333};
    float32x2_t v339 = (float32x2_t){v338, v338};
    float32x2_t v344 = (float32x2_t){v342, v343};
    float32x2_t v352 = (float32x2_t){v350, v351};
    float32x2_t v360 = (float32x2_t){v358, v359};
    const float32x2_t *v913 = &v5[0];
    float32x2_t *v923 = &v6[0];
    float32x4_t v999 = vld1q_f32((const float32_t *)v826);
    float32x4_t v61 = vtrn1q_f32(v999, v999);
    float32x4_t v62 = vtrn2q_f32(v999, v999);
    float32x4_t v66 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v68 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v78 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v80 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v128 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v130 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v140 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v142 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v190 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v192 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v202 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v204 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v252 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v254 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v264 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v266 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v304 = vcombine_f32(v303, v303);
    float32x4_t v317 = vcombine_f32(v316, v316);
    float32x2_t v323 = vmul_f32(v361, v321);
    float32x4_t v330 = vcombine_f32(v329, v329);
    float32x4_t v335 = vcombine_f32(v334, v334);
    float32x4_t v340 = vcombine_f32(v339, v339);
    float32x2_t v346 = vmul_f32(v361, v344);
    float32x2_t v354 = vmul_f32(v361, v352);
    float32x2_t v362 = vmul_f32(v361, v360);
    const float32x2_t *v835 = &v5[istride * 8];
    const float32x2_t *v846 = &v5[istride * 7];
    const float32x2_t *v856 = &v5[istride * 2];
    const float32x2_t *v868 = &v5[istride * 3];
    const float32x2_t *v878 = &v5[istride * 6];
    const float32x2_t *v890 = &v5[istride * 4];
    const float32x2_t *v900 = &v5[istride * 5];
    float32x2_t *v941 = &v6[ostride * 2];
    float32x2_t *v950 = &v6[ostride * 3];
    float32x2_t *v959 = &v6[ostride * 4];
    float32x2_t *v968 = &v6[ostride * 5];
    float32x2_t *v977 = &v6[ostride * 6];
    float32x2_t *v986 = &v6[ostride * 7];
    float32x2_t *v995 = &v6[ostride * 8];
    float32x4_t v1015 = vld1q_f32((const float32_t *)v913);
    float32x4_t v67 = vmulq_f32(v61, v66);
    float32x4_t v325 = vcombine_f32(v323, v323);
    float32x4_t v348 = vcombine_f32(v346, v346);
    float32x4_t v356 = vcombine_f32(v354, v354);
    float32x4_t v364 = vcombine_f32(v362, v362);
    float32x4_t v1001 = vld1q_f32((const float32_t *)v835);
    float32x4_t v1003 = vld1q_f32((const float32_t *)v846);
    float32x4_t v1005 = vld1q_f32((const float32_t *)v856);
    float32x4_t v1007 = vld1q_f32((const float32_t *)v868);
    float32x4_t v1009 = vld1q_f32((const float32_t *)v878);
    float32x4_t v1011 = vld1q_f32((const float32_t *)v890);
    float32x4_t v1013 = vld1q_f32((const float32_t *)v900);
    float32x4_t v70 = vfmaq_f32(v67, v62, v68);
    float32x4_t v73 = vtrn1q_f32(v1001, v1001);
    float32x4_t v74 = vtrn2q_f32(v1001, v1001);
    float32x4_t v123 = vtrn1q_f32(v1003, v1003);
    float32x4_t v124 = vtrn2q_f32(v1003, v1003);
    float32x4_t v135 = vtrn1q_f32(v1005, v1005);
    float32x4_t v136 = vtrn2q_f32(v1005, v1005);
    float32x4_t v185 = vtrn1q_f32(v1007, v1007);
    float32x4_t v186 = vtrn2q_f32(v1007, v1007);
    float32x4_t v197 = vtrn1q_f32(v1009, v1009);
    float32x4_t v198 = vtrn2q_f32(v1009, v1009);
    float32x4_t v247 = vtrn1q_f32(v1011, v1011);
    float32x4_t v248 = vtrn2q_f32(v1011, v1011);
    float32x4_t v259 = vtrn1q_f32(v1013, v1013);
    float32x4_t v260 = vtrn2q_f32(v1013, v1013);
    float32x4_t v79 = vmulq_f32(v73, v78);
    float32x4_t v129 = vmulq_f32(v123, v128);
    float32x4_t v141 = vmulq_f32(v135, v140);
    float32x4_t v191 = vmulq_f32(v185, v190);
    float32x4_t v203 = vmulq_f32(v197, v202);
    float32x4_t v253 = vmulq_f32(v247, v252);
    float32x4_t v265 = vmulq_f32(v259, v264);
    float32x4_t v82 = vfmaq_f32(v79, v74, v80);
    float32x4_t v132 = vfmaq_f32(v129, v124, v130);
    float32x4_t v144 = vfmaq_f32(v141, v136, v142);
    float32x4_t v194 = vfmaq_f32(v191, v186, v192);
    float32x4_t v206 = vfmaq_f32(v203, v198, v204);
    float32x4_t v256 = vfmaq_f32(v253, v248, v254);
    float32x4_t v268 = vfmaq_f32(v265, v260, v266);
    float32x4_t v269 = vaddq_f32(v70, v82);
    float32x4_t v270 = vsubq_f32(v70, v82);
    float32x4_t v271 = vaddq_f32(v132, v144);
    float32x4_t v272 = vsubq_f32(v132, v144);
    float32x4_t v273 = vaddq_f32(v194, v206);
    float32x4_t v274 = vsubq_f32(v194, v206);
    float32x4_t v275 = vaddq_f32(v256, v268);
    float32x4_t v276 = vsubq_f32(v256, v268);
    float32x4_t v277 = vaddq_f32(v269, v271);
    float32x4_t v288 = vaddq_f32(v270, v272);
    float32x4_t v290 = vsubq_f32(v269, v271);
    float32x4_t v291 = vsubq_f32(v271, v275);
    float32x4_t v292 = vsubq_f32(v275, v269);
    float32x4_t v293 = vsubq_f32(v270, v272);
    float32x4_t v294 = vsubq_f32(v272, v276);
    float32x4_t v295 = vsubq_f32(v276, v270);
    float32x4_t v318 = vmulq_f32(v273, v317);
    float32x4_t v324 = vrev64q_f32(v274);
    float32x4_t v278 = vaddq_f32(v277, v275);
    float32x4_t v289 = vaddq_f32(v288, v276);
    float32x4_t v326 = vmulq_f32(v324, v325);
    float32x4_t v331 = vmulq_f32(v290, v330);
    float32x4_t v336 = vmulq_f32(v291, v335);
    float32x4_t v341 = vmulq_f32(v292, v340);
    float32x4_t v347 = vrev64q_f32(v293);
    float32x4_t v355 = vrev64q_f32(v294);
    float32x4_t v363 = vrev64q_f32(v295);
    float32x4_t v279 = vaddq_f32(v278, v273);
    float32x4_t v305 = vmulq_f32(v278, v304);
    float32x4_t v311 = vrev64q_f32(v289);
    float32x4_t v349 = vmulq_f32(v347, v348);
    float32x4_t v357 = vmulq_f32(v355, v356);
    float32x4_t v365 = vmulq_f32(v363, v364);
    float32x4_t v287 = vaddq_f32(v279, v1015);
    float32x4_t v313 = vmulq_f32(v311, v325);
    float32x4_t v366 = vaddq_f32(v305, v305);
    float32x4_t v379 = vaddq_f32(v326, v349);
    float32x4_t v381 = vsubq_f32(v326, v357);
    float32x4_t v383 = vsubq_f32(v326, v349);
    float32x4_t v367 = vaddq_f32(v366, v305);
    float32x4_t v371 = vaddq_f32(v287, v318);
    float32x4_t v380 = vaddq_f32(v379, v357);
    float32x4_t v382 = vaddq_f32(v381, v365);
    float32x4_t v384 = vsubq_f32(v383, v365);
    vst1q_f32((float32_t *)v923, v287);
    float32x4_t v368 = vaddq_f32(v287, v367);
    float32x4_t v372 = vaddq_f32(v371, v366);
    float32x4_t v369 = vaddq_f32(v368, v313);
    float32x4_t v370 = vsubq_f32(v368, v313);
    float32x4_t v373 = vaddq_f32(v372, v331);
    float32x4_t v375 = vsubq_f32(v372, v336);
    float32x4_t v377 = vsubq_f32(v372, v331);
    float32x4_t v374 = vaddq_f32(v373, v336);
    float32x4_t v376 = vaddq_f32(v375, v341);
    float32x4_t v378 = vsubq_f32(v377, v341);
    vst1q_f32((float32_t *)v950, v370);
    vst1q_f32((float32_t *)v977, v369);
    float32x4_t v385 = vaddq_f32(v374, v380);
    float32x4_t v386 = vsubq_f32(v374, v380);
    float32x4_t v387 = vaddq_f32(v376, v382);
    float32x4_t v388 = vsubq_f32(v376, v382);
    float32x4_t v389 = vaddq_f32(v378, v384);
    float32x4_t v390 = vsubq_f32(v378, v384);
    vst1q_f32((float32_t *)v932, v386);
    vst1q_f32((float32_t *)v941, v387);
    vst1q_f32((float32_t *)v959, v390);
    vst1q_f32((float32_t *)v968, v389);
    vst1q_f32((float32_t *)v986, v388);
    vst1q_f32((float32_t *)v995, v385);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v454 * 2; j < howmany; j += 1) {
    float32x2_t v466 = v5[istride];
    float v692 = -5.0000000000000000e-01F;
    float v703 = -1.4999999999999998e+00F;
    float v706 = 8.6602540378443871e-01F;
    float v707 = -8.6602540378443871e-01F;
    float v714 = 7.6604444311897801e-01F;
    float v718 = 9.3969262078590832e-01F;
    float v722 = -1.7364817766693039e-01F;
    float v725 = 6.4278760968653925e-01F;
    float v726 = -6.4278760968653925e-01F;
    float v732 = -3.4202014332566888e-01F;
    float v733 = 3.4202014332566888e-01F;
    float v739 = 9.8480775301220802e-01F;
    float v740 = -9.8480775301220802e-01F;
    float32x2_t v742 = (float32x2_t){v4, v4};
    float32x2_t v493 = v7[0];
    float32x2_t v494 = vtrn1_f32(v466, v466);
    float32x2_t v495 = vtrn2_f32(v466, v466);
    float32x2_t v498 = v7[1];
    float32x2_t v503 = v7[14];
    float32x2_t v508 = v7[15];
    float32x2_t v543 = v7[12];
    float32x2_t v548 = v7[13];
    float32x2_t v553 = v7[2];
    float32x2_t v558 = v7[3];
    float32x2_t v593 = v7[4];
    float32x2_t v598 = v7[5];
    float32x2_t v603 = v7[10];
    float32x2_t v608 = v7[11];
    float32x2_t v643 = v7[6];
    float32x2_t v648 = v7[7];
    float32x2_t v653 = v7[8];
    float32x2_t v658 = v7[9];
    float32x2_t v677 = v5[0];
    float32x2_t v693 = (float32x2_t){v692, v692};
    float32x2_t v704 = (float32x2_t){v703, v703};
    float32x2_t v708 = (float32x2_t){v706, v707};
    float32x2_t v715 = (float32x2_t){v714, v714};
    float32x2_t v719 = (float32x2_t){v718, v718};
    float32x2_t v723 = (float32x2_t){v722, v722};
    float32x2_t v727 = (float32x2_t){v725, v726};
    float32x2_t v734 = (float32x2_t){v732, v733};
    float32x2_t v741 = (float32x2_t){v739, v740};
    float32x2_t v481 = v5[istride * 8];
    float32x2_t v499 = vmul_f32(v494, v493);
    float32x2_t v516 = v5[istride * 7];
    float32x2_t v531 = v5[istride * 2];
    float32x2_t v566 = v5[istride * 3];
    float32x2_t v581 = v5[istride * 6];
    float32x2_t v616 = v5[istride * 4];
    float32x2_t v631 = v5[istride * 5];
    float32x2_t v710 = vmul_f32(v742, v708);
    float32x2_t v729 = vmul_f32(v742, v727);
    float32x2_t v736 = vmul_f32(v742, v734);
    float32x2_t v743 = vmul_f32(v742, v741);
    float32x2_t v501 = vfma_f32(v499, v495, v498);
    float32x2_t v504 = vtrn1_f32(v481, v481);
    float32x2_t v505 = vtrn2_f32(v481, v481);
    float32x2_t v544 = vtrn1_f32(v516, v516);
    float32x2_t v545 = vtrn2_f32(v516, v516);
    float32x2_t v554 = vtrn1_f32(v531, v531);
    float32x2_t v555 = vtrn2_f32(v531, v531);
    float32x2_t v594 = vtrn1_f32(v566, v566);
    float32x2_t v595 = vtrn2_f32(v566, v566);
    float32x2_t v604 = vtrn1_f32(v581, v581);
    float32x2_t v605 = vtrn2_f32(v581, v581);
    float32x2_t v644 = vtrn1_f32(v616, v616);
    float32x2_t v645 = vtrn2_f32(v616, v616);
    float32x2_t v654 = vtrn1_f32(v631, v631);
    float32x2_t v655 = vtrn2_f32(v631, v631);
    float32x2_t v509 = vmul_f32(v504, v503);
    float32x2_t v549 = vmul_f32(v544, v543);
    float32x2_t v559 = vmul_f32(v554, v553);
    float32x2_t v599 = vmul_f32(v594, v593);
    float32x2_t v609 = vmul_f32(v604, v603);
    float32x2_t v649 = vmul_f32(v644, v643);
    float32x2_t v659 = vmul_f32(v654, v653);
    float32x2_t v511 = vfma_f32(v509, v505, v508);
    float32x2_t v551 = vfma_f32(v549, v545, v548);
    float32x2_t v561 = vfma_f32(v559, v555, v558);
    float32x2_t v601 = vfma_f32(v599, v595, v598);
    float32x2_t v611 = vfma_f32(v609, v605, v608);
    float32x2_t v651 = vfma_f32(v649, v645, v648);
    float32x2_t v661 = vfma_f32(v659, v655, v658);
    float32x2_t v662 = vadd_f32(v501, v511);
    float32x2_t v663 = vsub_f32(v501, v511);
    float32x2_t v664 = vadd_f32(v551, v561);
    float32x2_t v665 = vsub_f32(v551, v561);
    float32x2_t v666 = vadd_f32(v601, v611);
    float32x2_t v667 = vsub_f32(v601, v611);
    float32x2_t v668 = vadd_f32(v651, v661);
    float32x2_t v669 = vsub_f32(v651, v661);
    float32x2_t v670 = vadd_f32(v662, v664);
    float32x2_t v679 = vadd_f32(v663, v665);
    float32x2_t v681 = vsub_f32(v662, v664);
    float32x2_t v682 = vsub_f32(v664, v668);
    float32x2_t v683 = vsub_f32(v668, v662);
    float32x2_t v684 = vsub_f32(v663, v665);
    float32x2_t v685 = vsub_f32(v665, v669);
    float32x2_t v686 = vsub_f32(v669, v663);
    float32x2_t v705 = vmul_f32(v666, v704);
    float32x2_t v711 = vrev64_f32(v667);
    float32x2_t v671 = vadd_f32(v670, v668);
    float32x2_t v680 = vadd_f32(v679, v669);
    float32x2_t v712 = vmul_f32(v711, v710);
    float32x2_t v716 = vmul_f32(v681, v715);
    float32x2_t v720 = vmul_f32(v682, v719);
    float32x2_t v724 = vmul_f32(v683, v723);
    float32x2_t v730 = vrev64_f32(v684);
    float32x2_t v737 = vrev64_f32(v685);
    float32x2_t v744 = vrev64_f32(v686);
    float32x2_t v672 = vadd_f32(v671, v666);
    float32x2_t v694 = vmul_f32(v671, v693);
    float32x2_t v700 = vrev64_f32(v680);
    float32x2_t v731 = vmul_f32(v730, v729);
    float32x2_t v738 = vmul_f32(v737, v736);
    float32x2_t v745 = vmul_f32(v744, v743);
    float32x2_t v678 = vadd_f32(v672, v677);
    float32x2_t v701 = vmul_f32(v700, v710);
    float32x2_t v746 = vadd_f32(v694, v694);
    float32x2_t v759 = vadd_f32(v712, v731);
    float32x2_t v761 = vsub_f32(v712, v738);
    float32x2_t v763 = vsub_f32(v712, v731);
    float32x2_t v747 = vadd_f32(v746, v694);
    float32x2_t v751 = vadd_f32(v678, v705);
    float32x2_t v760 = vadd_f32(v759, v738);
    float32x2_t v762 = vadd_f32(v761, v745);
    float32x2_t v764 = vsub_f32(v763, v745);
    v6[0] = v678;
    float32x2_t v748 = vadd_f32(v678, v747);
    float32x2_t v752 = vadd_f32(v751, v746);
    float32x2_t v749 = vadd_f32(v748, v701);
    float32x2_t v750 = vsub_f32(v748, v701);
    float32x2_t v753 = vadd_f32(v752, v716);
    float32x2_t v755 = vsub_f32(v752, v720);
    float32x2_t v757 = vsub_f32(v752, v716);
    float32x2_t v754 = vadd_f32(v753, v720);
    float32x2_t v756 = vadd_f32(v755, v724);
    float32x2_t v758 = vsub_f32(v757, v724);
    v6[ostride * 3] = v750;
    v6[ostride * 6] = v749;
    float32x2_t v765 = vadd_f32(v754, v760);
    float32x2_t v766 = vsub_f32(v754, v760);
    float32x2_t v767 = vadd_f32(v756, v762);
    float32x2_t v768 = vsub_f32(v756, v762);
    float32x2_t v769 = vadd_f32(v758, v764);
    float32x2_t v770 = vsub_f32(v758, v764);
    v6[ostride] = v766;
    v6[ostride * 2] = v767;
    v6[ostride * 4] = v770;
    v6[ostride * 5] = v769;
    v6[ostride * 7] = v768;
    v6[ostride * 8] = v765;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu9(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v171 = -5.0000000000000000e-01F;
    float v183 = -1.4999999999999998e+00F;
    float v188 = -8.6602540378443871e-01F;
    float v195 = 7.6604444311897801e-01F;
    float v200 = 9.3969262078590832e-01F;
    float v205 = -1.7364817766693039e-01F;
    float v210 = -6.4278760968653925e-01F;
    float v217 = 3.4202014332566888e-01F;
    float v224 = -9.8480775301220802e-01F;
    const float32x2_t *v324 = &v5[v0];
    float32x2_t *v427 = &v6[v2];
    int64_t v30 = v0 * 8;
    int64_t v49 = v0 * 7;
    int64_t v60 = v0 * 2;
    int64_t v79 = v0 * 3;
    int64_t v90 = v0 * 6;
    int64_t v109 = v0 * 4;
    int64_t v120 = v0 * 5;
    float v191 = v4 * v188;
    float v213 = v4 * v210;
    float v220 = v4 * v217;
    float v227 = v4 * v224;
    int64_t v270 = v2 * 2;
    int64_t v277 = v2 * 3;
    int64_t v284 = v2 * 4;
    int64_t v291 = v2 * 5;
    int64_t v298 = v2 * 6;
    int64_t v305 = v2 * 7;
    int64_t v312 = v2 * 8;
    const float32x2_t *v397 = &v5[0];
    svfloat32_t v401 = svdup_n_f32(v171);
    svfloat32_t v403 = svdup_n_f32(v183);
    svfloat32_t v405 = svdup_n_f32(v195);
    svfloat32_t v406 = svdup_n_f32(v200);
    svfloat32_t v407 = svdup_n_f32(v205);
    float32x2_t *v418 = &v6[0];
    svfloat32_t v494 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v324)[0]));
    svfloat32_t v42 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v46 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v72 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v76 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v102 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v106 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v132 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v136 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    const float32x2_t *v333 = &v5[v30];
    const float32x2_t *v342 = &v5[v49];
    const float32x2_t *v351 = &v5[v60];
    const float32x2_t *v360 = &v5[v79];
    const float32x2_t *v369 = &v5[v90];
    const float32x2_t *v378 = &v5[v109];
    const float32x2_t *v387 = &v5[v120];
    svfloat32_t v404 = svdup_n_f32(v191);
    svfloat32_t v408 = svdup_n_f32(v213);
    svfloat32_t v409 = svdup_n_f32(v220);
    svfloat32_t v410 = svdup_n_f32(v227);
    float32x2_t *v436 = &v6[v270];
    float32x2_t *v445 = &v6[v277];
    float32x2_t *v454 = &v6[v284];
    float32x2_t *v463 = &v6[v291];
    float32x2_t *v472 = &v6[v298];
    float32x2_t *v481 = &v6[v305];
    float32x2_t *v490 = &v6[v312];
    svfloat32_t v510 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v397)[0]));
    svfloat32_t zero43 = svdup_n_f32(0);
    svfloat32_t v43 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero43, v494, v42, 0),
                     v494, v42, 90);
    svfloat32_t v496 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v333)[0]));
    svfloat32_t v498 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v342)[0]));
    svfloat32_t v500 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v351)[0]));
    svfloat32_t v502 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v360)[0]));
    svfloat32_t v504 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v369)[0]));
    svfloat32_t v506 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v378)[0]));
    svfloat32_t v508 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v387)[0]));
    svfloat32_t zero47 = svdup_n_f32(0);
    svfloat32_t v47 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero47, v496, v46, 0),
                     v496, v46, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v498, v72, 0),
                     v498, v72, 90);
    svfloat32_t zero77 = svdup_n_f32(0);
    svfloat32_t v77 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero77, v500, v76, 0),
                     v500, v76, 90);
    svfloat32_t zero103 = svdup_n_f32(0);
    svfloat32_t v103 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero103, v502, v102, 0),
                     v502, v102, 90);
    svfloat32_t zero107 = svdup_n_f32(0);
    svfloat32_t v107 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero107, v504, v106, 0),
                     v504, v106, 90);
    svfloat32_t zero133 = svdup_n_f32(0);
    svfloat32_t v133 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero133, v506, v132, 0),
                     v506, v132, 90);
    svfloat32_t zero137 = svdup_n_f32(0);
    svfloat32_t v137 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero137, v508, v136, 0),
                     v508, v136, 90);
    svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v139 = svsub_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v140 = svadd_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v141 = svsub_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v142 = svadd_f32_x(svptrue_b32(), v103, v107);
    svfloat32_t v143 = svsub_f32_x(svptrue_b32(), v103, v107);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v133, v137);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v133, v137);
    svfloat32_t v146 = svadd_f32_x(svptrue_b32(), v138, v140);
    svfloat32_t v157 = svadd_f32_x(svptrue_b32(), v139, v141);
    svfloat32_t v159 = svsub_f32_x(svptrue_b32(), v138, v140);
    svfloat32_t v160 = svsub_f32_x(svptrue_b32(), v140, v144);
    svfloat32_t v161 = svsub_f32_x(svptrue_b32(), v144, v138);
    svfloat32_t v162 = svsub_f32_x(svptrue_b32(), v139, v141);
    svfloat32_t v163 = svsub_f32_x(svptrue_b32(), v141, v145);
    svfloat32_t v164 = svsub_f32_x(svptrue_b32(), v145, v139);
    svfloat32_t zero193 = svdup_n_f32(0);
    svfloat32_t v193 = svcmla_f32_x(pred_full, zero193, v404, v143, 90);
    svfloat32_t v147 = svadd_f32_x(svptrue_b32(), v146, v144);
    svfloat32_t v158 = svadd_f32_x(svptrue_b32(), v157, v145);
    svfloat32_t zero215 = svdup_n_f32(0);
    svfloat32_t v215 = svcmla_f32_x(pred_full, zero215, v408, v162, 90);
    svfloat32_t zero222 = svdup_n_f32(0);
    svfloat32_t v222 = svcmla_f32_x(pred_full, zero222, v409, v163, 90);
    svfloat32_t zero229 = svdup_n_f32(0);
    svfloat32_t v229 = svcmla_f32_x(pred_full, zero229, v410, v164, 90);
    svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v147, v142);
    svfloat32_t v174 = svmul_f32_x(svptrue_b32(), v147, v401);
    svfloat32_t zero181 = svdup_n_f32(0);
    svfloat32_t v181 = svcmla_f32_x(pred_full, zero181, v404, v158, 90);
    svfloat32_t v243 = svadd_f32_x(svptrue_b32(), v193, v215);
    svfloat32_t v245 = svsub_f32_x(svptrue_b32(), v193, v222);
    svfloat32_t v247 = svsub_f32_x(svptrue_b32(), v193, v215);
    svfloat32_t v156 = svadd_f32_x(svptrue_b32(), v148, v510);
    svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v174, v174);
    svfloat32_t v244 = svadd_f32_x(svptrue_b32(), v243, v222);
    svfloat32_t v246 = svadd_f32_x(svptrue_b32(), v245, v229);
    svfloat32_t v248 = svsub_f32_x(svptrue_b32(), v247, v229);
    svfloat32_t v231 = svmla_f32_x(pred_full, v230, v147, v401);
    svfloat32_t v235 = svmla_f32_x(pred_full, v156, v142, v403);
    svst1_f64(pred_full, (double *)(v418), svreinterpret_f64_f32(v156));
    svfloat32_t v232 = svadd_f32_x(svptrue_b32(), v156, v231);
    svfloat32_t v236 = svadd_f32_x(svptrue_b32(), v235, v230);
    svfloat32_t v233 = svadd_f32_x(svptrue_b32(), v232, v181);
    svfloat32_t v234 = svsub_f32_x(svptrue_b32(), v232, v181);
    svfloat32_t v237 = svmla_f32_x(pred_full, v236, v159, v405);
    svfloat32_t v239 = svmls_f32_x(pred_full, v236, v160, v406);
    svfloat32_t v241 = svmls_f32_x(pred_full, v236, v159, v405);
    svfloat32_t v238 = svmla_f32_x(pred_full, v237, v160, v406);
    svfloat32_t v240 = svmla_f32_x(pred_full, v239, v161, v407);
    svfloat32_t v242 = svmls_f32_x(pred_full, v241, v161, v407);
    svst1_f64(pred_full, (double *)(v445), svreinterpret_f64_f32(v234));
    svst1_f64(pred_full, (double *)(v472), svreinterpret_f64_f32(v233));
    svfloat32_t v249 = svadd_f32_x(svptrue_b32(), v238, v244);
    svfloat32_t v250 = svsub_f32_x(svptrue_b32(), v238, v244);
    svfloat32_t v251 = svadd_f32_x(svptrue_b32(), v240, v246);
    svfloat32_t v252 = svsub_f32_x(svptrue_b32(), v240, v246);
    svfloat32_t v253 = svadd_f32_x(svptrue_b32(), v242, v248);
    svfloat32_t v254 = svsub_f32_x(svptrue_b32(), v242, v248);
    svst1_f64(pred_full, (double *)(v427), svreinterpret_f64_f32(v250));
    svst1_f64(pred_full, (double *)(v436), svreinterpret_f64_f32(v251));
    svst1_f64(pred_full, (double *)(v454), svreinterpret_f64_f32(v254));
    svst1_f64(pred_full, (double *)(v463), svreinterpret_f64_f32(v253));
    svst1_f64(pred_full, (double *)(v481), svreinterpret_f64_f32(v252));
    svst1_f64(pred_full, (double *)(v490), svreinterpret_f64_f32(v249));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu11(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v634 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v381 = 1.1000000000000001e+00F;
    float v385 = 3.3166247903554003e-01F;
    float v386 = -3.3166247903554003e-01F;
    float v394 = 5.1541501300188641e-01F;
    float v399 = 9.4125353283118118e-01F;
    float v404 = 1.4143537075597825e+00F;
    float v409 = 8.5949297361449750e-01F;
    float v414 = 4.2314838273285138e-02F;
    float v419 = 3.8639279888589606e-01F;
    float v424 = 5.1254589567200015e-01F;
    float v429 = 1.0702757469471715e+00F;
    float v434 = 5.5486073394528512e-01F;
    float v438 = 1.2412944743900585e+00F;
    float v439 = -1.2412944743900585e+00F;
    float v446 = 2.0897833842005756e-01F;
    float v447 = -2.0897833842005756e-01F;
    float v454 = 3.7415717312460811e-01F;
    float v455 = -3.7415717312460811e-01F;
    float v462 = 4.9929922194110327e-02F;
    float v463 = -4.9929922194110327e-02F;
    float v470 = 6.5815896284539266e-01F;
    float v471 = -6.5815896284539266e-01F;
    float v478 = 6.3306543373877577e-01F;
    float v479 = -6.3306543373877577e-01F;
    float v486 = 1.0822460581641109e+00F;
    float v487 = -1.0822460581641109e+00F;
    float v494 = 8.1720737907134022e-01F;
    float v495 = -8.1720737907134022e-01F;
    float v502 = 4.2408709531871824e-01F;
    float v503 = -4.2408709531871824e-01F;
    float32x2_t v505 = (float32x2_t){v4, v4};
    const float32x2_t *v1160 = &v5[istride];
    float32x2_t *v1369 = &v6[ostride];
    float32x2_t v382 = (float32x2_t){v381, v381};
    float32x2_t v387 = (float32x2_t){v385, v386};
    float32x2_t v395 = (float32x2_t){v394, v394};
    float32x2_t v400 = (float32x2_t){v399, v399};
    float32x2_t v405 = (float32x2_t){v404, v404};
    float32x2_t v410 = (float32x2_t){v409, v409};
    float32x2_t v415 = (float32x2_t){v414, v414};
    float32x2_t v420 = (float32x2_t){v419, v419};
    float32x2_t v425 = (float32x2_t){v424, v424};
    float32x2_t v430 = (float32x2_t){v429, v429};
    float32x2_t v435 = (float32x2_t){v434, v434};
    float32x2_t v440 = (float32x2_t){v438, v439};
    float32x2_t v448 = (float32x2_t){v446, v447};
    float32x2_t v456 = (float32x2_t){v454, v455};
    float32x2_t v464 = (float32x2_t){v462, v463};
    float32x2_t v472 = (float32x2_t){v470, v471};
    float32x2_t v480 = (float32x2_t){v478, v479};
    float32x2_t v488 = (float32x2_t){v486, v487};
    float32x2_t v496 = (float32x2_t){v494, v495};
    float32x2_t v504 = (float32x2_t){v502, v503};
    const float32x2_t *v1269 = &v5[0];
    float32x2_t *v1279 = &v6[0];
    float32x4_t v1373 = vld1q_f32((const float32_t *)v1160);
    float32x4_t v213 = vtrn1q_f32(v1373, v1373);
    float32x4_t v214 = vtrn2q_f32(v1373, v1373);
    float32x4_t v218 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v220 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v230 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v232 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v242 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v244 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v254 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v256 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v266 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v268 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v278 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v280 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v290 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v292 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v302 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v304 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v314 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v316 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v326 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v328 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v383 = vcombine_f32(v382, v382);
    float32x2_t v389 = vmul_f32(v505, v387);
    float32x4_t v396 = vcombine_f32(v395, v395);
    float32x4_t v401 = vcombine_f32(v400, v400);
    float32x4_t v406 = vcombine_f32(v405, v405);
    float32x4_t v411 = vcombine_f32(v410, v410);
    float32x4_t v416 = vcombine_f32(v415, v415);
    float32x4_t v421 = vcombine_f32(v420, v420);
    float32x4_t v426 = vcombine_f32(v425, v425);
    float32x4_t v431 = vcombine_f32(v430, v430);
    float32x4_t v436 = vcombine_f32(v435, v435);
    float32x2_t v442 = vmul_f32(v505, v440);
    float32x2_t v450 = vmul_f32(v505, v448);
    float32x2_t v458 = vmul_f32(v505, v456);
    float32x2_t v466 = vmul_f32(v505, v464);
    float32x2_t v474 = vmul_f32(v505, v472);
    float32x2_t v482 = vmul_f32(v505, v480);
    float32x2_t v490 = vmul_f32(v505, v488);
    float32x2_t v498 = vmul_f32(v505, v496);
    float32x2_t v506 = vmul_f32(v505, v504);
    const float32x2_t *v1169 = &v5[istride * 10];
    const float32x2_t *v1179 = &v5[istride * 2];
    const float32x2_t *v1189 = &v5[istride * 9];
    const float32x2_t *v1199 = &v5[istride * 3];
    const float32x2_t *v1209 = &v5[istride * 8];
    const float32x2_t *v1219 = &v5[istride * 4];
    const float32x2_t *v1229 = &v5[istride * 7];
    const float32x2_t *v1239 = &v5[istride * 5];
    const float32x2_t *v1249 = &v5[istride * 6];
    float32x2_t *v1288 = &v6[ostride * 10];
    float32x2_t *v1297 = &v6[ostride * 9];
    float32x2_t *v1306 = &v6[ostride * 8];
    float32x2_t *v1315 = &v6[ostride * 7];
    float32x2_t *v1324 = &v6[ostride * 6];
    float32x2_t *v1333 = &v6[ostride * 5];
    float32x2_t *v1342 = &v6[ostride * 4];
    float32x2_t *v1351 = &v6[ostride * 3];
    float32x2_t *v1360 = &v6[ostride * 2];
    float32x4_t v1393 = vld1q_f32((const float32_t *)v1269);
    float32x4_t v219 = vmulq_f32(v213, v218);
    float32x4_t v391 = vcombine_f32(v389, v389);
    float32x4_t v444 = vcombine_f32(v442, v442);
    float32x4_t v452 = vcombine_f32(v450, v450);
    float32x4_t v460 = vcombine_f32(v458, v458);
    float32x4_t v468 = vcombine_f32(v466, v466);
    float32x4_t v476 = vcombine_f32(v474, v474);
    float32x4_t v484 = vcombine_f32(v482, v482);
    float32x4_t v492 = vcombine_f32(v490, v490);
    float32x4_t v500 = vcombine_f32(v498, v498);
    float32x4_t v508 = vcombine_f32(v506, v506);
    float32x4_t v1375 = vld1q_f32((const float32_t *)v1169);
    float32x4_t v1377 = vld1q_f32((const float32_t *)v1179);
    float32x4_t v1379 = vld1q_f32((const float32_t *)v1189);
    float32x4_t v1381 = vld1q_f32((const float32_t *)v1199);
    float32x4_t v1383 = vld1q_f32((const float32_t *)v1209);
    float32x4_t v1385 = vld1q_f32((const float32_t *)v1219);
    float32x4_t v1387 = vld1q_f32((const float32_t *)v1229);
    float32x4_t v1389 = vld1q_f32((const float32_t *)v1239);
    float32x4_t v1391 = vld1q_f32((const float32_t *)v1249);
    float32x4_t v222 = vfmaq_f32(v219, v214, v220);
    float32x4_t v225 = vtrn1q_f32(v1375, v1375);
    float32x4_t v226 = vtrn2q_f32(v1375, v1375);
    float32x4_t v237 = vtrn1q_f32(v1377, v1377);
    float32x4_t v238 = vtrn2q_f32(v1377, v1377);
    float32x4_t v249 = vtrn1q_f32(v1379, v1379);
    float32x4_t v250 = vtrn2q_f32(v1379, v1379);
    float32x4_t v261 = vtrn1q_f32(v1381, v1381);
    float32x4_t v262 = vtrn2q_f32(v1381, v1381);
    float32x4_t v273 = vtrn1q_f32(v1383, v1383);
    float32x4_t v274 = vtrn2q_f32(v1383, v1383);
    float32x4_t v285 = vtrn1q_f32(v1385, v1385);
    float32x4_t v286 = vtrn2q_f32(v1385, v1385);
    float32x4_t v297 = vtrn1q_f32(v1387, v1387);
    float32x4_t v298 = vtrn2q_f32(v1387, v1387);
    float32x4_t v309 = vtrn1q_f32(v1389, v1389);
    float32x4_t v310 = vtrn2q_f32(v1389, v1389);
    float32x4_t v321 = vtrn1q_f32(v1391, v1391);
    float32x4_t v322 = vtrn2q_f32(v1391, v1391);
    float32x4_t v231 = vmulq_f32(v225, v230);
    float32x4_t v243 = vmulq_f32(v237, v242);
    float32x4_t v255 = vmulq_f32(v249, v254);
    float32x4_t v267 = vmulq_f32(v261, v266);
    float32x4_t v279 = vmulq_f32(v273, v278);
    float32x4_t v291 = vmulq_f32(v285, v290);
    float32x4_t v303 = vmulq_f32(v297, v302);
    float32x4_t v315 = vmulq_f32(v309, v314);
    float32x4_t v327 = vmulq_f32(v321, v326);
    float32x4_t v234 = vfmaq_f32(v231, v226, v232);
    float32x4_t v246 = vfmaq_f32(v243, v238, v244);
    float32x4_t v258 = vfmaq_f32(v255, v250, v256);
    float32x4_t v270 = vfmaq_f32(v267, v262, v268);
    float32x4_t v282 = vfmaq_f32(v279, v274, v280);
    float32x4_t v294 = vfmaq_f32(v291, v286, v292);
    float32x4_t v306 = vfmaq_f32(v303, v298, v304);
    float32x4_t v318 = vfmaq_f32(v315, v310, v316);
    float32x4_t v330 = vfmaq_f32(v327, v322, v328);
    float32x4_t v331 = vaddq_f32(v222, v234);
    float32x4_t v332 = vaddq_f32(v246, v258);
    float32x4_t v333 = vaddq_f32(v270, v282);
    float32x4_t v334 = vaddq_f32(v294, v306);
    float32x4_t v335 = vaddq_f32(v318, v330);
    float32x4_t v336 = vsubq_f32(v222, v234);
    float32x4_t v337 = vsubq_f32(v246, v258);
    float32x4_t v338 = vsubq_f32(v270, v282);
    float32x4_t v339 = vsubq_f32(v294, v306);
    float32x4_t v340 = vsubq_f32(v318, v330);
    float32x4_t v341 = vaddq_f32(v331, v332);
    float32x4_t v342 = vaddq_f32(v333, v335);
    float32x4_t v344 = vsubq_f32(v337, v338);
    float32x4_t v345 = vaddq_f32(v336, v340);
    float32x4_t v357 = vsubq_f32(v332, v334);
    float32x4_t v358 = vsubq_f32(v331, v334);
    float32x4_t v359 = vsubq_f32(v332, v331);
    float32x4_t v360 = vsubq_f32(v335, v334);
    float32x4_t v361 = vsubq_f32(v333, v334);
    float32x4_t v362 = vsubq_f32(v335, v333);
    float32x4_t v363 = vsubq_f32(v332, v335);
    float32x4_t v364 = vsubq_f32(v331, v333);
    float32x4_t v366 = vaddq_f32(v337, v339);
    float32x4_t v367 = vsubq_f32(v336, v339);
    float32x4_t v368 = vaddq_f32(v336, v337);
    float32x4_t v369 = vsubq_f32(v339, v340);
    float32x4_t v370 = vsubq_f32(v338, v339);
    float32x4_t v371 = vsubq_f32(v338, v340);
    float32x4_t v372 = vaddq_f32(v337, v340);
    float32x4_t v373 = vsubq_f32(v336, v338);
    float32x4_t v343 = vaddq_f32(v334, v341);
    float32x4_t v355 = vsubq_f32(v344, v345);
    float32x4_t v365 = vsubq_f32(v342, v341);
    float32x4_t v374 = vaddq_f32(v344, v345);
    float32x4_t v397 = vmulq_f32(v357, v396);
    float32x4_t v402 = vmulq_f32(v358, v401);
    float32x4_t v407 = vmulq_f32(v359, v406);
    float32x4_t v412 = vmulq_f32(v360, v411);
    float32x4_t v417 = vmulq_f32(v361, v416);
    float32x4_t v422 = vmulq_f32(v362, v421);
    float32x4_t v427 = vmulq_f32(v363, v426);
    float32x4_t v432 = vmulq_f32(v364, v431);
    float32x4_t v443 = vrev64q_f32(v366);
    float32x4_t v451 = vrev64q_f32(v367);
    float32x4_t v459 = vrev64q_f32(v368);
    float32x4_t v467 = vrev64q_f32(v369);
    float32x4_t v475 = vrev64q_f32(v370);
    float32x4_t v483 = vrev64q_f32(v371);
    float32x4_t v491 = vrev64q_f32(v372);
    float32x4_t v499 = vrev64q_f32(v373);
    float32x4_t v346 = vaddq_f32(v343, v342);
    float32x4_t v356 = vsubq_f32(v355, v339);
    float32x4_t v437 = vmulq_f32(v365, v436);
    float32x4_t v445 = vmulq_f32(v443, v444);
    float32x4_t v453 = vmulq_f32(v451, v452);
    float32x4_t v461 = vmulq_f32(v459, v460);
    float32x4_t v469 = vmulq_f32(v467, v468);
    float32x4_t v477 = vmulq_f32(v475, v476);
    float32x4_t v485 = vmulq_f32(v483, v484);
    float32x4_t v493 = vmulq_f32(v491, v492);
    float32x4_t v501 = vmulq_f32(v499, v500);
    float32x4_t v507 = vrev64q_f32(v374);
    float32x4_t v511 = vaddq_f32(v397, v402);
    float32x4_t v512 = vaddq_f32(v402, v407);
    float32x4_t v513 = vsubq_f32(v397, v407);
    float32x4_t v514 = vaddq_f32(v412, v417);
    float32x4_t v515 = vaddq_f32(v417, v422);
    float32x4_t v516 = vsubq_f32(v412, v422);
    float32x4_t v354 = vaddq_f32(v1393, v346);
    float32x4_t v384 = vmulq_f32(v346, v383);
    float32x4_t v390 = vrev64q_f32(v356);
    float32x4_t v509 = vmulq_f32(v507, v508);
    float32x4_t v517 = vaddq_f32(v432, v437);
    float32x4_t v518 = vaddq_f32(v427, v437);
    float32x4_t v519 = vaddq_f32(v453, v461);
    float32x4_t v520 = vsubq_f32(v445, v461);
    float32x4_t v521 = vaddq_f32(v477, v485);
    float32x4_t v522 = vsubq_f32(v469, v485);
    float32x4_t v392 = vmulq_f32(v390, v391);
    float32x4_t v510 = vsubq_f32(v354, v384);
    float32x4_t v523 = vaddq_f32(v501, v509);
    float32x4_t v524 = vsubq_f32(v493, v509);
    float32x4_t v525 = vaddq_f32(v515, v517);
    float32x4_t v543 = vaddq_f32(v519, v520);
    vst1q_f32((float32_t *)v1279, v354);
    float32x4_t v526 = vaddq_f32(v525, v510);
    float32x4_t v527 = vsubq_f32(v510, v512);
    float32x4_t v529 = vaddq_f32(v510, v516);
    float32x4_t v531 = vsubq_f32(v510, v513);
    float32x4_t v533 = vaddq_f32(v510, v511);
    float32x4_t v535 = vaddq_f32(v392, v521);
    float32x4_t v537 = vsubq_f32(v523, v519);
    float32x4_t v539 = vaddq_f32(v392, v524);
    float32x4_t v541 = vsubq_f32(v524, v520);
    float32x4_t v544 = vaddq_f32(v543, v521);
    float32x4_t v528 = vsubq_f32(v527, v517);
    float32x4_t v530 = vaddq_f32(v529, v518);
    float32x4_t v532 = vsubq_f32(v531, v518);
    float32x4_t v534 = vsubq_f32(v533, v514);
    float32x4_t v536 = vaddq_f32(v535, v523);
    float32x4_t v538 = vsubq_f32(v537, v392);
    float32x4_t v540 = vaddq_f32(v539, v522);
    float32x4_t v542 = vsubq_f32(v541, v392);
    float32x4_t v545 = vaddq_f32(v544, v522);
    float32x4_t v546 = vsubq_f32(v545, v392);
    float32x4_t v548 = vaddq_f32(v526, v536);
    float32x4_t v549 = vaddq_f32(v528, v538);
    float32x4_t v550 = vsubq_f32(v530, v540);
    float32x4_t v551 = vaddq_f32(v532, v542);
    float32x4_t v552 = vsubq_f32(v532, v542);
    float32x4_t v553 = vaddq_f32(v530, v540);
    float32x4_t v554 = vsubq_f32(v528, v538);
    float32x4_t v555 = vsubq_f32(v526, v536);
    float32x4_t v547 = vaddq_f32(v534, v546);
    float32x4_t v556 = vsubq_f32(v534, v546);
    vst1q_f32((float32_t *)v1297, v548);
    vst1q_f32((float32_t *)v1306, v549);
    vst1q_f32((float32_t *)v1315, v550);
    vst1q_f32((float32_t *)v1324, v551);
    vst1q_f32((float32_t *)v1333, v552);
    vst1q_f32((float32_t *)v1342, v553);
    vst1q_f32((float32_t *)v1351, v554);
    vst1q_f32((float32_t *)v1360, v555);
    vst1q_f32((float32_t *)v1288, v547);
    vst1q_f32((float32_t *)v1369, v556);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v634 * 2; j < howmany; j += 1) {
    float32x2_t v646 = v5[istride];
    float v939 = 1.1000000000000001e+00F;
    float v942 = 3.3166247903554003e-01F;
    float v943 = -3.3166247903554003e-01F;
    float v950 = 5.1541501300188641e-01F;
    float v954 = 9.4125353283118118e-01F;
    float v958 = 1.4143537075597825e+00F;
    float v962 = 8.5949297361449750e-01F;
    float v966 = 4.2314838273285138e-02F;
    float v970 = 3.8639279888589606e-01F;
    float v974 = 5.1254589567200015e-01F;
    float v978 = 1.0702757469471715e+00F;
    float v982 = 5.5486073394528512e-01F;
    float v985 = 1.2412944743900585e+00F;
    float v986 = -1.2412944743900585e+00F;
    float v992 = 2.0897833842005756e-01F;
    float v993 = -2.0897833842005756e-01F;
    float v999 = 3.7415717312460811e-01F;
    float v1000 = -3.7415717312460811e-01F;
    float v1006 = 4.9929922194110327e-02F;
    float v1007 = -4.9929922194110327e-02F;
    float v1013 = 6.5815896284539266e-01F;
    float v1014 = -6.5815896284539266e-01F;
    float v1020 = 6.3306543373877577e-01F;
    float v1021 = -6.3306543373877577e-01F;
    float v1027 = 1.0822460581641109e+00F;
    float v1028 = -1.0822460581641109e+00F;
    float v1034 = 8.1720737907134022e-01F;
    float v1035 = -8.1720737907134022e-01F;
    float v1041 = 4.2408709531871824e-01F;
    float v1042 = -4.2408709531871824e-01F;
    float32x2_t v1044 = (float32x2_t){v4, v4};
    float32x2_t v793 = v7[0];
    float32x2_t v794 = vtrn1_f32(v646, v646);
    float32x2_t v795 = vtrn2_f32(v646, v646);
    float32x2_t v798 = v7[1];
    float32x2_t v803 = v7[18];
    float32x2_t v808 = v7[19];
    float32x2_t v813 = v7[2];
    float32x2_t v818 = v7[3];
    float32x2_t v823 = v7[16];
    float32x2_t v828 = v7[17];
    float32x2_t v833 = v7[4];
    float32x2_t v838 = v7[5];
    float32x2_t v843 = v7[14];
    float32x2_t v848 = v7[15];
    float32x2_t v853 = v7[6];
    float32x2_t v858 = v7[7];
    float32x2_t v863 = v7[12];
    float32x2_t v868 = v7[13];
    float32x2_t v873 = v7[8];
    float32x2_t v878 = v7[9];
    float32x2_t v883 = v7[10];
    float32x2_t v888 = v7[11];
    float32x2_t v912 = v5[0];
    float32x2_t v940 = (float32x2_t){v939, v939};
    float32x2_t v944 = (float32x2_t){v942, v943};
    float32x2_t v951 = (float32x2_t){v950, v950};
    float32x2_t v955 = (float32x2_t){v954, v954};
    float32x2_t v959 = (float32x2_t){v958, v958};
    float32x2_t v963 = (float32x2_t){v962, v962};
    float32x2_t v967 = (float32x2_t){v966, v966};
    float32x2_t v971 = (float32x2_t){v970, v970};
    float32x2_t v975 = (float32x2_t){v974, v974};
    float32x2_t v979 = (float32x2_t){v978, v978};
    float32x2_t v983 = (float32x2_t){v982, v982};
    float32x2_t v987 = (float32x2_t){v985, v986};
    float32x2_t v994 = (float32x2_t){v992, v993};
    float32x2_t v1001 = (float32x2_t){v999, v1000};
    float32x2_t v1008 = (float32x2_t){v1006, v1007};
    float32x2_t v1015 = (float32x2_t){v1013, v1014};
    float32x2_t v1022 = (float32x2_t){v1020, v1021};
    float32x2_t v1029 = (float32x2_t){v1027, v1028};
    float32x2_t v1036 = (float32x2_t){v1034, v1035};
    float32x2_t v1043 = (float32x2_t){v1041, v1042};
    float32x2_t v661 = v5[istride * 10];
    float32x2_t v676 = v5[istride * 2];
    float32x2_t v691 = v5[istride * 9];
    float32x2_t v706 = v5[istride * 3];
    float32x2_t v721 = v5[istride * 8];
    float32x2_t v736 = v5[istride * 4];
    float32x2_t v751 = v5[istride * 7];
    float32x2_t v766 = v5[istride * 5];
    float32x2_t v781 = v5[istride * 6];
    float32x2_t v799 = vmul_f32(v794, v793);
    float32x2_t v946 = vmul_f32(v1044, v944);
    float32x2_t v989 = vmul_f32(v1044, v987);
    float32x2_t v996 = vmul_f32(v1044, v994);
    float32x2_t v1003 = vmul_f32(v1044, v1001);
    float32x2_t v1010 = vmul_f32(v1044, v1008);
    float32x2_t v1017 = vmul_f32(v1044, v1015);
    float32x2_t v1024 = vmul_f32(v1044, v1022);
    float32x2_t v1031 = vmul_f32(v1044, v1029);
    float32x2_t v1038 = vmul_f32(v1044, v1036);
    float32x2_t v1045 = vmul_f32(v1044, v1043);
    float32x2_t v801 = vfma_f32(v799, v795, v798);
    float32x2_t v804 = vtrn1_f32(v661, v661);
    float32x2_t v805 = vtrn2_f32(v661, v661);
    float32x2_t v814 = vtrn1_f32(v676, v676);
    float32x2_t v815 = vtrn2_f32(v676, v676);
    float32x2_t v824 = vtrn1_f32(v691, v691);
    float32x2_t v825 = vtrn2_f32(v691, v691);
    float32x2_t v834 = vtrn1_f32(v706, v706);
    float32x2_t v835 = vtrn2_f32(v706, v706);
    float32x2_t v844 = vtrn1_f32(v721, v721);
    float32x2_t v845 = vtrn2_f32(v721, v721);
    float32x2_t v854 = vtrn1_f32(v736, v736);
    float32x2_t v855 = vtrn2_f32(v736, v736);
    float32x2_t v864 = vtrn1_f32(v751, v751);
    float32x2_t v865 = vtrn2_f32(v751, v751);
    float32x2_t v874 = vtrn1_f32(v766, v766);
    float32x2_t v875 = vtrn2_f32(v766, v766);
    float32x2_t v884 = vtrn1_f32(v781, v781);
    float32x2_t v885 = vtrn2_f32(v781, v781);
    float32x2_t v809 = vmul_f32(v804, v803);
    float32x2_t v819 = vmul_f32(v814, v813);
    float32x2_t v829 = vmul_f32(v824, v823);
    float32x2_t v839 = vmul_f32(v834, v833);
    float32x2_t v849 = vmul_f32(v844, v843);
    float32x2_t v859 = vmul_f32(v854, v853);
    float32x2_t v869 = vmul_f32(v864, v863);
    float32x2_t v879 = vmul_f32(v874, v873);
    float32x2_t v889 = vmul_f32(v884, v883);
    float32x2_t v811 = vfma_f32(v809, v805, v808);
    float32x2_t v821 = vfma_f32(v819, v815, v818);
    float32x2_t v831 = vfma_f32(v829, v825, v828);
    float32x2_t v841 = vfma_f32(v839, v835, v838);
    float32x2_t v851 = vfma_f32(v849, v845, v848);
    float32x2_t v861 = vfma_f32(v859, v855, v858);
    float32x2_t v871 = vfma_f32(v869, v865, v868);
    float32x2_t v881 = vfma_f32(v879, v875, v878);
    float32x2_t v891 = vfma_f32(v889, v885, v888);
    float32x2_t v892 = vadd_f32(v801, v811);
    float32x2_t v893 = vadd_f32(v821, v831);
    float32x2_t v894 = vadd_f32(v841, v851);
    float32x2_t v895 = vadd_f32(v861, v871);
    float32x2_t v896 = vadd_f32(v881, v891);
    float32x2_t v897 = vsub_f32(v801, v811);
    float32x2_t v898 = vsub_f32(v821, v831);
    float32x2_t v899 = vsub_f32(v841, v851);
    float32x2_t v900 = vsub_f32(v861, v871);
    float32x2_t v901 = vsub_f32(v881, v891);
    float32x2_t v902 = vadd_f32(v892, v893);
    float32x2_t v903 = vadd_f32(v894, v896);
    float32x2_t v905 = vsub_f32(v898, v899);
    float32x2_t v906 = vadd_f32(v897, v901);
    float32x2_t v916 = vsub_f32(v893, v895);
    float32x2_t v917 = vsub_f32(v892, v895);
    float32x2_t v918 = vsub_f32(v893, v892);
    float32x2_t v919 = vsub_f32(v896, v895);
    float32x2_t v920 = vsub_f32(v894, v895);
    float32x2_t v921 = vsub_f32(v896, v894);
    float32x2_t v922 = vsub_f32(v893, v896);
    float32x2_t v923 = vsub_f32(v892, v894);
    float32x2_t v925 = vadd_f32(v898, v900);
    float32x2_t v926 = vsub_f32(v897, v900);
    float32x2_t v927 = vadd_f32(v897, v898);
    float32x2_t v928 = vsub_f32(v900, v901);
    float32x2_t v929 = vsub_f32(v899, v900);
    float32x2_t v930 = vsub_f32(v899, v901);
    float32x2_t v931 = vadd_f32(v898, v901);
    float32x2_t v932 = vsub_f32(v897, v899);
    float32x2_t v904 = vadd_f32(v895, v902);
    float32x2_t v914 = vsub_f32(v905, v906);
    float32x2_t v924 = vsub_f32(v903, v902);
    float32x2_t v933 = vadd_f32(v905, v906);
    float32x2_t v952 = vmul_f32(v916, v951);
    float32x2_t v956 = vmul_f32(v917, v955);
    float32x2_t v960 = vmul_f32(v918, v959);
    float32x2_t v964 = vmul_f32(v919, v963);
    float32x2_t v968 = vmul_f32(v920, v967);
    float32x2_t v972 = vmul_f32(v921, v971);
    float32x2_t v976 = vmul_f32(v922, v975);
    float32x2_t v980 = vmul_f32(v923, v979);
    float32x2_t v990 = vrev64_f32(v925);
    float32x2_t v997 = vrev64_f32(v926);
    float32x2_t v1004 = vrev64_f32(v927);
    float32x2_t v1011 = vrev64_f32(v928);
    float32x2_t v1018 = vrev64_f32(v929);
    float32x2_t v1025 = vrev64_f32(v930);
    float32x2_t v1032 = vrev64_f32(v931);
    float32x2_t v1039 = vrev64_f32(v932);
    float32x2_t v907 = vadd_f32(v904, v903);
    float32x2_t v915 = vsub_f32(v914, v900);
    float32x2_t v984 = vmul_f32(v924, v983);
    float32x2_t v991 = vmul_f32(v990, v989);
    float32x2_t v998 = vmul_f32(v997, v996);
    float32x2_t v1005 = vmul_f32(v1004, v1003);
    float32x2_t v1012 = vmul_f32(v1011, v1010);
    float32x2_t v1019 = vmul_f32(v1018, v1017);
    float32x2_t v1026 = vmul_f32(v1025, v1024);
    float32x2_t v1033 = vmul_f32(v1032, v1031);
    float32x2_t v1040 = vmul_f32(v1039, v1038);
    float32x2_t v1046 = vrev64_f32(v933);
    float32x2_t v1049 = vadd_f32(v952, v956);
    float32x2_t v1050 = vadd_f32(v956, v960);
    float32x2_t v1051 = vsub_f32(v952, v960);
    float32x2_t v1052 = vadd_f32(v964, v968);
    float32x2_t v1053 = vadd_f32(v968, v972);
    float32x2_t v1054 = vsub_f32(v964, v972);
    float32x2_t v913 = vadd_f32(v912, v907);
    float32x2_t v941 = vmul_f32(v907, v940);
    float32x2_t v947 = vrev64_f32(v915);
    float32x2_t v1047 = vmul_f32(v1046, v1045);
    float32x2_t v1055 = vadd_f32(v980, v984);
    float32x2_t v1056 = vadd_f32(v976, v984);
    float32x2_t v1057 = vadd_f32(v998, v1005);
    float32x2_t v1058 = vsub_f32(v991, v1005);
    float32x2_t v1059 = vadd_f32(v1019, v1026);
    float32x2_t v1060 = vsub_f32(v1012, v1026);
    float32x2_t v948 = vmul_f32(v947, v946);
    float32x2_t v1048 = vsub_f32(v913, v941);
    float32x2_t v1061 = vadd_f32(v1040, v1047);
    float32x2_t v1062 = vsub_f32(v1033, v1047);
    float32x2_t v1063 = vadd_f32(v1053, v1055);
    float32x2_t v1081 = vadd_f32(v1057, v1058);
    v6[0] = v913;
    float32x2_t v1064 = vadd_f32(v1063, v1048);
    float32x2_t v1065 = vsub_f32(v1048, v1050);
    float32x2_t v1067 = vadd_f32(v1048, v1054);
    float32x2_t v1069 = vsub_f32(v1048, v1051);
    float32x2_t v1071 = vadd_f32(v1048, v1049);
    float32x2_t v1073 = vadd_f32(v948, v1059);
    float32x2_t v1075 = vsub_f32(v1061, v1057);
    float32x2_t v1077 = vadd_f32(v948, v1062);
    float32x2_t v1079 = vsub_f32(v1062, v1058);
    float32x2_t v1082 = vadd_f32(v1081, v1059);
    float32x2_t v1066 = vsub_f32(v1065, v1055);
    float32x2_t v1068 = vadd_f32(v1067, v1056);
    float32x2_t v1070 = vsub_f32(v1069, v1056);
    float32x2_t v1072 = vsub_f32(v1071, v1052);
    float32x2_t v1074 = vadd_f32(v1073, v1061);
    float32x2_t v1076 = vsub_f32(v1075, v948);
    float32x2_t v1078 = vadd_f32(v1077, v1060);
    float32x2_t v1080 = vsub_f32(v1079, v948);
    float32x2_t v1083 = vadd_f32(v1082, v1060);
    float32x2_t v1084 = vsub_f32(v1083, v948);
    float32x2_t v1086 = vadd_f32(v1064, v1074);
    float32x2_t v1087 = vadd_f32(v1066, v1076);
    float32x2_t v1088 = vsub_f32(v1068, v1078);
    float32x2_t v1089 = vadd_f32(v1070, v1080);
    float32x2_t v1090 = vsub_f32(v1070, v1080);
    float32x2_t v1091 = vadd_f32(v1068, v1078);
    float32x2_t v1092 = vsub_f32(v1066, v1076);
    float32x2_t v1093 = vsub_f32(v1064, v1074);
    float32x2_t v1085 = vadd_f32(v1072, v1084);
    float32x2_t v1094 = vsub_f32(v1072, v1084);
    v6[ostride * 9] = v1086;
    v6[ostride * 8] = v1087;
    v6[ostride * 7] = v1088;
    v6[ostride * 6] = v1089;
    v6[ostride * 5] = v1090;
    v6[ostride * 4] = v1091;
    v6[ostride * 3] = v1092;
    v6[ostride * 2] = v1093;
    v6[ostride * 10] = v1085;
    v6[ostride] = v1094;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu11(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v218 = 1.1000000000000001e+00F;
    float v223 = -3.3166247903554003e-01F;
    float v230 = 5.1541501300188641e-01F;
    float v235 = 9.4125353283118118e-01F;
    float v240 = 1.4143537075597825e+00F;
    float v245 = 8.5949297361449750e-01F;
    float v250 = 4.2314838273285138e-02F;
    float v255 = 3.8639279888589606e-01F;
    float v260 = 5.1254589567200015e-01F;
    float v265 = 1.0702757469471715e+00F;
    float v270 = 5.5486073394528512e-01F;
    float v275 = -1.2412944743900585e+00F;
    float v282 = -2.0897833842005756e-01F;
    float v289 = -3.7415717312460811e-01F;
    float v296 = -4.9929922194110327e-02F;
    float v303 = -6.5815896284539266e-01F;
    float v310 = -6.3306543373877577e-01F;
    float v317 = -1.0822460581641109e+00F;
    float v324 = -8.1720737907134022e-01F;
    float v331 = -4.2408709531871824e-01F;
    const float32x2_t *v467 = &v5[v0];
    float32x2_t *v679 = &v6[v2];
    int64_t v30 = v0 * 10;
    int64_t v41 = v0 * 2;
    int64_t v52 = v0 * 9;
    int64_t v63 = v0 * 3;
    int64_t v74 = v0 * 8;
    int64_t v85 = v0 * 4;
    int64_t v96 = v0 * 7;
    int64_t v107 = v0 * 5;
    int64_t v118 = v0 * 6;
    float v226 = v4 * v223;
    float v278 = v4 * v275;
    float v285 = v4 * v282;
    float v292 = v4 * v289;
    float v299 = v4 * v296;
    float v306 = v4 * v303;
    float v313 = v4 * v310;
    float v320 = v4 * v317;
    float v327 = v4 * v324;
    float v334 = v4 * v331;
    int64_t v392 = v2 * 10;
    int64_t v399 = v2 * 9;
    int64_t v406 = v2 * 8;
    int64_t v413 = v2 * 7;
    int64_t v420 = v2 * 6;
    int64_t v427 = v2 * 5;
    int64_t v434 = v2 * 4;
    int64_t v441 = v2 * 3;
    int64_t v448 = v2 * 2;
    const float32x2_t *v558 = &v5[0];
    svfloat32_t v562 = svdup_n_f32(v218);
    svfloat32_t v564 = svdup_n_f32(v230);
    svfloat32_t v565 = svdup_n_f32(v235);
    svfloat32_t v566 = svdup_n_f32(v240);
    svfloat32_t v567 = svdup_n_f32(v245);
    svfloat32_t v568 = svdup_n_f32(v250);
    svfloat32_t v569 = svdup_n_f32(v255);
    svfloat32_t v570 = svdup_n_f32(v260);
    svfloat32_t v571 = svdup_n_f32(v265);
    svfloat32_t v572 = svdup_n_f32(v270);
    float32x2_t *v589 = &v6[0];
    svfloat32_t v683 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v467)[0]));
    svfloat32_t v130 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v134 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v138 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v142 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v146 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v150 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v154 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v158 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v162 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v166 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    const float32x2_t *v476 = &v5[v30];
    const float32x2_t *v485 = &v5[v41];
    const float32x2_t *v494 = &v5[v52];
    const float32x2_t *v503 = &v5[v63];
    const float32x2_t *v512 = &v5[v74];
    const float32x2_t *v521 = &v5[v85];
    const float32x2_t *v530 = &v5[v96];
    const float32x2_t *v539 = &v5[v107];
    const float32x2_t *v548 = &v5[v118];
    svfloat32_t v563 = svdup_n_f32(v226);
    svfloat32_t v573 = svdup_n_f32(v278);
    svfloat32_t v574 = svdup_n_f32(v285);
    svfloat32_t v575 = svdup_n_f32(v292);
    svfloat32_t v576 = svdup_n_f32(v299);
    svfloat32_t v577 = svdup_n_f32(v306);
    svfloat32_t v578 = svdup_n_f32(v313);
    svfloat32_t v579 = svdup_n_f32(v320);
    svfloat32_t v580 = svdup_n_f32(v327);
    svfloat32_t v581 = svdup_n_f32(v334);
    float32x2_t *v598 = &v6[v392];
    float32x2_t *v607 = &v6[v399];
    float32x2_t *v616 = &v6[v406];
    float32x2_t *v625 = &v6[v413];
    float32x2_t *v634 = &v6[v420];
    float32x2_t *v643 = &v6[v427];
    float32x2_t *v652 = &v6[v434];
    float32x2_t *v661 = &v6[v441];
    float32x2_t *v670 = &v6[v448];
    svfloat32_t v703 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v558)[0]));
    svfloat32_t zero131 = svdup_n_f32(0);
    svfloat32_t v131 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero131, v683, v130, 0),
                     v683, v130, 90);
    svfloat32_t v685 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v476)[0]));
    svfloat32_t v687 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v485)[0]));
    svfloat32_t v689 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v494)[0]));
    svfloat32_t v691 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v503)[0]));
    svfloat32_t v693 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v512)[0]));
    svfloat32_t v695 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v521)[0]));
    svfloat32_t v697 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v530)[0]));
    svfloat32_t v699 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v539)[0]));
    svfloat32_t v701 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v548)[0]));
    svfloat32_t zero135 = svdup_n_f32(0);
    svfloat32_t v135 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero135, v685, v134, 0),
                     v685, v134, 90);
    svfloat32_t zero139 = svdup_n_f32(0);
    svfloat32_t v139 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero139, v687, v138, 0),
                     v687, v138, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero143, v689, v142, 0),
                     v689, v142, 90);
    svfloat32_t zero147 = svdup_n_f32(0);
    svfloat32_t v147 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero147, v691, v146, 0),
                     v691, v146, 90);
    svfloat32_t zero151 = svdup_n_f32(0);
    svfloat32_t v151 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero151, v693, v150, 0),
                     v693, v150, 90);
    svfloat32_t zero155 = svdup_n_f32(0);
    svfloat32_t v155 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero155, v695, v154, 0),
                     v695, v154, 90);
    svfloat32_t zero159 = svdup_n_f32(0);
    svfloat32_t v159 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero159, v697, v158, 0),
                     v697, v158, 90);
    svfloat32_t zero163 = svdup_n_f32(0);
    svfloat32_t v163 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero163, v699, v162, 0),
                     v699, v162, 90);
    svfloat32_t zero167 = svdup_n_f32(0);
    svfloat32_t v167 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero167, v701, v166, 0),
                     v701, v166, 90);
    svfloat32_t v168 = svadd_f32_x(svptrue_b32(), v131, v135);
    svfloat32_t v169 = svadd_f32_x(svptrue_b32(), v139, v143);
    svfloat32_t v170 = svadd_f32_x(svptrue_b32(), v147, v151);
    svfloat32_t v171 = svadd_f32_x(svptrue_b32(), v155, v159);
    svfloat32_t v172 = svadd_f32_x(svptrue_b32(), v163, v167);
    svfloat32_t v173 = svsub_f32_x(svptrue_b32(), v131, v135);
    svfloat32_t v174 = svsub_f32_x(svptrue_b32(), v139, v143);
    svfloat32_t v175 = svsub_f32_x(svptrue_b32(), v147, v151);
    svfloat32_t v176 = svsub_f32_x(svptrue_b32(), v155, v159);
    svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v163, v167);
    svfloat32_t v178 = svadd_f32_x(svptrue_b32(), v168, v169);
    svfloat32_t v179 = svadd_f32_x(svptrue_b32(), v170, v172);
    svfloat32_t v181 = svsub_f32_x(svptrue_b32(), v174, v175);
    svfloat32_t v182 = svadd_f32_x(svptrue_b32(), v173, v177);
    svfloat32_t v194 = svsub_f32_x(svptrue_b32(), v169, v171);
    svfloat32_t v195 = svsub_f32_x(svptrue_b32(), v168, v171);
    svfloat32_t v196 = svsub_f32_x(svptrue_b32(), v169, v168);
    svfloat32_t v197 = svsub_f32_x(svptrue_b32(), v172, v171);
    svfloat32_t v198 = svsub_f32_x(svptrue_b32(), v170, v171);
    svfloat32_t v199 = svsub_f32_x(svptrue_b32(), v172, v170);
    svfloat32_t v200 = svsub_f32_x(svptrue_b32(), v169, v172);
    svfloat32_t v201 = svsub_f32_x(svptrue_b32(), v168, v170);
    svfloat32_t v203 = svadd_f32_x(svptrue_b32(), v174, v176);
    svfloat32_t v204 = svsub_f32_x(svptrue_b32(), v173, v176);
    svfloat32_t v205 = svadd_f32_x(svptrue_b32(), v173, v174);
    svfloat32_t v206 = svsub_f32_x(svptrue_b32(), v176, v177);
    svfloat32_t v207 = svsub_f32_x(svptrue_b32(), v175, v176);
    svfloat32_t v208 = svsub_f32_x(svptrue_b32(), v175, v177);
    svfloat32_t v209 = svadd_f32_x(svptrue_b32(), v174, v177);
    svfloat32_t v210 = svsub_f32_x(svptrue_b32(), v173, v175);
    svfloat32_t v180 = svadd_f32_x(svptrue_b32(), v171, v178);
    svfloat32_t v192 = svsub_f32_x(svptrue_b32(), v181, v182);
    svfloat32_t v202 = svsub_f32_x(svptrue_b32(), v179, v178);
    svfloat32_t v211 = svadd_f32_x(svptrue_b32(), v181, v182);
    svfloat32_t v238 = svmul_f32_x(svptrue_b32(), v195, v565);
    svfloat32_t v243 = svmul_f32_x(svptrue_b32(), v196, v566);
    svfloat32_t v253 = svmul_f32_x(svptrue_b32(), v198, v568);
    svfloat32_t v258 = svmul_f32_x(svptrue_b32(), v199, v569);
    svfloat32_t zero280 = svdup_n_f32(0);
    svfloat32_t v280 = svcmla_f32_x(pred_full, zero280, v573, v203, 90);
    svfloat32_t zero294 = svdup_n_f32(0);
    svfloat32_t v294 = svcmla_f32_x(pred_full, zero294, v575, v205, 90);
    svfloat32_t zero301 = svdup_n_f32(0);
    svfloat32_t v301 = svcmla_f32_x(pred_full, zero301, v576, v206, 90);
    svfloat32_t zero315 = svdup_n_f32(0);
    svfloat32_t v315 = svcmla_f32_x(pred_full, zero315, v578, v208, 90);
    svfloat32_t zero322 = svdup_n_f32(0);
    svfloat32_t v322 = svcmla_f32_x(pred_full, zero322, v579, v209, 90);
    svfloat32_t v183 = svadd_f32_x(svptrue_b32(), v180, v179);
    svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v192, v176);
    svfloat32_t v273 = svmul_f32_x(svptrue_b32(), v202, v572);
    svfloat32_t zero336 = svdup_n_f32(0);
    svfloat32_t v336 = svcmla_f32_x(pred_full, zero336, v581, v211, 90);
    svfloat32_t v338 = svmla_f32_x(pred_full, v238, v194, v564);
    svfloat32_t v339 = svmla_f32_x(pred_full, v243, v195, v565);
    svfloat32_t v340 = svnmls_f32_x(pred_full, v243, v194, v564);
    svfloat32_t v341 = svmla_f32_x(pred_full, v253, v197, v567);
    svfloat32_t v342 = svmla_f32_x(pred_full, v258, v198, v568);
    svfloat32_t v343 = svnmls_f32_x(pred_full, v258, v197, v567);
    svfloat32_t v346 = svcmla_f32_x(pred_full, v294, v574, v204, 90);
    svfloat32_t v347 = svsub_f32_x(svptrue_b32(), v280, v294);
    svfloat32_t v348 = svcmla_f32_x(pred_full, v315, v577, v207, 90);
    svfloat32_t v349 = svsub_f32_x(svptrue_b32(), v301, v315);
    svfloat32_t v191 = svadd_f32_x(svptrue_b32(), v703, v183);
    svfloat32_t zero228 = svdup_n_f32(0);
    svfloat32_t v228 = svcmla_f32_x(pred_full, zero228, v563, v193, 90);
    svfloat32_t v344 = svmla_f32_x(pred_full, v273, v201, v571);
    svfloat32_t v345 = svmla_f32_x(pred_full, v273, v200, v570);
    svfloat32_t v350 = svcmla_f32_x(pred_full, v336, v580, v210, 90);
    svfloat32_t v351 = svsub_f32_x(svptrue_b32(), v322, v336);
    svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v346, v347);
    svfloat32_t v337 = svmls_f32_x(pred_full, v191, v183, v562);
    svfloat32_t v352 = svadd_f32_x(svptrue_b32(), v342, v344);
    svfloat32_t v362 = svadd_f32_x(svptrue_b32(), v228, v348);
    svfloat32_t v364 = svsub_f32_x(svptrue_b32(), v350, v346);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v228, v351);
    svfloat32_t v368 = svsub_f32_x(svptrue_b32(), v351, v347);
    svfloat32_t v371 = svadd_f32_x(svptrue_b32(), v370, v348);
    svst1_f64(pred_full, (double *)(v589), svreinterpret_f64_f32(v191));
    svfloat32_t v353 = svadd_f32_x(svptrue_b32(), v352, v337);
    svfloat32_t v354 = svsub_f32_x(svptrue_b32(), v337, v339);
    svfloat32_t v356 = svadd_f32_x(svptrue_b32(), v337, v343);
    svfloat32_t v358 = svsub_f32_x(svptrue_b32(), v337, v340);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v337, v338);
    svfloat32_t v363 = svadd_f32_x(svptrue_b32(), v362, v350);
    svfloat32_t v365 = svsub_f32_x(svptrue_b32(), v364, v228);
    svfloat32_t v367 = svadd_f32_x(svptrue_b32(), v366, v349);
    svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v368, v228);
    svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v371, v349);
    svfloat32_t v355 = svsub_f32_x(svptrue_b32(), v354, v344);
    svfloat32_t v357 = svadd_f32_x(svptrue_b32(), v356, v345);
    svfloat32_t v359 = svsub_f32_x(svptrue_b32(), v358, v345);
    svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v360, v341);
    svfloat32_t v373 = svsub_f32_x(svptrue_b32(), v372, v228);
    svfloat32_t v375 = svadd_f32_x(svptrue_b32(), v353, v363);
    svfloat32_t v382 = svsub_f32_x(svptrue_b32(), v353, v363);
    svfloat32_t v374 = svadd_f32_x(svptrue_b32(), v361, v373);
    svfloat32_t v376 = svadd_f32_x(svptrue_b32(), v355, v365);
    svfloat32_t v377 = svsub_f32_x(svptrue_b32(), v357, v367);
    svfloat32_t v378 = svadd_f32_x(svptrue_b32(), v359, v369);
    svfloat32_t v379 = svsub_f32_x(svptrue_b32(), v359, v369);
    svfloat32_t v380 = svadd_f32_x(svptrue_b32(), v357, v367);
    svfloat32_t v381 = svsub_f32_x(svptrue_b32(), v355, v365);
    svfloat32_t v383 = svsub_f32_x(svptrue_b32(), v361, v373);
    svst1_f64(pred_full, (double *)(v607), svreinterpret_f64_f32(v375));
    svst1_f64(pred_full, (double *)(v670), svreinterpret_f64_f32(v382));
    svst1_f64(pred_full, (double *)(v598), svreinterpret_f64_f32(v374));
    svst1_f64(pred_full, (double *)(v616), svreinterpret_f64_f32(v376));
    svst1_f64(pred_full, (double *)(v625), svreinterpret_f64_f32(v377));
    svst1_f64(pred_full, (double *)(v634), svreinterpret_f64_f32(v378));
    svst1_f64(pred_full, (double *)(v643), svreinterpret_f64_f32(v379));
    svst1_f64(pred_full, (double *)(v652), svreinterpret_f64_f32(v380));
    svst1_f64(pred_full, (double *)(v661), svreinterpret_f64_f32(v381));
    svst1_f64(pred_full, (double *)(v679), svreinterpret_f64_f32(v383));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu13(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v726 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v449 = 1.0833333333333333e+00F;
    float v454 = -3.0046260628866578e-01F;
    float v458 = 7.4927933062613905e-01F;
    float v459 = -7.4927933062613905e-01F;
    float v466 = 4.0100212832186721e-01F;
    float v467 = -4.0100212832186721e-01F;
    float v474 = 5.7514072947400308e-01F;
    float v475 = -5.7514072947400308e-01F;
    float v483 = 5.2422663952658211e-01F;
    float v488 = 5.1652078062348972e-01F;
    float v493 = 7.7058589030924258e-03F;
    float v498 = 4.2763404682656941e-01F;
    float v503 = 1.5180597207438440e-01F;
    float v508 = 5.7944001890096386e-01F;
    float v512 = 1.1543953381323635e+00F;
    float v513 = -1.1543953381323635e+00F;
    float v520 = 9.0655220171271012e-01F;
    float v521 = -9.0655220171271012e-01F;
    float v528 = 8.1857027294591811e-01F;
    float v529 = -8.1857027294591811e-01F;
    float v536 = 1.1971367726043427e+00F;
    float v537 = -1.1971367726043427e+00F;
    float v544 = 8.6131170741789742e-01F;
    float v545 = -8.6131170741789742e-01F;
    float v552 = 1.1091548438375507e+00F;
    float v553 = -1.1091548438375507e+00F;
    float v560 = 4.2741434471979367e-02F;
    float v561 = -4.2741434471979367e-02F;
    float v568 = -4.5240494294812715e-02F;
    float v569 = 4.5240494294812715e-02F;
    float v576 = 2.9058457089163264e-01F;
    float v577 = -2.9058457089163264e-01F;
    float32x2_t v579 = (float32x2_t){v4, v4};
    const float32x2_t *v1328 = &v5[istride];
    float32x2_t *v1577 = &v6[ostride];
    float32x2_t v450 = (float32x2_t){v449, v449};
    float32x2_t v455 = (float32x2_t){v454, v454};
    float32x2_t v460 = (float32x2_t){v458, v459};
    float32x2_t v468 = (float32x2_t){v466, v467};
    float32x2_t v476 = (float32x2_t){v474, v475};
    float32x2_t v484 = (float32x2_t){v483, v483};
    float32x2_t v489 = (float32x2_t){v488, v488};
    float32x2_t v494 = (float32x2_t){v493, v493};
    float32x2_t v499 = (float32x2_t){v498, v498};
    float32x2_t v504 = (float32x2_t){v503, v503};
    float32x2_t v509 = (float32x2_t){v508, v508};
    float32x2_t v514 = (float32x2_t){v512, v513};
    float32x2_t v522 = (float32x2_t){v520, v521};
    float32x2_t v530 = (float32x2_t){v528, v529};
    float32x2_t v538 = (float32x2_t){v536, v537};
    float32x2_t v546 = (float32x2_t){v544, v545};
    float32x2_t v554 = (float32x2_t){v552, v553};
    float32x2_t v562 = (float32x2_t){v560, v561};
    float32x2_t v570 = (float32x2_t){v568, v569};
    float32x2_t v578 = (float32x2_t){v576, v577};
    const float32x2_t *v1459 = &v5[0];
    float32x2_t *v1469 = &v6[0];
    float32x4_t v1581 = vld1q_f32((const float32_t *)v1328);
    float32x4_t v251 = vtrn1q_f32(v1581, v1581);
    float32x4_t v252 = vtrn2q_f32(v1581, v1581);
    float32x4_t v256 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v258 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v268 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v270 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v280 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v282 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v292 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v294 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v304 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v306 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v316 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v318 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v328 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v330 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v340 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v342 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v352 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v354 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v364 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v366 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v376 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v378 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v388 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v390 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v451 = vcombine_f32(v450, v450);
    float32x4_t v456 = vcombine_f32(v455, v455);
    float32x2_t v462 = vmul_f32(v579, v460);
    float32x2_t v470 = vmul_f32(v579, v468);
    float32x2_t v478 = vmul_f32(v579, v476);
    float32x4_t v485 = vcombine_f32(v484, v484);
    float32x4_t v490 = vcombine_f32(v489, v489);
    float32x4_t v495 = vcombine_f32(v494, v494);
    float32x4_t v500 = vcombine_f32(v499, v499);
    float32x4_t v505 = vcombine_f32(v504, v504);
    float32x4_t v510 = vcombine_f32(v509, v509);
    float32x2_t v516 = vmul_f32(v579, v514);
    float32x2_t v524 = vmul_f32(v579, v522);
    float32x2_t v532 = vmul_f32(v579, v530);
    float32x2_t v540 = vmul_f32(v579, v538);
    float32x2_t v548 = vmul_f32(v579, v546);
    float32x2_t v556 = vmul_f32(v579, v554);
    float32x2_t v564 = vmul_f32(v579, v562);
    float32x2_t v572 = vmul_f32(v579, v570);
    float32x2_t v580 = vmul_f32(v579, v578);
    const float32x2_t *v1337 = &v5[istride * 12];
    const float32x2_t *v1347 = &v5[istride * 2];
    const float32x2_t *v1357 = &v5[istride * 11];
    const float32x2_t *v1367 = &v5[istride * 3];
    const float32x2_t *v1377 = &v5[istride * 10];
    const float32x2_t *v1387 = &v5[istride * 4];
    const float32x2_t *v1397 = &v5[istride * 9];
    const float32x2_t *v1407 = &v5[istride * 5];
    const float32x2_t *v1417 = &v5[istride * 8];
    const float32x2_t *v1427 = &v5[istride * 6];
    const float32x2_t *v1437 = &v5[istride * 7];
    float32x2_t *v1478 = &v6[ostride * 12];
    float32x2_t *v1487 = &v6[ostride * 11];
    float32x2_t *v1496 = &v6[ostride * 10];
    float32x2_t *v1505 = &v6[ostride * 9];
    float32x2_t *v1514 = &v6[ostride * 8];
    float32x2_t *v1523 = &v6[ostride * 7];
    float32x2_t *v1532 = &v6[ostride * 6];
    float32x2_t *v1541 = &v6[ostride * 5];
    float32x2_t *v1550 = &v6[ostride * 4];
    float32x2_t *v1559 = &v6[ostride * 3];
    float32x2_t *v1568 = &v6[ostride * 2];
    float32x4_t v1605 = vld1q_f32((const float32_t *)v1459);
    float32x4_t v257 = vmulq_f32(v251, v256);
    float32x4_t v464 = vcombine_f32(v462, v462);
    float32x4_t v472 = vcombine_f32(v470, v470);
    float32x4_t v480 = vcombine_f32(v478, v478);
    float32x4_t v518 = vcombine_f32(v516, v516);
    float32x4_t v526 = vcombine_f32(v524, v524);
    float32x4_t v534 = vcombine_f32(v532, v532);
    float32x4_t v542 = vcombine_f32(v540, v540);
    float32x4_t v550 = vcombine_f32(v548, v548);
    float32x4_t v558 = vcombine_f32(v556, v556);
    float32x4_t v566 = vcombine_f32(v564, v564);
    float32x4_t v574 = vcombine_f32(v572, v572);
    float32x4_t v582 = vcombine_f32(v580, v580);
    float32x4_t v1583 = vld1q_f32((const float32_t *)v1337);
    float32x4_t v1585 = vld1q_f32((const float32_t *)v1347);
    float32x4_t v1587 = vld1q_f32((const float32_t *)v1357);
    float32x4_t v1589 = vld1q_f32((const float32_t *)v1367);
    float32x4_t v1591 = vld1q_f32((const float32_t *)v1377);
    float32x4_t v1593 = vld1q_f32((const float32_t *)v1387);
    float32x4_t v1595 = vld1q_f32((const float32_t *)v1397);
    float32x4_t v1597 = vld1q_f32((const float32_t *)v1407);
    float32x4_t v1599 = vld1q_f32((const float32_t *)v1417);
    float32x4_t v1601 = vld1q_f32((const float32_t *)v1427);
    float32x4_t v1603 = vld1q_f32((const float32_t *)v1437);
    float32x4_t v260 = vfmaq_f32(v257, v252, v258);
    float32x4_t v263 = vtrn1q_f32(v1583, v1583);
    float32x4_t v264 = vtrn2q_f32(v1583, v1583);
    float32x4_t v275 = vtrn1q_f32(v1585, v1585);
    float32x4_t v276 = vtrn2q_f32(v1585, v1585);
    float32x4_t v287 = vtrn1q_f32(v1587, v1587);
    float32x4_t v288 = vtrn2q_f32(v1587, v1587);
    float32x4_t v299 = vtrn1q_f32(v1589, v1589);
    float32x4_t v300 = vtrn2q_f32(v1589, v1589);
    float32x4_t v311 = vtrn1q_f32(v1591, v1591);
    float32x4_t v312 = vtrn2q_f32(v1591, v1591);
    float32x4_t v323 = vtrn1q_f32(v1593, v1593);
    float32x4_t v324 = vtrn2q_f32(v1593, v1593);
    float32x4_t v335 = vtrn1q_f32(v1595, v1595);
    float32x4_t v336 = vtrn2q_f32(v1595, v1595);
    float32x4_t v347 = vtrn1q_f32(v1597, v1597);
    float32x4_t v348 = vtrn2q_f32(v1597, v1597);
    float32x4_t v359 = vtrn1q_f32(v1599, v1599);
    float32x4_t v360 = vtrn2q_f32(v1599, v1599);
    float32x4_t v371 = vtrn1q_f32(v1601, v1601);
    float32x4_t v372 = vtrn2q_f32(v1601, v1601);
    float32x4_t v383 = vtrn1q_f32(v1603, v1603);
    float32x4_t v384 = vtrn2q_f32(v1603, v1603);
    float32x4_t v269 = vmulq_f32(v263, v268);
    float32x4_t v281 = vmulq_f32(v275, v280);
    float32x4_t v293 = vmulq_f32(v287, v292);
    float32x4_t v305 = vmulq_f32(v299, v304);
    float32x4_t v317 = vmulq_f32(v311, v316);
    float32x4_t v329 = vmulq_f32(v323, v328);
    float32x4_t v341 = vmulq_f32(v335, v340);
    float32x4_t v353 = vmulq_f32(v347, v352);
    float32x4_t v365 = vmulq_f32(v359, v364);
    float32x4_t v377 = vmulq_f32(v371, v376);
    float32x4_t v389 = vmulq_f32(v383, v388);
    float32x4_t v272 = vfmaq_f32(v269, v264, v270);
    float32x4_t v284 = vfmaq_f32(v281, v276, v282);
    float32x4_t v296 = vfmaq_f32(v293, v288, v294);
    float32x4_t v308 = vfmaq_f32(v305, v300, v306);
    float32x4_t v320 = vfmaq_f32(v317, v312, v318);
    float32x4_t v332 = vfmaq_f32(v329, v324, v330);
    float32x4_t v344 = vfmaq_f32(v341, v336, v342);
    float32x4_t v356 = vfmaq_f32(v353, v348, v354);
    float32x4_t v368 = vfmaq_f32(v365, v360, v366);
    float32x4_t v380 = vfmaq_f32(v377, v372, v378);
    float32x4_t v392 = vfmaq_f32(v389, v384, v390);
    float32x4_t v393 = vaddq_f32(v260, v272);
    float32x4_t v394 = vaddq_f32(v284, v296);
    float32x4_t v395 = vaddq_f32(v308, v320);
    float32x4_t v396 = vaddq_f32(v332, v344);
    float32x4_t v397 = vaddq_f32(v356, v368);
    float32x4_t v398 = vaddq_f32(v380, v392);
    float32x4_t v399 = vsubq_f32(v260, v272);
    float32x4_t v400 = vsubq_f32(v284, v296);
    float32x4_t v401 = vsubq_f32(v308, v320);
    float32x4_t v402 = vsubq_f32(v332, v344);
    float32x4_t v403 = vsubq_f32(v356, v368);
    float32x4_t v404 = vsubq_f32(v380, v392);
    float32x4_t v405 = vaddq_f32(v394, v397);
    float32x4_t v407 = vaddq_f32(v393, v395);
    float32x4_t v410 = vaddq_f32(v400, v403);
    float32x4_t v412 = vaddq_f32(v399, v401);
    float32x4_t v414 = vsubq_f32(v394, v398);
    float32x4_t v415 = vsubq_f32(v395, v396);
    float32x4_t v416 = vsubq_f32(v393, v396);
    float32x4_t v417 = vsubq_f32(v397, v398);
    float32x4_t v422 = vsubq_f32(v400, v404);
    float32x4_t v423 = vsubq_f32(v399, v401);
    float32x4_t v424 = vsubq_f32(v400, v403);
    float32x4_t v425 = vaddq_f32(v399, v402);
    float32x4_t v426 = vsubq_f32(v403, v404);
    float32x4_t v427 = vaddq_f32(v401, v402);
    float32x4_t v406 = vaddq_f32(v405, v398);
    float32x4_t v408 = vaddq_f32(v407, v396);
    float32x4_t v411 = vaddq_f32(v410, v404);
    float32x4_t v413 = vsubq_f32(v412, v402);
    float32x4_t v418 = vsubq_f32(v414, v415);
    float32x4_t v419 = vsubq_f32(v416, v417);
    float32x4_t v420 = vaddq_f32(v414, v415);
    float32x4_t v421 = vaddq_f32(v416, v417);
    float32x4_t v440 = vaddq_f32(v422, v423);
    float32x4_t v441 = vaddq_f32(v424, v425);
    float32x4_t v442 = vsubq_f32(v426, v427);
    float32x4_t v517 = vrev64q_f32(v422);
    float32x4_t v525 = vrev64q_f32(v423);
    float32x4_t v541 = vrev64q_f32(v424);
    float32x4_t v549 = vrev64q_f32(v425);
    float32x4_t v565 = vrev64q_f32(v426);
    float32x4_t v573 = vrev64q_f32(v427);
    float32x4_t v409 = vaddq_f32(v406, v408);
    float32x4_t v436 = vsubq_f32(v408, v406);
    float32x4_t v437 = vaddq_f32(v411, v413);
    float32x4_t v438 = vaddq_f32(v418, v419);
    float32x4_t v439 = vsubq_f32(v420, v421);
    float32x4_t v463 = vrev64q_f32(v411);
    float32x4_t v471 = vrev64q_f32(v413);
    float32x4_t v486 = vmulq_f32(v418, v485);
    float32x4_t v491 = vmulq_f32(v419, v490);
    float32x4_t v501 = vmulq_f32(v420, v500);
    float32x4_t v506 = vmulq_f32(v421, v505);
    float32x4_t v519 = vmulq_f32(v517, v518);
    float32x4_t v527 = vmulq_f32(v525, v526);
    float32x4_t v533 = vrev64q_f32(v440);
    float32x4_t v543 = vmulq_f32(v541, v542);
    float32x4_t v551 = vmulq_f32(v549, v550);
    float32x4_t v557 = vrev64q_f32(v441);
    float32x4_t v567 = vmulq_f32(v565, v566);
    float32x4_t v575 = vmulq_f32(v573, v574);
    float32x4_t v581 = vrev64q_f32(v442);
    float32x4_t v435 = vaddq_f32(v1605, v409);
    float32x4_t v452 = vmulq_f32(v409, v451);
    float32x4_t v457 = vmulq_f32(v436, v456);
    float32x4_t v465 = vmulq_f32(v463, v464);
    float32x4_t v473 = vmulq_f32(v471, v472);
    float32x4_t v479 = vrev64q_f32(v437);
    float32x4_t v496 = vmulq_f32(v438, v495);
    float32x4_t v511 = vmulq_f32(v439, v510);
    float32x4_t v535 = vmulq_f32(v533, v534);
    float32x4_t v559 = vmulq_f32(v557, v558);
    float32x4_t v583 = vmulq_f32(v581, v582);
    float32x4_t v585 = vaddq_f32(v491, v486);
    float32x4_t v481 = vmulq_f32(v479, v480);
    float32x4_t v584 = vsubq_f32(v435, v452);
    float32x4_t v586 = vsubq_f32(v585, v457);
    float32x4_t v587 = vaddq_f32(v491, v496);
    float32x4_t v589 = vsubq_f32(v496, v486);
    float32x4_t v597 = vsubq_f32(v519, v535);
    float32x4_t v598 = vsubq_f32(v527, v535);
    float32x4_t v599 = vsubq_f32(v543, v559);
    float32x4_t v600 = vsubq_f32(v551, v559);
    float32x4_t v601 = vsubq_f32(v567, v583);
    float32x4_t v602 = vaddq_f32(v575, v583);
    vst1q_f32((float32_t *)v1469, v435);
    float32x4_t v588 = vaddq_f32(v587, v457);
    float32x4_t v590 = vsubq_f32(v589, v457);
    float32x4_t v591 = vaddq_f32(v584, v501);
    float32x4_t v593 = vsubq_f32(v584, v506);
    float32x4_t v595 = vsubq_f32(v584, v501);
    float32x4_t v603 = vsubq_f32(v465, v481);
    float32x4_t v604 = vsubq_f32(v473, v481);
    float32x4_t v615 = vaddq_f32(v597, v601);
    float32x4_t v617 = vaddq_f32(v599, v601);
    float32x4_t v619 = vsubq_f32(v598, v602);
    float32x4_t v592 = vaddq_f32(v591, v506);
    float32x4_t v594 = vsubq_f32(v593, v511);
    float32x4_t v596 = vaddq_f32(v595, v511);
    float32x4_t v611 = vsubq_f32(v604, v597);
    float32x4_t v613 = vsubq_f32(v602, v603);
    float32x4_t v616 = vaddq_f32(v615, v604);
    float32x4_t v618 = vsubq_f32(v617, v604);
    float32x4_t v620 = vsubq_f32(v619, v603);
    float32x4_t v621 = vaddq_f32(v603, v598);
    float32x4_t v605 = vaddq_f32(v586, v592);
    float32x4_t v606 = vaddq_f32(v588, v594);
    float32x4_t v607 = vsubq_f32(v594, v588);
    float32x4_t v608 = vaddq_f32(v590, v596);
    float32x4_t v609 = vsubq_f32(v592, v586);
    float32x4_t v610 = vsubq_f32(v596, v590);
    float32x4_t v612 = vaddq_f32(v611, v599);
    float32x4_t v614 = vsubq_f32(v613, v600);
    float32x4_t v622 = vsubq_f32(v621, v600);
    float32x4_t v623 = vsubq_f32(v605, v612);
    float32x4_t v624 = vaddq_f32(v606, v614);
    float32x4_t v625 = vsubq_f32(v607, v616);
    float32x4_t v626 = vsubq_f32(v608, v618);
    float32x4_t v627 = vaddq_f32(v609, v620);
    float32x4_t v628 = vsubq_f32(v610, v622);
    float32x4_t v629 = vaddq_f32(v610, v622);
    float32x4_t v630 = vsubq_f32(v609, v620);
    float32x4_t v631 = vaddq_f32(v608, v618);
    float32x4_t v632 = vaddq_f32(v607, v616);
    float32x4_t v633 = vsubq_f32(v606, v614);
    float32x4_t v634 = vaddq_f32(v605, v612);
    vst1q_f32((float32_t *)v1478, v623);
    vst1q_f32((float32_t *)v1487, v624);
    vst1q_f32((float32_t *)v1496, v625);
    vst1q_f32((float32_t *)v1505, v626);
    vst1q_f32((float32_t *)v1514, v627);
    vst1q_f32((float32_t *)v1523, v628);
    vst1q_f32((float32_t *)v1532, v629);
    vst1q_f32((float32_t *)v1541, v630);
    vst1q_f32((float32_t *)v1550, v631);
    vst1q_f32((float32_t *)v1559, v632);
    vst1q_f32((float32_t *)v1568, v633);
    vst1q_f32((float32_t *)v1577, v634);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v726 * 2; j < howmany; j += 1) {
    float32x2_t v738 = v5[istride];
    float v1087 = 1.0833333333333333e+00F;
    float v1091 = -3.0046260628866578e-01F;
    float v1094 = 7.4927933062613905e-01F;
    float v1095 = -7.4927933062613905e-01F;
    float v1101 = 4.0100212832186721e-01F;
    float v1102 = -4.0100212832186721e-01F;
    float v1108 = 5.7514072947400308e-01F;
    float v1109 = -5.7514072947400308e-01F;
    float v1116 = 5.2422663952658211e-01F;
    float v1120 = 5.1652078062348972e-01F;
    float v1124 = 7.7058589030924258e-03F;
    float v1128 = 4.2763404682656941e-01F;
    float v1132 = 1.5180597207438440e-01F;
    float v1136 = 5.7944001890096386e-01F;
    float v1139 = 1.1543953381323635e+00F;
    float v1140 = -1.1543953381323635e+00F;
    float v1146 = 9.0655220171271012e-01F;
    float v1147 = -9.0655220171271012e-01F;
    float v1153 = 8.1857027294591811e-01F;
    float v1154 = -8.1857027294591811e-01F;
    float v1160 = 1.1971367726043427e+00F;
    float v1161 = -1.1971367726043427e+00F;
    float v1167 = 8.6131170741789742e-01F;
    float v1168 = -8.6131170741789742e-01F;
    float v1174 = 1.1091548438375507e+00F;
    float v1175 = -1.1091548438375507e+00F;
    float v1181 = 4.2741434471979367e-02F;
    float v1182 = -4.2741434471979367e-02F;
    float v1188 = -4.5240494294812715e-02F;
    float v1189 = 4.5240494294812715e-02F;
    float v1195 = 2.9058457089163264e-01F;
    float v1196 = -2.9058457089163264e-01F;
    float32x2_t v1198 = (float32x2_t){v4, v4};
    float32x2_t v915 = v7[0];
    float32x2_t v916 = vtrn1_f32(v738, v738);
    float32x2_t v917 = vtrn2_f32(v738, v738);
    float32x2_t v920 = v7[1];
    float32x2_t v925 = v7[22];
    float32x2_t v930 = v7[23];
    float32x2_t v935 = v7[2];
    float32x2_t v940 = v7[3];
    float32x2_t v945 = v7[20];
    float32x2_t v950 = v7[21];
    float32x2_t v955 = v7[4];
    float32x2_t v960 = v7[5];
    float32x2_t v965 = v7[18];
    float32x2_t v970 = v7[19];
    float32x2_t v975 = v7[6];
    float32x2_t v980 = v7[7];
    float32x2_t v985 = v7[16];
    float32x2_t v990 = v7[17];
    float32x2_t v995 = v7[8];
    float32x2_t v1000 = v7[9];
    float32x2_t v1005 = v7[14];
    float32x2_t v1010 = v7[15];
    float32x2_t v1015 = v7[10];
    float32x2_t v1020 = v7[11];
    float32x2_t v1025 = v7[12];
    float32x2_t v1030 = v7[13];
    float32x2_t v1073 = v5[0];
    float32x2_t v1088 = (float32x2_t){v1087, v1087};
    float32x2_t v1092 = (float32x2_t){v1091, v1091};
    float32x2_t v1096 = (float32x2_t){v1094, v1095};
    float32x2_t v1103 = (float32x2_t){v1101, v1102};
    float32x2_t v1110 = (float32x2_t){v1108, v1109};
    float32x2_t v1117 = (float32x2_t){v1116, v1116};
    float32x2_t v1121 = (float32x2_t){v1120, v1120};
    float32x2_t v1125 = (float32x2_t){v1124, v1124};
    float32x2_t v1129 = (float32x2_t){v1128, v1128};
    float32x2_t v1133 = (float32x2_t){v1132, v1132};
    float32x2_t v1137 = (float32x2_t){v1136, v1136};
    float32x2_t v1141 = (float32x2_t){v1139, v1140};
    float32x2_t v1148 = (float32x2_t){v1146, v1147};
    float32x2_t v1155 = (float32x2_t){v1153, v1154};
    float32x2_t v1162 = (float32x2_t){v1160, v1161};
    float32x2_t v1169 = (float32x2_t){v1167, v1168};
    float32x2_t v1176 = (float32x2_t){v1174, v1175};
    float32x2_t v1183 = (float32x2_t){v1181, v1182};
    float32x2_t v1190 = (float32x2_t){v1188, v1189};
    float32x2_t v1197 = (float32x2_t){v1195, v1196};
    float32x2_t v753 = v5[istride * 12];
    float32x2_t v768 = v5[istride * 2];
    float32x2_t v783 = v5[istride * 11];
    float32x2_t v798 = v5[istride * 3];
    float32x2_t v813 = v5[istride * 10];
    float32x2_t v828 = v5[istride * 4];
    float32x2_t v843 = v5[istride * 9];
    float32x2_t v858 = v5[istride * 5];
    float32x2_t v873 = v5[istride * 8];
    float32x2_t v888 = v5[istride * 6];
    float32x2_t v903 = v5[istride * 7];
    float32x2_t v921 = vmul_f32(v916, v915);
    float32x2_t v1098 = vmul_f32(v1198, v1096);
    float32x2_t v1105 = vmul_f32(v1198, v1103);
    float32x2_t v1112 = vmul_f32(v1198, v1110);
    float32x2_t v1143 = vmul_f32(v1198, v1141);
    float32x2_t v1150 = vmul_f32(v1198, v1148);
    float32x2_t v1157 = vmul_f32(v1198, v1155);
    float32x2_t v1164 = vmul_f32(v1198, v1162);
    float32x2_t v1171 = vmul_f32(v1198, v1169);
    float32x2_t v1178 = vmul_f32(v1198, v1176);
    float32x2_t v1185 = vmul_f32(v1198, v1183);
    float32x2_t v1192 = vmul_f32(v1198, v1190);
    float32x2_t v1199 = vmul_f32(v1198, v1197);
    float32x2_t v923 = vfma_f32(v921, v917, v920);
    float32x2_t v926 = vtrn1_f32(v753, v753);
    float32x2_t v927 = vtrn2_f32(v753, v753);
    float32x2_t v936 = vtrn1_f32(v768, v768);
    float32x2_t v937 = vtrn2_f32(v768, v768);
    float32x2_t v946 = vtrn1_f32(v783, v783);
    float32x2_t v947 = vtrn2_f32(v783, v783);
    float32x2_t v956 = vtrn1_f32(v798, v798);
    float32x2_t v957 = vtrn2_f32(v798, v798);
    float32x2_t v966 = vtrn1_f32(v813, v813);
    float32x2_t v967 = vtrn2_f32(v813, v813);
    float32x2_t v976 = vtrn1_f32(v828, v828);
    float32x2_t v977 = vtrn2_f32(v828, v828);
    float32x2_t v986 = vtrn1_f32(v843, v843);
    float32x2_t v987 = vtrn2_f32(v843, v843);
    float32x2_t v996 = vtrn1_f32(v858, v858);
    float32x2_t v997 = vtrn2_f32(v858, v858);
    float32x2_t v1006 = vtrn1_f32(v873, v873);
    float32x2_t v1007 = vtrn2_f32(v873, v873);
    float32x2_t v1016 = vtrn1_f32(v888, v888);
    float32x2_t v1017 = vtrn2_f32(v888, v888);
    float32x2_t v1026 = vtrn1_f32(v903, v903);
    float32x2_t v1027 = vtrn2_f32(v903, v903);
    float32x2_t v931 = vmul_f32(v926, v925);
    float32x2_t v941 = vmul_f32(v936, v935);
    float32x2_t v951 = vmul_f32(v946, v945);
    float32x2_t v961 = vmul_f32(v956, v955);
    float32x2_t v971 = vmul_f32(v966, v965);
    float32x2_t v981 = vmul_f32(v976, v975);
    float32x2_t v991 = vmul_f32(v986, v985);
    float32x2_t v1001 = vmul_f32(v996, v995);
    float32x2_t v1011 = vmul_f32(v1006, v1005);
    float32x2_t v1021 = vmul_f32(v1016, v1015);
    float32x2_t v1031 = vmul_f32(v1026, v1025);
    float32x2_t v933 = vfma_f32(v931, v927, v930);
    float32x2_t v943 = vfma_f32(v941, v937, v940);
    float32x2_t v953 = vfma_f32(v951, v947, v950);
    float32x2_t v963 = vfma_f32(v961, v957, v960);
    float32x2_t v973 = vfma_f32(v971, v967, v970);
    float32x2_t v983 = vfma_f32(v981, v977, v980);
    float32x2_t v993 = vfma_f32(v991, v987, v990);
    float32x2_t v1003 = vfma_f32(v1001, v997, v1000);
    float32x2_t v1013 = vfma_f32(v1011, v1007, v1010);
    float32x2_t v1023 = vfma_f32(v1021, v1017, v1020);
    float32x2_t v1033 = vfma_f32(v1031, v1027, v1030);
    float32x2_t v1034 = vadd_f32(v923, v933);
    float32x2_t v1035 = vadd_f32(v943, v953);
    float32x2_t v1036 = vadd_f32(v963, v973);
    float32x2_t v1037 = vadd_f32(v983, v993);
    float32x2_t v1038 = vadd_f32(v1003, v1013);
    float32x2_t v1039 = vadd_f32(v1023, v1033);
    float32x2_t v1040 = vsub_f32(v923, v933);
    float32x2_t v1041 = vsub_f32(v943, v953);
    float32x2_t v1042 = vsub_f32(v963, v973);
    float32x2_t v1043 = vsub_f32(v983, v993);
    float32x2_t v1044 = vsub_f32(v1003, v1013);
    float32x2_t v1045 = vsub_f32(v1023, v1033);
    float32x2_t v1046 = vadd_f32(v1035, v1038);
    float32x2_t v1048 = vadd_f32(v1034, v1036);
    float32x2_t v1051 = vadd_f32(v1041, v1044);
    float32x2_t v1053 = vadd_f32(v1040, v1042);
    float32x2_t v1055 = vsub_f32(v1035, v1039);
    float32x2_t v1056 = vsub_f32(v1036, v1037);
    float32x2_t v1057 = vsub_f32(v1034, v1037);
    float32x2_t v1058 = vsub_f32(v1038, v1039);
    float32x2_t v1063 = vsub_f32(v1041, v1045);
    float32x2_t v1064 = vsub_f32(v1040, v1042);
    float32x2_t v1065 = vsub_f32(v1041, v1044);
    float32x2_t v1066 = vadd_f32(v1040, v1043);
    float32x2_t v1067 = vsub_f32(v1044, v1045);
    float32x2_t v1068 = vadd_f32(v1042, v1043);
    float32x2_t v1047 = vadd_f32(v1046, v1039);
    float32x2_t v1049 = vadd_f32(v1048, v1037);
    float32x2_t v1052 = vadd_f32(v1051, v1045);
    float32x2_t v1054 = vsub_f32(v1053, v1043);
    float32x2_t v1059 = vsub_f32(v1055, v1056);
    float32x2_t v1060 = vsub_f32(v1057, v1058);
    float32x2_t v1061 = vadd_f32(v1055, v1056);
    float32x2_t v1062 = vadd_f32(v1057, v1058);
    float32x2_t v1079 = vadd_f32(v1063, v1064);
    float32x2_t v1080 = vadd_f32(v1065, v1066);
    float32x2_t v1081 = vsub_f32(v1067, v1068);
    float32x2_t v1144 = vrev64_f32(v1063);
    float32x2_t v1151 = vrev64_f32(v1064);
    float32x2_t v1165 = vrev64_f32(v1065);
    float32x2_t v1172 = vrev64_f32(v1066);
    float32x2_t v1186 = vrev64_f32(v1067);
    float32x2_t v1193 = vrev64_f32(v1068);
    float32x2_t v1050 = vadd_f32(v1047, v1049);
    float32x2_t v1075 = vsub_f32(v1049, v1047);
    float32x2_t v1076 = vadd_f32(v1052, v1054);
    float32x2_t v1077 = vadd_f32(v1059, v1060);
    float32x2_t v1078 = vsub_f32(v1061, v1062);
    float32x2_t v1099 = vrev64_f32(v1052);
    float32x2_t v1106 = vrev64_f32(v1054);
    float32x2_t v1118 = vmul_f32(v1059, v1117);
    float32x2_t v1122 = vmul_f32(v1060, v1121);
    float32x2_t v1130 = vmul_f32(v1061, v1129);
    float32x2_t v1134 = vmul_f32(v1062, v1133);
    float32x2_t v1145 = vmul_f32(v1144, v1143);
    float32x2_t v1152 = vmul_f32(v1151, v1150);
    float32x2_t v1158 = vrev64_f32(v1079);
    float32x2_t v1166 = vmul_f32(v1165, v1164);
    float32x2_t v1173 = vmul_f32(v1172, v1171);
    float32x2_t v1179 = vrev64_f32(v1080);
    float32x2_t v1187 = vmul_f32(v1186, v1185);
    float32x2_t v1194 = vmul_f32(v1193, v1192);
    float32x2_t v1200 = vrev64_f32(v1081);
    float32x2_t v1074 = vadd_f32(v1073, v1050);
    float32x2_t v1089 = vmul_f32(v1050, v1088);
    float32x2_t v1093 = vmul_f32(v1075, v1092);
    float32x2_t v1100 = vmul_f32(v1099, v1098);
    float32x2_t v1107 = vmul_f32(v1106, v1105);
    float32x2_t v1113 = vrev64_f32(v1076);
    float32x2_t v1126 = vmul_f32(v1077, v1125);
    float32x2_t v1138 = vmul_f32(v1078, v1137);
    float32x2_t v1159 = vmul_f32(v1158, v1157);
    float32x2_t v1180 = vmul_f32(v1179, v1178);
    float32x2_t v1201 = vmul_f32(v1200, v1199);
    float32x2_t v1203 = vadd_f32(v1122, v1118);
    float32x2_t v1114 = vmul_f32(v1113, v1112);
    float32x2_t v1202 = vsub_f32(v1074, v1089);
    float32x2_t v1204 = vsub_f32(v1203, v1093);
    float32x2_t v1205 = vadd_f32(v1122, v1126);
    float32x2_t v1207 = vsub_f32(v1126, v1118);
    float32x2_t v1215 = vsub_f32(v1145, v1159);
    float32x2_t v1216 = vsub_f32(v1152, v1159);
    float32x2_t v1217 = vsub_f32(v1166, v1180);
    float32x2_t v1218 = vsub_f32(v1173, v1180);
    float32x2_t v1219 = vsub_f32(v1187, v1201);
    float32x2_t v1220 = vadd_f32(v1194, v1201);
    v6[0] = v1074;
    float32x2_t v1206 = vadd_f32(v1205, v1093);
    float32x2_t v1208 = vsub_f32(v1207, v1093);
    float32x2_t v1209 = vadd_f32(v1202, v1130);
    float32x2_t v1211 = vsub_f32(v1202, v1134);
    float32x2_t v1213 = vsub_f32(v1202, v1130);
    float32x2_t v1221 = vsub_f32(v1100, v1114);
    float32x2_t v1222 = vsub_f32(v1107, v1114);
    float32x2_t v1233 = vadd_f32(v1215, v1219);
    float32x2_t v1235 = vadd_f32(v1217, v1219);
    float32x2_t v1237 = vsub_f32(v1216, v1220);
    float32x2_t v1210 = vadd_f32(v1209, v1134);
    float32x2_t v1212 = vsub_f32(v1211, v1138);
    float32x2_t v1214 = vadd_f32(v1213, v1138);
    float32x2_t v1229 = vsub_f32(v1222, v1215);
    float32x2_t v1231 = vsub_f32(v1220, v1221);
    float32x2_t v1234 = vadd_f32(v1233, v1222);
    float32x2_t v1236 = vsub_f32(v1235, v1222);
    float32x2_t v1238 = vsub_f32(v1237, v1221);
    float32x2_t v1239 = vadd_f32(v1221, v1216);
    float32x2_t v1223 = vadd_f32(v1204, v1210);
    float32x2_t v1224 = vadd_f32(v1206, v1212);
    float32x2_t v1225 = vsub_f32(v1212, v1206);
    float32x2_t v1226 = vadd_f32(v1208, v1214);
    float32x2_t v1227 = vsub_f32(v1210, v1204);
    float32x2_t v1228 = vsub_f32(v1214, v1208);
    float32x2_t v1230 = vadd_f32(v1229, v1217);
    float32x2_t v1232 = vsub_f32(v1231, v1218);
    float32x2_t v1240 = vsub_f32(v1239, v1218);
    float32x2_t v1241 = vsub_f32(v1223, v1230);
    float32x2_t v1242 = vadd_f32(v1224, v1232);
    float32x2_t v1243 = vsub_f32(v1225, v1234);
    float32x2_t v1244 = vsub_f32(v1226, v1236);
    float32x2_t v1245 = vadd_f32(v1227, v1238);
    float32x2_t v1246 = vsub_f32(v1228, v1240);
    float32x2_t v1247 = vadd_f32(v1228, v1240);
    float32x2_t v1248 = vsub_f32(v1227, v1238);
    float32x2_t v1249 = vadd_f32(v1226, v1236);
    float32x2_t v1250 = vadd_f32(v1225, v1234);
    float32x2_t v1251 = vsub_f32(v1224, v1232);
    float32x2_t v1252 = vadd_f32(v1223, v1230);
    v6[ostride * 12] = v1241;
    v6[ostride * 11] = v1242;
    v6[ostride * 10] = v1243;
    v6[ostride * 9] = v1244;
    v6[ostride * 8] = v1245;
    v6[ostride * 7] = v1246;
    v6[ostride * 6] = v1247;
    v6[ostride * 5] = v1248;
    v6[ostride * 4] = v1249;
    v6[ostride * 3] = v1250;
    v6[ostride * 2] = v1251;
    v6[ostride] = v1252;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu13(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v254 = 1.0833333333333333e+00F;
    float v259 = -3.0046260628866578e-01F;
    float v264 = -7.4927933062613905e-01F;
    float v271 = -4.0100212832186721e-01F;
    float v278 = -5.7514072947400308e-01F;
    float v285 = 5.2422663952658211e-01F;
    float v290 = 5.1652078062348972e-01F;
    float v295 = 7.7058589030924258e-03F;
    float v300 = 4.2763404682656941e-01F;
    float v305 = 1.5180597207438440e-01F;
    float v310 = 5.7944001890096386e-01F;
    float v315 = -1.1543953381323635e+00F;
    float v322 = -9.0655220171271012e-01F;
    float v329 = -8.1857027294591811e-01F;
    float v336 = -1.1971367726043427e+00F;
    float v343 = -8.6131170741789742e-01F;
    float v350 = -1.1091548438375507e+00F;
    float v357 = -4.2741434471979367e-02F;
    float v364 = 4.5240494294812715e-02F;
    float v371 = -2.9058457089163264e-01F;
    const float32x2_t *v525 = &v5[v0];
    float32x2_t *v773 = &v6[v2];
    int64_t v30 = v0 * 12;
    int64_t v41 = v0 * 2;
    int64_t v52 = v0 * 11;
    int64_t v63 = v0 * 3;
    int64_t v74 = v0 * 10;
    int64_t v85 = v0 * 4;
    int64_t v96 = v0 * 9;
    int64_t v107 = v0 * 5;
    int64_t v118 = v0 * 8;
    int64_t v129 = v0 * 6;
    int64_t v140 = v0 * 7;
    float v267 = v4 * v264;
    float v274 = v4 * v271;
    float v281 = v4 * v278;
    float v318 = v4 * v315;
    float v325 = v4 * v322;
    float v332 = v4 * v329;
    float v339 = v4 * v336;
    float v346 = v4 * v343;
    float v353 = v4 * v350;
    float v360 = v4 * v357;
    float v367 = v4 * v364;
    float v374 = v4 * v371;
    int64_t v436 = v2 * 12;
    int64_t v443 = v2 * 11;
    int64_t v450 = v2 * 10;
    int64_t v457 = v2 * 9;
    int64_t v464 = v2 * 8;
    int64_t v471 = v2 * 7;
    int64_t v478 = v2 * 6;
    int64_t v485 = v2 * 5;
    int64_t v492 = v2 * 4;
    int64_t v499 = v2 * 3;
    int64_t v506 = v2 * 2;
    const float32x2_t *v634 = &v5[0];
    svfloat32_t v638 = svdup_n_f32(v254);
    svfloat32_t v639 = svdup_n_f32(v259);
    svfloat32_t v643 = svdup_n_f32(v285);
    svfloat32_t v644 = svdup_n_f32(v290);
    svfloat32_t v645 = svdup_n_f32(v295);
    svfloat32_t v646 = svdup_n_f32(v300);
    svfloat32_t v647 = svdup_n_f32(v305);
    svfloat32_t v648 = svdup_n_f32(v310);
    float32x2_t *v665 = &v6[0];
    svfloat32_t v777 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v525)[0]));
    svfloat32_t v152 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v156 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v160 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v164 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v168 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v172 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v176 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v180 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v184 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v188 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v192 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v196 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    const float32x2_t *v534 = &v5[v30];
    const float32x2_t *v543 = &v5[v41];
    const float32x2_t *v552 = &v5[v52];
    const float32x2_t *v561 = &v5[v63];
    const float32x2_t *v570 = &v5[v74];
    const float32x2_t *v579 = &v5[v85];
    const float32x2_t *v588 = &v5[v96];
    const float32x2_t *v597 = &v5[v107];
    const float32x2_t *v606 = &v5[v118];
    const float32x2_t *v615 = &v5[v129];
    const float32x2_t *v624 = &v5[v140];
    svfloat32_t v640 = svdup_n_f32(v267);
    svfloat32_t v641 = svdup_n_f32(v274);
    svfloat32_t v642 = svdup_n_f32(v281);
    svfloat32_t v649 = svdup_n_f32(v318);
    svfloat32_t v650 = svdup_n_f32(v325);
    svfloat32_t v651 = svdup_n_f32(v332);
    svfloat32_t v652 = svdup_n_f32(v339);
    svfloat32_t v653 = svdup_n_f32(v346);
    svfloat32_t v654 = svdup_n_f32(v353);
    svfloat32_t v655 = svdup_n_f32(v360);
    svfloat32_t v656 = svdup_n_f32(v367);
    svfloat32_t v657 = svdup_n_f32(v374);
    float32x2_t *v674 = &v6[v436];
    float32x2_t *v683 = &v6[v443];
    float32x2_t *v692 = &v6[v450];
    float32x2_t *v701 = &v6[v457];
    float32x2_t *v710 = &v6[v464];
    float32x2_t *v719 = &v6[v471];
    float32x2_t *v728 = &v6[v478];
    float32x2_t *v737 = &v6[v485];
    float32x2_t *v746 = &v6[v492];
    float32x2_t *v755 = &v6[v499];
    float32x2_t *v764 = &v6[v506];
    svfloat32_t v801 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v634)[0]));
    svfloat32_t zero153 = svdup_n_f32(0);
    svfloat32_t v153 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero153, v777, v152, 0),
                     v777, v152, 90);
    svfloat32_t v779 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v534)[0]));
    svfloat32_t v781 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v543)[0]));
    svfloat32_t v783 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v552)[0]));
    svfloat32_t v785 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v561)[0]));
    svfloat32_t v787 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v570)[0]));
    svfloat32_t v789 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v579)[0]));
    svfloat32_t v791 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v588)[0]));
    svfloat32_t v793 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v597)[0]));
    svfloat32_t v795 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v606)[0]));
    svfloat32_t v797 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v615)[0]));
    svfloat32_t v799 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v624)[0]));
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v779, v156, 0),
                     v779, v156, 90);
    svfloat32_t zero161 = svdup_n_f32(0);
    svfloat32_t v161 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero161, v781, v160, 0),
                     v781, v160, 90);
    svfloat32_t zero165 = svdup_n_f32(0);
    svfloat32_t v165 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero165, v783, v164, 0),
                     v783, v164, 90);
    svfloat32_t zero169 = svdup_n_f32(0);
    svfloat32_t v169 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero169, v785, v168, 0),
                     v785, v168, 90);
    svfloat32_t zero173 = svdup_n_f32(0);
    svfloat32_t v173 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero173, v787, v172, 0),
                     v787, v172, 90);
    svfloat32_t zero177 = svdup_n_f32(0);
    svfloat32_t v177 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero177, v789, v176, 0),
                     v789, v176, 90);
    svfloat32_t zero181 = svdup_n_f32(0);
    svfloat32_t v181 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero181, v791, v180, 0),
                     v791, v180, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero185, v793, v184, 0),
                     v793, v184, 90);
    svfloat32_t zero189 = svdup_n_f32(0);
    svfloat32_t v189 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero189, v795, v188, 0),
                     v795, v188, 90);
    svfloat32_t zero193 = svdup_n_f32(0);
    svfloat32_t v193 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero193, v797, v192, 0),
                     v797, v192, 90);
    svfloat32_t zero197 = svdup_n_f32(0);
    svfloat32_t v197 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero197, v799, v196, 0),
                     v799, v196, 90);
    svfloat32_t v198 = svadd_f32_x(svptrue_b32(), v153, v157);
    svfloat32_t v199 = svadd_f32_x(svptrue_b32(), v161, v165);
    svfloat32_t v200 = svadd_f32_x(svptrue_b32(), v169, v173);
    svfloat32_t v201 = svadd_f32_x(svptrue_b32(), v177, v181);
    svfloat32_t v202 = svadd_f32_x(svptrue_b32(), v185, v189);
    svfloat32_t v203 = svadd_f32_x(svptrue_b32(), v193, v197);
    svfloat32_t v204 = svsub_f32_x(svptrue_b32(), v153, v157);
    svfloat32_t v205 = svsub_f32_x(svptrue_b32(), v161, v165);
    svfloat32_t v206 = svsub_f32_x(svptrue_b32(), v169, v173);
    svfloat32_t v207 = svsub_f32_x(svptrue_b32(), v177, v181);
    svfloat32_t v208 = svsub_f32_x(svptrue_b32(), v185, v189);
    svfloat32_t v209 = svsub_f32_x(svptrue_b32(), v193, v197);
    svfloat32_t v210 = svadd_f32_x(svptrue_b32(), v199, v202);
    svfloat32_t v212 = svadd_f32_x(svptrue_b32(), v198, v200);
    svfloat32_t v215 = svadd_f32_x(svptrue_b32(), v205, v208);
    svfloat32_t v217 = svadd_f32_x(svptrue_b32(), v204, v206);
    svfloat32_t v219 = svsub_f32_x(svptrue_b32(), v199, v203);
    svfloat32_t v220 = svsub_f32_x(svptrue_b32(), v200, v201);
    svfloat32_t v221 = svsub_f32_x(svptrue_b32(), v198, v201);
    svfloat32_t v222 = svsub_f32_x(svptrue_b32(), v202, v203);
    svfloat32_t v227 = svsub_f32_x(svptrue_b32(), v205, v209);
    svfloat32_t v228 = svsub_f32_x(svptrue_b32(), v204, v206);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v205, v208);
    svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v204, v207);
    svfloat32_t v231 = svsub_f32_x(svptrue_b32(), v208, v209);
    svfloat32_t v232 = svadd_f32_x(svptrue_b32(), v206, v207);
    svfloat32_t v211 = svadd_f32_x(svptrue_b32(), v210, v203);
    svfloat32_t v213 = svadd_f32_x(svptrue_b32(), v212, v201);
    svfloat32_t v216 = svadd_f32_x(svptrue_b32(), v215, v209);
    svfloat32_t v218 = svsub_f32_x(svptrue_b32(), v217, v207);
    svfloat32_t v223 = svsub_f32_x(svptrue_b32(), v219, v220);
    svfloat32_t v224 = svsub_f32_x(svptrue_b32(), v221, v222);
    svfloat32_t v225 = svadd_f32_x(svptrue_b32(), v219, v220);
    svfloat32_t v226 = svadd_f32_x(svptrue_b32(), v221, v222);
    svfloat32_t v245 = svadd_f32_x(svptrue_b32(), v227, v228);
    svfloat32_t v246 = svadd_f32_x(svptrue_b32(), v229, v230);
    svfloat32_t v247 = svsub_f32_x(svptrue_b32(), v231, v232);
    svfloat32_t zero320 = svdup_n_f32(0);
    svfloat32_t v320 = svcmla_f32_x(pred_full, zero320, v649, v227, 90);
    svfloat32_t zero327 = svdup_n_f32(0);
    svfloat32_t v327 = svcmla_f32_x(pred_full, zero327, v650, v228, 90);
    svfloat32_t zero341 = svdup_n_f32(0);
    svfloat32_t v341 = svcmla_f32_x(pred_full, zero341, v652, v229, 90);
    svfloat32_t zero348 = svdup_n_f32(0);
    svfloat32_t v348 = svcmla_f32_x(pred_full, zero348, v653, v230, 90);
    svfloat32_t zero362 = svdup_n_f32(0);
    svfloat32_t v362 = svcmla_f32_x(pred_full, zero362, v655, v231, 90);
    svfloat32_t v214 = svadd_f32_x(svptrue_b32(), v211, v213);
    svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v213, v211);
    svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v216, v218);
    svfloat32_t v243 = svadd_f32_x(svptrue_b32(), v223, v224);
    svfloat32_t v244 = svsub_f32_x(svptrue_b32(), v225, v226);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 = svcmla_f32_x(pred_full, zero269, v640, v216, 90);
    svfloat32_t zero276 = svdup_n_f32(0);
    svfloat32_t v276 = svcmla_f32_x(pred_full, zero276, v641, v218, 90);
    svfloat32_t v288 = svmul_f32_x(svptrue_b32(), v223, v643);
    svfloat32_t zero334 = svdup_n_f32(0);
    svfloat32_t v334 = svcmla_f32_x(pred_full, zero334, v651, v245, 90);
    svfloat32_t zero355 = svdup_n_f32(0);
    svfloat32_t v355 = svcmla_f32_x(pred_full, zero355, v654, v246, 90);
    svfloat32_t zero376 = svdup_n_f32(0);
    svfloat32_t v376 = svcmla_f32_x(pred_full, zero376, v657, v247, 90);
    svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v801, v214);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 = svcmla_f32_x(pred_full, zero283, v642, v242, 90);
    svfloat32_t v298 = svmul_f32_x(svptrue_b32(), v243, v645);
    svfloat32_t v378 = svmla_f32_x(pred_full, v288, v224, v644);
    svfloat32_t v390 = svsub_f32_x(svptrue_b32(), v320, v334);
    svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v327, v334);
    svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v341, v355);
    svfloat32_t v393 = svsub_f32_x(svptrue_b32(), v348, v355);
    svfloat32_t v394 = svsub_f32_x(svptrue_b32(), v362, v376);
    svfloat32_t v395 = svcmla_f32_x(pred_full, v376, v656, v232, 90);
    svfloat32_t v377 = svmls_f32_x(pred_full, v240, v214, v638);
    svfloat32_t v379 = svmls_f32_x(pred_full, v378, v241, v639);
    svfloat32_t v380 = svmla_f32_x(pred_full, v298, v224, v644);
    svfloat32_t v382 = svnmls_f32_x(pred_full, v288, v243, v645);
    svfloat32_t v396 = svsub_f32_x(svptrue_b32(), v269, v283);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v276, v283);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v390, v394);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v392, v394);
    svfloat32_t v412 = svsub_f32_x(svptrue_b32(), v391, v395);
    svst1_f64(pred_full, (double *)(v665), svreinterpret_f64_f32(v240));
    svfloat32_t v381 = svmla_f32_x(pred_full, v380, v241, v639);
    svfloat32_t v383 = svmls_f32_x(pred_full, v382, v241, v639);
    svfloat32_t v384 = svmla_f32_x(pred_full, v377, v225, v646);
    svfloat32_t v386 = svmls_f32_x(pred_full, v377, v226, v647);
    svfloat32_t v388 = svmls_f32_x(pred_full, v377, v225, v646);
    svfloat32_t v404 = svsub_f32_x(svptrue_b32(), v397, v390);
    svfloat32_t v406 = svsub_f32_x(svptrue_b32(), v395, v396);
    svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v408, v397);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v410, v397);
    svfloat32_t v413 = svsub_f32_x(svptrue_b32(), v412, v396);
    svfloat32_t v414 = svadd_f32_x(svptrue_b32(), v396, v391);
    svfloat32_t v385 = svmla_f32_x(pred_full, v384, v226, v647);
    svfloat32_t v387 = svmls_f32_x(pred_full, v386, v244, v648);
    svfloat32_t v389 = svmla_f32_x(pred_full, v388, v244, v648);
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v404, v392);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v406, v393);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v414, v393);
    svfloat32_t v398 = svadd_f32_x(svptrue_b32(), v379, v385);
    svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v381, v387);
    svfloat32_t v400 = svsub_f32_x(svptrue_b32(), v387, v381);
    svfloat32_t v401 = svadd_f32_x(svptrue_b32(), v383, v389);
    svfloat32_t v402 = svsub_f32_x(svptrue_b32(), v385, v379);
    svfloat32_t v403 = svsub_f32_x(svptrue_b32(), v389, v383);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v398, v405);
    svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v399, v407);
    svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v400, v409);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v401, v411);
    svfloat32_t v420 = svadd_f32_x(svptrue_b32(), v402, v413);
    svfloat32_t v421 = svsub_f32_x(svptrue_b32(), v403, v415);
    svfloat32_t v422 = svadd_f32_x(svptrue_b32(), v403, v415);
    svfloat32_t v423 = svsub_f32_x(svptrue_b32(), v402, v413);
    svfloat32_t v424 = svadd_f32_x(svptrue_b32(), v401, v411);
    svfloat32_t v425 = svadd_f32_x(svptrue_b32(), v400, v409);
    svfloat32_t v426 = svsub_f32_x(svptrue_b32(), v399, v407);
    svfloat32_t v427 = svadd_f32_x(svptrue_b32(), v398, v405);
    svst1_f64(pred_full, (double *)(v674), svreinterpret_f64_f32(v416));
    svst1_f64(pred_full, (double *)(v683), svreinterpret_f64_f32(v417));
    svst1_f64(pred_full, (double *)(v692), svreinterpret_f64_f32(v418));
    svst1_f64(pred_full, (double *)(v701), svreinterpret_f64_f32(v419));
    svst1_f64(pred_full, (double *)(v710), svreinterpret_f64_f32(v420));
    svst1_f64(pred_full, (double *)(v719), svreinterpret_f64_f32(v421));
    svst1_f64(pred_full, (double *)(v728), svreinterpret_f64_f32(v422));
    svst1_f64(pred_full, (double *)(v737), svreinterpret_f64_f32(v423));
    svst1_f64(pred_full, (double *)(v746), svreinterpret_f64_f32(v424));
    svst1_f64(pred_full, (double *)(v755), svreinterpret_f64_f32(v425));
    svst1_f64(pred_full, (double *)(v764), svreinterpret_f64_f32(v426));
    svst1_f64(pred_full, (double *)(v773), svreinterpret_f64_f32(v427));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu14(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v729 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v561 = -1.1666666666666665e+00F;
    float v566 = 7.9015646852540022e-01F;
    float v571 = 5.5854267289647742e-02F;
    float v576 = 7.3430220123575241e-01F;
    float v580 = 4.4095855184409838e-01F;
    float v581 = -4.4095855184409838e-01F;
    float v588 = 3.4087293062393137e-01F;
    float v589 = -3.4087293062393137e-01F;
    float v596 = -5.3396936033772524e-01F;
    float v597 = 5.3396936033772524e-01F;
    float v604 = 8.7484229096165667e-01F;
    float v605 = -8.7484229096165667e-01F;
    float32x2_t v607 = (float32x2_t){v4, v4};
    const float32x2_t *v1416 = &v5[istride];
    float32x2_t *v1508 = &v6[ostride];
    float32x2_t v562 = (float32x2_t){v561, v561};
    float32x2_t v567 = (float32x2_t){v566, v566};
    float32x2_t v572 = (float32x2_t){v571, v571};
    float32x2_t v577 = (float32x2_t){v576, v576};
    float32x2_t v582 = (float32x2_t){v580, v581};
    float32x2_t v590 = (float32x2_t){v588, v589};
    float32x2_t v598 = (float32x2_t){v596, v597};
    float32x2_t v606 = (float32x2_t){v604, v605};
    const float32x2_t *v1471 = &v5[0];
    float32x2_t *v1481 = &v6[0];
    float32x4_t v1618 = vld1q_f32((const float32_t *)v1416);
    float32x4_t v47 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v49 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v97 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v99 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v109 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v111 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v159 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v161 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v171 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v173 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v221 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v223 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v233 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v235 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v283 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v285 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v290 = vtrn1q_f32(v1618, v1618);
    float32x4_t v291 = vtrn2q_f32(v1618, v1618);
    float32x4_t v295 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v297 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v345 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v347 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v357 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v359 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v407 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v409 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v419 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v421 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v563 = vcombine_f32(v562, v562);
    float32x4_t v568 = vcombine_f32(v567, v567);
    float32x4_t v573 = vcombine_f32(v572, v572);
    float32x4_t v578 = vcombine_f32(v577, v577);
    float32x2_t v584 = vmul_f32(v607, v582);
    float32x2_t v592 = vmul_f32(v607, v590);
    float32x2_t v600 = vmul_f32(v607, v598);
    float32x2_t v608 = vmul_f32(v607, v606);
    const float32x2_t *v1329 = &v5[istride * 7];
    const float32x2_t *v1340 = &v5[istride * 2];
    const float32x2_t *v1350 = &v5[istride * 9];
    const float32x2_t *v1362 = &v5[istride * 4];
    const float32x2_t *v1372 = &v5[istride * 11];
    const float32x2_t *v1384 = &v5[istride * 6];
    const float32x2_t *v1394 = &v5[istride * 13];
    const float32x2_t *v1406 = &v5[istride * 8];
    const float32x2_t *v1426 = &v5[istride * 10];
    const float32x2_t *v1436 = &v5[istride * 3];
    const float32x2_t *v1448 = &v5[istride * 12];
    const float32x2_t *v1458 = &v5[istride * 5];
    float32x2_t *v1490 = &v6[ostride * 7];
    float32x2_t *v1499 = &v6[ostride * 8];
    float32x2_t *v1517 = &v6[ostride * 2];
    float32x2_t *v1526 = &v6[ostride * 9];
    float32x2_t *v1535 = &v6[ostride * 10];
    float32x2_t *v1544 = &v6[ostride * 3];
    float32x2_t *v1553 = &v6[ostride * 4];
    float32x2_t *v1562 = &v6[ostride * 11];
    float32x2_t *v1571 = &v6[ostride * 12];
    float32x2_t *v1580 = &v6[ostride * 5];
    float32x2_t *v1589 = &v6[ostride * 6];
    float32x2_t *v1598 = &v6[ostride * 13];
    float32x4_t v1628 = vld1q_f32((const float32_t *)v1471);
    float32x4_t v296 = vmulq_f32(v290, v295);
    float32x4_t v586 = vcombine_f32(v584, v584);
    float32x4_t v594 = vcombine_f32(v592, v592);
    float32x4_t v602 = vcombine_f32(v600, v600);
    float32x4_t v610 = vcombine_f32(v608, v608);
    float32x4_t v1602 = vld1q_f32((const float32_t *)v1329);
    float32x4_t v1604 = vld1q_f32((const float32_t *)v1340);
    float32x4_t v1606 = vld1q_f32((const float32_t *)v1350);
    float32x4_t v1608 = vld1q_f32((const float32_t *)v1362);
    float32x4_t v1610 = vld1q_f32((const float32_t *)v1372);
    float32x4_t v1612 = vld1q_f32((const float32_t *)v1384);
    float32x4_t v1614 = vld1q_f32((const float32_t *)v1394);
    float32x4_t v1616 = vld1q_f32((const float32_t *)v1406);
    float32x4_t v1620 = vld1q_f32((const float32_t *)v1426);
    float32x4_t v1622 = vld1q_f32((const float32_t *)v1436);
    float32x4_t v1624 = vld1q_f32((const float32_t *)v1448);
    float32x4_t v1626 = vld1q_f32((const float32_t *)v1458);
    float32x4_t v42 = vtrn1q_f32(v1602, v1602);
    float32x4_t v43 = vtrn2q_f32(v1602, v1602);
    float32x4_t v92 = vtrn1q_f32(v1604, v1604);
    float32x4_t v93 = vtrn2q_f32(v1604, v1604);
    float32x4_t v104 = vtrn1q_f32(v1606, v1606);
    float32x4_t v105 = vtrn2q_f32(v1606, v1606);
    float32x4_t v154 = vtrn1q_f32(v1608, v1608);
    float32x4_t v155 = vtrn2q_f32(v1608, v1608);
    float32x4_t v166 = vtrn1q_f32(v1610, v1610);
    float32x4_t v167 = vtrn2q_f32(v1610, v1610);
    float32x4_t v216 = vtrn1q_f32(v1612, v1612);
    float32x4_t v217 = vtrn2q_f32(v1612, v1612);
    float32x4_t v228 = vtrn1q_f32(v1614, v1614);
    float32x4_t v229 = vtrn2q_f32(v1614, v1614);
    float32x4_t v278 = vtrn1q_f32(v1616, v1616);
    float32x4_t v279 = vtrn2q_f32(v1616, v1616);
    float32x4_t v299 = vfmaq_f32(v296, v291, v297);
    float32x4_t v340 = vtrn1q_f32(v1620, v1620);
    float32x4_t v341 = vtrn2q_f32(v1620, v1620);
    float32x4_t v352 = vtrn1q_f32(v1622, v1622);
    float32x4_t v353 = vtrn2q_f32(v1622, v1622);
    float32x4_t v402 = vtrn1q_f32(v1624, v1624);
    float32x4_t v403 = vtrn2q_f32(v1624, v1624);
    float32x4_t v414 = vtrn1q_f32(v1626, v1626);
    float32x4_t v415 = vtrn2q_f32(v1626, v1626);
    float32x4_t v48 = vmulq_f32(v42, v47);
    float32x4_t v98 = vmulq_f32(v92, v97);
    float32x4_t v110 = vmulq_f32(v104, v109);
    float32x4_t v160 = vmulq_f32(v154, v159);
    float32x4_t v172 = vmulq_f32(v166, v171);
    float32x4_t v222 = vmulq_f32(v216, v221);
    float32x4_t v234 = vmulq_f32(v228, v233);
    float32x4_t v284 = vmulq_f32(v278, v283);
    float32x4_t v346 = vmulq_f32(v340, v345);
    float32x4_t v358 = vmulq_f32(v352, v357);
    float32x4_t v408 = vmulq_f32(v402, v407);
    float32x4_t v420 = vmulq_f32(v414, v419);
    float32x4_t v51 = vfmaq_f32(v48, v43, v49);
    float32x4_t v101 = vfmaq_f32(v98, v93, v99);
    float32x4_t v113 = vfmaq_f32(v110, v105, v111);
    float32x4_t v163 = vfmaq_f32(v160, v155, v161);
    float32x4_t v175 = vfmaq_f32(v172, v167, v173);
    float32x4_t v225 = vfmaq_f32(v222, v217, v223);
    float32x4_t v237 = vfmaq_f32(v234, v229, v235);
    float32x4_t v287 = vfmaq_f32(v284, v279, v285);
    float32x4_t v349 = vfmaq_f32(v346, v341, v347);
    float32x4_t v361 = vfmaq_f32(v358, v353, v359);
    float32x4_t v411 = vfmaq_f32(v408, v403, v409);
    float32x4_t v423 = vfmaq_f32(v420, v415, v421);
    float32x4_t v431 = vaddq_f32(v1628, v51);
    float32x4_t v432 = vsubq_f32(v1628, v51);
    float32x4_t v433 = vaddq_f32(v101, v113);
    float32x4_t v434 = vsubq_f32(v101, v113);
    float32x4_t v435 = vaddq_f32(v163, v175);
    float32x4_t v436 = vsubq_f32(v163, v175);
    float32x4_t v437 = vaddq_f32(v225, v237);
    float32x4_t v438 = vsubq_f32(v225, v237);
    float32x4_t v439 = vaddq_f32(v287, v299);
    float32x4_t v440 = vsubq_f32(v287, v299);
    float32x4_t v441 = vaddq_f32(v349, v361);
    float32x4_t v442 = vsubq_f32(v349, v361);
    float32x4_t v443 = vaddq_f32(v411, v423);
    float32x4_t v444 = vsubq_f32(v411, v423);
    float32x4_t v445 = vaddq_f32(v433, v443);
    float32x4_t v446 = vsubq_f32(v433, v443);
    float32x4_t v447 = vaddq_f32(v439, v437);
    float32x4_t v448 = vsubq_f32(v439, v437);
    float32x4_t v449 = vaddq_f32(v435, v441);
    float32x4_t v450 = vsubq_f32(v435, v441);
    float32x4_t v538 = vaddq_f32(v434, v444);
    float32x4_t v539 = vsubq_f32(v434, v444);
    float32x4_t v540 = vaddq_f32(v440, v438);
    float32x4_t v541 = vsubq_f32(v440, v438);
    float32x4_t v542 = vaddq_f32(v436, v442);
    float32x4_t v543 = vsubq_f32(v436, v442);
    float32x4_t v451 = vaddq_f32(v445, v447);
    float32x4_t v454 = vsubq_f32(v445, v447);
    float32x4_t v455 = vsubq_f32(v447, v449);
    float32x4_t v456 = vsubq_f32(v449, v445);
    float32x4_t v457 = vaddq_f32(v446, v448);
    float32x4_t v459 = vsubq_f32(v446, v448);
    float32x4_t v460 = vsubq_f32(v448, v450);
    float32x4_t v461 = vsubq_f32(v450, v446);
    float32x4_t v544 = vaddq_f32(v538, v540);
    float32x4_t v547 = vsubq_f32(v538, v540);
    float32x4_t v548 = vsubq_f32(v540, v542);
    float32x4_t v549 = vsubq_f32(v542, v538);
    float32x4_t v550 = vaddq_f32(v539, v541);
    float32x4_t v552 = vsubq_f32(v539, v541);
    float32x4_t v553 = vsubq_f32(v541, v543);
    float32x4_t v554 = vsubq_f32(v543, v539);
    float32x4_t v452 = vaddq_f32(v451, v449);
    float32x4_t v458 = vaddq_f32(v457, v450);
    float32x4_t v476 = vmulq_f32(v454, v568);
    float32x4_t v481 = vmulq_f32(v455, v573);
    float32x4_t v486 = vmulq_f32(v456, v578);
    float32x4_t v500 = vrev64q_f32(v459);
    float32x4_t v508 = vrev64q_f32(v460);
    float32x4_t v516 = vrev64q_f32(v461);
    float32x4_t v545 = vaddq_f32(v544, v542);
    float32x4_t v551 = vaddq_f32(v550, v543);
    float32x4_t v569 = vmulq_f32(v547, v568);
    float32x4_t v574 = vmulq_f32(v548, v573);
    float32x4_t v579 = vmulq_f32(v549, v578);
    float32x4_t v593 = vrev64q_f32(v552);
    float32x4_t v601 = vrev64q_f32(v553);
    float32x4_t v609 = vrev64q_f32(v554);
    float32x4_t v453 = vaddq_f32(v452, v431);
    float32x4_t v471 = vmulq_f32(v452, v563);
    float32x4_t v492 = vrev64q_f32(v458);
    float32x4_t v502 = vmulq_f32(v500, v594);
    float32x4_t v510 = vmulq_f32(v508, v602);
    float32x4_t v518 = vmulq_f32(v516, v610);
    float32x4_t v546 = vaddq_f32(v545, v432);
    float32x4_t v564 = vmulq_f32(v545, v563);
    float32x4_t v585 = vrev64q_f32(v551);
    float32x4_t v595 = vmulq_f32(v593, v594);
    float32x4_t v603 = vmulq_f32(v601, v602);
    float32x4_t v611 = vmulq_f32(v609, v610);
    float32x4_t v494 = vmulq_f32(v492, v586);
    float32x4_t v519 = vaddq_f32(v453, v471);
    float32x4_t v587 = vmulq_f32(v585, v586);
    float32x4_t v612 = vaddq_f32(v546, v564);
    vst1q_f32((float32_t *)v1481, v453);
    vst1q_f32((float32_t *)v1490, v546);
    float32x4_t v520 = vaddq_f32(v519, v476);
    float32x4_t v522 = vsubq_f32(v519, v476);
    float32x4_t v524 = vsubq_f32(v519, v481);
    float32x4_t v526 = vaddq_f32(v494, v502);
    float32x4_t v528 = vsubq_f32(v494, v502);
    float32x4_t v530 = vsubq_f32(v494, v510);
    float32x4_t v613 = vaddq_f32(v612, v569);
    float32x4_t v615 = vsubq_f32(v612, v569);
    float32x4_t v617 = vsubq_f32(v612, v574);
    float32x4_t v619 = vaddq_f32(v587, v595);
    float32x4_t v621 = vsubq_f32(v587, v595);
    float32x4_t v623 = vsubq_f32(v587, v603);
    float32x4_t v521 = vaddq_f32(v520, v481);
    float32x4_t v523 = vsubq_f32(v522, v486);
    float32x4_t v525 = vaddq_f32(v524, v486);
    float32x4_t v527 = vaddq_f32(v526, v510);
    float32x4_t v529 = vsubq_f32(v528, v518);
    float32x4_t v531 = vaddq_f32(v530, v518);
    float32x4_t v614 = vaddq_f32(v613, v574);
    float32x4_t v616 = vsubq_f32(v615, v579);
    float32x4_t v618 = vaddq_f32(v617, v579);
    float32x4_t v620 = vaddq_f32(v619, v603);
    float32x4_t v622 = vsubq_f32(v621, v611);
    float32x4_t v624 = vaddq_f32(v623, v611);
    float32x4_t v532 = vaddq_f32(v521, v527);
    float32x4_t v533 = vsubq_f32(v521, v527);
    float32x4_t v534 = vaddq_f32(v523, v529);
    float32x4_t v535 = vsubq_f32(v523, v529);
    float32x4_t v536 = vaddq_f32(v525, v531);
    float32x4_t v537 = vsubq_f32(v525, v531);
    float32x4_t v625 = vaddq_f32(v614, v620);
    float32x4_t v626 = vsubq_f32(v614, v620);
    float32x4_t v627 = vaddq_f32(v616, v622);
    float32x4_t v628 = vsubq_f32(v616, v622);
    float32x4_t v629 = vaddq_f32(v618, v624);
    float32x4_t v630 = vsubq_f32(v618, v624);
    vst1q_f32((float32_t *)v1499, v533);
    vst1q_f32((float32_t *)v1508, v626);
    vst1q_f32((float32_t *)v1517, v535);
    vst1q_f32((float32_t *)v1526, v628);
    vst1q_f32((float32_t *)v1535, v536);
    vst1q_f32((float32_t *)v1544, v629);
    vst1q_f32((float32_t *)v1553, v537);
    vst1q_f32((float32_t *)v1562, v630);
    vst1q_f32((float32_t *)v1571, v534);
    vst1q_f32((float32_t *)v1580, v627);
    vst1q_f32((float32_t *)v1589, v532);
    vst1q_f32((float32_t *)v1598, v625);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v729 * 2; j < howmany; j += 1) {
    float32x2_t v931 = v5[istride];
    float v1187 = -1.1666666666666665e+00F;
    float v1191 = 7.9015646852540022e-01F;
    float v1195 = 5.5854267289647742e-02F;
    float v1199 = 7.3430220123575241e-01F;
    float v1202 = 4.4095855184409838e-01F;
    float v1203 = -4.4095855184409838e-01F;
    float v1209 = 3.4087293062393137e-01F;
    float v1210 = -3.4087293062393137e-01F;
    float v1216 = -5.3396936033772524e-01F;
    float v1217 = 5.3396936033772524e-01F;
    float v1223 = 8.7484229096165667e-01F;
    float v1224 = -8.7484229096165667e-01F;
    float32x2_t v1226 = (float32x2_t){v4, v4};
    float32x2_t v753 = v7[12];
    float32x2_t v758 = v7[13];
    float32x2_t v793 = v7[2];
    float32x2_t v798 = v7[3];
    float32x2_t v803 = v7[16];
    float32x2_t v808 = v7[17];
    float32x2_t v843 = v7[6];
    float32x2_t v848 = v7[7];
    float32x2_t v853 = v7[20];
    float32x2_t v858 = v7[21];
    float32x2_t v893 = v7[10];
    float32x2_t v898 = v7[11];
    float32x2_t v903 = v7[24];
    float32x2_t v908 = v7[25];
    float32x2_t v943 = v7[14];
    float32x2_t v948 = v7[15];
    float32x2_t v953 = v7[0];
    float32x2_t v954 = vtrn1_f32(v931, v931);
    float32x2_t v955 = vtrn2_f32(v931, v931);
    float32x2_t v958 = v7[1];
    float32x2_t v993 = v7[18];
    float32x2_t v998 = v7[19];
    float32x2_t v1003 = v7[4];
    float32x2_t v1008 = v7[5];
    float32x2_t v1043 = v7[22];
    float32x2_t v1048 = v7[23];
    float32x2_t v1053 = v7[8];
    float32x2_t v1058 = v7[9];
    float32x2_t v1066 = v5[0];
    float32x2_t v1188 = (float32x2_t){v1187, v1187};
    float32x2_t v1192 = (float32x2_t){v1191, v1191};
    float32x2_t v1196 = (float32x2_t){v1195, v1195};
    float32x2_t v1200 = (float32x2_t){v1199, v1199};
    float32x2_t v1204 = (float32x2_t){v1202, v1203};
    float32x2_t v1211 = (float32x2_t){v1209, v1210};
    float32x2_t v1218 = (float32x2_t){v1216, v1217};
    float32x2_t v1225 = (float32x2_t){v1223, v1224};
    float32x2_t v741 = v5[istride * 7];
    float32x2_t v766 = v5[istride * 2];
    float32x2_t v781 = v5[istride * 9];
    float32x2_t v816 = v5[istride * 4];
    float32x2_t v831 = v5[istride * 11];
    float32x2_t v866 = v5[istride * 6];
    float32x2_t v881 = v5[istride * 13];
    float32x2_t v916 = v5[istride * 8];
    float32x2_t v959 = vmul_f32(v954, v953);
    float32x2_t v966 = v5[istride * 10];
    float32x2_t v981 = v5[istride * 3];
    float32x2_t v1016 = v5[istride * 12];
    float32x2_t v1031 = v5[istride * 5];
    float32x2_t v1206 = vmul_f32(v1226, v1204);
    float32x2_t v1213 = vmul_f32(v1226, v1211);
    float32x2_t v1220 = vmul_f32(v1226, v1218);
    float32x2_t v1227 = vmul_f32(v1226, v1225);
    float32x2_t v754 = vtrn1_f32(v741, v741);
    float32x2_t v755 = vtrn2_f32(v741, v741);
    float32x2_t v794 = vtrn1_f32(v766, v766);
    float32x2_t v795 = vtrn2_f32(v766, v766);
    float32x2_t v804 = vtrn1_f32(v781, v781);
    float32x2_t v805 = vtrn2_f32(v781, v781);
    float32x2_t v844 = vtrn1_f32(v816, v816);
    float32x2_t v845 = vtrn2_f32(v816, v816);
    float32x2_t v854 = vtrn1_f32(v831, v831);
    float32x2_t v855 = vtrn2_f32(v831, v831);
    float32x2_t v894 = vtrn1_f32(v866, v866);
    float32x2_t v895 = vtrn2_f32(v866, v866);
    float32x2_t v904 = vtrn1_f32(v881, v881);
    float32x2_t v905 = vtrn2_f32(v881, v881);
    float32x2_t v944 = vtrn1_f32(v916, v916);
    float32x2_t v945 = vtrn2_f32(v916, v916);
    float32x2_t v961 = vfma_f32(v959, v955, v958);
    float32x2_t v994 = vtrn1_f32(v966, v966);
    float32x2_t v995 = vtrn2_f32(v966, v966);
    float32x2_t v1004 = vtrn1_f32(v981, v981);
    float32x2_t v1005 = vtrn2_f32(v981, v981);
    float32x2_t v1044 = vtrn1_f32(v1016, v1016);
    float32x2_t v1045 = vtrn2_f32(v1016, v1016);
    float32x2_t v1054 = vtrn1_f32(v1031, v1031);
    float32x2_t v1055 = vtrn2_f32(v1031, v1031);
    float32x2_t v759 = vmul_f32(v754, v753);
    float32x2_t v799 = vmul_f32(v794, v793);
    float32x2_t v809 = vmul_f32(v804, v803);
    float32x2_t v849 = vmul_f32(v844, v843);
    float32x2_t v859 = vmul_f32(v854, v853);
    float32x2_t v899 = vmul_f32(v894, v893);
    float32x2_t v909 = vmul_f32(v904, v903);
    float32x2_t v949 = vmul_f32(v944, v943);
    float32x2_t v999 = vmul_f32(v994, v993);
    float32x2_t v1009 = vmul_f32(v1004, v1003);
    float32x2_t v1049 = vmul_f32(v1044, v1043);
    float32x2_t v1059 = vmul_f32(v1054, v1053);
    float32x2_t v761 = vfma_f32(v759, v755, v758);
    float32x2_t v801 = vfma_f32(v799, v795, v798);
    float32x2_t v811 = vfma_f32(v809, v805, v808);
    float32x2_t v851 = vfma_f32(v849, v845, v848);
    float32x2_t v861 = vfma_f32(v859, v855, v858);
    float32x2_t v901 = vfma_f32(v899, v895, v898);
    float32x2_t v911 = vfma_f32(v909, v905, v908);
    float32x2_t v951 = vfma_f32(v949, v945, v948);
    float32x2_t v1001 = vfma_f32(v999, v995, v998);
    float32x2_t v1011 = vfma_f32(v1009, v1005, v1008);
    float32x2_t v1051 = vfma_f32(v1049, v1045, v1048);
    float32x2_t v1061 = vfma_f32(v1059, v1055, v1058);
    float32x2_t v1067 = vadd_f32(v1066, v761);
    float32x2_t v1068 = vsub_f32(v1066, v761);
    float32x2_t v1069 = vadd_f32(v801, v811);
    float32x2_t v1070 = vsub_f32(v801, v811);
    float32x2_t v1071 = vadd_f32(v851, v861);
    float32x2_t v1072 = vsub_f32(v851, v861);
    float32x2_t v1073 = vadd_f32(v901, v911);
    float32x2_t v1074 = vsub_f32(v901, v911);
    float32x2_t v1075 = vadd_f32(v951, v961);
    float32x2_t v1076 = vsub_f32(v951, v961);
    float32x2_t v1077 = vadd_f32(v1001, v1011);
    float32x2_t v1078 = vsub_f32(v1001, v1011);
    float32x2_t v1079 = vadd_f32(v1051, v1061);
    float32x2_t v1080 = vsub_f32(v1051, v1061);
    float32x2_t v1081 = vadd_f32(v1069, v1079);
    float32x2_t v1082 = vsub_f32(v1069, v1079);
    float32x2_t v1083 = vadd_f32(v1075, v1073);
    float32x2_t v1084 = vsub_f32(v1075, v1073);
    float32x2_t v1085 = vadd_f32(v1071, v1077);
    float32x2_t v1086 = vsub_f32(v1071, v1077);
    float32x2_t v1165 = vadd_f32(v1070, v1080);
    float32x2_t v1166 = vsub_f32(v1070, v1080);
    float32x2_t v1167 = vadd_f32(v1076, v1074);
    float32x2_t v1168 = vsub_f32(v1076, v1074);
    float32x2_t v1169 = vadd_f32(v1072, v1078);
    float32x2_t v1170 = vsub_f32(v1072, v1078);
    float32x2_t v1087 = vadd_f32(v1081, v1083);
    float32x2_t v1090 = vsub_f32(v1081, v1083);
    float32x2_t v1091 = vsub_f32(v1083, v1085);
    float32x2_t v1092 = vsub_f32(v1085, v1081);
    float32x2_t v1093 = vadd_f32(v1082, v1084);
    float32x2_t v1095 = vsub_f32(v1082, v1084);
    float32x2_t v1096 = vsub_f32(v1084, v1086);
    float32x2_t v1097 = vsub_f32(v1086, v1082);
    float32x2_t v1171 = vadd_f32(v1165, v1167);
    float32x2_t v1174 = vsub_f32(v1165, v1167);
    float32x2_t v1175 = vsub_f32(v1167, v1169);
    float32x2_t v1176 = vsub_f32(v1169, v1165);
    float32x2_t v1177 = vadd_f32(v1166, v1168);
    float32x2_t v1179 = vsub_f32(v1166, v1168);
    float32x2_t v1180 = vsub_f32(v1168, v1170);
    float32x2_t v1181 = vsub_f32(v1170, v1166);
    float32x2_t v1088 = vadd_f32(v1087, v1085);
    float32x2_t v1094 = vadd_f32(v1093, v1086);
    float32x2_t v1109 = vmul_f32(v1090, v1192);
    float32x2_t v1113 = vmul_f32(v1091, v1196);
    float32x2_t v1117 = vmul_f32(v1092, v1200);
    float32x2_t v1130 = vrev64_f32(v1095);
    float32x2_t v1137 = vrev64_f32(v1096);
    float32x2_t v1144 = vrev64_f32(v1097);
    float32x2_t v1172 = vadd_f32(v1171, v1169);
    float32x2_t v1178 = vadd_f32(v1177, v1170);
    float32x2_t v1193 = vmul_f32(v1174, v1192);
    float32x2_t v1197 = vmul_f32(v1175, v1196);
    float32x2_t v1201 = vmul_f32(v1176, v1200);
    float32x2_t v1214 = vrev64_f32(v1179);
    float32x2_t v1221 = vrev64_f32(v1180);
    float32x2_t v1228 = vrev64_f32(v1181);
    float32x2_t v1089 = vadd_f32(v1088, v1067);
    float32x2_t v1105 = vmul_f32(v1088, v1188);
    float32x2_t v1123 = vrev64_f32(v1094);
    float32x2_t v1131 = vmul_f32(v1130, v1213);
    float32x2_t v1138 = vmul_f32(v1137, v1220);
    float32x2_t v1145 = vmul_f32(v1144, v1227);
    float32x2_t v1173 = vadd_f32(v1172, v1068);
    float32x2_t v1189 = vmul_f32(v1172, v1188);
    float32x2_t v1207 = vrev64_f32(v1178);
    float32x2_t v1215 = vmul_f32(v1214, v1213);
    float32x2_t v1222 = vmul_f32(v1221, v1220);
    float32x2_t v1229 = vmul_f32(v1228, v1227);
    float32x2_t v1124 = vmul_f32(v1123, v1206);
    float32x2_t v1146 = vadd_f32(v1089, v1105);
    float32x2_t v1208 = vmul_f32(v1207, v1206);
    float32x2_t v1230 = vadd_f32(v1173, v1189);
    v6[0] = v1089;
    v6[ostride * 7] = v1173;
    float32x2_t v1147 = vadd_f32(v1146, v1109);
    float32x2_t v1149 = vsub_f32(v1146, v1109);
    float32x2_t v1151 = vsub_f32(v1146, v1113);
    float32x2_t v1153 = vadd_f32(v1124, v1131);
    float32x2_t v1155 = vsub_f32(v1124, v1131);
    float32x2_t v1157 = vsub_f32(v1124, v1138);
    float32x2_t v1231 = vadd_f32(v1230, v1193);
    float32x2_t v1233 = vsub_f32(v1230, v1193);
    float32x2_t v1235 = vsub_f32(v1230, v1197);
    float32x2_t v1237 = vadd_f32(v1208, v1215);
    float32x2_t v1239 = vsub_f32(v1208, v1215);
    float32x2_t v1241 = vsub_f32(v1208, v1222);
    float32x2_t v1148 = vadd_f32(v1147, v1113);
    float32x2_t v1150 = vsub_f32(v1149, v1117);
    float32x2_t v1152 = vadd_f32(v1151, v1117);
    float32x2_t v1154 = vadd_f32(v1153, v1138);
    float32x2_t v1156 = vsub_f32(v1155, v1145);
    float32x2_t v1158 = vadd_f32(v1157, v1145);
    float32x2_t v1232 = vadd_f32(v1231, v1197);
    float32x2_t v1234 = vsub_f32(v1233, v1201);
    float32x2_t v1236 = vadd_f32(v1235, v1201);
    float32x2_t v1238 = vadd_f32(v1237, v1222);
    float32x2_t v1240 = vsub_f32(v1239, v1229);
    float32x2_t v1242 = vadd_f32(v1241, v1229);
    float32x2_t v1159 = vadd_f32(v1148, v1154);
    float32x2_t v1160 = vsub_f32(v1148, v1154);
    float32x2_t v1161 = vadd_f32(v1150, v1156);
    float32x2_t v1162 = vsub_f32(v1150, v1156);
    float32x2_t v1163 = vadd_f32(v1152, v1158);
    float32x2_t v1164 = vsub_f32(v1152, v1158);
    float32x2_t v1243 = vadd_f32(v1232, v1238);
    float32x2_t v1244 = vsub_f32(v1232, v1238);
    float32x2_t v1245 = vadd_f32(v1234, v1240);
    float32x2_t v1246 = vsub_f32(v1234, v1240);
    float32x2_t v1247 = vadd_f32(v1236, v1242);
    float32x2_t v1248 = vsub_f32(v1236, v1242);
    v6[ostride * 8] = v1160;
    v6[ostride] = v1244;
    v6[ostride * 2] = v1162;
    v6[ostride * 9] = v1246;
    v6[ostride * 10] = v1163;
    v6[ostride * 3] = v1247;
    v6[ostride * 4] = v1164;
    v6[ostride * 11] = v1248;
    v6[ostride * 12] = v1161;
    v6[ostride * 5] = v1245;
    v6[ostride * 6] = v1159;
    v6[ostride * 13] = v1243;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu14(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v346 = -1.1666666666666665e+00F;
    float v351 = 7.9015646852540022e-01F;
    float v356 = 5.5854267289647742e-02F;
    float v361 = 7.3430220123575241e-01F;
    float v366 = -4.4095855184409838e-01F;
    float v373 = -3.4087293062393137e-01F;
    float v380 = 5.3396936033772524e-01F;
    float v387 = -8.7484229096165667e-01F;
    const float32x2_t *v588 = &v5[v0];
    float32x2_t *v689 = &v6[v2];
    int64_t v19 = v0 * 7;
    int64_t v34 = v0 * 2;
    int64_t v45 = v0 * 9;
    int64_t v64 = v0 * 4;
    int64_t v75 = v0 * 11;
    int64_t v94 = v0 * 6;
    int64_t v105 = v0 * 13;
    int64_t v124 = v0 * 8;
    int64_t v154 = v0 * 10;
    int64_t v165 = v0 * 3;
    int64_t v184 = v0 * 12;
    int64_t v195 = v0 * 5;
    float v369 = v4 * v366;
    float v376 = v4 * v373;
    float v383 = v4 * v380;
    float v390 = v4 * v387;
    int64_t v420 = v2 * 7;
    int64_t v427 = v2 * 8;
    int64_t v441 = v2 * 2;
    int64_t v448 = v2 * 9;
    int64_t v455 = v2 * 10;
    int64_t v462 = v2 * 3;
    int64_t v469 = v2 * 4;
    int64_t v476 = v2 * 11;
    int64_t v483 = v2 * 12;
    int64_t v490 = v2 * 5;
    int64_t v497 = v2 * 6;
    int64_t v504 = v2 * 13;
    const float32x2_t *v634 = &v5[0];
    svfloat32_t v647 = svdup_n_f32(v346);
    svfloat32_t v648 = svdup_n_f32(v351);
    svfloat32_t v649 = svdup_n_f32(v356);
    svfloat32_t v650 = svdup_n_f32(v361);
    float32x2_t *v662 = &v6[0];
    svfloat32_t v799 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v588)[0]));
    svfloat32_t v31 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v57 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v61 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v87 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v91 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v117 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v121 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v147 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v151 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v177 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v181 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v207 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v211 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    const float32x2_t *v516 = &v5[v19];
    const float32x2_t *v525 = &v5[v34];
    const float32x2_t *v534 = &v5[v45];
    const float32x2_t *v543 = &v5[v64];
    const float32x2_t *v552 = &v5[v75];
    const float32x2_t *v561 = &v5[v94];
    const float32x2_t *v570 = &v5[v105];
    const float32x2_t *v579 = &v5[v124];
    const float32x2_t *v597 = &v5[v154];
    const float32x2_t *v606 = &v5[v165];
    const float32x2_t *v615 = &v5[v184];
    const float32x2_t *v624 = &v5[v195];
    svfloat32_t v651 = svdup_n_f32(v369);
    svfloat32_t v652 = svdup_n_f32(v376);
    svfloat32_t v653 = svdup_n_f32(v383);
    svfloat32_t v654 = svdup_n_f32(v390);
    float32x2_t *v671 = &v6[v420];
    float32x2_t *v680 = &v6[v427];
    float32x2_t *v698 = &v6[v441];
    float32x2_t *v707 = &v6[v448];
    float32x2_t *v716 = &v6[v455];
    float32x2_t *v725 = &v6[v462];
    float32x2_t *v734 = &v6[v469];
    float32x2_t *v743 = &v6[v476];
    float32x2_t *v752 = &v6[v483];
    float32x2_t *v761 = &v6[v490];
    float32x2_t *v770 = &v6[v497];
    float32x2_t *v779 = &v6[v504];
    svfloat32_t v809 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v634)[0]));
    svfloat32_t zero152 = svdup_n_f32(0);
    svfloat32_t v152 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero152, v799, v151, 0),
                     v799, v151, 90);
    svfloat32_t v783 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v516)[0]));
    svfloat32_t v785 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v525)[0]));
    svfloat32_t v787 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v534)[0]));
    svfloat32_t v789 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v543)[0]));
    svfloat32_t v791 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v552)[0]));
    svfloat32_t v793 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v561)[0]));
    svfloat32_t v795 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v570)[0]));
    svfloat32_t v797 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v579)[0]));
    svfloat32_t v801 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v597)[0]));
    svfloat32_t v803 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v606)[0]));
    svfloat32_t v805 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v615)[0]));
    svfloat32_t v807 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v624)[0]));
    svfloat32_t zero32 = svdup_n_f32(0);
    svfloat32_t v32 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero32, v783, v31, 0),
                     v783, v31, 90);
    svfloat32_t zero58 = svdup_n_f32(0);
    svfloat32_t v58 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero58, v785, v57, 0),
                     v785, v57, 90);
    svfloat32_t zero62 = svdup_n_f32(0);
    svfloat32_t v62 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero62, v787, v61, 0),
                     v787, v61, 90);
    svfloat32_t zero88 = svdup_n_f32(0);
    svfloat32_t v88 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero88, v789, v87, 0),
                     v789, v87, 90);
    svfloat32_t zero92 = svdup_n_f32(0);
    svfloat32_t v92 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero92, v791, v91, 0),
                     v791, v91, 90);
    svfloat32_t zero118 = svdup_n_f32(0);
    svfloat32_t v118 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero118, v793, v117, 0),
                     v793, v117, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v795, v121, 0),
                     v795, v121, 90);
    svfloat32_t zero148 = svdup_n_f32(0);
    svfloat32_t v148 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero148, v797, v147, 0),
                     v797, v147, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero178, v801, v177, 0),
                     v801, v177, 90);
    svfloat32_t zero182 = svdup_n_f32(0);
    svfloat32_t v182 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero182, v803, v181, 0),
                     v803, v181, 90);
    svfloat32_t zero208 = svdup_n_f32(0);
    svfloat32_t v208 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero208, v805, v207, 0),
                     v805, v207, 90);
    svfloat32_t zero212 = svdup_n_f32(0);
    svfloat32_t v212 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero212, v807, v211, 0),
                     v807, v211, 90);
    svfloat32_t v220 = svadd_f32_x(svptrue_b32(), v809, v32);
    svfloat32_t v221 = svsub_f32_x(svptrue_b32(), v809, v32);
    svfloat32_t v222 = svadd_f32_x(svptrue_b32(), v58, v62);
    svfloat32_t v223 = svsub_f32_x(svptrue_b32(), v58, v62);
    svfloat32_t v224 = svadd_f32_x(svptrue_b32(), v88, v92);
    svfloat32_t v225 = svsub_f32_x(svptrue_b32(), v88, v92);
    svfloat32_t v226 = svadd_f32_x(svptrue_b32(), v118, v122);
    svfloat32_t v227 = svsub_f32_x(svptrue_b32(), v118, v122);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v148, v152);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v148, v152);
    svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v178, v182);
    svfloat32_t v231 = svsub_f32_x(svptrue_b32(), v178, v182);
    svfloat32_t v232 = svadd_f32_x(svptrue_b32(), v208, v212);
    svfloat32_t v233 = svsub_f32_x(svptrue_b32(), v208, v212);
    svfloat32_t v234 = svadd_f32_x(svptrue_b32(), v222, v232);
    svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v222, v232);
    svfloat32_t v236 = svadd_f32_x(svptrue_b32(), v228, v226);
    svfloat32_t v237 = svsub_f32_x(svptrue_b32(), v228, v226);
    svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v224, v230);
    svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v224, v230);
    svfloat32_t v323 = svadd_f32_x(svptrue_b32(), v223, v233);
    svfloat32_t v324 = svsub_f32_x(svptrue_b32(), v223, v233);
    svfloat32_t v325 = svadd_f32_x(svptrue_b32(), v229, v227);
    svfloat32_t v326 = svsub_f32_x(svptrue_b32(), v229, v227);
    svfloat32_t v327 = svadd_f32_x(svptrue_b32(), v225, v231);
    svfloat32_t v328 = svsub_f32_x(svptrue_b32(), v225, v231);
    svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v234, v236);
    svfloat32_t v243 = svsub_f32_x(svptrue_b32(), v234, v236);
    svfloat32_t v244 = svsub_f32_x(svptrue_b32(), v236, v238);
    svfloat32_t v245 = svsub_f32_x(svptrue_b32(), v238, v234);
    svfloat32_t v246 = svadd_f32_x(svptrue_b32(), v235, v237);
    svfloat32_t v248 = svsub_f32_x(svptrue_b32(), v235, v237);
    svfloat32_t v249 = svsub_f32_x(svptrue_b32(), v237, v239);
    svfloat32_t v250 = svsub_f32_x(svptrue_b32(), v239, v235);
    svfloat32_t v329 = svadd_f32_x(svptrue_b32(), v323, v325);
    svfloat32_t v332 = svsub_f32_x(svptrue_b32(), v323, v325);
    svfloat32_t v333 = svsub_f32_x(svptrue_b32(), v325, v327);
    svfloat32_t v334 = svsub_f32_x(svptrue_b32(), v327, v323);
    svfloat32_t v335 = svadd_f32_x(svptrue_b32(), v324, v326);
    svfloat32_t v337 = svsub_f32_x(svptrue_b32(), v324, v326);
    svfloat32_t v338 = svsub_f32_x(svptrue_b32(), v326, v328);
    svfloat32_t v339 = svsub_f32_x(svptrue_b32(), v328, v324);
    svfloat32_t v241 = svadd_f32_x(svptrue_b32(), v240, v238);
    svfloat32_t v247 = svadd_f32_x(svptrue_b32(), v246, v239);
    svfloat32_t zero289 = svdup_n_f32(0);
    svfloat32_t v289 = svcmla_f32_x(pred_full, zero289, v652, v248, 90);
    svfloat32_t zero296 = svdup_n_f32(0);
    svfloat32_t v296 = svcmla_f32_x(pred_full, zero296, v653, v249, 90);
    svfloat32_t zero303 = svdup_n_f32(0);
    svfloat32_t v303 = svcmla_f32_x(pred_full, zero303, v654, v250, 90);
    svfloat32_t v330 = svadd_f32_x(svptrue_b32(), v329, v327);
    svfloat32_t v336 = svadd_f32_x(svptrue_b32(), v335, v328);
    svfloat32_t zero378 = svdup_n_f32(0);
    svfloat32_t v378 = svcmla_f32_x(pred_full, zero378, v652, v337, 90);
    svfloat32_t zero385 = svdup_n_f32(0);
    svfloat32_t v385 = svcmla_f32_x(pred_full, zero385, v653, v338, 90);
    svfloat32_t zero392 = svdup_n_f32(0);
    svfloat32_t v392 = svcmla_f32_x(pred_full, zero392, v654, v339, 90);
    svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v241, v220);
    svfloat32_t zero282 = svdup_n_f32(0);
    svfloat32_t v282 = svcmla_f32_x(pred_full, zero282, v651, v247, 90);
    svfloat32_t v331 = svadd_f32_x(svptrue_b32(), v330, v221);
    svfloat32_t zero371 = svdup_n_f32(0);
    svfloat32_t v371 = svcmla_f32_x(pred_full, zero371, v651, v336, 90);
    svfloat32_t v304 = svmla_f32_x(pred_full, v242, v241, v647);
    svfloat32_t v311 = svadd_f32_x(svptrue_b32(), v282, v289);
    svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v282, v289);
    svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v282, v296);
    svfloat32_t v393 = svmla_f32_x(pred_full, v331, v330, v647);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v371, v378);
    svfloat32_t v402 = svsub_f32_x(svptrue_b32(), v371, v378);
    svfloat32_t v404 = svsub_f32_x(svptrue_b32(), v371, v385);
    svst1_f64(pred_full, (double *)(v662), svreinterpret_f64_f32(v242));
    svst1_f64(pred_full, (double *)(v671), svreinterpret_f64_f32(v331));
    svfloat32_t v305 = svmla_f32_x(pred_full, v304, v243, v648);
    svfloat32_t v307 = svmls_f32_x(pred_full, v304, v243, v648);
    svfloat32_t v309 = svmls_f32_x(pred_full, v304, v244, v649);
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v311, v296);
    svfloat32_t v314 = svsub_f32_x(svptrue_b32(), v313, v303);
    svfloat32_t v316 = svadd_f32_x(svptrue_b32(), v315, v303);
    svfloat32_t v394 = svmla_f32_x(pred_full, v393, v332, v648);
    svfloat32_t v396 = svmls_f32_x(pred_full, v393, v332, v648);
    svfloat32_t v398 = svmls_f32_x(pred_full, v393, v333, v649);
    svfloat32_t v401 = svadd_f32_x(svptrue_b32(), v400, v385);
    svfloat32_t v403 = svsub_f32_x(svptrue_b32(), v402, v392);
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v404, v392);
    svfloat32_t v306 = svmla_f32_x(pred_full, v305, v244, v649);
    svfloat32_t v308 = svmls_f32_x(pred_full, v307, v245, v650);
    svfloat32_t v310 = svmla_f32_x(pred_full, v309, v245, v650);
    svfloat32_t v395 = svmla_f32_x(pred_full, v394, v333, v649);
    svfloat32_t v397 = svmls_f32_x(pred_full, v396, v334, v650);
    svfloat32_t v399 = svmla_f32_x(pred_full, v398, v334, v650);
    svfloat32_t v317 = svadd_f32_x(svptrue_b32(), v306, v312);
    svfloat32_t v318 = svsub_f32_x(svptrue_b32(), v306, v312);
    svfloat32_t v319 = svadd_f32_x(svptrue_b32(), v308, v314);
    svfloat32_t v320 = svsub_f32_x(svptrue_b32(), v308, v314);
    svfloat32_t v321 = svadd_f32_x(svptrue_b32(), v310, v316);
    svfloat32_t v322 = svsub_f32_x(svptrue_b32(), v310, v316);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v395, v401);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v395, v401);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v397, v403);
    svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v397, v403);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v399, v405);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v399, v405);
    svst1_f64(pred_full, (double *)(v680), svreinterpret_f64_f32(v318));
    svst1_f64(pred_full, (double *)(v689), svreinterpret_f64_f32(v407));
    svst1_f64(pred_full, (double *)(v698), svreinterpret_f64_f32(v320));
    svst1_f64(pred_full, (double *)(v707), svreinterpret_f64_f32(v409));
    svst1_f64(pred_full, (double *)(v716), svreinterpret_f64_f32(v321));
    svst1_f64(pred_full, (double *)(v725), svreinterpret_f64_f32(v410));
    svst1_f64(pred_full, (double *)(v734), svreinterpret_f64_f32(v322));
    svst1_f64(pred_full, (double *)(v743), svreinterpret_f64_f32(v411));
    svst1_f64(pred_full, (double *)(v752), svreinterpret_f64_f32(v319));
    svst1_f64(pred_full, (double *)(v761), svreinterpret_f64_f32(v408));
    svst1_f64(pred_full, (double *)(v770), svreinterpret_f64_f32(v317));
    svst1_f64(pred_full, (double *)(v779), svreinterpret_f64_f32(v406));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu15(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v717 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v443 = -1.2500000000000000e+00F;
    float v448 = 5.5901699437494745e-01F;
    float v452 = 1.5388417685876268e+00F;
    float v453 = -1.5388417685876268e+00F;
    float v460 = 5.8778525229247325e-01F;
    float v461 = -5.8778525229247325e-01F;
    float v468 = 3.6327126400268028e-01F;
    float v469 = -3.6327126400268028e-01F;
    float v494 = -1.4999999999999998e+00F;
    float v499 = 1.8749999999999998e+00F;
    float v504 = -8.3852549156242107e-01F;
    float v508 = -2.3082626528814396e+00F;
    float v509 = 2.3082626528814396e+00F;
    float v516 = -8.8167787843870971e-01F;
    float v517 = 8.8167787843870971e-01F;
    float v524 = -5.4490689600402031e-01F;
    float v525 = 5.4490689600402031e-01F;
    float v549 = 8.6602540378443871e-01F;
    float v550 = -8.6602540378443871e-01F;
    float v557 = -1.0825317547305484e+00F;
    float v558 = 1.0825317547305484e+00F;
    float v565 = 4.8412291827592718e-01F;
    float v566 = -4.8412291827592718e-01F;
    float32x2_t v568 = (float32x2_t){v4, v4};
    float v574 = -1.3326760640014592e+00F;
    float v579 = -5.0903696045512736e-01F;
    float v584 = -3.1460214309120460e-01F;
    const float32x2_t *v1369 = &v5[istride];
    float32x2_t *v1500 = &v6[ostride];
    float32x2_t v444 = (float32x2_t){v443, v443};
    float32x2_t v449 = (float32x2_t){v448, v448};
    float32x2_t v454 = (float32x2_t){v452, v453};
    float32x2_t v462 = (float32x2_t){v460, v461};
    float32x2_t v470 = (float32x2_t){v468, v469};
    float32x2_t v495 = (float32x2_t){v494, v494};
    float32x2_t v500 = (float32x2_t){v499, v499};
    float32x2_t v505 = (float32x2_t){v504, v504};
    float32x2_t v510 = (float32x2_t){v508, v509};
    float32x2_t v518 = (float32x2_t){v516, v517};
    float32x2_t v526 = (float32x2_t){v524, v525};
    float32x2_t v551 = (float32x2_t){v549, v550};
    float32x2_t v559 = (float32x2_t){v557, v558};
    float32x2_t v567 = (float32x2_t){v565, v566};
    float32x2_t v575 = (float32x2_t){v574, v574};
    float32x2_t v580 = (float32x2_t){v579, v579};
    float32x2_t v585 = (float32x2_t){v584, v584};
    const float32x2_t *v1454 = &v5[0];
    float32x2_t *v1464 = &v6[0];
    float32x4_t v1606 = vld1q_f32((const float32_t *)v1369);
    float32x4_t v66 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v68 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v78 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v80 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v128 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v130 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v140 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v142 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v159 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v161 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v209 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v211 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v216 = vtrn1q_f32(v1606, v1606);
    float32x4_t v217 = vtrn2q_f32(v1606, v1606);
    float32x4_t v221 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v223 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v240 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v242 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v290 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v292 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v302 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v304 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v321 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v323 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v371 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v373 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v383 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v385 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v402 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v404 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v445 = vcombine_f32(v444, v444);
    float32x4_t v450 = vcombine_f32(v449, v449);
    float32x2_t v456 = vmul_f32(v568, v454);
    float32x2_t v464 = vmul_f32(v568, v462);
    float32x2_t v472 = vmul_f32(v568, v470);
    float32x4_t v496 = vcombine_f32(v495, v495);
    float32x4_t v501 = vcombine_f32(v500, v500);
    float32x4_t v506 = vcombine_f32(v505, v505);
    float32x2_t v512 = vmul_f32(v568, v510);
    float32x2_t v520 = vmul_f32(v568, v518);
    float32x2_t v528 = vmul_f32(v568, v526);
    float32x2_t v553 = vmul_f32(v568, v551);
    float32x2_t v561 = vmul_f32(v568, v559);
    float32x2_t v569 = vmul_f32(v568, v567);
    float32x4_t v576 = vcombine_f32(v575, v575);
    float32x4_t v581 = vcombine_f32(v580, v580);
    float32x4_t v586 = vcombine_f32(v585, v585);
    const float32x2_t *v1305 = &v5[istride * 5];
    const float32x2_t *v1315 = &v5[istride * 10];
    const float32x2_t *v1327 = &v5[istride * 8];
    const float32x2_t *v1337 = &v5[istride * 13];
    const float32x2_t *v1349 = &v5[istride * 3];
    const float32x2_t *v1359 = &v5[istride * 11];
    const float32x2_t *v1379 = &v5[istride * 6];
    const float32x2_t *v1389 = &v5[istride * 14];
    const float32x2_t *v1399 = &v5[istride * 4];
    const float32x2_t *v1411 = &v5[istride * 9];
    const float32x2_t *v1421 = &v5[istride * 2];
    const float32x2_t *v1431 = &v5[istride * 7];
    const float32x2_t *v1443 = &v5[istride * 12];
    float32x2_t *v1473 = &v6[ostride * 10];
    float32x2_t *v1482 = &v6[ostride * 5];
    float32x2_t *v1491 = &v6[ostride * 6];
    float32x2_t *v1509 = &v6[ostride * 11];
    float32x2_t *v1518 = &v6[ostride * 12];
    float32x2_t *v1527 = &v6[ostride * 7];
    float32x2_t *v1536 = &v6[ostride * 2];
    float32x2_t *v1545 = &v6[ostride * 3];
    float32x2_t *v1554 = &v6[ostride * 13];
    float32x2_t *v1563 = &v6[ostride * 8];
    float32x2_t *v1572 = &v6[ostride * 9];
    float32x2_t *v1581 = &v6[ostride * 4];
    float32x2_t *v1590 = &v6[ostride * 14];
    float32x4_t v1622 = vld1q_f32((const float32_t *)v1454);
    float32x4_t v222 = vmulq_f32(v216, v221);
    float32x4_t v458 = vcombine_f32(v456, v456);
    float32x4_t v466 = vcombine_f32(v464, v464);
    float32x4_t v474 = vcombine_f32(v472, v472);
    float32x4_t v514 = vcombine_f32(v512, v512);
    float32x4_t v522 = vcombine_f32(v520, v520);
    float32x4_t v530 = vcombine_f32(v528, v528);
    float32x4_t v555 = vcombine_f32(v553, v553);
    float32x4_t v563 = vcombine_f32(v561, v561);
    float32x4_t v571 = vcombine_f32(v569, v569);
    float32x4_t v1594 = vld1q_f32((const float32_t *)v1305);
    float32x4_t v1596 = vld1q_f32((const float32_t *)v1315);
    float32x4_t v1598 = vld1q_f32((const float32_t *)v1327);
    float32x4_t v1600 = vld1q_f32((const float32_t *)v1337);
    float32x4_t v1602 = vld1q_f32((const float32_t *)v1349);
    float32x4_t v1604 = vld1q_f32((const float32_t *)v1359);
    float32x4_t v1608 = vld1q_f32((const float32_t *)v1379);
    float32x4_t v1610 = vld1q_f32((const float32_t *)v1389);
    float32x4_t v1612 = vld1q_f32((const float32_t *)v1399);
    float32x4_t v1614 = vld1q_f32((const float32_t *)v1411);
    float32x4_t v1616 = vld1q_f32((const float32_t *)v1421);
    float32x4_t v1618 = vld1q_f32((const float32_t *)v1431);
    float32x4_t v1620 = vld1q_f32((const float32_t *)v1443);
    float32x4_t v61 = vtrn1q_f32(v1594, v1594);
    float32x4_t v62 = vtrn2q_f32(v1594, v1594);
    float32x4_t v73 = vtrn1q_f32(v1596, v1596);
    float32x4_t v74 = vtrn2q_f32(v1596, v1596);
    float32x4_t v123 = vtrn1q_f32(v1598, v1598);
    float32x4_t v124 = vtrn2q_f32(v1598, v1598);
    float32x4_t v135 = vtrn1q_f32(v1600, v1600);
    float32x4_t v136 = vtrn2q_f32(v1600, v1600);
    float32x4_t v154 = vtrn1q_f32(v1602, v1602);
    float32x4_t v155 = vtrn2q_f32(v1602, v1602);
    float32x4_t v204 = vtrn1q_f32(v1604, v1604);
    float32x4_t v205 = vtrn2q_f32(v1604, v1604);
    float32x4_t v225 = vfmaq_f32(v222, v217, v223);
    float32x4_t v235 = vtrn1q_f32(v1608, v1608);
    float32x4_t v236 = vtrn2q_f32(v1608, v1608);
    float32x4_t v285 = vtrn1q_f32(v1610, v1610);
    float32x4_t v286 = vtrn2q_f32(v1610, v1610);
    float32x4_t v297 = vtrn1q_f32(v1612, v1612);
    float32x4_t v298 = vtrn2q_f32(v1612, v1612);
    float32x4_t v316 = vtrn1q_f32(v1614, v1614);
    float32x4_t v317 = vtrn2q_f32(v1614, v1614);
    float32x4_t v366 = vtrn1q_f32(v1616, v1616);
    float32x4_t v367 = vtrn2q_f32(v1616, v1616);
    float32x4_t v378 = vtrn1q_f32(v1618, v1618);
    float32x4_t v379 = vtrn2q_f32(v1618, v1618);
    float32x4_t v397 = vtrn1q_f32(v1620, v1620);
    float32x4_t v398 = vtrn2q_f32(v1620, v1620);
    float32x4_t v67 = vmulq_f32(v61, v66);
    float32x4_t v79 = vmulq_f32(v73, v78);
    float32x4_t v129 = vmulq_f32(v123, v128);
    float32x4_t v141 = vmulq_f32(v135, v140);
    float32x4_t v160 = vmulq_f32(v154, v159);
    float32x4_t v210 = vmulq_f32(v204, v209);
    float32x4_t v241 = vmulq_f32(v235, v240);
    float32x4_t v291 = vmulq_f32(v285, v290);
    float32x4_t v303 = vmulq_f32(v297, v302);
    float32x4_t v322 = vmulq_f32(v316, v321);
    float32x4_t v372 = vmulq_f32(v366, v371);
    float32x4_t v384 = vmulq_f32(v378, v383);
    float32x4_t v403 = vmulq_f32(v397, v402);
    float32x4_t v70 = vfmaq_f32(v67, v62, v68);
    float32x4_t v82 = vfmaq_f32(v79, v74, v80);
    float32x4_t v132 = vfmaq_f32(v129, v124, v130);
    float32x4_t v144 = vfmaq_f32(v141, v136, v142);
    float32x4_t v163 = vfmaq_f32(v160, v155, v161);
    float32x4_t v213 = vfmaq_f32(v210, v205, v211);
    float32x4_t v244 = vfmaq_f32(v241, v236, v242);
    float32x4_t v294 = vfmaq_f32(v291, v286, v292);
    float32x4_t v306 = vfmaq_f32(v303, v298, v304);
    float32x4_t v325 = vfmaq_f32(v322, v317, v323);
    float32x4_t v375 = vfmaq_f32(v372, v367, v373);
    float32x4_t v387 = vfmaq_f32(v384, v379, v385);
    float32x4_t v406 = vfmaq_f32(v403, v398, v404);
    float32x4_t v407 = vaddq_f32(v70, v82);
    float32x4_t v408 = vsubq_f32(v70, v82);
    float32x4_t v417 = vaddq_f32(v132, v144);
    float32x4_t v418 = vsubq_f32(v132, v144);
    float32x4_t v420 = vaddq_f32(v213, v225);
    float32x4_t v421 = vsubq_f32(v213, v225);
    float32x4_t v423 = vaddq_f32(v294, v306);
    float32x4_t v424 = vsubq_f32(v294, v306);
    float32x4_t v426 = vaddq_f32(v375, v387);
    float32x4_t v427 = vsubq_f32(v375, v387);
    float32x4_t v416 = vaddq_f32(v407, v1622);
    float32x4_t v419 = vaddq_f32(v417, v163);
    float32x4_t v422 = vaddq_f32(v420, v244);
    float32x4_t v425 = vaddq_f32(v423, v325);
    float32x4_t v428 = vaddq_f32(v426, v406);
    float32x4_t v485 = vaddq_f32(v417, v426);
    float32x4_t v486 = vsubq_f32(v417, v426);
    float32x4_t v487 = vaddq_f32(v423, v420);
    float32x4_t v488 = vsubq_f32(v423, v420);
    float32x4_t v541 = vaddq_f32(v418, v427);
    float32x4_t v542 = vsubq_f32(v418, v427);
    float32x4_t v543 = vaddq_f32(v424, v421);
    float32x4_t v544 = vsubq_f32(v424, v421);
    float32x4_t v429 = vaddq_f32(v419, v428);
    float32x4_t v430 = vsubq_f32(v419, v428);
    float32x4_t v431 = vaddq_f32(v425, v422);
    float32x4_t v432 = vsubq_f32(v425, v422);
    float32x4_t v489 = vaddq_f32(v485, v487);
    float32x4_t v490 = vsubq_f32(v485, v487);
    float32x4_t v491 = vaddq_f32(v486, v488);
    float32x4_t v513 = vrev64q_f32(v486);
    float32x4_t v529 = vrev64q_f32(v488);
    float32x4_t v545 = vaddq_f32(v541, v543);
    float32x4_t v546 = vsubq_f32(v541, v543);
    float32x4_t v547 = vaddq_f32(v542, v544);
    float32x4_t v577 = vmulq_f32(v542, v576);
    float32x4_t v587 = vmulq_f32(v544, v586);
    float32x4_t v433 = vaddq_f32(v429, v431);
    float32x4_t v434 = vsubq_f32(v429, v431);
    float32x4_t v435 = vaddq_f32(v430, v432);
    float32x4_t v457 = vrev64q_f32(v430);
    float32x4_t v473 = vrev64q_f32(v432);
    float32x4_t v492 = vaddq_f32(v489, v407);
    float32x4_t v502 = vmulq_f32(v489, v501);
    float32x4_t v507 = vmulq_f32(v490, v506);
    float32x4_t v515 = vmulq_f32(v513, v514);
    float32x4_t v521 = vrev64q_f32(v491);
    float32x4_t v531 = vmulq_f32(v529, v530);
    float32x4_t v548 = vaddq_f32(v545, v408);
    float32x4_t v562 = vrev64q_f32(v545);
    float32x4_t v570 = vrev64q_f32(v546);
    float32x4_t v582 = vmulq_f32(v547, v581);
    float32x4_t v436 = vaddq_f32(v433, v416);
    float32x4_t v446 = vmulq_f32(v433, v445);
    float32x4_t v451 = vmulq_f32(v434, v450);
    float32x4_t v459 = vmulq_f32(v457, v458);
    float32x4_t v465 = vrev64q_f32(v435);
    float32x4_t v475 = vmulq_f32(v473, v474);
    float32x4_t v497 = vmulq_f32(v492, v496);
    float32x4_t v523 = vmulq_f32(v521, v522);
    float32x4_t v554 = vrev64q_f32(v548);
    float32x4_t v564 = vmulq_f32(v562, v563);
    float32x4_t v572 = vmulq_f32(v570, v571);
    float32x4_t v591 = vsubq_f32(v577, v582);
    float32x4_t v592 = vaddq_f32(v582, v587);
    float32x4_t v467 = vmulq_f32(v465, v466);
    float32x4_t v476 = vaddq_f32(v436, v446);
    float32x4_t v532 = vaddq_f32(v497, v502);
    float32x4_t v535 = vsubq_f32(v515, v523);
    float32x4_t v536 = vaddq_f32(v523, v531);
    float32x4_t v556 = vmulq_f32(v554, v555);
    float32x4_t v597 = vaddq_f32(v436, v497);
    vst1q_f32((float32_t *)v1464, v436);
    float32x4_t v477 = vaddq_f32(v476, v451);
    float32x4_t v478 = vsubq_f32(v476, v451);
    float32x4_t v479 = vsubq_f32(v459, v467);
    float32x4_t v480 = vaddq_f32(v467, v475);
    float32x4_t v533 = vaddq_f32(v532, v507);
    float32x4_t v534 = vsubq_f32(v532, v507);
    float32x4_t v588 = vaddq_f32(v556, v564);
    float32x4_t v598 = vaddq_f32(v597, v556);
    float32x4_t v599 = vsubq_f32(v597, v556);
    float32x4_t v481 = vaddq_f32(v477, v479);
    float32x4_t v482 = vsubq_f32(v477, v479);
    float32x4_t v483 = vaddq_f32(v478, v480);
    float32x4_t v484 = vsubq_f32(v478, v480);
    float32x4_t v537 = vaddq_f32(v533, v535);
    float32x4_t v538 = vsubq_f32(v533, v535);
    float32x4_t v539 = vaddq_f32(v534, v536);
    float32x4_t v540 = vsubq_f32(v534, v536);
    float32x4_t v589 = vaddq_f32(v588, v572);
    float32x4_t v590 = vsubq_f32(v588, v572);
    vst1q_f32((float32_t *)v1473, v599);
    vst1q_f32((float32_t *)v1482, v598);
    float32x4_t v593 = vaddq_f32(v589, v591);
    float32x4_t v594 = vsubq_f32(v589, v591);
    float32x4_t v595 = vaddq_f32(v590, v592);
    float32x4_t v596 = vsubq_f32(v590, v592);
    float32x4_t v621 = vaddq_f32(v482, v538);
    float32x4_t v645 = vaddq_f32(v484, v540);
    float32x4_t v669 = vaddq_f32(v483, v539);
    float32x4_t v693 = vaddq_f32(v481, v537);
    vst1q_f32((float32_t *)v1491, v482);
    vst1q_f32((float32_t *)v1518, v484);
    vst1q_f32((float32_t *)v1545, v483);
    vst1q_f32((float32_t *)v1572, v481);
    float32x4_t v622 = vaddq_f32(v621, v594);
    float32x4_t v623 = vsubq_f32(v621, v594);
    float32x4_t v646 = vaddq_f32(v645, v596);
    float32x4_t v647 = vsubq_f32(v645, v596);
    float32x4_t v670 = vaddq_f32(v669, v595);
    float32x4_t v671 = vsubq_f32(v669, v595);
    float32x4_t v694 = vaddq_f32(v693, v593);
    float32x4_t v695 = vsubq_f32(v693, v593);
    vst1q_f32((float32_t *)v1500, v623);
    vst1q_f32((float32_t *)v1509, v622);
    vst1q_f32((float32_t *)v1527, v647);
    vst1q_f32((float32_t *)v1536, v646);
    vst1q_f32((float32_t *)v1554, v671);
    vst1q_f32((float32_t *)v1563, v670);
    vst1q_f32((float32_t *)v1581, v695);
    vst1q_f32((float32_t *)v1590, v694);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v717 * 2; j < howmany; j += 1) {
    float32x2_t v859 = v5[istride];
    float v1068 = -1.2500000000000000e+00F;
    float v1072 = 5.5901699437494745e-01F;
    float v1075 = 1.5388417685876268e+00F;
    float v1076 = -1.5388417685876268e+00F;
    float v1082 = 5.8778525229247325e-01F;
    float v1083 = -5.8778525229247325e-01F;
    float v1089 = 3.6327126400268028e-01F;
    float v1090 = -3.6327126400268028e-01F;
    float v1114 = -1.4999999999999998e+00F;
    float v1118 = 1.8749999999999998e+00F;
    float v1122 = -8.3852549156242107e-01F;
    float v1125 = -2.3082626528814396e+00F;
    float v1126 = 2.3082626528814396e+00F;
    float v1132 = -8.8167787843870971e-01F;
    float v1133 = 8.8167787843870971e-01F;
    float v1139 = -5.4490689600402031e-01F;
    float v1140 = 5.4490689600402031e-01F;
    float v1163 = 8.6602540378443871e-01F;
    float v1164 = -8.6602540378443871e-01F;
    float v1170 = -1.0825317547305484e+00F;
    float v1171 = 1.0825317547305484e+00F;
    float v1177 = 4.8412291827592718e-01F;
    float v1178 = -4.8412291827592718e-01F;
    float32x2_t v1180 = (float32x2_t){v4, v4};
    float v1185 = -1.3326760640014592e+00F;
    float v1189 = -5.0903696045512736e-01F;
    float v1193 = -3.1460214309120460e-01F;
    float32x2_t v756 = v7[8];
    float32x2_t v761 = v7[9];
    float32x2_t v766 = v7[18];
    float32x2_t v771 = v7[19];
    float32x2_t v806 = v7[14];
    float32x2_t v811 = v7[15];
    float32x2_t v816 = v7[24];
    float32x2_t v821 = v7[25];
    float32x2_t v831 = v7[4];
    float32x2_t v836 = v7[5];
    float32x2_t v871 = v7[20];
    float32x2_t v876 = v7[21];
    float32x2_t v881 = v7[0];
    float32x2_t v882 = vtrn1_f32(v859, v859);
    float32x2_t v883 = vtrn2_f32(v859, v859);
    float32x2_t v886 = v7[1];
    float32x2_t v896 = v7[10];
    float32x2_t v901 = v7[11];
    float32x2_t v936 = v7[26];
    float32x2_t v941 = v7[27];
    float32x2_t v946 = v7[6];
    float32x2_t v951 = v7[7];
    float32x2_t v961 = v7[16];
    float32x2_t v966 = v7[17];
    float32x2_t v1001 = v7[2];
    float32x2_t v1006 = v7[3];
    float32x2_t v1011 = v7[12];
    float32x2_t v1016 = v7[13];
    float32x2_t v1026 = v7[22];
    float32x2_t v1031 = v7[23];
    float32x2_t v1041 = v5[0];
    float32x2_t v1069 = (float32x2_t){v1068, v1068};
    float32x2_t v1073 = (float32x2_t){v1072, v1072};
    float32x2_t v1077 = (float32x2_t){v1075, v1076};
    float32x2_t v1084 = (float32x2_t){v1082, v1083};
    float32x2_t v1091 = (float32x2_t){v1089, v1090};
    float32x2_t v1115 = (float32x2_t){v1114, v1114};
    float32x2_t v1119 = (float32x2_t){v1118, v1118};
    float32x2_t v1123 = (float32x2_t){v1122, v1122};
    float32x2_t v1127 = (float32x2_t){v1125, v1126};
    float32x2_t v1134 = (float32x2_t){v1132, v1133};
    float32x2_t v1141 = (float32x2_t){v1139, v1140};
    float32x2_t v1165 = (float32x2_t){v1163, v1164};
    float32x2_t v1172 = (float32x2_t){v1170, v1171};
    float32x2_t v1179 = (float32x2_t){v1177, v1178};
    float32x2_t v1186 = (float32x2_t){v1185, v1185};
    float32x2_t v1190 = (float32x2_t){v1189, v1189};
    float32x2_t v1194 = (float32x2_t){v1193, v1193};
    float32x2_t v729 = v5[istride * 5];
    float32x2_t v744 = v5[istride * 10];
    float32x2_t v779 = v5[istride * 8];
    float32x2_t v794 = v5[istride * 13];
    float32x2_t v829 = v5[istride * 3];
    float32x2_t v844 = v5[istride * 11];
    float32x2_t v887 = vmul_f32(v882, v881);
    float32x2_t v894 = v5[istride * 6];
    float32x2_t v909 = v5[istride * 14];
    float32x2_t v924 = v5[istride * 4];
    float32x2_t v959 = v5[istride * 9];
    float32x2_t v974 = v5[istride * 2];
    float32x2_t v989 = v5[istride * 7];
    float32x2_t v1024 = v5[istride * 12];
    float32x2_t v1079 = vmul_f32(v1180, v1077);
    float32x2_t v1086 = vmul_f32(v1180, v1084);
    float32x2_t v1093 = vmul_f32(v1180, v1091);
    float32x2_t v1129 = vmul_f32(v1180, v1127);
    float32x2_t v1136 = vmul_f32(v1180, v1134);
    float32x2_t v1143 = vmul_f32(v1180, v1141);
    float32x2_t v1167 = vmul_f32(v1180, v1165);
    float32x2_t v1174 = vmul_f32(v1180, v1172);
    float32x2_t v1181 = vmul_f32(v1180, v1179);
    float32x2_t v757 = vtrn1_f32(v729, v729);
    float32x2_t v758 = vtrn2_f32(v729, v729);
    float32x2_t v767 = vtrn1_f32(v744, v744);
    float32x2_t v768 = vtrn2_f32(v744, v744);
    float32x2_t v807 = vtrn1_f32(v779, v779);
    float32x2_t v808 = vtrn2_f32(v779, v779);
    float32x2_t v817 = vtrn1_f32(v794, v794);
    float32x2_t v818 = vtrn2_f32(v794, v794);
    float32x2_t v832 = vtrn1_f32(v829, v829);
    float32x2_t v833 = vtrn2_f32(v829, v829);
    float32x2_t v872 = vtrn1_f32(v844, v844);
    float32x2_t v873 = vtrn2_f32(v844, v844);
    float32x2_t v889 = vfma_f32(v887, v883, v886);
    float32x2_t v897 = vtrn1_f32(v894, v894);
    float32x2_t v898 = vtrn2_f32(v894, v894);
    float32x2_t v937 = vtrn1_f32(v909, v909);
    float32x2_t v938 = vtrn2_f32(v909, v909);
    float32x2_t v947 = vtrn1_f32(v924, v924);
    float32x2_t v948 = vtrn2_f32(v924, v924);
    float32x2_t v962 = vtrn1_f32(v959, v959);
    float32x2_t v963 = vtrn2_f32(v959, v959);
    float32x2_t v1002 = vtrn1_f32(v974, v974);
    float32x2_t v1003 = vtrn2_f32(v974, v974);
    float32x2_t v1012 = vtrn1_f32(v989, v989);
    float32x2_t v1013 = vtrn2_f32(v989, v989);
    float32x2_t v1027 = vtrn1_f32(v1024, v1024);
    float32x2_t v1028 = vtrn2_f32(v1024, v1024);
    float32x2_t v762 = vmul_f32(v757, v756);
    float32x2_t v772 = vmul_f32(v767, v766);
    float32x2_t v812 = vmul_f32(v807, v806);
    float32x2_t v822 = vmul_f32(v817, v816);
    float32x2_t v837 = vmul_f32(v832, v831);
    float32x2_t v877 = vmul_f32(v872, v871);
    float32x2_t v902 = vmul_f32(v897, v896);
    float32x2_t v942 = vmul_f32(v937, v936);
    float32x2_t v952 = vmul_f32(v947, v946);
    float32x2_t v967 = vmul_f32(v962, v961);
    float32x2_t v1007 = vmul_f32(v1002, v1001);
    float32x2_t v1017 = vmul_f32(v1012, v1011);
    float32x2_t v1032 = vmul_f32(v1027, v1026);
    float32x2_t v764 = vfma_f32(v762, v758, v761);
    float32x2_t v774 = vfma_f32(v772, v768, v771);
    float32x2_t v814 = vfma_f32(v812, v808, v811);
    float32x2_t v824 = vfma_f32(v822, v818, v821);
    float32x2_t v839 = vfma_f32(v837, v833, v836);
    float32x2_t v879 = vfma_f32(v877, v873, v876);
    float32x2_t v904 = vfma_f32(v902, v898, v901);
    float32x2_t v944 = vfma_f32(v942, v938, v941);
    float32x2_t v954 = vfma_f32(v952, v948, v951);
    float32x2_t v969 = vfma_f32(v967, v963, v966);
    float32x2_t v1009 = vfma_f32(v1007, v1003, v1006);
    float32x2_t v1019 = vfma_f32(v1017, v1013, v1016);
    float32x2_t v1034 = vfma_f32(v1032, v1028, v1031);
    float32x2_t v1035 = vadd_f32(v764, v774);
    float32x2_t v1036 = vsub_f32(v764, v774);
    float32x2_t v1043 = vadd_f32(v814, v824);
    float32x2_t v1044 = vsub_f32(v814, v824);
    float32x2_t v1046 = vadd_f32(v879, v889);
    float32x2_t v1047 = vsub_f32(v879, v889);
    float32x2_t v1049 = vadd_f32(v944, v954);
    float32x2_t v1050 = vsub_f32(v944, v954);
    float32x2_t v1052 = vadd_f32(v1009, v1019);
    float32x2_t v1053 = vsub_f32(v1009, v1019);
    float32x2_t v1042 = vadd_f32(v1035, v1041);
    float32x2_t v1045 = vadd_f32(v1043, v839);
    float32x2_t v1048 = vadd_f32(v1046, v904);
    float32x2_t v1051 = vadd_f32(v1049, v969);
    float32x2_t v1054 = vadd_f32(v1052, v1034);
    float32x2_t v1105 = vadd_f32(v1043, v1052);
    float32x2_t v1106 = vsub_f32(v1043, v1052);
    float32x2_t v1107 = vadd_f32(v1049, v1046);
    float32x2_t v1108 = vsub_f32(v1049, v1046);
    float32x2_t v1155 = vadd_f32(v1044, v1053);
    float32x2_t v1156 = vsub_f32(v1044, v1053);
    float32x2_t v1157 = vadd_f32(v1050, v1047);
    float32x2_t v1158 = vsub_f32(v1050, v1047);
    float32x2_t v1055 = vadd_f32(v1045, v1054);
    float32x2_t v1056 = vsub_f32(v1045, v1054);
    float32x2_t v1057 = vadd_f32(v1051, v1048);
    float32x2_t v1058 = vsub_f32(v1051, v1048);
    float32x2_t v1109 = vadd_f32(v1105, v1107);
    float32x2_t v1110 = vsub_f32(v1105, v1107);
    float32x2_t v1111 = vadd_f32(v1106, v1108);
    float32x2_t v1130 = vrev64_f32(v1106);
    float32x2_t v1144 = vrev64_f32(v1108);
    float32x2_t v1159 = vadd_f32(v1155, v1157);
    float32x2_t v1160 = vsub_f32(v1155, v1157);
    float32x2_t v1161 = vadd_f32(v1156, v1158);
    float32x2_t v1187 = vmul_f32(v1156, v1186);
    float32x2_t v1195 = vmul_f32(v1158, v1194);
    float32x2_t v1059 = vadd_f32(v1055, v1057);
    float32x2_t v1060 = vsub_f32(v1055, v1057);
    float32x2_t v1061 = vadd_f32(v1056, v1058);
    float32x2_t v1080 = vrev64_f32(v1056);
    float32x2_t v1094 = vrev64_f32(v1058);
    float32x2_t v1112 = vadd_f32(v1109, v1035);
    float32x2_t v1120 = vmul_f32(v1109, v1119);
    float32x2_t v1124 = vmul_f32(v1110, v1123);
    float32x2_t v1131 = vmul_f32(v1130, v1129);
    float32x2_t v1137 = vrev64_f32(v1111);
    float32x2_t v1145 = vmul_f32(v1144, v1143);
    float32x2_t v1162 = vadd_f32(v1159, v1036);
    float32x2_t v1175 = vrev64_f32(v1159);
    float32x2_t v1182 = vrev64_f32(v1160);
    float32x2_t v1191 = vmul_f32(v1161, v1190);
    float32x2_t v1062 = vadd_f32(v1059, v1042);
    float32x2_t v1070 = vmul_f32(v1059, v1069);
    float32x2_t v1074 = vmul_f32(v1060, v1073);
    float32x2_t v1081 = vmul_f32(v1080, v1079);
    float32x2_t v1087 = vrev64_f32(v1061);
    float32x2_t v1095 = vmul_f32(v1094, v1093);
    float32x2_t v1116 = vmul_f32(v1112, v1115);
    float32x2_t v1138 = vmul_f32(v1137, v1136);
    float32x2_t v1168 = vrev64_f32(v1162);
    float32x2_t v1176 = vmul_f32(v1175, v1174);
    float32x2_t v1183 = vmul_f32(v1182, v1181);
    float32x2_t v1199 = vsub_f32(v1187, v1191);
    float32x2_t v1200 = vadd_f32(v1191, v1195);
    float32x2_t v1088 = vmul_f32(v1087, v1086);
    float32x2_t v1096 = vadd_f32(v1062, v1070);
    float32x2_t v1146 = vadd_f32(v1116, v1120);
    float32x2_t v1149 = vsub_f32(v1131, v1138);
    float32x2_t v1150 = vadd_f32(v1138, v1145);
    float32x2_t v1169 = vmul_f32(v1168, v1167);
    float32x2_t v1205 = vadd_f32(v1062, v1116);
    v6[0] = v1062;
    float32x2_t v1097 = vadd_f32(v1096, v1074);
    float32x2_t v1098 = vsub_f32(v1096, v1074);
    float32x2_t v1099 = vsub_f32(v1081, v1088);
    float32x2_t v1100 = vadd_f32(v1088, v1095);
    float32x2_t v1147 = vadd_f32(v1146, v1124);
    float32x2_t v1148 = vsub_f32(v1146, v1124);
    float32x2_t v1196 = vadd_f32(v1169, v1176);
    float32x2_t v1206 = vadd_f32(v1205, v1169);
    float32x2_t v1207 = vsub_f32(v1205, v1169);
    float32x2_t v1101 = vadd_f32(v1097, v1099);
    float32x2_t v1102 = vsub_f32(v1097, v1099);
    float32x2_t v1103 = vadd_f32(v1098, v1100);
    float32x2_t v1104 = vsub_f32(v1098, v1100);
    float32x2_t v1151 = vadd_f32(v1147, v1149);
    float32x2_t v1152 = vsub_f32(v1147, v1149);
    float32x2_t v1153 = vadd_f32(v1148, v1150);
    float32x2_t v1154 = vsub_f32(v1148, v1150);
    float32x2_t v1197 = vadd_f32(v1196, v1183);
    float32x2_t v1198 = vsub_f32(v1196, v1183);
    v6[ostride * 10] = v1207;
    v6[ostride * 5] = v1206;
    float32x2_t v1201 = vadd_f32(v1197, v1199);
    float32x2_t v1202 = vsub_f32(v1197, v1199);
    float32x2_t v1203 = vadd_f32(v1198, v1200);
    float32x2_t v1204 = vsub_f32(v1198, v1200);
    float32x2_t v1223 = vadd_f32(v1102, v1152);
    v6[ostride * 6] = v1102;
    float32x2_t v1241 = vadd_f32(v1104, v1154);
    v6[ostride * 12] = v1104;
    float32x2_t v1259 = vadd_f32(v1103, v1153);
    v6[ostride * 3] = v1103;
    float32x2_t v1277 = vadd_f32(v1101, v1151);
    v6[ostride * 9] = v1101;
    float32x2_t v1224 = vadd_f32(v1223, v1202);
    float32x2_t v1225 = vsub_f32(v1223, v1202);
    float32x2_t v1242 = vadd_f32(v1241, v1204);
    float32x2_t v1243 = vsub_f32(v1241, v1204);
    float32x2_t v1260 = vadd_f32(v1259, v1203);
    float32x2_t v1261 = vsub_f32(v1259, v1203);
    float32x2_t v1278 = vadd_f32(v1277, v1201);
    float32x2_t v1279 = vsub_f32(v1277, v1201);
    v6[ostride] = v1225;
    v6[ostride * 11] = v1224;
    v6[ostride * 7] = v1243;
    v6[ostride * 2] = v1242;
    v6[ostride * 13] = v1261;
    v6[ostride * 8] = v1260;
    v6[ostride * 4] = v1279;
    v6[ostride * 14] = v1278;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu15(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v248 = -1.2500000000000000e+00F;
    float v253 = 5.5901699437494745e-01F;
    float v258 = -1.5388417685876268e+00F;
    float v265 = -5.8778525229247325e-01F;
    float v272 = -3.6327126400268028e-01F;
    float v296 = -1.4999999999999998e+00F;
    float v301 = 1.8749999999999998e+00F;
    float v306 = -8.3852549156242107e-01F;
    float v311 = 2.3082626528814396e+00F;
    float v318 = 8.8167787843870971e-01F;
    float v325 = 5.4490689600402031e-01F;
    float v349 = -8.6602540378443871e-01F;
    float v356 = 1.0825317547305484e+00F;
    float v363 = -4.8412291827592718e-01F;
    float v370 = -1.3326760640014592e+00F;
    float v375 = -5.0903696045512736e-01F;
    float v380 = -3.1460214309120460e-01F;
    const float32x2_t *v573 = &v5[v0];
    float32x2_t *v710 = &v6[v2];
    int64_t v19 = v0 * 5;
    int64_t v30 = v0 * 10;
    int64_t v49 = v0 * 8;
    int64_t v60 = v0 * 13;
    int64_t v79 = v0 * 3;
    int64_t v90 = v0 * 11;
    int64_t v120 = v0 * 6;
    int64_t v131 = v0 * 14;
    int64_t v142 = v0 * 4;
    int64_t v161 = v0 * 9;
    int64_t v172 = v0 * 2;
    int64_t v183 = v0 * 7;
    int64_t v202 = v0 * 12;
    float v261 = v4 * v258;
    float v268 = v4 * v265;
    float v275 = v4 * v272;
    float v314 = v4 * v311;
    float v321 = v4 * v318;
    float v328 = v4 * v325;
    float v352 = v4 * v349;
    float v359 = v4 * v356;
    float v366 = v4 * v363;
    int64_t v404 = v2 * 10;
    int64_t v411 = v2 * 5;
    int64_t v421 = v2 * 6;
    int64_t v435 = v2 * 11;
    int64_t v445 = v2 * 12;
    int64_t v452 = v2 * 7;
    int64_t v459 = v2 * 2;
    int64_t v469 = v2 * 3;
    int64_t v476 = v2 * 13;
    int64_t v483 = v2 * 8;
    int64_t v493 = v2 * 9;
    int64_t v500 = v2 * 4;
    int64_t v507 = v2 * 14;
    const float32x2_t *v646 = &v5[0];
    svfloat32_t v650 = svdup_n_f32(v248);
    svfloat32_t v651 = svdup_n_f32(v253);
    svfloat32_t v655 = svdup_n_f32(v296);
    svfloat32_t v656 = svdup_n_f32(v301);
    svfloat32_t v657 = svdup_n_f32(v306);
    svfloat32_t v664 = svdup_n_f32(v370);
    svfloat32_t v665 = svdup_n_f32(v375);
    svfloat32_t v666 = svdup_n_f32(v380);
    float32x2_t *v674 = &v6[0];
    svfloat32_t v816 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v573)[0]));
    svfloat32_t v42 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v46 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v72 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v76 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v87 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v113 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v117 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v128 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v154 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v158 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v169 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v195 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v199 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v210 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    const float32x2_t *v519 = &v5[v19];
    const float32x2_t *v528 = &v5[v30];
    const float32x2_t *v537 = &v5[v49];
    const float32x2_t *v546 = &v5[v60];
    const float32x2_t *v555 = &v5[v79];
    const float32x2_t *v564 = &v5[v90];
    const float32x2_t *v582 = &v5[v120];
    const float32x2_t *v591 = &v5[v131];
    const float32x2_t *v600 = &v5[v142];
    const float32x2_t *v609 = &v5[v161];
    const float32x2_t *v618 = &v5[v172];
    const float32x2_t *v627 = &v5[v183];
    const float32x2_t *v636 = &v5[v202];
    svfloat32_t v652 = svdup_n_f32(v261);
    svfloat32_t v653 = svdup_n_f32(v268);
    svfloat32_t v654 = svdup_n_f32(v275);
    svfloat32_t v658 = svdup_n_f32(v314);
    svfloat32_t v659 = svdup_n_f32(v321);
    svfloat32_t v660 = svdup_n_f32(v328);
    svfloat32_t v661 = svdup_n_f32(v352);
    svfloat32_t v662 = svdup_n_f32(v359);
    svfloat32_t v663 = svdup_n_f32(v366);
    float32x2_t *v683 = &v6[v404];
    float32x2_t *v692 = &v6[v411];
    float32x2_t *v701 = &v6[v421];
    float32x2_t *v719 = &v6[v435];
    float32x2_t *v728 = &v6[v445];
    float32x2_t *v737 = &v6[v452];
    float32x2_t *v746 = &v6[v459];
    float32x2_t *v755 = &v6[v469];
    float32x2_t *v764 = &v6[v476];
    float32x2_t *v773 = &v6[v483];
    float32x2_t *v782 = &v6[v493];
    float32x2_t *v791 = &v6[v500];
    float32x2_t *v800 = &v6[v507];
    svfloat32_t v832 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v646)[0]));
    svfloat32_t zero118 = svdup_n_f32(0);
    svfloat32_t v118 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero118, v816, v117, 0),
                     v816, v117, 90);
    svfloat32_t v804 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v519)[0]));
    svfloat32_t v806 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v528)[0]));
    svfloat32_t v808 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v537)[0]));
    svfloat32_t v810 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v546)[0]));
    svfloat32_t v812 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v555)[0]));
    svfloat32_t v814 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v564)[0]));
    svfloat32_t v818 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v582)[0]));
    svfloat32_t v820 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v591)[0]));
    svfloat32_t v822 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v600)[0]));
    svfloat32_t v824 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v609)[0]));
    svfloat32_t v826 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v618)[0]));
    svfloat32_t v828 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v627)[0]));
    svfloat32_t v830 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v636)[0]));
    svfloat32_t zero43 = svdup_n_f32(0);
    svfloat32_t v43 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero43, v804, v42, 0),
                     v804, v42, 90);
    svfloat32_t zero47 = svdup_n_f32(0);
    svfloat32_t v47 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero47, v806, v46, 0),
                     v806, v46, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v808, v72, 0),
                     v808, v72, 90);
    svfloat32_t zero77 = svdup_n_f32(0);
    svfloat32_t v77 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero77, v810, v76, 0),
                     v810, v76, 90);
    svfloat32_t zero114 = svdup_n_f32(0);
    svfloat32_t v114 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero114, v814, v113, 0),
                     v814, v113, 90);
    svfloat32_t zero155 = svdup_n_f32(0);
    svfloat32_t v155 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero155, v820, v154, 0),
                     v820, v154, 90);
    svfloat32_t zero159 = svdup_n_f32(0);
    svfloat32_t v159 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero159, v822, v158, 0),
                     v822, v158, 90);
    svfloat32_t zero196 = svdup_n_f32(0);
    svfloat32_t v196 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero196, v826, v195, 0),
                     v826, v195, 90);
    svfloat32_t zero200 = svdup_n_f32(0);
    svfloat32_t v200 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero200, v828, v199, 0),
                     v828, v199, 90);
    svfloat32_t v212 = svadd_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v213 = svsub_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v222 = svadd_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v223 = svsub_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v225 = svadd_f32_x(svptrue_b32(), v114, v118);
    svfloat32_t v226 = svsub_f32_x(svptrue_b32(), v114, v118);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v155, v159);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v155, v159);
    svfloat32_t v231 = svadd_f32_x(svptrue_b32(), v196, v200);
    svfloat32_t v232 = svsub_f32_x(svptrue_b32(), v196, v200);
    svfloat32_t v221 = svadd_f32_x(svptrue_b32(), v212, v832);
    svfloat32_t v224 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, v222, v812, v87, 0), v812, v87, 90);
    svfloat32_t v227 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v225, v818, v128, 0),
                     v818, v128, 90);
    svfloat32_t v230 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v228, v824, v169, 0),
                     v824, v169, 90);
    svfloat32_t v233 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v231, v830, v210, 0),
                     v830, v210, 90);
    svfloat32_t v287 = svadd_f32_x(svptrue_b32(), v222, v231);
    svfloat32_t v288 = svsub_f32_x(svptrue_b32(), v222, v231);
    svfloat32_t v289 = svadd_f32_x(svptrue_b32(), v228, v225);
    svfloat32_t v290 = svsub_f32_x(svptrue_b32(), v228, v225);
    svfloat32_t v340 = svadd_f32_x(svptrue_b32(), v223, v232);
    svfloat32_t v341 = svsub_f32_x(svptrue_b32(), v223, v232);
    svfloat32_t v342 = svadd_f32_x(svptrue_b32(), v229, v226);
    svfloat32_t v343 = svsub_f32_x(svptrue_b32(), v229, v226);
    svfloat32_t v234 = svadd_f32_x(svptrue_b32(), v224, v233);
    svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v224, v233);
    svfloat32_t v236 = svadd_f32_x(svptrue_b32(), v230, v227);
    svfloat32_t v237 = svsub_f32_x(svptrue_b32(), v230, v227);
    svfloat32_t v291 = svadd_f32_x(svptrue_b32(), v287, v289);
    svfloat32_t v292 = svsub_f32_x(svptrue_b32(), v287, v289);
    svfloat32_t v293 = svadd_f32_x(svptrue_b32(), v288, v290);
    svfloat32_t zero316 = svdup_n_f32(0);
    svfloat32_t v316 = svcmla_f32_x(pred_full, zero316, v658, v288, 90);
    svfloat32_t v344 = svadd_f32_x(svptrue_b32(), v340, v342);
    svfloat32_t v345 = svsub_f32_x(svptrue_b32(), v340, v342);
    svfloat32_t v346 = svadd_f32_x(svptrue_b32(), v341, v343);
    svfloat32_t v383 = svmul_f32_x(svptrue_b32(), v343, v666);
    svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v234, v236);
    svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v234, v236);
    svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v235, v237);
    svfloat32_t zero263 = svdup_n_f32(0);
    svfloat32_t v263 = svcmla_f32_x(pred_full, zero263, v652, v235, 90);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v291, v212);
    svfloat32_t v304 = svmul_f32_x(svptrue_b32(), v291, v656);
    svfloat32_t zero323 = svdup_n_f32(0);
    svfloat32_t v323 = svcmla_f32_x(pred_full, zero323, v659, v293, 90);
    svfloat32_t v347 = svadd_f32_x(svptrue_b32(), v344, v213);
    svfloat32_t zero368 = svdup_n_f32(0);
    svfloat32_t v368 = svcmla_f32_x(pred_full, zero368, v663, v345, 90);
    svfloat32_t v378 = svmul_f32_x(svptrue_b32(), v346, v665);
    svfloat32_t v241 = svadd_f32_x(svptrue_b32(), v238, v221);
    svfloat32_t zero270 = svdup_n_f32(0);
    svfloat32_t v270 = svcmla_f32_x(pred_full, zero270, v653, v240, 90);
    svfloat32_t v334 = svsub_f32_x(svptrue_b32(), v316, v323);
    svfloat32_t v335 = svcmla_f32_x(pred_full, v323, v660, v290, 90);
    svfloat32_t zero354 = svdup_n_f32(0);
    svfloat32_t v354 = svcmla_f32_x(pred_full, zero354, v661, v347, 90);
    svfloat32_t v387 = svnmls_f32_x(pred_full, v378, v341, v664);
    svfloat32_t v388 = svmla_f32_x(pred_full, v383, v346, v665);
    svfloat32_t v278 = svmla_f32_x(pred_full, v241, v238, v650);
    svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v263, v270);
    svfloat32_t v282 = svcmla_f32_x(pred_full, v270, v654, v237, 90);
    svfloat32_t v331 = svmla_f32_x(pred_full, v304, v294, v655);
    svfloat32_t v384 = svcmla_f32_x(pred_full, v354, v662, v344, 90);
    svfloat32_t v393 = svmla_f32_x(pred_full, v241, v294, v655);
    svst1_f64(pred_full, (double *)(v674), svreinterpret_f64_f32(v241));
    svfloat32_t v279 = svmla_f32_x(pred_full, v278, v239, v651);
    svfloat32_t v280 = svmls_f32_x(pred_full, v278, v239, v651);
    svfloat32_t v332 = svmla_f32_x(pred_full, v331, v292, v657);
    svfloat32_t v333 = svmls_f32_x(pred_full, v331, v292, v657);
    svfloat32_t v385 = svadd_f32_x(svptrue_b32(), v384, v368);
    svfloat32_t v386 = svsub_f32_x(svptrue_b32(), v384, v368);
    svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v393, v354);
    svfloat32_t v395 = svsub_f32_x(svptrue_b32(), v393, v354);
    svfloat32_t v283 = svadd_f32_x(svptrue_b32(), v279, v281);
    svfloat32_t v284 = svsub_f32_x(svptrue_b32(), v279, v281);
    svfloat32_t v285 = svadd_f32_x(svptrue_b32(), v280, v282);
    svfloat32_t v286 = svsub_f32_x(svptrue_b32(), v280, v282);
    svfloat32_t v336 = svadd_f32_x(svptrue_b32(), v332, v334);
    svfloat32_t v337 = svsub_f32_x(svptrue_b32(), v332, v334);
    svfloat32_t v338 = svadd_f32_x(svptrue_b32(), v333, v335);
    svfloat32_t v339 = svsub_f32_x(svptrue_b32(), v333, v335);
    svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v385, v387);
    svfloat32_t v390 = svsub_f32_x(svptrue_b32(), v385, v387);
    svfloat32_t v391 = svadd_f32_x(svptrue_b32(), v386, v388);
    svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v386, v388);
    svst1_f64(pred_full, (double *)(v683), svreinterpret_f64_f32(v395));
    svst1_f64(pred_full, (double *)(v692), svreinterpret_f64_f32(v394));
    svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v284, v337);
    svfloat32_t v441 = svadd_f32_x(svptrue_b32(), v286, v339);
    svfloat32_t v465 = svadd_f32_x(svptrue_b32(), v285, v338);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v283, v336);
    svst1_f64(pred_full, (double *)(v701), svreinterpret_f64_f32(v284));
    svst1_f64(pred_full, (double *)(v728), svreinterpret_f64_f32(v286));
    svst1_f64(pred_full, (double *)(v755), svreinterpret_f64_f32(v285));
    svst1_f64(pred_full, (double *)(v782), svreinterpret_f64_f32(v283));
    svfloat32_t v418 = svadd_f32_x(svptrue_b32(), v417, v390);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v417, v390);
    svfloat32_t v442 = svadd_f32_x(svptrue_b32(), v441, v392);
    svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v441, v392);
    svfloat32_t v466 = svadd_f32_x(svptrue_b32(), v465, v391);
    svfloat32_t v467 = svsub_f32_x(svptrue_b32(), v465, v391);
    svfloat32_t v490 = svadd_f32_x(svptrue_b32(), v489, v389);
    svfloat32_t v491 = svsub_f32_x(svptrue_b32(), v489, v389);
    svst1_f64(pred_full, (double *)(v710), svreinterpret_f64_f32(v419));
    svst1_f64(pred_full, (double *)(v719), svreinterpret_f64_f32(v418));
    svst1_f64(pred_full, (double *)(v737), svreinterpret_f64_f32(v443));
    svst1_f64(pred_full, (double *)(v746), svreinterpret_f64_f32(v442));
    svst1_f64(pred_full, (double *)(v764), svreinterpret_f64_f32(v467));
    svst1_f64(pred_full, (double *)(v773), svreinterpret_f64_f32(v466));
    svst1_f64(pred_full, (double *)(v791), svreinterpret_f64_f32(v491));
    svst1_f64(pred_full, (double *)(v800), svreinterpret_f64_f32(v490));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu16(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v803 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v587 = 1.0000000000000000e+00F;
    float v588 = -1.0000000000000000e+00F;
    float v596 = -7.0710678118654746e-01F;
    float v604 = 7.0710678118654757e-01F;
    float v608 = 9.2387953251128674e-01F;
    float v609 = -9.2387953251128674e-01F;
    float v617 = 5.4119610014619690e-01F;
    float v625 = -1.3065629648763766e+00F;
    float32x2_t v627 = (float32x2_t){v4, v4};
    float v633 = 3.8268343236508984e-01F;
    float v638 = 1.3065629648763766e+00F;
    float v643 = -5.4119610014619690e-01F;
    const float32x2_t *v1538 = &v5[istride];
    float32x2_t *v1644 = &v6[ostride];
    float32x2_t v589 = (float32x2_t){v587, v588};
    float32x2_t v597 = (float32x2_t){v604, v596};
    float32x2_t v605 = (float32x2_t){v604, v604};
    float32x2_t v610 = (float32x2_t){v608, v609};
    float32x2_t v618 = (float32x2_t){v643, v617};
    float32x2_t v626 = (float32x2_t){v638, v625};
    float32x2_t v634 = (float32x2_t){v633, v633};
    float32x2_t v639 = (float32x2_t){v638, v638};
    float32x2_t v644 = (float32x2_t){v643, v643};
    const float32x2_t *v1625 = &v5[0];
    float32x2_t *v1635 = &v6[0];
    float32x4_t v1788 = vld1q_f32((const float32_t *)v1538);
    float32x4_t v47 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v49 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v97 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v99 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v109 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v111 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v159 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v161 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v171 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v173 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v221 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v223 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v233 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v235 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v278 = vtrn1q_f32(v1788, v1788);
    float32x4_t v279 = vtrn2q_f32(v1788, v1788);
    float32x4_t v283 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v285 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v295 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v297 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v345 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v347 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v357 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v359 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v407 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v409 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v419 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v421 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v469 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v471 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v481 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[28]));
    float32x4_t v483 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[29]));
    float32x2_t v591 = vmul_f32(v627, v589);
    float32x2_t v599 = vmul_f32(v627, v597);
    float32x4_t v606 = vcombine_f32(v605, v605);
    float32x2_t v612 = vmul_f32(v627, v610);
    float32x2_t v620 = vmul_f32(v627, v618);
    float32x2_t v628 = vmul_f32(v627, v626);
    float32x4_t v635 = vcombine_f32(v634, v634);
    float32x4_t v640 = vcombine_f32(v639, v639);
    float32x4_t v645 = vcombine_f32(v644, v644);
    const float32x2_t *v1461 = &v5[istride * 8];
    const float32x2_t *v1472 = &v5[istride * 4];
    const float32x2_t *v1482 = &v5[istride * 12];
    const float32x2_t *v1494 = &v5[istride * 2];
    const float32x2_t *v1504 = &v5[istride * 10];
    const float32x2_t *v1516 = &v5[istride * 6];
    const float32x2_t *v1526 = &v5[istride * 14];
    const float32x2_t *v1547 = &v5[istride * 9];
    const float32x2_t *v1558 = &v5[istride * 5];
    const float32x2_t *v1568 = &v5[istride * 13];
    const float32x2_t *v1580 = &v5[istride * 3];
    const float32x2_t *v1590 = &v5[istride * 11];
    const float32x2_t *v1602 = &v5[istride * 7];
    const float32x2_t *v1612 = &v5[istride * 15];
    float32x2_t *v1653 = &v6[ostride * 2];
    float32x2_t *v1662 = &v6[ostride * 3];
    float32x2_t *v1671 = &v6[ostride * 4];
    float32x2_t *v1680 = &v6[ostride * 5];
    float32x2_t *v1689 = &v6[ostride * 6];
    float32x2_t *v1698 = &v6[ostride * 7];
    float32x2_t *v1707 = &v6[ostride * 8];
    float32x2_t *v1716 = &v6[ostride * 9];
    float32x2_t *v1725 = &v6[ostride * 10];
    float32x2_t *v1734 = &v6[ostride * 11];
    float32x2_t *v1743 = &v6[ostride * 12];
    float32x2_t *v1752 = &v6[ostride * 13];
    float32x2_t *v1761 = &v6[ostride * 14];
    float32x2_t *v1770 = &v6[ostride * 15];
    float32x4_t v1804 = vld1q_f32((const float32_t *)v1625);
    float32x4_t v284 = vmulq_f32(v278, v283);
    float32x4_t v593 = vcombine_f32(v591, v591);
    float32x4_t v601 = vcombine_f32(v599, v599);
    float32x4_t v614 = vcombine_f32(v612, v612);
    float32x4_t v622 = vcombine_f32(v620, v620);
    float32x4_t v630 = vcombine_f32(v628, v628);
    float32x4_t v1774 = vld1q_f32((const float32_t *)v1461);
    float32x4_t v1776 = vld1q_f32((const float32_t *)v1472);
    float32x4_t v1778 = vld1q_f32((const float32_t *)v1482);
    float32x4_t v1780 = vld1q_f32((const float32_t *)v1494);
    float32x4_t v1782 = vld1q_f32((const float32_t *)v1504);
    float32x4_t v1784 = vld1q_f32((const float32_t *)v1516);
    float32x4_t v1786 = vld1q_f32((const float32_t *)v1526);
    float32x4_t v1790 = vld1q_f32((const float32_t *)v1547);
    float32x4_t v1792 = vld1q_f32((const float32_t *)v1558);
    float32x4_t v1794 = vld1q_f32((const float32_t *)v1568);
    float32x4_t v1796 = vld1q_f32((const float32_t *)v1580);
    float32x4_t v1798 = vld1q_f32((const float32_t *)v1590);
    float32x4_t v1800 = vld1q_f32((const float32_t *)v1602);
    float32x4_t v1802 = vld1q_f32((const float32_t *)v1612);
    float32x4_t v42 = vtrn1q_f32(v1774, v1774);
    float32x4_t v43 = vtrn2q_f32(v1774, v1774);
    float32x4_t v92 = vtrn1q_f32(v1776, v1776);
    float32x4_t v93 = vtrn2q_f32(v1776, v1776);
    float32x4_t v104 = vtrn1q_f32(v1778, v1778);
    float32x4_t v105 = vtrn2q_f32(v1778, v1778);
    float32x4_t v154 = vtrn1q_f32(v1780, v1780);
    float32x4_t v155 = vtrn2q_f32(v1780, v1780);
    float32x4_t v166 = vtrn1q_f32(v1782, v1782);
    float32x4_t v167 = vtrn2q_f32(v1782, v1782);
    float32x4_t v216 = vtrn1q_f32(v1784, v1784);
    float32x4_t v217 = vtrn2q_f32(v1784, v1784);
    float32x4_t v228 = vtrn1q_f32(v1786, v1786);
    float32x4_t v229 = vtrn2q_f32(v1786, v1786);
    float32x4_t v287 = vfmaq_f32(v284, v279, v285);
    float32x4_t v290 = vtrn1q_f32(v1790, v1790);
    float32x4_t v291 = vtrn2q_f32(v1790, v1790);
    float32x4_t v340 = vtrn1q_f32(v1792, v1792);
    float32x4_t v341 = vtrn2q_f32(v1792, v1792);
    float32x4_t v352 = vtrn1q_f32(v1794, v1794);
    float32x4_t v353 = vtrn2q_f32(v1794, v1794);
    float32x4_t v402 = vtrn1q_f32(v1796, v1796);
    float32x4_t v403 = vtrn2q_f32(v1796, v1796);
    float32x4_t v414 = vtrn1q_f32(v1798, v1798);
    float32x4_t v415 = vtrn2q_f32(v1798, v1798);
    float32x4_t v464 = vtrn1q_f32(v1800, v1800);
    float32x4_t v465 = vtrn2q_f32(v1800, v1800);
    float32x4_t v476 = vtrn1q_f32(v1802, v1802);
    float32x4_t v477 = vtrn2q_f32(v1802, v1802);
    float32x4_t v48 = vmulq_f32(v42, v47);
    float32x4_t v98 = vmulq_f32(v92, v97);
    float32x4_t v110 = vmulq_f32(v104, v109);
    float32x4_t v160 = vmulq_f32(v154, v159);
    float32x4_t v172 = vmulq_f32(v166, v171);
    float32x4_t v222 = vmulq_f32(v216, v221);
    float32x4_t v234 = vmulq_f32(v228, v233);
    float32x4_t v296 = vmulq_f32(v290, v295);
    float32x4_t v346 = vmulq_f32(v340, v345);
    float32x4_t v358 = vmulq_f32(v352, v357);
    float32x4_t v408 = vmulq_f32(v402, v407);
    float32x4_t v420 = vmulq_f32(v414, v419);
    float32x4_t v470 = vmulq_f32(v464, v469);
    float32x4_t v482 = vmulq_f32(v476, v481);
    float32x4_t v51 = vfmaq_f32(v48, v43, v49);
    float32x4_t v101 = vfmaq_f32(v98, v93, v99);
    float32x4_t v113 = vfmaq_f32(v110, v105, v111);
    float32x4_t v163 = vfmaq_f32(v160, v155, v161);
    float32x4_t v175 = vfmaq_f32(v172, v167, v173);
    float32x4_t v225 = vfmaq_f32(v222, v217, v223);
    float32x4_t v237 = vfmaq_f32(v234, v229, v235);
    float32x4_t v299 = vfmaq_f32(v296, v291, v297);
    float32x4_t v349 = vfmaq_f32(v346, v341, v347);
    float32x4_t v361 = vfmaq_f32(v358, v353, v359);
    float32x4_t v411 = vfmaq_f32(v408, v403, v409);
    float32x4_t v423 = vfmaq_f32(v420, v415, v421);
    float32x4_t v473 = vfmaq_f32(v470, v465, v471);
    float32x4_t v485 = vfmaq_f32(v482, v477, v483);
    float32x4_t v493 = vaddq_f32(v1804, v51);
    float32x4_t v494 = vsubq_f32(v1804, v51);
    float32x4_t v495 = vaddq_f32(v101, v113);
    float32x4_t v496 = vsubq_f32(v101, v113);
    float32x4_t v497 = vaddq_f32(v163, v175);
    float32x4_t v498 = vsubq_f32(v163, v175);
    float32x4_t v499 = vaddq_f32(v225, v237);
    float32x4_t v500 = vsubq_f32(v225, v237);
    float32x4_t v501 = vaddq_f32(v287, v299);
    float32x4_t v502 = vsubq_f32(v287, v299);
    float32x4_t v503 = vaddq_f32(v349, v361);
    float32x4_t v504 = vsubq_f32(v349, v361);
    float32x4_t v505 = vaddq_f32(v411, v423);
    float32x4_t v506 = vsubq_f32(v411, v423);
    float32x4_t v507 = vaddq_f32(v473, v485);
    float32x4_t v508 = vsubq_f32(v473, v485);
    float32x4_t v509 = vaddq_f32(v493, v495);
    float32x4_t v510 = vsubq_f32(v493, v495);
    float32x4_t v511 = vaddq_f32(v497, v499);
    float32x4_t v512 = vsubq_f32(v497, v499);
    float32x4_t v513 = vaddq_f32(v501, v503);
    float32x4_t v514 = vsubq_f32(v501, v503);
    float32x4_t v515 = vaddq_f32(v505, v507);
    float32x4_t v516 = vsubq_f32(v505, v507);
    float32x4_t v525 = vaddq_f32(v498, v500);
    float32x4_t v526 = vsubq_f32(v498, v500);
    float32x4_t v527 = vaddq_f32(v502, v508);
    float32x4_t v528 = vsubq_f32(v502, v508);
    float32x4_t v529 = vaddq_f32(v504, v506);
    float32x4_t v530 = vsubq_f32(v504, v506);
    float32x4_t v592 = vrev64q_f32(v496);
    float32x4_t v517 = vaddq_f32(v509, v511);
    float32x4_t v518 = vsubq_f32(v509, v511);
    float32x4_t v519 = vaddq_f32(v513, v515);
    float32x4_t v520 = vsubq_f32(v513, v515);
    float32x4_t v523 = vaddq_f32(v514, v516);
    float32x4_t v524 = vsubq_f32(v514, v516);
    float32x4_t v531 = vaddq_f32(v527, v529);
    float32x4_t v532 = vaddq_f32(v528, v530);
    float32x4_t v566 = vrev64q_f32(v512);
    float32x4_t v594 = vmulq_f32(v592, v593);
    float32x4_t v600 = vrev64q_f32(v525);
    float32x4_t v607 = vmulq_f32(v526, v606);
    float32x4_t v621 = vrev64q_f32(v527);
    float32x4_t v629 = vrev64q_f32(v529);
    float32x4_t v641 = vmulq_f32(v528, v640);
    float32x4_t v646 = vmulq_f32(v530, v645);
    float32x4_t v521 = vaddq_f32(v517, v519);
    float32x4_t v522 = vsubq_f32(v517, v519);
    float32x4_t v553 = vrev64q_f32(v520);
    float32x4_t v568 = vmulq_f32(v566, v593);
    float32x4_t v574 = vrev64q_f32(v523);
    float32x4_t v581 = vmulq_f32(v524, v606);
    float32x4_t v602 = vmulq_f32(v600, v601);
    float32x4_t v613 = vrev64q_f32(v531);
    float32x4_t v623 = vmulq_f32(v621, v622);
    float32x4_t v631 = vmulq_f32(v629, v630);
    float32x4_t v636 = vmulq_f32(v532, v635);
    float32x4_t v657 = vaddq_f32(v494, v607);
    float32x4_t v658 = vsubq_f32(v494, v607);
    float32x4_t v555 = vmulq_f32(v553, v593);
    float32x4_t v576 = vmulq_f32(v574, v601);
    float32x4_t v615 = vmulq_f32(v613, v614);
    float32x4_t v649 = vaddq_f32(v510, v581);
    float32x4_t v651 = vsubq_f32(v510, v581);
    float32x4_t v659 = vaddq_f32(v594, v602);
    float32x4_t v660 = vsubq_f32(v594, v602);
    float32x4_t v663 = vsubq_f32(v641, v636);
    float32x4_t v664 = vsubq_f32(v646, v636);
    float32x4_t v665 = vsubq_f32(v636, v641);
    float32x4_t v666 = vsubq_f32(v636, v646);
    vst1q_f32((float32_t *)v1635, v521);
    vst1q_f32((float32_t *)v1707, v522);
    float32x4_t v647 = vaddq_f32(v518, v555);
    float32x4_t v648 = vsubq_f32(v518, v555);
    float32x4_t v650 = vaddq_f32(v568, v576);
    float32x4_t v652 = vsubq_f32(v576, v568);
    float32x4_t v661 = vaddq_f32(v615, v623);
    float32x4_t v662 = vsubq_f32(v615, v631);
    float32x4_t v667 = vaddq_f32(v657, v663);
    float32x4_t v668 = vsubq_f32(v657, v663);
    float32x4_t v669 = vaddq_f32(v657, v665);
    float32x4_t v670 = vsubq_f32(v657, v665);
    float32x4_t v671 = vaddq_f32(v658, v660);
    float32x4_t v672 = vsubq_f32(v658, v660);
    float32x4_t v673 = vaddq_f32(v658, v666);
    float32x4_t v674 = vsubq_f32(v658, v666);
    float32x4_t v653 = vaddq_f32(v649, v650);
    float32x4_t v654 = vaddq_f32(v651, v652);
    float32x4_t v655 = vsubq_f32(v651, v652);
    float32x4_t v656 = vsubq_f32(v649, v650);
    float32x4_t v677 = vaddq_f32(v661, v659);
    float32x4_t v678 = vsubq_f32(v661, v659);
    float32x4_t v679 = vaddq_f32(v662, v664);
    float32x4_t v680 = vsubq_f32(v662, v664);
    float32x4_t v681 = vaddq_f32(v662, v660);
    float32x4_t v682 = vsubq_f32(v662, v660);
    vst1q_f32((float32_t *)v1671, v648);
    vst1q_f32((float32_t *)v1743, v647);
    float32x4_t v683 = vaddq_f32(v667, v677);
    float32x4_t v684 = vaddq_f32(v668, v678);
    float32x4_t v685 = vsubq_f32(v669, v678);
    float32x4_t v686 = vsubq_f32(v670, v677);
    float32x4_t v687 = vaddq_f32(v671, v679);
    float32x4_t v688 = vaddq_f32(v672, v680);
    float32x4_t v689 = vsubq_f32(v673, v682);
    float32x4_t v690 = vsubq_f32(v674, v681);
    vst1q_f32((float32_t *)v1653, v656);
    vst1q_f32((float32_t *)v1689, v655);
    vst1q_f32((float32_t *)v1725, v654);
    vst1q_f32((float32_t *)v1761, v653);
    vst1q_f32((float32_t *)v1644, v686);
    vst1q_f32((float32_t *)v1662, v689);
    vst1q_f32((float32_t *)v1680, v690);
    vst1q_f32((float32_t *)v1698, v685);
    vst1q_f32((float32_t *)v1716, v684);
    vst1q_f32((float32_t *)v1734, v687);
    vst1q_f32((float32_t *)v1752, v688);
    vst1q_f32((float32_t *)v1770, v683);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v803 * 2; j < howmany; j += 1) {
    float32x2_t v990 = v5[istride];
    float v1276 = 1.0000000000000000e+00F;
    float v1277 = -1.0000000000000000e+00F;
    float v1284 = -7.0710678118654746e-01F;
    float v1291 = 7.0710678118654757e-01F;
    float v1294 = 9.2387953251128674e-01F;
    float v1295 = -9.2387953251128674e-01F;
    float v1302 = 5.4119610014619690e-01F;
    float v1309 = -1.3065629648763766e+00F;
    float32x2_t v1311 = (float32x2_t){v4, v4};
    float v1316 = 3.8268343236508984e-01F;
    float v1320 = 1.3065629648763766e+00F;
    float v1324 = -5.4119610014619690e-01F;
    float32x2_t v827 = v7[14];
    float32x2_t v832 = v7[15];
    float32x2_t v867 = v7[6];
    float32x2_t v872 = v7[7];
    float32x2_t v877 = v7[22];
    float32x2_t v882 = v7[23];
    float32x2_t v917 = v7[2];
    float32x2_t v922 = v7[3];
    float32x2_t v927 = v7[18];
    float32x2_t v932 = v7[19];
    float32x2_t v967 = v7[10];
    float32x2_t v972 = v7[11];
    float32x2_t v977 = v7[26];
    float32x2_t v982 = v7[27];
    float32x2_t v1017 = v7[0];
    float32x2_t v1018 = vtrn1_f32(v990, v990);
    float32x2_t v1019 = vtrn2_f32(v990, v990);
    float32x2_t v1022 = v7[1];
    float32x2_t v1027 = v7[16];
    float32x2_t v1032 = v7[17];
    float32x2_t v1067 = v7[8];
    float32x2_t v1072 = v7[9];
    float32x2_t v1077 = v7[24];
    float32x2_t v1082 = v7[25];
    float32x2_t v1117 = v7[4];
    float32x2_t v1122 = v7[5];
    float32x2_t v1127 = v7[20];
    float32x2_t v1132 = v7[21];
    float32x2_t v1167 = v7[12];
    float32x2_t v1172 = v7[13];
    float32x2_t v1177 = v7[28];
    float32x2_t v1182 = v7[29];
    float32x2_t v1190 = v5[0];
    float32x2_t v1278 = (float32x2_t){v1276, v1277};
    float32x2_t v1285 = (float32x2_t){v1291, v1284};
    float32x2_t v1292 = (float32x2_t){v1291, v1291};
    float32x2_t v1296 = (float32x2_t){v1294, v1295};
    float32x2_t v1303 = (float32x2_t){v1324, v1302};
    float32x2_t v1310 = (float32x2_t){v1320, v1309};
    float32x2_t v1317 = (float32x2_t){v1316, v1316};
    float32x2_t v1321 = (float32x2_t){v1320, v1320};
    float32x2_t v1325 = (float32x2_t){v1324, v1324};
    float32x2_t v815 = v5[istride * 8];
    float32x2_t v840 = v5[istride * 4];
    float32x2_t v855 = v5[istride * 12];
    float32x2_t v890 = v5[istride * 2];
    float32x2_t v905 = v5[istride * 10];
    float32x2_t v940 = v5[istride * 6];
    float32x2_t v955 = v5[istride * 14];
    float32x2_t v1005 = v5[istride * 9];
    float32x2_t v1023 = vmul_f32(v1018, v1017);
    float32x2_t v1040 = v5[istride * 5];
    float32x2_t v1055 = v5[istride * 13];
    float32x2_t v1090 = v5[istride * 3];
    float32x2_t v1105 = v5[istride * 11];
    float32x2_t v1140 = v5[istride * 7];
    float32x2_t v1155 = v5[istride * 15];
    float32x2_t v1280 = vmul_f32(v1311, v1278);
    float32x2_t v1287 = vmul_f32(v1311, v1285);
    float32x2_t v1298 = vmul_f32(v1311, v1296);
    float32x2_t v1305 = vmul_f32(v1311, v1303);
    float32x2_t v1312 = vmul_f32(v1311, v1310);
    float32x2_t v828 = vtrn1_f32(v815, v815);
    float32x2_t v829 = vtrn2_f32(v815, v815);
    float32x2_t v868 = vtrn1_f32(v840, v840);
    float32x2_t v869 = vtrn2_f32(v840, v840);
    float32x2_t v878 = vtrn1_f32(v855, v855);
    float32x2_t v879 = vtrn2_f32(v855, v855);
    float32x2_t v918 = vtrn1_f32(v890, v890);
    float32x2_t v919 = vtrn2_f32(v890, v890);
    float32x2_t v928 = vtrn1_f32(v905, v905);
    float32x2_t v929 = vtrn2_f32(v905, v905);
    float32x2_t v968 = vtrn1_f32(v940, v940);
    float32x2_t v969 = vtrn2_f32(v940, v940);
    float32x2_t v978 = vtrn1_f32(v955, v955);
    float32x2_t v979 = vtrn2_f32(v955, v955);
    float32x2_t v1025 = vfma_f32(v1023, v1019, v1022);
    float32x2_t v1028 = vtrn1_f32(v1005, v1005);
    float32x2_t v1029 = vtrn2_f32(v1005, v1005);
    float32x2_t v1068 = vtrn1_f32(v1040, v1040);
    float32x2_t v1069 = vtrn2_f32(v1040, v1040);
    float32x2_t v1078 = vtrn1_f32(v1055, v1055);
    float32x2_t v1079 = vtrn2_f32(v1055, v1055);
    float32x2_t v1118 = vtrn1_f32(v1090, v1090);
    float32x2_t v1119 = vtrn2_f32(v1090, v1090);
    float32x2_t v1128 = vtrn1_f32(v1105, v1105);
    float32x2_t v1129 = vtrn2_f32(v1105, v1105);
    float32x2_t v1168 = vtrn1_f32(v1140, v1140);
    float32x2_t v1169 = vtrn2_f32(v1140, v1140);
    float32x2_t v1178 = vtrn1_f32(v1155, v1155);
    float32x2_t v1179 = vtrn2_f32(v1155, v1155);
    float32x2_t v833 = vmul_f32(v828, v827);
    float32x2_t v873 = vmul_f32(v868, v867);
    float32x2_t v883 = vmul_f32(v878, v877);
    float32x2_t v923 = vmul_f32(v918, v917);
    float32x2_t v933 = vmul_f32(v928, v927);
    float32x2_t v973 = vmul_f32(v968, v967);
    float32x2_t v983 = vmul_f32(v978, v977);
    float32x2_t v1033 = vmul_f32(v1028, v1027);
    float32x2_t v1073 = vmul_f32(v1068, v1067);
    float32x2_t v1083 = vmul_f32(v1078, v1077);
    float32x2_t v1123 = vmul_f32(v1118, v1117);
    float32x2_t v1133 = vmul_f32(v1128, v1127);
    float32x2_t v1173 = vmul_f32(v1168, v1167);
    float32x2_t v1183 = vmul_f32(v1178, v1177);
    float32x2_t v835 = vfma_f32(v833, v829, v832);
    float32x2_t v875 = vfma_f32(v873, v869, v872);
    float32x2_t v885 = vfma_f32(v883, v879, v882);
    float32x2_t v925 = vfma_f32(v923, v919, v922);
    float32x2_t v935 = vfma_f32(v933, v929, v932);
    float32x2_t v975 = vfma_f32(v973, v969, v972);
    float32x2_t v985 = vfma_f32(v983, v979, v982);
    float32x2_t v1035 = vfma_f32(v1033, v1029, v1032);
    float32x2_t v1075 = vfma_f32(v1073, v1069, v1072);
    float32x2_t v1085 = vfma_f32(v1083, v1079, v1082);
    float32x2_t v1125 = vfma_f32(v1123, v1119, v1122);
    float32x2_t v1135 = vfma_f32(v1133, v1129, v1132);
    float32x2_t v1175 = vfma_f32(v1173, v1169, v1172);
    float32x2_t v1185 = vfma_f32(v1183, v1179, v1182);
    float32x2_t v1191 = vadd_f32(v1190, v835);
    float32x2_t v1192 = vsub_f32(v1190, v835);
    float32x2_t v1193 = vadd_f32(v875, v885);
    float32x2_t v1194 = vsub_f32(v875, v885);
    float32x2_t v1195 = vadd_f32(v925, v935);
    float32x2_t v1196 = vsub_f32(v925, v935);
    float32x2_t v1197 = vadd_f32(v975, v985);
    float32x2_t v1198 = vsub_f32(v975, v985);
    float32x2_t v1199 = vadd_f32(v1025, v1035);
    float32x2_t v1200 = vsub_f32(v1025, v1035);
    float32x2_t v1201 = vadd_f32(v1075, v1085);
    float32x2_t v1202 = vsub_f32(v1075, v1085);
    float32x2_t v1203 = vadd_f32(v1125, v1135);
    float32x2_t v1204 = vsub_f32(v1125, v1135);
    float32x2_t v1205 = vadd_f32(v1175, v1185);
    float32x2_t v1206 = vsub_f32(v1175, v1185);
    float32x2_t v1207 = vadd_f32(v1191, v1193);
    float32x2_t v1208 = vsub_f32(v1191, v1193);
    float32x2_t v1209 = vadd_f32(v1195, v1197);
    float32x2_t v1210 = vsub_f32(v1195, v1197);
    float32x2_t v1211 = vadd_f32(v1199, v1201);
    float32x2_t v1212 = vsub_f32(v1199, v1201);
    float32x2_t v1213 = vadd_f32(v1203, v1205);
    float32x2_t v1214 = vsub_f32(v1203, v1205);
    float32x2_t v1223 = vadd_f32(v1196, v1198);
    float32x2_t v1224 = vsub_f32(v1196, v1198);
    float32x2_t v1225 = vadd_f32(v1200, v1206);
    float32x2_t v1226 = vsub_f32(v1200, v1206);
    float32x2_t v1227 = vadd_f32(v1202, v1204);
    float32x2_t v1228 = vsub_f32(v1202, v1204);
    float32x2_t v1281 = vrev64_f32(v1194);
    float32x2_t v1215 = vadd_f32(v1207, v1209);
    float32x2_t v1216 = vsub_f32(v1207, v1209);
    float32x2_t v1217 = vadd_f32(v1211, v1213);
    float32x2_t v1218 = vsub_f32(v1211, v1213);
    float32x2_t v1221 = vadd_f32(v1212, v1214);
    float32x2_t v1222 = vsub_f32(v1212, v1214);
    float32x2_t v1229 = vadd_f32(v1225, v1227);
    float32x2_t v1230 = vadd_f32(v1226, v1228);
    float32x2_t v1259 = vrev64_f32(v1210);
    float32x2_t v1282 = vmul_f32(v1281, v1280);
    float32x2_t v1288 = vrev64_f32(v1223);
    float32x2_t v1293 = vmul_f32(v1224, v1292);
    float32x2_t v1306 = vrev64_f32(v1225);
    float32x2_t v1313 = vrev64_f32(v1227);
    float32x2_t v1322 = vmul_f32(v1226, v1321);
    float32x2_t v1326 = vmul_f32(v1228, v1325);
    float32x2_t v1219 = vadd_f32(v1215, v1217);
    float32x2_t v1220 = vsub_f32(v1215, v1217);
    float32x2_t v1248 = vrev64_f32(v1218);
    float32x2_t v1260 = vmul_f32(v1259, v1280);
    float32x2_t v1266 = vrev64_f32(v1221);
    float32x2_t v1271 = vmul_f32(v1222, v1292);
    float32x2_t v1289 = vmul_f32(v1288, v1287);
    float32x2_t v1299 = vrev64_f32(v1229);
    float32x2_t v1307 = vmul_f32(v1306, v1305);
    float32x2_t v1314 = vmul_f32(v1313, v1312);
    float32x2_t v1318 = vmul_f32(v1230, v1317);
    float32x2_t v1337 = vadd_f32(v1192, v1293);
    float32x2_t v1338 = vsub_f32(v1192, v1293);
    float32x2_t v1249 = vmul_f32(v1248, v1280);
    float32x2_t v1267 = vmul_f32(v1266, v1287);
    float32x2_t v1300 = vmul_f32(v1299, v1298);
    float32x2_t v1329 = vadd_f32(v1208, v1271);
    float32x2_t v1331 = vsub_f32(v1208, v1271);
    float32x2_t v1339 = vadd_f32(v1282, v1289);
    float32x2_t v1340 = vsub_f32(v1282, v1289);
    float32x2_t v1343 = vsub_f32(v1322, v1318);
    float32x2_t v1344 = vsub_f32(v1326, v1318);
    float32x2_t v1345 = vsub_f32(v1318, v1322);
    float32x2_t v1346 = vsub_f32(v1318, v1326);
    v6[0] = v1219;
    v6[ostride * 8] = v1220;
    float32x2_t v1327 = vadd_f32(v1216, v1249);
    float32x2_t v1328 = vsub_f32(v1216, v1249);
    float32x2_t v1330 = vadd_f32(v1260, v1267);
    float32x2_t v1332 = vsub_f32(v1267, v1260);
    float32x2_t v1341 = vadd_f32(v1300, v1307);
    float32x2_t v1342 = vsub_f32(v1300, v1314);
    float32x2_t v1347 = vadd_f32(v1337, v1343);
    float32x2_t v1348 = vsub_f32(v1337, v1343);
    float32x2_t v1349 = vadd_f32(v1337, v1345);
    float32x2_t v1350 = vsub_f32(v1337, v1345);
    float32x2_t v1351 = vadd_f32(v1338, v1340);
    float32x2_t v1352 = vsub_f32(v1338, v1340);
    float32x2_t v1353 = vadd_f32(v1338, v1346);
    float32x2_t v1354 = vsub_f32(v1338, v1346);
    float32x2_t v1333 = vadd_f32(v1329, v1330);
    float32x2_t v1334 = vadd_f32(v1331, v1332);
    float32x2_t v1335 = vsub_f32(v1331, v1332);
    float32x2_t v1336 = vsub_f32(v1329, v1330);
    float32x2_t v1357 = vadd_f32(v1341, v1339);
    float32x2_t v1358 = vsub_f32(v1341, v1339);
    float32x2_t v1359 = vadd_f32(v1342, v1344);
    float32x2_t v1360 = vsub_f32(v1342, v1344);
    float32x2_t v1361 = vadd_f32(v1342, v1340);
    float32x2_t v1362 = vsub_f32(v1342, v1340);
    v6[ostride * 4] = v1328;
    v6[ostride * 12] = v1327;
    float32x2_t v1363 = vadd_f32(v1347, v1357);
    float32x2_t v1364 = vadd_f32(v1348, v1358);
    float32x2_t v1365 = vsub_f32(v1349, v1358);
    float32x2_t v1366 = vsub_f32(v1350, v1357);
    float32x2_t v1367 = vadd_f32(v1351, v1359);
    float32x2_t v1368 = vadd_f32(v1352, v1360);
    float32x2_t v1369 = vsub_f32(v1353, v1362);
    float32x2_t v1370 = vsub_f32(v1354, v1361);
    v6[ostride * 2] = v1336;
    v6[ostride * 6] = v1335;
    v6[ostride * 10] = v1334;
    v6[ostride * 14] = v1333;
    v6[ostride] = v1366;
    v6[ostride * 3] = v1369;
    v6[ostride * 5] = v1370;
    v6[ostride * 7] = v1365;
    v6[ostride * 9] = v1364;
    v6[ostride * 11] = v1367;
    v6[ostride * 13] = v1368;
    v6[ostride * 15] = v1363;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu16(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v342 = -1.0000000000000000e+00F;
    float v349 = -7.0710678118654746e-01F;
    float v356 = 7.0710678118654757e-01F;
    float v361 = -9.2387953251128674e-01F;
    float v368 = 5.4119610014619690e-01F;
    float v375 = -1.3065629648763766e+00F;
    float v382 = 3.8268343236508984e-01F;
    float v387 = 1.3065629648763766e+00F;
    float v392 = -5.4119610014619690e-01F;
    const float32x2_t *v621 = &v5[v0];
    float32x2_t *v731 = &v6[v2];
    int64_t v19 = v0 * 8;
    int64_t v34 = v0 * 4;
    int64_t v45 = v0 * 12;
    int64_t v64 = v0 * 2;
    int64_t v75 = v0 * 10;
    int64_t v94 = v0 * 6;
    int64_t v105 = v0 * 14;
    int64_t v135 = v0 * 9;
    int64_t v154 = v0 * 5;
    int64_t v165 = v0 * 13;
    int64_t v184 = v0 * 3;
    int64_t v195 = v0 * 11;
    int64_t v214 = v0 * 7;
    int64_t v225 = v0 * 15;
    float v345 = v4 * v342;
    float v352 = v4 * v349;
    float v364 = v4 * v361;
    float v371 = v4 * v368;
    float v378 = v4 * v375;
    int64_t v455 = v2 * 2;
    int64_t v462 = v2 * 3;
    int64_t v469 = v2 * 4;
    int64_t v476 = v2 * 5;
    int64_t v483 = v2 * 6;
    int64_t v490 = v2 * 7;
    int64_t v497 = v2 * 8;
    int64_t v504 = v2 * 9;
    int64_t v511 = v2 * 10;
    int64_t v518 = v2 * 11;
    int64_t v525 = v2 * 12;
    int64_t v532 = v2 * 13;
    int64_t v539 = v2 * 14;
    int64_t v546 = v2 * 15;
    const float32x2_t *v694 = &v5[0];
    svfloat32_t v708 = svdup_n_f32(v356);
    svfloat32_t v712 = svdup_n_f32(v382);
    svfloat32_t v713 = svdup_n_f32(v387);
    svfloat32_t v714 = svdup_n_f32(v392);
    float32x2_t *v722 = &v6[0];
    svfloat32_t v875 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v621)[0]));
    svfloat32_t v31 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v57 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v61 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v87 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v91 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v117 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v121 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v147 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v151 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v177 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v181 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v207 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v211 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v237 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v241 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[14]));
    const float32x2_t *v558 = &v5[v19];
    const float32x2_t *v567 = &v5[v34];
    const float32x2_t *v576 = &v5[v45];
    const float32x2_t *v585 = &v5[v64];
    const float32x2_t *v594 = &v5[v75];
    const float32x2_t *v603 = &v5[v94];
    const float32x2_t *v612 = &v5[v105];
    const float32x2_t *v630 = &v5[v135];
    const float32x2_t *v639 = &v5[v154];
    const float32x2_t *v648 = &v5[v165];
    const float32x2_t *v657 = &v5[v184];
    const float32x2_t *v666 = &v5[v195];
    const float32x2_t *v675 = &v5[v214];
    const float32x2_t *v684 = &v5[v225];
    svfloat32_t v706 = svdup_n_f32(v345);
    svfloat32_t v707 = svdup_n_f32(v352);
    svfloat32_t v709 = svdup_n_f32(v364);
    svfloat32_t v710 = svdup_n_f32(v371);
    svfloat32_t v711 = svdup_n_f32(v378);
    float32x2_t *v740 = &v6[v455];
    float32x2_t *v749 = &v6[v462];
    float32x2_t *v758 = &v6[v469];
    float32x2_t *v767 = &v6[v476];
    float32x2_t *v776 = &v6[v483];
    float32x2_t *v785 = &v6[v490];
    float32x2_t *v794 = &v6[v497];
    float32x2_t *v803 = &v6[v504];
    float32x2_t *v812 = &v6[v511];
    float32x2_t *v821 = &v6[v518];
    float32x2_t *v830 = &v6[v525];
    float32x2_t *v839 = &v6[v532];
    float32x2_t *v848 = &v6[v539];
    float32x2_t *v857 = &v6[v546];
    svfloat32_t v891 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v694)[0]));
    svfloat32_t zero148 = svdup_n_f32(0);
    svfloat32_t v148 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero148, v875, v147, 0),
                     v875, v147, 90);
    svfloat32_t v861 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v558)[0]));
    svfloat32_t v863 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v567)[0]));
    svfloat32_t v865 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v576)[0]));
    svfloat32_t v867 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v585)[0]));
    svfloat32_t v869 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v594)[0]));
    svfloat32_t v871 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v603)[0]));
    svfloat32_t v873 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v612)[0]));
    svfloat32_t v877 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v630)[0]));
    svfloat32_t v879 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v639)[0]));
    svfloat32_t v881 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v648)[0]));
    svfloat32_t v883 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v657)[0]));
    svfloat32_t v885 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v666)[0]));
    svfloat32_t v887 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v675)[0]));
    svfloat32_t v889 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v684)[0]));
    svfloat32_t zero32 = svdup_n_f32(0);
    svfloat32_t v32 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero32, v861, v31, 0),
                     v861, v31, 90);
    svfloat32_t zero58 = svdup_n_f32(0);
    svfloat32_t v58 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero58, v863, v57, 0),
                     v863, v57, 90);
    svfloat32_t zero62 = svdup_n_f32(0);
    svfloat32_t v62 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero62, v865, v61, 0),
                     v865, v61, 90);
    svfloat32_t zero88 = svdup_n_f32(0);
    svfloat32_t v88 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero88, v867, v87, 0),
                     v867, v87, 90);
    svfloat32_t zero92 = svdup_n_f32(0);
    svfloat32_t v92 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero92, v869, v91, 0),
                     v869, v91, 90);
    svfloat32_t zero118 = svdup_n_f32(0);
    svfloat32_t v118 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero118, v871, v117, 0),
                     v871, v117, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v873, v121, 0),
                     v873, v121, 90);
    svfloat32_t zero152 = svdup_n_f32(0);
    svfloat32_t v152 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero152, v877, v151, 0),
                     v877, v151, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero178, v879, v177, 0),
                     v879, v177, 90);
    svfloat32_t zero182 = svdup_n_f32(0);
    svfloat32_t v182 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero182, v881, v181, 0),
                     v881, v181, 90);
    svfloat32_t zero208 = svdup_n_f32(0);
    svfloat32_t v208 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero208, v883, v207, 0),
                     v883, v207, 90);
    svfloat32_t zero212 = svdup_n_f32(0);
    svfloat32_t v212 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero212, v885, v211, 0),
                     v885, v211, 90);
    svfloat32_t zero238 = svdup_n_f32(0);
    svfloat32_t v238 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero238, v887, v237, 0),
                     v887, v237, 90);
    svfloat32_t zero242 = svdup_n_f32(0);
    svfloat32_t v242 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero242, v889, v241, 0),
                     v889, v241, 90);
    svfloat32_t v250 = svadd_f32_x(svptrue_b32(), v891, v32);
    svfloat32_t v251 = svsub_f32_x(svptrue_b32(), v891, v32);
    svfloat32_t v252 = svadd_f32_x(svptrue_b32(), v58, v62);
    svfloat32_t v253 = svsub_f32_x(svptrue_b32(), v58, v62);
    svfloat32_t v254 = svadd_f32_x(svptrue_b32(), v88, v92);
    svfloat32_t v255 = svsub_f32_x(svptrue_b32(), v88, v92);
    svfloat32_t v256 = svadd_f32_x(svptrue_b32(), v118, v122);
    svfloat32_t v257 = svsub_f32_x(svptrue_b32(), v118, v122);
    svfloat32_t v258 = svadd_f32_x(svptrue_b32(), v148, v152);
    svfloat32_t v259 = svsub_f32_x(svptrue_b32(), v148, v152);
    svfloat32_t v260 = svadd_f32_x(svptrue_b32(), v178, v182);
    svfloat32_t v261 = svsub_f32_x(svptrue_b32(), v178, v182);
    svfloat32_t v262 = svadd_f32_x(svptrue_b32(), v208, v212);
    svfloat32_t v263 = svsub_f32_x(svptrue_b32(), v208, v212);
    svfloat32_t v264 = svadd_f32_x(svptrue_b32(), v238, v242);
    svfloat32_t v265 = svsub_f32_x(svptrue_b32(), v238, v242);
    svfloat32_t v266 = svadd_f32_x(svptrue_b32(), v250, v252);
    svfloat32_t v267 = svsub_f32_x(svptrue_b32(), v250, v252);
    svfloat32_t v268 = svadd_f32_x(svptrue_b32(), v254, v256);
    svfloat32_t v269 = svsub_f32_x(svptrue_b32(), v254, v256);
    svfloat32_t v270 = svadd_f32_x(svptrue_b32(), v258, v260);
    svfloat32_t v271 = svsub_f32_x(svptrue_b32(), v258, v260);
    svfloat32_t v272 = svadd_f32_x(svptrue_b32(), v262, v264);
    svfloat32_t v273 = svsub_f32_x(svptrue_b32(), v262, v264);
    svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v255, v257);
    svfloat32_t v283 = svsub_f32_x(svptrue_b32(), v255, v257);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v259, v265);
    svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v259, v265);
    svfloat32_t v286 = svadd_f32_x(svptrue_b32(), v261, v263);
    svfloat32_t v287 = svsub_f32_x(svptrue_b32(), v261, v263);
    svfloat32_t zero347 = svdup_n_f32(0);
    svfloat32_t v347 = svcmla_f32_x(pred_full, zero347, v706, v253, 90);
    svfloat32_t v274 = svadd_f32_x(svptrue_b32(), v266, v268);
    svfloat32_t v275 = svsub_f32_x(svptrue_b32(), v266, v268);
    svfloat32_t v276 = svadd_f32_x(svptrue_b32(), v270, v272);
    svfloat32_t v277 = svsub_f32_x(svptrue_b32(), v270, v272);
    svfloat32_t v280 = svadd_f32_x(svptrue_b32(), v271, v273);
    svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v271, v273);
    svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v284, v286);
    svfloat32_t v289 = svadd_f32_x(svptrue_b32(), v285, v287);
    svfloat32_t zero323 = svdup_n_f32(0);
    svfloat32_t v323 = svcmla_f32_x(pred_full, zero323, v706, v269, 90);
    svfloat32_t zero354 = svdup_n_f32(0);
    svfloat32_t v354 = svcmla_f32_x(pred_full, zero354, v707, v282, 90);
    svfloat32_t zero380 = svdup_n_f32(0);
    svfloat32_t v380 = svcmla_f32_x(pred_full, zero380, v711, v286, 90);
    svfloat32_t v390 = svmul_f32_x(svptrue_b32(), v285, v713);
    svfloat32_t v395 = svmul_f32_x(svptrue_b32(), v287, v714);
    svfloat32_t v278 = svadd_f32_x(svptrue_b32(), v274, v276);
    svfloat32_t v279 = svsub_f32_x(svptrue_b32(), v274, v276);
    svfloat32_t zero311 = svdup_n_f32(0);
    svfloat32_t v311 = svcmla_f32_x(pred_full, zero311, v706, v277, 90);
    svfloat32_t zero330 = svdup_n_f32(0);
    svfloat32_t v330 = svcmla_f32_x(pred_full, zero330, v707, v280, 90);
    svfloat32_t zero366 = svdup_n_f32(0);
    svfloat32_t v366 = svcmla_f32_x(pred_full, zero366, v709, v288, 90);
    svfloat32_t v385 = svmul_f32_x(svptrue_b32(), v289, v712);
    svfloat32_t v406 = svmla_f32_x(pred_full, v251, v283, v708);
    svfloat32_t v407 = svmls_f32_x(pred_full, v251, v283, v708);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v347, v354);
    svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v347, v354);
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v275, v311);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v275, v311);
    svfloat32_t v398 = svmla_f32_x(pred_full, v267, v281, v708);
    svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v323, v330);
    svfloat32_t v400 = svmls_f32_x(pred_full, v267, v281, v708);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v330, v323);
    svfloat32_t v410 = svcmla_f32_x(pred_full, v366, v710, v284, 90);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v366, v380);
    svfloat32_t v412 = svnmls_f32_x(pred_full, v385, v285, v713);
    svfloat32_t v413 = svnmls_f32_x(pred_full, v385, v287, v714);
    svfloat32_t v414 = svnmls_f32_x(pred_full, v390, v289, v712);
    svfloat32_t v415 = svnmls_f32_x(pred_full, v395, v289, v712);
    svfloat32_t v420 = svadd_f32_x(svptrue_b32(), v407, v409);
    svfloat32_t v421 = svsub_f32_x(svptrue_b32(), v407, v409);
    svst1_f64(pred_full, (double *)(v722), svreinterpret_f64_f32(v278));
    svst1_f64(pred_full, (double *)(v794), svreinterpret_f64_f32(v279));
    svfloat32_t v402 = svadd_f32_x(svptrue_b32(), v398, v399);
    svfloat32_t v403 = svadd_f32_x(svptrue_b32(), v400, v401);
    svfloat32_t v404 = svsub_f32_x(svptrue_b32(), v400, v401);
    svfloat32_t v405 = svsub_f32_x(svptrue_b32(), v398, v399);
    svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v406, v412);
    svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v406, v412);
    svfloat32_t v418 = svadd_f32_x(svptrue_b32(), v406, v414);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v406, v414);
    svfloat32_t v422 = svadd_f32_x(svptrue_b32(), v407, v415);
    svfloat32_t v423 = svsub_f32_x(svptrue_b32(), v407, v415);
    svfloat32_t v426 = svadd_f32_x(svptrue_b32(), v410, v408);
    svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v410, v408);
    svfloat32_t v428 = svadd_f32_x(svptrue_b32(), v411, v413);
    svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v411, v413);
    svfloat32_t v430 = svadd_f32_x(svptrue_b32(), v411, v409);
    svfloat32_t v431 = svsub_f32_x(svptrue_b32(), v411, v409);
    svst1_f64(pred_full, (double *)(v758), svreinterpret_f64_f32(v397));
    svst1_f64(pred_full, (double *)(v830), svreinterpret_f64_f32(v396));
    svfloat32_t v432 = svadd_f32_x(svptrue_b32(), v416, v426);
    svfloat32_t v433 = svadd_f32_x(svptrue_b32(), v417, v427);
    svfloat32_t v434 = svsub_f32_x(svptrue_b32(), v418, v427);
    svfloat32_t v435 = svsub_f32_x(svptrue_b32(), v419, v426);
    svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v420, v428);
    svfloat32_t v437 = svadd_f32_x(svptrue_b32(), v421, v429);
    svfloat32_t v438 = svsub_f32_x(svptrue_b32(), v422, v431);
    svfloat32_t v439 = svsub_f32_x(svptrue_b32(), v423, v430);
    svst1_f64(pred_full, (double *)(v740), svreinterpret_f64_f32(v405));
    svst1_f64(pred_full, (double *)(v776), svreinterpret_f64_f32(v404));
    svst1_f64(pred_full, (double *)(v812), svreinterpret_f64_f32(v403));
    svst1_f64(pred_full, (double *)(v848), svreinterpret_f64_f32(v402));
    svst1_f64(pred_full, (double *)(v731), svreinterpret_f64_f32(v435));
    svst1_f64(pred_full, (double *)(v749), svreinterpret_f64_f32(v438));
    svst1_f64(pred_full, (double *)(v767), svreinterpret_f64_f32(v439));
    svst1_f64(pred_full, (double *)(v785), svreinterpret_f64_f32(v434));
    svst1_f64(pred_full, (double *)(v803), svreinterpret_f64_f32(v433));
    svst1_f64(pred_full, (double *)(v821), svreinterpret_f64_f32(v436));
    svst1_f64(pred_full, (double *)(v839), svreinterpret_f64_f32(v437));
    svst1_f64(pred_full, (double *)(v857), svreinterpret_f64_f32(v432));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu17(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v1043 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v597 = -4.2602849117736000e-02F;
    float v602 = 2.0497965023262180e-01F;
    float v607 = 1.0451835201736759e+00F;
    float v612 = 1.7645848660222969e+00F;
    float v617 = -7.2340797728605655e-01F;
    float v622 = -8.9055591620606403e-02F;
    float v627 = -1.0625000000000000e+00F;
    float v632 = 2.5769410160110379e-01F;
    float v637 = 7.7980260789483757e-01F;
    float v642 = 5.4389318464570580e-01F;
    float v647 = 4.2010193497052700e-01F;
    float v652 = 1.2810929434228073e+00F;
    float v657 = 4.4088907348175338e-01F;
    float v662 = 3.1717619283272508e-01F;
    float v666 = -9.0138318648016680e-01F;
    float v667 = 9.0138318648016680e-01F;
    float v674 = -4.3248756360072310e-01F;
    float v675 = 4.3248756360072310e-01F;
    float v682 = 6.6693537504044498e-01F;
    float v683 = -6.6693537504044498e-01F;
    float v690 = -6.0389004312516970e-01F;
    float v691 = 6.0389004312516970e-01F;
    float v698 = -3.6924873198582547e-01F;
    float v699 = 3.6924873198582547e-01F;
    float v706 = 4.8656938755549761e-01F;
    float v707 = -4.8656938755549761e-01F;
    float v714 = 2.3813712136760609e-01F;
    float v715 = -2.3813712136760609e-01F;
    float v722 = -1.5573820617422458e+00F;
    float v723 = 1.5573820617422458e+00F;
    float v730 = 6.5962247018731990e-01F;
    float v731 = -6.5962247018731990e-01F;
    float v738 = -1.4316961569866241e-01F;
    float v739 = 1.4316961569866241e-01F;
    float v746 = 2.3903469959860771e-01F;
    float v747 = -2.3903469959860771e-01F;
    float v754 = -4.7932541949972603e-02F;
    float v755 = 4.7932541949972603e-02F;
    float v762 = -2.3188014856550065e+00F;
    float v763 = 2.3188014856550065e+00F;
    float v770 = 7.8914568419206255e-01F;
    float v771 = -7.8914568419206255e-01F;
    float v778 = 3.8484572871179505e+00F;
    float v779 = -3.8484572871179505e+00F;
    float v786 = -1.3003804568801376e+00F;
    float v787 = 1.3003804568801376e+00F;
    float v794 = 4.0814769046889037e+00F;
    float v795 = -4.0814769046889037e+00F;
    float v802 = -1.4807159909286283e+00F;
    float v803 = 1.4807159909286283e+00F;
    float v810 = -1.3332470363551400e-02F;
    float v811 = 1.3332470363551400e-02F;
    float v818 = -3.7139778690557629e-01F;
    float v819 = 3.7139778690557629e-01F;
    float v826 = 1.9236512863456379e-01F;
    float v827 = -1.9236512863456379e-01F;
    float32x2_t v829 = (float32x2_t){v4, v4};
    const float32x2_t *v1915 = &v5[istride];
    float32x2_t *v2109 = &v6[ostride];
    float32x2_t v598 = (float32x2_t){v597, v597};
    float32x2_t v603 = (float32x2_t){v602, v602};
    float32x2_t v608 = (float32x2_t){v607, v607};
    float32x2_t v613 = (float32x2_t){v612, v612};
    float32x2_t v618 = (float32x2_t){v617, v617};
    float32x2_t v623 = (float32x2_t){v622, v622};
    float32x2_t v628 = (float32x2_t){v627, v627};
    float32x2_t v633 = (float32x2_t){v632, v632};
    float32x2_t v638 = (float32x2_t){v637, v637};
    float32x2_t v643 = (float32x2_t){v642, v642};
    float32x2_t v648 = (float32x2_t){v647, v647};
    float32x2_t v653 = (float32x2_t){v652, v652};
    float32x2_t v658 = (float32x2_t){v657, v657};
    float32x2_t v663 = (float32x2_t){v662, v662};
    float32x2_t v668 = (float32x2_t){v666, v667};
    float32x2_t v676 = (float32x2_t){v674, v675};
    float32x2_t v684 = (float32x2_t){v682, v683};
    float32x2_t v692 = (float32x2_t){v690, v691};
    float32x2_t v700 = (float32x2_t){v698, v699};
    float32x2_t v708 = (float32x2_t){v706, v707};
    float32x2_t v716 = (float32x2_t){v714, v715};
    float32x2_t v724 = (float32x2_t){v722, v723};
    float32x2_t v732 = (float32x2_t){v730, v731};
    float32x2_t v740 = (float32x2_t){v738, v739};
    float32x2_t v748 = (float32x2_t){v746, v747};
    float32x2_t v756 = (float32x2_t){v754, v755};
    float32x2_t v764 = (float32x2_t){v762, v763};
    float32x2_t v772 = (float32x2_t){v770, v771};
    float32x2_t v780 = (float32x2_t){v778, v779};
    float32x2_t v788 = (float32x2_t){v786, v787};
    float32x2_t v796 = (float32x2_t){v794, v795};
    float32x2_t v804 = (float32x2_t){v802, v803};
    float32x2_t v812 = (float32x2_t){v810, v811};
    float32x2_t v820 = (float32x2_t){v818, v819};
    float32x2_t v828 = (float32x2_t){v826, v827};
    const float32x2_t *v2090 = &v5[0];
    float32x2_t *v2100 = &v6[0];
    float32x4_t v2248 = vld1q_f32((const float32_t *)v1915);
    float32x4_t v61 = vtrn1q_f32(v2248, v2248);
    float32x4_t v62 = vtrn2q_f32(v2248, v2248);
    float32x4_t v66 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v68 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v78 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[30]));
    float32x4_t v80 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[31]));
    float32x4_t v128 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v130 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v140 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v142 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v190 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v192 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v202 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v204 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v252 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v254 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v264 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v266 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v314 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v316 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v326 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v328 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v376 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v378 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v388 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v390 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v438 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[28]));
    float32x4_t v440 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[29]));
    float32x4_t v450 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v452 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v500 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v502 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v512 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v514 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v599 = vcombine_f32(v598, v598);
    float32x4_t v604 = vcombine_f32(v603, v603);
    float32x4_t v609 = vcombine_f32(v608, v608);
    float32x4_t v614 = vcombine_f32(v613, v613);
    float32x4_t v619 = vcombine_f32(v618, v618);
    float32x4_t v624 = vcombine_f32(v623, v623);
    float32x4_t v629 = vcombine_f32(v628, v628);
    float32x4_t v634 = vcombine_f32(v633, v633);
    float32x4_t v639 = vcombine_f32(v638, v638);
    float32x4_t v644 = vcombine_f32(v643, v643);
    float32x4_t v649 = vcombine_f32(v648, v648);
    float32x4_t v654 = vcombine_f32(v653, v653);
    float32x4_t v659 = vcombine_f32(v658, v658);
    float32x4_t v664 = vcombine_f32(v663, v663);
    float32x2_t v670 = vmul_f32(v829, v668);
    float32x2_t v678 = vmul_f32(v829, v676);
    float32x2_t v686 = vmul_f32(v829, v684);
    float32x2_t v694 = vmul_f32(v829, v692);
    float32x2_t v702 = vmul_f32(v829, v700);
    float32x2_t v710 = vmul_f32(v829, v708);
    float32x2_t v718 = vmul_f32(v829, v716);
    float32x2_t v726 = vmul_f32(v829, v724);
    float32x2_t v734 = vmul_f32(v829, v732);
    float32x2_t v742 = vmul_f32(v829, v740);
    float32x2_t v750 = vmul_f32(v829, v748);
    float32x2_t v758 = vmul_f32(v829, v756);
    float32x2_t v766 = vmul_f32(v829, v764);
    float32x2_t v774 = vmul_f32(v829, v772);
    float32x2_t v782 = vmul_f32(v829, v780);
    float32x2_t v790 = vmul_f32(v829, v788);
    float32x2_t v798 = vmul_f32(v829, v796);
    float32x2_t v806 = vmul_f32(v829, v804);
    float32x2_t v814 = vmul_f32(v829, v812);
    float32x2_t v822 = vmul_f32(v829, v820);
    float32x2_t v830 = vmul_f32(v829, v828);
    const float32x2_t *v1924 = &v5[istride * 16];
    const float32x2_t *v1935 = &v5[istride * 3];
    const float32x2_t *v1945 = &v5[istride * 14];
    const float32x2_t *v1957 = &v5[istride * 9];
    const float32x2_t *v1967 = &v5[istride * 8];
    const float32x2_t *v1979 = &v5[istride * 10];
    const float32x2_t *v1989 = &v5[istride * 7];
    const float32x2_t *v2001 = &v5[istride * 13];
    const float32x2_t *v2011 = &v5[istride * 4];
    const float32x2_t *v2023 = &v5[istride * 5];
    const float32x2_t *v2033 = &v5[istride * 12];
    const float32x2_t *v2045 = &v5[istride * 15];
    const float32x2_t *v2055 = &v5[istride * 2];
    const float32x2_t *v2067 = &v5[istride * 11];
    const float32x2_t *v2077 = &v5[istride * 6];
    float32x2_t *v2118 = &v6[ostride * 16];
    float32x2_t *v2127 = &v6[ostride * 2];
    float32x2_t *v2136 = &v6[ostride * 15];
    float32x2_t *v2145 = &v6[ostride * 3];
    float32x2_t *v2154 = &v6[ostride * 14];
    float32x2_t *v2163 = &v6[ostride * 4];
    float32x2_t *v2172 = &v6[ostride * 13];
    float32x2_t *v2181 = &v6[ostride * 5];
    float32x2_t *v2190 = &v6[ostride * 12];
    float32x2_t *v2199 = &v6[ostride * 6];
    float32x2_t *v2208 = &v6[ostride * 11];
    float32x2_t *v2217 = &v6[ostride * 7];
    float32x2_t *v2226 = &v6[ostride * 10];
    float32x2_t *v2235 = &v6[ostride * 8];
    float32x2_t *v2244 = &v6[ostride * 9];
    float32x4_t v2280 = vld1q_f32((const float32_t *)v2090);
    float32x4_t v67 = vmulq_f32(v61, v66);
    float32x4_t v672 = vcombine_f32(v670, v670);
    float32x4_t v680 = vcombine_f32(v678, v678);
    float32x4_t v688 = vcombine_f32(v686, v686);
    float32x4_t v696 = vcombine_f32(v694, v694);
    float32x4_t v704 = vcombine_f32(v702, v702);
    float32x4_t v712 = vcombine_f32(v710, v710);
    float32x4_t v720 = vcombine_f32(v718, v718);
    float32x4_t v728 = vcombine_f32(v726, v726);
    float32x4_t v736 = vcombine_f32(v734, v734);
    float32x4_t v744 = vcombine_f32(v742, v742);
    float32x4_t v752 = vcombine_f32(v750, v750);
    float32x4_t v760 = vcombine_f32(v758, v758);
    float32x4_t v768 = vcombine_f32(v766, v766);
    float32x4_t v776 = vcombine_f32(v774, v774);
    float32x4_t v784 = vcombine_f32(v782, v782);
    float32x4_t v792 = vcombine_f32(v790, v790);
    float32x4_t v800 = vcombine_f32(v798, v798);
    float32x4_t v808 = vcombine_f32(v806, v806);
    float32x4_t v816 = vcombine_f32(v814, v814);
    float32x4_t v824 = vcombine_f32(v822, v822);
    float32x4_t v832 = vcombine_f32(v830, v830);
    float32x4_t v2250 = vld1q_f32((const float32_t *)v1924);
    float32x4_t v2252 = vld1q_f32((const float32_t *)v1935);
    float32x4_t v2254 = vld1q_f32((const float32_t *)v1945);
    float32x4_t v2256 = vld1q_f32((const float32_t *)v1957);
    float32x4_t v2258 = vld1q_f32((const float32_t *)v1967);
    float32x4_t v2260 = vld1q_f32((const float32_t *)v1979);
    float32x4_t v2262 = vld1q_f32((const float32_t *)v1989);
    float32x4_t v2264 = vld1q_f32((const float32_t *)v2001);
    float32x4_t v2266 = vld1q_f32((const float32_t *)v2011);
    float32x4_t v2268 = vld1q_f32((const float32_t *)v2023);
    float32x4_t v2270 = vld1q_f32((const float32_t *)v2033);
    float32x4_t v2272 = vld1q_f32((const float32_t *)v2045);
    float32x4_t v2274 = vld1q_f32((const float32_t *)v2055);
    float32x4_t v2276 = vld1q_f32((const float32_t *)v2067);
    float32x4_t v2278 = vld1q_f32((const float32_t *)v2077);
    float32x4_t v70 = vfmaq_f32(v67, v62, v68);
    float32x4_t v73 = vtrn1q_f32(v2250, v2250);
    float32x4_t v74 = vtrn2q_f32(v2250, v2250);
    float32x4_t v123 = vtrn1q_f32(v2252, v2252);
    float32x4_t v124 = vtrn2q_f32(v2252, v2252);
    float32x4_t v135 = vtrn1q_f32(v2254, v2254);
    float32x4_t v136 = vtrn2q_f32(v2254, v2254);
    float32x4_t v185 = vtrn1q_f32(v2256, v2256);
    float32x4_t v186 = vtrn2q_f32(v2256, v2256);
    float32x4_t v197 = vtrn1q_f32(v2258, v2258);
    float32x4_t v198 = vtrn2q_f32(v2258, v2258);
    float32x4_t v247 = vtrn1q_f32(v2260, v2260);
    float32x4_t v248 = vtrn2q_f32(v2260, v2260);
    float32x4_t v259 = vtrn1q_f32(v2262, v2262);
    float32x4_t v260 = vtrn2q_f32(v2262, v2262);
    float32x4_t v309 = vtrn1q_f32(v2264, v2264);
    float32x4_t v310 = vtrn2q_f32(v2264, v2264);
    float32x4_t v321 = vtrn1q_f32(v2266, v2266);
    float32x4_t v322 = vtrn2q_f32(v2266, v2266);
    float32x4_t v371 = vtrn1q_f32(v2268, v2268);
    float32x4_t v372 = vtrn2q_f32(v2268, v2268);
    float32x4_t v383 = vtrn1q_f32(v2270, v2270);
    float32x4_t v384 = vtrn2q_f32(v2270, v2270);
    float32x4_t v433 = vtrn1q_f32(v2272, v2272);
    float32x4_t v434 = vtrn2q_f32(v2272, v2272);
    float32x4_t v445 = vtrn1q_f32(v2274, v2274);
    float32x4_t v446 = vtrn2q_f32(v2274, v2274);
    float32x4_t v495 = vtrn1q_f32(v2276, v2276);
    float32x4_t v496 = vtrn2q_f32(v2276, v2276);
    float32x4_t v507 = vtrn1q_f32(v2278, v2278);
    float32x4_t v508 = vtrn2q_f32(v2278, v2278);
    float32x4_t v79 = vmulq_f32(v73, v78);
    float32x4_t v129 = vmulq_f32(v123, v128);
    float32x4_t v141 = vmulq_f32(v135, v140);
    float32x4_t v191 = vmulq_f32(v185, v190);
    float32x4_t v203 = vmulq_f32(v197, v202);
    float32x4_t v253 = vmulq_f32(v247, v252);
    float32x4_t v265 = vmulq_f32(v259, v264);
    float32x4_t v315 = vmulq_f32(v309, v314);
    float32x4_t v327 = vmulq_f32(v321, v326);
    float32x4_t v377 = vmulq_f32(v371, v376);
    float32x4_t v389 = vmulq_f32(v383, v388);
    float32x4_t v439 = vmulq_f32(v433, v438);
    float32x4_t v451 = vmulq_f32(v445, v450);
    float32x4_t v501 = vmulq_f32(v495, v500);
    float32x4_t v513 = vmulq_f32(v507, v512);
    float32x4_t v82 = vfmaq_f32(v79, v74, v80);
    float32x4_t v132 = vfmaq_f32(v129, v124, v130);
    float32x4_t v144 = vfmaq_f32(v141, v136, v142);
    float32x4_t v194 = vfmaq_f32(v191, v186, v192);
    float32x4_t v206 = vfmaq_f32(v203, v198, v204);
    float32x4_t v256 = vfmaq_f32(v253, v248, v254);
    float32x4_t v268 = vfmaq_f32(v265, v260, v266);
    float32x4_t v318 = vfmaq_f32(v315, v310, v316);
    float32x4_t v330 = vfmaq_f32(v327, v322, v328);
    float32x4_t v380 = vfmaq_f32(v377, v372, v378);
    float32x4_t v392 = vfmaq_f32(v389, v384, v390);
    float32x4_t v442 = vfmaq_f32(v439, v434, v440);
    float32x4_t v454 = vfmaq_f32(v451, v446, v452);
    float32x4_t v504 = vfmaq_f32(v501, v496, v502);
    float32x4_t v516 = vfmaq_f32(v513, v508, v514);
    float32x4_t v517 = vaddq_f32(v70, v82);
    float32x4_t v518 = vsubq_f32(v70, v82);
    float32x4_t v519 = vaddq_f32(v132, v144);
    float32x4_t v520 = vsubq_f32(v132, v144);
    float32x4_t v521 = vaddq_f32(v194, v206);
    float32x4_t v522 = vsubq_f32(v194, v206);
    float32x4_t v523 = vaddq_f32(v256, v268);
    float32x4_t v524 = vsubq_f32(v256, v268);
    float32x4_t v525 = vaddq_f32(v318, v330);
    float32x4_t v526 = vsubq_f32(v318, v330);
    float32x4_t v527 = vaddq_f32(v380, v392);
    float32x4_t v528 = vsubq_f32(v380, v392);
    float32x4_t v529 = vaddq_f32(v442, v454);
    float32x4_t v530 = vsubq_f32(v442, v454);
    float32x4_t v531 = vaddq_f32(v504, v516);
    float32x4_t v532 = vsubq_f32(v504, v516);
    float32x4_t v533 = vaddq_f32(v517, v525);
    float32x4_t v534 = vaddq_f32(v519, v527);
    float32x4_t v535 = vaddq_f32(v521, v529);
    float32x4_t v536 = vaddq_f32(v523, v531);
    float32x4_t v539 = vsubq_f32(v517, v525);
    float32x4_t v540 = vsubq_f32(v519, v527);
    float32x4_t v541 = vsubq_f32(v521, v529);
    float32x4_t v542 = vsubq_f32(v523, v531);
    float32x4_t v553 = vaddq_f32(v518, v522);
    float32x4_t v554 = vaddq_f32(v520, v524);
    float32x4_t v555 = vsubq_f32(v518, v522);
    float32x4_t v556 = vsubq_f32(v532, v528);
    float32x4_t v557 = vaddq_f32(v526, v530);
    float32x4_t v558 = vaddq_f32(v528, v532);
    float32x4_t v559 = vsubq_f32(v526, v530);
    float32x4_t v560 = vsubq_f32(v520, v524);
    float32x4_t v573 = vaddq_f32(v518, v526);
    float32x4_t v574 = vaddq_f32(v524, v532);
    float32x4_t v775 = vrev64q_f32(v518);
    float32x4_t v783 = vrev64q_f32(v526);
    float32x4_t v799 = vrev64q_f32(v524);
    float32x4_t v807 = vrev64q_f32(v532);
    float32x4_t v537 = vaddq_f32(v533, v535);
    float32x4_t v538 = vaddq_f32(v534, v536);
    float32x4_t v543 = vsubq_f32(v533, v535);
    float32x4_t v544 = vsubq_f32(v534, v536);
    float32x4_t v547 = vaddq_f32(v540, v542);
    float32x4_t v548 = vaddq_f32(v539, v541);
    float32x4_t v550 = vsubq_f32(v541, v542);
    float32x4_t v551 = vsubq_f32(v539, v540);
    float32x4_t v561 = vaddq_f32(v553, v554);
    float32x4_t v562 = vaddq_f32(v557, v558);
    float32x4_t v564 = vsubq_f32(v553, v554);
    float32x4_t v565 = vsubq_f32(v557, v558);
    float32x4_t v567 = vaddq_f32(v555, v556);
    float32x4_t v568 = vaddq_f32(v559, v560);
    float32x4_t v570 = vsubq_f32(v555, v556);
    float32x4_t v571 = vsubq_f32(v559, v560);
    float32x4_t v600 = vmulq_f32(v539, v599);
    float32x4_t v605 = vmulq_f32(v540, v604);
    float32x4_t v610 = vmulq_f32(v541, v609);
    float32x4_t v615 = vmulq_f32(v542, v614);
    float32x4_t v767 = vrev64q_f32(v573);
    float32x4_t v777 = vmulq_f32(v775, v776);
    float32x4_t v785 = vmulq_f32(v783, v784);
    float32x4_t v791 = vrev64q_f32(v574);
    float32x4_t v801 = vmulq_f32(v799, v800);
    float32x4_t v809 = vmulq_f32(v807, v808);
    float32x4_t v545 = vaddq_f32(v537, v538);
    float32x4_t v546 = vsubq_f32(v537, v538);
    float32x4_t v549 = vsubq_f32(v548, v547);
    float32x4_t v552 = vaddq_f32(v543, v544);
    float32x4_t v563 = vaddq_f32(v561, v562);
    float32x4_t v566 = vaddq_f32(v564, v565);
    float32x4_t v569 = vaddq_f32(v567, v568);
    float32x4_t v572 = vaddq_f32(v570, v571);
    float32x4_t v575 = vsubq_f32(v568, v562);
    float32x4_t v578 = vsubq_f32(v561, v567);
    float32x4_t v620 = vmulq_f32(v543, v619);
    float32x4_t v625 = vmulq_f32(v544, v624);
    float32x4_t v640 = vmulq_f32(v547, v639);
    float32x4_t v645 = vmulq_f32(v548, v644);
    float32x4_t v655 = vmulq_f32(v550, v654);
    float32x4_t v660 = vmulq_f32(v551, v659);
    float32x4_t v671 = vrev64q_f32(v561);
    float32x4_t v679 = vrev64q_f32(v562);
    float32x4_t v695 = vrev64q_f32(v564);
    float32x4_t v703 = vrev64q_f32(v565);
    float32x4_t v719 = vrev64q_f32(v567);
    float32x4_t v727 = vrev64q_f32(v568);
    float32x4_t v743 = vrev64q_f32(v570);
    float32x4_t v751 = vrev64q_f32(v571);
    float32x4_t v769 = vmulq_f32(v767, v768);
    float32x4_t v793 = vmulq_f32(v791, v792);
    float32x4_t v576 = vaddq_f32(v575, v518);
    float32x4_t v579 = vaddq_f32(v578, v524);
    float32x4_t v590 = vaddq_f32(v2280, v545);
    float32x4_t v630 = vmulq_f32(v545, v629);
    float32x4_t v635 = vmulq_f32(v546, v634);
    float32x4_t v650 = vmulq_f32(v549, v649);
    float32x4_t v665 = vmulq_f32(v552, v664);
    float32x4_t v673 = vmulq_f32(v671, v672);
    float32x4_t v681 = vmulq_f32(v679, v680);
    float32x4_t v687 = vrev64q_f32(v563);
    float32x4_t v697 = vmulq_f32(v695, v696);
    float32x4_t v705 = vmulq_f32(v703, v704);
    float32x4_t v711 = vrev64q_f32(v566);
    float32x4_t v721 = vmulq_f32(v719, v720);
    float32x4_t v729 = vmulq_f32(v727, v728);
    float32x4_t v735 = vrev64q_f32(v569);
    float32x4_t v745 = vmulq_f32(v743, v744);
    float32x4_t v753 = vmulq_f32(v751, v752);
    float32x4_t v759 = vrev64q_f32(v572);
    float32x4_t v836 = vaddq_f32(v615, v655);
    float32x4_t v837 = vsubq_f32(v655, v610);
    float32x4_t v838 = vaddq_f32(v605, v660);
    float32x4_t v839 = vsubq_f32(v600, v660);
    float32x4_t v577 = vsubq_f32(v576, v574);
    float32x4_t v580 = vaddq_f32(v579, v526);
    float32x4_t v689 = vmulq_f32(v687, v688);
    float32x4_t v713 = vmulq_f32(v711, v712);
    float32x4_t v737 = vmulq_f32(v735, v736);
    float32x4_t v761 = vmulq_f32(v759, v760);
    float32x4_t v834 = vaddq_f32(v640, v650);
    float32x4_t v835 = vsubq_f32(v645, v650);
    float32x4_t v840 = vsubq_f32(v665, v625);
    float32x4_t v841 = vaddq_f32(v665, v620);
    float32x4_t v842 = vaddq_f32(v630, v590);
    vst1q_f32((float32_t *)v2100, v590);
    float32x4_t v581 = vsubq_f32(v580, v532);
    float32x4_t v815 = vrev64q_f32(v577);
    float32x4_t v843 = vaddq_f32(v635, v842);
    float32x4_t v844 = vsubq_f32(v842, v635);
    float32x4_t v845 = vsubq_f32(v834, v836);
    float32x4_t v847 = vaddq_f32(v835, v837);
    float32x4_t v849 = vaddq_f32(v834, v838);
    float32x4_t v851 = vaddq_f32(v835, v839);
    float32x4_t v861 = vaddq_f32(v673, v689);
    float32x4_t v862 = vaddq_f32(v681, v689);
    float32x4_t v863 = vaddq_f32(v697, v713);
    float32x4_t v864 = vaddq_f32(v705, v713);
    float32x4_t v865 = vaddq_f32(v721, v737);
    float32x4_t v866 = vaddq_f32(v729, v737);
    float32x4_t v867 = vaddq_f32(v745, v761);
    float32x4_t v868 = vaddq_f32(v753, v761);
    float32x4_t v582 = vaddq_f32(v577, v581);
    float32x4_t v817 = vmulq_f32(v815, v816);
    float32x4_t v823 = vrev64q_f32(v581);
    float32x4_t v846 = vaddq_f32(v840, v843);
    float32x4_t v848 = vaddq_f32(v841, v844);
    float32x4_t v850 = vsubq_f32(v843, v840);
    float32x4_t v852 = vsubq_f32(v844, v841);
    float32x4_t v872 = vaddq_f32(v861, v863);
    float32x4_t v873 = vsubq_f32(v861, v863);
    float32x4_t v874 = vaddq_f32(v862, v864);
    float32x4_t v875 = vsubq_f32(v862, v864);
    float32x4_t v876 = vaddq_f32(v865, v867);
    float32x4_t v877 = vsubq_f32(v867, v865);
    float32x4_t v878 = vaddq_f32(v866, v868);
    float32x4_t v879 = vsubq_f32(v868, v866);
    float32x4_t v825 = vmulq_f32(v823, v824);
    float32x4_t v831 = vrev64q_f32(v582);
    float32x4_t v853 = vaddq_f32(v845, v846);
    float32x4_t v854 = vaddq_f32(v847, v848);
    float32x4_t v855 = vaddq_f32(v849, v850);
    float32x4_t v856 = vaddq_f32(v851, v852);
    float32x4_t v857 = vsubq_f32(v846, v845);
    float32x4_t v858 = vsubq_f32(v848, v847);
    float32x4_t v859 = vsubq_f32(v850, v849);
    float32x4_t v860 = vsubq_f32(v852, v851);
    float32x4_t v889 = vaddq_f32(v874, v878);
    float32x4_t v891 = vaddq_f32(v873, v879);
    float32x4_t v893 = vsubq_f32(v872, v876);
    float32x4_t v895 = vsubq_f32(v879, v873);
    float32x4_t v897 = vaddq_f32(v872, v876);
    float32x4_t v900 = vsubq_f32(v877, v875);
    float32x4_t v903 = vsubq_f32(v878, v874);
    float32x4_t v906 = vaddq_f32(v875, v877);
    float32x4_t v833 = vmulq_f32(v831, v832);
    float32x4_t v880 = vsubq_f32(v817, v825);
    float32x4_t v869 = vaddq_f32(v833, v825);
    float32x4_t v882 = vaddq_f32(v880, v880);
    float32x4_t v907 = vsubq_f32(v906, v880);
    float32x4_t v870 = vaddq_f32(v769, v869);
    float32x4_t v883 = vsubq_f32(v793, v882);
    float32x4_t v886 = vaddq_f32(v869, v869);
    float32x4_t v904 = vaddq_f32(v903, v882);
    float32x4_t v947 = vaddq_f32(v860, v907);
    float32x4_t v955 = vsubq_f32(v860, v907);
    float32x4_t v871 = vaddq_f32(v870, v777);
    float32x4_t v881 = vaddq_f32(v870, v785);
    float32x4_t v884 = vaddq_f32(v883, v801);
    float32x4_t v885 = vaddq_f32(v883, v809);
    float32x4_t v887 = vaddq_f32(v886, v886);
    float32x4_t v888 = vaddq_f32(v880, v886);
    float32x4_t v894 = vaddq_f32(v893, v886);
    float32x4_t v905 = vaddq_f32(v904, v886);
    vst1q_f32((float32_t *)v2145, v947);
    vst1q_f32((float32_t *)v2154, v955);
    float32x4_t v890 = vaddq_f32(v889, v881);
    float32x4_t v892 = vaddq_f32(v891, v884);
    float32x4_t v896 = vsubq_f32(v895, v888);
    float32x4_t v898 = vaddq_f32(v897, v871);
    float32x4_t v901 = vsubq_f32(v900, v885);
    float32x4_t v931 = vaddq_f32(v855, v894);
    float32x4_t v939 = vsubq_f32(v855, v894);
    float32x4_t v1027 = vaddq_f32(v859, v905);
    float32x4_t v1035 = vsubq_f32(v859, v905);
    float32x4_t v899 = vaddq_f32(v898, v880);
    float32x4_t v902 = vaddq_f32(v901, v887);
    float32x4_t v915 = vaddq_f32(v853, v890);
    float32x4_t v923 = vsubq_f32(v853, v890);
    float32x4_t v979 = vaddq_f32(v856, v896);
    float32x4_t v987 = vsubq_f32(v856, v896);
    float32x4_t v995 = vaddq_f32(v854, v892);
    float32x4_t v1003 = vsubq_f32(v854, v892);
    vst1q_f32((float32_t *)v2127, v931);
    vst1q_f32((float32_t *)v2136, v939);
    vst1q_f32((float32_t *)v2235, v1027);
    vst1q_f32((float32_t *)v2244, v1035);
    float32x4_t v963 = vaddq_f32(v857, v899);
    float32x4_t v971 = vsubq_f32(v857, v899);
    float32x4_t v1011 = vaddq_f32(v858, v902);
    float32x4_t v1019 = vsubq_f32(v858, v902);
    vst1q_f32((float32_t *)v2109, v915);
    vst1q_f32((float32_t *)v2118, v923);
    vst1q_f32((float32_t *)v2181, v979);
    vst1q_f32((float32_t *)v2190, v987);
    vst1q_f32((float32_t *)v2199, v995);
    vst1q_f32((float32_t *)v2208, v1003);
    vst1q_f32((float32_t *)v2163, v963);
    vst1q_f32((float32_t *)v2172, v971);
    vst1q_f32((float32_t *)v2217, v1011);
    vst1q_f32((float32_t *)v2226, v1019);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v1043 * 2; j < howmany; j += 1) {
    float32x2_t v1055 = v5[istride];
    float v1528 = -4.2602849117736000e-02F;
    float v1532 = 2.0497965023262180e-01F;
    float v1536 = 1.0451835201736759e+00F;
    float v1540 = 1.7645848660222969e+00F;
    float v1544 = -7.2340797728605655e-01F;
    float v1548 = -8.9055591620606403e-02F;
    float v1552 = -1.0625000000000000e+00F;
    float v1556 = 2.5769410160110379e-01F;
    float v1560 = 7.7980260789483757e-01F;
    float v1564 = 5.4389318464570580e-01F;
    float v1568 = 4.2010193497052700e-01F;
    float v1572 = 1.2810929434228073e+00F;
    float v1576 = 4.4088907348175338e-01F;
    float v1580 = 3.1717619283272508e-01F;
    float v1583 = -9.0138318648016680e-01F;
    float v1584 = 9.0138318648016680e-01F;
    float v1590 = -4.3248756360072310e-01F;
    float v1591 = 4.3248756360072310e-01F;
    float v1597 = 6.6693537504044498e-01F;
    float v1598 = -6.6693537504044498e-01F;
    float v1604 = -6.0389004312516970e-01F;
    float v1605 = 6.0389004312516970e-01F;
    float v1611 = -3.6924873198582547e-01F;
    float v1612 = 3.6924873198582547e-01F;
    float v1618 = 4.8656938755549761e-01F;
    float v1619 = -4.8656938755549761e-01F;
    float v1625 = 2.3813712136760609e-01F;
    float v1626 = -2.3813712136760609e-01F;
    float v1632 = -1.5573820617422458e+00F;
    float v1633 = 1.5573820617422458e+00F;
    float v1639 = 6.5962247018731990e-01F;
    float v1640 = -6.5962247018731990e-01F;
    float v1646 = -1.4316961569866241e-01F;
    float v1647 = 1.4316961569866241e-01F;
    float v1653 = 2.3903469959860771e-01F;
    float v1654 = -2.3903469959860771e-01F;
    float v1660 = -4.7932541949972603e-02F;
    float v1661 = 4.7932541949972603e-02F;
    float v1667 = -2.3188014856550065e+00F;
    float v1668 = 2.3188014856550065e+00F;
    float v1674 = 7.8914568419206255e-01F;
    float v1675 = -7.8914568419206255e-01F;
    float v1681 = 3.8484572871179505e+00F;
    float v1682 = -3.8484572871179505e+00F;
    float v1688 = -1.3003804568801376e+00F;
    float v1689 = 1.3003804568801376e+00F;
    float v1695 = 4.0814769046889037e+00F;
    float v1696 = -4.0814769046889037e+00F;
    float v1702 = -1.4807159909286283e+00F;
    float v1703 = 1.4807159909286283e+00F;
    float v1709 = -1.3332470363551400e-02F;
    float v1710 = 1.3332470363551400e-02F;
    float v1716 = -3.7139778690557629e-01F;
    float v1717 = 3.7139778690557629e-01F;
    float v1723 = 1.9236512863456379e-01F;
    float v1724 = -1.9236512863456379e-01F;
    float32x2_t v1726 = (float32x2_t){v4, v4};
    float32x2_t v1082 = v7[0];
    float32x2_t v1083 = vtrn1_f32(v1055, v1055);
    float32x2_t v1084 = vtrn2_f32(v1055, v1055);
    float32x2_t v1087 = v7[1];
    float32x2_t v1092 = v7[30];
    float32x2_t v1097 = v7[31];
    float32x2_t v1132 = v7[4];
    float32x2_t v1137 = v7[5];
    float32x2_t v1142 = v7[26];
    float32x2_t v1147 = v7[27];
    float32x2_t v1182 = v7[16];
    float32x2_t v1187 = v7[17];
    float32x2_t v1192 = v7[14];
    float32x2_t v1197 = v7[15];
    float32x2_t v1232 = v7[18];
    float32x2_t v1237 = v7[19];
    float32x2_t v1242 = v7[12];
    float32x2_t v1247 = v7[13];
    float32x2_t v1282 = v7[24];
    float32x2_t v1287 = v7[25];
    float32x2_t v1292 = v7[6];
    float32x2_t v1297 = v7[7];
    float32x2_t v1332 = v7[8];
    float32x2_t v1337 = v7[9];
    float32x2_t v1342 = v7[22];
    float32x2_t v1347 = v7[23];
    float32x2_t v1382 = v7[28];
    float32x2_t v1387 = v7[29];
    float32x2_t v1392 = v7[2];
    float32x2_t v1397 = v7[3];
    float32x2_t v1432 = v7[20];
    float32x2_t v1437 = v7[21];
    float32x2_t v1442 = v7[10];
    float32x2_t v1447 = v7[11];
    float32x2_t v1521 = v5[0];
    float32x2_t v1529 = (float32x2_t){v1528, v1528};
    float32x2_t v1533 = (float32x2_t){v1532, v1532};
    float32x2_t v1537 = (float32x2_t){v1536, v1536};
    float32x2_t v1541 = (float32x2_t){v1540, v1540};
    float32x2_t v1545 = (float32x2_t){v1544, v1544};
    float32x2_t v1549 = (float32x2_t){v1548, v1548};
    float32x2_t v1553 = (float32x2_t){v1552, v1552};
    float32x2_t v1557 = (float32x2_t){v1556, v1556};
    float32x2_t v1561 = (float32x2_t){v1560, v1560};
    float32x2_t v1565 = (float32x2_t){v1564, v1564};
    float32x2_t v1569 = (float32x2_t){v1568, v1568};
    float32x2_t v1573 = (float32x2_t){v1572, v1572};
    float32x2_t v1577 = (float32x2_t){v1576, v1576};
    float32x2_t v1581 = (float32x2_t){v1580, v1580};
    float32x2_t v1585 = (float32x2_t){v1583, v1584};
    float32x2_t v1592 = (float32x2_t){v1590, v1591};
    float32x2_t v1599 = (float32x2_t){v1597, v1598};
    float32x2_t v1606 = (float32x2_t){v1604, v1605};
    float32x2_t v1613 = (float32x2_t){v1611, v1612};
    float32x2_t v1620 = (float32x2_t){v1618, v1619};
    float32x2_t v1627 = (float32x2_t){v1625, v1626};
    float32x2_t v1634 = (float32x2_t){v1632, v1633};
    float32x2_t v1641 = (float32x2_t){v1639, v1640};
    float32x2_t v1648 = (float32x2_t){v1646, v1647};
    float32x2_t v1655 = (float32x2_t){v1653, v1654};
    float32x2_t v1662 = (float32x2_t){v1660, v1661};
    float32x2_t v1669 = (float32x2_t){v1667, v1668};
    float32x2_t v1676 = (float32x2_t){v1674, v1675};
    float32x2_t v1683 = (float32x2_t){v1681, v1682};
    float32x2_t v1690 = (float32x2_t){v1688, v1689};
    float32x2_t v1697 = (float32x2_t){v1695, v1696};
    float32x2_t v1704 = (float32x2_t){v1702, v1703};
    float32x2_t v1711 = (float32x2_t){v1709, v1710};
    float32x2_t v1718 = (float32x2_t){v1716, v1717};
    float32x2_t v1725 = (float32x2_t){v1723, v1724};
    float32x2_t v1070 = v5[istride * 16];
    float32x2_t v1088 = vmul_f32(v1083, v1082);
    float32x2_t v1105 = v5[istride * 3];
    float32x2_t v1120 = v5[istride * 14];
    float32x2_t v1155 = v5[istride * 9];
    float32x2_t v1170 = v5[istride * 8];
    float32x2_t v1205 = v5[istride * 10];
    float32x2_t v1220 = v5[istride * 7];
    float32x2_t v1255 = v5[istride * 13];
    float32x2_t v1270 = v5[istride * 4];
    float32x2_t v1305 = v5[istride * 5];
    float32x2_t v1320 = v5[istride * 12];
    float32x2_t v1355 = v5[istride * 15];
    float32x2_t v1370 = v5[istride * 2];
    float32x2_t v1405 = v5[istride * 11];
    float32x2_t v1420 = v5[istride * 6];
    float32x2_t v1587 = vmul_f32(v1726, v1585);
    float32x2_t v1594 = vmul_f32(v1726, v1592);
    float32x2_t v1601 = vmul_f32(v1726, v1599);
    float32x2_t v1608 = vmul_f32(v1726, v1606);
    float32x2_t v1615 = vmul_f32(v1726, v1613);
    float32x2_t v1622 = vmul_f32(v1726, v1620);
    float32x2_t v1629 = vmul_f32(v1726, v1627);
    float32x2_t v1636 = vmul_f32(v1726, v1634);
    float32x2_t v1643 = vmul_f32(v1726, v1641);
    float32x2_t v1650 = vmul_f32(v1726, v1648);
    float32x2_t v1657 = vmul_f32(v1726, v1655);
    float32x2_t v1664 = vmul_f32(v1726, v1662);
    float32x2_t v1671 = vmul_f32(v1726, v1669);
    float32x2_t v1678 = vmul_f32(v1726, v1676);
    float32x2_t v1685 = vmul_f32(v1726, v1683);
    float32x2_t v1692 = vmul_f32(v1726, v1690);
    float32x2_t v1699 = vmul_f32(v1726, v1697);
    float32x2_t v1706 = vmul_f32(v1726, v1704);
    float32x2_t v1713 = vmul_f32(v1726, v1711);
    float32x2_t v1720 = vmul_f32(v1726, v1718);
    float32x2_t v1727 = vmul_f32(v1726, v1725);
    float32x2_t v1090 = vfma_f32(v1088, v1084, v1087);
    float32x2_t v1093 = vtrn1_f32(v1070, v1070);
    float32x2_t v1094 = vtrn2_f32(v1070, v1070);
    float32x2_t v1133 = vtrn1_f32(v1105, v1105);
    float32x2_t v1134 = vtrn2_f32(v1105, v1105);
    float32x2_t v1143 = vtrn1_f32(v1120, v1120);
    float32x2_t v1144 = vtrn2_f32(v1120, v1120);
    float32x2_t v1183 = vtrn1_f32(v1155, v1155);
    float32x2_t v1184 = vtrn2_f32(v1155, v1155);
    float32x2_t v1193 = vtrn1_f32(v1170, v1170);
    float32x2_t v1194 = vtrn2_f32(v1170, v1170);
    float32x2_t v1233 = vtrn1_f32(v1205, v1205);
    float32x2_t v1234 = vtrn2_f32(v1205, v1205);
    float32x2_t v1243 = vtrn1_f32(v1220, v1220);
    float32x2_t v1244 = vtrn2_f32(v1220, v1220);
    float32x2_t v1283 = vtrn1_f32(v1255, v1255);
    float32x2_t v1284 = vtrn2_f32(v1255, v1255);
    float32x2_t v1293 = vtrn1_f32(v1270, v1270);
    float32x2_t v1294 = vtrn2_f32(v1270, v1270);
    float32x2_t v1333 = vtrn1_f32(v1305, v1305);
    float32x2_t v1334 = vtrn2_f32(v1305, v1305);
    float32x2_t v1343 = vtrn1_f32(v1320, v1320);
    float32x2_t v1344 = vtrn2_f32(v1320, v1320);
    float32x2_t v1383 = vtrn1_f32(v1355, v1355);
    float32x2_t v1384 = vtrn2_f32(v1355, v1355);
    float32x2_t v1393 = vtrn1_f32(v1370, v1370);
    float32x2_t v1394 = vtrn2_f32(v1370, v1370);
    float32x2_t v1433 = vtrn1_f32(v1405, v1405);
    float32x2_t v1434 = vtrn2_f32(v1405, v1405);
    float32x2_t v1443 = vtrn1_f32(v1420, v1420);
    float32x2_t v1444 = vtrn2_f32(v1420, v1420);
    float32x2_t v1098 = vmul_f32(v1093, v1092);
    float32x2_t v1138 = vmul_f32(v1133, v1132);
    float32x2_t v1148 = vmul_f32(v1143, v1142);
    float32x2_t v1188 = vmul_f32(v1183, v1182);
    float32x2_t v1198 = vmul_f32(v1193, v1192);
    float32x2_t v1238 = vmul_f32(v1233, v1232);
    float32x2_t v1248 = vmul_f32(v1243, v1242);
    float32x2_t v1288 = vmul_f32(v1283, v1282);
    float32x2_t v1298 = vmul_f32(v1293, v1292);
    float32x2_t v1338 = vmul_f32(v1333, v1332);
    float32x2_t v1348 = vmul_f32(v1343, v1342);
    float32x2_t v1388 = vmul_f32(v1383, v1382);
    float32x2_t v1398 = vmul_f32(v1393, v1392);
    float32x2_t v1438 = vmul_f32(v1433, v1432);
    float32x2_t v1448 = vmul_f32(v1443, v1442);
    float32x2_t v1100 = vfma_f32(v1098, v1094, v1097);
    float32x2_t v1140 = vfma_f32(v1138, v1134, v1137);
    float32x2_t v1150 = vfma_f32(v1148, v1144, v1147);
    float32x2_t v1190 = vfma_f32(v1188, v1184, v1187);
    float32x2_t v1200 = vfma_f32(v1198, v1194, v1197);
    float32x2_t v1240 = vfma_f32(v1238, v1234, v1237);
    float32x2_t v1250 = vfma_f32(v1248, v1244, v1247);
    float32x2_t v1290 = vfma_f32(v1288, v1284, v1287);
    float32x2_t v1300 = vfma_f32(v1298, v1294, v1297);
    float32x2_t v1340 = vfma_f32(v1338, v1334, v1337);
    float32x2_t v1350 = vfma_f32(v1348, v1344, v1347);
    float32x2_t v1390 = vfma_f32(v1388, v1384, v1387);
    float32x2_t v1400 = vfma_f32(v1398, v1394, v1397);
    float32x2_t v1440 = vfma_f32(v1438, v1434, v1437);
    float32x2_t v1450 = vfma_f32(v1448, v1444, v1447);
    float32x2_t v1451 = vadd_f32(v1090, v1100);
    float32x2_t v1452 = vsub_f32(v1090, v1100);
    float32x2_t v1453 = vadd_f32(v1140, v1150);
    float32x2_t v1454 = vsub_f32(v1140, v1150);
    float32x2_t v1455 = vadd_f32(v1190, v1200);
    float32x2_t v1456 = vsub_f32(v1190, v1200);
    float32x2_t v1457 = vadd_f32(v1240, v1250);
    float32x2_t v1458 = vsub_f32(v1240, v1250);
    float32x2_t v1459 = vadd_f32(v1290, v1300);
    float32x2_t v1460 = vsub_f32(v1290, v1300);
    float32x2_t v1461 = vadd_f32(v1340, v1350);
    float32x2_t v1462 = vsub_f32(v1340, v1350);
    float32x2_t v1463 = vadd_f32(v1390, v1400);
    float32x2_t v1464 = vsub_f32(v1390, v1400);
    float32x2_t v1465 = vadd_f32(v1440, v1450);
    float32x2_t v1466 = vsub_f32(v1440, v1450);
    float32x2_t v1467 = vadd_f32(v1451, v1459);
    float32x2_t v1468 = vadd_f32(v1453, v1461);
    float32x2_t v1469 = vadd_f32(v1455, v1463);
    float32x2_t v1470 = vadd_f32(v1457, v1465);
    float32x2_t v1473 = vsub_f32(v1451, v1459);
    float32x2_t v1474 = vsub_f32(v1453, v1461);
    float32x2_t v1475 = vsub_f32(v1455, v1463);
    float32x2_t v1476 = vsub_f32(v1457, v1465);
    float32x2_t v1487 = vadd_f32(v1452, v1456);
    float32x2_t v1488 = vadd_f32(v1454, v1458);
    float32x2_t v1489 = vsub_f32(v1452, v1456);
    float32x2_t v1490 = vsub_f32(v1466, v1462);
    float32x2_t v1491 = vadd_f32(v1460, v1464);
    float32x2_t v1492 = vadd_f32(v1462, v1466);
    float32x2_t v1493 = vsub_f32(v1460, v1464);
    float32x2_t v1494 = vsub_f32(v1454, v1458);
    float32x2_t v1507 = vadd_f32(v1452, v1460);
    float32x2_t v1508 = vadd_f32(v1458, v1466);
    float32x2_t v1679 = vrev64_f32(v1452);
    float32x2_t v1686 = vrev64_f32(v1460);
    float32x2_t v1700 = vrev64_f32(v1458);
    float32x2_t v1707 = vrev64_f32(v1466);
    float32x2_t v1471 = vadd_f32(v1467, v1469);
    float32x2_t v1472 = vadd_f32(v1468, v1470);
    float32x2_t v1477 = vsub_f32(v1467, v1469);
    float32x2_t v1478 = vsub_f32(v1468, v1470);
    float32x2_t v1481 = vadd_f32(v1474, v1476);
    float32x2_t v1482 = vadd_f32(v1473, v1475);
    float32x2_t v1484 = vsub_f32(v1475, v1476);
    float32x2_t v1485 = vsub_f32(v1473, v1474);
    float32x2_t v1495 = vadd_f32(v1487, v1488);
    float32x2_t v1496 = vadd_f32(v1491, v1492);
    float32x2_t v1498 = vsub_f32(v1487, v1488);
    float32x2_t v1499 = vsub_f32(v1491, v1492);
    float32x2_t v1501 = vadd_f32(v1489, v1490);
    float32x2_t v1502 = vadd_f32(v1493, v1494);
    float32x2_t v1504 = vsub_f32(v1489, v1490);
    float32x2_t v1505 = vsub_f32(v1493, v1494);
    float32x2_t v1530 = vmul_f32(v1473, v1529);
    float32x2_t v1534 = vmul_f32(v1474, v1533);
    float32x2_t v1538 = vmul_f32(v1475, v1537);
    float32x2_t v1542 = vmul_f32(v1476, v1541);
    float32x2_t v1672 = vrev64_f32(v1507);
    float32x2_t v1680 = vmul_f32(v1679, v1678);
    float32x2_t v1687 = vmul_f32(v1686, v1685);
    float32x2_t v1693 = vrev64_f32(v1508);
    float32x2_t v1701 = vmul_f32(v1700, v1699);
    float32x2_t v1708 = vmul_f32(v1707, v1706);
    float32x2_t v1479 = vadd_f32(v1471, v1472);
    float32x2_t v1480 = vsub_f32(v1471, v1472);
    float32x2_t v1483 = vsub_f32(v1482, v1481);
    float32x2_t v1486 = vadd_f32(v1477, v1478);
    float32x2_t v1497 = vadd_f32(v1495, v1496);
    float32x2_t v1500 = vadd_f32(v1498, v1499);
    float32x2_t v1503 = vadd_f32(v1501, v1502);
    float32x2_t v1506 = vadd_f32(v1504, v1505);
    float32x2_t v1509 = vsub_f32(v1502, v1496);
    float32x2_t v1512 = vsub_f32(v1495, v1501);
    float32x2_t v1546 = vmul_f32(v1477, v1545);
    float32x2_t v1550 = vmul_f32(v1478, v1549);
    float32x2_t v1562 = vmul_f32(v1481, v1561);
    float32x2_t v1566 = vmul_f32(v1482, v1565);
    float32x2_t v1574 = vmul_f32(v1484, v1573);
    float32x2_t v1578 = vmul_f32(v1485, v1577);
    float32x2_t v1588 = vrev64_f32(v1495);
    float32x2_t v1595 = vrev64_f32(v1496);
    float32x2_t v1609 = vrev64_f32(v1498);
    float32x2_t v1616 = vrev64_f32(v1499);
    float32x2_t v1630 = vrev64_f32(v1501);
    float32x2_t v1637 = vrev64_f32(v1502);
    float32x2_t v1651 = vrev64_f32(v1504);
    float32x2_t v1658 = vrev64_f32(v1505);
    float32x2_t v1673 = vmul_f32(v1672, v1671);
    float32x2_t v1694 = vmul_f32(v1693, v1692);
    float32x2_t v1510 = vadd_f32(v1509, v1452);
    float32x2_t v1513 = vadd_f32(v1512, v1458);
    float32x2_t v1522 = vadd_f32(v1521, v1479);
    float32x2_t v1554 = vmul_f32(v1479, v1553);
    float32x2_t v1558 = vmul_f32(v1480, v1557);
    float32x2_t v1570 = vmul_f32(v1483, v1569);
    float32x2_t v1582 = vmul_f32(v1486, v1581);
    float32x2_t v1589 = vmul_f32(v1588, v1587);
    float32x2_t v1596 = vmul_f32(v1595, v1594);
    float32x2_t v1602 = vrev64_f32(v1497);
    float32x2_t v1610 = vmul_f32(v1609, v1608);
    float32x2_t v1617 = vmul_f32(v1616, v1615);
    float32x2_t v1623 = vrev64_f32(v1500);
    float32x2_t v1631 = vmul_f32(v1630, v1629);
    float32x2_t v1638 = vmul_f32(v1637, v1636);
    float32x2_t v1644 = vrev64_f32(v1503);
    float32x2_t v1652 = vmul_f32(v1651, v1650);
    float32x2_t v1659 = vmul_f32(v1658, v1657);
    float32x2_t v1665 = vrev64_f32(v1506);
    float32x2_t v1732 = vadd_f32(v1542, v1574);
    float32x2_t v1733 = vsub_f32(v1574, v1538);
    float32x2_t v1734 = vadd_f32(v1534, v1578);
    float32x2_t v1735 = vsub_f32(v1530, v1578);
    float32x2_t v1511 = vsub_f32(v1510, v1508);
    float32x2_t v1514 = vadd_f32(v1513, v1460);
    float32x2_t v1603 = vmul_f32(v1602, v1601);
    float32x2_t v1624 = vmul_f32(v1623, v1622);
    float32x2_t v1645 = vmul_f32(v1644, v1643);
    float32x2_t v1666 = vmul_f32(v1665, v1664);
    float32x2_t v1730 = vadd_f32(v1562, v1570);
    float32x2_t v1731 = vsub_f32(v1566, v1570);
    float32x2_t v1736 = vsub_f32(v1582, v1550);
    float32x2_t v1737 = vadd_f32(v1582, v1546);
    float32x2_t v1738 = vadd_f32(v1554, v1522);
    v6[0] = v1522;
    float32x2_t v1515 = vsub_f32(v1514, v1466);
    float32x2_t v1714 = vrev64_f32(v1511);
    float32x2_t v1739 = vadd_f32(v1558, v1738);
    float32x2_t v1740 = vsub_f32(v1738, v1558);
    float32x2_t v1741 = vsub_f32(v1730, v1732);
    float32x2_t v1743 = vadd_f32(v1731, v1733);
    float32x2_t v1745 = vadd_f32(v1730, v1734);
    float32x2_t v1747 = vadd_f32(v1731, v1735);
    float32x2_t v1757 = vadd_f32(v1589, v1603);
    float32x2_t v1758 = vadd_f32(v1596, v1603);
    float32x2_t v1759 = vadd_f32(v1610, v1624);
    float32x2_t v1760 = vadd_f32(v1617, v1624);
    float32x2_t v1761 = vadd_f32(v1631, v1645);
    float32x2_t v1762 = vadd_f32(v1638, v1645);
    float32x2_t v1763 = vadd_f32(v1652, v1666);
    float32x2_t v1764 = vadd_f32(v1659, v1666);
    float32x2_t v1516 = vadd_f32(v1511, v1515);
    float32x2_t v1715 = vmul_f32(v1714, v1713);
    float32x2_t v1721 = vrev64_f32(v1515);
    float32x2_t v1742 = vadd_f32(v1736, v1739);
    float32x2_t v1744 = vadd_f32(v1737, v1740);
    float32x2_t v1746 = vsub_f32(v1739, v1736);
    float32x2_t v1748 = vsub_f32(v1740, v1737);
    float32x2_t v1768 = vadd_f32(v1757, v1759);
    float32x2_t v1769 = vsub_f32(v1757, v1759);
    float32x2_t v1770 = vadd_f32(v1758, v1760);
    float32x2_t v1771 = vsub_f32(v1758, v1760);
    float32x2_t v1772 = vadd_f32(v1761, v1763);
    float32x2_t v1773 = vsub_f32(v1763, v1761);
    float32x2_t v1774 = vadd_f32(v1762, v1764);
    float32x2_t v1775 = vsub_f32(v1764, v1762);
    float32x2_t v1722 = vmul_f32(v1721, v1720);
    float32x2_t v1728 = vrev64_f32(v1516);
    float32x2_t v1749 = vadd_f32(v1741, v1742);
    float32x2_t v1750 = vadd_f32(v1743, v1744);
    float32x2_t v1751 = vadd_f32(v1745, v1746);
    float32x2_t v1752 = vadd_f32(v1747, v1748);
    float32x2_t v1753 = vsub_f32(v1742, v1741);
    float32x2_t v1754 = vsub_f32(v1744, v1743);
    float32x2_t v1755 = vsub_f32(v1746, v1745);
    float32x2_t v1756 = vsub_f32(v1748, v1747);
    float32x2_t v1785 = vadd_f32(v1770, v1774);
    float32x2_t v1787 = vadd_f32(v1769, v1775);
    float32x2_t v1789 = vsub_f32(v1768, v1772);
    float32x2_t v1791 = vsub_f32(v1775, v1769);
    float32x2_t v1793 = vadd_f32(v1768, v1772);
    float32x2_t v1796 = vsub_f32(v1773, v1771);
    float32x2_t v1799 = vsub_f32(v1774, v1770);
    float32x2_t v1802 = vadd_f32(v1771, v1773);
    float32x2_t v1729 = vmul_f32(v1728, v1727);
    float32x2_t v1776 = vsub_f32(v1715, v1722);
    float32x2_t v1765 = vadd_f32(v1729, v1722);
    float32x2_t v1778 = vadd_f32(v1776, v1776);
    float32x2_t v1803 = vsub_f32(v1802, v1776);
    float32x2_t v1766 = vadd_f32(v1673, v1765);
    float32x2_t v1779 = vsub_f32(v1694, v1778);
    float32x2_t v1782 = vadd_f32(v1765, v1765);
    float32x2_t v1800 = vadd_f32(v1799, v1778);
    float32x2_t v1833 = vadd_f32(v1756, v1803);
    float32x2_t v1839 = vsub_f32(v1756, v1803);
    float32x2_t v1767 = vadd_f32(v1766, v1680);
    float32x2_t v1777 = vadd_f32(v1766, v1687);
    float32x2_t v1780 = vadd_f32(v1779, v1701);
    float32x2_t v1781 = vadd_f32(v1779, v1708);
    float32x2_t v1783 = vadd_f32(v1782, v1782);
    float32x2_t v1784 = vadd_f32(v1776, v1782);
    float32x2_t v1790 = vadd_f32(v1789, v1782);
    float32x2_t v1801 = vadd_f32(v1800, v1782);
    v6[ostride * 3] = v1833;
    v6[ostride * 14] = v1839;
    float32x2_t v1786 = vadd_f32(v1785, v1777);
    float32x2_t v1788 = vadd_f32(v1787, v1780);
    float32x2_t v1792 = vsub_f32(v1791, v1784);
    float32x2_t v1794 = vadd_f32(v1793, v1767);
    float32x2_t v1797 = vsub_f32(v1796, v1781);
    float32x2_t v1821 = vadd_f32(v1751, v1790);
    float32x2_t v1827 = vsub_f32(v1751, v1790);
    float32x2_t v1893 = vadd_f32(v1755, v1801);
    float32x2_t v1899 = vsub_f32(v1755, v1801);
    float32x2_t v1795 = vadd_f32(v1794, v1776);
    float32x2_t v1798 = vadd_f32(v1797, v1783);
    float32x2_t v1809 = vadd_f32(v1749, v1786);
    float32x2_t v1815 = vsub_f32(v1749, v1786);
    v6[ostride * 2] = v1821;
    v6[ostride * 15] = v1827;
    float32x2_t v1857 = vadd_f32(v1752, v1792);
    float32x2_t v1863 = vsub_f32(v1752, v1792);
    float32x2_t v1869 = vadd_f32(v1750, v1788);
    float32x2_t v1875 = vsub_f32(v1750, v1788);
    v6[ostride * 8] = v1893;
    v6[ostride * 9] = v1899;
    v6[ostride] = v1809;
    v6[ostride * 16] = v1815;
    float32x2_t v1845 = vadd_f32(v1753, v1795);
    float32x2_t v1851 = vsub_f32(v1753, v1795);
    v6[ostride * 5] = v1857;
    v6[ostride * 12] = v1863;
    v6[ostride * 6] = v1869;
    v6[ostride * 11] = v1875;
    float32x2_t v1881 = vadd_f32(v1754, v1798);
    float32x2_t v1887 = vsub_f32(v1754, v1798);
    v6[ostride * 4] = v1845;
    v6[ostride * 13] = v1851;
    v6[ostride * 7] = v1881;
    v6[ostride * 10] = v1887;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu17(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v338 = -4.2602849117736000e-02F;
    float v343 = 2.0497965023262180e-01F;
    float v348 = 1.0451835201736759e+00F;
    float v353 = 1.7645848660222969e+00F;
    float v358 = -7.2340797728605655e-01F;
    float v363 = -8.9055591620606403e-02F;
    float v368 = -1.0625000000000000e+00F;
    float v373 = 2.5769410160110379e-01F;
    float v378 = 7.7980260789483757e-01F;
    float v383 = 5.4389318464570580e-01F;
    float v388 = 4.2010193497052700e-01F;
    float v393 = 1.2810929434228073e+00F;
    float v398 = 4.4088907348175338e-01F;
    float v403 = 3.1717619283272508e-01F;
    float v408 = 9.0138318648016680e-01F;
    float v415 = 4.3248756360072310e-01F;
    float v422 = -6.6693537504044498e-01F;
    float v429 = 6.0389004312516970e-01F;
    float v436 = 3.6924873198582547e-01F;
    float v443 = -4.8656938755549761e-01F;
    float v450 = -2.3813712136760609e-01F;
    float v457 = 1.5573820617422458e+00F;
    float v464 = -6.5962247018731990e-01F;
    float v471 = 1.4316961569866241e-01F;
    float v478 = -2.3903469959860771e-01F;
    float v485 = 4.7932541949972603e-02F;
    float v492 = 2.3188014856550065e+00F;
    float v499 = -7.8914568419206255e-01F;
    float v506 = -3.8484572871179505e+00F;
    float v513 = 1.3003804568801376e+00F;
    float v520 = -4.0814769046889037e+00F;
    float v527 = 1.4807159909286283e+00F;
    float v534 = 1.3332470363551400e-02F;
    float v541 = 3.7139778690557629e-01F;
    float v548 = -1.9236512863456379e-01F;
    const float32x2_t *v769 = &v5[v0];
    float32x2_t *v969 = &v6[v2];
    int64_t v30 = v0 * 16;
    int64_t v49 = v0 * 3;
    int64_t v60 = v0 * 14;
    int64_t v79 = v0 * 9;
    int64_t v90 = v0 * 8;
    int64_t v109 = v0 * 10;
    int64_t v120 = v0 * 7;
    int64_t v139 = v0 * 13;
    int64_t v150 = v0 * 4;
    int64_t v169 = v0 * 5;
    int64_t v180 = v0 * 12;
    int64_t v199 = v0 * 15;
    int64_t v210 = v0 * 2;
    int64_t v229 = v0 * 11;
    int64_t v240 = v0 * 6;
    float v411 = v4 * v408;
    float v418 = v4 * v415;
    float v425 = v4 * v422;
    float v432 = v4 * v429;
    float v439 = v4 * v436;
    float v446 = v4 * v443;
    float v453 = v4 * v450;
    float v460 = v4 * v457;
    float v467 = v4 * v464;
    float v474 = v4 * v471;
    float v481 = v4 * v478;
    float v488 = v4 * v485;
    float v495 = v4 * v492;
    float v502 = v4 * v499;
    float v509 = v4 * v506;
    float v516 = v4 * v513;
    float v523 = v4 * v520;
    float v530 = v4 * v527;
    float v537 = v4 * v534;
    float v544 = v4 * v541;
    float v551 = v4 * v548;
    int64_t v645 = v2 * 16;
    int64_t v653 = v2 * 2;
    int64_t v661 = v2 * 15;
    int64_t v669 = v2 * 3;
    int64_t v677 = v2 * 14;
    int64_t v685 = v2 * 4;
    int64_t v693 = v2 * 13;
    int64_t v701 = v2 * 5;
    int64_t v709 = v2 * 12;
    int64_t v717 = v2 * 6;
    int64_t v725 = v2 * 11;
    int64_t v733 = v2 * 7;
    int64_t v741 = v2 * 10;
    int64_t v749 = v2 * 8;
    int64_t v757 = v2 * 9;
    const float32x2_t *v914 = &v5[0];
    svfloat32_t v918 = svdup_n_f32(v338);
    svfloat32_t v919 = svdup_n_f32(v343);
    svfloat32_t v920 = svdup_n_f32(v348);
    svfloat32_t v921 = svdup_n_f32(v353);
    svfloat32_t v922 = svdup_n_f32(v358);
    svfloat32_t v923 = svdup_n_f32(v363);
    svfloat32_t v924 = svdup_n_f32(v368);
    svfloat32_t v925 = svdup_n_f32(v373);
    svfloat32_t v926 = svdup_n_f32(v378);
    svfloat32_t v927 = svdup_n_f32(v383);
    svfloat32_t v928 = svdup_n_f32(v388);
    svfloat32_t v929 = svdup_n_f32(v393);
    svfloat32_t v930 = svdup_n_f32(v398);
    svfloat32_t v931 = svdup_n_f32(v403);
    float32x2_t *v960 = &v6[0];
    svfloat32_t v1108 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v769)[0]));
    svfloat32_t v42 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v46 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[15]));
    svfloat32_t v72 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v76 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v102 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v106 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v132 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v136 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v162 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v166 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v192 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v196 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v222 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[14]));
    svfloat32_t v226 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v252 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v256 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    const float32x2_t *v778 = &v5[v30];
    const float32x2_t *v787 = &v5[v49];
    const float32x2_t *v796 = &v5[v60];
    const float32x2_t *v805 = &v5[v79];
    const float32x2_t *v814 = &v5[v90];
    const float32x2_t *v823 = &v5[v109];
    const float32x2_t *v832 = &v5[v120];
    const float32x2_t *v841 = &v5[v139];
    const float32x2_t *v850 = &v5[v150];
    const float32x2_t *v859 = &v5[v169];
    const float32x2_t *v868 = &v5[v180];
    const float32x2_t *v877 = &v5[v199];
    const float32x2_t *v886 = &v5[v210];
    const float32x2_t *v895 = &v5[v229];
    const float32x2_t *v904 = &v5[v240];
    svfloat32_t v932 = svdup_n_f32(v411);
    svfloat32_t v933 = svdup_n_f32(v418);
    svfloat32_t v934 = svdup_n_f32(v425);
    svfloat32_t v935 = svdup_n_f32(v432);
    svfloat32_t v936 = svdup_n_f32(v439);
    svfloat32_t v937 = svdup_n_f32(v446);
    svfloat32_t v938 = svdup_n_f32(v453);
    svfloat32_t v939 = svdup_n_f32(v460);
    svfloat32_t v940 = svdup_n_f32(v467);
    svfloat32_t v941 = svdup_n_f32(v474);
    svfloat32_t v942 = svdup_n_f32(v481);
    svfloat32_t v943 = svdup_n_f32(v488);
    svfloat32_t v944 = svdup_n_f32(v495);
    svfloat32_t v945 = svdup_n_f32(v502);
    svfloat32_t v946 = svdup_n_f32(v509);
    svfloat32_t v947 = svdup_n_f32(v516);
    svfloat32_t v948 = svdup_n_f32(v523);
    svfloat32_t v949 = svdup_n_f32(v530);
    svfloat32_t v950 = svdup_n_f32(v537);
    svfloat32_t v951 = svdup_n_f32(v544);
    svfloat32_t v952 = svdup_n_f32(v551);
    float32x2_t *v978 = &v6[v645];
    float32x2_t *v987 = &v6[v653];
    float32x2_t *v996 = &v6[v661];
    float32x2_t *v1005 = &v6[v669];
    float32x2_t *v1014 = &v6[v677];
    float32x2_t *v1023 = &v6[v685];
    float32x2_t *v1032 = &v6[v693];
    float32x2_t *v1041 = &v6[v701];
    float32x2_t *v1050 = &v6[v709];
    float32x2_t *v1059 = &v6[v717];
    float32x2_t *v1068 = &v6[v725];
    float32x2_t *v1077 = &v6[v733];
    float32x2_t *v1086 = &v6[v741];
    float32x2_t *v1095 = &v6[v749];
    float32x2_t *v1104 = &v6[v757];
    svfloat32_t v1140 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v914)[0]));
    svfloat32_t zero43 = svdup_n_f32(0);
    svfloat32_t v43 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero43, v1108, v42, 0),
                     v1108, v42, 90);
    svfloat32_t v1110 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v778)[0]));
    svfloat32_t v1112 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v787)[0]));
    svfloat32_t v1114 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v796)[0]));
    svfloat32_t v1116 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v805)[0]));
    svfloat32_t v1118 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v814)[0]));
    svfloat32_t v1120 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v823)[0]));
    svfloat32_t v1122 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v832)[0]));
    svfloat32_t v1124 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v841)[0]));
    svfloat32_t v1126 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v850)[0]));
    svfloat32_t v1128 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v859)[0]));
    svfloat32_t v1130 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v868)[0]));
    svfloat32_t v1132 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v877)[0]));
    svfloat32_t v1134 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v886)[0]));
    svfloat32_t v1136 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v895)[0]));
    svfloat32_t v1138 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v904)[0]));
    svfloat32_t zero47 = svdup_n_f32(0);
    svfloat32_t v47 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero47, v1110, v46, 0),
                     v1110, v46, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v1112, v72, 0),
                     v1112, v72, 90);
    svfloat32_t zero77 = svdup_n_f32(0);
    svfloat32_t v77 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero77, v1114, v76, 0),
                     v1114, v76, 90);
    svfloat32_t zero103 = svdup_n_f32(0);
    svfloat32_t v103 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero103, v1116, v102, 0), v1116,
        v102, 90);
    svfloat32_t zero107 = svdup_n_f32(0);
    svfloat32_t v107 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero107, v1118, v106, 0), v1118,
        v106, 90);
    svfloat32_t zero133 = svdup_n_f32(0);
    svfloat32_t v133 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero133, v1120, v132, 0), v1120,
        v132, 90);
    svfloat32_t zero137 = svdup_n_f32(0);
    svfloat32_t v137 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero137, v1122, v136, 0), v1122,
        v136, 90);
    svfloat32_t zero163 = svdup_n_f32(0);
    svfloat32_t v163 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero163, v1124, v162, 0), v1124,
        v162, 90);
    svfloat32_t zero167 = svdup_n_f32(0);
    svfloat32_t v167 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero167, v1126, v166, 0), v1126,
        v166, 90);
    svfloat32_t zero193 = svdup_n_f32(0);
    svfloat32_t v193 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero193, v1128, v192, 0), v1128,
        v192, 90);
    svfloat32_t zero197 = svdup_n_f32(0);
    svfloat32_t v197 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero197, v1130, v196, 0), v1130,
        v196, 90);
    svfloat32_t zero223 = svdup_n_f32(0);
    svfloat32_t v223 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero223, v1132, v222, 0), v1132,
        v222, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero227, v1134, v226, 0), v1134,
        v226, 90);
    svfloat32_t zero253 = svdup_n_f32(0);
    svfloat32_t v253 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero253, v1136, v252, 0), v1136,
        v252, 90);
    svfloat32_t zero257 = svdup_n_f32(0);
    svfloat32_t v257 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero257, v1138, v256, 0), v1138,
        v256, 90);
    svfloat32_t v258 = svadd_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v259 = svsub_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v260 = svadd_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v261 = svsub_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v262 = svadd_f32_x(svptrue_b32(), v103, v107);
    svfloat32_t v263 = svsub_f32_x(svptrue_b32(), v103, v107);
    svfloat32_t v264 = svadd_f32_x(svptrue_b32(), v133, v137);
    svfloat32_t v265 = svsub_f32_x(svptrue_b32(), v133, v137);
    svfloat32_t v266 = svadd_f32_x(svptrue_b32(), v163, v167);
    svfloat32_t v267 = svsub_f32_x(svptrue_b32(), v163, v167);
    svfloat32_t v268 = svadd_f32_x(svptrue_b32(), v193, v197);
    svfloat32_t v269 = svsub_f32_x(svptrue_b32(), v193, v197);
    svfloat32_t v270 = svadd_f32_x(svptrue_b32(), v223, v227);
    svfloat32_t v271 = svsub_f32_x(svptrue_b32(), v223, v227);
    svfloat32_t v272 = svadd_f32_x(svptrue_b32(), v253, v257);
    svfloat32_t v273 = svsub_f32_x(svptrue_b32(), v253, v257);
    svfloat32_t v274 = svadd_f32_x(svptrue_b32(), v258, v266);
    svfloat32_t v275 = svadd_f32_x(svptrue_b32(), v260, v268);
    svfloat32_t v276 = svadd_f32_x(svptrue_b32(), v262, v270);
    svfloat32_t v277 = svadd_f32_x(svptrue_b32(), v264, v272);
    svfloat32_t v280 = svsub_f32_x(svptrue_b32(), v258, v266);
    svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v260, v268);
    svfloat32_t v282 = svsub_f32_x(svptrue_b32(), v262, v270);
    svfloat32_t v283 = svsub_f32_x(svptrue_b32(), v264, v272);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v259, v263);
    svfloat32_t v295 = svadd_f32_x(svptrue_b32(), v261, v265);
    svfloat32_t v296 = svsub_f32_x(svptrue_b32(), v259, v263);
    svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v273, v269);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v267, v271);
    svfloat32_t v299 = svadd_f32_x(svptrue_b32(), v269, v273);
    svfloat32_t v300 = svsub_f32_x(svptrue_b32(), v267, v271);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v261, v265);
    svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v259, v267);
    svfloat32_t v315 = svadd_f32_x(svptrue_b32(), v265, v273);
    svfloat32_t v278 = svadd_f32_x(svptrue_b32(), v274, v276);
    svfloat32_t v279 = svadd_f32_x(svptrue_b32(), v275, v277);
    svfloat32_t v284 = svsub_f32_x(svptrue_b32(), v274, v276);
    svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v275, v277);
    svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v281, v283);
    svfloat32_t v289 = svadd_f32_x(svptrue_b32(), v280, v282);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v282, v283);
    svfloat32_t v292 = svsub_f32_x(svptrue_b32(), v280, v281);
    svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v294, v295);
    svfloat32_t v303 = svadd_f32_x(svptrue_b32(), v298, v299);
    svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v294, v295);
    svfloat32_t v306 = svsub_f32_x(svptrue_b32(), v298, v299);
    svfloat32_t v308 = svadd_f32_x(svptrue_b32(), v296, v297);
    svfloat32_t v309 = svadd_f32_x(svptrue_b32(), v300, v301);
    svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v296, v297);
    svfloat32_t v312 = svsub_f32_x(svptrue_b32(), v300, v301);
    svfloat32_t v351 = svmul_f32_x(svptrue_b32(), v282, v920);
    svfloat32_t zero518 = svdup_n_f32(0);
    svfloat32_t v518 = svcmla_f32_x(pred_full, zero518, v947, v315, 90);
    svfloat32_t v286 = svadd_f32_x(svptrue_b32(), v278, v279);
    svfloat32_t v287 = svsub_f32_x(svptrue_b32(), v278, v279);
    svfloat32_t v290 = svsub_f32_x(svptrue_b32(), v289, v288);
    svfloat32_t v293 = svadd_f32_x(svptrue_b32(), v284, v285);
    svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v302, v303);
    svfloat32_t v307 = svadd_f32_x(svptrue_b32(), v305, v306);
    svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v308, v309);
    svfloat32_t v313 = svadd_f32_x(svptrue_b32(), v311, v312);
    svfloat32_t v316 = svsub_f32_x(svptrue_b32(), v309, v303);
    svfloat32_t v319 = svsub_f32_x(svptrue_b32(), v302, v308);
    svfloat32_t v361 = svmul_f32_x(svptrue_b32(), v284, v922);
    svfloat32_t v366 = svmul_f32_x(svptrue_b32(), v285, v923);
    svfloat32_t v396 = svmul_f32_x(svptrue_b32(), v291, v929);
    svfloat32_t v401 = svmul_f32_x(svptrue_b32(), v292, v930);
    svfloat32_t v317 = svadd_f32_x(svptrue_b32(), v316, v259);
    svfloat32_t v320 = svadd_f32_x(svptrue_b32(), v319, v265);
    svfloat32_t v331 = svadd_f32_x(svptrue_b32(), v1140, v286);
    svfloat32_t v391 = svmul_f32_x(svptrue_b32(), v290, v928);
    svfloat32_t zero427 = svdup_n_f32(0);
    svfloat32_t v427 = svcmla_f32_x(pred_full, zero427, v934, v304, 90);
    svfloat32_t zero448 = svdup_n_f32(0);
    svfloat32_t v448 = svcmla_f32_x(pred_full, zero448, v937, v307, 90);
    svfloat32_t zero469 = svdup_n_f32(0);
    svfloat32_t v469 = svcmla_f32_x(pred_full, zero469, v940, v310, 90);
    svfloat32_t zero490 = svdup_n_f32(0);
    svfloat32_t v490 = svcmla_f32_x(pred_full, zero490, v943, v313, 90);
    svfloat32_t v556 = svmla_f32_x(pred_full, v396, v283, v921);
    svfloat32_t v557 = svnmls_f32_x(pred_full, v351, v291, v929);
    svfloat32_t v558 = svmla_f32_x(pred_full, v401, v281, v919);
    svfloat32_t v559 = svnmls_f32_x(pred_full, v401, v280, v918);
    svfloat32_t v318 = svsub_f32_x(svptrue_b32(), v317, v315);
    svfloat32_t v321 = svadd_f32_x(svptrue_b32(), v320, v267);
    svfloat32_t v554 = svmla_f32_x(pred_full, v391, v288, v926);
    svfloat32_t v555 = svnmls_f32_x(pred_full, v391, v289, v927);
    svfloat32_t v560 = svnmls_f32_x(pred_full, v366, v293, v931);
    svfloat32_t v561 = svmla_f32_x(pred_full, v361, v293, v931);
    svfloat32_t v562 = svmla_f32_x(pred_full, v331, v286, v924);
    svfloat32_t v581 = svcmla_f32_x(pred_full, v427, v932, v302, 90);
    svfloat32_t v582 = svcmla_f32_x(pred_full, v427, v933, v303, 90);
    svfloat32_t v583 = svcmla_f32_x(pred_full, v448, v935, v305, 90);
    svfloat32_t v584 = svcmla_f32_x(pred_full, v448, v936, v306, 90);
    svfloat32_t v585 = svcmla_f32_x(pred_full, v469, v938, v308, 90);
    svfloat32_t v586 = svcmla_f32_x(pred_full, v469, v939, v309, 90);
    svfloat32_t v587 = svcmla_f32_x(pred_full, v490, v941, v311, 90);
    svfloat32_t v588 = svcmla_f32_x(pred_full, v490, v942, v312, 90);
    svst1_f64(pred_full, (double *)(v960), svreinterpret_f64_f32(v331));
    svfloat32_t v322 = svsub_f32_x(svptrue_b32(), v321, v273);
    svfloat32_t zero539 = svdup_n_f32(0);
    svfloat32_t v539 = svcmla_f32_x(pred_full, zero539, v950, v318, 90);
    svfloat32_t v563 = svmla_f32_x(pred_full, v562, v287, v925);
    svfloat32_t v564 = svmls_f32_x(pred_full, v562, v287, v925);
    svfloat32_t v565 = svsub_f32_x(svptrue_b32(), v554, v556);
    svfloat32_t v567 = svadd_f32_x(svptrue_b32(), v555, v557);
    svfloat32_t v569 = svadd_f32_x(svptrue_b32(), v554, v558);
    svfloat32_t v571 = svadd_f32_x(svptrue_b32(), v555, v559);
    svfloat32_t v592 = svadd_f32_x(svptrue_b32(), v581, v583);
    svfloat32_t v593 = svsub_f32_x(svptrue_b32(), v581, v583);
    svfloat32_t v594 = svadd_f32_x(svptrue_b32(), v582, v584);
    svfloat32_t v595 = svsub_f32_x(svptrue_b32(), v582, v584);
    svfloat32_t v596 = svadd_f32_x(svptrue_b32(), v585, v587);
    svfloat32_t v597 = svsub_f32_x(svptrue_b32(), v587, v585);
    svfloat32_t v598 = svadd_f32_x(svptrue_b32(), v586, v588);
    svfloat32_t v599 = svsub_f32_x(svptrue_b32(), v588, v586);
    svfloat32_t v323 = svadd_f32_x(svptrue_b32(), v318, v322);
    svfloat32_t zero546 = svdup_n_f32(0);
    svfloat32_t v546 = svcmla_f32_x(pred_full, zero546, v951, v322, 90);
    svfloat32_t v566 = svadd_f32_x(svptrue_b32(), v560, v563);
    svfloat32_t v568 = svadd_f32_x(svptrue_b32(), v561, v564);
    svfloat32_t v570 = svsub_f32_x(svptrue_b32(), v563, v560);
    svfloat32_t v572 = svsub_f32_x(svptrue_b32(), v564, v561);
    svfloat32_t v609 = svadd_f32_x(svptrue_b32(), v594, v598);
    svfloat32_t v611 = svadd_f32_x(svptrue_b32(), v593, v599);
    svfloat32_t v613 = svsub_f32_x(svptrue_b32(), v592, v596);
    svfloat32_t v615 = svsub_f32_x(svptrue_b32(), v599, v593);
    svfloat32_t v617 = svadd_f32_x(svptrue_b32(), v592, v596);
    svfloat32_t v620 = svsub_f32_x(svptrue_b32(), v597, v595);
    svfloat32_t v623 = svsub_f32_x(svptrue_b32(), v598, v594);
    svfloat32_t v626 = svadd_f32_x(svptrue_b32(), v595, v597);
    svfloat32_t v573 = svadd_f32_x(svptrue_b32(), v565, v566);
    svfloat32_t v574 = svadd_f32_x(svptrue_b32(), v567, v568);
    svfloat32_t v575 = svadd_f32_x(svptrue_b32(), v569, v570);
    svfloat32_t v576 = svadd_f32_x(svptrue_b32(), v571, v572);
    svfloat32_t v577 = svsub_f32_x(svptrue_b32(), v566, v565);
    svfloat32_t v578 = svsub_f32_x(svptrue_b32(), v568, v567);
    svfloat32_t v579 = svsub_f32_x(svptrue_b32(), v570, v569);
    svfloat32_t v580 = svsub_f32_x(svptrue_b32(), v572, v571);
    svfloat32_t v600 = svsub_f32_x(svptrue_b32(), v539, v546);
    svfloat32_t v589 = svcmla_f32_x(pred_full, v546, v952, v323, 90);
    svfloat32_t v602 = svadd_f32_x(svptrue_b32(), v600, v600);
    svfloat32_t v627 = svsub_f32_x(svptrue_b32(), v626, v600);
    svfloat32_t v590 = svcmla_f32_x(pred_full, v589, v944, v314, 90);
    svfloat32_t v603 = svsub_f32_x(svptrue_b32(), v518, v602);
    svfloat32_t v606 = svadd_f32_x(svptrue_b32(), v589, v589);
    svfloat32_t v624 = svadd_f32_x(svptrue_b32(), v623, v602);
    svfloat32_t v667 = svadd_f32_x(svptrue_b32(), v580, v627);
    svfloat32_t v675 = svsub_f32_x(svptrue_b32(), v580, v627);
    svfloat32_t v591 = svcmla_f32_x(pred_full, v590, v945, v259, 90);
    svfloat32_t v601 = svcmla_f32_x(pred_full, v590, v946, v267, 90);
    svfloat32_t v604 = svcmla_f32_x(pred_full, v603, v948, v265, 90);
    svfloat32_t v605 = svcmla_f32_x(pred_full, v603, v949, v273, 90);
    svfloat32_t v607 = svadd_f32_x(svptrue_b32(), v606, v606);
    svfloat32_t v608 = svadd_f32_x(svptrue_b32(), v600, v606);
    svfloat32_t v614 = svadd_f32_x(svptrue_b32(), v613, v606);
    svfloat32_t v625 = svadd_f32_x(svptrue_b32(), v624, v606);
    svst1_f64(pred_full, (double *)(v1005), svreinterpret_f64_f32(v667));
    svst1_f64(pred_full, (double *)(v1014), svreinterpret_f64_f32(v675));
    svfloat32_t v610 = svadd_f32_x(svptrue_b32(), v609, v601);
    svfloat32_t v612 = svadd_f32_x(svptrue_b32(), v611, v604);
    svfloat32_t v616 = svsub_f32_x(svptrue_b32(), v615, v608);
    svfloat32_t v618 = svadd_f32_x(svptrue_b32(), v617, v591);
    svfloat32_t v621 = svsub_f32_x(svptrue_b32(), v620, v605);
    svfloat32_t v651 = svadd_f32_x(svptrue_b32(), v575, v614);
    svfloat32_t v659 = svsub_f32_x(svptrue_b32(), v575, v614);
    svfloat32_t v747 = svadd_f32_x(svptrue_b32(), v579, v625);
    svfloat32_t v755 = svsub_f32_x(svptrue_b32(), v579, v625);
    svfloat32_t v619 = svadd_f32_x(svptrue_b32(), v618, v600);
    svfloat32_t v622 = svadd_f32_x(svptrue_b32(), v621, v607);
    svfloat32_t v635 = svadd_f32_x(svptrue_b32(), v573, v610);
    svfloat32_t v643 = svsub_f32_x(svptrue_b32(), v573, v610);
    svfloat32_t v699 = svadd_f32_x(svptrue_b32(), v576, v616);
    svfloat32_t v707 = svsub_f32_x(svptrue_b32(), v576, v616);
    svfloat32_t v715 = svadd_f32_x(svptrue_b32(), v574, v612);
    svfloat32_t v723 = svsub_f32_x(svptrue_b32(), v574, v612);
    svst1_f64(pred_full, (double *)(v987), svreinterpret_f64_f32(v651));
    svst1_f64(pred_full, (double *)(v996), svreinterpret_f64_f32(v659));
    svst1_f64(pred_full, (double *)(v1095), svreinterpret_f64_f32(v747));
    svst1_f64(pred_full, (double *)(v1104), svreinterpret_f64_f32(v755));
    svfloat32_t v683 = svadd_f32_x(svptrue_b32(), v577, v619);
    svfloat32_t v691 = svsub_f32_x(svptrue_b32(), v577, v619);
    svfloat32_t v731 = svadd_f32_x(svptrue_b32(), v578, v622);
    svfloat32_t v739 = svsub_f32_x(svptrue_b32(), v578, v622);
    svst1_f64(pred_full, (double *)(v969), svreinterpret_f64_f32(v635));
    svst1_f64(pred_full, (double *)(v978), svreinterpret_f64_f32(v643));
    svst1_f64(pred_full, (double *)(v1041), svreinterpret_f64_f32(v699));
    svst1_f64(pred_full, (double *)(v1050), svreinterpret_f64_f32(v707));
    svst1_f64(pred_full, (double *)(v1059), svreinterpret_f64_f32(v715));
    svst1_f64(pred_full, (double *)(v1068), svreinterpret_f64_f32(v723));
    svst1_f64(pred_full, (double *)(v1023), svreinterpret_f64_f32(v683));
    svst1_f64(pred_full, (double *)(v1032), svreinterpret_f64_f32(v691));
    svst1_f64(pred_full, (double *)(v1077), svreinterpret_f64_f32(v731));
    svst1_f64(pred_full, (double *)(v1086), svreinterpret_f64_f32(v739));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu18(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v929 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v714 = -5.0000000000000000e-01F;
    float v727 = -1.4999999999999998e+00F;
    float v731 = 8.6602540378443871e-01F;
    float v732 = -8.6602540378443871e-01F;
    float v740 = 7.6604444311897801e-01F;
    float v745 = 9.3969262078590832e-01F;
    float v750 = -1.7364817766693039e-01F;
    float v754 = 6.4278760968653925e-01F;
    float v755 = -6.4278760968653925e-01F;
    float v762 = -3.4202014332566888e-01F;
    float v763 = 3.4202014332566888e-01F;
    float v770 = 9.8480775301220802e-01F;
    float v771 = -9.8480775301220802e-01F;
    float32x2_t v773 = (float32x2_t){v4, v4};
    const float32x2_t *v1802 = &v5[istride];
    float32x2_t *v1916 = &v6[ostride];
    float32x2_t v715 = (float32x2_t){v714, v714};
    float32x2_t v728 = (float32x2_t){v727, v727};
    float32x2_t v733 = (float32x2_t){v731, v732};
    float32x2_t v741 = (float32x2_t){v740, v740};
    float32x2_t v746 = (float32x2_t){v745, v745};
    float32x2_t v751 = (float32x2_t){v750, v750};
    float32x2_t v756 = (float32x2_t){v754, v755};
    float32x2_t v764 = (float32x2_t){v762, v763};
    float32x2_t v772 = (float32x2_t){v770, v771};
    const float32x2_t *v1879 = &v5[0];
    float32x2_t *v1889 = &v6[0];
    float32x4_t v2066 = vld1q_f32((const float32_t *)v1802);
    float32x4_t v47 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v49 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v97 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v99 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v109 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v111 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v159 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v161 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v171 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v173 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v221 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v223 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v233 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[28]));
    float32x4_t v235 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[29]));
    float32x4_t v283 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v285 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v295 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[32]));
    float32x4_t v297 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[33]));
    float32x4_t v345 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v347 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v352 = vtrn1q_f32(v2066, v2066);
    float32x4_t v353 = vtrn2q_f32(v2066, v2066);
    float32x4_t v357 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v359 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v407 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v409 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v419 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v421 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v469 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v471 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v481 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v483 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v531 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[30]));
    float32x4_t v533 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[31]));
    float32x4_t v543 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v545 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v716 = vcombine_f32(v715, v715);
    float32x4_t v729 = vcombine_f32(v728, v728);
    float32x2_t v735 = vmul_f32(v773, v733);
    float32x4_t v742 = vcombine_f32(v741, v741);
    float32x4_t v747 = vcombine_f32(v746, v746);
    float32x4_t v752 = vcombine_f32(v751, v751);
    float32x2_t v758 = vmul_f32(v773, v756);
    float32x2_t v766 = vmul_f32(v773, v764);
    float32x2_t v774 = vmul_f32(v773, v772);
    const float32x2_t *v1693 = &v5[istride * 9];
    const float32x2_t *v1704 = &v5[istride * 2];
    const float32x2_t *v1714 = &v5[istride * 11];
    const float32x2_t *v1726 = &v5[istride * 4];
    const float32x2_t *v1736 = &v5[istride * 13];
    const float32x2_t *v1748 = &v5[istride * 6];
    const float32x2_t *v1758 = &v5[istride * 15];
    const float32x2_t *v1770 = &v5[istride * 8];
    const float32x2_t *v1780 = &v5[istride * 17];
    const float32x2_t *v1792 = &v5[istride * 10];
    const float32x2_t *v1812 = &v5[istride * 12];
    const float32x2_t *v1822 = &v5[istride * 3];
    const float32x2_t *v1834 = &v5[istride * 14];
    const float32x2_t *v1844 = &v5[istride * 5];
    const float32x2_t *v1856 = &v5[istride * 16];
    const float32x2_t *v1866 = &v5[istride * 7];
    float32x2_t *v1898 = &v6[ostride * 9];
    float32x2_t *v1907 = &v6[ostride * 10];
    float32x2_t *v1925 = &v6[ostride * 2];
    float32x2_t *v1934 = &v6[ostride * 11];
    float32x2_t *v1943 = &v6[ostride * 12];
    float32x2_t *v1952 = &v6[ostride * 3];
    float32x2_t *v1961 = &v6[ostride * 4];
    float32x2_t *v1970 = &v6[ostride * 13];
    float32x2_t *v1979 = &v6[ostride * 14];
    float32x2_t *v1988 = &v6[ostride * 5];
    float32x2_t *v1997 = &v6[ostride * 6];
    float32x2_t *v2006 = &v6[ostride * 15];
    float32x2_t *v2015 = &v6[ostride * 16];
    float32x2_t *v2024 = &v6[ostride * 7];
    float32x2_t *v2033 = &v6[ostride * 8];
    float32x2_t *v2042 = &v6[ostride * 17];
    float32x4_t v2080 = vld1q_f32((const float32_t *)v1879);
    float32x4_t v358 = vmulq_f32(v352, v357);
    float32x4_t v737 = vcombine_f32(v735, v735);
    float32x4_t v760 = vcombine_f32(v758, v758);
    float32x4_t v768 = vcombine_f32(v766, v766);
    float32x4_t v776 = vcombine_f32(v774, v774);
    float32x4_t v2046 = vld1q_f32((const float32_t *)v1693);
    float32x4_t v2048 = vld1q_f32((const float32_t *)v1704);
    float32x4_t v2050 = vld1q_f32((const float32_t *)v1714);
    float32x4_t v2052 = vld1q_f32((const float32_t *)v1726);
    float32x4_t v2054 = vld1q_f32((const float32_t *)v1736);
    float32x4_t v2056 = vld1q_f32((const float32_t *)v1748);
    float32x4_t v2058 = vld1q_f32((const float32_t *)v1758);
    float32x4_t v2060 = vld1q_f32((const float32_t *)v1770);
    float32x4_t v2062 = vld1q_f32((const float32_t *)v1780);
    float32x4_t v2064 = vld1q_f32((const float32_t *)v1792);
    float32x4_t v2068 = vld1q_f32((const float32_t *)v1812);
    float32x4_t v2070 = vld1q_f32((const float32_t *)v1822);
    float32x4_t v2072 = vld1q_f32((const float32_t *)v1834);
    float32x4_t v2074 = vld1q_f32((const float32_t *)v1844);
    float32x4_t v2076 = vld1q_f32((const float32_t *)v1856);
    float32x4_t v2078 = vld1q_f32((const float32_t *)v1866);
    float32x4_t v42 = vtrn1q_f32(v2046, v2046);
    float32x4_t v43 = vtrn2q_f32(v2046, v2046);
    float32x4_t v92 = vtrn1q_f32(v2048, v2048);
    float32x4_t v93 = vtrn2q_f32(v2048, v2048);
    float32x4_t v104 = vtrn1q_f32(v2050, v2050);
    float32x4_t v105 = vtrn2q_f32(v2050, v2050);
    float32x4_t v154 = vtrn1q_f32(v2052, v2052);
    float32x4_t v155 = vtrn2q_f32(v2052, v2052);
    float32x4_t v166 = vtrn1q_f32(v2054, v2054);
    float32x4_t v167 = vtrn2q_f32(v2054, v2054);
    float32x4_t v216 = vtrn1q_f32(v2056, v2056);
    float32x4_t v217 = vtrn2q_f32(v2056, v2056);
    float32x4_t v228 = vtrn1q_f32(v2058, v2058);
    float32x4_t v229 = vtrn2q_f32(v2058, v2058);
    float32x4_t v278 = vtrn1q_f32(v2060, v2060);
    float32x4_t v279 = vtrn2q_f32(v2060, v2060);
    float32x4_t v290 = vtrn1q_f32(v2062, v2062);
    float32x4_t v291 = vtrn2q_f32(v2062, v2062);
    float32x4_t v340 = vtrn1q_f32(v2064, v2064);
    float32x4_t v341 = vtrn2q_f32(v2064, v2064);
    float32x4_t v361 = vfmaq_f32(v358, v353, v359);
    float32x4_t v402 = vtrn1q_f32(v2068, v2068);
    float32x4_t v403 = vtrn2q_f32(v2068, v2068);
    float32x4_t v414 = vtrn1q_f32(v2070, v2070);
    float32x4_t v415 = vtrn2q_f32(v2070, v2070);
    float32x4_t v464 = vtrn1q_f32(v2072, v2072);
    float32x4_t v465 = vtrn2q_f32(v2072, v2072);
    float32x4_t v476 = vtrn1q_f32(v2074, v2074);
    float32x4_t v477 = vtrn2q_f32(v2074, v2074);
    float32x4_t v526 = vtrn1q_f32(v2076, v2076);
    float32x4_t v527 = vtrn2q_f32(v2076, v2076);
    float32x4_t v538 = vtrn1q_f32(v2078, v2078);
    float32x4_t v539 = vtrn2q_f32(v2078, v2078);
    float32x4_t v48 = vmulq_f32(v42, v47);
    float32x4_t v98 = vmulq_f32(v92, v97);
    float32x4_t v110 = vmulq_f32(v104, v109);
    float32x4_t v160 = vmulq_f32(v154, v159);
    float32x4_t v172 = vmulq_f32(v166, v171);
    float32x4_t v222 = vmulq_f32(v216, v221);
    float32x4_t v234 = vmulq_f32(v228, v233);
    float32x4_t v284 = vmulq_f32(v278, v283);
    float32x4_t v296 = vmulq_f32(v290, v295);
    float32x4_t v346 = vmulq_f32(v340, v345);
    float32x4_t v408 = vmulq_f32(v402, v407);
    float32x4_t v420 = vmulq_f32(v414, v419);
    float32x4_t v470 = vmulq_f32(v464, v469);
    float32x4_t v482 = vmulq_f32(v476, v481);
    float32x4_t v532 = vmulq_f32(v526, v531);
    float32x4_t v544 = vmulq_f32(v538, v543);
    float32x4_t v51 = vfmaq_f32(v48, v43, v49);
    float32x4_t v101 = vfmaq_f32(v98, v93, v99);
    float32x4_t v113 = vfmaq_f32(v110, v105, v111);
    float32x4_t v163 = vfmaq_f32(v160, v155, v161);
    float32x4_t v175 = vfmaq_f32(v172, v167, v173);
    float32x4_t v225 = vfmaq_f32(v222, v217, v223);
    float32x4_t v237 = vfmaq_f32(v234, v229, v235);
    float32x4_t v287 = vfmaq_f32(v284, v279, v285);
    float32x4_t v299 = vfmaq_f32(v296, v291, v297);
    float32x4_t v349 = vfmaq_f32(v346, v341, v347);
    float32x4_t v411 = vfmaq_f32(v408, v403, v409);
    float32x4_t v423 = vfmaq_f32(v420, v415, v421);
    float32x4_t v473 = vfmaq_f32(v470, v465, v471);
    float32x4_t v485 = vfmaq_f32(v482, v477, v483);
    float32x4_t v535 = vfmaq_f32(v532, v527, v533);
    float32x4_t v547 = vfmaq_f32(v544, v539, v545);
    float32x4_t v555 = vaddq_f32(v2080, v51);
    float32x4_t v556 = vsubq_f32(v2080, v51);
    float32x4_t v557 = vaddq_f32(v101, v113);
    float32x4_t v558 = vsubq_f32(v101, v113);
    float32x4_t v559 = vaddq_f32(v163, v175);
    float32x4_t v560 = vsubq_f32(v163, v175);
    float32x4_t v561 = vaddq_f32(v225, v237);
    float32x4_t v562 = vsubq_f32(v225, v237);
    float32x4_t v563 = vaddq_f32(v287, v299);
    float32x4_t v564 = vsubq_f32(v287, v299);
    float32x4_t v565 = vaddq_f32(v349, v361);
    float32x4_t v566 = vsubq_f32(v349, v361);
    float32x4_t v567 = vaddq_f32(v411, v423);
    float32x4_t v568 = vsubq_f32(v411, v423);
    float32x4_t v569 = vaddq_f32(v473, v485);
    float32x4_t v570 = vsubq_f32(v473, v485);
    float32x4_t v571 = vaddq_f32(v535, v547);
    float32x4_t v572 = vsubq_f32(v535, v547);
    float32x4_t v573 = vaddq_f32(v557, v571);
    float32x4_t v574 = vsubq_f32(v557, v571);
    float32x4_t v575 = vaddq_f32(v569, v559);
    float32x4_t v576 = vsubq_f32(v569, v559);
    float32x4_t v577 = vaddq_f32(v561, v567);
    float32x4_t v578 = vsubq_f32(v561, v567);
    float32x4_t v579 = vaddq_f32(v563, v565);
    float32x4_t v580 = vsubq_f32(v563, v565);
    float32x4_t v688 = vaddq_f32(v558, v572);
    float32x4_t v689 = vsubq_f32(v558, v572);
    float32x4_t v690 = vaddq_f32(v570, v560);
    float32x4_t v691 = vsubq_f32(v570, v560);
    float32x4_t v692 = vaddq_f32(v562, v568);
    float32x4_t v693 = vsubq_f32(v562, v568);
    float32x4_t v694 = vaddq_f32(v564, v566);
    float32x4_t v695 = vsubq_f32(v564, v566);
    float32x4_t v581 = vaddq_f32(v573, v575);
    float32x4_t v585 = vaddq_f32(v574, v576);
    float32x4_t v587 = vsubq_f32(v573, v575);
    float32x4_t v588 = vsubq_f32(v575, v579);
    float32x4_t v589 = vsubq_f32(v579, v573);
    float32x4_t v590 = vsubq_f32(v574, v576);
    float32x4_t v591 = vsubq_f32(v576, v580);
    float32x4_t v592 = vsubq_f32(v580, v574);
    float32x4_t v615 = vmulq_f32(v577, v729);
    float32x4_t v621 = vrev64q_f32(v578);
    float32x4_t v696 = vaddq_f32(v688, v690);
    float32x4_t v700 = vaddq_f32(v689, v691);
    float32x4_t v702 = vsubq_f32(v688, v690);
    float32x4_t v703 = vsubq_f32(v690, v694);
    float32x4_t v704 = vsubq_f32(v694, v688);
    float32x4_t v705 = vsubq_f32(v689, v691);
    float32x4_t v706 = vsubq_f32(v691, v695);
    float32x4_t v707 = vsubq_f32(v695, v689);
    float32x4_t v730 = vmulq_f32(v692, v729);
    float32x4_t v736 = vrev64q_f32(v693);
    float32x4_t v582 = vaddq_f32(v581, v579);
    float32x4_t v586 = vaddq_f32(v585, v580);
    float32x4_t v623 = vmulq_f32(v621, v737);
    float32x4_t v628 = vmulq_f32(v587, v742);
    float32x4_t v633 = vmulq_f32(v588, v747);
    float32x4_t v638 = vmulq_f32(v589, v752);
    float32x4_t v644 = vrev64q_f32(v590);
    float32x4_t v652 = vrev64q_f32(v591);
    float32x4_t v660 = vrev64q_f32(v592);
    float32x4_t v697 = vaddq_f32(v696, v694);
    float32x4_t v701 = vaddq_f32(v700, v695);
    float32x4_t v738 = vmulq_f32(v736, v737);
    float32x4_t v743 = vmulq_f32(v702, v742);
    float32x4_t v748 = vmulq_f32(v703, v747);
    float32x4_t v753 = vmulq_f32(v704, v752);
    float32x4_t v759 = vrev64q_f32(v705);
    float32x4_t v767 = vrev64q_f32(v706);
    float32x4_t v775 = vrev64q_f32(v707);
    float32x4_t v583 = vaddq_f32(v582, v577);
    float32x4_t v602 = vmulq_f32(v582, v716);
    float32x4_t v608 = vrev64q_f32(v586);
    float32x4_t v646 = vmulq_f32(v644, v760);
    float32x4_t v654 = vmulq_f32(v652, v768);
    float32x4_t v662 = vmulq_f32(v660, v776);
    float32x4_t v698 = vaddq_f32(v697, v692);
    float32x4_t v717 = vmulq_f32(v697, v716);
    float32x4_t v723 = vrev64q_f32(v701);
    float32x4_t v761 = vmulq_f32(v759, v760);
    float32x4_t v769 = vmulq_f32(v767, v768);
    float32x4_t v777 = vmulq_f32(v775, v776);
    float32x4_t v584 = vaddq_f32(v583, v555);
    float32x4_t v610 = vmulq_f32(v608, v737);
    float32x4_t v663 = vaddq_f32(v602, v602);
    float32x4_t v676 = vaddq_f32(v623, v646);
    float32x4_t v678 = vsubq_f32(v623, v654);
    float32x4_t v680 = vsubq_f32(v623, v646);
    float32x4_t v699 = vaddq_f32(v698, v556);
    float32x4_t v725 = vmulq_f32(v723, v737);
    float32x4_t v778 = vaddq_f32(v717, v717);
    float32x4_t v791 = vaddq_f32(v738, v761);
    float32x4_t v793 = vsubq_f32(v738, v769);
    float32x4_t v795 = vsubq_f32(v738, v761);
    float32x4_t v664 = vaddq_f32(v663, v602);
    float32x4_t v668 = vaddq_f32(v584, v615);
    float32x4_t v677 = vaddq_f32(v676, v654);
    float32x4_t v679 = vaddq_f32(v678, v662);
    float32x4_t v681 = vsubq_f32(v680, v662);
    float32x4_t v779 = vaddq_f32(v778, v717);
    float32x4_t v783 = vaddq_f32(v699, v730);
    float32x4_t v792 = vaddq_f32(v791, v769);
    float32x4_t v794 = vaddq_f32(v793, v777);
    float32x4_t v796 = vsubq_f32(v795, v777);
    vst1q_f32((float32_t *)v1889, v584);
    vst1q_f32((float32_t *)v1898, v699);
    float32x4_t v665 = vaddq_f32(v584, v664);
    float32x4_t v669 = vaddq_f32(v668, v663);
    float32x4_t v780 = vaddq_f32(v699, v779);
    float32x4_t v784 = vaddq_f32(v783, v778);
    float32x4_t v666 = vaddq_f32(v665, v610);
    float32x4_t v667 = vsubq_f32(v665, v610);
    float32x4_t v670 = vaddq_f32(v669, v628);
    float32x4_t v672 = vsubq_f32(v669, v633);
    float32x4_t v674 = vsubq_f32(v669, v628);
    float32x4_t v781 = vaddq_f32(v780, v725);
    float32x4_t v782 = vsubq_f32(v780, v725);
    float32x4_t v785 = vaddq_f32(v784, v743);
    float32x4_t v787 = vsubq_f32(v784, v748);
    float32x4_t v789 = vsubq_f32(v784, v743);
    float32x4_t v671 = vaddq_f32(v670, v633);
    float32x4_t v673 = vaddq_f32(v672, v638);
    float32x4_t v675 = vsubq_f32(v674, v638);
    float32x4_t v786 = vaddq_f32(v785, v748);
    float32x4_t v788 = vaddq_f32(v787, v753);
    float32x4_t v790 = vsubq_f32(v789, v753);
    vst1q_f32((float32_t *)v1943, v667);
    vst1q_f32((float32_t *)v1952, v782);
    vst1q_f32((float32_t *)v1997, v666);
    vst1q_f32((float32_t *)v2006, v781);
    float32x4_t v682 = vaddq_f32(v671, v677);
    float32x4_t v683 = vsubq_f32(v671, v677);
    float32x4_t v684 = vaddq_f32(v673, v679);
    float32x4_t v685 = vsubq_f32(v673, v679);
    float32x4_t v686 = vaddq_f32(v675, v681);
    float32x4_t v687 = vsubq_f32(v675, v681);
    float32x4_t v797 = vaddq_f32(v786, v792);
    float32x4_t v798 = vsubq_f32(v786, v792);
    float32x4_t v799 = vaddq_f32(v788, v794);
    float32x4_t v800 = vsubq_f32(v788, v794);
    float32x4_t v801 = vaddq_f32(v790, v796);
    float32x4_t v802 = vsubq_f32(v790, v796);
    vst1q_f32((float32_t *)v1907, v683);
    vst1q_f32((float32_t *)v1916, v798);
    vst1q_f32((float32_t *)v1925, v684);
    vst1q_f32((float32_t *)v1934, v799);
    vst1q_f32((float32_t *)v1961, v687);
    vst1q_f32((float32_t *)v1970, v802);
    vst1q_f32((float32_t *)v1979, v686);
    vst1q_f32((float32_t *)v1988, v801);
    vst1q_f32((float32_t *)v2015, v685);
    vst1q_f32((float32_t *)v2024, v800);
    vst1q_f32((float32_t *)v2033, v682);
    vst1q_f32((float32_t *)v2042, v797);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v929 * 2; j < howmany; j += 1) {
    float32x2_t v1181 = v5[istride];
    float v1514 = -5.0000000000000000e-01F;
    float v1525 = -1.4999999999999998e+00F;
    float v1528 = 8.6602540378443871e-01F;
    float v1529 = -8.6602540378443871e-01F;
    float v1536 = 7.6604444311897801e-01F;
    float v1540 = 9.3969262078590832e-01F;
    float v1544 = -1.7364817766693039e-01F;
    float v1547 = 6.4278760968653925e-01F;
    float v1548 = -6.4278760968653925e-01F;
    float v1554 = -3.4202014332566888e-01F;
    float v1555 = 3.4202014332566888e-01F;
    float v1561 = 9.8480775301220802e-01F;
    float v1562 = -9.8480775301220802e-01F;
    float32x2_t v1564 = (float32x2_t){v4, v4};
    float32x2_t v953 = v7[16];
    float32x2_t v958 = v7[17];
    float32x2_t v993 = v7[2];
    float32x2_t v998 = v7[3];
    float32x2_t v1003 = v7[20];
    float32x2_t v1008 = v7[21];
    float32x2_t v1043 = v7[6];
    float32x2_t v1048 = v7[7];
    float32x2_t v1053 = v7[24];
    float32x2_t v1058 = v7[25];
    float32x2_t v1093 = v7[10];
    float32x2_t v1098 = v7[11];
    float32x2_t v1103 = v7[28];
    float32x2_t v1108 = v7[29];
    float32x2_t v1143 = v7[14];
    float32x2_t v1148 = v7[15];
    float32x2_t v1153 = v7[32];
    float32x2_t v1158 = v7[33];
    float32x2_t v1193 = v7[18];
    float32x2_t v1198 = v7[19];
    float32x2_t v1203 = v7[0];
    float32x2_t v1204 = vtrn1_f32(v1181, v1181);
    float32x2_t v1205 = vtrn2_f32(v1181, v1181);
    float32x2_t v1208 = v7[1];
    float32x2_t v1243 = v7[22];
    float32x2_t v1248 = v7[23];
    float32x2_t v1253 = v7[4];
    float32x2_t v1258 = v7[5];
    float32x2_t v1293 = v7[26];
    float32x2_t v1298 = v7[27];
    float32x2_t v1303 = v7[8];
    float32x2_t v1308 = v7[9];
    float32x2_t v1343 = v7[30];
    float32x2_t v1348 = v7[31];
    float32x2_t v1353 = v7[12];
    float32x2_t v1358 = v7[13];
    float32x2_t v1366 = v5[0];
    float32x2_t v1515 = (float32x2_t){v1514, v1514};
    float32x2_t v1526 = (float32x2_t){v1525, v1525};
    float32x2_t v1530 = (float32x2_t){v1528, v1529};
    float32x2_t v1537 = (float32x2_t){v1536, v1536};
    float32x2_t v1541 = (float32x2_t){v1540, v1540};
    float32x2_t v1545 = (float32x2_t){v1544, v1544};
    float32x2_t v1549 = (float32x2_t){v1547, v1548};
    float32x2_t v1556 = (float32x2_t){v1554, v1555};
    float32x2_t v1563 = (float32x2_t){v1561, v1562};
    float32x2_t v941 = v5[istride * 9];
    float32x2_t v966 = v5[istride * 2];
    float32x2_t v981 = v5[istride * 11];
    float32x2_t v1016 = v5[istride * 4];
    float32x2_t v1031 = v5[istride * 13];
    float32x2_t v1066 = v5[istride * 6];
    float32x2_t v1081 = v5[istride * 15];
    float32x2_t v1116 = v5[istride * 8];
    float32x2_t v1131 = v5[istride * 17];
    float32x2_t v1166 = v5[istride * 10];
    float32x2_t v1209 = vmul_f32(v1204, v1203);
    float32x2_t v1216 = v5[istride * 12];
    float32x2_t v1231 = v5[istride * 3];
    float32x2_t v1266 = v5[istride * 14];
    float32x2_t v1281 = v5[istride * 5];
    float32x2_t v1316 = v5[istride * 16];
    float32x2_t v1331 = v5[istride * 7];
    float32x2_t v1532 = vmul_f32(v1564, v1530);
    float32x2_t v1551 = vmul_f32(v1564, v1549);
    float32x2_t v1558 = vmul_f32(v1564, v1556);
    float32x2_t v1565 = vmul_f32(v1564, v1563);
    float32x2_t v954 = vtrn1_f32(v941, v941);
    float32x2_t v955 = vtrn2_f32(v941, v941);
    float32x2_t v994 = vtrn1_f32(v966, v966);
    float32x2_t v995 = vtrn2_f32(v966, v966);
    float32x2_t v1004 = vtrn1_f32(v981, v981);
    float32x2_t v1005 = vtrn2_f32(v981, v981);
    float32x2_t v1044 = vtrn1_f32(v1016, v1016);
    float32x2_t v1045 = vtrn2_f32(v1016, v1016);
    float32x2_t v1054 = vtrn1_f32(v1031, v1031);
    float32x2_t v1055 = vtrn2_f32(v1031, v1031);
    float32x2_t v1094 = vtrn1_f32(v1066, v1066);
    float32x2_t v1095 = vtrn2_f32(v1066, v1066);
    float32x2_t v1104 = vtrn1_f32(v1081, v1081);
    float32x2_t v1105 = vtrn2_f32(v1081, v1081);
    float32x2_t v1144 = vtrn1_f32(v1116, v1116);
    float32x2_t v1145 = vtrn2_f32(v1116, v1116);
    float32x2_t v1154 = vtrn1_f32(v1131, v1131);
    float32x2_t v1155 = vtrn2_f32(v1131, v1131);
    float32x2_t v1194 = vtrn1_f32(v1166, v1166);
    float32x2_t v1195 = vtrn2_f32(v1166, v1166);
    float32x2_t v1211 = vfma_f32(v1209, v1205, v1208);
    float32x2_t v1244 = vtrn1_f32(v1216, v1216);
    float32x2_t v1245 = vtrn2_f32(v1216, v1216);
    float32x2_t v1254 = vtrn1_f32(v1231, v1231);
    float32x2_t v1255 = vtrn2_f32(v1231, v1231);
    float32x2_t v1294 = vtrn1_f32(v1266, v1266);
    float32x2_t v1295 = vtrn2_f32(v1266, v1266);
    float32x2_t v1304 = vtrn1_f32(v1281, v1281);
    float32x2_t v1305 = vtrn2_f32(v1281, v1281);
    float32x2_t v1344 = vtrn1_f32(v1316, v1316);
    float32x2_t v1345 = vtrn2_f32(v1316, v1316);
    float32x2_t v1354 = vtrn1_f32(v1331, v1331);
    float32x2_t v1355 = vtrn2_f32(v1331, v1331);
    float32x2_t v959 = vmul_f32(v954, v953);
    float32x2_t v999 = vmul_f32(v994, v993);
    float32x2_t v1009 = vmul_f32(v1004, v1003);
    float32x2_t v1049 = vmul_f32(v1044, v1043);
    float32x2_t v1059 = vmul_f32(v1054, v1053);
    float32x2_t v1099 = vmul_f32(v1094, v1093);
    float32x2_t v1109 = vmul_f32(v1104, v1103);
    float32x2_t v1149 = vmul_f32(v1144, v1143);
    float32x2_t v1159 = vmul_f32(v1154, v1153);
    float32x2_t v1199 = vmul_f32(v1194, v1193);
    float32x2_t v1249 = vmul_f32(v1244, v1243);
    float32x2_t v1259 = vmul_f32(v1254, v1253);
    float32x2_t v1299 = vmul_f32(v1294, v1293);
    float32x2_t v1309 = vmul_f32(v1304, v1303);
    float32x2_t v1349 = vmul_f32(v1344, v1343);
    float32x2_t v1359 = vmul_f32(v1354, v1353);
    float32x2_t v961 = vfma_f32(v959, v955, v958);
    float32x2_t v1001 = vfma_f32(v999, v995, v998);
    float32x2_t v1011 = vfma_f32(v1009, v1005, v1008);
    float32x2_t v1051 = vfma_f32(v1049, v1045, v1048);
    float32x2_t v1061 = vfma_f32(v1059, v1055, v1058);
    float32x2_t v1101 = vfma_f32(v1099, v1095, v1098);
    float32x2_t v1111 = vfma_f32(v1109, v1105, v1108);
    float32x2_t v1151 = vfma_f32(v1149, v1145, v1148);
    float32x2_t v1161 = vfma_f32(v1159, v1155, v1158);
    float32x2_t v1201 = vfma_f32(v1199, v1195, v1198);
    float32x2_t v1251 = vfma_f32(v1249, v1245, v1248);
    float32x2_t v1261 = vfma_f32(v1259, v1255, v1258);
    float32x2_t v1301 = vfma_f32(v1299, v1295, v1298);
    float32x2_t v1311 = vfma_f32(v1309, v1305, v1308);
    float32x2_t v1351 = vfma_f32(v1349, v1345, v1348);
    float32x2_t v1361 = vfma_f32(v1359, v1355, v1358);
    float32x2_t v1367 = vadd_f32(v1366, v961);
    float32x2_t v1368 = vsub_f32(v1366, v961);
    float32x2_t v1369 = vadd_f32(v1001, v1011);
    float32x2_t v1370 = vsub_f32(v1001, v1011);
    float32x2_t v1371 = vadd_f32(v1051, v1061);
    float32x2_t v1372 = vsub_f32(v1051, v1061);
    float32x2_t v1373 = vadd_f32(v1101, v1111);
    float32x2_t v1374 = vsub_f32(v1101, v1111);
    float32x2_t v1375 = vadd_f32(v1151, v1161);
    float32x2_t v1376 = vsub_f32(v1151, v1161);
    float32x2_t v1377 = vadd_f32(v1201, v1211);
    float32x2_t v1378 = vsub_f32(v1201, v1211);
    float32x2_t v1379 = vadd_f32(v1251, v1261);
    float32x2_t v1380 = vsub_f32(v1251, v1261);
    float32x2_t v1381 = vadd_f32(v1301, v1311);
    float32x2_t v1382 = vsub_f32(v1301, v1311);
    float32x2_t v1383 = vadd_f32(v1351, v1361);
    float32x2_t v1384 = vsub_f32(v1351, v1361);
    float32x2_t v1385 = vadd_f32(v1369, v1383);
    float32x2_t v1386 = vsub_f32(v1369, v1383);
    float32x2_t v1387 = vadd_f32(v1381, v1371);
    float32x2_t v1388 = vsub_f32(v1381, v1371);
    float32x2_t v1389 = vadd_f32(v1373, v1379);
    float32x2_t v1390 = vsub_f32(v1373, v1379);
    float32x2_t v1391 = vadd_f32(v1375, v1377);
    float32x2_t v1392 = vsub_f32(v1375, v1377);
    float32x2_t v1489 = vadd_f32(v1370, v1384);
    float32x2_t v1490 = vsub_f32(v1370, v1384);
    float32x2_t v1491 = vadd_f32(v1382, v1372);
    float32x2_t v1492 = vsub_f32(v1382, v1372);
    float32x2_t v1493 = vadd_f32(v1374, v1380);
    float32x2_t v1494 = vsub_f32(v1374, v1380);
    float32x2_t v1495 = vadd_f32(v1376, v1378);
    float32x2_t v1496 = vsub_f32(v1376, v1378);
    float32x2_t v1393 = vadd_f32(v1385, v1387);
    float32x2_t v1397 = vadd_f32(v1386, v1388);
    float32x2_t v1399 = vsub_f32(v1385, v1387);
    float32x2_t v1400 = vsub_f32(v1387, v1391);
    float32x2_t v1401 = vsub_f32(v1391, v1385);
    float32x2_t v1402 = vsub_f32(v1386, v1388);
    float32x2_t v1403 = vsub_f32(v1388, v1392);
    float32x2_t v1404 = vsub_f32(v1392, v1386);
    float32x2_t v1423 = vmul_f32(v1389, v1526);
    float32x2_t v1429 = vrev64_f32(v1390);
    float32x2_t v1497 = vadd_f32(v1489, v1491);
    float32x2_t v1501 = vadd_f32(v1490, v1492);
    float32x2_t v1503 = vsub_f32(v1489, v1491);
    float32x2_t v1504 = vsub_f32(v1491, v1495);
    float32x2_t v1505 = vsub_f32(v1495, v1489);
    float32x2_t v1506 = vsub_f32(v1490, v1492);
    float32x2_t v1507 = vsub_f32(v1492, v1496);
    float32x2_t v1508 = vsub_f32(v1496, v1490);
    float32x2_t v1527 = vmul_f32(v1493, v1526);
    float32x2_t v1533 = vrev64_f32(v1494);
    float32x2_t v1394 = vadd_f32(v1393, v1391);
    float32x2_t v1398 = vadd_f32(v1397, v1392);
    float32x2_t v1430 = vmul_f32(v1429, v1532);
    float32x2_t v1434 = vmul_f32(v1399, v1537);
    float32x2_t v1438 = vmul_f32(v1400, v1541);
    float32x2_t v1442 = vmul_f32(v1401, v1545);
    float32x2_t v1448 = vrev64_f32(v1402);
    float32x2_t v1455 = vrev64_f32(v1403);
    float32x2_t v1462 = vrev64_f32(v1404);
    float32x2_t v1498 = vadd_f32(v1497, v1495);
    float32x2_t v1502 = vadd_f32(v1501, v1496);
    float32x2_t v1534 = vmul_f32(v1533, v1532);
    float32x2_t v1538 = vmul_f32(v1503, v1537);
    float32x2_t v1542 = vmul_f32(v1504, v1541);
    float32x2_t v1546 = vmul_f32(v1505, v1545);
    float32x2_t v1552 = vrev64_f32(v1506);
    float32x2_t v1559 = vrev64_f32(v1507);
    float32x2_t v1566 = vrev64_f32(v1508);
    float32x2_t v1395 = vadd_f32(v1394, v1389);
    float32x2_t v1412 = vmul_f32(v1394, v1515);
    float32x2_t v1418 = vrev64_f32(v1398);
    float32x2_t v1449 = vmul_f32(v1448, v1551);
    float32x2_t v1456 = vmul_f32(v1455, v1558);
    float32x2_t v1463 = vmul_f32(v1462, v1565);
    float32x2_t v1499 = vadd_f32(v1498, v1493);
    float32x2_t v1516 = vmul_f32(v1498, v1515);
    float32x2_t v1522 = vrev64_f32(v1502);
    float32x2_t v1553 = vmul_f32(v1552, v1551);
    float32x2_t v1560 = vmul_f32(v1559, v1558);
    float32x2_t v1567 = vmul_f32(v1566, v1565);
    float32x2_t v1396 = vadd_f32(v1395, v1367);
    float32x2_t v1419 = vmul_f32(v1418, v1532);
    float32x2_t v1464 = vadd_f32(v1412, v1412);
    float32x2_t v1477 = vadd_f32(v1430, v1449);
    float32x2_t v1479 = vsub_f32(v1430, v1456);
    float32x2_t v1481 = vsub_f32(v1430, v1449);
    float32x2_t v1500 = vadd_f32(v1499, v1368);
    float32x2_t v1523 = vmul_f32(v1522, v1532);
    float32x2_t v1568 = vadd_f32(v1516, v1516);
    float32x2_t v1581 = vadd_f32(v1534, v1553);
    float32x2_t v1583 = vsub_f32(v1534, v1560);
    float32x2_t v1585 = vsub_f32(v1534, v1553);
    float32x2_t v1465 = vadd_f32(v1464, v1412);
    float32x2_t v1469 = vadd_f32(v1396, v1423);
    float32x2_t v1478 = vadd_f32(v1477, v1456);
    float32x2_t v1480 = vadd_f32(v1479, v1463);
    float32x2_t v1482 = vsub_f32(v1481, v1463);
    float32x2_t v1569 = vadd_f32(v1568, v1516);
    float32x2_t v1573 = vadd_f32(v1500, v1527);
    float32x2_t v1582 = vadd_f32(v1581, v1560);
    float32x2_t v1584 = vadd_f32(v1583, v1567);
    float32x2_t v1586 = vsub_f32(v1585, v1567);
    v6[0] = v1396;
    v6[ostride * 9] = v1500;
    float32x2_t v1466 = vadd_f32(v1396, v1465);
    float32x2_t v1470 = vadd_f32(v1469, v1464);
    float32x2_t v1570 = vadd_f32(v1500, v1569);
    float32x2_t v1574 = vadd_f32(v1573, v1568);
    float32x2_t v1467 = vadd_f32(v1466, v1419);
    float32x2_t v1468 = vsub_f32(v1466, v1419);
    float32x2_t v1471 = vadd_f32(v1470, v1434);
    float32x2_t v1473 = vsub_f32(v1470, v1438);
    float32x2_t v1475 = vsub_f32(v1470, v1434);
    float32x2_t v1571 = vadd_f32(v1570, v1523);
    float32x2_t v1572 = vsub_f32(v1570, v1523);
    float32x2_t v1575 = vadd_f32(v1574, v1538);
    float32x2_t v1577 = vsub_f32(v1574, v1542);
    float32x2_t v1579 = vsub_f32(v1574, v1538);
    float32x2_t v1472 = vadd_f32(v1471, v1438);
    float32x2_t v1474 = vadd_f32(v1473, v1442);
    float32x2_t v1476 = vsub_f32(v1475, v1442);
    float32x2_t v1576 = vadd_f32(v1575, v1542);
    float32x2_t v1578 = vadd_f32(v1577, v1546);
    float32x2_t v1580 = vsub_f32(v1579, v1546);
    v6[ostride * 12] = v1468;
    v6[ostride * 3] = v1572;
    v6[ostride * 6] = v1467;
    v6[ostride * 15] = v1571;
    float32x2_t v1483 = vadd_f32(v1472, v1478);
    float32x2_t v1484 = vsub_f32(v1472, v1478);
    float32x2_t v1485 = vadd_f32(v1474, v1480);
    float32x2_t v1486 = vsub_f32(v1474, v1480);
    float32x2_t v1487 = vadd_f32(v1476, v1482);
    float32x2_t v1488 = vsub_f32(v1476, v1482);
    float32x2_t v1587 = vadd_f32(v1576, v1582);
    float32x2_t v1588 = vsub_f32(v1576, v1582);
    float32x2_t v1589 = vadd_f32(v1578, v1584);
    float32x2_t v1590 = vsub_f32(v1578, v1584);
    float32x2_t v1591 = vadd_f32(v1580, v1586);
    float32x2_t v1592 = vsub_f32(v1580, v1586);
    v6[ostride * 10] = v1484;
    v6[ostride] = v1588;
    v6[ostride * 2] = v1485;
    v6[ostride * 11] = v1589;
    v6[ostride * 4] = v1488;
    v6[ostride * 13] = v1592;
    v6[ostride * 14] = v1487;
    v6[ostride * 5] = v1591;
    v6[ostride * 16] = v1486;
    v6[ostride * 7] = v1590;
    v6[ostride * 8] = v1483;
    v6[ostride * 17] = v1587;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu18(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v434 = -5.0000000000000000e-01F;
    float v446 = -1.4999999999999998e+00F;
    float v451 = -8.6602540378443871e-01F;
    float v458 = 7.6604444311897801e-01F;
    float v463 = 9.3969262078590832e-01F;
    float v468 = -1.7364817766693039e-01F;
    float v473 = -6.4278760968653925e-01F;
    float v480 = 3.4202014332566888e-01F;
    float v487 = -9.8480775301220802e-01F;
    const float32x2_t *v740 = &v5[v0];
    float32x2_t *v863 = &v6[v2];
    int64_t v19 = v0 * 9;
    int64_t v34 = v0 * 2;
    int64_t v45 = v0 * 11;
    int64_t v64 = v0 * 4;
    int64_t v75 = v0 * 13;
    int64_t v94 = v0 * 6;
    int64_t v105 = v0 * 15;
    int64_t v124 = v0 * 8;
    int64_t v135 = v0 * 17;
    int64_t v154 = v0 * 10;
    int64_t v184 = v0 * 12;
    int64_t v195 = v0 * 3;
    int64_t v214 = v0 * 14;
    int64_t v225 = v0 * 5;
    int64_t v244 = v0 * 16;
    int64_t v255 = v0 * 7;
    float v454 = v4 * v451;
    float v476 = v4 * v473;
    float v483 = v4 * v480;
    float v490 = v4 * v487;
    int64_t v526 = v2 * 9;
    int64_t v533 = v2 * 10;
    int64_t v547 = v2 * 2;
    int64_t v554 = v2 * 11;
    int64_t v561 = v2 * 12;
    int64_t v568 = v2 * 3;
    int64_t v575 = v2 * 4;
    int64_t v582 = v2 * 13;
    int64_t v589 = v2 * 14;
    int64_t v596 = v2 * 5;
    int64_t v603 = v2 * 6;
    int64_t v610 = v2 * 15;
    int64_t v617 = v2 * 16;
    int64_t v624 = v2 * 7;
    int64_t v631 = v2 * 8;
    int64_t v638 = v2 * 17;
    const float32x2_t *v804 = &v5[0];
    svfloat32_t v819 = svdup_n_f32(v434);
    svfloat32_t v821 = svdup_n_f32(v446);
    svfloat32_t v823 = svdup_n_f32(v458);
    svfloat32_t v824 = svdup_n_f32(v463);
    svfloat32_t v825 = svdup_n_f32(v468);
    float32x2_t *v836 = &v6[0];
    svfloat32_t v1013 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v740)[0]));
    svfloat32_t v31 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v57 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v61 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v87 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v91 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v117 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v121 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[14]));
    svfloat32_t v147 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v151 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[16]));
    svfloat32_t v177 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v181 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v207 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v211 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v237 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v241 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v267 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[15]));
    svfloat32_t v271 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    const float32x2_t *v650 = &v5[v19];
    const float32x2_t *v659 = &v5[v34];
    const float32x2_t *v668 = &v5[v45];
    const float32x2_t *v677 = &v5[v64];
    const float32x2_t *v686 = &v5[v75];
    const float32x2_t *v695 = &v5[v94];
    const float32x2_t *v704 = &v5[v105];
    const float32x2_t *v713 = &v5[v124];
    const float32x2_t *v722 = &v5[v135];
    const float32x2_t *v731 = &v5[v154];
    const float32x2_t *v749 = &v5[v184];
    const float32x2_t *v758 = &v5[v195];
    const float32x2_t *v767 = &v5[v214];
    const float32x2_t *v776 = &v5[v225];
    const float32x2_t *v785 = &v5[v244];
    const float32x2_t *v794 = &v5[v255];
    svfloat32_t v822 = svdup_n_f32(v454);
    svfloat32_t v826 = svdup_n_f32(v476);
    svfloat32_t v827 = svdup_n_f32(v483);
    svfloat32_t v828 = svdup_n_f32(v490);
    float32x2_t *v845 = &v6[v526];
    float32x2_t *v854 = &v6[v533];
    float32x2_t *v872 = &v6[v547];
    float32x2_t *v881 = &v6[v554];
    float32x2_t *v890 = &v6[v561];
    float32x2_t *v899 = &v6[v568];
    float32x2_t *v908 = &v6[v575];
    float32x2_t *v917 = &v6[v582];
    float32x2_t *v926 = &v6[v589];
    float32x2_t *v935 = &v6[v596];
    float32x2_t *v944 = &v6[v603];
    float32x2_t *v953 = &v6[v610];
    float32x2_t *v962 = &v6[v617];
    float32x2_t *v971 = &v6[v624];
    float32x2_t *v980 = &v6[v631];
    float32x2_t *v989 = &v6[v638];
    svfloat32_t v1027 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v804)[0]));
    svfloat32_t zero182 = svdup_n_f32(0);
    svfloat32_t v182 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero182, v1013, v181, 0), v1013,
        v181, 90);
    svfloat32_t v993 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v650)[0]));
    svfloat32_t v995 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v659)[0]));
    svfloat32_t v997 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v668)[0]));
    svfloat32_t v999 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v677)[0]));
    svfloat32_t v1001 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v686)[0]));
    svfloat32_t v1003 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v695)[0]));
    svfloat32_t v1005 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v704)[0]));
    svfloat32_t v1007 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v713)[0]));
    svfloat32_t v1009 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v722)[0]));
    svfloat32_t v1011 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v731)[0]));
    svfloat32_t v1015 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v749)[0]));
    svfloat32_t v1017 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v758)[0]));
    svfloat32_t v1019 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v767)[0]));
    svfloat32_t v1021 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v776)[0]));
    svfloat32_t v1023 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v785)[0]));
    svfloat32_t v1025 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v794)[0]));
    svfloat32_t zero32 = svdup_n_f32(0);
    svfloat32_t v32 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero32, v993, v31, 0),
                     v993, v31, 90);
    svfloat32_t zero58 = svdup_n_f32(0);
    svfloat32_t v58 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero58, v995, v57, 0),
                     v995, v57, 90);
    svfloat32_t zero62 = svdup_n_f32(0);
    svfloat32_t v62 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero62, v997, v61, 0),
                     v997, v61, 90);
    svfloat32_t zero88 = svdup_n_f32(0);
    svfloat32_t v88 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero88, v999, v87, 0),
                     v999, v87, 90);
    svfloat32_t zero92 = svdup_n_f32(0);
    svfloat32_t v92 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero92, v1001, v91, 0),
                     v1001, v91, 90);
    svfloat32_t zero118 = svdup_n_f32(0);
    svfloat32_t v118 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero118, v1003, v117, 0), v1003,
        v117, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero122, v1005, v121, 0), v1005,
        v121, 90);
    svfloat32_t zero148 = svdup_n_f32(0);
    svfloat32_t v148 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero148, v1007, v147, 0), v1007,
        v147, 90);
    svfloat32_t zero152 = svdup_n_f32(0);
    svfloat32_t v152 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero152, v1009, v151, 0), v1009,
        v151, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero178, v1011, v177, 0), v1011,
        v177, 90);
    svfloat32_t zero208 = svdup_n_f32(0);
    svfloat32_t v208 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero208, v1015, v207, 0), v1015,
        v207, 90);
    svfloat32_t zero212 = svdup_n_f32(0);
    svfloat32_t v212 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero212, v1017, v211, 0), v1017,
        v211, 90);
    svfloat32_t zero238 = svdup_n_f32(0);
    svfloat32_t v238 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero238, v1019, v237, 0), v1019,
        v237, 90);
    svfloat32_t zero242 = svdup_n_f32(0);
    svfloat32_t v242 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero242, v1021, v241, 0), v1021,
        v241, 90);
    svfloat32_t zero268 = svdup_n_f32(0);
    svfloat32_t v268 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero268, v1023, v267, 0), v1023,
        v267, 90);
    svfloat32_t zero272 = svdup_n_f32(0);
    svfloat32_t v272 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero272, v1025, v271, 0), v1025,
        v271, 90);
    svfloat32_t v280 = svadd_f32_x(svptrue_b32(), v1027, v32);
    svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v1027, v32);
    svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v58, v62);
    svfloat32_t v283 = svsub_f32_x(svptrue_b32(), v58, v62);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v88, v92);
    svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v88, v92);
    svfloat32_t v286 = svadd_f32_x(svptrue_b32(), v118, v122);
    svfloat32_t v287 = svsub_f32_x(svptrue_b32(), v118, v122);
    svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v148, v152);
    svfloat32_t v289 = svsub_f32_x(svptrue_b32(), v148, v152);
    svfloat32_t v290 = svadd_f32_x(svptrue_b32(), v178, v182);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v178, v182);
    svfloat32_t v292 = svadd_f32_x(svptrue_b32(), v208, v212);
    svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v208, v212);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v238, v242);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v238, v242);
    svfloat32_t v296 = svadd_f32_x(svptrue_b32(), v268, v272);
    svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v268, v272);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v282, v296);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v282, v296);
    svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v294, v284);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v294, v284);
    svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v286, v292);
    svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v286, v292);
    svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v288, v290);
    svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v288, v290);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v283, v297);
    svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v283, v297);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v295, v285);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v295, v285);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v287, v293);
    svfloat32_t v413 = svsub_f32_x(svptrue_b32(), v287, v293);
    svfloat32_t v414 = svadd_f32_x(svptrue_b32(), v289, v291);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v289, v291);
    svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v298, v300);
    svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v299, v301);
    svfloat32_t v312 = svsub_f32_x(svptrue_b32(), v298, v300);
    svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v300, v304);
    svfloat32_t v314 = svsub_f32_x(svptrue_b32(), v304, v298);
    svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v299, v301);
    svfloat32_t v316 = svsub_f32_x(svptrue_b32(), v301, v305);
    svfloat32_t v317 = svsub_f32_x(svptrue_b32(), v305, v299);
    svfloat32_t zero346 = svdup_n_f32(0);
    svfloat32_t v346 = svcmla_f32_x(pred_full, zero346, v822, v303, 90);
    svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v408, v410);
    svfloat32_t v420 = svadd_f32_x(svptrue_b32(), v409, v411);
    svfloat32_t v422 = svsub_f32_x(svptrue_b32(), v408, v410);
    svfloat32_t v423 = svsub_f32_x(svptrue_b32(), v410, v414);
    svfloat32_t v424 = svsub_f32_x(svptrue_b32(), v414, v408);
    svfloat32_t v425 = svsub_f32_x(svptrue_b32(), v409, v411);
    svfloat32_t v426 = svsub_f32_x(svptrue_b32(), v411, v415);
    svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v415, v409);
    svfloat32_t zero456 = svdup_n_f32(0);
    svfloat32_t v456 = svcmla_f32_x(pred_full, zero456, v822, v413, 90);
    svfloat32_t v307 = svadd_f32_x(svptrue_b32(), v306, v304);
    svfloat32_t v311 = svadd_f32_x(svptrue_b32(), v310, v305);
    svfloat32_t zero368 = svdup_n_f32(0);
    svfloat32_t v368 = svcmla_f32_x(pred_full, zero368, v826, v315, 90);
    svfloat32_t zero375 = svdup_n_f32(0);
    svfloat32_t v375 = svcmla_f32_x(pred_full, zero375, v827, v316, 90);
    svfloat32_t zero382 = svdup_n_f32(0);
    svfloat32_t v382 = svcmla_f32_x(pred_full, zero382, v828, v317, 90);
    svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v416, v414);
    svfloat32_t v421 = svadd_f32_x(svptrue_b32(), v420, v415);
    svfloat32_t zero478 = svdup_n_f32(0);
    svfloat32_t v478 = svcmla_f32_x(pred_full, zero478, v826, v425, 90);
    svfloat32_t zero485 = svdup_n_f32(0);
    svfloat32_t v485 = svcmla_f32_x(pred_full, zero485, v827, v426, 90);
    svfloat32_t zero492 = svdup_n_f32(0);
    svfloat32_t v492 = svcmla_f32_x(pred_full, zero492, v828, v427, 90);
    svfloat32_t v308 = svadd_f32_x(svptrue_b32(), v307, v302);
    svfloat32_t v327 = svmul_f32_x(svptrue_b32(), v307, v819);
    svfloat32_t zero334 = svdup_n_f32(0);
    svfloat32_t v334 = svcmla_f32_x(pred_full, zero334, v822, v311, 90);
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v346, v368);
    svfloat32_t v398 = svsub_f32_x(svptrue_b32(), v346, v375);
    svfloat32_t v400 = svsub_f32_x(svptrue_b32(), v346, v368);
    svfloat32_t v418 = svadd_f32_x(svptrue_b32(), v417, v412);
    svfloat32_t v437 = svmul_f32_x(svptrue_b32(), v417, v819);
    svfloat32_t zero444 = svdup_n_f32(0);
    svfloat32_t v444 = svcmla_f32_x(pred_full, zero444, v822, v421, 90);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v456, v478);
    svfloat32_t v508 = svsub_f32_x(svptrue_b32(), v456, v485);
    svfloat32_t v510 = svsub_f32_x(svptrue_b32(), v456, v478);
    svfloat32_t v309 = svadd_f32_x(svptrue_b32(), v308, v280);
    svfloat32_t v383 = svadd_f32_x(svptrue_b32(), v327, v327);
    svfloat32_t v397 = svadd_f32_x(svptrue_b32(), v396, v375);
    svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v398, v382);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v400, v382);
    svfloat32_t v419 = svadd_f32_x(svptrue_b32(), v418, v281);
    svfloat32_t v493 = svadd_f32_x(svptrue_b32(), v437, v437);
    svfloat32_t v507 = svadd_f32_x(svptrue_b32(), v506, v485);
    svfloat32_t v509 = svadd_f32_x(svptrue_b32(), v508, v492);
    svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v510, v492);
    svfloat32_t v384 = svmla_f32_x(pred_full, v383, v307, v819);
    svfloat32_t v388 = svmla_f32_x(pred_full, v309, v302, v821);
    svfloat32_t v494 = svmla_f32_x(pred_full, v493, v417, v819);
    svfloat32_t v498 = svmla_f32_x(pred_full, v419, v412, v821);
    svst1_f64(pred_full, (double *)(v836), svreinterpret_f64_f32(v309));
    svst1_f64(pred_full, (double *)(v845), svreinterpret_f64_f32(v419));
    svfloat32_t v385 = svadd_f32_x(svptrue_b32(), v309, v384);
    svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v388, v383);
    svfloat32_t v495 = svadd_f32_x(svptrue_b32(), v419, v494);
    svfloat32_t v499 = svadd_f32_x(svptrue_b32(), v498, v493);
    svfloat32_t v386 = svadd_f32_x(svptrue_b32(), v385, v334);
    svfloat32_t v387 = svsub_f32_x(svptrue_b32(), v385, v334);
    svfloat32_t v390 = svmla_f32_x(pred_full, v389, v312, v823);
    svfloat32_t v392 = svmls_f32_x(pred_full, v389, v313, v824);
    svfloat32_t v394 = svmls_f32_x(pred_full, v389, v312, v823);
    svfloat32_t v496 = svadd_f32_x(svptrue_b32(), v495, v444);
    svfloat32_t v497 = svsub_f32_x(svptrue_b32(), v495, v444);
    svfloat32_t v500 = svmla_f32_x(pred_full, v499, v422, v823);
    svfloat32_t v502 = svmls_f32_x(pred_full, v499, v423, v824);
    svfloat32_t v504 = svmls_f32_x(pred_full, v499, v422, v823);
    svfloat32_t v391 = svmla_f32_x(pred_full, v390, v313, v824);
    svfloat32_t v393 = svmla_f32_x(pred_full, v392, v314, v825);
    svfloat32_t v395 = svmls_f32_x(pred_full, v394, v314, v825);
    svfloat32_t v501 = svmla_f32_x(pred_full, v500, v423, v824);
    svfloat32_t v503 = svmla_f32_x(pred_full, v502, v424, v825);
    svfloat32_t v505 = svmls_f32_x(pred_full, v504, v424, v825);
    svst1_f64(pred_full, (double *)(v890), svreinterpret_f64_f32(v387));
    svst1_f64(pred_full, (double *)(v899), svreinterpret_f64_f32(v497));
    svst1_f64(pred_full, (double *)(v944), svreinterpret_f64_f32(v386));
    svst1_f64(pred_full, (double *)(v953), svreinterpret_f64_f32(v496));
    svfloat32_t v402 = svadd_f32_x(svptrue_b32(), v391, v397);
    svfloat32_t v403 = svsub_f32_x(svptrue_b32(), v391, v397);
    svfloat32_t v404 = svadd_f32_x(svptrue_b32(), v393, v399);
    svfloat32_t v405 = svsub_f32_x(svptrue_b32(), v393, v399);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v395, v401);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v395, v401);
    svfloat32_t v512 = svadd_f32_x(svptrue_b32(), v501, v507);
    svfloat32_t v513 = svsub_f32_x(svptrue_b32(), v501, v507);
    svfloat32_t v514 = svadd_f32_x(svptrue_b32(), v503, v509);
    svfloat32_t v515 = svsub_f32_x(svptrue_b32(), v503, v509);
    svfloat32_t v516 = svadd_f32_x(svptrue_b32(), v505, v511);
    svfloat32_t v517 = svsub_f32_x(svptrue_b32(), v505, v511);
    svst1_f64(pred_full, (double *)(v854), svreinterpret_f64_f32(v403));
    svst1_f64(pred_full, (double *)(v863), svreinterpret_f64_f32(v513));
    svst1_f64(pred_full, (double *)(v872), svreinterpret_f64_f32(v404));
    svst1_f64(pred_full, (double *)(v881), svreinterpret_f64_f32(v514));
    svst1_f64(pred_full, (double *)(v908), svreinterpret_f64_f32(v407));
    svst1_f64(pred_full, (double *)(v917), svreinterpret_f64_f32(v517));
    svst1_f64(pred_full, (double *)(v926), svreinterpret_f64_f32(v406));
    svst1_f64(pred_full, (double *)(v935), svreinterpret_f64_f32(v516));
    svst1_f64(pred_full, (double *)(v962), svreinterpret_f64_f32(v405));
    svst1_f64(pred_full, (double *)(v971), svreinterpret_f64_f32(v515));
    svst1_f64(pred_full, (double *)(v980), svreinterpret_f64_f32(v402));
    svst1_f64(pred_full, (double *)(v989), svreinterpret_f64_f32(v512));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu19(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v1157 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v675 = -1.0555555555555556e+00F;
    float v680 = 1.7752228513927079e-01F;
    float v685 = -1.2820077502191529e-01F;
    float v690 = 4.9321510117355499e-02F;
    float v695 = 5.7611011491005903e-01F;
    float v700 = -7.4996449655536279e-01F;
    float v705 = -1.7385438164530381e-01F;
    float v710 = -2.1729997561977314e+00F;
    float v715 = -1.7021211726914738e+00F;
    float v720 = 4.7087858350625778e-01F;
    float v725 = -2.0239400846888440e+00F;
    float v730 = 1.0551641201664090e-01F;
    float v735 = 2.1294564967054850e+00F;
    float v740 = -7.5087543897371167e-01F;
    float v745 = 1.4812817695157160e-01F;
    float v750 = 8.9900361592528333e-01F;
    float v755 = -6.2148246772602778e-01F;
    float v760 = -7.9869352098712687e-01F;
    float v765 = -4.7339199623771833e-01F;
    float v769 = -2.4216105241892630e-01F;
    float v770 = 2.4216105241892630e-01F;
    float v777 = -5.9368607967505101e-02F;
    float v778 = 5.9368607967505101e-02F;
    float v785 = 1.2578688255176201e-02F;
    float v786 = -1.2578688255176201e-02F;
    float v793 = -4.6789919712328903e-02F;
    float v794 = 4.6789919712328903e-02F;
    float v801 = -9.3750121913782358e-01F;
    float v802 = 9.3750121913782358e-01F;
    float v809 = -5.0111537043352902e-02F;
    float v810 = 5.0111537043352902e-02F;
    float v817 = -9.8761275618117661e-01F;
    float v818 = 9.8761275618117661e-01F;
    float v825 = -1.1745786501205959e+00F;
    float v826 = 1.1745786501205959e+00F;
    float v833 = 1.1114482296234993e+00F;
    float v834 = -1.1114482296234993e+00F;
    float v841 = 2.2860268797440955e+00F;
    float v842 = -2.2860268797440955e+00F;
    float v849 = 2.6420523257930939e-01F;
    float v850 = -2.6420523257930939e-01F;
    float v857 = 2.1981792779352136e+00F;
    float v858 = -2.1981792779352136e+00F;
    float v865 = 1.9339740453559042e+00F;
    float v866 = -1.9339740453559042e+00F;
    float v873 = -7.4825847091254893e-01F;
    float v874 = 7.4825847091254893e-01F;
    float v881 = -4.7820835642768872e-01F;
    float v882 = 4.7820835642768872e-01F;
    float v889 = 2.7005011448486022e-01F;
    float v890 = -2.7005011448486022e-01F;
    float v897 = -3.4642356159542270e-01F;
    float v898 = 3.4642356159542270e-01F;
    float v905 = -8.3485429360688279e-01F;
    float v906 = 8.3485429360688279e-01F;
    float v913 = -3.9375928506743518e-01F;
    float v914 = 3.9375928506743518e-01F;
    float32x2_t v916 = (float32x2_t){v4, v4};
    const float32x2_t *v2124 = &v5[istride];
    float32x2_t *v2340 = &v6[ostride];
    float32x2_t v676 = (float32x2_t){v675, v675};
    float32x2_t v681 = (float32x2_t){v680, v680};
    float32x2_t v686 = (float32x2_t){v685, v685};
    float32x2_t v691 = (float32x2_t){v690, v690};
    float32x2_t v696 = (float32x2_t){v695, v695};
    float32x2_t v701 = (float32x2_t){v700, v700};
    float32x2_t v706 = (float32x2_t){v705, v705};
    float32x2_t v711 = (float32x2_t){v710, v710};
    float32x2_t v716 = (float32x2_t){v715, v715};
    float32x2_t v721 = (float32x2_t){v720, v720};
    float32x2_t v726 = (float32x2_t){v725, v725};
    float32x2_t v731 = (float32x2_t){v730, v730};
    float32x2_t v736 = (float32x2_t){v735, v735};
    float32x2_t v741 = (float32x2_t){v740, v740};
    float32x2_t v746 = (float32x2_t){v745, v745};
    float32x2_t v751 = (float32x2_t){v750, v750};
    float32x2_t v756 = (float32x2_t){v755, v755};
    float32x2_t v761 = (float32x2_t){v760, v760};
    float32x2_t v766 = (float32x2_t){v765, v765};
    float32x2_t v771 = (float32x2_t){v769, v770};
    float32x2_t v779 = (float32x2_t){v777, v778};
    float32x2_t v787 = (float32x2_t){v785, v786};
    float32x2_t v795 = (float32x2_t){v793, v794};
    float32x2_t v803 = (float32x2_t){v801, v802};
    float32x2_t v811 = (float32x2_t){v809, v810};
    float32x2_t v819 = (float32x2_t){v817, v818};
    float32x2_t v827 = (float32x2_t){v825, v826};
    float32x2_t v835 = (float32x2_t){v833, v834};
    float32x2_t v843 = (float32x2_t){v841, v842};
    float32x2_t v851 = (float32x2_t){v849, v850};
    float32x2_t v859 = (float32x2_t){v857, v858};
    float32x2_t v867 = (float32x2_t){v865, v866};
    float32x2_t v875 = (float32x2_t){v873, v874};
    float32x2_t v883 = (float32x2_t){v881, v882};
    float32x2_t v891 = (float32x2_t){v889, v890};
    float32x2_t v899 = (float32x2_t){v897, v898};
    float32x2_t v907 = (float32x2_t){v905, v906};
    float32x2_t v915 = (float32x2_t){v913, v914};
    const float32x2_t *v2321 = &v5[0];
    float32x2_t *v2331 = &v6[0];
    float32x4_t v2497 = vld1q_f32((const float32_t *)v2124);
    float32x4_t v61 = vtrn1q_f32(v2497, v2497);
    float32x4_t v62 = vtrn2q_f32(v2497, v2497);
    float32x4_t v66 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v68 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v78 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[34]));
    float32x4_t v80 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[35]));
    float32x4_t v128 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[32]));
    float32x4_t v130 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[33]));
    float32x4_t v140 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v142 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v190 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v192 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v202 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[28]));
    float32x4_t v204 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[29]));
    float32x4_t v252 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v254 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v264 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v266 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v314 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[30]));
    float32x4_t v316 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[31]));
    float32x4_t v326 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v328 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v376 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v378 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v388 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v390 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v438 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v440 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v450 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v452 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v500 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v502 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v512 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v514 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v562 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v564 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v574 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v576 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v677 = vcombine_f32(v676, v676);
    float32x4_t v682 = vcombine_f32(v681, v681);
    float32x4_t v687 = vcombine_f32(v686, v686);
    float32x4_t v692 = vcombine_f32(v691, v691);
    float32x4_t v697 = vcombine_f32(v696, v696);
    float32x4_t v702 = vcombine_f32(v701, v701);
    float32x4_t v707 = vcombine_f32(v706, v706);
    float32x4_t v712 = vcombine_f32(v711, v711);
    float32x4_t v717 = vcombine_f32(v716, v716);
    float32x4_t v722 = vcombine_f32(v721, v721);
    float32x4_t v727 = vcombine_f32(v726, v726);
    float32x4_t v732 = vcombine_f32(v731, v731);
    float32x4_t v737 = vcombine_f32(v736, v736);
    float32x4_t v742 = vcombine_f32(v741, v741);
    float32x4_t v747 = vcombine_f32(v746, v746);
    float32x4_t v752 = vcombine_f32(v751, v751);
    float32x4_t v757 = vcombine_f32(v756, v756);
    float32x4_t v762 = vcombine_f32(v761, v761);
    float32x4_t v767 = vcombine_f32(v766, v766);
    float32x2_t v773 = vmul_f32(v916, v771);
    float32x2_t v781 = vmul_f32(v916, v779);
    float32x2_t v789 = vmul_f32(v916, v787);
    float32x2_t v797 = vmul_f32(v916, v795);
    float32x2_t v805 = vmul_f32(v916, v803);
    float32x2_t v813 = vmul_f32(v916, v811);
    float32x2_t v821 = vmul_f32(v916, v819);
    float32x2_t v829 = vmul_f32(v916, v827);
    float32x2_t v837 = vmul_f32(v916, v835);
    float32x2_t v845 = vmul_f32(v916, v843);
    float32x2_t v853 = vmul_f32(v916, v851);
    float32x2_t v861 = vmul_f32(v916, v859);
    float32x2_t v869 = vmul_f32(v916, v867);
    float32x2_t v877 = vmul_f32(v916, v875);
    float32x2_t v885 = vmul_f32(v916, v883);
    float32x2_t v893 = vmul_f32(v916, v891);
    float32x2_t v901 = vmul_f32(v916, v899);
    float32x2_t v909 = vmul_f32(v916, v907);
    float32x2_t v917 = vmul_f32(v916, v915);
    const float32x2_t *v2133 = &v5[istride * 18];
    const float32x2_t *v2144 = &v5[istride * 2];
    const float32x2_t *v2154 = &v5[istride * 17];
    const float32x2_t *v2166 = &v5[istride * 4];
    const float32x2_t *v2176 = &v5[istride * 15];
    const float32x2_t *v2188 = &v5[istride * 8];
    const float32x2_t *v2198 = &v5[istride * 11];
    const float32x2_t *v2210 = &v5[istride * 16];
    const float32x2_t *v2220 = &v5[istride * 3];
    const float32x2_t *v2232 = &v5[istride * 13];
    const float32x2_t *v2242 = &v5[istride * 6];
    const float32x2_t *v2254 = &v5[istride * 7];
    const float32x2_t *v2264 = &v5[istride * 12];
    const float32x2_t *v2276 = &v5[istride * 14];
    const float32x2_t *v2286 = &v5[istride * 5];
    const float32x2_t *v2298 = &v5[istride * 9];
    const float32x2_t *v2308 = &v5[istride * 10];
    float32x2_t *v2349 = &v6[ostride * 18];
    float32x2_t *v2358 = &v6[ostride * 2];
    float32x2_t *v2367 = &v6[ostride * 17];
    float32x2_t *v2376 = &v6[ostride * 3];
    float32x2_t *v2385 = &v6[ostride * 16];
    float32x2_t *v2394 = &v6[ostride * 4];
    float32x2_t *v2403 = &v6[ostride * 15];
    float32x2_t *v2412 = &v6[ostride * 5];
    float32x2_t *v2421 = &v6[ostride * 14];
    float32x2_t *v2430 = &v6[ostride * 6];
    float32x2_t *v2439 = &v6[ostride * 13];
    float32x2_t *v2448 = &v6[ostride * 7];
    float32x2_t *v2457 = &v6[ostride * 12];
    float32x2_t *v2466 = &v6[ostride * 8];
    float32x2_t *v2475 = &v6[ostride * 11];
    float32x2_t *v2484 = &v6[ostride * 9];
    float32x2_t *v2493 = &v6[ostride * 10];
    float32x4_t v2533 = vld1q_f32((const float32_t *)v2321);
    float32x4_t v67 = vmulq_f32(v61, v66);
    float32x4_t v775 = vcombine_f32(v773, v773);
    float32x4_t v783 = vcombine_f32(v781, v781);
    float32x4_t v791 = vcombine_f32(v789, v789);
    float32x4_t v799 = vcombine_f32(v797, v797);
    float32x4_t v807 = vcombine_f32(v805, v805);
    float32x4_t v815 = vcombine_f32(v813, v813);
    float32x4_t v823 = vcombine_f32(v821, v821);
    float32x4_t v831 = vcombine_f32(v829, v829);
    float32x4_t v839 = vcombine_f32(v837, v837);
    float32x4_t v847 = vcombine_f32(v845, v845);
    float32x4_t v855 = vcombine_f32(v853, v853);
    float32x4_t v863 = vcombine_f32(v861, v861);
    float32x4_t v871 = vcombine_f32(v869, v869);
    float32x4_t v879 = vcombine_f32(v877, v877);
    float32x4_t v887 = vcombine_f32(v885, v885);
    float32x4_t v895 = vcombine_f32(v893, v893);
    float32x4_t v903 = vcombine_f32(v901, v901);
    float32x4_t v911 = vcombine_f32(v909, v909);
    float32x4_t v919 = vcombine_f32(v917, v917);
    float32x4_t v2499 = vld1q_f32((const float32_t *)v2133);
    float32x4_t v2501 = vld1q_f32((const float32_t *)v2144);
    float32x4_t v2503 = vld1q_f32((const float32_t *)v2154);
    float32x4_t v2505 = vld1q_f32((const float32_t *)v2166);
    float32x4_t v2507 = vld1q_f32((const float32_t *)v2176);
    float32x4_t v2509 = vld1q_f32((const float32_t *)v2188);
    float32x4_t v2511 = vld1q_f32((const float32_t *)v2198);
    float32x4_t v2513 = vld1q_f32((const float32_t *)v2210);
    float32x4_t v2515 = vld1q_f32((const float32_t *)v2220);
    float32x4_t v2517 = vld1q_f32((const float32_t *)v2232);
    float32x4_t v2519 = vld1q_f32((const float32_t *)v2242);
    float32x4_t v2521 = vld1q_f32((const float32_t *)v2254);
    float32x4_t v2523 = vld1q_f32((const float32_t *)v2264);
    float32x4_t v2525 = vld1q_f32((const float32_t *)v2276);
    float32x4_t v2527 = vld1q_f32((const float32_t *)v2286);
    float32x4_t v2529 = vld1q_f32((const float32_t *)v2298);
    float32x4_t v2531 = vld1q_f32((const float32_t *)v2308);
    float32x4_t v70 = vfmaq_f32(v67, v62, v68);
    float32x4_t v73 = vtrn1q_f32(v2499, v2499);
    float32x4_t v74 = vtrn2q_f32(v2499, v2499);
    float32x4_t v123 = vtrn1q_f32(v2503, v2503);
    float32x4_t v124 = vtrn2q_f32(v2503, v2503);
    float32x4_t v135 = vtrn1q_f32(v2501, v2501);
    float32x4_t v136 = vtrn2q_f32(v2501, v2501);
    float32x4_t v185 = vtrn1q_f32(v2505, v2505);
    float32x4_t v186 = vtrn2q_f32(v2505, v2505);
    float32x4_t v197 = vtrn1q_f32(v2507, v2507);
    float32x4_t v198 = vtrn2q_f32(v2507, v2507);
    float32x4_t v247 = vtrn1q_f32(v2511, v2511);
    float32x4_t v248 = vtrn2q_f32(v2511, v2511);
    float32x4_t v259 = vtrn1q_f32(v2509, v2509);
    float32x4_t v260 = vtrn2q_f32(v2509, v2509);
    float32x4_t v309 = vtrn1q_f32(v2513, v2513);
    float32x4_t v310 = vtrn2q_f32(v2513, v2513);
    float32x4_t v321 = vtrn1q_f32(v2515, v2515);
    float32x4_t v322 = vtrn2q_f32(v2515, v2515);
    float32x4_t v371 = vtrn1q_f32(v2519, v2519);
    float32x4_t v372 = vtrn2q_f32(v2519, v2519);
    float32x4_t v383 = vtrn1q_f32(v2517, v2517);
    float32x4_t v384 = vtrn2q_f32(v2517, v2517);
    float32x4_t v433 = vtrn1q_f32(v2521, v2521);
    float32x4_t v434 = vtrn2q_f32(v2521, v2521);
    float32x4_t v445 = vtrn1q_f32(v2523, v2523);
    float32x4_t v446 = vtrn2q_f32(v2523, v2523);
    float32x4_t v495 = vtrn1q_f32(v2527, v2527);
    float32x4_t v496 = vtrn2q_f32(v2527, v2527);
    float32x4_t v507 = vtrn1q_f32(v2525, v2525);
    float32x4_t v508 = vtrn2q_f32(v2525, v2525);
    float32x4_t v557 = vtrn1q_f32(v2529, v2529);
    float32x4_t v558 = vtrn2q_f32(v2529, v2529);
    float32x4_t v569 = vtrn1q_f32(v2531, v2531);
    float32x4_t v570 = vtrn2q_f32(v2531, v2531);
    float32x4_t v79 = vmulq_f32(v73, v78);
    float32x4_t v129 = vmulq_f32(v123, v128);
    float32x4_t v141 = vmulq_f32(v135, v140);
    float32x4_t v191 = vmulq_f32(v185, v190);
    float32x4_t v203 = vmulq_f32(v197, v202);
    float32x4_t v253 = vmulq_f32(v247, v252);
    float32x4_t v265 = vmulq_f32(v259, v264);
    float32x4_t v315 = vmulq_f32(v309, v314);
    float32x4_t v327 = vmulq_f32(v321, v326);
    float32x4_t v377 = vmulq_f32(v371, v376);
    float32x4_t v389 = vmulq_f32(v383, v388);
    float32x4_t v439 = vmulq_f32(v433, v438);
    float32x4_t v451 = vmulq_f32(v445, v450);
    float32x4_t v501 = vmulq_f32(v495, v500);
    float32x4_t v513 = vmulq_f32(v507, v512);
    float32x4_t v563 = vmulq_f32(v557, v562);
    float32x4_t v575 = vmulq_f32(v569, v574);
    float32x4_t v82 = vfmaq_f32(v79, v74, v80);
    float32x4_t v132 = vfmaq_f32(v129, v124, v130);
    float32x4_t v144 = vfmaq_f32(v141, v136, v142);
    float32x4_t v194 = vfmaq_f32(v191, v186, v192);
    float32x4_t v206 = vfmaq_f32(v203, v198, v204);
    float32x4_t v256 = vfmaq_f32(v253, v248, v254);
    float32x4_t v268 = vfmaq_f32(v265, v260, v266);
    float32x4_t v318 = vfmaq_f32(v315, v310, v316);
    float32x4_t v330 = vfmaq_f32(v327, v322, v328);
    float32x4_t v380 = vfmaq_f32(v377, v372, v378);
    float32x4_t v392 = vfmaq_f32(v389, v384, v390);
    float32x4_t v442 = vfmaq_f32(v439, v434, v440);
    float32x4_t v454 = vfmaq_f32(v451, v446, v452);
    float32x4_t v504 = vfmaq_f32(v501, v496, v502);
    float32x4_t v516 = vfmaq_f32(v513, v508, v514);
    float32x4_t v566 = vfmaq_f32(v563, v558, v564);
    float32x4_t v578 = vfmaq_f32(v575, v570, v576);
    float32x4_t v579 = vaddq_f32(v70, v82);
    float32x4_t v580 = vsubq_f32(v70, v82);
    float32x4_t v581 = vaddq_f32(v144, v132);
    float32x4_t v582 = vsubq_f32(v132, v144);
    float32x4_t v583 = vaddq_f32(v194, v206);
    float32x4_t v584 = vsubq_f32(v194, v206);
    float32x4_t v585 = vaddq_f32(v268, v256);
    float32x4_t v586 = vsubq_f32(v256, v268);
    float32x4_t v587 = vaddq_f32(v318, v330);
    float32x4_t v588 = vsubq_f32(v318, v330);
    float32x4_t v589 = vaddq_f32(v392, v380);
    float32x4_t v590 = vsubq_f32(v380, v392);
    float32x4_t v591 = vaddq_f32(v442, v454);
    float32x4_t v592 = vsubq_f32(v442, v454);
    float32x4_t v593 = vaddq_f32(v516, v504);
    float32x4_t v594 = vsubq_f32(v504, v516);
    float32x4_t v595 = vaddq_f32(v566, v578);
    float32x4_t v596 = vsubq_f32(v566, v578);
    float32x4_t v597 = vsubq_f32(v579, v591);
    float32x4_t v598 = vsubq_f32(v581, v593);
    float32x4_t v599 = vsubq_f32(v583, v595);
    float32x4_t v600 = vsubq_f32(v585, v591);
    float32x4_t v601 = vsubq_f32(v587, v593);
    float32x4_t v602 = vsubq_f32(v589, v595);
    float32x4_t v603 = vaddq_f32(v579, v585);
    float32x4_t v605 = vaddq_f32(v581, v587);
    float32x4_t v607 = vaddq_f32(v583, v589);
    float32x4_t v637 = vsubq_f32(v580, v592);
    float32x4_t v638 = vsubq_f32(v582, v594);
    float32x4_t v639 = vsubq_f32(v584, v596);
    float32x4_t v640 = vsubq_f32(v586, v592);
    float32x4_t v641 = vsubq_f32(v588, v594);
    float32x4_t v642 = vsubq_f32(v590, v596);
    float32x4_t v643 = vaddq_f32(v580, v586);
    float32x4_t v645 = vaddq_f32(v582, v588);
    float32x4_t v647 = vaddq_f32(v584, v590);
    float32x4_t v604 = vaddq_f32(v603, v591);
    float32x4_t v606 = vaddq_f32(v605, v593);
    float32x4_t v608 = vaddq_f32(v607, v595);
    float32x4_t v609 = vaddq_f32(v597, v599);
    float32x4_t v610 = vaddq_f32(v600, v602);
    float32x4_t v627 = vsubq_f32(v597, v600);
    float32x4_t v628 = vsubq_f32(v599, v602);
    float32x4_t v644 = vaddq_f32(v643, v592);
    float32x4_t v646 = vaddq_f32(v645, v594);
    float32x4_t v648 = vaddq_f32(v647, v596);
    float32x4_t v649 = vaddq_f32(v637, v639);
    float32x4_t v650 = vaddq_f32(v640, v642);
    float32x4_t v659 = vsubq_f32(v637, v640);
    float32x4_t v660 = vsubq_f32(v639, v642);
    float32x4_t v713 = vmulq_f32(v600, v712);
    float32x4_t v728 = vmulq_f32(v602, v727);
    float32x4_t v738 = vmulq_f32(v599, v737);
    float32x4_t v830 = vrev64q_f32(v640);
    float32x4_t v846 = vrev64q_f32(v637);
    float32x4_t v854 = vrev64q_f32(v642);
    float32x4_t v870 = vrev64q_f32(v639);
    float32x4_t v611 = vaddq_f32(v604, v606);
    float32x4_t v621 = vaddq_f32(v610, v601);
    float32x4_t v622 = vaddq_f32(v609, v598);
    float32x4_t v624 = vsubq_f32(v610, v601);
    float32x4_t v625 = vsubq_f32(v609, v598);
    float32x4_t v629 = vsubq_f32(v597, v628);
    float32x4_t v631 = vaddq_f32(v627, v602);
    float32x4_t v634 = vsubq_f32(v604, v608);
    float32x4_t v635 = vsubq_f32(v606, v608);
    float32x4_t v651 = vaddq_f32(v644, v646);
    float32x4_t v653 = vaddq_f32(v650, v641);
    float32x4_t v654 = vaddq_f32(v649, v638);
    float32x4_t v656 = vsubq_f32(v650, v641);
    float32x4_t v657 = vsubq_f32(v649, v638);
    float32x4_t v661 = vsubq_f32(v637, v660);
    float32x4_t v663 = vaddq_f32(v659, v642);
    float32x4_t v666 = vsubq_f32(v644, v648);
    float32x4_t v667 = vsubq_f32(v646, v648);
    float32x4_t v718 = vmulq_f32(v627, v717);
    float32x4_t v733 = vmulq_f32(v628, v732);
    float32x4_t v832 = vmulq_f32(v830, v831);
    float32x4_t v838 = vrev64q_f32(v659);
    float32x4_t v856 = vmulq_f32(v854, v855);
    float32x4_t v862 = vrev64q_f32(v660);
    float32x4_t v872 = vmulq_f32(v870, v871);
    float32x4_t v612 = vaddq_f32(v611, v608);
    float32x4_t v623 = vsubq_f32(v622, v621);
    float32x4_t v626 = vsubq_f32(v625, v624);
    float32x4_t v630 = vsubq_f32(v629, v601);
    float32x4_t v632 = vsubq_f32(v631, v598);
    float32x4_t v636 = vaddq_f32(v634, v635);
    float32x4_t v652 = vaddq_f32(v651, v648);
    float32x4_t v655 = vsubq_f32(v654, v653);
    float32x4_t v658 = vsubq_f32(v657, v656);
    float32x4_t v662 = vsubq_f32(v661, v641);
    float32x4_t v664 = vsubq_f32(v663, v638);
    float32x4_t v668 = vaddq_f32(v666, v667);
    float32x4_t v683 = vmulq_f32(v621, v682);
    float32x4_t v688 = vmulq_f32(v622, v687);
    float32x4_t v698 = vmulq_f32(v624, v697);
    float32x4_t v703 = vmulq_f32(v625, v702);
    float32x4_t v758 = vmulq_f32(v634, v757);
    float32x4_t v763 = vmulq_f32(v635, v762);
    float32x4_t v782 = vrev64q_f32(v653);
    float32x4_t v790 = vrev64q_f32(v654);
    float32x4_t v806 = vrev64q_f32(v656);
    float32x4_t v814 = vrev64q_f32(v657);
    float32x4_t v840 = vmulq_f32(v838, v839);
    float32x4_t v864 = vmulq_f32(v862, v863);
    float32x4_t v902 = vrev64q_f32(v666);
    float32x4_t v910 = vrev64q_f32(v667);
    float32x4_t v620 = vaddq_f32(v2533, v612);
    float32x4_t v633 = vsubq_f32(v630, v632);
    float32x4_t v665 = vsubq_f32(v662, v664);
    float32x4_t v678 = vmulq_f32(v612, v677);
    float32x4_t v693 = vmulq_f32(v623, v692);
    float32x4_t v708 = vmulq_f32(v626, v707);
    float32x4_t v743 = vmulq_f32(v630, v742);
    float32x4_t v748 = vmulq_f32(v632, v747);
    float32x4_t v768 = vmulq_f32(v636, v767);
    float32x4_t v774 = vrev64q_f32(v652);
    float32x4_t v784 = vmulq_f32(v782, v783);
    float32x4_t v792 = vmulq_f32(v790, v791);
    float32x4_t v798 = vrev64q_f32(v655);
    float32x4_t v808 = vmulq_f32(v806, v807);
    float32x4_t v816 = vmulq_f32(v814, v815);
    float32x4_t v822 = vrev64q_f32(v658);
    float32x4_t v878 = vrev64q_f32(v662);
    float32x4_t v886 = vrev64q_f32(v664);
    float32x4_t v904 = vmulq_f32(v902, v903);
    float32x4_t v912 = vmulq_f32(v910, v911);
    float32x4_t v918 = vrev64q_f32(v668);
    float32x4_t v921 = vaddq_f32(v683, v688);
    float32x4_t v922 = vaddq_f32(v698, v703);
    float32x4_t v753 = vmulq_f32(v633, v752);
    float32x4_t v776 = vmulq_f32(v774, v775);
    float32x4_t v800 = vmulq_f32(v798, v799);
    float32x4_t v824 = vmulq_f32(v822, v823);
    float32x4_t v880 = vmulq_f32(v878, v879);
    float32x4_t v888 = vmulq_f32(v886, v887);
    float32x4_t v894 = vrev64q_f32(v665);
    float32x4_t v920 = vmulq_f32(v918, v919);
    float32x4_t v924 = vaddq_f32(v921, v922);
    float32x4_t v925 = vaddq_f32(v683, v693);
    float32x4_t v926 = vaddq_f32(v698, v708);
    float32x4_t v943 = vsubq_f32(v921, v922);
    float32x4_t v945 = vsubq_f32(v758, v768);
    float32x4_t v946 = vsubq_f32(v763, v768);
    float32x4_t v947 = vaddq_f32(v678, v620);
    float32x4_t v952 = vaddq_f32(v784, v792);
    float32x4_t v953 = vaddq_f32(v808, v816);
    vst1q_f32((float32_t *)v2331, v620);
    float32x4_t v896 = vmulq_f32(v894, v895);
    float32x4_t v923 = vaddq_f32(v748, v753);
    float32x4_t v927 = vaddq_f32(v743, v753);
    float32x4_t v928 = vsubq_f32(v713, v924);
    float32x4_t v929 = vaddq_f32(v925, v926);
    float32x4_t v935 = vsubq_f32(v925, v926);
    float32x4_t v940 = vaddq_f32(v924, v738);
    float32x4_t v948 = vaddq_f32(v947, v945);
    float32x4_t v949 = vsubq_f32(v947, v945);
    float32x4_t v951 = vaddq_f32(v947, v946);
    float32x4_t v955 = vaddq_f32(v952, v953);
    float32x4_t v956 = vaddq_f32(v784, v800);
    float32x4_t v957 = vaddq_f32(v808, v824);
    float32x4_t v974 = vsubq_f32(v952, v953);
    float32x4_t v976 = vsubq_f32(v904, v920);
    float32x4_t v977 = vsubq_f32(v912, v920);
    float32x4_t v930 = vsubq_f32(v728, v927);
    float32x4_t v931 = vaddq_f32(v718, v923);
    float32x4_t v933 = vaddq_f32(v929, v733);
    float32x4_t v936 = vaddq_f32(v935, v923);
    float32x4_t v937 = vaddq_f32(v928, v929);
    float32x4_t v944 = vaddq_f32(v943, v927);
    float32x4_t v950 = vsubq_f32(v949, v946);
    float32x4_t v954 = vaddq_f32(v888, v896);
    float32x4_t v958 = vaddq_f32(v880, v896);
    float32x4_t v959 = vsubq_f32(v832, v955);
    float32x4_t v960 = vaddq_f32(v956, v957);
    float32x4_t v966 = vsubq_f32(v956, v957);
    float32x4_t v971 = vaddq_f32(v955, v872);
    float32x4_t v978 = vaddq_f32(v776, v976);
    float32x4_t v979 = vsubq_f32(v776, v976);
    float32x4_t v981 = vaddq_f32(v776, v977);
    float32x4_t v932 = vaddq_f32(v931, v928);
    float32x4_t v934 = vaddq_f32(v933, v930);
    float32x4_t v938 = vfmaq_f32(v937, v597, v722);
    float32x4_t v941 = vaddq_f32(v940, v930);
    float32x4_t v961 = vsubq_f32(v856, v958);
    float32x4_t v962 = vaddq_f32(v840, v954);
    float32x4_t v964 = vaddq_f32(v960, v864);
    float32x4_t v967 = vaddq_f32(v966, v954);
    float32x4_t v968 = vaddq_f32(v959, v960);
    float32x4_t v975 = vaddq_f32(v974, v958);
    float32x4_t v980 = vsubq_f32(v979, v977);
    float32x4_t v986 = vsubq_f32(v944, v936);
    float32x4_t v990 = vsubq_f32(v951, v944);
    float32x4_t v993 = vaddq_f32(v936, v951);
    float32x4_t v939 = vaddq_f32(v938, v927);
    float32x4_t v942 = vaddq_f32(v941, v923);
    float32x4_t v963 = vaddq_f32(v962, v959);
    float32x4_t v965 = vaddq_f32(v964, v961);
    float32x4_t v969 = vfmaq_f32(v968, v846, v847);
    float32x4_t v972 = vaddq_f32(v971, v961);
    float32x4_t v987 = vaddq_f32(v986, v951);
    float32x4_t v991 = vaddq_f32(v932, v948);
    float32x4_t v992 = vaddq_f32(v934, v950);
    float32x4_t v998 = vsubq_f32(v975, v967);
    float32x4_t v1002 = vsubq_f32(v975, v981);
    float32x4_t v1005 = vaddq_f32(v967, v981);
    float32x4_t v970 = vaddq_f32(v969, v958);
    float32x4_t v973 = vaddq_f32(v972, v954);
    float32x4_t v982 = vsubq_f32(v939, v932);
    float32x4_t v984 = vsubq_f32(v942, v934);
    float32x4_t v988 = vsubq_f32(v948, v939);
    float32x4_t v989 = vsubq_f32(v950, v942);
    float32x4_t v999 = vaddq_f32(v998, v981);
    float32x4_t v1003 = vaddq_f32(v963, v978);
    float32x4_t v1004 = vaddq_f32(v965, v980);
    float32x4_t v1029 = vsubq_f32(v993, v1005);
    float32x4_t v1037 = vaddq_f32(v993, v1005);
    float32x4_t v1045 = vaddq_f32(v990, v1002);
    float32x4_t v1053 = vsubq_f32(v990, v1002);
    float32x4_t v983 = vaddq_f32(v982, v948);
    float32x4_t v985 = vaddq_f32(v984, v950);
    float32x4_t v994 = vsubq_f32(v970, v963);
    float32x4_t v996 = vsubq_f32(v973, v965);
    float32x4_t v1000 = vsubq_f32(v978, v970);
    float32x4_t v1001 = vsubq_f32(v980, v973);
    float32x4_t v1061 = vaddq_f32(v992, v1004);
    float32x4_t v1069 = vsubq_f32(v992, v1004);
    float32x4_t v1077 = vaddq_f32(v987, v999);
    float32x4_t v1085 = vsubq_f32(v987, v999);
    float32x4_t v1125 = vsubq_f32(v991, v1003);
    float32x4_t v1133 = vaddq_f32(v991, v1003);
    vst1q_f32((float32_t *)v2358, v1029);
    vst1q_f32((float32_t *)v2367, v1037);
    vst1q_f32((float32_t *)v2376, v1045);
    vst1q_f32((float32_t *)v2385, v1053);
    float32x4_t v995 = vaddq_f32(v994, v978);
    float32x4_t v997 = vaddq_f32(v996, v980);
    float32x4_t v1093 = vaddq_f32(v989, v1001);
    float32x4_t v1101 = vsubq_f32(v989, v1001);
    float32x4_t v1109 = vaddq_f32(v988, v1000);
    float32x4_t v1117 = vsubq_f32(v988, v1000);
    vst1q_f32((float32_t *)v2394, v1061);
    vst1q_f32((float32_t *)v2403, v1069);
    vst1q_f32((float32_t *)v2412, v1077);
    vst1q_f32((float32_t *)v2421, v1085);
    vst1q_f32((float32_t *)v2466, v1125);
    vst1q_f32((float32_t *)v2475, v1133);
    float32x4_t v1013 = vaddq_f32(v983, v995);
    float32x4_t v1021 = vsubq_f32(v983, v995);
    float32x4_t v1141 = vaddq_f32(v985, v997);
    float32x4_t v1149 = vsubq_f32(v985, v997);
    vst1q_f32((float32_t *)v2430, v1093);
    vst1q_f32((float32_t *)v2439, v1101);
    vst1q_f32((float32_t *)v2448, v1109);
    vst1q_f32((float32_t *)v2457, v1117);
    vst1q_f32((float32_t *)v2340, v1013);
    vst1q_f32((float32_t *)v2349, v1021);
    vst1q_f32((float32_t *)v2484, v1141);
    vst1q_f32((float32_t *)v2493, v1149);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v1157 * 2; j < howmany; j += 1) {
    float32x2_t v1169 = v5[istride];
    float v1708 = -1.0555555555555556e+00F;
    float v1712 = 1.7752228513927079e-01F;
    float v1716 = -1.2820077502191529e-01F;
    float v1720 = 4.9321510117355499e-02F;
    float v1724 = 5.7611011491005903e-01F;
    float v1728 = -7.4996449655536279e-01F;
    float v1732 = -1.7385438164530381e-01F;
    float v1736 = -2.1729997561977314e+00F;
    float v1740 = -1.7021211726914738e+00F;
    float v1744 = 4.7087858350625778e-01F;
    float v1748 = -2.0239400846888440e+00F;
    float v1752 = 1.0551641201664090e-01F;
    float v1756 = 2.1294564967054850e+00F;
    float v1760 = -7.5087543897371167e-01F;
    float v1764 = 1.4812817695157160e-01F;
    float v1768 = 8.9900361592528333e-01F;
    float v1772 = -6.2148246772602778e-01F;
    float v1776 = -7.9869352098712687e-01F;
    float v1780 = -4.7339199623771833e-01F;
    float v1783 = -2.4216105241892630e-01F;
    float v1784 = 2.4216105241892630e-01F;
    float v1790 = -5.9368607967505101e-02F;
    float v1791 = 5.9368607967505101e-02F;
    float v1797 = 1.2578688255176201e-02F;
    float v1798 = -1.2578688255176201e-02F;
    float v1804 = -4.6789919712328903e-02F;
    float v1805 = 4.6789919712328903e-02F;
    float v1811 = -9.3750121913782358e-01F;
    float v1812 = 9.3750121913782358e-01F;
    float v1818 = -5.0111537043352902e-02F;
    float v1819 = 5.0111537043352902e-02F;
    float v1825 = -9.8761275618117661e-01F;
    float v1826 = 9.8761275618117661e-01F;
    float v1832 = -1.1745786501205959e+00F;
    float v1833 = 1.1745786501205959e+00F;
    float v1839 = 1.1114482296234993e+00F;
    float v1840 = -1.1114482296234993e+00F;
    float v1846 = 2.2860268797440955e+00F;
    float v1847 = -2.2860268797440955e+00F;
    float v1853 = 2.6420523257930939e-01F;
    float v1854 = -2.6420523257930939e-01F;
    float v1860 = 2.1981792779352136e+00F;
    float v1861 = -2.1981792779352136e+00F;
    float v1867 = 1.9339740453559042e+00F;
    float v1868 = -1.9339740453559042e+00F;
    float v1874 = -7.4825847091254893e-01F;
    float v1875 = 7.4825847091254893e-01F;
    float v1881 = -4.7820835642768872e-01F;
    float v1882 = 4.7820835642768872e-01F;
    float v1888 = 2.7005011448486022e-01F;
    float v1889 = -2.7005011448486022e-01F;
    float v1895 = -3.4642356159542270e-01F;
    float v1896 = 3.4642356159542270e-01F;
    float v1902 = -8.3485429360688279e-01F;
    float v1903 = 8.3485429360688279e-01F;
    float v1909 = -3.9375928506743518e-01F;
    float v1910 = 3.9375928506743518e-01F;
    float32x2_t v1912 = (float32x2_t){v4, v4};
    float32x2_t v1196 = v7[0];
    float32x2_t v1197 = vtrn1_f32(v1169, v1169);
    float32x2_t v1198 = vtrn2_f32(v1169, v1169);
    float32x2_t v1201 = v7[1];
    float32x2_t v1206 = v7[34];
    float32x2_t v1211 = v7[35];
    float32x2_t v1246 = v7[32];
    float32x2_t v1251 = v7[33];
    float32x2_t v1256 = v7[2];
    float32x2_t v1261 = v7[3];
    float32x2_t v1296 = v7[6];
    float32x2_t v1301 = v7[7];
    float32x2_t v1306 = v7[28];
    float32x2_t v1311 = v7[29];
    float32x2_t v1346 = v7[20];
    float32x2_t v1351 = v7[21];
    float32x2_t v1356 = v7[14];
    float32x2_t v1361 = v7[15];
    float32x2_t v1396 = v7[30];
    float32x2_t v1401 = v7[31];
    float32x2_t v1406 = v7[4];
    float32x2_t v1411 = v7[5];
    float32x2_t v1446 = v7[10];
    float32x2_t v1451 = v7[11];
    float32x2_t v1456 = v7[24];
    float32x2_t v1461 = v7[25];
    float32x2_t v1496 = v7[12];
    float32x2_t v1501 = v7[13];
    float32x2_t v1506 = v7[22];
    float32x2_t v1511 = v7[23];
    float32x2_t v1546 = v7[8];
    float32x2_t v1551 = v7[9];
    float32x2_t v1556 = v7[26];
    float32x2_t v1561 = v7[27];
    float32x2_t v1596 = v7[16];
    float32x2_t v1601 = v7[17];
    float32x2_t v1606 = v7[18];
    float32x2_t v1611 = v7[19];
    float32x2_t v1653 = v5[0];
    float32x2_t v1709 = (float32x2_t){v1708, v1708};
    float32x2_t v1713 = (float32x2_t){v1712, v1712};
    float32x2_t v1717 = (float32x2_t){v1716, v1716};
    float32x2_t v1721 = (float32x2_t){v1720, v1720};
    float32x2_t v1725 = (float32x2_t){v1724, v1724};
    float32x2_t v1729 = (float32x2_t){v1728, v1728};
    float32x2_t v1733 = (float32x2_t){v1732, v1732};
    float32x2_t v1737 = (float32x2_t){v1736, v1736};
    float32x2_t v1741 = (float32x2_t){v1740, v1740};
    float32x2_t v1745 = (float32x2_t){v1744, v1744};
    float32x2_t v1749 = (float32x2_t){v1748, v1748};
    float32x2_t v1753 = (float32x2_t){v1752, v1752};
    float32x2_t v1757 = (float32x2_t){v1756, v1756};
    float32x2_t v1761 = (float32x2_t){v1760, v1760};
    float32x2_t v1765 = (float32x2_t){v1764, v1764};
    float32x2_t v1769 = (float32x2_t){v1768, v1768};
    float32x2_t v1773 = (float32x2_t){v1772, v1772};
    float32x2_t v1777 = (float32x2_t){v1776, v1776};
    float32x2_t v1781 = (float32x2_t){v1780, v1780};
    float32x2_t v1785 = (float32x2_t){v1783, v1784};
    float32x2_t v1792 = (float32x2_t){v1790, v1791};
    float32x2_t v1799 = (float32x2_t){v1797, v1798};
    float32x2_t v1806 = (float32x2_t){v1804, v1805};
    float32x2_t v1813 = (float32x2_t){v1811, v1812};
    float32x2_t v1820 = (float32x2_t){v1818, v1819};
    float32x2_t v1827 = (float32x2_t){v1825, v1826};
    float32x2_t v1834 = (float32x2_t){v1832, v1833};
    float32x2_t v1841 = (float32x2_t){v1839, v1840};
    float32x2_t v1848 = (float32x2_t){v1846, v1847};
    float32x2_t v1855 = (float32x2_t){v1853, v1854};
    float32x2_t v1862 = (float32x2_t){v1860, v1861};
    float32x2_t v1869 = (float32x2_t){v1867, v1868};
    float32x2_t v1876 = (float32x2_t){v1874, v1875};
    float32x2_t v1883 = (float32x2_t){v1881, v1882};
    float32x2_t v1890 = (float32x2_t){v1888, v1889};
    float32x2_t v1897 = (float32x2_t){v1895, v1896};
    float32x2_t v1904 = (float32x2_t){v1902, v1903};
    float32x2_t v1911 = (float32x2_t){v1909, v1910};
    float32x2_t v1184 = v5[istride * 18];
    float32x2_t v1202 = vmul_f32(v1197, v1196);
    float32x2_t v1219 = v5[istride * 2];
    float32x2_t v1234 = v5[istride * 17];
    float32x2_t v1269 = v5[istride * 4];
    float32x2_t v1284 = v5[istride * 15];
    float32x2_t v1319 = v5[istride * 8];
    float32x2_t v1334 = v5[istride * 11];
    float32x2_t v1369 = v5[istride * 16];
    float32x2_t v1384 = v5[istride * 3];
    float32x2_t v1419 = v5[istride * 13];
    float32x2_t v1434 = v5[istride * 6];
    float32x2_t v1469 = v5[istride * 7];
    float32x2_t v1484 = v5[istride * 12];
    float32x2_t v1519 = v5[istride * 14];
    float32x2_t v1534 = v5[istride * 5];
    float32x2_t v1569 = v5[istride * 9];
    float32x2_t v1584 = v5[istride * 10];
    float32x2_t v1787 = vmul_f32(v1912, v1785);
    float32x2_t v1794 = vmul_f32(v1912, v1792);
    float32x2_t v1801 = vmul_f32(v1912, v1799);
    float32x2_t v1808 = vmul_f32(v1912, v1806);
    float32x2_t v1815 = vmul_f32(v1912, v1813);
    float32x2_t v1822 = vmul_f32(v1912, v1820);
    float32x2_t v1829 = vmul_f32(v1912, v1827);
    float32x2_t v1836 = vmul_f32(v1912, v1834);
    float32x2_t v1843 = vmul_f32(v1912, v1841);
    float32x2_t v1850 = vmul_f32(v1912, v1848);
    float32x2_t v1857 = vmul_f32(v1912, v1855);
    float32x2_t v1864 = vmul_f32(v1912, v1862);
    float32x2_t v1871 = vmul_f32(v1912, v1869);
    float32x2_t v1878 = vmul_f32(v1912, v1876);
    float32x2_t v1885 = vmul_f32(v1912, v1883);
    float32x2_t v1892 = vmul_f32(v1912, v1890);
    float32x2_t v1899 = vmul_f32(v1912, v1897);
    float32x2_t v1906 = vmul_f32(v1912, v1904);
    float32x2_t v1913 = vmul_f32(v1912, v1911);
    float32x2_t v1204 = vfma_f32(v1202, v1198, v1201);
    float32x2_t v1207 = vtrn1_f32(v1184, v1184);
    float32x2_t v1208 = vtrn2_f32(v1184, v1184);
    float32x2_t v1247 = vtrn1_f32(v1234, v1234);
    float32x2_t v1248 = vtrn2_f32(v1234, v1234);
    float32x2_t v1257 = vtrn1_f32(v1219, v1219);
    float32x2_t v1258 = vtrn2_f32(v1219, v1219);
    float32x2_t v1297 = vtrn1_f32(v1269, v1269);
    float32x2_t v1298 = vtrn2_f32(v1269, v1269);
    float32x2_t v1307 = vtrn1_f32(v1284, v1284);
    float32x2_t v1308 = vtrn2_f32(v1284, v1284);
    float32x2_t v1347 = vtrn1_f32(v1334, v1334);
    float32x2_t v1348 = vtrn2_f32(v1334, v1334);
    float32x2_t v1357 = vtrn1_f32(v1319, v1319);
    float32x2_t v1358 = vtrn2_f32(v1319, v1319);
    float32x2_t v1397 = vtrn1_f32(v1369, v1369);
    float32x2_t v1398 = vtrn2_f32(v1369, v1369);
    float32x2_t v1407 = vtrn1_f32(v1384, v1384);
    float32x2_t v1408 = vtrn2_f32(v1384, v1384);
    float32x2_t v1447 = vtrn1_f32(v1434, v1434);
    float32x2_t v1448 = vtrn2_f32(v1434, v1434);
    float32x2_t v1457 = vtrn1_f32(v1419, v1419);
    float32x2_t v1458 = vtrn2_f32(v1419, v1419);
    float32x2_t v1497 = vtrn1_f32(v1469, v1469);
    float32x2_t v1498 = vtrn2_f32(v1469, v1469);
    float32x2_t v1507 = vtrn1_f32(v1484, v1484);
    float32x2_t v1508 = vtrn2_f32(v1484, v1484);
    float32x2_t v1547 = vtrn1_f32(v1534, v1534);
    float32x2_t v1548 = vtrn2_f32(v1534, v1534);
    float32x2_t v1557 = vtrn1_f32(v1519, v1519);
    float32x2_t v1558 = vtrn2_f32(v1519, v1519);
    float32x2_t v1597 = vtrn1_f32(v1569, v1569);
    float32x2_t v1598 = vtrn2_f32(v1569, v1569);
    float32x2_t v1607 = vtrn1_f32(v1584, v1584);
    float32x2_t v1608 = vtrn2_f32(v1584, v1584);
    float32x2_t v1212 = vmul_f32(v1207, v1206);
    float32x2_t v1252 = vmul_f32(v1247, v1246);
    float32x2_t v1262 = vmul_f32(v1257, v1256);
    float32x2_t v1302 = vmul_f32(v1297, v1296);
    float32x2_t v1312 = vmul_f32(v1307, v1306);
    float32x2_t v1352 = vmul_f32(v1347, v1346);
    float32x2_t v1362 = vmul_f32(v1357, v1356);
    float32x2_t v1402 = vmul_f32(v1397, v1396);
    float32x2_t v1412 = vmul_f32(v1407, v1406);
    float32x2_t v1452 = vmul_f32(v1447, v1446);
    float32x2_t v1462 = vmul_f32(v1457, v1456);
    float32x2_t v1502 = vmul_f32(v1497, v1496);
    float32x2_t v1512 = vmul_f32(v1507, v1506);
    float32x2_t v1552 = vmul_f32(v1547, v1546);
    float32x2_t v1562 = vmul_f32(v1557, v1556);
    float32x2_t v1602 = vmul_f32(v1597, v1596);
    float32x2_t v1612 = vmul_f32(v1607, v1606);
    float32x2_t v1214 = vfma_f32(v1212, v1208, v1211);
    float32x2_t v1254 = vfma_f32(v1252, v1248, v1251);
    float32x2_t v1264 = vfma_f32(v1262, v1258, v1261);
    float32x2_t v1304 = vfma_f32(v1302, v1298, v1301);
    float32x2_t v1314 = vfma_f32(v1312, v1308, v1311);
    float32x2_t v1354 = vfma_f32(v1352, v1348, v1351);
    float32x2_t v1364 = vfma_f32(v1362, v1358, v1361);
    float32x2_t v1404 = vfma_f32(v1402, v1398, v1401);
    float32x2_t v1414 = vfma_f32(v1412, v1408, v1411);
    float32x2_t v1454 = vfma_f32(v1452, v1448, v1451);
    float32x2_t v1464 = vfma_f32(v1462, v1458, v1461);
    float32x2_t v1504 = vfma_f32(v1502, v1498, v1501);
    float32x2_t v1514 = vfma_f32(v1512, v1508, v1511);
    float32x2_t v1554 = vfma_f32(v1552, v1548, v1551);
    float32x2_t v1564 = vfma_f32(v1562, v1558, v1561);
    float32x2_t v1604 = vfma_f32(v1602, v1598, v1601);
    float32x2_t v1614 = vfma_f32(v1612, v1608, v1611);
    float32x2_t v1615 = vadd_f32(v1204, v1214);
    float32x2_t v1616 = vsub_f32(v1204, v1214);
    float32x2_t v1617 = vadd_f32(v1264, v1254);
    float32x2_t v1618 = vsub_f32(v1254, v1264);
    float32x2_t v1619 = vadd_f32(v1304, v1314);
    float32x2_t v1620 = vsub_f32(v1304, v1314);
    float32x2_t v1621 = vadd_f32(v1364, v1354);
    float32x2_t v1622 = vsub_f32(v1354, v1364);
    float32x2_t v1623 = vadd_f32(v1404, v1414);
    float32x2_t v1624 = vsub_f32(v1404, v1414);
    float32x2_t v1625 = vadd_f32(v1464, v1454);
    float32x2_t v1626 = vsub_f32(v1454, v1464);
    float32x2_t v1627 = vadd_f32(v1504, v1514);
    float32x2_t v1628 = vsub_f32(v1504, v1514);
    float32x2_t v1629 = vadd_f32(v1564, v1554);
    float32x2_t v1630 = vsub_f32(v1554, v1564);
    float32x2_t v1631 = vadd_f32(v1604, v1614);
    float32x2_t v1632 = vsub_f32(v1604, v1614);
    float32x2_t v1633 = vsub_f32(v1615, v1627);
    float32x2_t v1634 = vsub_f32(v1617, v1629);
    float32x2_t v1635 = vsub_f32(v1619, v1631);
    float32x2_t v1636 = vsub_f32(v1621, v1627);
    float32x2_t v1637 = vsub_f32(v1623, v1629);
    float32x2_t v1638 = vsub_f32(v1625, v1631);
    float32x2_t v1639 = vadd_f32(v1615, v1621);
    float32x2_t v1641 = vadd_f32(v1617, v1623);
    float32x2_t v1643 = vadd_f32(v1619, v1625);
    float32x2_t v1671 = vsub_f32(v1616, v1628);
    float32x2_t v1672 = vsub_f32(v1618, v1630);
    float32x2_t v1673 = vsub_f32(v1620, v1632);
    float32x2_t v1674 = vsub_f32(v1622, v1628);
    float32x2_t v1675 = vsub_f32(v1624, v1630);
    float32x2_t v1676 = vsub_f32(v1626, v1632);
    float32x2_t v1677 = vadd_f32(v1616, v1622);
    float32x2_t v1679 = vadd_f32(v1618, v1624);
    float32x2_t v1681 = vadd_f32(v1620, v1626);
    float32x2_t v1640 = vadd_f32(v1639, v1627);
    float32x2_t v1642 = vadd_f32(v1641, v1629);
    float32x2_t v1644 = vadd_f32(v1643, v1631);
    float32x2_t v1645 = vadd_f32(v1633, v1635);
    float32x2_t v1646 = vadd_f32(v1636, v1638);
    float32x2_t v1661 = vsub_f32(v1633, v1636);
    float32x2_t v1662 = vsub_f32(v1635, v1638);
    float32x2_t v1678 = vadd_f32(v1677, v1628);
    float32x2_t v1680 = vadd_f32(v1679, v1630);
    float32x2_t v1682 = vadd_f32(v1681, v1632);
    float32x2_t v1683 = vadd_f32(v1671, v1673);
    float32x2_t v1684 = vadd_f32(v1674, v1676);
    float32x2_t v1693 = vsub_f32(v1671, v1674);
    float32x2_t v1694 = vsub_f32(v1673, v1676);
    float32x2_t v1738 = vmul_f32(v1636, v1737);
    float32x2_t v1750 = vmul_f32(v1638, v1749);
    float32x2_t v1758 = vmul_f32(v1635, v1757);
    float32x2_t v1837 = vrev64_f32(v1674);
    float32x2_t v1851 = vrev64_f32(v1671);
    float32x2_t v1858 = vrev64_f32(v1676);
    float32x2_t v1872 = vrev64_f32(v1673);
    float32x2_t v1647 = vadd_f32(v1640, v1642);
    float32x2_t v1655 = vadd_f32(v1646, v1637);
    float32x2_t v1656 = vadd_f32(v1645, v1634);
    float32x2_t v1658 = vsub_f32(v1646, v1637);
    float32x2_t v1659 = vsub_f32(v1645, v1634);
    float32x2_t v1663 = vsub_f32(v1633, v1662);
    float32x2_t v1665 = vadd_f32(v1661, v1638);
    float32x2_t v1668 = vsub_f32(v1640, v1644);
    float32x2_t v1669 = vsub_f32(v1642, v1644);
    float32x2_t v1685 = vadd_f32(v1678, v1680);
    float32x2_t v1687 = vadd_f32(v1684, v1675);
    float32x2_t v1688 = vadd_f32(v1683, v1672);
    float32x2_t v1690 = vsub_f32(v1684, v1675);
    float32x2_t v1691 = vsub_f32(v1683, v1672);
    float32x2_t v1695 = vsub_f32(v1671, v1694);
    float32x2_t v1697 = vadd_f32(v1693, v1676);
    float32x2_t v1700 = vsub_f32(v1678, v1682);
    float32x2_t v1701 = vsub_f32(v1680, v1682);
    float32x2_t v1742 = vmul_f32(v1661, v1741);
    float32x2_t v1754 = vmul_f32(v1662, v1753);
    float32x2_t v1838 = vmul_f32(v1837, v1836);
    float32x2_t v1844 = vrev64_f32(v1693);
    float32x2_t v1859 = vmul_f32(v1858, v1857);
    float32x2_t v1865 = vrev64_f32(v1694);
    float32x2_t v1873 = vmul_f32(v1872, v1871);
    float32x2_t v1648 = vadd_f32(v1647, v1644);
    float32x2_t v1657 = vsub_f32(v1656, v1655);
    float32x2_t v1660 = vsub_f32(v1659, v1658);
    float32x2_t v1664 = vsub_f32(v1663, v1637);
    float32x2_t v1666 = vsub_f32(v1665, v1634);
    float32x2_t v1670 = vadd_f32(v1668, v1669);
    float32x2_t v1686 = vadd_f32(v1685, v1682);
    float32x2_t v1689 = vsub_f32(v1688, v1687);
    float32x2_t v1692 = vsub_f32(v1691, v1690);
    float32x2_t v1696 = vsub_f32(v1695, v1675);
    float32x2_t v1698 = vsub_f32(v1697, v1672);
    float32x2_t v1702 = vadd_f32(v1700, v1701);
    float32x2_t v1714 = vmul_f32(v1655, v1713);
    float32x2_t v1718 = vmul_f32(v1656, v1717);
    float32x2_t v1726 = vmul_f32(v1658, v1725);
    float32x2_t v1730 = vmul_f32(v1659, v1729);
    float32x2_t v1774 = vmul_f32(v1668, v1773);
    float32x2_t v1778 = vmul_f32(v1669, v1777);
    float32x2_t v1795 = vrev64_f32(v1687);
    float32x2_t v1802 = vrev64_f32(v1688);
    float32x2_t v1816 = vrev64_f32(v1690);
    float32x2_t v1823 = vrev64_f32(v1691);
    float32x2_t v1845 = vmul_f32(v1844, v1843);
    float32x2_t v1866 = vmul_f32(v1865, v1864);
    float32x2_t v1900 = vrev64_f32(v1700);
    float32x2_t v1907 = vrev64_f32(v1701);
    float32x2_t v1654 = vadd_f32(v1653, v1648);
    float32x2_t v1667 = vsub_f32(v1664, v1666);
    float32x2_t v1699 = vsub_f32(v1696, v1698);
    float32x2_t v1710 = vmul_f32(v1648, v1709);
    float32x2_t v1722 = vmul_f32(v1657, v1721);
    float32x2_t v1734 = vmul_f32(v1660, v1733);
    float32x2_t v1762 = vmul_f32(v1664, v1761);
    float32x2_t v1766 = vmul_f32(v1666, v1765);
    float32x2_t v1782 = vmul_f32(v1670, v1781);
    float32x2_t v1788 = vrev64_f32(v1686);
    float32x2_t v1796 = vmul_f32(v1795, v1794);
    float32x2_t v1803 = vmul_f32(v1802, v1801);
    float32x2_t v1809 = vrev64_f32(v1689);
    float32x2_t v1817 = vmul_f32(v1816, v1815);
    float32x2_t v1824 = vmul_f32(v1823, v1822);
    float32x2_t v1830 = vrev64_f32(v1692);
    float32x2_t v1879 = vrev64_f32(v1696);
    float32x2_t v1886 = vrev64_f32(v1698);
    float32x2_t v1901 = vmul_f32(v1900, v1899);
    float32x2_t v1908 = vmul_f32(v1907, v1906);
    float32x2_t v1914 = vrev64_f32(v1702);
    float32x2_t v1916 = vadd_f32(v1714, v1718);
    float32x2_t v1917 = vadd_f32(v1726, v1730);
    float32x2_t v1770 = vmul_f32(v1667, v1769);
    float32x2_t v1789 = vmul_f32(v1788, v1787);
    float32x2_t v1810 = vmul_f32(v1809, v1808);
    float32x2_t v1831 = vmul_f32(v1830, v1829);
    float32x2_t v1880 = vmul_f32(v1879, v1878);
    float32x2_t v1887 = vmul_f32(v1886, v1885);
    float32x2_t v1893 = vrev64_f32(v1699);
    float32x2_t v1915 = vmul_f32(v1914, v1913);
    float32x2_t v1919 = vadd_f32(v1916, v1917);
    float32x2_t v1920 = vadd_f32(v1714, v1722);
    float32x2_t v1921 = vadd_f32(v1726, v1734);
    float32x2_t v1938 = vsub_f32(v1916, v1917);
    float32x2_t v1940 = vsub_f32(v1774, v1782);
    float32x2_t v1941 = vsub_f32(v1778, v1782);
    float32x2_t v1942 = vadd_f32(v1710, v1654);
    float32x2_t v1947 = vadd_f32(v1796, v1803);
    float32x2_t v1948 = vadd_f32(v1817, v1824);
    v6[0] = v1654;
    float32x2_t v1894 = vmul_f32(v1893, v1892);
    float32x2_t v1918 = vadd_f32(v1766, v1770);
    float32x2_t v1922 = vadd_f32(v1762, v1770);
    float32x2_t v1923 = vsub_f32(v1738, v1919);
    float32x2_t v1924 = vadd_f32(v1920, v1921);
    float32x2_t v1930 = vsub_f32(v1920, v1921);
    float32x2_t v1935 = vadd_f32(v1919, v1758);
    float32x2_t v1943 = vadd_f32(v1942, v1940);
    float32x2_t v1944 = vsub_f32(v1942, v1940);
    float32x2_t v1946 = vadd_f32(v1942, v1941);
    float32x2_t v1950 = vadd_f32(v1947, v1948);
    float32x2_t v1951 = vadd_f32(v1796, v1810);
    float32x2_t v1952 = vadd_f32(v1817, v1831);
    float32x2_t v1969 = vsub_f32(v1947, v1948);
    float32x2_t v1971 = vsub_f32(v1901, v1915);
    float32x2_t v1972 = vsub_f32(v1908, v1915);
    float32x2_t v1925 = vsub_f32(v1750, v1922);
    float32x2_t v1926 = vadd_f32(v1742, v1918);
    float32x2_t v1928 = vadd_f32(v1924, v1754);
    float32x2_t v1931 = vadd_f32(v1930, v1918);
    float32x2_t v1932 = vadd_f32(v1923, v1924);
    float32x2_t v1939 = vadd_f32(v1938, v1922);
    float32x2_t v1945 = vsub_f32(v1944, v1941);
    float32x2_t v1949 = vadd_f32(v1887, v1894);
    float32x2_t v1953 = vadd_f32(v1880, v1894);
    float32x2_t v1954 = vsub_f32(v1838, v1950);
    float32x2_t v1955 = vadd_f32(v1951, v1952);
    float32x2_t v1961 = vsub_f32(v1951, v1952);
    float32x2_t v1966 = vadd_f32(v1950, v1873);
    float32x2_t v1973 = vadd_f32(v1789, v1971);
    float32x2_t v1974 = vsub_f32(v1789, v1971);
    float32x2_t v1976 = vadd_f32(v1789, v1972);
    float32x2_t v1927 = vadd_f32(v1926, v1923);
    float32x2_t v1929 = vadd_f32(v1928, v1925);
    float32x2_t v1933 = vfma_f32(v1932, v1633, v1745);
    float32x2_t v1936 = vadd_f32(v1935, v1925);
    float32x2_t v1956 = vsub_f32(v1859, v1953);
    float32x2_t v1957 = vadd_f32(v1845, v1949);
    float32x2_t v1959 = vadd_f32(v1955, v1866);
    float32x2_t v1962 = vadd_f32(v1961, v1949);
    float32x2_t v1963 = vadd_f32(v1954, v1955);
    float32x2_t v1970 = vadd_f32(v1969, v1953);
    float32x2_t v1975 = vsub_f32(v1974, v1972);
    float32x2_t v1981 = vsub_f32(v1939, v1931);
    float32x2_t v1985 = vsub_f32(v1946, v1939);
    float32x2_t v1988 = vadd_f32(v1931, v1946);
    float32x2_t v1934 = vadd_f32(v1933, v1922);
    float32x2_t v1937 = vadd_f32(v1936, v1918);
    float32x2_t v1958 = vadd_f32(v1957, v1954);
    float32x2_t v1960 = vadd_f32(v1959, v1956);
    float32x2_t v1964 = vfma_f32(v1963, v1851, v1850);
    float32x2_t v1967 = vadd_f32(v1966, v1956);
    float32x2_t v1982 = vadd_f32(v1981, v1946);
    float32x2_t v1986 = vadd_f32(v1927, v1943);
    float32x2_t v1987 = vadd_f32(v1929, v1945);
    float32x2_t v1993 = vsub_f32(v1970, v1962);
    float32x2_t v1997 = vsub_f32(v1970, v1976);
    float32x2_t v2000 = vadd_f32(v1962, v1976);
    float32x2_t v1965 = vadd_f32(v1964, v1953);
    float32x2_t v1968 = vadd_f32(v1967, v1949);
    float32x2_t v1977 = vsub_f32(v1934, v1927);
    float32x2_t v1979 = vsub_f32(v1937, v1929);
    float32x2_t v1983 = vsub_f32(v1943, v1934);
    float32x2_t v1984 = vsub_f32(v1945, v1937);
    float32x2_t v1994 = vadd_f32(v1993, v1976);
    float32x2_t v1998 = vadd_f32(v1958, v1973);
    float32x2_t v1999 = vadd_f32(v1960, v1975);
    float32x2_t v2018 = vsub_f32(v1988, v2000);
    float32x2_t v2024 = vadd_f32(v1988, v2000);
    float32x2_t v2030 = vadd_f32(v1985, v1997);
    float32x2_t v2036 = vsub_f32(v1985, v1997);
    float32x2_t v1978 = vadd_f32(v1977, v1943);
    float32x2_t v1980 = vadd_f32(v1979, v1945);
    float32x2_t v1989 = vsub_f32(v1965, v1958);
    float32x2_t v1991 = vsub_f32(v1968, v1960);
    float32x2_t v1995 = vsub_f32(v1973, v1965);
    float32x2_t v1996 = vsub_f32(v1975, v1968);
    v6[ostride * 2] = v2018;
    v6[ostride * 17] = v2024;
    v6[ostride * 3] = v2030;
    v6[ostride * 16] = v2036;
    float32x2_t v2042 = vadd_f32(v1987, v1999);
    float32x2_t v2048 = vsub_f32(v1987, v1999);
    float32x2_t v2054 = vadd_f32(v1982, v1994);
    float32x2_t v2060 = vsub_f32(v1982, v1994);
    float32x2_t v2090 = vsub_f32(v1986, v1998);
    float32x2_t v2096 = vadd_f32(v1986, v1998);
    float32x2_t v1990 = vadd_f32(v1989, v1973);
    float32x2_t v1992 = vadd_f32(v1991, v1975);
    v6[ostride * 4] = v2042;
    v6[ostride * 15] = v2048;
    v6[ostride * 5] = v2054;
    v6[ostride * 14] = v2060;
    float32x2_t v2066 = vadd_f32(v1984, v1996);
    float32x2_t v2072 = vsub_f32(v1984, v1996);
    float32x2_t v2078 = vadd_f32(v1983, v1995);
    float32x2_t v2084 = vsub_f32(v1983, v1995);
    v6[ostride * 8] = v2090;
    v6[ostride * 11] = v2096;
    float32x2_t v2006 = vadd_f32(v1978, v1990);
    float32x2_t v2012 = vsub_f32(v1978, v1990);
    v6[ostride * 6] = v2066;
    v6[ostride * 13] = v2072;
    v6[ostride * 7] = v2078;
    v6[ostride * 12] = v2084;
    float32x2_t v2102 = vadd_f32(v1980, v1992);
    float32x2_t v2108 = vsub_f32(v1980, v1992);
    v6[ostride] = v2006;
    v6[ostride * 18] = v2012;
    v6[ostride * 9] = v2102;
    v6[ostride * 10] = v2108;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu19(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v384 = -1.0555555555555556e+00F;
    float v389 = 1.7752228513927079e-01F;
    float v394 = -1.2820077502191529e-01F;
    float v399 = 4.9321510117355499e-02F;
    float v404 = 5.7611011491005903e-01F;
    float v409 = -7.4996449655536279e-01F;
    float v414 = -1.7385438164530381e-01F;
    float v419 = -2.1729997561977314e+00F;
    float v424 = -1.7021211726914738e+00F;
    float v429 = 4.7087858350625778e-01F;
    float v434 = -2.0239400846888440e+00F;
    float v439 = 1.0551641201664090e-01F;
    float v444 = 2.1294564967054850e+00F;
    float v449 = -7.5087543897371167e-01F;
    float v454 = 1.4812817695157160e-01F;
    float v459 = 8.9900361592528333e-01F;
    float v464 = -6.2148246772602778e-01F;
    float v469 = -7.9869352098712687e-01F;
    float v474 = -4.7339199623771833e-01F;
    float v479 = 2.4216105241892630e-01F;
    float v486 = 5.9368607967505101e-02F;
    float v493 = -1.2578688255176201e-02F;
    float v500 = 4.6789919712328903e-02F;
    float v507 = 9.3750121913782358e-01F;
    float v514 = 5.0111537043352902e-02F;
    float v521 = 9.8761275618117661e-01F;
    float v528 = 1.1745786501205959e+00F;
    float v535 = -1.1114482296234993e+00F;
    float v542 = -2.2860268797440955e+00F;
    float v549 = -2.6420523257930939e-01F;
    float v556 = -2.1981792779352136e+00F;
    float v563 = -1.9339740453559042e+00F;
    float v570 = 7.4825847091254893e-01F;
    float v577 = 4.7820835642768872e-01F;
    float v584 = -2.7005011448486022e-01F;
    float v591 = 3.4642356159542270e-01F;
    float v598 = 8.3485429360688279e-01F;
    float v605 = 3.9375928506743518e-01F;
    const float32x2_t *v853 = &v5[v0];
    float32x2_t *v1074 = &v6[v2];
    int64_t v30 = v0 * 18;
    int64_t v49 = v0 * 2;
    int64_t v60 = v0 * 17;
    int64_t v79 = v0 * 4;
    int64_t v90 = v0 * 15;
    int64_t v109 = v0 * 8;
    int64_t v120 = v0 * 11;
    int64_t v139 = v0 * 16;
    int64_t v150 = v0 * 3;
    int64_t v169 = v0 * 13;
    int64_t v180 = v0 * 6;
    int64_t v199 = v0 * 7;
    int64_t v210 = v0 * 12;
    int64_t v229 = v0 * 14;
    int64_t v240 = v0 * 5;
    int64_t v259 = v0 * 9;
    int64_t v270 = v0 * 10;
    float v482 = v4 * v479;
    float v489 = v4 * v486;
    float v496 = v4 * v493;
    float v503 = v4 * v500;
    float v510 = v4 * v507;
    float v517 = v4 * v514;
    float v524 = v4 * v521;
    float v531 = v4 * v528;
    float v538 = v4 * v535;
    float v545 = v4 * v542;
    float v552 = v4 * v549;
    float v559 = v4 * v556;
    float v566 = v4 * v563;
    float v573 = v4 * v570;
    float v580 = v4 * v577;
    float v587 = v4 * v584;
    float v594 = v4 * v591;
    float v601 = v4 * v598;
    float v608 = v4 * v605;
    int64_t v713 = v2 * 18;
    int64_t v721 = v2 * 2;
    int64_t v729 = v2 * 17;
    int64_t v737 = v2 * 3;
    int64_t v745 = v2 * 16;
    int64_t v753 = v2 * 4;
    int64_t v761 = v2 * 15;
    int64_t v769 = v2 * 5;
    int64_t v777 = v2 * 14;
    int64_t v785 = v2 * 6;
    int64_t v793 = v2 * 13;
    int64_t v801 = v2 * 7;
    int64_t v809 = v2 * 12;
    int64_t v817 = v2 * 8;
    int64_t v825 = v2 * 11;
    int64_t v833 = v2 * 9;
    int64_t v841 = v2 * 10;
    const float32x2_t *v1016 = &v5[0];
    svfloat32_t v1020 = svdup_n_f32(v384);
    svfloat32_t v1021 = svdup_n_f32(v389);
    svfloat32_t v1022 = svdup_n_f32(v394);
    svfloat32_t v1023 = svdup_n_f32(v399);
    svfloat32_t v1024 = svdup_n_f32(v404);
    svfloat32_t v1025 = svdup_n_f32(v409);
    svfloat32_t v1026 = svdup_n_f32(v414);
    svfloat32_t v1027 = svdup_n_f32(v419);
    svfloat32_t v1028 = svdup_n_f32(v424);
    svfloat32_t v1029 = svdup_n_f32(v429);
    svfloat32_t v1030 = svdup_n_f32(v434);
    svfloat32_t v1031 = svdup_n_f32(v439);
    svfloat32_t v1032 = svdup_n_f32(v444);
    svfloat32_t v1033 = svdup_n_f32(v449);
    svfloat32_t v1034 = svdup_n_f32(v454);
    svfloat32_t v1035 = svdup_n_f32(v459);
    svfloat32_t v1036 = svdup_n_f32(v464);
    svfloat32_t v1037 = svdup_n_f32(v469);
    svfloat32_t v1038 = svdup_n_f32(v474);
    float32x2_t *v1065 = &v6[0];
    svfloat32_t v1231 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v853)[0]));
    svfloat32_t v42 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v46 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[17]));
    svfloat32_t v72 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[16]));
    svfloat32_t v76 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v102 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v106 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[14]));
    svfloat32_t v132 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v136 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v162 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[15]));
    svfloat32_t v166 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v192 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v196 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v222 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v226 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v252 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v256 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v282 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v286 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    const float32x2_t *v862 = &v5[v30];
    const float32x2_t *v871 = &v5[v49];
    const float32x2_t *v880 = &v5[v60];
    const float32x2_t *v889 = &v5[v79];
    const float32x2_t *v898 = &v5[v90];
    const float32x2_t *v907 = &v5[v109];
    const float32x2_t *v916 = &v5[v120];
    const float32x2_t *v925 = &v5[v139];
    const float32x2_t *v934 = &v5[v150];
    const float32x2_t *v943 = &v5[v169];
    const float32x2_t *v952 = &v5[v180];
    const float32x2_t *v961 = &v5[v199];
    const float32x2_t *v970 = &v5[v210];
    const float32x2_t *v979 = &v5[v229];
    const float32x2_t *v988 = &v5[v240];
    const float32x2_t *v997 = &v5[v259];
    const float32x2_t *v1006 = &v5[v270];
    svfloat32_t v1039 = svdup_n_f32(v482);
    svfloat32_t v1040 = svdup_n_f32(v489);
    svfloat32_t v1041 = svdup_n_f32(v496);
    svfloat32_t v1042 = svdup_n_f32(v503);
    svfloat32_t v1043 = svdup_n_f32(v510);
    svfloat32_t v1044 = svdup_n_f32(v517);
    svfloat32_t v1045 = svdup_n_f32(v524);
    svfloat32_t v1046 = svdup_n_f32(v531);
    svfloat32_t v1047 = svdup_n_f32(v538);
    svfloat32_t v1048 = svdup_n_f32(v545);
    svfloat32_t v1049 = svdup_n_f32(v552);
    svfloat32_t v1050 = svdup_n_f32(v559);
    svfloat32_t v1051 = svdup_n_f32(v566);
    svfloat32_t v1052 = svdup_n_f32(v573);
    svfloat32_t v1053 = svdup_n_f32(v580);
    svfloat32_t v1054 = svdup_n_f32(v587);
    svfloat32_t v1055 = svdup_n_f32(v594);
    svfloat32_t v1056 = svdup_n_f32(v601);
    svfloat32_t v1057 = svdup_n_f32(v608);
    float32x2_t *v1083 = &v6[v713];
    float32x2_t *v1092 = &v6[v721];
    float32x2_t *v1101 = &v6[v729];
    float32x2_t *v1110 = &v6[v737];
    float32x2_t *v1119 = &v6[v745];
    float32x2_t *v1128 = &v6[v753];
    float32x2_t *v1137 = &v6[v761];
    float32x2_t *v1146 = &v6[v769];
    float32x2_t *v1155 = &v6[v777];
    float32x2_t *v1164 = &v6[v785];
    float32x2_t *v1173 = &v6[v793];
    float32x2_t *v1182 = &v6[v801];
    float32x2_t *v1191 = &v6[v809];
    float32x2_t *v1200 = &v6[v817];
    float32x2_t *v1209 = &v6[v825];
    float32x2_t *v1218 = &v6[v833];
    float32x2_t *v1227 = &v6[v841];
    svfloat32_t v1267 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1016)[0]));
    svfloat32_t zero43 = svdup_n_f32(0);
    svfloat32_t v43 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero43, v1231, v42, 0),
                     v1231, v42, 90);
    svfloat32_t v1233 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v862)[0]));
    svfloat32_t v1235 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v871)[0]));
    svfloat32_t v1237 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v880)[0]));
    svfloat32_t v1239 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v889)[0]));
    svfloat32_t v1241 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v898)[0]));
    svfloat32_t v1243 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v907)[0]));
    svfloat32_t v1245 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v916)[0]));
    svfloat32_t v1247 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v925)[0]));
    svfloat32_t v1249 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v934)[0]));
    svfloat32_t v1251 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v943)[0]));
    svfloat32_t v1253 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v952)[0]));
    svfloat32_t v1255 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v961)[0]));
    svfloat32_t v1257 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v970)[0]));
    svfloat32_t v1259 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v979)[0]));
    svfloat32_t v1261 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v988)[0]));
    svfloat32_t v1263 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v997)[0]));
    svfloat32_t v1265 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1006)[0]));
    svfloat32_t zero47 = svdup_n_f32(0);
    svfloat32_t v47 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero47, v1233, v46, 0),
                     v1233, v46, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v1237, v72, 0),
                     v1237, v72, 90);
    svfloat32_t zero77 = svdup_n_f32(0);
    svfloat32_t v77 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero77, v1235, v76, 0),
                     v1235, v76, 90);
    svfloat32_t zero103 = svdup_n_f32(0);
    svfloat32_t v103 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero103, v1239, v102, 0), v1239,
        v102, 90);
    svfloat32_t zero107 = svdup_n_f32(0);
    svfloat32_t v107 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero107, v1241, v106, 0), v1241,
        v106, 90);
    svfloat32_t zero133 = svdup_n_f32(0);
    svfloat32_t v133 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero133, v1245, v132, 0), v1245,
        v132, 90);
    svfloat32_t zero137 = svdup_n_f32(0);
    svfloat32_t v137 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero137, v1243, v136, 0), v1243,
        v136, 90);
    svfloat32_t zero163 = svdup_n_f32(0);
    svfloat32_t v163 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero163, v1247, v162, 0), v1247,
        v162, 90);
    svfloat32_t zero167 = svdup_n_f32(0);
    svfloat32_t v167 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero167, v1249, v166, 0), v1249,
        v166, 90);
    svfloat32_t zero193 = svdup_n_f32(0);
    svfloat32_t v193 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero193, v1253, v192, 0), v1253,
        v192, 90);
    svfloat32_t zero197 = svdup_n_f32(0);
    svfloat32_t v197 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero197, v1251, v196, 0), v1251,
        v196, 90);
    svfloat32_t zero223 = svdup_n_f32(0);
    svfloat32_t v223 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero223, v1255, v222, 0), v1255,
        v222, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero227, v1257, v226, 0), v1257,
        v226, 90);
    svfloat32_t zero253 = svdup_n_f32(0);
    svfloat32_t v253 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero253, v1261, v252, 0), v1261,
        v252, 90);
    svfloat32_t zero257 = svdup_n_f32(0);
    svfloat32_t v257 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero257, v1259, v256, 0), v1259,
        v256, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero283, v1263, v282, 0), v1263,
        v282, 90);
    svfloat32_t zero287 = svdup_n_f32(0);
    svfloat32_t v287 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero287, v1265, v286, 0), v1265,
        v286, 90);
    svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v289 = svsub_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v290 = svadd_f32_x(svptrue_b32(), v77, v73);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v292 = svadd_f32_x(svptrue_b32(), v103, v107);
    svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v103, v107);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v137, v133);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v133, v137);
    svfloat32_t v296 = svadd_f32_x(svptrue_b32(), v163, v167);
    svfloat32_t v297 = svsub_f32_x(svptrue_b32(), v163, v167);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v197, v193);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v193, v197);
    svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v223, v227);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v223, v227);
    svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v257, v253);
    svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v253, v257);
    svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v283, v287);
    svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v283, v287);
    svfloat32_t v306 = svsub_f32_x(svptrue_b32(), v288, v300);
    svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v290, v302);
    svfloat32_t v308 = svsub_f32_x(svptrue_b32(), v292, v304);
    svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v294, v300);
    svfloat32_t v310 = svsub_f32_x(svptrue_b32(), v296, v302);
    svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v298, v304);
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v288, v294);
    svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v290, v296);
    svfloat32_t v316 = svadd_f32_x(svptrue_b32(), v292, v298);
    svfloat32_t v346 = svsub_f32_x(svptrue_b32(), v289, v301);
    svfloat32_t v347 = svsub_f32_x(svptrue_b32(), v291, v303);
    svfloat32_t v348 = svsub_f32_x(svptrue_b32(), v293, v305);
    svfloat32_t v349 = svsub_f32_x(svptrue_b32(), v295, v301);
    svfloat32_t v350 = svsub_f32_x(svptrue_b32(), v297, v303);
    svfloat32_t v351 = svsub_f32_x(svptrue_b32(), v299, v305);
    svfloat32_t v352 = svadd_f32_x(svptrue_b32(), v289, v295);
    svfloat32_t v354 = svadd_f32_x(svptrue_b32(), v291, v297);
    svfloat32_t v356 = svadd_f32_x(svptrue_b32(), v293, v299);
    svfloat32_t v313 = svadd_f32_x(svptrue_b32(), v312, v300);
    svfloat32_t v315 = svadd_f32_x(svptrue_b32(), v314, v302);
    svfloat32_t v317 = svadd_f32_x(svptrue_b32(), v316, v304);
    svfloat32_t v318 = svadd_f32_x(svptrue_b32(), v306, v308);
    svfloat32_t v319 = svadd_f32_x(svptrue_b32(), v309, v311);
    svfloat32_t v336 = svsub_f32_x(svptrue_b32(), v306, v309);
    svfloat32_t v337 = svsub_f32_x(svptrue_b32(), v308, v311);
    svfloat32_t v353 = svadd_f32_x(svptrue_b32(), v352, v301);
    svfloat32_t v355 = svadd_f32_x(svptrue_b32(), v354, v303);
    svfloat32_t v357 = svadd_f32_x(svptrue_b32(), v356, v305);
    svfloat32_t v358 = svadd_f32_x(svptrue_b32(), v346, v348);
    svfloat32_t v359 = svadd_f32_x(svptrue_b32(), v349, v351);
    svfloat32_t v368 = svsub_f32_x(svptrue_b32(), v346, v349);
    svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v348, v351);
    svfloat32_t zero533 = svdup_n_f32(0);
    svfloat32_t v533 = svcmla_f32_x(pred_full, zero533, v1046, v349, 90);
    svfloat32_t zero554 = svdup_n_f32(0);
    svfloat32_t v554 = svcmla_f32_x(pred_full, zero554, v1049, v351, 90);
    svfloat32_t v320 = svadd_f32_x(svptrue_b32(), v313, v315);
    svfloat32_t v330 = svadd_f32_x(svptrue_b32(), v319, v310);
    svfloat32_t v331 = svadd_f32_x(svptrue_b32(), v318, v307);
    svfloat32_t v333 = svsub_f32_x(svptrue_b32(), v319, v310);
    svfloat32_t v334 = svsub_f32_x(svptrue_b32(), v318, v307);
    svfloat32_t v338 = svsub_f32_x(svptrue_b32(), v306, v337);
    svfloat32_t v340 = svadd_f32_x(svptrue_b32(), v336, v311);
    svfloat32_t v343 = svsub_f32_x(svptrue_b32(), v313, v317);
    svfloat32_t v344 = svsub_f32_x(svptrue_b32(), v315, v317);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v353, v355);
    svfloat32_t v362 = svadd_f32_x(svptrue_b32(), v359, v350);
    svfloat32_t v363 = svadd_f32_x(svptrue_b32(), v358, v347);
    svfloat32_t v365 = svsub_f32_x(svptrue_b32(), v359, v350);
    svfloat32_t v366 = svsub_f32_x(svptrue_b32(), v358, v347);
    svfloat32_t v370 = svsub_f32_x(svptrue_b32(), v346, v369);
    svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v368, v351);
    svfloat32_t v375 = svsub_f32_x(svptrue_b32(), v353, v357);
    svfloat32_t v376 = svsub_f32_x(svptrue_b32(), v355, v357);
    svfloat32_t v321 = svadd_f32_x(svptrue_b32(), v320, v317);
    svfloat32_t v332 = svsub_f32_x(svptrue_b32(), v331, v330);
    svfloat32_t v335 = svsub_f32_x(svptrue_b32(), v334, v333);
    svfloat32_t v339 = svsub_f32_x(svptrue_b32(), v338, v310);
    svfloat32_t v341 = svsub_f32_x(svptrue_b32(), v340, v307);
    svfloat32_t v345 = svadd_f32_x(svptrue_b32(), v343, v344);
    svfloat32_t v361 = svadd_f32_x(svptrue_b32(), v360, v357);
    svfloat32_t v364 = svsub_f32_x(svptrue_b32(), v363, v362);
    svfloat32_t v367 = svsub_f32_x(svptrue_b32(), v366, v365);
    svfloat32_t v371 = svsub_f32_x(svptrue_b32(), v370, v350);
    svfloat32_t v373 = svsub_f32_x(svptrue_b32(), v372, v347);
    svfloat32_t v377 = svadd_f32_x(svptrue_b32(), v375, v376);
    svfloat32_t v397 = svmul_f32_x(svptrue_b32(), v331, v1022);
    svfloat32_t v412 = svmul_f32_x(svptrue_b32(), v334, v1025);
    svfloat32_t zero491 = svdup_n_f32(0);
    svfloat32_t v491 = svcmla_f32_x(pred_full, zero491, v1040, v362, 90);
    svfloat32_t zero512 = svdup_n_f32(0);
    svfloat32_t v512 = svcmla_f32_x(pred_full, zero512, v1043, v365, 90);
    svfloat32_t zero596 = svdup_n_f32(0);
    svfloat32_t v596 = svcmla_f32_x(pred_full, zero596, v1055, v375, 90);
    svfloat32_t zero603 = svdup_n_f32(0);
    svfloat32_t v603 = svcmla_f32_x(pred_full, zero603, v1056, v376, 90);
    svfloat32_t v329 = svadd_f32_x(svptrue_b32(), v1267, v321);
    svfloat32_t v342 = svsub_f32_x(svptrue_b32(), v339, v341);
    svfloat32_t v374 = svsub_f32_x(svptrue_b32(), v371, v373);
    svfloat32_t v402 = svmul_f32_x(svptrue_b32(), v332, v1023);
    svfloat32_t v417 = svmul_f32_x(svptrue_b32(), v335, v1026);
    svfloat32_t v477 = svmul_f32_x(svptrue_b32(), v345, v1038);
    svfloat32_t zero484 = svdup_n_f32(0);
    svfloat32_t v484 = svcmla_f32_x(pred_full, zero484, v1039, v361, 90);
    svfloat32_t zero610 = svdup_n_f32(0);
    svfloat32_t v610 = svcmla_f32_x(pred_full, zero610, v1057, v377, 90);
    svfloat32_t v611 = svmla_f32_x(pred_full, v397, v330, v1021);
    svfloat32_t v612 = svmla_f32_x(pred_full, v412, v333, v1024);
    svfloat32_t v642 = svcmla_f32_x(pred_full, v491, v1041, v363, 90);
    svfloat32_t v643 = svcmla_f32_x(pred_full, v512, v1044, v366, 90);
    svfloat32_t v462 = svmul_f32_x(svptrue_b32(), v342, v1035);
    svfloat32_t zero589 = svdup_n_f32(0);
    svfloat32_t v589 = svcmla_f32_x(pred_full, zero589, v1054, v374, 90);
    svfloat32_t v614 = svadd_f32_x(svptrue_b32(), v611, v612);
    svfloat32_t v615 = svmla_f32_x(pred_full, v402, v330, v1021);
    svfloat32_t v616 = svmla_f32_x(pred_full, v417, v333, v1024);
    svfloat32_t v633 = svsub_f32_x(svptrue_b32(), v611, v612);
    svfloat32_t v635 = svnmls_f32_x(pred_full, v477, v343, v1036);
    svfloat32_t v636 = svnmls_f32_x(pred_full, v477, v344, v1037);
    svfloat32_t v637 = svmla_f32_x(pred_full, v329, v321, v1020);
    svfloat32_t v645 = svadd_f32_x(svptrue_b32(), v642, v643);
    svfloat32_t v646 = svcmla_f32_x(pred_full, v491, v1042, v364, 90);
    svfloat32_t v647 = svcmla_f32_x(pred_full, v512, v1045, v367, 90);
    svfloat32_t v664 = svsub_f32_x(svptrue_b32(), v642, v643);
    svfloat32_t v666 = svsub_f32_x(svptrue_b32(), v596, v610);
    svfloat32_t v667 = svsub_f32_x(svptrue_b32(), v603, v610);
    svst1_f64(pred_full, (double *)(v1065), svreinterpret_f64_f32(v329));
    svfloat32_t v613 = svmla_f32_x(pred_full, v462, v341, v1034);
    svfloat32_t v617 = svmla_f32_x(pred_full, v462, v339, v1033);
    svfloat32_t v618 = svnmls_f32_x(pred_full, v614, v309, v1027);
    svfloat32_t v619 = svadd_f32_x(svptrue_b32(), v615, v616);
    svfloat32_t v625 = svsub_f32_x(svptrue_b32(), v615, v616);
    svfloat32_t v630 = svmla_f32_x(pred_full, v614, v308, v1032);
    svfloat32_t v638 = svadd_f32_x(svptrue_b32(), v637, v635);
    svfloat32_t v639 = svsub_f32_x(svptrue_b32(), v637, v635);
    svfloat32_t v641 = svadd_f32_x(svptrue_b32(), v637, v636);
    svfloat32_t v644 = svcmla_f32_x(pred_full, v589, v1053, v373, 90);
    svfloat32_t v648 = svcmla_f32_x(pred_full, v589, v1052, v371, 90);
    svfloat32_t v649 = svsub_f32_x(svptrue_b32(), v533, v645);
    svfloat32_t v650 = svadd_f32_x(svptrue_b32(), v646, v647);
    svfloat32_t v656 = svsub_f32_x(svptrue_b32(), v646, v647);
    svfloat32_t v661 = svcmla_f32_x(pred_full, v645, v1051, v348, 90);
    svfloat32_t v668 = svadd_f32_x(svptrue_b32(), v484, v666);
    svfloat32_t v669 = svsub_f32_x(svptrue_b32(), v484, v666);
    svfloat32_t v671 = svadd_f32_x(svptrue_b32(), v484, v667);
    svfloat32_t v620 = svnmls_f32_x(pred_full, v617, v311, v1030);
    svfloat32_t v621 = svmla_f32_x(pred_full, v613, v336, v1028);
    svfloat32_t v623 = svmla_f32_x(pred_full, v619, v337, v1031);
    svfloat32_t v626 = svadd_f32_x(svptrue_b32(), v625, v613);
    svfloat32_t v627 = svadd_f32_x(svptrue_b32(), v618, v619);
    svfloat32_t v634 = svadd_f32_x(svptrue_b32(), v633, v617);
    svfloat32_t v640 = svsub_f32_x(svptrue_b32(), v639, v636);
    svfloat32_t v651 = svsub_f32_x(svptrue_b32(), v554, v648);
    svfloat32_t v652 = svcmla_f32_x(pred_full, v644, v1047, v368, 90);
    svfloat32_t v654 = svcmla_f32_x(pred_full, v650, v1050, v369, 90);
    svfloat32_t v657 = svadd_f32_x(svptrue_b32(), v656, v644);
    svfloat32_t v658 = svadd_f32_x(svptrue_b32(), v649, v650);
    svfloat32_t v665 = svadd_f32_x(svptrue_b32(), v664, v648);
    svfloat32_t v670 = svsub_f32_x(svptrue_b32(), v669, v667);
    svfloat32_t v622 = svadd_f32_x(svptrue_b32(), v621, v618);
    svfloat32_t v624 = svadd_f32_x(svptrue_b32(), v623, v620);
    svfloat32_t v628 = svmla_f32_x(pred_full, v627, v306, v1029);
    svfloat32_t v631 = svadd_f32_x(svptrue_b32(), v630, v620);
    svfloat32_t v653 = svadd_f32_x(svptrue_b32(), v652, v649);
    svfloat32_t v655 = svadd_f32_x(svptrue_b32(), v654, v651);
    svfloat32_t v659 = svcmla_f32_x(pred_full, v658, v1048, v346, 90);
    svfloat32_t v662 = svadd_f32_x(svptrue_b32(), v661, v651);
    svfloat32_t v676 = svsub_f32_x(svptrue_b32(), v634, v626);
    svfloat32_t v680 = svsub_f32_x(svptrue_b32(), v641, v634);
    svfloat32_t v683 = svadd_f32_x(svptrue_b32(), v626, v641);
    svfloat32_t v688 = svsub_f32_x(svptrue_b32(), v665, v657);
    svfloat32_t v692 = svsub_f32_x(svptrue_b32(), v665, v671);
    svfloat32_t v695 = svadd_f32_x(svptrue_b32(), v657, v671);
    svfloat32_t v629 = svadd_f32_x(svptrue_b32(), v628, v617);
    svfloat32_t v632 = svadd_f32_x(svptrue_b32(), v631, v613);
    svfloat32_t v660 = svadd_f32_x(svptrue_b32(), v659, v648);
    svfloat32_t v663 = svadd_f32_x(svptrue_b32(), v662, v644);
    svfloat32_t v677 = svadd_f32_x(svptrue_b32(), v676, v641);
    svfloat32_t v681 = svadd_f32_x(svptrue_b32(), v622, v638);
    svfloat32_t v682 = svadd_f32_x(svptrue_b32(), v624, v640);
    svfloat32_t v689 = svadd_f32_x(svptrue_b32(), v688, v671);
    svfloat32_t v693 = svadd_f32_x(svptrue_b32(), v653, v668);
    svfloat32_t v694 = svadd_f32_x(svptrue_b32(), v655, v670);
    svfloat32_t v719 = svsub_f32_x(svptrue_b32(), v683, v695);
    svfloat32_t v727 = svadd_f32_x(svptrue_b32(), v683, v695);
    svfloat32_t v735 = svadd_f32_x(svptrue_b32(), v680, v692);
    svfloat32_t v743 = svsub_f32_x(svptrue_b32(), v680, v692);
    svfloat32_t v672 = svsub_f32_x(svptrue_b32(), v629, v622);
    svfloat32_t v674 = svsub_f32_x(svptrue_b32(), v632, v624);
    svfloat32_t v678 = svsub_f32_x(svptrue_b32(), v638, v629);
    svfloat32_t v679 = svsub_f32_x(svptrue_b32(), v640, v632);
    svfloat32_t v684 = svsub_f32_x(svptrue_b32(), v660, v653);
    svfloat32_t v686 = svsub_f32_x(svptrue_b32(), v663, v655);
    svfloat32_t v690 = svsub_f32_x(svptrue_b32(), v668, v660);
    svfloat32_t v691 = svsub_f32_x(svptrue_b32(), v670, v663);
    svfloat32_t v751 = svadd_f32_x(svptrue_b32(), v682, v694);
    svfloat32_t v759 = svsub_f32_x(svptrue_b32(), v682, v694);
    svfloat32_t v767 = svadd_f32_x(svptrue_b32(), v677, v689);
    svfloat32_t v775 = svsub_f32_x(svptrue_b32(), v677, v689);
    svfloat32_t v815 = svsub_f32_x(svptrue_b32(), v681, v693);
    svfloat32_t v823 = svadd_f32_x(svptrue_b32(), v681, v693);
    svst1_f64(pred_full, (double *)(v1092), svreinterpret_f64_f32(v719));
    svst1_f64(pred_full, (double *)(v1101), svreinterpret_f64_f32(v727));
    svst1_f64(pred_full, (double *)(v1110), svreinterpret_f64_f32(v735));
    svst1_f64(pred_full, (double *)(v1119), svreinterpret_f64_f32(v743));
    svfloat32_t v673 = svadd_f32_x(svptrue_b32(), v672, v638);
    svfloat32_t v675 = svadd_f32_x(svptrue_b32(), v674, v640);
    svfloat32_t v685 = svadd_f32_x(svptrue_b32(), v684, v668);
    svfloat32_t v687 = svadd_f32_x(svptrue_b32(), v686, v670);
    svfloat32_t v783 = svadd_f32_x(svptrue_b32(), v679, v691);
    svfloat32_t v791 = svsub_f32_x(svptrue_b32(), v679, v691);
    svfloat32_t v799 = svadd_f32_x(svptrue_b32(), v678, v690);
    svfloat32_t v807 = svsub_f32_x(svptrue_b32(), v678, v690);
    svst1_f64(pred_full, (double *)(v1128), svreinterpret_f64_f32(v751));
    svst1_f64(pred_full, (double *)(v1137), svreinterpret_f64_f32(v759));
    svst1_f64(pred_full, (double *)(v1146), svreinterpret_f64_f32(v767));
    svst1_f64(pred_full, (double *)(v1155), svreinterpret_f64_f32(v775));
    svst1_f64(pred_full, (double *)(v1200), svreinterpret_f64_f32(v815));
    svst1_f64(pred_full, (double *)(v1209), svreinterpret_f64_f32(v823));
    svfloat32_t v703 = svadd_f32_x(svptrue_b32(), v673, v685);
    svfloat32_t v711 = svsub_f32_x(svptrue_b32(), v673, v685);
    svfloat32_t v831 = svadd_f32_x(svptrue_b32(), v675, v687);
    svfloat32_t v839 = svsub_f32_x(svptrue_b32(), v675, v687);
    svst1_f64(pred_full, (double *)(v1164), svreinterpret_f64_f32(v783));
    svst1_f64(pred_full, (double *)(v1173), svreinterpret_f64_f32(v791));
    svst1_f64(pred_full, (double *)(v1182), svreinterpret_f64_f32(v799));
    svst1_f64(pred_full, (double *)(v1191), svreinterpret_f64_f32(v807));
    svst1_f64(pred_full, (double *)(v1074), svreinterpret_f64_f32(v703));
    svst1_f64(pred_full, (double *)(v1083), svreinterpret_f64_f32(v711));
    svst1_f64(pred_full, (double *)(v1218), svreinterpret_f64_f32(v831));
    svst1_f64(pred_full, (double *)(v1227), svreinterpret_f64_f32(v839));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu20(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v1021 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v782 = 1.5388417685876268e+00F;
    float v790 = 5.8778525229247325e-01F;
    float v798 = 3.6327126400268028e-01F;
    float v823 = 1.0000000000000000e+00F;
    float v824 = -1.0000000000000000e+00F;
    float v831 = -1.2500000000000000e+00F;
    float v832 = 1.2500000000000000e+00F;
    float v839 = 5.5901699437494745e-01F;
    float v840 = -5.5901699437494745e-01F;
    float32x2_t v842 = (float32x2_t){v4, v4};
    float v848 = -1.5388417685876268e+00F;
    float v853 = -5.8778525229247325e-01F;
    float v858 = -3.6327126400268028e-01F;
    const float32x2_t *v2046 = &v5[istride];
    float32x2_t *v2122 = &v6[ostride];
    float32x2_t v774 = (float32x2_t){v831, v831};
    float32x2_t v779 = (float32x2_t){v839, v839};
    float32x2_t v784 = (float32x2_t){v782, v848};
    float32x2_t v792 = (float32x2_t){v790, v853};
    float32x2_t v800 = (float32x2_t){v798, v858};
    float32x2_t v825 = (float32x2_t){v823, v824};
    float32x2_t v833 = (float32x2_t){v831, v832};
    float32x2_t v841 = (float32x2_t){v839, v840};
    float32x2_t v849 = (float32x2_t){v848, v848};
    float32x2_t v854 = (float32x2_t){v853, v853};
    float32x2_t v859 = (float32x2_t){v858, v858};
    const float32x2_t *v2067 = &v5[0];
    float32x2_t *v2077 = &v6[0];
    float32x4_t v2286 = vld1q_f32((const float32_t *)v2046);
    float32x4_t v47 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v49 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v97 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v99 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v109 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[28]));
    float32x4_t v111 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[29]));
    float32x4_t v159 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v161 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v171 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v173 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v221 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v223 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v233 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[36]));
    float32x4_t v235 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[37]));
    float32x4_t v283 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v285 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v295 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[34]));
    float32x4_t v297 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[35]));
    float32x4_t v345 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v347 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v357 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v359 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v407 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v409 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v419 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v421 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v469 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[32]));
    float32x4_t v471 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[33]));
    float32x4_t v481 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v483 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v531 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[30]));
    float32x4_t v533 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[31]));
    float32x4_t v543 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v545 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v588 = vtrn1q_f32(v2286, v2286);
    float32x4_t v589 = vtrn2q_f32(v2286, v2286);
    float32x4_t v593 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v595 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v605 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v607 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v775 = vcombine_f32(v774, v774);
    float32x4_t v780 = vcombine_f32(v779, v779);
    float32x2_t v786 = vmul_f32(v842, v784);
    float32x2_t v794 = vmul_f32(v842, v792);
    float32x2_t v802 = vmul_f32(v842, v800);
    float32x2_t v827 = vmul_f32(v842, v825);
    float32x2_t v835 = vmul_f32(v842, v833);
    float32x2_t v843 = vmul_f32(v842, v841);
    float32x4_t v850 = vcombine_f32(v849, v849);
    float32x4_t v855 = vcombine_f32(v854, v854);
    float32x4_t v860 = vcombine_f32(v859, v859);
    const float32x2_t *v1859 = &v5[istride * 10];
    const float32x2_t *v1870 = &v5[istride * 5];
    const float32x2_t *v1880 = &v5[istride * 15];
    const float32x2_t *v1892 = &v5[istride * 4];
    const float32x2_t *v1902 = &v5[istride * 14];
    const float32x2_t *v1914 = &v5[istride * 9];
    const float32x2_t *v1924 = &v5[istride * 19];
    const float32x2_t *v1936 = &v5[istride * 8];
    const float32x2_t *v1946 = &v5[istride * 18];
    const float32x2_t *v1958 = &v5[istride * 13];
    const float32x2_t *v1968 = &v5[istride * 3];
    const float32x2_t *v1980 = &v5[istride * 12];
    const float32x2_t *v1990 = &v5[istride * 2];
    const float32x2_t *v2002 = &v5[istride * 17];
    const float32x2_t *v2012 = &v5[istride * 7];
    const float32x2_t *v2024 = &v5[istride * 16];
    const float32x2_t *v2034 = &v5[istride * 6];
    const float32x2_t *v2055 = &v5[istride * 11];
    float32x2_t *v2086 = &v6[ostride * 5];
    float32x2_t *v2095 = &v6[ostride * 10];
    float32x2_t *v2104 = &v6[ostride * 15];
    float32x2_t *v2113 = &v6[ostride * 16];
    float32x2_t *v2131 = &v6[ostride * 6];
    float32x2_t *v2140 = &v6[ostride * 11];
    float32x2_t *v2149 = &v6[ostride * 12];
    float32x2_t *v2158 = &v6[ostride * 17];
    float32x2_t *v2167 = &v6[ostride * 2];
    float32x2_t *v2176 = &v6[ostride * 7];
    float32x2_t *v2185 = &v6[ostride * 8];
    float32x2_t *v2194 = &v6[ostride * 13];
    float32x2_t *v2203 = &v6[ostride * 18];
    float32x2_t *v2212 = &v6[ostride * 3];
    float32x2_t *v2221 = &v6[ostride * 4];
    float32x2_t *v2230 = &v6[ostride * 9];
    float32x2_t *v2239 = &v6[ostride * 14];
    float32x2_t *v2248 = &v6[ostride * 19];
    float32x4_t v2290 = vld1q_f32((const float32_t *)v2067);
    float32x4_t v594 = vmulq_f32(v588, v593);
    float32x4_t v788 = vcombine_f32(v786, v786);
    float32x4_t v796 = vcombine_f32(v794, v794);
    float32x4_t v804 = vcombine_f32(v802, v802);
    float32x4_t v829 = vcombine_f32(v827, v827);
    float32x4_t v837 = vcombine_f32(v835, v835);
    float32x4_t v845 = vcombine_f32(v843, v843);
    float32x4_t v2252 = vld1q_f32((const float32_t *)v1859);
    float32x4_t v2254 = vld1q_f32((const float32_t *)v1870);
    float32x4_t v2256 = vld1q_f32((const float32_t *)v1880);
    float32x4_t v2258 = vld1q_f32((const float32_t *)v1892);
    float32x4_t v2260 = vld1q_f32((const float32_t *)v1902);
    float32x4_t v2262 = vld1q_f32((const float32_t *)v1914);
    float32x4_t v2264 = vld1q_f32((const float32_t *)v1924);
    float32x4_t v2266 = vld1q_f32((const float32_t *)v1936);
    float32x4_t v2268 = vld1q_f32((const float32_t *)v1946);
    float32x4_t v2270 = vld1q_f32((const float32_t *)v1958);
    float32x4_t v2272 = vld1q_f32((const float32_t *)v1968);
    float32x4_t v2274 = vld1q_f32((const float32_t *)v1980);
    float32x4_t v2276 = vld1q_f32((const float32_t *)v1990);
    float32x4_t v2278 = vld1q_f32((const float32_t *)v2002);
    float32x4_t v2280 = vld1q_f32((const float32_t *)v2012);
    float32x4_t v2282 = vld1q_f32((const float32_t *)v2024);
    float32x4_t v2284 = vld1q_f32((const float32_t *)v2034);
    float32x4_t v2288 = vld1q_f32((const float32_t *)v2055);
    float32x4_t v42 = vtrn1q_f32(v2252, v2252);
    float32x4_t v43 = vtrn2q_f32(v2252, v2252);
    float32x4_t v92 = vtrn1q_f32(v2254, v2254);
    float32x4_t v93 = vtrn2q_f32(v2254, v2254);
    float32x4_t v104 = vtrn1q_f32(v2256, v2256);
    float32x4_t v105 = vtrn2q_f32(v2256, v2256);
    float32x4_t v154 = vtrn1q_f32(v2258, v2258);
    float32x4_t v155 = vtrn2q_f32(v2258, v2258);
    float32x4_t v166 = vtrn1q_f32(v2260, v2260);
    float32x4_t v167 = vtrn2q_f32(v2260, v2260);
    float32x4_t v216 = vtrn1q_f32(v2262, v2262);
    float32x4_t v217 = vtrn2q_f32(v2262, v2262);
    float32x4_t v228 = vtrn1q_f32(v2264, v2264);
    float32x4_t v229 = vtrn2q_f32(v2264, v2264);
    float32x4_t v278 = vtrn1q_f32(v2266, v2266);
    float32x4_t v279 = vtrn2q_f32(v2266, v2266);
    float32x4_t v290 = vtrn1q_f32(v2268, v2268);
    float32x4_t v291 = vtrn2q_f32(v2268, v2268);
    float32x4_t v340 = vtrn1q_f32(v2270, v2270);
    float32x4_t v341 = vtrn2q_f32(v2270, v2270);
    float32x4_t v352 = vtrn1q_f32(v2272, v2272);
    float32x4_t v353 = vtrn2q_f32(v2272, v2272);
    float32x4_t v402 = vtrn1q_f32(v2274, v2274);
    float32x4_t v403 = vtrn2q_f32(v2274, v2274);
    float32x4_t v414 = vtrn1q_f32(v2276, v2276);
    float32x4_t v415 = vtrn2q_f32(v2276, v2276);
    float32x4_t v464 = vtrn1q_f32(v2278, v2278);
    float32x4_t v465 = vtrn2q_f32(v2278, v2278);
    float32x4_t v476 = vtrn1q_f32(v2280, v2280);
    float32x4_t v477 = vtrn2q_f32(v2280, v2280);
    float32x4_t v526 = vtrn1q_f32(v2282, v2282);
    float32x4_t v527 = vtrn2q_f32(v2282, v2282);
    float32x4_t v538 = vtrn1q_f32(v2284, v2284);
    float32x4_t v539 = vtrn2q_f32(v2284, v2284);
    float32x4_t v597 = vfmaq_f32(v594, v589, v595);
    float32x4_t v600 = vtrn1q_f32(v2288, v2288);
    float32x4_t v601 = vtrn2q_f32(v2288, v2288);
    float32x4_t v48 = vmulq_f32(v42, v47);
    float32x4_t v98 = vmulq_f32(v92, v97);
    float32x4_t v110 = vmulq_f32(v104, v109);
    float32x4_t v160 = vmulq_f32(v154, v159);
    float32x4_t v172 = vmulq_f32(v166, v171);
    float32x4_t v222 = vmulq_f32(v216, v221);
    float32x4_t v234 = vmulq_f32(v228, v233);
    float32x4_t v284 = vmulq_f32(v278, v283);
    float32x4_t v296 = vmulq_f32(v290, v295);
    float32x4_t v346 = vmulq_f32(v340, v345);
    float32x4_t v358 = vmulq_f32(v352, v357);
    float32x4_t v408 = vmulq_f32(v402, v407);
    float32x4_t v420 = vmulq_f32(v414, v419);
    float32x4_t v470 = vmulq_f32(v464, v469);
    float32x4_t v482 = vmulq_f32(v476, v481);
    float32x4_t v532 = vmulq_f32(v526, v531);
    float32x4_t v544 = vmulq_f32(v538, v543);
    float32x4_t v606 = vmulq_f32(v600, v605);
    float32x4_t v51 = vfmaq_f32(v48, v43, v49);
    float32x4_t v101 = vfmaq_f32(v98, v93, v99);
    float32x4_t v113 = vfmaq_f32(v110, v105, v111);
    float32x4_t v163 = vfmaq_f32(v160, v155, v161);
    float32x4_t v175 = vfmaq_f32(v172, v167, v173);
    float32x4_t v225 = vfmaq_f32(v222, v217, v223);
    float32x4_t v237 = vfmaq_f32(v234, v229, v235);
    float32x4_t v287 = vfmaq_f32(v284, v279, v285);
    float32x4_t v299 = vfmaq_f32(v296, v291, v297);
    float32x4_t v349 = vfmaq_f32(v346, v341, v347);
    float32x4_t v361 = vfmaq_f32(v358, v353, v359);
    float32x4_t v411 = vfmaq_f32(v408, v403, v409);
    float32x4_t v423 = vfmaq_f32(v420, v415, v421);
    float32x4_t v473 = vfmaq_f32(v470, v465, v471);
    float32x4_t v485 = vfmaq_f32(v482, v477, v483);
    float32x4_t v535 = vfmaq_f32(v532, v527, v533);
    float32x4_t v547 = vfmaq_f32(v544, v539, v545);
    float32x4_t v609 = vfmaq_f32(v606, v601, v607);
    float32x4_t v617 = vaddq_f32(v2290, v51);
    float32x4_t v618 = vsubq_f32(v2290, v51);
    float32x4_t v619 = vaddq_f32(v101, v113);
    float32x4_t v620 = vsubq_f32(v101, v113);
    float32x4_t v623 = vaddq_f32(v163, v175);
    float32x4_t v624 = vsubq_f32(v163, v175);
    float32x4_t v625 = vaddq_f32(v225, v237);
    float32x4_t v626 = vsubq_f32(v225, v237);
    float32x4_t v629 = vaddq_f32(v287, v299);
    float32x4_t v630 = vsubq_f32(v287, v299);
    float32x4_t v631 = vaddq_f32(v349, v361);
    float32x4_t v632 = vsubq_f32(v349, v361);
    float32x4_t v635 = vaddq_f32(v411, v423);
    float32x4_t v636 = vsubq_f32(v411, v423);
    float32x4_t v637 = vaddq_f32(v473, v485);
    float32x4_t v638 = vsubq_f32(v473, v485);
    float32x4_t v641 = vaddq_f32(v535, v547);
    float32x4_t v642 = vsubq_f32(v535, v547);
    float32x4_t v643 = vaddq_f32(v597, v609);
    float32x4_t v644 = vsubq_f32(v597, v609);
    float32x4_t v621 = vaddq_f32(v617, v619);
    float32x4_t v622 = vsubq_f32(v617, v619);
    float32x4_t v627 = vaddq_f32(v623, v625);
    float32x4_t v628 = vsubq_f32(v623, v625);
    float32x4_t v633 = vaddq_f32(v629, v631);
    float32x4_t v634 = vsubq_f32(v629, v631);
    float32x4_t v639 = vaddq_f32(v635, v637);
    float32x4_t v640 = vsubq_f32(v635, v637);
    float32x4_t v645 = vaddq_f32(v641, v643);
    float32x4_t v646 = vsubq_f32(v641, v643);
    float32x4_t v759 = vaddq_f32(v624, v642);
    float32x4_t v760 = vsubq_f32(v624, v642);
    float32x4_t v761 = vaddq_f32(v636, v630);
    float32x4_t v762 = vsubq_f32(v636, v630);
    float32x4_t v815 = vaddq_f32(v626, v644);
    float32x4_t v816 = vsubq_f32(v626, v644);
    float32x4_t v817 = vaddq_f32(v638, v632);
    float32x4_t v818 = vsubq_f32(v638, v632);
    float32x4_t v647 = vaddq_f32(v627, v645);
    float32x4_t v648 = vsubq_f32(v627, v645);
    float32x4_t v649 = vaddq_f32(v639, v633);
    float32x4_t v650 = vsubq_f32(v639, v633);
    float32x4_t v703 = vaddq_f32(v628, v646);
    float32x4_t v704 = vsubq_f32(v628, v646);
    float32x4_t v705 = vaddq_f32(v640, v634);
    float32x4_t v706 = vsubq_f32(v640, v634);
    float32x4_t v763 = vaddq_f32(v759, v761);
    float32x4_t v764 = vsubq_f32(v759, v761);
    float32x4_t v765 = vaddq_f32(v760, v762);
    float32x4_t v787 = vrev64q_f32(v760);
    float32x4_t v803 = vrev64q_f32(v762);
    float32x4_t v819 = vaddq_f32(v815, v817);
    float32x4_t v820 = vsubq_f32(v815, v817);
    float32x4_t v821 = vaddq_f32(v816, v818);
    float32x4_t v851 = vmulq_f32(v816, v850);
    float32x4_t v861 = vmulq_f32(v818, v860);
    float32x4_t v651 = vaddq_f32(v647, v649);
    float32x4_t v652 = vsubq_f32(v647, v649);
    float32x4_t v653 = vaddq_f32(v648, v650);
    float32x4_t v675 = vrev64q_f32(v648);
    float32x4_t v691 = vrev64q_f32(v650);
    float32x4_t v707 = vaddq_f32(v703, v705);
    float32x4_t v708 = vsubq_f32(v703, v705);
    float32x4_t v709 = vaddq_f32(v704, v706);
    float32x4_t v731 = vrev64q_f32(v704);
    float32x4_t v747 = vrev64q_f32(v706);
    float32x4_t v766 = vaddq_f32(v763, v618);
    float32x4_t v776 = vmulq_f32(v763, v775);
    float32x4_t v781 = vmulq_f32(v764, v780);
    float32x4_t v789 = vmulq_f32(v787, v788);
    float32x4_t v795 = vrev64q_f32(v765);
    float32x4_t v805 = vmulq_f32(v803, v804);
    float32x4_t v822 = vaddq_f32(v819, v620);
    float32x4_t v836 = vrev64q_f32(v819);
    float32x4_t v844 = vrev64q_f32(v820);
    float32x4_t v856 = vmulq_f32(v821, v855);
    float32x4_t v654 = vaddq_f32(v651, v621);
    float32x4_t v664 = vmulq_f32(v651, v775);
    float32x4_t v669 = vmulq_f32(v652, v780);
    float32x4_t v677 = vmulq_f32(v675, v788);
    float32x4_t v683 = vrev64q_f32(v653);
    float32x4_t v693 = vmulq_f32(v691, v804);
    float32x4_t v710 = vaddq_f32(v707, v622);
    float32x4_t v720 = vmulq_f32(v707, v775);
    float32x4_t v725 = vmulq_f32(v708, v780);
    float32x4_t v733 = vmulq_f32(v731, v788);
    float32x4_t v739 = vrev64q_f32(v709);
    float32x4_t v749 = vmulq_f32(v747, v804);
    float32x4_t v797 = vmulq_f32(v795, v796);
    float32x4_t v806 = vaddq_f32(v766, v776);
    float32x4_t v828 = vrev64q_f32(v822);
    float32x4_t v838 = vmulq_f32(v836, v837);
    float32x4_t v846 = vmulq_f32(v844, v845);
    float32x4_t v865 = vsubq_f32(v851, v856);
    float32x4_t v866 = vaddq_f32(v856, v861);
    float32x4_t v685 = vmulq_f32(v683, v796);
    float32x4_t v694 = vaddq_f32(v654, v664);
    float32x4_t v741 = vmulq_f32(v739, v796);
    float32x4_t v750 = vaddq_f32(v710, v720);
    float32x4_t v807 = vaddq_f32(v806, v781);
    float32x4_t v808 = vsubq_f32(v806, v781);
    float32x4_t v809 = vsubq_f32(v789, v797);
    float32x4_t v810 = vaddq_f32(v797, v805);
    float32x4_t v830 = vmulq_f32(v828, v829);
    vst1q_f32((float32_t *)v2077, v654);
    vst1q_f32((float32_t *)v2095, v710);
    float32x4_t v695 = vaddq_f32(v694, v669);
    float32x4_t v696 = vsubq_f32(v694, v669);
    float32x4_t v697 = vsubq_f32(v677, v685);
    float32x4_t v698 = vaddq_f32(v685, v693);
    float32x4_t v751 = vaddq_f32(v750, v725);
    float32x4_t v752 = vsubq_f32(v750, v725);
    float32x4_t v753 = vsubq_f32(v733, v741);
    float32x4_t v754 = vaddq_f32(v741, v749);
    float32x4_t v811 = vaddq_f32(v807, v809);
    float32x4_t v812 = vsubq_f32(v807, v809);
    float32x4_t v813 = vaddq_f32(v808, v810);
    float32x4_t v814 = vsubq_f32(v808, v810);
    float32x4_t v862 = vaddq_f32(v830, v838);
    float32x4_t v871 = vaddq_f32(v766, v830);
    float32x4_t v872 = vsubq_f32(v766, v830);
    float32x4_t v699 = vaddq_f32(v695, v697);
    float32x4_t v700 = vsubq_f32(v695, v697);
    float32x4_t v701 = vaddq_f32(v696, v698);
    float32x4_t v702 = vsubq_f32(v696, v698);
    float32x4_t v755 = vaddq_f32(v751, v753);
    float32x4_t v756 = vsubq_f32(v751, v753);
    float32x4_t v757 = vaddq_f32(v752, v754);
    float32x4_t v758 = vsubq_f32(v752, v754);
    float32x4_t v863 = vaddq_f32(v862, v846);
    float32x4_t v864 = vsubq_f32(v862, v846);
    vst1q_f32((float32_t *)v2086, v872);
    vst1q_f32((float32_t *)v2104, v871);
    float32x4_t v867 = vaddq_f32(v863, v865);
    float32x4_t v868 = vsubq_f32(v863, v865);
    float32x4_t v869 = vaddq_f32(v864, v866);
    float32x4_t v870 = vsubq_f32(v864, v866);
    vst1q_f32((float32_t *)v2113, v700);
    vst1q_f32((float32_t *)v2131, v756);
    vst1q_f32((float32_t *)v2149, v702);
    vst1q_f32((float32_t *)v2167, v758);
    vst1q_f32((float32_t *)v2185, v701);
    vst1q_f32((float32_t *)v2203, v757);
    vst1q_f32((float32_t *)v2221, v699);
    vst1q_f32((float32_t *)v2239, v755);
    float32x4_t v901 = vaddq_f32(v812, v868);
    float32x4_t v902 = vsubq_f32(v812, v868);
    float32x4_t v931 = vaddq_f32(v814, v870);
    float32x4_t v932 = vsubq_f32(v814, v870);
    float32x4_t v961 = vaddq_f32(v813, v869);
    float32x4_t v962 = vsubq_f32(v813, v869);
    float32x4_t v991 = vaddq_f32(v811, v867);
    float32x4_t v992 = vsubq_f32(v811, v867);
    vst1q_f32((float32_t *)v2122, v902);
    vst1q_f32((float32_t *)v2140, v901);
    vst1q_f32((float32_t *)v2158, v932);
    vst1q_f32((float32_t *)v2176, v931);
    vst1q_f32((float32_t *)v2194, v962);
    vst1q_f32((float32_t *)v2212, v961);
    vst1q_f32((float32_t *)v2230, v992);
    vst1q_f32((float32_t *)v2248, v991);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v1021 * 2; j < howmany; j += 1) {
    float32x2_t v1458 = v5[istride];
    float v1659 = 1.5388417685876268e+00F;
    float v1666 = 5.8778525229247325e-01F;
    float v1673 = 3.6327126400268028e-01F;
    float v1697 = 1.0000000000000000e+00F;
    float v1698 = -1.0000000000000000e+00F;
    float v1704 = -1.2500000000000000e+00F;
    float v1705 = 1.2500000000000000e+00F;
    float v1711 = 5.5901699437494745e-01F;
    float v1712 = -5.5901699437494745e-01F;
    float32x2_t v1714 = (float32x2_t){v4, v4};
    float v1719 = -1.5388417685876268e+00F;
    float v1723 = -5.8778525229247325e-01F;
    float v1727 = -3.6327126400268028e-01F;
    float32x2_t v1045 = v7[18];
    float32x2_t v1050 = v7[19];
    float32x2_t v1085 = v7[8];
    float32x2_t v1090 = v7[9];
    float32x2_t v1095 = v7[28];
    float32x2_t v1100 = v7[29];
    float32x2_t v1135 = v7[6];
    float32x2_t v1140 = v7[7];
    float32x2_t v1145 = v7[26];
    float32x2_t v1150 = v7[27];
    float32x2_t v1185 = v7[16];
    float32x2_t v1190 = v7[17];
    float32x2_t v1195 = v7[36];
    float32x2_t v1200 = v7[37];
    float32x2_t v1235 = v7[14];
    float32x2_t v1240 = v7[15];
    float32x2_t v1245 = v7[34];
    float32x2_t v1250 = v7[35];
    float32x2_t v1285 = v7[24];
    float32x2_t v1290 = v7[25];
    float32x2_t v1295 = v7[4];
    float32x2_t v1300 = v7[5];
    float32x2_t v1335 = v7[22];
    float32x2_t v1340 = v7[23];
    float32x2_t v1345 = v7[2];
    float32x2_t v1350 = v7[3];
    float32x2_t v1385 = v7[32];
    float32x2_t v1390 = v7[33];
    float32x2_t v1395 = v7[12];
    float32x2_t v1400 = v7[13];
    float32x2_t v1435 = v7[30];
    float32x2_t v1440 = v7[31];
    float32x2_t v1445 = v7[10];
    float32x2_t v1450 = v7[11];
    float32x2_t v1485 = v7[0];
    float32x2_t v1486 = vtrn1_f32(v1458, v1458);
    float32x2_t v1487 = vtrn2_f32(v1458, v1458);
    float32x2_t v1490 = v7[1];
    float32x2_t v1495 = v7[20];
    float32x2_t v1500 = v7[21];
    float32x2_t v1508 = v5[0];
    float32x2_t v1653 = (float32x2_t){v1704, v1704};
    float32x2_t v1657 = (float32x2_t){v1711, v1711};
    float32x2_t v1661 = (float32x2_t){v1659, v1719};
    float32x2_t v1668 = (float32x2_t){v1666, v1723};
    float32x2_t v1675 = (float32x2_t){v1673, v1727};
    float32x2_t v1699 = (float32x2_t){v1697, v1698};
    float32x2_t v1706 = (float32x2_t){v1704, v1705};
    float32x2_t v1713 = (float32x2_t){v1711, v1712};
    float32x2_t v1720 = (float32x2_t){v1719, v1719};
    float32x2_t v1724 = (float32x2_t){v1723, v1723};
    float32x2_t v1728 = (float32x2_t){v1727, v1727};
    float32x2_t v1033 = v5[istride * 10];
    float32x2_t v1058 = v5[istride * 5];
    float32x2_t v1073 = v5[istride * 15];
    float32x2_t v1108 = v5[istride * 4];
    float32x2_t v1123 = v5[istride * 14];
    float32x2_t v1158 = v5[istride * 9];
    float32x2_t v1173 = v5[istride * 19];
    float32x2_t v1208 = v5[istride * 8];
    float32x2_t v1223 = v5[istride * 18];
    float32x2_t v1258 = v5[istride * 13];
    float32x2_t v1273 = v5[istride * 3];
    float32x2_t v1308 = v5[istride * 12];
    float32x2_t v1323 = v5[istride * 2];
    float32x2_t v1358 = v5[istride * 17];
    float32x2_t v1373 = v5[istride * 7];
    float32x2_t v1408 = v5[istride * 16];
    float32x2_t v1423 = v5[istride * 6];
    float32x2_t v1473 = v5[istride * 11];
    float32x2_t v1491 = vmul_f32(v1486, v1485);
    float32x2_t v1663 = vmul_f32(v1714, v1661);
    float32x2_t v1670 = vmul_f32(v1714, v1668);
    float32x2_t v1677 = vmul_f32(v1714, v1675);
    float32x2_t v1701 = vmul_f32(v1714, v1699);
    float32x2_t v1708 = vmul_f32(v1714, v1706);
    float32x2_t v1715 = vmul_f32(v1714, v1713);
    float32x2_t v1046 = vtrn1_f32(v1033, v1033);
    float32x2_t v1047 = vtrn2_f32(v1033, v1033);
    float32x2_t v1086 = vtrn1_f32(v1058, v1058);
    float32x2_t v1087 = vtrn2_f32(v1058, v1058);
    float32x2_t v1096 = vtrn1_f32(v1073, v1073);
    float32x2_t v1097 = vtrn2_f32(v1073, v1073);
    float32x2_t v1136 = vtrn1_f32(v1108, v1108);
    float32x2_t v1137 = vtrn2_f32(v1108, v1108);
    float32x2_t v1146 = vtrn1_f32(v1123, v1123);
    float32x2_t v1147 = vtrn2_f32(v1123, v1123);
    float32x2_t v1186 = vtrn1_f32(v1158, v1158);
    float32x2_t v1187 = vtrn2_f32(v1158, v1158);
    float32x2_t v1196 = vtrn1_f32(v1173, v1173);
    float32x2_t v1197 = vtrn2_f32(v1173, v1173);
    float32x2_t v1236 = vtrn1_f32(v1208, v1208);
    float32x2_t v1237 = vtrn2_f32(v1208, v1208);
    float32x2_t v1246 = vtrn1_f32(v1223, v1223);
    float32x2_t v1247 = vtrn2_f32(v1223, v1223);
    float32x2_t v1286 = vtrn1_f32(v1258, v1258);
    float32x2_t v1287 = vtrn2_f32(v1258, v1258);
    float32x2_t v1296 = vtrn1_f32(v1273, v1273);
    float32x2_t v1297 = vtrn2_f32(v1273, v1273);
    float32x2_t v1336 = vtrn1_f32(v1308, v1308);
    float32x2_t v1337 = vtrn2_f32(v1308, v1308);
    float32x2_t v1346 = vtrn1_f32(v1323, v1323);
    float32x2_t v1347 = vtrn2_f32(v1323, v1323);
    float32x2_t v1386 = vtrn1_f32(v1358, v1358);
    float32x2_t v1387 = vtrn2_f32(v1358, v1358);
    float32x2_t v1396 = vtrn1_f32(v1373, v1373);
    float32x2_t v1397 = vtrn2_f32(v1373, v1373);
    float32x2_t v1436 = vtrn1_f32(v1408, v1408);
    float32x2_t v1437 = vtrn2_f32(v1408, v1408);
    float32x2_t v1446 = vtrn1_f32(v1423, v1423);
    float32x2_t v1447 = vtrn2_f32(v1423, v1423);
    float32x2_t v1493 = vfma_f32(v1491, v1487, v1490);
    float32x2_t v1496 = vtrn1_f32(v1473, v1473);
    float32x2_t v1497 = vtrn2_f32(v1473, v1473);
    float32x2_t v1051 = vmul_f32(v1046, v1045);
    float32x2_t v1091 = vmul_f32(v1086, v1085);
    float32x2_t v1101 = vmul_f32(v1096, v1095);
    float32x2_t v1141 = vmul_f32(v1136, v1135);
    float32x2_t v1151 = vmul_f32(v1146, v1145);
    float32x2_t v1191 = vmul_f32(v1186, v1185);
    float32x2_t v1201 = vmul_f32(v1196, v1195);
    float32x2_t v1241 = vmul_f32(v1236, v1235);
    float32x2_t v1251 = vmul_f32(v1246, v1245);
    float32x2_t v1291 = vmul_f32(v1286, v1285);
    float32x2_t v1301 = vmul_f32(v1296, v1295);
    float32x2_t v1341 = vmul_f32(v1336, v1335);
    float32x2_t v1351 = vmul_f32(v1346, v1345);
    float32x2_t v1391 = vmul_f32(v1386, v1385);
    float32x2_t v1401 = vmul_f32(v1396, v1395);
    float32x2_t v1441 = vmul_f32(v1436, v1435);
    float32x2_t v1451 = vmul_f32(v1446, v1445);
    float32x2_t v1501 = vmul_f32(v1496, v1495);
    float32x2_t v1053 = vfma_f32(v1051, v1047, v1050);
    float32x2_t v1093 = vfma_f32(v1091, v1087, v1090);
    float32x2_t v1103 = vfma_f32(v1101, v1097, v1100);
    float32x2_t v1143 = vfma_f32(v1141, v1137, v1140);
    float32x2_t v1153 = vfma_f32(v1151, v1147, v1150);
    float32x2_t v1193 = vfma_f32(v1191, v1187, v1190);
    float32x2_t v1203 = vfma_f32(v1201, v1197, v1200);
    float32x2_t v1243 = vfma_f32(v1241, v1237, v1240);
    float32x2_t v1253 = vfma_f32(v1251, v1247, v1250);
    float32x2_t v1293 = vfma_f32(v1291, v1287, v1290);
    float32x2_t v1303 = vfma_f32(v1301, v1297, v1300);
    float32x2_t v1343 = vfma_f32(v1341, v1337, v1340);
    float32x2_t v1353 = vfma_f32(v1351, v1347, v1350);
    float32x2_t v1393 = vfma_f32(v1391, v1387, v1390);
    float32x2_t v1403 = vfma_f32(v1401, v1397, v1400);
    float32x2_t v1443 = vfma_f32(v1441, v1437, v1440);
    float32x2_t v1453 = vfma_f32(v1451, v1447, v1450);
    float32x2_t v1503 = vfma_f32(v1501, v1497, v1500);
    float32x2_t v1509 = vadd_f32(v1508, v1053);
    float32x2_t v1510 = vsub_f32(v1508, v1053);
    float32x2_t v1511 = vadd_f32(v1093, v1103);
    float32x2_t v1512 = vsub_f32(v1093, v1103);
    float32x2_t v1515 = vadd_f32(v1143, v1153);
    float32x2_t v1516 = vsub_f32(v1143, v1153);
    float32x2_t v1517 = vadd_f32(v1193, v1203);
    float32x2_t v1518 = vsub_f32(v1193, v1203);
    float32x2_t v1521 = vadd_f32(v1243, v1253);
    float32x2_t v1522 = vsub_f32(v1243, v1253);
    float32x2_t v1523 = vadd_f32(v1293, v1303);
    float32x2_t v1524 = vsub_f32(v1293, v1303);
    float32x2_t v1527 = vadd_f32(v1343, v1353);
    float32x2_t v1528 = vsub_f32(v1343, v1353);
    float32x2_t v1529 = vadd_f32(v1393, v1403);
    float32x2_t v1530 = vsub_f32(v1393, v1403);
    float32x2_t v1533 = vadd_f32(v1443, v1453);
    float32x2_t v1534 = vsub_f32(v1443, v1453);
    float32x2_t v1535 = vadd_f32(v1493, v1503);
    float32x2_t v1536 = vsub_f32(v1493, v1503);
    float32x2_t v1513 = vadd_f32(v1509, v1511);
    float32x2_t v1514 = vsub_f32(v1509, v1511);
    float32x2_t v1519 = vadd_f32(v1515, v1517);
    float32x2_t v1520 = vsub_f32(v1515, v1517);
    float32x2_t v1525 = vadd_f32(v1521, v1523);
    float32x2_t v1526 = vsub_f32(v1521, v1523);
    float32x2_t v1531 = vadd_f32(v1527, v1529);
    float32x2_t v1532 = vsub_f32(v1527, v1529);
    float32x2_t v1537 = vadd_f32(v1533, v1535);
    float32x2_t v1538 = vsub_f32(v1533, v1535);
    float32x2_t v1639 = vadd_f32(v1516, v1534);
    float32x2_t v1640 = vsub_f32(v1516, v1534);
    float32x2_t v1641 = vadd_f32(v1528, v1522);
    float32x2_t v1642 = vsub_f32(v1528, v1522);
    float32x2_t v1689 = vadd_f32(v1518, v1536);
    float32x2_t v1690 = vsub_f32(v1518, v1536);
    float32x2_t v1691 = vadd_f32(v1530, v1524);
    float32x2_t v1692 = vsub_f32(v1530, v1524);
    float32x2_t v1539 = vadd_f32(v1519, v1537);
    float32x2_t v1540 = vsub_f32(v1519, v1537);
    float32x2_t v1541 = vadd_f32(v1531, v1525);
    float32x2_t v1542 = vsub_f32(v1531, v1525);
    float32x2_t v1589 = vadd_f32(v1520, v1538);
    float32x2_t v1590 = vsub_f32(v1520, v1538);
    float32x2_t v1591 = vadd_f32(v1532, v1526);
    float32x2_t v1592 = vsub_f32(v1532, v1526);
    float32x2_t v1643 = vadd_f32(v1639, v1641);
    float32x2_t v1644 = vsub_f32(v1639, v1641);
    float32x2_t v1645 = vadd_f32(v1640, v1642);
    float32x2_t v1664 = vrev64_f32(v1640);
    float32x2_t v1678 = vrev64_f32(v1642);
    float32x2_t v1693 = vadd_f32(v1689, v1691);
    float32x2_t v1694 = vsub_f32(v1689, v1691);
    float32x2_t v1695 = vadd_f32(v1690, v1692);
    float32x2_t v1721 = vmul_f32(v1690, v1720);
    float32x2_t v1729 = vmul_f32(v1692, v1728);
    float32x2_t v1543 = vadd_f32(v1539, v1541);
    float32x2_t v1544 = vsub_f32(v1539, v1541);
    float32x2_t v1545 = vadd_f32(v1540, v1542);
    float32x2_t v1564 = vrev64_f32(v1540);
    float32x2_t v1578 = vrev64_f32(v1542);
    float32x2_t v1593 = vadd_f32(v1589, v1591);
    float32x2_t v1594 = vsub_f32(v1589, v1591);
    float32x2_t v1595 = vadd_f32(v1590, v1592);
    float32x2_t v1614 = vrev64_f32(v1590);
    float32x2_t v1628 = vrev64_f32(v1592);
    float32x2_t v1646 = vadd_f32(v1643, v1510);
    float32x2_t v1654 = vmul_f32(v1643, v1653);
    float32x2_t v1658 = vmul_f32(v1644, v1657);
    float32x2_t v1665 = vmul_f32(v1664, v1663);
    float32x2_t v1671 = vrev64_f32(v1645);
    float32x2_t v1679 = vmul_f32(v1678, v1677);
    float32x2_t v1696 = vadd_f32(v1693, v1512);
    float32x2_t v1709 = vrev64_f32(v1693);
    float32x2_t v1716 = vrev64_f32(v1694);
    float32x2_t v1725 = vmul_f32(v1695, v1724);
    float32x2_t v1546 = vadd_f32(v1543, v1513);
    float32x2_t v1554 = vmul_f32(v1543, v1653);
    float32x2_t v1558 = vmul_f32(v1544, v1657);
    float32x2_t v1565 = vmul_f32(v1564, v1663);
    float32x2_t v1571 = vrev64_f32(v1545);
    float32x2_t v1579 = vmul_f32(v1578, v1677);
    float32x2_t v1596 = vadd_f32(v1593, v1514);
    float32x2_t v1604 = vmul_f32(v1593, v1653);
    float32x2_t v1608 = vmul_f32(v1594, v1657);
    float32x2_t v1615 = vmul_f32(v1614, v1663);
    float32x2_t v1621 = vrev64_f32(v1595);
    float32x2_t v1629 = vmul_f32(v1628, v1677);
    float32x2_t v1672 = vmul_f32(v1671, v1670);
    float32x2_t v1680 = vadd_f32(v1646, v1654);
    float32x2_t v1702 = vrev64_f32(v1696);
    float32x2_t v1710 = vmul_f32(v1709, v1708);
    float32x2_t v1717 = vmul_f32(v1716, v1715);
    float32x2_t v1733 = vsub_f32(v1721, v1725);
    float32x2_t v1734 = vadd_f32(v1725, v1729);
    float32x2_t v1572 = vmul_f32(v1571, v1670);
    float32x2_t v1580 = vadd_f32(v1546, v1554);
    float32x2_t v1622 = vmul_f32(v1621, v1670);
    float32x2_t v1630 = vadd_f32(v1596, v1604);
    float32x2_t v1681 = vadd_f32(v1680, v1658);
    float32x2_t v1682 = vsub_f32(v1680, v1658);
    float32x2_t v1683 = vsub_f32(v1665, v1672);
    float32x2_t v1684 = vadd_f32(v1672, v1679);
    float32x2_t v1703 = vmul_f32(v1702, v1701);
    v6[0] = v1546;
    v6[ostride * 10] = v1596;
    float32x2_t v1581 = vadd_f32(v1580, v1558);
    float32x2_t v1582 = vsub_f32(v1580, v1558);
    float32x2_t v1583 = vsub_f32(v1565, v1572);
    float32x2_t v1584 = vadd_f32(v1572, v1579);
    float32x2_t v1631 = vadd_f32(v1630, v1608);
    float32x2_t v1632 = vsub_f32(v1630, v1608);
    float32x2_t v1633 = vsub_f32(v1615, v1622);
    float32x2_t v1634 = vadd_f32(v1622, v1629);
    float32x2_t v1685 = vadd_f32(v1681, v1683);
    float32x2_t v1686 = vsub_f32(v1681, v1683);
    float32x2_t v1687 = vadd_f32(v1682, v1684);
    float32x2_t v1688 = vsub_f32(v1682, v1684);
    float32x2_t v1730 = vadd_f32(v1703, v1710);
    float32x2_t v1739 = vadd_f32(v1646, v1703);
    float32x2_t v1740 = vsub_f32(v1646, v1703);
    float32x2_t v1585 = vadd_f32(v1581, v1583);
    float32x2_t v1586 = vsub_f32(v1581, v1583);
    float32x2_t v1587 = vadd_f32(v1582, v1584);
    float32x2_t v1588 = vsub_f32(v1582, v1584);
    float32x2_t v1635 = vadd_f32(v1631, v1633);
    float32x2_t v1636 = vsub_f32(v1631, v1633);
    float32x2_t v1637 = vadd_f32(v1632, v1634);
    float32x2_t v1638 = vsub_f32(v1632, v1634);
    float32x2_t v1731 = vadd_f32(v1730, v1717);
    float32x2_t v1732 = vsub_f32(v1730, v1717);
    v6[ostride * 5] = v1740;
    v6[ostride * 15] = v1739;
    float32x2_t v1735 = vadd_f32(v1731, v1733);
    float32x2_t v1736 = vsub_f32(v1731, v1733);
    float32x2_t v1737 = vadd_f32(v1732, v1734);
    float32x2_t v1738 = vsub_f32(v1732, v1734);
    v6[ostride * 16] = v1586;
    v6[ostride * 6] = v1636;
    v6[ostride * 12] = v1588;
    v6[ostride * 2] = v1638;
    v6[ostride * 8] = v1587;
    v6[ostride * 18] = v1637;
    v6[ostride * 4] = v1585;
    v6[ostride * 14] = v1635;
    float32x2_t v1761 = vadd_f32(v1686, v1736);
    float32x2_t v1762 = vsub_f32(v1686, v1736);
    float32x2_t v1783 = vadd_f32(v1688, v1738);
    float32x2_t v1784 = vsub_f32(v1688, v1738);
    float32x2_t v1805 = vadd_f32(v1687, v1737);
    float32x2_t v1806 = vsub_f32(v1687, v1737);
    float32x2_t v1827 = vadd_f32(v1685, v1735);
    float32x2_t v1828 = vsub_f32(v1685, v1735);
    v6[ostride] = v1762;
    v6[ostride * 11] = v1761;
    v6[ostride * 17] = v1784;
    v6[ostride * 7] = v1783;
    v6[ostride * 13] = v1806;
    v6[ostride * 3] = v1805;
    v6[ostride * 9] = v1828;
    v6[ostride * 19] = v1827;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu20(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v460 = -1.2500000000000000e+00F;
    float v465 = 5.5901699437494745e-01F;
    float v508 = -1.0000000000000000e+00F;
    float v515 = 1.2500000000000000e+00F;
    float v522 = -5.5901699437494745e-01F;
    float v529 = -1.5388417685876268e+00F;
    float v534 = -5.8778525229247325e-01F;
    float v539 = -3.6327126400268028e-01F;
    const float32x2_t *v861 = &v5[v0];
    float32x2_t *v959 = &v6[v2];
    int64_t v19 = v0 * 10;
    int64_t v34 = v0 * 5;
    int64_t v45 = v0 * 15;
    int64_t v64 = v0 * 4;
    int64_t v75 = v0 * 14;
    int64_t v94 = v0 * 9;
    int64_t v105 = v0 * 19;
    int64_t v124 = v0 * 8;
    int64_t v135 = v0 * 18;
    int64_t v154 = v0 * 13;
    int64_t v165 = v0 * 3;
    int64_t v184 = v0 * 12;
    int64_t v195 = v0 * 2;
    int64_t v214 = v0 * 17;
    int64_t v225 = v0 * 7;
    int64_t v244 = v0 * 16;
    int64_t v255 = v0 * 6;
    int64_t v285 = v0 * 11;
    float v473 = v4 * v529;
    float v480 = v4 * v534;
    float v487 = v4 * v539;
    float v511 = v4 * v508;
    float v518 = v4 * v515;
    float v525 = v4 * v522;
    int64_t v562 = v2 * 5;
    int64_t v569 = v2 * 10;
    int64_t v576 = v2 * 15;
    int64_t v585 = v2 * 16;
    int64_t v599 = v2 * 6;
    int64_t v606 = v2 * 11;
    int64_t v615 = v2 * 12;
    int64_t v622 = v2 * 17;
    int64_t v629 = v2 * 2;
    int64_t v636 = v2 * 7;
    int64_t v645 = v2 * 8;
    int64_t v652 = v2 * 13;
    int64_t v659 = v2 * 18;
    int64_t v666 = v2 * 3;
    int64_t v675 = v2 * 4;
    int64_t v682 = v2 * 9;
    int64_t v689 = v2 * 14;
    int64_t v696 = v2 * 19;
    const float32x2_t *v880 = &v5[0];
    svfloat32_t v896 = svdup_n_f32(v460);
    svfloat32_t v897 = svdup_n_f32(v465);
    svfloat32_t v904 = svdup_n_f32(v529);
    svfloat32_t v905 = svdup_n_f32(v534);
    svfloat32_t v906 = svdup_n_f32(v539);
    float32x2_t *v914 = &v6[0];
    svfloat32_t v1123 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v861)[0]));
    svfloat32_t v31 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v57 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v61 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[14]));
    svfloat32_t v87 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v91 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v117 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v121 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[18]));
    svfloat32_t v147 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v151 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[17]));
    svfloat32_t v177 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v181 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v207 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v211 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v237 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[16]));
    svfloat32_t v241 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v267 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[15]));
    svfloat32_t v271 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v297 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v301 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    const float32x2_t *v708 = &v5[v19];
    const float32x2_t *v717 = &v5[v34];
    const float32x2_t *v726 = &v5[v45];
    const float32x2_t *v735 = &v5[v64];
    const float32x2_t *v744 = &v5[v75];
    const float32x2_t *v753 = &v5[v94];
    const float32x2_t *v762 = &v5[v105];
    const float32x2_t *v771 = &v5[v124];
    const float32x2_t *v780 = &v5[v135];
    const float32x2_t *v789 = &v5[v154];
    const float32x2_t *v798 = &v5[v165];
    const float32x2_t *v807 = &v5[v184];
    const float32x2_t *v816 = &v5[v195];
    const float32x2_t *v825 = &v5[v214];
    const float32x2_t *v834 = &v5[v225];
    const float32x2_t *v843 = &v5[v244];
    const float32x2_t *v852 = &v5[v255];
    const float32x2_t *v870 = &v5[v285];
    svfloat32_t v898 = svdup_n_f32(v473);
    svfloat32_t v899 = svdup_n_f32(v480);
    svfloat32_t v900 = svdup_n_f32(v487);
    svfloat32_t v901 = svdup_n_f32(v511);
    svfloat32_t v902 = svdup_n_f32(v518);
    svfloat32_t v903 = svdup_n_f32(v525);
    float32x2_t *v923 = &v6[v562];
    float32x2_t *v932 = &v6[v569];
    float32x2_t *v941 = &v6[v576];
    float32x2_t *v950 = &v6[v585];
    float32x2_t *v968 = &v6[v599];
    float32x2_t *v977 = &v6[v606];
    float32x2_t *v986 = &v6[v615];
    float32x2_t *v995 = &v6[v622];
    float32x2_t *v1004 = &v6[v629];
    float32x2_t *v1013 = &v6[v636];
    float32x2_t *v1022 = &v6[v645];
    float32x2_t *v1031 = &v6[v652];
    float32x2_t *v1040 = &v6[v659];
    float32x2_t *v1049 = &v6[v666];
    float32x2_t *v1058 = &v6[v675];
    float32x2_t *v1067 = &v6[v682];
    float32x2_t *v1076 = &v6[v689];
    float32x2_t *v1085 = &v6[v696];
    svfloat32_t v1127 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v880)[0]));
    svfloat32_t zero298 = svdup_n_f32(0);
    svfloat32_t v298 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero298, v1123, v297, 0), v1123,
        v297, 90);
    svfloat32_t v1089 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v708)[0]));
    svfloat32_t v1091 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v717)[0]));
    svfloat32_t v1093 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v726)[0]));
    svfloat32_t v1095 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v735)[0]));
    svfloat32_t v1097 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v744)[0]));
    svfloat32_t v1099 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v753)[0]));
    svfloat32_t v1101 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v762)[0]));
    svfloat32_t v1103 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v771)[0]));
    svfloat32_t v1105 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v780)[0]));
    svfloat32_t v1107 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v789)[0]));
    svfloat32_t v1109 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v798)[0]));
    svfloat32_t v1111 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v807)[0]));
    svfloat32_t v1113 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v816)[0]));
    svfloat32_t v1115 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v825)[0]));
    svfloat32_t v1117 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v834)[0]));
    svfloat32_t v1119 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v843)[0]));
    svfloat32_t v1121 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v852)[0]));
    svfloat32_t v1125 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v870)[0]));
    svfloat32_t zero32 = svdup_n_f32(0);
    svfloat32_t v32 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero32, v1089, v31, 0),
                     v1089, v31, 90);
    svfloat32_t zero58 = svdup_n_f32(0);
    svfloat32_t v58 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero58, v1091, v57, 0),
                     v1091, v57, 90);
    svfloat32_t zero62 = svdup_n_f32(0);
    svfloat32_t v62 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero62, v1093, v61, 0),
                     v1093, v61, 90);
    svfloat32_t zero88 = svdup_n_f32(0);
    svfloat32_t v88 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero88, v1095, v87, 0),
                     v1095, v87, 90);
    svfloat32_t zero92 = svdup_n_f32(0);
    svfloat32_t v92 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero92, v1097, v91, 0),
                     v1097, v91, 90);
    svfloat32_t zero118 = svdup_n_f32(0);
    svfloat32_t v118 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero118, v1099, v117, 0), v1099,
        v117, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero122, v1101, v121, 0), v1101,
        v121, 90);
    svfloat32_t zero148 = svdup_n_f32(0);
    svfloat32_t v148 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero148, v1103, v147, 0), v1103,
        v147, 90);
    svfloat32_t zero152 = svdup_n_f32(0);
    svfloat32_t v152 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero152, v1105, v151, 0), v1105,
        v151, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero178, v1107, v177, 0), v1107,
        v177, 90);
    svfloat32_t zero182 = svdup_n_f32(0);
    svfloat32_t v182 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero182, v1109, v181, 0), v1109,
        v181, 90);
    svfloat32_t zero208 = svdup_n_f32(0);
    svfloat32_t v208 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero208, v1111, v207, 0), v1111,
        v207, 90);
    svfloat32_t zero212 = svdup_n_f32(0);
    svfloat32_t v212 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero212, v1113, v211, 0), v1113,
        v211, 90);
    svfloat32_t zero238 = svdup_n_f32(0);
    svfloat32_t v238 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero238, v1115, v237, 0), v1115,
        v237, 90);
    svfloat32_t zero242 = svdup_n_f32(0);
    svfloat32_t v242 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero242, v1117, v241, 0), v1117,
        v241, 90);
    svfloat32_t zero268 = svdup_n_f32(0);
    svfloat32_t v268 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero268, v1119, v267, 0), v1119,
        v267, 90);
    svfloat32_t zero272 = svdup_n_f32(0);
    svfloat32_t v272 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero272, v1121, v271, 0), v1121,
        v271, 90);
    svfloat32_t zero302 = svdup_n_f32(0);
    svfloat32_t v302 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero302, v1125, v301, 0), v1125,
        v301, 90);
    svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v1127, v32);
    svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v1127, v32);
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v58, v62);
    svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v58, v62);
    svfloat32_t v316 = svadd_f32_x(svptrue_b32(), v88, v92);
    svfloat32_t v317 = svsub_f32_x(svptrue_b32(), v88, v92);
    svfloat32_t v318 = svadd_f32_x(svptrue_b32(), v118, v122);
    svfloat32_t v319 = svsub_f32_x(svptrue_b32(), v118, v122);
    svfloat32_t v322 = svadd_f32_x(svptrue_b32(), v148, v152);
    svfloat32_t v323 = svsub_f32_x(svptrue_b32(), v148, v152);
    svfloat32_t v324 = svadd_f32_x(svptrue_b32(), v178, v182);
    svfloat32_t v325 = svsub_f32_x(svptrue_b32(), v178, v182);
    svfloat32_t v328 = svadd_f32_x(svptrue_b32(), v208, v212);
    svfloat32_t v329 = svsub_f32_x(svptrue_b32(), v208, v212);
    svfloat32_t v330 = svadd_f32_x(svptrue_b32(), v238, v242);
    svfloat32_t v331 = svsub_f32_x(svptrue_b32(), v238, v242);
    svfloat32_t v334 = svadd_f32_x(svptrue_b32(), v268, v272);
    svfloat32_t v335 = svsub_f32_x(svptrue_b32(), v268, v272);
    svfloat32_t v336 = svadd_f32_x(svptrue_b32(), v298, v302);
    svfloat32_t v337 = svsub_f32_x(svptrue_b32(), v298, v302);
    svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v310, v312);
    svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v310, v312);
    svfloat32_t v320 = svadd_f32_x(svptrue_b32(), v316, v318);
    svfloat32_t v321 = svsub_f32_x(svptrue_b32(), v316, v318);
    svfloat32_t v326 = svadd_f32_x(svptrue_b32(), v322, v324);
    svfloat32_t v327 = svsub_f32_x(svptrue_b32(), v322, v324);
    svfloat32_t v332 = svadd_f32_x(svptrue_b32(), v328, v330);
    svfloat32_t v333 = svsub_f32_x(svptrue_b32(), v328, v330);
    svfloat32_t v338 = svadd_f32_x(svptrue_b32(), v334, v336);
    svfloat32_t v339 = svsub_f32_x(svptrue_b32(), v334, v336);
    svfloat32_t v446 = svadd_f32_x(svptrue_b32(), v317, v335);
    svfloat32_t v447 = svsub_f32_x(svptrue_b32(), v317, v335);
    svfloat32_t v448 = svadd_f32_x(svptrue_b32(), v329, v323);
    svfloat32_t v449 = svsub_f32_x(svptrue_b32(), v329, v323);
    svfloat32_t v499 = svadd_f32_x(svptrue_b32(), v319, v337);
    svfloat32_t v500 = svsub_f32_x(svptrue_b32(), v319, v337);
    svfloat32_t v501 = svadd_f32_x(svptrue_b32(), v331, v325);
    svfloat32_t v502 = svsub_f32_x(svptrue_b32(), v331, v325);
    svfloat32_t v340 = svadd_f32_x(svptrue_b32(), v320, v338);
    svfloat32_t v341 = svsub_f32_x(svptrue_b32(), v320, v338);
    svfloat32_t v342 = svadd_f32_x(svptrue_b32(), v332, v326);
    svfloat32_t v343 = svsub_f32_x(svptrue_b32(), v332, v326);
    svfloat32_t v393 = svadd_f32_x(svptrue_b32(), v321, v339);
    svfloat32_t v394 = svsub_f32_x(svptrue_b32(), v321, v339);
    svfloat32_t v395 = svadd_f32_x(svptrue_b32(), v333, v327);
    svfloat32_t v396 = svsub_f32_x(svptrue_b32(), v333, v327);
    svfloat32_t v450 = svadd_f32_x(svptrue_b32(), v446, v448);
    svfloat32_t v451 = svsub_f32_x(svptrue_b32(), v446, v448);
    svfloat32_t v452 = svadd_f32_x(svptrue_b32(), v447, v449);
    svfloat32_t zero475 = svdup_n_f32(0);
    svfloat32_t v475 = svcmla_f32_x(pred_full, zero475, v898, v447, 90);
    svfloat32_t v503 = svadd_f32_x(svptrue_b32(), v499, v501);
    svfloat32_t v504 = svsub_f32_x(svptrue_b32(), v499, v501);
    svfloat32_t v505 = svadd_f32_x(svptrue_b32(), v500, v502);
    svfloat32_t v542 = svmul_f32_x(svptrue_b32(), v502, v906);
    svfloat32_t v344 = svadd_f32_x(svptrue_b32(), v340, v342);
    svfloat32_t v345 = svsub_f32_x(svptrue_b32(), v340, v342);
    svfloat32_t v346 = svadd_f32_x(svptrue_b32(), v341, v343);
    svfloat32_t zero369 = svdup_n_f32(0);
    svfloat32_t v369 = svcmla_f32_x(pred_full, zero369, v898, v341, 90);
    svfloat32_t v397 = svadd_f32_x(svptrue_b32(), v393, v395);
    svfloat32_t v398 = svsub_f32_x(svptrue_b32(), v393, v395);
    svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v394, v396);
    svfloat32_t zero422 = svdup_n_f32(0);
    svfloat32_t v422 = svcmla_f32_x(pred_full, zero422, v898, v394, 90);
    svfloat32_t v453 = svadd_f32_x(svptrue_b32(), v450, v311);
    svfloat32_t zero482 = svdup_n_f32(0);
    svfloat32_t v482 = svcmla_f32_x(pred_full, zero482, v899, v452, 90);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v503, v313);
    svfloat32_t zero527 = svdup_n_f32(0);
    svfloat32_t v527 = svcmla_f32_x(pred_full, zero527, v903, v504, 90);
    svfloat32_t v537 = svmul_f32_x(svptrue_b32(), v505, v905);
    svfloat32_t v347 = svadd_f32_x(svptrue_b32(), v344, v314);
    svfloat32_t zero376 = svdup_n_f32(0);
    svfloat32_t v376 = svcmla_f32_x(pred_full, zero376, v899, v346, 90);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v397, v315);
    svfloat32_t zero429 = svdup_n_f32(0);
    svfloat32_t v429 = svcmla_f32_x(pred_full, zero429, v899, v399, 90);
    svfloat32_t v490 = svmla_f32_x(pred_full, v453, v450, v896);
    svfloat32_t v493 = svsub_f32_x(svptrue_b32(), v475, v482);
    svfloat32_t v494 = svcmla_f32_x(pred_full, v482, v900, v449, 90);
    svfloat32_t zero513 = svdup_n_f32(0);
    svfloat32_t v513 = svcmla_f32_x(pred_full, zero513, v901, v506, 90);
    svfloat32_t v546 = svnmls_f32_x(pred_full, v537, v500, v904);
    svfloat32_t v547 = svmla_f32_x(pred_full, v542, v505, v905);
    svfloat32_t v384 = svmla_f32_x(pred_full, v347, v344, v896);
    svfloat32_t v387 = svsub_f32_x(svptrue_b32(), v369, v376);
    svfloat32_t v388 = svcmla_f32_x(pred_full, v376, v900, v343, 90);
    svfloat32_t v437 = svmla_f32_x(pred_full, v400, v397, v896);
    svfloat32_t v440 = svsub_f32_x(svptrue_b32(), v422, v429);
    svfloat32_t v441 = svcmla_f32_x(pred_full, v429, v900, v396, 90);
    svfloat32_t v491 = svmla_f32_x(pred_full, v490, v451, v897);
    svfloat32_t v492 = svmls_f32_x(pred_full, v490, v451, v897);
    svfloat32_t v543 = svcmla_f32_x(pred_full, v513, v902, v503, 90);
    svfloat32_t v552 = svadd_f32_x(svptrue_b32(), v453, v513);
    svfloat32_t v553 = svsub_f32_x(svptrue_b32(), v453, v513);
    svst1_f64(pred_full, (double *)(v914), svreinterpret_f64_f32(v347));
    svst1_f64(pred_full, (double *)(v932), svreinterpret_f64_f32(v400));
    svfloat32_t v385 = svmla_f32_x(pred_full, v384, v345, v897);
    svfloat32_t v386 = svmls_f32_x(pred_full, v384, v345, v897);
    svfloat32_t v438 = svmla_f32_x(pred_full, v437, v398, v897);
    svfloat32_t v439 = svmls_f32_x(pred_full, v437, v398, v897);
    svfloat32_t v495 = svadd_f32_x(svptrue_b32(), v491, v493);
    svfloat32_t v496 = svsub_f32_x(svptrue_b32(), v491, v493);
    svfloat32_t v497 = svadd_f32_x(svptrue_b32(), v492, v494);
    svfloat32_t v498 = svsub_f32_x(svptrue_b32(), v492, v494);
    svfloat32_t v544 = svadd_f32_x(svptrue_b32(), v543, v527);
    svfloat32_t v545 = svsub_f32_x(svptrue_b32(), v543, v527);
    svst1_f64(pred_full, (double *)(v923), svreinterpret_f64_f32(v553));
    svst1_f64(pred_full, (double *)(v941), svreinterpret_f64_f32(v552));
    svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v385, v387);
    svfloat32_t v390 = svsub_f32_x(svptrue_b32(), v385, v387);
    svfloat32_t v391 = svadd_f32_x(svptrue_b32(), v386, v388);
    svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v386, v388);
    svfloat32_t v442 = svadd_f32_x(svptrue_b32(), v438, v440);
    svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v438, v440);
    svfloat32_t v444 = svadd_f32_x(svptrue_b32(), v439, v441);
    svfloat32_t v445 = svsub_f32_x(svptrue_b32(), v439, v441);
    svfloat32_t v548 = svadd_f32_x(svptrue_b32(), v544, v546);
    svfloat32_t v549 = svsub_f32_x(svptrue_b32(), v544, v546);
    svfloat32_t v550 = svadd_f32_x(svptrue_b32(), v545, v547);
    svfloat32_t v551 = svsub_f32_x(svptrue_b32(), v545, v547);
    svfloat32_t v582 = svadd_f32_x(svptrue_b32(), v496, v549);
    svfloat32_t v583 = svsub_f32_x(svptrue_b32(), v496, v549);
    svfloat32_t v612 = svadd_f32_x(svptrue_b32(), v498, v551);
    svfloat32_t v613 = svsub_f32_x(svptrue_b32(), v498, v551);
    svfloat32_t v642 = svadd_f32_x(svptrue_b32(), v497, v550);
    svfloat32_t v643 = svsub_f32_x(svptrue_b32(), v497, v550);
    svfloat32_t v672 = svadd_f32_x(svptrue_b32(), v495, v548);
    svfloat32_t v673 = svsub_f32_x(svptrue_b32(), v495, v548);
    svst1_f64(pred_full, (double *)(v950), svreinterpret_f64_f32(v390));
    svst1_f64(pred_full, (double *)(v968), svreinterpret_f64_f32(v443));
    svst1_f64(pred_full, (double *)(v986), svreinterpret_f64_f32(v392));
    svst1_f64(pred_full, (double *)(v1004), svreinterpret_f64_f32(v445));
    svst1_f64(pred_full, (double *)(v1022), svreinterpret_f64_f32(v391));
    svst1_f64(pred_full, (double *)(v1040), svreinterpret_f64_f32(v444));
    svst1_f64(pred_full, (double *)(v1058), svreinterpret_f64_f32(v389));
    svst1_f64(pred_full, (double *)(v1076), svreinterpret_f64_f32(v442));
    svst1_f64(pred_full, (double *)(v959), svreinterpret_f64_f32(v583));
    svst1_f64(pred_full, (double *)(v977), svreinterpret_f64_f32(v582));
    svst1_f64(pred_full, (double *)(v995), svreinterpret_f64_f32(v613));
    svst1_f64(pred_full, (double *)(v1013), svreinterpret_f64_f32(v612));
    svst1_f64(pred_full, (double *)(v1031), svreinterpret_f64_f32(v643));
    svst1_f64(pred_full, (double *)(v1049), svreinterpret_f64_f32(v642));
    svst1_f64(pred_full, (double *)(v1067), svreinterpret_f64_f32(v673));
    svst1_f64(pred_full, (double *)(v1085), svreinterpret_f64_f32(v672));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu21(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v1047 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v620 = -1.1666666666666665e+00F;
    float v625 = 7.9015646852540022e-01F;
    float v630 = 5.5854267289647742e-02F;
    float v635 = 7.3430220123575241e-01F;
    float v639 = 4.4095855184409838e-01F;
    float v640 = -4.4095855184409838e-01F;
    float v647 = 3.4087293062393137e-01F;
    float v648 = -3.4087293062393137e-01F;
    float v655 = -5.3396936033772524e-01F;
    float v656 = 5.3396936033772524e-01F;
    float v663 = 8.7484229096165667e-01F;
    float v664 = -8.7484229096165667e-01F;
    float v708 = -1.4999999999999998e+00F;
    float v713 = 1.7499999999999996e+00F;
    float v718 = -1.1852347027881001e+00F;
    float v723 = -8.3781400934471603e-02F;
    float v728 = -1.1014533018536286e+00F;
    float v732 = -6.6143782776614746e-01F;
    float v733 = 6.6143782776614746e-01F;
    float v740 = -5.1130939593589697e-01F;
    float v741 = 5.1130939593589697e-01F;
    float v748 = 8.0095404050658769e-01F;
    float v749 = -8.0095404050658769e-01F;
    float v756 = -1.3122634364424848e+00F;
    float v757 = 1.3122634364424848e+00F;
    float v800 = 8.6602540378443871e-01F;
    float v801 = -8.6602540378443871e-01F;
    float v808 = -1.0103629710818451e+00F;
    float v809 = 1.0103629710818451e+00F;
    float v816 = 6.8429557470759583e-01F;
    float v817 = -6.8429557470759583e-01F;
    float v824 = 4.8371214382601155e-02F;
    float v825 = -4.8371214382601155e-02F;
    float v832 = 6.3592436032499466e-01F;
    float v833 = -6.3592436032499466e-01F;
    float32x2_t v835 = (float32x2_t){v4, v4};
    float v841 = -3.8188130791298663e-01F;
    float v846 = -2.9520461738277515e-01F;
    float v851 = 4.6243103089499693e-01F;
    float v856 = -7.5763564827777208e-01F;
    const float32x2_t *v2062 = &v5[istride];
    float32x2_t *v2171 = &v6[ostride];
    float32x2_t v621 = (float32x2_t){v620, v620};
    float32x2_t v626 = (float32x2_t){v625, v625};
    float32x2_t v631 = (float32x2_t){v630, v630};
    float32x2_t v636 = (float32x2_t){v635, v635};
    float32x2_t v641 = (float32x2_t){v639, v640};
    float32x2_t v649 = (float32x2_t){v647, v648};
    float32x2_t v657 = (float32x2_t){v655, v656};
    float32x2_t v665 = (float32x2_t){v663, v664};
    float32x2_t v709 = (float32x2_t){v708, v708};
    float32x2_t v714 = (float32x2_t){v713, v713};
    float32x2_t v719 = (float32x2_t){v718, v718};
    float32x2_t v724 = (float32x2_t){v723, v723};
    float32x2_t v729 = (float32x2_t){v728, v728};
    float32x2_t v734 = (float32x2_t){v732, v733};
    float32x2_t v742 = (float32x2_t){v740, v741};
    float32x2_t v750 = (float32x2_t){v748, v749};
    float32x2_t v758 = (float32x2_t){v756, v757};
    float32x2_t v802 = (float32x2_t){v800, v801};
    float32x2_t v810 = (float32x2_t){v808, v809};
    float32x2_t v818 = (float32x2_t){v816, v817};
    float32x2_t v826 = (float32x2_t){v824, v825};
    float32x2_t v834 = (float32x2_t){v832, v833};
    float32x2_t v842 = (float32x2_t){v841, v841};
    float32x2_t v847 = (float32x2_t){v846, v846};
    float32x2_t v852 = (float32x2_t){v851, v851};
    float32x2_t v857 = (float32x2_t){v856, v856};
    const float32x2_t *v2125 = &v5[0];
    float32x2_t *v2135 = &v6[0];
    float32x4_t v2347 = vld1q_f32((const float32_t *)v2062);
    float32x4_t v66 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v68 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v78 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v80 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v128 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v130 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v140 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[32]));
    float32x4_t v142 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[33]));
    float32x4_t v159 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v161 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v209 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v211 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v221 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[38]));
    float32x4_t v223 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[39]));
    float32x4_t v240 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v242 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v290 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[30]));
    float32x4_t v292 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[31]));
    float32x4_t v302 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v304 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v321 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v323 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v371 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[36]));
    float32x4_t v373 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[37]));
    float32x4_t v383 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v385 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v402 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v404 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v447 = vtrn1q_f32(v2347, v2347);
    float32x4_t v448 = vtrn2q_f32(v2347, v2347);
    float32x4_t v452 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v454 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v464 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v466 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v483 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[28]));
    float32x4_t v485 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[29]));
    float32x4_t v533 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v535 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v545 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v547 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v564 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[34]));
    float32x4_t v566 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[35]));
    float32x4_t v622 = vcombine_f32(v621, v621);
    float32x4_t v627 = vcombine_f32(v626, v626);
    float32x4_t v632 = vcombine_f32(v631, v631);
    float32x4_t v637 = vcombine_f32(v636, v636);
    float32x2_t v643 = vmul_f32(v835, v641);
    float32x2_t v651 = vmul_f32(v835, v649);
    float32x2_t v659 = vmul_f32(v835, v657);
    float32x2_t v667 = vmul_f32(v835, v665);
    float32x4_t v710 = vcombine_f32(v709, v709);
    float32x4_t v715 = vcombine_f32(v714, v714);
    float32x4_t v720 = vcombine_f32(v719, v719);
    float32x4_t v725 = vcombine_f32(v724, v724);
    float32x4_t v730 = vcombine_f32(v729, v729);
    float32x2_t v736 = vmul_f32(v835, v734);
    float32x2_t v744 = vmul_f32(v835, v742);
    float32x2_t v752 = vmul_f32(v835, v750);
    float32x2_t v760 = vmul_f32(v835, v758);
    float32x2_t v804 = vmul_f32(v835, v802);
    float32x2_t v812 = vmul_f32(v835, v810);
    float32x2_t v820 = vmul_f32(v835, v818);
    float32x2_t v828 = vmul_f32(v835, v826);
    float32x2_t v836 = vmul_f32(v835, v834);
    float32x4_t v843 = vcombine_f32(v842, v842);
    float32x4_t v848 = vcombine_f32(v847, v847);
    float32x4_t v853 = vcombine_f32(v852, v852);
    float32x4_t v858 = vcombine_f32(v857, v857);
    const float32x2_t *v1912 = &v5[istride * 7];
    const float32x2_t *v1922 = &v5[istride * 14];
    const float32x2_t *v1934 = &v5[istride * 10];
    const float32x2_t *v1944 = &v5[istride * 17];
    const float32x2_t *v1956 = &v5[istride * 3];
    const float32x2_t *v1966 = &v5[istride * 13];
    const float32x2_t *v1976 = &v5[istride * 20];
    const float32x2_t *v1988 = &v5[istride * 6];
    const float32x2_t *v1998 = &v5[istride * 16];
    const float32x2_t *v2008 = &v5[istride * 2];
    const float32x2_t *v2020 = &v5[istride * 9];
    const float32x2_t *v2030 = &v5[istride * 19];
    const float32x2_t *v2040 = &v5[istride * 5];
    const float32x2_t *v2052 = &v5[istride * 12];
    const float32x2_t *v2071 = &v5[istride * 8];
    const float32x2_t *v2082 = &v5[istride * 15];
    const float32x2_t *v2092 = &v5[istride * 4];
    const float32x2_t *v2102 = &v5[istride * 11];
    const float32x2_t *v2114 = &v5[istride * 18];
    float32x2_t *v2144 = &v6[ostride * 7];
    float32x2_t *v2153 = &v6[ostride * 14];
    float32x2_t *v2162 = &v6[ostride * 15];
    float32x2_t *v2180 = &v6[ostride * 8];
    float32x2_t *v2189 = &v6[ostride * 9];
    float32x2_t *v2198 = &v6[ostride * 16];
    float32x2_t *v2207 = &v6[ostride * 2];
    float32x2_t *v2216 = &v6[ostride * 3];
    float32x2_t *v2225 = &v6[ostride * 10];
    float32x2_t *v2234 = &v6[ostride * 17];
    float32x2_t *v2243 = &v6[ostride * 18];
    float32x2_t *v2252 = &v6[ostride * 4];
    float32x2_t *v2261 = &v6[ostride * 11];
    float32x2_t *v2270 = &v6[ostride * 12];
    float32x2_t *v2279 = &v6[ostride * 19];
    float32x2_t *v2288 = &v6[ostride * 5];
    float32x2_t *v2297 = &v6[ostride * 6];
    float32x2_t *v2306 = &v6[ostride * 13];
    float32x2_t *v2315 = &v6[ostride * 20];
    float32x4_t v2359 = vld1q_f32((const float32_t *)v2125);
    float32x4_t v453 = vmulq_f32(v447, v452);
    float32x4_t v645 = vcombine_f32(v643, v643);
    float32x4_t v653 = vcombine_f32(v651, v651);
    float32x4_t v661 = vcombine_f32(v659, v659);
    float32x4_t v669 = vcombine_f32(v667, v667);
    float32x4_t v738 = vcombine_f32(v736, v736);
    float32x4_t v746 = vcombine_f32(v744, v744);
    float32x4_t v754 = vcombine_f32(v752, v752);
    float32x4_t v762 = vcombine_f32(v760, v760);
    float32x4_t v806 = vcombine_f32(v804, v804);
    float32x4_t v814 = vcombine_f32(v812, v812);
    float32x4_t v822 = vcombine_f32(v820, v820);
    float32x4_t v830 = vcombine_f32(v828, v828);
    float32x4_t v838 = vcombine_f32(v836, v836);
    float32x4_t v2319 = vld1q_f32((const float32_t *)v1912);
    float32x4_t v2321 = vld1q_f32((const float32_t *)v1922);
    float32x4_t v2323 = vld1q_f32((const float32_t *)v1934);
    float32x4_t v2325 = vld1q_f32((const float32_t *)v1944);
    float32x4_t v2327 = vld1q_f32((const float32_t *)v1956);
    float32x4_t v2329 = vld1q_f32((const float32_t *)v1966);
    float32x4_t v2331 = vld1q_f32((const float32_t *)v1976);
    float32x4_t v2333 = vld1q_f32((const float32_t *)v1988);
    float32x4_t v2335 = vld1q_f32((const float32_t *)v1998);
    float32x4_t v2337 = vld1q_f32((const float32_t *)v2008);
    float32x4_t v2339 = vld1q_f32((const float32_t *)v2020);
    float32x4_t v2341 = vld1q_f32((const float32_t *)v2030);
    float32x4_t v2343 = vld1q_f32((const float32_t *)v2040);
    float32x4_t v2345 = vld1q_f32((const float32_t *)v2052);
    float32x4_t v2349 = vld1q_f32((const float32_t *)v2071);
    float32x4_t v2351 = vld1q_f32((const float32_t *)v2082);
    float32x4_t v2353 = vld1q_f32((const float32_t *)v2092);
    float32x4_t v2355 = vld1q_f32((const float32_t *)v2102);
    float32x4_t v2357 = vld1q_f32((const float32_t *)v2114);
    float32x4_t v61 = vtrn1q_f32(v2319, v2319);
    float32x4_t v62 = vtrn2q_f32(v2319, v2319);
    float32x4_t v73 = vtrn1q_f32(v2321, v2321);
    float32x4_t v74 = vtrn2q_f32(v2321, v2321);
    float32x4_t v123 = vtrn1q_f32(v2323, v2323);
    float32x4_t v124 = vtrn2q_f32(v2323, v2323);
    float32x4_t v135 = vtrn1q_f32(v2325, v2325);
    float32x4_t v136 = vtrn2q_f32(v2325, v2325);
    float32x4_t v154 = vtrn1q_f32(v2327, v2327);
    float32x4_t v155 = vtrn2q_f32(v2327, v2327);
    float32x4_t v204 = vtrn1q_f32(v2329, v2329);
    float32x4_t v205 = vtrn2q_f32(v2329, v2329);
    float32x4_t v216 = vtrn1q_f32(v2331, v2331);
    float32x4_t v217 = vtrn2q_f32(v2331, v2331);
    float32x4_t v235 = vtrn1q_f32(v2333, v2333);
    float32x4_t v236 = vtrn2q_f32(v2333, v2333);
    float32x4_t v285 = vtrn1q_f32(v2335, v2335);
    float32x4_t v286 = vtrn2q_f32(v2335, v2335);
    float32x4_t v297 = vtrn1q_f32(v2337, v2337);
    float32x4_t v298 = vtrn2q_f32(v2337, v2337);
    float32x4_t v316 = vtrn1q_f32(v2339, v2339);
    float32x4_t v317 = vtrn2q_f32(v2339, v2339);
    float32x4_t v366 = vtrn1q_f32(v2341, v2341);
    float32x4_t v367 = vtrn2q_f32(v2341, v2341);
    float32x4_t v378 = vtrn1q_f32(v2343, v2343);
    float32x4_t v379 = vtrn2q_f32(v2343, v2343);
    float32x4_t v397 = vtrn1q_f32(v2345, v2345);
    float32x4_t v398 = vtrn2q_f32(v2345, v2345);
    float32x4_t v456 = vfmaq_f32(v453, v448, v454);
    float32x4_t v459 = vtrn1q_f32(v2349, v2349);
    float32x4_t v460 = vtrn2q_f32(v2349, v2349);
    float32x4_t v478 = vtrn1q_f32(v2351, v2351);
    float32x4_t v479 = vtrn2q_f32(v2351, v2351);
    float32x4_t v528 = vtrn1q_f32(v2353, v2353);
    float32x4_t v529 = vtrn2q_f32(v2353, v2353);
    float32x4_t v540 = vtrn1q_f32(v2355, v2355);
    float32x4_t v541 = vtrn2q_f32(v2355, v2355);
    float32x4_t v559 = vtrn1q_f32(v2357, v2357);
    float32x4_t v560 = vtrn2q_f32(v2357, v2357);
    float32x4_t v67 = vmulq_f32(v61, v66);
    float32x4_t v79 = vmulq_f32(v73, v78);
    float32x4_t v129 = vmulq_f32(v123, v128);
    float32x4_t v141 = vmulq_f32(v135, v140);
    float32x4_t v160 = vmulq_f32(v154, v159);
    float32x4_t v210 = vmulq_f32(v204, v209);
    float32x4_t v222 = vmulq_f32(v216, v221);
    float32x4_t v241 = vmulq_f32(v235, v240);
    float32x4_t v291 = vmulq_f32(v285, v290);
    float32x4_t v303 = vmulq_f32(v297, v302);
    float32x4_t v322 = vmulq_f32(v316, v321);
    float32x4_t v372 = vmulq_f32(v366, v371);
    float32x4_t v384 = vmulq_f32(v378, v383);
    float32x4_t v403 = vmulq_f32(v397, v402);
    float32x4_t v465 = vmulq_f32(v459, v464);
    float32x4_t v484 = vmulq_f32(v478, v483);
    float32x4_t v534 = vmulq_f32(v528, v533);
    float32x4_t v546 = vmulq_f32(v540, v545);
    float32x4_t v565 = vmulq_f32(v559, v564);
    float32x4_t v70 = vfmaq_f32(v67, v62, v68);
    float32x4_t v82 = vfmaq_f32(v79, v74, v80);
    float32x4_t v132 = vfmaq_f32(v129, v124, v130);
    float32x4_t v144 = vfmaq_f32(v141, v136, v142);
    float32x4_t v163 = vfmaq_f32(v160, v155, v161);
    float32x4_t v213 = vfmaq_f32(v210, v205, v211);
    float32x4_t v225 = vfmaq_f32(v222, v217, v223);
    float32x4_t v244 = vfmaq_f32(v241, v236, v242);
    float32x4_t v294 = vfmaq_f32(v291, v286, v292);
    float32x4_t v306 = vfmaq_f32(v303, v298, v304);
    float32x4_t v325 = vfmaq_f32(v322, v317, v323);
    float32x4_t v375 = vfmaq_f32(v372, v367, v373);
    float32x4_t v387 = vfmaq_f32(v384, v379, v385);
    float32x4_t v406 = vfmaq_f32(v403, v398, v404);
    float32x4_t v468 = vfmaq_f32(v465, v460, v466);
    float32x4_t v487 = vfmaq_f32(v484, v479, v485);
    float32x4_t v537 = vfmaq_f32(v534, v529, v535);
    float32x4_t v549 = vfmaq_f32(v546, v541, v547);
    float32x4_t v568 = vfmaq_f32(v565, v560, v566);
    float32x4_t v569 = vaddq_f32(v70, v82);
    float32x4_t v570 = vsubq_f32(v70, v82);
    float32x4_t v579 = vaddq_f32(v132, v144);
    float32x4_t v580 = vsubq_f32(v132, v144);
    float32x4_t v582 = vaddq_f32(v213, v225);
    float32x4_t v583 = vsubq_f32(v213, v225);
    float32x4_t v585 = vaddq_f32(v294, v306);
    float32x4_t v586 = vsubq_f32(v294, v306);
    float32x4_t v588 = vaddq_f32(v375, v387);
    float32x4_t v589 = vsubq_f32(v375, v387);
    float32x4_t v591 = vaddq_f32(v456, v468);
    float32x4_t v592 = vsubq_f32(v456, v468);
    float32x4_t v594 = vaddq_f32(v537, v549);
    float32x4_t v595 = vsubq_f32(v537, v549);
    float32x4_t v578 = vaddq_f32(v569, v2359);
    float32x4_t v581 = vaddq_f32(v579, v163);
    float32x4_t v584 = vaddq_f32(v582, v244);
    float32x4_t v587 = vaddq_f32(v585, v325);
    float32x4_t v590 = vaddq_f32(v588, v406);
    float32x4_t v593 = vaddq_f32(v591, v487);
    float32x4_t v596 = vaddq_f32(v594, v568);
    float32x4_t v690 = vaddq_f32(v579, v594);
    float32x4_t v691 = vsubq_f32(v579, v594);
    float32x4_t v692 = vaddq_f32(v588, v585);
    float32x4_t v693 = vsubq_f32(v588, v585);
    float32x4_t v694 = vaddq_f32(v582, v591);
    float32x4_t v695 = vsubq_f32(v582, v591);
    float32x4_t v783 = vaddq_f32(v580, v595);
    float32x4_t v784 = vsubq_f32(v580, v595);
    float32x4_t v785 = vaddq_f32(v589, v586);
    float32x4_t v786 = vsubq_f32(v589, v586);
    float32x4_t v787 = vaddq_f32(v583, v592);
    float32x4_t v788 = vsubq_f32(v583, v592);
    float32x4_t v597 = vaddq_f32(v581, v596);
    float32x4_t v598 = vsubq_f32(v581, v596);
    float32x4_t v599 = vaddq_f32(v590, v587);
    float32x4_t v600 = vsubq_f32(v590, v587);
    float32x4_t v601 = vaddq_f32(v584, v593);
    float32x4_t v602 = vsubq_f32(v584, v593);
    float32x4_t v696 = vaddq_f32(v690, v692);
    float32x4_t v699 = vsubq_f32(v690, v692);
    float32x4_t v700 = vsubq_f32(v692, v694);
    float32x4_t v701 = vsubq_f32(v694, v690);
    float32x4_t v702 = vaddq_f32(v691, v693);
    float32x4_t v704 = vsubq_f32(v691, v693);
    float32x4_t v705 = vsubq_f32(v693, v695);
    float32x4_t v706 = vsubq_f32(v695, v691);
    float32x4_t v789 = vaddq_f32(v783, v785);
    float32x4_t v792 = vsubq_f32(v783, v785);
    float32x4_t v793 = vsubq_f32(v785, v787);
    float32x4_t v794 = vsubq_f32(v787, v783);
    float32x4_t v795 = vaddq_f32(v784, v786);
    float32x4_t v797 = vsubq_f32(v784, v786);
    float32x4_t v798 = vsubq_f32(v786, v788);
    float32x4_t v799 = vsubq_f32(v788, v784);
    float32x4_t v603 = vaddq_f32(v597, v599);
    float32x4_t v606 = vsubq_f32(v597, v599);
    float32x4_t v607 = vsubq_f32(v599, v601);
    float32x4_t v608 = vsubq_f32(v601, v597);
    float32x4_t v609 = vaddq_f32(v598, v600);
    float32x4_t v611 = vsubq_f32(v598, v600);
    float32x4_t v612 = vsubq_f32(v600, v602);
    float32x4_t v613 = vsubq_f32(v602, v598);
    float32x4_t v697 = vaddq_f32(v696, v694);
    float32x4_t v703 = vaddq_f32(v702, v695);
    float32x4_t v721 = vmulq_f32(v699, v720);
    float32x4_t v726 = vmulq_f32(v700, v725);
    float32x4_t v731 = vmulq_f32(v701, v730);
    float32x4_t v745 = vrev64q_f32(v704);
    float32x4_t v753 = vrev64q_f32(v705);
    float32x4_t v761 = vrev64q_f32(v706);
    float32x4_t v790 = vaddq_f32(v789, v787);
    float32x4_t v796 = vaddq_f32(v795, v788);
    float32x4_t v821 = vrev64q_f32(v792);
    float32x4_t v829 = vrev64q_f32(v793);
    float32x4_t v837 = vrev64q_f32(v794);
    float32x4_t v849 = vmulq_f32(v797, v848);
    float32x4_t v854 = vmulq_f32(v798, v853);
    float32x4_t v859 = vmulq_f32(v799, v858);
    float32x4_t v604 = vaddq_f32(v603, v601);
    float32x4_t v610 = vaddq_f32(v609, v602);
    float32x4_t v628 = vmulq_f32(v606, v627);
    float32x4_t v633 = vmulq_f32(v607, v632);
    float32x4_t v638 = vmulq_f32(v608, v637);
    float32x4_t v652 = vrev64q_f32(v611);
    float32x4_t v660 = vrev64q_f32(v612);
    float32x4_t v668 = vrev64q_f32(v613);
    float32x4_t v698 = vaddq_f32(v697, v569);
    float32x4_t v716 = vmulq_f32(v697, v715);
    float32x4_t v737 = vrev64q_f32(v703);
    float32x4_t v747 = vmulq_f32(v745, v746);
    float32x4_t v755 = vmulq_f32(v753, v754);
    float32x4_t v763 = vmulq_f32(v761, v762);
    float32x4_t v791 = vaddq_f32(v790, v570);
    float32x4_t v813 = vrev64q_f32(v790);
    float32x4_t v823 = vmulq_f32(v821, v822);
    float32x4_t v831 = vmulq_f32(v829, v830);
    float32x4_t v839 = vmulq_f32(v837, v838);
    float32x4_t v844 = vmulq_f32(v796, v843);
    float32x4_t v605 = vaddq_f32(v604, v578);
    float32x4_t v623 = vmulq_f32(v604, v622);
    float32x4_t v644 = vrev64q_f32(v610);
    float32x4_t v654 = vmulq_f32(v652, v653);
    float32x4_t v662 = vmulq_f32(v660, v661);
    float32x4_t v670 = vmulq_f32(v668, v669);
    float32x4_t v711 = vmulq_f32(v698, v710);
    float32x4_t v739 = vmulq_f32(v737, v738);
    float32x4_t v805 = vrev64q_f32(v791);
    float32x4_t v815 = vmulq_f32(v813, v814);
    float32x4_t v867 = vaddq_f32(v844, v849);
    float32x4_t v869 = vsubq_f32(v844, v849);
    float32x4_t v871 = vsubq_f32(v844, v854);
    float32x4_t v646 = vmulq_f32(v644, v645);
    float32x4_t v671 = vaddq_f32(v605, v623);
    float32x4_t v764 = vaddq_f32(v711, v716);
    float32x4_t v771 = vaddq_f32(v739, v747);
    float32x4_t v773 = vsubq_f32(v739, v747);
    float32x4_t v775 = vsubq_f32(v739, v755);
    float32x4_t v807 = vmulq_f32(v805, v806);
    float32x4_t v868 = vaddq_f32(v867, v854);
    float32x4_t v870 = vsubq_f32(v869, v859);
    float32x4_t v872 = vaddq_f32(v871, v859);
    float32x4_t v879 = vaddq_f32(v605, v711);
    vst1q_f32((float32_t *)v2135, v605);
    float32x4_t v672 = vaddq_f32(v671, v628);
    float32x4_t v674 = vsubq_f32(v671, v628);
    float32x4_t v676 = vsubq_f32(v671, v633);
    float32x4_t v678 = vaddq_f32(v646, v654);
    float32x4_t v680 = vsubq_f32(v646, v654);
    float32x4_t v682 = vsubq_f32(v646, v662);
    float32x4_t v765 = vaddq_f32(v764, v721);
    float32x4_t v767 = vsubq_f32(v764, v721);
    float32x4_t v769 = vsubq_f32(v764, v726);
    float32x4_t v772 = vaddq_f32(v771, v755);
    float32x4_t v774 = vsubq_f32(v773, v763);
    float32x4_t v776 = vaddq_f32(v775, v763);
    float32x4_t v860 = vaddq_f32(v807, v815);
    float32x4_t v880 = vaddq_f32(v879, v807);
    float32x4_t v881 = vsubq_f32(v879, v807);
    float32x4_t v673 = vaddq_f32(v672, v633);
    float32x4_t v675 = vsubq_f32(v674, v638);
    float32x4_t v677 = vaddq_f32(v676, v638);
    float32x4_t v679 = vaddq_f32(v678, v662);
    float32x4_t v681 = vsubq_f32(v680, v670);
    float32x4_t v683 = vaddq_f32(v682, v670);
    float32x4_t v766 = vaddq_f32(v765, v726);
    float32x4_t v768 = vsubq_f32(v767, v731);
    float32x4_t v770 = vaddq_f32(v769, v731);
    float32x4_t v861 = vaddq_f32(v860, v823);
    float32x4_t v863 = vsubq_f32(v860, v823);
    float32x4_t v865 = vsubq_f32(v860, v831);
    vst1q_f32((float32_t *)v2144, v881);
    vst1q_f32((float32_t *)v2153, v880);
    float32x4_t v684 = vaddq_f32(v673, v679);
    float32x4_t v685 = vsubq_f32(v673, v679);
    float32x4_t v686 = vaddq_f32(v675, v681);
    float32x4_t v687 = vsubq_f32(v675, v681);
    float32x4_t v688 = vaddq_f32(v677, v683);
    float32x4_t v689 = vsubq_f32(v677, v683);
    float32x4_t v777 = vaddq_f32(v766, v772);
    float32x4_t v778 = vsubq_f32(v766, v772);
    float32x4_t v779 = vaddq_f32(v768, v774);
    float32x4_t v780 = vsubq_f32(v768, v774);
    float32x4_t v781 = vaddq_f32(v770, v776);
    float32x4_t v782 = vsubq_f32(v770, v776);
    float32x4_t v862 = vaddq_f32(v861, v831);
    float32x4_t v864 = vsubq_f32(v863, v839);
    float32x4_t v866 = vaddq_f32(v865, v839);
    float32x4_t v873 = vaddq_f32(v862, v868);
    float32x4_t v874 = vsubq_f32(v862, v868);
    float32x4_t v875 = vaddq_f32(v864, v870);
    float32x4_t v876 = vsubq_f32(v864, v870);
    float32x4_t v877 = vaddq_f32(v866, v872);
    float32x4_t v878 = vsubq_f32(v866, v872);
    float32x4_t v903 = vaddq_f32(v685, v778);
    float32x4_t v927 = vaddq_f32(v687, v780);
    float32x4_t v951 = vaddq_f32(v688, v781);
    float32x4_t v975 = vaddq_f32(v689, v782);
    float32x4_t v999 = vaddq_f32(v686, v779);
    float32x4_t v1023 = vaddq_f32(v684, v777);
    vst1q_f32((float32_t *)v2162, v685);
    vst1q_f32((float32_t *)v2189, v687);
    vst1q_f32((float32_t *)v2216, v688);
    vst1q_f32((float32_t *)v2243, v689);
    vst1q_f32((float32_t *)v2270, v686);
    vst1q_f32((float32_t *)v2297, v684);
    float32x4_t v904 = vaddq_f32(v903, v874);
    float32x4_t v905 = vsubq_f32(v903, v874);
    float32x4_t v928 = vaddq_f32(v927, v876);
    float32x4_t v929 = vsubq_f32(v927, v876);
    float32x4_t v952 = vaddq_f32(v951, v877);
    float32x4_t v953 = vsubq_f32(v951, v877);
    float32x4_t v976 = vaddq_f32(v975, v878);
    float32x4_t v977 = vsubq_f32(v975, v878);
    float32x4_t v1000 = vaddq_f32(v999, v875);
    float32x4_t v1001 = vsubq_f32(v999, v875);
    float32x4_t v1024 = vaddq_f32(v1023, v873);
    float32x4_t v1025 = vsubq_f32(v1023, v873);
    vst1q_f32((float32_t *)v2171, v905);
    vst1q_f32((float32_t *)v2180, v904);
    vst1q_f32((float32_t *)v2198, v929);
    vst1q_f32((float32_t *)v2207, v928);
    vst1q_f32((float32_t *)v2225, v953);
    vst1q_f32((float32_t *)v2234, v952);
    vst1q_f32((float32_t *)v2252, v977);
    vst1q_f32((float32_t *)v2261, v976);
    vst1q_f32((float32_t *)v2279, v1001);
    vst1q_f32((float32_t *)v2288, v1000);
    vst1q_f32((float32_t *)v2306, v1025);
    vst1q_f32((float32_t *)v2315, v1024);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v1047 * 2; j < howmany; j += 1) {
    float32x2_t v1369 = v5[istride];
    float v1543 = -1.1666666666666665e+00F;
    float v1547 = 7.9015646852540022e-01F;
    float v1551 = 5.5854267289647742e-02F;
    float v1555 = 7.3430220123575241e-01F;
    float v1558 = 4.4095855184409838e-01F;
    float v1559 = -4.4095855184409838e-01F;
    float v1565 = 3.4087293062393137e-01F;
    float v1566 = -3.4087293062393137e-01F;
    float v1572 = -5.3396936033772524e-01F;
    float v1573 = 5.3396936033772524e-01F;
    float v1579 = 8.7484229096165667e-01F;
    float v1580 = -8.7484229096165667e-01F;
    float v1623 = -1.4999999999999998e+00F;
    float v1627 = 1.7499999999999996e+00F;
    float v1631 = -1.1852347027881001e+00F;
    float v1635 = -8.3781400934471603e-02F;
    float v1639 = -1.1014533018536286e+00F;
    float v1642 = -6.6143782776614746e-01F;
    float v1643 = 6.6143782776614746e-01F;
    float v1649 = -5.1130939593589697e-01F;
    float v1650 = 5.1130939593589697e-01F;
    float v1656 = 8.0095404050658769e-01F;
    float v1657 = -8.0095404050658769e-01F;
    float v1663 = -1.3122634364424848e+00F;
    float v1664 = 1.3122634364424848e+00F;
    float v1706 = 8.6602540378443871e-01F;
    float v1707 = -8.6602540378443871e-01F;
    float v1713 = -1.0103629710818451e+00F;
    float v1714 = 1.0103629710818451e+00F;
    float v1720 = 6.8429557470759583e-01F;
    float v1721 = -6.8429557470759583e-01F;
    float v1727 = 4.8371214382601155e-02F;
    float v1728 = -4.8371214382601155e-02F;
    float v1734 = 6.3592436032499466e-01F;
    float v1735 = -6.3592436032499466e-01F;
    float32x2_t v1737 = (float32x2_t){v4, v4};
    float v1742 = -3.8188130791298663e-01F;
    float v1746 = -2.9520461738277515e-01F;
    float v1750 = 4.6243103089499693e-01F;
    float v1754 = -7.5763564827777208e-01F;
    float32x2_t v1086 = v7[12];
    float32x2_t v1091 = v7[13];
    float32x2_t v1096 = v7[26];
    float32x2_t v1101 = v7[27];
    float32x2_t v1136 = v7[18];
    float32x2_t v1141 = v7[19];
    float32x2_t v1146 = v7[32];
    float32x2_t v1151 = v7[33];
    float32x2_t v1161 = v7[4];
    float32x2_t v1166 = v7[5];
    float32x2_t v1201 = v7[24];
    float32x2_t v1206 = v7[25];
    float32x2_t v1211 = v7[38];
    float32x2_t v1216 = v7[39];
    float32x2_t v1226 = v7[10];
    float32x2_t v1231 = v7[11];
    float32x2_t v1266 = v7[30];
    float32x2_t v1271 = v7[31];
    float32x2_t v1276 = v7[2];
    float32x2_t v1281 = v7[3];
    float32x2_t v1291 = v7[16];
    float32x2_t v1296 = v7[17];
    float32x2_t v1331 = v7[36];
    float32x2_t v1336 = v7[37];
    float32x2_t v1341 = v7[8];
    float32x2_t v1346 = v7[9];
    float32x2_t v1356 = v7[22];
    float32x2_t v1361 = v7[23];
    float32x2_t v1396 = v7[0];
    float32x2_t v1397 = vtrn1_f32(v1369, v1369);
    float32x2_t v1398 = vtrn2_f32(v1369, v1369);
    float32x2_t v1401 = v7[1];
    float32x2_t v1406 = v7[14];
    float32x2_t v1411 = v7[15];
    float32x2_t v1421 = v7[28];
    float32x2_t v1426 = v7[29];
    float32x2_t v1461 = v7[6];
    float32x2_t v1466 = v7[7];
    float32x2_t v1471 = v7[20];
    float32x2_t v1476 = v7[21];
    float32x2_t v1486 = v7[34];
    float32x2_t v1491 = v7[35];
    float32x2_t v1501 = v5[0];
    float32x2_t v1544 = (float32x2_t){v1543, v1543};
    float32x2_t v1548 = (float32x2_t){v1547, v1547};
    float32x2_t v1552 = (float32x2_t){v1551, v1551};
    float32x2_t v1556 = (float32x2_t){v1555, v1555};
    float32x2_t v1560 = (float32x2_t){v1558, v1559};
    float32x2_t v1567 = (float32x2_t){v1565, v1566};
    float32x2_t v1574 = (float32x2_t){v1572, v1573};
    float32x2_t v1581 = (float32x2_t){v1579, v1580};
    float32x2_t v1624 = (float32x2_t){v1623, v1623};
    float32x2_t v1628 = (float32x2_t){v1627, v1627};
    float32x2_t v1632 = (float32x2_t){v1631, v1631};
    float32x2_t v1636 = (float32x2_t){v1635, v1635};
    float32x2_t v1640 = (float32x2_t){v1639, v1639};
    float32x2_t v1644 = (float32x2_t){v1642, v1643};
    float32x2_t v1651 = (float32x2_t){v1649, v1650};
    float32x2_t v1658 = (float32x2_t){v1656, v1657};
    float32x2_t v1665 = (float32x2_t){v1663, v1664};
    float32x2_t v1708 = (float32x2_t){v1706, v1707};
    float32x2_t v1715 = (float32x2_t){v1713, v1714};
    float32x2_t v1722 = (float32x2_t){v1720, v1721};
    float32x2_t v1729 = (float32x2_t){v1727, v1728};
    float32x2_t v1736 = (float32x2_t){v1734, v1735};
    float32x2_t v1743 = (float32x2_t){v1742, v1742};
    float32x2_t v1747 = (float32x2_t){v1746, v1746};
    float32x2_t v1751 = (float32x2_t){v1750, v1750};
    float32x2_t v1755 = (float32x2_t){v1754, v1754};
    float32x2_t v1059 = v5[istride * 7];
    float32x2_t v1074 = v5[istride * 14];
    float32x2_t v1109 = v5[istride * 10];
    float32x2_t v1124 = v5[istride * 17];
    float32x2_t v1159 = v5[istride * 3];
    float32x2_t v1174 = v5[istride * 13];
    float32x2_t v1189 = v5[istride * 20];
    float32x2_t v1224 = v5[istride * 6];
    float32x2_t v1239 = v5[istride * 16];
    float32x2_t v1254 = v5[istride * 2];
    float32x2_t v1289 = v5[istride * 9];
    float32x2_t v1304 = v5[istride * 19];
    float32x2_t v1319 = v5[istride * 5];
    float32x2_t v1354 = v5[istride * 12];
    float32x2_t v1384 = v5[istride * 8];
    float32x2_t v1402 = vmul_f32(v1397, v1396);
    float32x2_t v1419 = v5[istride * 15];
    float32x2_t v1434 = v5[istride * 4];
    float32x2_t v1449 = v5[istride * 11];
    float32x2_t v1484 = v5[istride * 18];
    float32x2_t v1562 = vmul_f32(v1737, v1560);
    float32x2_t v1569 = vmul_f32(v1737, v1567);
    float32x2_t v1576 = vmul_f32(v1737, v1574);
    float32x2_t v1583 = vmul_f32(v1737, v1581);
    float32x2_t v1646 = vmul_f32(v1737, v1644);
    float32x2_t v1653 = vmul_f32(v1737, v1651);
    float32x2_t v1660 = vmul_f32(v1737, v1658);
    float32x2_t v1667 = vmul_f32(v1737, v1665);
    float32x2_t v1710 = vmul_f32(v1737, v1708);
    float32x2_t v1717 = vmul_f32(v1737, v1715);
    float32x2_t v1724 = vmul_f32(v1737, v1722);
    float32x2_t v1731 = vmul_f32(v1737, v1729);
    float32x2_t v1738 = vmul_f32(v1737, v1736);
    float32x2_t v1087 = vtrn1_f32(v1059, v1059);
    float32x2_t v1088 = vtrn2_f32(v1059, v1059);
    float32x2_t v1097 = vtrn1_f32(v1074, v1074);
    float32x2_t v1098 = vtrn2_f32(v1074, v1074);
    float32x2_t v1137 = vtrn1_f32(v1109, v1109);
    float32x2_t v1138 = vtrn2_f32(v1109, v1109);
    float32x2_t v1147 = vtrn1_f32(v1124, v1124);
    float32x2_t v1148 = vtrn2_f32(v1124, v1124);
    float32x2_t v1162 = vtrn1_f32(v1159, v1159);
    float32x2_t v1163 = vtrn2_f32(v1159, v1159);
    float32x2_t v1202 = vtrn1_f32(v1174, v1174);
    float32x2_t v1203 = vtrn2_f32(v1174, v1174);
    float32x2_t v1212 = vtrn1_f32(v1189, v1189);
    float32x2_t v1213 = vtrn2_f32(v1189, v1189);
    float32x2_t v1227 = vtrn1_f32(v1224, v1224);
    float32x2_t v1228 = vtrn2_f32(v1224, v1224);
    float32x2_t v1267 = vtrn1_f32(v1239, v1239);
    float32x2_t v1268 = vtrn2_f32(v1239, v1239);
    float32x2_t v1277 = vtrn1_f32(v1254, v1254);
    float32x2_t v1278 = vtrn2_f32(v1254, v1254);
    float32x2_t v1292 = vtrn1_f32(v1289, v1289);
    float32x2_t v1293 = vtrn2_f32(v1289, v1289);
    float32x2_t v1332 = vtrn1_f32(v1304, v1304);
    float32x2_t v1333 = vtrn2_f32(v1304, v1304);
    float32x2_t v1342 = vtrn1_f32(v1319, v1319);
    float32x2_t v1343 = vtrn2_f32(v1319, v1319);
    float32x2_t v1357 = vtrn1_f32(v1354, v1354);
    float32x2_t v1358 = vtrn2_f32(v1354, v1354);
    float32x2_t v1404 = vfma_f32(v1402, v1398, v1401);
    float32x2_t v1407 = vtrn1_f32(v1384, v1384);
    float32x2_t v1408 = vtrn2_f32(v1384, v1384);
    float32x2_t v1422 = vtrn1_f32(v1419, v1419);
    float32x2_t v1423 = vtrn2_f32(v1419, v1419);
    float32x2_t v1462 = vtrn1_f32(v1434, v1434);
    float32x2_t v1463 = vtrn2_f32(v1434, v1434);
    float32x2_t v1472 = vtrn1_f32(v1449, v1449);
    float32x2_t v1473 = vtrn2_f32(v1449, v1449);
    float32x2_t v1487 = vtrn1_f32(v1484, v1484);
    float32x2_t v1488 = vtrn2_f32(v1484, v1484);
    float32x2_t v1092 = vmul_f32(v1087, v1086);
    float32x2_t v1102 = vmul_f32(v1097, v1096);
    float32x2_t v1142 = vmul_f32(v1137, v1136);
    float32x2_t v1152 = vmul_f32(v1147, v1146);
    float32x2_t v1167 = vmul_f32(v1162, v1161);
    float32x2_t v1207 = vmul_f32(v1202, v1201);
    float32x2_t v1217 = vmul_f32(v1212, v1211);
    float32x2_t v1232 = vmul_f32(v1227, v1226);
    float32x2_t v1272 = vmul_f32(v1267, v1266);
    float32x2_t v1282 = vmul_f32(v1277, v1276);
    float32x2_t v1297 = vmul_f32(v1292, v1291);
    float32x2_t v1337 = vmul_f32(v1332, v1331);
    float32x2_t v1347 = vmul_f32(v1342, v1341);
    float32x2_t v1362 = vmul_f32(v1357, v1356);
    float32x2_t v1412 = vmul_f32(v1407, v1406);
    float32x2_t v1427 = vmul_f32(v1422, v1421);
    float32x2_t v1467 = vmul_f32(v1462, v1461);
    float32x2_t v1477 = vmul_f32(v1472, v1471);
    float32x2_t v1492 = vmul_f32(v1487, v1486);
    float32x2_t v1094 = vfma_f32(v1092, v1088, v1091);
    float32x2_t v1104 = vfma_f32(v1102, v1098, v1101);
    float32x2_t v1144 = vfma_f32(v1142, v1138, v1141);
    float32x2_t v1154 = vfma_f32(v1152, v1148, v1151);
    float32x2_t v1169 = vfma_f32(v1167, v1163, v1166);
    float32x2_t v1209 = vfma_f32(v1207, v1203, v1206);
    float32x2_t v1219 = vfma_f32(v1217, v1213, v1216);
    float32x2_t v1234 = vfma_f32(v1232, v1228, v1231);
    float32x2_t v1274 = vfma_f32(v1272, v1268, v1271);
    float32x2_t v1284 = vfma_f32(v1282, v1278, v1281);
    float32x2_t v1299 = vfma_f32(v1297, v1293, v1296);
    float32x2_t v1339 = vfma_f32(v1337, v1333, v1336);
    float32x2_t v1349 = vfma_f32(v1347, v1343, v1346);
    float32x2_t v1364 = vfma_f32(v1362, v1358, v1361);
    float32x2_t v1414 = vfma_f32(v1412, v1408, v1411);
    float32x2_t v1429 = vfma_f32(v1427, v1423, v1426);
    float32x2_t v1469 = vfma_f32(v1467, v1463, v1466);
    float32x2_t v1479 = vfma_f32(v1477, v1473, v1476);
    float32x2_t v1494 = vfma_f32(v1492, v1488, v1491);
    float32x2_t v1495 = vadd_f32(v1094, v1104);
    float32x2_t v1496 = vsub_f32(v1094, v1104);
    float32x2_t v1503 = vadd_f32(v1144, v1154);
    float32x2_t v1504 = vsub_f32(v1144, v1154);
    float32x2_t v1506 = vadd_f32(v1209, v1219);
    float32x2_t v1507 = vsub_f32(v1209, v1219);
    float32x2_t v1509 = vadd_f32(v1274, v1284);
    float32x2_t v1510 = vsub_f32(v1274, v1284);
    float32x2_t v1512 = vadd_f32(v1339, v1349);
    float32x2_t v1513 = vsub_f32(v1339, v1349);
    float32x2_t v1515 = vadd_f32(v1404, v1414);
    float32x2_t v1516 = vsub_f32(v1404, v1414);
    float32x2_t v1518 = vadd_f32(v1469, v1479);
    float32x2_t v1519 = vsub_f32(v1469, v1479);
    float32x2_t v1502 = vadd_f32(v1495, v1501);
    float32x2_t v1505 = vadd_f32(v1503, v1169);
    float32x2_t v1508 = vadd_f32(v1506, v1234);
    float32x2_t v1511 = vadd_f32(v1509, v1299);
    float32x2_t v1514 = vadd_f32(v1512, v1364);
    float32x2_t v1517 = vadd_f32(v1515, v1429);
    float32x2_t v1520 = vadd_f32(v1518, v1494);
    float32x2_t v1605 = vadd_f32(v1503, v1518);
    float32x2_t v1606 = vsub_f32(v1503, v1518);
    float32x2_t v1607 = vadd_f32(v1512, v1509);
    float32x2_t v1608 = vsub_f32(v1512, v1509);
    float32x2_t v1609 = vadd_f32(v1506, v1515);
    float32x2_t v1610 = vsub_f32(v1506, v1515);
    float32x2_t v1689 = vadd_f32(v1504, v1519);
    float32x2_t v1690 = vsub_f32(v1504, v1519);
    float32x2_t v1691 = vadd_f32(v1513, v1510);
    float32x2_t v1692 = vsub_f32(v1513, v1510);
    float32x2_t v1693 = vadd_f32(v1507, v1516);
    float32x2_t v1694 = vsub_f32(v1507, v1516);
    float32x2_t v1521 = vadd_f32(v1505, v1520);
    float32x2_t v1522 = vsub_f32(v1505, v1520);
    float32x2_t v1523 = vadd_f32(v1514, v1511);
    float32x2_t v1524 = vsub_f32(v1514, v1511);
    float32x2_t v1525 = vadd_f32(v1508, v1517);
    float32x2_t v1526 = vsub_f32(v1508, v1517);
    float32x2_t v1611 = vadd_f32(v1605, v1607);
    float32x2_t v1614 = vsub_f32(v1605, v1607);
    float32x2_t v1615 = vsub_f32(v1607, v1609);
    float32x2_t v1616 = vsub_f32(v1609, v1605);
    float32x2_t v1617 = vadd_f32(v1606, v1608);
    float32x2_t v1619 = vsub_f32(v1606, v1608);
    float32x2_t v1620 = vsub_f32(v1608, v1610);
    float32x2_t v1621 = vsub_f32(v1610, v1606);
    float32x2_t v1695 = vadd_f32(v1689, v1691);
    float32x2_t v1698 = vsub_f32(v1689, v1691);
    float32x2_t v1699 = vsub_f32(v1691, v1693);
    float32x2_t v1700 = vsub_f32(v1693, v1689);
    float32x2_t v1701 = vadd_f32(v1690, v1692);
    float32x2_t v1703 = vsub_f32(v1690, v1692);
    float32x2_t v1704 = vsub_f32(v1692, v1694);
    float32x2_t v1705 = vsub_f32(v1694, v1690);
    float32x2_t v1527 = vadd_f32(v1521, v1523);
    float32x2_t v1530 = vsub_f32(v1521, v1523);
    float32x2_t v1531 = vsub_f32(v1523, v1525);
    float32x2_t v1532 = vsub_f32(v1525, v1521);
    float32x2_t v1533 = vadd_f32(v1522, v1524);
    float32x2_t v1535 = vsub_f32(v1522, v1524);
    float32x2_t v1536 = vsub_f32(v1524, v1526);
    float32x2_t v1537 = vsub_f32(v1526, v1522);
    float32x2_t v1612 = vadd_f32(v1611, v1609);
    float32x2_t v1618 = vadd_f32(v1617, v1610);
    float32x2_t v1633 = vmul_f32(v1614, v1632);
    float32x2_t v1637 = vmul_f32(v1615, v1636);
    float32x2_t v1641 = vmul_f32(v1616, v1640);
    float32x2_t v1654 = vrev64_f32(v1619);
    float32x2_t v1661 = vrev64_f32(v1620);
    float32x2_t v1668 = vrev64_f32(v1621);
    float32x2_t v1696 = vadd_f32(v1695, v1693);
    float32x2_t v1702 = vadd_f32(v1701, v1694);
    float32x2_t v1725 = vrev64_f32(v1698);
    float32x2_t v1732 = vrev64_f32(v1699);
    float32x2_t v1739 = vrev64_f32(v1700);
    float32x2_t v1748 = vmul_f32(v1703, v1747);
    float32x2_t v1752 = vmul_f32(v1704, v1751);
    float32x2_t v1756 = vmul_f32(v1705, v1755);
    float32x2_t v1528 = vadd_f32(v1527, v1525);
    float32x2_t v1534 = vadd_f32(v1533, v1526);
    float32x2_t v1549 = vmul_f32(v1530, v1548);
    float32x2_t v1553 = vmul_f32(v1531, v1552);
    float32x2_t v1557 = vmul_f32(v1532, v1556);
    float32x2_t v1570 = vrev64_f32(v1535);
    float32x2_t v1577 = vrev64_f32(v1536);
    float32x2_t v1584 = vrev64_f32(v1537);
    float32x2_t v1613 = vadd_f32(v1612, v1495);
    float32x2_t v1629 = vmul_f32(v1612, v1628);
    float32x2_t v1647 = vrev64_f32(v1618);
    float32x2_t v1655 = vmul_f32(v1654, v1653);
    float32x2_t v1662 = vmul_f32(v1661, v1660);
    float32x2_t v1669 = vmul_f32(v1668, v1667);
    float32x2_t v1697 = vadd_f32(v1696, v1496);
    float32x2_t v1718 = vrev64_f32(v1696);
    float32x2_t v1726 = vmul_f32(v1725, v1724);
    float32x2_t v1733 = vmul_f32(v1732, v1731);
    float32x2_t v1740 = vmul_f32(v1739, v1738);
    float32x2_t v1744 = vmul_f32(v1702, v1743);
    float32x2_t v1529 = vadd_f32(v1528, v1502);
    float32x2_t v1545 = vmul_f32(v1528, v1544);
    float32x2_t v1563 = vrev64_f32(v1534);
    float32x2_t v1571 = vmul_f32(v1570, v1569);
    float32x2_t v1578 = vmul_f32(v1577, v1576);
    float32x2_t v1585 = vmul_f32(v1584, v1583);
    float32x2_t v1625 = vmul_f32(v1613, v1624);
    float32x2_t v1648 = vmul_f32(v1647, v1646);
    float32x2_t v1711 = vrev64_f32(v1697);
    float32x2_t v1719 = vmul_f32(v1718, v1717);
    float32x2_t v1764 = vadd_f32(v1744, v1748);
    float32x2_t v1766 = vsub_f32(v1744, v1748);
    float32x2_t v1768 = vsub_f32(v1744, v1752);
    float32x2_t v1564 = vmul_f32(v1563, v1562);
    float32x2_t v1586 = vadd_f32(v1529, v1545);
    float32x2_t v1670 = vadd_f32(v1625, v1629);
    float32x2_t v1677 = vadd_f32(v1648, v1655);
    float32x2_t v1679 = vsub_f32(v1648, v1655);
    float32x2_t v1681 = vsub_f32(v1648, v1662);
    float32x2_t v1712 = vmul_f32(v1711, v1710);
    float32x2_t v1765 = vadd_f32(v1764, v1752);
    float32x2_t v1767 = vsub_f32(v1766, v1756);
    float32x2_t v1769 = vadd_f32(v1768, v1756);
    float32x2_t v1776 = vadd_f32(v1529, v1625);
    v6[0] = v1529;
    float32x2_t v1587 = vadd_f32(v1586, v1549);
    float32x2_t v1589 = vsub_f32(v1586, v1549);
    float32x2_t v1591 = vsub_f32(v1586, v1553);
    float32x2_t v1593 = vadd_f32(v1564, v1571);
    float32x2_t v1595 = vsub_f32(v1564, v1571);
    float32x2_t v1597 = vsub_f32(v1564, v1578);
    float32x2_t v1671 = vadd_f32(v1670, v1633);
    float32x2_t v1673 = vsub_f32(v1670, v1633);
    float32x2_t v1675 = vsub_f32(v1670, v1637);
    float32x2_t v1678 = vadd_f32(v1677, v1662);
    float32x2_t v1680 = vsub_f32(v1679, v1669);
    float32x2_t v1682 = vadd_f32(v1681, v1669);
    float32x2_t v1757 = vadd_f32(v1712, v1719);
    float32x2_t v1777 = vadd_f32(v1776, v1712);
    float32x2_t v1778 = vsub_f32(v1776, v1712);
    float32x2_t v1588 = vadd_f32(v1587, v1553);
    float32x2_t v1590 = vsub_f32(v1589, v1557);
    float32x2_t v1592 = vadd_f32(v1591, v1557);
    float32x2_t v1594 = vadd_f32(v1593, v1578);
    float32x2_t v1596 = vsub_f32(v1595, v1585);
    float32x2_t v1598 = vadd_f32(v1597, v1585);
    float32x2_t v1672 = vadd_f32(v1671, v1637);
    float32x2_t v1674 = vsub_f32(v1673, v1641);
    float32x2_t v1676 = vadd_f32(v1675, v1641);
    float32x2_t v1758 = vadd_f32(v1757, v1726);
    float32x2_t v1760 = vsub_f32(v1757, v1726);
    float32x2_t v1762 = vsub_f32(v1757, v1733);
    v6[ostride * 7] = v1778;
    v6[ostride * 14] = v1777;
    float32x2_t v1599 = vadd_f32(v1588, v1594);
    float32x2_t v1600 = vsub_f32(v1588, v1594);
    float32x2_t v1601 = vadd_f32(v1590, v1596);
    float32x2_t v1602 = vsub_f32(v1590, v1596);
    float32x2_t v1603 = vadd_f32(v1592, v1598);
    float32x2_t v1604 = vsub_f32(v1592, v1598);
    float32x2_t v1683 = vadd_f32(v1672, v1678);
    float32x2_t v1684 = vsub_f32(v1672, v1678);
    float32x2_t v1685 = vadd_f32(v1674, v1680);
    float32x2_t v1686 = vsub_f32(v1674, v1680);
    float32x2_t v1687 = vadd_f32(v1676, v1682);
    float32x2_t v1688 = vsub_f32(v1676, v1682);
    float32x2_t v1759 = vadd_f32(v1758, v1733);
    float32x2_t v1761 = vsub_f32(v1760, v1740);
    float32x2_t v1763 = vadd_f32(v1762, v1740);
    float32x2_t v1770 = vadd_f32(v1759, v1765);
    float32x2_t v1771 = vsub_f32(v1759, v1765);
    float32x2_t v1772 = vadd_f32(v1761, v1767);
    float32x2_t v1773 = vsub_f32(v1761, v1767);
    float32x2_t v1774 = vadd_f32(v1763, v1769);
    float32x2_t v1775 = vsub_f32(v1763, v1769);
    float32x2_t v1794 = vadd_f32(v1600, v1684);
    v6[ostride * 15] = v1600;
    float32x2_t v1812 = vadd_f32(v1602, v1686);
    v6[ostride * 9] = v1602;
    float32x2_t v1830 = vadd_f32(v1603, v1687);
    v6[ostride * 3] = v1603;
    float32x2_t v1848 = vadd_f32(v1604, v1688);
    v6[ostride * 18] = v1604;
    float32x2_t v1866 = vadd_f32(v1601, v1685);
    v6[ostride * 12] = v1601;
    float32x2_t v1884 = vadd_f32(v1599, v1683);
    v6[ostride * 6] = v1599;
    float32x2_t v1795 = vadd_f32(v1794, v1771);
    float32x2_t v1796 = vsub_f32(v1794, v1771);
    float32x2_t v1813 = vadd_f32(v1812, v1773);
    float32x2_t v1814 = vsub_f32(v1812, v1773);
    float32x2_t v1831 = vadd_f32(v1830, v1774);
    float32x2_t v1832 = vsub_f32(v1830, v1774);
    float32x2_t v1849 = vadd_f32(v1848, v1775);
    float32x2_t v1850 = vsub_f32(v1848, v1775);
    float32x2_t v1867 = vadd_f32(v1866, v1772);
    float32x2_t v1868 = vsub_f32(v1866, v1772);
    float32x2_t v1885 = vadd_f32(v1884, v1770);
    float32x2_t v1886 = vsub_f32(v1884, v1770);
    v6[ostride] = v1796;
    v6[ostride * 8] = v1795;
    v6[ostride * 16] = v1814;
    v6[ostride * 2] = v1813;
    v6[ostride * 10] = v1832;
    v6[ostride * 17] = v1831;
    v6[ostride * 4] = v1850;
    v6[ostride * 11] = v1849;
    v6[ostride * 19] = v1868;
    v6[ostride * 5] = v1867;
    v6[ostride * 13] = v1886;
    v6[ostride * 20] = v1885;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu21(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v345 = -1.1666666666666665e+00F;
    float v350 = 7.9015646852540022e-01F;
    float v355 = 5.5854267289647742e-02F;
    float v360 = 7.3430220123575241e-01F;
    float v365 = -4.4095855184409838e-01F;
    float v372 = -3.4087293062393137e-01F;
    float v379 = 5.3396936033772524e-01F;
    float v386 = -8.7484229096165667e-01F;
    float v429 = -1.4999999999999998e+00F;
    float v434 = 1.7499999999999996e+00F;
    float v439 = -1.1852347027881001e+00F;
    float v444 = -8.3781400934471603e-02F;
    float v449 = -1.1014533018536286e+00F;
    float v454 = 6.6143782776614746e-01F;
    float v461 = 5.1130939593589697e-01F;
    float v468 = -8.0095404050658769e-01F;
    float v475 = 1.3122634364424848e+00F;
    float v518 = -8.6602540378443871e-01F;
    float v525 = 1.0103629710818451e+00F;
    float v532 = -6.8429557470759583e-01F;
    float v539 = -4.8371214382601155e-02F;
    float v546 = -6.3592436032499466e-01F;
    float v553 = -3.8188130791298663e-01F;
    float v558 = -2.9520461738277515e-01F;
    float v563 = 4.6243103089499693e-01F;
    float v568 = -7.5763564827777208e-01F;
    const float32x2_t *v891 = &v5[v0];
    float32x2_t *v1019 = &v6[v2];
    int64_t v19 = v0 * 7;
    int64_t v30 = v0 * 14;
    int64_t v49 = v0 * 10;
    int64_t v60 = v0 * 17;
    int64_t v79 = v0 * 3;
    int64_t v90 = v0 * 13;
    int64_t v101 = v0 * 20;
    int64_t v120 = v0 * 6;
    int64_t v131 = v0 * 16;
    int64_t v142 = v0 * 2;
    int64_t v161 = v0 * 9;
    int64_t v172 = v0 * 19;
    int64_t v183 = v0 * 5;
    int64_t v202 = v0 * 12;
    int64_t v224 = v0 * 8;
    int64_t v243 = v0 * 15;
    int64_t v254 = v0 * 4;
    int64_t v265 = v0 * 11;
    int64_t v284 = v0 * 18;
    float v368 = v4 * v365;
    float v375 = v4 * v372;
    float v382 = v4 * v379;
    float v389 = v4 * v386;
    float v457 = v4 * v454;
    float v464 = v4 * v461;
    float v471 = v4 * v468;
    float v478 = v4 * v475;
    float v521 = v4 * v518;
    float v528 = v4 * v525;
    float v535 = v4 * v532;
    float v542 = v4 * v539;
    float v549 = v4 * v546;
    int64_t v602 = v2 * 7;
    int64_t v609 = v2 * 14;
    int64_t v619 = v2 * 15;
    int64_t v633 = v2 * 8;
    int64_t v643 = v2 * 9;
    int64_t v650 = v2 * 16;
    int64_t v657 = v2 * 2;
    int64_t v667 = v2 * 3;
    int64_t v674 = v2 * 10;
    int64_t v681 = v2 * 17;
    int64_t v691 = v2 * 18;
    int64_t v698 = v2 * 4;
    int64_t v705 = v2 * 11;
    int64_t v715 = v2 * 12;
    int64_t v722 = v2 * 19;
    int64_t v729 = v2 * 5;
    int64_t v739 = v2 * 6;
    int64_t v746 = v2 * 13;
    int64_t v753 = v2 * 20;
    const float32x2_t *v946 = &v5[0];
    svfloat32_t v950 = svdup_n_f32(v345);
    svfloat32_t v951 = svdup_n_f32(v350);
    svfloat32_t v952 = svdup_n_f32(v355);
    svfloat32_t v953 = svdup_n_f32(v360);
    svfloat32_t v958 = svdup_n_f32(v429);
    svfloat32_t v959 = svdup_n_f32(v434);
    svfloat32_t v960 = svdup_n_f32(v439);
    svfloat32_t v961 = svdup_n_f32(v444);
    svfloat32_t v962 = svdup_n_f32(v449);
    svfloat32_t v972 = svdup_n_f32(v553);
    svfloat32_t v973 = svdup_n_f32(v558);
    svfloat32_t v974 = svdup_n_f32(v563);
    svfloat32_t v975 = svdup_n_f32(v568);
    float32x2_t *v983 = &v6[0];
    svfloat32_t v1195 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v891)[0]));
    svfloat32_t v42 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v46 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v72 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v76 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[16]));
    svfloat32_t v87 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v113 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v117 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[19]));
    svfloat32_t v128 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v154 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[15]));
    svfloat32_t v158 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v169 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v195 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[18]));
    svfloat32_t v199 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v210 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v236 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v240 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v251 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[14]));
    svfloat32_t v277 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v281 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v292 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[17]));
    const float32x2_t *v765 = &v5[v19];
    const float32x2_t *v774 = &v5[v30];
    const float32x2_t *v783 = &v5[v49];
    const float32x2_t *v792 = &v5[v60];
    const float32x2_t *v801 = &v5[v79];
    const float32x2_t *v810 = &v5[v90];
    const float32x2_t *v819 = &v5[v101];
    const float32x2_t *v828 = &v5[v120];
    const float32x2_t *v837 = &v5[v131];
    const float32x2_t *v846 = &v5[v142];
    const float32x2_t *v855 = &v5[v161];
    const float32x2_t *v864 = &v5[v172];
    const float32x2_t *v873 = &v5[v183];
    const float32x2_t *v882 = &v5[v202];
    const float32x2_t *v900 = &v5[v224];
    const float32x2_t *v909 = &v5[v243];
    const float32x2_t *v918 = &v5[v254];
    const float32x2_t *v927 = &v5[v265];
    const float32x2_t *v936 = &v5[v284];
    svfloat32_t v954 = svdup_n_f32(v368);
    svfloat32_t v955 = svdup_n_f32(v375);
    svfloat32_t v956 = svdup_n_f32(v382);
    svfloat32_t v957 = svdup_n_f32(v389);
    svfloat32_t v963 = svdup_n_f32(v457);
    svfloat32_t v964 = svdup_n_f32(v464);
    svfloat32_t v965 = svdup_n_f32(v471);
    svfloat32_t v966 = svdup_n_f32(v478);
    svfloat32_t v967 = svdup_n_f32(v521);
    svfloat32_t v968 = svdup_n_f32(v528);
    svfloat32_t v969 = svdup_n_f32(v535);
    svfloat32_t v970 = svdup_n_f32(v542);
    svfloat32_t v971 = svdup_n_f32(v549);
    float32x2_t *v992 = &v6[v602];
    float32x2_t *v1001 = &v6[v609];
    float32x2_t *v1010 = &v6[v619];
    float32x2_t *v1028 = &v6[v633];
    float32x2_t *v1037 = &v6[v643];
    float32x2_t *v1046 = &v6[v650];
    float32x2_t *v1055 = &v6[v657];
    float32x2_t *v1064 = &v6[v667];
    float32x2_t *v1073 = &v6[v674];
    float32x2_t *v1082 = &v6[v681];
    float32x2_t *v1091 = &v6[v691];
    float32x2_t *v1100 = &v6[v698];
    float32x2_t *v1109 = &v6[v705];
    float32x2_t *v1118 = &v6[v715];
    float32x2_t *v1127 = &v6[v722];
    float32x2_t *v1136 = &v6[v729];
    float32x2_t *v1145 = &v6[v739];
    float32x2_t *v1154 = &v6[v746];
    float32x2_t *v1163 = &v6[v753];
    svfloat32_t v1207 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v946)[0]));
    svfloat32_t zero237 = svdup_n_f32(0);
    svfloat32_t v237 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero237, v1195, v236, 0), v1195,
        v236, 90);
    svfloat32_t v1167 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v765)[0]));
    svfloat32_t v1169 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v774)[0]));
    svfloat32_t v1171 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v783)[0]));
    svfloat32_t v1173 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v792)[0]));
    svfloat32_t v1175 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v801)[0]));
    svfloat32_t v1177 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v810)[0]));
    svfloat32_t v1179 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v819)[0]));
    svfloat32_t v1181 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v828)[0]));
    svfloat32_t v1183 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v837)[0]));
    svfloat32_t v1185 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v846)[0]));
    svfloat32_t v1187 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v855)[0]));
    svfloat32_t v1189 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v864)[0]));
    svfloat32_t v1191 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v873)[0]));
    svfloat32_t v1193 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v882)[0]));
    svfloat32_t v1197 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v900)[0]));
    svfloat32_t v1199 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v909)[0]));
    svfloat32_t v1201 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v918)[0]));
    svfloat32_t v1203 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v927)[0]));
    svfloat32_t v1205 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v936)[0]));
    svfloat32_t zero43 = svdup_n_f32(0);
    svfloat32_t v43 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero43, v1167, v42, 0),
                     v1167, v42, 90);
    svfloat32_t zero47 = svdup_n_f32(0);
    svfloat32_t v47 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero47, v1169, v46, 0),
                     v1169, v46, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v1171, v72, 0),
                     v1171, v72, 90);
    svfloat32_t zero77 = svdup_n_f32(0);
    svfloat32_t v77 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero77, v1173, v76, 0),
                     v1173, v76, 90);
    svfloat32_t zero114 = svdup_n_f32(0);
    svfloat32_t v114 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero114, v1177, v113, 0), v1177,
        v113, 90);
    svfloat32_t zero118 = svdup_n_f32(0);
    svfloat32_t v118 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero118, v1179, v117, 0), v1179,
        v117, 90);
    svfloat32_t zero155 = svdup_n_f32(0);
    svfloat32_t v155 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero155, v1183, v154, 0), v1183,
        v154, 90);
    svfloat32_t zero159 = svdup_n_f32(0);
    svfloat32_t v159 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero159, v1185, v158, 0), v1185,
        v158, 90);
    svfloat32_t zero196 = svdup_n_f32(0);
    svfloat32_t v196 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero196, v1189, v195, 0), v1189,
        v195, 90);
    svfloat32_t zero200 = svdup_n_f32(0);
    svfloat32_t v200 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero200, v1191, v199, 0), v1191,
        v199, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero241, v1197, v240, 0), v1197,
        v240, 90);
    svfloat32_t zero278 = svdup_n_f32(0);
    svfloat32_t v278 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero278, v1201, v277, 0), v1201,
        v277, 90);
    svfloat32_t zero282 = svdup_n_f32(0);
    svfloat32_t v282 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero282, v1203, v281, 0), v1203,
        v281, 90);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v307 = svadd_f32_x(svptrue_b32(), v114, v118);
    svfloat32_t v308 = svsub_f32_x(svptrue_b32(), v114, v118);
    svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v155, v159);
    svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v155, v159);
    svfloat32_t v313 = svadd_f32_x(svptrue_b32(), v196, v200);
    svfloat32_t v314 = svsub_f32_x(svptrue_b32(), v196, v200);
    svfloat32_t v316 = svadd_f32_x(svptrue_b32(), v237, v241);
    svfloat32_t v317 = svsub_f32_x(svptrue_b32(), v237, v241);
    svfloat32_t v319 = svadd_f32_x(svptrue_b32(), v278, v282);
    svfloat32_t v320 = svsub_f32_x(svptrue_b32(), v278, v282);
    svfloat32_t v303 = svadd_f32_x(svptrue_b32(), v294, v1207);
    svfloat32_t v306 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v304, v1175, v87, 0),
                     v1175, v87, 90);
    svfloat32_t v309 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v307, v1181, v128, 0),
                     v1181, v128, 90);
    svfloat32_t v312 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v310, v1187, v169, 0),
                     v1187, v169, 90);
    svfloat32_t v315 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v313, v1193, v210, 0),
                     v1193, v210, 90);
    svfloat32_t v318 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v316, v1199, v251, 0),
                     v1199, v251, 90);
    svfloat32_t v321 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v319, v1205, v292, 0),
                     v1205, v292, 90);
    svfloat32_t v411 = svadd_f32_x(svptrue_b32(), v304, v319);
    svfloat32_t v412 = svsub_f32_x(svptrue_b32(), v304, v319);
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v313, v310);
    svfloat32_t v414 = svsub_f32_x(svptrue_b32(), v313, v310);
    svfloat32_t v415 = svadd_f32_x(svptrue_b32(), v307, v316);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v307, v316);
    svfloat32_t v500 = svadd_f32_x(svptrue_b32(), v305, v320);
    svfloat32_t v501 = svsub_f32_x(svptrue_b32(), v305, v320);
    svfloat32_t v502 = svadd_f32_x(svptrue_b32(), v314, v311);
    svfloat32_t v503 = svsub_f32_x(svptrue_b32(), v314, v311);
    svfloat32_t v504 = svadd_f32_x(svptrue_b32(), v308, v317);
    svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v308, v317);
    svfloat32_t v322 = svadd_f32_x(svptrue_b32(), v306, v321);
    svfloat32_t v323 = svsub_f32_x(svptrue_b32(), v306, v321);
    svfloat32_t v324 = svadd_f32_x(svptrue_b32(), v315, v312);
    svfloat32_t v325 = svsub_f32_x(svptrue_b32(), v315, v312);
    svfloat32_t v326 = svadd_f32_x(svptrue_b32(), v309, v318);
    svfloat32_t v327 = svsub_f32_x(svptrue_b32(), v309, v318);
    svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v411, v413);
    svfloat32_t v420 = svsub_f32_x(svptrue_b32(), v411, v413);
    svfloat32_t v421 = svsub_f32_x(svptrue_b32(), v413, v415);
    svfloat32_t v422 = svsub_f32_x(svptrue_b32(), v415, v411);
    svfloat32_t v423 = svadd_f32_x(svptrue_b32(), v412, v414);
    svfloat32_t v425 = svsub_f32_x(svptrue_b32(), v412, v414);
    svfloat32_t v426 = svsub_f32_x(svptrue_b32(), v414, v416);
    svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v416, v412);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v500, v502);
    svfloat32_t v509 = svsub_f32_x(svptrue_b32(), v500, v502);
    svfloat32_t v510 = svsub_f32_x(svptrue_b32(), v502, v504);
    svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v504, v500);
    svfloat32_t v512 = svadd_f32_x(svptrue_b32(), v501, v503);
    svfloat32_t v514 = svsub_f32_x(svptrue_b32(), v501, v503);
    svfloat32_t v515 = svsub_f32_x(svptrue_b32(), v503, v505);
    svfloat32_t v516 = svsub_f32_x(svptrue_b32(), v505, v501);
    svfloat32_t v328 = svadd_f32_x(svptrue_b32(), v322, v324);
    svfloat32_t v331 = svsub_f32_x(svptrue_b32(), v322, v324);
    svfloat32_t v332 = svsub_f32_x(svptrue_b32(), v324, v326);
    svfloat32_t v333 = svsub_f32_x(svptrue_b32(), v326, v322);
    svfloat32_t v334 = svadd_f32_x(svptrue_b32(), v323, v325);
    svfloat32_t v336 = svsub_f32_x(svptrue_b32(), v323, v325);
    svfloat32_t v337 = svsub_f32_x(svptrue_b32(), v325, v327);
    svfloat32_t v338 = svsub_f32_x(svptrue_b32(), v327, v323);
    svfloat32_t v418 = svadd_f32_x(svptrue_b32(), v417, v415);
    svfloat32_t v424 = svadd_f32_x(svptrue_b32(), v423, v416);
    svfloat32_t zero466 = svdup_n_f32(0);
    svfloat32_t v466 = svcmla_f32_x(pred_full, zero466, v964, v425, 90);
    svfloat32_t zero473 = svdup_n_f32(0);
    svfloat32_t v473 = svcmla_f32_x(pred_full, zero473, v965, v426, 90);
    svfloat32_t zero480 = svdup_n_f32(0);
    svfloat32_t v480 = svcmla_f32_x(pred_full, zero480, v966, v427, 90);
    svfloat32_t v507 = svadd_f32_x(svptrue_b32(), v506, v504);
    svfloat32_t v513 = svadd_f32_x(svptrue_b32(), v512, v505);
    svfloat32_t zero537 = svdup_n_f32(0);
    svfloat32_t v537 = svcmla_f32_x(pred_full, zero537, v969, v509, 90);
    svfloat32_t zero544 = svdup_n_f32(0);
    svfloat32_t v544 = svcmla_f32_x(pred_full, zero544, v970, v510, 90);
    svfloat32_t zero551 = svdup_n_f32(0);
    svfloat32_t v551 = svcmla_f32_x(pred_full, zero551, v971, v511, 90);
    svfloat32_t v561 = svmul_f32_x(svptrue_b32(), v514, v973);
    svfloat32_t v566 = svmul_f32_x(svptrue_b32(), v515, v974);
    svfloat32_t v329 = svadd_f32_x(svptrue_b32(), v328, v326);
    svfloat32_t v335 = svadd_f32_x(svptrue_b32(), v334, v327);
    svfloat32_t zero377 = svdup_n_f32(0);
    svfloat32_t v377 = svcmla_f32_x(pred_full, zero377, v955, v336, 90);
    svfloat32_t zero384 = svdup_n_f32(0);
    svfloat32_t v384 = svcmla_f32_x(pred_full, zero384, v956, v337, 90);
    svfloat32_t zero391 = svdup_n_f32(0);
    svfloat32_t v391 = svcmla_f32_x(pred_full, zero391, v957, v338, 90);
    svfloat32_t v419 = svadd_f32_x(svptrue_b32(), v418, v294);
    svfloat32_t v437 = svmul_f32_x(svptrue_b32(), v418, v959);
    svfloat32_t zero459 = svdup_n_f32(0);
    svfloat32_t v459 = svcmla_f32_x(pred_full, zero459, v963, v424, 90);
    svfloat32_t v508 = svadd_f32_x(svptrue_b32(), v507, v295);
    svfloat32_t v330 = svadd_f32_x(svptrue_b32(), v329, v303);
    svfloat32_t zero370 = svdup_n_f32(0);
    svfloat32_t v370 = svcmla_f32_x(pred_full, zero370, v954, v335, 90);
    svfloat32_t v488 = svadd_f32_x(svptrue_b32(), v459, v466);
    svfloat32_t v490 = svsub_f32_x(svptrue_b32(), v459, v466);
    svfloat32_t v492 = svsub_f32_x(svptrue_b32(), v459, v473);
    svfloat32_t zero523 = svdup_n_f32(0);
    svfloat32_t v523 = svcmla_f32_x(pred_full, zero523, v967, v508, 90);
    svfloat32_t v579 = svmla_f32_x(pred_full, v561, v513, v972);
    svfloat32_t v581 = svnmls_f32_x(pred_full, v561, v513, v972);
    svfloat32_t v583 = svnmls_f32_x(pred_full, v566, v513, v972);
    svfloat32_t v392 = svmla_f32_x(pred_full, v330, v329, v950);
    svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v370, v377);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v370, v377);
    svfloat32_t v403 = svsub_f32_x(svptrue_b32(), v370, v384);
    svfloat32_t v481 = svmla_f32_x(pred_full, v437, v419, v958);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v488, v473);
    svfloat32_t v491 = svsub_f32_x(svptrue_b32(), v490, v480);
    svfloat32_t v493 = svadd_f32_x(svptrue_b32(), v492, v480);
    svfloat32_t v572 = svcmla_f32_x(pred_full, v523, v968, v507, 90);
    svfloat32_t v580 = svmla_f32_x(pred_full, v579, v515, v974);
    svfloat32_t v582 = svmls_f32_x(pred_full, v581, v516, v975);
    svfloat32_t v584 = svmla_f32_x(pred_full, v583, v516, v975);
    svfloat32_t v591 = svmla_f32_x(pred_full, v330, v419, v958);
    svst1_f64(pred_full, (double *)(v983), svreinterpret_f64_f32(v330));
    svfloat32_t v393 = svmla_f32_x(pred_full, v392, v331, v951);
    svfloat32_t v395 = svmls_f32_x(pred_full, v392, v331, v951);
    svfloat32_t v397 = svmls_f32_x(pred_full, v392, v332, v952);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v399, v384);
    svfloat32_t v402 = svsub_f32_x(svptrue_b32(), v401, v391);
    svfloat32_t v404 = svadd_f32_x(svptrue_b32(), v403, v391);
    svfloat32_t v482 = svmla_f32_x(pred_full, v481, v420, v960);
    svfloat32_t v484 = svmls_f32_x(pred_full, v481, v420, v960);
    svfloat32_t v486 = svmls_f32_x(pred_full, v481, v421, v961);
    svfloat32_t v573 = svadd_f32_x(svptrue_b32(), v572, v537);
    svfloat32_t v575 = svsub_f32_x(svptrue_b32(), v572, v537);
    svfloat32_t v577 = svsub_f32_x(svptrue_b32(), v572, v544);
    svfloat32_t v592 = svadd_f32_x(svptrue_b32(), v591, v523);
    svfloat32_t v593 = svsub_f32_x(svptrue_b32(), v591, v523);
    svfloat32_t v394 = svmla_f32_x(pred_full, v393, v332, v952);
    svfloat32_t v396 = svmls_f32_x(pred_full, v395, v333, v953);
    svfloat32_t v398 = svmla_f32_x(pred_full, v397, v333, v953);
    svfloat32_t v483 = svmla_f32_x(pred_full, v482, v421, v961);
    svfloat32_t v485 = svmls_f32_x(pred_full, v484, v422, v962);
    svfloat32_t v487 = svmla_f32_x(pred_full, v486, v422, v962);
    svfloat32_t v574 = svadd_f32_x(svptrue_b32(), v573, v544);
    svfloat32_t v576 = svsub_f32_x(svptrue_b32(), v575, v551);
    svfloat32_t v578 = svadd_f32_x(svptrue_b32(), v577, v551);
    svst1_f64(pred_full, (double *)(v992), svreinterpret_f64_f32(v593));
    svst1_f64(pred_full, (double *)(v1001), svreinterpret_f64_f32(v592));
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v394, v400);
    svfloat32_t v406 = svsub_f32_x(svptrue_b32(), v394, v400);
    svfloat32_t v407 = svadd_f32_x(svptrue_b32(), v396, v402);
    svfloat32_t v408 = svsub_f32_x(svptrue_b32(), v396, v402);
    svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v398, v404);
    svfloat32_t v410 = svsub_f32_x(svptrue_b32(), v398, v404);
    svfloat32_t v494 = svadd_f32_x(svptrue_b32(), v483, v489);
    svfloat32_t v495 = svsub_f32_x(svptrue_b32(), v483, v489);
    svfloat32_t v496 = svadd_f32_x(svptrue_b32(), v485, v491);
    svfloat32_t v497 = svsub_f32_x(svptrue_b32(), v485, v491);
    svfloat32_t v498 = svadd_f32_x(svptrue_b32(), v487, v493);
    svfloat32_t v499 = svsub_f32_x(svptrue_b32(), v487, v493);
    svfloat32_t v585 = svadd_f32_x(svptrue_b32(), v574, v580);
    svfloat32_t v586 = svsub_f32_x(svptrue_b32(), v574, v580);
    svfloat32_t v587 = svadd_f32_x(svptrue_b32(), v576, v582);
    svfloat32_t v588 = svsub_f32_x(svptrue_b32(), v576, v582);
    svfloat32_t v589 = svadd_f32_x(svptrue_b32(), v578, v584);
    svfloat32_t v590 = svsub_f32_x(svptrue_b32(), v578, v584);
    svfloat32_t v615 = svadd_f32_x(svptrue_b32(), v406, v495);
    svfloat32_t v639 = svadd_f32_x(svptrue_b32(), v408, v497);
    svfloat32_t v663 = svadd_f32_x(svptrue_b32(), v409, v498);
    svfloat32_t v687 = svadd_f32_x(svptrue_b32(), v410, v499);
    svfloat32_t v711 = svadd_f32_x(svptrue_b32(), v407, v496);
    svfloat32_t v735 = svadd_f32_x(svptrue_b32(), v405, v494);
    svst1_f64(pred_full, (double *)(v1010), svreinterpret_f64_f32(v406));
    svst1_f64(pred_full, (double *)(v1037), svreinterpret_f64_f32(v408));
    svst1_f64(pred_full, (double *)(v1064), svreinterpret_f64_f32(v409));
    svst1_f64(pred_full, (double *)(v1091), svreinterpret_f64_f32(v410));
    svst1_f64(pred_full, (double *)(v1118), svreinterpret_f64_f32(v407));
    svst1_f64(pred_full, (double *)(v1145), svreinterpret_f64_f32(v405));
    svfloat32_t v616 = svadd_f32_x(svptrue_b32(), v615, v586);
    svfloat32_t v617 = svsub_f32_x(svptrue_b32(), v615, v586);
    svfloat32_t v640 = svadd_f32_x(svptrue_b32(), v639, v588);
    svfloat32_t v641 = svsub_f32_x(svptrue_b32(), v639, v588);
    svfloat32_t v664 = svadd_f32_x(svptrue_b32(), v663, v589);
    svfloat32_t v665 = svsub_f32_x(svptrue_b32(), v663, v589);
    svfloat32_t v688 = svadd_f32_x(svptrue_b32(), v687, v590);
    svfloat32_t v689 = svsub_f32_x(svptrue_b32(), v687, v590);
    svfloat32_t v712 = svadd_f32_x(svptrue_b32(), v711, v587);
    svfloat32_t v713 = svsub_f32_x(svptrue_b32(), v711, v587);
    svfloat32_t v736 = svadd_f32_x(svptrue_b32(), v735, v585);
    svfloat32_t v737 = svsub_f32_x(svptrue_b32(), v735, v585);
    svst1_f64(pred_full, (double *)(v1019), svreinterpret_f64_f32(v617));
    svst1_f64(pred_full, (double *)(v1028), svreinterpret_f64_f32(v616));
    svst1_f64(pred_full, (double *)(v1046), svreinterpret_f64_f32(v641));
    svst1_f64(pred_full, (double *)(v1055), svreinterpret_f64_f32(v640));
    svst1_f64(pred_full, (double *)(v1073), svreinterpret_f64_f32(v665));
    svst1_f64(pred_full, (double *)(v1082), svreinterpret_f64_f32(v664));
    svst1_f64(pred_full, (double *)(v1100), svreinterpret_f64_f32(v689));
    svst1_f64(pred_full, (double *)(v1109), svreinterpret_f64_f32(v688));
    svst1_f64(pred_full, (double *)(v1127), svreinterpret_f64_f32(v713));
    svst1_f64(pred_full, (double *)(v1136), svreinterpret_f64_f32(v712));
    svst1_f64(pred_full, (double *)(v1154), svreinterpret_f64_f32(v737));
    svst1_f64(pred_full, (double *)(v1163), svreinterpret_f64_f32(v736));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu22(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v1293 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v963 = 1.1000000000000001e+00F;
    float v967 = 3.3166247903554003e-01F;
    float v968 = -3.3166247903554003e-01F;
    float v976 = 5.1541501300188641e-01F;
    float v981 = 9.4125353283118118e-01F;
    float v986 = 1.4143537075597825e+00F;
    float v991 = 8.5949297361449750e-01F;
    float v996 = 4.2314838273285138e-02F;
    float v1001 = 3.8639279888589606e-01F;
    float v1006 = 5.1254589567200015e-01F;
    float v1011 = 1.0702757469471715e+00F;
    float v1016 = 5.5486073394528512e-01F;
    float v1020 = 1.2412944743900585e+00F;
    float v1021 = -1.2412944743900585e+00F;
    float v1028 = 2.0897833842005756e-01F;
    float v1029 = -2.0897833842005756e-01F;
    float v1036 = 3.7415717312460811e-01F;
    float v1037 = -3.7415717312460811e-01F;
    float v1044 = 4.9929922194110327e-02F;
    float v1045 = -4.9929922194110327e-02F;
    float v1052 = 6.5815896284539266e-01F;
    float v1053 = -6.5815896284539266e-01F;
    float v1060 = 6.3306543373877577e-01F;
    float v1061 = -6.3306543373877577e-01F;
    float v1068 = 1.0822460581641109e+00F;
    float v1069 = -1.0822460581641109e+00F;
    float v1076 = 8.1720737907134022e-01F;
    float v1077 = -8.1720737907134022e-01F;
    float v1084 = 4.2408709531871824e-01F;
    float v1085 = -4.2408709531871824e-01F;
    float32x2_t v1087 = (float32x2_t){v4, v4};
    const float32x2_t *v2500 = &v5[istride];
    float32x2_t *v2636 = &v6[ostride];
    float32x2_t v964 = (float32x2_t){v963, v963};
    float32x2_t v969 = (float32x2_t){v967, v968};
    float32x2_t v977 = (float32x2_t){v976, v976};
    float32x2_t v982 = (float32x2_t){v981, v981};
    float32x2_t v987 = (float32x2_t){v986, v986};
    float32x2_t v992 = (float32x2_t){v991, v991};
    float32x2_t v997 = (float32x2_t){v996, v996};
    float32x2_t v1002 = (float32x2_t){v1001, v1001};
    float32x2_t v1007 = (float32x2_t){v1006, v1006};
    float32x2_t v1012 = (float32x2_t){v1011, v1011};
    float32x2_t v1017 = (float32x2_t){v1016, v1016};
    float32x2_t v1022 = (float32x2_t){v1020, v1021};
    float32x2_t v1030 = (float32x2_t){v1028, v1029};
    float32x2_t v1038 = (float32x2_t){v1036, v1037};
    float32x2_t v1046 = (float32x2_t){v1044, v1045};
    float32x2_t v1054 = (float32x2_t){v1052, v1053};
    float32x2_t v1062 = (float32x2_t){v1060, v1061};
    float32x2_t v1070 = (float32x2_t){v1068, v1069};
    float32x2_t v1078 = (float32x2_t){v1076, v1077};
    float32x2_t v1086 = (float32x2_t){v1084, v1085};
    const float32x2_t *v2599 = &v5[0];
    float32x2_t *v2609 = &v6[0];
    float32x4_t v2826 = vld1q_f32((const float32_t *)v2500);
    float32x4_t v47 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v49 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v97 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v99 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v109 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v111 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v159 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v161 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v171 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[28]));
    float32x4_t v173 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[29]));
    float32x4_t v221 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v223 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v233 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[32]));
    float32x4_t v235 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[33]));
    float32x4_t v283 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v285 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v295 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[36]));
    float32x4_t v297 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[37]));
    float32x4_t v345 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v347 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v357 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[40]));
    float32x4_t v359 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[41]));
    float32x4_t v407 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v409 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v414 = vtrn1q_f32(v2826, v2826);
    float32x4_t v415 = vtrn2q_f32(v2826, v2826);
    float32x4_t v419 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v421 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v469 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v471 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v481 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v483 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v531 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[30]));
    float32x4_t v533 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[31]));
    float32x4_t v543 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v545 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v593 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[34]));
    float32x4_t v595 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[35]));
    float32x4_t v605 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v607 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v655 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[38]));
    float32x4_t v657 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[39]));
    float32x4_t v667 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v669 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v965 = vcombine_f32(v964, v964);
    float32x2_t v971 = vmul_f32(v1087, v969);
    float32x4_t v978 = vcombine_f32(v977, v977);
    float32x4_t v983 = vcombine_f32(v982, v982);
    float32x4_t v988 = vcombine_f32(v987, v987);
    float32x4_t v993 = vcombine_f32(v992, v992);
    float32x4_t v998 = vcombine_f32(v997, v997);
    float32x4_t v1003 = vcombine_f32(v1002, v1002);
    float32x4_t v1008 = vcombine_f32(v1007, v1007);
    float32x4_t v1013 = vcombine_f32(v1012, v1012);
    float32x4_t v1018 = vcombine_f32(v1017, v1017);
    float32x2_t v1024 = vmul_f32(v1087, v1022);
    float32x2_t v1032 = vmul_f32(v1087, v1030);
    float32x2_t v1040 = vmul_f32(v1087, v1038);
    float32x2_t v1048 = vmul_f32(v1087, v1046);
    float32x2_t v1056 = vmul_f32(v1087, v1054);
    float32x2_t v1064 = vmul_f32(v1087, v1062);
    float32x2_t v1072 = vmul_f32(v1087, v1070);
    float32x2_t v1080 = vmul_f32(v1087, v1078);
    float32x2_t v1088 = vmul_f32(v1087, v1086);
    const float32x2_t *v2369 = &v5[istride * 11];
    const float32x2_t *v2380 = &v5[istride * 2];
    const float32x2_t *v2390 = &v5[istride * 13];
    const float32x2_t *v2402 = &v5[istride * 4];
    const float32x2_t *v2412 = &v5[istride * 15];
    const float32x2_t *v2424 = &v5[istride * 6];
    const float32x2_t *v2434 = &v5[istride * 17];
    const float32x2_t *v2446 = &v5[istride * 8];
    const float32x2_t *v2456 = &v5[istride * 19];
    const float32x2_t *v2468 = &v5[istride * 10];
    const float32x2_t *v2478 = &v5[istride * 21];
    const float32x2_t *v2490 = &v5[istride * 12];
    const float32x2_t *v2510 = &v5[istride * 14];
    const float32x2_t *v2520 = &v5[istride * 3];
    const float32x2_t *v2532 = &v5[istride * 16];
    const float32x2_t *v2542 = &v5[istride * 5];
    const float32x2_t *v2554 = &v5[istride * 18];
    const float32x2_t *v2564 = &v5[istride * 7];
    const float32x2_t *v2576 = &v5[istride * 20];
    const float32x2_t *v2586 = &v5[istride * 9];
    float32x2_t *v2618 = &v6[ostride * 11];
    float32x2_t *v2627 = &v6[ostride * 12];
    float32x2_t *v2645 = &v6[ostride * 2];
    float32x2_t *v2654 = &v6[ostride * 13];
    float32x2_t *v2663 = &v6[ostride * 14];
    float32x2_t *v2672 = &v6[ostride * 3];
    float32x2_t *v2681 = &v6[ostride * 4];
    float32x2_t *v2690 = &v6[ostride * 15];
    float32x2_t *v2699 = &v6[ostride * 16];
    float32x2_t *v2708 = &v6[ostride * 5];
    float32x2_t *v2717 = &v6[ostride * 6];
    float32x2_t *v2726 = &v6[ostride * 17];
    float32x2_t *v2735 = &v6[ostride * 18];
    float32x2_t *v2744 = &v6[ostride * 7];
    float32x2_t *v2753 = &v6[ostride * 8];
    float32x2_t *v2762 = &v6[ostride * 19];
    float32x2_t *v2771 = &v6[ostride * 20];
    float32x2_t *v2780 = &v6[ostride * 9];
    float32x2_t *v2789 = &v6[ostride * 10];
    float32x2_t *v2798 = &v6[ostride * 21];
    float32x4_t v2844 = vld1q_f32((const float32_t *)v2599);
    float32x4_t v420 = vmulq_f32(v414, v419);
    float32x4_t v973 = vcombine_f32(v971, v971);
    float32x4_t v1026 = vcombine_f32(v1024, v1024);
    float32x4_t v1034 = vcombine_f32(v1032, v1032);
    float32x4_t v1042 = vcombine_f32(v1040, v1040);
    float32x4_t v1050 = vcombine_f32(v1048, v1048);
    float32x4_t v1058 = vcombine_f32(v1056, v1056);
    float32x4_t v1066 = vcombine_f32(v1064, v1064);
    float32x4_t v1074 = vcombine_f32(v1072, v1072);
    float32x4_t v1082 = vcombine_f32(v1080, v1080);
    float32x4_t v1090 = vcombine_f32(v1088, v1088);
    float32x4_t v2802 = vld1q_f32((const float32_t *)v2369);
    float32x4_t v2804 = vld1q_f32((const float32_t *)v2380);
    float32x4_t v2806 = vld1q_f32((const float32_t *)v2390);
    float32x4_t v2808 = vld1q_f32((const float32_t *)v2402);
    float32x4_t v2810 = vld1q_f32((const float32_t *)v2412);
    float32x4_t v2812 = vld1q_f32((const float32_t *)v2424);
    float32x4_t v2814 = vld1q_f32((const float32_t *)v2434);
    float32x4_t v2816 = vld1q_f32((const float32_t *)v2446);
    float32x4_t v2818 = vld1q_f32((const float32_t *)v2456);
    float32x4_t v2820 = vld1q_f32((const float32_t *)v2468);
    float32x4_t v2822 = vld1q_f32((const float32_t *)v2478);
    float32x4_t v2824 = vld1q_f32((const float32_t *)v2490);
    float32x4_t v2828 = vld1q_f32((const float32_t *)v2510);
    float32x4_t v2830 = vld1q_f32((const float32_t *)v2520);
    float32x4_t v2832 = vld1q_f32((const float32_t *)v2532);
    float32x4_t v2834 = vld1q_f32((const float32_t *)v2542);
    float32x4_t v2836 = vld1q_f32((const float32_t *)v2554);
    float32x4_t v2838 = vld1q_f32((const float32_t *)v2564);
    float32x4_t v2840 = vld1q_f32((const float32_t *)v2576);
    float32x4_t v2842 = vld1q_f32((const float32_t *)v2586);
    float32x4_t v42 = vtrn1q_f32(v2802, v2802);
    float32x4_t v43 = vtrn2q_f32(v2802, v2802);
    float32x4_t v92 = vtrn1q_f32(v2804, v2804);
    float32x4_t v93 = vtrn2q_f32(v2804, v2804);
    float32x4_t v104 = vtrn1q_f32(v2806, v2806);
    float32x4_t v105 = vtrn2q_f32(v2806, v2806);
    float32x4_t v154 = vtrn1q_f32(v2808, v2808);
    float32x4_t v155 = vtrn2q_f32(v2808, v2808);
    float32x4_t v166 = vtrn1q_f32(v2810, v2810);
    float32x4_t v167 = vtrn2q_f32(v2810, v2810);
    float32x4_t v216 = vtrn1q_f32(v2812, v2812);
    float32x4_t v217 = vtrn2q_f32(v2812, v2812);
    float32x4_t v228 = vtrn1q_f32(v2814, v2814);
    float32x4_t v229 = vtrn2q_f32(v2814, v2814);
    float32x4_t v278 = vtrn1q_f32(v2816, v2816);
    float32x4_t v279 = vtrn2q_f32(v2816, v2816);
    float32x4_t v290 = vtrn1q_f32(v2818, v2818);
    float32x4_t v291 = vtrn2q_f32(v2818, v2818);
    float32x4_t v340 = vtrn1q_f32(v2820, v2820);
    float32x4_t v341 = vtrn2q_f32(v2820, v2820);
    float32x4_t v352 = vtrn1q_f32(v2822, v2822);
    float32x4_t v353 = vtrn2q_f32(v2822, v2822);
    float32x4_t v402 = vtrn1q_f32(v2824, v2824);
    float32x4_t v403 = vtrn2q_f32(v2824, v2824);
    float32x4_t v423 = vfmaq_f32(v420, v415, v421);
    float32x4_t v464 = vtrn1q_f32(v2828, v2828);
    float32x4_t v465 = vtrn2q_f32(v2828, v2828);
    float32x4_t v476 = vtrn1q_f32(v2830, v2830);
    float32x4_t v477 = vtrn2q_f32(v2830, v2830);
    float32x4_t v526 = vtrn1q_f32(v2832, v2832);
    float32x4_t v527 = vtrn2q_f32(v2832, v2832);
    float32x4_t v538 = vtrn1q_f32(v2834, v2834);
    float32x4_t v539 = vtrn2q_f32(v2834, v2834);
    float32x4_t v588 = vtrn1q_f32(v2836, v2836);
    float32x4_t v589 = vtrn2q_f32(v2836, v2836);
    float32x4_t v600 = vtrn1q_f32(v2838, v2838);
    float32x4_t v601 = vtrn2q_f32(v2838, v2838);
    float32x4_t v650 = vtrn1q_f32(v2840, v2840);
    float32x4_t v651 = vtrn2q_f32(v2840, v2840);
    float32x4_t v662 = vtrn1q_f32(v2842, v2842);
    float32x4_t v663 = vtrn2q_f32(v2842, v2842);
    float32x4_t v48 = vmulq_f32(v42, v47);
    float32x4_t v98 = vmulq_f32(v92, v97);
    float32x4_t v110 = vmulq_f32(v104, v109);
    float32x4_t v160 = vmulq_f32(v154, v159);
    float32x4_t v172 = vmulq_f32(v166, v171);
    float32x4_t v222 = vmulq_f32(v216, v221);
    float32x4_t v234 = vmulq_f32(v228, v233);
    float32x4_t v284 = vmulq_f32(v278, v283);
    float32x4_t v296 = vmulq_f32(v290, v295);
    float32x4_t v346 = vmulq_f32(v340, v345);
    float32x4_t v358 = vmulq_f32(v352, v357);
    float32x4_t v408 = vmulq_f32(v402, v407);
    float32x4_t v470 = vmulq_f32(v464, v469);
    float32x4_t v482 = vmulq_f32(v476, v481);
    float32x4_t v532 = vmulq_f32(v526, v531);
    float32x4_t v544 = vmulq_f32(v538, v543);
    float32x4_t v594 = vmulq_f32(v588, v593);
    float32x4_t v606 = vmulq_f32(v600, v605);
    float32x4_t v656 = vmulq_f32(v650, v655);
    float32x4_t v668 = vmulq_f32(v662, v667);
    float32x4_t v51 = vfmaq_f32(v48, v43, v49);
    float32x4_t v101 = vfmaq_f32(v98, v93, v99);
    float32x4_t v113 = vfmaq_f32(v110, v105, v111);
    float32x4_t v163 = vfmaq_f32(v160, v155, v161);
    float32x4_t v175 = vfmaq_f32(v172, v167, v173);
    float32x4_t v225 = vfmaq_f32(v222, v217, v223);
    float32x4_t v237 = vfmaq_f32(v234, v229, v235);
    float32x4_t v287 = vfmaq_f32(v284, v279, v285);
    float32x4_t v299 = vfmaq_f32(v296, v291, v297);
    float32x4_t v349 = vfmaq_f32(v346, v341, v347);
    float32x4_t v361 = vfmaq_f32(v358, v353, v359);
    float32x4_t v411 = vfmaq_f32(v408, v403, v409);
    float32x4_t v473 = vfmaq_f32(v470, v465, v471);
    float32x4_t v485 = vfmaq_f32(v482, v477, v483);
    float32x4_t v535 = vfmaq_f32(v532, v527, v533);
    float32x4_t v547 = vfmaq_f32(v544, v539, v545);
    float32x4_t v597 = vfmaq_f32(v594, v589, v595);
    float32x4_t v609 = vfmaq_f32(v606, v601, v607);
    float32x4_t v659 = vfmaq_f32(v656, v651, v657);
    float32x4_t v671 = vfmaq_f32(v668, v663, v669);
    float32x4_t v679 = vaddq_f32(v2844, v51);
    float32x4_t v680 = vsubq_f32(v2844, v51);
    float32x4_t v681 = vaddq_f32(v101, v113);
    float32x4_t v682 = vsubq_f32(v101, v113);
    float32x4_t v683 = vaddq_f32(v163, v175);
    float32x4_t v684 = vsubq_f32(v163, v175);
    float32x4_t v685 = vaddq_f32(v225, v237);
    float32x4_t v686 = vsubq_f32(v225, v237);
    float32x4_t v687 = vaddq_f32(v287, v299);
    float32x4_t v688 = vsubq_f32(v287, v299);
    float32x4_t v689 = vaddq_f32(v349, v361);
    float32x4_t v690 = vsubq_f32(v349, v361);
    float32x4_t v691 = vaddq_f32(v411, v423);
    float32x4_t v692 = vsubq_f32(v411, v423);
    float32x4_t v693 = vaddq_f32(v473, v485);
    float32x4_t v694 = vsubq_f32(v473, v485);
    float32x4_t v695 = vaddq_f32(v535, v547);
    float32x4_t v696 = vsubq_f32(v535, v547);
    float32x4_t v697 = vaddq_f32(v597, v609);
    float32x4_t v698 = vsubq_f32(v597, v609);
    float32x4_t v699 = vaddq_f32(v659, v671);
    float32x4_t v700 = vsubq_f32(v659, v671);
    float32x4_t v701 = vaddq_f32(v681, v699);
    float32x4_t v702 = vaddq_f32(v683, v697);
    float32x4_t v703 = vaddq_f32(v685, v695);
    float32x4_t v704 = vaddq_f32(v687, v693);
    float32x4_t v705 = vaddq_f32(v689, v691);
    float32x4_t v706 = vsubq_f32(v681, v699);
    float32x4_t v707 = vsubq_f32(v683, v697);
    float32x4_t v708 = vsubq_f32(v685, v695);
    float32x4_t v709 = vsubq_f32(v687, v693);
    float32x4_t v710 = vsubq_f32(v689, v691);
    float32x4_t v920 = vaddq_f32(v682, v700);
    float32x4_t v921 = vaddq_f32(v684, v698);
    float32x4_t v922 = vaddq_f32(v686, v696);
    float32x4_t v923 = vaddq_f32(v688, v694);
    float32x4_t v924 = vaddq_f32(v690, v692);
    float32x4_t v925 = vsubq_f32(v682, v700);
    float32x4_t v926 = vsubq_f32(v684, v698);
    float32x4_t v927 = vsubq_f32(v686, v696);
    float32x4_t v928 = vsubq_f32(v688, v694);
    float32x4_t v929 = vsubq_f32(v690, v692);
    float32x4_t v711 = vaddq_f32(v701, v702);
    float32x4_t v712 = vaddq_f32(v703, v705);
    float32x4_t v714 = vsubq_f32(v707, v708);
    float32x4_t v715 = vaddq_f32(v706, v710);
    float32x4_t v720 = vsubq_f32(v702, v704);
    float32x4_t v721 = vsubq_f32(v701, v704);
    float32x4_t v722 = vsubq_f32(v702, v701);
    float32x4_t v723 = vsubq_f32(v705, v704);
    float32x4_t v724 = vsubq_f32(v703, v704);
    float32x4_t v725 = vsubq_f32(v705, v703);
    float32x4_t v726 = vsubq_f32(v702, v705);
    float32x4_t v727 = vsubq_f32(v701, v703);
    float32x4_t v729 = vaddq_f32(v707, v709);
    float32x4_t v730 = vsubq_f32(v706, v709);
    float32x4_t v731 = vaddq_f32(v706, v707);
    float32x4_t v732 = vsubq_f32(v709, v710);
    float32x4_t v733 = vsubq_f32(v708, v709);
    float32x4_t v734 = vsubq_f32(v708, v710);
    float32x4_t v735 = vaddq_f32(v707, v710);
    float32x4_t v736 = vsubq_f32(v706, v708);
    float32x4_t v930 = vaddq_f32(v920, v921);
    float32x4_t v931 = vaddq_f32(v922, v924);
    float32x4_t v933 = vsubq_f32(v926, v927);
    float32x4_t v934 = vaddq_f32(v925, v929);
    float32x4_t v939 = vsubq_f32(v921, v923);
    float32x4_t v940 = vsubq_f32(v920, v923);
    float32x4_t v941 = vsubq_f32(v921, v920);
    float32x4_t v942 = vsubq_f32(v924, v923);
    float32x4_t v943 = vsubq_f32(v922, v923);
    float32x4_t v944 = vsubq_f32(v924, v922);
    float32x4_t v945 = vsubq_f32(v921, v924);
    float32x4_t v946 = vsubq_f32(v920, v922);
    float32x4_t v948 = vaddq_f32(v926, v928);
    float32x4_t v949 = vsubq_f32(v925, v928);
    float32x4_t v950 = vaddq_f32(v925, v926);
    float32x4_t v951 = vsubq_f32(v928, v929);
    float32x4_t v952 = vsubq_f32(v927, v928);
    float32x4_t v953 = vsubq_f32(v927, v929);
    float32x4_t v954 = vaddq_f32(v926, v929);
    float32x4_t v955 = vsubq_f32(v925, v927);
    float32x4_t v713 = vaddq_f32(v704, v711);
    float32x4_t v718 = vsubq_f32(v714, v715);
    float32x4_t v728 = vsubq_f32(v712, v711);
    float32x4_t v737 = vaddq_f32(v714, v715);
    float32x4_t v760 = vmulq_f32(v720, v978);
    float32x4_t v765 = vmulq_f32(v721, v983);
    float32x4_t v770 = vmulq_f32(v722, v988);
    float32x4_t v775 = vmulq_f32(v723, v993);
    float32x4_t v780 = vmulq_f32(v724, v998);
    float32x4_t v785 = vmulq_f32(v725, v1003);
    float32x4_t v790 = vmulq_f32(v726, v1008);
    float32x4_t v795 = vmulq_f32(v727, v1013);
    float32x4_t v806 = vrev64q_f32(v729);
    float32x4_t v814 = vrev64q_f32(v730);
    float32x4_t v822 = vrev64q_f32(v731);
    float32x4_t v830 = vrev64q_f32(v732);
    float32x4_t v838 = vrev64q_f32(v733);
    float32x4_t v846 = vrev64q_f32(v734);
    float32x4_t v854 = vrev64q_f32(v735);
    float32x4_t v862 = vrev64q_f32(v736);
    float32x4_t v932 = vaddq_f32(v923, v930);
    float32x4_t v937 = vsubq_f32(v933, v934);
    float32x4_t v947 = vsubq_f32(v931, v930);
    float32x4_t v956 = vaddq_f32(v933, v934);
    float32x4_t v979 = vmulq_f32(v939, v978);
    float32x4_t v984 = vmulq_f32(v940, v983);
    float32x4_t v989 = vmulq_f32(v941, v988);
    float32x4_t v994 = vmulq_f32(v942, v993);
    float32x4_t v999 = vmulq_f32(v943, v998);
    float32x4_t v1004 = vmulq_f32(v944, v1003);
    float32x4_t v1009 = vmulq_f32(v945, v1008);
    float32x4_t v1014 = vmulq_f32(v946, v1013);
    float32x4_t v1025 = vrev64q_f32(v948);
    float32x4_t v1033 = vrev64q_f32(v949);
    float32x4_t v1041 = vrev64q_f32(v950);
    float32x4_t v1049 = vrev64q_f32(v951);
    float32x4_t v1057 = vrev64q_f32(v952);
    float32x4_t v1065 = vrev64q_f32(v953);
    float32x4_t v1073 = vrev64q_f32(v954);
    float32x4_t v1081 = vrev64q_f32(v955);
    float32x4_t v716 = vaddq_f32(v713, v712);
    float32x4_t v719 = vsubq_f32(v718, v709);
    float32x4_t v800 = vmulq_f32(v728, v1018);
    float32x4_t v808 = vmulq_f32(v806, v1026);
    float32x4_t v816 = vmulq_f32(v814, v1034);
    float32x4_t v824 = vmulq_f32(v822, v1042);
    float32x4_t v832 = vmulq_f32(v830, v1050);
    float32x4_t v840 = vmulq_f32(v838, v1058);
    float32x4_t v848 = vmulq_f32(v846, v1066);
    float32x4_t v856 = vmulq_f32(v854, v1074);
    float32x4_t v864 = vmulq_f32(v862, v1082);
    float32x4_t v870 = vrev64q_f32(v737);
    float32x4_t v874 = vaddq_f32(v760, v765);
    float32x4_t v875 = vaddq_f32(v765, v770);
    float32x4_t v876 = vsubq_f32(v760, v770);
    float32x4_t v877 = vaddq_f32(v775, v780);
    float32x4_t v878 = vaddq_f32(v780, v785);
    float32x4_t v879 = vsubq_f32(v775, v785);
    float32x4_t v935 = vaddq_f32(v932, v931);
    float32x4_t v938 = vsubq_f32(v937, v928);
    float32x4_t v1019 = vmulq_f32(v947, v1018);
    float32x4_t v1027 = vmulq_f32(v1025, v1026);
    float32x4_t v1035 = vmulq_f32(v1033, v1034);
    float32x4_t v1043 = vmulq_f32(v1041, v1042);
    float32x4_t v1051 = vmulq_f32(v1049, v1050);
    float32x4_t v1059 = vmulq_f32(v1057, v1058);
    float32x4_t v1067 = vmulq_f32(v1065, v1066);
    float32x4_t v1075 = vmulq_f32(v1073, v1074);
    float32x4_t v1083 = vmulq_f32(v1081, v1082);
    float32x4_t v1089 = vrev64q_f32(v956);
    float32x4_t v1093 = vaddq_f32(v979, v984);
    float32x4_t v1094 = vaddq_f32(v984, v989);
    float32x4_t v1095 = vsubq_f32(v979, v989);
    float32x4_t v1096 = vaddq_f32(v994, v999);
    float32x4_t v1097 = vaddq_f32(v999, v1004);
    float32x4_t v1098 = vsubq_f32(v994, v1004);
    float32x4_t v717 = vaddq_f32(v679, v716);
    float32x4_t v747 = vmulq_f32(v716, v965);
    float32x4_t v753 = vrev64q_f32(v719);
    float32x4_t v872 = vmulq_f32(v870, v1090);
    float32x4_t v880 = vaddq_f32(v795, v800);
    float32x4_t v881 = vaddq_f32(v790, v800);
    float32x4_t v882 = vaddq_f32(v816, v824);
    float32x4_t v883 = vsubq_f32(v808, v824);
    float32x4_t v884 = vaddq_f32(v840, v848);
    float32x4_t v885 = vsubq_f32(v832, v848);
    float32x4_t v936 = vaddq_f32(v680, v935);
    float32x4_t v966 = vmulq_f32(v935, v965);
    float32x4_t v972 = vrev64q_f32(v938);
    float32x4_t v1091 = vmulq_f32(v1089, v1090);
    float32x4_t v1099 = vaddq_f32(v1014, v1019);
    float32x4_t v1100 = vaddq_f32(v1009, v1019);
    float32x4_t v1101 = vaddq_f32(v1035, v1043);
    float32x4_t v1102 = vsubq_f32(v1027, v1043);
    float32x4_t v1103 = vaddq_f32(v1059, v1067);
    float32x4_t v1104 = vsubq_f32(v1051, v1067);
    float32x4_t v755 = vmulq_f32(v753, v973);
    float32x4_t v873 = vsubq_f32(v717, v747);
    float32x4_t v886 = vaddq_f32(v864, v872);
    float32x4_t v887 = vsubq_f32(v856, v872);
    float32x4_t v888 = vaddq_f32(v878, v880);
    float32x4_t v906 = vaddq_f32(v882, v883);
    float32x4_t v974 = vmulq_f32(v972, v973);
    float32x4_t v1092 = vsubq_f32(v936, v966);
    float32x4_t v1105 = vaddq_f32(v1083, v1091);
    float32x4_t v1106 = vsubq_f32(v1075, v1091);
    float32x4_t v1107 = vaddq_f32(v1097, v1099);
    float32x4_t v1125 = vaddq_f32(v1101, v1102);
    vst1q_f32((float32_t *)v2609, v717);
    vst1q_f32((float32_t *)v2618, v936);
    float32x4_t v889 = vaddq_f32(v888, v873);
    float32x4_t v890 = vsubq_f32(v873, v875);
    float32x4_t v892 = vaddq_f32(v873, v879);
    float32x4_t v894 = vsubq_f32(v873, v876);
    float32x4_t v896 = vaddq_f32(v873, v874);
    float32x4_t v898 = vaddq_f32(v755, v884);
    float32x4_t v900 = vsubq_f32(v886, v882);
    float32x4_t v902 = vaddq_f32(v755, v887);
    float32x4_t v904 = vsubq_f32(v887, v883);
    float32x4_t v907 = vaddq_f32(v906, v884);
    float32x4_t v1108 = vaddq_f32(v1107, v1092);
    float32x4_t v1109 = vsubq_f32(v1092, v1094);
    float32x4_t v1111 = vaddq_f32(v1092, v1098);
    float32x4_t v1113 = vsubq_f32(v1092, v1095);
    float32x4_t v1115 = vaddq_f32(v1092, v1093);
    float32x4_t v1117 = vaddq_f32(v974, v1103);
    float32x4_t v1119 = vsubq_f32(v1105, v1101);
    float32x4_t v1121 = vaddq_f32(v974, v1106);
    float32x4_t v1123 = vsubq_f32(v1106, v1102);
    float32x4_t v1126 = vaddq_f32(v1125, v1103);
    float32x4_t v891 = vsubq_f32(v890, v880);
    float32x4_t v893 = vaddq_f32(v892, v881);
    float32x4_t v895 = vsubq_f32(v894, v881);
    float32x4_t v897 = vsubq_f32(v896, v877);
    float32x4_t v899 = vaddq_f32(v898, v886);
    float32x4_t v901 = vsubq_f32(v900, v755);
    float32x4_t v903 = vaddq_f32(v902, v885);
    float32x4_t v905 = vsubq_f32(v904, v755);
    float32x4_t v908 = vaddq_f32(v907, v885);
    float32x4_t v1110 = vsubq_f32(v1109, v1099);
    float32x4_t v1112 = vaddq_f32(v1111, v1100);
    float32x4_t v1114 = vsubq_f32(v1113, v1100);
    float32x4_t v1116 = vsubq_f32(v1115, v1096);
    float32x4_t v1118 = vaddq_f32(v1117, v1105);
    float32x4_t v1120 = vsubq_f32(v1119, v974);
    float32x4_t v1122 = vaddq_f32(v1121, v1104);
    float32x4_t v1124 = vsubq_f32(v1123, v974);
    float32x4_t v1127 = vaddq_f32(v1126, v1104);
    float32x4_t v909 = vsubq_f32(v908, v755);
    float32x4_t v911 = vaddq_f32(v889, v899);
    float32x4_t v912 = vaddq_f32(v891, v901);
    float32x4_t v913 = vsubq_f32(v893, v903);
    float32x4_t v914 = vaddq_f32(v895, v905);
    float32x4_t v915 = vsubq_f32(v895, v905);
    float32x4_t v916 = vaddq_f32(v893, v903);
    float32x4_t v917 = vsubq_f32(v891, v901);
    float32x4_t v918 = vsubq_f32(v889, v899);
    float32x4_t v1128 = vsubq_f32(v1127, v974);
    float32x4_t v1130 = vaddq_f32(v1108, v1118);
    float32x4_t v1131 = vaddq_f32(v1110, v1120);
    float32x4_t v1132 = vsubq_f32(v1112, v1122);
    float32x4_t v1133 = vaddq_f32(v1114, v1124);
    float32x4_t v1134 = vsubq_f32(v1114, v1124);
    float32x4_t v1135 = vaddq_f32(v1112, v1122);
    float32x4_t v1136 = vsubq_f32(v1110, v1120);
    float32x4_t v1137 = vsubq_f32(v1108, v1118);
    float32x4_t v910 = vaddq_f32(v897, v909);
    float32x4_t v919 = vsubq_f32(v897, v909);
    float32x4_t v1129 = vaddq_f32(v1116, v1128);
    float32x4_t v1138 = vsubq_f32(v1116, v1128);
    vst1q_f32((float32_t *)v2645, v918);
    vst1q_f32((float32_t *)v2654, v1137);
    vst1q_f32((float32_t *)v2663, v917);
    vst1q_f32((float32_t *)v2672, v1136);
    vst1q_f32((float32_t *)v2681, v916);
    vst1q_f32((float32_t *)v2690, v1135);
    vst1q_f32((float32_t *)v2699, v915);
    vst1q_f32((float32_t *)v2708, v1134);
    vst1q_f32((float32_t *)v2717, v914);
    vst1q_f32((float32_t *)v2726, v1133);
    vst1q_f32((float32_t *)v2735, v913);
    vst1q_f32((float32_t *)v2744, v1132);
    vst1q_f32((float32_t *)v2753, v912);
    vst1q_f32((float32_t *)v2762, v1131);
    vst1q_f32((float32_t *)v2771, v911);
    vst1q_f32((float32_t *)v2780, v1130);
    vst1q_f32((float32_t *)v2627, v919);
    vst1q_f32((float32_t *)v2636, v1138);
    vst1q_f32((float32_t *)v2789, v910);
    vst1q_f32((float32_t *)v2798, v1129);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v1293 * 2; j < howmany; j += 1) {
    float32x2_t v1595 = v5[istride];
    float v2093 = 1.1000000000000001e+00F;
    float v2096 = 3.3166247903554003e-01F;
    float v2097 = -3.3166247903554003e-01F;
    float v2104 = 5.1541501300188641e-01F;
    float v2108 = 9.4125353283118118e-01F;
    float v2112 = 1.4143537075597825e+00F;
    float v2116 = 8.5949297361449750e-01F;
    float v2120 = 4.2314838273285138e-02F;
    float v2124 = 3.8639279888589606e-01F;
    float v2128 = 5.1254589567200015e-01F;
    float v2132 = 1.0702757469471715e+00F;
    float v2136 = 5.5486073394528512e-01F;
    float v2139 = 1.2412944743900585e+00F;
    float v2140 = -1.2412944743900585e+00F;
    float v2146 = 2.0897833842005756e-01F;
    float v2147 = -2.0897833842005756e-01F;
    float v2153 = 3.7415717312460811e-01F;
    float v2154 = -3.7415717312460811e-01F;
    float v2160 = 4.9929922194110327e-02F;
    float v2161 = -4.9929922194110327e-02F;
    float v2167 = 6.5815896284539266e-01F;
    float v2168 = -6.5815896284539266e-01F;
    float v2174 = 6.3306543373877577e-01F;
    float v2175 = -6.3306543373877577e-01F;
    float v2181 = 1.0822460581641109e+00F;
    float v2182 = -1.0822460581641109e+00F;
    float v2188 = 8.1720737907134022e-01F;
    float v2189 = -8.1720737907134022e-01F;
    float v2195 = 4.2408709531871824e-01F;
    float v2196 = -4.2408709531871824e-01F;
    float32x2_t v2198 = (float32x2_t){v4, v4};
    float32x2_t v1317 = v7[20];
    float32x2_t v1322 = v7[21];
    float32x2_t v1357 = v7[2];
    float32x2_t v1362 = v7[3];
    float32x2_t v1367 = v7[24];
    float32x2_t v1372 = v7[25];
    float32x2_t v1407 = v7[6];
    float32x2_t v1412 = v7[7];
    float32x2_t v1417 = v7[28];
    float32x2_t v1422 = v7[29];
    float32x2_t v1457 = v7[10];
    float32x2_t v1462 = v7[11];
    float32x2_t v1467 = v7[32];
    float32x2_t v1472 = v7[33];
    float32x2_t v1507 = v7[14];
    float32x2_t v1512 = v7[15];
    float32x2_t v1517 = v7[36];
    float32x2_t v1522 = v7[37];
    float32x2_t v1557 = v7[18];
    float32x2_t v1562 = v7[19];
    float32x2_t v1567 = v7[40];
    float32x2_t v1572 = v7[41];
    float32x2_t v1607 = v7[22];
    float32x2_t v1612 = v7[23];
    float32x2_t v1617 = v7[0];
    float32x2_t v1618 = vtrn1_f32(v1595, v1595);
    float32x2_t v1619 = vtrn2_f32(v1595, v1595);
    float32x2_t v1622 = v7[1];
    float32x2_t v1657 = v7[26];
    float32x2_t v1662 = v7[27];
    float32x2_t v1667 = v7[4];
    float32x2_t v1672 = v7[5];
    float32x2_t v1707 = v7[30];
    float32x2_t v1712 = v7[31];
    float32x2_t v1717 = v7[8];
    float32x2_t v1722 = v7[9];
    float32x2_t v1757 = v7[34];
    float32x2_t v1762 = v7[35];
    float32x2_t v1767 = v7[12];
    float32x2_t v1772 = v7[13];
    float32x2_t v1807 = v7[38];
    float32x2_t v1812 = v7[39];
    float32x2_t v1817 = v7[16];
    float32x2_t v1822 = v7[17];
    float32x2_t v1830 = v5[0];
    float32x2_t v2094 = (float32x2_t){v2093, v2093};
    float32x2_t v2098 = (float32x2_t){v2096, v2097};
    float32x2_t v2105 = (float32x2_t){v2104, v2104};
    float32x2_t v2109 = (float32x2_t){v2108, v2108};
    float32x2_t v2113 = (float32x2_t){v2112, v2112};
    float32x2_t v2117 = (float32x2_t){v2116, v2116};
    float32x2_t v2121 = (float32x2_t){v2120, v2120};
    float32x2_t v2125 = (float32x2_t){v2124, v2124};
    float32x2_t v2129 = (float32x2_t){v2128, v2128};
    float32x2_t v2133 = (float32x2_t){v2132, v2132};
    float32x2_t v2137 = (float32x2_t){v2136, v2136};
    float32x2_t v2141 = (float32x2_t){v2139, v2140};
    float32x2_t v2148 = (float32x2_t){v2146, v2147};
    float32x2_t v2155 = (float32x2_t){v2153, v2154};
    float32x2_t v2162 = (float32x2_t){v2160, v2161};
    float32x2_t v2169 = (float32x2_t){v2167, v2168};
    float32x2_t v2176 = (float32x2_t){v2174, v2175};
    float32x2_t v2183 = (float32x2_t){v2181, v2182};
    float32x2_t v2190 = (float32x2_t){v2188, v2189};
    float32x2_t v2197 = (float32x2_t){v2195, v2196};
    float32x2_t v1305 = v5[istride * 11];
    float32x2_t v1330 = v5[istride * 2];
    float32x2_t v1345 = v5[istride * 13];
    float32x2_t v1380 = v5[istride * 4];
    float32x2_t v1395 = v5[istride * 15];
    float32x2_t v1430 = v5[istride * 6];
    float32x2_t v1445 = v5[istride * 17];
    float32x2_t v1480 = v5[istride * 8];
    float32x2_t v1495 = v5[istride * 19];
    float32x2_t v1530 = v5[istride * 10];
    float32x2_t v1545 = v5[istride * 21];
    float32x2_t v1580 = v5[istride * 12];
    float32x2_t v1623 = vmul_f32(v1618, v1617);
    float32x2_t v1630 = v5[istride * 14];
    float32x2_t v1645 = v5[istride * 3];
    float32x2_t v1680 = v5[istride * 16];
    float32x2_t v1695 = v5[istride * 5];
    float32x2_t v1730 = v5[istride * 18];
    float32x2_t v1745 = v5[istride * 7];
    float32x2_t v1780 = v5[istride * 20];
    float32x2_t v1795 = v5[istride * 9];
    float32x2_t v2100 = vmul_f32(v2198, v2098);
    float32x2_t v2143 = vmul_f32(v2198, v2141);
    float32x2_t v2150 = vmul_f32(v2198, v2148);
    float32x2_t v2157 = vmul_f32(v2198, v2155);
    float32x2_t v2164 = vmul_f32(v2198, v2162);
    float32x2_t v2171 = vmul_f32(v2198, v2169);
    float32x2_t v2178 = vmul_f32(v2198, v2176);
    float32x2_t v2185 = vmul_f32(v2198, v2183);
    float32x2_t v2192 = vmul_f32(v2198, v2190);
    float32x2_t v2199 = vmul_f32(v2198, v2197);
    float32x2_t v1318 = vtrn1_f32(v1305, v1305);
    float32x2_t v1319 = vtrn2_f32(v1305, v1305);
    float32x2_t v1358 = vtrn1_f32(v1330, v1330);
    float32x2_t v1359 = vtrn2_f32(v1330, v1330);
    float32x2_t v1368 = vtrn1_f32(v1345, v1345);
    float32x2_t v1369 = vtrn2_f32(v1345, v1345);
    float32x2_t v1408 = vtrn1_f32(v1380, v1380);
    float32x2_t v1409 = vtrn2_f32(v1380, v1380);
    float32x2_t v1418 = vtrn1_f32(v1395, v1395);
    float32x2_t v1419 = vtrn2_f32(v1395, v1395);
    float32x2_t v1458 = vtrn1_f32(v1430, v1430);
    float32x2_t v1459 = vtrn2_f32(v1430, v1430);
    float32x2_t v1468 = vtrn1_f32(v1445, v1445);
    float32x2_t v1469 = vtrn2_f32(v1445, v1445);
    float32x2_t v1508 = vtrn1_f32(v1480, v1480);
    float32x2_t v1509 = vtrn2_f32(v1480, v1480);
    float32x2_t v1518 = vtrn1_f32(v1495, v1495);
    float32x2_t v1519 = vtrn2_f32(v1495, v1495);
    float32x2_t v1558 = vtrn1_f32(v1530, v1530);
    float32x2_t v1559 = vtrn2_f32(v1530, v1530);
    float32x2_t v1568 = vtrn1_f32(v1545, v1545);
    float32x2_t v1569 = vtrn2_f32(v1545, v1545);
    float32x2_t v1608 = vtrn1_f32(v1580, v1580);
    float32x2_t v1609 = vtrn2_f32(v1580, v1580);
    float32x2_t v1625 = vfma_f32(v1623, v1619, v1622);
    float32x2_t v1658 = vtrn1_f32(v1630, v1630);
    float32x2_t v1659 = vtrn2_f32(v1630, v1630);
    float32x2_t v1668 = vtrn1_f32(v1645, v1645);
    float32x2_t v1669 = vtrn2_f32(v1645, v1645);
    float32x2_t v1708 = vtrn1_f32(v1680, v1680);
    float32x2_t v1709 = vtrn2_f32(v1680, v1680);
    float32x2_t v1718 = vtrn1_f32(v1695, v1695);
    float32x2_t v1719 = vtrn2_f32(v1695, v1695);
    float32x2_t v1758 = vtrn1_f32(v1730, v1730);
    float32x2_t v1759 = vtrn2_f32(v1730, v1730);
    float32x2_t v1768 = vtrn1_f32(v1745, v1745);
    float32x2_t v1769 = vtrn2_f32(v1745, v1745);
    float32x2_t v1808 = vtrn1_f32(v1780, v1780);
    float32x2_t v1809 = vtrn2_f32(v1780, v1780);
    float32x2_t v1818 = vtrn1_f32(v1795, v1795);
    float32x2_t v1819 = vtrn2_f32(v1795, v1795);
    float32x2_t v1323 = vmul_f32(v1318, v1317);
    float32x2_t v1363 = vmul_f32(v1358, v1357);
    float32x2_t v1373 = vmul_f32(v1368, v1367);
    float32x2_t v1413 = vmul_f32(v1408, v1407);
    float32x2_t v1423 = vmul_f32(v1418, v1417);
    float32x2_t v1463 = vmul_f32(v1458, v1457);
    float32x2_t v1473 = vmul_f32(v1468, v1467);
    float32x2_t v1513 = vmul_f32(v1508, v1507);
    float32x2_t v1523 = vmul_f32(v1518, v1517);
    float32x2_t v1563 = vmul_f32(v1558, v1557);
    float32x2_t v1573 = vmul_f32(v1568, v1567);
    float32x2_t v1613 = vmul_f32(v1608, v1607);
    float32x2_t v1663 = vmul_f32(v1658, v1657);
    float32x2_t v1673 = vmul_f32(v1668, v1667);
    float32x2_t v1713 = vmul_f32(v1708, v1707);
    float32x2_t v1723 = vmul_f32(v1718, v1717);
    float32x2_t v1763 = vmul_f32(v1758, v1757);
    float32x2_t v1773 = vmul_f32(v1768, v1767);
    float32x2_t v1813 = vmul_f32(v1808, v1807);
    float32x2_t v1823 = vmul_f32(v1818, v1817);
    float32x2_t v1325 = vfma_f32(v1323, v1319, v1322);
    float32x2_t v1365 = vfma_f32(v1363, v1359, v1362);
    float32x2_t v1375 = vfma_f32(v1373, v1369, v1372);
    float32x2_t v1415 = vfma_f32(v1413, v1409, v1412);
    float32x2_t v1425 = vfma_f32(v1423, v1419, v1422);
    float32x2_t v1465 = vfma_f32(v1463, v1459, v1462);
    float32x2_t v1475 = vfma_f32(v1473, v1469, v1472);
    float32x2_t v1515 = vfma_f32(v1513, v1509, v1512);
    float32x2_t v1525 = vfma_f32(v1523, v1519, v1522);
    float32x2_t v1565 = vfma_f32(v1563, v1559, v1562);
    float32x2_t v1575 = vfma_f32(v1573, v1569, v1572);
    float32x2_t v1615 = vfma_f32(v1613, v1609, v1612);
    float32x2_t v1665 = vfma_f32(v1663, v1659, v1662);
    float32x2_t v1675 = vfma_f32(v1673, v1669, v1672);
    float32x2_t v1715 = vfma_f32(v1713, v1709, v1712);
    float32x2_t v1725 = vfma_f32(v1723, v1719, v1722);
    float32x2_t v1765 = vfma_f32(v1763, v1759, v1762);
    float32x2_t v1775 = vfma_f32(v1773, v1769, v1772);
    float32x2_t v1815 = vfma_f32(v1813, v1809, v1812);
    float32x2_t v1825 = vfma_f32(v1823, v1819, v1822);
    float32x2_t v1831 = vadd_f32(v1830, v1325);
    float32x2_t v1832 = vsub_f32(v1830, v1325);
    float32x2_t v1833 = vadd_f32(v1365, v1375);
    float32x2_t v1834 = vsub_f32(v1365, v1375);
    float32x2_t v1835 = vadd_f32(v1415, v1425);
    float32x2_t v1836 = vsub_f32(v1415, v1425);
    float32x2_t v1837 = vadd_f32(v1465, v1475);
    float32x2_t v1838 = vsub_f32(v1465, v1475);
    float32x2_t v1839 = vadd_f32(v1515, v1525);
    float32x2_t v1840 = vsub_f32(v1515, v1525);
    float32x2_t v1841 = vadd_f32(v1565, v1575);
    float32x2_t v1842 = vsub_f32(v1565, v1575);
    float32x2_t v1843 = vadd_f32(v1615, v1625);
    float32x2_t v1844 = vsub_f32(v1615, v1625);
    float32x2_t v1845 = vadd_f32(v1665, v1675);
    float32x2_t v1846 = vsub_f32(v1665, v1675);
    float32x2_t v1847 = vadd_f32(v1715, v1725);
    float32x2_t v1848 = vsub_f32(v1715, v1725);
    float32x2_t v1849 = vadd_f32(v1765, v1775);
    float32x2_t v1850 = vsub_f32(v1765, v1775);
    float32x2_t v1851 = vadd_f32(v1815, v1825);
    float32x2_t v1852 = vsub_f32(v1815, v1825);
    float32x2_t v1853 = vadd_f32(v1833, v1851);
    float32x2_t v1854 = vadd_f32(v1835, v1849);
    float32x2_t v1855 = vadd_f32(v1837, v1847);
    float32x2_t v1856 = vadd_f32(v1839, v1845);
    float32x2_t v1857 = vadd_f32(v1841, v1843);
    float32x2_t v1858 = vsub_f32(v1833, v1851);
    float32x2_t v1859 = vsub_f32(v1835, v1849);
    float32x2_t v1860 = vsub_f32(v1837, v1847);
    float32x2_t v1861 = vsub_f32(v1839, v1845);
    float32x2_t v1862 = vsub_f32(v1841, v1843);
    float32x2_t v2051 = vadd_f32(v1834, v1852);
    float32x2_t v2052 = vadd_f32(v1836, v1850);
    float32x2_t v2053 = vadd_f32(v1838, v1848);
    float32x2_t v2054 = vadd_f32(v1840, v1846);
    float32x2_t v2055 = vadd_f32(v1842, v1844);
    float32x2_t v2056 = vsub_f32(v1834, v1852);
    float32x2_t v2057 = vsub_f32(v1836, v1850);
    float32x2_t v2058 = vsub_f32(v1838, v1848);
    float32x2_t v2059 = vsub_f32(v1840, v1846);
    float32x2_t v2060 = vsub_f32(v1842, v1844);
    float32x2_t v1863 = vadd_f32(v1853, v1854);
    float32x2_t v1864 = vadd_f32(v1855, v1857);
    float32x2_t v1866 = vsub_f32(v1859, v1860);
    float32x2_t v1867 = vadd_f32(v1858, v1862);
    float32x2_t v1872 = vsub_f32(v1854, v1856);
    float32x2_t v1873 = vsub_f32(v1853, v1856);
    float32x2_t v1874 = vsub_f32(v1854, v1853);
    float32x2_t v1875 = vsub_f32(v1857, v1856);
    float32x2_t v1876 = vsub_f32(v1855, v1856);
    float32x2_t v1877 = vsub_f32(v1857, v1855);
    float32x2_t v1878 = vsub_f32(v1854, v1857);
    float32x2_t v1879 = vsub_f32(v1853, v1855);
    float32x2_t v1881 = vadd_f32(v1859, v1861);
    float32x2_t v1882 = vsub_f32(v1858, v1861);
    float32x2_t v1883 = vadd_f32(v1858, v1859);
    float32x2_t v1884 = vsub_f32(v1861, v1862);
    float32x2_t v1885 = vsub_f32(v1860, v1861);
    float32x2_t v1886 = vsub_f32(v1860, v1862);
    float32x2_t v1887 = vadd_f32(v1859, v1862);
    float32x2_t v1888 = vsub_f32(v1858, v1860);
    float32x2_t v2061 = vadd_f32(v2051, v2052);
    float32x2_t v2062 = vadd_f32(v2053, v2055);
    float32x2_t v2064 = vsub_f32(v2057, v2058);
    float32x2_t v2065 = vadd_f32(v2056, v2060);
    float32x2_t v2070 = vsub_f32(v2052, v2054);
    float32x2_t v2071 = vsub_f32(v2051, v2054);
    float32x2_t v2072 = vsub_f32(v2052, v2051);
    float32x2_t v2073 = vsub_f32(v2055, v2054);
    float32x2_t v2074 = vsub_f32(v2053, v2054);
    float32x2_t v2075 = vsub_f32(v2055, v2053);
    float32x2_t v2076 = vsub_f32(v2052, v2055);
    float32x2_t v2077 = vsub_f32(v2051, v2053);
    float32x2_t v2079 = vadd_f32(v2057, v2059);
    float32x2_t v2080 = vsub_f32(v2056, v2059);
    float32x2_t v2081 = vadd_f32(v2056, v2057);
    float32x2_t v2082 = vsub_f32(v2059, v2060);
    float32x2_t v2083 = vsub_f32(v2058, v2059);
    float32x2_t v2084 = vsub_f32(v2058, v2060);
    float32x2_t v2085 = vadd_f32(v2057, v2060);
    float32x2_t v2086 = vsub_f32(v2056, v2058);
    float32x2_t v1865 = vadd_f32(v1856, v1863);
    float32x2_t v1870 = vsub_f32(v1866, v1867);
    float32x2_t v1880 = vsub_f32(v1864, v1863);
    float32x2_t v1889 = vadd_f32(v1866, v1867);
    float32x2_t v1908 = vmul_f32(v1872, v2105);
    float32x2_t v1912 = vmul_f32(v1873, v2109);
    float32x2_t v1916 = vmul_f32(v1874, v2113);
    float32x2_t v1920 = vmul_f32(v1875, v2117);
    float32x2_t v1924 = vmul_f32(v1876, v2121);
    float32x2_t v1928 = vmul_f32(v1877, v2125);
    float32x2_t v1932 = vmul_f32(v1878, v2129);
    float32x2_t v1936 = vmul_f32(v1879, v2133);
    float32x2_t v1946 = vrev64_f32(v1881);
    float32x2_t v1953 = vrev64_f32(v1882);
    float32x2_t v1960 = vrev64_f32(v1883);
    float32x2_t v1967 = vrev64_f32(v1884);
    float32x2_t v1974 = vrev64_f32(v1885);
    float32x2_t v1981 = vrev64_f32(v1886);
    float32x2_t v1988 = vrev64_f32(v1887);
    float32x2_t v1995 = vrev64_f32(v1888);
    float32x2_t v2063 = vadd_f32(v2054, v2061);
    float32x2_t v2068 = vsub_f32(v2064, v2065);
    float32x2_t v2078 = vsub_f32(v2062, v2061);
    float32x2_t v2087 = vadd_f32(v2064, v2065);
    float32x2_t v2106 = vmul_f32(v2070, v2105);
    float32x2_t v2110 = vmul_f32(v2071, v2109);
    float32x2_t v2114 = vmul_f32(v2072, v2113);
    float32x2_t v2118 = vmul_f32(v2073, v2117);
    float32x2_t v2122 = vmul_f32(v2074, v2121);
    float32x2_t v2126 = vmul_f32(v2075, v2125);
    float32x2_t v2130 = vmul_f32(v2076, v2129);
    float32x2_t v2134 = vmul_f32(v2077, v2133);
    float32x2_t v2144 = vrev64_f32(v2079);
    float32x2_t v2151 = vrev64_f32(v2080);
    float32x2_t v2158 = vrev64_f32(v2081);
    float32x2_t v2165 = vrev64_f32(v2082);
    float32x2_t v2172 = vrev64_f32(v2083);
    float32x2_t v2179 = vrev64_f32(v2084);
    float32x2_t v2186 = vrev64_f32(v2085);
    float32x2_t v2193 = vrev64_f32(v2086);
    float32x2_t v1868 = vadd_f32(v1865, v1864);
    float32x2_t v1871 = vsub_f32(v1870, v1861);
    float32x2_t v1940 = vmul_f32(v1880, v2137);
    float32x2_t v1947 = vmul_f32(v1946, v2143);
    float32x2_t v1954 = vmul_f32(v1953, v2150);
    float32x2_t v1961 = vmul_f32(v1960, v2157);
    float32x2_t v1968 = vmul_f32(v1967, v2164);
    float32x2_t v1975 = vmul_f32(v1974, v2171);
    float32x2_t v1982 = vmul_f32(v1981, v2178);
    float32x2_t v1989 = vmul_f32(v1988, v2185);
    float32x2_t v1996 = vmul_f32(v1995, v2192);
    float32x2_t v2002 = vrev64_f32(v1889);
    float32x2_t v2005 = vadd_f32(v1908, v1912);
    float32x2_t v2006 = vadd_f32(v1912, v1916);
    float32x2_t v2007 = vsub_f32(v1908, v1916);
    float32x2_t v2008 = vadd_f32(v1920, v1924);
    float32x2_t v2009 = vadd_f32(v1924, v1928);
    float32x2_t v2010 = vsub_f32(v1920, v1928);
    float32x2_t v2066 = vadd_f32(v2063, v2062);
    float32x2_t v2069 = vsub_f32(v2068, v2059);
    float32x2_t v2138 = vmul_f32(v2078, v2137);
    float32x2_t v2145 = vmul_f32(v2144, v2143);
    float32x2_t v2152 = vmul_f32(v2151, v2150);
    float32x2_t v2159 = vmul_f32(v2158, v2157);
    float32x2_t v2166 = vmul_f32(v2165, v2164);
    float32x2_t v2173 = vmul_f32(v2172, v2171);
    float32x2_t v2180 = vmul_f32(v2179, v2178);
    float32x2_t v2187 = vmul_f32(v2186, v2185);
    float32x2_t v2194 = vmul_f32(v2193, v2192);
    float32x2_t v2200 = vrev64_f32(v2087);
    float32x2_t v2203 = vadd_f32(v2106, v2110);
    float32x2_t v2204 = vadd_f32(v2110, v2114);
    float32x2_t v2205 = vsub_f32(v2106, v2114);
    float32x2_t v2206 = vadd_f32(v2118, v2122);
    float32x2_t v2207 = vadd_f32(v2122, v2126);
    float32x2_t v2208 = vsub_f32(v2118, v2126);
    float32x2_t v1869 = vadd_f32(v1831, v1868);
    float32x2_t v1897 = vmul_f32(v1868, v2094);
    float32x2_t v1903 = vrev64_f32(v1871);
    float32x2_t v2003 = vmul_f32(v2002, v2199);
    float32x2_t v2011 = vadd_f32(v1936, v1940);
    float32x2_t v2012 = vadd_f32(v1932, v1940);
    float32x2_t v2013 = vadd_f32(v1954, v1961);
    float32x2_t v2014 = vsub_f32(v1947, v1961);
    float32x2_t v2015 = vadd_f32(v1975, v1982);
    float32x2_t v2016 = vsub_f32(v1968, v1982);
    float32x2_t v2067 = vadd_f32(v1832, v2066);
    float32x2_t v2095 = vmul_f32(v2066, v2094);
    float32x2_t v2101 = vrev64_f32(v2069);
    float32x2_t v2201 = vmul_f32(v2200, v2199);
    float32x2_t v2209 = vadd_f32(v2134, v2138);
    float32x2_t v2210 = vadd_f32(v2130, v2138);
    float32x2_t v2211 = vadd_f32(v2152, v2159);
    float32x2_t v2212 = vsub_f32(v2145, v2159);
    float32x2_t v2213 = vadd_f32(v2173, v2180);
    float32x2_t v2214 = vsub_f32(v2166, v2180);
    float32x2_t v1904 = vmul_f32(v1903, v2100);
    float32x2_t v2004 = vsub_f32(v1869, v1897);
    float32x2_t v2017 = vadd_f32(v1996, v2003);
    float32x2_t v2018 = vsub_f32(v1989, v2003);
    float32x2_t v2019 = vadd_f32(v2009, v2011);
    float32x2_t v2037 = vadd_f32(v2013, v2014);
    float32x2_t v2102 = vmul_f32(v2101, v2100);
    float32x2_t v2202 = vsub_f32(v2067, v2095);
    float32x2_t v2215 = vadd_f32(v2194, v2201);
    float32x2_t v2216 = vsub_f32(v2187, v2201);
    float32x2_t v2217 = vadd_f32(v2207, v2209);
    float32x2_t v2235 = vadd_f32(v2211, v2212);
    v6[0] = v1869;
    v6[ostride * 11] = v2067;
    float32x2_t v2020 = vadd_f32(v2019, v2004);
    float32x2_t v2021 = vsub_f32(v2004, v2006);
    float32x2_t v2023 = vadd_f32(v2004, v2010);
    float32x2_t v2025 = vsub_f32(v2004, v2007);
    float32x2_t v2027 = vadd_f32(v2004, v2005);
    float32x2_t v2029 = vadd_f32(v1904, v2015);
    float32x2_t v2031 = vsub_f32(v2017, v2013);
    float32x2_t v2033 = vadd_f32(v1904, v2018);
    float32x2_t v2035 = vsub_f32(v2018, v2014);
    float32x2_t v2038 = vadd_f32(v2037, v2015);
    float32x2_t v2218 = vadd_f32(v2217, v2202);
    float32x2_t v2219 = vsub_f32(v2202, v2204);
    float32x2_t v2221 = vadd_f32(v2202, v2208);
    float32x2_t v2223 = vsub_f32(v2202, v2205);
    float32x2_t v2225 = vadd_f32(v2202, v2203);
    float32x2_t v2227 = vadd_f32(v2102, v2213);
    float32x2_t v2229 = vsub_f32(v2215, v2211);
    float32x2_t v2231 = vadd_f32(v2102, v2216);
    float32x2_t v2233 = vsub_f32(v2216, v2212);
    float32x2_t v2236 = vadd_f32(v2235, v2213);
    float32x2_t v2022 = vsub_f32(v2021, v2011);
    float32x2_t v2024 = vadd_f32(v2023, v2012);
    float32x2_t v2026 = vsub_f32(v2025, v2012);
    float32x2_t v2028 = vsub_f32(v2027, v2008);
    float32x2_t v2030 = vadd_f32(v2029, v2017);
    float32x2_t v2032 = vsub_f32(v2031, v1904);
    float32x2_t v2034 = vadd_f32(v2033, v2016);
    float32x2_t v2036 = vsub_f32(v2035, v1904);
    float32x2_t v2039 = vadd_f32(v2038, v2016);
    float32x2_t v2220 = vsub_f32(v2219, v2209);
    float32x2_t v2222 = vadd_f32(v2221, v2210);
    float32x2_t v2224 = vsub_f32(v2223, v2210);
    float32x2_t v2226 = vsub_f32(v2225, v2206);
    float32x2_t v2228 = vadd_f32(v2227, v2215);
    float32x2_t v2230 = vsub_f32(v2229, v2102);
    float32x2_t v2232 = vadd_f32(v2231, v2214);
    float32x2_t v2234 = vsub_f32(v2233, v2102);
    float32x2_t v2237 = vadd_f32(v2236, v2214);
    float32x2_t v2040 = vsub_f32(v2039, v1904);
    float32x2_t v2042 = vadd_f32(v2020, v2030);
    float32x2_t v2043 = vadd_f32(v2022, v2032);
    float32x2_t v2044 = vsub_f32(v2024, v2034);
    float32x2_t v2045 = vadd_f32(v2026, v2036);
    float32x2_t v2046 = vsub_f32(v2026, v2036);
    float32x2_t v2047 = vadd_f32(v2024, v2034);
    float32x2_t v2048 = vsub_f32(v2022, v2032);
    float32x2_t v2049 = vsub_f32(v2020, v2030);
    float32x2_t v2238 = vsub_f32(v2237, v2102);
    float32x2_t v2240 = vadd_f32(v2218, v2228);
    float32x2_t v2241 = vadd_f32(v2220, v2230);
    float32x2_t v2242 = vsub_f32(v2222, v2232);
    float32x2_t v2243 = vadd_f32(v2224, v2234);
    float32x2_t v2244 = vsub_f32(v2224, v2234);
    float32x2_t v2245 = vadd_f32(v2222, v2232);
    float32x2_t v2246 = vsub_f32(v2220, v2230);
    float32x2_t v2247 = vsub_f32(v2218, v2228);
    float32x2_t v2041 = vadd_f32(v2028, v2040);
    float32x2_t v2050 = vsub_f32(v2028, v2040);
    float32x2_t v2239 = vadd_f32(v2226, v2238);
    float32x2_t v2248 = vsub_f32(v2226, v2238);
    v6[ostride * 2] = v2049;
    v6[ostride * 13] = v2247;
    v6[ostride * 14] = v2048;
    v6[ostride * 3] = v2246;
    v6[ostride * 4] = v2047;
    v6[ostride * 15] = v2245;
    v6[ostride * 16] = v2046;
    v6[ostride * 5] = v2244;
    v6[ostride * 6] = v2045;
    v6[ostride * 17] = v2243;
    v6[ostride * 18] = v2044;
    v6[ostride * 7] = v2242;
    v6[ostride * 8] = v2043;
    v6[ostride * 19] = v2241;
    v6[ostride * 20] = v2042;
    v6[ostride * 9] = v2240;
    v6[ostride * 12] = v2050;
    v6[ostride] = v2248;
    v6[ostride * 10] = v2041;
    v6[ostride * 21] = v2239;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu22(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v614 = 1.1000000000000001e+00F;
    float v619 = -3.3166247903554003e-01F;
    float v626 = 5.1541501300188641e-01F;
    float v631 = 9.4125353283118118e-01F;
    float v636 = 1.4143537075597825e+00F;
    float v641 = 8.5949297361449750e-01F;
    float v646 = 4.2314838273285138e-02F;
    float v651 = 3.8639279888589606e-01F;
    float v656 = 5.1254589567200015e-01F;
    float v661 = 1.0702757469471715e+00F;
    float v666 = 5.5486073394528512e-01F;
    float v671 = -1.2412944743900585e+00F;
    float v678 = -2.0897833842005756e-01F;
    float v685 = -3.7415717312460811e-01F;
    float v692 = -4.9929922194110327e-02F;
    float v699 = -6.5815896284539266e-01F;
    float v706 = -6.3306543373877577e-01F;
    float v713 = -1.0822460581641109e+00F;
    float v720 = -8.1720737907134022e-01F;
    float v727 = -4.2408709531871824e-01F;
    const float32x2_t *v1048 = &v5[v0];
    float32x2_t *v1209 = &v6[v2];
    int64_t v19 = v0 * 11;
    int64_t v34 = v0 * 2;
    int64_t v45 = v0 * 13;
    int64_t v64 = v0 * 4;
    int64_t v75 = v0 * 15;
    int64_t v94 = v0 * 6;
    int64_t v105 = v0 * 17;
    int64_t v124 = v0 * 8;
    int64_t v135 = v0 * 19;
    int64_t v154 = v0 * 10;
    int64_t v165 = v0 * 21;
    int64_t v184 = v0 * 12;
    int64_t v214 = v0 * 14;
    int64_t v225 = v0 * 3;
    int64_t v244 = v0 * 16;
    int64_t v255 = v0 * 5;
    int64_t v274 = v0 * 18;
    int64_t v285 = v0 * 7;
    int64_t v304 = v0 * 20;
    int64_t v315 = v0 * 9;
    float v622 = v4 * v619;
    float v674 = v4 * v671;
    float v681 = v4 * v678;
    float v688 = v4 * v685;
    float v695 = v4 * v692;
    float v702 = v4 * v699;
    float v709 = v4 * v706;
    float v716 = v4 * v713;
    float v723 = v4 * v720;
    float v730 = v4 * v727;
    int64_t v788 = v2 * 11;
    int64_t v795 = v2 * 12;
    int64_t v809 = v2 * 2;
    int64_t v816 = v2 * 13;
    int64_t v823 = v2 * 14;
    int64_t v830 = v2 * 3;
    int64_t v837 = v2 * 4;
    int64_t v844 = v2 * 15;
    int64_t v851 = v2 * 16;
    int64_t v858 = v2 * 5;
    int64_t v865 = v2 * 6;
    int64_t v872 = v2 * 17;
    int64_t v879 = v2 * 18;
    int64_t v886 = v2 * 7;
    int64_t v893 = v2 * 8;
    int64_t v900 = v2 * 19;
    int64_t v907 = v2 * 20;
    int64_t v914 = v2 * 9;
    int64_t v921 = v2 * 10;
    int64_t v928 = v2 * 21;
    const float32x2_t *v1130 = &v5[0];
    svfloat32_t v1155 = svdup_n_f32(v614);
    svfloat32_t v1157 = svdup_n_f32(v626);
    svfloat32_t v1158 = svdup_n_f32(v631);
    svfloat32_t v1159 = svdup_n_f32(v636);
    svfloat32_t v1160 = svdup_n_f32(v641);
    svfloat32_t v1161 = svdup_n_f32(v646);
    svfloat32_t v1162 = svdup_n_f32(v651);
    svfloat32_t v1163 = svdup_n_f32(v656);
    svfloat32_t v1164 = svdup_n_f32(v661);
    svfloat32_t v1165 = svdup_n_f32(v666);
    float32x2_t *v1182 = &v6[0];
    svfloat32_t v1399 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1048)[0]));
    svfloat32_t v31 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v57 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v61 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v87 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v91 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[14]));
    svfloat32_t v117 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v121 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[16]));
    svfloat32_t v147 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v151 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[18]));
    svfloat32_t v177 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v181 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[20]));
    svfloat32_t v207 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v211 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v237 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v241 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v267 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[15]));
    svfloat32_t v271 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v297 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[17]));
    svfloat32_t v301 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v327 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[19]));
    svfloat32_t v331 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    const float32x2_t *v940 = &v5[v19];
    const float32x2_t *v949 = &v5[v34];
    const float32x2_t *v958 = &v5[v45];
    const float32x2_t *v967 = &v5[v64];
    const float32x2_t *v976 = &v5[v75];
    const float32x2_t *v985 = &v5[v94];
    const float32x2_t *v994 = &v5[v105];
    const float32x2_t *v1003 = &v5[v124];
    const float32x2_t *v1012 = &v5[v135];
    const float32x2_t *v1021 = &v5[v154];
    const float32x2_t *v1030 = &v5[v165];
    const float32x2_t *v1039 = &v5[v184];
    const float32x2_t *v1057 = &v5[v214];
    const float32x2_t *v1066 = &v5[v225];
    const float32x2_t *v1075 = &v5[v244];
    const float32x2_t *v1084 = &v5[v255];
    const float32x2_t *v1093 = &v5[v274];
    const float32x2_t *v1102 = &v5[v285];
    const float32x2_t *v1111 = &v5[v304];
    const float32x2_t *v1120 = &v5[v315];
    svfloat32_t v1156 = svdup_n_f32(v622);
    svfloat32_t v1166 = svdup_n_f32(v674);
    svfloat32_t v1167 = svdup_n_f32(v681);
    svfloat32_t v1168 = svdup_n_f32(v688);
    svfloat32_t v1169 = svdup_n_f32(v695);
    svfloat32_t v1170 = svdup_n_f32(v702);
    svfloat32_t v1171 = svdup_n_f32(v709);
    svfloat32_t v1172 = svdup_n_f32(v716);
    svfloat32_t v1173 = svdup_n_f32(v723);
    svfloat32_t v1174 = svdup_n_f32(v730);
    float32x2_t *v1191 = &v6[v788];
    float32x2_t *v1200 = &v6[v795];
    float32x2_t *v1218 = &v6[v809];
    float32x2_t *v1227 = &v6[v816];
    float32x2_t *v1236 = &v6[v823];
    float32x2_t *v1245 = &v6[v830];
    float32x2_t *v1254 = &v6[v837];
    float32x2_t *v1263 = &v6[v844];
    float32x2_t *v1272 = &v6[v851];
    float32x2_t *v1281 = &v6[v858];
    float32x2_t *v1290 = &v6[v865];
    float32x2_t *v1299 = &v6[v872];
    float32x2_t *v1308 = &v6[v879];
    float32x2_t *v1317 = &v6[v886];
    float32x2_t *v1326 = &v6[v893];
    float32x2_t *v1335 = &v6[v900];
    float32x2_t *v1344 = &v6[v907];
    float32x2_t *v1353 = &v6[v914];
    float32x2_t *v1362 = &v6[v921];
    float32x2_t *v1371 = &v6[v928];
    svfloat32_t v1417 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1130)[0]));
    svfloat32_t zero212 = svdup_n_f32(0);
    svfloat32_t v212 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero212, v1399, v211, 0), v1399,
        v211, 90);
    svfloat32_t v1375 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v940)[0]));
    svfloat32_t v1377 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v949)[0]));
    svfloat32_t v1379 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v958)[0]));
    svfloat32_t v1381 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v967)[0]));
    svfloat32_t v1383 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v976)[0]));
    svfloat32_t v1385 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v985)[0]));
    svfloat32_t v1387 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v994)[0]));
    svfloat32_t v1389 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1003)[0]));
    svfloat32_t v1391 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1012)[0]));
    svfloat32_t v1393 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1021)[0]));
    svfloat32_t v1395 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1030)[0]));
    svfloat32_t v1397 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1039)[0]));
    svfloat32_t v1401 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1057)[0]));
    svfloat32_t v1403 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1066)[0]));
    svfloat32_t v1405 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1075)[0]));
    svfloat32_t v1407 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1084)[0]));
    svfloat32_t v1409 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1093)[0]));
    svfloat32_t v1411 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1102)[0]));
    svfloat32_t v1413 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1111)[0]));
    svfloat32_t v1415 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1120)[0]));
    svfloat32_t zero32 = svdup_n_f32(0);
    svfloat32_t v32 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero32, v1375, v31, 0),
                     v1375, v31, 90);
    svfloat32_t zero58 = svdup_n_f32(0);
    svfloat32_t v58 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero58, v1377, v57, 0),
                     v1377, v57, 90);
    svfloat32_t zero62 = svdup_n_f32(0);
    svfloat32_t v62 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero62, v1379, v61, 0),
                     v1379, v61, 90);
    svfloat32_t zero88 = svdup_n_f32(0);
    svfloat32_t v88 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero88, v1381, v87, 0),
                     v1381, v87, 90);
    svfloat32_t zero92 = svdup_n_f32(0);
    svfloat32_t v92 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero92, v1383, v91, 0),
                     v1383, v91, 90);
    svfloat32_t zero118 = svdup_n_f32(0);
    svfloat32_t v118 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero118, v1385, v117, 0), v1385,
        v117, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero122, v1387, v121, 0), v1387,
        v121, 90);
    svfloat32_t zero148 = svdup_n_f32(0);
    svfloat32_t v148 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero148, v1389, v147, 0), v1389,
        v147, 90);
    svfloat32_t zero152 = svdup_n_f32(0);
    svfloat32_t v152 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero152, v1391, v151, 0), v1391,
        v151, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero178, v1393, v177, 0), v1393,
        v177, 90);
    svfloat32_t zero182 = svdup_n_f32(0);
    svfloat32_t v182 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero182, v1395, v181, 0), v1395,
        v181, 90);
    svfloat32_t zero208 = svdup_n_f32(0);
    svfloat32_t v208 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero208, v1397, v207, 0), v1397,
        v207, 90);
    svfloat32_t zero238 = svdup_n_f32(0);
    svfloat32_t v238 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero238, v1401, v237, 0), v1401,
        v237, 90);
    svfloat32_t zero242 = svdup_n_f32(0);
    svfloat32_t v242 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero242, v1403, v241, 0), v1403,
        v241, 90);
    svfloat32_t zero268 = svdup_n_f32(0);
    svfloat32_t v268 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero268, v1405, v267, 0), v1405,
        v267, 90);
    svfloat32_t zero272 = svdup_n_f32(0);
    svfloat32_t v272 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero272, v1407, v271, 0), v1407,
        v271, 90);
    svfloat32_t zero298 = svdup_n_f32(0);
    svfloat32_t v298 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero298, v1409, v297, 0), v1409,
        v297, 90);
    svfloat32_t zero302 = svdup_n_f32(0);
    svfloat32_t v302 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero302, v1411, v301, 0), v1411,
        v301, 90);
    svfloat32_t zero328 = svdup_n_f32(0);
    svfloat32_t v328 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero328, v1413, v327, 0), v1413,
        v327, 90);
    svfloat32_t zero332 = svdup_n_f32(0);
    svfloat32_t v332 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero332, v1415, v331, 0), v1415,
        v331, 90);
    svfloat32_t v340 = svadd_f32_x(svptrue_b32(), v1417, v32);
    svfloat32_t v341 = svsub_f32_x(svptrue_b32(), v1417, v32);
    svfloat32_t v342 = svadd_f32_x(svptrue_b32(), v58, v62);
    svfloat32_t v343 = svsub_f32_x(svptrue_b32(), v58, v62);
    svfloat32_t v344 = svadd_f32_x(svptrue_b32(), v88, v92);
    svfloat32_t v345 = svsub_f32_x(svptrue_b32(), v88, v92);
    svfloat32_t v346 = svadd_f32_x(svptrue_b32(), v118, v122);
    svfloat32_t v347 = svsub_f32_x(svptrue_b32(), v118, v122);
    svfloat32_t v348 = svadd_f32_x(svptrue_b32(), v148, v152);
    svfloat32_t v349 = svsub_f32_x(svptrue_b32(), v148, v152);
    svfloat32_t v350 = svadd_f32_x(svptrue_b32(), v178, v182);
    svfloat32_t v351 = svsub_f32_x(svptrue_b32(), v178, v182);
    svfloat32_t v352 = svadd_f32_x(svptrue_b32(), v208, v212);
    svfloat32_t v353 = svsub_f32_x(svptrue_b32(), v208, v212);
    svfloat32_t v354 = svadd_f32_x(svptrue_b32(), v238, v242);
    svfloat32_t v355 = svsub_f32_x(svptrue_b32(), v238, v242);
    svfloat32_t v356 = svadd_f32_x(svptrue_b32(), v268, v272);
    svfloat32_t v357 = svsub_f32_x(svptrue_b32(), v268, v272);
    svfloat32_t v358 = svadd_f32_x(svptrue_b32(), v298, v302);
    svfloat32_t v359 = svsub_f32_x(svptrue_b32(), v298, v302);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v328, v332);
    svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v328, v332);
    svfloat32_t v362 = svadd_f32_x(svptrue_b32(), v342, v360);
    svfloat32_t v363 = svadd_f32_x(svptrue_b32(), v344, v358);
    svfloat32_t v364 = svadd_f32_x(svptrue_b32(), v346, v356);
    svfloat32_t v365 = svadd_f32_x(svptrue_b32(), v348, v354);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v350, v352);
    svfloat32_t v367 = svsub_f32_x(svptrue_b32(), v342, v360);
    svfloat32_t v368 = svsub_f32_x(svptrue_b32(), v344, v358);
    svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v346, v356);
    svfloat32_t v370 = svsub_f32_x(svptrue_b32(), v348, v354);
    svfloat32_t v371 = svsub_f32_x(svptrue_b32(), v350, v352);
    svfloat32_t v571 = svadd_f32_x(svptrue_b32(), v343, v361);
    svfloat32_t v572 = svadd_f32_x(svptrue_b32(), v345, v359);
    svfloat32_t v573 = svadd_f32_x(svptrue_b32(), v347, v357);
    svfloat32_t v574 = svadd_f32_x(svptrue_b32(), v349, v355);
    svfloat32_t v575 = svadd_f32_x(svptrue_b32(), v351, v353);
    svfloat32_t v576 = svsub_f32_x(svptrue_b32(), v343, v361);
    svfloat32_t v577 = svsub_f32_x(svptrue_b32(), v345, v359);
    svfloat32_t v578 = svsub_f32_x(svptrue_b32(), v347, v357);
    svfloat32_t v579 = svsub_f32_x(svptrue_b32(), v349, v355);
    svfloat32_t v580 = svsub_f32_x(svptrue_b32(), v351, v353);
    svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v362, v363);
    svfloat32_t v373 = svadd_f32_x(svptrue_b32(), v364, v366);
    svfloat32_t v375 = svsub_f32_x(svptrue_b32(), v368, v369);
    svfloat32_t v376 = svadd_f32_x(svptrue_b32(), v367, v371);
    svfloat32_t v381 = svsub_f32_x(svptrue_b32(), v363, v365);
    svfloat32_t v382 = svsub_f32_x(svptrue_b32(), v362, v365);
    svfloat32_t v383 = svsub_f32_x(svptrue_b32(), v363, v362);
    svfloat32_t v384 = svsub_f32_x(svptrue_b32(), v366, v365);
    svfloat32_t v385 = svsub_f32_x(svptrue_b32(), v364, v365);
    svfloat32_t v386 = svsub_f32_x(svptrue_b32(), v366, v364);
    svfloat32_t v387 = svsub_f32_x(svptrue_b32(), v363, v366);
    svfloat32_t v388 = svsub_f32_x(svptrue_b32(), v362, v364);
    svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v368, v370);
    svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v367, v370);
    svfloat32_t v392 = svadd_f32_x(svptrue_b32(), v367, v368);
    svfloat32_t v393 = svsub_f32_x(svptrue_b32(), v370, v371);
    svfloat32_t v394 = svsub_f32_x(svptrue_b32(), v369, v370);
    svfloat32_t v395 = svsub_f32_x(svptrue_b32(), v369, v371);
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v368, v371);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v367, v369);
    svfloat32_t v581 = svadd_f32_x(svptrue_b32(), v571, v572);
    svfloat32_t v582 = svadd_f32_x(svptrue_b32(), v573, v575);
    svfloat32_t v584 = svsub_f32_x(svptrue_b32(), v577, v578);
    svfloat32_t v585 = svadd_f32_x(svptrue_b32(), v576, v580);
    svfloat32_t v590 = svsub_f32_x(svptrue_b32(), v572, v574);
    svfloat32_t v591 = svsub_f32_x(svptrue_b32(), v571, v574);
    svfloat32_t v592 = svsub_f32_x(svptrue_b32(), v572, v571);
    svfloat32_t v593 = svsub_f32_x(svptrue_b32(), v575, v574);
    svfloat32_t v594 = svsub_f32_x(svptrue_b32(), v573, v574);
    svfloat32_t v595 = svsub_f32_x(svptrue_b32(), v575, v573);
    svfloat32_t v596 = svsub_f32_x(svptrue_b32(), v572, v575);
    svfloat32_t v597 = svsub_f32_x(svptrue_b32(), v571, v573);
    svfloat32_t v599 = svadd_f32_x(svptrue_b32(), v577, v579);
    svfloat32_t v600 = svsub_f32_x(svptrue_b32(), v576, v579);
    svfloat32_t v601 = svadd_f32_x(svptrue_b32(), v576, v577);
    svfloat32_t v602 = svsub_f32_x(svptrue_b32(), v579, v580);
    svfloat32_t v603 = svsub_f32_x(svptrue_b32(), v578, v579);
    svfloat32_t v604 = svsub_f32_x(svptrue_b32(), v578, v580);
    svfloat32_t v605 = svadd_f32_x(svptrue_b32(), v577, v580);
    svfloat32_t v606 = svsub_f32_x(svptrue_b32(), v576, v578);
    svfloat32_t v374 = svadd_f32_x(svptrue_b32(), v365, v372);
    svfloat32_t v379 = svsub_f32_x(svptrue_b32(), v375, v376);
    svfloat32_t v389 = svsub_f32_x(svptrue_b32(), v373, v372);
    svfloat32_t v398 = svadd_f32_x(svptrue_b32(), v375, v376);
    svfloat32_t v425 = svmul_f32_x(svptrue_b32(), v382, v1158);
    svfloat32_t v430 = svmul_f32_x(svptrue_b32(), v383, v1159);
    svfloat32_t v440 = svmul_f32_x(svptrue_b32(), v385, v1161);
    svfloat32_t v445 = svmul_f32_x(svptrue_b32(), v386, v1162);
    svfloat32_t zero467 = svdup_n_f32(0);
    svfloat32_t v467 = svcmla_f32_x(pred_full, zero467, v1166, v390, 90);
    svfloat32_t zero481 = svdup_n_f32(0);
    svfloat32_t v481 = svcmla_f32_x(pred_full, zero481, v1168, v392, 90);
    svfloat32_t zero488 = svdup_n_f32(0);
    svfloat32_t v488 = svcmla_f32_x(pred_full, zero488, v1169, v393, 90);
    svfloat32_t zero502 = svdup_n_f32(0);
    svfloat32_t v502 = svcmla_f32_x(pred_full, zero502, v1171, v395, 90);
    svfloat32_t zero509 = svdup_n_f32(0);
    svfloat32_t v509 = svcmla_f32_x(pred_full, zero509, v1172, v396, 90);
    svfloat32_t v583 = svadd_f32_x(svptrue_b32(), v574, v581);
    svfloat32_t v588 = svsub_f32_x(svptrue_b32(), v584, v585);
    svfloat32_t v598 = svsub_f32_x(svptrue_b32(), v582, v581);
    svfloat32_t v607 = svadd_f32_x(svptrue_b32(), v584, v585);
    svfloat32_t v634 = svmul_f32_x(svptrue_b32(), v591, v1158);
    svfloat32_t v639 = svmul_f32_x(svptrue_b32(), v592, v1159);
    svfloat32_t v649 = svmul_f32_x(svptrue_b32(), v594, v1161);
    svfloat32_t v654 = svmul_f32_x(svptrue_b32(), v595, v1162);
    svfloat32_t zero676 = svdup_n_f32(0);
    svfloat32_t v676 = svcmla_f32_x(pred_full, zero676, v1166, v599, 90);
    svfloat32_t zero690 = svdup_n_f32(0);
    svfloat32_t v690 = svcmla_f32_x(pred_full, zero690, v1168, v601, 90);
    svfloat32_t zero697 = svdup_n_f32(0);
    svfloat32_t v697 = svcmla_f32_x(pred_full, zero697, v1169, v602, 90);
    svfloat32_t zero711 = svdup_n_f32(0);
    svfloat32_t v711 = svcmla_f32_x(pred_full, zero711, v1171, v604, 90);
    svfloat32_t zero718 = svdup_n_f32(0);
    svfloat32_t v718 = svcmla_f32_x(pred_full, zero718, v1172, v605, 90);
    svfloat32_t v377 = svadd_f32_x(svptrue_b32(), v374, v373);
    svfloat32_t v380 = svsub_f32_x(svptrue_b32(), v379, v370);
    svfloat32_t v460 = svmul_f32_x(svptrue_b32(), v389, v1165);
    svfloat32_t zero523 = svdup_n_f32(0);
    svfloat32_t v523 = svcmla_f32_x(pred_full, zero523, v1174, v398, 90);
    svfloat32_t v525 = svmla_f32_x(pred_full, v425, v381, v1157);
    svfloat32_t v526 = svmla_f32_x(pred_full, v430, v382, v1158);
    svfloat32_t v527 = svnmls_f32_x(pred_full, v430, v381, v1157);
    svfloat32_t v528 = svmla_f32_x(pred_full, v440, v384, v1160);
    svfloat32_t v529 = svmla_f32_x(pred_full, v445, v385, v1161);
    svfloat32_t v530 = svnmls_f32_x(pred_full, v445, v384, v1160);
    svfloat32_t v533 = svcmla_f32_x(pred_full, v481, v1167, v391, 90);
    svfloat32_t v534 = svsub_f32_x(svptrue_b32(), v467, v481);
    svfloat32_t v535 = svcmla_f32_x(pred_full, v502, v1170, v394, 90);
    svfloat32_t v536 = svsub_f32_x(svptrue_b32(), v488, v502);
    svfloat32_t v586 = svadd_f32_x(svptrue_b32(), v583, v582);
    svfloat32_t v589 = svsub_f32_x(svptrue_b32(), v588, v579);
    svfloat32_t v669 = svmul_f32_x(svptrue_b32(), v598, v1165);
    svfloat32_t zero732 = svdup_n_f32(0);
    svfloat32_t v732 = svcmla_f32_x(pred_full, zero732, v1174, v607, 90);
    svfloat32_t v734 = svmla_f32_x(pred_full, v634, v590, v1157);
    svfloat32_t v735 = svmla_f32_x(pred_full, v639, v591, v1158);
    svfloat32_t v736 = svnmls_f32_x(pred_full, v639, v590, v1157);
    svfloat32_t v737 = svmla_f32_x(pred_full, v649, v593, v1160);
    svfloat32_t v738 = svmla_f32_x(pred_full, v654, v594, v1161);
    svfloat32_t v739 = svnmls_f32_x(pred_full, v654, v593, v1160);
    svfloat32_t v742 = svcmla_f32_x(pred_full, v690, v1167, v600, 90);
    svfloat32_t v743 = svsub_f32_x(svptrue_b32(), v676, v690);
    svfloat32_t v744 = svcmla_f32_x(pred_full, v711, v1170, v603, 90);
    svfloat32_t v745 = svsub_f32_x(svptrue_b32(), v697, v711);
    svfloat32_t v378 = svadd_f32_x(svptrue_b32(), v340, v377);
    svfloat32_t zero415 = svdup_n_f32(0);
    svfloat32_t v415 = svcmla_f32_x(pred_full, zero415, v1156, v380, 90);
    svfloat32_t v531 = svmla_f32_x(pred_full, v460, v388, v1164);
    svfloat32_t v532 = svmla_f32_x(pred_full, v460, v387, v1163);
    svfloat32_t v537 = svcmla_f32_x(pred_full, v523, v1173, v397, 90);
    svfloat32_t v538 = svsub_f32_x(svptrue_b32(), v509, v523);
    svfloat32_t v557 = svadd_f32_x(svptrue_b32(), v533, v534);
    svfloat32_t v587 = svadd_f32_x(svptrue_b32(), v341, v586);
    svfloat32_t zero624 = svdup_n_f32(0);
    svfloat32_t v624 = svcmla_f32_x(pred_full, zero624, v1156, v589, 90);
    svfloat32_t v740 = svmla_f32_x(pred_full, v669, v597, v1164);
    svfloat32_t v741 = svmla_f32_x(pred_full, v669, v596, v1163);
    svfloat32_t v746 = svcmla_f32_x(pred_full, v732, v1173, v606, 90);
    svfloat32_t v747 = svsub_f32_x(svptrue_b32(), v718, v732);
    svfloat32_t v766 = svadd_f32_x(svptrue_b32(), v742, v743);
    svfloat32_t v524 = svmls_f32_x(pred_full, v378, v377, v1155);
    svfloat32_t v539 = svadd_f32_x(svptrue_b32(), v529, v531);
    svfloat32_t v549 = svadd_f32_x(svptrue_b32(), v415, v535);
    svfloat32_t v551 = svsub_f32_x(svptrue_b32(), v537, v533);
    svfloat32_t v553 = svadd_f32_x(svptrue_b32(), v415, v538);
    svfloat32_t v555 = svsub_f32_x(svptrue_b32(), v538, v534);
    svfloat32_t v558 = svadd_f32_x(svptrue_b32(), v557, v535);
    svfloat32_t v733 = svmls_f32_x(pred_full, v587, v586, v1155);
    svfloat32_t v748 = svadd_f32_x(svptrue_b32(), v738, v740);
    svfloat32_t v758 = svadd_f32_x(svptrue_b32(), v624, v744);
    svfloat32_t v760 = svsub_f32_x(svptrue_b32(), v746, v742);
    svfloat32_t v762 = svadd_f32_x(svptrue_b32(), v624, v747);
    svfloat32_t v764 = svsub_f32_x(svptrue_b32(), v747, v743);
    svfloat32_t v767 = svadd_f32_x(svptrue_b32(), v766, v744);
    svst1_f64(pred_full, (double *)(v1182), svreinterpret_f64_f32(v378));
    svst1_f64(pred_full, (double *)(v1191), svreinterpret_f64_f32(v587));
    svfloat32_t v540 = svadd_f32_x(svptrue_b32(), v539, v524);
    svfloat32_t v541 = svsub_f32_x(svptrue_b32(), v524, v526);
    svfloat32_t v543 = svadd_f32_x(svptrue_b32(), v524, v530);
    svfloat32_t v545 = svsub_f32_x(svptrue_b32(), v524, v527);
    svfloat32_t v547 = svadd_f32_x(svptrue_b32(), v524, v525);
    svfloat32_t v550 = svadd_f32_x(svptrue_b32(), v549, v537);
    svfloat32_t v552 = svsub_f32_x(svptrue_b32(), v551, v415);
    svfloat32_t v554 = svadd_f32_x(svptrue_b32(), v553, v536);
    svfloat32_t v556 = svsub_f32_x(svptrue_b32(), v555, v415);
    svfloat32_t v559 = svadd_f32_x(svptrue_b32(), v558, v536);
    svfloat32_t v749 = svadd_f32_x(svptrue_b32(), v748, v733);
    svfloat32_t v750 = svsub_f32_x(svptrue_b32(), v733, v735);
    svfloat32_t v752 = svadd_f32_x(svptrue_b32(), v733, v739);
    svfloat32_t v754 = svsub_f32_x(svptrue_b32(), v733, v736);
    svfloat32_t v756 = svadd_f32_x(svptrue_b32(), v733, v734);
    svfloat32_t v759 = svadd_f32_x(svptrue_b32(), v758, v746);
    svfloat32_t v761 = svsub_f32_x(svptrue_b32(), v760, v624);
    svfloat32_t v763 = svadd_f32_x(svptrue_b32(), v762, v745);
    svfloat32_t v765 = svsub_f32_x(svptrue_b32(), v764, v624);
    svfloat32_t v768 = svadd_f32_x(svptrue_b32(), v767, v745);
    svfloat32_t v542 = svsub_f32_x(svptrue_b32(), v541, v531);
    svfloat32_t v544 = svadd_f32_x(svptrue_b32(), v543, v532);
    svfloat32_t v546 = svsub_f32_x(svptrue_b32(), v545, v532);
    svfloat32_t v548 = svsub_f32_x(svptrue_b32(), v547, v528);
    svfloat32_t v560 = svsub_f32_x(svptrue_b32(), v559, v415);
    svfloat32_t v562 = svadd_f32_x(svptrue_b32(), v540, v550);
    svfloat32_t v569 = svsub_f32_x(svptrue_b32(), v540, v550);
    svfloat32_t v751 = svsub_f32_x(svptrue_b32(), v750, v740);
    svfloat32_t v753 = svadd_f32_x(svptrue_b32(), v752, v741);
    svfloat32_t v755 = svsub_f32_x(svptrue_b32(), v754, v741);
    svfloat32_t v757 = svsub_f32_x(svptrue_b32(), v756, v737);
    svfloat32_t v769 = svsub_f32_x(svptrue_b32(), v768, v624);
    svfloat32_t v771 = svadd_f32_x(svptrue_b32(), v749, v759);
    svfloat32_t v778 = svsub_f32_x(svptrue_b32(), v749, v759);
    svfloat32_t v561 = svadd_f32_x(svptrue_b32(), v548, v560);
    svfloat32_t v563 = svadd_f32_x(svptrue_b32(), v542, v552);
    svfloat32_t v564 = svsub_f32_x(svptrue_b32(), v544, v554);
    svfloat32_t v565 = svadd_f32_x(svptrue_b32(), v546, v556);
    svfloat32_t v566 = svsub_f32_x(svptrue_b32(), v546, v556);
    svfloat32_t v567 = svadd_f32_x(svptrue_b32(), v544, v554);
    svfloat32_t v568 = svsub_f32_x(svptrue_b32(), v542, v552);
    svfloat32_t v570 = svsub_f32_x(svptrue_b32(), v548, v560);
    svfloat32_t v770 = svadd_f32_x(svptrue_b32(), v757, v769);
    svfloat32_t v772 = svadd_f32_x(svptrue_b32(), v751, v761);
    svfloat32_t v773 = svsub_f32_x(svptrue_b32(), v753, v763);
    svfloat32_t v774 = svadd_f32_x(svptrue_b32(), v755, v765);
    svfloat32_t v775 = svsub_f32_x(svptrue_b32(), v755, v765);
    svfloat32_t v776 = svadd_f32_x(svptrue_b32(), v753, v763);
    svfloat32_t v777 = svsub_f32_x(svptrue_b32(), v751, v761);
    svfloat32_t v779 = svsub_f32_x(svptrue_b32(), v757, v769);
    svst1_f64(pred_full, (double *)(v1218), svreinterpret_f64_f32(v569));
    svst1_f64(pred_full, (double *)(v1227), svreinterpret_f64_f32(v778));
    svst1_f64(pred_full, (double *)(v1344), svreinterpret_f64_f32(v562));
    svst1_f64(pred_full, (double *)(v1353), svreinterpret_f64_f32(v771));
    svst1_f64(pred_full, (double *)(v1200), svreinterpret_f64_f32(v570));
    svst1_f64(pred_full, (double *)(v1209), svreinterpret_f64_f32(v779));
    svst1_f64(pred_full, (double *)(v1236), svreinterpret_f64_f32(v568));
    svst1_f64(pred_full, (double *)(v1245), svreinterpret_f64_f32(v777));
    svst1_f64(pred_full, (double *)(v1254), svreinterpret_f64_f32(v567));
    svst1_f64(pred_full, (double *)(v1263), svreinterpret_f64_f32(v776));
    svst1_f64(pred_full, (double *)(v1272), svreinterpret_f64_f32(v566));
    svst1_f64(pred_full, (double *)(v1281), svreinterpret_f64_f32(v775));
    svst1_f64(pred_full, (double *)(v1290), svreinterpret_f64_f32(v565));
    svst1_f64(pred_full, (double *)(v1299), svreinterpret_f64_f32(v774));
    svst1_f64(pred_full, (double *)(v1308), svreinterpret_f64_f32(v564));
    svst1_f64(pred_full, (double *)(v1317), svreinterpret_f64_f32(v773));
    svst1_f64(pred_full, (double *)(v1326), svreinterpret_f64_f32(v563));
    svst1_f64(pred_full, (double *)(v1335), svreinterpret_f64_f32(v772));
    svst1_f64(pred_full, (double *)(v1362), svreinterpret_f64_f32(v561));
    svst1_f64(pred_full, (double *)(v1371), svreinterpret_f64_f32(v770));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu24(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v1104 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v725 = 1.0000000000000000e+00F;
    float v726 = -1.0000000000000000e+00F;
    float v734 = -7.0710678118654746e-01F;
    float v742 = 7.0710678118654757e-01F;
    float v800 = -1.4999999999999998e+00F;
    float v801 = 1.4999999999999998e+00F;
    float v809 = 1.0606601717798210e+00F;
    float v817 = -1.0606601717798212e+00F;
    float v876 = 8.6602540378443871e-01F;
    float v885 = -8.6602540378443871e-01F;
    float v894 = 6.1237243569579458e-01F;
    float v895 = -6.1237243569579458e-01F;
    float32x2_t v897 = (float32x2_t){v4, v4};
    const float32x2_t *v2103 = &v5[istride];
    float32x2_t *v2298 = &v6[ostride];
    float32x2_t v727 = (float32x2_t){v725, v726};
    float32x2_t v735 = (float32x2_t){v742, v734};
    float32x2_t v743 = (float32x2_t){v742, v742};
    float32x2_t v797 = (float32x2_t){v800, v800};
    float32x2_t v802 = (float32x2_t){v800, v801};
    float32x2_t v810 = (float32x2_t){v817, v809};
    float32x2_t v818 = (float32x2_t){v817, v817};
    float32x2_t v878 = (float32x2_t){v876, v885};
    float32x2_t v886 = (float32x2_t){v885, v885};
    float32x2_t v891 = (float32x2_t){v895, v895};
    float32x2_t v896 = (float32x2_t){v894, v895};
    const float32x2_t *v2252 = &v5[0];
    float32x2_t *v2262 = &v6[0];
    float32x4_t v2491 = vld1q_f32((const float32_t *)v2103);
    float32x4_t v66 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v68 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v78 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[30]));
    float32x4_t v80 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[31]));
    float32x4_t v128 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v130 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v140 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[36]));
    float32x4_t v142 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[37]));
    float32x4_t v159 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v161 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v209 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v211 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v221 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[42]));
    float32x4_t v223 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[43]));
    float32x4_t v240 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v242 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v290 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[32]));
    float32x4_t v292 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[33]));
    float32x4_t v297 = vtrn1q_f32(v2491, v2491);
    float32x4_t v298 = vtrn2q_f32(v2491, v2491);
    float32x4_t v302 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v304 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v321 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v323 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v371 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[38]));
    float32x4_t v373 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[39]));
    float32x4_t v383 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v385 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v402 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v404 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v452 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[44]));
    float32x4_t v454 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[45]));
    float32x4_t v464 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v466 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v483 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[28]));
    float32x4_t v485 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[29]));
    float32x4_t v533 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v535 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v545 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v547 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v564 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[34]));
    float32x4_t v566 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[35]));
    float32x4_t v614 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v616 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v626 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v628 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v645 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[40]));
    float32x4_t v647 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[41]));
    float32x2_t v729 = vmul_f32(v897, v727);
    float32x2_t v737 = vmul_f32(v897, v735);
    float32x4_t v744 = vcombine_f32(v743, v743);
    float32x4_t v798 = vcombine_f32(v797, v797);
    float32x2_t v804 = vmul_f32(v897, v802);
    float32x2_t v812 = vmul_f32(v897, v810);
    float32x4_t v819 = vcombine_f32(v818, v818);
    float32x2_t v880 = vmul_f32(v897, v878);
    float32x4_t v887 = vcombine_f32(v886, v886);
    float32x4_t v892 = vcombine_f32(v891, v891);
    float32x2_t v898 = vmul_f32(v897, v896);
    const float32x2_t *v2007 = &v5[istride * 8];
    const float32x2_t *v2017 = &v5[istride * 16];
    const float32x2_t *v2029 = &v5[istride * 11];
    const float32x2_t *v2039 = &v5[istride * 19];
    const float32x2_t *v2051 = &v5[istride * 3];
    const float32x2_t *v2061 = &v5[istride * 14];
    const float32x2_t *v2071 = &v5[istride * 22];
    const float32x2_t *v2083 = &v5[istride * 6];
    const float32x2_t *v2093 = &v5[istride * 17];
    const float32x2_t *v2113 = &v5[istride * 9];
    const float32x2_t *v2123 = &v5[istride * 20];
    const float32x2_t *v2133 = &v5[istride * 4];
    const float32x2_t *v2145 = &v5[istride * 12];
    const float32x2_t *v2155 = &v5[istride * 23];
    const float32x2_t *v2165 = &v5[istride * 7];
    const float32x2_t *v2177 = &v5[istride * 15];
    const float32x2_t *v2187 = &v5[istride * 2];
    const float32x2_t *v2197 = &v5[istride * 10];
    const float32x2_t *v2209 = &v5[istride * 18];
    const float32x2_t *v2219 = &v5[istride * 5];
    const float32x2_t *v2229 = &v5[istride * 13];
    const float32x2_t *v2241 = &v5[istride * 21];
    float32x2_t *v2271 = &v6[ostride * 16];
    float32x2_t *v2280 = &v6[ostride * 8];
    float32x2_t *v2289 = &v6[ostride * 9];
    float32x2_t *v2307 = &v6[ostride * 17];
    float32x2_t *v2316 = &v6[ostride * 18];
    float32x2_t *v2325 = &v6[ostride * 10];
    float32x2_t *v2334 = &v6[ostride * 2];
    float32x2_t *v2343 = &v6[ostride * 3];
    float32x2_t *v2352 = &v6[ostride * 19];
    float32x2_t *v2361 = &v6[ostride * 11];
    float32x2_t *v2370 = &v6[ostride * 12];
    float32x2_t *v2379 = &v6[ostride * 4];
    float32x2_t *v2388 = &v6[ostride * 20];
    float32x2_t *v2397 = &v6[ostride * 21];
    float32x2_t *v2406 = &v6[ostride * 13];
    float32x2_t *v2415 = &v6[ostride * 5];
    float32x2_t *v2424 = &v6[ostride * 6];
    float32x2_t *v2433 = &v6[ostride * 22];
    float32x2_t *v2442 = &v6[ostride * 14];
    float32x2_t *v2451 = &v6[ostride * 15];
    float32x2_t *v2460 = &v6[ostride * 7];
    float32x2_t *v2469 = &v6[ostride * 23];
    float32x4_t v2519 = vld1q_f32((const float32_t *)v2252);
    float32x4_t v303 = vmulq_f32(v297, v302);
    float32x4_t v731 = vcombine_f32(v729, v729);
    float32x4_t v739 = vcombine_f32(v737, v737);
    float32x4_t v806 = vcombine_f32(v804, v804);
    float32x4_t v814 = vcombine_f32(v812, v812);
    float32x4_t v882 = vcombine_f32(v880, v880);
    float32x4_t v900 = vcombine_f32(v898, v898);
    float32x4_t v2473 = vld1q_f32((const float32_t *)v2007);
    float32x4_t v2475 = vld1q_f32((const float32_t *)v2017);
    float32x4_t v2477 = vld1q_f32((const float32_t *)v2029);
    float32x4_t v2479 = vld1q_f32((const float32_t *)v2039);
    float32x4_t v2481 = vld1q_f32((const float32_t *)v2051);
    float32x4_t v2483 = vld1q_f32((const float32_t *)v2061);
    float32x4_t v2485 = vld1q_f32((const float32_t *)v2071);
    float32x4_t v2487 = vld1q_f32((const float32_t *)v2083);
    float32x4_t v2489 = vld1q_f32((const float32_t *)v2093);
    float32x4_t v2493 = vld1q_f32((const float32_t *)v2113);
    float32x4_t v2495 = vld1q_f32((const float32_t *)v2123);
    float32x4_t v2497 = vld1q_f32((const float32_t *)v2133);
    float32x4_t v2499 = vld1q_f32((const float32_t *)v2145);
    float32x4_t v2501 = vld1q_f32((const float32_t *)v2155);
    float32x4_t v2503 = vld1q_f32((const float32_t *)v2165);
    float32x4_t v2505 = vld1q_f32((const float32_t *)v2177);
    float32x4_t v2507 = vld1q_f32((const float32_t *)v2187);
    float32x4_t v2509 = vld1q_f32((const float32_t *)v2197);
    float32x4_t v2511 = vld1q_f32((const float32_t *)v2209);
    float32x4_t v2513 = vld1q_f32((const float32_t *)v2219);
    float32x4_t v2515 = vld1q_f32((const float32_t *)v2229);
    float32x4_t v2517 = vld1q_f32((const float32_t *)v2241);
    float32x4_t v61 = vtrn1q_f32(v2473, v2473);
    float32x4_t v62 = vtrn2q_f32(v2473, v2473);
    float32x4_t v73 = vtrn1q_f32(v2475, v2475);
    float32x4_t v74 = vtrn2q_f32(v2475, v2475);
    float32x4_t v123 = vtrn1q_f32(v2477, v2477);
    float32x4_t v124 = vtrn2q_f32(v2477, v2477);
    float32x4_t v135 = vtrn1q_f32(v2479, v2479);
    float32x4_t v136 = vtrn2q_f32(v2479, v2479);
    float32x4_t v154 = vtrn1q_f32(v2481, v2481);
    float32x4_t v155 = vtrn2q_f32(v2481, v2481);
    float32x4_t v204 = vtrn1q_f32(v2483, v2483);
    float32x4_t v205 = vtrn2q_f32(v2483, v2483);
    float32x4_t v216 = vtrn1q_f32(v2485, v2485);
    float32x4_t v217 = vtrn2q_f32(v2485, v2485);
    float32x4_t v235 = vtrn1q_f32(v2487, v2487);
    float32x4_t v236 = vtrn2q_f32(v2487, v2487);
    float32x4_t v285 = vtrn1q_f32(v2489, v2489);
    float32x4_t v286 = vtrn2q_f32(v2489, v2489);
    float32x4_t v306 = vfmaq_f32(v303, v298, v304);
    float32x4_t v316 = vtrn1q_f32(v2493, v2493);
    float32x4_t v317 = vtrn2q_f32(v2493, v2493);
    float32x4_t v366 = vtrn1q_f32(v2495, v2495);
    float32x4_t v367 = vtrn2q_f32(v2495, v2495);
    float32x4_t v378 = vtrn1q_f32(v2497, v2497);
    float32x4_t v379 = vtrn2q_f32(v2497, v2497);
    float32x4_t v397 = vtrn1q_f32(v2499, v2499);
    float32x4_t v398 = vtrn2q_f32(v2499, v2499);
    float32x4_t v447 = vtrn1q_f32(v2501, v2501);
    float32x4_t v448 = vtrn2q_f32(v2501, v2501);
    float32x4_t v459 = vtrn1q_f32(v2503, v2503);
    float32x4_t v460 = vtrn2q_f32(v2503, v2503);
    float32x4_t v478 = vtrn1q_f32(v2505, v2505);
    float32x4_t v479 = vtrn2q_f32(v2505, v2505);
    float32x4_t v528 = vtrn1q_f32(v2507, v2507);
    float32x4_t v529 = vtrn2q_f32(v2507, v2507);
    float32x4_t v540 = vtrn1q_f32(v2509, v2509);
    float32x4_t v541 = vtrn2q_f32(v2509, v2509);
    float32x4_t v559 = vtrn1q_f32(v2511, v2511);
    float32x4_t v560 = vtrn2q_f32(v2511, v2511);
    float32x4_t v609 = vtrn1q_f32(v2513, v2513);
    float32x4_t v610 = vtrn2q_f32(v2513, v2513);
    float32x4_t v621 = vtrn1q_f32(v2515, v2515);
    float32x4_t v622 = vtrn2q_f32(v2515, v2515);
    float32x4_t v640 = vtrn1q_f32(v2517, v2517);
    float32x4_t v641 = vtrn2q_f32(v2517, v2517);
    float32x4_t v67 = vmulq_f32(v61, v66);
    float32x4_t v79 = vmulq_f32(v73, v78);
    float32x4_t v129 = vmulq_f32(v123, v128);
    float32x4_t v141 = vmulq_f32(v135, v140);
    float32x4_t v160 = vmulq_f32(v154, v159);
    float32x4_t v210 = vmulq_f32(v204, v209);
    float32x4_t v222 = vmulq_f32(v216, v221);
    float32x4_t v241 = vmulq_f32(v235, v240);
    float32x4_t v291 = vmulq_f32(v285, v290);
    float32x4_t v322 = vmulq_f32(v316, v321);
    float32x4_t v372 = vmulq_f32(v366, v371);
    float32x4_t v384 = vmulq_f32(v378, v383);
    float32x4_t v403 = vmulq_f32(v397, v402);
    float32x4_t v453 = vmulq_f32(v447, v452);
    float32x4_t v465 = vmulq_f32(v459, v464);
    float32x4_t v484 = vmulq_f32(v478, v483);
    float32x4_t v534 = vmulq_f32(v528, v533);
    float32x4_t v546 = vmulq_f32(v540, v545);
    float32x4_t v565 = vmulq_f32(v559, v564);
    float32x4_t v615 = vmulq_f32(v609, v614);
    float32x4_t v627 = vmulq_f32(v621, v626);
    float32x4_t v646 = vmulq_f32(v640, v645);
    float32x4_t v70 = vfmaq_f32(v67, v62, v68);
    float32x4_t v82 = vfmaq_f32(v79, v74, v80);
    float32x4_t v132 = vfmaq_f32(v129, v124, v130);
    float32x4_t v144 = vfmaq_f32(v141, v136, v142);
    float32x4_t v163 = vfmaq_f32(v160, v155, v161);
    float32x4_t v213 = vfmaq_f32(v210, v205, v211);
    float32x4_t v225 = vfmaq_f32(v222, v217, v223);
    float32x4_t v244 = vfmaq_f32(v241, v236, v242);
    float32x4_t v294 = vfmaq_f32(v291, v286, v292);
    float32x4_t v325 = vfmaq_f32(v322, v317, v323);
    float32x4_t v375 = vfmaq_f32(v372, v367, v373);
    float32x4_t v387 = vfmaq_f32(v384, v379, v385);
    float32x4_t v406 = vfmaq_f32(v403, v398, v404);
    float32x4_t v456 = vfmaq_f32(v453, v448, v454);
    float32x4_t v468 = vfmaq_f32(v465, v460, v466);
    float32x4_t v487 = vfmaq_f32(v484, v479, v485);
    float32x4_t v537 = vfmaq_f32(v534, v529, v535);
    float32x4_t v549 = vfmaq_f32(v546, v541, v547);
    float32x4_t v568 = vfmaq_f32(v565, v560, v566);
    float32x4_t v618 = vfmaq_f32(v615, v610, v616);
    float32x4_t v630 = vfmaq_f32(v627, v622, v628);
    float32x4_t v649 = vfmaq_f32(v646, v641, v647);
    float32x4_t v650 = vaddq_f32(v70, v82);
    float32x4_t v651 = vsubq_f32(v70, v82);
    float32x4_t v660 = vaddq_f32(v132, v144);
    float32x4_t v661 = vsubq_f32(v132, v144);
    float32x4_t v663 = vaddq_f32(v213, v225);
    float32x4_t v664 = vsubq_f32(v213, v225);
    float32x4_t v666 = vaddq_f32(v294, v306);
    float32x4_t v667 = vsubq_f32(v294, v306);
    float32x4_t v669 = vaddq_f32(v375, v387);
    float32x4_t v670 = vsubq_f32(v375, v387);
    float32x4_t v672 = vaddq_f32(v456, v468);
    float32x4_t v673 = vsubq_f32(v456, v468);
    float32x4_t v675 = vaddq_f32(v537, v549);
    float32x4_t v676 = vsubq_f32(v537, v549);
    float32x4_t v678 = vaddq_f32(v618, v630);
    float32x4_t v679 = vsubq_f32(v618, v630);
    float32x4_t v659 = vaddq_f32(v650, v2519);
    float32x4_t v662 = vaddq_f32(v660, v163);
    float32x4_t v665 = vaddq_f32(v663, v244);
    float32x4_t v668 = vaddq_f32(v666, v325);
    float32x4_t v671 = vaddq_f32(v669, v406);
    float32x4_t v674 = vaddq_f32(v672, v487);
    float32x4_t v677 = vaddq_f32(v675, v568);
    float32x4_t v680 = vaddq_f32(v678, v649);
    float32x4_t v756 = vaddq_f32(v650, v669);
    float32x4_t v757 = vsubq_f32(v650, v669);
    float32x4_t v758 = vaddq_f32(v663, v675);
    float32x4_t v759 = vsubq_f32(v663, v675);
    float32x4_t v760 = vaddq_f32(v660, v672);
    float32x4_t v761 = vsubq_f32(v660, v672);
    float32x4_t v762 = vaddq_f32(v666, v678);
    float32x4_t v763 = vsubq_f32(v666, v678);
    float32x4_t v831 = vaddq_f32(v651, v670);
    float32x4_t v832 = vsubq_f32(v651, v670);
    float32x4_t v833 = vaddq_f32(v664, v676);
    float32x4_t v834 = vsubq_f32(v664, v676);
    float32x4_t v835 = vaddq_f32(v661, v673);
    float32x4_t v836 = vsubq_f32(v661, v673);
    float32x4_t v837 = vaddq_f32(v667, v679);
    float32x4_t v838 = vsubq_f32(v667, v679);
    float32x4_t v681 = vaddq_f32(v659, v671);
    float32x4_t v682 = vsubq_f32(v659, v671);
    float32x4_t v683 = vaddq_f32(v665, v677);
    float32x4_t v684 = vsubq_f32(v665, v677);
    float32x4_t v685 = vaddq_f32(v662, v674);
    float32x4_t v686 = vsubq_f32(v662, v674);
    float32x4_t v687 = vaddq_f32(v668, v680);
    float32x4_t v688 = vsubq_f32(v668, v680);
    float32x4_t v764 = vaddq_f32(v756, v758);
    float32x4_t v765 = vsubq_f32(v756, v758);
    float32x4_t v766 = vaddq_f32(v760, v762);
    float32x4_t v767 = vsubq_f32(v760, v762);
    float32x4_t v770 = vaddq_f32(v761, v763);
    float32x4_t v771 = vsubq_f32(v761, v763);
    float32x4_t v799 = vmulq_f32(v757, v798);
    float32x4_t v805 = vrev64q_f32(v759);
    float32x4_t v839 = vaddq_f32(v831, v833);
    float32x4_t v840 = vsubq_f32(v831, v833);
    float32x4_t v841 = vaddq_f32(v835, v837);
    float32x4_t v842 = vsubq_f32(v835, v837);
    float32x4_t v845 = vaddq_f32(v836, v838);
    float32x4_t v846 = vsubq_f32(v836, v838);
    float32x4_t v881 = vrev64q_f32(v832);
    float32x4_t v888 = vmulq_f32(v834, v887);
    float32x4_t v689 = vaddq_f32(v681, v683);
    float32x4_t v690 = vsubq_f32(v681, v683);
    float32x4_t v691 = vaddq_f32(v685, v687);
    float32x4_t v692 = vsubq_f32(v685, v687);
    float32x4_t v695 = vaddq_f32(v686, v688);
    float32x4_t v696 = vsubq_f32(v686, v688);
    float32x4_t v730 = vrev64q_f32(v684);
    float32x4_t v768 = vaddq_f32(v764, v766);
    float32x4_t v769 = vsubq_f32(v764, v766);
    float32x4_t v786 = vmulq_f32(v765, v798);
    float32x4_t v792 = vrev64q_f32(v767);
    float32x4_t v807 = vmulq_f32(v805, v806);
    float32x4_t v813 = vrev64q_f32(v770);
    float32x4_t v820 = vmulq_f32(v771, v819);
    float32x4_t v843 = vaddq_f32(v839, v841);
    float32x4_t v844 = vsubq_f32(v839, v841);
    float32x4_t v868 = vrev64q_f32(v840);
    float32x4_t v875 = vmulq_f32(v842, v887);
    float32x4_t v883 = vmulq_f32(v881, v882);
    float32x4_t v893 = vmulq_f32(v845, v892);
    float32x4_t v899 = vrev64q_f32(v846);
    float32x4_t v693 = vaddq_f32(v689, v691);
    float32x4_t v694 = vsubq_f32(v689, v691);
    float32x4_t v717 = vrev64q_f32(v692);
    float32x4_t v732 = vmulq_f32(v730, v731);
    float32x4_t v738 = vrev64q_f32(v695);
    float32x4_t v745 = vmulq_f32(v696, v744);
    float32x4_t v776 = vmulq_f32(v768, v798);
    float32x4_t v781 = vmulq_f32(v769, v798);
    float32x4_t v794 = vmulq_f32(v792, v806);
    float32x4_t v815 = vmulq_f32(v813, v814);
    float32x4_t v823 = vaddq_f32(v799, v820);
    float32x4_t v824 = vsubq_f32(v799, v820);
    float32x4_t v852 = vrev64q_f32(v843);
    float32x4_t v860 = vrev64q_f32(v844);
    float32x4_t v870 = vmulq_f32(v868, v882);
    float32x4_t v901 = vmulq_f32(v899, v900);
    float32x4_t v906 = vaddq_f32(v888, v893);
    float32x4_t v907 = vsubq_f32(v888, v893);
    float32x4_t v719 = vmulq_f32(v717, v731);
    float32x4_t v740 = vmulq_f32(v738, v739);
    float32x4_t v748 = vaddq_f32(v682, v745);
    float32x4_t v749 = vsubq_f32(v682, v745);
    float32x4_t v821 = vaddq_f32(v786, v794);
    float32x4_t v822 = vsubq_f32(v786, v794);
    float32x4_t v825 = vaddq_f32(v807, v815);
    float32x4_t v826 = vsubq_f32(v807, v815);
    float32x4_t v854 = vmulq_f32(v852, v882);
    float32x4_t v862 = vmulq_f32(v860, v882);
    float32x4_t v902 = vaddq_f32(v870, v875);
    float32x4_t v903 = vsubq_f32(v870, v875);
    float32x4_t v904 = vaddq_f32(v883, v901);
    float32x4_t v905 = vsubq_f32(v883, v901);
    float32x4_t v912 = vaddq_f32(v693, v776);
    float32x4_t v1008 = vaddq_f32(v694, v781);
    vst1q_f32((float32_t *)v2262, v693);
    vst1q_f32((float32_t *)v2370, v694);
    float32x4_t v746 = vaddq_f32(v690, v719);
    float32x4_t v747 = vsubq_f32(v690, v719);
    float32x4_t v750 = vaddq_f32(v732, v740);
    float32x4_t v751 = vsubq_f32(v732, v740);
    float32x4_t v827 = vaddq_f32(v823, v825);
    float32x4_t v828 = vsubq_f32(v823, v825);
    float32x4_t v829 = vaddq_f32(v824, v826);
    float32x4_t v830 = vsubq_f32(v824, v826);
    float32x4_t v908 = vaddq_f32(v904, v906);
    float32x4_t v909 = vsubq_f32(v904, v906);
    float32x4_t v910 = vaddq_f32(v905, v907);
    float32x4_t v911 = vsubq_f32(v905, v907);
    float32x4_t v913 = vaddq_f32(v912, v854);
    float32x4_t v914 = vsubq_f32(v912, v854);
    float32x4_t v1009 = vaddq_f32(v1008, v862);
    float32x4_t v1010 = vsubq_f32(v1008, v862);
    float32x4_t v752 = vaddq_f32(v748, v750);
    float32x4_t v753 = vsubq_f32(v748, v750);
    float32x4_t v754 = vaddq_f32(v749, v751);
    float32x4_t v755 = vsubq_f32(v749, v751);
    float32x4_t v960 = vaddq_f32(v747, v822);
    float32x4_t v1056 = vaddq_f32(v746, v821);
    vst1q_f32((float32_t *)v2271, v914);
    vst1q_f32((float32_t *)v2280, v913);
    vst1q_f32((float32_t *)v2316, v747);
    vst1q_f32((float32_t *)v2379, v1010);
    vst1q_f32((float32_t *)v2388, v1009);
    vst1q_f32((float32_t *)v2424, v746);
    float32x4_t v936 = vaddq_f32(v753, v828);
    float32x4_t v961 = vaddq_f32(v960, v903);
    float32x4_t v962 = vsubq_f32(v960, v903);
    float32x4_t v984 = vaddq_f32(v754, v829);
    float32x4_t v1032 = vaddq_f32(v755, v830);
    float32x4_t v1057 = vaddq_f32(v1056, v902);
    float32x4_t v1058 = vsubq_f32(v1056, v902);
    float32x4_t v1080 = vaddq_f32(v752, v827);
    vst1q_f32((float32_t *)v2289, v753);
    vst1q_f32((float32_t *)v2343, v754);
    vst1q_f32((float32_t *)v2397, v755);
    vst1q_f32((float32_t *)v2451, v752);
    float32x4_t v937 = vaddq_f32(v936, v909);
    float32x4_t v938 = vsubq_f32(v936, v909);
    float32x4_t v985 = vaddq_f32(v984, v910);
    float32x4_t v986 = vsubq_f32(v984, v910);
    float32x4_t v1033 = vaddq_f32(v1032, v911);
    float32x4_t v1034 = vsubq_f32(v1032, v911);
    float32x4_t v1081 = vaddq_f32(v1080, v908);
    float32x4_t v1082 = vsubq_f32(v1080, v908);
    vst1q_f32((float32_t *)v2325, v962);
    vst1q_f32((float32_t *)v2334, v961);
    vst1q_f32((float32_t *)v2433, v1058);
    vst1q_f32((float32_t *)v2442, v1057);
    vst1q_f32((float32_t *)v2298, v938);
    vst1q_f32((float32_t *)v2307, v937);
    vst1q_f32((float32_t *)v2352, v986);
    vst1q_f32((float32_t *)v2361, v985);
    vst1q_f32((float32_t *)v2406, v1034);
    vst1q_f32((float32_t *)v2415, v1033);
    vst1q_f32((float32_t *)v2460, v1082);
    vst1q_f32((float32_t *)v2469, v1081);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v1104 * 2; j < howmany; j += 1) {
    float32x2_t v1311 = v5[istride];
    float v1685 = 1.0000000000000000e+00F;
    float v1686 = -1.0000000000000000e+00F;
    float v1693 = -7.0710678118654746e-01F;
    float v1700 = 7.0710678118654757e-01F;
    float v1752 = -1.4999999999999998e+00F;
    float v1753 = 1.4999999999999998e+00F;
    float v1760 = 1.0606601717798210e+00F;
    float v1767 = -1.0606601717798212e+00F;
    float v1821 = 8.6602540378443871e-01F;
    float v1829 = -8.6602540378443871e-01F;
    float v1836 = 6.1237243569579458e-01F;
    float v1837 = -6.1237243569579458e-01F;
    float32x2_t v1839 = (float32x2_t){v4, v4};
    float32x2_t v1143 = v7[14];
    float32x2_t v1148 = v7[15];
    float32x2_t v1153 = v7[30];
    float32x2_t v1158 = v7[31];
    float32x2_t v1193 = v7[20];
    float32x2_t v1198 = v7[21];
    float32x2_t v1203 = v7[36];
    float32x2_t v1208 = v7[37];
    float32x2_t v1218 = v7[4];
    float32x2_t v1223 = v7[5];
    float32x2_t v1258 = v7[26];
    float32x2_t v1263 = v7[27];
    float32x2_t v1268 = v7[42];
    float32x2_t v1273 = v7[43];
    float32x2_t v1283 = v7[10];
    float32x2_t v1288 = v7[11];
    float32x2_t v1323 = v7[32];
    float32x2_t v1328 = v7[33];
    float32x2_t v1333 = v7[0];
    float32x2_t v1334 = vtrn1_f32(v1311, v1311);
    float32x2_t v1335 = vtrn2_f32(v1311, v1311);
    float32x2_t v1338 = v7[1];
    float32x2_t v1348 = v7[16];
    float32x2_t v1353 = v7[17];
    float32x2_t v1388 = v7[38];
    float32x2_t v1393 = v7[39];
    float32x2_t v1398 = v7[6];
    float32x2_t v1403 = v7[7];
    float32x2_t v1413 = v7[22];
    float32x2_t v1418 = v7[23];
    float32x2_t v1453 = v7[44];
    float32x2_t v1458 = v7[45];
    float32x2_t v1463 = v7[12];
    float32x2_t v1468 = v7[13];
    float32x2_t v1478 = v7[28];
    float32x2_t v1483 = v7[29];
    float32x2_t v1518 = v7[2];
    float32x2_t v1523 = v7[3];
    float32x2_t v1528 = v7[18];
    float32x2_t v1533 = v7[19];
    float32x2_t v1543 = v7[34];
    float32x2_t v1548 = v7[35];
    float32x2_t v1583 = v7[8];
    float32x2_t v1588 = v7[9];
    float32x2_t v1593 = v7[24];
    float32x2_t v1598 = v7[25];
    float32x2_t v1608 = v7[40];
    float32x2_t v1613 = v7[41];
    float32x2_t v1623 = v5[0];
    float32x2_t v1687 = (float32x2_t){v1685, v1686};
    float32x2_t v1694 = (float32x2_t){v1700, v1693};
    float32x2_t v1701 = (float32x2_t){v1700, v1700};
    float32x2_t v1750 = (float32x2_t){v1752, v1752};
    float32x2_t v1754 = (float32x2_t){v1752, v1753};
    float32x2_t v1761 = (float32x2_t){v1767, v1760};
    float32x2_t v1768 = (float32x2_t){v1767, v1767};
    float32x2_t v1823 = (float32x2_t){v1821, v1829};
    float32x2_t v1830 = (float32x2_t){v1829, v1829};
    float32x2_t v1834 = (float32x2_t){v1837, v1837};
    float32x2_t v1838 = (float32x2_t){v1836, v1837};
    float32x2_t v1116 = v5[istride * 8];
    float32x2_t v1131 = v5[istride * 16];
    float32x2_t v1166 = v5[istride * 11];
    float32x2_t v1181 = v5[istride * 19];
    float32x2_t v1216 = v5[istride * 3];
    float32x2_t v1231 = v5[istride * 14];
    float32x2_t v1246 = v5[istride * 22];
    float32x2_t v1281 = v5[istride * 6];
    float32x2_t v1296 = v5[istride * 17];
    float32x2_t v1339 = vmul_f32(v1334, v1333);
    float32x2_t v1346 = v5[istride * 9];
    float32x2_t v1361 = v5[istride * 20];
    float32x2_t v1376 = v5[istride * 4];
    float32x2_t v1411 = v5[istride * 12];
    float32x2_t v1426 = v5[istride * 23];
    float32x2_t v1441 = v5[istride * 7];
    float32x2_t v1476 = v5[istride * 15];
    float32x2_t v1491 = v5[istride * 2];
    float32x2_t v1506 = v5[istride * 10];
    float32x2_t v1541 = v5[istride * 18];
    float32x2_t v1556 = v5[istride * 5];
    float32x2_t v1571 = v5[istride * 13];
    float32x2_t v1606 = v5[istride * 21];
    float32x2_t v1689 = vmul_f32(v1839, v1687);
    float32x2_t v1696 = vmul_f32(v1839, v1694);
    float32x2_t v1756 = vmul_f32(v1839, v1754);
    float32x2_t v1763 = vmul_f32(v1839, v1761);
    float32x2_t v1825 = vmul_f32(v1839, v1823);
    float32x2_t v1840 = vmul_f32(v1839, v1838);
    float32x2_t v1144 = vtrn1_f32(v1116, v1116);
    float32x2_t v1145 = vtrn2_f32(v1116, v1116);
    float32x2_t v1154 = vtrn1_f32(v1131, v1131);
    float32x2_t v1155 = vtrn2_f32(v1131, v1131);
    float32x2_t v1194 = vtrn1_f32(v1166, v1166);
    float32x2_t v1195 = vtrn2_f32(v1166, v1166);
    float32x2_t v1204 = vtrn1_f32(v1181, v1181);
    float32x2_t v1205 = vtrn2_f32(v1181, v1181);
    float32x2_t v1219 = vtrn1_f32(v1216, v1216);
    float32x2_t v1220 = vtrn2_f32(v1216, v1216);
    float32x2_t v1259 = vtrn1_f32(v1231, v1231);
    float32x2_t v1260 = vtrn2_f32(v1231, v1231);
    float32x2_t v1269 = vtrn1_f32(v1246, v1246);
    float32x2_t v1270 = vtrn2_f32(v1246, v1246);
    float32x2_t v1284 = vtrn1_f32(v1281, v1281);
    float32x2_t v1285 = vtrn2_f32(v1281, v1281);
    float32x2_t v1324 = vtrn1_f32(v1296, v1296);
    float32x2_t v1325 = vtrn2_f32(v1296, v1296);
    float32x2_t v1341 = vfma_f32(v1339, v1335, v1338);
    float32x2_t v1349 = vtrn1_f32(v1346, v1346);
    float32x2_t v1350 = vtrn2_f32(v1346, v1346);
    float32x2_t v1389 = vtrn1_f32(v1361, v1361);
    float32x2_t v1390 = vtrn2_f32(v1361, v1361);
    float32x2_t v1399 = vtrn1_f32(v1376, v1376);
    float32x2_t v1400 = vtrn2_f32(v1376, v1376);
    float32x2_t v1414 = vtrn1_f32(v1411, v1411);
    float32x2_t v1415 = vtrn2_f32(v1411, v1411);
    float32x2_t v1454 = vtrn1_f32(v1426, v1426);
    float32x2_t v1455 = vtrn2_f32(v1426, v1426);
    float32x2_t v1464 = vtrn1_f32(v1441, v1441);
    float32x2_t v1465 = vtrn2_f32(v1441, v1441);
    float32x2_t v1479 = vtrn1_f32(v1476, v1476);
    float32x2_t v1480 = vtrn2_f32(v1476, v1476);
    float32x2_t v1519 = vtrn1_f32(v1491, v1491);
    float32x2_t v1520 = vtrn2_f32(v1491, v1491);
    float32x2_t v1529 = vtrn1_f32(v1506, v1506);
    float32x2_t v1530 = vtrn2_f32(v1506, v1506);
    float32x2_t v1544 = vtrn1_f32(v1541, v1541);
    float32x2_t v1545 = vtrn2_f32(v1541, v1541);
    float32x2_t v1584 = vtrn1_f32(v1556, v1556);
    float32x2_t v1585 = vtrn2_f32(v1556, v1556);
    float32x2_t v1594 = vtrn1_f32(v1571, v1571);
    float32x2_t v1595 = vtrn2_f32(v1571, v1571);
    float32x2_t v1609 = vtrn1_f32(v1606, v1606);
    float32x2_t v1610 = vtrn2_f32(v1606, v1606);
    float32x2_t v1149 = vmul_f32(v1144, v1143);
    float32x2_t v1159 = vmul_f32(v1154, v1153);
    float32x2_t v1199 = vmul_f32(v1194, v1193);
    float32x2_t v1209 = vmul_f32(v1204, v1203);
    float32x2_t v1224 = vmul_f32(v1219, v1218);
    float32x2_t v1264 = vmul_f32(v1259, v1258);
    float32x2_t v1274 = vmul_f32(v1269, v1268);
    float32x2_t v1289 = vmul_f32(v1284, v1283);
    float32x2_t v1329 = vmul_f32(v1324, v1323);
    float32x2_t v1354 = vmul_f32(v1349, v1348);
    float32x2_t v1394 = vmul_f32(v1389, v1388);
    float32x2_t v1404 = vmul_f32(v1399, v1398);
    float32x2_t v1419 = vmul_f32(v1414, v1413);
    float32x2_t v1459 = vmul_f32(v1454, v1453);
    float32x2_t v1469 = vmul_f32(v1464, v1463);
    float32x2_t v1484 = vmul_f32(v1479, v1478);
    float32x2_t v1524 = vmul_f32(v1519, v1518);
    float32x2_t v1534 = vmul_f32(v1529, v1528);
    float32x2_t v1549 = vmul_f32(v1544, v1543);
    float32x2_t v1589 = vmul_f32(v1584, v1583);
    float32x2_t v1599 = vmul_f32(v1594, v1593);
    float32x2_t v1614 = vmul_f32(v1609, v1608);
    float32x2_t v1151 = vfma_f32(v1149, v1145, v1148);
    float32x2_t v1161 = vfma_f32(v1159, v1155, v1158);
    float32x2_t v1201 = vfma_f32(v1199, v1195, v1198);
    float32x2_t v1211 = vfma_f32(v1209, v1205, v1208);
    float32x2_t v1226 = vfma_f32(v1224, v1220, v1223);
    float32x2_t v1266 = vfma_f32(v1264, v1260, v1263);
    float32x2_t v1276 = vfma_f32(v1274, v1270, v1273);
    float32x2_t v1291 = vfma_f32(v1289, v1285, v1288);
    float32x2_t v1331 = vfma_f32(v1329, v1325, v1328);
    float32x2_t v1356 = vfma_f32(v1354, v1350, v1353);
    float32x2_t v1396 = vfma_f32(v1394, v1390, v1393);
    float32x2_t v1406 = vfma_f32(v1404, v1400, v1403);
    float32x2_t v1421 = vfma_f32(v1419, v1415, v1418);
    float32x2_t v1461 = vfma_f32(v1459, v1455, v1458);
    float32x2_t v1471 = vfma_f32(v1469, v1465, v1468);
    float32x2_t v1486 = vfma_f32(v1484, v1480, v1483);
    float32x2_t v1526 = vfma_f32(v1524, v1520, v1523);
    float32x2_t v1536 = vfma_f32(v1534, v1530, v1533);
    float32x2_t v1551 = vfma_f32(v1549, v1545, v1548);
    float32x2_t v1591 = vfma_f32(v1589, v1585, v1588);
    float32x2_t v1601 = vfma_f32(v1599, v1595, v1598);
    float32x2_t v1616 = vfma_f32(v1614, v1610, v1613);
    float32x2_t v1617 = vadd_f32(v1151, v1161);
    float32x2_t v1618 = vsub_f32(v1151, v1161);
    float32x2_t v1625 = vadd_f32(v1201, v1211);
    float32x2_t v1626 = vsub_f32(v1201, v1211);
    float32x2_t v1628 = vadd_f32(v1266, v1276);
    float32x2_t v1629 = vsub_f32(v1266, v1276);
    float32x2_t v1631 = vadd_f32(v1331, v1341);
    float32x2_t v1632 = vsub_f32(v1331, v1341);
    float32x2_t v1634 = vadd_f32(v1396, v1406);
    float32x2_t v1635 = vsub_f32(v1396, v1406);
    float32x2_t v1637 = vadd_f32(v1461, v1471);
    float32x2_t v1638 = vsub_f32(v1461, v1471);
    float32x2_t v1640 = vadd_f32(v1526, v1536);
    float32x2_t v1641 = vsub_f32(v1526, v1536);
    float32x2_t v1643 = vadd_f32(v1591, v1601);
    float32x2_t v1644 = vsub_f32(v1591, v1601);
    float32x2_t v1624 = vadd_f32(v1617, v1623);
    float32x2_t v1627 = vadd_f32(v1625, v1226);
    float32x2_t v1630 = vadd_f32(v1628, v1291);
    float32x2_t v1633 = vadd_f32(v1631, v1356);
    float32x2_t v1636 = vadd_f32(v1634, v1421);
    float32x2_t v1639 = vadd_f32(v1637, v1486);
    float32x2_t v1642 = vadd_f32(v1640, v1551);
    float32x2_t v1645 = vadd_f32(v1643, v1616);
    float32x2_t v1713 = vadd_f32(v1617, v1634);
    float32x2_t v1714 = vsub_f32(v1617, v1634);
    float32x2_t v1715 = vadd_f32(v1628, v1640);
    float32x2_t v1716 = vsub_f32(v1628, v1640);
    float32x2_t v1717 = vadd_f32(v1625, v1637);
    float32x2_t v1718 = vsub_f32(v1625, v1637);
    float32x2_t v1719 = vadd_f32(v1631, v1643);
    float32x2_t v1720 = vsub_f32(v1631, v1643);
    float32x2_t v1780 = vadd_f32(v1618, v1635);
    float32x2_t v1781 = vsub_f32(v1618, v1635);
    float32x2_t v1782 = vadd_f32(v1629, v1641);
    float32x2_t v1783 = vsub_f32(v1629, v1641);
    float32x2_t v1784 = vadd_f32(v1626, v1638);
    float32x2_t v1785 = vsub_f32(v1626, v1638);
    float32x2_t v1786 = vadd_f32(v1632, v1644);
    float32x2_t v1787 = vsub_f32(v1632, v1644);
    float32x2_t v1646 = vadd_f32(v1624, v1636);
    float32x2_t v1647 = vsub_f32(v1624, v1636);
    float32x2_t v1648 = vadd_f32(v1630, v1642);
    float32x2_t v1649 = vsub_f32(v1630, v1642);
    float32x2_t v1650 = vadd_f32(v1627, v1639);
    float32x2_t v1651 = vsub_f32(v1627, v1639);
    float32x2_t v1652 = vadd_f32(v1633, v1645);
    float32x2_t v1653 = vsub_f32(v1633, v1645);
    float32x2_t v1721 = vadd_f32(v1713, v1715);
    float32x2_t v1722 = vsub_f32(v1713, v1715);
    float32x2_t v1723 = vadd_f32(v1717, v1719);
    float32x2_t v1724 = vsub_f32(v1717, v1719);
    float32x2_t v1727 = vadd_f32(v1718, v1720);
    float32x2_t v1728 = vsub_f32(v1718, v1720);
    float32x2_t v1751 = vmul_f32(v1714, v1750);
    float32x2_t v1757 = vrev64_f32(v1716);
    float32x2_t v1788 = vadd_f32(v1780, v1782);
    float32x2_t v1789 = vsub_f32(v1780, v1782);
    float32x2_t v1790 = vadd_f32(v1784, v1786);
    float32x2_t v1791 = vsub_f32(v1784, v1786);
    float32x2_t v1794 = vadd_f32(v1785, v1787);
    float32x2_t v1795 = vsub_f32(v1785, v1787);
    float32x2_t v1826 = vrev64_f32(v1781);
    float32x2_t v1831 = vmul_f32(v1783, v1830);
    float32x2_t v1654 = vadd_f32(v1646, v1648);
    float32x2_t v1655 = vsub_f32(v1646, v1648);
    float32x2_t v1656 = vadd_f32(v1650, v1652);
    float32x2_t v1657 = vsub_f32(v1650, v1652);
    float32x2_t v1660 = vadd_f32(v1651, v1653);
    float32x2_t v1661 = vsub_f32(v1651, v1653);
    float32x2_t v1690 = vrev64_f32(v1649);
    float32x2_t v1725 = vadd_f32(v1721, v1723);
    float32x2_t v1726 = vsub_f32(v1721, v1723);
    float32x2_t v1740 = vmul_f32(v1722, v1750);
    float32x2_t v1746 = vrev64_f32(v1724);
    float32x2_t v1758 = vmul_f32(v1757, v1756);
    float32x2_t v1764 = vrev64_f32(v1727);
    float32x2_t v1769 = vmul_f32(v1728, v1768);
    float32x2_t v1792 = vadd_f32(v1788, v1790);
    float32x2_t v1793 = vsub_f32(v1788, v1790);
    float32x2_t v1815 = vrev64_f32(v1789);
    float32x2_t v1820 = vmul_f32(v1791, v1830);
    float32x2_t v1827 = vmul_f32(v1826, v1825);
    float32x2_t v1835 = vmul_f32(v1794, v1834);
    float32x2_t v1841 = vrev64_f32(v1795);
    float32x2_t v1658 = vadd_f32(v1654, v1656);
    float32x2_t v1659 = vsub_f32(v1654, v1656);
    float32x2_t v1679 = vrev64_f32(v1657);
    float32x2_t v1691 = vmul_f32(v1690, v1689);
    float32x2_t v1697 = vrev64_f32(v1660);
    float32x2_t v1702 = vmul_f32(v1661, v1701);
    float32x2_t v1732 = vmul_f32(v1725, v1750);
    float32x2_t v1736 = vmul_f32(v1726, v1750);
    float32x2_t v1747 = vmul_f32(v1746, v1756);
    float32x2_t v1765 = vmul_f32(v1764, v1763);
    float32x2_t v1772 = vadd_f32(v1751, v1769);
    float32x2_t v1773 = vsub_f32(v1751, v1769);
    float32x2_t v1801 = vrev64_f32(v1792);
    float32x2_t v1808 = vrev64_f32(v1793);
    float32x2_t v1816 = vmul_f32(v1815, v1825);
    float32x2_t v1842 = vmul_f32(v1841, v1840);
    float32x2_t v1847 = vadd_f32(v1831, v1835);
    float32x2_t v1848 = vsub_f32(v1831, v1835);
    float32x2_t v1680 = vmul_f32(v1679, v1689);
    float32x2_t v1698 = vmul_f32(v1697, v1696);
    float32x2_t v1705 = vadd_f32(v1647, v1702);
    float32x2_t v1706 = vsub_f32(v1647, v1702);
    float32x2_t v1770 = vadd_f32(v1740, v1747);
    float32x2_t v1771 = vsub_f32(v1740, v1747);
    float32x2_t v1774 = vadd_f32(v1758, v1765);
    float32x2_t v1775 = vsub_f32(v1758, v1765);
    float32x2_t v1802 = vmul_f32(v1801, v1825);
    float32x2_t v1809 = vmul_f32(v1808, v1825);
    float32x2_t v1843 = vadd_f32(v1816, v1820);
    float32x2_t v1844 = vsub_f32(v1816, v1820);
    float32x2_t v1845 = vadd_f32(v1827, v1842);
    float32x2_t v1846 = vsub_f32(v1827, v1842);
    float32x2_t v1853 = vadd_f32(v1658, v1732);
    v6[0] = v1658;
    float32x2_t v1925 = vadd_f32(v1659, v1736);
    v6[ostride * 12] = v1659;
    float32x2_t v1703 = vadd_f32(v1655, v1680);
    float32x2_t v1704 = vsub_f32(v1655, v1680);
    float32x2_t v1707 = vadd_f32(v1691, v1698);
    float32x2_t v1708 = vsub_f32(v1691, v1698);
    float32x2_t v1776 = vadd_f32(v1772, v1774);
    float32x2_t v1777 = vsub_f32(v1772, v1774);
    float32x2_t v1778 = vadd_f32(v1773, v1775);
    float32x2_t v1779 = vsub_f32(v1773, v1775);
    float32x2_t v1849 = vadd_f32(v1845, v1847);
    float32x2_t v1850 = vsub_f32(v1845, v1847);
    float32x2_t v1851 = vadd_f32(v1846, v1848);
    float32x2_t v1852 = vsub_f32(v1846, v1848);
    float32x2_t v1854 = vadd_f32(v1853, v1802);
    float32x2_t v1855 = vsub_f32(v1853, v1802);
    float32x2_t v1926 = vadd_f32(v1925, v1809);
    float32x2_t v1927 = vsub_f32(v1925, v1809);
    float32x2_t v1709 = vadd_f32(v1705, v1707);
    float32x2_t v1710 = vsub_f32(v1705, v1707);
    float32x2_t v1711 = vadd_f32(v1706, v1708);
    float32x2_t v1712 = vsub_f32(v1706, v1708);
    v6[ostride * 16] = v1855;
    v6[ostride * 8] = v1854;
    float32x2_t v1889 = vadd_f32(v1704, v1771);
    v6[ostride * 18] = v1704;
    v6[ostride * 4] = v1927;
    v6[ostride * 20] = v1926;
    float32x2_t v1961 = vadd_f32(v1703, v1770);
    v6[ostride * 6] = v1703;
    float32x2_t v1871 = vadd_f32(v1710, v1777);
    v6[ostride * 9] = v1710;
    float32x2_t v1890 = vadd_f32(v1889, v1844);
    float32x2_t v1891 = vsub_f32(v1889, v1844);
    float32x2_t v1907 = vadd_f32(v1711, v1778);
    v6[ostride * 3] = v1711;
    float32x2_t v1943 = vadd_f32(v1712, v1779);
    v6[ostride * 21] = v1712;
    float32x2_t v1962 = vadd_f32(v1961, v1843);
    float32x2_t v1963 = vsub_f32(v1961, v1843);
    float32x2_t v1979 = vadd_f32(v1709, v1776);
    v6[ostride * 15] = v1709;
    float32x2_t v1872 = vadd_f32(v1871, v1850);
    float32x2_t v1873 = vsub_f32(v1871, v1850);
    v6[ostride * 10] = v1891;
    v6[ostride * 2] = v1890;
    float32x2_t v1908 = vadd_f32(v1907, v1851);
    float32x2_t v1909 = vsub_f32(v1907, v1851);
    float32x2_t v1944 = vadd_f32(v1943, v1852);
    float32x2_t v1945 = vsub_f32(v1943, v1852);
    v6[ostride * 22] = v1963;
    v6[ostride * 14] = v1962;
    float32x2_t v1980 = vadd_f32(v1979, v1849);
    float32x2_t v1981 = vsub_f32(v1979, v1849);
    v6[ostride] = v1873;
    v6[ostride * 17] = v1872;
    v6[ostride * 19] = v1909;
    v6[ostride * 11] = v1908;
    v6[ostride * 13] = v1945;
    v6[ostride * 5] = v1944;
    v6[ostride * 7] = v1981;
    v6[ostride * 23] = v1980;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu24(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v410 = -1.0000000000000000e+00F;
    float v417 = -7.0710678118654746e-01F;
    float v424 = 7.0710678118654757e-01F;
    float v477 = -1.4999999999999998e+00F;
    float v482 = 1.4999999999999998e+00F;
    float v489 = 1.0606601717798210e+00F;
    float v496 = -1.0606601717798212e+00F;
    float v560 = -8.6602540378443871e-01F;
    float v570 = -6.1237243569579458e-01F;
    const float32x2_t *v865 = &v5[v0];
    float32x2_t *v1062 = &v6[v2];
    int64_t v19 = v0 * 8;
    int64_t v30 = v0 * 16;
    int64_t v49 = v0 * 11;
    int64_t v60 = v0 * 19;
    int64_t v79 = v0 * 3;
    int64_t v90 = v0 * 14;
    int64_t v101 = v0 * 22;
    int64_t v120 = v0 * 6;
    int64_t v131 = v0 * 17;
    int64_t v161 = v0 * 9;
    int64_t v172 = v0 * 20;
    int64_t v183 = v0 * 4;
    int64_t v202 = v0 * 12;
    int64_t v213 = v0 * 23;
    int64_t v224 = v0 * 7;
    int64_t v243 = v0 * 15;
    int64_t v254 = v0 * 2;
    int64_t v265 = v0 * 10;
    int64_t v284 = v0 * 18;
    int64_t v295 = v0 * 5;
    int64_t v306 = v0 * 13;
    int64_t v325 = v0 * 21;
    float v413 = v4 * v410;
    float v420 = v4 * v417;
    float v485 = v4 * v482;
    float v492 = v4 * v489;
    float v556 = v4 * v560;
    float v573 = v4 * v570;
    int64_t v597 = v2 * 16;
    int64_t v604 = v2 * 8;
    int64_t v614 = v2 * 9;
    int64_t v628 = v2 * 17;
    int64_t v638 = v2 * 18;
    int64_t v645 = v2 * 10;
    int64_t v652 = v2 * 2;
    int64_t v662 = v2 * 3;
    int64_t v669 = v2 * 19;
    int64_t v676 = v2 * 11;
    int64_t v686 = v2 * 12;
    int64_t v693 = v2 * 4;
    int64_t v700 = v2 * 20;
    int64_t v710 = v2 * 21;
    int64_t v717 = v2 * 13;
    int64_t v724 = v2 * 5;
    int64_t v734 = v2 * 6;
    int64_t v741 = v2 * 22;
    int64_t v748 = v2 * 14;
    int64_t v758 = v2 * 15;
    int64_t v765 = v2 * 7;
    int64_t v772 = v2 * 23;
    const float32x2_t *v992 = &v5[0];
    svfloat32_t v1002 = svdup_n_f32(v424);
    svfloat32_t v1007 = svdup_n_f32(v477);
    svfloat32_t v1010 = svdup_n_f32(v496);
    svfloat32_t v1016 = svdup_n_f32(v560);
    svfloat32_t v1017 = svdup_n_f32(v570);
    float32x2_t *v1026 = &v6[0];
    svfloat32_t v1255 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v865)[0]));
    svfloat32_t v42 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v46 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[15]));
    svfloat32_t v72 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v76 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[18]));
    svfloat32_t v87 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v113 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v117 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[21]));
    svfloat32_t v128 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v154 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[16]));
    svfloat32_t v158 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v169 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v195 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[19]));
    svfloat32_t v199 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v210 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v236 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[22]));
    svfloat32_t v240 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v251 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[14]));
    svfloat32_t v277 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v281 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v292 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[17]));
    svfloat32_t v318 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v322 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v333 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[20]));
    const float32x2_t *v784 = &v5[v19];
    const float32x2_t *v793 = &v5[v30];
    const float32x2_t *v802 = &v5[v49];
    const float32x2_t *v811 = &v5[v60];
    const float32x2_t *v820 = &v5[v79];
    const float32x2_t *v829 = &v5[v90];
    const float32x2_t *v838 = &v5[v101];
    const float32x2_t *v847 = &v5[v120];
    const float32x2_t *v856 = &v5[v131];
    const float32x2_t *v874 = &v5[v161];
    const float32x2_t *v883 = &v5[v172];
    const float32x2_t *v892 = &v5[v183];
    const float32x2_t *v901 = &v5[v202];
    const float32x2_t *v910 = &v5[v213];
    const float32x2_t *v919 = &v5[v224];
    const float32x2_t *v928 = &v5[v243];
    const float32x2_t *v937 = &v5[v254];
    const float32x2_t *v946 = &v5[v265];
    const float32x2_t *v955 = &v5[v284];
    const float32x2_t *v964 = &v5[v295];
    const float32x2_t *v973 = &v5[v306];
    const float32x2_t *v982 = &v5[v325];
    svfloat32_t v1000 = svdup_n_f32(v413);
    svfloat32_t v1001 = svdup_n_f32(v420);
    svfloat32_t v1008 = svdup_n_f32(v485);
    svfloat32_t v1009 = svdup_n_f32(v492);
    svfloat32_t v1015 = svdup_n_f32(v556);
    svfloat32_t v1018 = svdup_n_f32(v573);
    float32x2_t *v1035 = &v6[v597];
    float32x2_t *v1044 = &v6[v604];
    float32x2_t *v1053 = &v6[v614];
    float32x2_t *v1071 = &v6[v628];
    float32x2_t *v1080 = &v6[v638];
    float32x2_t *v1089 = &v6[v645];
    float32x2_t *v1098 = &v6[v652];
    float32x2_t *v1107 = &v6[v662];
    float32x2_t *v1116 = &v6[v669];
    float32x2_t *v1125 = &v6[v676];
    float32x2_t *v1134 = &v6[v686];
    float32x2_t *v1143 = &v6[v693];
    float32x2_t *v1152 = &v6[v700];
    float32x2_t *v1161 = &v6[v710];
    float32x2_t *v1170 = &v6[v717];
    float32x2_t *v1179 = &v6[v724];
    float32x2_t *v1188 = &v6[v734];
    float32x2_t *v1197 = &v6[v741];
    float32x2_t *v1206 = &v6[v748];
    float32x2_t *v1215 = &v6[v758];
    float32x2_t *v1224 = &v6[v765];
    float32x2_t *v1233 = &v6[v772];
    svfloat32_t v1283 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v992)[0]));
    svfloat32_t zero159 = svdup_n_f32(0);
    svfloat32_t v159 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero159, v1255, v158, 0), v1255,
        v158, 90);
    svfloat32_t v1237 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v784)[0]));
    svfloat32_t v1239 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v793)[0]));
    svfloat32_t v1241 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v802)[0]));
    svfloat32_t v1243 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v811)[0]));
    svfloat32_t v1245 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v820)[0]));
    svfloat32_t v1247 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v829)[0]));
    svfloat32_t v1249 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v838)[0]));
    svfloat32_t v1251 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v847)[0]));
    svfloat32_t v1253 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v856)[0]));
    svfloat32_t v1257 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v874)[0]));
    svfloat32_t v1259 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v883)[0]));
    svfloat32_t v1261 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v892)[0]));
    svfloat32_t v1263 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v901)[0]));
    svfloat32_t v1265 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v910)[0]));
    svfloat32_t v1267 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v919)[0]));
    svfloat32_t v1269 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v928)[0]));
    svfloat32_t v1271 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v937)[0]));
    svfloat32_t v1273 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v946)[0]));
    svfloat32_t v1275 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v955)[0]));
    svfloat32_t v1277 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v964)[0]));
    svfloat32_t v1279 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v973)[0]));
    svfloat32_t v1281 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v982)[0]));
    svfloat32_t zero43 = svdup_n_f32(0);
    svfloat32_t v43 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero43, v1237, v42, 0),
                     v1237, v42, 90);
    svfloat32_t zero47 = svdup_n_f32(0);
    svfloat32_t v47 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero47, v1239, v46, 0),
                     v1239, v46, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v1241, v72, 0),
                     v1241, v72, 90);
    svfloat32_t zero77 = svdup_n_f32(0);
    svfloat32_t v77 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero77, v1243, v76, 0),
                     v1243, v76, 90);
    svfloat32_t zero114 = svdup_n_f32(0);
    svfloat32_t v114 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero114, v1247, v113, 0), v1247,
        v113, 90);
    svfloat32_t zero118 = svdup_n_f32(0);
    svfloat32_t v118 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero118, v1249, v117, 0), v1249,
        v117, 90);
    svfloat32_t zero155 = svdup_n_f32(0);
    svfloat32_t v155 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero155, v1253, v154, 0), v1253,
        v154, 90);
    svfloat32_t zero196 = svdup_n_f32(0);
    svfloat32_t v196 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero196, v1259, v195, 0), v1259,
        v195, 90);
    svfloat32_t zero200 = svdup_n_f32(0);
    svfloat32_t v200 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero200, v1261, v199, 0), v1261,
        v199, 90);
    svfloat32_t zero237 = svdup_n_f32(0);
    svfloat32_t v237 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero237, v1265, v236, 0), v1265,
        v236, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero241, v1267, v240, 0), v1267,
        v240, 90);
    svfloat32_t zero278 = svdup_n_f32(0);
    svfloat32_t v278 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero278, v1271, v277, 0), v1271,
        v277, 90);
    svfloat32_t zero282 = svdup_n_f32(0);
    svfloat32_t v282 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero282, v1273, v281, 0), v1273,
        v281, 90);
    svfloat32_t zero319 = svdup_n_f32(0);
    svfloat32_t v319 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero319, v1277, v318, 0), v1277,
        v318, 90);
    svfloat32_t zero323 = svdup_n_f32(0);
    svfloat32_t v323 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero323, v1279, v322, 0), v1279,
        v322, 90);
    svfloat32_t v335 = svadd_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v336 = svsub_f32_x(svptrue_b32(), v43, v47);
    svfloat32_t v345 = svadd_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v346 = svsub_f32_x(svptrue_b32(), v73, v77);
    svfloat32_t v348 = svadd_f32_x(svptrue_b32(), v114, v118);
    svfloat32_t v349 = svsub_f32_x(svptrue_b32(), v114, v118);
    svfloat32_t v351 = svadd_f32_x(svptrue_b32(), v155, v159);
    svfloat32_t v352 = svsub_f32_x(svptrue_b32(), v155, v159);
    svfloat32_t v354 = svadd_f32_x(svptrue_b32(), v196, v200);
    svfloat32_t v355 = svsub_f32_x(svptrue_b32(), v196, v200);
    svfloat32_t v357 = svadd_f32_x(svptrue_b32(), v237, v241);
    svfloat32_t v358 = svsub_f32_x(svptrue_b32(), v237, v241);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v278, v282);
    svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v278, v282);
    svfloat32_t v363 = svadd_f32_x(svptrue_b32(), v319, v323);
    svfloat32_t v364 = svsub_f32_x(svptrue_b32(), v319, v323);
    svfloat32_t v344 = svadd_f32_x(svptrue_b32(), v335, v1283);
    svfloat32_t v347 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v345, v1245, v87, 0),
                     v1245, v87, 90);
    svfloat32_t v350 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v348, v1251, v128, 0),
                     v1251, v128, 90);
    svfloat32_t v353 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v351, v1257, v169, 0),
                     v1257, v169, 90);
    svfloat32_t v356 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v354, v1263, v210, 0),
                     v1263, v210, 90);
    svfloat32_t v359 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v357, v1269, v251, 0),
                     v1269, v251, 90);
    svfloat32_t v362 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v360, v1275, v292, 0),
                     v1275, v292, 90);
    svfloat32_t v365 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v363, v1281, v333, 0),
                     v1281, v333, 90);
    svfloat32_t v438 = svadd_f32_x(svptrue_b32(), v335, v354);
    svfloat32_t v439 = svsub_f32_x(svptrue_b32(), v335, v354);
    svfloat32_t v440 = svadd_f32_x(svptrue_b32(), v348, v360);
    svfloat32_t v441 = svsub_f32_x(svptrue_b32(), v348, v360);
    svfloat32_t v442 = svadd_f32_x(svptrue_b32(), v345, v357);
    svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v345, v357);
    svfloat32_t v444 = svadd_f32_x(svptrue_b32(), v351, v363);
    svfloat32_t v445 = svsub_f32_x(svptrue_b32(), v351, v363);
    svfloat32_t v510 = svadd_f32_x(svptrue_b32(), v336, v355);
    svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v336, v355);
    svfloat32_t v512 = svadd_f32_x(svptrue_b32(), v349, v361);
    svfloat32_t v513 = svsub_f32_x(svptrue_b32(), v349, v361);
    svfloat32_t v514 = svadd_f32_x(svptrue_b32(), v346, v358);
    svfloat32_t v515 = svsub_f32_x(svptrue_b32(), v346, v358);
    svfloat32_t v516 = svadd_f32_x(svptrue_b32(), v352, v364);
    svfloat32_t v517 = svsub_f32_x(svptrue_b32(), v352, v364);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v344, v356);
    svfloat32_t v367 = svsub_f32_x(svptrue_b32(), v344, v356);
    svfloat32_t v368 = svadd_f32_x(svptrue_b32(), v350, v362);
    svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v350, v362);
    svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v347, v359);
    svfloat32_t v371 = svsub_f32_x(svptrue_b32(), v347, v359);
    svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v353, v365);
    svfloat32_t v373 = svsub_f32_x(svptrue_b32(), v353, v365);
    svfloat32_t v446 = svadd_f32_x(svptrue_b32(), v438, v440);
    svfloat32_t v447 = svsub_f32_x(svptrue_b32(), v438, v440);
    svfloat32_t v448 = svadd_f32_x(svptrue_b32(), v442, v444);
    svfloat32_t v449 = svsub_f32_x(svptrue_b32(), v442, v444);
    svfloat32_t v452 = svadd_f32_x(svptrue_b32(), v443, v445);
    svfloat32_t v453 = svsub_f32_x(svptrue_b32(), v443, v445);
    svfloat32_t zero487 = svdup_n_f32(0);
    svfloat32_t v487 = svcmla_f32_x(pred_full, zero487, v1008, v441, 90);
    svfloat32_t v518 = svadd_f32_x(svptrue_b32(), v510, v512);
    svfloat32_t v519 = svsub_f32_x(svptrue_b32(), v510, v512);
    svfloat32_t v520 = svadd_f32_x(svptrue_b32(), v514, v516);
    svfloat32_t v521 = svsub_f32_x(svptrue_b32(), v514, v516);
    svfloat32_t v524 = svadd_f32_x(svptrue_b32(), v515, v517);
    svfloat32_t v525 = svsub_f32_x(svptrue_b32(), v515, v517);
    svfloat32_t zero558 = svdup_n_f32(0);
    svfloat32_t v558 = svcmla_f32_x(pred_full, zero558, v1015, v511, 90);
    svfloat32_t v374 = svadd_f32_x(svptrue_b32(), v366, v368);
    svfloat32_t v375 = svsub_f32_x(svptrue_b32(), v366, v368);
    svfloat32_t v376 = svadd_f32_x(svptrue_b32(), v370, v372);
    svfloat32_t v377 = svsub_f32_x(svptrue_b32(), v370, v372);
    svfloat32_t v380 = svadd_f32_x(svptrue_b32(), v371, v373);
    svfloat32_t v381 = svsub_f32_x(svptrue_b32(), v371, v373);
    svfloat32_t zero415 = svdup_n_f32(0);
    svfloat32_t v415 = svcmla_f32_x(pred_full, zero415, v1000, v369, 90);
    svfloat32_t v450 = svadd_f32_x(svptrue_b32(), v446, v448);
    svfloat32_t v451 = svsub_f32_x(svptrue_b32(), v446, v448);
    svfloat32_t zero475 = svdup_n_f32(0);
    svfloat32_t v475 = svcmla_f32_x(pred_full, zero475, v1008, v449, 90);
    svfloat32_t zero494 = svdup_n_f32(0);
    svfloat32_t v494 = svcmla_f32_x(pred_full, zero494, v1009, v452, 90);
    svfloat32_t v499 = svmul_f32_x(svptrue_b32(), v453, v1010);
    svfloat32_t v522 = svadd_f32_x(svptrue_b32(), v518, v520);
    svfloat32_t v523 = svsub_f32_x(svptrue_b32(), v518, v520);
    svfloat32_t zero546 = svdup_n_f32(0);
    svfloat32_t v546 = svcmla_f32_x(pred_full, zero546, v1015, v519, 90);
    svfloat32_t v568 = svmul_f32_x(svptrue_b32(), v524, v1017);
    svfloat32_t zero575 = svdup_n_f32(0);
    svfloat32_t v575 = svcmla_f32_x(pred_full, zero575, v1018, v525, 90);
    svfloat32_t v378 = svadd_f32_x(svptrue_b32(), v374, v376);
    svfloat32_t v379 = svsub_f32_x(svptrue_b32(), v374, v376);
    svfloat32_t zero403 = svdup_n_f32(0);
    svfloat32_t v403 = svcmla_f32_x(pred_full, zero403, v1000, v377, 90);
    svfloat32_t zero422 = svdup_n_f32(0);
    svfloat32_t v422 = svcmla_f32_x(pred_full, zero422, v1001, v380, 90);
    svfloat32_t v500 = svmla_f32_x(pred_full, v475, v447, v1007);
    svfloat32_t v501 = svnmls_f32_x(pred_full, v475, v447, v1007);
    svfloat32_t v502 = svmla_f32_x(pred_full, v499, v439, v1007);
    svfloat32_t v503 = svnmls_f32_x(pred_full, v499, v439, v1007);
    svfloat32_t v504 = svadd_f32_x(svptrue_b32(), v487, v494);
    svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v487, v494);
    svfloat32_t zero532 = svdup_n_f32(0);
    svfloat32_t v532 = svcmla_f32_x(pred_full, zero532, v1015, v522, 90);
    svfloat32_t zero539 = svdup_n_f32(0);
    svfloat32_t v539 = svcmla_f32_x(pred_full, zero539, v1015, v523, 90);
    svfloat32_t v576 = svmla_f32_x(pred_full, v546, v521, v1016);
    svfloat32_t v577 = svmls_f32_x(pred_full, v546, v521, v1016);
    svfloat32_t v578 = svadd_f32_x(svptrue_b32(), v558, v575);
    svfloat32_t v579 = svsub_f32_x(svptrue_b32(), v558, v575);
    svfloat32_t v580 = svmla_f32_x(pred_full, v568, v513, v1016);
    svfloat32_t v581 = svnmls_f32_x(pred_full, v568, v513, v1016);
    svfloat32_t v428 = svadd_f32_x(svptrue_b32(), v375, v403);
    svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v375, v403);
    svfloat32_t v430 = svmla_f32_x(pred_full, v367, v381, v1002);
    svfloat32_t v431 = svmls_f32_x(pred_full, v367, v381, v1002);
    svfloat32_t v432 = svadd_f32_x(svptrue_b32(), v415, v422);
    svfloat32_t v433 = svsub_f32_x(svptrue_b32(), v415, v422);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v502, v504);
    svfloat32_t v507 = svsub_f32_x(svptrue_b32(), v502, v504);
    svfloat32_t v508 = svadd_f32_x(svptrue_b32(), v503, v505);
    svfloat32_t v509 = svsub_f32_x(svptrue_b32(), v503, v505);
    svfloat32_t v582 = svadd_f32_x(svptrue_b32(), v578, v580);
    svfloat32_t v583 = svsub_f32_x(svptrue_b32(), v578, v580);
    svfloat32_t v584 = svadd_f32_x(svptrue_b32(), v579, v581);
    svfloat32_t v585 = svsub_f32_x(svptrue_b32(), v579, v581);
    svfloat32_t v586 = svmla_f32_x(pred_full, v378, v450, v1007);
    svfloat32_t v682 = svmla_f32_x(pred_full, v379, v451, v1007);
    svst1_f64(pred_full, (double *)(v1026), svreinterpret_f64_f32(v378));
    svst1_f64(pred_full, (double *)(v1134), svreinterpret_f64_f32(v379));
    svfloat32_t v434 = svadd_f32_x(svptrue_b32(), v430, v432);
    svfloat32_t v435 = svsub_f32_x(svptrue_b32(), v430, v432);
    svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v431, v433);
    svfloat32_t v437 = svsub_f32_x(svptrue_b32(), v431, v433);
    svfloat32_t v587 = svadd_f32_x(svptrue_b32(), v586, v532);
    svfloat32_t v588 = svsub_f32_x(svptrue_b32(), v586, v532);
    svfloat32_t v634 = svadd_f32_x(svptrue_b32(), v429, v501);
    svfloat32_t v683 = svadd_f32_x(svptrue_b32(), v682, v539);
    svfloat32_t v684 = svsub_f32_x(svptrue_b32(), v682, v539);
    svfloat32_t v730 = svadd_f32_x(svptrue_b32(), v428, v500);
    svst1_f64(pred_full, (double *)(v1080), svreinterpret_f64_f32(v429));
    svst1_f64(pred_full, (double *)(v1188), svreinterpret_f64_f32(v428));
    svfloat32_t v610 = svadd_f32_x(svptrue_b32(), v435, v507);
    svfloat32_t v635 = svadd_f32_x(svptrue_b32(), v634, v577);
    svfloat32_t v636 = svsub_f32_x(svptrue_b32(), v634, v577);
    svfloat32_t v658 = svadd_f32_x(svptrue_b32(), v436, v508);
    svfloat32_t v706 = svadd_f32_x(svptrue_b32(), v437, v509);
    svfloat32_t v731 = svadd_f32_x(svptrue_b32(), v730, v576);
    svfloat32_t v732 = svsub_f32_x(svptrue_b32(), v730, v576);
    svfloat32_t v754 = svadd_f32_x(svptrue_b32(), v434, v506);
    svst1_f64(pred_full, (double *)(v1035), svreinterpret_f64_f32(v588));
    svst1_f64(pred_full, (double *)(v1044), svreinterpret_f64_f32(v587));
    svst1_f64(pred_full, (double *)(v1053), svreinterpret_f64_f32(v435));
    svst1_f64(pred_full, (double *)(v1107), svreinterpret_f64_f32(v436));
    svst1_f64(pred_full, (double *)(v1143), svreinterpret_f64_f32(v684));
    svst1_f64(pred_full, (double *)(v1152), svreinterpret_f64_f32(v683));
    svst1_f64(pred_full, (double *)(v1161), svreinterpret_f64_f32(v437));
    svst1_f64(pred_full, (double *)(v1215), svreinterpret_f64_f32(v434));
    svfloat32_t v611 = svadd_f32_x(svptrue_b32(), v610, v583);
    svfloat32_t v612 = svsub_f32_x(svptrue_b32(), v610, v583);
    svfloat32_t v659 = svadd_f32_x(svptrue_b32(), v658, v584);
    svfloat32_t v660 = svsub_f32_x(svptrue_b32(), v658, v584);
    svfloat32_t v707 = svadd_f32_x(svptrue_b32(), v706, v585);
    svfloat32_t v708 = svsub_f32_x(svptrue_b32(), v706, v585);
    svfloat32_t v755 = svadd_f32_x(svptrue_b32(), v754, v582);
    svfloat32_t v756 = svsub_f32_x(svptrue_b32(), v754, v582);
    svst1_f64(pred_full, (double *)(v1089), svreinterpret_f64_f32(v636));
    svst1_f64(pred_full, (double *)(v1098), svreinterpret_f64_f32(v635));
    svst1_f64(pred_full, (double *)(v1197), svreinterpret_f64_f32(v732));
    svst1_f64(pred_full, (double *)(v1206), svreinterpret_f64_f32(v731));
    svst1_f64(pred_full, (double *)(v1062), svreinterpret_f64_f32(v612));
    svst1_f64(pred_full, (double *)(v1071), svreinterpret_f64_f32(v611));
    svst1_f64(pred_full, (double *)(v1116), svreinterpret_f64_f32(v660));
    svst1_f64(pred_full, (double *)(v1125), svreinterpret_f64_f32(v659));
    svst1_f64(pred_full, (double *)(v1170), svreinterpret_f64_f32(v708));
    svst1_f64(pred_full, (double *)(v1179), svreinterpret_f64_f32(v707));
    svst1_f64(pred_full, (double *)(v1224), svreinterpret_f64_f32(v756));
    svst1_f64(pred_full, (double *)(v1233), svreinterpret_f64_f32(v755));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu25(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v1989 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v1318 = 9.6858316112863108e-01F;
    float v1322 = -2.4868988716485479e-01F;
    float v1323 = 2.4868988716485479e-01F;
    float v1486 = 8.7630668004386358e-01F;
    float v1490 = -4.8175367410171532e-01F;
    float v1491 = 4.8175367410171532e-01F;
    float v1654 = 7.2896862742141155e-01F;
    float v1658 = -6.8454710592868862e-01F;
    float v1659 = 6.8454710592868862e-01F;
    float v1668 = 6.2790519529313527e-02F;
    float v1672 = -9.9802672842827156e-01F;
    float v1673 = 9.9802672842827156e-01F;
    float v1822 = 5.3582679497899655e-01F;
    float v1826 = -8.4432792550201508e-01F;
    float v1827 = 8.4432792550201508e-01F;
    float v1836 = -4.2577929156507272e-01F;
    float v1840 = -9.0482705246601947e-01F;
    float v1841 = 9.0482705246601947e-01F;
    float v1850 = -6.3742398974868952e-01F;
    float v1854 = 7.7051324277578936e-01F;
    float v1855 = -7.7051324277578936e-01F;
    float v1871 = -9.9211470131447776e-01F;
    float v1875 = -1.2533323356430454e-01F;
    float v1876 = 1.2533323356430454e-01F;
    float v1894 = 2.5000000000000000e-01F;
    float v1906 = 5.5901699437494745e-01F;
    float v1918 = 6.1803398874989490e-01F;
    float v1947 = 9.5105651629515353e-01F;
    float v1948 = -9.5105651629515353e-01F;
    float32x2_t v1950 = (float32x2_t){v4, v4};
    float v1977 = 2.0000000000000000e+00F;
    const float32x2_t *v3677 = &v5[istride];
    float32x2_t *v3932 = &v6[ostride];
    float32x2_t v1319 = (float32x2_t){v1318, v1318};
    float32x2_t v1324 = (float32x2_t){v1322, v1323};
    float32x2_t v1487 = (float32x2_t){v1486, v1486};
    float32x2_t v1492 = (float32x2_t){v1490, v1491};
    float32x2_t v1655 = (float32x2_t){v1654, v1654};
    float32x2_t v1660 = (float32x2_t){v1658, v1659};
    float32x2_t v1669 = (float32x2_t){v1668, v1668};
    float32x2_t v1674 = (float32x2_t){v1672, v1673};
    float32x2_t v1709 = (float32x2_t){v1855, v1854};
    float32x2_t v1823 = (float32x2_t){v1822, v1822};
    float32x2_t v1828 = (float32x2_t){v1826, v1827};
    float32x2_t v1837 = (float32x2_t){v1836, v1836};
    float32x2_t v1842 = (float32x2_t){v1840, v1841};
    float32x2_t v1851 = (float32x2_t){v1850, v1850};
    float32x2_t v1856 = (float32x2_t){v1854, v1855};
    float32x2_t v1872 = (float32x2_t){v1871, v1871};
    float32x2_t v1877 = (float32x2_t){v1875, v1876};
    float32x2_t v1895 = (float32x2_t){v1894, v1894};
    float32x2_t v1907 = (float32x2_t){v1906, v1906};
    float32x2_t v1919 = (float32x2_t){v1918, v1918};
    float32x2_t v1949 = (float32x2_t){v1947, v1948};
    float32x2_t v1978 = (float32x2_t){v1977, v1977};
    const float32x2_t *v3877 = &v5[0];
    float32x2_t *v3887 = &v6[0];
    float32x4_t v4163 = vld1q_f32((const float32_t *)v3677);
    float32x4_t v35 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v37 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v54 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v56 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v73 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[28]));
    float32x4_t v75 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[29]));
    float32x4_t v92 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[38]));
    float32x4_t v94 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[39]));
    float32x4_t v106 = vtrn1q_f32(v4163, v4163);
    float32x4_t v107 = vtrn2q_f32(v4163, v4163);
    float32x4_t v111 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v113 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v130 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v132 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v149 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v151 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v168 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[30]));
    float32x4_t v170 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[31]));
    float32x4_t v187 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[40]));
    float32x4_t v189 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[41]));
    float32x4_t v206 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v208 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v225 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v227 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v244 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v246 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v263 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[32]));
    float32x4_t v265 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[33]));
    float32x4_t v282 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[42]));
    float32x4_t v284 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[43]));
    float32x4_t v301 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v303 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v320 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v322 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v339 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v341 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v358 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[34]));
    float32x4_t v360 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[35]));
    float32x4_t v377 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[44]));
    float32x4_t v379 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[45]));
    float32x4_t v396 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v398 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v415 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v417 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v434 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v436 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v453 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[36]));
    float32x4_t v455 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[37]));
    float32x4_t v472 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[46]));
    float32x4_t v474 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[47]));
    float32x4_t v1320 = vcombine_f32(v1319, v1319);
    float32x2_t v1326 = vmul_f32(v1950, v1324);
    float32x4_t v1488 = vcombine_f32(v1487, v1487);
    float32x2_t v1494 = vmul_f32(v1950, v1492);
    float32x4_t v1656 = vcombine_f32(v1655, v1655);
    float32x2_t v1662 = vmul_f32(v1950, v1660);
    float32x4_t v1670 = vcombine_f32(v1669, v1669);
    float32x2_t v1676 = vmul_f32(v1950, v1674);
    float32x2_t v1711 = vmul_f32(v1950, v1709);
    float32x4_t v1824 = vcombine_f32(v1823, v1823);
    float32x2_t v1830 = vmul_f32(v1950, v1828);
    float32x4_t v1838 = vcombine_f32(v1837, v1837);
    float32x2_t v1844 = vmul_f32(v1950, v1842);
    float32x4_t v1852 = vcombine_f32(v1851, v1851);
    float32x2_t v1858 = vmul_f32(v1950, v1856);
    float32x4_t v1873 = vcombine_f32(v1872, v1872);
    float32x2_t v1879 = vmul_f32(v1950, v1877);
    float32x4_t v1896 = vcombine_f32(v1895, v1895);
    float32x4_t v1908 = vcombine_f32(v1907, v1907);
    float32x4_t v1920 = vcombine_f32(v1919, v1919);
    float32x2_t v1951 = vmul_f32(v1950, v1949);
    float32x4_t v1979 = vcombine_f32(v1978, v1978);
    const float32x2_t *v3637 = &v5[istride * 5];
    const float32x2_t *v3647 = &v5[istride * 10];
    const float32x2_t *v3657 = &v5[istride * 15];
    const float32x2_t *v3667 = &v5[istride * 20];
    const float32x2_t *v3686 = &v5[istride * 6];
    const float32x2_t *v3696 = &v5[istride * 11];
    const float32x2_t *v3706 = &v5[istride * 16];
    const float32x2_t *v3716 = &v5[istride * 21];
    const float32x2_t *v3726 = &v5[istride * 2];
    const float32x2_t *v3736 = &v5[istride * 7];
    const float32x2_t *v3746 = &v5[istride * 12];
    const float32x2_t *v3756 = &v5[istride * 17];
    const float32x2_t *v3766 = &v5[istride * 22];
    const float32x2_t *v3776 = &v5[istride * 3];
    const float32x2_t *v3786 = &v5[istride * 8];
    const float32x2_t *v3796 = &v5[istride * 13];
    const float32x2_t *v3806 = &v5[istride * 18];
    const float32x2_t *v3816 = &v5[istride * 23];
    const float32x2_t *v3826 = &v5[istride * 4];
    const float32x2_t *v3836 = &v5[istride * 9];
    const float32x2_t *v3846 = &v5[istride * 14];
    const float32x2_t *v3856 = &v5[istride * 19];
    const float32x2_t *v3866 = &v5[istride * 24];
    float32x2_t *v3896 = &v6[ostride * 5];
    float32x2_t *v3905 = &v6[ostride * 10];
    float32x2_t *v3914 = &v6[ostride * 15];
    float32x2_t *v3923 = &v6[ostride * 20];
    float32x2_t *v3941 = &v6[ostride * 6];
    float32x2_t *v3950 = &v6[ostride * 11];
    float32x2_t *v3959 = &v6[ostride * 16];
    float32x2_t *v3968 = &v6[ostride * 21];
    float32x2_t *v3977 = &v6[ostride * 2];
    float32x2_t *v3986 = &v6[ostride * 7];
    float32x2_t *v3995 = &v6[ostride * 12];
    float32x2_t *v4004 = &v6[ostride * 17];
    float32x2_t *v4013 = &v6[ostride * 22];
    float32x2_t *v4022 = &v6[ostride * 3];
    float32x2_t *v4031 = &v6[ostride * 8];
    float32x2_t *v4040 = &v6[ostride * 13];
    float32x2_t *v4049 = &v6[ostride * 18];
    float32x2_t *v4058 = &v6[ostride * 23];
    float32x2_t *v4067 = &v6[ostride * 4];
    float32x2_t *v4076 = &v6[ostride * 9];
    float32x2_t *v4085 = &v6[ostride * 14];
    float32x2_t *v4094 = &v6[ostride * 19];
    float32x2_t *v4103 = &v6[ostride * 24];
    float32x4_t v4203 = vld1q_f32((const float32_t *)v3877);
    float32x4_t v112 = vmulq_f32(v106, v111);
    float32x4_t v1328 = vcombine_f32(v1326, v1326);
    float32x4_t v1496 = vcombine_f32(v1494, v1494);
    float32x4_t v1664 = vcombine_f32(v1662, v1662);
    float32x4_t v1678 = vcombine_f32(v1676, v1676);
    float32x4_t v1713 = vcombine_f32(v1711, v1711);
    float32x4_t v1832 = vcombine_f32(v1830, v1830);
    float32x4_t v1846 = vcombine_f32(v1844, v1844);
    float32x4_t v1860 = vcombine_f32(v1858, v1858);
    float32x4_t v1881 = vcombine_f32(v1879, v1879);
    float32x4_t v1953 = vcombine_f32(v1951, v1951);
    float32x4_t v4155 = vld1q_f32((const float32_t *)v3637);
    float32x4_t v4157 = vld1q_f32((const float32_t *)v3647);
    float32x4_t v4159 = vld1q_f32((const float32_t *)v3657);
    float32x4_t v4161 = vld1q_f32((const float32_t *)v3667);
    float32x4_t v4165 = vld1q_f32((const float32_t *)v3686);
    float32x4_t v4167 = vld1q_f32((const float32_t *)v3696);
    float32x4_t v4169 = vld1q_f32((const float32_t *)v3706);
    float32x4_t v4171 = vld1q_f32((const float32_t *)v3716);
    float32x4_t v4173 = vld1q_f32((const float32_t *)v3726);
    float32x4_t v4175 = vld1q_f32((const float32_t *)v3736);
    float32x4_t v4177 = vld1q_f32((const float32_t *)v3746);
    float32x4_t v4179 = vld1q_f32((const float32_t *)v3756);
    float32x4_t v4181 = vld1q_f32((const float32_t *)v3766);
    float32x4_t v4183 = vld1q_f32((const float32_t *)v3776);
    float32x4_t v4185 = vld1q_f32((const float32_t *)v3786);
    float32x4_t v4187 = vld1q_f32((const float32_t *)v3796);
    float32x4_t v4189 = vld1q_f32((const float32_t *)v3806);
    float32x4_t v4191 = vld1q_f32((const float32_t *)v3816);
    float32x4_t v4193 = vld1q_f32((const float32_t *)v3826);
    float32x4_t v4195 = vld1q_f32((const float32_t *)v3836);
    float32x4_t v4197 = vld1q_f32((const float32_t *)v3846);
    float32x4_t v4199 = vld1q_f32((const float32_t *)v3856);
    float32x4_t v4201 = vld1q_f32((const float32_t *)v3866);
    float32x4_t v30 = vtrn1q_f32(v4155, v4155);
    float32x4_t v31 = vtrn2q_f32(v4155, v4155);
    float32x4_t v49 = vtrn1q_f32(v4157, v4157);
    float32x4_t v50 = vtrn2q_f32(v4157, v4157);
    float32x4_t v68 = vtrn1q_f32(v4159, v4159);
    float32x4_t v69 = vtrn2q_f32(v4159, v4159);
    float32x4_t v87 = vtrn1q_f32(v4161, v4161);
    float32x4_t v88 = vtrn2q_f32(v4161, v4161);
    float32x4_t v115 = vfmaq_f32(v112, v107, v113);
    float32x4_t v125 = vtrn1q_f32(v4165, v4165);
    float32x4_t v126 = vtrn2q_f32(v4165, v4165);
    float32x4_t v144 = vtrn1q_f32(v4167, v4167);
    float32x4_t v145 = vtrn2q_f32(v4167, v4167);
    float32x4_t v163 = vtrn1q_f32(v4169, v4169);
    float32x4_t v164 = vtrn2q_f32(v4169, v4169);
    float32x4_t v182 = vtrn1q_f32(v4171, v4171);
    float32x4_t v183 = vtrn2q_f32(v4171, v4171);
    float32x4_t v201 = vtrn1q_f32(v4173, v4173);
    float32x4_t v202 = vtrn2q_f32(v4173, v4173);
    float32x4_t v220 = vtrn1q_f32(v4175, v4175);
    float32x4_t v221 = vtrn2q_f32(v4175, v4175);
    float32x4_t v239 = vtrn1q_f32(v4177, v4177);
    float32x4_t v240 = vtrn2q_f32(v4177, v4177);
    float32x4_t v258 = vtrn1q_f32(v4179, v4179);
    float32x4_t v259 = vtrn2q_f32(v4179, v4179);
    float32x4_t v277 = vtrn1q_f32(v4181, v4181);
    float32x4_t v278 = vtrn2q_f32(v4181, v4181);
    float32x4_t v296 = vtrn1q_f32(v4183, v4183);
    float32x4_t v297 = vtrn2q_f32(v4183, v4183);
    float32x4_t v315 = vtrn1q_f32(v4185, v4185);
    float32x4_t v316 = vtrn2q_f32(v4185, v4185);
    float32x4_t v334 = vtrn1q_f32(v4187, v4187);
    float32x4_t v335 = vtrn2q_f32(v4187, v4187);
    float32x4_t v353 = vtrn1q_f32(v4189, v4189);
    float32x4_t v354 = vtrn2q_f32(v4189, v4189);
    float32x4_t v372 = vtrn1q_f32(v4191, v4191);
    float32x4_t v373 = vtrn2q_f32(v4191, v4191);
    float32x4_t v391 = vtrn1q_f32(v4193, v4193);
    float32x4_t v392 = vtrn2q_f32(v4193, v4193);
    float32x4_t v410 = vtrn1q_f32(v4195, v4195);
    float32x4_t v411 = vtrn2q_f32(v4195, v4195);
    float32x4_t v429 = vtrn1q_f32(v4197, v4197);
    float32x4_t v430 = vtrn2q_f32(v4197, v4197);
    float32x4_t v448 = vtrn1q_f32(v4199, v4199);
    float32x4_t v449 = vtrn2q_f32(v4199, v4199);
    float32x4_t v467 = vtrn1q_f32(v4201, v4201);
    float32x4_t v468 = vtrn2q_f32(v4201, v4201);
    float32x4_t v36 = vmulq_f32(v30, v35);
    float32x4_t v55 = vmulq_f32(v49, v54);
    float32x4_t v74 = vmulq_f32(v68, v73);
    float32x4_t v93 = vmulq_f32(v87, v92);
    float32x4_t v131 = vmulq_f32(v125, v130);
    float32x4_t v150 = vmulq_f32(v144, v149);
    float32x4_t v169 = vmulq_f32(v163, v168);
    float32x4_t v188 = vmulq_f32(v182, v187);
    float32x4_t v207 = vmulq_f32(v201, v206);
    float32x4_t v226 = vmulq_f32(v220, v225);
    float32x4_t v245 = vmulq_f32(v239, v244);
    float32x4_t v264 = vmulq_f32(v258, v263);
    float32x4_t v283 = vmulq_f32(v277, v282);
    float32x4_t v302 = vmulq_f32(v296, v301);
    float32x4_t v321 = vmulq_f32(v315, v320);
    float32x4_t v340 = vmulq_f32(v334, v339);
    float32x4_t v359 = vmulq_f32(v353, v358);
    float32x4_t v378 = vmulq_f32(v372, v377);
    float32x4_t v397 = vmulq_f32(v391, v396);
    float32x4_t v416 = vmulq_f32(v410, v415);
    float32x4_t v435 = vmulq_f32(v429, v434);
    float32x4_t v454 = vmulq_f32(v448, v453);
    float32x4_t v473 = vmulq_f32(v467, v472);
    float32x4_t v39 = vfmaq_f32(v36, v31, v37);
    float32x4_t v58 = vfmaq_f32(v55, v50, v56);
    float32x4_t v77 = vfmaq_f32(v74, v69, v75);
    float32x4_t v96 = vfmaq_f32(v93, v88, v94);
    float32x4_t v134 = vfmaq_f32(v131, v126, v132);
    float32x4_t v153 = vfmaq_f32(v150, v145, v151);
    float32x4_t v172 = vfmaq_f32(v169, v164, v170);
    float32x4_t v191 = vfmaq_f32(v188, v183, v189);
    float32x4_t v210 = vfmaq_f32(v207, v202, v208);
    float32x4_t v229 = vfmaq_f32(v226, v221, v227);
    float32x4_t v248 = vfmaq_f32(v245, v240, v246);
    float32x4_t v267 = vfmaq_f32(v264, v259, v265);
    float32x4_t v286 = vfmaq_f32(v283, v278, v284);
    float32x4_t v305 = vfmaq_f32(v302, v297, v303);
    float32x4_t v324 = vfmaq_f32(v321, v316, v322);
    float32x4_t v343 = vfmaq_f32(v340, v335, v341);
    float32x4_t v362 = vfmaq_f32(v359, v354, v360);
    float32x4_t v381 = vfmaq_f32(v378, v373, v379);
    float32x4_t v400 = vfmaq_f32(v397, v392, v398);
    float32x4_t v419 = vfmaq_f32(v416, v411, v417);
    float32x4_t v438 = vfmaq_f32(v435, v430, v436);
    float32x4_t v457 = vfmaq_f32(v454, v449, v455);
    float32x4_t v476 = vfmaq_f32(v473, v468, v474);
    float32x4_t v526 = vsubq_f32(v39, v96);
    float32x4_t v531 = vmulq_f32(v39, v1979);
    float32x4_t v547 = vsubq_f32(v58, v77);
    float32x4_t v552 = vmulq_f32(v58, v1979);
    float32x4_t v659 = vsubq_f32(v134, v191);
    float32x4_t v664 = vmulq_f32(v134, v1979);
    float32x4_t v680 = vsubq_f32(v153, v172);
    float32x4_t v685 = vmulq_f32(v153, v1979);
    float32x4_t v792 = vsubq_f32(v229, v286);
    float32x4_t v797 = vmulq_f32(v229, v1979);
    float32x4_t v813 = vsubq_f32(v248, v267);
    float32x4_t v818 = vmulq_f32(v248, v1979);
    float32x4_t v925 = vsubq_f32(v324, v381);
    float32x4_t v930 = vmulq_f32(v324, v1979);
    float32x4_t v946 = vsubq_f32(v343, v362);
    float32x4_t v951 = vmulq_f32(v343, v1979);
    float32x4_t v1058 = vsubq_f32(v419, v476);
    float32x4_t v1063 = vmulq_f32(v419, v1979);
    float32x4_t v1079 = vsubq_f32(v438, v457);
    float32x4_t v1084 = vmulq_f32(v438, v1979);
    float32x4_t v532 = vsubq_f32(v531, v526);
    float32x4_t v553 = vsubq_f32(v552, v547);
    float32x4_t v566 = vmulq_f32(v547, v1920);
    float32x4_t v584 = vmulq_f32(v526, v1920);
    float32x4_t v665 = vsubq_f32(v664, v659);
    float32x4_t v686 = vsubq_f32(v685, v680);
    float32x4_t v699 = vmulq_f32(v680, v1920);
    float32x4_t v717 = vmulq_f32(v659, v1920);
    float32x4_t v798 = vsubq_f32(v797, v792);
    float32x4_t v819 = vsubq_f32(v818, v813);
    float32x4_t v832 = vmulq_f32(v813, v1920);
    float32x4_t v850 = vmulq_f32(v792, v1920);
    float32x4_t v931 = vsubq_f32(v930, v925);
    float32x4_t v952 = vsubq_f32(v951, v946);
    float32x4_t v965 = vmulq_f32(v946, v1920);
    float32x4_t v983 = vmulq_f32(v925, v1920);
    float32x4_t v1064 = vsubq_f32(v1063, v1058);
    float32x4_t v1085 = vsubq_f32(v1084, v1079);
    float32x4_t v1098 = vmulq_f32(v1079, v1920);
    float32x4_t v1116 = vmulq_f32(v1058, v1920);
    float32x4_t v554 = vaddq_f32(v532, v553);
    float32x4_t v555 = vsubq_f32(v532, v553);
    float32x4_t v567 = vaddq_f32(v526, v566);
    float32x4_t v585 = vsubq_f32(v584, v547);
    float32x4_t v687 = vaddq_f32(v665, v686);
    float32x4_t v688 = vsubq_f32(v665, v686);
    float32x4_t v700 = vaddq_f32(v659, v699);
    float32x4_t v718 = vsubq_f32(v717, v680);
    float32x4_t v820 = vaddq_f32(v798, v819);
    float32x4_t v821 = vsubq_f32(v798, v819);
    float32x4_t v833 = vaddq_f32(v792, v832);
    float32x4_t v851 = vsubq_f32(v850, v813);
    float32x4_t v953 = vaddq_f32(v931, v952);
    float32x4_t v954 = vsubq_f32(v931, v952);
    float32x4_t v966 = vaddq_f32(v925, v965);
    float32x4_t v984 = vsubq_f32(v983, v946);
    float32x4_t v1086 = vaddq_f32(v1064, v1085);
    float32x4_t v1087 = vsubq_f32(v1064, v1085);
    float32x4_t v1099 = vaddq_f32(v1058, v1098);
    float32x4_t v1117 = vsubq_f32(v1116, v1079);
    float32x4_t v560 = vmulq_f32(v554, v1896);
    float32x4_t v572 = vmulq_f32(v555, v1908);
    float32x4_t v586 = vaddq_f32(v4203, v554);
    float32x4_t v592 = vrev64q_f32(v567);
    float32x4_t v601 = vrev64q_f32(v585);
    float32x4_t v693 = vmulq_f32(v687, v1896);
    float32x4_t v705 = vmulq_f32(v688, v1908);
    float32x4_t v719 = vaddq_f32(v115, v687);
    float32x4_t v725 = vrev64q_f32(v700);
    float32x4_t v734 = vrev64q_f32(v718);
    float32x4_t v826 = vmulq_f32(v820, v1896);
    float32x4_t v838 = vmulq_f32(v821, v1908);
    float32x4_t v852 = vaddq_f32(v210, v820);
    float32x4_t v858 = vrev64q_f32(v833);
    float32x4_t v867 = vrev64q_f32(v851);
    float32x4_t v959 = vmulq_f32(v953, v1896);
    float32x4_t v971 = vmulq_f32(v954, v1908);
    float32x4_t v985 = vaddq_f32(v305, v953);
    float32x4_t v991 = vrev64q_f32(v966);
    float32x4_t v1000 = vrev64q_f32(v984);
    float32x4_t v1092 = vmulq_f32(v1086, v1896);
    float32x4_t v1104 = vmulq_f32(v1087, v1908);
    float32x4_t v1118 = vaddq_f32(v400, v1086);
    float32x4_t v1124 = vrev64q_f32(v1099);
    float32x4_t v1133 = vrev64q_f32(v1117);
    float32x4_t v561 = vsubq_f32(v4203, v560);
    float32x4_t v594 = vmulq_f32(v592, v1953);
    float32x4_t v603 = vmulq_f32(v601, v1953);
    float32x4_t v694 = vsubq_f32(v115, v693);
    float32x4_t v727 = vmulq_f32(v725, v1953);
    float32x4_t v736 = vmulq_f32(v734, v1953);
    float32x4_t v827 = vsubq_f32(v210, v826);
    float32x4_t v860 = vmulq_f32(v858, v1953);
    float32x4_t v869 = vmulq_f32(v867, v1953);
    float32x4_t v960 = vsubq_f32(v305, v959);
    float32x4_t v993 = vmulq_f32(v991, v1953);
    float32x4_t v1002 = vmulq_f32(v1000, v1953);
    float32x4_t v1093 = vsubq_f32(v400, v1092);
    float32x4_t v1126 = vmulq_f32(v1124, v1953);
    float32x4_t v1135 = vmulq_f32(v1133, v1953);
    float32x4_t v1191 = vsubq_f32(v719, v1118);
    float32x4_t v1196 = vmulq_f32(v719, v1979);
    float32x4_t v1212 = vsubq_f32(v852, v985);
    float32x4_t v1217 = vmulq_f32(v852, v1979);
    float32x4_t v573 = vsubq_f32(v561, v572);
    float32x4_t v578 = vmulq_f32(v561, v1979);
    float32x4_t v706 = vsubq_f32(v694, v705);
    float32x4_t v711 = vmulq_f32(v694, v1979);
    float32x4_t v839 = vsubq_f32(v827, v838);
    float32x4_t v844 = vmulq_f32(v827, v1979);
    float32x4_t v972 = vsubq_f32(v960, v971);
    float32x4_t v977 = vmulq_f32(v960, v1979);
    float32x4_t v1105 = vsubq_f32(v1093, v1104);
    float32x4_t v1110 = vmulq_f32(v1093, v1979);
    float32x4_t v1197 = vsubq_f32(v1196, v1191);
    float32x4_t v1218 = vsubq_f32(v1217, v1212);
    float32x4_t v1231 = vmulq_f32(v1212, v1920);
    float32x4_t v1249 = vmulq_f32(v1191, v1920);
    float32x4_t v579 = vsubq_f32(v578, v573);
    float32x4_t v604 = vsubq_f32(v573, v603);
    float32x4_t v609 = vmulq_f32(v573, v1979);
    float32x4_t v712 = vsubq_f32(v711, v706);
    float32x4_t v737 = vsubq_f32(v706, v736);
    float32x4_t v742 = vmulq_f32(v706, v1979);
    float32x4_t v845 = vsubq_f32(v844, v839);
    float32x4_t v870 = vsubq_f32(v839, v869);
    float32x4_t v875 = vmulq_f32(v839, v1979);
    float32x4_t v978 = vsubq_f32(v977, v972);
    float32x4_t v1003 = vsubq_f32(v972, v1002);
    float32x4_t v1008 = vmulq_f32(v972, v1979);
    float32x4_t v1111 = vsubq_f32(v1110, v1105);
    float32x4_t v1136 = vsubq_f32(v1105, v1135);
    float32x4_t v1141 = vmulq_f32(v1105, v1979);
    float32x4_t v1219 = vaddq_f32(v1197, v1218);
    float32x4_t v1220 = vsubq_f32(v1197, v1218);
    float32x4_t v1232 = vaddq_f32(v1191, v1231);
    float32x4_t v1250 = vsubq_f32(v1249, v1212);
    float32x4_t v595 = vsubq_f32(v579, v594);
    float32x4_t v610 = vsubq_f32(v609, v604);
    float32x4_t v615 = vmulq_f32(v579, v1979);
    float32x4_t v728 = vsubq_f32(v712, v727);
    float32x4_t v743 = vsubq_f32(v742, v737);
    float32x4_t v748 = vmulq_f32(v712, v1979);
    float32x4_t v861 = vsubq_f32(v845, v860);
    float32x4_t v876 = vsubq_f32(v875, v870);
    float32x4_t v881 = vmulq_f32(v845, v1979);
    float32x4_t v994 = vsubq_f32(v978, v993);
    float32x4_t v1009 = vsubq_f32(v1008, v1003);
    float32x4_t v1014 = vmulq_f32(v978, v1979);
    float32x4_t v1127 = vsubq_f32(v1111, v1126);
    float32x4_t v1142 = vsubq_f32(v1141, v1136);
    float32x4_t v1147 = vmulq_f32(v1111, v1979);
    float32x4_t v1225 = vmulq_f32(v1219, v1896);
    float32x4_t v1237 = vmulq_f32(v1220, v1908);
    float32x4_t v1251 = vaddq_f32(v586, v1219);
    float32x4_t v1264 = vrev64q_f32(v1232);
    float32x4_t v1280 = vrev64q_f32(v1250);
    float32x4_t v1495 = vrev64q_f32(v737);
    float32x4_t v1509 = vrev64q_f32(v870);
    float32x4_t v1523 = vrev64q_f32(v1136);
    float32x4_t v1544 = vrev64q_f32(v1003);
    float32x4_t v616 = vsubq_f32(v615, v595);
    float32x4_t v749 = vsubq_f32(v748, v728);
    float32x4_t v882 = vsubq_f32(v881, v861);
    float32x4_t v1015 = vsubq_f32(v1014, v994);
    float32x4_t v1148 = vsubq_f32(v1147, v1127);
    float32x4_t v1226 = vsubq_f32(v586, v1225);
    float32x4_t v1266 = vmulq_f32(v1264, v1953);
    float32x4_t v1282 = vmulq_f32(v1280, v1953);
    float32x4_t v1327 = vrev64q_f32(v728);
    float32x4_t v1341 = vrev64q_f32(v861);
    float32x4_t v1355 = vrev64q_f32(v1127);
    float32x4_t v1376 = vrev64q_f32(v994);
    float32x4_t v1497 = vmulq_f32(v1495, v1496);
    float32x4_t v1511 = vmulq_f32(v1509, v1832);
    float32x4_t v1525 = vmulq_f32(v1523, v1846);
    float32x4_t v1546 = vmulq_f32(v1544, v1678);
    float32x4_t v1663 = vrev64q_f32(v743);
    float32x4_t v1677 = vrev64q_f32(v876);
    float32x4_t v1691 = vrev64q_f32(v1142);
    float32x4_t v1712 = vrev64q_f32(v1009);
    vst1q_f32((float32_t *)v3887, v1251);
    float32x4_t v1238 = vsubq_f32(v1226, v1237);
    float32x4_t v1243 = vmulq_f32(v1226, v1979);
    float32x4_t v1329 = vmulq_f32(v1327, v1328);
    float32x4_t v1343 = vmulq_f32(v1341, v1496);
    float32x4_t v1357 = vmulq_f32(v1355, v1832);
    float32x4_t v1378 = vmulq_f32(v1376, v1664);
    float32x4_t v1498 = vfmaq_f32(v1497, v737, v1488);
    float32x4_t v1512 = vfmaq_f32(v1511, v870, v1824);
    float32x4_t v1526 = vfmaq_f32(v1525, v1136, v1838);
    float32x4_t v1547 = vfmaq_f32(v1546, v1003, v1670);
    float32x4_t v1665 = vmulq_f32(v1663, v1664);
    float32x4_t v1679 = vmulq_f32(v1677, v1678);
    float32x4_t v1693 = vmulq_f32(v1691, v1881);
    float32x4_t v1714 = vmulq_f32(v1712, v1713);
    float32x4_t v1831 = vrev64q_f32(v749);
    float32x4_t v1845 = vrev64q_f32(v882);
    float32x4_t v1859 = vrev64q_f32(v1148);
    float32x4_t v1880 = vrev64q_f32(v1015);
    float32x4_t v1244 = vsubq_f32(v1243, v1238);
    float32x4_t v1283 = vsubq_f32(v1238, v1282);
    float32x4_t v1295 = vmulq_f32(v1238, v1979);
    float32x4_t v1330 = vfmaq_f32(v1329, v728, v1320);
    float32x4_t v1344 = vfmaq_f32(v1343, v861, v1488);
    float32x4_t v1358 = vfmaq_f32(v1357, v1127, v1824);
    float32x4_t v1379 = vfmaq_f32(v1378, v994, v1656);
    float32x4_t v1527 = vsubq_f32(v1498, v1526);
    float32x4_t v1532 = vmulq_f32(v1498, v1979);
    float32x4_t v1548 = vsubq_f32(v1512, v1547);
    float32x4_t v1553 = vmulq_f32(v1512, v1979);
    float32x4_t v1666 = vfmaq_f32(v1665, v743, v1656);
    float32x4_t v1680 = vfmaq_f32(v1679, v876, v1670);
    float32x4_t v1694 = vfmaq_f32(v1693, v1142, v1873);
    float32x4_t v1715 = vfmaq_f32(v1714, v1009, v1852);
    float32x4_t v1833 = vmulq_f32(v1831, v1832);
    float32x4_t v1847 = vmulq_f32(v1845, v1846);
    float32x4_t v1861 = vmulq_f32(v1859, v1860);
    float32x4_t v1882 = vmulq_f32(v1880, v1881);
    float32x4_t v1267 = vsubq_f32(v1244, v1266);
    float32x4_t v1296 = vsubq_f32(v1295, v1283);
    float32x4_t v1308 = vmulq_f32(v1244, v1979);
    float32x4_t v1359 = vsubq_f32(v1330, v1358);
    float32x4_t v1364 = vmulq_f32(v1330, v1979);
    float32x4_t v1380 = vsubq_f32(v1344, v1379);
    float32x4_t v1385 = vmulq_f32(v1344, v1979);
    float32x4_t v1533 = vsubq_f32(v1532, v1527);
    float32x4_t v1554 = vsubq_f32(v1553, v1548);
    float32x4_t v1567 = vmulq_f32(v1548, v1920);
    float32x4_t v1585 = vmulq_f32(v1527, v1920);
    float32x4_t v1695 = vsubq_f32(v1666, v1694);
    float32x4_t v1700 = vmulq_f32(v1666, v1979);
    float32x4_t v1716 = vsubq_f32(v1680, v1715);
    float32x4_t v1721 = vmulq_f32(v1680, v1979);
    float32x4_t v1834 = vfmaq_f32(v1833, v749, v1824);
    float32x4_t v1848 = vfmaq_f32(v1847, v882, v1838);
    float32x4_t v1862 = vfmaq_f32(v1861, v1148, v1852);
    float32x4_t v1883 = vfmaq_f32(v1882, v1015, v1873);
    vst1q_f32((float32_t *)v3905, v1283);
    float32x4_t v1309 = vsubq_f32(v1308, v1267);
    float32x4_t v1365 = vsubq_f32(v1364, v1359);
    float32x4_t v1386 = vsubq_f32(v1385, v1380);
    float32x4_t v1399 = vmulq_f32(v1380, v1920);
    float32x4_t v1417 = vmulq_f32(v1359, v1920);
    float32x4_t v1555 = vaddq_f32(v1533, v1554);
    float32x4_t v1556 = vsubq_f32(v1533, v1554);
    float32x4_t v1568 = vaddq_f32(v1527, v1567);
    float32x4_t v1586 = vsubq_f32(v1585, v1548);
    float32x4_t v1701 = vsubq_f32(v1700, v1695);
    float32x4_t v1722 = vsubq_f32(v1721, v1716);
    float32x4_t v1735 = vmulq_f32(v1716, v1920);
    float32x4_t v1753 = vmulq_f32(v1695, v1920);
    float32x4_t v1863 = vsubq_f32(v1834, v1862);
    float32x4_t v1868 = vmulq_f32(v1834, v1979);
    float32x4_t v1884 = vsubq_f32(v1848, v1883);
    float32x4_t v1889 = vmulq_f32(v1848, v1979);
    vst1q_f32((float32_t *)v3896, v1267);
    vst1q_f32((float32_t *)v3914, v1296);
    float32x4_t v1387 = vaddq_f32(v1365, v1386);
    float32x4_t v1388 = vsubq_f32(v1365, v1386);
    float32x4_t v1400 = vaddq_f32(v1359, v1399);
    float32x4_t v1418 = vsubq_f32(v1417, v1380);
    float32x4_t v1561 = vmulq_f32(v1555, v1896);
    float32x4_t v1573 = vmulq_f32(v1556, v1908);
    float32x4_t v1587 = vaddq_f32(v604, v1555);
    float32x4_t v1600 = vrev64q_f32(v1568);
    float32x4_t v1616 = vrev64q_f32(v1586);
    float32x4_t v1723 = vaddq_f32(v1701, v1722);
    float32x4_t v1724 = vsubq_f32(v1701, v1722);
    float32x4_t v1736 = vaddq_f32(v1695, v1735);
    float32x4_t v1754 = vsubq_f32(v1753, v1716);
    float32x4_t v1869 = vsubq_f32(v1868, v1863);
    float32x4_t v1890 = vsubq_f32(v1889, v1884);
    float32x4_t v1903 = vmulq_f32(v1884, v1920);
    float32x4_t v1921 = vmulq_f32(v1863, v1920);
    vst1q_f32((float32_t *)v3923, v1309);
    float32x4_t v1393 = vmulq_f32(v1387, v1896);
    float32x4_t v1405 = vmulq_f32(v1388, v1908);
    float32x4_t v1419 = vaddq_f32(v595, v1387);
    float32x4_t v1432 = vrev64q_f32(v1400);
    float32x4_t v1448 = vrev64q_f32(v1418);
    float32x4_t v1562 = vsubq_f32(v604, v1561);
    float32x4_t v1602 = vmulq_f32(v1600, v1953);
    float32x4_t v1618 = vmulq_f32(v1616, v1953);
    float32x4_t v1729 = vmulq_f32(v1723, v1896);
    float32x4_t v1741 = vmulq_f32(v1724, v1908);
    float32x4_t v1755 = vaddq_f32(v610, v1723);
    float32x4_t v1768 = vrev64q_f32(v1736);
    float32x4_t v1784 = vrev64q_f32(v1754);
    float32x4_t v1891 = vaddq_f32(v1869, v1890);
    float32x4_t v1892 = vsubq_f32(v1869, v1890);
    float32x4_t v1904 = vaddq_f32(v1863, v1903);
    float32x4_t v1922 = vsubq_f32(v1921, v1884);
    vst1q_f32((float32_t *)v3977, v1587);
    float32x4_t v1394 = vsubq_f32(v595, v1393);
    float32x4_t v1434 = vmulq_f32(v1432, v1953);
    float32x4_t v1450 = vmulq_f32(v1448, v1953);
    float32x4_t v1574 = vsubq_f32(v1562, v1573);
    float32x4_t v1579 = vmulq_f32(v1562, v1979);
    float32x4_t v1730 = vsubq_f32(v610, v1729);
    float32x4_t v1770 = vmulq_f32(v1768, v1953);
    float32x4_t v1786 = vmulq_f32(v1784, v1953);
    float32x4_t v1897 = vmulq_f32(v1891, v1896);
    float32x4_t v1909 = vmulq_f32(v1892, v1908);
    float32x4_t v1923 = vaddq_f32(v616, v1891);
    float32x4_t v1936 = vrev64q_f32(v1904);
    float32x4_t v1952 = vrev64q_f32(v1922);
    vst1q_f32((float32_t *)v3932, v1419);
    vst1q_f32((float32_t *)v4022, v1755);
    float32x4_t v1406 = vsubq_f32(v1394, v1405);
    float32x4_t v1411 = vmulq_f32(v1394, v1979);
    float32x4_t v1580 = vsubq_f32(v1579, v1574);
    float32x4_t v1619 = vsubq_f32(v1574, v1618);
    float32x4_t v1631 = vmulq_f32(v1574, v1979);
    float32x4_t v1742 = vsubq_f32(v1730, v1741);
    float32x4_t v1747 = vmulq_f32(v1730, v1979);
    float32x4_t v1898 = vsubq_f32(v616, v1897);
    float32x4_t v1938 = vmulq_f32(v1936, v1953);
    float32x4_t v1954 = vmulq_f32(v1952, v1953);
    vst1q_f32((float32_t *)v4067, v1923);
    float32x4_t v1412 = vsubq_f32(v1411, v1406);
    float32x4_t v1451 = vsubq_f32(v1406, v1450);
    float32x4_t v1463 = vmulq_f32(v1406, v1979);
    float32x4_t v1603 = vsubq_f32(v1580, v1602);
    float32x4_t v1632 = vsubq_f32(v1631, v1619);
    float32x4_t v1644 = vmulq_f32(v1580, v1979);
    float32x4_t v1748 = vsubq_f32(v1747, v1742);
    float32x4_t v1787 = vsubq_f32(v1742, v1786);
    float32x4_t v1799 = vmulq_f32(v1742, v1979);
    float32x4_t v1910 = vsubq_f32(v1898, v1909);
    float32x4_t v1915 = vmulq_f32(v1898, v1979);
    vst1q_f32((float32_t *)v3995, v1619);
    float32x4_t v1435 = vsubq_f32(v1412, v1434);
    float32x4_t v1464 = vsubq_f32(v1463, v1451);
    float32x4_t v1476 = vmulq_f32(v1412, v1979);
    float32x4_t v1645 = vsubq_f32(v1644, v1603);
    float32x4_t v1771 = vsubq_f32(v1748, v1770);
    float32x4_t v1800 = vsubq_f32(v1799, v1787);
    float32x4_t v1812 = vmulq_f32(v1748, v1979);
    float32x4_t v1916 = vsubq_f32(v1915, v1910);
    float32x4_t v1955 = vsubq_f32(v1910, v1954);
    float32x4_t v1967 = vmulq_f32(v1910, v1979);
    vst1q_f32((float32_t *)v3950, v1451);
    vst1q_f32((float32_t *)v3986, v1603);
    vst1q_f32((float32_t *)v4004, v1632);
    vst1q_f32((float32_t *)v4040, v1787);
    float32x4_t v1477 = vsubq_f32(v1476, v1435);
    float32x4_t v1813 = vsubq_f32(v1812, v1771);
    float32x4_t v1939 = vsubq_f32(v1916, v1938);
    float32x4_t v1968 = vsubq_f32(v1967, v1955);
    float32x4_t v1980 = vmulq_f32(v1916, v1979);
    vst1q_f32((float32_t *)v3941, v1435);
    vst1q_f32((float32_t *)v3959, v1464);
    vst1q_f32((float32_t *)v4013, v1645);
    vst1q_f32((float32_t *)v4031, v1771);
    vst1q_f32((float32_t *)v4049, v1800);
    vst1q_f32((float32_t *)v4085, v1955);
    float32x4_t v1981 = vsubq_f32(v1980, v1939);
    vst1q_f32((float32_t *)v3968, v1477);
    vst1q_f32((float32_t *)v4058, v1813);
    vst1q_f32((float32_t *)v4076, v1939);
    vst1q_f32((float32_t *)v4094, v1968);
    vst1q_f32((float32_t *)v4103, v1981);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v1989 * 2; j < howmany; j += 1) {
    float32x2_t v2061 = v5[istride];
    float v3072 = 9.6858316112863108e-01F;
    float v3075 = -2.4868988716485479e-01F;
    float v3076 = 2.4868988716485479e-01F;
    float v3211 = 8.7630668004386358e-01F;
    float v3214 = -4.8175367410171532e-01F;
    float v3215 = 4.8175367410171532e-01F;
    float v3350 = 7.2896862742141155e-01F;
    float v3353 = -6.8454710592868862e-01F;
    float v3354 = 6.8454710592868862e-01F;
    float v3362 = 6.2790519529313527e-02F;
    float v3365 = -9.9802672842827156e-01F;
    float v3366 = 9.9802672842827156e-01F;
    float v3489 = 5.3582679497899655e-01F;
    float v3492 = -8.4432792550201508e-01F;
    float v3493 = 8.4432792550201508e-01F;
    float v3501 = -4.2577929156507272e-01F;
    float v3504 = -9.0482705246601947e-01F;
    float v3505 = 9.0482705246601947e-01F;
    float v3513 = -6.3742398974868952e-01F;
    float v3516 = 7.7051324277578936e-01F;
    float v3517 = -7.7051324277578936e-01F;
    float v3531 = -9.9211470131447776e-01F;
    float v3534 = -1.2533323356430454e-01F;
    float v3535 = 1.2533323356430454e-01F;
    float v3551 = 2.5000000000000000e-01F;
    float v3561 = 5.5901699437494745e-01F;
    float v3571 = 6.1803398874989490e-01F;
    float v3594 = 9.5105651629515353e-01F;
    float v3595 = -9.5105651629515353e-01F;
    float32x2_t v3597 = (float32x2_t){v4, v4};
    float v3618 = 2.0000000000000000e+00F;
    float32x2_t v2003 = v7[8];
    float32x2_t v2008 = v7[9];
    float32x2_t v2018 = v7[18];
    float32x2_t v2023 = v7[19];
    float32x2_t v2033 = v7[28];
    float32x2_t v2038 = v7[29];
    float32x2_t v2048 = v7[38];
    float32x2_t v2053 = v7[39];
    float32x2_t v2063 = v7[0];
    float32x2_t v2064 = vtrn1_f32(v2061, v2061);
    float32x2_t v2065 = vtrn2_f32(v2061, v2061);
    float32x2_t v2068 = v7[1];
    float32x2_t v2078 = v7[10];
    float32x2_t v2083 = v7[11];
    float32x2_t v2093 = v7[20];
    float32x2_t v2098 = v7[21];
    float32x2_t v2108 = v7[30];
    float32x2_t v2113 = v7[31];
    float32x2_t v2123 = v7[40];
    float32x2_t v2128 = v7[41];
    float32x2_t v2138 = v7[2];
    float32x2_t v2143 = v7[3];
    float32x2_t v2153 = v7[12];
    float32x2_t v2158 = v7[13];
    float32x2_t v2168 = v7[22];
    float32x2_t v2173 = v7[23];
    float32x2_t v2183 = v7[32];
    float32x2_t v2188 = v7[33];
    float32x2_t v2198 = v7[42];
    float32x2_t v2203 = v7[43];
    float32x2_t v2213 = v7[4];
    float32x2_t v2218 = v7[5];
    float32x2_t v2228 = v7[14];
    float32x2_t v2233 = v7[15];
    float32x2_t v2243 = v7[24];
    float32x2_t v2248 = v7[25];
    float32x2_t v2258 = v7[34];
    float32x2_t v2263 = v7[35];
    float32x2_t v2273 = v7[44];
    float32x2_t v2278 = v7[45];
    float32x2_t v2288 = v7[6];
    float32x2_t v2293 = v7[7];
    float32x2_t v2303 = v7[16];
    float32x2_t v2308 = v7[17];
    float32x2_t v2318 = v7[26];
    float32x2_t v2323 = v7[27];
    float32x2_t v2333 = v7[36];
    float32x2_t v2338 = v7[37];
    float32x2_t v2348 = v7[46];
    float32x2_t v2353 = v7[47];
    float32x2_t v2361 = v5[0];
    float32x2_t v3073 = (float32x2_t){v3072, v3072};
    float32x2_t v3077 = (float32x2_t){v3075, v3076};
    float32x2_t v3212 = (float32x2_t){v3211, v3211};
    float32x2_t v3216 = (float32x2_t){v3214, v3215};
    float32x2_t v3351 = (float32x2_t){v3350, v3350};
    float32x2_t v3355 = (float32x2_t){v3353, v3354};
    float32x2_t v3363 = (float32x2_t){v3362, v3362};
    float32x2_t v3367 = (float32x2_t){v3365, v3366};
    float32x2_t v3397 = (float32x2_t){v3517, v3516};
    float32x2_t v3490 = (float32x2_t){v3489, v3489};
    float32x2_t v3494 = (float32x2_t){v3492, v3493};
    float32x2_t v3502 = (float32x2_t){v3501, v3501};
    float32x2_t v3506 = (float32x2_t){v3504, v3505};
    float32x2_t v3514 = (float32x2_t){v3513, v3513};
    float32x2_t v3518 = (float32x2_t){v3516, v3517};
    float32x2_t v3532 = (float32x2_t){v3531, v3531};
    float32x2_t v3536 = (float32x2_t){v3534, v3535};
    float32x2_t v3552 = (float32x2_t){v3551, v3551};
    float32x2_t v3562 = (float32x2_t){v3561, v3561};
    float32x2_t v3572 = (float32x2_t){v3571, v3571};
    float32x2_t v3596 = (float32x2_t){v3594, v3595};
    float32x2_t v3619 = (float32x2_t){v3618, v3618};
    float32x2_t v2001 = v5[istride * 5];
    float32x2_t v2016 = v5[istride * 10];
    float32x2_t v2031 = v5[istride * 15];
    float32x2_t v2046 = v5[istride * 20];
    float32x2_t v2069 = vmul_f32(v2064, v2063);
    float32x2_t v2076 = v5[istride * 6];
    float32x2_t v2091 = v5[istride * 11];
    float32x2_t v2106 = v5[istride * 16];
    float32x2_t v2121 = v5[istride * 21];
    float32x2_t v2136 = v5[istride * 2];
    float32x2_t v2151 = v5[istride * 7];
    float32x2_t v2166 = v5[istride * 12];
    float32x2_t v2181 = v5[istride * 17];
    float32x2_t v2196 = v5[istride * 22];
    float32x2_t v2211 = v5[istride * 3];
    float32x2_t v2226 = v5[istride * 8];
    float32x2_t v2241 = v5[istride * 13];
    float32x2_t v2256 = v5[istride * 18];
    float32x2_t v2271 = v5[istride * 23];
    float32x2_t v2286 = v5[istride * 4];
    float32x2_t v2301 = v5[istride * 9];
    float32x2_t v2316 = v5[istride * 14];
    float32x2_t v2331 = v5[istride * 19];
    float32x2_t v2346 = v5[istride * 24];
    float32x2_t v3079 = vmul_f32(v3597, v3077);
    float32x2_t v3218 = vmul_f32(v3597, v3216);
    float32x2_t v3357 = vmul_f32(v3597, v3355);
    float32x2_t v3369 = vmul_f32(v3597, v3367);
    float32x2_t v3399 = vmul_f32(v3597, v3397);
    float32x2_t v3496 = vmul_f32(v3597, v3494);
    float32x2_t v3508 = vmul_f32(v3597, v3506);
    float32x2_t v3520 = vmul_f32(v3597, v3518);
    float32x2_t v3538 = vmul_f32(v3597, v3536);
    float32x2_t v3598 = vmul_f32(v3597, v3596);
    float32x2_t v2004 = vtrn1_f32(v2001, v2001);
    float32x2_t v2005 = vtrn2_f32(v2001, v2001);
    float32x2_t v2019 = vtrn1_f32(v2016, v2016);
    float32x2_t v2020 = vtrn2_f32(v2016, v2016);
    float32x2_t v2034 = vtrn1_f32(v2031, v2031);
    float32x2_t v2035 = vtrn2_f32(v2031, v2031);
    float32x2_t v2049 = vtrn1_f32(v2046, v2046);
    float32x2_t v2050 = vtrn2_f32(v2046, v2046);
    float32x2_t v2071 = vfma_f32(v2069, v2065, v2068);
    float32x2_t v2079 = vtrn1_f32(v2076, v2076);
    float32x2_t v2080 = vtrn2_f32(v2076, v2076);
    float32x2_t v2094 = vtrn1_f32(v2091, v2091);
    float32x2_t v2095 = vtrn2_f32(v2091, v2091);
    float32x2_t v2109 = vtrn1_f32(v2106, v2106);
    float32x2_t v2110 = vtrn2_f32(v2106, v2106);
    float32x2_t v2124 = vtrn1_f32(v2121, v2121);
    float32x2_t v2125 = vtrn2_f32(v2121, v2121);
    float32x2_t v2139 = vtrn1_f32(v2136, v2136);
    float32x2_t v2140 = vtrn2_f32(v2136, v2136);
    float32x2_t v2154 = vtrn1_f32(v2151, v2151);
    float32x2_t v2155 = vtrn2_f32(v2151, v2151);
    float32x2_t v2169 = vtrn1_f32(v2166, v2166);
    float32x2_t v2170 = vtrn2_f32(v2166, v2166);
    float32x2_t v2184 = vtrn1_f32(v2181, v2181);
    float32x2_t v2185 = vtrn2_f32(v2181, v2181);
    float32x2_t v2199 = vtrn1_f32(v2196, v2196);
    float32x2_t v2200 = vtrn2_f32(v2196, v2196);
    float32x2_t v2214 = vtrn1_f32(v2211, v2211);
    float32x2_t v2215 = vtrn2_f32(v2211, v2211);
    float32x2_t v2229 = vtrn1_f32(v2226, v2226);
    float32x2_t v2230 = vtrn2_f32(v2226, v2226);
    float32x2_t v2244 = vtrn1_f32(v2241, v2241);
    float32x2_t v2245 = vtrn2_f32(v2241, v2241);
    float32x2_t v2259 = vtrn1_f32(v2256, v2256);
    float32x2_t v2260 = vtrn2_f32(v2256, v2256);
    float32x2_t v2274 = vtrn1_f32(v2271, v2271);
    float32x2_t v2275 = vtrn2_f32(v2271, v2271);
    float32x2_t v2289 = vtrn1_f32(v2286, v2286);
    float32x2_t v2290 = vtrn2_f32(v2286, v2286);
    float32x2_t v2304 = vtrn1_f32(v2301, v2301);
    float32x2_t v2305 = vtrn2_f32(v2301, v2301);
    float32x2_t v2319 = vtrn1_f32(v2316, v2316);
    float32x2_t v2320 = vtrn2_f32(v2316, v2316);
    float32x2_t v2334 = vtrn1_f32(v2331, v2331);
    float32x2_t v2335 = vtrn2_f32(v2331, v2331);
    float32x2_t v2349 = vtrn1_f32(v2346, v2346);
    float32x2_t v2350 = vtrn2_f32(v2346, v2346);
    float32x2_t v2009 = vmul_f32(v2004, v2003);
    float32x2_t v2024 = vmul_f32(v2019, v2018);
    float32x2_t v2039 = vmul_f32(v2034, v2033);
    float32x2_t v2054 = vmul_f32(v2049, v2048);
    float32x2_t v2084 = vmul_f32(v2079, v2078);
    float32x2_t v2099 = vmul_f32(v2094, v2093);
    float32x2_t v2114 = vmul_f32(v2109, v2108);
    float32x2_t v2129 = vmul_f32(v2124, v2123);
    float32x2_t v2144 = vmul_f32(v2139, v2138);
    float32x2_t v2159 = vmul_f32(v2154, v2153);
    float32x2_t v2174 = vmul_f32(v2169, v2168);
    float32x2_t v2189 = vmul_f32(v2184, v2183);
    float32x2_t v2204 = vmul_f32(v2199, v2198);
    float32x2_t v2219 = vmul_f32(v2214, v2213);
    float32x2_t v2234 = vmul_f32(v2229, v2228);
    float32x2_t v2249 = vmul_f32(v2244, v2243);
    float32x2_t v2264 = vmul_f32(v2259, v2258);
    float32x2_t v2279 = vmul_f32(v2274, v2273);
    float32x2_t v2294 = vmul_f32(v2289, v2288);
    float32x2_t v2309 = vmul_f32(v2304, v2303);
    float32x2_t v2324 = vmul_f32(v2319, v2318);
    float32x2_t v2339 = vmul_f32(v2334, v2333);
    float32x2_t v2354 = vmul_f32(v2349, v2348);
    float32x2_t v2011 = vfma_f32(v2009, v2005, v2008);
    float32x2_t v2026 = vfma_f32(v2024, v2020, v2023);
    float32x2_t v2041 = vfma_f32(v2039, v2035, v2038);
    float32x2_t v2056 = vfma_f32(v2054, v2050, v2053);
    float32x2_t v2086 = vfma_f32(v2084, v2080, v2083);
    float32x2_t v2101 = vfma_f32(v2099, v2095, v2098);
    float32x2_t v2116 = vfma_f32(v2114, v2110, v2113);
    float32x2_t v2131 = vfma_f32(v2129, v2125, v2128);
    float32x2_t v2146 = vfma_f32(v2144, v2140, v2143);
    float32x2_t v2161 = vfma_f32(v2159, v2155, v2158);
    float32x2_t v2176 = vfma_f32(v2174, v2170, v2173);
    float32x2_t v2191 = vfma_f32(v2189, v2185, v2188);
    float32x2_t v2206 = vfma_f32(v2204, v2200, v2203);
    float32x2_t v2221 = vfma_f32(v2219, v2215, v2218);
    float32x2_t v2236 = vfma_f32(v2234, v2230, v2233);
    float32x2_t v2251 = vfma_f32(v2249, v2245, v2248);
    float32x2_t v2266 = vfma_f32(v2264, v2260, v2263);
    float32x2_t v2281 = vfma_f32(v2279, v2275, v2278);
    float32x2_t v2296 = vfma_f32(v2294, v2290, v2293);
    float32x2_t v2311 = vfma_f32(v2309, v2305, v2308);
    float32x2_t v2326 = vfma_f32(v2324, v2320, v2323);
    float32x2_t v2341 = vfma_f32(v2339, v2335, v2338);
    float32x2_t v2356 = vfma_f32(v2354, v2350, v2353);
    float32x2_t v2398 = vsub_f32(v2011, v2056);
    float32x2_t v2402 = vmul_f32(v2011, v3619);
    float32x2_t v2416 = vsub_f32(v2026, v2041);
    float32x2_t v2420 = vmul_f32(v2026, v3619);
    float32x2_t v2512 = vsub_f32(v2086, v2131);
    float32x2_t v2516 = vmul_f32(v2086, v3619);
    float32x2_t v2530 = vsub_f32(v2101, v2116);
    float32x2_t v2534 = vmul_f32(v2101, v3619);
    float32x2_t v2626 = vsub_f32(v2161, v2206);
    float32x2_t v2630 = vmul_f32(v2161, v3619);
    float32x2_t v2644 = vsub_f32(v2176, v2191);
    float32x2_t v2648 = vmul_f32(v2176, v3619);
    float32x2_t v2740 = vsub_f32(v2236, v2281);
    float32x2_t v2744 = vmul_f32(v2236, v3619);
    float32x2_t v2758 = vsub_f32(v2251, v2266);
    float32x2_t v2762 = vmul_f32(v2251, v3619);
    float32x2_t v2854 = vsub_f32(v2311, v2356);
    float32x2_t v2858 = vmul_f32(v2311, v3619);
    float32x2_t v2872 = vsub_f32(v2326, v2341);
    float32x2_t v2876 = vmul_f32(v2326, v3619);
    float32x2_t v2403 = vsub_f32(v2402, v2398);
    float32x2_t v2421 = vsub_f32(v2420, v2416);
    float32x2_t v2432 = vmul_f32(v2416, v3572);
    float32x2_t v2447 = vmul_f32(v2398, v3572);
    float32x2_t v2517 = vsub_f32(v2516, v2512);
    float32x2_t v2535 = vsub_f32(v2534, v2530);
    float32x2_t v2546 = vmul_f32(v2530, v3572);
    float32x2_t v2561 = vmul_f32(v2512, v3572);
    float32x2_t v2631 = vsub_f32(v2630, v2626);
    float32x2_t v2649 = vsub_f32(v2648, v2644);
    float32x2_t v2660 = vmul_f32(v2644, v3572);
    float32x2_t v2675 = vmul_f32(v2626, v3572);
    float32x2_t v2745 = vsub_f32(v2744, v2740);
    float32x2_t v2763 = vsub_f32(v2762, v2758);
    float32x2_t v2774 = vmul_f32(v2758, v3572);
    float32x2_t v2789 = vmul_f32(v2740, v3572);
    float32x2_t v2859 = vsub_f32(v2858, v2854);
    float32x2_t v2877 = vsub_f32(v2876, v2872);
    float32x2_t v2888 = vmul_f32(v2872, v3572);
    float32x2_t v2903 = vmul_f32(v2854, v3572);
    float32x2_t v2422 = vadd_f32(v2403, v2421);
    float32x2_t v2423 = vsub_f32(v2403, v2421);
    float32x2_t v2433 = vadd_f32(v2398, v2432);
    float32x2_t v2448 = vsub_f32(v2447, v2416);
    float32x2_t v2536 = vadd_f32(v2517, v2535);
    float32x2_t v2537 = vsub_f32(v2517, v2535);
    float32x2_t v2547 = vadd_f32(v2512, v2546);
    float32x2_t v2562 = vsub_f32(v2561, v2530);
    float32x2_t v2650 = vadd_f32(v2631, v2649);
    float32x2_t v2651 = vsub_f32(v2631, v2649);
    float32x2_t v2661 = vadd_f32(v2626, v2660);
    float32x2_t v2676 = vsub_f32(v2675, v2644);
    float32x2_t v2764 = vadd_f32(v2745, v2763);
    float32x2_t v2765 = vsub_f32(v2745, v2763);
    float32x2_t v2775 = vadd_f32(v2740, v2774);
    float32x2_t v2790 = vsub_f32(v2789, v2758);
    float32x2_t v2878 = vadd_f32(v2859, v2877);
    float32x2_t v2879 = vsub_f32(v2859, v2877);
    float32x2_t v2889 = vadd_f32(v2854, v2888);
    float32x2_t v2904 = vsub_f32(v2903, v2872);
    float32x2_t v2427 = vmul_f32(v2422, v3552);
    float32x2_t v2437 = vmul_f32(v2423, v3562);
    float32x2_t v2449 = vadd_f32(v2361, v2422);
    float32x2_t v2455 = vrev64_f32(v2433);
    float32x2_t v2463 = vrev64_f32(v2448);
    float32x2_t v2541 = vmul_f32(v2536, v3552);
    float32x2_t v2551 = vmul_f32(v2537, v3562);
    float32x2_t v2563 = vadd_f32(v2071, v2536);
    float32x2_t v2569 = vrev64_f32(v2547);
    float32x2_t v2577 = vrev64_f32(v2562);
    float32x2_t v2655 = vmul_f32(v2650, v3552);
    float32x2_t v2665 = vmul_f32(v2651, v3562);
    float32x2_t v2677 = vadd_f32(v2146, v2650);
    float32x2_t v2683 = vrev64_f32(v2661);
    float32x2_t v2691 = vrev64_f32(v2676);
    float32x2_t v2769 = vmul_f32(v2764, v3552);
    float32x2_t v2779 = vmul_f32(v2765, v3562);
    float32x2_t v2791 = vadd_f32(v2221, v2764);
    float32x2_t v2797 = vrev64_f32(v2775);
    float32x2_t v2805 = vrev64_f32(v2790);
    float32x2_t v2883 = vmul_f32(v2878, v3552);
    float32x2_t v2893 = vmul_f32(v2879, v3562);
    float32x2_t v2905 = vadd_f32(v2296, v2878);
    float32x2_t v2911 = vrev64_f32(v2889);
    float32x2_t v2919 = vrev64_f32(v2904);
    float32x2_t v2428 = vsub_f32(v2361, v2427);
    float32x2_t v2456 = vmul_f32(v2455, v3598);
    float32x2_t v2464 = vmul_f32(v2463, v3598);
    float32x2_t v2542 = vsub_f32(v2071, v2541);
    float32x2_t v2570 = vmul_f32(v2569, v3598);
    float32x2_t v2578 = vmul_f32(v2577, v3598);
    float32x2_t v2656 = vsub_f32(v2146, v2655);
    float32x2_t v2684 = vmul_f32(v2683, v3598);
    float32x2_t v2692 = vmul_f32(v2691, v3598);
    float32x2_t v2770 = vsub_f32(v2221, v2769);
    float32x2_t v2798 = vmul_f32(v2797, v3598);
    float32x2_t v2806 = vmul_f32(v2805, v3598);
    float32x2_t v2884 = vsub_f32(v2296, v2883);
    float32x2_t v2912 = vmul_f32(v2911, v3598);
    float32x2_t v2920 = vmul_f32(v2919, v3598);
    float32x2_t v2968 = vsub_f32(v2563, v2905);
    float32x2_t v2972 = vmul_f32(v2563, v3619);
    float32x2_t v2986 = vsub_f32(v2677, v2791);
    float32x2_t v2990 = vmul_f32(v2677, v3619);
    float32x2_t v2438 = vsub_f32(v2428, v2437);
    float32x2_t v2442 = vmul_f32(v2428, v3619);
    float32x2_t v2552 = vsub_f32(v2542, v2551);
    float32x2_t v2556 = vmul_f32(v2542, v3619);
    float32x2_t v2666 = vsub_f32(v2656, v2665);
    float32x2_t v2670 = vmul_f32(v2656, v3619);
    float32x2_t v2780 = vsub_f32(v2770, v2779);
    float32x2_t v2784 = vmul_f32(v2770, v3619);
    float32x2_t v2894 = vsub_f32(v2884, v2893);
    float32x2_t v2898 = vmul_f32(v2884, v3619);
    float32x2_t v2973 = vsub_f32(v2972, v2968);
    float32x2_t v2991 = vsub_f32(v2990, v2986);
    float32x2_t v3002 = vmul_f32(v2986, v3572);
    float32x2_t v3017 = vmul_f32(v2968, v3572);
    float32x2_t v2443 = vsub_f32(v2442, v2438);
    float32x2_t v2465 = vsub_f32(v2438, v2464);
    float32x2_t v2469 = vmul_f32(v2438, v3619);
    float32x2_t v2557 = vsub_f32(v2556, v2552);
    float32x2_t v2579 = vsub_f32(v2552, v2578);
    float32x2_t v2583 = vmul_f32(v2552, v3619);
    float32x2_t v2671 = vsub_f32(v2670, v2666);
    float32x2_t v2693 = vsub_f32(v2666, v2692);
    float32x2_t v2697 = vmul_f32(v2666, v3619);
    float32x2_t v2785 = vsub_f32(v2784, v2780);
    float32x2_t v2807 = vsub_f32(v2780, v2806);
    float32x2_t v2811 = vmul_f32(v2780, v3619);
    float32x2_t v2899 = vsub_f32(v2898, v2894);
    float32x2_t v2921 = vsub_f32(v2894, v2920);
    float32x2_t v2925 = vmul_f32(v2894, v3619);
    float32x2_t v2992 = vadd_f32(v2973, v2991);
    float32x2_t v2993 = vsub_f32(v2973, v2991);
    float32x2_t v3003 = vadd_f32(v2968, v3002);
    float32x2_t v3018 = vsub_f32(v3017, v2986);
    float32x2_t v2457 = vsub_f32(v2443, v2456);
    float32x2_t v2470 = vsub_f32(v2469, v2465);
    float32x2_t v2474 = vmul_f32(v2443, v3619);
    float32x2_t v2571 = vsub_f32(v2557, v2570);
    float32x2_t v2584 = vsub_f32(v2583, v2579);
    float32x2_t v2588 = vmul_f32(v2557, v3619);
    float32x2_t v2685 = vsub_f32(v2671, v2684);
    float32x2_t v2698 = vsub_f32(v2697, v2693);
    float32x2_t v2702 = vmul_f32(v2671, v3619);
    float32x2_t v2799 = vsub_f32(v2785, v2798);
    float32x2_t v2812 = vsub_f32(v2811, v2807);
    float32x2_t v2816 = vmul_f32(v2785, v3619);
    float32x2_t v2913 = vsub_f32(v2899, v2912);
    float32x2_t v2926 = vsub_f32(v2925, v2921);
    float32x2_t v2930 = vmul_f32(v2899, v3619);
    float32x2_t v2997 = vmul_f32(v2992, v3552);
    float32x2_t v3007 = vmul_f32(v2993, v3562);
    float32x2_t v3019 = vadd_f32(v2449, v2992);
    float32x2_t v3030 = vrev64_f32(v3003);
    float32x2_t v3043 = vrev64_f32(v3018);
    float32x2_t v3219 = vrev64_f32(v2579);
    float32x2_t v3231 = vrev64_f32(v2693);
    float32x2_t v3243 = vrev64_f32(v2921);
    float32x2_t v3261 = vrev64_f32(v2807);
    float32x2_t v2475 = vsub_f32(v2474, v2457);
    float32x2_t v2589 = vsub_f32(v2588, v2571);
    float32x2_t v2703 = vsub_f32(v2702, v2685);
    float32x2_t v2817 = vsub_f32(v2816, v2799);
    float32x2_t v2931 = vsub_f32(v2930, v2913);
    float32x2_t v2998 = vsub_f32(v2449, v2997);
    v6[0] = v3019;
    float32x2_t v3031 = vmul_f32(v3030, v3598);
    float32x2_t v3044 = vmul_f32(v3043, v3598);
    float32x2_t v3080 = vrev64_f32(v2571);
    float32x2_t v3092 = vrev64_f32(v2685);
    float32x2_t v3104 = vrev64_f32(v2913);
    float32x2_t v3122 = vrev64_f32(v2799);
    float32x2_t v3220 = vmul_f32(v3219, v3218);
    float32x2_t v3232 = vmul_f32(v3231, v3496);
    float32x2_t v3244 = vmul_f32(v3243, v3508);
    float32x2_t v3262 = vmul_f32(v3261, v3369);
    float32x2_t v3358 = vrev64_f32(v2584);
    float32x2_t v3370 = vrev64_f32(v2698);
    float32x2_t v3382 = vrev64_f32(v2926);
    float32x2_t v3400 = vrev64_f32(v2812);
    float32x2_t v3008 = vsub_f32(v2998, v3007);
    float32x2_t v3012 = vmul_f32(v2998, v3619);
    float32x2_t v3081 = vmul_f32(v3080, v3079);
    float32x2_t v3093 = vmul_f32(v3092, v3218);
    float32x2_t v3105 = vmul_f32(v3104, v3496);
    float32x2_t v3123 = vmul_f32(v3122, v3357);
    float32x2_t v3221 = vfma_f32(v3220, v2579, v3212);
    float32x2_t v3233 = vfma_f32(v3232, v2693, v3490);
    float32x2_t v3245 = vfma_f32(v3244, v2921, v3502);
    float32x2_t v3263 = vfma_f32(v3262, v2807, v3363);
    float32x2_t v3359 = vmul_f32(v3358, v3357);
    float32x2_t v3371 = vmul_f32(v3370, v3369);
    float32x2_t v3383 = vmul_f32(v3382, v3538);
    float32x2_t v3401 = vmul_f32(v3400, v3399);
    float32x2_t v3497 = vrev64_f32(v2589);
    float32x2_t v3509 = vrev64_f32(v2703);
    float32x2_t v3521 = vrev64_f32(v2931);
    float32x2_t v3539 = vrev64_f32(v2817);
    float32x2_t v3013 = vsub_f32(v3012, v3008);
    float32x2_t v3045 = vsub_f32(v3008, v3044);
    float32x2_t v3054 = vmul_f32(v3008, v3619);
    float32x2_t v3082 = vfma_f32(v3081, v2571, v3073);
    float32x2_t v3094 = vfma_f32(v3093, v2685, v3212);
    float32x2_t v3106 = vfma_f32(v3105, v2913, v3490);
    float32x2_t v3124 = vfma_f32(v3123, v2799, v3351);
    float32x2_t v3246 = vsub_f32(v3221, v3245);
    float32x2_t v3250 = vmul_f32(v3221, v3619);
    float32x2_t v3264 = vsub_f32(v3233, v3263);
    float32x2_t v3268 = vmul_f32(v3233, v3619);
    float32x2_t v3360 = vfma_f32(v3359, v2584, v3351);
    float32x2_t v3372 = vfma_f32(v3371, v2698, v3363);
    float32x2_t v3384 = vfma_f32(v3383, v2926, v3532);
    float32x2_t v3402 = vfma_f32(v3401, v2812, v3514);
    float32x2_t v3498 = vmul_f32(v3497, v3496);
    float32x2_t v3510 = vmul_f32(v3509, v3508);
    float32x2_t v3522 = vmul_f32(v3521, v3520);
    float32x2_t v3540 = vmul_f32(v3539, v3538);
    float32x2_t v3032 = vsub_f32(v3013, v3031);
    v6[ostride * 10] = v3045;
    float32x2_t v3055 = vsub_f32(v3054, v3045);
    float32x2_t v3064 = vmul_f32(v3013, v3619);
    float32x2_t v3107 = vsub_f32(v3082, v3106);
    float32x2_t v3111 = vmul_f32(v3082, v3619);
    float32x2_t v3125 = vsub_f32(v3094, v3124);
    float32x2_t v3129 = vmul_f32(v3094, v3619);
    float32x2_t v3251 = vsub_f32(v3250, v3246);
    float32x2_t v3269 = vsub_f32(v3268, v3264);
    float32x2_t v3280 = vmul_f32(v3264, v3572);
    float32x2_t v3295 = vmul_f32(v3246, v3572);
    float32x2_t v3385 = vsub_f32(v3360, v3384);
    float32x2_t v3389 = vmul_f32(v3360, v3619);
    float32x2_t v3403 = vsub_f32(v3372, v3402);
    float32x2_t v3407 = vmul_f32(v3372, v3619);
    float32x2_t v3499 = vfma_f32(v3498, v2589, v3490);
    float32x2_t v3511 = vfma_f32(v3510, v2703, v3502);
    float32x2_t v3523 = vfma_f32(v3522, v2931, v3514);
    float32x2_t v3541 = vfma_f32(v3540, v2817, v3532);
    v6[ostride * 5] = v3032;
    v6[ostride * 15] = v3055;
    float32x2_t v3065 = vsub_f32(v3064, v3032);
    float32x2_t v3112 = vsub_f32(v3111, v3107);
    float32x2_t v3130 = vsub_f32(v3129, v3125);
    float32x2_t v3141 = vmul_f32(v3125, v3572);
    float32x2_t v3156 = vmul_f32(v3107, v3572);
    float32x2_t v3270 = vadd_f32(v3251, v3269);
    float32x2_t v3271 = vsub_f32(v3251, v3269);
    float32x2_t v3281 = vadd_f32(v3246, v3280);
    float32x2_t v3296 = vsub_f32(v3295, v3264);
    float32x2_t v3390 = vsub_f32(v3389, v3385);
    float32x2_t v3408 = vsub_f32(v3407, v3403);
    float32x2_t v3419 = vmul_f32(v3403, v3572);
    float32x2_t v3434 = vmul_f32(v3385, v3572);
    float32x2_t v3524 = vsub_f32(v3499, v3523);
    float32x2_t v3528 = vmul_f32(v3499, v3619);
    float32x2_t v3542 = vsub_f32(v3511, v3541);
    float32x2_t v3546 = vmul_f32(v3511, v3619);
    v6[ostride * 20] = v3065;
    float32x2_t v3131 = vadd_f32(v3112, v3130);
    float32x2_t v3132 = vsub_f32(v3112, v3130);
    float32x2_t v3142 = vadd_f32(v3107, v3141);
    float32x2_t v3157 = vsub_f32(v3156, v3125);
    float32x2_t v3275 = vmul_f32(v3270, v3552);
    float32x2_t v3285 = vmul_f32(v3271, v3562);
    float32x2_t v3297 = vadd_f32(v2465, v3270);
    float32x2_t v3308 = vrev64_f32(v3281);
    float32x2_t v3321 = vrev64_f32(v3296);
    float32x2_t v3409 = vadd_f32(v3390, v3408);
    float32x2_t v3410 = vsub_f32(v3390, v3408);
    float32x2_t v3420 = vadd_f32(v3385, v3419);
    float32x2_t v3435 = vsub_f32(v3434, v3403);
    float32x2_t v3529 = vsub_f32(v3528, v3524);
    float32x2_t v3547 = vsub_f32(v3546, v3542);
    float32x2_t v3558 = vmul_f32(v3542, v3572);
    float32x2_t v3573 = vmul_f32(v3524, v3572);
    float32x2_t v3136 = vmul_f32(v3131, v3552);
    float32x2_t v3146 = vmul_f32(v3132, v3562);
    float32x2_t v3158 = vadd_f32(v2457, v3131);
    float32x2_t v3169 = vrev64_f32(v3142);
    float32x2_t v3182 = vrev64_f32(v3157);
    float32x2_t v3276 = vsub_f32(v2465, v3275);
    v6[ostride * 2] = v3297;
    float32x2_t v3309 = vmul_f32(v3308, v3598);
    float32x2_t v3322 = vmul_f32(v3321, v3598);
    float32x2_t v3414 = vmul_f32(v3409, v3552);
    float32x2_t v3424 = vmul_f32(v3410, v3562);
    float32x2_t v3436 = vadd_f32(v2470, v3409);
    float32x2_t v3447 = vrev64_f32(v3420);
    float32x2_t v3460 = vrev64_f32(v3435);
    float32x2_t v3548 = vadd_f32(v3529, v3547);
    float32x2_t v3549 = vsub_f32(v3529, v3547);
    float32x2_t v3559 = vadd_f32(v3524, v3558);
    float32x2_t v3574 = vsub_f32(v3573, v3542);
    float32x2_t v3137 = vsub_f32(v2457, v3136);
    v6[ostride] = v3158;
    float32x2_t v3170 = vmul_f32(v3169, v3598);
    float32x2_t v3183 = vmul_f32(v3182, v3598);
    float32x2_t v3286 = vsub_f32(v3276, v3285);
    float32x2_t v3290 = vmul_f32(v3276, v3619);
    float32x2_t v3415 = vsub_f32(v2470, v3414);
    v6[ostride * 3] = v3436;
    float32x2_t v3448 = vmul_f32(v3447, v3598);
    float32x2_t v3461 = vmul_f32(v3460, v3598);
    float32x2_t v3553 = vmul_f32(v3548, v3552);
    float32x2_t v3563 = vmul_f32(v3549, v3562);
    float32x2_t v3575 = vadd_f32(v2475, v3548);
    float32x2_t v3586 = vrev64_f32(v3559);
    float32x2_t v3599 = vrev64_f32(v3574);
    float32x2_t v3147 = vsub_f32(v3137, v3146);
    float32x2_t v3151 = vmul_f32(v3137, v3619);
    float32x2_t v3291 = vsub_f32(v3290, v3286);
    float32x2_t v3323 = vsub_f32(v3286, v3322);
    float32x2_t v3332 = vmul_f32(v3286, v3619);
    float32x2_t v3425 = vsub_f32(v3415, v3424);
    float32x2_t v3429 = vmul_f32(v3415, v3619);
    float32x2_t v3554 = vsub_f32(v2475, v3553);
    v6[ostride * 4] = v3575;
    float32x2_t v3587 = vmul_f32(v3586, v3598);
    float32x2_t v3600 = vmul_f32(v3599, v3598);
    float32x2_t v3152 = vsub_f32(v3151, v3147);
    float32x2_t v3184 = vsub_f32(v3147, v3183);
    float32x2_t v3193 = vmul_f32(v3147, v3619);
    float32x2_t v3310 = vsub_f32(v3291, v3309);
    v6[ostride * 12] = v3323;
    float32x2_t v3333 = vsub_f32(v3332, v3323);
    float32x2_t v3342 = vmul_f32(v3291, v3619);
    float32x2_t v3430 = vsub_f32(v3429, v3425);
    float32x2_t v3462 = vsub_f32(v3425, v3461);
    float32x2_t v3471 = vmul_f32(v3425, v3619);
    float32x2_t v3564 = vsub_f32(v3554, v3563);
    float32x2_t v3568 = vmul_f32(v3554, v3619);
    float32x2_t v3171 = vsub_f32(v3152, v3170);
    v6[ostride * 11] = v3184;
    float32x2_t v3194 = vsub_f32(v3193, v3184);
    float32x2_t v3203 = vmul_f32(v3152, v3619);
    v6[ostride * 7] = v3310;
    v6[ostride * 17] = v3333;
    float32x2_t v3343 = vsub_f32(v3342, v3310);
    float32x2_t v3449 = vsub_f32(v3430, v3448);
    v6[ostride * 13] = v3462;
    float32x2_t v3472 = vsub_f32(v3471, v3462);
    float32x2_t v3481 = vmul_f32(v3430, v3619);
    float32x2_t v3569 = vsub_f32(v3568, v3564);
    float32x2_t v3601 = vsub_f32(v3564, v3600);
    float32x2_t v3610 = vmul_f32(v3564, v3619);
    v6[ostride * 6] = v3171;
    v6[ostride * 16] = v3194;
    float32x2_t v3204 = vsub_f32(v3203, v3171);
    v6[ostride * 22] = v3343;
    v6[ostride * 8] = v3449;
    v6[ostride * 18] = v3472;
    float32x2_t v3482 = vsub_f32(v3481, v3449);
    float32x2_t v3588 = vsub_f32(v3569, v3587);
    v6[ostride * 14] = v3601;
    float32x2_t v3611 = vsub_f32(v3610, v3601);
    float32x2_t v3620 = vmul_f32(v3569, v3619);
    v6[ostride * 21] = v3204;
    v6[ostride * 23] = v3482;
    v6[ostride * 9] = v3588;
    v6[ostride * 19] = v3611;
    float32x2_t v3621 = vsub_f32(v3620, v3588);
    v6[ostride * 24] = v3621;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu25(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v1087 = 9.6858316112863108e-01F;
    float v1092 = 2.4868988716485479e-01F;
    float v1249 = 8.7630668004386358e-01F;
    float v1254 = 4.8175367410171532e-01F;
    float v1411 = 7.2896862742141155e-01F;
    float v1416 = 6.8454710592868862e-01F;
    float v1424 = 6.2790519529313527e-02F;
    float v1429 = 9.9802672842827156e-01F;
    float v1462 = 7.7051324277578925e-01F;
    float v1573 = 5.3582679497899655e-01F;
    float v1578 = 8.4432792550201508e-01F;
    float v1586 = -4.2577929156507272e-01F;
    float v1591 = 9.0482705246601947e-01F;
    float v1599 = -6.3742398974868952e-01F;
    float v1604 = -7.7051324277578936e-01F;
    float v1619 = -9.9211470131447776e-01F;
    float v1624 = 1.2533323356430454e-01F;
    float v1641 = 2.5000000000000000e-01F;
    float v1653 = 5.5901699437494745e-01F;
    float v1665 = 6.1803398874989490e-01F;
    float v1694 = -9.5105651629515353e-01F;
    float v1722 = 2.0000000000000000e+00F;
    const float32x2_t *v1776 = &v5[v0];
    float32x2_t *v2141 = &v6[v2];
    int64_t v19 = v0 * 5;
    int64_t v30 = v0 * 10;
    int64_t v41 = v0 * 15;
    int64_t v52 = v0 * 20;
    int64_t v74 = v0 * 6;
    int64_t v85 = v0 * 11;
    int64_t v96 = v0 * 16;
    int64_t v107 = v0 * 21;
    int64_t v118 = v0 * 2;
    int64_t v129 = v0 * 7;
    int64_t v140 = v0 * 12;
    int64_t v151 = v0 * 17;
    int64_t v162 = v0 * 22;
    int64_t v173 = v0 * 3;
    int64_t v184 = v0 * 8;
    int64_t v195 = v0 * 13;
    int64_t v206 = v0 * 18;
    int64_t v217 = v0 * 23;
    int64_t v228 = v0 * 4;
    int64_t v239 = v0 * 9;
    int64_t v250 = v0 * 14;
    int64_t v261 = v0 * 19;
    int64_t v272 = v0 * 24;
    int64_t v1039 = v2 * 5;
    int64_t v1054 = v2 * 10;
    int64_t v1067 = v2 * 15;
    int64_t v1080 = v2 * 20;
    float v1095 = v4 * v1092;
    int64_t v1201 = v2 * 6;
    int64_t v1216 = v2 * 11;
    int64_t v1229 = v2 * 16;
    int64_t v1242 = v2 * 21;
    float v1257 = v4 * v1254;
    int64_t v1348 = v2 * 2;
    int64_t v1363 = v2 * 7;
    int64_t v1378 = v2 * 12;
    int64_t v1391 = v2 * 17;
    int64_t v1404 = v2 * 22;
    float v1419 = v4 * v1416;
    float v1432 = v4 * v1429;
    float v1465 = v4 * v1462;
    int64_t v1510 = v2 * 3;
    int64_t v1525 = v2 * 8;
    int64_t v1540 = v2 * 13;
    int64_t v1553 = v2 * 18;
    int64_t v1566 = v2 * 23;
    float v1581 = v4 * v1578;
    float v1594 = v4 * v1591;
    float v1607 = v4 * v1604;
    float v1627 = v4 * v1624;
    int64_t v1672 = v2 * 4;
    int64_t v1687 = v2 * 9;
    float v1697 = v4 * v1694;
    int64_t v1702 = v2 * 14;
    int64_t v1715 = v2 * 19;
    int64_t v1728 = v2 * 24;
    const float32x2_t *v1957 = &v5[0];
    svfloat32_t v2063 = svdup_n_f32(0);
    float32x2_t *v2077 = &v6[0];
    svfloat32_t v2120 = svdup_n_f32(v1087);
    svfloat32_t v2184 = svdup_n_f32(v1249);
    svfloat32_t v2248 = svdup_n_f32(v1411);
    svfloat32_t v2250 = svdup_n_f32(v1424);
    svfloat32_t v2312 = svdup_n_f32(v1573);
    svfloat32_t v2314 = svdup_n_f32(v1586);
    svfloat32_t v2316 = svdup_n_f32(v1599);
    svfloat32_t v2319 = svdup_n_f32(v1619);
    svfloat32_t v2322 = svdup_n_f32(v1641);
    svfloat32_t v2324 = svdup_n_f32(v1653);
    svfloat32_t v2326 = svdup_n_f32(v1665);
    svfloat32_t v2366 = svdup_n_f32(v1722);
    svfloat32_t v2409 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1776)[0]));
    svfloat32_t v27 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v38 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v49 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[14]));
    svfloat32_t v60 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[19]));
    svfloat32_t v71 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v82 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v93 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v104 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[15]));
    svfloat32_t v115 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[20]));
    svfloat32_t v126 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v137 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v148 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v159 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[16]));
    svfloat32_t v170 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[21]));
    svfloat32_t v181 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v192 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v203 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v214 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[17]));
    svfloat32_t v225 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[22]));
    svfloat32_t v236 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v247 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v258 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v269 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[18]));
    svfloat32_t v280 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[23]));
    const float32x2_t *v1740 = &v5[v19];
    const float32x2_t *v1749 = &v5[v30];
    const float32x2_t *v1758 = &v5[v41];
    const float32x2_t *v1767 = &v5[v52];
    const float32x2_t *v1785 = &v5[v74];
    const float32x2_t *v1794 = &v5[v85];
    const float32x2_t *v1803 = &v5[v96];
    const float32x2_t *v1812 = &v5[v107];
    const float32x2_t *v1821 = &v5[v118];
    const float32x2_t *v1830 = &v5[v129];
    const float32x2_t *v1839 = &v5[v140];
    const float32x2_t *v1848 = &v5[v151];
    const float32x2_t *v1857 = &v5[v162];
    const float32x2_t *v1866 = &v5[v173];
    const float32x2_t *v1875 = &v5[v184];
    const float32x2_t *v1884 = &v5[v195];
    const float32x2_t *v1893 = &v5[v206];
    const float32x2_t *v1902 = &v5[v217];
    const float32x2_t *v1911 = &v5[v228];
    const float32x2_t *v1920 = &v5[v239];
    const float32x2_t *v1929 = &v5[v250];
    const float32x2_t *v1938 = &v5[v261];
    const float32x2_t *v1947 = &v5[v272];
    float32x2_t *v2087 = &v6[v1039];
    float32x2_t *v2097 = &v6[v1054];
    float32x2_t *v2107 = &v6[v1067];
    float32x2_t *v2117 = &v6[v1080];
    svfloat32_t v2121 = svdup_n_f32(v1095);
    float32x2_t *v2151 = &v6[v1201];
    float32x2_t *v2161 = &v6[v1216];
    float32x2_t *v2171 = &v6[v1229];
    float32x2_t *v2181 = &v6[v1242];
    svfloat32_t v2185 = svdup_n_f32(v1257);
    float32x2_t *v2205 = &v6[v1348];
    float32x2_t *v2215 = &v6[v1363];
    float32x2_t *v2225 = &v6[v1378];
    float32x2_t *v2235 = &v6[v1391];
    float32x2_t *v2245 = &v6[v1404];
    svfloat32_t v2249 = svdup_n_f32(v1419);
    svfloat32_t v2251 = svdup_n_f32(v1432);
    svfloat32_t v2256 = svdup_n_f32(v1465);
    float32x2_t *v2269 = &v6[v1510];
    float32x2_t *v2279 = &v6[v1525];
    float32x2_t *v2289 = &v6[v1540];
    float32x2_t *v2299 = &v6[v1553];
    float32x2_t *v2309 = &v6[v1566];
    svfloat32_t v2313 = svdup_n_f32(v1581);
    svfloat32_t v2315 = svdup_n_f32(v1594);
    svfloat32_t v2317 = svdup_n_f32(v1607);
    svfloat32_t v2320 = svdup_n_f32(v1627);
    float32x2_t *v2333 = &v6[v1672];
    float32x2_t *v2343 = &v6[v1687];
    svfloat32_t v2346 = svdup_n_f32(v1697);
    float32x2_t *v2353 = &v6[v1702];
    float32x2_t *v2363 = &v6[v1715];
    float32x2_t *v2373 = &v6[v1728];
    svfloat32_t v2449 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1957)[0]));
    svfloat32_t zero72 = svdup_n_f32(0);
    svfloat32_t v72 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero72, v2409, v71, 0),
                     v2409, v71, 90);
    svfloat32_t v2401 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1740)[0]));
    svfloat32_t v2403 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1749)[0]));
    svfloat32_t v2405 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1758)[0]));
    svfloat32_t v2407 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1767)[0]));
    svfloat32_t v2411 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1785)[0]));
    svfloat32_t v2413 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1794)[0]));
    svfloat32_t v2415 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1803)[0]));
    svfloat32_t v2417 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1812)[0]));
    svfloat32_t v2419 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1821)[0]));
    svfloat32_t v2421 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1830)[0]));
    svfloat32_t v2423 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1839)[0]));
    svfloat32_t v2425 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1848)[0]));
    svfloat32_t v2427 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1857)[0]));
    svfloat32_t v2429 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1866)[0]));
    svfloat32_t v2431 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1875)[0]));
    svfloat32_t v2433 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1884)[0]));
    svfloat32_t v2435 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1893)[0]));
    svfloat32_t v2437 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1902)[0]));
    svfloat32_t v2439 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1911)[0]));
    svfloat32_t v2441 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1920)[0]));
    svfloat32_t v2443 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1929)[0]));
    svfloat32_t v2445 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1938)[0]));
    svfloat32_t v2447 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1947)[0]));
    svfloat32_t zero28 = svdup_n_f32(0);
    svfloat32_t v28 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero28, v2401, v27, 0),
                     v2401, v27, 90);
    svfloat32_t zero39 = svdup_n_f32(0);
    svfloat32_t v39 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero39, v2403, v38, 0),
                     v2403, v38, 90);
    svfloat32_t zero50 = svdup_n_f32(0);
    svfloat32_t v50 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero50, v2405, v49, 0),
                     v2405, v49, 90);
    svfloat32_t zero61 = svdup_n_f32(0);
    svfloat32_t v61 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero61, v2407, v60, 0),
                     v2407, v60, 90);
    svfloat32_t zero83 = svdup_n_f32(0);
    svfloat32_t v83 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero83, v2411, v82, 0),
                     v2411, v82, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v2413, v93, 0),
                     v2413, v93, 90);
    svfloat32_t zero105 = svdup_n_f32(0);
    svfloat32_t v105 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero105, v2415, v104, 0), v2415,
        v104, 90);
    svfloat32_t zero116 = svdup_n_f32(0);
    svfloat32_t v116 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero116, v2417, v115, 0), v2417,
        v115, 90);
    svfloat32_t zero127 = svdup_n_f32(0);
    svfloat32_t v127 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero127, v2419, v126, 0), v2419,
        v126, 90);
    svfloat32_t zero138 = svdup_n_f32(0);
    svfloat32_t v138 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero138, v2421, v137, 0), v2421,
        v137, 90);
    svfloat32_t zero149 = svdup_n_f32(0);
    svfloat32_t v149 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero149, v2423, v148, 0), v2423,
        v148, 90);
    svfloat32_t zero160 = svdup_n_f32(0);
    svfloat32_t v160 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero160, v2425, v159, 0), v2425,
        v159, 90);
    svfloat32_t zero171 = svdup_n_f32(0);
    svfloat32_t v171 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero171, v2427, v170, 0), v2427,
        v170, 90);
    svfloat32_t zero182 = svdup_n_f32(0);
    svfloat32_t v182 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero182, v2429, v181, 0), v2429,
        v181, 90);
    svfloat32_t zero193 = svdup_n_f32(0);
    svfloat32_t v193 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero193, v2431, v192, 0), v2431,
        v192, 90);
    svfloat32_t zero204 = svdup_n_f32(0);
    svfloat32_t v204 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero204, v2433, v203, 0), v2433,
        v203, 90);
    svfloat32_t zero215 = svdup_n_f32(0);
    svfloat32_t v215 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero215, v2435, v214, 0), v2435,
        v214, 90);
    svfloat32_t zero226 = svdup_n_f32(0);
    svfloat32_t v226 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero226, v2437, v225, 0), v2437,
        v225, 90);
    svfloat32_t zero237 = svdup_n_f32(0);
    svfloat32_t v237 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero237, v2439, v236, 0), v2439,
        v236, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero248, v2441, v247, 0), v2441,
        v247, 90);
    svfloat32_t zero259 = svdup_n_f32(0);
    svfloat32_t v259 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero259, v2443, v258, 0), v2443,
        v258, 90);
    svfloat32_t zero270 = svdup_n_f32(0);
    svfloat32_t v270 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero270, v2445, v269, 0), v2445,
        v269, 90);
    svfloat32_t zero281 = svdup_n_f32(0);
    svfloat32_t v281 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero281, v2447, v280, 0), v2447,
        v280, 90);
    svfloat32_t v301 = svcmla_f32_x(pred_full, v28, v2063, v28, 90);
    svfloat32_t v314 = svcmla_f32_x(pred_full, v39, v2063, v39, 90);
    svfloat32_t v327 = svcmla_f32_x(pred_full, v61, v2063, v61, 90);
    svfloat32_t v347 = svcmla_f32_x(pred_full, v50, v2063, v50, 90);
    svfloat32_t v428 = svcmla_f32_x(pred_full, v83, v2063, v83, 90);
    svfloat32_t v441 = svcmla_f32_x(pred_full, v94, v2063, v94, 90);
    svfloat32_t v454 = svcmla_f32_x(pred_full, v116, v2063, v116, 90);
    svfloat32_t v474 = svcmla_f32_x(pred_full, v105, v2063, v105, 90);
    svfloat32_t v555 = svcmla_f32_x(pred_full, v138, v2063, v138, 90);
    svfloat32_t v568 = svcmla_f32_x(pred_full, v149, v2063, v149, 90);
    svfloat32_t v581 = svcmla_f32_x(pred_full, v171, v2063, v171, 90);
    svfloat32_t v601 = svcmla_f32_x(pred_full, v160, v2063, v160, 90);
    svfloat32_t v682 = svcmla_f32_x(pred_full, v193, v2063, v193, 90);
    svfloat32_t v695 = svcmla_f32_x(pred_full, v204, v2063, v204, 90);
    svfloat32_t v708 = svcmla_f32_x(pred_full, v226, v2063, v226, 90);
    svfloat32_t v728 = svcmla_f32_x(pred_full, v215, v2063, v215, 90);
    svfloat32_t v809 = svcmla_f32_x(pred_full, v248, v2063, v248, 90);
    svfloat32_t v822 = svcmla_f32_x(pred_full, v259, v2063, v259, 90);
    svfloat32_t v835 = svcmla_f32_x(pred_full, v281, v2063, v281, 90);
    svfloat32_t v855 = svcmla_f32_x(pred_full, v270, v2063, v270, 90);
    svfloat32_t v328 = svsub_f32_x(svptrue_b32(), v301, v327);
    svfloat32_t v348 = svsub_f32_x(svptrue_b32(), v314, v347);
    svfloat32_t v455 = svsub_f32_x(svptrue_b32(), v428, v454);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v441, v474);
    svfloat32_t v582 = svsub_f32_x(svptrue_b32(), v555, v581);
    svfloat32_t v602 = svsub_f32_x(svptrue_b32(), v568, v601);
    svfloat32_t v709 = svsub_f32_x(svptrue_b32(), v682, v708);
    svfloat32_t v729 = svsub_f32_x(svptrue_b32(), v695, v728);
    svfloat32_t v836 = svsub_f32_x(svptrue_b32(), v809, v835);
    svfloat32_t v856 = svsub_f32_x(svptrue_b32(), v822, v855);
    svfloat32_t v334 = svnmls_f32_x(pred_full, v328, v301, v2366);
    svfloat32_t v354 = svnmls_f32_x(pred_full, v348, v314, v2366);
    svfloat32_t v461 = svnmls_f32_x(pred_full, v455, v428, v2366);
    svfloat32_t v481 = svnmls_f32_x(pred_full, v475, v441, v2366);
    svfloat32_t v588 = svnmls_f32_x(pred_full, v582, v555, v2366);
    svfloat32_t v608 = svnmls_f32_x(pred_full, v602, v568, v2366);
    svfloat32_t v715 = svnmls_f32_x(pred_full, v709, v682, v2366);
    svfloat32_t v735 = svnmls_f32_x(pred_full, v729, v695, v2366);
    svfloat32_t v842 = svnmls_f32_x(pred_full, v836, v809, v2366);
    svfloat32_t v862 = svnmls_f32_x(pred_full, v856, v822, v2366);
    svfloat32_t v355 = svadd_f32_x(svptrue_b32(), v334, v354);
    svfloat32_t v356 = svsub_f32_x(svptrue_b32(), v334, v354);
    svfloat32_t v368 = svmla_f32_x(pred_full, v328, v348, v2326);
    svfloat32_t v386 = svnmls_f32_x(pred_full, v348, v328, v2326);
    svfloat32_t v482 = svadd_f32_x(svptrue_b32(), v461, v481);
    svfloat32_t v483 = svsub_f32_x(svptrue_b32(), v461, v481);
    svfloat32_t v495 = svmla_f32_x(pred_full, v455, v475, v2326);
    svfloat32_t v513 = svnmls_f32_x(pred_full, v475, v455, v2326);
    svfloat32_t v609 = svadd_f32_x(svptrue_b32(), v588, v608);
    svfloat32_t v610 = svsub_f32_x(svptrue_b32(), v588, v608);
    svfloat32_t v622 = svmla_f32_x(pred_full, v582, v602, v2326);
    svfloat32_t v640 = svnmls_f32_x(pred_full, v602, v582, v2326);
    svfloat32_t v736 = svadd_f32_x(svptrue_b32(), v715, v735);
    svfloat32_t v737 = svsub_f32_x(svptrue_b32(), v715, v735);
    svfloat32_t v749 = svmla_f32_x(pred_full, v709, v729, v2326);
    svfloat32_t v767 = svnmls_f32_x(pred_full, v729, v709, v2326);
    svfloat32_t v863 = svadd_f32_x(svptrue_b32(), v842, v862);
    svfloat32_t v864 = svsub_f32_x(svptrue_b32(), v842, v862);
    svfloat32_t v876 = svmla_f32_x(pred_full, v836, v856, v2326);
    svfloat32_t v894 = svnmls_f32_x(pred_full, v856, v836, v2326);
    svfloat32_t v387 = svadd_f32_x(svptrue_b32(), v2449, v355);
    svfloat32_t zero394 = svdup_n_f32(0);
    svfloat32_t v394 = svcmla_f32_x(pred_full, zero394, v2346, v368, 90);
    svfloat32_t zero402 = svdup_n_f32(0);
    svfloat32_t v402 = svcmla_f32_x(pred_full, zero402, v2346, v386, 90);
    svfloat32_t v514 = svadd_f32_x(svptrue_b32(), v72, v482);
    svfloat32_t zero521 = svdup_n_f32(0);
    svfloat32_t v521 = svcmla_f32_x(pred_full, zero521, v2346, v495, 90);
    svfloat32_t zero529 = svdup_n_f32(0);
    svfloat32_t v529 = svcmla_f32_x(pred_full, zero529, v2346, v513, 90);
    svfloat32_t v641 = svadd_f32_x(svptrue_b32(), v127, v609);
    svfloat32_t zero648 = svdup_n_f32(0);
    svfloat32_t v648 = svcmla_f32_x(pred_full, zero648, v2346, v622, 90);
    svfloat32_t zero656 = svdup_n_f32(0);
    svfloat32_t v656 = svcmla_f32_x(pred_full, zero656, v2346, v640, 90);
    svfloat32_t v768 = svadd_f32_x(svptrue_b32(), v182, v736);
    svfloat32_t zero775 = svdup_n_f32(0);
    svfloat32_t v775 = svcmla_f32_x(pred_full, zero775, v2346, v749, 90);
    svfloat32_t zero783 = svdup_n_f32(0);
    svfloat32_t v783 = svcmla_f32_x(pred_full, zero783, v2346, v767, 90);
    svfloat32_t v895 = svadd_f32_x(svptrue_b32(), v237, v863);
    svfloat32_t zero902 = svdup_n_f32(0);
    svfloat32_t v902 = svcmla_f32_x(pred_full, zero902, v2346, v876, 90);
    svfloat32_t zero910 = svdup_n_f32(0);
    svfloat32_t v910 = svcmla_f32_x(pred_full, zero910, v2346, v894, 90);
    svfloat32_t v362 = svmls_f32_x(pred_full, v2449, v355, v2322);
    svfloat32_t v489 = svmls_f32_x(pred_full, v72, v482, v2322);
    svfloat32_t v616 = svmls_f32_x(pred_full, v127, v609, v2322);
    svfloat32_t v743 = svmls_f32_x(pred_full, v182, v736, v2322);
    svfloat32_t v870 = svmls_f32_x(pred_full, v237, v863, v2322);
    svfloat32_t v374 = svmls_f32_x(pred_full, v362, v356, v2324);
    svfloat32_t v501 = svmls_f32_x(pred_full, v489, v483, v2324);
    svfloat32_t v628 = svmls_f32_x(pred_full, v616, v610, v2324);
    svfloat32_t v755 = svmls_f32_x(pred_full, v743, v737, v2324);
    svfloat32_t v882 = svmls_f32_x(pred_full, v870, v864, v2324);
    svfloat32_t v936 = svcmla_f32_x(pred_full, v514, v2063, v514, 90);
    svfloat32_t v949 = svcmla_f32_x(pred_full, v641, v2063, v641, 90);
    svfloat32_t v962 = svcmla_f32_x(pred_full, v895, v2063, v895, 90);
    svfloat32_t v982 = svcmla_f32_x(pred_full, v768, v2063, v768, 90);
    svfloat32_t v380 = svnmls_f32_x(pred_full, v374, v362, v2366);
    svfloat32_t v403 = svsub_f32_x(svptrue_b32(), v374, v402);
    svfloat32_t v507 = svnmls_f32_x(pred_full, v501, v489, v2366);
    svfloat32_t v530 = svsub_f32_x(svptrue_b32(), v501, v529);
    svfloat32_t v634 = svnmls_f32_x(pred_full, v628, v616, v2366);
    svfloat32_t v657 = svsub_f32_x(svptrue_b32(), v628, v656);
    svfloat32_t v761 = svnmls_f32_x(pred_full, v755, v743, v2366);
    svfloat32_t v784 = svsub_f32_x(svptrue_b32(), v755, v783);
    svfloat32_t v888 = svnmls_f32_x(pred_full, v882, v870, v2366);
    svfloat32_t v911 = svsub_f32_x(svptrue_b32(), v882, v910);
    svfloat32_t v963 = svsub_f32_x(svptrue_b32(), v936, v962);
    svfloat32_t v983 = svsub_f32_x(svptrue_b32(), v949, v982);
    svfloat32_t v395 = svsub_f32_x(svptrue_b32(), v380, v394);
    svfloat32_t v409 = svnmls_f32_x(pred_full, v403, v374, v2366);
    svfloat32_t v522 = svsub_f32_x(svptrue_b32(), v507, v521);
    svfloat32_t v536 = svnmls_f32_x(pred_full, v530, v501, v2366);
    svfloat32_t v649 = svsub_f32_x(svptrue_b32(), v634, v648);
    svfloat32_t v663 = svnmls_f32_x(pred_full, v657, v628, v2366);
    svfloat32_t v776 = svsub_f32_x(svptrue_b32(), v761, v775);
    svfloat32_t v790 = svnmls_f32_x(pred_full, v784, v755, v2366);
    svfloat32_t v903 = svsub_f32_x(svptrue_b32(), v888, v902);
    svfloat32_t v917 = svnmls_f32_x(pred_full, v911, v882, v2366);
    svfloat32_t v969 = svnmls_f32_x(pred_full, v963, v936, v2366);
    svfloat32_t v989 = svnmls_f32_x(pred_full, v983, v949, v2366);
    svfloat32_t v1252 = svmul_f32_x(svptrue_b32(), v530, v2184);
    svfloat32_t v1265 = svmul_f32_x(svptrue_b32(), v657, v2312);
    svfloat32_t v1278 = svmul_f32_x(svptrue_b32(), v911, v2314);
    svfloat32_t v1298 = svmul_f32_x(svptrue_b32(), v784, v2250);
    svfloat32_t v415 = svnmls_f32_x(pred_full, v395, v380, v2366);
    svfloat32_t v542 = svnmls_f32_x(pred_full, v522, v507, v2366);
    svfloat32_t v669 = svnmls_f32_x(pred_full, v649, v634, v2366);
    svfloat32_t v796 = svnmls_f32_x(pred_full, v776, v761, v2366);
    svfloat32_t v923 = svnmls_f32_x(pred_full, v903, v888, v2366);
    svfloat32_t v990 = svadd_f32_x(svptrue_b32(), v969, v989);
    svfloat32_t v991 = svsub_f32_x(svptrue_b32(), v969, v989);
    svfloat32_t v1003 = svmla_f32_x(pred_full, v963, v983, v2326);
    svfloat32_t v1021 = svnmls_f32_x(pred_full, v983, v963, v2326);
    svfloat32_t v1090 = svmul_f32_x(svptrue_b32(), v522, v2120);
    svfloat32_t v1103 = svmul_f32_x(svptrue_b32(), v649, v2184);
    svfloat32_t v1116 = svmul_f32_x(svptrue_b32(), v903, v2312);
    svfloat32_t v1136 = svmul_f32_x(svptrue_b32(), v776, v2248);
    svfloat32_t v1260 = svcmla_f32_x(pred_full, v1252, v2185, v530, 90);
    svfloat32_t v1273 = svcmla_f32_x(pred_full, v1265, v2313, v657, 90);
    svfloat32_t v1286 = svcmla_f32_x(pred_full, v1278, v2315, v911, 90);
    svfloat32_t v1306 = svcmla_f32_x(pred_full, v1298, v2251, v784, 90);
    svfloat32_t v1414 = svmul_f32_x(svptrue_b32(), v536, v2248);
    svfloat32_t v1427 = svmul_f32_x(svptrue_b32(), v663, v2250);
    svfloat32_t v1440 = svmul_f32_x(svptrue_b32(), v917, v2319);
    svfloat32_t v1460 = svmul_f32_x(svptrue_b32(), v790, v2316);
    svfloat32_t v1022 = svadd_f32_x(svptrue_b32(), v387, v990);
    svfloat32_t zero1036 = svdup_n_f32(0);
    svfloat32_t v1036 = svcmla_f32_x(pred_full, zero1036, v2346, v1003, 90);
    svfloat32_t zero1051 = svdup_n_f32(0);
    svfloat32_t v1051 = svcmla_f32_x(pred_full, zero1051, v2346, v1021, 90);
    svfloat32_t v1098 = svcmla_f32_x(pred_full, v1090, v2121, v522, 90);
    svfloat32_t v1111 = svcmla_f32_x(pred_full, v1103, v2185, v649, 90);
    svfloat32_t v1124 = svcmla_f32_x(pred_full, v1116, v2313, v903, 90);
    svfloat32_t v1144 = svcmla_f32_x(pred_full, v1136, v2249, v776, 90);
    svfloat32_t v1287 = svsub_f32_x(svptrue_b32(), v1260, v1286);
    svfloat32_t v1307 = svsub_f32_x(svptrue_b32(), v1273, v1306);
    svfloat32_t v1422 = svcmla_f32_x(pred_full, v1414, v2249, v536, 90);
    svfloat32_t v1435 = svcmla_f32_x(pred_full, v1427, v2251, v663, 90);
    svfloat32_t v1448 = svcmla_f32_x(pred_full, v1440, v2320, v917, 90);
    svfloat32_t v1468 = svcmla_f32_x(pred_full, v1460, v2256, v790, 90);
    svfloat32_t v1576 = svmul_f32_x(svptrue_b32(), v542, v2312);
    svfloat32_t v1589 = svmul_f32_x(svptrue_b32(), v669, v2314);
    svfloat32_t v1602 = svmul_f32_x(svptrue_b32(), v923, v2316);
    svfloat32_t v1622 = svmul_f32_x(svptrue_b32(), v796, v2319);
    svfloat32_t v997 = svmls_f32_x(pred_full, v387, v990, v2322);
    svfloat32_t v1125 = svsub_f32_x(svptrue_b32(), v1098, v1124);
    svfloat32_t v1145 = svsub_f32_x(svptrue_b32(), v1111, v1144);
    svfloat32_t v1293 = svnmls_f32_x(pred_full, v1287, v1260, v2366);
    svfloat32_t v1313 = svnmls_f32_x(pred_full, v1307, v1273, v2366);
    svfloat32_t v1449 = svsub_f32_x(svptrue_b32(), v1422, v1448);
    svfloat32_t v1469 = svsub_f32_x(svptrue_b32(), v1435, v1468);
    svfloat32_t v1584 = svcmla_f32_x(pred_full, v1576, v2313, v542, 90);
    svfloat32_t v1597 = svcmla_f32_x(pred_full, v1589, v2315, v669, 90);
    svfloat32_t v1610 = svcmla_f32_x(pred_full, v1602, v2317, v923, 90);
    svfloat32_t v1630 = svcmla_f32_x(pred_full, v1622, v2320, v796, 90);
    svst1_f64(pred_full, (double *)(v2077), svreinterpret_f64_f32(v1022));
    svfloat32_t v1009 = svmls_f32_x(pred_full, v997, v991, v2324);
    svfloat32_t v1131 = svnmls_f32_x(pred_full, v1125, v1098, v2366);
    svfloat32_t v1151 = svnmls_f32_x(pred_full, v1145, v1111, v2366);
    svfloat32_t v1314 = svadd_f32_x(svptrue_b32(), v1293, v1313);
    svfloat32_t v1315 = svsub_f32_x(svptrue_b32(), v1293, v1313);
    svfloat32_t v1327 = svmla_f32_x(pred_full, v1287, v1307, v2326);
    svfloat32_t v1345 = svnmls_f32_x(pred_full, v1307, v1287, v2326);
    svfloat32_t v1455 = svnmls_f32_x(pred_full, v1449, v1422, v2366);
    svfloat32_t v1475 = svnmls_f32_x(pred_full, v1469, v1435, v2366);
    svfloat32_t v1611 = svsub_f32_x(svptrue_b32(), v1584, v1610);
    svfloat32_t v1631 = svsub_f32_x(svptrue_b32(), v1597, v1630);
    svfloat32_t v1015 = svnmls_f32_x(pred_full, v1009, v997, v2366);
    svfloat32_t v1052 = svsub_f32_x(svptrue_b32(), v1009, v1051);
    svfloat32_t v1152 = svadd_f32_x(svptrue_b32(), v1131, v1151);
    svfloat32_t v1153 = svsub_f32_x(svptrue_b32(), v1131, v1151);
    svfloat32_t v1165 = svmla_f32_x(pred_full, v1125, v1145, v2326);
    svfloat32_t v1183 = svnmls_f32_x(pred_full, v1145, v1125, v2326);
    svfloat32_t v1346 = svadd_f32_x(svptrue_b32(), v403, v1314);
    svfloat32_t zero1360 = svdup_n_f32(0);
    svfloat32_t v1360 = svcmla_f32_x(pred_full, zero1360, v2346, v1327, 90);
    svfloat32_t zero1375 = svdup_n_f32(0);
    svfloat32_t v1375 = svcmla_f32_x(pred_full, zero1375, v2346, v1345, 90);
    svfloat32_t v1476 = svadd_f32_x(svptrue_b32(), v1455, v1475);
    svfloat32_t v1477 = svsub_f32_x(svptrue_b32(), v1455, v1475);
    svfloat32_t v1489 = svmla_f32_x(pred_full, v1449, v1469, v2326);
    svfloat32_t v1507 = svnmls_f32_x(pred_full, v1469, v1449, v2326);
    svfloat32_t v1617 = svnmls_f32_x(pred_full, v1611, v1584, v2366);
    svfloat32_t v1637 = svnmls_f32_x(pred_full, v1631, v1597, v2366);
    svfloat32_t v1037 = svsub_f32_x(svptrue_b32(), v1015, v1036);
    svfloat32_t v1065 = svnmls_f32_x(pred_full, v1052, v1009, v2366);
    svfloat32_t v1184 = svadd_f32_x(svptrue_b32(), v395, v1152);
    svfloat32_t zero1198 = svdup_n_f32(0);
    svfloat32_t v1198 = svcmla_f32_x(pred_full, zero1198, v2346, v1165, 90);
    svfloat32_t zero1213 = svdup_n_f32(0);
    svfloat32_t v1213 = svcmla_f32_x(pred_full, zero1213, v2346, v1183, 90);
    svfloat32_t v1321 = svmls_f32_x(pred_full, v403, v1314, v2322);
    svfloat32_t v1508 = svadd_f32_x(svptrue_b32(), v409, v1476);
    svfloat32_t zero1522 = svdup_n_f32(0);
    svfloat32_t v1522 = svcmla_f32_x(pred_full, zero1522, v2346, v1489, 90);
    svfloat32_t zero1537 = svdup_n_f32(0);
    svfloat32_t v1537 = svcmla_f32_x(pred_full, zero1537, v2346, v1507, 90);
    svfloat32_t v1638 = svadd_f32_x(svptrue_b32(), v1617, v1637);
    svfloat32_t v1639 = svsub_f32_x(svptrue_b32(), v1617, v1637);
    svfloat32_t v1651 = svmla_f32_x(pred_full, v1611, v1631, v2326);
    svfloat32_t v1669 = svnmls_f32_x(pred_full, v1631, v1611, v2326);
    svst1_f64(pred_full, (double *)(v2097), svreinterpret_f64_f32(v1052));
    svst1_f64(pred_full, (double *)(v2205), svreinterpret_f64_f32(v1346));
    svfloat32_t v1078 = svnmls_f32_x(pred_full, v1037, v1015, v2366);
    svfloat32_t v1159 = svmls_f32_x(pred_full, v395, v1152, v2322);
    svfloat32_t v1333 = svmls_f32_x(pred_full, v1321, v1315, v2324);
    svfloat32_t v1483 = svmls_f32_x(pred_full, v409, v1476, v2322);
    svfloat32_t v1670 = svadd_f32_x(svptrue_b32(), v415, v1638);
    svfloat32_t zero1684 = svdup_n_f32(0);
    svfloat32_t v1684 = svcmla_f32_x(pred_full, zero1684, v2346, v1651, 90);
    svfloat32_t zero1699 = svdup_n_f32(0);
    svfloat32_t v1699 = svcmla_f32_x(pred_full, zero1699, v2346, v1669, 90);
    svst1_f64(pred_full, (double *)(v2087), svreinterpret_f64_f32(v1037));
    svst1_f64(pred_full, (double *)(v2107), svreinterpret_f64_f32(v1065));
    svst1_f64(pred_full, (double *)(v2141), svreinterpret_f64_f32(v1184));
    svst1_f64(pred_full, (double *)(v2269), svreinterpret_f64_f32(v1508));
    svfloat32_t v1171 = svmls_f32_x(pred_full, v1159, v1153, v2324);
    svfloat32_t v1339 = svnmls_f32_x(pred_full, v1333, v1321, v2366);
    svfloat32_t v1376 = svsub_f32_x(svptrue_b32(), v1333, v1375);
    svfloat32_t v1495 = svmls_f32_x(pred_full, v1483, v1477, v2324);
    svfloat32_t v1645 = svmls_f32_x(pred_full, v415, v1638, v2322);
    svst1_f64(pred_full, (double *)(v2117), svreinterpret_f64_f32(v1078));
    svst1_f64(pred_full, (double *)(v2333), svreinterpret_f64_f32(v1670));
    svfloat32_t v1177 = svnmls_f32_x(pred_full, v1171, v1159, v2366);
    svfloat32_t v1214 = svsub_f32_x(svptrue_b32(), v1171, v1213);
    svfloat32_t v1361 = svsub_f32_x(svptrue_b32(), v1339, v1360);
    svfloat32_t v1389 = svnmls_f32_x(pred_full, v1376, v1333, v2366);
    svfloat32_t v1501 = svnmls_f32_x(pred_full, v1495, v1483, v2366);
    svfloat32_t v1538 = svsub_f32_x(svptrue_b32(), v1495, v1537);
    svfloat32_t v1657 = svmls_f32_x(pred_full, v1645, v1639, v2324);
    svst1_f64(pred_full, (double *)(v2225), svreinterpret_f64_f32(v1376));
    svfloat32_t v1199 = svsub_f32_x(svptrue_b32(), v1177, v1198);
    svfloat32_t v1227 = svnmls_f32_x(pred_full, v1214, v1171, v2366);
    svfloat32_t v1402 = svnmls_f32_x(pred_full, v1361, v1339, v2366);
    svfloat32_t v1523 = svsub_f32_x(svptrue_b32(), v1501, v1522);
    svfloat32_t v1551 = svnmls_f32_x(pred_full, v1538, v1495, v2366);
    svfloat32_t v1663 = svnmls_f32_x(pred_full, v1657, v1645, v2366);
    svfloat32_t v1700 = svsub_f32_x(svptrue_b32(), v1657, v1699);
    svst1_f64(pred_full, (double *)(v2161), svreinterpret_f64_f32(v1214));
    svst1_f64(pred_full, (double *)(v2215), svreinterpret_f64_f32(v1361));
    svst1_f64(pred_full, (double *)(v2235), svreinterpret_f64_f32(v1389));
    svst1_f64(pred_full, (double *)(v2289), svreinterpret_f64_f32(v1538));
    svfloat32_t v1240 = svnmls_f32_x(pred_full, v1199, v1177, v2366);
    svfloat32_t v1564 = svnmls_f32_x(pred_full, v1523, v1501, v2366);
    svfloat32_t v1685 = svsub_f32_x(svptrue_b32(), v1663, v1684);
    svfloat32_t v1713 = svnmls_f32_x(pred_full, v1700, v1657, v2366);
    svst1_f64(pred_full, (double *)(v2151), svreinterpret_f64_f32(v1199));
    svst1_f64(pred_full, (double *)(v2171), svreinterpret_f64_f32(v1227));
    svst1_f64(pred_full, (double *)(v2245), svreinterpret_f64_f32(v1402));
    svst1_f64(pred_full, (double *)(v2279), svreinterpret_f64_f32(v1523));
    svst1_f64(pred_full, (double *)(v2299), svreinterpret_f64_f32(v1551));
    svst1_f64(pred_full, (double *)(v2353), svreinterpret_f64_f32(v1700));
    svfloat32_t v1726 = svnmls_f32_x(pred_full, v1685, v1663, v2366);
    svst1_f64(pred_full, (double *)(v2181), svreinterpret_f64_f32(v1240));
    svst1_f64(pred_full, (double *)(v2309), svreinterpret_f64_f32(v1564));
    svst1_f64(pred_full, (double *)(v2343), svreinterpret_f64_f32(v1685));
    svst1_f64(pred_full, (double *)(v2363), svreinterpret_f64_f32(v1713));
    svst1_f64(pred_full, (double *)(v2373), svreinterpret_f64_f32(v1726));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu32(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v12 = howmany - 1;
  int64_t v1801 = howmany / 2;
  for (int j = 0; j < v12; j += 2) {
    float v1522 = 7.0710678118654757e-01F;
    float v1535 = -7.0710678118654746e-01F;
    float v1592 = 5.5557023301960229e-01F;
    float v1609 = -1.9509032201612861e-01F;
    float v1667 = 9.2387953251128674e-01F;
    float v1675 = -9.2387953251128685e-01F;
    float v1679 = 3.8268343236508967e-01F;
    float v1680 = -3.8268343236508967e-01F;
    float v1732 = 1.9509032201612833e-01F;
    float v1736 = -9.8078528040323043e-01F;
    float v1737 = 9.8078528040323043e-01F;
    float v1745 = -5.5557023301960218e-01F;
    float v1749 = 8.3146961230254524e-01F;
    float v1750 = -8.3146961230254524e-01F;
    float v1761 = -1.0000000000000000e+00F;
    float v1762 = 1.0000000000000000e+00F;
    float32x2_t v1764 = (float32x2_t){v4, v4};
    const float32x2_t *v3451 = &v5[istride];
    float32x2_t *v3668 = &v6[ostride];
    float32x2_t v1313 = (float32x2_t){v1737, v1737};
    float32x2_t v1383 = (float32x2_t){v1667, v1667};
    float32x2_t v1388 = (float32x2_t){v1680, v1679};
    float32x2_t v1453 = (float32x2_t){v1749, v1749};
    float32x2_t v1458 = (float32x2_t){v1745, v1592};
    float32x2_t v1466 = (float32x2_t){v1609, v1609};
    float32x2_t v1523 = (float32x2_t){v1522, v1522};
    float32x2_t v1536 = (float32x2_t){v1535, v1535};
    float32x2_t v1541 = (float32x2_t){v1762, v1761};
    float32x2_t v1593 = (float32x2_t){v1592, v1592};
    float32x2_t v1598 = (float32x2_t){v1750, v1749};
    float32x2_t v1606 = (float32x2_t){v1736, v1736};
    float32x2_t v1611 = (float32x2_t){v1609, v1732};
    float32x2_t v1663 = (float32x2_t){v1679, v1679};
    float32x2_t v1668 = (float32x2_t){v1675, v1667};
    float32x2_t v1676 = (float32x2_t){v1675, v1675};
    float32x2_t v1681 = (float32x2_t){v1679, v1680};
    float32x2_t v1733 = (float32x2_t){v1732, v1732};
    float32x2_t v1738 = (float32x2_t){v1736, v1737};
    float32x2_t v1746 = (float32x2_t){v1745, v1745};
    float32x2_t v1751 = (float32x2_t){v1749, v1750};
    float32x2_t v1763 = (float32x2_t){v1761, v1762};
    const float32x2_t *v3622 = &v5[0];
    float32x2_t *v3632 = &v6[0];
    float32x4_t v3945 = vld1q_f32((const float32_t *)v3451);
    float32x4_t v47 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[30]));
    float32x4_t v49 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[31]));
    float32x4_t v66 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[14]));
    float32x4_t v68 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[15]));
    float32x4_t v85 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[46]));
    float32x4_t v87 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[47]));
    float32x4_t v135 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[6]));
    float32x4_t v137 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[7]));
    float32x4_t v147 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[38]));
    float32x4_t v149 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[39]));
    float32x4_t v197 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[22]));
    float32x4_t v199 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[23]));
    float32x4_t v209 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[54]));
    float32x4_t v211 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[55]));
    float32x4_t v259 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[2]));
    float32x4_t v261 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[3]));
    float32x4_t v271 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[34]));
    float32x4_t v273 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[35]));
    float32x4_t v290 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[18]));
    float32x4_t v292 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[19]));
    float32x4_t v309 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[50]));
    float32x4_t v311 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[51]));
    float32x4_t v359 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[10]));
    float32x4_t v361 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[11]));
    float32x4_t v371 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[42]));
    float32x4_t v373 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[43]));
    float32x4_t v390 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[26]));
    float32x4_t v392 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[27]));
    float32x4_t v409 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[58]));
    float32x4_t v411 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[59]));
    float32x4_t v454 = vtrn1q_f32(v3945, v3945);
    float32x4_t v455 = vtrn2q_f32(v3945, v3945);
    float32x4_t v459 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[0]));
    float32x4_t v461 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[1]));
    float32x4_t v471 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[32]));
    float32x4_t v473 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[33]));
    float32x4_t v490 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[16]));
    float32x4_t v492 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[17]));
    float32x4_t v509 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[48]));
    float32x4_t v511 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[49]));
    float32x4_t v559 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[8]));
    float32x4_t v561 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[9]));
    float32x4_t v571 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[40]));
    float32x4_t v573 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[41]));
    float32x4_t v621 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[24]));
    float32x4_t v623 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[25]));
    float32x4_t v633 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[56]));
    float32x4_t v635 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[57]));
    float32x4_t v683 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[4]));
    float32x4_t v685 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[5]));
    float32x4_t v695 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[36]));
    float32x4_t v697 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[37]));
    float32x4_t v714 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[20]));
    float32x4_t v716 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[21]));
    float32x4_t v733 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[52]));
    float32x4_t v735 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[53]));
    float32x4_t v783 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[12]));
    float32x4_t v785 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[13]));
    float32x4_t v795 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[44]));
    float32x4_t v797 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[45]));
    float32x4_t v845 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[28]));
    float32x4_t v847 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[29]));
    float32x4_t v857 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[60]));
    float32x4_t v859 =
        vreinterpretq_f32_u64(vld1q_dup_u64((const uint64_t *)&v7[61]));
    float32x4_t v1314 = vcombine_f32(v1313, v1313);
    float32x4_t v1384 = vcombine_f32(v1383, v1383);
    float32x2_t v1390 = vmul_f32(v1764, v1388);
    float32x4_t v1454 = vcombine_f32(v1453, v1453);
    float32x2_t v1460 = vmul_f32(v1764, v1458);
    float32x4_t v1467 = vcombine_f32(v1466, v1466);
    float32x4_t v1524 = vcombine_f32(v1523, v1523);
    float32x4_t v1537 = vcombine_f32(v1536, v1536);
    float32x2_t v1543 = vmul_f32(v1764, v1541);
    float32x4_t v1594 = vcombine_f32(v1593, v1593);
    float32x2_t v1600 = vmul_f32(v1764, v1598);
    float32x4_t v1607 = vcombine_f32(v1606, v1606);
    float32x2_t v1613 = vmul_f32(v1764, v1611);
    float32x4_t v1664 = vcombine_f32(v1663, v1663);
    float32x2_t v1670 = vmul_f32(v1764, v1668);
    float32x4_t v1677 = vcombine_f32(v1676, v1676);
    float32x2_t v1683 = vmul_f32(v1764, v1681);
    float32x4_t v1734 = vcombine_f32(v1733, v1733);
    float32x2_t v1740 = vmul_f32(v1764, v1738);
    float32x4_t v1747 = vcombine_f32(v1746, v1746);
    float32x2_t v1753 = vmul_f32(v1764, v1751);
    float32x2_t v1765 = vmul_f32(v1764, v1763);
    const float32x2_t *v3292 = &v5[istride * 16];
    const float32x2_t *v3303 = &v5[istride * 8];
    const float32x2_t *v3313 = &v5[istride * 24];
    const float32x2_t *v3323 = &v5[istride * 4];
    const float32x2_t *v3333 = &v5[istride * 20];
    const float32x2_t *v3345 = &v5[istride * 12];
    const float32x2_t *v3355 = &v5[istride * 28];
    const float32x2_t *v3367 = &v5[istride * 2];
    const float32x2_t *v3377 = &v5[istride * 18];
    const float32x2_t *v3389 = &v5[istride * 10];
    const float32x2_t *v3399 = &v5[istride * 26];
    const float32x2_t *v3409 = &v5[istride * 6];
    const float32x2_t *v3419 = &v5[istride * 22];
    const float32x2_t *v3431 = &v5[istride * 14];
    const float32x2_t *v3441 = &v5[istride * 30];
    const float32x2_t *v3460 = &v5[istride * 17];
    const float32x2_t *v3471 = &v5[istride * 9];
    const float32x2_t *v3481 = &v5[istride * 25];
    const float32x2_t *v3491 = &v5[istride * 5];
    const float32x2_t *v3501 = &v5[istride * 21];
    const float32x2_t *v3513 = &v5[istride * 13];
    const float32x2_t *v3523 = &v5[istride * 29];
    const float32x2_t *v3535 = &v5[istride * 3];
    const float32x2_t *v3545 = &v5[istride * 19];
    const float32x2_t *v3557 = &v5[istride * 11];
    const float32x2_t *v3567 = &v5[istride * 27];
    const float32x2_t *v3577 = &v5[istride * 7];
    const float32x2_t *v3587 = &v5[istride * 23];
    const float32x2_t *v3599 = &v5[istride * 15];
    const float32x2_t *v3609 = &v5[istride * 31];
    float32x2_t *v3641 = &v6[ostride * 8];
    float32x2_t *v3650 = &v6[ostride * 16];
    float32x2_t *v3659 = &v6[ostride * 24];
    float32x2_t *v3677 = &v6[ostride * 9];
    float32x2_t *v3686 = &v6[ostride * 17];
    float32x2_t *v3695 = &v6[ostride * 25];
    float32x2_t *v3704 = &v6[ostride * 2];
    float32x2_t *v3713 = &v6[ostride * 10];
    float32x2_t *v3722 = &v6[ostride * 18];
    float32x2_t *v3731 = &v6[ostride * 26];
    float32x2_t *v3740 = &v6[ostride * 3];
    float32x2_t *v3749 = &v6[ostride * 11];
    float32x2_t *v3758 = &v6[ostride * 19];
    float32x2_t *v3767 = &v6[ostride * 27];
    float32x2_t *v3776 = &v6[ostride * 4];
    float32x2_t *v3785 = &v6[ostride * 12];
    float32x2_t *v3794 = &v6[ostride * 20];
    float32x2_t *v3803 = &v6[ostride * 28];
    float32x2_t *v3812 = &v6[ostride * 5];
    float32x2_t *v3821 = &v6[ostride * 13];
    float32x2_t *v3830 = &v6[ostride * 21];
    float32x2_t *v3839 = &v6[ostride * 29];
    float32x2_t *v3848 = &v6[ostride * 6];
    float32x2_t *v3857 = &v6[ostride * 14];
    float32x2_t *v3866 = &v6[ostride * 22];
    float32x2_t *v3875 = &v6[ostride * 30];
    float32x2_t *v3884 = &v6[ostride * 7];
    float32x2_t *v3893 = &v6[ostride * 15];
    float32x2_t *v3902 = &v6[ostride * 23];
    float32x2_t *v3911 = &v6[ostride * 31];
    float32x4_t v3977 = vld1q_f32((const float32_t *)v3622);
    float32x4_t v460 = vmulq_f32(v454, v459);
    float32x4_t v1392 = vcombine_f32(v1390, v1390);
    float32x4_t v1462 = vcombine_f32(v1460, v1460);
    float32x4_t v1545 = vcombine_f32(v1543, v1543);
    float32x4_t v1602 = vcombine_f32(v1600, v1600);
    float32x4_t v1615 = vcombine_f32(v1613, v1613);
    float32x4_t v1672 = vcombine_f32(v1670, v1670);
    float32x4_t v1685 = vcombine_f32(v1683, v1683);
    float32x4_t v1742 = vcombine_f32(v1740, v1740);
    float32x4_t v1755 = vcombine_f32(v1753, v1753);
    float32x4_t v1767 = vcombine_f32(v1765, v1765);
    float32x4_t v3915 = vld1q_f32((const float32_t *)v3292);
    float32x4_t v3917 = vld1q_f32((const float32_t *)v3303);
    float32x4_t v3919 = vld1q_f32((const float32_t *)v3313);
    float32x4_t v3921 = vld1q_f32((const float32_t *)v3323);
    float32x4_t v3923 = vld1q_f32((const float32_t *)v3333);
    float32x4_t v3925 = vld1q_f32((const float32_t *)v3345);
    float32x4_t v3927 = vld1q_f32((const float32_t *)v3355);
    float32x4_t v3929 = vld1q_f32((const float32_t *)v3367);
    float32x4_t v3931 = vld1q_f32((const float32_t *)v3377);
    float32x4_t v3933 = vld1q_f32((const float32_t *)v3389);
    float32x4_t v3935 = vld1q_f32((const float32_t *)v3399);
    float32x4_t v3937 = vld1q_f32((const float32_t *)v3409);
    float32x4_t v3939 = vld1q_f32((const float32_t *)v3419);
    float32x4_t v3941 = vld1q_f32((const float32_t *)v3431);
    float32x4_t v3943 = vld1q_f32((const float32_t *)v3441);
    float32x4_t v3947 = vld1q_f32((const float32_t *)v3460);
    float32x4_t v3949 = vld1q_f32((const float32_t *)v3471);
    float32x4_t v3951 = vld1q_f32((const float32_t *)v3481);
    float32x4_t v3953 = vld1q_f32((const float32_t *)v3491);
    float32x4_t v3955 = vld1q_f32((const float32_t *)v3501);
    float32x4_t v3957 = vld1q_f32((const float32_t *)v3513);
    float32x4_t v3959 = vld1q_f32((const float32_t *)v3523);
    float32x4_t v3961 = vld1q_f32((const float32_t *)v3535);
    float32x4_t v3963 = vld1q_f32((const float32_t *)v3545);
    float32x4_t v3965 = vld1q_f32((const float32_t *)v3557);
    float32x4_t v3967 = vld1q_f32((const float32_t *)v3567);
    float32x4_t v3969 = vld1q_f32((const float32_t *)v3577);
    float32x4_t v3971 = vld1q_f32((const float32_t *)v3587);
    float32x4_t v3973 = vld1q_f32((const float32_t *)v3599);
    float32x4_t v3975 = vld1q_f32((const float32_t *)v3609);
    float32x4_t v42 = vtrn1q_f32(v3915, v3915);
    float32x4_t v43 = vtrn2q_f32(v3915, v3915);
    float32x4_t v61 = vtrn1q_f32(v3917, v3917);
    float32x4_t v62 = vtrn2q_f32(v3917, v3917);
    float32x4_t v80 = vtrn1q_f32(v3919, v3919);
    float32x4_t v81 = vtrn2q_f32(v3919, v3919);
    float32x4_t v130 = vtrn1q_f32(v3921, v3921);
    float32x4_t v131 = vtrn2q_f32(v3921, v3921);
    float32x4_t v142 = vtrn1q_f32(v3923, v3923);
    float32x4_t v143 = vtrn2q_f32(v3923, v3923);
    float32x4_t v192 = vtrn1q_f32(v3925, v3925);
    float32x4_t v193 = vtrn2q_f32(v3925, v3925);
    float32x4_t v204 = vtrn1q_f32(v3927, v3927);
    float32x4_t v205 = vtrn2q_f32(v3927, v3927);
    float32x4_t v254 = vtrn1q_f32(v3929, v3929);
    float32x4_t v255 = vtrn2q_f32(v3929, v3929);
    float32x4_t v266 = vtrn1q_f32(v3931, v3931);
    float32x4_t v267 = vtrn2q_f32(v3931, v3931);
    float32x4_t v285 = vtrn1q_f32(v3933, v3933);
    float32x4_t v286 = vtrn2q_f32(v3933, v3933);
    float32x4_t v304 = vtrn1q_f32(v3935, v3935);
    float32x4_t v305 = vtrn2q_f32(v3935, v3935);
    float32x4_t v354 = vtrn1q_f32(v3937, v3937);
    float32x4_t v355 = vtrn2q_f32(v3937, v3937);
    float32x4_t v366 = vtrn1q_f32(v3939, v3939);
    float32x4_t v367 = vtrn2q_f32(v3939, v3939);
    float32x4_t v385 = vtrn1q_f32(v3941, v3941);
    float32x4_t v386 = vtrn2q_f32(v3941, v3941);
    float32x4_t v404 = vtrn1q_f32(v3943, v3943);
    float32x4_t v405 = vtrn2q_f32(v3943, v3943);
    float32x4_t v463 = vfmaq_f32(v460, v455, v461);
    float32x4_t v466 = vtrn1q_f32(v3947, v3947);
    float32x4_t v467 = vtrn2q_f32(v3947, v3947);
    float32x4_t v485 = vtrn1q_f32(v3949, v3949);
    float32x4_t v486 = vtrn2q_f32(v3949, v3949);
    float32x4_t v504 = vtrn1q_f32(v3951, v3951);
    float32x4_t v505 = vtrn2q_f32(v3951, v3951);
    float32x4_t v554 = vtrn1q_f32(v3953, v3953);
    float32x4_t v555 = vtrn2q_f32(v3953, v3953);
    float32x4_t v566 = vtrn1q_f32(v3955, v3955);
    float32x4_t v567 = vtrn2q_f32(v3955, v3955);
    float32x4_t v616 = vtrn1q_f32(v3957, v3957);
    float32x4_t v617 = vtrn2q_f32(v3957, v3957);
    float32x4_t v628 = vtrn1q_f32(v3959, v3959);
    float32x4_t v629 = vtrn2q_f32(v3959, v3959);
    float32x4_t v678 = vtrn1q_f32(v3961, v3961);
    float32x4_t v679 = vtrn2q_f32(v3961, v3961);
    float32x4_t v690 = vtrn1q_f32(v3963, v3963);
    float32x4_t v691 = vtrn2q_f32(v3963, v3963);
    float32x4_t v709 = vtrn1q_f32(v3965, v3965);
    float32x4_t v710 = vtrn2q_f32(v3965, v3965);
    float32x4_t v728 = vtrn1q_f32(v3967, v3967);
    float32x4_t v729 = vtrn2q_f32(v3967, v3967);
    float32x4_t v778 = vtrn1q_f32(v3969, v3969);
    float32x4_t v779 = vtrn2q_f32(v3969, v3969);
    float32x4_t v790 = vtrn1q_f32(v3971, v3971);
    float32x4_t v791 = vtrn2q_f32(v3971, v3971);
    float32x4_t v840 = vtrn1q_f32(v3973, v3973);
    float32x4_t v841 = vtrn2q_f32(v3973, v3973);
    float32x4_t v852 = vtrn1q_f32(v3975, v3975);
    float32x4_t v853 = vtrn2q_f32(v3975, v3975);
    float32x4_t v48 = vmulq_f32(v42, v47);
    float32x4_t v67 = vmulq_f32(v61, v66);
    float32x4_t v86 = vmulq_f32(v80, v85);
    float32x4_t v136 = vmulq_f32(v130, v135);
    float32x4_t v148 = vmulq_f32(v142, v147);
    float32x4_t v198 = vmulq_f32(v192, v197);
    float32x4_t v210 = vmulq_f32(v204, v209);
    float32x4_t v260 = vmulq_f32(v254, v259);
    float32x4_t v272 = vmulq_f32(v266, v271);
    float32x4_t v291 = vmulq_f32(v285, v290);
    float32x4_t v310 = vmulq_f32(v304, v309);
    float32x4_t v360 = vmulq_f32(v354, v359);
    float32x4_t v372 = vmulq_f32(v366, v371);
    float32x4_t v391 = vmulq_f32(v385, v390);
    float32x4_t v410 = vmulq_f32(v404, v409);
    float32x4_t v472 = vmulq_f32(v466, v471);
    float32x4_t v491 = vmulq_f32(v485, v490);
    float32x4_t v510 = vmulq_f32(v504, v509);
    float32x4_t v560 = vmulq_f32(v554, v559);
    float32x4_t v572 = vmulq_f32(v566, v571);
    float32x4_t v622 = vmulq_f32(v616, v621);
    float32x4_t v634 = vmulq_f32(v628, v633);
    float32x4_t v684 = vmulq_f32(v678, v683);
    float32x4_t v696 = vmulq_f32(v690, v695);
    float32x4_t v715 = vmulq_f32(v709, v714);
    float32x4_t v734 = vmulq_f32(v728, v733);
    float32x4_t v784 = vmulq_f32(v778, v783);
    float32x4_t v796 = vmulq_f32(v790, v795);
    float32x4_t v846 = vmulq_f32(v840, v845);
    float32x4_t v858 = vmulq_f32(v852, v857);
    float32x4_t v51 = vfmaq_f32(v48, v43, v49);
    float32x4_t v70 = vfmaq_f32(v67, v62, v68);
    float32x4_t v89 = vfmaq_f32(v86, v81, v87);
    float32x4_t v139 = vfmaq_f32(v136, v131, v137);
    float32x4_t v151 = vfmaq_f32(v148, v143, v149);
    float32x4_t v201 = vfmaq_f32(v198, v193, v199);
    float32x4_t v213 = vfmaq_f32(v210, v205, v211);
    float32x4_t v263 = vfmaq_f32(v260, v255, v261);
    float32x4_t v275 = vfmaq_f32(v272, v267, v273);
    float32x4_t v294 = vfmaq_f32(v291, v286, v292);
    float32x4_t v313 = vfmaq_f32(v310, v305, v311);
    float32x4_t v363 = vfmaq_f32(v360, v355, v361);
    float32x4_t v375 = vfmaq_f32(v372, v367, v373);
    float32x4_t v394 = vfmaq_f32(v391, v386, v392);
    float32x4_t v413 = vfmaq_f32(v410, v405, v411);
    float32x4_t v475 = vfmaq_f32(v472, v467, v473);
    float32x4_t v494 = vfmaq_f32(v491, v486, v492);
    float32x4_t v513 = vfmaq_f32(v510, v505, v511);
    float32x4_t v563 = vfmaq_f32(v560, v555, v561);
    float32x4_t v575 = vfmaq_f32(v572, v567, v573);
    float32x4_t v625 = vfmaq_f32(v622, v617, v623);
    float32x4_t v637 = vfmaq_f32(v634, v629, v635);
    float32x4_t v687 = vfmaq_f32(v684, v679, v685);
    float32x4_t v699 = vfmaq_f32(v696, v691, v697);
    float32x4_t v718 = vfmaq_f32(v715, v710, v716);
    float32x4_t v737 = vfmaq_f32(v734, v729, v735);
    float32x4_t v787 = vfmaq_f32(v784, v779, v785);
    float32x4_t v799 = vfmaq_f32(v796, v791, v797);
    float32x4_t v849 = vfmaq_f32(v846, v841, v847);
    float32x4_t v861 = vfmaq_f32(v858, v853, v859);
    float32x4_t v869 = vaddq_f32(v3977, v51);
    float32x4_t v870 = vsubq_f32(v3977, v51);
    float32x4_t v871 = vaddq_f32(v70, v89);
    float32x4_t v872 = vsubq_f32(v70, v89);
    float32x4_t v885 = vaddq_f32(v139, v151);
    float32x4_t v886 = vsubq_f32(v139, v151);
    float32x4_t v887 = vaddq_f32(v201, v213);
    float32x4_t v888 = vsubq_f32(v201, v213);
    float32x4_t v945 = vaddq_f32(v263, v275);
    float32x4_t v946 = vsubq_f32(v263, v275);
    float32x4_t v947 = vaddq_f32(v294, v313);
    float32x4_t v948 = vsubq_f32(v294, v313);
    float32x4_t v961 = vaddq_f32(v363, v375);
    float32x4_t v962 = vsubq_f32(v363, v375);
    float32x4_t v963 = vaddq_f32(v394, v413);
    float32x4_t v964 = vsubq_f32(v394, v413);
    float32x4_t v1117 = vaddq_f32(v463, v475);
    float32x4_t v1118 = vsubq_f32(v463, v475);
    float32x4_t v1119 = vaddq_f32(v494, v513);
    float32x4_t v1120 = vsubq_f32(v494, v513);
    float32x4_t v1133 = vaddq_f32(v563, v575);
    float32x4_t v1134 = vsubq_f32(v563, v575);
    float32x4_t v1135 = vaddq_f32(v625, v637);
    float32x4_t v1136 = vsubq_f32(v625, v637);
    float32x4_t v1193 = vaddq_f32(v687, v699);
    float32x4_t v1194 = vsubq_f32(v687, v699);
    float32x4_t v1195 = vaddq_f32(v718, v737);
    float32x4_t v1196 = vsubq_f32(v718, v737);
    float32x4_t v1209 = vaddq_f32(v787, v799);
    float32x4_t v1210 = vsubq_f32(v787, v799);
    float32x4_t v1211 = vaddq_f32(v849, v861);
    float32x4_t v1212 = vsubq_f32(v849, v861);
    float32x4_t v878 = vrev64q_f32(v872);
    float32x4_t v881 = vaddq_f32(v869, v871);
    float32x4_t v882 = vsubq_f32(v869, v871);
    float32x4_t v889 = vaddq_f32(v885, v887);
    float32x4_t v890 = vsubq_f32(v885, v887);
    float32x4_t v907 = vmulq_f32(v886, v1524);
    float32x4_t v920 = vmulq_f32(v888, v1537);
    float32x4_t v954 = vrev64q_f32(v948);
    float32x4_t v957 = vaddq_f32(v945, v947);
    float32x4_t v958 = vsubq_f32(v945, v947);
    float32x4_t v970 = vrev64q_f32(v964);
    float32x4_t v973 = vaddq_f32(v961, v963);
    float32x4_t v974 = vsubq_f32(v961, v963);
    float32x4_t v1126 = vrev64q_f32(v1120);
    float32x4_t v1129 = vaddq_f32(v1117, v1119);
    float32x4_t v1130 = vsubq_f32(v1117, v1119);
    float32x4_t v1137 = vaddq_f32(v1133, v1135);
    float32x4_t v1138 = vsubq_f32(v1133, v1135);
    float32x4_t v1155 = vmulq_f32(v1134, v1524);
    float32x4_t v1168 = vmulq_f32(v1136, v1537);
    float32x4_t v1202 = vrev64q_f32(v1196);
    float32x4_t v1205 = vaddq_f32(v1193, v1195);
    float32x4_t v1206 = vsubq_f32(v1193, v1195);
    float32x4_t v1213 = vaddq_f32(v1209, v1211);
    float32x4_t v1214 = vsubq_f32(v1209, v1211);
    float32x4_t v1231 = vmulq_f32(v1210, v1524);
    float32x4_t v1244 = vmulq_f32(v1212, v1537);
    float32x4_t v880 = vmulq_f32(v878, v1545);
    float32x4_t v896 = vrev64q_f32(v890);
    float32x4_t v899 = vaddq_f32(v881, v889);
    float32x4_t v900 = vsubq_f32(v881, v889);
    float32x4_t v913 = vrev64q_f32(v907);
    float32x4_t v926 = vrev64q_f32(v920);
    float32x4_t v956 = vmulq_f32(v954, v1545);
    float32x4_t v972 = vmulq_f32(v970, v1545);
    float32x4_t v977 = vaddq_f32(v957, v973);
    float32x4_t v978 = vsubq_f32(v957, v973);
    float32x4_t v1037 = vmulq_f32(v958, v1524);
    float32x4_t v1050 = vmulq_f32(v974, v1537);
    float32x4_t v1128 = vmulq_f32(v1126, v1545);
    float32x4_t v1144 = vrev64q_f32(v1138);
    float32x4_t v1147 = vaddq_f32(v1129, v1137);
    float32x4_t v1148 = vsubq_f32(v1129, v1137);
    float32x4_t v1161 = vrev64q_f32(v1155);
    float32x4_t v1174 = vrev64q_f32(v1168);
    float32x4_t v1204 = vmulq_f32(v1202, v1545);
    float32x4_t v1220 = vrev64q_f32(v1214);
    float32x4_t v1223 = vaddq_f32(v1205, v1213);
    float32x4_t v1224 = vsubq_f32(v1205, v1213);
    float32x4_t v1237 = vrev64q_f32(v1231);
    float32x4_t v1250 = vrev64q_f32(v1244);
    float32x4_t v883 = vsubq_f32(v870, v880);
    float32x4_t v884 = vaddq_f32(v870, v880);
    float32x4_t v898 = vmulq_f32(v896, v1545);
    float32x4_t v915 = vmulq_f32(v913, v1767);
    float32x4_t v928 = vmulq_f32(v926, v1545);
    float32x4_t v959 = vsubq_f32(v946, v956);
    float32x4_t v960 = vaddq_f32(v946, v956);
    float32x4_t v975 = vsubq_f32(v962, v972);
    float32x4_t v976 = vaddq_f32(v962, v972);
    float32x4_t v984 = vrev64q_f32(v978);
    float32x4_t v987 = vaddq_f32(v899, v977);
    float32x4_t v988 = vsubq_f32(v899, v977);
    float32x4_t v1043 = vrev64q_f32(v1037);
    float32x4_t v1056 = vrev64q_f32(v1050);
    float32x4_t v1131 = vsubq_f32(v1118, v1128);
    float32x4_t v1132 = vaddq_f32(v1118, v1128);
    float32x4_t v1146 = vmulq_f32(v1144, v1545);
    float32x4_t v1163 = vmulq_f32(v1161, v1767);
    float32x4_t v1176 = vmulq_f32(v1174, v1545);
    float32x4_t v1207 = vsubq_f32(v1194, v1204);
    float32x4_t v1208 = vaddq_f32(v1194, v1204);
    float32x4_t v1222 = vmulq_f32(v1220, v1545);
    float32x4_t v1239 = vmulq_f32(v1237, v1767);
    float32x4_t v1252 = vmulq_f32(v1250, v1545);
    float32x4_t v1269 = vaddq_f32(v1147, v1223);
    float32x4_t v1270 = vsubq_f32(v1147, v1223);
    float32x4_t v1525 = vmulq_f32(v1148, v1524);
    float32x4_t v1538 = vmulq_f32(v1224, v1537);
    float32x4_t v901 = vsubq_f32(v882, v898);
    float32x4_t v902 = vaddq_f32(v882, v898);
    float32x4_t v929 = vaddq_f32(v907, v915);
    float32x4_t v930 = vaddq_f32(v920, v928);
    float32x4_t v986 = vmulq_f32(v984, v1545);
    float32x4_t v995 = vmulq_f32(v959, v1384);
    float32x4_t v1001 = vrev64q_f32(v959);
    float32x4_t v1008 = vmulq_f32(v975, v1664);
    float32x4_t v1014 = vrev64q_f32(v975);
    float32x4_t v1045 = vmulq_f32(v1043, v1767);
    float32x4_t v1058 = vmulq_f32(v1056, v1545);
    float32x4_t v1079 = vmulq_f32(v960, v1664);
    float32x4_t v1085 = vrev64q_f32(v960);
    float32x4_t v1092 = vmulq_f32(v976, v1677);
    float32x4_t v1098 = vrev64q_f32(v976);
    float32x4_t v1149 = vsubq_f32(v1130, v1146);
    float32x4_t v1150 = vaddq_f32(v1130, v1146);
    float32x4_t v1177 = vaddq_f32(v1155, v1163);
    float32x4_t v1178 = vaddq_f32(v1168, v1176);
    float32x4_t v1225 = vsubq_f32(v1206, v1222);
    float32x4_t v1226 = vaddq_f32(v1206, v1222);
    float32x4_t v1253 = vaddq_f32(v1231, v1239);
    float32x4_t v1254 = vaddq_f32(v1244, v1252);
    float32x4_t v1276 = vrev64q_f32(v1270);
    float32x4_t v1279 = vaddq_f32(v987, v1269);
    float32x4_t v1280 = vsubq_f32(v987, v1269);
    float32x4_t v1531 = vrev64q_f32(v1525);
    float32x4_t v1544 = vrev64q_f32(v1538);
    float32x4_t v931 = vaddq_f32(v929, v930);
    float32x4_t v932 = vsubq_f32(v930, v929);
    float32x4_t v989 = vsubq_f32(v900, v986);
    float32x4_t v990 = vaddq_f32(v900, v986);
    float32x4_t v1059 = vaddq_f32(v1037, v1045);
    float32x4_t v1060 = vaddq_f32(v1050, v1058);
    float32x4_t v1179 = vaddq_f32(v1177, v1178);
    float32x4_t v1180 = vsubq_f32(v1178, v1177);
    float32x4_t v1255 = vaddq_f32(v1253, v1254);
    float32x4_t v1256 = vsubq_f32(v1254, v1253);
    float32x4_t v1278 = vmulq_f32(v1276, v1545);
    float32x4_t v1385 = vmulq_f32(v1149, v1384);
    float32x4_t v1391 = vrev64q_f32(v1149);
    float32x4_t v1398 = vmulq_f32(v1225, v1664);
    float32x4_t v1404 = vrev64q_f32(v1225);
    float32x4_t v1533 = vmulq_f32(v1531, v1767);
    float32x4_t v1546 = vmulq_f32(v1544, v1545);
    float32x4_t v1665 = vmulq_f32(v1150, v1664);
    float32x4_t v1671 = vrev64q_f32(v1150);
    float32x4_t v1678 = vmulq_f32(v1226, v1677);
    float32x4_t v1684 = vrev64q_f32(v1226);
    vst1q_f32((float32_t *)v3632, v1279);
    vst1q_f32((float32_t *)v3650, v1280);
    float32x4_t v938 = vrev64q_f32(v932);
    float32x4_t v941 = vaddq_f32(v883, v931);
    float32x4_t v942 = vsubq_f32(v883, v931);
    float32x4_t v1017 = vfmaq_f32(v995, v1001, v1392);
    float32x4_t v1018 = vfmaq_f32(v1008, v1014, v1672);
    float32x4_t v1061 = vaddq_f32(v1059, v1060);
    float32x4_t v1062 = vsubq_f32(v1060, v1059);
    float32x4_t v1101 = vfmaq_f32(v1079, v1085, v1672);
    float32x4_t v1102 = vfmaq_f32(v1092, v1098, v1685);
    float32x4_t v1186 = vrev64q_f32(v1180);
    float32x4_t v1189 = vaddq_f32(v1131, v1179);
    float32x4_t v1190 = vsubq_f32(v1131, v1179);
    float32x4_t v1262 = vrev64q_f32(v1256);
    float32x4_t v1265 = vaddq_f32(v1207, v1255);
    float32x4_t v1266 = vsubq_f32(v1207, v1255);
    float32x4_t v1281 = vsubq_f32(v988, v1278);
    float32x4_t v1282 = vaddq_f32(v988, v1278);
    float32x4_t v1547 = vaddq_f32(v1525, v1533);
    float32x4_t v1548 = vaddq_f32(v1538, v1546);
    float32x4_t v940 = vmulq_f32(v938, v1767);
    float32x4_t v1019 = vaddq_f32(v1017, v1018);
    float32x4_t v1020 = vsubq_f32(v1018, v1017);
    float32x4_t v1068 = vrev64q_f32(v1062);
    float32x4_t v1071 = vaddq_f32(v901, v1061);
    float32x4_t v1072 = vsubq_f32(v901, v1061);
    float32x4_t v1103 = vaddq_f32(v1101, v1102);
    float32x4_t v1104 = vsubq_f32(v1102, v1101);
    float32x4_t v1188 = vmulq_f32(v1186, v1767);
    float32x4_t v1264 = vmulq_f32(v1262, v1767);
    float32x4_t v1315 = vmulq_f32(v1189, v1314);
    float32x4_t v1321 = vrev64q_f32(v1189);
    float32x4_t v1328 = vmulq_f32(v1265, v1454);
    float32x4_t v1334 = vrev64q_f32(v1265);
    float32x4_t v1407 = vfmaq_f32(v1385, v1391, v1392);
    float32x4_t v1408 = vfmaq_f32(v1398, v1404, v1672);
    float32x4_t v1549 = vaddq_f32(v1547, v1548);
    float32x4_t v1550 = vsubq_f32(v1548, v1547);
    float32x4_t v1595 = vmulq_f32(v1190, v1594);
    float32x4_t v1601 = vrev64q_f32(v1190);
    float32x4_t v1608 = vmulq_f32(v1266, v1607);
    float32x4_t v1614 = vrev64q_f32(v1266);
    float32x4_t v1687 = vfmaq_f32(v1665, v1671, v1672);
    float32x4_t v1688 = vfmaq_f32(v1678, v1684, v1685);
    vst1q_f32((float32_t *)v3641, v1281);
    vst1q_f32((float32_t *)v3659, v1282);
    float32x4_t v943 = vsubq_f32(v884, v940);
    float32x4_t v944 = vaddq_f32(v884, v940);
    float32x4_t v1026 = vrev64q_f32(v1020);
    float32x4_t v1029 = vaddq_f32(v941, v1019);
    float32x4_t v1030 = vsubq_f32(v941, v1019);
    float32x4_t v1070 = vmulq_f32(v1068, v1767);
    float32x4_t v1110 = vrev64q_f32(v1104);
    float32x4_t v1191 = vsubq_f32(v1132, v1188);
    float32x4_t v1192 = vaddq_f32(v1132, v1188);
    float32x4_t v1267 = vsubq_f32(v1208, v1264);
    float32x4_t v1268 = vaddq_f32(v1208, v1264);
    float32x4_t v1409 = vaddq_f32(v1407, v1408);
    float32x4_t v1410 = vsubq_f32(v1408, v1407);
    float32x4_t v1556 = vrev64q_f32(v1550);
    float32x4_t v1559 = vaddq_f32(v989, v1549);
    float32x4_t v1560 = vsubq_f32(v989, v1549);
    float32x4_t v1689 = vaddq_f32(v1687, v1688);
    float32x4_t v1690 = vsubq_f32(v1688, v1687);
    float32x4_t v1028 = vmulq_f32(v1026, v1767);
    float32x4_t v1073 = vsubq_f32(v902, v1070);
    float32x4_t v1074 = vaddq_f32(v902, v1070);
    float32x4_t v1112 = vmulq_f32(v1110, v1767);
    float32x4_t v1113 = vaddq_f32(v943, v1103);
    float32x4_t v1114 = vsubq_f32(v943, v1103);
    float32x4_t v1337 = vfmaq_f32(v1315, v1321, v1615);
    float32x4_t v1338 = vfmaq_f32(v1328, v1334, v1462);
    float32x4_t v1416 = vrev64q_f32(v1410);
    float32x4_t v1419 = vaddq_f32(v1071, v1409);
    float32x4_t v1420 = vsubq_f32(v1071, v1409);
    float32x4_t v1455 = vmulq_f32(v1191, v1454);
    float32x4_t v1461 = vrev64q_f32(v1191);
    float32x4_t v1468 = vmulq_f32(v1267, v1467);
    float32x4_t v1474 = vrev64q_f32(v1267);
    float32x4_t v1558 = vmulq_f32(v1556, v1767);
    float32x4_t v1617 = vfmaq_f32(v1595, v1601, v1602);
    float32x4_t v1618 = vfmaq_f32(v1608, v1614, v1615);
    float32x4_t v1696 = vrev64q_f32(v1690);
    float32x4_t v1735 = vmulq_f32(v1192, v1734);
    float32x4_t v1741 = vrev64q_f32(v1192);
    float32x4_t v1748 = vmulq_f32(v1268, v1747);
    float32x4_t v1754 = vrev64q_f32(v1268);
    vst1q_f32((float32_t *)v3776, v1559);
    vst1q_f32((float32_t *)v3794, v1560);
    float32x4_t v1031 = vsubq_f32(v942, v1028);
    float32x4_t v1032 = vaddq_f32(v942, v1028);
    float32x4_t v1115 = vsubq_f32(v944, v1112);
    float32x4_t v1116 = vaddq_f32(v944, v1112);
    float32x4_t v1339 = vaddq_f32(v1337, v1338);
    float32x4_t v1340 = vsubq_f32(v1338, v1337);
    float32x4_t v1418 = vmulq_f32(v1416, v1767);
    float32x4_t v1561 = vsubq_f32(v990, v1558);
    float32x4_t v1562 = vaddq_f32(v990, v1558);
    float32x4_t v1619 = vaddq_f32(v1617, v1618);
    float32x4_t v1620 = vsubq_f32(v1618, v1617);
    float32x4_t v1698 = vmulq_f32(v1696, v1767);
    float32x4_t v1699 = vaddq_f32(v1073, v1689);
    float32x4_t v1700 = vsubq_f32(v1073, v1689);
    vst1q_f32((float32_t *)v3704, v1419);
    vst1q_f32((float32_t *)v3722, v1420);
    float32x4_t v1346 = vrev64q_f32(v1340);
    float32x4_t v1349 = vaddq_f32(v1029, v1339);
    float32x4_t v1350 = vsubq_f32(v1029, v1339);
    float32x4_t v1421 = vsubq_f32(v1072, v1418);
    float32x4_t v1422 = vaddq_f32(v1072, v1418);
    float32x4_t v1477 = vfmaq_f32(v1455, v1461, v1462);
    float32x4_t v1478 = vfmaq_f32(v1468, v1474, v1742);
    float32x4_t v1626 = vrev64q_f32(v1620);
    float32x4_t v1629 = vaddq_f32(v1031, v1619);
    float32x4_t v1630 = vsubq_f32(v1031, v1619);
    float32x4_t v1701 = vsubq_f32(v1074, v1698);
    float32x4_t v1702 = vaddq_f32(v1074, v1698);
    float32x4_t v1757 = vfmaq_f32(v1735, v1741, v1742);
    float32x4_t v1758 = vfmaq_f32(v1748, v1754, v1755);
    vst1q_f32((float32_t *)v3785, v1561);
    vst1q_f32((float32_t *)v3803, v1562);
    vst1q_f32((float32_t *)v3848, v1699);
    vst1q_f32((float32_t *)v3866, v1700);
    float32x4_t v1348 = vmulq_f32(v1346, v1767);
    float32x4_t v1479 = vaddq_f32(v1477, v1478);
    float32x4_t v1480 = vsubq_f32(v1478, v1477);
    float32x4_t v1628 = vmulq_f32(v1626, v1767);
    float32x4_t v1759 = vaddq_f32(v1757, v1758);
    float32x4_t v1760 = vsubq_f32(v1758, v1757);
    vst1q_f32((float32_t *)v3668, v1349);
    vst1q_f32((float32_t *)v3686, v1350);
    vst1q_f32((float32_t *)v3713, v1421);
    vst1q_f32((float32_t *)v3731, v1422);
    vst1q_f32((float32_t *)v3812, v1629);
    vst1q_f32((float32_t *)v3830, v1630);
    vst1q_f32((float32_t *)v3857, v1701);
    vst1q_f32((float32_t *)v3875, v1702);
    float32x4_t v1351 = vsubq_f32(v1030, v1348);
    float32x4_t v1352 = vaddq_f32(v1030, v1348);
    float32x4_t v1486 = vrev64q_f32(v1480);
    float32x4_t v1489 = vaddq_f32(v1113, v1479);
    float32x4_t v1490 = vsubq_f32(v1113, v1479);
    float32x4_t v1631 = vsubq_f32(v1032, v1628);
    float32x4_t v1632 = vaddq_f32(v1032, v1628);
    float32x4_t v1766 = vrev64q_f32(v1760);
    float32x4_t v1769 = vaddq_f32(v1115, v1759);
    float32x4_t v1770 = vsubq_f32(v1115, v1759);
    float32x4_t v1488 = vmulq_f32(v1486, v1767);
    float32x4_t v1768 = vmulq_f32(v1766, v1767);
    vst1q_f32((float32_t *)v3677, v1351);
    vst1q_f32((float32_t *)v3695, v1352);
    vst1q_f32((float32_t *)v3740, v1489);
    vst1q_f32((float32_t *)v3758, v1490);
    vst1q_f32((float32_t *)v3821, v1631);
    vst1q_f32((float32_t *)v3839, v1632);
    vst1q_f32((float32_t *)v3884, v1769);
    vst1q_f32((float32_t *)v3902, v1770);
    float32x4_t v1491 = vsubq_f32(v1114, v1488);
    float32x4_t v1492 = vaddq_f32(v1114, v1488);
    float32x4_t v1771 = vsubq_f32(v1116, v1768);
    float32x4_t v1772 = vaddq_f32(v1116, v1768);
    vst1q_f32((float32_t *)v3749, v1491);
    vst1q_f32((float32_t *)v3767, v1492);
    vst1q_f32((float32_t *)v3893, v1771);
    vst1q_f32((float32_t *)v3911, v1772);
    v5 += 2 * 1;
    v6 += 2 * 1;
  }
  for (int j = v1801 * 2; j < howmany; j += 1) {
    float32x2_t v2128 = v5[istride];
    float v3055 = 7.0710678118654757e-01F;
    float v3066 = -7.0710678118654746e-01F;
    float v3112 = 5.5557023301960229e-01F;
    float v3126 = -1.9509032201612861e-01F;
    float v3173 = 9.2387953251128674e-01F;
    float v3180 = -9.2387953251128685e-01F;
    float v3183 = 3.8268343236508967e-01F;
    float v3184 = -3.8268343236508967e-01F;
    float v3226 = 1.9509032201612833e-01F;
    float v3229 = -9.8078528040323043e-01F;
    float v3230 = 9.8078528040323043e-01F;
    float v3237 = -5.5557023301960218e-01F;
    float v3240 = 8.3146961230254524e-01F;
    float v3241 = -8.3146961230254524e-01F;
    float v3251 = -1.0000000000000000e+00F;
    float v3252 = 1.0000000000000000e+00F;
    float32x2_t v3254 = (float32x2_t){v4, v4};
    float32x2_t v1825 = v7[30];
    float32x2_t v1830 = v7[31];
    float32x2_t v1840 = v7[14];
    float32x2_t v1845 = v7[15];
    float32x2_t v1855 = v7[46];
    float32x2_t v1860 = v7[47];
    float32x2_t v1895 = v7[6];
    float32x2_t v1900 = v7[7];
    float32x2_t v1905 = v7[38];
    float32x2_t v1910 = v7[39];
    float32x2_t v1945 = v7[22];
    float32x2_t v1950 = v7[23];
    float32x2_t v1955 = v7[54];
    float32x2_t v1960 = v7[55];
    float32x2_t v1995 = v7[2];
    float32x2_t v2000 = v7[3];
    float32x2_t v2005 = v7[34];
    float32x2_t v2010 = v7[35];
    float32x2_t v2020 = v7[18];
    float32x2_t v2025 = v7[19];
    float32x2_t v2035 = v7[50];
    float32x2_t v2040 = v7[51];
    float32x2_t v2075 = v7[10];
    float32x2_t v2080 = v7[11];
    float32x2_t v2085 = v7[42];
    float32x2_t v2090 = v7[43];
    float32x2_t v2100 = v7[26];
    float32x2_t v2105 = v7[27];
    float32x2_t v2115 = v7[58];
    float32x2_t v2120 = v7[59];
    float32x2_t v2155 = v7[0];
    float32x2_t v2156 = vtrn1_f32(v2128, v2128);
    float32x2_t v2157 = vtrn2_f32(v2128, v2128);
    float32x2_t v2160 = v7[1];
    float32x2_t v2165 = v7[32];
    float32x2_t v2170 = v7[33];
    float32x2_t v2180 = v7[16];
    float32x2_t v2185 = v7[17];
    float32x2_t v2195 = v7[48];
    float32x2_t v2200 = v7[49];
    float32x2_t v2235 = v7[8];
    float32x2_t v2240 = v7[9];
    float32x2_t v2245 = v7[40];
    float32x2_t v2250 = v7[41];
    float32x2_t v2285 = v7[24];
    float32x2_t v2290 = v7[25];
    float32x2_t v2295 = v7[56];
    float32x2_t v2300 = v7[57];
    float32x2_t v2335 = v7[4];
    float32x2_t v2340 = v7[5];
    float32x2_t v2345 = v7[36];
    float32x2_t v2350 = v7[37];
    float32x2_t v2360 = v7[20];
    float32x2_t v2365 = v7[21];
    float32x2_t v2375 = v7[52];
    float32x2_t v2380 = v7[53];
    float32x2_t v2415 = v7[12];
    float32x2_t v2420 = v7[13];
    float32x2_t v2425 = v7[44];
    float32x2_t v2430 = v7[45];
    float32x2_t v2465 = v7[28];
    float32x2_t v2470 = v7[29];
    float32x2_t v2475 = v7[60];
    float32x2_t v2480 = v7[61];
    float32x2_t v2488 = v5[0];
    float32x2_t v2885 = (float32x2_t){v3230, v3230};
    float32x2_t v2942 = (float32x2_t){v3173, v3173};
    float32x2_t v2946 = (float32x2_t){v3184, v3183};
    float32x2_t v2999 = (float32x2_t){v3240, v3240};
    float32x2_t v3003 = (float32x2_t){v3237, v3112};
    float32x2_t v3010 = (float32x2_t){v3126, v3126};
    float32x2_t v3056 = (float32x2_t){v3055, v3055};
    float32x2_t v3067 = (float32x2_t){v3066, v3066};
    float32x2_t v3071 = (float32x2_t){v3252, v3251};
    float32x2_t v3113 = (float32x2_t){v3112, v3112};
    float32x2_t v3117 = (float32x2_t){v3241, v3240};
    float32x2_t v3124 = (float32x2_t){v3229, v3229};
    float32x2_t v3128 = (float32x2_t){v3126, v3226};
    float32x2_t v3170 = (float32x2_t){v3183, v3183};
    float32x2_t v3174 = (float32x2_t){v3180, v3173};
    float32x2_t v3181 = (float32x2_t){v3180, v3180};
    float32x2_t v3185 = (float32x2_t){v3183, v3184};
    float32x2_t v3227 = (float32x2_t){v3226, v3226};
    float32x2_t v3231 = (float32x2_t){v3229, v3230};
    float32x2_t v3238 = (float32x2_t){v3237, v3237};
    float32x2_t v3242 = (float32x2_t){v3240, v3241};
    float32x2_t v3253 = (float32x2_t){v3251, v3252};
    float32x2_t v1813 = v5[istride * 16];
    float32x2_t v1838 = v5[istride * 8];
    float32x2_t v1853 = v5[istride * 24];
    float32x2_t v1868 = v5[istride * 4];
    float32x2_t v1883 = v5[istride * 20];
    float32x2_t v1918 = v5[istride * 12];
    float32x2_t v1933 = v5[istride * 28];
    float32x2_t v1968 = v5[istride * 2];
    float32x2_t v1983 = v5[istride * 18];
    float32x2_t v2018 = v5[istride * 10];
    float32x2_t v2033 = v5[istride * 26];
    float32x2_t v2048 = v5[istride * 6];
    float32x2_t v2063 = v5[istride * 22];
    float32x2_t v2098 = v5[istride * 14];
    float32x2_t v2113 = v5[istride * 30];
    float32x2_t v2143 = v5[istride * 17];
    float32x2_t v2161 = vmul_f32(v2156, v2155);
    float32x2_t v2178 = v5[istride * 9];
    float32x2_t v2193 = v5[istride * 25];
    float32x2_t v2208 = v5[istride * 5];
    float32x2_t v2223 = v5[istride * 21];
    float32x2_t v2258 = v5[istride * 13];
    float32x2_t v2273 = v5[istride * 29];
    float32x2_t v2308 = v5[istride * 3];
    float32x2_t v2323 = v5[istride * 19];
    float32x2_t v2358 = v5[istride * 11];
    float32x2_t v2373 = v5[istride * 27];
    float32x2_t v2388 = v5[istride * 7];
    float32x2_t v2403 = v5[istride * 23];
    float32x2_t v2438 = v5[istride * 15];
    float32x2_t v2453 = v5[istride * 31];
    float32x2_t v2948 = vmul_f32(v3254, v2946);
    float32x2_t v3005 = vmul_f32(v3254, v3003);
    float32x2_t v3073 = vmul_f32(v3254, v3071);
    float32x2_t v3119 = vmul_f32(v3254, v3117);
    float32x2_t v3130 = vmul_f32(v3254, v3128);
    float32x2_t v3176 = vmul_f32(v3254, v3174);
    float32x2_t v3187 = vmul_f32(v3254, v3185);
    float32x2_t v3233 = vmul_f32(v3254, v3231);
    float32x2_t v3244 = vmul_f32(v3254, v3242);
    float32x2_t v3255 = vmul_f32(v3254, v3253);
    float32x2_t v1826 = vtrn1_f32(v1813, v1813);
    float32x2_t v1827 = vtrn2_f32(v1813, v1813);
    float32x2_t v1841 = vtrn1_f32(v1838, v1838);
    float32x2_t v1842 = vtrn2_f32(v1838, v1838);
    float32x2_t v1856 = vtrn1_f32(v1853, v1853);
    float32x2_t v1857 = vtrn2_f32(v1853, v1853);
    float32x2_t v1896 = vtrn1_f32(v1868, v1868);
    float32x2_t v1897 = vtrn2_f32(v1868, v1868);
    float32x2_t v1906 = vtrn1_f32(v1883, v1883);
    float32x2_t v1907 = vtrn2_f32(v1883, v1883);
    float32x2_t v1946 = vtrn1_f32(v1918, v1918);
    float32x2_t v1947 = vtrn2_f32(v1918, v1918);
    float32x2_t v1956 = vtrn1_f32(v1933, v1933);
    float32x2_t v1957 = vtrn2_f32(v1933, v1933);
    float32x2_t v1996 = vtrn1_f32(v1968, v1968);
    float32x2_t v1997 = vtrn2_f32(v1968, v1968);
    float32x2_t v2006 = vtrn1_f32(v1983, v1983);
    float32x2_t v2007 = vtrn2_f32(v1983, v1983);
    float32x2_t v2021 = vtrn1_f32(v2018, v2018);
    float32x2_t v2022 = vtrn2_f32(v2018, v2018);
    float32x2_t v2036 = vtrn1_f32(v2033, v2033);
    float32x2_t v2037 = vtrn2_f32(v2033, v2033);
    float32x2_t v2076 = vtrn1_f32(v2048, v2048);
    float32x2_t v2077 = vtrn2_f32(v2048, v2048);
    float32x2_t v2086 = vtrn1_f32(v2063, v2063);
    float32x2_t v2087 = vtrn2_f32(v2063, v2063);
    float32x2_t v2101 = vtrn1_f32(v2098, v2098);
    float32x2_t v2102 = vtrn2_f32(v2098, v2098);
    float32x2_t v2116 = vtrn1_f32(v2113, v2113);
    float32x2_t v2117 = vtrn2_f32(v2113, v2113);
    float32x2_t v2163 = vfma_f32(v2161, v2157, v2160);
    float32x2_t v2166 = vtrn1_f32(v2143, v2143);
    float32x2_t v2167 = vtrn2_f32(v2143, v2143);
    float32x2_t v2181 = vtrn1_f32(v2178, v2178);
    float32x2_t v2182 = vtrn2_f32(v2178, v2178);
    float32x2_t v2196 = vtrn1_f32(v2193, v2193);
    float32x2_t v2197 = vtrn2_f32(v2193, v2193);
    float32x2_t v2236 = vtrn1_f32(v2208, v2208);
    float32x2_t v2237 = vtrn2_f32(v2208, v2208);
    float32x2_t v2246 = vtrn1_f32(v2223, v2223);
    float32x2_t v2247 = vtrn2_f32(v2223, v2223);
    float32x2_t v2286 = vtrn1_f32(v2258, v2258);
    float32x2_t v2287 = vtrn2_f32(v2258, v2258);
    float32x2_t v2296 = vtrn1_f32(v2273, v2273);
    float32x2_t v2297 = vtrn2_f32(v2273, v2273);
    float32x2_t v2336 = vtrn1_f32(v2308, v2308);
    float32x2_t v2337 = vtrn2_f32(v2308, v2308);
    float32x2_t v2346 = vtrn1_f32(v2323, v2323);
    float32x2_t v2347 = vtrn2_f32(v2323, v2323);
    float32x2_t v2361 = vtrn1_f32(v2358, v2358);
    float32x2_t v2362 = vtrn2_f32(v2358, v2358);
    float32x2_t v2376 = vtrn1_f32(v2373, v2373);
    float32x2_t v2377 = vtrn2_f32(v2373, v2373);
    float32x2_t v2416 = vtrn1_f32(v2388, v2388);
    float32x2_t v2417 = vtrn2_f32(v2388, v2388);
    float32x2_t v2426 = vtrn1_f32(v2403, v2403);
    float32x2_t v2427 = vtrn2_f32(v2403, v2403);
    float32x2_t v2466 = vtrn1_f32(v2438, v2438);
    float32x2_t v2467 = vtrn2_f32(v2438, v2438);
    float32x2_t v2476 = vtrn1_f32(v2453, v2453);
    float32x2_t v2477 = vtrn2_f32(v2453, v2453);
    float32x2_t v1831 = vmul_f32(v1826, v1825);
    float32x2_t v1846 = vmul_f32(v1841, v1840);
    float32x2_t v1861 = vmul_f32(v1856, v1855);
    float32x2_t v1901 = vmul_f32(v1896, v1895);
    float32x2_t v1911 = vmul_f32(v1906, v1905);
    float32x2_t v1951 = vmul_f32(v1946, v1945);
    float32x2_t v1961 = vmul_f32(v1956, v1955);
    float32x2_t v2001 = vmul_f32(v1996, v1995);
    float32x2_t v2011 = vmul_f32(v2006, v2005);
    float32x2_t v2026 = vmul_f32(v2021, v2020);
    float32x2_t v2041 = vmul_f32(v2036, v2035);
    float32x2_t v2081 = vmul_f32(v2076, v2075);
    float32x2_t v2091 = vmul_f32(v2086, v2085);
    float32x2_t v2106 = vmul_f32(v2101, v2100);
    float32x2_t v2121 = vmul_f32(v2116, v2115);
    float32x2_t v2171 = vmul_f32(v2166, v2165);
    float32x2_t v2186 = vmul_f32(v2181, v2180);
    float32x2_t v2201 = vmul_f32(v2196, v2195);
    float32x2_t v2241 = vmul_f32(v2236, v2235);
    float32x2_t v2251 = vmul_f32(v2246, v2245);
    float32x2_t v2291 = vmul_f32(v2286, v2285);
    float32x2_t v2301 = vmul_f32(v2296, v2295);
    float32x2_t v2341 = vmul_f32(v2336, v2335);
    float32x2_t v2351 = vmul_f32(v2346, v2345);
    float32x2_t v2366 = vmul_f32(v2361, v2360);
    float32x2_t v2381 = vmul_f32(v2376, v2375);
    float32x2_t v2421 = vmul_f32(v2416, v2415);
    float32x2_t v2431 = vmul_f32(v2426, v2425);
    float32x2_t v2471 = vmul_f32(v2466, v2465);
    float32x2_t v2481 = vmul_f32(v2476, v2475);
    float32x2_t v1833 = vfma_f32(v1831, v1827, v1830);
    float32x2_t v1848 = vfma_f32(v1846, v1842, v1845);
    float32x2_t v1863 = vfma_f32(v1861, v1857, v1860);
    float32x2_t v1903 = vfma_f32(v1901, v1897, v1900);
    float32x2_t v1913 = vfma_f32(v1911, v1907, v1910);
    float32x2_t v1953 = vfma_f32(v1951, v1947, v1950);
    float32x2_t v1963 = vfma_f32(v1961, v1957, v1960);
    float32x2_t v2003 = vfma_f32(v2001, v1997, v2000);
    float32x2_t v2013 = vfma_f32(v2011, v2007, v2010);
    float32x2_t v2028 = vfma_f32(v2026, v2022, v2025);
    float32x2_t v2043 = vfma_f32(v2041, v2037, v2040);
    float32x2_t v2083 = vfma_f32(v2081, v2077, v2080);
    float32x2_t v2093 = vfma_f32(v2091, v2087, v2090);
    float32x2_t v2108 = vfma_f32(v2106, v2102, v2105);
    float32x2_t v2123 = vfma_f32(v2121, v2117, v2120);
    float32x2_t v2173 = vfma_f32(v2171, v2167, v2170);
    float32x2_t v2188 = vfma_f32(v2186, v2182, v2185);
    float32x2_t v2203 = vfma_f32(v2201, v2197, v2200);
    float32x2_t v2243 = vfma_f32(v2241, v2237, v2240);
    float32x2_t v2253 = vfma_f32(v2251, v2247, v2250);
    float32x2_t v2293 = vfma_f32(v2291, v2287, v2290);
    float32x2_t v2303 = vfma_f32(v2301, v2297, v2300);
    float32x2_t v2343 = vfma_f32(v2341, v2337, v2340);
    float32x2_t v2353 = vfma_f32(v2351, v2347, v2350);
    float32x2_t v2368 = vfma_f32(v2366, v2362, v2365);
    float32x2_t v2383 = vfma_f32(v2381, v2377, v2380);
    float32x2_t v2423 = vfma_f32(v2421, v2417, v2420);
    float32x2_t v2433 = vfma_f32(v2431, v2427, v2430);
    float32x2_t v2473 = vfma_f32(v2471, v2467, v2470);
    float32x2_t v2483 = vfma_f32(v2481, v2477, v2480);
    float32x2_t v2489 = vadd_f32(v2488, v1833);
    float32x2_t v2490 = vsub_f32(v2488, v1833);
    float32x2_t v2491 = vadd_f32(v1848, v1863);
    float32x2_t v2492 = vsub_f32(v1848, v1863);
    float32x2_t v2504 = vadd_f32(v1903, v1913);
    float32x2_t v2505 = vsub_f32(v1903, v1913);
    float32x2_t v2506 = vadd_f32(v1953, v1963);
    float32x2_t v2507 = vsub_f32(v1953, v1963);
    float32x2_t v2558 = vadd_f32(v2003, v2013);
    float32x2_t v2559 = vsub_f32(v2003, v2013);
    float32x2_t v2560 = vadd_f32(v2028, v2043);
    float32x2_t v2561 = vsub_f32(v2028, v2043);
    float32x2_t v2573 = vadd_f32(v2083, v2093);
    float32x2_t v2574 = vsub_f32(v2083, v2093);
    float32x2_t v2575 = vadd_f32(v2108, v2123);
    float32x2_t v2576 = vsub_f32(v2108, v2123);
    float32x2_t v2712 = vadd_f32(v2163, v2173);
    float32x2_t v2713 = vsub_f32(v2163, v2173);
    float32x2_t v2714 = vadd_f32(v2188, v2203);
    float32x2_t v2715 = vsub_f32(v2188, v2203);
    float32x2_t v2727 = vadd_f32(v2243, v2253);
    float32x2_t v2728 = vsub_f32(v2243, v2253);
    float32x2_t v2729 = vadd_f32(v2293, v2303);
    float32x2_t v2730 = vsub_f32(v2293, v2303);
    float32x2_t v2781 = vadd_f32(v2343, v2353);
    float32x2_t v2782 = vsub_f32(v2343, v2353);
    float32x2_t v2783 = vadd_f32(v2368, v2383);
    float32x2_t v2784 = vsub_f32(v2368, v2383);
    float32x2_t v2796 = vadd_f32(v2423, v2433);
    float32x2_t v2797 = vsub_f32(v2423, v2433);
    float32x2_t v2798 = vadd_f32(v2473, v2483);
    float32x2_t v2799 = vsub_f32(v2473, v2483);
    float32x2_t v2498 = vrev64_f32(v2492);
    float32x2_t v2500 = vadd_f32(v2489, v2491);
    float32x2_t v2501 = vsub_f32(v2489, v2491);
    float32x2_t v2508 = vadd_f32(v2504, v2506);
    float32x2_t v2509 = vsub_f32(v2504, v2506);
    float32x2_t v2524 = vmul_f32(v2505, v3056);
    float32x2_t v2535 = vmul_f32(v2507, v3067);
    float32x2_t v2567 = vrev64_f32(v2561);
    float32x2_t v2569 = vadd_f32(v2558, v2560);
    float32x2_t v2570 = vsub_f32(v2558, v2560);
    float32x2_t v2582 = vrev64_f32(v2576);
    float32x2_t v2584 = vadd_f32(v2573, v2575);
    float32x2_t v2585 = vsub_f32(v2573, v2575);
    float32x2_t v2721 = vrev64_f32(v2715);
    float32x2_t v2723 = vadd_f32(v2712, v2714);
    float32x2_t v2724 = vsub_f32(v2712, v2714);
    float32x2_t v2731 = vadd_f32(v2727, v2729);
    float32x2_t v2732 = vsub_f32(v2727, v2729);
    float32x2_t v2747 = vmul_f32(v2728, v3056);
    float32x2_t v2758 = vmul_f32(v2730, v3067);
    float32x2_t v2790 = vrev64_f32(v2784);
    float32x2_t v2792 = vadd_f32(v2781, v2783);
    float32x2_t v2793 = vsub_f32(v2781, v2783);
    float32x2_t v2800 = vadd_f32(v2796, v2798);
    float32x2_t v2801 = vsub_f32(v2796, v2798);
    float32x2_t v2816 = vmul_f32(v2797, v3056);
    float32x2_t v2827 = vmul_f32(v2799, v3067);
    float32x2_t v2499 = vmul_f32(v2498, v3073);
    float32x2_t v2515 = vrev64_f32(v2509);
    float32x2_t v2517 = vadd_f32(v2500, v2508);
    float32x2_t v2518 = vsub_f32(v2500, v2508);
    float32x2_t v2530 = vrev64_f32(v2524);
    float32x2_t v2541 = vrev64_f32(v2535);
    float32x2_t v2568 = vmul_f32(v2567, v3073);
    float32x2_t v2583 = vmul_f32(v2582, v3073);
    float32x2_t v2588 = vadd_f32(v2569, v2584);
    float32x2_t v2589 = vsub_f32(v2569, v2584);
    float32x2_t v2641 = vmul_f32(v2570, v3056);
    float32x2_t v2652 = vmul_f32(v2585, v3067);
    float32x2_t v2722 = vmul_f32(v2721, v3073);
    float32x2_t v2738 = vrev64_f32(v2732);
    float32x2_t v2740 = vadd_f32(v2723, v2731);
    float32x2_t v2741 = vsub_f32(v2723, v2731);
    float32x2_t v2753 = vrev64_f32(v2747);
    float32x2_t v2764 = vrev64_f32(v2758);
    float32x2_t v2791 = vmul_f32(v2790, v3073);
    float32x2_t v2807 = vrev64_f32(v2801);
    float32x2_t v2809 = vadd_f32(v2792, v2800);
    float32x2_t v2810 = vsub_f32(v2792, v2800);
    float32x2_t v2822 = vrev64_f32(v2816);
    float32x2_t v2833 = vrev64_f32(v2827);
    float32x2_t v2502 = vsub_f32(v2490, v2499);
    float32x2_t v2503 = vadd_f32(v2490, v2499);
    float32x2_t v2516 = vmul_f32(v2515, v3073);
    float32x2_t v2531 = vmul_f32(v2530, v3255);
    float32x2_t v2542 = vmul_f32(v2541, v3073);
    float32x2_t v2571 = vsub_f32(v2559, v2568);
    float32x2_t v2572 = vadd_f32(v2559, v2568);
    float32x2_t v2586 = vsub_f32(v2574, v2583);
    float32x2_t v2587 = vadd_f32(v2574, v2583);
    float32x2_t v2595 = vrev64_f32(v2589);
    float32x2_t v2597 = vadd_f32(v2517, v2588);
    float32x2_t v2598 = vsub_f32(v2517, v2588);
    float32x2_t v2647 = vrev64_f32(v2641);
    float32x2_t v2658 = vrev64_f32(v2652);
    float32x2_t v2725 = vsub_f32(v2713, v2722);
    float32x2_t v2726 = vadd_f32(v2713, v2722);
    float32x2_t v2739 = vmul_f32(v2738, v3073);
    float32x2_t v2754 = vmul_f32(v2753, v3255);
    float32x2_t v2765 = vmul_f32(v2764, v3073);
    float32x2_t v2794 = vsub_f32(v2782, v2791);
    float32x2_t v2795 = vadd_f32(v2782, v2791);
    float32x2_t v2808 = vmul_f32(v2807, v3073);
    float32x2_t v2823 = vmul_f32(v2822, v3255);
    float32x2_t v2834 = vmul_f32(v2833, v3073);
    float32x2_t v2850 = vadd_f32(v2740, v2809);
    float32x2_t v2851 = vsub_f32(v2740, v2809);
    float32x2_t v3057 = vmul_f32(v2741, v3056);
    float32x2_t v3068 = vmul_f32(v2810, v3067);
    float32x2_t v2519 = vsub_f32(v2501, v2516);
    float32x2_t v2520 = vadd_f32(v2501, v2516);
    float32x2_t v2543 = vadd_f32(v2524, v2531);
    float32x2_t v2544 = vadd_f32(v2535, v2542);
    float32x2_t v2596 = vmul_f32(v2595, v3073);
    float32x2_t v2604 = vmul_f32(v2571, v2942);
    float32x2_t v2610 = vrev64_f32(v2571);
    float32x2_t v2615 = vmul_f32(v2586, v3170);
    float32x2_t v2621 = vrev64_f32(v2586);
    float32x2_t v2648 = vmul_f32(v2647, v3255);
    float32x2_t v2659 = vmul_f32(v2658, v3073);
    float32x2_t v2678 = vmul_f32(v2572, v3170);
    float32x2_t v2684 = vrev64_f32(v2572);
    float32x2_t v2689 = vmul_f32(v2587, v3181);
    float32x2_t v2695 = vrev64_f32(v2587);
    float32x2_t v2742 = vsub_f32(v2724, v2739);
    float32x2_t v2743 = vadd_f32(v2724, v2739);
    float32x2_t v2766 = vadd_f32(v2747, v2754);
    float32x2_t v2767 = vadd_f32(v2758, v2765);
    float32x2_t v2811 = vsub_f32(v2793, v2808);
    float32x2_t v2812 = vadd_f32(v2793, v2808);
    float32x2_t v2835 = vadd_f32(v2816, v2823);
    float32x2_t v2836 = vadd_f32(v2827, v2834);
    float32x2_t v2857 = vrev64_f32(v2851);
    float32x2_t v2859 = vadd_f32(v2597, v2850);
    float32x2_t v2860 = vsub_f32(v2597, v2850);
    float32x2_t v3063 = vrev64_f32(v3057);
    float32x2_t v3074 = vrev64_f32(v3068);
    float32x2_t v2545 = vadd_f32(v2543, v2544);
    float32x2_t v2546 = vsub_f32(v2544, v2543);
    float32x2_t v2599 = vsub_f32(v2518, v2596);
    float32x2_t v2600 = vadd_f32(v2518, v2596);
    float32x2_t v2660 = vadd_f32(v2641, v2648);
    float32x2_t v2661 = vadd_f32(v2652, v2659);
    float32x2_t v2768 = vadd_f32(v2766, v2767);
    float32x2_t v2769 = vsub_f32(v2767, v2766);
    float32x2_t v2837 = vadd_f32(v2835, v2836);
    float32x2_t v2838 = vsub_f32(v2836, v2835);
    float32x2_t v2858 = vmul_f32(v2857, v3073);
    v6[0] = v2859;
    v6[ostride * 16] = v2860;
    float32x2_t v2943 = vmul_f32(v2742, v2942);
    float32x2_t v2949 = vrev64_f32(v2742);
    float32x2_t v2954 = vmul_f32(v2811, v3170);
    float32x2_t v2960 = vrev64_f32(v2811);
    float32x2_t v3064 = vmul_f32(v3063, v3255);
    float32x2_t v3075 = vmul_f32(v3074, v3073);
    float32x2_t v3171 = vmul_f32(v2743, v3170);
    float32x2_t v3177 = vrev64_f32(v2743);
    float32x2_t v3182 = vmul_f32(v2812, v3181);
    float32x2_t v3188 = vrev64_f32(v2812);
    float32x2_t v2552 = vrev64_f32(v2546);
    float32x2_t v2554 = vadd_f32(v2502, v2545);
    float32x2_t v2555 = vsub_f32(v2502, v2545);
    float32x2_t v2623 = vfma_f32(v2604, v2610, v2948);
    float32x2_t v2624 = vfma_f32(v2615, v2621, v3176);
    float32x2_t v2662 = vadd_f32(v2660, v2661);
    float32x2_t v2663 = vsub_f32(v2661, v2660);
    float32x2_t v2697 = vfma_f32(v2678, v2684, v3176);
    float32x2_t v2698 = vfma_f32(v2689, v2695, v3187);
    float32x2_t v2775 = vrev64_f32(v2769);
    float32x2_t v2777 = vadd_f32(v2725, v2768);
    float32x2_t v2778 = vsub_f32(v2725, v2768);
    float32x2_t v2844 = vrev64_f32(v2838);
    float32x2_t v2846 = vadd_f32(v2794, v2837);
    float32x2_t v2847 = vsub_f32(v2794, v2837);
    float32x2_t v2861 = vsub_f32(v2598, v2858);
    float32x2_t v2862 = vadd_f32(v2598, v2858);
    float32x2_t v3076 = vadd_f32(v3057, v3064);
    float32x2_t v3077 = vadd_f32(v3068, v3075);
    float32x2_t v2553 = vmul_f32(v2552, v3255);
    float32x2_t v2625 = vadd_f32(v2623, v2624);
    float32x2_t v2626 = vsub_f32(v2624, v2623);
    float32x2_t v2669 = vrev64_f32(v2663);
    float32x2_t v2671 = vadd_f32(v2519, v2662);
    float32x2_t v2672 = vsub_f32(v2519, v2662);
    float32x2_t v2699 = vadd_f32(v2697, v2698);
    float32x2_t v2700 = vsub_f32(v2698, v2697);
    float32x2_t v2776 = vmul_f32(v2775, v3255);
    float32x2_t v2845 = vmul_f32(v2844, v3255);
    v6[ostride * 8] = v2861;
    v6[ostride * 24] = v2862;
    float32x2_t v2886 = vmul_f32(v2777, v2885);
    float32x2_t v2892 = vrev64_f32(v2777);
    float32x2_t v2897 = vmul_f32(v2846, v2999);
    float32x2_t v2903 = vrev64_f32(v2846);
    float32x2_t v2962 = vfma_f32(v2943, v2949, v2948);
    float32x2_t v2963 = vfma_f32(v2954, v2960, v3176);
    float32x2_t v3078 = vadd_f32(v3076, v3077);
    float32x2_t v3079 = vsub_f32(v3077, v3076);
    float32x2_t v3114 = vmul_f32(v2778, v3113);
    float32x2_t v3120 = vrev64_f32(v2778);
    float32x2_t v3125 = vmul_f32(v2847, v3124);
    float32x2_t v3131 = vrev64_f32(v2847);
    float32x2_t v3190 = vfma_f32(v3171, v3177, v3176);
    float32x2_t v3191 = vfma_f32(v3182, v3188, v3187);
    float32x2_t v2556 = vsub_f32(v2503, v2553);
    float32x2_t v2557 = vadd_f32(v2503, v2553);
    float32x2_t v2632 = vrev64_f32(v2626);
    float32x2_t v2634 = vadd_f32(v2554, v2625);
    float32x2_t v2635 = vsub_f32(v2554, v2625);
    float32x2_t v2670 = vmul_f32(v2669, v3255);
    float32x2_t v2706 = vrev64_f32(v2700);
    float32x2_t v2779 = vsub_f32(v2726, v2776);
    float32x2_t v2780 = vadd_f32(v2726, v2776);
    float32x2_t v2848 = vsub_f32(v2795, v2845);
    float32x2_t v2849 = vadd_f32(v2795, v2845);
    float32x2_t v2964 = vadd_f32(v2962, v2963);
    float32x2_t v2965 = vsub_f32(v2963, v2962);
    float32x2_t v3085 = vrev64_f32(v3079);
    float32x2_t v3087 = vadd_f32(v2599, v3078);
    float32x2_t v3088 = vsub_f32(v2599, v3078);
    float32x2_t v3192 = vadd_f32(v3190, v3191);
    float32x2_t v3193 = vsub_f32(v3191, v3190);
    float32x2_t v2633 = vmul_f32(v2632, v3255);
    float32x2_t v2673 = vsub_f32(v2520, v2670);
    float32x2_t v2674 = vadd_f32(v2520, v2670);
    float32x2_t v2707 = vmul_f32(v2706, v3255);
    float32x2_t v2708 = vadd_f32(v2556, v2699);
    float32x2_t v2709 = vsub_f32(v2556, v2699);
    float32x2_t v2905 = vfma_f32(v2886, v2892, v3130);
    float32x2_t v2906 = vfma_f32(v2897, v2903, v3005);
    float32x2_t v2971 = vrev64_f32(v2965);
    float32x2_t v2973 = vadd_f32(v2671, v2964);
    float32x2_t v2974 = vsub_f32(v2671, v2964);
    float32x2_t v3000 = vmul_f32(v2779, v2999);
    float32x2_t v3006 = vrev64_f32(v2779);
    float32x2_t v3011 = vmul_f32(v2848, v3010);
    float32x2_t v3017 = vrev64_f32(v2848);
    float32x2_t v3086 = vmul_f32(v3085, v3255);
    v6[ostride * 4] = v3087;
    v6[ostride * 20] = v3088;
    float32x2_t v3133 = vfma_f32(v3114, v3120, v3119);
    float32x2_t v3134 = vfma_f32(v3125, v3131, v3130);
    float32x2_t v3199 = vrev64_f32(v3193);
    float32x2_t v3228 = vmul_f32(v2780, v3227);
    float32x2_t v3234 = vrev64_f32(v2780);
    float32x2_t v3239 = vmul_f32(v2849, v3238);
    float32x2_t v3245 = vrev64_f32(v2849);
    float32x2_t v2636 = vsub_f32(v2555, v2633);
    float32x2_t v2637 = vadd_f32(v2555, v2633);
    float32x2_t v2710 = vsub_f32(v2557, v2707);
    float32x2_t v2711 = vadd_f32(v2557, v2707);
    float32x2_t v2907 = vadd_f32(v2905, v2906);
    float32x2_t v2908 = vsub_f32(v2906, v2905);
    float32x2_t v2972 = vmul_f32(v2971, v3255);
    v6[ostride * 2] = v2973;
    v6[ostride * 18] = v2974;
    float32x2_t v3089 = vsub_f32(v2600, v3086);
    float32x2_t v3090 = vadd_f32(v2600, v3086);
    float32x2_t v3135 = vadd_f32(v3133, v3134);
    float32x2_t v3136 = vsub_f32(v3134, v3133);
    float32x2_t v3200 = vmul_f32(v3199, v3255);
    float32x2_t v3201 = vadd_f32(v2673, v3192);
    float32x2_t v3202 = vsub_f32(v2673, v3192);
    float32x2_t v2914 = vrev64_f32(v2908);
    float32x2_t v2916 = vadd_f32(v2634, v2907);
    float32x2_t v2917 = vsub_f32(v2634, v2907);
    float32x2_t v2975 = vsub_f32(v2672, v2972);
    float32x2_t v2976 = vadd_f32(v2672, v2972);
    float32x2_t v3019 = vfma_f32(v3000, v3006, v3005);
    float32x2_t v3020 = vfma_f32(v3011, v3017, v3233);
    v6[ostride * 12] = v3089;
    v6[ostride * 28] = v3090;
    float32x2_t v3142 = vrev64_f32(v3136);
    float32x2_t v3144 = vadd_f32(v2636, v3135);
    float32x2_t v3145 = vsub_f32(v2636, v3135);
    float32x2_t v3203 = vsub_f32(v2674, v3200);
    float32x2_t v3204 = vadd_f32(v2674, v3200);
    v6[ostride * 6] = v3201;
    v6[ostride * 22] = v3202;
    float32x2_t v3247 = vfma_f32(v3228, v3234, v3233);
    float32x2_t v3248 = vfma_f32(v3239, v3245, v3244);
    float32x2_t v2915 = vmul_f32(v2914, v3255);
    v6[ostride] = v2916;
    v6[ostride * 17] = v2917;
    v6[ostride * 10] = v2975;
    v6[ostride * 26] = v2976;
    float32x2_t v3021 = vadd_f32(v3019, v3020);
    float32x2_t v3022 = vsub_f32(v3020, v3019);
    float32x2_t v3143 = vmul_f32(v3142, v3255);
    v6[ostride * 5] = v3144;
    v6[ostride * 21] = v3145;
    v6[ostride * 14] = v3203;
    v6[ostride * 30] = v3204;
    float32x2_t v3249 = vadd_f32(v3247, v3248);
    float32x2_t v3250 = vsub_f32(v3248, v3247);
    float32x2_t v2918 = vsub_f32(v2635, v2915);
    float32x2_t v2919 = vadd_f32(v2635, v2915);
    float32x2_t v3028 = vrev64_f32(v3022);
    float32x2_t v3030 = vadd_f32(v2708, v3021);
    float32x2_t v3031 = vsub_f32(v2708, v3021);
    float32x2_t v3146 = vsub_f32(v2637, v3143);
    float32x2_t v3147 = vadd_f32(v2637, v3143);
    float32x2_t v3256 = vrev64_f32(v3250);
    float32x2_t v3258 = vadd_f32(v2710, v3249);
    float32x2_t v3259 = vsub_f32(v2710, v3249);
    v6[ostride * 9] = v2918;
    v6[ostride * 25] = v2919;
    float32x2_t v3029 = vmul_f32(v3028, v3255);
    v6[ostride * 3] = v3030;
    v6[ostride * 19] = v3031;
    v6[ostride * 13] = v3146;
    v6[ostride * 29] = v3147;
    float32x2_t v3257 = vmul_f32(v3256, v3255);
    v6[ostride * 7] = v3258;
    v6[ostride * 23] = v3259;
    float32x2_t v3032 = vsub_f32(v2709, v3029);
    float32x2_t v3033 = vadd_f32(v2709, v3029);
    float32x2_t v3260 = vsub_f32(v2711, v3257);
    float32x2_t v3261 = vadd_f32(v2711, v3257);
    v6[ostride * 11] = v3032;
    v6[ostride * 27] = v3033;
    v6[ostride * 15] = v3260;
    v6[ostride * 31] = v3261;
    v5 += 1 * 1;
    v6 += 1 * 1;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ac_t_uu32(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, float dir) {
  int64_t v0 = istride;
  int64_t v2 = ostride;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * 1;
  int64_t v12 = v10 * 1;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    float v1011 = -1.9509032201612819e-01F;
    float v1066 = 7.0710678118654757e-01F;
    float v1078 = -7.0710678118654746e-01F;
    float v1083 = -1.0000000000000000e+00F;
    float v1133 = 5.5557023301960229e-01F;
    float v1138 = 8.3146961230254524e-01F;
    float v1145 = -9.8078528040323043e-01F;
    float v1200 = 3.8268343236508984e-01F;
    float v1205 = 9.2387953251128674e-01F;
    float v1212 = -9.2387953251128685e-01F;
    float v1217 = -3.8268343236508967e-01F;
    float v1267 = 1.9509032201612833e-01F;
    float v1272 = 9.8078528040323043e-01F;
    float v1279 = -5.5557023301960218e-01F;
    float v1284 = -8.3146961230254524e-01F;
    const float32x2_t *v1474 = &v5[v0];
    float32x2_t *v1710 = &v6[v2];
    int64_t v19 = v0 * 16;
    int64_t v34 = v0 * 8;
    int64_t v45 = v0 * 24;
    int64_t v56 = v0 * 4;
    int64_t v67 = v0 * 20;
    int64_t v86 = v0 * 12;
    int64_t v97 = v0 * 28;
    int64_t v116 = v0 * 2;
    int64_t v127 = v0 * 18;
    int64_t v146 = v0 * 10;
    int64_t v157 = v0 * 26;
    int64_t v168 = v0 * 6;
    int64_t v179 = v0 * 22;
    int64_t v198 = v0 * 14;
    int64_t v209 = v0 * 30;
    int64_t v231 = v0 * 17;
    int64_t v250 = v0 * 9;
    int64_t v261 = v0 * 25;
    int64_t v272 = v0 * 5;
    int64_t v283 = v0 * 21;
    int64_t v302 = v0 * 13;
    int64_t v313 = v0 * 29;
    int64_t v332 = v0 * 3;
    int64_t v343 = v0 * 19;
    int64_t v362 = v0 * 11;
    int64_t v373 = v0 * 27;
    int64_t v384 = v0 * 7;
    int64_t v395 = v0 * 23;
    int64_t v414 = v0 * 15;
    int64_t v425 = v0 * 31;
    int64_t v844 = v2 * 8;
    int64_t v851 = v2 * 16;
    int64_t v858 = v2 * 24;
    int64_t v911 = v2 * 9;
    int64_t v918 = v2 * 17;
    int64_t v925 = v2 * 25;
    float v940 = v4 * v1200;
    int64_t v971 = v2 * 2;
    int64_t v978 = v2 * 10;
    int64_t v985 = v2 * 18;
    int64_t v992 = v2 * 26;
    float v1007 = v4 * v1133;
    int64_t v1038 = v2 * 3;
    int64_t v1045 = v2 * 11;
    int64_t v1052 = v2 * 19;
    int64_t v1059 = v2 * 27;
    float v1086 = v4 * v1083;
    int64_t v1105 = v2 * 4;
    int64_t v1112 = v2 * 12;
    int64_t v1119 = v2 * 20;
    int64_t v1126 = v2 * 28;
    float v1141 = v4 * v1138;
    float v1153 = v4 * v1267;
    int64_t v1172 = v2 * 5;
    int64_t v1179 = v2 * 13;
    int64_t v1186 = v2 * 21;
    int64_t v1193 = v2 * 29;
    float v1208 = v4 * v1205;
    float v1220 = v4 * v1217;
    int64_t v1239 = v2 * 6;
    int64_t v1246 = v2 * 14;
    int64_t v1253 = v2 * 22;
    int64_t v1260 = v2 * 30;
    float v1275 = v4 * v1272;
    float v1287 = v4 * v1284;
    int64_t v1306 = v2 * 7;
    int64_t v1313 = v2 * 15;
    int64_t v1320 = v2 * 23;
    int64_t v1327 = v2 * 31;
    const float32x2_t *v1619 = &v5[0];
    float32x2_t *v1669 = &v6[0];
    svfloat32_t v1699 = svdup_n_f32(v1272);
    svfloat32_t v1740 = svdup_n_f32(v1205);
    svfloat32_t v1781 = svdup_n_f32(v1138);
    svfloat32_t v1783 = svdup_n_f32(v1011);
    svfloat32_t v1822 = svdup_n_f32(v1066);
    svfloat32_t v1824 = svdup_n_f32(v1078);
    svfloat32_t v1863 = svdup_n_f32(v1133);
    svfloat32_t v1865 = svdup_n_f32(v1145);
    svfloat32_t v1904 = svdup_n_f32(v1200);
    svfloat32_t v1906 = svdup_n_f32(v1212);
    svfloat32_t v1945 = svdup_n_f32(v1267);
    svfloat32_t v1947 = svdup_n_f32(v1279);
    svfloat32_t v1949 = svdup_n_f32(v4);
    svfloat32_t v2017 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1474)[0]));
    svfloat32_t v31 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[15]));
    svfloat32_t v42 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[7]));
    svfloat32_t v53 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[23]));
    svfloat32_t v79 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[3]));
    svfloat32_t v83 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[19]));
    svfloat32_t v109 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[11]));
    svfloat32_t v113 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[27]));
    svfloat32_t v139 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[1]));
    svfloat32_t v143 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[17]));
    svfloat32_t v154 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[9]));
    svfloat32_t v165 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[25]));
    svfloat32_t v191 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[5]));
    svfloat32_t v195 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[21]));
    svfloat32_t v206 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[13]));
    svfloat32_t v217 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[29]));
    svfloat32_t v243 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[0]));
    svfloat32_t v247 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[16]));
    svfloat32_t v258 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[8]));
    svfloat32_t v269 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[24]));
    svfloat32_t v295 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[4]));
    svfloat32_t v299 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[20]));
    svfloat32_t v325 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[12]));
    svfloat32_t v329 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[28]));
    svfloat32_t v355 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[2]));
    svfloat32_t v359 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[18]));
    svfloat32_t v370 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[10]));
    svfloat32_t v381 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[26]));
    svfloat32_t v407 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[6]));
    svfloat32_t v411 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[22]));
    svfloat32_t v437 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[14]));
    svfloat32_t v441 =
        svreinterpret_f32_u64(svdup_n_u64(((const uint64_t *)v7)[30]));
    const float32x2_t *v1339 = &v5[v19];
    const float32x2_t *v1348 = &v5[v34];
    const float32x2_t *v1357 = &v5[v45];
    const float32x2_t *v1366 = &v5[v56];
    const float32x2_t *v1375 = &v5[v67];
    const float32x2_t *v1384 = &v5[v86];
    const float32x2_t *v1393 = &v5[v97];
    const float32x2_t *v1402 = &v5[v116];
    const float32x2_t *v1411 = &v5[v127];
    const float32x2_t *v1420 = &v5[v146];
    const float32x2_t *v1429 = &v5[v157];
    const float32x2_t *v1438 = &v5[v168];
    const float32x2_t *v1447 = &v5[v179];
    const float32x2_t *v1456 = &v5[v198];
    const float32x2_t *v1465 = &v5[v209];
    const float32x2_t *v1483 = &v5[v231];
    const float32x2_t *v1492 = &v5[v250];
    const float32x2_t *v1501 = &v5[v261];
    const float32x2_t *v1510 = &v5[v272];
    const float32x2_t *v1519 = &v5[v283];
    const float32x2_t *v1528 = &v5[v302];
    const float32x2_t *v1537 = &v5[v313];
    const float32x2_t *v1546 = &v5[v332];
    const float32x2_t *v1555 = &v5[v343];
    const float32x2_t *v1564 = &v5[v362];
    const float32x2_t *v1573 = &v5[v373];
    const float32x2_t *v1582 = &v5[v384];
    const float32x2_t *v1591 = &v5[v395];
    const float32x2_t *v1600 = &v5[v414];
    const float32x2_t *v1609 = &v5[v425];
    float32x2_t *v1678 = &v6[v844];
    float32x2_t *v1687 = &v6[v851];
    float32x2_t *v1696 = &v6[v858];
    float32x2_t *v1719 = &v6[v911];
    float32x2_t *v1728 = &v6[v918];
    float32x2_t *v1737 = &v6[v925];
    svfloat32_t v1741 = svdup_n_f32(v940);
    float32x2_t *v1751 = &v6[v971];
    float32x2_t *v1760 = &v6[v978];
    float32x2_t *v1769 = &v6[v985];
    float32x2_t *v1778 = &v6[v992];
    svfloat32_t v1782 = svdup_n_f32(v1007);
    float32x2_t *v1792 = &v6[v1038];
    float32x2_t *v1801 = &v6[v1045];
    float32x2_t *v1810 = &v6[v1052];
    float32x2_t *v1819 = &v6[v1059];
    svfloat32_t v1825 = svdup_n_f32(v1086);
    float32x2_t *v1833 = &v6[v1105];
    float32x2_t *v1842 = &v6[v1112];
    float32x2_t *v1851 = &v6[v1119];
    float32x2_t *v1860 = &v6[v1126];
    svfloat32_t v1864 = svdup_n_f32(v1141);
    svfloat32_t v1866 = svdup_n_f32(v1153);
    float32x2_t *v1874 = &v6[v1172];
    float32x2_t *v1883 = &v6[v1179];
    float32x2_t *v1892 = &v6[v1186];
    float32x2_t *v1901 = &v6[v1193];
    svfloat32_t v1905 = svdup_n_f32(v1208);
    svfloat32_t v1907 = svdup_n_f32(v1220);
    float32x2_t *v1915 = &v6[v1239];
    float32x2_t *v1924 = &v6[v1246];
    float32x2_t *v1933 = &v6[v1253];
    float32x2_t *v1942 = &v6[v1260];
    svfloat32_t v1946 = svdup_n_f32(v1275);
    svfloat32_t v1948 = svdup_n_f32(v1287);
    float32x2_t *v1956 = &v6[v1306];
    float32x2_t *v1965 = &v6[v1313];
    float32x2_t *v1974 = &v6[v1320];
    float32x2_t *v1983 = &v6[v1327];
    svfloat32_t v2049 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1619)[0]));
    svfloat32_t zero244 = svdup_n_f32(0);
    svfloat32_t v244 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero244, v2017, v243, 0), v2017,
        v243, 90);
    svfloat32_t v1987 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1339)[0]));
    svfloat32_t v1989 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1348)[0]));
    svfloat32_t v1991 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1357)[0]));
    svfloat32_t v1993 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1366)[0]));
    svfloat32_t v1995 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1375)[0]));
    svfloat32_t v1997 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1384)[0]));
    svfloat32_t v1999 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1393)[0]));
    svfloat32_t v2001 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1402)[0]));
    svfloat32_t v2003 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1411)[0]));
    svfloat32_t v2005 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1420)[0]));
    svfloat32_t v2007 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1429)[0]));
    svfloat32_t v2009 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1438)[0]));
    svfloat32_t v2011 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1447)[0]));
    svfloat32_t v2013 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1456)[0]));
    svfloat32_t v2015 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1465)[0]));
    svfloat32_t v2019 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1483)[0]));
    svfloat32_t v2021 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1492)[0]));
    svfloat32_t v2023 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1501)[0]));
    svfloat32_t v2025 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1510)[0]));
    svfloat32_t v2027 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1519)[0]));
    svfloat32_t v2029 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1528)[0]));
    svfloat32_t v2031 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1537)[0]));
    svfloat32_t v2033 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1546)[0]));
    svfloat32_t v2035 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1555)[0]));
    svfloat32_t v2037 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1564)[0]));
    svfloat32_t v2039 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1573)[0]));
    svfloat32_t v2041 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1582)[0]));
    svfloat32_t v2043 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1591)[0]));
    svfloat32_t v2045 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1600)[0]));
    svfloat32_t v2047 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v1609)[0]));
    svfloat32_t zero32 = svdup_n_f32(0);
    svfloat32_t v32 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero32, v1987, v31, 0),
                     v1987, v31, 90);
    svfloat32_t zero43 = svdup_n_f32(0);
    svfloat32_t v43 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero43, v1989, v42, 0),
                     v1989, v42, 90);
    svfloat32_t zero54 = svdup_n_f32(0);
    svfloat32_t v54 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero54, v1991, v53, 0),
                     v1991, v53, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v1993, v79, 0),
                     v1993, v79, 90);
    svfloat32_t zero84 = svdup_n_f32(0);
    svfloat32_t v84 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero84, v1995, v83, 0),
                     v1995, v83, 90);
    svfloat32_t zero110 = svdup_n_f32(0);
    svfloat32_t v110 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero110, v1997, v109, 0), v1997,
        v109, 90);
    svfloat32_t zero114 = svdup_n_f32(0);
    svfloat32_t v114 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero114, v1999, v113, 0), v1999,
        v113, 90);
    svfloat32_t zero140 = svdup_n_f32(0);
    svfloat32_t v140 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero140, v2001, v139, 0), v2001,
        v139, 90);
    svfloat32_t zero144 = svdup_n_f32(0);
    svfloat32_t v144 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero144, v2003, v143, 0), v2003,
        v143, 90);
    svfloat32_t zero155 = svdup_n_f32(0);
    svfloat32_t v155 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero155, v2005, v154, 0), v2005,
        v154, 90);
    svfloat32_t zero166 = svdup_n_f32(0);
    svfloat32_t v166 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero166, v2007, v165, 0), v2007,
        v165, 90);
    svfloat32_t zero192 = svdup_n_f32(0);
    svfloat32_t v192 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero192, v2009, v191, 0), v2009,
        v191, 90);
    svfloat32_t zero196 = svdup_n_f32(0);
    svfloat32_t v196 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero196, v2011, v195, 0), v2011,
        v195, 90);
    svfloat32_t zero207 = svdup_n_f32(0);
    svfloat32_t v207 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero207, v2013, v206, 0), v2013,
        v206, 90);
    svfloat32_t zero218 = svdup_n_f32(0);
    svfloat32_t v218 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero218, v2015, v217, 0), v2015,
        v217, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero248, v2019, v247, 0), v2019,
        v247, 90);
    svfloat32_t zero259 = svdup_n_f32(0);
    svfloat32_t v259 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero259, v2021, v258, 0), v2021,
        v258, 90);
    svfloat32_t zero270 = svdup_n_f32(0);
    svfloat32_t v270 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero270, v2023, v269, 0), v2023,
        v269, 90);
    svfloat32_t zero296 = svdup_n_f32(0);
    svfloat32_t v296 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero296, v2025, v295, 0), v2025,
        v295, 90);
    svfloat32_t zero300 = svdup_n_f32(0);
    svfloat32_t v300 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero300, v2027, v299, 0), v2027,
        v299, 90);
    svfloat32_t zero326 = svdup_n_f32(0);
    svfloat32_t v326 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero326, v2029, v325, 0), v2029,
        v325, 90);
    svfloat32_t zero330 = svdup_n_f32(0);
    svfloat32_t v330 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero330, v2031, v329, 0), v2031,
        v329, 90);
    svfloat32_t zero356 = svdup_n_f32(0);
    svfloat32_t v356 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero356, v2033, v355, 0), v2033,
        v355, 90);
    svfloat32_t zero360 = svdup_n_f32(0);
    svfloat32_t v360 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero360, v2035, v359, 0), v2035,
        v359, 90);
    svfloat32_t zero371 = svdup_n_f32(0);
    svfloat32_t v371 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero371, v2037, v370, 0), v2037,
        v370, 90);
    svfloat32_t zero382 = svdup_n_f32(0);
    svfloat32_t v382 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero382, v2039, v381, 0), v2039,
        v381, 90);
    svfloat32_t zero408 = svdup_n_f32(0);
    svfloat32_t v408 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero408, v2041, v407, 0), v2041,
        v407, 90);
    svfloat32_t zero412 = svdup_n_f32(0);
    svfloat32_t v412 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero412, v2043, v411, 0), v2043,
        v411, 90);
    svfloat32_t zero438 = svdup_n_f32(0);
    svfloat32_t v438 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero438, v2045, v437, 0), v2045,
        v437, 90);
    svfloat32_t zero442 = svdup_n_f32(0);
    svfloat32_t v442 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero442, v2047, v441, 0), v2047,
        v441, 90);
    svfloat32_t v450 = svadd_f32_x(svptrue_b32(), v2049, v32);
    svfloat32_t v451 = svsub_f32_x(svptrue_b32(), v2049, v32);
    svfloat32_t v452 = svadd_f32_x(svptrue_b32(), v43, v54);
    svfloat32_t v453 = svsub_f32_x(svptrue_b32(), v43, v54);
    svfloat32_t v465 = svadd_f32_x(svptrue_b32(), v80, v84);
    svfloat32_t v466 = svsub_f32_x(svptrue_b32(), v80, v84);
    svfloat32_t v467 = svadd_f32_x(svptrue_b32(), v110, v114);
    svfloat32_t v468 = svsub_f32_x(svptrue_b32(), v110, v114);
    svfloat32_t v521 = svadd_f32_x(svptrue_b32(), v140, v144);
    svfloat32_t v522 = svsub_f32_x(svptrue_b32(), v140, v144);
    svfloat32_t v523 = svadd_f32_x(svptrue_b32(), v155, v166);
    svfloat32_t v524 = svsub_f32_x(svptrue_b32(), v155, v166);
    svfloat32_t v536 = svadd_f32_x(svptrue_b32(), v192, v196);
    svfloat32_t v537 = svsub_f32_x(svptrue_b32(), v192, v196);
    svfloat32_t v538 = svadd_f32_x(svptrue_b32(), v207, v218);
    svfloat32_t v539 = svsub_f32_x(svptrue_b32(), v207, v218);
    svfloat32_t v681 = svadd_f32_x(svptrue_b32(), v244, v248);
    svfloat32_t v682 = svsub_f32_x(svptrue_b32(), v244, v248);
    svfloat32_t v683 = svadd_f32_x(svptrue_b32(), v259, v270);
    svfloat32_t v684 = svsub_f32_x(svptrue_b32(), v259, v270);
    svfloat32_t v696 = svadd_f32_x(svptrue_b32(), v296, v300);
    svfloat32_t v697 = svsub_f32_x(svptrue_b32(), v296, v300);
    svfloat32_t v698 = svadd_f32_x(svptrue_b32(), v326, v330);
    svfloat32_t v699 = svsub_f32_x(svptrue_b32(), v326, v330);
    svfloat32_t v752 = svadd_f32_x(svptrue_b32(), v356, v360);
    svfloat32_t v753 = svsub_f32_x(svptrue_b32(), v356, v360);
    svfloat32_t v754 = svadd_f32_x(svptrue_b32(), v371, v382);
    svfloat32_t v755 = svsub_f32_x(svptrue_b32(), v371, v382);
    svfloat32_t v767 = svadd_f32_x(svptrue_b32(), v408, v412);
    svfloat32_t v768 = svsub_f32_x(svptrue_b32(), v408, v412);
    svfloat32_t v769 = svadd_f32_x(svptrue_b32(), v438, v442);
    svfloat32_t v770 = svsub_f32_x(svptrue_b32(), v438, v442);
    svfloat32_t zero460 = svdup_n_f32(0);
    svfloat32_t v460 = svcmla_f32_x(pred_full, zero460, v1825, v453, 90);
    svfloat32_t v461 = svadd_f32_x(svptrue_b32(), v450, v452);
    svfloat32_t v462 = svsub_f32_x(svptrue_b32(), v450, v452);
    svfloat32_t v469 = svadd_f32_x(svptrue_b32(), v465, v467);
    svfloat32_t v470 = svsub_f32_x(svptrue_b32(), v465, v467);
    svfloat32_t v486 = svmul_f32_x(svptrue_b32(), v466, v1822);
    svfloat32_t v498 = svmul_f32_x(svptrue_b32(), v468, v1824);
    svfloat32_t zero531 = svdup_n_f32(0);
    svfloat32_t v531 = svcmla_f32_x(pred_full, zero531, v1825, v524, 90);
    svfloat32_t v532 = svadd_f32_x(svptrue_b32(), v521, v523);
    svfloat32_t v533 = svsub_f32_x(svptrue_b32(), v521, v523);
    svfloat32_t zero546 = svdup_n_f32(0);
    svfloat32_t v546 = svcmla_f32_x(pred_full, zero546, v1825, v539, 90);
    svfloat32_t v547 = svadd_f32_x(svptrue_b32(), v536, v538);
    svfloat32_t v548 = svsub_f32_x(svptrue_b32(), v536, v538);
    svfloat32_t zero691 = svdup_n_f32(0);
    svfloat32_t v691 = svcmla_f32_x(pred_full, zero691, v1825, v684, 90);
    svfloat32_t v692 = svadd_f32_x(svptrue_b32(), v681, v683);
    svfloat32_t v693 = svsub_f32_x(svptrue_b32(), v681, v683);
    svfloat32_t v700 = svadd_f32_x(svptrue_b32(), v696, v698);
    svfloat32_t v701 = svsub_f32_x(svptrue_b32(), v696, v698);
    svfloat32_t v717 = svmul_f32_x(svptrue_b32(), v697, v1822);
    svfloat32_t v729 = svmul_f32_x(svptrue_b32(), v699, v1824);
    svfloat32_t zero762 = svdup_n_f32(0);
    svfloat32_t v762 = svcmla_f32_x(pred_full, zero762, v1825, v755, 90);
    svfloat32_t v763 = svadd_f32_x(svptrue_b32(), v752, v754);
    svfloat32_t v764 = svsub_f32_x(svptrue_b32(), v752, v754);
    svfloat32_t v771 = svadd_f32_x(svptrue_b32(), v767, v769);
    svfloat32_t v772 = svsub_f32_x(svptrue_b32(), v767, v769);
    svfloat32_t v788 = svmul_f32_x(svptrue_b32(), v768, v1822);
    svfloat32_t v800 = svmul_f32_x(svptrue_b32(), v770, v1824);
    svfloat32_t v463 = svsub_f32_x(svptrue_b32(), v451, v460);
    svfloat32_t v464 = svadd_f32_x(svptrue_b32(), v451, v460);
    svfloat32_t zero477 = svdup_n_f32(0);
    svfloat32_t v477 = svcmla_f32_x(pred_full, zero477, v1825, v470, 90);
    svfloat32_t v478 = svadd_f32_x(svptrue_b32(), v461, v469);
    svfloat32_t v479 = svsub_f32_x(svptrue_b32(), v461, v469);
    svfloat32_t v534 = svsub_f32_x(svptrue_b32(), v522, v531);
    svfloat32_t v535 = svadd_f32_x(svptrue_b32(), v522, v531);
    svfloat32_t v549 = svsub_f32_x(svptrue_b32(), v537, v546);
    svfloat32_t v550 = svadd_f32_x(svptrue_b32(), v537, v546);
    svfloat32_t v551 = svadd_f32_x(svptrue_b32(), v532, v547);
    svfloat32_t v552 = svsub_f32_x(svptrue_b32(), v532, v547);
    svfloat32_t v607 = svmul_f32_x(svptrue_b32(), v533, v1822);
    svfloat32_t v619 = svmul_f32_x(svptrue_b32(), v548, v1824);
    svfloat32_t v694 = svsub_f32_x(svptrue_b32(), v682, v691);
    svfloat32_t v695 = svadd_f32_x(svptrue_b32(), v682, v691);
    svfloat32_t zero708 = svdup_n_f32(0);
    svfloat32_t v708 = svcmla_f32_x(pred_full, zero708, v1825, v701, 90);
    svfloat32_t v709 = svadd_f32_x(svptrue_b32(), v692, v700);
    svfloat32_t v710 = svsub_f32_x(svptrue_b32(), v692, v700);
    svfloat32_t v765 = svsub_f32_x(svptrue_b32(), v753, v762);
    svfloat32_t v766 = svadd_f32_x(svptrue_b32(), v753, v762);
    svfloat32_t zero779 = svdup_n_f32(0);
    svfloat32_t v779 = svcmla_f32_x(pred_full, zero779, v1825, v772, 90);
    svfloat32_t v780 = svadd_f32_x(svptrue_b32(), v763, v771);
    svfloat32_t v781 = svsub_f32_x(svptrue_b32(), v763, v771);
    svfloat32_t v480 = svsub_f32_x(svptrue_b32(), v462, v477);
    svfloat32_t v481 = svadd_f32_x(svptrue_b32(), v462, v477);
    svfloat32_t v506 = svcmla_f32_x(pred_full, v486, v1949, v486, 90);
    svfloat32_t v507 = svcmla_f32_x(pred_full, v498, v1825, v498, 90);
    svfloat32_t zero559 = svdup_n_f32(0);
    svfloat32_t v559 = svcmla_f32_x(pred_full, zero559, v1825, v552, 90);
    svfloat32_t v560 = svadd_f32_x(svptrue_b32(), v478, v551);
    svfloat32_t v561 = svsub_f32_x(svptrue_b32(), v478, v551);
    svfloat32_t v568 = svmul_f32_x(svptrue_b32(), v534, v1740);
    svfloat32_t v580 = svmul_f32_x(svptrue_b32(), v549, v1904);
    svfloat32_t v646 = svmul_f32_x(svptrue_b32(), v535, v1904);
    svfloat32_t v658 = svmul_f32_x(svptrue_b32(), v550, v1906);
    svfloat32_t v711 = svsub_f32_x(svptrue_b32(), v693, v708);
    svfloat32_t v712 = svadd_f32_x(svptrue_b32(), v693, v708);
    svfloat32_t v737 = svcmla_f32_x(pred_full, v717, v1949, v717, 90);
    svfloat32_t v738 = svcmla_f32_x(pred_full, v729, v1825, v729, 90);
    svfloat32_t v782 = svsub_f32_x(svptrue_b32(), v764, v779);
    svfloat32_t v783 = svadd_f32_x(svptrue_b32(), v764, v779);
    svfloat32_t v808 = svcmla_f32_x(pred_full, v788, v1949, v788, 90);
    svfloat32_t v809 = svcmla_f32_x(pred_full, v800, v1825, v800, 90);
    svfloat32_t v823 = svadd_f32_x(svptrue_b32(), v709, v780);
    svfloat32_t v824 = svsub_f32_x(svptrue_b32(), v709, v780);
    svfloat32_t v1069 = svmul_f32_x(svptrue_b32(), v710, v1822);
    svfloat32_t v1081 = svmul_f32_x(svptrue_b32(), v781, v1824);
    svfloat32_t v508 = svadd_f32_x(svptrue_b32(), v506, v507);
    svfloat32_t v509 = svsub_f32_x(svptrue_b32(), v507, v506);
    svfloat32_t v562 = svsub_f32_x(svptrue_b32(), v479, v559);
    svfloat32_t v563 = svadd_f32_x(svptrue_b32(), v479, v559);
    svfloat32_t v588 = svcmla_f32_x(pred_full, v568, v1741, v534, 90);
    svfloat32_t v589 = svcmla_f32_x(pred_full, v580, v1905, v549, 90);
    svfloat32_t v627 = svcmla_f32_x(pred_full, v607, v1949, v607, 90);
    svfloat32_t v628 = svcmla_f32_x(pred_full, v619, v1825, v619, 90);
    svfloat32_t v666 = svcmla_f32_x(pred_full, v646, v1905, v535, 90);
    svfloat32_t v667 = svcmla_f32_x(pred_full, v658, v1907, v550, 90);
    svfloat32_t v739 = svadd_f32_x(svptrue_b32(), v737, v738);
    svfloat32_t v740 = svsub_f32_x(svptrue_b32(), v738, v737);
    svfloat32_t v810 = svadd_f32_x(svptrue_b32(), v808, v809);
    svfloat32_t v811 = svsub_f32_x(svptrue_b32(), v809, v808);
    svfloat32_t zero831 = svdup_n_f32(0);
    svfloat32_t v831 = svcmla_f32_x(pred_full, zero831, v1825, v824, 90);
    svfloat32_t v832 = svadd_f32_x(svptrue_b32(), v560, v823);
    svfloat32_t v833 = svsub_f32_x(svptrue_b32(), v560, v823);
    svfloat32_t v935 = svmul_f32_x(svptrue_b32(), v711, v1740);
    svfloat32_t v947 = svmul_f32_x(svptrue_b32(), v782, v1904);
    svfloat32_t v1203 = svmul_f32_x(svptrue_b32(), v712, v1904);
    svfloat32_t v1215 = svmul_f32_x(svptrue_b32(), v783, v1906);
    svfloat32_t zero516 = svdup_n_f32(0);
    svfloat32_t v516 = svcmla_f32_x(pred_full, zero516, v1949, v509, 90);
    svfloat32_t v517 = svadd_f32_x(svptrue_b32(), v463, v508);
    svfloat32_t v518 = svsub_f32_x(svptrue_b32(), v463, v508);
    svfloat32_t v590 = svadd_f32_x(svptrue_b32(), v588, v589);
    svfloat32_t v591 = svsub_f32_x(svptrue_b32(), v589, v588);
    svfloat32_t v629 = svadd_f32_x(svptrue_b32(), v627, v628);
    svfloat32_t v630 = svsub_f32_x(svptrue_b32(), v628, v627);
    svfloat32_t v668 = svadd_f32_x(svptrue_b32(), v666, v667);
    svfloat32_t v669 = svsub_f32_x(svptrue_b32(), v667, v666);
    svfloat32_t zero747 = svdup_n_f32(0);
    svfloat32_t v747 = svcmla_f32_x(pred_full, zero747, v1949, v740, 90);
    svfloat32_t v748 = svadd_f32_x(svptrue_b32(), v694, v739);
    svfloat32_t v749 = svsub_f32_x(svptrue_b32(), v694, v739);
    svfloat32_t zero818 = svdup_n_f32(0);
    svfloat32_t v818 = svcmla_f32_x(pred_full, zero818, v1949, v811, 90);
    svfloat32_t v819 = svadd_f32_x(svptrue_b32(), v765, v810);
    svfloat32_t v820 = svsub_f32_x(svptrue_b32(), v765, v810);
    svfloat32_t v834 = svsub_f32_x(svptrue_b32(), v561, v831);
    svfloat32_t v835 = svadd_f32_x(svptrue_b32(), v561, v831);
    svfloat32_t v955 = svcmla_f32_x(pred_full, v935, v1741, v711, 90);
    svfloat32_t v956 = svcmla_f32_x(pred_full, v947, v1905, v782, 90);
    svfloat32_t v1089 = svcmla_f32_x(pred_full, v1069, v1949, v1069, 90);
    svfloat32_t v1090 = svcmla_f32_x(pred_full, v1081, v1825, v1081, 90);
    svfloat32_t v1223 = svcmla_f32_x(pred_full, v1203, v1905, v712, 90);
    svfloat32_t v1224 = svcmla_f32_x(pred_full, v1215, v1907, v783, 90);
    svst1_f64(pred_full, (double *)(v1669), svreinterpret_f64_f32(v832));
    svst1_f64(pred_full, (double *)(v1687), svreinterpret_f64_f32(v833));
    svfloat32_t v519 = svsub_f32_x(svptrue_b32(), v464, v516);
    svfloat32_t v520 = svadd_f32_x(svptrue_b32(), v464, v516);
    svfloat32_t zero598 = svdup_n_f32(0);
    svfloat32_t v598 = svcmla_f32_x(pred_full, zero598, v1949, v591, 90);
    svfloat32_t v599 = svadd_f32_x(svptrue_b32(), v517, v590);
    svfloat32_t v600 = svsub_f32_x(svptrue_b32(), v517, v590);
    svfloat32_t zero637 = svdup_n_f32(0);
    svfloat32_t v637 = svcmla_f32_x(pred_full, zero637, v1949, v630, 90);
    svfloat32_t v638 = svadd_f32_x(svptrue_b32(), v480, v629);
    svfloat32_t v639 = svsub_f32_x(svptrue_b32(), v480, v629);
    svfloat32_t zero676 = svdup_n_f32(0);
    svfloat32_t v676 = svcmla_f32_x(pred_full, zero676, v1949, v669, 90);
    svfloat32_t v750 = svsub_f32_x(svptrue_b32(), v695, v747);
    svfloat32_t v751 = svadd_f32_x(svptrue_b32(), v695, v747);
    svfloat32_t v821 = svsub_f32_x(svptrue_b32(), v766, v818);
    svfloat32_t v822 = svadd_f32_x(svptrue_b32(), v766, v818);
    svfloat32_t v868 = svmul_f32_x(svptrue_b32(), v748, v1699);
    svfloat32_t v880 = svmul_f32_x(svptrue_b32(), v819, v1781);
    svfloat32_t v957 = svadd_f32_x(svptrue_b32(), v955, v956);
    svfloat32_t v958 = svsub_f32_x(svptrue_b32(), v956, v955);
    svfloat32_t v1091 = svadd_f32_x(svptrue_b32(), v1089, v1090);
    svfloat32_t v1092 = svsub_f32_x(svptrue_b32(), v1090, v1089);
    svfloat32_t v1136 = svmul_f32_x(svptrue_b32(), v749, v1863);
    svfloat32_t v1148 = svmul_f32_x(svptrue_b32(), v820, v1865);
    svfloat32_t v1225 = svadd_f32_x(svptrue_b32(), v1223, v1224);
    svfloat32_t v1226 = svsub_f32_x(svptrue_b32(), v1224, v1223);
    svst1_f64(pred_full, (double *)(v1678), svreinterpret_f64_f32(v834));
    svst1_f64(pred_full, (double *)(v1696), svreinterpret_f64_f32(v835));
    svfloat32_t v601 = svsub_f32_x(svptrue_b32(), v518, v598);
    svfloat32_t v602 = svadd_f32_x(svptrue_b32(), v518, v598);
    svfloat32_t v640 = svsub_f32_x(svptrue_b32(), v481, v637);
    svfloat32_t v641 = svadd_f32_x(svptrue_b32(), v481, v637);
    svfloat32_t v677 = svadd_f32_x(svptrue_b32(), v519, v668);
    svfloat32_t v678 = svsub_f32_x(svptrue_b32(), v519, v668);
    svfloat32_t v679 = svsub_f32_x(svptrue_b32(), v520, v676);
    svfloat32_t v680 = svadd_f32_x(svptrue_b32(), v520, v676);
    svfloat32_t v888 = svcmla_f32_x(pred_full, v868, v1866, v748, 90);
    svfloat32_t v889 = svcmla_f32_x(pred_full, v880, v1782, v819, 90);
    svfloat32_t zero965 = svdup_n_f32(0);
    svfloat32_t v965 = svcmla_f32_x(pred_full, zero965, v1949, v958, 90);
    svfloat32_t v966 = svadd_f32_x(svptrue_b32(), v638, v957);
    svfloat32_t v967 = svsub_f32_x(svptrue_b32(), v638, v957);
    svfloat32_t v1002 = svmul_f32_x(svptrue_b32(), v750, v1781);
    svfloat32_t v1014 = svmul_f32_x(svptrue_b32(), v821, v1783);
    svfloat32_t zero1099 = svdup_n_f32(0);
    svfloat32_t v1099 = svcmla_f32_x(pred_full, zero1099, v1949, v1092, 90);
    svfloat32_t v1100 = svadd_f32_x(svptrue_b32(), v562, v1091);
    svfloat32_t v1101 = svsub_f32_x(svptrue_b32(), v562, v1091);
    svfloat32_t v1156 = svcmla_f32_x(pred_full, v1136, v1864, v749, 90);
    svfloat32_t v1157 = svcmla_f32_x(pred_full, v1148, v1866, v820, 90);
    svfloat32_t zero1233 = svdup_n_f32(0);
    svfloat32_t v1233 = svcmla_f32_x(pred_full, zero1233, v1949, v1226, 90);
    svfloat32_t v1270 = svmul_f32_x(svptrue_b32(), v751, v1945);
    svfloat32_t v1282 = svmul_f32_x(svptrue_b32(), v822, v1947);
    svfloat32_t v890 = svadd_f32_x(svptrue_b32(), v888, v889);
    svfloat32_t v891 = svsub_f32_x(svptrue_b32(), v889, v888);
    svfloat32_t v968 = svsub_f32_x(svptrue_b32(), v639, v965);
    svfloat32_t v969 = svadd_f32_x(svptrue_b32(), v639, v965);
    svfloat32_t v1022 = svcmla_f32_x(pred_full, v1002, v1782, v750, 90);
    svfloat32_t v1023 = svcmla_f32_x(pred_full, v1014, v1946, v821, 90);
    svfloat32_t v1102 = svsub_f32_x(svptrue_b32(), v563, v1099);
    svfloat32_t v1103 = svadd_f32_x(svptrue_b32(), v563, v1099);
    svfloat32_t v1158 = svadd_f32_x(svptrue_b32(), v1156, v1157);
    svfloat32_t v1159 = svsub_f32_x(svptrue_b32(), v1157, v1156);
    svfloat32_t v1234 = svadd_f32_x(svptrue_b32(), v640, v1225);
    svfloat32_t v1235 = svsub_f32_x(svptrue_b32(), v640, v1225);
    svfloat32_t v1236 = svsub_f32_x(svptrue_b32(), v641, v1233);
    svfloat32_t v1237 = svadd_f32_x(svptrue_b32(), v641, v1233);
    svfloat32_t v1290 = svcmla_f32_x(pred_full, v1270, v1946, v751, 90);
    svfloat32_t v1291 = svcmla_f32_x(pred_full, v1282, v1948, v822, 90);
    svst1_f64(pred_full, (double *)(v1751), svreinterpret_f64_f32(v966));
    svst1_f64(pred_full, (double *)(v1769), svreinterpret_f64_f32(v967));
    svst1_f64(pred_full, (double *)(v1833), svreinterpret_f64_f32(v1100));
    svst1_f64(pred_full, (double *)(v1851), svreinterpret_f64_f32(v1101));
    svfloat32_t zero898 = svdup_n_f32(0);
    svfloat32_t v898 = svcmla_f32_x(pred_full, zero898, v1949, v891, 90);
    svfloat32_t v899 = svadd_f32_x(svptrue_b32(), v599, v890);
    svfloat32_t v900 = svsub_f32_x(svptrue_b32(), v599, v890);
    svfloat32_t v1024 = svadd_f32_x(svptrue_b32(), v1022, v1023);
    svfloat32_t v1025 = svsub_f32_x(svptrue_b32(), v1023, v1022);
    svfloat32_t zero1166 = svdup_n_f32(0);
    svfloat32_t v1166 = svcmla_f32_x(pred_full, zero1166, v1949, v1159, 90);
    svfloat32_t v1167 = svadd_f32_x(svptrue_b32(), v601, v1158);
    svfloat32_t v1168 = svsub_f32_x(svptrue_b32(), v601, v1158);
    svfloat32_t v1292 = svadd_f32_x(svptrue_b32(), v1290, v1291);
    svfloat32_t v1293 = svsub_f32_x(svptrue_b32(), v1291, v1290);
    svst1_f64(pred_full, (double *)(v1760), svreinterpret_f64_f32(v968));
    svst1_f64(pred_full, (double *)(v1778), svreinterpret_f64_f32(v969));
    svst1_f64(pred_full, (double *)(v1842), svreinterpret_f64_f32(v1102));
    svst1_f64(pred_full, (double *)(v1860), svreinterpret_f64_f32(v1103));
    svst1_f64(pred_full, (double *)(v1915), svreinterpret_f64_f32(v1234));
    svst1_f64(pred_full, (double *)(v1924), svreinterpret_f64_f32(v1236));
    svst1_f64(pred_full, (double *)(v1933), svreinterpret_f64_f32(v1235));
    svst1_f64(pred_full, (double *)(v1942), svreinterpret_f64_f32(v1237));
    svfloat32_t v901 = svsub_f32_x(svptrue_b32(), v600, v898);
    svfloat32_t v902 = svadd_f32_x(svptrue_b32(), v600, v898);
    svfloat32_t zero1032 = svdup_n_f32(0);
    svfloat32_t v1032 = svcmla_f32_x(pred_full, zero1032, v1949, v1025, 90);
    svfloat32_t v1033 = svadd_f32_x(svptrue_b32(), v677, v1024);
    svfloat32_t v1034 = svsub_f32_x(svptrue_b32(), v677, v1024);
    svfloat32_t v1169 = svsub_f32_x(svptrue_b32(), v602, v1166);
    svfloat32_t v1170 = svadd_f32_x(svptrue_b32(), v602, v1166);
    svfloat32_t zero1300 = svdup_n_f32(0);
    svfloat32_t v1300 = svcmla_f32_x(pred_full, zero1300, v1949, v1293, 90);
    svfloat32_t v1301 = svadd_f32_x(svptrue_b32(), v679, v1292);
    svfloat32_t v1302 = svsub_f32_x(svptrue_b32(), v679, v1292);
    svst1_f64(pred_full, (double *)(v1710), svreinterpret_f64_f32(v899));
    svst1_f64(pred_full, (double *)(v1728), svreinterpret_f64_f32(v900));
    svst1_f64(pred_full, (double *)(v1874), svreinterpret_f64_f32(v1167));
    svst1_f64(pred_full, (double *)(v1892), svreinterpret_f64_f32(v1168));
    svfloat32_t v1035 = svsub_f32_x(svptrue_b32(), v678, v1032);
    svfloat32_t v1036 = svadd_f32_x(svptrue_b32(), v678, v1032);
    svfloat32_t v1303 = svsub_f32_x(svptrue_b32(), v680, v1300);
    svfloat32_t v1304 = svadd_f32_x(svptrue_b32(), v680, v1300);
    svst1_f64(pred_full, (double *)(v1719), svreinterpret_f64_f32(v901));
    svst1_f64(pred_full, (double *)(v1737), svreinterpret_f64_f32(v902));
    svst1_f64(pred_full, (double *)(v1792), svreinterpret_f64_f32(v1033));
    svst1_f64(pred_full, (double *)(v1810), svreinterpret_f64_f32(v1034));
    svst1_f64(pred_full, (double *)(v1883), svreinterpret_f64_f32(v1169));
    svst1_f64(pred_full, (double *)(v1901), svreinterpret_f64_f32(v1170));
    svst1_f64(pred_full, (double *)(v1956), svreinterpret_f64_f32(v1301));
    svst1_f64(pred_full, (double *)(v1974), svreinterpret_f64_f32(v1302));
    svst1_f64(pred_full, (double *)(v1801), svreinterpret_f64_f32(v1035));
    svst1_f64(pred_full, (double *)(v1819), svreinterpret_f64_f32(v1036));
    svst1_f64(pred_full, (double *)(v1965), svreinterpret_f64_f32(v1303));
    svst1_f64(pred_full, (double *)(v1983), svreinterpret_f64_f32(v1304));
    v5 += v11;
    v6 += v12;
  }
}
#endif
