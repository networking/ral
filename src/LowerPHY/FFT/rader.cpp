/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "rader.hpp"
#include "fft_execute.hpp"
#include "rader_generator.hpp"

#include <cmath>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

namespace armral::fft {

template<typename Tx, typename Ty, typename Tw>
std::optional<rader<Tx, Ty, Tw>> make_rader(int n, armral_fft_direction_t dir,
                                            int n_whole) {
  using real_t = armral::fft::real_t<Tw>;

  auto g = find_group_generator(n);
  if (!g) {
    return std::nullopt;
  }
  auto g_inv = find_inverse_mod_n(g, n);

  // try to plan recursive calls, but do not allow recursive use of Rader's
  // algorithm since that tends to be slower than just using Bluestein.
  armral_fft_plan_t *pf = nullptr;
  armral_fft_plan_t *pb = nullptr;
  // We get a performance benefit from using additional kernels provided the n
  // we are creating a Rader's plan for isn't the only factor of n_whole.
  bool use_all_kernels = n_whole > n;
  // Only allow uun kernels to be used if we know the plans will be executed
  // with howmany = 1. This will be the case if the level that the Rader's plan
  // is being created for has n2 = 1, i.e. if n = n_whole.
  bool want_uun = n == n_whole;
  armral::fft::create_plan<Tw, Tw, Tw>(
      &pf, n - 1, armral_fft_direction_t::ARMRAL_FFT_FORWARDS, false,
      use_all_kernels, want_uun);
  armral::fft::create_plan<Tw, Tw, Tw>(
      &pb, n - 1, armral_fft_direction_t::ARMRAL_FFT_BACKWARDS, false,
      use_all_kernels, want_uun);
  if (!pf || !pb) {
    if (pf) {
      armral::fft::destroy_plan(&pf);
    } else if (pb) {
      armral::fft::destroy_plan(&pb);
    }
    return std::nullopt;
  }

  // fill out permutation vectors to avoid needing to do expensive
  // modulus operations in the actual execute call.
  int *gmul_fw_perm = static_cast<int *>(malloc((n - 1) * sizeof(int)));
  int *gmul_bw_perm = static_cast<int *>(malloc((n - 1) * sizeof(int)));
  int *ginvmul_fw_perm = static_cast<int *>(malloc((n - 1) * sizeof(int)));
  int *ginvmul_bw_perm = static_cast<int *>(malloc((n - 1) * sizeof(int)));
  for (int j = 1, gmul = 1, ginvmul = 1; j < n; ++j) {
    gmul_fw_perm[j - 1] = gmul;
    ginvmul_fw_perm[j - 1] = ginvmul;
    gmul_bw_perm[gmul - 1] = j;
    ginvmul_bw_perm[ginvmul - 1] = j;
    gmul = (gmul * g) % n;
    ginvmul = (ginvmul * g_inv) % n;
  }

  // Populate the vector b to be used in the convolution
  Tw *b = static_cast<Tw *>(malloc((n - 1) * sizeof(Tw)));
  double dir_float =
      dir == armral_fft_direction_t::ARMRAL_FFT_FORWARDS ? -1.0 : 1.0;
  for (int i = 0; i < n - 1; i++) {
    double x = ginvmul_fw_perm[i];
    double in = ((2. * M_PI * x) / n) * dir_float;
    b[i] = Tw{(real_t)cos(in), (real_t)sin(in)};
  }
  armral::fft::execute<Tw, Tw, Tw>(pf, b, b, 1, 1, 1);

  // Multiply output from FFT of b with 1/n_pad
  real_t n1 = 1.0 / (n - 1);
  for (int i = 0; i < n - 1; i++) {
    b[i].re *= n1;
    b[i].im *= n1;
  }

  return rader<Tx, Ty, Tw>{n,
                           g,
                           g_inv,
                           pf,
                           pb,
                           b,
                           gmul_fw_perm,
                           gmul_bw_perm,
                           ginvmul_fw_perm,
                           ginvmul_bw_perm};
}

template std::optional<
    rader<armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t>>
make_rader(int n, armral_fft_direction_t dir, int n_whole);
template std::optional<
    rader<armral_cmplx_int16_t, armral_cmplx_f32_t, armral_cmplx_f32_t>>
make_rader(int n, armral_fft_direction_t dir, int n_whole);
template std::optional<
    rader<armral_cmplx_int16_t, armral_cmplx_int16_t, armral_cmplx_f32_t>>
make_rader(int n, armral_fft_direction_t dir, int n_whole);
template std::optional<
    rader<armral_cmplx_f32_t, armral_cmplx_int16_t, armral_cmplx_f32_t>>
make_rader(int n, armral_fft_direction_t dir, int n_whole);

template struct rader<armral_cmplx_f32_t, armral_cmplx_f32_t,
                      armral_cmplx_f32_t>;
template struct rader<armral_cmplx_int16_t, armral_cmplx_int16_t,
                      armral_cmplx_f32_t>;
template struct rader<armral_cmplx_int16_t, armral_cmplx_f32_t,
                      armral_cmplx_f32_t>;
template struct rader<armral_cmplx_f32_t, armral_cmplx_int16_t,
                      armral_cmplx_f32_t>;

template<typename Tx, typename Ty, typename Tw>
static inline void rader_init(const rader<Tx, Ty, Tw> &r, const Tx *x, Tw *x0,
                              Tw *y0, int istride, const Tw *w, int howmany,
                              int idist, Tw *work_ptr) {
  auto nm1 = r.n - 1;
  x0[0] = armral::fft::cast<Tw>(x[0]);
  y0[0] = x0[0];
  for (int j = 0; j < nm1; ++j) {
    auto xidx = r.gmul_fw_perm[j] * istride;
    auto xi = armral::fft::cast<Tw>(x[xidx]);
    y0[0].re += xi.re;
    y0[0].im += xi.im;
    work_ptr[j * howmany] = xi;
  }

  if (w) {
    for (int i = 1; i < howmany; ++i) {
      // calculate y0 (sum)
      x0[i] = armral::fft::cast<Tw>(x[i * idist]);
      y0[i] = x0[i];

      // permute x into work and finish calculating y0.
      for (int j = 0; j < nm1; ++j) {
        auto xidx = i * idist + r.gmul_fw_perm[j] * istride;
        auto xi = armral::fft::cast<Tw>(x[xidx]);
        Tw tmp = xi;
        auto idx = (nm1 * (i - 1) + (r.gmul_fw_perm[j] - 1)) * 2;
        tmp.re = w[idx].re * xi.re - w[idx].im * xi.im;
        tmp.im = w[idx].im * xi.re + w[idx].re * xi.im;
        y0[i].re += tmp.re;
        y0[i].im += tmp.im;
        work_ptr[i + j * howmany] = tmp;
      }
    }
  } else {
    for (int i = 1; i < howmany; ++i) {
      // calculate y0 (sum)
      x0[i] = armral::fft::cast<Tw>(x[i * idist]);
      y0[i] = x0[i];

      // permute x into work and finish calculating y0.
      for (int j = 0; j < nm1; ++j) {
        auto xidx = i * idist + r.gmul_fw_perm[j] * istride;
        auto xi = armral::fft::cast<Tw>(x[xidx]);
        y0[i].re += xi.re;
        y0[i].im += xi.im;
        work_ptr[i + j * howmany] = xi;
      }
    }
  }
}

template<typename Tx, typename Ty, typename Tw>
void execute_rader(const rader<Tx, Ty, Tw> &r, const Tx *x, Ty *y, int istride,
                   int ostride, const Tw *w, int howmany, int idist,
                   int odist) {
  static_assert(std::is_same_v<Tw, armral_cmplx_f32_t>);
  static_assert(armral::fft::is_armral_cmplx_v<Tx>);
  static_assert(armral::fft::is_armral_cmplx_v<Ty>);
  // TODO: Use a statically allocated array rather than calling
  // malloc every time
  auto nm1 = r.n - 1;
  auto *work_ptr = static_cast<Tw *>(malloc(howmany * nm1 * sizeof(Tw)));
  auto *x0 = static_cast<Tw *>(malloc(howmany * sizeof(Tw)));
  auto *y0 = static_cast<Tw *>(malloc(howmany * sizeof(Tw)));

  rader_init(r, x, x0, y0, istride, w, howmany, idist, work_ptr);

  // do forwards FFT!
#ifndef __clang__
// GCC is having an issue seeing that work_ptr is initialized. We know that it
// is, and if we call calloc instead of malloc to get rid of the warning, the
// code runs significantly slower for smaller sizes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
  armral::fft::execute<Tw, Tw, Tw>(r.pf, work_ptr, work_ptr, howmany, howmany,
                                   howmany);
#ifndef __clang__
#pragma GCC diagnostic pop
#endif

  // do pointwise multiplication by b
  for (int i = 0; i < howmany; ++i) {
    for (int j = 0; j < nm1; ++j) {
      Tw tmp = {work_ptr[i + j * howmany].re * r.b[j].re -
                    work_ptr[i + j * howmany].im * r.b[j].im,
                work_ptr[i + j * howmany].im * r.b[j].re +
                    work_ptr[i + j * howmany].re * r.b[j].im};
      work_ptr[i + j * howmany] = tmp;
    }
  }

  // do backwards FFT!
#ifndef __clang__
// GCC is having an issue seeing that work_ptr is initialized. We know that it
// is, and if we call calloc instead of malloc to get rid of the warning, the
// code runs significantly slower for smaller sizes.
#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wmaybe-uninitialized"
#endif
  armral::fft::execute<Tw, Tw, Tw>(r.pb, work_ptr, work_ptr, howmany, howmany,
                                   howmany);
#ifndef __clang__
#pragma GCC diagnostic pop
#endif

  // permute work into y, add x0
  for (int i = 0; i < howmany; ++i) {
    y[i * odist] = armral::fft::cast<Ty>(y0[i]);
    for (int j = 0; j < nm1; ++j) {
      auto yelem = Tw{work_ptr[i + j * howmany].re + x0[i].re,
                      work_ptr[i + j * howmany].im + x0[i].im};
      y[i * odist + r.ginvmul_fw_perm[j] * ostride] =
          armral::fft::cast<Ty>(yelem);
    }
  }
  free(work_ptr);
  free(x0);
  free(y0);
}

template void
execute_rader<armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t>(
    const rader<armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t> &r,
    const armral_cmplx_f32_t *x, armral_cmplx_f32_t *y, int istride,
    int ostride, const armral_cmplx_f32_t *W, int howmany, int idist,
    int odist);

template void
execute_rader<armral_cmplx_int16_t, armral_cmplx_int16_t, armral_cmplx_f32_t>(
    const rader<armral_cmplx_int16_t, armral_cmplx_int16_t, armral_cmplx_f32_t>
        &r,
    const armral_cmplx_int16_t *x, armral_cmplx_int16_t *y, int istride,
    int ostride, const armral_cmplx_f32_t *W, int howmany, int idist,
    int odist);

template void
execute_rader<armral_cmplx_f32_t, armral_cmplx_int16_t, armral_cmplx_f32_t>(
    const rader<armral_cmplx_f32_t, armral_cmplx_int16_t, armral_cmplx_f32_t>
        &r,
    const armral_cmplx_f32_t *x, armral_cmplx_int16_t *y, int istride,
    int ostride, const armral_cmplx_f32_t *w, int howmany, int idist,
    int odist);

template void
execute_rader<armral_cmplx_int16_t, armral_cmplx_f32_t, armral_cmplx_f32_t>(
    const rader<armral_cmplx_int16_t, armral_cmplx_f32_t, armral_cmplx_f32_t>
        &r,
    const armral_cmplx_int16_t *x, armral_cmplx_f32_t *y, int istride,
    int ostride, const armral_cmplx_f32_t *w, int howmany, int idist,
    int odist);

template<typename Tx, typename Ty, typename Tw>
rader<Tx, Ty, Tw>::~rader() {
  if (pf != nullptr) {
    armral::fft::destroy_plan(&pf);
    free(pf);
    pf = nullptr;
  }
  if (pb != nullptr) {
    armral::fft::destroy_plan(&pb);
    free(pb);
    pb = nullptr;
  }
  if (b) {
    free(const_cast<Tw *>(b));
    b = nullptr;
  }
  if (gmul_fw_perm != nullptr) {
    free(const_cast<int *>(gmul_fw_perm));
    gmul_fw_perm = nullptr;
  }
  if (gmul_bw_perm != nullptr) {
    free(const_cast<int *>(gmul_bw_perm));
    gmul_bw_perm = nullptr;
  }
  if (ginvmul_fw_perm != nullptr) {
    free(const_cast<int *>(ginvmul_fw_perm));
    ginvmul_fw_perm = nullptr;
  }
  if (ginvmul_bw_perm != nullptr) {
    free(const_cast<int *>(ginvmul_bw_perm));
    ginvmul_bw_perm = nullptr;
  }
}

template rader<armral_cmplx_f32_t, armral_cmplx_f32_t,
               armral_cmplx_f32_t>::~rader();
template rader<armral_cmplx_f32_t, armral_cmplx_int16_t,
               armral_cmplx_f32_t>::~rader();
template rader<armral_cmplx_int16_t, armral_cmplx_int16_t,
               armral_cmplx_f32_t>::~rader();
template rader<armral_cmplx_int16_t, armral_cmplx_f32_t,
               armral_cmplx_f32_t>::~rader();

} // namespace armral::fft
