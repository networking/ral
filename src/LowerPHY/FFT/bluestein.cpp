/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2024-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "bluestein.hpp"
#include "fft_execute.hpp"

#include <cmath>

namespace armral::fft {

template<typename Tx, typename Ty, typename Tw>
std::optional<bluestein<Tx, Ty, Tw>>
make_bluestein(int n, armral_fft_direction_t dir, const int *base_kernels,
               int len_base_kernels) {
  using real_t = armral::fft::real_t<Tw>;

  // Look for the next size > 2n-1 which would allow us to use fast kernels
  // alone
  int n_pad = 2 * n - 1;
  n_pad--;
  int curn_n = 0;
  do {
    curn_n = ++n_pad;
    for (int i = 0; i < len_base_kernels; i++) {
      if (!base_kernels[i]) {
        continue;
      }
      while (curn_n % i == 0) {
        curn_n /= i;
      }
    }
  } while (curn_n != 1);

  Tw *a = static_cast<Tw *>(malloc(n_pad * sizeof(Tw)));
  Tw *b = static_cast<Tw *>(calloc(n_pad, sizeof(Tw)));

  // Populate the two vectors to be used in the convolution, a and b.
  for (int i = 0; i < n; i++) {
    auto c = (real_t)cos(M_PI * (real_t)i * (real_t)i / (real_t)n);
    auto s = (real_t)sin(M_PI * (real_t)i * (real_t)i / (real_t)n);
    a[i] = {c, (real_t)-s}; // { cos(-pi*i*i/n), sin(-pi*i*i/n) }
    b[i] = {c, s};          // { cos( pi*i*i/n), sin( pi*i*i/n) }
  }

  // Make sure b is periodic
  int j = n_pad - n + 1;
  for (int i = n - 1; i > 0; i--) {
    b[j++] = b[i];
  }

  // Create 2 plans: forward and backward
  armral_fft_plan_t *pf = nullptr;
  armral_fft_plan_t *pb = nullptr;
  auto pf_status = armral::fft::create_plan<Tw, Tw, Tw>(
      &pf, n_pad, armral_fft_direction_t::ARMRAL_FFT_FORWARDS, false);
  auto pb_status = armral::fft::create_plan<Tw, Tw, Tw>(
      &pb, n_pad, armral_fft_direction_t::ARMRAL_FFT_BACKWARDS, false);

  if (pf_status == ARMRAL_ARGUMENT_ERROR ||
      pb_status == ARMRAL_ARGUMENT_ERROR) {
    if (pf) {
      armral::fft::destroy_plan(&pf);
    } else if (pb) {
      armral::fft::destroy_plan(&pb);
    }
    return std::nullopt;
  }

  // Execute fwds plan transforming series b
  armral::fft::execute<Tw, Tw, Tw>(pf, b, b, 1, 1, 1);

  // Multiply output from FFT of b with 1/n_pad
  real_t recip_npad = 1.0 / n_pad;
  for (int i = 0; i < n_pad; i++) {
    b[i] = {b[i].re * recip_npad, b[i].im * recip_npad};
  }

  return bluestein<Tx, Ty, Tw>{n, n_pad, dir, pf, pb, a, b};
}

template std::optional<
    bluestein<armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t>>
make_bluestein(int n, armral_fft_direction_t dir, const int *base_kernels,
               int len_base_kernels);
template std::optional<
    bluestein<armral_cmplx_int16_t, armral_cmplx_f32_t, armral_cmplx_f32_t>>
make_bluestein(int n, armral_fft_direction_t dir, const int *base_kernels,
               int len_base_kernels);
template std::optional<
    bluestein<armral_cmplx_int16_t, armral_cmplx_int16_t, armral_cmplx_f32_t>>
make_bluestein(int n, armral_fft_direction_t dir, const int *base_kernels,
               int len_base_kernels);
template std::optional<
    bluestein<armral_cmplx_f32_t, armral_cmplx_int16_t, armral_cmplx_f32_t>>
make_bluestein(int n, armral_fft_direction_t dir, const int *base_kernels,
               int len_base_kernels);

template struct bluestein<armral_cmplx_f32_t, armral_cmplx_f32_t,
                          armral_cmplx_f32_t>;
template struct bluestein<armral_cmplx_int16_t, armral_cmplx_int16_t,
                          armral_cmplx_f32_t>;
template struct bluestein<armral_cmplx_f32_t, armral_cmplx_int16_t,
                          armral_cmplx_f32_t>;
template struct bluestein<armral_cmplx_int16_t, armral_cmplx_f32_t,
                          armral_cmplx_f32_t>;

template<typename Tx, typename Tw>
static inline void multiply_a_x(Tw *work_ptr, const Tx *x, const Tw *a, int n,
                                int n_pad, int istride) {
  for (int i = 0; i < n; i++) {
    Tw xi = armral::fft::cast<Tw>(x[i * istride]);
    work_ptr[i].re = a[i].re * xi.re - a[i].im * xi.im;
    work_ptr[i].im = a[i].re * xi.im + a[i].im * xi.re;
  }
  for (int i = n; i < n_pad; i++) {
    work_ptr[i] = {0, 0};
  }
}

template<typename Tx, typename Tw>
static inline void multiply_a_x_dit(Tw *work_ptr, const Tx *x, const Tw *a,
                                    int n, int n_pad, int istride,
                                    const Tw *w) {
  Tw xi = armral::fft::cast<Tw>(x[0]);
  work_ptr[0].re = a[0].re * xi.re - a[0].im * xi.im;
  work_ptr[0].im = a[0].re * xi.im + a[0].im * xi.re;
  for (int i = 1; i < n; i++) {
    xi = armral::fft::cast<Tw>(x[i * istride]);
    Tw wi = w[(i - 1) * 2];
    Tw tmp = {xi.re * wi.re - xi.im * wi.im, xi.re * wi.im + xi.im * wi.re};
    work_ptr[i] = {a[i].re * tmp.re - a[i].im * tmp.im,
                   a[i].re * tmp.im + a[i].im * tmp.re};
  }
  for (int i = n; i < n_pad; i++) {
    work_ptr[i] = {0, 0};
  }
}

template<typename Ty, typename Tw>
static inline void multiply_y_a(const Tw *work_ptr, Ty *y, const Tw *a, int n,
                                int ostride, armral_fft_direction_t dir) {
  Tw tmp = {work_ptr[0].re * a[0].re - work_ptr[0].im * a[0].im,
            work_ptr[0].re * a[0].im + work_ptr[0].im * a[0].re};
  y[0] = armral::fft::cast<Ty>(tmp);
  if (dir == armral_fft_direction_t::ARMRAL_FFT_BACKWARDS) {
    y = y + n * ostride;
    ostride = -ostride;
  }
  for (int i = 1; i < n; i++) {
    tmp.re = work_ptr[i].re * a[i].re - work_ptr[i].im * a[i].im;
    tmp.im = work_ptr[i].re * a[i].im + work_ptr[i].im * a[i].re;
    y[i * ostride] = armral::fft::cast<Ty>(tmp);
  }
}

template<typename Tx, typename Ty, typename Tw>
void execute_bluestein(const bluestein<Tx, Ty, Tw> &bs, const Tx *x, Ty *y,
                       int istride, int ostride, const Tw *w, int howmany,
                       int idist, int odist) {

  Tw *work_ptr = static_cast<Tw *>(malloc(bs.n_pad * sizeof(Tw)));

  for (int i = 0; i < howmany; i++) {
    // Multiply input by a and store in work
    if (w != NULL && i > 0) {
      multiply_a_x_dit(work_ptr, &x[i * idist], bs.a, bs.n, bs.n_pad, istride,
                       &w[2 * (bs.n - 1) * (i - 1)]);
    } else {
      multiply_a_x(work_ptr, &x[i * idist], bs.a, bs.n, bs.n_pad, istride);
    }

    armral::fft::execute<Tw, Tw, Tw>(bs.pf, work_ptr, work_ptr, 1, 1, 1);
    for (int j = 0; j < bs.n_pad; j++) {
      Tw tmp = {work_ptr[j].re * bs.b[j].re - work_ptr[j].im * bs.b[j].im,
                work_ptr[j].re * bs.b[j].im + work_ptr[j].im * bs.b[j].re};
      work_ptr[j] = tmp;
    }

    armral::fft::execute<Tw, Tw, Tw>(bs.pb, work_ptr, work_ptr, 1, 1, 1);

    // Multiply by a and store in output vector y
    multiply_y_a(work_ptr, &y[i * odist], bs.a, bs.n, ostride, bs.dir);
  }
  free(work_ptr);
}

template void
execute_bluestein<armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t>(
    const bluestein<armral_cmplx_f32_t, armral_cmplx_f32_t, armral_cmplx_f32_t>
        &bs,
    const armral_cmplx_f32_t *x, armral_cmplx_f32_t *y, int istride,
    int ostride, const armral_cmplx_f32_t *w, int howmany, int idist,
    int odist);
template void execute_bluestein<armral_cmplx_int16_t, armral_cmplx_int16_t,
                                armral_cmplx_f32_t>(
    const bluestein<armral_cmplx_int16_t, armral_cmplx_int16_t,
                    armral_cmplx_f32_t> &bs,
    const armral_cmplx_int16_t *x, armral_cmplx_int16_t *y, int istride,
    int ostride, const armral_cmplx_f32_t *w, int howmany, int idist,
    int odist);
template void
execute_bluestein<armral_cmplx_int16_t, armral_cmplx_f32_t, armral_cmplx_f32_t>(
    const bluestein<armral_cmplx_int16_t, armral_cmplx_f32_t,
                    armral_cmplx_f32_t> &bs,
    const armral_cmplx_int16_t *x, armral_cmplx_f32_t *y, int istride,
    int ostride, const armral_cmplx_f32_t *w, int howmany, int idist,
    int odist);
template void
execute_bluestein<armral_cmplx_f32_t, armral_cmplx_int16_t, armral_cmplx_f32_t>(
    const bluestein<armral_cmplx_f32_t, armral_cmplx_int16_t,
                    armral_cmplx_f32_t> &bs,
    const armral_cmplx_f32_t *x, armral_cmplx_int16_t *y, int istride,
    int ostride, const armral_cmplx_f32_t *w, int howmany, int idist,
    int odist);

template<typename Tx, typename Ty, typename Tw>
bluestein<Tx, Ty, Tw>::~bluestein() {
  if (pf != nullptr) {
    armral::fft::destroy_plan(&pf);
    free(pf);
    pf = nullptr;
  }
  if (pb != nullptr) {
    armral::fft::destroy_plan(&pb);
    free(pb);
    pb = nullptr;
  }
  if (a) {
    free(const_cast<Tw *>(a));
    a = nullptr;
  }
  if (b) {
    free(const_cast<Tw *>(b));
    b = nullptr;
  }
}

template bluestein<armral_cmplx_f32_t, armral_cmplx_f32_t,
                   armral_cmplx_f32_t>::~bluestein();
template bluestein<armral_cmplx_int16_t, armral_cmplx_int16_t,
                   armral_cmplx_f32_t>::~bluestein();
template bluestein<armral_cmplx_f32_t, armral_cmplx_int16_t,
                   armral_cmplx_f32_t>::~bluestein();
template bluestein<armral_cmplx_int16_t, armral_cmplx_f32_t,
                   armral_cmplx_f32_t>::~bluestein();

} // end namespace armral::fft
