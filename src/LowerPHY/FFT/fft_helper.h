/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include <arm_neon.h>

// Load 32 bits and zero extend
static inline int16x4_t vld1s_s16(const int *v) {
  int16x4_t r;
  asm("ldr %s[out], [%[addr]]" : [out] "=w"(r) : [addr] "r"((const void *)v));
  return r;
}
