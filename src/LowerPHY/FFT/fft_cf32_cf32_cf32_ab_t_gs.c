/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "fft_cf32_cf32_cf32_ab_t_gs.h"

#include <arm_neon.h>
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs2(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    float32x2_t v51 = v5[0];
    float32x2_t v38 = v7[j * 2];
    int64_t v42 = j * 2 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v52 = vadd_f32(v51, v46);
    float32x2_t v53 = vsub_f32(v51, v46);
    v6[0] = v52;
    v6[ostride] = v53;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs2(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    const float32x2_t *v78 = &v5[v0];
    float32x2_t *v111 = &v6[v2];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v13]));
    const float32x2_t *v90 = &v5[0];
    svint64_t v91 = svindex_s64(0, v1);
    float32x2_t *v102 = &v6[0];
    svint64_t v112 = svindex_s64(0, v3);
    svfloat32_t v80 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v78), v91));
    svfloat32_t v92 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v90), v91));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero38, v80, v37, 0), v80, v37, 90);
    svfloat32_t v46 = svadd_f32_x(svptrue_b32(), v92, v38);
    svfloat32_t v47 = svsub_f32_x(svptrue_b32(), v92, v38);
    svst1_scatter_s64index_f64(pred_full, (double *)(v102), v112,
                               svreinterpret_f64_f32(v46));
    svst1_scatter_s64index_f64(pred_full, (double *)(v111), v112,
                               svreinterpret_f64_f32(v47));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs3(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v91 = -1.4999999999999998e+00F;
    float v94 = 8.6602540378443871e-01F;
    float v95 = -8.6602540378443871e-01F;
    float32x2_t v97 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v84 = v5[0];
    float32x2_t v92 = (float32x2_t){v91, v91};
    float32x2_t v96 = (float32x2_t){v94, v95};
    float32x2_t v38 = v5[istride * 2];
    float32x2_t v56 = v7[j * 4];
    int64_t v60 = j * 4 + 1;
    int64_t v68 = 2 + j * 4;
    float32x2_t v98 = vmul_f32(v97, v96);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v78 = vadd_f32(v64, v77);
    float32x2_t v79 = vsub_f32(v64, v77);
    float32x2_t v85 = vadd_f32(v78, v84);
    float32x2_t v93 = vmul_f32(v78, v92);
    float32x2_t v99 = vrev64_f32(v79);
    float32x2_t v100 = vmul_f32(v99, v98);
    float32x2_t v101 = vadd_f32(v85, v93);
    v6[0] = v85;
    float32x2_t v102 = vadd_f32(v101, v100);
    float32x2_t v103 = vsub_f32(v101, v100);
    v6[ostride] = v103;
    v6[ostride * 2] = v102;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs3(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v76 = -1.4999999999999998e+00F;
    float v81 = -8.6602540378443871e-01F;
    const float32x2_t *v117 = &v5[v0];
    float32x2_t *v160 = &v6[v2];
    int64_t v33 = v0 * 2;
    int64_t v56 = v13 * 2;
    float v84 = v4 * v81;
    int64_t v105 = v2 * 2;
    const float32x2_t *v138 = &v5[0];
    svint64_t v139 = svindex_s64(0, v1);
    svfloat32_t v142 = svdup_n_f32(v76);
    float32x2_t *v151 = &v6[0];
    svint64_t v170 = svindex_s64(0, v3);
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v56]));
    int64_t v57 = v10 + v56;
    svfloat32_t v119 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v117), v139));
    const float32x2_t *v127 = &v5[v33];
    svfloat32_t v140 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v138), v139));
    svfloat32_t v143 = svdup_n_f32(v84);
    float32x2_t *v169 = &v6[v105];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v119, v51, 0),
                     v119, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v129 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v127), v139));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v129, v58, 0),
                     v129, v58, 90);
    svfloat32_t v60 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v61 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v69 = svadd_f32_x(svptrue_b32(), v60, v140);
    svfloat32_t zero86 = svdup_n_f32(0);
    svfloat32_t v86 = svcmla_f32_x(pred_full, zero86, v143, v61, 90);
    svfloat32_t v87 = svmla_f32_x(pred_full, v69, v60, v142);
    svst1_scatter_s64index_f64(pred_full, (double *)(v151), v170,
                               svreinterpret_f64_f32(v69));
    svfloat32_t v88 = svadd_f32_x(svptrue_b32(), v87, v86);
    svfloat32_t v89 = svsub_f32_x(svptrue_b32(), v87, v86);
    svst1_scatter_s64index_f64(pred_full, (double *)(v160), v170,
                               svreinterpret_f64_f32(v89));
    svst1_scatter_s64index_f64(pred_full, (double *)(v169), v170,
                               svreinterpret_f64_f32(v88));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs4(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v51 = v5[istride];
    float v132 = 1.0000000000000000e+00F;
    float v133 = -1.0000000000000000e+00F;
    float32x2_t v135 = (float32x2_t){v4, v4};
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    float32x2_t v113 = v5[0];
    float32x2_t v134 = (float32x2_t){v132, v133};
    float32x2_t v20 = v5[istride * 2];
    int64_t v37 = 2 + j * 6;
    float32x2_t v69 = v5[istride * 3];
    float32x2_t v87 = v7[j * 6];
    int64_t v91 = j * 6 + 1;
    int64_t v99 = 4 + j * 6;
    float32x2_t v136 = vmul_f32(v135, v134);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v114 = vadd_f32(v113, v46);
    float32x2_t v115 = vsub_f32(v113, v46);
    float32x2_t v116 = vadd_f32(v95, v108);
    float32x2_t v117 = vsub_f32(v95, v108);
    float32x2_t v118 = vadd_f32(v114, v116);
    float32x2_t v119 = vsub_f32(v114, v116);
    float32x2_t v137 = vrev64_f32(v117);
    float32x2_t v138 = vmul_f32(v137, v136);
    v6[0] = v118;
    v6[ostride * 2] = v119;
    float32x2_t v139 = vadd_f32(v115, v138);
    float32x2_t v140 = vsub_f32(v115, v138);
    v6[ostride] = v140;
    v6[ostride * 3] = v139;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs4(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v110 = -1.0000000000000000e+00F;
    const float32x2_t *v161 = &v5[v0];
    float32x2_t *v205 = &v6[v2];
    int64_t v19 = v0 * 2;
    int64_t v54 = v0 * 3;
    int64_t v76 = v10 * 2;
    int64_t v77 = v13 * 3;
    float v113 = v4 * v110;
    int64_t v133 = v2 * 2;
    int64_t v140 = v2 * 3;
    const float32x2_t *v182 = &v5[0];
    svint64_t v183 = svindex_s64(0, v1);
    float32x2_t *v196 = &v6[0];
    svint64_t v224 = svindex_s64(0, v3);
    int64_t v36 = v10 + v77;
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v77]));
    int64_t v78 = v76 + v77;
    const float32x2_t *v152 = &v5[v19];
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v161), v183));
    const float32x2_t *v171 = &v5[v54];
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v182), v183));
    svfloat32_t v188 = svdup_n_f32(v113);
    float32x2_t *v214 = &v6[v133];
    float32x2_t *v223 = &v6[v140];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v163, v72, 0),
                     v163, v72, 90);
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v154 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v152), v183));
    svfloat32_t v173 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v171), v183));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v154, v37, 0),
                     v154, v37, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v173, v79, 0),
                     v173, v79, 90);
    svfloat32_t v88 = svadd_f32_x(svptrue_b32(), v184, v38);
    svfloat32_t v89 = svsub_f32_x(svptrue_b32(), v184, v38);
    svfloat32_t v90 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v91 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v92 = svadd_f32_x(svptrue_b32(), v88, v90);
    svfloat32_t v93 = svsub_f32_x(svptrue_b32(), v88, v90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 = svcmla_f32_x(pred_full, zero115, v188, v91, 90);
    svfloat32_t v116 = svadd_f32_x(svptrue_b32(), v89, v115);
    svfloat32_t v117 = svsub_f32_x(svptrue_b32(), v89, v115);
    svst1_scatter_s64index_f64(pred_full, (double *)(v196), v224,
                               svreinterpret_f64_f32(v92));
    svst1_scatter_s64index_f64(pred_full, (double *)(v214), v224,
                               svreinterpret_f64_f32(v93));
    svst1_scatter_s64index_f64(pred_full, (double *)(v205), v224,
                               svreinterpret_f64_f32(v117));
    svst1_scatter_s64index_f64(pred_full, (double *)(v223), v224,
                               svreinterpret_f64_f32(v116));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs5(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v158 = -1.2500000000000000e+00F;
    float v162 = 5.5901699437494745e-01F;
    float v165 = 1.5388417685876268e+00F;
    float v166 = -1.5388417685876268e+00F;
    float v172 = 5.8778525229247325e-01F;
    float v173 = -5.8778525229247325e-01F;
    float v179 = 3.6327126400268028e-01F;
    float v180 = -3.6327126400268028e-01F;
    float32x2_t v182 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v151 = v5[0];
    float32x2_t v159 = (float32x2_t){v158, v158};
    float32x2_t v163 = (float32x2_t){v162, v162};
    float32x2_t v167 = (float32x2_t){v165, v166};
    float32x2_t v174 = (float32x2_t){v172, v173};
    float32x2_t v181 = (float32x2_t){v179, v180};
    float32x2_t v38 = v5[istride * 4];
    float32x2_t v56 = v7[j * 8];
    int64_t v60 = j * 8 + 1;
    int64_t v68 = 6 + j * 8;
    float32x2_t v82 = v5[istride * 3];
    float32x2_t v100 = v5[istride * 2];
    int64_t v117 = 4 + j * 8;
    int64_t v130 = 2 + j * 8;
    float32x2_t v169 = vmul_f32(v182, v167);
    float32x2_t v176 = vmul_f32(v182, v174);
    float32x2_t v183 = vmul_f32(v182, v181);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v140 = vadd_f32(v64, v77);
    float32x2_t v141 = vsub_f32(v64, v77);
    float32x2_t v142 = vadd_f32(v126, v139);
    float32x2_t v143 = vsub_f32(v126, v139);
    float32x2_t v144 = vadd_f32(v140, v142);
    float32x2_t v145 = vsub_f32(v140, v142);
    float32x2_t v146 = vadd_f32(v141, v143);
    float32x2_t v170 = vrev64_f32(v141);
    float32x2_t v184 = vrev64_f32(v143);
    float32x2_t v152 = vadd_f32(v144, v151);
    float32x2_t v160 = vmul_f32(v144, v159);
    float32x2_t v164 = vmul_f32(v145, v163);
    float32x2_t v171 = vmul_f32(v170, v169);
    float32x2_t v177 = vrev64_f32(v146);
    float32x2_t v185 = vmul_f32(v184, v183);
    float32x2_t v178 = vmul_f32(v177, v176);
    float32x2_t v186 = vadd_f32(v152, v160);
    v6[0] = v152;
    float32x2_t v187 = vadd_f32(v186, v164);
    float32x2_t v188 = vsub_f32(v186, v164);
    float32x2_t v189 = vsub_f32(v171, v178);
    float32x2_t v190 = vadd_f32(v178, v185);
    float32x2_t v191 = vadd_f32(v187, v189);
    float32x2_t v192 = vsub_f32(v187, v189);
    float32x2_t v193 = vadd_f32(v188, v190);
    float32x2_t v194 = vsub_f32(v188, v190);
    v6[ostride] = v192;
    v6[ostride * 2] = v194;
    v6[ostride * 3] = v193;
    v6[ostride * 4] = v191;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs5(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v123 = -1.2500000000000000e+00F;
    float v128 = 5.5901699437494745e-01F;
    float v133 = -1.5388417685876268e+00F;
    float v140 = -5.8778525229247325e-01F;
    float v147 = -3.6327126400268028e-01F;
    const float32x2_t *v203 = &v5[v0];
    float32x2_t *v267 = &v6[v2];
    int64_t v33 = v0 * 4;
    int64_t v55 = v10 * 3;
    int64_t v61 = v0 * 3;
    int64_t v75 = v0 * 2;
    int64_t v90 = v10 * 2;
    int64_t v98 = v13 * 4;
    float v136 = v4 * v133;
    float v143 = v4 * v140;
    float v150 = v4 * v147;
    int64_t v177 = v2 * 2;
    int64_t v184 = v2 * 3;
    int64_t v191 = v2 * 4;
    const float32x2_t *v242 = &v5[0];
    svint64_t v243 = svindex_s64(0, v1);
    svfloat32_t v246 = svdup_n_f32(v123);
    svfloat32_t v247 = svdup_n_f32(v128);
    float32x2_t *v258 = &v6[0];
    svint64_t v295 = svindex_s64(0, v3);
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v98]));
    int64_t v57 = v55 + v98;
    int64_t v92 = v90 + v98;
    int64_t v99 = v10 + v98;
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v203), v243));
    const float32x2_t *v213 = &v5[v33];
    const float32x2_t *v223 = &v5[v61];
    const float32x2_t *v232 = &v5[v75];
    svfloat32_t v244 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v242), v243));
    svfloat32_t v248 = svdup_n_f32(v136);
    svfloat32_t v249 = svdup_n_f32(v143);
    svfloat32_t v250 = svdup_n_f32(v150);
    float32x2_t *v276 = &v6[v177];
    float32x2_t *v285 = &v6[v184];
    float32x2_t *v294 = &v6[v191];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v205, v51, 0),
                     v205, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v215 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v213), v243));
    svfloat32_t v225 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v223), v243));
    svfloat32_t v234 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v232), v243));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v215, v58, 0),
                     v215, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v225, v93, 0),
                     v225, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v234, v100, 0),
                     v234, v100, 90);
    svfloat32_t v102 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v103 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v104 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v105 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v106 = svadd_f32_x(svptrue_b32(), v102, v104);
    svfloat32_t v107 = svsub_f32_x(svptrue_b32(), v102, v104);
    svfloat32_t v108 = svadd_f32_x(svptrue_b32(), v103, v105);
    svfloat32_t zero138 = svdup_n_f32(0);
    svfloat32_t v138 = svcmla_f32_x(pred_full, zero138, v248, v103, 90);
    svfloat32_t v116 = svadd_f32_x(svptrue_b32(), v106, v244);
    svfloat32_t zero145 = svdup_n_f32(0);
    svfloat32_t v145 = svcmla_f32_x(pred_full, zero145, v249, v108, 90);
    svfloat32_t v153 = svmla_f32_x(pred_full, v116, v106, v246);
    svfloat32_t v156 = svsub_f32_x(svptrue_b32(), v138, v145);
    svfloat32_t v157 = svcmla_f32_x(pred_full, v145, v250, v105, 90);
    svst1_scatter_s64index_f64(pred_full, (double *)(v258), v295,
                               svreinterpret_f64_f32(v116));
    svfloat32_t v154 = svmla_f32_x(pred_full, v153, v107, v247);
    svfloat32_t v155 = svmls_f32_x(pred_full, v153, v107, v247);
    svfloat32_t v158 = svadd_f32_x(svptrue_b32(), v154, v156);
    svfloat32_t v159 = svsub_f32_x(svptrue_b32(), v154, v156);
    svfloat32_t v160 = svadd_f32_x(svptrue_b32(), v155, v157);
    svfloat32_t v161 = svsub_f32_x(svptrue_b32(), v155, v157);
    svst1_scatter_s64index_f64(pred_full, (double *)(v267), v295,
                               svreinterpret_f64_f32(v159));
    svst1_scatter_s64index_f64(pred_full, (double *)(v276), v295,
                               svreinterpret_f64_f32(v161));
    svst1_scatter_s64index_f64(pred_full, (double *)(v285), v295,
                               svreinterpret_f64_f32(v160));
    svst1_scatter_s64index_f64(pred_full, (double *)(v294), v295,
                               svreinterpret_f64_f32(v158));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs6(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v131 = v5[istride];
    float v211 = -1.4999999999999998e+00F;
    float v214 = 8.6602540378443871e-01F;
    float v215 = -8.6602540378443871e-01F;
    float32x2_t v217 = (float32x2_t){v4, v4};
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    float32x2_t v175 = v5[0];
    float32x2_t v212 = (float32x2_t){v211, v211};
    float32x2_t v216 = (float32x2_t){v214, v215};
    float32x2_t v20 = v5[istride * 3];
    int64_t v37 = 4 + j * 10;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 5];
    int64_t v86 = 2 + j * 10;
    int64_t v99 = 8 + j * 10;
    float32x2_t v113 = v5[istride * 4];
    int64_t v148 = 6 + j * 10;
    float32x2_t v162 = v7[j * 10];
    int64_t v166 = j * 10 + 1;
    float32x2_t v218 = vmul_f32(v217, v216);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v176 = vadd_f32(v175, v46);
    float32x2_t v177 = vsub_f32(v175, v46);
    float32x2_t v178 = vadd_f32(v95, v108);
    float32x2_t v179 = vsub_f32(v95, v108);
    float32x2_t v180 = vadd_f32(v157, v170);
    float32x2_t v181 = vsub_f32(v157, v170);
    float32x2_t v182 = vadd_f32(v178, v180);
    float32x2_t v183 = vsub_f32(v178, v180);
    float32x2_t v203 = vadd_f32(v179, v181);
    float32x2_t v204 = vsub_f32(v179, v181);
    float32x2_t v184 = vadd_f32(v182, v176);
    float32x2_t v192 = vmul_f32(v182, v212);
    float32x2_t v198 = vrev64_f32(v183);
    float32x2_t v205 = vadd_f32(v203, v177);
    float32x2_t v213 = vmul_f32(v203, v212);
    float32x2_t v219 = vrev64_f32(v204);
    float32x2_t v199 = vmul_f32(v198, v218);
    float32x2_t v200 = vadd_f32(v184, v192);
    float32x2_t v220 = vmul_f32(v219, v218);
    float32x2_t v221 = vadd_f32(v205, v213);
    v6[0] = v184;
    v6[ostride * 3] = v205;
    float32x2_t v201 = vadd_f32(v200, v199);
    float32x2_t v202 = vsub_f32(v200, v199);
    float32x2_t v222 = vadd_f32(v221, v220);
    float32x2_t v223 = vsub_f32(v221, v220);
    v6[ostride * 4] = v202;
    v6[ostride] = v223;
    v6[ostride * 2] = v201;
    v6[ostride * 5] = v222;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs6(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v168 = -1.4999999999999998e+00F;
    float v173 = -8.6602540378443871e-01F;
    const float32x2_t *v266 = &v5[v0];
    float32x2_t *v321 = &v6[v2];
    int64_t v19 = v0 * 3;
    int64_t v34 = v10 * 2;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 5;
    int64_t v76 = v10 * 4;
    int64_t v82 = v0 * 4;
    int64_t v111 = v10 * 3;
    int64_t v119 = v13 * 5;
    float v176 = v4 * v173;
    int64_t v190 = v2 * 3;
    int64_t v197 = v2 * 4;
    int64_t v211 = v2 * 2;
    int64_t v218 = v2 * 5;
    const float32x2_t *v278 = &v5[0];
    svint64_t v279 = svindex_s64(0, v1);
    svfloat32_t v285 = svdup_n_f32(v168);
    float32x2_t *v294 = &v6[0];
    svint64_t v340 = svindex_s64(0, v3);
    int64_t v36 = v34 + v119;
    int64_t v71 = v10 + v119;
    int64_t v78 = v76 + v119;
    int64_t v113 = v111 + v119;
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v119]));
    const float32x2_t *v230 = &v5[v19];
    const float32x2_t *v239 = &v5[v40];
    const float32x2_t *v248 = &v5[v54];
    const float32x2_t *v257 = &v5[v82];
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v266), v279));
    svfloat32_t v280 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v278), v279));
    svfloat32_t v286 = svdup_n_f32(v176);
    float32x2_t *v303 = &v6[v190];
    float32x2_t *v312 = &v6[v197];
    float32x2_t *v330 = &v6[v211];
    float32x2_t *v339 = &v6[v218];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v268, v121, 0),
                     v268, v121, 90);
    svfloat32_t v232 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v230), v279));
    svfloat32_t v241 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v239), v279));
    svfloat32_t v250 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v248), v279));
    svfloat32_t v259 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v257), v279));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v232, v37, 0),
                     v232, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v241, v72, 0),
                     v241, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v250, v79, 0),
                     v250, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v259, v114, 0),
                     v259, v114, 90);
    svfloat32_t v130 = svadd_f32_x(svptrue_b32(), v280, v38);
    svfloat32_t v131 = svsub_f32_x(svptrue_b32(), v280, v38);
    svfloat32_t v132 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v133 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v134 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v135 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v136 = svadd_f32_x(svptrue_b32(), v132, v134);
    svfloat32_t v137 = svsub_f32_x(svptrue_b32(), v132, v134);
    svfloat32_t v159 = svadd_f32_x(svptrue_b32(), v133, v135);
    svfloat32_t v160 = svsub_f32_x(svptrue_b32(), v133, v135);
    svfloat32_t v138 = svadd_f32_x(svptrue_b32(), v136, v130);
    svfloat32_t zero155 = svdup_n_f32(0);
    svfloat32_t v155 = svcmla_f32_x(pred_full, zero155, v286, v137, 90);
    svfloat32_t v161 = svadd_f32_x(svptrue_b32(), v159, v131);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 = svcmla_f32_x(pred_full, zero178, v286, v160, 90);
    svfloat32_t v156 = svmla_f32_x(pred_full, v138, v136, v285);
    svfloat32_t v179 = svmla_f32_x(pred_full, v161, v159, v285);
    svst1_scatter_s64index_f64(pred_full, (double *)(v294), v340,
                               svreinterpret_f64_f32(v138));
    svst1_scatter_s64index_f64(pred_full, (double *)(v303), v340,
                               svreinterpret_f64_f32(v161));
    svfloat32_t v157 = svadd_f32_x(svptrue_b32(), v156, v155);
    svfloat32_t v158 = svsub_f32_x(svptrue_b32(), v156, v155);
    svfloat32_t v180 = svadd_f32_x(svptrue_b32(), v179, v178);
    svfloat32_t v181 = svsub_f32_x(svptrue_b32(), v179, v178);
    svst1_scatter_s64index_f64(pred_full, (double *)(v312), v340,
                               svreinterpret_f64_f32(v158));
    svst1_scatter_s64index_f64(pred_full, (double *)(v321), v340,
                               svreinterpret_f64_f32(v181));
    svst1_scatter_s64index_f64(pred_full, (double *)(v330), v340,
                               svreinterpret_f64_f32(v157));
    svst1_scatter_s64index_f64(pred_full, (double *)(v339), v340,
                               svreinterpret_f64_f32(v180));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs7(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v229 = -1.1666666666666665e+00F;
    float v233 = 7.9015646852540022e-01F;
    float v237 = 5.5854267289647742e-02F;
    float v241 = 7.3430220123575241e-01F;
    float v244 = 4.4095855184409838e-01F;
    float v245 = -4.4095855184409838e-01F;
    float v251 = 3.4087293062393137e-01F;
    float v252 = -3.4087293062393137e-01F;
    float v258 = -5.3396936033772524e-01F;
    float v259 = 5.3396936033772524e-01F;
    float v265 = 8.7484229096165667e-01F;
    float v266 = -8.7484229096165667e-01F;
    float32x2_t v268 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v214 = v5[0];
    float32x2_t v230 = (float32x2_t){v229, v229};
    float32x2_t v234 = (float32x2_t){v233, v233};
    float32x2_t v238 = (float32x2_t){v237, v237};
    float32x2_t v242 = (float32x2_t){v241, v241};
    float32x2_t v246 = (float32x2_t){v244, v245};
    float32x2_t v253 = (float32x2_t){v251, v252};
    float32x2_t v260 = (float32x2_t){v258, v259};
    float32x2_t v267 = (float32x2_t){v265, v266};
    float32x2_t v38 = v5[istride * 6];
    float32x2_t v56 = v7[j * 12];
    int64_t v60 = j * 12 + 1;
    int64_t v68 = 10 + j * 12;
    float32x2_t v82 = v5[istride * 4];
    float32x2_t v100 = v5[istride * 3];
    int64_t v117 = 6 + j * 12;
    int64_t v130 = 4 + j * 12;
    float32x2_t v144 = v5[istride * 2];
    float32x2_t v162 = v5[istride * 5];
    int64_t v179 = 2 + j * 12;
    int64_t v192 = 8 + j * 12;
    float32x2_t v248 = vmul_f32(v268, v246);
    float32x2_t v255 = vmul_f32(v268, v253);
    float32x2_t v262 = vmul_f32(v268, v260);
    float32x2_t v269 = vmul_f32(v268, v267);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v180 = v7[v179];
    float32x2_t v181 = vtrn1_f32(v144, v144);
    float32x2_t v182 = vtrn2_f32(v144, v144);
    int64_t v184 = v179 + 1;
    float32x2_t v193 = v7[v192];
    float32x2_t v194 = vtrn1_f32(v162, v162);
    float32x2_t v195 = vtrn2_f32(v162, v162);
    int64_t v197 = v192 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v185 = v7[v184];
    float32x2_t v186 = vmul_f32(v181, v180);
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vmul_f32(v194, v193);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v188 = vfma_f32(v186, v182, v185);
    float32x2_t v201 = vfma_f32(v199, v195, v198);
    float32x2_t v202 = vadd_f32(v64, v77);
    float32x2_t v203 = vsub_f32(v64, v77);
    float32x2_t v204 = vadd_f32(v126, v139);
    float32x2_t v205 = vsub_f32(v126, v139);
    float32x2_t v206 = vadd_f32(v188, v201);
    float32x2_t v207 = vsub_f32(v188, v201);
    float32x2_t v208 = vadd_f32(v202, v204);
    float32x2_t v216 = vsub_f32(v202, v204);
    float32x2_t v217 = vsub_f32(v204, v206);
    float32x2_t v218 = vsub_f32(v206, v202);
    float32x2_t v219 = vadd_f32(v203, v205);
    float32x2_t v221 = vsub_f32(v203, v205);
    float32x2_t v222 = vsub_f32(v205, v207);
    float32x2_t v223 = vsub_f32(v207, v203);
    float32x2_t v209 = vadd_f32(v208, v206);
    float32x2_t v220 = vadd_f32(v219, v207);
    float32x2_t v235 = vmul_f32(v216, v234);
    float32x2_t v239 = vmul_f32(v217, v238);
    float32x2_t v243 = vmul_f32(v218, v242);
    float32x2_t v256 = vrev64_f32(v221);
    float32x2_t v263 = vrev64_f32(v222);
    float32x2_t v270 = vrev64_f32(v223);
    float32x2_t v215 = vadd_f32(v209, v214);
    float32x2_t v231 = vmul_f32(v209, v230);
    float32x2_t v249 = vrev64_f32(v220);
    float32x2_t v257 = vmul_f32(v256, v255);
    float32x2_t v264 = vmul_f32(v263, v262);
    float32x2_t v271 = vmul_f32(v270, v269);
    float32x2_t v250 = vmul_f32(v249, v248);
    float32x2_t v272 = vadd_f32(v215, v231);
    v6[0] = v215;
    float32x2_t v273 = vadd_f32(v272, v235);
    float32x2_t v275 = vsub_f32(v272, v235);
    float32x2_t v277 = vsub_f32(v272, v239);
    float32x2_t v279 = vadd_f32(v250, v257);
    float32x2_t v281 = vsub_f32(v250, v257);
    float32x2_t v283 = vsub_f32(v250, v264);
    float32x2_t v274 = vadd_f32(v273, v239);
    float32x2_t v276 = vsub_f32(v275, v243);
    float32x2_t v278 = vadd_f32(v277, v243);
    float32x2_t v280 = vadd_f32(v279, v264);
    float32x2_t v282 = vsub_f32(v281, v271);
    float32x2_t v284 = vadd_f32(v283, v271);
    float32x2_t v285 = vadd_f32(v274, v280);
    float32x2_t v286 = vsub_f32(v274, v280);
    float32x2_t v287 = vadd_f32(v276, v282);
    float32x2_t v288 = vsub_f32(v276, v282);
    float32x2_t v289 = vadd_f32(v278, v284);
    float32x2_t v290 = vsub_f32(v278, v284);
    v6[ostride] = v286;
    v6[ostride * 2] = v288;
    v6[ostride * 3] = v289;
    v6[ostride * 4] = v290;
    v6[ostride * 5] = v287;
    v6[ostride * 6] = v285;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs7(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v174 = -1.1666666666666665e+00F;
    float v179 = 7.9015646852540022e-01F;
    float v184 = 5.5854267289647742e-02F;
    float v189 = 7.3430220123575241e-01F;
    float v194 = -4.4095855184409838e-01F;
    float v201 = -3.4087293062393137e-01F;
    float v208 = 5.3396936033772524e-01F;
    float v215 = -8.7484229096165667e-01F;
    const float32x2_t *v295 = &v5[v0];
    float32x2_t *v380 = &v6[v2];
    int64_t v33 = v0 * 6;
    int64_t v55 = v10 * 5;
    int64_t v61 = v0 * 4;
    int64_t v75 = v0 * 3;
    int64_t v90 = v10 * 3;
    int64_t v97 = v10 * 2;
    int64_t v103 = v0 * 2;
    int64_t v117 = v0 * 5;
    int64_t v139 = v10 * 4;
    int64_t v140 = v13 * 6;
    float v197 = v4 * v194;
    float v204 = v4 * v201;
    float v211 = v4 * v208;
    float v218 = v4 * v215;
    int64_t v255 = v2 * 2;
    int64_t v262 = v2 * 3;
    int64_t v269 = v2 * 4;
    int64_t v276 = v2 * 5;
    int64_t v283 = v2 * 6;
    const float32x2_t *v352 = &v5[0];
    svint64_t v353 = svindex_s64(0, v1);
    svfloat32_t v356 = svdup_n_f32(v174);
    svfloat32_t v357 = svdup_n_f32(v179);
    svfloat32_t v358 = svdup_n_f32(v184);
    svfloat32_t v359 = svdup_n_f32(v189);
    float32x2_t *v371 = &v6[0];
    svint64_t v426 = svindex_s64(0, v3);
    svfloat32_t v51 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v140]));
    int64_t v57 = v55 + v140;
    int64_t v92 = v90 + v140;
    int64_t v99 = v97 + v140;
    int64_t v134 = v10 + v140;
    int64_t v141 = v139 + v140;
    svfloat32_t v297 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v295), v353));
    const float32x2_t *v305 = &v5[v33];
    const float32x2_t *v315 = &v5[v61];
    const float32x2_t *v324 = &v5[v75];
    const float32x2_t *v333 = &v5[v103];
    const float32x2_t *v342 = &v5[v117];
    svfloat32_t v354 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v352), v353));
    svfloat32_t v360 = svdup_n_f32(v197);
    svfloat32_t v361 = svdup_n_f32(v204);
    svfloat32_t v362 = svdup_n_f32(v211);
    svfloat32_t v363 = svdup_n_f32(v218);
    float32x2_t *v389 = &v6[v255];
    float32x2_t *v398 = &v6[v262];
    float32x2_t *v407 = &v6[v269];
    float32x2_t *v416 = &v6[v276];
    float32x2_t *v425 = &v6[v283];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v297, v51, 0),
                     v297, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v135 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v134]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v307 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v305), v353));
    svfloat32_t v317 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v315), v353));
    svfloat32_t v326 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v324), v353));
    svfloat32_t v335 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v333), v353));
    svfloat32_t v344 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v342), v353));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v307, v58, 0),
                     v307, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v317, v93, 0),
                     v317, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v326, v100, 0),
                     v326, v100, 90);
    svfloat32_t zero136 = svdup_n_f32(0);
    svfloat32_t v136 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero136, v335, v135, 0),
                     v335, v135, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero143, v344, v142, 0),
                     v344, v142, 90);
    svfloat32_t v144 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v145 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v146 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v147 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v148 = svadd_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v149 = svsub_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v150 = svadd_f32_x(svptrue_b32(), v144, v146);
    svfloat32_t v160 = svsub_f32_x(svptrue_b32(), v144, v146);
    svfloat32_t v161 = svsub_f32_x(svptrue_b32(), v146, v148);
    svfloat32_t v162 = svsub_f32_x(svptrue_b32(), v148, v144);
    svfloat32_t v163 = svadd_f32_x(svptrue_b32(), v145, v147);
    svfloat32_t v165 = svsub_f32_x(svptrue_b32(), v145, v147);
    svfloat32_t v166 = svsub_f32_x(svptrue_b32(), v147, v149);
    svfloat32_t v167 = svsub_f32_x(svptrue_b32(), v149, v145);
    svfloat32_t v151 = svadd_f32_x(svptrue_b32(), v150, v148);
    svfloat32_t v164 = svadd_f32_x(svptrue_b32(), v163, v149);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 = svcmla_f32_x(pred_full, zero206, v361, v165, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 = svcmla_f32_x(pred_full, zero213, v362, v166, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 = svcmla_f32_x(pred_full, zero220, v363, v167, 90);
    svfloat32_t v159 = svadd_f32_x(svptrue_b32(), v151, v354);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 = svcmla_f32_x(pred_full, zero199, v360, v164, 90);
    svfloat32_t v221 = svmla_f32_x(pred_full, v159, v151, v356);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v230 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v232 = svsub_f32_x(svptrue_b32(), v199, v213);
    svst1_scatter_s64index_f64(pred_full, (double *)(v371), v426,
                               svreinterpret_f64_f32(v159));
    svfloat32_t v222 = svmla_f32_x(pred_full, v221, v160, v357);
    svfloat32_t v224 = svmls_f32_x(pred_full, v221, v160, v357);
    svfloat32_t v226 = svmls_f32_x(pred_full, v221, v161, v358);
    svfloat32_t v229 = svadd_f32_x(svptrue_b32(), v228, v213);
    svfloat32_t v231 = svsub_f32_x(svptrue_b32(), v230, v220);
    svfloat32_t v233 = svadd_f32_x(svptrue_b32(), v232, v220);
    svfloat32_t v223 = svmla_f32_x(pred_full, v222, v161, v358);
    svfloat32_t v225 = svmls_f32_x(pred_full, v224, v162, v359);
    svfloat32_t v227 = svmla_f32_x(pred_full, v226, v162, v359);
    svfloat32_t v234 = svadd_f32_x(svptrue_b32(), v223, v229);
    svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v223, v229);
    svfloat32_t v236 = svadd_f32_x(svptrue_b32(), v225, v231);
    svfloat32_t v237 = svsub_f32_x(svptrue_b32(), v225, v231);
    svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v227, v233);
    svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v227, v233);
    svst1_scatter_s64index_f64(pred_full, (double *)(v380), v426,
                               svreinterpret_f64_f32(v235));
    svst1_scatter_s64index_f64(pred_full, (double *)(v389), v426,
                               svreinterpret_f64_f32(v237));
    svst1_scatter_s64index_f64(pred_full, (double *)(v398), v426,
                               svreinterpret_f64_f32(v238));
    svst1_scatter_s64index_f64(pred_full, (double *)(v407), v426,
                               svreinterpret_f64_f32(v239));
    svst1_scatter_s64index_f64(pred_full, (double *)(v416), v426,
                               svreinterpret_f64_f32(v236));
    svst1_scatter_s64index_f64(pred_full, (double *)(v425), v426,
                               svreinterpret_f64_f32(v234));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs8(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v113 = v5[istride];
    float v277 = 1.0000000000000000e+00F;
    float v278 = -1.0000000000000000e+00F;
    float v285 = -7.0710678118654746e-01F;
    float32x2_t v287 = (float32x2_t){v4, v4};
    float v292 = 7.0710678118654757e-01F;
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    float32x2_t v237 = v5[0];
    float32x2_t v279 = (float32x2_t){v277, v278};
    float32x2_t v286 = (float32x2_t){v292, v285};
    float32x2_t v293 = (float32x2_t){v292, v292};
    float32x2_t v20 = v5[istride * 4];
    int64_t v37 = 6 + j * 14;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 6];
    int64_t v86 = 2 + j * 14;
    int64_t v99 = 10 + j * 14;
    float32x2_t v131 = v5[istride * 5];
    float32x2_t v149 = v7[j * 14];
    int64_t v153 = j * 14 + 1;
    int64_t v161 = 8 + j * 14;
    float32x2_t v175 = v5[istride * 3];
    float32x2_t v193 = v5[istride * 7];
    int64_t v210 = 4 + j * 14;
    int64_t v223 = 12 + j * 14;
    float32x2_t v281 = vmul_f32(v287, v279);
    float32x2_t v288 = vmul_f32(v287, v286);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v238 = vadd_f32(v237, v46);
    float32x2_t v239 = vsub_f32(v237, v46);
    float32x2_t v240 = vadd_f32(v95, v108);
    float32x2_t v241 = vsub_f32(v95, v108);
    float32x2_t v242 = vadd_f32(v157, v170);
    float32x2_t v243 = vsub_f32(v157, v170);
    float32x2_t v244 = vadd_f32(v219, v232);
    float32x2_t v245 = vsub_f32(v219, v232);
    float32x2_t v246 = vadd_f32(v238, v240);
    float32x2_t v247 = vsub_f32(v238, v240);
    float32x2_t v248 = vadd_f32(v242, v244);
    float32x2_t v249 = vsub_f32(v242, v244);
    float32x2_t v252 = vadd_f32(v243, v245);
    float32x2_t v253 = vsub_f32(v243, v245);
    float32x2_t v282 = vrev64_f32(v241);
    float32x2_t v250 = vadd_f32(v246, v248);
    float32x2_t v251 = vsub_f32(v246, v248);
    float32x2_t v271 = vrev64_f32(v249);
    float32x2_t v283 = vmul_f32(v282, v281);
    float32x2_t v289 = vrev64_f32(v252);
    float32x2_t v294 = vmul_f32(v253, v293);
    float32x2_t v272 = vmul_f32(v271, v281);
    float32x2_t v290 = vmul_f32(v289, v288);
    float32x2_t v297 = vadd_f32(v239, v294);
    float32x2_t v298 = vsub_f32(v239, v294);
    v6[0] = v250;
    v6[ostride * 4] = v251;
    float32x2_t v295 = vadd_f32(v247, v272);
    float32x2_t v296 = vsub_f32(v247, v272);
    float32x2_t v299 = vadd_f32(v283, v290);
    float32x2_t v300 = vsub_f32(v283, v290);
    float32x2_t v301 = vadd_f32(v297, v299);
    float32x2_t v302 = vsub_f32(v297, v299);
    float32x2_t v303 = vadd_f32(v298, v300);
    float32x2_t v304 = vsub_f32(v298, v300);
    v6[ostride * 2] = v296;
    v6[ostride * 6] = v295;
    v6[ostride] = v302;
    v6[ostride * 3] = v303;
    v6[ostride * 5] = v304;
    v6[ostride * 7] = v301;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs8(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v216 = -1.0000000000000000e+00F;
    float v223 = -7.0710678118654746e-01F;
    float v230 = 7.0710678118654757e-01F;
    const float32x2_t *v333 = &v5[v0];
    float32x2_t *v399 = &v6[v2];
    int64_t v19 = v0 * 4;
    int64_t v34 = v10 * 3;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 6;
    int64_t v76 = v10 * 5;
    int64_t v96 = v0 * 5;
    int64_t v118 = v10 * 4;
    int64_t v124 = v0 * 3;
    int64_t v138 = v0 * 7;
    int64_t v153 = v10 * 2;
    int64_t v160 = v10 * 6;
    int64_t v161 = v13 * 7;
    float v219 = v4 * v216;
    float v226 = v4 * v223;
    int64_t v259 = v2 * 2;
    int64_t v266 = v2 * 3;
    int64_t v273 = v2 * 4;
    int64_t v280 = v2 * 5;
    int64_t v287 = v2 * 6;
    int64_t v294 = v2 * 7;
    const float32x2_t *v372 = &v5[0];
    svint64_t v373 = svindex_s64(0, v1);
    svfloat32_t v382 = svdup_n_f32(v230);
    float32x2_t *v390 = &v6[0];
    svint64_t v454 = svindex_s64(0, v3);
    int64_t v36 = v34 + v161;
    int64_t v71 = v10 + v161;
    int64_t v78 = v76 + v161;
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v161]));
    int64_t v120 = v118 + v161;
    int64_t v155 = v153 + v161;
    int64_t v162 = v160 + v161;
    const float32x2_t *v306 = &v5[v19];
    const float32x2_t *v315 = &v5[v40];
    const float32x2_t *v324 = &v5[v54];
    svfloat32_t v335 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v333), v373));
    const float32x2_t *v343 = &v5[v96];
    const float32x2_t *v353 = &v5[v124];
    const float32x2_t *v362 = &v5[v138];
    svfloat32_t v374 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v372), v373));
    svfloat32_t v380 = svdup_n_f32(v219);
    svfloat32_t v381 = svdup_n_f32(v226);
    float32x2_t *v408 = &v6[v259];
    float32x2_t *v417 = &v6[v266];
    float32x2_t *v426 = &v6[v273];
    float32x2_t *v435 = &v6[v280];
    float32x2_t *v444 = &v6[v287];
    float32x2_t *v453 = &v6[v294];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v335, v114, 0),
                     v335, v114, 90);
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t v308 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v306), v373));
    svfloat32_t v317 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v315), v373));
    svfloat32_t v326 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v324), v373));
    svfloat32_t v345 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v343), v373));
    svfloat32_t v355 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v353), v373));
    svfloat32_t v364 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v362), v373));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v308, v37, 0),
                     v308, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v317, v72, 0),
                     v317, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v326, v79, 0),
                     v326, v79, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v345, v121, 0),
                     v345, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v355, v156, 0),
                     v355, v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v364, v163, 0),
                     v364, v163, 90);
    svfloat32_t v172 = svadd_f32_x(svptrue_b32(), v374, v38);
    svfloat32_t v173 = svsub_f32_x(svptrue_b32(), v374, v38);
    svfloat32_t v174 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v175 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v176 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v177 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v178 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v179 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v180 = svadd_f32_x(svptrue_b32(), v172, v174);
    svfloat32_t v181 = svsub_f32_x(svptrue_b32(), v172, v174);
    svfloat32_t v182 = svadd_f32_x(svptrue_b32(), v176, v178);
    svfloat32_t v183 = svsub_f32_x(svptrue_b32(), v176, v178);
    svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v177, v179);
    svfloat32_t v187 = svsub_f32_x(svptrue_b32(), v177, v179);
    svfloat32_t zero221 = svdup_n_f32(0);
    svfloat32_t v221 = svcmla_f32_x(pred_full, zero221, v380, v175, 90);
    svfloat32_t v184 = svadd_f32_x(svptrue_b32(), v180, v182);
    svfloat32_t v185 = svsub_f32_x(svptrue_b32(), v180, v182);
    svfloat32_t zero209 = svdup_n_f32(0);
    svfloat32_t v209 = svcmla_f32_x(pred_full, zero209, v380, v183, 90);
    svfloat32_t zero228 = svdup_n_f32(0);
    svfloat32_t v228 = svcmla_f32_x(pred_full, zero228, v381, v186, 90);
    svfloat32_t v234 = svadd_f32_x(svptrue_b32(), v181, v209);
    svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v181, v209);
    svfloat32_t v236 = svmla_f32_x(pred_full, v173, v187, v382);
    svfloat32_t v237 = svmls_f32_x(pred_full, v173, v187, v382);
    svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v221, v228);
    svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v221, v228);
    svst1_scatter_s64index_f64(pred_full, (double *)(v390), v454,
                               svreinterpret_f64_f32(v184));
    svst1_scatter_s64index_f64(pred_full, (double *)(v426), v454,
                               svreinterpret_f64_f32(v185));
    svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v236, v238);
    svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v236, v238);
    svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v237, v239);
    svfloat32_t v243 = svsub_f32_x(svptrue_b32(), v237, v239);
    svst1_scatter_s64index_f64(pred_full, (double *)(v408), v454,
                               svreinterpret_f64_f32(v235));
    svst1_scatter_s64index_f64(pred_full, (double *)(v444), v454,
                               svreinterpret_f64_f32(v234));
    svst1_scatter_s64index_f64(pred_full, (double *)(v399), v454,
                               svreinterpret_f64_f32(v241));
    svst1_scatter_s64index_f64(pred_full, (double *)(v417), v454,
                               svreinterpret_f64_f32(v242));
    svst1_scatter_s64index_f64(pred_full, (double *)(v435), v454,
                               svreinterpret_f64_f32(v243));
    svst1_scatter_s64index_f64(pred_full, (double *)(v453), v454,
                               svreinterpret_f64_f32(v240));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs9(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v294 = -5.0000000000000000e-01F;
    float v305 = -1.4999999999999998e+00F;
    float v308 = 8.6602540378443871e-01F;
    float v309 = -8.6602540378443871e-01F;
    float v316 = 7.6604444311897801e-01F;
    float v320 = 9.3969262078590832e-01F;
    float v324 = -1.7364817766693039e-01F;
    float v327 = 6.4278760968653925e-01F;
    float v328 = -6.4278760968653925e-01F;
    float v334 = -3.4202014332566888e-01F;
    float v335 = 3.4202014332566888e-01F;
    float v341 = 9.8480775301220802e-01F;
    float v342 = -9.8480775301220802e-01F;
    float32x2_t v344 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v279 = v5[0];
    float32x2_t v295 = (float32x2_t){v294, v294};
    float32x2_t v306 = (float32x2_t){v305, v305};
    float32x2_t v310 = (float32x2_t){v308, v309};
    float32x2_t v317 = (float32x2_t){v316, v316};
    float32x2_t v321 = (float32x2_t){v320, v320};
    float32x2_t v325 = (float32x2_t){v324, v324};
    float32x2_t v329 = (float32x2_t){v327, v328};
    float32x2_t v336 = (float32x2_t){v334, v335};
    float32x2_t v343 = (float32x2_t){v341, v342};
    float32x2_t v38 = v5[istride * 8];
    float32x2_t v56 = v7[j * 16];
    int64_t v60 = j * 16 + 1;
    int64_t v68 = 14 + j * 16;
    float32x2_t v82 = v5[istride * 7];
    float32x2_t v100 = v5[istride * 2];
    int64_t v117 = 12 + j * 16;
    int64_t v130 = 2 + j * 16;
    float32x2_t v144 = v5[istride * 3];
    float32x2_t v162 = v5[istride * 6];
    int64_t v179 = 4 + j * 16;
    int64_t v192 = 10 + j * 16;
    float32x2_t v206 = v5[istride * 4];
    float32x2_t v224 = v5[istride * 5];
    int64_t v241 = 6 + j * 16;
    int64_t v254 = 8 + j * 16;
    float32x2_t v312 = vmul_f32(v344, v310);
    float32x2_t v331 = vmul_f32(v344, v329);
    float32x2_t v338 = vmul_f32(v344, v336);
    float32x2_t v345 = vmul_f32(v344, v343);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v180 = v7[v179];
    float32x2_t v181 = vtrn1_f32(v144, v144);
    float32x2_t v182 = vtrn2_f32(v144, v144);
    int64_t v184 = v179 + 1;
    float32x2_t v193 = v7[v192];
    float32x2_t v194 = vtrn1_f32(v162, v162);
    float32x2_t v195 = vtrn2_f32(v162, v162);
    int64_t v197 = v192 + 1;
    float32x2_t v242 = v7[v241];
    float32x2_t v243 = vtrn1_f32(v206, v206);
    float32x2_t v244 = vtrn2_f32(v206, v206);
    int64_t v246 = v241 + 1;
    float32x2_t v255 = v7[v254];
    float32x2_t v256 = vtrn1_f32(v224, v224);
    float32x2_t v257 = vtrn2_f32(v224, v224);
    int64_t v259 = v254 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v185 = v7[v184];
    float32x2_t v186 = vmul_f32(v181, v180);
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vmul_f32(v194, v193);
    float32x2_t v247 = v7[v246];
    float32x2_t v248 = vmul_f32(v243, v242);
    float32x2_t v260 = v7[v259];
    float32x2_t v261 = vmul_f32(v256, v255);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v188 = vfma_f32(v186, v182, v185);
    float32x2_t v201 = vfma_f32(v199, v195, v198);
    float32x2_t v250 = vfma_f32(v248, v244, v247);
    float32x2_t v263 = vfma_f32(v261, v257, v260);
    float32x2_t v264 = vadd_f32(v64, v77);
    float32x2_t v265 = vsub_f32(v64, v77);
    float32x2_t v266 = vadd_f32(v126, v139);
    float32x2_t v267 = vsub_f32(v126, v139);
    float32x2_t v268 = vadd_f32(v188, v201);
    float32x2_t v269 = vsub_f32(v188, v201);
    float32x2_t v270 = vadd_f32(v250, v263);
    float32x2_t v271 = vsub_f32(v250, v263);
    float32x2_t v272 = vadd_f32(v264, v266);
    float32x2_t v281 = vadd_f32(v265, v267);
    float32x2_t v283 = vsub_f32(v264, v266);
    float32x2_t v284 = vsub_f32(v266, v270);
    float32x2_t v285 = vsub_f32(v270, v264);
    float32x2_t v286 = vsub_f32(v265, v267);
    float32x2_t v287 = vsub_f32(v267, v271);
    float32x2_t v288 = vsub_f32(v271, v265);
    float32x2_t v307 = vmul_f32(v268, v306);
    float32x2_t v313 = vrev64_f32(v269);
    float32x2_t v273 = vadd_f32(v272, v270);
    float32x2_t v282 = vadd_f32(v281, v271);
    float32x2_t v314 = vmul_f32(v313, v312);
    float32x2_t v318 = vmul_f32(v283, v317);
    float32x2_t v322 = vmul_f32(v284, v321);
    float32x2_t v326 = vmul_f32(v285, v325);
    float32x2_t v332 = vrev64_f32(v286);
    float32x2_t v339 = vrev64_f32(v287);
    float32x2_t v346 = vrev64_f32(v288);
    float32x2_t v274 = vadd_f32(v273, v268);
    float32x2_t v296 = vmul_f32(v273, v295);
    float32x2_t v302 = vrev64_f32(v282);
    float32x2_t v333 = vmul_f32(v332, v331);
    float32x2_t v340 = vmul_f32(v339, v338);
    float32x2_t v347 = vmul_f32(v346, v345);
    float32x2_t v280 = vadd_f32(v274, v279);
    float32x2_t v303 = vmul_f32(v302, v312);
    float32x2_t v348 = vadd_f32(v296, v296);
    float32x2_t v361 = vadd_f32(v314, v333);
    float32x2_t v363 = vsub_f32(v314, v340);
    float32x2_t v365 = vsub_f32(v314, v333);
    float32x2_t v349 = vadd_f32(v348, v296);
    float32x2_t v353 = vadd_f32(v280, v307);
    float32x2_t v362 = vadd_f32(v361, v340);
    float32x2_t v364 = vadd_f32(v363, v347);
    float32x2_t v366 = vsub_f32(v365, v347);
    v6[0] = v280;
    float32x2_t v350 = vadd_f32(v280, v349);
    float32x2_t v354 = vadd_f32(v353, v348);
    float32x2_t v351 = vadd_f32(v350, v303);
    float32x2_t v352 = vsub_f32(v350, v303);
    float32x2_t v355 = vadd_f32(v354, v318);
    float32x2_t v357 = vsub_f32(v354, v322);
    float32x2_t v359 = vsub_f32(v354, v318);
    float32x2_t v356 = vadd_f32(v355, v322);
    float32x2_t v358 = vadd_f32(v357, v326);
    float32x2_t v360 = vsub_f32(v359, v326);
    v6[ostride * 3] = v352;
    v6[ostride * 6] = v351;
    float32x2_t v367 = vadd_f32(v356, v362);
    float32x2_t v368 = vsub_f32(v356, v362);
    float32x2_t v369 = vadd_f32(v358, v364);
    float32x2_t v370 = vsub_f32(v358, v364);
    float32x2_t v371 = vadd_f32(v360, v366);
    float32x2_t v372 = vsub_f32(v360, v366);
    v6[ostride] = v368;
    v6[ostride * 2] = v369;
    v6[ostride * 4] = v372;
    v6[ostride * 5] = v371;
    v6[ostride * 7] = v370;
    v6[ostride * 8] = v367;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs9(const armral_cmplx_f32_t *restrict x,
                                        armral_cmplx_f32_t *restrict y,
                                        int istride, int ostride,
                                        const armral_cmplx_f32_t *restrict w,
                                        int howmany, int idist, int odist,
                                        float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v219 = -5.0000000000000000e-01F;
    float v231 = -1.4999999999999998e+00F;
    float v236 = -8.6602540378443871e-01F;
    float v243 = 7.6604444311897801e-01F;
    float v248 = 9.3969262078590832e-01F;
    float v253 = -1.7364817766693039e-01F;
    float v258 = -6.4278760968653925e-01F;
    float v265 = 3.4202014332566888e-01F;
    float v272 = -9.8480775301220802e-01F;
    const float32x2_t *v372 = &v5[v0];
    float32x2_t *v477 = &v6[v2];
    int64_t v33 = v0 * 8;
    int64_t v55 = v10 * 7;
    int64_t v61 = v0 * 7;
    int64_t v75 = v0 * 2;
    int64_t v90 = v10 * 6;
    int64_t v103 = v0 * 3;
    int64_t v117 = v0 * 6;
    int64_t v132 = v10 * 2;
    int64_t v139 = v10 * 5;
    int64_t v145 = v0 * 4;
    int64_t v159 = v0 * 5;
    int64_t v174 = v10 * 3;
    int64_t v181 = v10 * 4;
    int64_t v182 = v13 * 8;
    float v239 = v4 * v236;
    float v261 = v4 * v258;
    float v268 = v4 * v265;
    float v275 = v4 * v272;
    int64_t v318 = v2 * 2;
    int64_t v325 = v2 * 3;
    int64_t v332 = v2 * 4;
    int64_t v339 = v2 * 5;
    int64_t v346 = v2 * 6;
    int64_t v353 = v2 * 7;
    int64_t v360 = v2 * 8;
    const float32x2_t *v447 = &v5[0];
    svint64_t v448 = svindex_s64(0, v1);
    svfloat32_t v451 = svdup_n_f32(v219);
    svfloat32_t v453 = svdup_n_f32(v231);
    svfloat32_t v455 = svdup_n_f32(v243);
    svfloat32_t v456 = svdup_n_f32(v248);
    svfloat32_t v457 = svdup_n_f32(v253);
    float32x2_t *v468 = &v6[0];
    svint64_t v541 = svindex_s64(0, v3);
    svfloat32_t v51 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v182]));
    int64_t v57 = v55 + v182;
    int64_t v92 = v90 + v182;
    int64_t v99 = v10 + v182;
    int64_t v134 = v132 + v182;
    int64_t v141 = v139 + v182;
    int64_t v176 = v174 + v182;
    int64_t v183 = v181 + v182;
    svfloat32_t v374 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v372), v448));
    const float32x2_t *v382 = &v5[v33];
    const float32x2_t *v392 = &v5[v61];
    const float32x2_t *v401 = &v5[v75];
    const float32x2_t *v410 = &v5[v103];
    const float32x2_t *v419 = &v5[v117];
    const float32x2_t *v428 = &v5[v145];
    const float32x2_t *v437 = &v5[v159];
    svfloat32_t v449 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v447), v448));
    svfloat32_t v454 = svdup_n_f32(v239);
    svfloat32_t v458 = svdup_n_f32(v261);
    svfloat32_t v459 = svdup_n_f32(v268);
    svfloat32_t v460 = svdup_n_f32(v275);
    float32x2_t *v486 = &v6[v318];
    float32x2_t *v495 = &v6[v325];
    float32x2_t *v504 = &v6[v332];
    float32x2_t *v513 = &v6[v339];
    float32x2_t *v522 = &v6[v346];
    float32x2_t *v531 = &v6[v353];
    float32x2_t *v540 = &v6[v360];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v374, v51, 0),
                     v374, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v135 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v134]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v177 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v176]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v384 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v382), v448));
    svfloat32_t v394 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v392), v448));
    svfloat32_t v403 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v401), v448));
    svfloat32_t v412 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v410), v448));
    svfloat32_t v421 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v419), v448));
    svfloat32_t v430 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v428), v448));
    svfloat32_t v439 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v437), v448));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v384, v58, 0),
                     v384, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v394, v93, 0),
                     v394, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v403, v100, 0),
                     v403, v100, 90);
    svfloat32_t zero136 = svdup_n_f32(0);
    svfloat32_t v136 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero136, v412, v135, 0),
                     v412, v135, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero143, v421, v142, 0),
                     v421, v142, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero178, v430, v177, 0),
                     v430, v177, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero185, v439, v184, 0),
                     v439, v184, 90);
    svfloat32_t v186 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v187 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v188 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v189 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v190 = svadd_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v191 = svsub_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v192 = svadd_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v193 = svsub_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v194 = svadd_f32_x(svptrue_b32(), v186, v188);
    svfloat32_t v205 = svadd_f32_x(svptrue_b32(), v187, v189);
    svfloat32_t v207 = svsub_f32_x(svptrue_b32(), v186, v188);
    svfloat32_t v208 = svsub_f32_x(svptrue_b32(), v188, v192);
    svfloat32_t v209 = svsub_f32_x(svptrue_b32(), v192, v186);
    svfloat32_t v210 = svsub_f32_x(svptrue_b32(), v187, v189);
    svfloat32_t v211 = svsub_f32_x(svptrue_b32(), v189, v193);
    svfloat32_t v212 = svsub_f32_x(svptrue_b32(), v193, v187);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 = svcmla_f32_x(pred_full, zero241, v454, v191, 90);
    svfloat32_t v195 = svadd_f32_x(svptrue_b32(), v194, v192);
    svfloat32_t v206 = svadd_f32_x(svptrue_b32(), v205, v193);
    svfloat32_t zero263 = svdup_n_f32(0);
    svfloat32_t v263 = svcmla_f32_x(pred_full, zero263, v458, v210, 90);
    svfloat32_t zero270 = svdup_n_f32(0);
    svfloat32_t v270 = svcmla_f32_x(pred_full, zero270, v459, v211, 90);
    svfloat32_t zero277 = svdup_n_f32(0);
    svfloat32_t v277 = svcmla_f32_x(pred_full, zero277, v460, v212, 90);
    svfloat32_t v196 = svadd_f32_x(svptrue_b32(), v195, v190);
    svfloat32_t v222 = svmul_f32_x(svptrue_b32(), v195, v451);
    svfloat32_t zero229 = svdup_n_f32(0);
    svfloat32_t v229 = svcmla_f32_x(pred_full, zero229, v454, v206, 90);
    svfloat32_t v291 = svadd_f32_x(svptrue_b32(), v241, v263);
    svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v241, v270);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v241, v263);
    svfloat32_t v204 = svadd_f32_x(svptrue_b32(), v196, v449);
    svfloat32_t v278 = svadd_f32_x(svptrue_b32(), v222, v222);
    svfloat32_t v292 = svadd_f32_x(svptrue_b32(), v291, v270);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v293, v277);
    svfloat32_t v296 = svsub_f32_x(svptrue_b32(), v295, v277);
    svfloat32_t v279 = svmla_f32_x(pred_full, v278, v195, v451);
    svfloat32_t v283 = svmla_f32_x(pred_full, v204, v190, v453);
    svst1_scatter_s64index_f64(pred_full, (double *)(v468), v541,
                               svreinterpret_f64_f32(v204));
    svfloat32_t v280 = svadd_f32_x(svptrue_b32(), v204, v279);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v283, v278);
    svfloat32_t v281 = svadd_f32_x(svptrue_b32(), v280, v229);
    svfloat32_t v282 = svsub_f32_x(svptrue_b32(), v280, v229);
    svfloat32_t v285 = svmla_f32_x(pred_full, v284, v207, v455);
    svfloat32_t v287 = svmls_f32_x(pred_full, v284, v208, v456);
    svfloat32_t v289 = svmls_f32_x(pred_full, v284, v207, v455);
    svfloat32_t v286 = svmla_f32_x(pred_full, v285, v208, v456);
    svfloat32_t v288 = svmla_f32_x(pred_full, v287, v209, v457);
    svfloat32_t v290 = svmls_f32_x(pred_full, v289, v209, v457);
    svst1_scatter_s64index_f64(pred_full, (double *)(v495), v541,
                               svreinterpret_f64_f32(v282));
    svst1_scatter_s64index_f64(pred_full, (double *)(v522), v541,
                               svreinterpret_f64_f32(v281));
    svfloat32_t v297 = svadd_f32_x(svptrue_b32(), v286, v292);
    svfloat32_t v298 = svsub_f32_x(svptrue_b32(), v286, v292);
    svfloat32_t v299 = svadd_f32_x(svptrue_b32(), v288, v294);
    svfloat32_t v300 = svsub_f32_x(svptrue_b32(), v288, v294);
    svfloat32_t v301 = svadd_f32_x(svptrue_b32(), v290, v296);
    svfloat32_t v302 = svsub_f32_x(svptrue_b32(), v290, v296);
    svst1_scatter_s64index_f64(pred_full, (double *)(v477), v541,
                               svreinterpret_f64_f32(v298));
    svst1_scatter_s64index_f64(pred_full, (double *)(v486), v541,
                               svreinterpret_f64_f32(v299));
    svst1_scatter_s64index_f64(pred_full, (double *)(v504), v541,
                               svreinterpret_f64_f32(v302));
    svst1_scatter_s64index_f64(pred_full, (double *)(v513), v541,
                               svreinterpret_f64_f32(v301));
    svst1_scatter_s64index_f64(pred_full, (double *)(v531), v541,
                               svreinterpret_f64_f32(v300));
    svst1_scatter_s64index_f64(pred_full, (double *)(v540), v541,
                               svreinterpret_f64_f32(v297));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs10(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v193 = v5[istride];
    float v373 = -1.2500000000000000e+00F;
    float v377 = 5.5901699437494745e-01F;
    float v380 = 1.5388417685876268e+00F;
    float v381 = -1.5388417685876268e+00F;
    float v387 = 5.8778525229247325e-01F;
    float v388 = -5.8778525229247325e-01F;
    float v394 = 3.6327126400268028e-01F;
    float v395 = -3.6327126400268028e-01F;
    float32x2_t v397 = (float32x2_t){v4, v4};
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    float32x2_t v299 = v5[0];
    float32x2_t v374 = (float32x2_t){v373, v373};
    float32x2_t v378 = (float32x2_t){v377, v377};
    float32x2_t v382 = (float32x2_t){v380, v381};
    float32x2_t v389 = (float32x2_t){v387, v388};
    float32x2_t v396 = (float32x2_t){v394, v395};
    float32x2_t v20 = v5[istride * 5];
    int64_t v37 = 8 + j * 18;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 7];
    int64_t v86 = 2 + j * 18;
    int64_t v99 = 12 + j * 18;
    float32x2_t v113 = v5[istride * 4];
    float32x2_t v131 = v5[istride * 9];
    int64_t v148 = 6 + j * 18;
    int64_t v161 = 16 + j * 18;
    float32x2_t v175 = v5[istride * 6];
    int64_t v210 = 10 + j * 18;
    float32x2_t v224 = v7[j * 18];
    int64_t v228 = j * 18 + 1;
    float32x2_t v237 = v5[istride * 8];
    float32x2_t v255 = v5[istride * 3];
    int64_t v272 = 14 + j * 18;
    int64_t v285 = 4 + j * 18;
    float32x2_t v384 = vmul_f32(v397, v382);
    float32x2_t v391 = vmul_f32(v397, v389);
    float32x2_t v398 = vmul_f32(v397, v396);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v273 = v7[v272];
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    int64_t v277 = v272 + 1;
    float32x2_t v286 = v7[v285];
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    int64_t v290 = v285 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v300 = vadd_f32(v299, v46);
    float32x2_t v301 = vsub_f32(v299, v46);
    float32x2_t v302 = vadd_f32(v95, v108);
    float32x2_t v303 = vsub_f32(v95, v108);
    float32x2_t v304 = vadd_f32(v157, v170);
    float32x2_t v305 = vsub_f32(v157, v170);
    float32x2_t v306 = vadd_f32(v219, v232);
    float32x2_t v307 = vsub_f32(v219, v232);
    float32x2_t v308 = vadd_f32(v281, v294);
    float32x2_t v309 = vsub_f32(v281, v294);
    float32x2_t v310 = vadd_f32(v302, v308);
    float32x2_t v311 = vsub_f32(v302, v308);
    float32x2_t v312 = vadd_f32(v306, v304);
    float32x2_t v313 = vsub_f32(v306, v304);
    float32x2_t v360 = vadd_f32(v303, v309);
    float32x2_t v361 = vsub_f32(v303, v309);
    float32x2_t v362 = vadd_f32(v307, v305);
    float32x2_t v363 = vsub_f32(v307, v305);
    float32x2_t v314 = vadd_f32(v310, v312);
    float32x2_t v315 = vsub_f32(v310, v312);
    float32x2_t v316 = vadd_f32(v311, v313);
    float32x2_t v335 = vrev64_f32(v311);
    float32x2_t v349 = vrev64_f32(v313);
    float32x2_t v364 = vadd_f32(v360, v362);
    float32x2_t v365 = vsub_f32(v360, v362);
    float32x2_t v366 = vadd_f32(v361, v363);
    float32x2_t v385 = vrev64_f32(v361);
    float32x2_t v399 = vrev64_f32(v363);
    float32x2_t v317 = vadd_f32(v314, v300);
    float32x2_t v325 = vmul_f32(v314, v374);
    float32x2_t v329 = vmul_f32(v315, v378);
    float32x2_t v336 = vmul_f32(v335, v384);
    float32x2_t v342 = vrev64_f32(v316);
    float32x2_t v350 = vmul_f32(v349, v398);
    float32x2_t v367 = vadd_f32(v364, v301);
    float32x2_t v375 = vmul_f32(v364, v374);
    float32x2_t v379 = vmul_f32(v365, v378);
    float32x2_t v386 = vmul_f32(v385, v384);
    float32x2_t v392 = vrev64_f32(v366);
    float32x2_t v400 = vmul_f32(v399, v398);
    float32x2_t v343 = vmul_f32(v342, v391);
    float32x2_t v351 = vadd_f32(v317, v325);
    float32x2_t v393 = vmul_f32(v392, v391);
    float32x2_t v401 = vadd_f32(v367, v375);
    v6[0] = v317;
    v6[ostride * 5] = v367;
    float32x2_t v352 = vadd_f32(v351, v329);
    float32x2_t v353 = vsub_f32(v351, v329);
    float32x2_t v354 = vsub_f32(v336, v343);
    float32x2_t v355 = vadd_f32(v343, v350);
    float32x2_t v402 = vadd_f32(v401, v379);
    float32x2_t v403 = vsub_f32(v401, v379);
    float32x2_t v404 = vsub_f32(v386, v393);
    float32x2_t v405 = vadd_f32(v393, v400);
    float32x2_t v356 = vadd_f32(v352, v354);
    float32x2_t v357 = vsub_f32(v352, v354);
    float32x2_t v358 = vadd_f32(v353, v355);
    float32x2_t v359 = vsub_f32(v353, v355);
    float32x2_t v406 = vadd_f32(v402, v404);
    float32x2_t v407 = vsub_f32(v402, v404);
    float32x2_t v408 = vadd_f32(v403, v405);
    float32x2_t v409 = vsub_f32(v403, v405);
    v6[ostride * 6] = v357;
    v6[ostride] = v407;
    v6[ostride * 2] = v359;
    v6[ostride * 7] = v409;
    v6[ostride * 8] = v358;
    v6[ostride * 3] = v408;
    v6[ostride * 4] = v356;
    v6[ostride * 9] = v406;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs10(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v291 = -1.2500000000000000e+00F;
    float v296 = 5.5901699437494745e-01F;
    float v301 = -1.5388417685876268e+00F;
    float v308 = -5.8778525229247325e-01F;
    float v315 = -3.6327126400268028e-01F;
    const float32x2_t *v460 = &v5[v0];
    float32x2_t *v539 = &v6[v2];
    int64_t v19 = v0 * 5;
    int64_t v34 = v10 * 4;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 7;
    int64_t v76 = v10 * 6;
    int64_t v82 = v0 * 4;
    int64_t v96 = v0 * 9;
    int64_t v111 = v10 * 3;
    int64_t v118 = v10 * 8;
    int64_t v124 = v0 * 6;
    int64_t v153 = v10 * 5;
    int64_t v166 = v0 * 8;
    int64_t v180 = v0 * 3;
    int64_t v195 = v10 * 7;
    int64_t v202 = v10 * 2;
    int64_t v203 = v13 * 9;
    float v304 = v4 * v301;
    float v311 = v4 * v308;
    float v318 = v4 * v315;
    int64_t v338 = v2 * 5;
    int64_t v345 = v2 * 6;
    int64_t v359 = v2 * 2;
    int64_t v366 = v2 * 7;
    int64_t v373 = v2 * 8;
    int64_t v380 = v2 * 3;
    int64_t v387 = v2 * 4;
    int64_t v394 = v2 * 9;
    const float32x2_t *v490 = &v5[0];
    svint64_t v491 = svindex_s64(0, v1);
    svfloat32_t v500 = svdup_n_f32(v291);
    svfloat32_t v501 = svdup_n_f32(v296);
    float32x2_t *v512 = &v6[0];
    svint64_t v594 = svindex_s64(0, v3);
    int64_t v36 = v34 + v203;
    int64_t v71 = v10 + v203;
    int64_t v78 = v76 + v203;
    int64_t v113 = v111 + v203;
    int64_t v120 = v118 + v203;
    int64_t v155 = v153 + v203;
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v203]));
    int64_t v197 = v195 + v203;
    int64_t v204 = v202 + v203;
    const float32x2_t *v406 = &v5[v19];
    const float32x2_t *v415 = &v5[v40];
    const float32x2_t *v424 = &v5[v54];
    const float32x2_t *v433 = &v5[v82];
    const float32x2_t *v442 = &v5[v96];
    const float32x2_t *v451 = &v5[v124];
    svfloat32_t v462 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v460), v491));
    const float32x2_t *v471 = &v5[v166];
    const float32x2_t *v480 = &v5[v180];
    svfloat32_t v492 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v490), v491));
    svfloat32_t v502 = svdup_n_f32(v304);
    svfloat32_t v503 = svdup_n_f32(v311);
    svfloat32_t v504 = svdup_n_f32(v318);
    float32x2_t *v521 = &v6[v338];
    float32x2_t *v530 = &v6[v345];
    float32x2_t *v548 = &v6[v359];
    float32x2_t *v557 = &v6[v366];
    float32x2_t *v566 = &v6[v373];
    float32x2_t *v575 = &v6[v380];
    float32x2_t *v584 = &v6[v387];
    float32x2_t *v593 = &v6[v394];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v462, v163, 0),
                     v462, v163, 90);
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v408 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v406), v491));
    svfloat32_t v417 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v415), v491));
    svfloat32_t v426 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v424), v491));
    svfloat32_t v435 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v433), v491));
    svfloat32_t v444 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v442), v491));
    svfloat32_t v453 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v451), v491));
    svfloat32_t v473 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v471), v491));
    svfloat32_t v482 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v480), v491));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v408, v37, 0),
                     v408, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v417, v72, 0),
                     v417, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v426, v79, 0),
                     v426, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v435, v114, 0),
                     v435, v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v444, v121, 0),
                     v444, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v453, v156, 0),
                     v453, v156, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v473, v198, 0),
                     v473, v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v482, v205, 0),
                     v482, v205, 90);
    svfloat32_t v214 = svadd_f32_x(svptrue_b32(), v492, v38);
    svfloat32_t v215 = svsub_f32_x(svptrue_b32(), v492, v38);
    svfloat32_t v216 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v217 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v218 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v219 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v220 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v221 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v222 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v223 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v224 = svadd_f32_x(svptrue_b32(), v216, v222);
    svfloat32_t v225 = svsub_f32_x(svptrue_b32(), v216, v222);
    svfloat32_t v226 = svadd_f32_x(svptrue_b32(), v220, v218);
    svfloat32_t v227 = svsub_f32_x(svptrue_b32(), v220, v218);
    svfloat32_t v277 = svadd_f32_x(svptrue_b32(), v217, v223);
    svfloat32_t v278 = svsub_f32_x(svptrue_b32(), v217, v223);
    svfloat32_t v279 = svadd_f32_x(svptrue_b32(), v221, v219);
    svfloat32_t v280 = svsub_f32_x(svptrue_b32(), v221, v219);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v224, v226);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v224, v226);
    svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v225, v227);
    svfloat32_t zero253 = svdup_n_f32(0);
    svfloat32_t v253 = svcmla_f32_x(pred_full, zero253, v502, v225, 90);
    svfloat32_t v281 = svadd_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t v282 = svsub_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t v283 = svadd_f32_x(svptrue_b32(), v278, v280);
    svfloat32_t zero306 = svdup_n_f32(0);
    svfloat32_t v306 = svcmla_f32_x(pred_full, zero306, v502, v278, 90);
    svfloat32_t v231 = svadd_f32_x(svptrue_b32(), v228, v214);
    svfloat32_t zero260 = svdup_n_f32(0);
    svfloat32_t v260 = svcmla_f32_x(pred_full, zero260, v503, v230, 90);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v281, v215);
    svfloat32_t zero313 = svdup_n_f32(0);
    svfloat32_t v313 = svcmla_f32_x(pred_full, zero313, v503, v283, 90);
    svfloat32_t v268 = svmla_f32_x(pred_full, v231, v228, v500);
    svfloat32_t v271 = svsub_f32_x(svptrue_b32(), v253, v260);
    svfloat32_t v272 = svcmla_f32_x(pred_full, v260, v504, v227, 90);
    svfloat32_t v321 = svmla_f32_x(pred_full, v284, v281, v500);
    svfloat32_t v324 = svsub_f32_x(svptrue_b32(), v306, v313);
    svfloat32_t v325 = svcmla_f32_x(pred_full, v313, v504, v280, 90);
    svst1_scatter_s64index_f64(pred_full, (double *)(v512), v594,
                               svreinterpret_f64_f32(v231));
    svst1_scatter_s64index_f64(pred_full, (double *)(v521), v594,
                               svreinterpret_f64_f32(v284));
    svfloat32_t v269 = svmla_f32_x(pred_full, v268, v229, v501);
    svfloat32_t v270 = svmls_f32_x(pred_full, v268, v229, v501);
    svfloat32_t v322 = svmla_f32_x(pred_full, v321, v282, v501);
    svfloat32_t v323 = svmls_f32_x(pred_full, v321, v282, v501);
    svfloat32_t v273 = svadd_f32_x(svptrue_b32(), v269, v271);
    svfloat32_t v274 = svsub_f32_x(svptrue_b32(), v269, v271);
    svfloat32_t v275 = svadd_f32_x(svptrue_b32(), v270, v272);
    svfloat32_t v276 = svsub_f32_x(svptrue_b32(), v270, v272);
    svfloat32_t v326 = svadd_f32_x(svptrue_b32(), v322, v324);
    svfloat32_t v327 = svsub_f32_x(svptrue_b32(), v322, v324);
    svfloat32_t v328 = svadd_f32_x(svptrue_b32(), v323, v325);
    svfloat32_t v329 = svsub_f32_x(svptrue_b32(), v323, v325);
    svst1_scatter_s64index_f64(pred_full, (double *)(v530), v594,
                               svreinterpret_f64_f32(v274));
    svst1_scatter_s64index_f64(pred_full, (double *)(v539), v594,
                               svreinterpret_f64_f32(v327));
    svst1_scatter_s64index_f64(pred_full, (double *)(v548), v594,
                               svreinterpret_f64_f32(v276));
    svst1_scatter_s64index_f64(pred_full, (double *)(v557), v594,
                               svreinterpret_f64_f32(v329));
    svst1_scatter_s64index_f64(pred_full, (double *)(v566), v594,
                               svreinterpret_f64_f32(v275));
    svst1_scatter_s64index_f64(pred_full, (double *)(v575), v594,
                               svreinterpret_f64_f32(v328));
    svst1_scatter_s64index_f64(pred_full, (double *)(v584), v594,
                               svreinterpret_f64_f32(v273));
    svst1_scatter_s64index_f64(pred_full, (double *)(v593), v594,
                               svreinterpret_f64_f32(v326));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs11(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v373 = 1.1000000000000001e+00F;
    float v376 = 3.3166247903554003e-01F;
    float v377 = -3.3166247903554003e-01F;
    float v384 = 5.1541501300188641e-01F;
    float v388 = 9.4125353283118118e-01F;
    float v392 = 1.4143537075597825e+00F;
    float v396 = 8.5949297361449750e-01F;
    float v400 = 4.2314838273285138e-02F;
    float v404 = 3.8639279888589606e-01F;
    float v408 = 5.1254589567200015e-01F;
    float v412 = 1.0702757469471715e+00F;
    float v416 = 5.5486073394528512e-01F;
    float v419 = 1.2412944743900585e+00F;
    float v420 = -1.2412944743900585e+00F;
    float v426 = 2.0897833842005756e-01F;
    float v427 = -2.0897833842005756e-01F;
    float v433 = 3.7415717312460811e-01F;
    float v434 = -3.7415717312460811e-01F;
    float v440 = 4.9929922194110327e-02F;
    float v441 = -4.9929922194110327e-02F;
    float v447 = 6.5815896284539266e-01F;
    float v448 = -6.5815896284539266e-01F;
    float v454 = 6.3306543373877577e-01F;
    float v455 = -6.3306543373877577e-01F;
    float v461 = 1.0822460581641109e+00F;
    float v462 = -1.0822460581641109e+00F;
    float v468 = 8.1720737907134022e-01F;
    float v469 = -8.1720737907134022e-01F;
    float v475 = 4.2408709531871824e-01F;
    float v476 = -4.2408709531871824e-01F;
    float32x2_t v478 = (float32x2_t){v4, v4};
    float32x2_t v201 = vtrn1_f32(v20, v20);
    float32x2_t v202 = vtrn2_f32(v20, v20);
    float32x2_t v346 = v5[0];
    float32x2_t v374 = (float32x2_t){v373, v373};
    float32x2_t v378 = (float32x2_t){v376, v377};
    float32x2_t v385 = (float32x2_t){v384, v384};
    float32x2_t v389 = (float32x2_t){v388, v388};
    float32x2_t v393 = (float32x2_t){v392, v392};
    float32x2_t v397 = (float32x2_t){v396, v396};
    float32x2_t v401 = (float32x2_t){v400, v400};
    float32x2_t v405 = (float32x2_t){v404, v404};
    float32x2_t v409 = (float32x2_t){v408, v408};
    float32x2_t v413 = (float32x2_t){v412, v412};
    float32x2_t v417 = (float32x2_t){v416, v416};
    float32x2_t v421 = (float32x2_t){v419, v420};
    float32x2_t v428 = (float32x2_t){v426, v427};
    float32x2_t v435 = (float32x2_t){v433, v434};
    float32x2_t v442 = (float32x2_t){v440, v441};
    float32x2_t v449 = (float32x2_t){v447, v448};
    float32x2_t v456 = (float32x2_t){v454, v455};
    float32x2_t v463 = (float32x2_t){v461, v462};
    float32x2_t v470 = (float32x2_t){v468, v469};
    float32x2_t v477 = (float32x2_t){v475, v476};
    float32x2_t v38 = v5[istride * 10];
    float32x2_t v56 = v5[istride * 2];
    float32x2_t v74 = v5[istride * 9];
    float32x2_t v92 = v5[istride * 3];
    float32x2_t v110 = v5[istride * 8];
    float32x2_t v128 = v5[istride * 4];
    float32x2_t v146 = v5[istride * 7];
    float32x2_t v164 = v5[istride * 5];
    float32x2_t v182 = v5[istride * 6];
    float32x2_t v200 = v7[j * 20];
    int64_t v204 = j * 20 + 1;
    int64_t v212 = 18 + j * 20;
    int64_t v225 = 2 + j * 20;
    int64_t v238 = 16 + j * 20;
    int64_t v251 = 4 + j * 20;
    int64_t v264 = 14 + j * 20;
    int64_t v277 = 6 + j * 20;
    int64_t v290 = 12 + j * 20;
    int64_t v303 = 8 + j * 20;
    int64_t v316 = 10 + j * 20;
    float32x2_t v380 = vmul_f32(v478, v378);
    float32x2_t v423 = vmul_f32(v478, v421);
    float32x2_t v430 = vmul_f32(v478, v428);
    float32x2_t v437 = vmul_f32(v478, v435);
    float32x2_t v444 = vmul_f32(v478, v442);
    float32x2_t v451 = vmul_f32(v478, v449);
    float32x2_t v458 = vmul_f32(v478, v456);
    float32x2_t v465 = vmul_f32(v478, v463);
    float32x2_t v472 = vmul_f32(v478, v470);
    float32x2_t v479 = vmul_f32(v478, v477);
    float32x2_t v205 = v7[v204];
    float32x2_t v206 = vmul_f32(v201, v200);
    float32x2_t v213 = v7[v212];
    float32x2_t v214 = vtrn1_f32(v38, v38);
    float32x2_t v215 = vtrn2_f32(v38, v38);
    int64_t v217 = v212 + 1;
    float32x2_t v226 = v7[v225];
    float32x2_t v227 = vtrn1_f32(v56, v56);
    float32x2_t v228 = vtrn2_f32(v56, v56);
    int64_t v230 = v225 + 1;
    float32x2_t v239 = v7[v238];
    float32x2_t v240 = vtrn1_f32(v74, v74);
    float32x2_t v241 = vtrn2_f32(v74, v74);
    int64_t v243 = v238 + 1;
    float32x2_t v252 = v7[v251];
    float32x2_t v253 = vtrn1_f32(v92, v92);
    float32x2_t v254 = vtrn2_f32(v92, v92);
    int64_t v256 = v251 + 1;
    float32x2_t v265 = v7[v264];
    float32x2_t v266 = vtrn1_f32(v110, v110);
    float32x2_t v267 = vtrn2_f32(v110, v110);
    int64_t v269 = v264 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vtrn1_f32(v128, v128);
    float32x2_t v280 = vtrn2_f32(v128, v128);
    int64_t v282 = v277 + 1;
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vtrn1_f32(v146, v146);
    float32x2_t v293 = vtrn2_f32(v146, v146);
    int64_t v295 = v290 + 1;
    float32x2_t v304 = v7[v303];
    float32x2_t v305 = vtrn1_f32(v164, v164);
    float32x2_t v306 = vtrn2_f32(v164, v164);
    int64_t v308 = v303 + 1;
    float32x2_t v317 = v7[v316];
    float32x2_t v318 = vtrn1_f32(v182, v182);
    float32x2_t v319 = vtrn2_f32(v182, v182);
    int64_t v321 = v316 + 1;
    float32x2_t v218 = v7[v217];
    float32x2_t v219 = vmul_f32(v214, v213);
    float32x2_t v231 = v7[v230];
    float32x2_t v232 = vmul_f32(v227, v226);
    float32x2_t v244 = v7[v243];
    float32x2_t v245 = vmul_f32(v240, v239);
    float32x2_t v257 = v7[v256];
    float32x2_t v258 = vmul_f32(v253, v252);
    float32x2_t v270 = v7[v269];
    float32x2_t v271 = vmul_f32(v266, v265);
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vmul_f32(v292, v291);
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vmul_f32(v305, v304);
    float32x2_t v322 = v7[v321];
    float32x2_t v323 = vmul_f32(v318, v317);
    float32x2_t v208 = vfma_f32(v206, v202, v205);
    float32x2_t v221 = vfma_f32(v219, v215, v218);
    float32x2_t v234 = vfma_f32(v232, v228, v231);
    float32x2_t v247 = vfma_f32(v245, v241, v244);
    float32x2_t v260 = vfma_f32(v258, v254, v257);
    float32x2_t v273 = vfma_f32(v271, v267, v270);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v299 = vfma_f32(v297, v293, v296);
    float32x2_t v312 = vfma_f32(v310, v306, v309);
    float32x2_t v325 = vfma_f32(v323, v319, v322);
    float32x2_t v326 = vadd_f32(v208, v221);
    float32x2_t v327 = vadd_f32(v234, v247);
    float32x2_t v328 = vadd_f32(v260, v273);
    float32x2_t v329 = vadd_f32(v286, v299);
    float32x2_t v330 = vadd_f32(v312, v325);
    float32x2_t v331 = vsub_f32(v208, v221);
    float32x2_t v332 = vsub_f32(v234, v247);
    float32x2_t v333 = vsub_f32(v260, v273);
    float32x2_t v334 = vsub_f32(v286, v299);
    float32x2_t v335 = vsub_f32(v312, v325);
    float32x2_t v336 = vadd_f32(v326, v327);
    float32x2_t v337 = vadd_f32(v328, v330);
    float32x2_t v339 = vsub_f32(v332, v333);
    float32x2_t v340 = vadd_f32(v331, v335);
    float32x2_t v350 = vsub_f32(v327, v329);
    float32x2_t v351 = vsub_f32(v326, v329);
    float32x2_t v352 = vsub_f32(v327, v326);
    float32x2_t v353 = vsub_f32(v330, v329);
    float32x2_t v354 = vsub_f32(v328, v329);
    float32x2_t v355 = vsub_f32(v330, v328);
    float32x2_t v356 = vsub_f32(v327, v330);
    float32x2_t v357 = vsub_f32(v326, v328);
    float32x2_t v359 = vadd_f32(v332, v334);
    float32x2_t v360 = vsub_f32(v331, v334);
    float32x2_t v361 = vadd_f32(v331, v332);
    float32x2_t v362 = vsub_f32(v334, v335);
    float32x2_t v363 = vsub_f32(v333, v334);
    float32x2_t v364 = vsub_f32(v333, v335);
    float32x2_t v365 = vadd_f32(v332, v335);
    float32x2_t v366 = vsub_f32(v331, v333);
    float32x2_t v338 = vadd_f32(v329, v336);
    float32x2_t v348 = vsub_f32(v339, v340);
    float32x2_t v358 = vsub_f32(v337, v336);
    float32x2_t v367 = vadd_f32(v339, v340);
    float32x2_t v386 = vmul_f32(v350, v385);
    float32x2_t v390 = vmul_f32(v351, v389);
    float32x2_t v394 = vmul_f32(v352, v393);
    float32x2_t v398 = vmul_f32(v353, v397);
    float32x2_t v402 = vmul_f32(v354, v401);
    float32x2_t v406 = vmul_f32(v355, v405);
    float32x2_t v410 = vmul_f32(v356, v409);
    float32x2_t v414 = vmul_f32(v357, v413);
    float32x2_t v424 = vrev64_f32(v359);
    float32x2_t v431 = vrev64_f32(v360);
    float32x2_t v438 = vrev64_f32(v361);
    float32x2_t v445 = vrev64_f32(v362);
    float32x2_t v452 = vrev64_f32(v363);
    float32x2_t v459 = vrev64_f32(v364);
    float32x2_t v466 = vrev64_f32(v365);
    float32x2_t v473 = vrev64_f32(v366);
    float32x2_t v341 = vadd_f32(v338, v337);
    float32x2_t v349 = vsub_f32(v348, v334);
    float32x2_t v418 = vmul_f32(v358, v417);
    float32x2_t v425 = vmul_f32(v424, v423);
    float32x2_t v432 = vmul_f32(v431, v430);
    float32x2_t v439 = vmul_f32(v438, v437);
    float32x2_t v446 = vmul_f32(v445, v444);
    float32x2_t v453 = vmul_f32(v452, v451);
    float32x2_t v460 = vmul_f32(v459, v458);
    float32x2_t v467 = vmul_f32(v466, v465);
    float32x2_t v474 = vmul_f32(v473, v472);
    float32x2_t v480 = vrev64_f32(v367);
    float32x2_t v483 = vadd_f32(v386, v390);
    float32x2_t v484 = vadd_f32(v390, v394);
    float32x2_t v485 = vsub_f32(v386, v394);
    float32x2_t v486 = vadd_f32(v398, v402);
    float32x2_t v487 = vadd_f32(v402, v406);
    float32x2_t v488 = vsub_f32(v398, v406);
    float32x2_t v347 = vadd_f32(v346, v341);
    float32x2_t v375 = vmul_f32(v341, v374);
    float32x2_t v381 = vrev64_f32(v349);
    float32x2_t v481 = vmul_f32(v480, v479);
    float32x2_t v489 = vadd_f32(v414, v418);
    float32x2_t v490 = vadd_f32(v410, v418);
    float32x2_t v491 = vadd_f32(v432, v439);
    float32x2_t v492 = vsub_f32(v425, v439);
    float32x2_t v493 = vadd_f32(v453, v460);
    float32x2_t v494 = vsub_f32(v446, v460);
    float32x2_t v382 = vmul_f32(v381, v380);
    float32x2_t v482 = vsub_f32(v347, v375);
    float32x2_t v495 = vadd_f32(v474, v481);
    float32x2_t v496 = vsub_f32(v467, v481);
    float32x2_t v497 = vadd_f32(v487, v489);
    float32x2_t v515 = vadd_f32(v491, v492);
    v6[0] = v347;
    float32x2_t v498 = vadd_f32(v497, v482);
    float32x2_t v499 = vsub_f32(v482, v484);
    float32x2_t v501 = vadd_f32(v482, v488);
    float32x2_t v503 = vsub_f32(v482, v485);
    float32x2_t v505 = vadd_f32(v482, v483);
    float32x2_t v507 = vadd_f32(v382, v493);
    float32x2_t v509 = vsub_f32(v495, v491);
    float32x2_t v511 = vadd_f32(v382, v496);
    float32x2_t v513 = vsub_f32(v496, v492);
    float32x2_t v516 = vadd_f32(v515, v493);
    float32x2_t v500 = vsub_f32(v499, v489);
    float32x2_t v502 = vadd_f32(v501, v490);
    float32x2_t v504 = vsub_f32(v503, v490);
    float32x2_t v506 = vsub_f32(v505, v486);
    float32x2_t v508 = vadd_f32(v507, v495);
    float32x2_t v510 = vsub_f32(v509, v382);
    float32x2_t v512 = vadd_f32(v511, v494);
    float32x2_t v514 = vsub_f32(v513, v382);
    float32x2_t v517 = vadd_f32(v516, v494);
    float32x2_t v518 = vsub_f32(v517, v382);
    float32x2_t v520 = vadd_f32(v498, v508);
    float32x2_t v521 = vadd_f32(v500, v510);
    float32x2_t v522 = vsub_f32(v502, v512);
    float32x2_t v523 = vadd_f32(v504, v514);
    float32x2_t v524 = vsub_f32(v504, v514);
    float32x2_t v525 = vadd_f32(v502, v512);
    float32x2_t v526 = vsub_f32(v500, v510);
    float32x2_t v527 = vsub_f32(v498, v508);
    float32x2_t v519 = vadd_f32(v506, v518);
    float32x2_t v528 = vsub_f32(v506, v518);
    v6[ostride * 9] = v520;
    v6[ostride * 8] = v521;
    v6[ostride * 7] = v522;
    v6[ostride * 6] = v523;
    v6[ostride * 5] = v524;
    v6[ostride * 4] = v525;
    v6[ostride * 3] = v526;
    v6[ostride * 2] = v527;
    v6[ostride * 10] = v519;
    v6[ostride] = v528;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs11(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v278 = 1.1000000000000001e+00F;
    float v283 = -3.3166247903554003e-01F;
    float v290 = 5.1541501300188641e-01F;
    float v295 = 9.4125353283118118e-01F;
    float v300 = 1.4143537075597825e+00F;
    float v305 = 8.5949297361449750e-01F;
    float v310 = 4.2314838273285138e-02F;
    float v315 = 3.8639279888589606e-01F;
    float v320 = 5.1254589567200015e-01F;
    float v325 = 1.0702757469471715e+00F;
    float v330 = 5.5486073394528512e-01F;
    float v335 = -1.2412944743900585e+00F;
    float v342 = -2.0897833842005756e-01F;
    float v349 = -3.7415717312460811e-01F;
    float v356 = -4.9929922194110327e-02F;
    float v363 = -6.5815896284539266e-01F;
    float v370 = -6.3306543373877577e-01F;
    float v377 = -1.0822460581641109e+00F;
    float v384 = -8.1720737907134022e-01F;
    float v391 = -4.2408709531871824e-01F;
    const float32x2_t *v527 = &v5[v0];
    float32x2_t *v741 = &v6[v2];
    int64_t v33 = v0 * 10;
    int64_t v47 = v0 * 2;
    int64_t v61 = v0 * 9;
    int64_t v75 = v0 * 3;
    int64_t v89 = v0 * 8;
    int64_t v103 = v0 * 4;
    int64_t v117 = v0 * 7;
    int64_t v131 = v0 * 5;
    int64_t v145 = v0 * 6;
    int64_t v167 = v10 * 9;
    int64_t v181 = v10 * 8;
    int64_t v188 = v10 * 2;
    int64_t v195 = v10 * 7;
    int64_t v202 = v10 * 3;
    int64_t v209 = v10 * 6;
    int64_t v216 = v10 * 4;
    int64_t v223 = v10 * 5;
    int64_t v224 = v13 * 10;
    float v286 = v4 * v283;
    float v338 = v4 * v335;
    float v345 = v4 * v342;
    float v352 = v4 * v349;
    float v359 = v4 * v356;
    float v366 = v4 * v363;
    float v373 = v4 * v370;
    float v380 = v4 * v377;
    float v387 = v4 * v384;
    float v394 = v4 * v391;
    int64_t v452 = v2 * 10;
    int64_t v459 = v2 * 9;
    int64_t v466 = v2 * 8;
    int64_t v473 = v2 * 7;
    int64_t v480 = v2 * 6;
    int64_t v487 = v2 * 5;
    int64_t v494 = v2 * 4;
    int64_t v501 = v2 * 3;
    int64_t v508 = v2 * 2;
    const float32x2_t *v620 = &v5[0];
    svint64_t v621 = svindex_s64(0, v1);
    svfloat32_t v624 = svdup_n_f32(v278);
    svfloat32_t v626 = svdup_n_f32(v290);
    svfloat32_t v627 = svdup_n_f32(v295);
    svfloat32_t v628 = svdup_n_f32(v300);
    svfloat32_t v629 = svdup_n_f32(v305);
    svfloat32_t v630 = svdup_n_f32(v310);
    svfloat32_t v631 = svdup_n_f32(v315);
    svfloat32_t v632 = svdup_n_f32(v320);
    svfloat32_t v633 = svdup_n_f32(v325);
    svfloat32_t v634 = svdup_n_f32(v330);
    float32x2_t *v651 = &v6[0];
    svint64_t v742 = svindex_s64(0, v3);
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v224]));
    int64_t v169 = v167 + v224;
    int64_t v176 = v10 + v224;
    int64_t v183 = v181 + v224;
    int64_t v190 = v188 + v224;
    int64_t v197 = v195 + v224;
    int64_t v204 = v202 + v224;
    int64_t v211 = v209 + v224;
    int64_t v218 = v216 + v224;
    int64_t v225 = v223 + v224;
    svfloat32_t v529 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v527), v621));
    const float32x2_t *v537 = &v5[v33];
    const float32x2_t *v546 = &v5[v47];
    const float32x2_t *v555 = &v5[v61];
    const float32x2_t *v564 = &v5[v75];
    const float32x2_t *v573 = &v5[v89];
    const float32x2_t *v582 = &v5[v103];
    const float32x2_t *v591 = &v5[v117];
    const float32x2_t *v600 = &v5[v131];
    const float32x2_t *v609 = &v5[v145];
    svfloat32_t v622 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v620), v621));
    svfloat32_t v625 = svdup_n_f32(v286);
    svfloat32_t v635 = svdup_n_f32(v338);
    svfloat32_t v636 = svdup_n_f32(v345);
    svfloat32_t v637 = svdup_n_f32(v352);
    svfloat32_t v638 = svdup_n_f32(v359);
    svfloat32_t v639 = svdup_n_f32(v366);
    svfloat32_t v640 = svdup_n_f32(v373);
    svfloat32_t v641 = svdup_n_f32(v380);
    svfloat32_t v642 = svdup_n_f32(v387);
    svfloat32_t v643 = svdup_n_f32(v394);
    float32x2_t *v660 = &v6[v452];
    float32x2_t *v669 = &v6[v459];
    float32x2_t *v678 = &v6[v466];
    float32x2_t *v687 = &v6[v473];
    float32x2_t *v696 = &v6[v480];
    float32x2_t *v705 = &v6[v487];
    float32x2_t *v714 = &v6[v494];
    float32x2_t *v723 = &v6[v501];
    float32x2_t *v732 = &v6[v508];
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v529, v163, 0),
                     v529, v163, 90);
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t v177 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v176]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v191 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v190]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v219 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v218]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v539 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v537), v621));
    svfloat32_t v548 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v546), v621));
    svfloat32_t v557 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v555), v621));
    svfloat32_t v566 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v564), v621));
    svfloat32_t v575 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v573), v621));
    svfloat32_t v584 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v582), v621));
    svfloat32_t v593 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v591), v621));
    svfloat32_t v602 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v600), v621));
    svfloat32_t v611 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v609), v621));
    svfloat32_t zero171 = svdup_n_f32(0);
    svfloat32_t v171 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero171, v539, v170, 0),
                     v539, v170, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero178, v548, v177, 0),
                     v548, v177, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero185, v557, v184, 0),
                     v557, v184, 90);
    svfloat32_t zero192 = svdup_n_f32(0);
    svfloat32_t v192 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero192, v566, v191, 0),
                     v566, v191, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v575, v198, 0),
                     v575, v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v584, v205, 0),
                     v584, v205, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v593, v212, 0),
                     v593, v212, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero220, v602, v219, 0),
                     v602, v219, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero227, v611, v226, 0),
                     v611, v226, 90);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v164, v171);
    svfloat32_t v229 = svadd_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v230 = svadd_f32_x(svptrue_b32(), v192, v199);
    svfloat32_t v231 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v232 = svadd_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v233 = svsub_f32_x(svptrue_b32(), v164, v171);
    svfloat32_t v234 = svsub_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v235 = svsub_f32_x(svptrue_b32(), v192, v199);
    svfloat32_t v236 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v237 = svsub_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v228, v229);
    svfloat32_t v239 = svadd_f32_x(svptrue_b32(), v230, v232);
    svfloat32_t v241 = svsub_f32_x(svptrue_b32(), v234, v235);
    svfloat32_t v242 = svadd_f32_x(svptrue_b32(), v233, v237);
    svfloat32_t v254 = svsub_f32_x(svptrue_b32(), v229, v231);
    svfloat32_t v255 = svsub_f32_x(svptrue_b32(), v228, v231);
    svfloat32_t v256 = svsub_f32_x(svptrue_b32(), v229, v228);
    svfloat32_t v257 = svsub_f32_x(svptrue_b32(), v232, v231);
    svfloat32_t v258 = svsub_f32_x(svptrue_b32(), v230, v231);
    svfloat32_t v259 = svsub_f32_x(svptrue_b32(), v232, v230);
    svfloat32_t v260 = svsub_f32_x(svptrue_b32(), v229, v232);
    svfloat32_t v261 = svsub_f32_x(svptrue_b32(), v228, v230);
    svfloat32_t v263 = svadd_f32_x(svptrue_b32(), v234, v236);
    svfloat32_t v264 = svsub_f32_x(svptrue_b32(), v233, v236);
    svfloat32_t v265 = svadd_f32_x(svptrue_b32(), v233, v234);
    svfloat32_t v266 = svsub_f32_x(svptrue_b32(), v236, v237);
    svfloat32_t v267 = svsub_f32_x(svptrue_b32(), v235, v236);
    svfloat32_t v268 = svsub_f32_x(svptrue_b32(), v235, v237);
    svfloat32_t v269 = svadd_f32_x(svptrue_b32(), v234, v237);
    svfloat32_t v270 = svsub_f32_x(svptrue_b32(), v233, v235);
    svfloat32_t v240 = svadd_f32_x(svptrue_b32(), v231, v238);
    svfloat32_t v252 = svsub_f32_x(svptrue_b32(), v241, v242);
    svfloat32_t v262 = svsub_f32_x(svptrue_b32(), v239, v238);
    svfloat32_t v271 = svadd_f32_x(svptrue_b32(), v241, v242);
    svfloat32_t v298 = svmul_f32_x(svptrue_b32(), v255, v627);
    svfloat32_t v303 = svmul_f32_x(svptrue_b32(), v256, v628);
    svfloat32_t v313 = svmul_f32_x(svptrue_b32(), v258, v630);
    svfloat32_t v318 = svmul_f32_x(svptrue_b32(), v259, v631);
    svfloat32_t zero340 = svdup_n_f32(0);
    svfloat32_t v340 = svcmla_f32_x(pred_full, zero340, v635, v263, 90);
    svfloat32_t zero354 = svdup_n_f32(0);
    svfloat32_t v354 = svcmla_f32_x(pred_full, zero354, v637, v265, 90);
    svfloat32_t zero361 = svdup_n_f32(0);
    svfloat32_t v361 = svcmla_f32_x(pred_full, zero361, v638, v266, 90);
    svfloat32_t zero375 = svdup_n_f32(0);
    svfloat32_t v375 = svcmla_f32_x(pred_full, zero375, v640, v268, 90);
    svfloat32_t zero382 = svdup_n_f32(0);
    svfloat32_t v382 = svcmla_f32_x(pred_full, zero382, v641, v269, 90);
    svfloat32_t v243 = svadd_f32_x(svptrue_b32(), v240, v239);
    svfloat32_t v253 = svsub_f32_x(svptrue_b32(), v252, v236);
    svfloat32_t v333 = svmul_f32_x(svptrue_b32(), v262, v634);
    svfloat32_t zero396 = svdup_n_f32(0);
    svfloat32_t v396 = svcmla_f32_x(pred_full, zero396, v643, v271, 90);
    svfloat32_t v398 = svmla_f32_x(pred_full, v298, v254, v626);
    svfloat32_t v399 = svmla_f32_x(pred_full, v303, v255, v627);
    svfloat32_t v400 = svnmls_f32_x(pred_full, v303, v254, v626);
    svfloat32_t v401 = svmla_f32_x(pred_full, v313, v257, v629);
    svfloat32_t v402 = svmla_f32_x(pred_full, v318, v258, v630);
    svfloat32_t v403 = svnmls_f32_x(pred_full, v318, v257, v629);
    svfloat32_t v406 = svcmla_f32_x(pred_full, v354, v636, v264, 90);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v340, v354);
    svfloat32_t v408 = svcmla_f32_x(pred_full, v375, v639, v267, 90);
    svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v361, v375);
    svfloat32_t v251 = svadd_f32_x(svptrue_b32(), v622, v243);
    svfloat32_t zero288 = svdup_n_f32(0);
    svfloat32_t v288 = svcmla_f32_x(pred_full, zero288, v625, v253, 90);
    svfloat32_t v404 = svmla_f32_x(pred_full, v333, v261, v633);
    svfloat32_t v405 = svmla_f32_x(pred_full, v333, v260, v632);
    svfloat32_t v410 = svcmla_f32_x(pred_full, v396, v642, v270, 90);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v382, v396);
    svfloat32_t v430 = svadd_f32_x(svptrue_b32(), v406, v407);
    svfloat32_t v397 = svmls_f32_x(pred_full, v251, v243, v624);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v402, v404);
    svfloat32_t v422 = svadd_f32_x(svptrue_b32(), v288, v408);
    svfloat32_t v424 = svsub_f32_x(svptrue_b32(), v410, v406);
    svfloat32_t v426 = svadd_f32_x(svptrue_b32(), v288, v411);
    svfloat32_t v428 = svsub_f32_x(svptrue_b32(), v411, v407);
    svfloat32_t v431 = svadd_f32_x(svptrue_b32(), v430, v408);
    svst1_scatter_s64index_f64(pred_full, (double *)(v651), v742,
                               svreinterpret_f64_f32(v251));
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v412, v397);
    svfloat32_t v414 = svsub_f32_x(svptrue_b32(), v397, v399);
    svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v397, v403);
    svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v397, v400);
    svfloat32_t v420 = svadd_f32_x(svptrue_b32(), v397, v398);
    svfloat32_t v423 = svadd_f32_x(svptrue_b32(), v422, v410);
    svfloat32_t v425 = svsub_f32_x(svptrue_b32(), v424, v288);
    svfloat32_t v427 = svadd_f32_x(svptrue_b32(), v426, v409);
    svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v428, v288);
    svfloat32_t v432 = svadd_f32_x(svptrue_b32(), v431, v409);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v414, v404);
    svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v416, v405);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v418, v405);
    svfloat32_t v421 = svsub_f32_x(svptrue_b32(), v420, v401);
    svfloat32_t v433 = svsub_f32_x(svptrue_b32(), v432, v288);
    svfloat32_t v435 = svadd_f32_x(svptrue_b32(), v413, v423);
    svfloat32_t v442 = svsub_f32_x(svptrue_b32(), v413, v423);
    svfloat32_t v434 = svadd_f32_x(svptrue_b32(), v421, v433);
    svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v415, v425);
    svfloat32_t v437 = svsub_f32_x(svptrue_b32(), v417, v427);
    svfloat32_t v438 = svadd_f32_x(svptrue_b32(), v419, v429);
    svfloat32_t v439 = svsub_f32_x(svptrue_b32(), v419, v429);
    svfloat32_t v440 = svadd_f32_x(svptrue_b32(), v417, v427);
    svfloat32_t v441 = svsub_f32_x(svptrue_b32(), v415, v425);
    svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v421, v433);
    svst1_scatter_s64index_f64(pred_full, (double *)(v669), v742,
                               svreinterpret_f64_f32(v435));
    svst1_scatter_s64index_f64(pred_full, (double *)(v732), v742,
                               svreinterpret_f64_f32(v442));
    svst1_scatter_s64index_f64(pred_full, (double *)(v660), v742,
                               svreinterpret_f64_f32(v434));
    svst1_scatter_s64index_f64(pred_full, (double *)(v678), v742,
                               svreinterpret_f64_f32(v436));
    svst1_scatter_s64index_f64(pred_full, (double *)(v687), v742,
                               svreinterpret_f64_f32(v437));
    svst1_scatter_s64index_f64(pred_full, (double *)(v696), v742,
                               svreinterpret_f64_f32(v438));
    svst1_scatter_s64index_f64(pred_full, (double *)(v705), v742,
                               svreinterpret_f64_f32(v439));
    svst1_scatter_s64index_f64(pred_full, (double *)(v714), v742,
                               svreinterpret_f64_f32(v440));
    svst1_scatter_s64index_f64(pred_full, (double *)(v723), v742,
                               svreinterpret_f64_f32(v441));
    svst1_scatter_s64index_f64(pred_full, (double *)(v741), v742,
                               svreinterpret_f64_f32(v443));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs12(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v242 = v5[istride];
    float v353 = 1.0000000000000000e+00F;
    float v354 = -1.0000000000000000e+00F;
    float v380 = -1.4999999999999998e+00F;
    float v381 = 1.4999999999999998e+00F;
    float v409 = 8.6602540378443871e-01F;
    float32x2_t v412 = (float32x2_t){v4, v4};
    float v417 = -8.6602540378443871e-01F;
    float32x2_t v279 = vtrn1_f32(v242, v242);
    float32x2_t v280 = vtrn2_f32(v242, v242);
    float32x2_t v324 = v5[0];
    float32x2_t v355 = (float32x2_t){v353, v354};
    float32x2_t v378 = (float32x2_t){v380, v380};
    float32x2_t v382 = (float32x2_t){v380, v381};
    float32x2_t v411 = (float32x2_t){v409, v417};
    float32x2_t v418 = (float32x2_t){v417, v417};
    float32x2_t v20 = v5[istride * 4];
    float32x2_t v38 = v5[istride * 8];
    int64_t v55 = 6 + j * 22;
    int64_t v68 = 14 + j * 22;
    float32x2_t v82 = v5[istride * 7];
    float32x2_t v100 = v5[istride * 11];
    int64_t v117 = 12 + j * 22;
    int64_t v130 = 20 + j * 22;
    float32x2_t v144 = v5[istride * 3];
    int64_t v148 = 4 + j * 22;
    float32x2_t v162 = v5[istride * 10];
    float32x2_t v180 = v5[istride * 2];
    int64_t v197 = 18 + j * 22;
    int64_t v210 = 2 + j * 22;
    float32x2_t v224 = v5[istride * 6];
    int64_t v228 = 10 + j * 22;
    float32x2_t v260 = v5[istride * 5];
    float32x2_t v278 = v7[j * 22];
    int64_t v282 = j * 22 + 1;
    int64_t v290 = 8 + j * 22;
    float32x2_t v304 = v5[istride * 9];
    int64_t v308 = 16 + j * 22;
    float32x2_t v357 = vmul_f32(v412, v355);
    float32x2_t v384 = vmul_f32(v412, v382);
    float32x2_t v413 = vmul_f32(v412, v411);
    float32x2_t v56 = v7[v55];
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    int64_t v60 = v55 + 1;
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v144, v144);
    float32x2_t v151 = vtrn2_f32(v144, v144);
    int64_t v153 = v148 + 1;
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vtrn1_f32(v162, v162);
    float32x2_t v200 = vtrn2_f32(v162, v162);
    int64_t v202 = v197 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v180, v180);
    float32x2_t v213 = vtrn2_f32(v180, v180);
    int64_t v215 = v210 + 1;
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vtrn1_f32(v224, v224);
    float32x2_t v231 = vtrn2_f32(v224, v224);
    int64_t v233 = v228 + 1;
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vtrn1_f32(v260, v260);
    float32x2_t v293 = vtrn2_f32(v260, v260);
    int64_t v295 = v290 + 1;
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vtrn1_f32(v304, v304);
    float32x2_t v311 = vtrn2_f32(v304, v304);
    int64_t v313 = v308 + 1;
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v203 = v7[v202];
    float32x2_t v204 = vmul_f32(v199, v198);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v234 = v7[v233];
    float32x2_t v235 = vmul_f32(v230, v229);
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vmul_f32(v292, v291);
    float32x2_t v314 = v7[v313];
    float32x2_t v315 = vmul_f32(v310, v309);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v206 = vfma_f32(v204, v200, v203);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v237 = vfma_f32(v235, v231, v234);
    float32x2_t v299 = vfma_f32(v297, v293, v296);
    float32x2_t v317 = vfma_f32(v315, v311, v314);
    float32x2_t v318 = vadd_f32(v64, v77);
    float32x2_t v319 = vsub_f32(v64, v77);
    float32x2_t v326 = vadd_f32(v126, v139);
    float32x2_t v327 = vsub_f32(v126, v139);
    float32x2_t v329 = vadd_f32(v206, v219);
    float32x2_t v330 = vsub_f32(v206, v219);
    float32x2_t v332 = vadd_f32(v286, v299);
    float32x2_t v333 = vsub_f32(v286, v299);
    float32x2_t v325 = vadd_f32(v318, v324);
    float32x2_t v328 = vadd_f32(v326, v157);
    float32x2_t v331 = vadd_f32(v329, v237);
    float32x2_t v334 = vadd_f32(v332, v317);
    float32x2_t v362 = vadd_f32(v318, v329);
    float32x2_t v363 = vsub_f32(v318, v329);
    float32x2_t v364 = vadd_f32(v326, v332);
    float32x2_t v365 = vsub_f32(v326, v332);
    float32x2_t v389 = vadd_f32(v319, v330);
    float32x2_t v390 = vsub_f32(v319, v330);
    float32x2_t v391 = vadd_f32(v327, v333);
    float32x2_t v392 = vsub_f32(v327, v333);
    float32x2_t v335 = vadd_f32(v325, v331);
    float32x2_t v336 = vsub_f32(v325, v331);
    float32x2_t v337 = vadd_f32(v328, v334);
    float32x2_t v338 = vsub_f32(v328, v334);
    float32x2_t v366 = vadd_f32(v362, v364);
    float32x2_t v367 = vsub_f32(v362, v364);
    float32x2_t v379 = vmul_f32(v363, v378);
    float32x2_t v385 = vrev64_f32(v365);
    float32x2_t v393 = vadd_f32(v389, v391);
    float32x2_t v394 = vsub_f32(v389, v391);
    float32x2_t v414 = vrev64_f32(v390);
    float32x2_t v419 = vmul_f32(v392, v418);
    float32x2_t v339 = vadd_f32(v335, v337);
    float32x2_t v340 = vsub_f32(v335, v337);
    float32x2_t v358 = vrev64_f32(v338);
    float32x2_t v371 = vmul_f32(v366, v378);
    float32x2_t v375 = vmul_f32(v367, v378);
    float32x2_t v386 = vmul_f32(v385, v384);
    float32x2_t v400 = vrev64_f32(v393);
    float32x2_t v407 = vrev64_f32(v394);
    float32x2_t v415 = vmul_f32(v414, v413);
    float32x2_t v359 = vmul_f32(v358, v357);
    float32x2_t v387 = vadd_f32(v379, v386);
    float32x2_t v388 = vsub_f32(v379, v386);
    float32x2_t v401 = vmul_f32(v400, v413);
    float32x2_t v408 = vmul_f32(v407, v413);
    float32x2_t v420 = vadd_f32(v415, v419);
    float32x2_t v421 = vsub_f32(v415, v419);
    float32x2_t v422 = vadd_f32(v339, v371);
    v6[0] = v339;
    float32x2_t v458 = vadd_f32(v340, v375);
    v6[ostride * 6] = v340;
    float32x2_t v360 = vadd_f32(v336, v359);
    float32x2_t v361 = vsub_f32(v336, v359);
    float32x2_t v423 = vadd_f32(v422, v401);
    float32x2_t v424 = vsub_f32(v422, v401);
    float32x2_t v459 = vadd_f32(v458, v408);
    float32x2_t v460 = vsub_f32(v458, v408);
    v6[ostride * 4] = v424;
    v6[ostride * 8] = v423;
    float32x2_t v440 = vadd_f32(v361, v388);
    v6[ostride * 9] = v361;
    v6[ostride * 10] = v460;
    v6[ostride * 2] = v459;
    float32x2_t v476 = vadd_f32(v360, v387);
    v6[ostride * 3] = v360;
    float32x2_t v441 = vadd_f32(v440, v421);
    float32x2_t v442 = vsub_f32(v440, v421);
    float32x2_t v477 = vadd_f32(v476, v420);
    float32x2_t v478 = vsub_f32(v476, v420);
    v6[ostride] = v442;
    v6[ostride * 5] = v441;
    v6[ostride * 7] = v478;
    v6[ostride * 11] = v477;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs12(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v269 = -1.0000000000000000e+00F;
    float v294 = -1.4999999999999998e+00F;
    float v299 = 1.4999999999999998e+00F;
    float v335 = -8.6602540378443871e-01F;
    const float32x2_t *v515 = &v5[v0];
    float32x2_t *v603 = &v6[v2];
    int64_t v19 = v0 * 4;
    int64_t v33 = v0 * 8;
    int64_t v48 = v10 * 3;
    int64_t v55 = v10 * 7;
    int64_t v61 = v0 * 7;
    int64_t v75 = v0 * 11;
    int64_t v90 = v10 * 6;
    int64_t v97 = v10 * 10;
    int64_t v103 = v0 * 3;
    int64_t v111 = v10 * 2;
    int64_t v117 = v0 * 10;
    int64_t v131 = v0 * 2;
    int64_t v146 = v10 * 9;
    int64_t v159 = v0 * 6;
    int64_t v167 = v10 * 5;
    int64_t v187 = v0 * 5;
    int64_t v209 = v10 * 4;
    int64_t v215 = v0 * 9;
    int64_t v223 = v10 * 8;
    int64_t v224 = v13 * 11;
    float v272 = v4 * v269;
    float v302 = v4 * v299;
    float v331 = v4 * v335;
    int64_t v352 = v2 * 4;
    int64_t v359 = v2 * 8;
    int64_t v369 = v2 * 9;
    int64_t v383 = v2 * 5;
    int64_t v393 = v2 * 6;
    int64_t v400 = v2 * 10;
    int64_t v407 = v2 * 2;
    int64_t v417 = v2 * 3;
    int64_t v424 = v2 * 7;
    int64_t v431 = v2 * 11;
    const float32x2_t *v545 = &v5[0];
    svint64_t v546 = svindex_s64(0, v1);
    svfloat32_t v554 = svdup_n_f32(v294);
    svfloat32_t v559 = svdup_n_f32(v335);
    float32x2_t *v567 = &v6[0];
    svint64_t v667 = svindex_s64(0, v3);
    int64_t v50 = v48 + v224;
    int64_t v57 = v55 + v224;
    int64_t v92 = v90 + v224;
    int64_t v99 = v97 + v224;
    int64_t v113 = v111 + v224;
    int64_t v148 = v146 + v224;
    int64_t v155 = v10 + v224;
    int64_t v169 = v167 + v224;
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v224]));
    int64_t v211 = v209 + v224;
    int64_t v225 = v223 + v224;
    const float32x2_t *v443 = &v5[v19];
    const float32x2_t *v452 = &v5[v33];
    const float32x2_t *v461 = &v5[v61];
    const float32x2_t *v470 = &v5[v75];
    const float32x2_t *v479 = &v5[v103];
    const float32x2_t *v488 = &v5[v117];
    const float32x2_t *v497 = &v5[v131];
    const float32x2_t *v506 = &v5[v159];
    svfloat32_t v517 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v515), v546));
    const float32x2_t *v525 = &v5[v187];
    const float32x2_t *v535 = &v5[v215];
    svfloat32_t v547 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v545), v546));
    svfloat32_t v551 = svdup_n_f32(v272);
    svfloat32_t v555 = svdup_n_f32(v302);
    svfloat32_t v558 = svdup_n_f32(v331);
    float32x2_t *v576 = &v6[v352];
    float32x2_t *v585 = &v6[v359];
    float32x2_t *v594 = &v6[v369];
    float32x2_t *v612 = &v6[v383];
    float32x2_t *v621 = &v6[v393];
    float32x2_t *v630 = &v6[v400];
    float32x2_t *v639 = &v6[v407];
    float32x2_t *v648 = &v6[v417];
    float32x2_t *v657 = &v6[v424];
    float32x2_t *v666 = &v6[v431];
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v50]));
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v149 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v148]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v517, v205, 0),
                     v517, v205, 90);
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v445 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v443), v546));
    svfloat32_t v454 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v452), v546));
    svfloat32_t v463 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v461), v546));
    svfloat32_t v472 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v470), v546));
    svfloat32_t v481 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v479), v546));
    svfloat32_t v490 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v488), v546));
    svfloat32_t v499 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v497), v546));
    svfloat32_t v508 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v506), v546));
    svfloat32_t v527 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v525), v546));
    svfloat32_t v537 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v535), v546));
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v445, v51, 0),
                     v445, v51, 90);
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v454, v58, 0),
                     v454, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v463, v93, 0),
                     v463, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v472, v100, 0),
                     v472, v100, 90);
    svfloat32_t zero150 = svdup_n_f32(0);
    svfloat32_t v150 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero150, v490, v149, 0),
                     v490, v149, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v499, v156, 0),
                     v499, v156, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v527, v212, 0),
                     v527, v212, 90);
    svfloat32_t v228 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v229 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v238 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v239 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v241 = svadd_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v242 = svsub_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v244 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v245 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v237 = svadd_f32_x(svptrue_b32(), v228, v547);
    svfloat32_t v240 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v238, v481, v114, 0),
                     v481, v114, 90);
    svfloat32_t v243 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v241, v508, v170, 0),
                     v508, v170, 90);
    svfloat32_t v246 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v244, v537, v226, 0),
                     v537, v226, 90);
    svfloat32_t v277 = svadd_f32_x(svptrue_b32(), v228, v241);
    svfloat32_t v278 = svsub_f32_x(svptrue_b32(), v228, v241);
    svfloat32_t v279 = svadd_f32_x(svptrue_b32(), v238, v244);
    svfloat32_t v280 = svsub_f32_x(svptrue_b32(), v238, v244);
    svfloat32_t v307 = svadd_f32_x(svptrue_b32(), v229, v242);
    svfloat32_t v308 = svsub_f32_x(svptrue_b32(), v229, v242);
    svfloat32_t v309 = svadd_f32_x(svptrue_b32(), v239, v245);
    svfloat32_t v310 = svsub_f32_x(svptrue_b32(), v239, v245);
    svfloat32_t v247 = svadd_f32_x(svptrue_b32(), v237, v243);
    svfloat32_t v248 = svsub_f32_x(svptrue_b32(), v237, v243);
    svfloat32_t v249 = svadd_f32_x(svptrue_b32(), v240, v246);
    svfloat32_t v250 = svsub_f32_x(svptrue_b32(), v240, v246);
    svfloat32_t v281 = svadd_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t v282 = svsub_f32_x(svptrue_b32(), v277, v279);
    svfloat32_t zero304 = svdup_n_f32(0);
    svfloat32_t v304 = svcmla_f32_x(pred_full, zero304, v555, v280, 90);
    svfloat32_t v311 = svadd_f32_x(svptrue_b32(), v307, v309);
    svfloat32_t v312 = svsub_f32_x(svptrue_b32(), v307, v309);
    svfloat32_t zero333 = svdup_n_f32(0);
    svfloat32_t v333 = svcmla_f32_x(pred_full, zero333, v558, v308, 90);
    svfloat32_t v251 = svadd_f32_x(svptrue_b32(), v247, v249);
    svfloat32_t v252 = svsub_f32_x(svptrue_b32(), v247, v249);
    svfloat32_t zero274 = svdup_n_f32(0);
    svfloat32_t v274 = svcmla_f32_x(pred_full, zero274, v551, v250, 90);
    svfloat32_t v305 = svmla_f32_x(pred_full, v304, v278, v554);
    svfloat32_t v306 = svnmls_f32_x(pred_full, v304, v278, v554);
    svfloat32_t zero319 = svdup_n_f32(0);
    svfloat32_t v319 = svcmla_f32_x(pred_full, zero319, v558, v311, 90);
    svfloat32_t zero326 = svdup_n_f32(0);
    svfloat32_t v326 = svcmla_f32_x(pred_full, zero326, v558, v312, 90);
    svfloat32_t v339 = svmla_f32_x(pred_full, v333, v310, v559);
    svfloat32_t v340 = svmls_f32_x(pred_full, v333, v310, v559);
    svfloat32_t v275 = svadd_f32_x(svptrue_b32(), v248, v274);
    svfloat32_t v276 = svsub_f32_x(svptrue_b32(), v248, v274);
    svfloat32_t v341 = svmla_f32_x(pred_full, v251, v281, v554);
    svfloat32_t v389 = svmla_f32_x(pred_full, v252, v282, v554);
    svst1_scatter_s64index_f64(pred_full, (double *)(v567), v667,
                               svreinterpret_f64_f32(v251));
    svst1_scatter_s64index_f64(pred_full, (double *)(v621), v667,
                               svreinterpret_f64_f32(v252));
    svfloat32_t v342 = svadd_f32_x(svptrue_b32(), v341, v319);
    svfloat32_t v343 = svsub_f32_x(svptrue_b32(), v341, v319);
    svfloat32_t v365 = svadd_f32_x(svptrue_b32(), v276, v306);
    svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v389, v326);
    svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v389, v326);
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v275, v305);
    svst1_scatter_s64index_f64(pred_full, (double *)(v594), v667,
                               svreinterpret_f64_f32(v276));
    svst1_scatter_s64index_f64(pred_full, (double *)(v648), v667,
                               svreinterpret_f64_f32(v275));
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v365, v340);
    svfloat32_t v367 = svsub_f32_x(svptrue_b32(), v365, v340);
    svfloat32_t v414 = svadd_f32_x(svptrue_b32(), v413, v339);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v413, v339);
    svst1_scatter_s64index_f64(pred_full, (double *)(v576), v667,
                               svreinterpret_f64_f32(v343));
    svst1_scatter_s64index_f64(pred_full, (double *)(v585), v667,
                               svreinterpret_f64_f32(v342));
    svst1_scatter_s64index_f64(pred_full, (double *)(v630), v667,
                               svreinterpret_f64_f32(v391));
    svst1_scatter_s64index_f64(pred_full, (double *)(v639), v667,
                               svreinterpret_f64_f32(v390));
    svst1_scatter_s64index_f64(pred_full, (double *)(v603), v667,
                               svreinterpret_f64_f32(v367));
    svst1_scatter_s64index_f64(pred_full, (double *)(v612), v667,
                               svreinterpret_f64_f32(v366));
    svst1_scatter_s64index_f64(pred_full, (double *)(v657), v667,
                               svreinterpret_f64_f32(v415));
    svst1_scatter_s64index_f64(pred_full, (double *)(v666), v667,
                               svreinterpret_f64_f32(v414));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs13(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v441 = 1.0833333333333333e+00F;
    float v445 = -3.0046260628866578e-01F;
    float v448 = 7.4927933062613905e-01F;
    float v449 = -7.4927933062613905e-01F;
    float v455 = 4.0100212832186721e-01F;
    float v456 = -4.0100212832186721e-01F;
    float v462 = 5.7514072947400308e-01F;
    float v463 = -5.7514072947400308e-01F;
    float v470 = 5.2422663952658211e-01F;
    float v474 = 5.1652078062348972e-01F;
    float v478 = 7.7058589030924258e-03F;
    float v482 = 4.2763404682656941e-01F;
    float v486 = 1.5180597207438440e-01F;
    float v490 = 5.7944001890096386e-01F;
    float v493 = 1.1543953381323635e+00F;
    float v494 = -1.1543953381323635e+00F;
    float v500 = 9.0655220171271012e-01F;
    float v501 = -9.0655220171271012e-01F;
    float v507 = 8.1857027294591811e-01F;
    float v508 = -8.1857027294591811e-01F;
    float v514 = 1.1971367726043427e+00F;
    float v515 = -1.1971367726043427e+00F;
    float v521 = 8.6131170741789742e-01F;
    float v522 = -8.6131170741789742e-01F;
    float v528 = 1.1091548438375507e+00F;
    float v529 = -1.1091548438375507e+00F;
    float v535 = 4.2741434471979367e-02F;
    float v536 = -4.2741434471979367e-02F;
    float v542 = -4.5240494294812715e-02F;
    float v543 = 4.5240494294812715e-02F;
    float v549 = 2.9058457089163264e-01F;
    float v550 = -2.9058457089163264e-01F;
    float32x2_t v552 = (float32x2_t){v4, v4};
    float32x2_t v237 = vtrn1_f32(v20, v20);
    float32x2_t v238 = vtrn2_f32(v20, v20);
    float32x2_t v427 = v5[0];
    float32x2_t v442 = (float32x2_t){v441, v441};
    float32x2_t v446 = (float32x2_t){v445, v445};
    float32x2_t v450 = (float32x2_t){v448, v449};
    float32x2_t v457 = (float32x2_t){v455, v456};
    float32x2_t v464 = (float32x2_t){v462, v463};
    float32x2_t v471 = (float32x2_t){v470, v470};
    float32x2_t v475 = (float32x2_t){v474, v474};
    float32x2_t v479 = (float32x2_t){v478, v478};
    float32x2_t v483 = (float32x2_t){v482, v482};
    float32x2_t v487 = (float32x2_t){v486, v486};
    float32x2_t v491 = (float32x2_t){v490, v490};
    float32x2_t v495 = (float32x2_t){v493, v494};
    float32x2_t v502 = (float32x2_t){v500, v501};
    float32x2_t v509 = (float32x2_t){v507, v508};
    float32x2_t v516 = (float32x2_t){v514, v515};
    float32x2_t v523 = (float32x2_t){v521, v522};
    float32x2_t v530 = (float32x2_t){v528, v529};
    float32x2_t v537 = (float32x2_t){v535, v536};
    float32x2_t v544 = (float32x2_t){v542, v543};
    float32x2_t v551 = (float32x2_t){v549, v550};
    float32x2_t v38 = v5[istride * 12];
    float32x2_t v56 = v5[istride * 2];
    float32x2_t v74 = v5[istride * 11];
    float32x2_t v92 = v5[istride * 3];
    float32x2_t v110 = v5[istride * 10];
    float32x2_t v128 = v5[istride * 4];
    float32x2_t v146 = v5[istride * 9];
    float32x2_t v164 = v5[istride * 5];
    float32x2_t v182 = v5[istride * 8];
    float32x2_t v200 = v5[istride * 6];
    float32x2_t v218 = v5[istride * 7];
    float32x2_t v236 = v7[j * 24];
    int64_t v240 = j * 24 + 1;
    int64_t v248 = 22 + j * 24;
    int64_t v261 = 2 + j * 24;
    int64_t v274 = 20 + j * 24;
    int64_t v287 = 4 + j * 24;
    int64_t v300 = 18 + j * 24;
    int64_t v313 = 6 + j * 24;
    int64_t v326 = 16 + j * 24;
    int64_t v339 = 8 + j * 24;
    int64_t v352 = 14 + j * 24;
    int64_t v365 = 10 + j * 24;
    int64_t v378 = 12 + j * 24;
    float32x2_t v452 = vmul_f32(v552, v450);
    float32x2_t v459 = vmul_f32(v552, v457);
    float32x2_t v466 = vmul_f32(v552, v464);
    float32x2_t v497 = vmul_f32(v552, v495);
    float32x2_t v504 = vmul_f32(v552, v502);
    float32x2_t v511 = vmul_f32(v552, v509);
    float32x2_t v518 = vmul_f32(v552, v516);
    float32x2_t v525 = vmul_f32(v552, v523);
    float32x2_t v532 = vmul_f32(v552, v530);
    float32x2_t v539 = vmul_f32(v552, v537);
    float32x2_t v546 = vmul_f32(v552, v544);
    float32x2_t v553 = vmul_f32(v552, v551);
    float32x2_t v241 = v7[v240];
    float32x2_t v242 = vmul_f32(v237, v236);
    float32x2_t v249 = v7[v248];
    float32x2_t v250 = vtrn1_f32(v38, v38);
    float32x2_t v251 = vtrn2_f32(v38, v38);
    int64_t v253 = v248 + 1;
    float32x2_t v262 = v7[v261];
    float32x2_t v263 = vtrn1_f32(v56, v56);
    float32x2_t v264 = vtrn2_f32(v56, v56);
    int64_t v266 = v261 + 1;
    float32x2_t v275 = v7[v274];
    float32x2_t v276 = vtrn1_f32(v74, v74);
    float32x2_t v277 = vtrn2_f32(v74, v74);
    int64_t v279 = v274 + 1;
    float32x2_t v288 = v7[v287];
    float32x2_t v289 = vtrn1_f32(v92, v92);
    float32x2_t v290 = vtrn2_f32(v92, v92);
    int64_t v292 = v287 + 1;
    float32x2_t v301 = v7[v300];
    float32x2_t v302 = vtrn1_f32(v110, v110);
    float32x2_t v303 = vtrn2_f32(v110, v110);
    int64_t v305 = v300 + 1;
    float32x2_t v314 = v7[v313];
    float32x2_t v315 = vtrn1_f32(v128, v128);
    float32x2_t v316 = vtrn2_f32(v128, v128);
    int64_t v318 = v313 + 1;
    float32x2_t v327 = v7[v326];
    float32x2_t v328 = vtrn1_f32(v146, v146);
    float32x2_t v329 = vtrn2_f32(v146, v146);
    int64_t v331 = v326 + 1;
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vtrn1_f32(v164, v164);
    float32x2_t v342 = vtrn2_f32(v164, v164);
    int64_t v344 = v339 + 1;
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vtrn1_f32(v182, v182);
    float32x2_t v355 = vtrn2_f32(v182, v182);
    int64_t v357 = v352 + 1;
    float32x2_t v366 = v7[v365];
    float32x2_t v367 = vtrn1_f32(v200, v200);
    float32x2_t v368 = vtrn2_f32(v200, v200);
    int64_t v370 = v365 + 1;
    float32x2_t v379 = v7[v378];
    float32x2_t v380 = vtrn1_f32(v218, v218);
    float32x2_t v381 = vtrn2_f32(v218, v218);
    int64_t v383 = v378 + 1;
    float32x2_t v254 = v7[v253];
    float32x2_t v255 = vmul_f32(v250, v249);
    float32x2_t v267 = v7[v266];
    float32x2_t v268 = vmul_f32(v263, v262);
    float32x2_t v280 = v7[v279];
    float32x2_t v281 = vmul_f32(v276, v275);
    float32x2_t v293 = v7[v292];
    float32x2_t v294 = vmul_f32(v289, v288);
    float32x2_t v306 = v7[v305];
    float32x2_t v307 = vmul_f32(v302, v301);
    float32x2_t v319 = v7[v318];
    float32x2_t v320 = vmul_f32(v315, v314);
    float32x2_t v332 = v7[v331];
    float32x2_t v333 = vmul_f32(v328, v327);
    float32x2_t v345 = v7[v344];
    float32x2_t v346 = vmul_f32(v341, v340);
    float32x2_t v358 = v7[v357];
    float32x2_t v359 = vmul_f32(v354, v353);
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vmul_f32(v367, v366);
    float32x2_t v384 = v7[v383];
    float32x2_t v385 = vmul_f32(v380, v379);
    float32x2_t v244 = vfma_f32(v242, v238, v241);
    float32x2_t v257 = vfma_f32(v255, v251, v254);
    float32x2_t v270 = vfma_f32(v268, v264, v267);
    float32x2_t v283 = vfma_f32(v281, v277, v280);
    float32x2_t v296 = vfma_f32(v294, v290, v293);
    float32x2_t v309 = vfma_f32(v307, v303, v306);
    float32x2_t v322 = vfma_f32(v320, v316, v319);
    float32x2_t v335 = vfma_f32(v333, v329, v332);
    float32x2_t v348 = vfma_f32(v346, v342, v345);
    float32x2_t v361 = vfma_f32(v359, v355, v358);
    float32x2_t v374 = vfma_f32(v372, v368, v371);
    float32x2_t v387 = vfma_f32(v385, v381, v384);
    float32x2_t v388 = vadd_f32(v244, v257);
    float32x2_t v389 = vadd_f32(v270, v283);
    float32x2_t v390 = vadd_f32(v296, v309);
    float32x2_t v391 = vadd_f32(v322, v335);
    float32x2_t v392 = vadd_f32(v348, v361);
    float32x2_t v393 = vadd_f32(v374, v387);
    float32x2_t v394 = vsub_f32(v244, v257);
    float32x2_t v395 = vsub_f32(v270, v283);
    float32x2_t v396 = vsub_f32(v296, v309);
    float32x2_t v397 = vsub_f32(v322, v335);
    float32x2_t v398 = vsub_f32(v348, v361);
    float32x2_t v399 = vsub_f32(v374, v387);
    float32x2_t v400 = vadd_f32(v389, v392);
    float32x2_t v402 = vadd_f32(v388, v390);
    float32x2_t v405 = vadd_f32(v395, v398);
    float32x2_t v407 = vadd_f32(v394, v396);
    float32x2_t v409 = vsub_f32(v389, v393);
    float32x2_t v410 = vsub_f32(v390, v391);
    float32x2_t v411 = vsub_f32(v388, v391);
    float32x2_t v412 = vsub_f32(v392, v393);
    float32x2_t v417 = vsub_f32(v395, v399);
    float32x2_t v418 = vsub_f32(v394, v396);
    float32x2_t v419 = vsub_f32(v395, v398);
    float32x2_t v420 = vadd_f32(v394, v397);
    float32x2_t v421 = vsub_f32(v398, v399);
    float32x2_t v422 = vadd_f32(v396, v397);
    float32x2_t v401 = vadd_f32(v400, v393);
    float32x2_t v403 = vadd_f32(v402, v391);
    float32x2_t v406 = vadd_f32(v405, v399);
    float32x2_t v408 = vsub_f32(v407, v397);
    float32x2_t v413 = vsub_f32(v409, v410);
    float32x2_t v414 = vsub_f32(v411, v412);
    float32x2_t v415 = vadd_f32(v409, v410);
    float32x2_t v416 = vadd_f32(v411, v412);
    float32x2_t v433 = vadd_f32(v417, v418);
    float32x2_t v434 = vadd_f32(v419, v420);
    float32x2_t v435 = vsub_f32(v421, v422);
    float32x2_t v498 = vrev64_f32(v417);
    float32x2_t v505 = vrev64_f32(v418);
    float32x2_t v519 = vrev64_f32(v419);
    float32x2_t v526 = vrev64_f32(v420);
    float32x2_t v540 = vrev64_f32(v421);
    float32x2_t v547 = vrev64_f32(v422);
    float32x2_t v404 = vadd_f32(v401, v403);
    float32x2_t v429 = vsub_f32(v403, v401);
    float32x2_t v430 = vadd_f32(v406, v408);
    float32x2_t v431 = vadd_f32(v413, v414);
    float32x2_t v432 = vsub_f32(v415, v416);
    float32x2_t v453 = vrev64_f32(v406);
    float32x2_t v460 = vrev64_f32(v408);
    float32x2_t v472 = vmul_f32(v413, v471);
    float32x2_t v476 = vmul_f32(v414, v475);
    float32x2_t v484 = vmul_f32(v415, v483);
    float32x2_t v488 = vmul_f32(v416, v487);
    float32x2_t v499 = vmul_f32(v498, v497);
    float32x2_t v506 = vmul_f32(v505, v504);
    float32x2_t v512 = vrev64_f32(v433);
    float32x2_t v520 = vmul_f32(v519, v518);
    float32x2_t v527 = vmul_f32(v526, v525);
    float32x2_t v533 = vrev64_f32(v434);
    float32x2_t v541 = vmul_f32(v540, v539);
    float32x2_t v548 = vmul_f32(v547, v546);
    float32x2_t v554 = vrev64_f32(v435);
    float32x2_t v428 = vadd_f32(v427, v404);
    float32x2_t v443 = vmul_f32(v404, v442);
    float32x2_t v447 = vmul_f32(v429, v446);
    float32x2_t v454 = vmul_f32(v453, v452);
    float32x2_t v461 = vmul_f32(v460, v459);
    float32x2_t v467 = vrev64_f32(v430);
    float32x2_t v480 = vmul_f32(v431, v479);
    float32x2_t v492 = vmul_f32(v432, v491);
    float32x2_t v513 = vmul_f32(v512, v511);
    float32x2_t v534 = vmul_f32(v533, v532);
    float32x2_t v555 = vmul_f32(v554, v553);
    float32x2_t v557 = vadd_f32(v476, v472);
    float32x2_t v468 = vmul_f32(v467, v466);
    float32x2_t v556 = vsub_f32(v428, v443);
    float32x2_t v558 = vsub_f32(v557, v447);
    float32x2_t v559 = vadd_f32(v476, v480);
    float32x2_t v561 = vsub_f32(v480, v472);
    float32x2_t v569 = vsub_f32(v499, v513);
    float32x2_t v570 = vsub_f32(v506, v513);
    float32x2_t v571 = vsub_f32(v520, v534);
    float32x2_t v572 = vsub_f32(v527, v534);
    float32x2_t v573 = vsub_f32(v541, v555);
    float32x2_t v574 = vadd_f32(v548, v555);
    v6[0] = v428;
    float32x2_t v560 = vadd_f32(v559, v447);
    float32x2_t v562 = vsub_f32(v561, v447);
    float32x2_t v563 = vadd_f32(v556, v484);
    float32x2_t v565 = vsub_f32(v556, v488);
    float32x2_t v567 = vsub_f32(v556, v484);
    float32x2_t v575 = vsub_f32(v454, v468);
    float32x2_t v576 = vsub_f32(v461, v468);
    float32x2_t v587 = vadd_f32(v569, v573);
    float32x2_t v589 = vadd_f32(v571, v573);
    float32x2_t v591 = vsub_f32(v570, v574);
    float32x2_t v564 = vadd_f32(v563, v488);
    float32x2_t v566 = vsub_f32(v565, v492);
    float32x2_t v568 = vadd_f32(v567, v492);
    float32x2_t v583 = vsub_f32(v576, v569);
    float32x2_t v585 = vsub_f32(v574, v575);
    float32x2_t v588 = vadd_f32(v587, v576);
    float32x2_t v590 = vsub_f32(v589, v576);
    float32x2_t v592 = vsub_f32(v591, v575);
    float32x2_t v593 = vadd_f32(v575, v570);
    float32x2_t v577 = vadd_f32(v558, v564);
    float32x2_t v578 = vadd_f32(v560, v566);
    float32x2_t v579 = vsub_f32(v566, v560);
    float32x2_t v580 = vadd_f32(v562, v568);
    float32x2_t v581 = vsub_f32(v564, v558);
    float32x2_t v582 = vsub_f32(v568, v562);
    float32x2_t v584 = vadd_f32(v583, v571);
    float32x2_t v586 = vsub_f32(v585, v572);
    float32x2_t v594 = vsub_f32(v593, v572);
    float32x2_t v595 = vsub_f32(v577, v584);
    float32x2_t v596 = vadd_f32(v578, v586);
    float32x2_t v597 = vsub_f32(v579, v588);
    float32x2_t v598 = vsub_f32(v580, v590);
    float32x2_t v599 = vadd_f32(v581, v592);
    float32x2_t v600 = vsub_f32(v582, v594);
    float32x2_t v601 = vadd_f32(v582, v594);
    float32x2_t v602 = vsub_f32(v581, v592);
    float32x2_t v603 = vadd_f32(v580, v590);
    float32x2_t v604 = vadd_f32(v579, v588);
    float32x2_t v605 = vsub_f32(v578, v586);
    float32x2_t v606 = vadd_f32(v577, v584);
    v6[ostride * 12] = v595;
    v6[ostride * 11] = v596;
    v6[ostride * 10] = v597;
    v6[ostride * 9] = v598;
    v6[ostride * 8] = v599;
    v6[ostride * 7] = v600;
    v6[ostride * 6] = v601;
    v6[ostride * 5] = v602;
    v6[ostride * 4] = v603;
    v6[ostride * 3] = v604;
    v6[ostride * 2] = v605;
    v6[ostride] = v606;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs13(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v326 = 1.0833333333333333e+00F;
    float v331 = -3.0046260628866578e-01F;
    float v336 = -7.4927933062613905e-01F;
    float v343 = -4.0100212832186721e-01F;
    float v350 = -5.7514072947400308e-01F;
    float v357 = 5.2422663952658211e-01F;
    float v362 = 5.1652078062348972e-01F;
    float v367 = 7.7058589030924258e-03F;
    float v372 = 4.2763404682656941e-01F;
    float v377 = 1.5180597207438440e-01F;
    float v382 = 5.7944001890096386e-01F;
    float v387 = -1.1543953381323635e+00F;
    float v394 = -9.0655220171271012e-01F;
    float v401 = -8.1857027294591811e-01F;
    float v408 = -1.1971367726043427e+00F;
    float v415 = -8.6131170741789742e-01F;
    float v422 = -1.1091548438375507e+00F;
    float v429 = -4.2741434471979367e-02F;
    float v436 = 4.5240494294812715e-02F;
    float v443 = -2.9058457089163264e-01F;
    const float32x2_t *v597 = &v5[v0];
    float32x2_t *v847 = &v6[v2];
    int64_t v33 = v0 * 12;
    int64_t v47 = v0 * 2;
    int64_t v61 = v0 * 11;
    int64_t v75 = v0 * 3;
    int64_t v89 = v0 * 10;
    int64_t v103 = v0 * 4;
    int64_t v117 = v0 * 9;
    int64_t v131 = v0 * 5;
    int64_t v145 = v0 * 8;
    int64_t v159 = v0 * 6;
    int64_t v173 = v0 * 7;
    int64_t v195 = v10 * 11;
    int64_t v209 = v10 * 10;
    int64_t v216 = v10 * 2;
    int64_t v223 = v10 * 9;
    int64_t v230 = v10 * 3;
    int64_t v237 = v10 * 8;
    int64_t v244 = v10 * 4;
    int64_t v251 = v10 * 7;
    int64_t v258 = v10 * 5;
    int64_t v265 = v10 * 6;
    int64_t v266 = v13 * 12;
    float v339 = v4 * v336;
    float v346 = v4 * v343;
    float v353 = v4 * v350;
    float v390 = v4 * v387;
    float v397 = v4 * v394;
    float v404 = v4 * v401;
    float v411 = v4 * v408;
    float v418 = v4 * v415;
    float v425 = v4 * v422;
    float v432 = v4 * v429;
    float v439 = v4 * v436;
    float v446 = v4 * v443;
    int64_t v508 = v2 * 12;
    int64_t v515 = v2 * 11;
    int64_t v522 = v2 * 10;
    int64_t v529 = v2 * 9;
    int64_t v536 = v2 * 8;
    int64_t v543 = v2 * 7;
    int64_t v550 = v2 * 6;
    int64_t v557 = v2 * 5;
    int64_t v564 = v2 * 4;
    int64_t v571 = v2 * 3;
    int64_t v578 = v2 * 2;
    const float32x2_t *v708 = &v5[0];
    svint64_t v709 = svindex_s64(0, v1);
    svfloat32_t v712 = svdup_n_f32(v326);
    svfloat32_t v713 = svdup_n_f32(v331);
    svfloat32_t v717 = svdup_n_f32(v357);
    svfloat32_t v718 = svdup_n_f32(v362);
    svfloat32_t v719 = svdup_n_f32(v367);
    svfloat32_t v720 = svdup_n_f32(v372);
    svfloat32_t v721 = svdup_n_f32(v377);
    svfloat32_t v722 = svdup_n_f32(v382);
    float32x2_t *v739 = &v6[0];
    svint64_t v848 = svindex_s64(0, v3);
    svfloat32_t v191 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v266]));
    int64_t v197 = v195 + v266;
    int64_t v204 = v10 + v266;
    int64_t v211 = v209 + v266;
    int64_t v218 = v216 + v266;
    int64_t v225 = v223 + v266;
    int64_t v232 = v230 + v266;
    int64_t v239 = v237 + v266;
    int64_t v246 = v244 + v266;
    int64_t v253 = v251 + v266;
    int64_t v260 = v258 + v266;
    int64_t v267 = v265 + v266;
    svfloat32_t v599 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v597), v709));
    const float32x2_t *v607 = &v5[v33];
    const float32x2_t *v616 = &v5[v47];
    const float32x2_t *v625 = &v5[v61];
    const float32x2_t *v634 = &v5[v75];
    const float32x2_t *v643 = &v5[v89];
    const float32x2_t *v652 = &v5[v103];
    const float32x2_t *v661 = &v5[v117];
    const float32x2_t *v670 = &v5[v131];
    const float32x2_t *v679 = &v5[v145];
    const float32x2_t *v688 = &v5[v159];
    const float32x2_t *v697 = &v5[v173];
    svfloat32_t v710 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v708), v709));
    svfloat32_t v714 = svdup_n_f32(v339);
    svfloat32_t v715 = svdup_n_f32(v346);
    svfloat32_t v716 = svdup_n_f32(v353);
    svfloat32_t v723 = svdup_n_f32(v390);
    svfloat32_t v724 = svdup_n_f32(v397);
    svfloat32_t v725 = svdup_n_f32(v404);
    svfloat32_t v726 = svdup_n_f32(v411);
    svfloat32_t v727 = svdup_n_f32(v418);
    svfloat32_t v728 = svdup_n_f32(v425);
    svfloat32_t v729 = svdup_n_f32(v432);
    svfloat32_t v730 = svdup_n_f32(v439);
    svfloat32_t v731 = svdup_n_f32(v446);
    float32x2_t *v748 = &v6[v508];
    float32x2_t *v757 = &v6[v515];
    float32x2_t *v766 = &v6[v522];
    float32x2_t *v775 = &v6[v529];
    float32x2_t *v784 = &v6[v536];
    float32x2_t *v793 = &v6[v543];
    float32x2_t *v802 = &v6[v550];
    float32x2_t *v811 = &v6[v557];
    float32x2_t *v820 = &v6[v564];
    float32x2_t *v829 = &v6[v571];
    float32x2_t *v838 = &v6[v578];
    svfloat32_t zero192 = svdup_n_f32(0);
    svfloat32_t v192 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero192, v599, v191, 0),
                     v599, v191, 90);
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v219 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v218]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v233 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v232]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v246]));
    svfloat32_t v254 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v253]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v609 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v607), v709));
    svfloat32_t v618 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v616), v709));
    svfloat32_t v627 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v625), v709));
    svfloat32_t v636 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v634), v709));
    svfloat32_t v645 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v643), v709));
    svfloat32_t v654 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v652), v709));
    svfloat32_t v663 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v661), v709));
    svfloat32_t v672 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v670), v709));
    svfloat32_t v681 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v679), v709));
    svfloat32_t v690 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v688), v709));
    svfloat32_t v699 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v697), v709));
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v609, v198, 0),
                     v609, v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v618, v205, 0),
                     v618, v205, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v627, v212, 0),
                     v627, v212, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero220, v636, v219, 0),
                     v636, v219, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero227, v645, v226, 0),
                     v645, v226, 90);
    svfloat32_t zero234 = svdup_n_f32(0);
    svfloat32_t v234 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero234, v654, v233, 0),
                     v654, v233, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero241, v663, v240, 0),
                     v663, v240, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero248, v672, v247, 0),
                     v672, v247, 90);
    svfloat32_t zero255 = svdup_n_f32(0);
    svfloat32_t v255 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero255, v681, v254, 0),
                     v681, v254, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero262, v690, v261, 0),
                     v690, v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero269, v699, v268, 0),
                     v699, v268, 90);
    svfloat32_t v270 = svadd_f32_x(svptrue_b32(), v192, v199);
    svfloat32_t v271 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v272 = svadd_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v273 = svadd_f32_x(svptrue_b32(), v234, v241);
    svfloat32_t v274 = svadd_f32_x(svptrue_b32(), v248, v255);
    svfloat32_t v275 = svadd_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v276 = svsub_f32_x(svptrue_b32(), v192, v199);
    svfloat32_t v277 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v278 = svsub_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v279 = svsub_f32_x(svptrue_b32(), v234, v241);
    svfloat32_t v280 = svsub_f32_x(svptrue_b32(), v248, v255);
    svfloat32_t v281 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v282 = svadd_f32_x(svptrue_b32(), v271, v274);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v270, v272);
    svfloat32_t v287 = svadd_f32_x(svptrue_b32(), v277, v280);
    svfloat32_t v289 = svadd_f32_x(svptrue_b32(), v276, v278);
    svfloat32_t v291 = svsub_f32_x(svptrue_b32(), v271, v275);
    svfloat32_t v292 = svsub_f32_x(svptrue_b32(), v272, v273);
    svfloat32_t v293 = svsub_f32_x(svptrue_b32(), v270, v273);
    svfloat32_t v294 = svsub_f32_x(svptrue_b32(), v274, v275);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v277, v281);
    svfloat32_t v300 = svsub_f32_x(svptrue_b32(), v276, v278);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v277, v280);
    svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v276, v279);
    svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v280, v281);
    svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v278, v279);
    svfloat32_t v283 = svadd_f32_x(svptrue_b32(), v282, v275);
    svfloat32_t v285 = svadd_f32_x(svptrue_b32(), v284, v273);
    svfloat32_t v288 = svadd_f32_x(svptrue_b32(), v287, v281);
    svfloat32_t v290 = svsub_f32_x(svptrue_b32(), v289, v279);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v291, v292);
    svfloat32_t v296 = svsub_f32_x(svptrue_b32(), v293, v294);
    svfloat32_t v297 = svadd_f32_x(svptrue_b32(), v291, v292);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v293, v294);
    svfloat32_t v317 = svadd_f32_x(svptrue_b32(), v299, v300);
    svfloat32_t v318 = svadd_f32_x(svptrue_b32(), v301, v302);
    svfloat32_t v319 = svsub_f32_x(svptrue_b32(), v303, v304);
    svfloat32_t zero392 = svdup_n_f32(0);
    svfloat32_t v392 = svcmla_f32_x(pred_full, zero392, v723, v299, 90);
    svfloat32_t zero399 = svdup_n_f32(0);
    svfloat32_t v399 = svcmla_f32_x(pred_full, zero399, v724, v300, 90);
    svfloat32_t zero413 = svdup_n_f32(0);
    svfloat32_t v413 = svcmla_f32_x(pred_full, zero413, v726, v301, 90);
    svfloat32_t zero420 = svdup_n_f32(0);
    svfloat32_t v420 = svcmla_f32_x(pred_full, zero420, v727, v302, 90);
    svfloat32_t zero434 = svdup_n_f32(0);
    svfloat32_t v434 = svcmla_f32_x(pred_full, zero434, v729, v303, 90);
    svfloat32_t v286 = svadd_f32_x(svptrue_b32(), v283, v285);
    svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v285, v283);
    svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v288, v290);
    svfloat32_t v315 = svadd_f32_x(svptrue_b32(), v295, v296);
    svfloat32_t v316 = svsub_f32_x(svptrue_b32(), v297, v298);
    svfloat32_t zero341 = svdup_n_f32(0);
    svfloat32_t v341 = svcmla_f32_x(pred_full, zero341, v714, v288, 90);
    svfloat32_t zero348 = svdup_n_f32(0);
    svfloat32_t v348 = svcmla_f32_x(pred_full, zero348, v715, v290, 90);
    svfloat32_t v360 = svmul_f32_x(svptrue_b32(), v295, v717);
    svfloat32_t zero406 = svdup_n_f32(0);
    svfloat32_t v406 = svcmla_f32_x(pred_full, zero406, v725, v317, 90);
    svfloat32_t zero427 = svdup_n_f32(0);
    svfloat32_t v427 = svcmla_f32_x(pred_full, zero427, v728, v318, 90);
    svfloat32_t zero448 = svdup_n_f32(0);
    svfloat32_t v448 = svcmla_f32_x(pred_full, zero448, v731, v319, 90);
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v710, v286);
    svfloat32_t zero355 = svdup_n_f32(0);
    svfloat32_t v355 = svcmla_f32_x(pred_full, zero355, v716, v314, 90);
    svfloat32_t v370 = svmul_f32_x(svptrue_b32(), v315, v719);
    svfloat32_t v450 = svmla_f32_x(pred_full, v360, v296, v718);
    svfloat32_t v462 = svsub_f32_x(svptrue_b32(), v392, v406);
    svfloat32_t v463 = svsub_f32_x(svptrue_b32(), v399, v406);
    svfloat32_t v464 = svsub_f32_x(svptrue_b32(), v413, v427);
    svfloat32_t v465 = svsub_f32_x(svptrue_b32(), v420, v427);
    svfloat32_t v466 = svsub_f32_x(svptrue_b32(), v434, v448);
    svfloat32_t v467 = svcmla_f32_x(pred_full, v448, v730, v304, 90);
    svfloat32_t v449 = svmls_f32_x(pred_full, v312, v286, v712);
    svfloat32_t v451 = svmls_f32_x(pred_full, v450, v313, v713);
    svfloat32_t v452 = svmla_f32_x(pred_full, v370, v296, v718);
    svfloat32_t v454 = svnmls_f32_x(pred_full, v360, v315, v719);
    svfloat32_t v468 = svsub_f32_x(svptrue_b32(), v341, v355);
    svfloat32_t v469 = svsub_f32_x(svptrue_b32(), v348, v355);
    svfloat32_t v480 = svadd_f32_x(svptrue_b32(), v462, v466);
    svfloat32_t v482 = svadd_f32_x(svptrue_b32(), v464, v466);
    svfloat32_t v484 = svsub_f32_x(svptrue_b32(), v463, v467);
    svst1_scatter_s64index_f64(pred_full, (double *)(v739), v848,
                               svreinterpret_f64_f32(v312));
    svfloat32_t v453 = svmla_f32_x(pred_full, v452, v313, v713);
    svfloat32_t v455 = svmls_f32_x(pred_full, v454, v313, v713);
    svfloat32_t v456 = svmla_f32_x(pred_full, v449, v297, v720);
    svfloat32_t v458 = svmls_f32_x(pred_full, v449, v298, v721);
    svfloat32_t v460 = svmls_f32_x(pred_full, v449, v297, v720);
    svfloat32_t v476 = svsub_f32_x(svptrue_b32(), v469, v462);
    svfloat32_t v478 = svsub_f32_x(svptrue_b32(), v467, v468);
    svfloat32_t v481 = svadd_f32_x(svptrue_b32(), v480, v469);
    svfloat32_t v483 = svsub_f32_x(svptrue_b32(), v482, v469);
    svfloat32_t v485 = svsub_f32_x(svptrue_b32(), v484, v468);
    svfloat32_t v486 = svadd_f32_x(svptrue_b32(), v468, v463);
    svfloat32_t v457 = svmla_f32_x(pred_full, v456, v298, v721);
    svfloat32_t v459 = svmls_f32_x(pred_full, v458, v316, v722);
    svfloat32_t v461 = svmla_f32_x(pred_full, v460, v316, v722);
    svfloat32_t v477 = svadd_f32_x(svptrue_b32(), v476, v464);
    svfloat32_t v479 = svsub_f32_x(svptrue_b32(), v478, v465);
    svfloat32_t v487 = svsub_f32_x(svptrue_b32(), v486, v465);
    svfloat32_t v470 = svadd_f32_x(svptrue_b32(), v451, v457);
    svfloat32_t v471 = svadd_f32_x(svptrue_b32(), v453, v459);
    svfloat32_t v472 = svsub_f32_x(svptrue_b32(), v459, v453);
    svfloat32_t v473 = svadd_f32_x(svptrue_b32(), v455, v461);
    svfloat32_t v474 = svsub_f32_x(svptrue_b32(), v457, v451);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v461, v455);
    svfloat32_t v488 = svsub_f32_x(svptrue_b32(), v470, v477);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v471, v479);
    svfloat32_t v490 = svsub_f32_x(svptrue_b32(), v472, v481);
    svfloat32_t v491 = svsub_f32_x(svptrue_b32(), v473, v483);
    svfloat32_t v492 = svadd_f32_x(svptrue_b32(), v474, v485);
    svfloat32_t v493 = svsub_f32_x(svptrue_b32(), v475, v487);
    svfloat32_t v494 = svadd_f32_x(svptrue_b32(), v475, v487);
    svfloat32_t v495 = svsub_f32_x(svptrue_b32(), v474, v485);
    svfloat32_t v496 = svadd_f32_x(svptrue_b32(), v473, v483);
    svfloat32_t v497 = svadd_f32_x(svptrue_b32(), v472, v481);
    svfloat32_t v498 = svsub_f32_x(svptrue_b32(), v471, v479);
    svfloat32_t v499 = svadd_f32_x(svptrue_b32(), v470, v477);
    svst1_scatter_s64index_f64(pred_full, (double *)(v748), v848,
                               svreinterpret_f64_f32(v488));
    svst1_scatter_s64index_f64(pred_full, (double *)(v757), v848,
                               svreinterpret_f64_f32(v489));
    svst1_scatter_s64index_f64(pred_full, (double *)(v766), v848,
                               svreinterpret_f64_f32(v490));
    svst1_scatter_s64index_f64(pred_full, (double *)(v775), v848,
                               svreinterpret_f64_f32(v491));
    svst1_scatter_s64index_f64(pred_full, (double *)(v784), v848,
                               svreinterpret_f64_f32(v492));
    svst1_scatter_s64index_f64(pred_full, (double *)(v793), v848,
                               svreinterpret_f64_f32(v493));
    svst1_scatter_s64index_f64(pred_full, (double *)(v802), v848,
                               svreinterpret_f64_f32(v494));
    svst1_scatter_s64index_f64(pred_full, (double *)(v811), v848,
                               svreinterpret_f64_f32(v495));
    svst1_scatter_s64index_f64(pred_full, (double *)(v820), v848,
                               svreinterpret_f64_f32(v496));
    svst1_scatter_s64index_f64(pred_full, (double *)(v829), v848,
                               svreinterpret_f64_f32(v497));
    svst1_scatter_s64index_f64(pred_full, (double *)(v838), v848,
                               svreinterpret_f64_f32(v498));
    svst1_scatter_s64index_f64(pred_full, (double *)(v847), v848,
                               svreinterpret_f64_f32(v499));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs14(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v255 = v5[istride];
    float v544 = -1.1666666666666665e+00F;
    float v548 = 7.9015646852540022e-01F;
    float v552 = 5.5854267289647742e-02F;
    float v556 = 7.3430220123575241e-01F;
    float v559 = 4.4095855184409838e-01F;
    float v560 = -4.4095855184409838e-01F;
    float v566 = 3.4087293062393137e-01F;
    float v567 = -3.4087293062393137e-01F;
    float v573 = -5.3396936033772524e-01F;
    float v574 = 5.3396936033772524e-01F;
    float v580 = 8.7484229096165667e-01F;
    float v581 = -8.7484229096165667e-01F;
    float32x2_t v583 = (float32x2_t){v4, v4};
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    float32x2_t v423 = v5[0];
    float32x2_t v545 = (float32x2_t){v544, v544};
    float32x2_t v549 = (float32x2_t){v548, v548};
    float32x2_t v553 = (float32x2_t){v552, v552};
    float32x2_t v557 = (float32x2_t){v556, v556};
    float32x2_t v561 = (float32x2_t){v559, v560};
    float32x2_t v568 = (float32x2_t){v566, v567};
    float32x2_t v575 = (float32x2_t){v573, v574};
    float32x2_t v582 = (float32x2_t){v580, v581};
    float32x2_t v20 = v5[istride * 7];
    int64_t v37 = 12 + j * 26;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 9];
    int64_t v86 = 2 + j * 26;
    int64_t v99 = 16 + j * 26;
    float32x2_t v113 = v5[istride * 4];
    float32x2_t v131 = v5[istride * 11];
    int64_t v148 = 6 + j * 26;
    int64_t v161 = 20 + j * 26;
    float32x2_t v175 = v5[istride * 6];
    float32x2_t v193 = v5[istride * 13];
    int64_t v210 = 10 + j * 26;
    int64_t v223 = 24 + j * 26;
    float32x2_t v237 = v5[istride * 8];
    int64_t v272 = 14 + j * 26;
    float32x2_t v286 = v7[j * 26];
    int64_t v290 = j * 26 + 1;
    float32x2_t v299 = v5[istride * 10];
    float32x2_t v317 = v5[istride * 3];
    int64_t v334 = 18 + j * 26;
    int64_t v347 = 4 + j * 26;
    float32x2_t v361 = v5[istride * 12];
    float32x2_t v379 = v5[istride * 5];
    int64_t v396 = 22 + j * 26;
    int64_t v409 = 8 + j * 26;
    float32x2_t v563 = vmul_f32(v583, v561);
    float32x2_t v570 = vmul_f32(v583, v568);
    float32x2_t v577 = vmul_f32(v583, v575);
    float32x2_t v584 = vmul_f32(v583, v582);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v273 = v7[v272];
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    int64_t v277 = v272 + 1;
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v335 = v7[v334];
    float32x2_t v336 = vtrn1_f32(v299, v299);
    float32x2_t v337 = vtrn2_f32(v299, v299);
    int64_t v339 = v334 + 1;
    float32x2_t v348 = v7[v347];
    float32x2_t v349 = vtrn1_f32(v317, v317);
    float32x2_t v350 = vtrn2_f32(v317, v317);
    int64_t v352 = v347 + 1;
    float32x2_t v397 = v7[v396];
    float32x2_t v398 = vtrn1_f32(v361, v361);
    float32x2_t v399 = vtrn2_f32(v361, v361);
    int64_t v401 = v396 + 1;
    float32x2_t v410 = v7[v409];
    float32x2_t v411 = vtrn1_f32(v379, v379);
    float32x2_t v412 = vtrn2_f32(v379, v379);
    int64_t v414 = v409 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vmul_f32(v336, v335);
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vmul_f32(v349, v348);
    float32x2_t v402 = v7[v401];
    float32x2_t v403 = vmul_f32(v398, v397);
    float32x2_t v415 = v7[v414];
    float32x2_t v416 = vmul_f32(v411, v410);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v343 = vfma_f32(v341, v337, v340);
    float32x2_t v356 = vfma_f32(v354, v350, v353);
    float32x2_t v405 = vfma_f32(v403, v399, v402);
    float32x2_t v418 = vfma_f32(v416, v412, v415);
    float32x2_t v424 = vadd_f32(v423, v46);
    float32x2_t v425 = vsub_f32(v423, v46);
    float32x2_t v426 = vadd_f32(v95, v108);
    float32x2_t v427 = vsub_f32(v95, v108);
    float32x2_t v428 = vadd_f32(v157, v170);
    float32x2_t v429 = vsub_f32(v157, v170);
    float32x2_t v430 = vadd_f32(v219, v232);
    float32x2_t v431 = vsub_f32(v219, v232);
    float32x2_t v432 = vadd_f32(v281, v294);
    float32x2_t v433 = vsub_f32(v281, v294);
    float32x2_t v434 = vadd_f32(v343, v356);
    float32x2_t v435 = vsub_f32(v343, v356);
    float32x2_t v436 = vadd_f32(v405, v418);
    float32x2_t v437 = vsub_f32(v405, v418);
    float32x2_t v438 = vadd_f32(v426, v436);
    float32x2_t v439 = vsub_f32(v426, v436);
    float32x2_t v440 = vadd_f32(v432, v430);
    float32x2_t v441 = vsub_f32(v432, v430);
    float32x2_t v442 = vadd_f32(v428, v434);
    float32x2_t v443 = vsub_f32(v428, v434);
    float32x2_t v522 = vadd_f32(v427, v437);
    float32x2_t v523 = vsub_f32(v427, v437);
    float32x2_t v524 = vadd_f32(v433, v431);
    float32x2_t v525 = vsub_f32(v433, v431);
    float32x2_t v526 = vadd_f32(v429, v435);
    float32x2_t v527 = vsub_f32(v429, v435);
    float32x2_t v444 = vadd_f32(v438, v440);
    float32x2_t v447 = vsub_f32(v438, v440);
    float32x2_t v448 = vsub_f32(v440, v442);
    float32x2_t v449 = vsub_f32(v442, v438);
    float32x2_t v450 = vadd_f32(v439, v441);
    float32x2_t v452 = vsub_f32(v439, v441);
    float32x2_t v453 = vsub_f32(v441, v443);
    float32x2_t v454 = vsub_f32(v443, v439);
    float32x2_t v528 = vadd_f32(v522, v524);
    float32x2_t v531 = vsub_f32(v522, v524);
    float32x2_t v532 = vsub_f32(v524, v526);
    float32x2_t v533 = vsub_f32(v526, v522);
    float32x2_t v534 = vadd_f32(v523, v525);
    float32x2_t v536 = vsub_f32(v523, v525);
    float32x2_t v537 = vsub_f32(v525, v527);
    float32x2_t v538 = vsub_f32(v527, v523);
    float32x2_t v445 = vadd_f32(v444, v442);
    float32x2_t v451 = vadd_f32(v450, v443);
    float32x2_t v466 = vmul_f32(v447, v549);
    float32x2_t v470 = vmul_f32(v448, v553);
    float32x2_t v474 = vmul_f32(v449, v557);
    float32x2_t v487 = vrev64_f32(v452);
    float32x2_t v494 = vrev64_f32(v453);
    float32x2_t v501 = vrev64_f32(v454);
    float32x2_t v529 = vadd_f32(v528, v526);
    float32x2_t v535 = vadd_f32(v534, v527);
    float32x2_t v550 = vmul_f32(v531, v549);
    float32x2_t v554 = vmul_f32(v532, v553);
    float32x2_t v558 = vmul_f32(v533, v557);
    float32x2_t v571 = vrev64_f32(v536);
    float32x2_t v578 = vrev64_f32(v537);
    float32x2_t v585 = vrev64_f32(v538);
    float32x2_t v446 = vadd_f32(v445, v424);
    float32x2_t v462 = vmul_f32(v445, v545);
    float32x2_t v480 = vrev64_f32(v451);
    float32x2_t v488 = vmul_f32(v487, v570);
    float32x2_t v495 = vmul_f32(v494, v577);
    float32x2_t v502 = vmul_f32(v501, v584);
    float32x2_t v530 = vadd_f32(v529, v425);
    float32x2_t v546 = vmul_f32(v529, v545);
    float32x2_t v564 = vrev64_f32(v535);
    float32x2_t v572 = vmul_f32(v571, v570);
    float32x2_t v579 = vmul_f32(v578, v577);
    float32x2_t v586 = vmul_f32(v585, v584);
    float32x2_t v481 = vmul_f32(v480, v563);
    float32x2_t v503 = vadd_f32(v446, v462);
    float32x2_t v565 = vmul_f32(v564, v563);
    float32x2_t v587 = vadd_f32(v530, v546);
    v6[0] = v446;
    v6[ostride * 7] = v530;
    float32x2_t v504 = vadd_f32(v503, v466);
    float32x2_t v506 = vsub_f32(v503, v466);
    float32x2_t v508 = vsub_f32(v503, v470);
    float32x2_t v510 = vadd_f32(v481, v488);
    float32x2_t v512 = vsub_f32(v481, v488);
    float32x2_t v514 = vsub_f32(v481, v495);
    float32x2_t v588 = vadd_f32(v587, v550);
    float32x2_t v590 = vsub_f32(v587, v550);
    float32x2_t v592 = vsub_f32(v587, v554);
    float32x2_t v594 = vadd_f32(v565, v572);
    float32x2_t v596 = vsub_f32(v565, v572);
    float32x2_t v598 = vsub_f32(v565, v579);
    float32x2_t v505 = vadd_f32(v504, v470);
    float32x2_t v507 = vsub_f32(v506, v474);
    float32x2_t v509 = vadd_f32(v508, v474);
    float32x2_t v511 = vadd_f32(v510, v495);
    float32x2_t v513 = vsub_f32(v512, v502);
    float32x2_t v515 = vadd_f32(v514, v502);
    float32x2_t v589 = vadd_f32(v588, v554);
    float32x2_t v591 = vsub_f32(v590, v558);
    float32x2_t v593 = vadd_f32(v592, v558);
    float32x2_t v595 = vadd_f32(v594, v579);
    float32x2_t v597 = vsub_f32(v596, v586);
    float32x2_t v599 = vadd_f32(v598, v586);
    float32x2_t v516 = vadd_f32(v505, v511);
    float32x2_t v517 = vsub_f32(v505, v511);
    float32x2_t v518 = vadd_f32(v507, v513);
    float32x2_t v519 = vsub_f32(v507, v513);
    float32x2_t v520 = vadd_f32(v509, v515);
    float32x2_t v521 = vsub_f32(v509, v515);
    float32x2_t v600 = vadd_f32(v589, v595);
    float32x2_t v601 = vsub_f32(v589, v595);
    float32x2_t v602 = vadd_f32(v591, v597);
    float32x2_t v603 = vsub_f32(v591, v597);
    float32x2_t v604 = vadd_f32(v593, v599);
    float32x2_t v605 = vsub_f32(v593, v599);
    v6[ostride * 8] = v517;
    v6[ostride] = v601;
    v6[ostride * 2] = v519;
    v6[ostride * 9] = v603;
    v6[ostride * 10] = v520;
    v6[ostride * 3] = v604;
    v6[ostride * 4] = v521;
    v6[ostride * 11] = v605;
    v6[ostride * 12] = v518;
    v6[ostride * 5] = v602;
    v6[ostride * 6] = v516;
    v6[ostride * 13] = v600;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs14(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v424 = -1.1666666666666665e+00F;
    float v429 = 7.9015646852540022e-01F;
    float v434 = 5.5854267289647742e-02F;
    float v439 = 7.3430220123575241e-01F;
    float v444 = -4.4095855184409838e-01F;
    float v451 = -3.4087293062393137e-01F;
    float v458 = 5.3396936033772524e-01F;
    float v465 = -8.7484229096165667e-01F;
    const float32x2_t *v666 = &v5[v0];
    float32x2_t *v769 = &v6[v2];
    int64_t v19 = v0 * 7;
    int64_t v34 = v10 * 6;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 9;
    int64_t v76 = v10 * 8;
    int64_t v82 = v0 * 4;
    int64_t v96 = v0 * 11;
    int64_t v111 = v10 * 3;
    int64_t v118 = v10 * 10;
    int64_t v124 = v0 * 6;
    int64_t v138 = v0 * 13;
    int64_t v153 = v10 * 5;
    int64_t v160 = v10 * 12;
    int64_t v166 = v0 * 8;
    int64_t v195 = v10 * 7;
    int64_t v208 = v0 * 10;
    int64_t v222 = v0 * 3;
    int64_t v237 = v10 * 9;
    int64_t v244 = v10 * 2;
    int64_t v250 = v0 * 12;
    int64_t v264 = v0 * 5;
    int64_t v279 = v10 * 11;
    int64_t v286 = v10 * 4;
    int64_t v287 = v13 * 13;
    float v447 = v4 * v444;
    float v454 = v4 * v451;
    float v461 = v4 * v458;
    float v468 = v4 * v465;
    int64_t v498 = v2 * 7;
    int64_t v505 = v2 * 8;
    int64_t v519 = v2 * 2;
    int64_t v526 = v2 * 9;
    int64_t v533 = v2 * 10;
    int64_t v540 = v2 * 3;
    int64_t v547 = v2 * 4;
    int64_t v554 = v2 * 11;
    int64_t v561 = v2 * 12;
    int64_t v568 = v2 * 5;
    int64_t v575 = v2 * 6;
    int64_t v582 = v2 * 13;
    const float32x2_t *v714 = &v5[0];
    svint64_t v715 = svindex_s64(0, v1);
    svfloat32_t v727 = svdup_n_f32(v424);
    svfloat32_t v728 = svdup_n_f32(v429);
    svfloat32_t v729 = svdup_n_f32(v434);
    svfloat32_t v730 = svdup_n_f32(v439);
    float32x2_t *v742 = &v6[0];
    svint64_t v860 = svindex_s64(0, v3);
    int64_t v36 = v34 + v287;
    int64_t v71 = v10 + v287;
    int64_t v78 = v76 + v287;
    int64_t v113 = v111 + v287;
    int64_t v120 = v118 + v287;
    int64_t v155 = v153 + v287;
    int64_t v162 = v160 + v287;
    int64_t v197 = v195 + v287;
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v287]));
    int64_t v239 = v237 + v287;
    int64_t v246 = v244 + v287;
    int64_t v281 = v279 + v287;
    int64_t v288 = v286 + v287;
    const float32x2_t *v594 = &v5[v19];
    const float32x2_t *v603 = &v5[v40];
    const float32x2_t *v612 = &v5[v54];
    const float32x2_t *v621 = &v5[v82];
    const float32x2_t *v630 = &v5[v96];
    const float32x2_t *v639 = &v5[v124];
    const float32x2_t *v648 = &v5[v138];
    const float32x2_t *v657 = &v5[v166];
    svfloat32_t v668 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v666), v715));
    const float32x2_t *v677 = &v5[v208];
    const float32x2_t *v686 = &v5[v222];
    const float32x2_t *v695 = &v5[v250];
    const float32x2_t *v704 = &v5[v264];
    svfloat32_t v716 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v714), v715));
    svfloat32_t v731 = svdup_n_f32(v447);
    svfloat32_t v732 = svdup_n_f32(v454);
    svfloat32_t v733 = svdup_n_f32(v461);
    svfloat32_t v734 = svdup_n_f32(v468);
    float32x2_t *v751 = &v6[v498];
    float32x2_t *v760 = &v6[v505];
    float32x2_t *v778 = &v6[v519];
    float32x2_t *v787 = &v6[v526];
    float32x2_t *v796 = &v6[v533];
    float32x2_t *v805 = &v6[v540];
    float32x2_t *v814 = &v6[v547];
    float32x2_t *v823 = &v6[v554];
    float32x2_t *v832 = &v6[v561];
    float32x2_t *v841 = &v6[v568];
    float32x2_t *v850 = &v6[v575];
    float32x2_t *v859 = &v6[v582];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v668, v205, 0),
                     v668, v205, 90);
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v246]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v288]));
    svfloat32_t v596 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v594), v715));
    svfloat32_t v605 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v603), v715));
    svfloat32_t v614 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v612), v715));
    svfloat32_t v623 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v621), v715));
    svfloat32_t v632 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v630), v715));
    svfloat32_t v641 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v639), v715));
    svfloat32_t v650 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v648), v715));
    svfloat32_t v659 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v657), v715));
    svfloat32_t v679 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v677), v715));
    svfloat32_t v688 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v686), v715));
    svfloat32_t v697 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v695), v715));
    svfloat32_t v706 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v704), v715));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v596, v37, 0),
                     v596, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v605, v72, 0),
                     v605, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v614, v79, 0),
                     v614, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v623, v114, 0),
                     v623, v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v632, v121, 0),
                     v632, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v641, v156, 0),
                     v641, v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v650, v163, 0),
                     v650, v163, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v659, v198, 0),
                     v659, v198, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero241, v679, v240, 0),
                     v679, v240, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero248, v688, v247, 0),
                     v688, v247, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero283, v697, v282, 0),
                     v697, v282, 90);
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero290, v706, v289, 0),
                     v706, v289, 90);
    svfloat32_t v298 = svadd_f32_x(svptrue_b32(), v716, v38);
    svfloat32_t v299 = svsub_f32_x(svptrue_b32(), v716, v38);
    svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v302 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v303 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v304 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v305 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v308 = svadd_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v300, v310);
    svfloat32_t v313 = svsub_f32_x(svptrue_b32(), v300, v310);
    svfloat32_t v314 = svadd_f32_x(svptrue_b32(), v306, v304);
    svfloat32_t v315 = svsub_f32_x(svptrue_b32(), v306, v304);
    svfloat32_t v316 = svadd_f32_x(svptrue_b32(), v302, v308);
    svfloat32_t v317 = svsub_f32_x(svptrue_b32(), v302, v308);
    svfloat32_t v401 = svadd_f32_x(svptrue_b32(), v301, v311);
    svfloat32_t v402 = svsub_f32_x(svptrue_b32(), v301, v311);
    svfloat32_t v403 = svadd_f32_x(svptrue_b32(), v307, v305);
    svfloat32_t v404 = svsub_f32_x(svptrue_b32(), v307, v305);
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v303, v309);
    svfloat32_t v406 = svsub_f32_x(svptrue_b32(), v303, v309);
    svfloat32_t v318 = svadd_f32_x(svptrue_b32(), v312, v314);
    svfloat32_t v321 = svsub_f32_x(svptrue_b32(), v312, v314);
    svfloat32_t v322 = svsub_f32_x(svptrue_b32(), v314, v316);
    svfloat32_t v323 = svsub_f32_x(svptrue_b32(), v316, v312);
    svfloat32_t v324 = svadd_f32_x(svptrue_b32(), v313, v315);
    svfloat32_t v326 = svsub_f32_x(svptrue_b32(), v313, v315);
    svfloat32_t v327 = svsub_f32_x(svptrue_b32(), v315, v317);
    svfloat32_t v328 = svsub_f32_x(svptrue_b32(), v317, v313);
    svfloat32_t v407 = svadd_f32_x(svptrue_b32(), v401, v403);
    svfloat32_t v410 = svsub_f32_x(svptrue_b32(), v401, v403);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v403, v405);
    svfloat32_t v412 = svsub_f32_x(svptrue_b32(), v405, v401);
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v402, v404);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v402, v404);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v404, v406);
    svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v406, v402);
    svfloat32_t v319 = svadd_f32_x(svptrue_b32(), v318, v316);
    svfloat32_t v325 = svadd_f32_x(svptrue_b32(), v324, v317);
    svfloat32_t zero367 = svdup_n_f32(0);
    svfloat32_t v367 = svcmla_f32_x(pred_full, zero367, v732, v326, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 = svcmla_f32_x(pred_full, zero374, v733, v327, 90);
    svfloat32_t zero381 = svdup_n_f32(0);
    svfloat32_t v381 = svcmla_f32_x(pred_full, zero381, v734, v328, 90);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v407, v405);
    svfloat32_t v414 = svadd_f32_x(svptrue_b32(), v413, v406);
    svfloat32_t zero456 = svdup_n_f32(0);
    svfloat32_t v456 = svcmla_f32_x(pred_full, zero456, v732, v415, 90);
    svfloat32_t zero463 = svdup_n_f32(0);
    svfloat32_t v463 = svcmla_f32_x(pred_full, zero463, v733, v416, 90);
    svfloat32_t zero470 = svdup_n_f32(0);
    svfloat32_t v470 = svcmla_f32_x(pred_full, zero470, v734, v417, 90);
    svfloat32_t v320 = svadd_f32_x(svptrue_b32(), v319, v298);
    svfloat32_t zero360 = svdup_n_f32(0);
    svfloat32_t v360 = svcmla_f32_x(pred_full, zero360, v731, v325, 90);
    svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v408, v299);
    svfloat32_t zero449 = svdup_n_f32(0);
    svfloat32_t v449 = svcmla_f32_x(pred_full, zero449, v731, v414, 90);
    svfloat32_t v382 = svmla_f32_x(pred_full, v320, v319, v727);
    svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v360, v367);
    svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v360, v367);
    svfloat32_t v393 = svsub_f32_x(svptrue_b32(), v360, v374);
    svfloat32_t v471 = svmla_f32_x(pred_full, v409, v408, v727);
    svfloat32_t v478 = svadd_f32_x(svptrue_b32(), v449, v456);
    svfloat32_t v480 = svsub_f32_x(svptrue_b32(), v449, v456);
    svfloat32_t v482 = svsub_f32_x(svptrue_b32(), v449, v463);
    svst1_scatter_s64index_f64(pred_full, (double *)(v742), v860,
                               svreinterpret_f64_f32(v320));
    svst1_scatter_s64index_f64(pred_full, (double *)(v751), v860,
                               svreinterpret_f64_f32(v409));
    svfloat32_t v383 = svmla_f32_x(pred_full, v382, v321, v728);
    svfloat32_t v385 = svmls_f32_x(pred_full, v382, v321, v728);
    svfloat32_t v387 = svmls_f32_x(pred_full, v382, v322, v729);
    svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v389, v374);
    svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v391, v381);
    svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v393, v381);
    svfloat32_t v472 = svmla_f32_x(pred_full, v471, v410, v728);
    svfloat32_t v474 = svmls_f32_x(pred_full, v471, v410, v728);
    svfloat32_t v476 = svmls_f32_x(pred_full, v471, v411, v729);
    svfloat32_t v479 = svadd_f32_x(svptrue_b32(), v478, v463);
    svfloat32_t v481 = svsub_f32_x(svptrue_b32(), v480, v470);
    svfloat32_t v483 = svadd_f32_x(svptrue_b32(), v482, v470);
    svfloat32_t v384 = svmla_f32_x(pred_full, v383, v322, v729);
    svfloat32_t v386 = svmls_f32_x(pred_full, v385, v323, v730);
    svfloat32_t v388 = svmla_f32_x(pred_full, v387, v323, v730);
    svfloat32_t v473 = svmla_f32_x(pred_full, v472, v411, v729);
    svfloat32_t v475 = svmls_f32_x(pred_full, v474, v412, v730);
    svfloat32_t v477 = svmla_f32_x(pred_full, v476, v412, v730);
    svfloat32_t v395 = svadd_f32_x(svptrue_b32(), v384, v390);
    svfloat32_t v396 = svsub_f32_x(svptrue_b32(), v384, v390);
    svfloat32_t v397 = svadd_f32_x(svptrue_b32(), v386, v392);
    svfloat32_t v398 = svsub_f32_x(svptrue_b32(), v386, v392);
    svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v388, v394);
    svfloat32_t v400 = svsub_f32_x(svptrue_b32(), v388, v394);
    svfloat32_t v484 = svadd_f32_x(svptrue_b32(), v473, v479);
    svfloat32_t v485 = svsub_f32_x(svptrue_b32(), v473, v479);
    svfloat32_t v486 = svadd_f32_x(svptrue_b32(), v475, v481);
    svfloat32_t v487 = svsub_f32_x(svptrue_b32(), v475, v481);
    svfloat32_t v488 = svadd_f32_x(svptrue_b32(), v477, v483);
    svfloat32_t v489 = svsub_f32_x(svptrue_b32(), v477, v483);
    svst1_scatter_s64index_f64(pred_full, (double *)(v760), v860,
                               svreinterpret_f64_f32(v396));
    svst1_scatter_s64index_f64(pred_full, (double *)(v769), v860,
                               svreinterpret_f64_f32(v485));
    svst1_scatter_s64index_f64(pred_full, (double *)(v778), v860,
                               svreinterpret_f64_f32(v398));
    svst1_scatter_s64index_f64(pred_full, (double *)(v787), v860,
                               svreinterpret_f64_f32(v487));
    svst1_scatter_s64index_f64(pred_full, (double *)(v796), v860,
                               svreinterpret_f64_f32(v399));
    svst1_scatter_s64index_f64(pred_full, (double *)(v805), v860,
                               svreinterpret_f64_f32(v488));
    svst1_scatter_s64index_f64(pred_full, (double *)(v814), v860,
                               svreinterpret_f64_f32(v400));
    svst1_scatter_s64index_f64(pred_full, (double *)(v823), v860,
                               svreinterpret_f64_f32(v489));
    svst1_scatter_s64index_f64(pred_full, (double *)(v832), v860,
                               svreinterpret_f64_f32(v397));
    svst1_scatter_s64index_f64(pred_full, (double *)(v841), v860,
                               svreinterpret_f64_f32(v486));
    svst1_scatter_s64index_f64(pred_full, (double *)(v850), v860,
                               svreinterpret_f64_f32(v395));
    svst1_scatter_s64index_f64(pred_full, (double *)(v859), v860,
                               svreinterpret_f64_f32(v484));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs15(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v180 = v5[istride];
    float v431 = -1.2500000000000000e+00F;
    float v435 = 5.5901699437494745e-01F;
    float v438 = 1.5388417685876268e+00F;
    float v439 = -1.5388417685876268e+00F;
    float v445 = 5.8778525229247325e-01F;
    float v446 = -5.8778525229247325e-01F;
    float v452 = 3.6327126400268028e-01F;
    float v453 = -3.6327126400268028e-01F;
    float v477 = -1.4999999999999998e+00F;
    float v481 = 1.8749999999999998e+00F;
    float v485 = -8.3852549156242107e-01F;
    float v488 = -2.3082626528814396e+00F;
    float v489 = 2.3082626528814396e+00F;
    float v495 = -8.8167787843870971e-01F;
    float v496 = 8.8167787843870971e-01F;
    float v502 = -5.4490689600402031e-01F;
    float v503 = 5.4490689600402031e-01F;
    float v526 = 8.6602540378443871e-01F;
    float v527 = -8.6602540378443871e-01F;
    float v533 = -1.0825317547305484e+00F;
    float v534 = 1.0825317547305484e+00F;
    float v540 = 4.8412291827592718e-01F;
    float v541 = -4.8412291827592718e-01F;
    float32x2_t v543 = (float32x2_t){v4, v4};
    float v548 = -1.3326760640014592e+00F;
    float v552 = -5.0903696045512736e-01F;
    float v556 = -3.1460214309120460e-01F;
    float32x2_t v212 = vtrn1_f32(v180, v180);
    float32x2_t v213 = vtrn2_f32(v180, v180);
    float32x2_t v404 = v5[0];
    float32x2_t v432 = (float32x2_t){v431, v431};
    float32x2_t v436 = (float32x2_t){v435, v435};
    float32x2_t v440 = (float32x2_t){v438, v439};
    float32x2_t v447 = (float32x2_t){v445, v446};
    float32x2_t v454 = (float32x2_t){v452, v453};
    float32x2_t v478 = (float32x2_t){v477, v477};
    float32x2_t v482 = (float32x2_t){v481, v481};
    float32x2_t v486 = (float32x2_t){v485, v485};
    float32x2_t v490 = (float32x2_t){v488, v489};
    float32x2_t v497 = (float32x2_t){v495, v496};
    float32x2_t v504 = (float32x2_t){v502, v503};
    float32x2_t v528 = (float32x2_t){v526, v527};
    float32x2_t v535 = (float32x2_t){v533, v534};
    float32x2_t v542 = (float32x2_t){v540, v541};
    float32x2_t v549 = (float32x2_t){v548, v548};
    float32x2_t v553 = (float32x2_t){v552, v552};
    float32x2_t v557 = (float32x2_t){v556, v556};
    float32x2_t v20 = v5[istride * 5];
    float32x2_t v38 = v5[istride * 10];
    int64_t v55 = 8 + j * 28;
    int64_t v68 = 18 + j * 28;
    float32x2_t v82 = v5[istride * 8];
    float32x2_t v100 = v5[istride * 13];
    int64_t v117 = 14 + j * 28;
    int64_t v130 = 24 + j * 28;
    float32x2_t v144 = v5[istride * 3];
    int64_t v148 = 4 + j * 28;
    float32x2_t v162 = v5[istride * 11];
    int64_t v197 = 20 + j * 28;
    float32x2_t v211 = v7[j * 28];
    int64_t v215 = j * 28 + 1;
    float32x2_t v224 = v5[istride * 6];
    int64_t v228 = 10 + j * 28;
    float32x2_t v242 = v5[istride * 14];
    float32x2_t v260 = v5[istride * 4];
    int64_t v277 = 26 + j * 28;
    int64_t v290 = 6 + j * 28;
    float32x2_t v304 = v5[istride * 9];
    int64_t v308 = 16 + j * 28;
    float32x2_t v322 = v5[istride * 2];
    float32x2_t v340 = v5[istride * 7];
    int64_t v357 = 2 + j * 28;
    int64_t v370 = 12 + j * 28;
    float32x2_t v384 = v5[istride * 12];
    int64_t v388 = 22 + j * 28;
    float32x2_t v442 = vmul_f32(v543, v440);
    float32x2_t v449 = vmul_f32(v543, v447);
    float32x2_t v456 = vmul_f32(v543, v454);
    float32x2_t v492 = vmul_f32(v543, v490);
    float32x2_t v499 = vmul_f32(v543, v497);
    float32x2_t v506 = vmul_f32(v543, v504);
    float32x2_t v530 = vmul_f32(v543, v528);
    float32x2_t v537 = vmul_f32(v543, v535);
    float32x2_t v544 = vmul_f32(v543, v542);
    float32x2_t v56 = v7[v55];
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    int64_t v60 = v55 + 1;
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v144, v144);
    float32x2_t v151 = vtrn2_f32(v144, v144);
    int64_t v153 = v148 + 1;
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vtrn1_f32(v162, v162);
    float32x2_t v200 = vtrn2_f32(v162, v162);
    int64_t v202 = v197 + 1;
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vtrn1_f32(v224, v224);
    float32x2_t v231 = vtrn2_f32(v224, v224);
    int64_t v233 = v228 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vtrn1_f32(v242, v242);
    float32x2_t v280 = vtrn2_f32(v242, v242);
    int64_t v282 = v277 + 1;
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vtrn1_f32(v260, v260);
    float32x2_t v293 = vtrn2_f32(v260, v260);
    int64_t v295 = v290 + 1;
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vtrn1_f32(v304, v304);
    float32x2_t v311 = vtrn2_f32(v304, v304);
    int64_t v313 = v308 + 1;
    float32x2_t v358 = v7[v357];
    float32x2_t v359 = vtrn1_f32(v322, v322);
    float32x2_t v360 = vtrn2_f32(v322, v322);
    int64_t v362 = v357 + 1;
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vtrn1_f32(v340, v340);
    float32x2_t v373 = vtrn2_f32(v340, v340);
    int64_t v375 = v370 + 1;
    float32x2_t v389 = v7[v388];
    float32x2_t v390 = vtrn1_f32(v384, v384);
    float32x2_t v391 = vtrn2_f32(v384, v384);
    int64_t v393 = v388 + 1;
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v203 = v7[v202];
    float32x2_t v204 = vmul_f32(v199, v198);
    float32x2_t v234 = v7[v233];
    float32x2_t v235 = vmul_f32(v230, v229);
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vmul_f32(v292, v291);
    float32x2_t v314 = v7[v313];
    float32x2_t v315 = vmul_f32(v310, v309);
    float32x2_t v363 = v7[v362];
    float32x2_t v364 = vmul_f32(v359, v358);
    float32x2_t v376 = v7[v375];
    float32x2_t v377 = vmul_f32(v372, v371);
    float32x2_t v394 = v7[v393];
    float32x2_t v395 = vmul_f32(v390, v389);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v206 = vfma_f32(v204, v200, v203);
    float32x2_t v237 = vfma_f32(v235, v231, v234);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v299 = vfma_f32(v297, v293, v296);
    float32x2_t v317 = vfma_f32(v315, v311, v314);
    float32x2_t v366 = vfma_f32(v364, v360, v363);
    float32x2_t v379 = vfma_f32(v377, v373, v376);
    float32x2_t v397 = vfma_f32(v395, v391, v394);
    float32x2_t v398 = vadd_f32(v64, v77);
    float32x2_t v399 = vsub_f32(v64, v77);
    float32x2_t v406 = vadd_f32(v126, v139);
    float32x2_t v407 = vsub_f32(v126, v139);
    float32x2_t v409 = vadd_f32(v206, v219);
    float32x2_t v410 = vsub_f32(v206, v219);
    float32x2_t v412 = vadd_f32(v286, v299);
    float32x2_t v413 = vsub_f32(v286, v299);
    float32x2_t v415 = vadd_f32(v366, v379);
    float32x2_t v416 = vsub_f32(v366, v379);
    float32x2_t v405 = vadd_f32(v398, v404);
    float32x2_t v408 = vadd_f32(v406, v157);
    float32x2_t v411 = vadd_f32(v409, v237);
    float32x2_t v414 = vadd_f32(v412, v317);
    float32x2_t v417 = vadd_f32(v415, v397);
    float32x2_t v468 = vadd_f32(v406, v415);
    float32x2_t v469 = vsub_f32(v406, v415);
    float32x2_t v470 = vadd_f32(v412, v409);
    float32x2_t v471 = vsub_f32(v412, v409);
    float32x2_t v518 = vadd_f32(v407, v416);
    float32x2_t v519 = vsub_f32(v407, v416);
    float32x2_t v520 = vadd_f32(v413, v410);
    float32x2_t v521 = vsub_f32(v413, v410);
    float32x2_t v418 = vadd_f32(v408, v417);
    float32x2_t v419 = vsub_f32(v408, v417);
    float32x2_t v420 = vadd_f32(v414, v411);
    float32x2_t v421 = vsub_f32(v414, v411);
    float32x2_t v472 = vadd_f32(v468, v470);
    float32x2_t v473 = vsub_f32(v468, v470);
    float32x2_t v474 = vadd_f32(v469, v471);
    float32x2_t v493 = vrev64_f32(v469);
    float32x2_t v507 = vrev64_f32(v471);
    float32x2_t v522 = vadd_f32(v518, v520);
    float32x2_t v523 = vsub_f32(v518, v520);
    float32x2_t v524 = vadd_f32(v519, v521);
    float32x2_t v550 = vmul_f32(v519, v549);
    float32x2_t v558 = vmul_f32(v521, v557);
    float32x2_t v422 = vadd_f32(v418, v420);
    float32x2_t v423 = vsub_f32(v418, v420);
    float32x2_t v424 = vadd_f32(v419, v421);
    float32x2_t v443 = vrev64_f32(v419);
    float32x2_t v457 = vrev64_f32(v421);
    float32x2_t v475 = vadd_f32(v472, v398);
    float32x2_t v483 = vmul_f32(v472, v482);
    float32x2_t v487 = vmul_f32(v473, v486);
    float32x2_t v494 = vmul_f32(v493, v492);
    float32x2_t v500 = vrev64_f32(v474);
    float32x2_t v508 = vmul_f32(v507, v506);
    float32x2_t v525 = vadd_f32(v522, v399);
    float32x2_t v538 = vrev64_f32(v522);
    float32x2_t v545 = vrev64_f32(v523);
    float32x2_t v554 = vmul_f32(v524, v553);
    float32x2_t v425 = vadd_f32(v422, v405);
    float32x2_t v433 = vmul_f32(v422, v432);
    float32x2_t v437 = vmul_f32(v423, v436);
    float32x2_t v444 = vmul_f32(v443, v442);
    float32x2_t v450 = vrev64_f32(v424);
    float32x2_t v458 = vmul_f32(v457, v456);
    float32x2_t v479 = vmul_f32(v475, v478);
    float32x2_t v501 = vmul_f32(v500, v499);
    float32x2_t v531 = vrev64_f32(v525);
    float32x2_t v539 = vmul_f32(v538, v537);
    float32x2_t v546 = vmul_f32(v545, v544);
    float32x2_t v562 = vsub_f32(v550, v554);
    float32x2_t v563 = vadd_f32(v554, v558);
    float32x2_t v451 = vmul_f32(v450, v449);
    float32x2_t v459 = vadd_f32(v425, v433);
    float32x2_t v509 = vadd_f32(v479, v483);
    float32x2_t v512 = vsub_f32(v494, v501);
    float32x2_t v513 = vadd_f32(v501, v508);
    float32x2_t v532 = vmul_f32(v531, v530);
    float32x2_t v568 = vadd_f32(v425, v479);
    v6[0] = v425;
    float32x2_t v460 = vadd_f32(v459, v437);
    float32x2_t v461 = vsub_f32(v459, v437);
    float32x2_t v462 = vsub_f32(v444, v451);
    float32x2_t v463 = vadd_f32(v451, v458);
    float32x2_t v510 = vadd_f32(v509, v487);
    float32x2_t v511 = vsub_f32(v509, v487);
    float32x2_t v559 = vadd_f32(v532, v539);
    float32x2_t v569 = vadd_f32(v568, v532);
    float32x2_t v570 = vsub_f32(v568, v532);
    float32x2_t v464 = vadd_f32(v460, v462);
    float32x2_t v465 = vsub_f32(v460, v462);
    float32x2_t v466 = vadd_f32(v461, v463);
    float32x2_t v467 = vsub_f32(v461, v463);
    float32x2_t v514 = vadd_f32(v510, v512);
    float32x2_t v515 = vsub_f32(v510, v512);
    float32x2_t v516 = vadd_f32(v511, v513);
    float32x2_t v517 = vsub_f32(v511, v513);
    float32x2_t v560 = vadd_f32(v559, v546);
    float32x2_t v561 = vsub_f32(v559, v546);
    v6[ostride * 10] = v570;
    v6[ostride * 5] = v569;
    float32x2_t v564 = vadd_f32(v560, v562);
    float32x2_t v565 = vsub_f32(v560, v562);
    float32x2_t v566 = vadd_f32(v561, v563);
    float32x2_t v567 = vsub_f32(v561, v563);
    float32x2_t v586 = vadd_f32(v465, v515);
    v6[ostride * 6] = v465;
    float32x2_t v604 = vadd_f32(v467, v517);
    v6[ostride * 12] = v467;
    float32x2_t v622 = vadd_f32(v466, v516);
    v6[ostride * 3] = v466;
    float32x2_t v640 = vadd_f32(v464, v514);
    v6[ostride * 9] = v464;
    float32x2_t v587 = vadd_f32(v586, v565);
    float32x2_t v588 = vsub_f32(v586, v565);
    float32x2_t v605 = vadd_f32(v604, v567);
    float32x2_t v606 = vsub_f32(v604, v567);
    float32x2_t v623 = vadd_f32(v622, v566);
    float32x2_t v624 = vsub_f32(v622, v566);
    float32x2_t v641 = vadd_f32(v640, v564);
    float32x2_t v642 = vsub_f32(v640, v564);
    v6[ostride] = v588;
    v6[ostride * 11] = v587;
    v6[ostride * 7] = v606;
    v6[ostride * 2] = v605;
    v6[ostride * 13] = v624;
    v6[ostride * 8] = v623;
    v6[ostride * 4] = v642;
    v6[ostride * 14] = v641;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs15(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v320 = -1.2500000000000000e+00F;
    float v325 = 5.5901699437494745e-01F;
    float v330 = -1.5388417685876268e+00F;
    float v337 = -5.8778525229247325e-01F;
    float v344 = -3.6327126400268028e-01F;
    float v368 = -1.4999999999999998e+00F;
    float v373 = 1.8749999999999998e+00F;
    float v378 = -8.3852549156242107e-01F;
    float v383 = 2.3082626528814396e+00F;
    float v390 = 8.8167787843870971e-01F;
    float v397 = 5.4490689600402031e-01F;
    float v421 = -8.6602540378443871e-01F;
    float v428 = 1.0825317547305484e+00F;
    float v435 = -4.8412291827592718e-01F;
    float v442 = -1.3326760640014592e+00F;
    float v447 = -5.0903696045512736e-01F;
    float v452 = -3.1460214309120460e-01F;
    const float32x2_t *v645 = &v5[v0];
    float32x2_t *v784 = &v6[v2];
    int64_t v19 = v0 * 5;
    int64_t v33 = v0 * 10;
    int64_t v48 = v10 * 4;
    int64_t v55 = v10 * 9;
    int64_t v61 = v0 * 8;
    int64_t v75 = v0 * 13;
    int64_t v90 = v10 * 7;
    int64_t v97 = v10 * 12;
    int64_t v103 = v0 * 3;
    int64_t v111 = v10 * 2;
    int64_t v117 = v0 * 11;
    int64_t v146 = v10 * 10;
    int64_t v159 = v0 * 6;
    int64_t v167 = v10 * 5;
    int64_t v173 = v0 * 14;
    int64_t v187 = v0 * 4;
    int64_t v202 = v10 * 13;
    int64_t v209 = v10 * 3;
    int64_t v215 = v0 * 9;
    int64_t v223 = v10 * 8;
    int64_t v229 = v0 * 2;
    int64_t v243 = v0 * 7;
    int64_t v265 = v10 * 6;
    int64_t v271 = v0 * 12;
    int64_t v279 = v10 * 11;
    int64_t v280 = v13 * 14;
    float v333 = v4 * v330;
    float v340 = v4 * v337;
    float v347 = v4 * v344;
    float v386 = v4 * v383;
    float v393 = v4 * v390;
    float v400 = v4 * v397;
    float v424 = v4 * v421;
    float v431 = v4 * v428;
    float v438 = v4 * v435;
    int64_t v476 = v2 * 10;
    int64_t v483 = v2 * 5;
    int64_t v493 = v2 * 6;
    int64_t v507 = v2 * 11;
    int64_t v517 = v2 * 12;
    int64_t v524 = v2 * 7;
    int64_t v531 = v2 * 2;
    int64_t v541 = v2 * 3;
    int64_t v548 = v2 * 13;
    int64_t v555 = v2 * 8;
    int64_t v565 = v2 * 9;
    int64_t v572 = v2 * 4;
    int64_t v579 = v2 * 14;
    const float32x2_t *v720 = &v5[0];
    svint64_t v721 = svindex_s64(0, v1);
    svfloat32_t v724 = svdup_n_f32(v320);
    svfloat32_t v725 = svdup_n_f32(v325);
    svfloat32_t v729 = svdup_n_f32(v368);
    svfloat32_t v730 = svdup_n_f32(v373);
    svfloat32_t v731 = svdup_n_f32(v378);
    svfloat32_t v738 = svdup_n_f32(v442);
    svfloat32_t v739 = svdup_n_f32(v447);
    svfloat32_t v740 = svdup_n_f32(v452);
    float32x2_t *v748 = &v6[0];
    svint64_t v875 = svindex_s64(0, v3);
    int64_t v50 = v48 + v280;
    int64_t v57 = v55 + v280;
    int64_t v92 = v90 + v280;
    int64_t v99 = v97 + v280;
    int64_t v113 = v111 + v280;
    int64_t v148 = v146 + v280;
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v280]));
    int64_t v169 = v167 + v280;
    int64_t v204 = v202 + v280;
    int64_t v211 = v209 + v280;
    int64_t v225 = v223 + v280;
    int64_t v260 = v10 + v280;
    int64_t v267 = v265 + v280;
    int64_t v281 = v279 + v280;
    const float32x2_t *v591 = &v5[v19];
    const float32x2_t *v600 = &v5[v33];
    const float32x2_t *v609 = &v5[v61];
    const float32x2_t *v618 = &v5[v75];
    const float32x2_t *v627 = &v5[v103];
    const float32x2_t *v636 = &v5[v117];
    svfloat32_t v647 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v645), v721));
    const float32x2_t *v656 = &v5[v159];
    const float32x2_t *v665 = &v5[v173];
    const float32x2_t *v674 = &v5[v187];
    const float32x2_t *v683 = &v5[v215];
    const float32x2_t *v692 = &v5[v229];
    const float32x2_t *v701 = &v5[v243];
    const float32x2_t *v710 = &v5[v271];
    svfloat32_t v722 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v720), v721));
    svfloat32_t v726 = svdup_n_f32(v333);
    svfloat32_t v727 = svdup_n_f32(v340);
    svfloat32_t v728 = svdup_n_f32(v347);
    svfloat32_t v732 = svdup_n_f32(v386);
    svfloat32_t v733 = svdup_n_f32(v393);
    svfloat32_t v734 = svdup_n_f32(v400);
    svfloat32_t v735 = svdup_n_f32(v424);
    svfloat32_t v736 = svdup_n_f32(v431);
    svfloat32_t v737 = svdup_n_f32(v438);
    float32x2_t *v757 = &v6[v476];
    float32x2_t *v766 = &v6[v483];
    float32x2_t *v775 = &v6[v493];
    float32x2_t *v793 = &v6[v507];
    float32x2_t *v802 = &v6[v517];
    float32x2_t *v811 = &v6[v524];
    float32x2_t *v820 = &v6[v531];
    float32x2_t *v829 = &v6[v541];
    float32x2_t *v838 = &v6[v548];
    float32x2_t *v847 = &v6[v555];
    float32x2_t *v856 = &v6[v565];
    float32x2_t *v865 = &v6[v572];
    float32x2_t *v874 = &v6[v579];
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v50]));
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v149 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v148]));
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v647, v156, 0),
                     v647, v156, 90);
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v593 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v591), v721));
    svfloat32_t v602 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v600), v721));
    svfloat32_t v611 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v609), v721));
    svfloat32_t v620 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v618), v721));
    svfloat32_t v629 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v627), v721));
    svfloat32_t v638 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v636), v721));
    svfloat32_t v658 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v656), v721));
    svfloat32_t v667 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v665), v721));
    svfloat32_t v676 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v674), v721));
    svfloat32_t v685 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v683), v721));
    svfloat32_t v694 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v692), v721));
    svfloat32_t v703 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v701), v721));
    svfloat32_t v712 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v710), v721));
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v593, v51, 0),
                     v593, v51, 90);
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v602, v58, 0),
                     v602, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v611, v93, 0),
                     v611, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v620, v100, 0),
                     v620, v100, 90);
    svfloat32_t zero150 = svdup_n_f32(0);
    svfloat32_t v150 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero150, v638, v149, 0),
                     v638, v149, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v667, v205, 0),
                     v667, v205, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v676, v212, 0),
                     v676, v212, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero262, v694, v261, 0),
                     v694, v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero269, v703, v268, 0),
                     v703, v268, 90);
    svfloat32_t v284 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v285 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v294 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v295 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v297 = svadd_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v298 = svsub_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v300 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v301 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v303 = svadd_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v304 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v293 = svadd_f32_x(svptrue_b32(), v284, v722);
    svfloat32_t v296 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v294, v629, v114, 0),
                     v629, v114, 90);
    svfloat32_t v299 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v297, v658, v170, 0),
                     v658, v170, 90);
    svfloat32_t v302 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v300, v685, v226, 0),
                     v685, v226, 90);
    svfloat32_t v305 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v303, v712, v282, 0),
                     v712, v282, 90);
    svfloat32_t v359 = svadd_f32_x(svptrue_b32(), v294, v303);
    svfloat32_t v360 = svsub_f32_x(svptrue_b32(), v294, v303);
    svfloat32_t v361 = svadd_f32_x(svptrue_b32(), v300, v297);
    svfloat32_t v362 = svsub_f32_x(svptrue_b32(), v300, v297);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v295, v304);
    svfloat32_t v413 = svsub_f32_x(svptrue_b32(), v295, v304);
    svfloat32_t v414 = svadd_f32_x(svptrue_b32(), v301, v298);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v301, v298);
    svfloat32_t v306 = svadd_f32_x(svptrue_b32(), v296, v305);
    svfloat32_t v307 = svsub_f32_x(svptrue_b32(), v296, v305);
    svfloat32_t v308 = svadd_f32_x(svptrue_b32(), v302, v299);
    svfloat32_t v309 = svsub_f32_x(svptrue_b32(), v302, v299);
    svfloat32_t v363 = svadd_f32_x(svptrue_b32(), v359, v361);
    svfloat32_t v364 = svsub_f32_x(svptrue_b32(), v359, v361);
    svfloat32_t v365 = svadd_f32_x(svptrue_b32(), v360, v362);
    svfloat32_t zero388 = svdup_n_f32(0);
    svfloat32_t v388 = svcmla_f32_x(pred_full, zero388, v732, v360, 90);
    svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v412, v414);
    svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v412, v414);
    svfloat32_t v418 = svadd_f32_x(svptrue_b32(), v413, v415);
    svfloat32_t v455 = svmul_f32_x(svptrue_b32(), v415, v740);
    svfloat32_t v310 = svadd_f32_x(svptrue_b32(), v306, v308);
    svfloat32_t v311 = svsub_f32_x(svptrue_b32(), v306, v308);
    svfloat32_t v312 = svadd_f32_x(svptrue_b32(), v307, v309);
    svfloat32_t zero335 = svdup_n_f32(0);
    svfloat32_t v335 = svcmla_f32_x(pred_full, zero335, v726, v307, 90);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v363, v284);
    svfloat32_t v376 = svmul_f32_x(svptrue_b32(), v363, v730);
    svfloat32_t zero395 = svdup_n_f32(0);
    svfloat32_t v395 = svcmla_f32_x(pred_full, zero395, v733, v365, 90);
    svfloat32_t v419 = svadd_f32_x(svptrue_b32(), v416, v285);
    svfloat32_t zero440 = svdup_n_f32(0);
    svfloat32_t v440 = svcmla_f32_x(pred_full, zero440, v737, v417, 90);
    svfloat32_t v450 = svmul_f32_x(svptrue_b32(), v418, v739);
    svfloat32_t v313 = svadd_f32_x(svptrue_b32(), v310, v293);
    svfloat32_t zero342 = svdup_n_f32(0);
    svfloat32_t v342 = svcmla_f32_x(pred_full, zero342, v727, v312, 90);
    svfloat32_t v406 = svsub_f32_x(svptrue_b32(), v388, v395);
    svfloat32_t v407 = svcmla_f32_x(pred_full, v395, v734, v362, 90);
    svfloat32_t zero426 = svdup_n_f32(0);
    svfloat32_t v426 = svcmla_f32_x(pred_full, zero426, v735, v419, 90);
    svfloat32_t v459 = svnmls_f32_x(pred_full, v450, v413, v738);
    svfloat32_t v460 = svmla_f32_x(pred_full, v455, v418, v739);
    svfloat32_t v350 = svmla_f32_x(pred_full, v313, v310, v724);
    svfloat32_t v353 = svsub_f32_x(svptrue_b32(), v335, v342);
    svfloat32_t v354 = svcmla_f32_x(pred_full, v342, v728, v309, 90);
    svfloat32_t v403 = svmla_f32_x(pred_full, v376, v366, v729);
    svfloat32_t v456 = svcmla_f32_x(pred_full, v426, v736, v416, 90);
    svfloat32_t v465 = svmla_f32_x(pred_full, v313, v366, v729);
    svst1_scatter_s64index_f64(pred_full, (double *)(v748), v875,
                               svreinterpret_f64_f32(v313));
    svfloat32_t v351 = svmla_f32_x(pred_full, v350, v311, v725);
    svfloat32_t v352 = svmls_f32_x(pred_full, v350, v311, v725);
    svfloat32_t v404 = svmla_f32_x(pred_full, v403, v364, v731);
    svfloat32_t v405 = svmls_f32_x(pred_full, v403, v364, v731);
    svfloat32_t v457 = svadd_f32_x(svptrue_b32(), v456, v440);
    svfloat32_t v458 = svsub_f32_x(svptrue_b32(), v456, v440);
    svfloat32_t v466 = svadd_f32_x(svptrue_b32(), v465, v426);
    svfloat32_t v467 = svsub_f32_x(svptrue_b32(), v465, v426);
    svfloat32_t v355 = svadd_f32_x(svptrue_b32(), v351, v353);
    svfloat32_t v356 = svsub_f32_x(svptrue_b32(), v351, v353);
    svfloat32_t v357 = svadd_f32_x(svptrue_b32(), v352, v354);
    svfloat32_t v358 = svsub_f32_x(svptrue_b32(), v352, v354);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v404, v406);
    svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v404, v406);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v405, v407);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v405, v407);
    svfloat32_t v461 = svadd_f32_x(svptrue_b32(), v457, v459);
    svfloat32_t v462 = svsub_f32_x(svptrue_b32(), v457, v459);
    svfloat32_t v463 = svadd_f32_x(svptrue_b32(), v458, v460);
    svfloat32_t v464 = svsub_f32_x(svptrue_b32(), v458, v460);
    svst1_scatter_s64index_f64(pred_full, (double *)(v757), v875,
                               svreinterpret_f64_f32(v467));
    svst1_scatter_s64index_f64(pred_full, (double *)(v766), v875,
                               svreinterpret_f64_f32(v466));
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v356, v409);
    svfloat32_t v513 = svadd_f32_x(svptrue_b32(), v358, v411);
    svfloat32_t v537 = svadd_f32_x(svptrue_b32(), v357, v410);
    svfloat32_t v561 = svadd_f32_x(svptrue_b32(), v355, v408);
    svst1_scatter_s64index_f64(pred_full, (double *)(v775), v875,
                               svreinterpret_f64_f32(v356));
    svst1_scatter_s64index_f64(pred_full, (double *)(v802), v875,
                               svreinterpret_f64_f32(v358));
    svst1_scatter_s64index_f64(pred_full, (double *)(v829), v875,
                               svreinterpret_f64_f32(v357));
    svst1_scatter_s64index_f64(pred_full, (double *)(v856), v875,
                               svreinterpret_f64_f32(v355));
    svfloat32_t v490 = svadd_f32_x(svptrue_b32(), v489, v462);
    svfloat32_t v491 = svsub_f32_x(svptrue_b32(), v489, v462);
    svfloat32_t v514 = svadd_f32_x(svptrue_b32(), v513, v464);
    svfloat32_t v515 = svsub_f32_x(svptrue_b32(), v513, v464);
    svfloat32_t v538 = svadd_f32_x(svptrue_b32(), v537, v463);
    svfloat32_t v539 = svsub_f32_x(svptrue_b32(), v537, v463);
    svfloat32_t v562 = svadd_f32_x(svptrue_b32(), v561, v461);
    svfloat32_t v563 = svsub_f32_x(svptrue_b32(), v561, v461);
    svst1_scatter_s64index_f64(pred_full, (double *)(v784), v875,
                               svreinterpret_f64_f32(v491));
    svst1_scatter_s64index_f64(pred_full, (double *)(v793), v875,
                               svreinterpret_f64_f32(v490));
    svst1_scatter_s64index_f64(pred_full, (double *)(v811), v875,
                               svreinterpret_f64_f32(v515));
    svst1_scatter_s64index_f64(pred_full, (double *)(v820), v875,
                               svreinterpret_f64_f32(v514));
    svst1_scatter_s64index_f64(pred_full, (double *)(v838), v875,
                               svreinterpret_f64_f32(v539));
    svst1_scatter_s64index_f64(pred_full, (double *)(v847), v875,
                               svreinterpret_f64_f32(v538));
    svst1_scatter_s64index_f64(pred_full, (double *)(v865), v875,
                               svreinterpret_f64_f32(v563));
    svst1_scatter_s64index_f64(pred_full, (double *)(v874), v875,
                               svreinterpret_f64_f32(v562));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs16(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v237 = v5[istride];
    float v571 = 1.0000000000000000e+00F;
    float v572 = -1.0000000000000000e+00F;
    float v579 = -7.0710678118654746e-01F;
    float v586 = 7.0710678118654757e-01F;
    float v589 = 9.2387953251128674e-01F;
    float v590 = -9.2387953251128674e-01F;
    float v597 = 5.4119610014619690e-01F;
    float v604 = -1.3065629648763766e+00F;
    float32x2_t v606 = (float32x2_t){v4, v4};
    float v611 = 3.8268343236508984e-01F;
    float v615 = 1.3065629648763766e+00F;
    float v619 = -5.4119610014619690e-01F;
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    float32x2_t v485 = v5[0];
    float32x2_t v573 = (float32x2_t){v571, v572};
    float32x2_t v580 = (float32x2_t){v586, v579};
    float32x2_t v587 = (float32x2_t){v586, v586};
    float32x2_t v591 = (float32x2_t){v589, v590};
    float32x2_t v598 = (float32x2_t){v619, v597};
    float32x2_t v605 = (float32x2_t){v615, v604};
    float32x2_t v612 = (float32x2_t){v611, v611};
    float32x2_t v616 = (float32x2_t){v615, v615};
    float32x2_t v620 = (float32x2_t){v619, v619};
    float32x2_t v20 = v5[istride * 8];
    int64_t v37 = 14 + j * 30;
    float32x2_t v51 = v5[istride * 4];
    float32x2_t v69 = v5[istride * 12];
    int64_t v86 = 6 + j * 30;
    int64_t v99 = 22 + j * 30;
    float32x2_t v113 = v5[istride * 2];
    float32x2_t v131 = v5[istride * 10];
    int64_t v148 = 2 + j * 30;
    int64_t v161 = 18 + j * 30;
    float32x2_t v175 = v5[istride * 6];
    float32x2_t v193 = v5[istride * 14];
    int64_t v210 = 10 + j * 30;
    int64_t v223 = 26 + j * 30;
    float32x2_t v255 = v5[istride * 9];
    float32x2_t v273 = v7[j * 30];
    int64_t v277 = j * 30 + 1;
    int64_t v285 = 16 + j * 30;
    float32x2_t v299 = v5[istride * 5];
    float32x2_t v317 = v5[istride * 13];
    int64_t v334 = 8 + j * 30;
    int64_t v347 = 24 + j * 30;
    float32x2_t v361 = v5[istride * 3];
    float32x2_t v379 = v5[istride * 11];
    int64_t v396 = 4 + j * 30;
    int64_t v409 = 20 + j * 30;
    float32x2_t v423 = v5[istride * 7];
    float32x2_t v441 = v5[istride * 15];
    int64_t v458 = 12 + j * 30;
    int64_t v471 = 28 + j * 30;
    float32x2_t v575 = vmul_f32(v606, v573);
    float32x2_t v582 = vmul_f32(v606, v580);
    float32x2_t v593 = vmul_f32(v606, v591);
    float32x2_t v600 = vmul_f32(v606, v598);
    float32x2_t v607 = vmul_f32(v606, v605);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v286 = v7[v285];
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    int64_t v290 = v285 + 1;
    float32x2_t v335 = v7[v334];
    float32x2_t v336 = vtrn1_f32(v299, v299);
    float32x2_t v337 = vtrn2_f32(v299, v299);
    int64_t v339 = v334 + 1;
    float32x2_t v348 = v7[v347];
    float32x2_t v349 = vtrn1_f32(v317, v317);
    float32x2_t v350 = vtrn2_f32(v317, v317);
    int64_t v352 = v347 + 1;
    float32x2_t v397 = v7[v396];
    float32x2_t v398 = vtrn1_f32(v361, v361);
    float32x2_t v399 = vtrn2_f32(v361, v361);
    int64_t v401 = v396 + 1;
    float32x2_t v410 = v7[v409];
    float32x2_t v411 = vtrn1_f32(v379, v379);
    float32x2_t v412 = vtrn2_f32(v379, v379);
    int64_t v414 = v409 + 1;
    float32x2_t v459 = v7[v458];
    float32x2_t v460 = vtrn1_f32(v423, v423);
    float32x2_t v461 = vtrn2_f32(v423, v423);
    int64_t v463 = v458 + 1;
    float32x2_t v472 = v7[v471];
    float32x2_t v473 = vtrn1_f32(v441, v441);
    float32x2_t v474 = vtrn2_f32(v441, v441);
    int64_t v476 = v471 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vmul_f32(v336, v335);
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vmul_f32(v349, v348);
    float32x2_t v402 = v7[v401];
    float32x2_t v403 = vmul_f32(v398, v397);
    float32x2_t v415 = v7[v414];
    float32x2_t v416 = vmul_f32(v411, v410);
    float32x2_t v464 = v7[v463];
    float32x2_t v465 = vmul_f32(v460, v459);
    float32x2_t v477 = v7[v476];
    float32x2_t v478 = vmul_f32(v473, v472);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v343 = vfma_f32(v341, v337, v340);
    float32x2_t v356 = vfma_f32(v354, v350, v353);
    float32x2_t v405 = vfma_f32(v403, v399, v402);
    float32x2_t v418 = vfma_f32(v416, v412, v415);
    float32x2_t v467 = vfma_f32(v465, v461, v464);
    float32x2_t v480 = vfma_f32(v478, v474, v477);
    float32x2_t v486 = vadd_f32(v485, v46);
    float32x2_t v487 = vsub_f32(v485, v46);
    float32x2_t v488 = vadd_f32(v95, v108);
    float32x2_t v489 = vsub_f32(v95, v108);
    float32x2_t v490 = vadd_f32(v157, v170);
    float32x2_t v491 = vsub_f32(v157, v170);
    float32x2_t v492 = vadd_f32(v219, v232);
    float32x2_t v493 = vsub_f32(v219, v232);
    float32x2_t v494 = vadd_f32(v281, v294);
    float32x2_t v495 = vsub_f32(v281, v294);
    float32x2_t v496 = vadd_f32(v343, v356);
    float32x2_t v497 = vsub_f32(v343, v356);
    float32x2_t v498 = vadd_f32(v405, v418);
    float32x2_t v499 = vsub_f32(v405, v418);
    float32x2_t v500 = vadd_f32(v467, v480);
    float32x2_t v501 = vsub_f32(v467, v480);
    float32x2_t v502 = vadd_f32(v486, v488);
    float32x2_t v503 = vsub_f32(v486, v488);
    float32x2_t v504 = vadd_f32(v490, v492);
    float32x2_t v505 = vsub_f32(v490, v492);
    float32x2_t v506 = vadd_f32(v494, v496);
    float32x2_t v507 = vsub_f32(v494, v496);
    float32x2_t v508 = vadd_f32(v498, v500);
    float32x2_t v509 = vsub_f32(v498, v500);
    float32x2_t v518 = vadd_f32(v491, v493);
    float32x2_t v519 = vsub_f32(v491, v493);
    float32x2_t v520 = vadd_f32(v495, v501);
    float32x2_t v521 = vsub_f32(v495, v501);
    float32x2_t v522 = vadd_f32(v497, v499);
    float32x2_t v523 = vsub_f32(v497, v499);
    float32x2_t v576 = vrev64_f32(v489);
    float32x2_t v510 = vadd_f32(v502, v504);
    float32x2_t v511 = vsub_f32(v502, v504);
    float32x2_t v512 = vadd_f32(v506, v508);
    float32x2_t v513 = vsub_f32(v506, v508);
    float32x2_t v516 = vadd_f32(v507, v509);
    float32x2_t v517 = vsub_f32(v507, v509);
    float32x2_t v524 = vadd_f32(v520, v522);
    float32x2_t v525 = vadd_f32(v521, v523);
    float32x2_t v554 = vrev64_f32(v505);
    float32x2_t v577 = vmul_f32(v576, v575);
    float32x2_t v583 = vrev64_f32(v518);
    float32x2_t v588 = vmul_f32(v519, v587);
    float32x2_t v601 = vrev64_f32(v520);
    float32x2_t v608 = vrev64_f32(v522);
    float32x2_t v617 = vmul_f32(v521, v616);
    float32x2_t v621 = vmul_f32(v523, v620);
    float32x2_t v514 = vadd_f32(v510, v512);
    float32x2_t v515 = vsub_f32(v510, v512);
    float32x2_t v543 = vrev64_f32(v513);
    float32x2_t v555 = vmul_f32(v554, v575);
    float32x2_t v561 = vrev64_f32(v516);
    float32x2_t v566 = vmul_f32(v517, v587);
    float32x2_t v584 = vmul_f32(v583, v582);
    float32x2_t v594 = vrev64_f32(v524);
    float32x2_t v602 = vmul_f32(v601, v600);
    float32x2_t v609 = vmul_f32(v608, v607);
    float32x2_t v613 = vmul_f32(v525, v612);
    float32x2_t v632 = vadd_f32(v487, v588);
    float32x2_t v633 = vsub_f32(v487, v588);
    float32x2_t v544 = vmul_f32(v543, v575);
    float32x2_t v562 = vmul_f32(v561, v582);
    float32x2_t v595 = vmul_f32(v594, v593);
    float32x2_t v624 = vadd_f32(v503, v566);
    float32x2_t v626 = vsub_f32(v503, v566);
    float32x2_t v634 = vadd_f32(v577, v584);
    float32x2_t v635 = vsub_f32(v577, v584);
    float32x2_t v638 = vsub_f32(v617, v613);
    float32x2_t v639 = vsub_f32(v621, v613);
    float32x2_t v640 = vsub_f32(v613, v617);
    float32x2_t v641 = vsub_f32(v613, v621);
    v6[0] = v514;
    v6[ostride * 8] = v515;
    float32x2_t v622 = vadd_f32(v511, v544);
    float32x2_t v623 = vsub_f32(v511, v544);
    float32x2_t v625 = vadd_f32(v555, v562);
    float32x2_t v627 = vsub_f32(v562, v555);
    float32x2_t v636 = vadd_f32(v595, v602);
    float32x2_t v637 = vsub_f32(v595, v609);
    float32x2_t v642 = vadd_f32(v632, v638);
    float32x2_t v643 = vsub_f32(v632, v638);
    float32x2_t v644 = vadd_f32(v632, v640);
    float32x2_t v645 = vsub_f32(v632, v640);
    float32x2_t v646 = vadd_f32(v633, v635);
    float32x2_t v647 = vsub_f32(v633, v635);
    float32x2_t v648 = vadd_f32(v633, v641);
    float32x2_t v649 = vsub_f32(v633, v641);
    float32x2_t v628 = vadd_f32(v624, v625);
    float32x2_t v629 = vadd_f32(v626, v627);
    float32x2_t v630 = vsub_f32(v626, v627);
    float32x2_t v631 = vsub_f32(v624, v625);
    float32x2_t v652 = vadd_f32(v636, v634);
    float32x2_t v653 = vsub_f32(v636, v634);
    float32x2_t v654 = vadd_f32(v637, v639);
    float32x2_t v655 = vsub_f32(v637, v639);
    float32x2_t v656 = vadd_f32(v637, v635);
    float32x2_t v657 = vsub_f32(v637, v635);
    v6[ostride * 4] = v623;
    v6[ostride * 12] = v622;
    float32x2_t v658 = vadd_f32(v642, v652);
    float32x2_t v659 = vadd_f32(v643, v653);
    float32x2_t v660 = vsub_f32(v644, v653);
    float32x2_t v661 = vsub_f32(v645, v652);
    float32x2_t v662 = vadd_f32(v646, v654);
    float32x2_t v663 = vadd_f32(v647, v655);
    float32x2_t v664 = vsub_f32(v648, v657);
    float32x2_t v665 = vsub_f32(v649, v656);
    v6[ostride * 2] = v631;
    v6[ostride * 6] = v630;
    v6[ostride * 10] = v629;
    v6[ostride * 14] = v628;
    v6[ostride] = v661;
    v6[ostride * 3] = v664;
    v6[ostride * 5] = v665;
    v6[ostride * 7] = v660;
    v6[ostride * 9] = v659;
    v6[ostride * 11] = v662;
    v6[ostride * 13] = v663;
    v6[ostride * 15] = v658;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs16(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v432 = -1.0000000000000000e+00F;
    float v439 = -7.0710678118654746e-01F;
    float v446 = 7.0710678118654757e-01F;
    float v451 = -9.2387953251128674e-01F;
    float v458 = 5.4119610014619690e-01F;
    float v465 = -1.3065629648763766e+00F;
    float v472 = 3.8268343236508984e-01F;
    float v477 = 1.3065629648763766e+00F;
    float v482 = -5.4119610014619690e-01F;
    const float32x2_t *v711 = &v5[v0];
    float32x2_t *v823 = &v6[v2];
    int64_t v19 = v0 * 8;
    int64_t v34 = v10 * 7;
    int64_t v40 = v0 * 4;
    int64_t v54 = v0 * 12;
    int64_t v69 = v10 * 3;
    int64_t v76 = v10 * 11;
    int64_t v82 = v0 * 2;
    int64_t v96 = v0 * 10;
    int64_t v118 = v10 * 9;
    int64_t v124 = v0 * 6;
    int64_t v138 = v0 * 14;
    int64_t v153 = v10 * 5;
    int64_t v160 = v10 * 13;
    int64_t v180 = v0 * 9;
    int64_t v202 = v10 * 8;
    int64_t v208 = v0 * 5;
    int64_t v222 = v0 * 13;
    int64_t v237 = v10 * 4;
    int64_t v244 = v10 * 12;
    int64_t v250 = v0 * 3;
    int64_t v264 = v0 * 11;
    int64_t v279 = v10 * 2;
    int64_t v286 = v10 * 10;
    int64_t v292 = v0 * 7;
    int64_t v306 = v0 * 15;
    int64_t v321 = v10 * 6;
    int64_t v328 = v10 * 14;
    int64_t v329 = v13 * 15;
    float v435 = v4 * v432;
    float v442 = v4 * v439;
    float v454 = v4 * v451;
    float v461 = v4 * v458;
    float v468 = v4 * v465;
    int64_t v545 = v2 * 2;
    int64_t v552 = v2 * 3;
    int64_t v559 = v2 * 4;
    int64_t v566 = v2 * 5;
    int64_t v573 = v2 * 6;
    int64_t v580 = v2 * 7;
    int64_t v587 = v2 * 8;
    int64_t v594 = v2 * 9;
    int64_t v601 = v2 * 10;
    int64_t v608 = v2 * 11;
    int64_t v615 = v2 * 12;
    int64_t v622 = v2 * 13;
    int64_t v629 = v2 * 14;
    int64_t v636 = v2 * 15;
    const float32x2_t *v786 = &v5[0];
    svint64_t v787 = svindex_s64(0, v1);
    svfloat32_t v800 = svdup_n_f32(v446);
    svfloat32_t v804 = svdup_n_f32(v472);
    svfloat32_t v805 = svdup_n_f32(v477);
    svfloat32_t v806 = svdup_n_f32(v482);
    float32x2_t *v814 = &v6[0];
    svint64_t v950 = svindex_s64(0, v3);
    int64_t v36 = v34 + v329;
    int64_t v71 = v69 + v329;
    int64_t v78 = v76 + v329;
    int64_t v113 = v10 + v329;
    int64_t v120 = v118 + v329;
    int64_t v155 = v153 + v329;
    int64_t v162 = v160 + v329;
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v329]));
    int64_t v204 = v202 + v329;
    int64_t v239 = v237 + v329;
    int64_t v246 = v244 + v329;
    int64_t v281 = v279 + v329;
    int64_t v288 = v286 + v329;
    int64_t v323 = v321 + v329;
    int64_t v330 = v328 + v329;
    const float32x2_t *v648 = &v5[v19];
    const float32x2_t *v657 = &v5[v40];
    const float32x2_t *v666 = &v5[v54];
    const float32x2_t *v675 = &v5[v82];
    const float32x2_t *v684 = &v5[v96];
    const float32x2_t *v693 = &v5[v124];
    const float32x2_t *v702 = &v5[v138];
    svfloat32_t v713 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v711), v787));
    const float32x2_t *v721 = &v5[v180];
    const float32x2_t *v731 = &v5[v208];
    const float32x2_t *v740 = &v5[v222];
    const float32x2_t *v749 = &v5[v250];
    const float32x2_t *v758 = &v5[v264];
    const float32x2_t *v767 = &v5[v292];
    const float32x2_t *v776 = &v5[v306];
    svfloat32_t v788 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v786), v787));
    svfloat32_t v798 = svdup_n_f32(v435);
    svfloat32_t v799 = svdup_n_f32(v442);
    svfloat32_t v801 = svdup_n_f32(v454);
    svfloat32_t v802 = svdup_n_f32(v461);
    svfloat32_t v803 = svdup_n_f32(v468);
    float32x2_t *v832 = &v6[v545];
    float32x2_t *v841 = &v6[v552];
    float32x2_t *v850 = &v6[v559];
    float32x2_t *v859 = &v6[v566];
    float32x2_t *v868 = &v6[v573];
    float32x2_t *v877 = &v6[v580];
    float32x2_t *v886 = &v6[v587];
    float32x2_t *v895 = &v6[v594];
    float32x2_t *v904 = &v6[v601];
    float32x2_t *v913 = &v6[v608];
    float32x2_t *v922 = &v6[v615];
    float32x2_t *v931 = &v6[v622];
    float32x2_t *v940 = &v6[v629];
    float32x2_t *v949 = &v6[v636];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v713, v198, 0),
                     v713, v198, 90);
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v246]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v288]));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v331 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v330]));
    svfloat32_t v650 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v648), v787));
    svfloat32_t v659 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v657), v787));
    svfloat32_t v668 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v666), v787));
    svfloat32_t v677 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v675), v787));
    svfloat32_t v686 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v684), v787));
    svfloat32_t v695 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v693), v787));
    svfloat32_t v704 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v702), v787));
    svfloat32_t v723 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v721), v787));
    svfloat32_t v733 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v731), v787));
    svfloat32_t v742 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v740), v787));
    svfloat32_t v751 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v749), v787));
    svfloat32_t v760 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v758), v787));
    svfloat32_t v769 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v767), v787));
    svfloat32_t v778 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v776), v787));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v650, v37, 0),
                     v650, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v659, v72, 0),
                     v659, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v668, v79, 0),
                     v668, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v677, v114, 0),
                     v677, v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v686, v121, 0),
                     v686, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v695, v156, 0),
                     v695, v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v704, v163, 0),
                     v704, v163, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v723, v205, 0),
                     v723, v205, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero241, v733, v240, 0),
                     v733, v240, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero248, v742, v247, 0),
                     v742, v247, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero283, v751, v282, 0),
                     v751, v282, 90);
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero290, v760, v289, 0),
                     v760, v289, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero325, v769, v324, 0),
                     v769, v324, 90);
    svfloat32_t zero332 = svdup_n_f32(0);
    svfloat32_t v332 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero332, v778, v331, 0),
                     v778, v331, 90);
    svfloat32_t v340 = svadd_f32_x(svptrue_b32(), v788, v38);
    svfloat32_t v341 = svsub_f32_x(svptrue_b32(), v788, v38);
    svfloat32_t v342 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v343 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v344 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v345 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v346 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v347 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v348 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v349 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v350 = svadd_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v351 = svsub_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v352 = svadd_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v353 = svsub_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v354 = svadd_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v355 = svsub_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v356 = svadd_f32_x(svptrue_b32(), v340, v342);
    svfloat32_t v357 = svsub_f32_x(svptrue_b32(), v340, v342);
    svfloat32_t v358 = svadd_f32_x(svptrue_b32(), v344, v346);
    svfloat32_t v359 = svsub_f32_x(svptrue_b32(), v344, v346);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v348, v350);
    svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v348, v350);
    svfloat32_t v362 = svadd_f32_x(svptrue_b32(), v352, v354);
    svfloat32_t v363 = svsub_f32_x(svptrue_b32(), v352, v354);
    svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v345, v347);
    svfloat32_t v373 = svsub_f32_x(svptrue_b32(), v345, v347);
    svfloat32_t v374 = svadd_f32_x(svptrue_b32(), v349, v355);
    svfloat32_t v375 = svsub_f32_x(svptrue_b32(), v349, v355);
    svfloat32_t v376 = svadd_f32_x(svptrue_b32(), v351, v353);
    svfloat32_t v377 = svsub_f32_x(svptrue_b32(), v351, v353);
    svfloat32_t zero437 = svdup_n_f32(0);
    svfloat32_t v437 = svcmla_f32_x(pred_full, zero437, v798, v343, 90);
    svfloat32_t v364 = svadd_f32_x(svptrue_b32(), v356, v358);
    svfloat32_t v365 = svsub_f32_x(svptrue_b32(), v356, v358);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v360, v362);
    svfloat32_t v367 = svsub_f32_x(svptrue_b32(), v360, v362);
    svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v361, v363);
    svfloat32_t v371 = svsub_f32_x(svptrue_b32(), v361, v363);
    svfloat32_t v378 = svadd_f32_x(svptrue_b32(), v374, v376);
    svfloat32_t v379 = svadd_f32_x(svptrue_b32(), v375, v377);
    svfloat32_t zero413 = svdup_n_f32(0);
    svfloat32_t v413 = svcmla_f32_x(pred_full, zero413, v798, v359, 90);
    svfloat32_t zero444 = svdup_n_f32(0);
    svfloat32_t v444 = svcmla_f32_x(pred_full, zero444, v799, v372, 90);
    svfloat32_t zero470 = svdup_n_f32(0);
    svfloat32_t v470 = svcmla_f32_x(pred_full, zero470, v803, v376, 90);
    svfloat32_t v480 = svmul_f32_x(svptrue_b32(), v375, v805);
    svfloat32_t v485 = svmul_f32_x(svptrue_b32(), v377, v806);
    svfloat32_t v368 = svadd_f32_x(svptrue_b32(), v364, v366);
    svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v364, v366);
    svfloat32_t zero401 = svdup_n_f32(0);
    svfloat32_t v401 = svcmla_f32_x(pred_full, zero401, v798, v367, 90);
    svfloat32_t zero420 = svdup_n_f32(0);
    svfloat32_t v420 = svcmla_f32_x(pred_full, zero420, v799, v370, 90);
    svfloat32_t zero456 = svdup_n_f32(0);
    svfloat32_t v456 = svcmla_f32_x(pred_full, zero456, v801, v378, 90);
    svfloat32_t v475 = svmul_f32_x(svptrue_b32(), v379, v804);
    svfloat32_t v496 = svmla_f32_x(pred_full, v341, v373, v800);
    svfloat32_t v497 = svmls_f32_x(pred_full, v341, v373, v800);
    svfloat32_t v498 = svadd_f32_x(svptrue_b32(), v437, v444);
    svfloat32_t v499 = svsub_f32_x(svptrue_b32(), v437, v444);
    svfloat32_t v486 = svadd_f32_x(svptrue_b32(), v365, v401);
    svfloat32_t v487 = svsub_f32_x(svptrue_b32(), v365, v401);
    svfloat32_t v488 = svmla_f32_x(pred_full, v357, v371, v800);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v413, v420);
    svfloat32_t v490 = svmls_f32_x(pred_full, v357, v371, v800);
    svfloat32_t v491 = svsub_f32_x(svptrue_b32(), v420, v413);
    svfloat32_t v500 = svcmla_f32_x(pred_full, v456, v802, v374, 90);
    svfloat32_t v501 = svsub_f32_x(svptrue_b32(), v456, v470);
    svfloat32_t v502 = svnmls_f32_x(pred_full, v475, v375, v805);
    svfloat32_t v503 = svnmls_f32_x(pred_full, v475, v377, v806);
    svfloat32_t v504 = svnmls_f32_x(pred_full, v480, v379, v804);
    svfloat32_t v505 = svnmls_f32_x(pred_full, v485, v379, v804);
    svfloat32_t v510 = svadd_f32_x(svptrue_b32(), v497, v499);
    svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v497, v499);
    svst1_scatter_s64index_f64(pred_full, (double *)(v814), v950,
                               svreinterpret_f64_f32(v368));
    svst1_scatter_s64index_f64(pred_full, (double *)(v886), v950,
                               svreinterpret_f64_f32(v369));
    svfloat32_t v492 = svadd_f32_x(svptrue_b32(), v488, v489);
    svfloat32_t v493 = svadd_f32_x(svptrue_b32(), v490, v491);
    svfloat32_t v494 = svsub_f32_x(svptrue_b32(), v490, v491);
    svfloat32_t v495 = svsub_f32_x(svptrue_b32(), v488, v489);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v496, v502);
    svfloat32_t v507 = svsub_f32_x(svptrue_b32(), v496, v502);
    svfloat32_t v508 = svadd_f32_x(svptrue_b32(), v496, v504);
    svfloat32_t v509 = svsub_f32_x(svptrue_b32(), v496, v504);
    svfloat32_t v512 = svadd_f32_x(svptrue_b32(), v497, v505);
    svfloat32_t v513 = svsub_f32_x(svptrue_b32(), v497, v505);
    svfloat32_t v516 = svadd_f32_x(svptrue_b32(), v500, v498);
    svfloat32_t v517 = svsub_f32_x(svptrue_b32(), v500, v498);
    svfloat32_t v518 = svadd_f32_x(svptrue_b32(), v501, v503);
    svfloat32_t v519 = svsub_f32_x(svptrue_b32(), v501, v503);
    svfloat32_t v520 = svadd_f32_x(svptrue_b32(), v501, v499);
    svfloat32_t v521 = svsub_f32_x(svptrue_b32(), v501, v499);
    svst1_scatter_s64index_f64(pred_full, (double *)(v850), v950,
                               svreinterpret_f64_f32(v487));
    svst1_scatter_s64index_f64(pred_full, (double *)(v922), v950,
                               svreinterpret_f64_f32(v486));
    svfloat32_t v522 = svadd_f32_x(svptrue_b32(), v506, v516);
    svfloat32_t v523 = svadd_f32_x(svptrue_b32(), v507, v517);
    svfloat32_t v524 = svsub_f32_x(svptrue_b32(), v508, v517);
    svfloat32_t v525 = svsub_f32_x(svptrue_b32(), v509, v516);
    svfloat32_t v526 = svadd_f32_x(svptrue_b32(), v510, v518);
    svfloat32_t v527 = svadd_f32_x(svptrue_b32(), v511, v519);
    svfloat32_t v528 = svsub_f32_x(svptrue_b32(), v512, v521);
    svfloat32_t v529 = svsub_f32_x(svptrue_b32(), v513, v520);
    svst1_scatter_s64index_f64(pred_full, (double *)(v832), v950,
                               svreinterpret_f64_f32(v495));
    svst1_scatter_s64index_f64(pred_full, (double *)(v868), v950,
                               svreinterpret_f64_f32(v494));
    svst1_scatter_s64index_f64(pred_full, (double *)(v904), v950,
                               svreinterpret_f64_f32(v493));
    svst1_scatter_s64index_f64(pred_full, (double *)(v940), v950,
                               svreinterpret_f64_f32(v492));
    svst1_scatter_s64index_f64(pred_full, (double *)(v823), v950,
                               svreinterpret_f64_f32(v525));
    svst1_scatter_s64index_f64(pred_full, (double *)(v841), v950,
                               svreinterpret_f64_f32(v528));
    svst1_scatter_s64index_f64(pred_full, (double *)(v859), v950,
                               svreinterpret_f64_f32(v529));
    svst1_scatter_s64index_f64(pred_full, (double *)(v877), v950,
                               svreinterpret_f64_f32(v524));
    svst1_scatter_s64index_f64(pred_full, (double *)(v895), v950,
                               svreinterpret_f64_f32(v523));
    svst1_scatter_s64index_f64(pred_full, (double *)(v913), v950,
                               svreinterpret_f64_f32(v526));
    svst1_scatter_s64index_f64(pred_full, (double *)(v931), v950,
                               svreinterpret_f64_f32(v527));
    svst1_scatter_s64index_f64(pred_full, (double *)(v949), v950,
                               svreinterpret_f64_f32(v522));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs17(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v589 = -4.2602849117736000e-02F;
    float v593 = 2.0497965023262180e-01F;
    float v597 = 1.0451835201736759e+00F;
    float v601 = 1.7645848660222969e+00F;
    float v605 = -7.2340797728605655e-01F;
    float v609 = -8.9055591620606403e-02F;
    float v613 = -1.0625000000000000e+00F;
    float v617 = 2.5769410160110379e-01F;
    float v621 = 7.7980260789483757e-01F;
    float v625 = 5.4389318464570580e-01F;
    float v629 = 4.2010193497052700e-01F;
    float v633 = 1.2810929434228073e+00F;
    float v637 = 4.4088907348175338e-01F;
    float v641 = 3.1717619283272508e-01F;
    float v644 = -9.0138318648016680e-01F;
    float v645 = 9.0138318648016680e-01F;
    float v651 = -4.3248756360072310e-01F;
    float v652 = 4.3248756360072310e-01F;
    float v658 = 6.6693537504044498e-01F;
    float v659 = -6.6693537504044498e-01F;
    float v665 = -6.0389004312516970e-01F;
    float v666 = 6.0389004312516970e-01F;
    float v672 = -3.6924873198582547e-01F;
    float v673 = 3.6924873198582547e-01F;
    float v679 = 4.8656938755549761e-01F;
    float v680 = -4.8656938755549761e-01F;
    float v686 = 2.3813712136760609e-01F;
    float v687 = -2.3813712136760609e-01F;
    float v693 = -1.5573820617422458e+00F;
    float v694 = 1.5573820617422458e+00F;
    float v700 = 6.5962247018731990e-01F;
    float v701 = -6.5962247018731990e-01F;
    float v707 = -1.4316961569866241e-01F;
    float v708 = 1.4316961569866241e-01F;
    float v714 = 2.3903469959860771e-01F;
    float v715 = -2.3903469959860771e-01F;
    float v721 = -4.7932541949972603e-02F;
    float v722 = 4.7932541949972603e-02F;
    float v728 = -2.3188014856550065e+00F;
    float v729 = 2.3188014856550065e+00F;
    float v735 = 7.8914568419206255e-01F;
    float v736 = -7.8914568419206255e-01F;
    float v742 = 3.8484572871179505e+00F;
    float v743 = -3.8484572871179505e+00F;
    float v749 = -1.3003804568801376e+00F;
    float v750 = 1.3003804568801376e+00F;
    float v756 = 4.0814769046889037e+00F;
    float v757 = -4.0814769046889037e+00F;
    float v763 = -1.4807159909286283e+00F;
    float v764 = 1.4807159909286283e+00F;
    float v770 = -1.3332470363551400e-02F;
    float v771 = 1.3332470363551400e-02F;
    float v777 = -3.7139778690557629e-01F;
    float v778 = 3.7139778690557629e-01F;
    float v784 = 1.9236512863456379e-01F;
    float v785 = -1.9236512863456379e-01F;
    float32x2_t v787 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v582 = v5[0];
    float32x2_t v590 = (float32x2_t){v589, v589};
    float32x2_t v594 = (float32x2_t){v593, v593};
    float32x2_t v598 = (float32x2_t){v597, v597};
    float32x2_t v602 = (float32x2_t){v601, v601};
    float32x2_t v606 = (float32x2_t){v605, v605};
    float32x2_t v610 = (float32x2_t){v609, v609};
    float32x2_t v614 = (float32x2_t){v613, v613};
    float32x2_t v618 = (float32x2_t){v617, v617};
    float32x2_t v622 = (float32x2_t){v621, v621};
    float32x2_t v626 = (float32x2_t){v625, v625};
    float32x2_t v630 = (float32x2_t){v629, v629};
    float32x2_t v634 = (float32x2_t){v633, v633};
    float32x2_t v638 = (float32x2_t){v637, v637};
    float32x2_t v642 = (float32x2_t){v641, v641};
    float32x2_t v646 = (float32x2_t){v644, v645};
    float32x2_t v653 = (float32x2_t){v651, v652};
    float32x2_t v660 = (float32x2_t){v658, v659};
    float32x2_t v667 = (float32x2_t){v665, v666};
    float32x2_t v674 = (float32x2_t){v672, v673};
    float32x2_t v681 = (float32x2_t){v679, v680};
    float32x2_t v688 = (float32x2_t){v686, v687};
    float32x2_t v695 = (float32x2_t){v693, v694};
    float32x2_t v702 = (float32x2_t){v700, v701};
    float32x2_t v709 = (float32x2_t){v707, v708};
    float32x2_t v716 = (float32x2_t){v714, v715};
    float32x2_t v723 = (float32x2_t){v721, v722};
    float32x2_t v730 = (float32x2_t){v728, v729};
    float32x2_t v737 = (float32x2_t){v735, v736};
    float32x2_t v744 = (float32x2_t){v742, v743};
    float32x2_t v751 = (float32x2_t){v749, v750};
    float32x2_t v758 = (float32x2_t){v756, v757};
    float32x2_t v765 = (float32x2_t){v763, v764};
    float32x2_t v772 = (float32x2_t){v770, v771};
    float32x2_t v779 = (float32x2_t){v777, v778};
    float32x2_t v786 = (float32x2_t){v784, v785};
    float32x2_t v38 = v5[istride * 16];
    float32x2_t v56 = v7[j * 32];
    int64_t v60 = j * 32 + 1;
    int64_t v68 = 30 + j * 32;
    float32x2_t v82 = v5[istride * 3];
    float32x2_t v100 = v5[istride * 14];
    int64_t v117 = 4 + j * 32;
    int64_t v130 = 26 + j * 32;
    float32x2_t v144 = v5[istride * 9];
    float32x2_t v162 = v5[istride * 8];
    int64_t v179 = 16 + j * 32;
    int64_t v192 = 14 + j * 32;
    float32x2_t v206 = v5[istride * 10];
    float32x2_t v224 = v5[istride * 7];
    int64_t v241 = 18 + j * 32;
    int64_t v254 = 12 + j * 32;
    float32x2_t v268 = v5[istride * 13];
    float32x2_t v286 = v5[istride * 4];
    int64_t v303 = 24 + j * 32;
    int64_t v316 = 6 + j * 32;
    float32x2_t v330 = v5[istride * 5];
    float32x2_t v348 = v5[istride * 12];
    int64_t v365 = 8 + j * 32;
    int64_t v378 = 22 + j * 32;
    float32x2_t v392 = v5[istride * 15];
    float32x2_t v410 = v5[istride * 2];
    int64_t v427 = 28 + j * 32;
    int64_t v440 = 2 + j * 32;
    float32x2_t v454 = v5[istride * 11];
    float32x2_t v472 = v5[istride * 6];
    int64_t v489 = 20 + j * 32;
    int64_t v502 = 10 + j * 32;
    float32x2_t v648 = vmul_f32(v787, v646);
    float32x2_t v655 = vmul_f32(v787, v653);
    float32x2_t v662 = vmul_f32(v787, v660);
    float32x2_t v669 = vmul_f32(v787, v667);
    float32x2_t v676 = vmul_f32(v787, v674);
    float32x2_t v683 = vmul_f32(v787, v681);
    float32x2_t v690 = vmul_f32(v787, v688);
    float32x2_t v697 = vmul_f32(v787, v695);
    float32x2_t v704 = vmul_f32(v787, v702);
    float32x2_t v711 = vmul_f32(v787, v709);
    float32x2_t v718 = vmul_f32(v787, v716);
    float32x2_t v725 = vmul_f32(v787, v723);
    float32x2_t v732 = vmul_f32(v787, v730);
    float32x2_t v739 = vmul_f32(v787, v737);
    float32x2_t v746 = vmul_f32(v787, v744);
    float32x2_t v753 = vmul_f32(v787, v751);
    float32x2_t v760 = vmul_f32(v787, v758);
    float32x2_t v767 = vmul_f32(v787, v765);
    float32x2_t v774 = vmul_f32(v787, v772);
    float32x2_t v781 = vmul_f32(v787, v779);
    float32x2_t v788 = vmul_f32(v787, v786);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v180 = v7[v179];
    float32x2_t v181 = vtrn1_f32(v144, v144);
    float32x2_t v182 = vtrn2_f32(v144, v144);
    int64_t v184 = v179 + 1;
    float32x2_t v193 = v7[v192];
    float32x2_t v194 = vtrn1_f32(v162, v162);
    float32x2_t v195 = vtrn2_f32(v162, v162);
    int64_t v197 = v192 + 1;
    float32x2_t v242 = v7[v241];
    float32x2_t v243 = vtrn1_f32(v206, v206);
    float32x2_t v244 = vtrn2_f32(v206, v206);
    int64_t v246 = v241 + 1;
    float32x2_t v255 = v7[v254];
    float32x2_t v256 = vtrn1_f32(v224, v224);
    float32x2_t v257 = vtrn2_f32(v224, v224);
    int64_t v259 = v254 + 1;
    float32x2_t v304 = v7[v303];
    float32x2_t v305 = vtrn1_f32(v268, v268);
    float32x2_t v306 = vtrn2_f32(v268, v268);
    int64_t v308 = v303 + 1;
    float32x2_t v317 = v7[v316];
    float32x2_t v318 = vtrn1_f32(v286, v286);
    float32x2_t v319 = vtrn2_f32(v286, v286);
    int64_t v321 = v316 + 1;
    float32x2_t v366 = v7[v365];
    float32x2_t v367 = vtrn1_f32(v330, v330);
    float32x2_t v368 = vtrn2_f32(v330, v330);
    int64_t v370 = v365 + 1;
    float32x2_t v379 = v7[v378];
    float32x2_t v380 = vtrn1_f32(v348, v348);
    float32x2_t v381 = vtrn2_f32(v348, v348);
    int64_t v383 = v378 + 1;
    float32x2_t v428 = v7[v427];
    float32x2_t v429 = vtrn1_f32(v392, v392);
    float32x2_t v430 = vtrn2_f32(v392, v392);
    int64_t v432 = v427 + 1;
    float32x2_t v441 = v7[v440];
    float32x2_t v442 = vtrn1_f32(v410, v410);
    float32x2_t v443 = vtrn2_f32(v410, v410);
    int64_t v445 = v440 + 1;
    float32x2_t v490 = v7[v489];
    float32x2_t v491 = vtrn1_f32(v454, v454);
    float32x2_t v492 = vtrn2_f32(v454, v454);
    int64_t v494 = v489 + 1;
    float32x2_t v503 = v7[v502];
    float32x2_t v504 = vtrn1_f32(v472, v472);
    float32x2_t v505 = vtrn2_f32(v472, v472);
    int64_t v507 = v502 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v185 = v7[v184];
    float32x2_t v186 = vmul_f32(v181, v180);
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vmul_f32(v194, v193);
    float32x2_t v247 = v7[v246];
    float32x2_t v248 = vmul_f32(v243, v242);
    float32x2_t v260 = v7[v259];
    float32x2_t v261 = vmul_f32(v256, v255);
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vmul_f32(v305, v304);
    float32x2_t v322 = v7[v321];
    float32x2_t v323 = vmul_f32(v318, v317);
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vmul_f32(v367, v366);
    float32x2_t v384 = v7[v383];
    float32x2_t v385 = vmul_f32(v380, v379);
    float32x2_t v433 = v7[v432];
    float32x2_t v434 = vmul_f32(v429, v428);
    float32x2_t v446 = v7[v445];
    float32x2_t v447 = vmul_f32(v442, v441);
    float32x2_t v495 = v7[v494];
    float32x2_t v496 = vmul_f32(v491, v490);
    float32x2_t v508 = v7[v507];
    float32x2_t v509 = vmul_f32(v504, v503);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v188 = vfma_f32(v186, v182, v185);
    float32x2_t v201 = vfma_f32(v199, v195, v198);
    float32x2_t v250 = vfma_f32(v248, v244, v247);
    float32x2_t v263 = vfma_f32(v261, v257, v260);
    float32x2_t v312 = vfma_f32(v310, v306, v309);
    float32x2_t v325 = vfma_f32(v323, v319, v322);
    float32x2_t v374 = vfma_f32(v372, v368, v371);
    float32x2_t v387 = vfma_f32(v385, v381, v384);
    float32x2_t v436 = vfma_f32(v434, v430, v433);
    float32x2_t v449 = vfma_f32(v447, v443, v446);
    float32x2_t v498 = vfma_f32(v496, v492, v495);
    float32x2_t v511 = vfma_f32(v509, v505, v508);
    float32x2_t v512 = vadd_f32(v64, v77);
    float32x2_t v513 = vsub_f32(v64, v77);
    float32x2_t v514 = vadd_f32(v126, v139);
    float32x2_t v515 = vsub_f32(v126, v139);
    float32x2_t v516 = vadd_f32(v188, v201);
    float32x2_t v517 = vsub_f32(v188, v201);
    float32x2_t v518 = vadd_f32(v250, v263);
    float32x2_t v519 = vsub_f32(v250, v263);
    float32x2_t v520 = vadd_f32(v312, v325);
    float32x2_t v521 = vsub_f32(v312, v325);
    float32x2_t v522 = vadd_f32(v374, v387);
    float32x2_t v523 = vsub_f32(v374, v387);
    float32x2_t v524 = vadd_f32(v436, v449);
    float32x2_t v525 = vsub_f32(v436, v449);
    float32x2_t v526 = vadd_f32(v498, v511);
    float32x2_t v527 = vsub_f32(v498, v511);
    float32x2_t v528 = vadd_f32(v512, v520);
    float32x2_t v529 = vadd_f32(v514, v522);
    float32x2_t v530 = vadd_f32(v516, v524);
    float32x2_t v531 = vadd_f32(v518, v526);
    float32x2_t v534 = vsub_f32(v512, v520);
    float32x2_t v535 = vsub_f32(v514, v522);
    float32x2_t v536 = vsub_f32(v516, v524);
    float32x2_t v537 = vsub_f32(v518, v526);
    float32x2_t v548 = vadd_f32(v513, v517);
    float32x2_t v549 = vadd_f32(v515, v519);
    float32x2_t v550 = vsub_f32(v513, v517);
    float32x2_t v551 = vsub_f32(v527, v523);
    float32x2_t v552 = vadd_f32(v521, v525);
    float32x2_t v553 = vadd_f32(v523, v527);
    float32x2_t v554 = vsub_f32(v521, v525);
    float32x2_t v555 = vsub_f32(v515, v519);
    float32x2_t v568 = vadd_f32(v513, v521);
    float32x2_t v569 = vadd_f32(v519, v527);
    float32x2_t v740 = vrev64_f32(v513);
    float32x2_t v747 = vrev64_f32(v521);
    float32x2_t v761 = vrev64_f32(v519);
    float32x2_t v768 = vrev64_f32(v527);
    float32x2_t v532 = vadd_f32(v528, v530);
    float32x2_t v533 = vadd_f32(v529, v531);
    float32x2_t v538 = vsub_f32(v528, v530);
    float32x2_t v539 = vsub_f32(v529, v531);
    float32x2_t v542 = vadd_f32(v535, v537);
    float32x2_t v543 = vadd_f32(v534, v536);
    float32x2_t v545 = vsub_f32(v536, v537);
    float32x2_t v546 = vsub_f32(v534, v535);
    float32x2_t v556 = vadd_f32(v548, v549);
    float32x2_t v557 = vadd_f32(v552, v553);
    float32x2_t v559 = vsub_f32(v548, v549);
    float32x2_t v560 = vsub_f32(v552, v553);
    float32x2_t v562 = vadd_f32(v550, v551);
    float32x2_t v563 = vadd_f32(v554, v555);
    float32x2_t v565 = vsub_f32(v550, v551);
    float32x2_t v566 = vsub_f32(v554, v555);
    float32x2_t v591 = vmul_f32(v534, v590);
    float32x2_t v595 = vmul_f32(v535, v594);
    float32x2_t v599 = vmul_f32(v536, v598);
    float32x2_t v603 = vmul_f32(v537, v602);
    float32x2_t v733 = vrev64_f32(v568);
    float32x2_t v741 = vmul_f32(v740, v739);
    float32x2_t v748 = vmul_f32(v747, v746);
    float32x2_t v754 = vrev64_f32(v569);
    float32x2_t v762 = vmul_f32(v761, v760);
    float32x2_t v769 = vmul_f32(v768, v767);
    float32x2_t v540 = vadd_f32(v532, v533);
    float32x2_t v541 = vsub_f32(v532, v533);
    float32x2_t v544 = vsub_f32(v543, v542);
    float32x2_t v547 = vadd_f32(v538, v539);
    float32x2_t v558 = vadd_f32(v556, v557);
    float32x2_t v561 = vadd_f32(v559, v560);
    float32x2_t v564 = vadd_f32(v562, v563);
    float32x2_t v567 = vadd_f32(v565, v566);
    float32x2_t v570 = vsub_f32(v563, v557);
    float32x2_t v573 = vsub_f32(v556, v562);
    float32x2_t v607 = vmul_f32(v538, v606);
    float32x2_t v611 = vmul_f32(v539, v610);
    float32x2_t v623 = vmul_f32(v542, v622);
    float32x2_t v627 = vmul_f32(v543, v626);
    float32x2_t v635 = vmul_f32(v545, v634);
    float32x2_t v639 = vmul_f32(v546, v638);
    float32x2_t v649 = vrev64_f32(v556);
    float32x2_t v656 = vrev64_f32(v557);
    float32x2_t v670 = vrev64_f32(v559);
    float32x2_t v677 = vrev64_f32(v560);
    float32x2_t v691 = vrev64_f32(v562);
    float32x2_t v698 = vrev64_f32(v563);
    float32x2_t v712 = vrev64_f32(v565);
    float32x2_t v719 = vrev64_f32(v566);
    float32x2_t v734 = vmul_f32(v733, v732);
    float32x2_t v755 = vmul_f32(v754, v753);
    float32x2_t v571 = vadd_f32(v570, v513);
    float32x2_t v574 = vadd_f32(v573, v519);
    float32x2_t v583 = vadd_f32(v582, v540);
    float32x2_t v615 = vmul_f32(v540, v614);
    float32x2_t v619 = vmul_f32(v541, v618);
    float32x2_t v631 = vmul_f32(v544, v630);
    float32x2_t v643 = vmul_f32(v547, v642);
    float32x2_t v650 = vmul_f32(v649, v648);
    float32x2_t v657 = vmul_f32(v656, v655);
    float32x2_t v663 = vrev64_f32(v558);
    float32x2_t v671 = vmul_f32(v670, v669);
    float32x2_t v678 = vmul_f32(v677, v676);
    float32x2_t v684 = vrev64_f32(v561);
    float32x2_t v692 = vmul_f32(v691, v690);
    float32x2_t v699 = vmul_f32(v698, v697);
    float32x2_t v705 = vrev64_f32(v564);
    float32x2_t v713 = vmul_f32(v712, v711);
    float32x2_t v720 = vmul_f32(v719, v718);
    float32x2_t v726 = vrev64_f32(v567);
    float32x2_t v793 = vadd_f32(v603, v635);
    float32x2_t v794 = vsub_f32(v635, v599);
    float32x2_t v795 = vadd_f32(v595, v639);
    float32x2_t v796 = vsub_f32(v591, v639);
    float32x2_t v572 = vsub_f32(v571, v569);
    float32x2_t v575 = vadd_f32(v574, v521);
    float32x2_t v664 = vmul_f32(v663, v662);
    float32x2_t v685 = vmul_f32(v684, v683);
    float32x2_t v706 = vmul_f32(v705, v704);
    float32x2_t v727 = vmul_f32(v726, v725);
    float32x2_t v791 = vadd_f32(v623, v631);
    float32x2_t v792 = vsub_f32(v627, v631);
    float32x2_t v797 = vsub_f32(v643, v611);
    float32x2_t v798 = vadd_f32(v643, v607);
    float32x2_t v799 = vadd_f32(v615, v583);
    v6[0] = v583;
    float32x2_t v576 = vsub_f32(v575, v527);
    float32x2_t v775 = vrev64_f32(v572);
    float32x2_t v800 = vadd_f32(v619, v799);
    float32x2_t v801 = vsub_f32(v799, v619);
    float32x2_t v802 = vsub_f32(v791, v793);
    float32x2_t v804 = vadd_f32(v792, v794);
    float32x2_t v806 = vadd_f32(v791, v795);
    float32x2_t v808 = vadd_f32(v792, v796);
    float32x2_t v818 = vadd_f32(v650, v664);
    float32x2_t v819 = vadd_f32(v657, v664);
    float32x2_t v820 = vadd_f32(v671, v685);
    float32x2_t v821 = vadd_f32(v678, v685);
    float32x2_t v822 = vadd_f32(v692, v706);
    float32x2_t v823 = vadd_f32(v699, v706);
    float32x2_t v824 = vadd_f32(v713, v727);
    float32x2_t v825 = vadd_f32(v720, v727);
    float32x2_t v577 = vadd_f32(v572, v576);
    float32x2_t v776 = vmul_f32(v775, v774);
    float32x2_t v782 = vrev64_f32(v576);
    float32x2_t v803 = vadd_f32(v797, v800);
    float32x2_t v805 = vadd_f32(v798, v801);
    float32x2_t v807 = vsub_f32(v800, v797);
    float32x2_t v809 = vsub_f32(v801, v798);
    float32x2_t v829 = vadd_f32(v818, v820);
    float32x2_t v830 = vsub_f32(v818, v820);
    float32x2_t v831 = vadd_f32(v819, v821);
    float32x2_t v832 = vsub_f32(v819, v821);
    float32x2_t v833 = vadd_f32(v822, v824);
    float32x2_t v834 = vsub_f32(v824, v822);
    float32x2_t v835 = vadd_f32(v823, v825);
    float32x2_t v836 = vsub_f32(v825, v823);
    float32x2_t v783 = vmul_f32(v782, v781);
    float32x2_t v789 = vrev64_f32(v577);
    float32x2_t v810 = vadd_f32(v802, v803);
    float32x2_t v811 = vadd_f32(v804, v805);
    float32x2_t v812 = vadd_f32(v806, v807);
    float32x2_t v813 = vadd_f32(v808, v809);
    float32x2_t v814 = vsub_f32(v803, v802);
    float32x2_t v815 = vsub_f32(v805, v804);
    float32x2_t v816 = vsub_f32(v807, v806);
    float32x2_t v817 = vsub_f32(v809, v808);
    float32x2_t v846 = vadd_f32(v831, v835);
    float32x2_t v848 = vadd_f32(v830, v836);
    float32x2_t v850 = vsub_f32(v829, v833);
    float32x2_t v852 = vsub_f32(v836, v830);
    float32x2_t v854 = vadd_f32(v829, v833);
    float32x2_t v857 = vsub_f32(v834, v832);
    float32x2_t v860 = vsub_f32(v835, v831);
    float32x2_t v863 = vadd_f32(v832, v834);
    float32x2_t v790 = vmul_f32(v789, v788);
    float32x2_t v837 = vsub_f32(v776, v783);
    float32x2_t v826 = vadd_f32(v790, v783);
    float32x2_t v839 = vadd_f32(v837, v837);
    float32x2_t v864 = vsub_f32(v863, v837);
    float32x2_t v827 = vadd_f32(v734, v826);
    float32x2_t v840 = vsub_f32(v755, v839);
    float32x2_t v843 = vadd_f32(v826, v826);
    float32x2_t v861 = vadd_f32(v860, v839);
    float32x2_t v894 = vadd_f32(v817, v864);
    float32x2_t v900 = vsub_f32(v817, v864);
    float32x2_t v828 = vadd_f32(v827, v741);
    float32x2_t v838 = vadd_f32(v827, v748);
    float32x2_t v841 = vadd_f32(v840, v762);
    float32x2_t v842 = vadd_f32(v840, v769);
    float32x2_t v844 = vadd_f32(v843, v843);
    float32x2_t v845 = vadd_f32(v837, v843);
    float32x2_t v851 = vadd_f32(v850, v843);
    float32x2_t v862 = vadd_f32(v861, v843);
    v6[ostride * 3] = v894;
    v6[ostride * 14] = v900;
    float32x2_t v847 = vadd_f32(v846, v838);
    float32x2_t v849 = vadd_f32(v848, v841);
    float32x2_t v853 = vsub_f32(v852, v845);
    float32x2_t v855 = vadd_f32(v854, v828);
    float32x2_t v858 = vsub_f32(v857, v842);
    float32x2_t v882 = vadd_f32(v812, v851);
    float32x2_t v888 = vsub_f32(v812, v851);
    float32x2_t v954 = vadd_f32(v816, v862);
    float32x2_t v960 = vsub_f32(v816, v862);
    float32x2_t v856 = vadd_f32(v855, v837);
    float32x2_t v859 = vadd_f32(v858, v844);
    float32x2_t v870 = vadd_f32(v810, v847);
    float32x2_t v876 = vsub_f32(v810, v847);
    v6[ostride * 2] = v882;
    v6[ostride * 15] = v888;
    float32x2_t v918 = vadd_f32(v813, v853);
    float32x2_t v924 = vsub_f32(v813, v853);
    float32x2_t v930 = vadd_f32(v811, v849);
    float32x2_t v936 = vsub_f32(v811, v849);
    v6[ostride * 8] = v954;
    v6[ostride * 9] = v960;
    v6[ostride] = v870;
    v6[ostride * 16] = v876;
    float32x2_t v906 = vadd_f32(v814, v856);
    float32x2_t v912 = vsub_f32(v814, v856);
    v6[ostride * 5] = v918;
    v6[ostride * 12] = v924;
    v6[ostride * 6] = v930;
    v6[ostride * 11] = v936;
    float32x2_t v942 = vadd_f32(v815, v859);
    float32x2_t v948 = vsub_f32(v815, v859);
    v6[ostride * 4] = v906;
    v6[ostride * 13] = v912;
    v6[ostride * 7] = v942;
    v6[ostride * 10] = v948;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs17(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v434 = -4.2602849117736000e-02F;
    float v439 = 2.0497965023262180e-01F;
    float v444 = 1.0451835201736759e+00F;
    float v449 = 1.7645848660222969e+00F;
    float v454 = -7.2340797728605655e-01F;
    float v459 = -8.9055591620606403e-02F;
    float v464 = -1.0625000000000000e+00F;
    float v469 = 2.5769410160110379e-01F;
    float v474 = 7.7980260789483757e-01F;
    float v479 = 5.4389318464570580e-01F;
    float v484 = 4.2010193497052700e-01F;
    float v489 = 1.2810929434228073e+00F;
    float v494 = 4.4088907348175338e-01F;
    float v499 = 3.1717619283272508e-01F;
    float v504 = 9.0138318648016680e-01F;
    float v511 = 4.3248756360072310e-01F;
    float v518 = -6.6693537504044498e-01F;
    float v525 = 6.0389004312516970e-01F;
    float v532 = 3.6924873198582547e-01F;
    float v539 = -4.8656938755549761e-01F;
    float v546 = -2.3813712136760609e-01F;
    float v553 = 1.5573820617422458e+00F;
    float v560 = -6.5962247018731990e-01F;
    float v567 = 1.4316961569866241e-01F;
    float v574 = -2.3903469959860771e-01F;
    float v581 = 4.7932541949972603e-02F;
    float v588 = 2.3188014856550065e+00F;
    float v595 = -7.8914568419206255e-01F;
    float v602 = -3.8484572871179505e+00F;
    float v609 = 1.3003804568801376e+00F;
    float v616 = -4.0814769046889037e+00F;
    float v623 = 1.4807159909286283e+00F;
    float v630 = 1.3332470363551400e-02F;
    float v637 = 3.7139778690557629e-01F;
    float v644 = -1.9236512863456379e-01F;
    const float32x2_t *v865 = &v5[v0];
    float32x2_t *v1067 = &v6[v2];
    int64_t v33 = v0 * 16;
    int64_t v55 = v10 * 15;
    int64_t v61 = v0 * 3;
    int64_t v75 = v0 * 14;
    int64_t v90 = v10 * 2;
    int64_t v97 = v10 * 13;
    int64_t v103 = v0 * 9;
    int64_t v117 = v0 * 8;
    int64_t v132 = v10 * 8;
    int64_t v139 = v10 * 7;
    int64_t v145 = v0 * 10;
    int64_t v159 = v0 * 7;
    int64_t v174 = v10 * 9;
    int64_t v181 = v10 * 6;
    int64_t v187 = v0 * 13;
    int64_t v201 = v0 * 4;
    int64_t v216 = v10 * 12;
    int64_t v223 = v10 * 3;
    int64_t v229 = v0 * 5;
    int64_t v243 = v0 * 12;
    int64_t v258 = v10 * 4;
    int64_t v265 = v10 * 11;
    int64_t v271 = v0 * 15;
    int64_t v285 = v0 * 2;
    int64_t v300 = v10 * 14;
    int64_t v313 = v0 * 11;
    int64_t v327 = v0 * 6;
    int64_t v342 = v10 * 10;
    int64_t v349 = v10 * 5;
    int64_t v350 = v13 * 16;
    float v507 = v4 * v504;
    float v514 = v4 * v511;
    float v521 = v4 * v518;
    float v528 = v4 * v525;
    float v535 = v4 * v532;
    float v542 = v4 * v539;
    float v549 = v4 * v546;
    float v556 = v4 * v553;
    float v563 = v4 * v560;
    float v570 = v4 * v567;
    float v577 = v4 * v574;
    float v584 = v4 * v581;
    float v591 = v4 * v588;
    float v598 = v4 * v595;
    float v605 = v4 * v602;
    float v612 = v4 * v609;
    float v619 = v4 * v616;
    float v626 = v4 * v623;
    float v633 = v4 * v630;
    float v640 = v4 * v637;
    float v647 = v4 * v644;
    int64_t v741 = v2 * 16;
    int64_t v749 = v2 * 2;
    int64_t v757 = v2 * 15;
    int64_t v765 = v2 * 3;
    int64_t v773 = v2 * 14;
    int64_t v781 = v2 * 4;
    int64_t v789 = v2 * 13;
    int64_t v797 = v2 * 5;
    int64_t v805 = v2 * 12;
    int64_t v813 = v2 * 6;
    int64_t v821 = v2 * 11;
    int64_t v829 = v2 * 7;
    int64_t v837 = v2 * 10;
    int64_t v845 = v2 * 8;
    int64_t v853 = v2 * 9;
    const float32x2_t *v1012 = &v5[0];
    svint64_t v1013 = svindex_s64(0, v1);
    svfloat32_t v1016 = svdup_n_f32(v434);
    svfloat32_t v1017 = svdup_n_f32(v439);
    svfloat32_t v1018 = svdup_n_f32(v444);
    svfloat32_t v1019 = svdup_n_f32(v449);
    svfloat32_t v1020 = svdup_n_f32(v454);
    svfloat32_t v1021 = svdup_n_f32(v459);
    svfloat32_t v1022 = svdup_n_f32(v464);
    svfloat32_t v1023 = svdup_n_f32(v469);
    svfloat32_t v1024 = svdup_n_f32(v474);
    svfloat32_t v1025 = svdup_n_f32(v479);
    svfloat32_t v1026 = svdup_n_f32(v484);
    svfloat32_t v1027 = svdup_n_f32(v489);
    svfloat32_t v1028 = svdup_n_f32(v494);
    svfloat32_t v1029 = svdup_n_f32(v499);
    float32x2_t *v1058 = &v6[0];
    svint64_t v1203 = svindex_s64(0, v3);
    svfloat32_t v51 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v350]));
    int64_t v57 = v55 + v350;
    int64_t v92 = v90 + v350;
    int64_t v99 = v97 + v350;
    int64_t v134 = v132 + v350;
    int64_t v141 = v139 + v350;
    int64_t v176 = v174 + v350;
    int64_t v183 = v181 + v350;
    int64_t v218 = v216 + v350;
    int64_t v225 = v223 + v350;
    int64_t v260 = v258 + v350;
    int64_t v267 = v265 + v350;
    int64_t v302 = v300 + v350;
    int64_t v309 = v10 + v350;
    int64_t v344 = v342 + v350;
    int64_t v351 = v349 + v350;
    svfloat32_t v867 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v865), v1013));
    const float32x2_t *v875 = &v5[v33];
    const float32x2_t *v885 = &v5[v61];
    const float32x2_t *v894 = &v5[v75];
    const float32x2_t *v903 = &v5[v103];
    const float32x2_t *v912 = &v5[v117];
    const float32x2_t *v921 = &v5[v145];
    const float32x2_t *v930 = &v5[v159];
    const float32x2_t *v939 = &v5[v187];
    const float32x2_t *v948 = &v5[v201];
    const float32x2_t *v957 = &v5[v229];
    const float32x2_t *v966 = &v5[v243];
    const float32x2_t *v975 = &v5[v271];
    const float32x2_t *v984 = &v5[v285];
    const float32x2_t *v993 = &v5[v313];
    const float32x2_t *v1002 = &v5[v327];
    svfloat32_t v1014 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1012), v1013));
    svfloat32_t v1030 = svdup_n_f32(v507);
    svfloat32_t v1031 = svdup_n_f32(v514);
    svfloat32_t v1032 = svdup_n_f32(v521);
    svfloat32_t v1033 = svdup_n_f32(v528);
    svfloat32_t v1034 = svdup_n_f32(v535);
    svfloat32_t v1035 = svdup_n_f32(v542);
    svfloat32_t v1036 = svdup_n_f32(v549);
    svfloat32_t v1037 = svdup_n_f32(v556);
    svfloat32_t v1038 = svdup_n_f32(v563);
    svfloat32_t v1039 = svdup_n_f32(v570);
    svfloat32_t v1040 = svdup_n_f32(v577);
    svfloat32_t v1041 = svdup_n_f32(v584);
    svfloat32_t v1042 = svdup_n_f32(v591);
    svfloat32_t v1043 = svdup_n_f32(v598);
    svfloat32_t v1044 = svdup_n_f32(v605);
    svfloat32_t v1045 = svdup_n_f32(v612);
    svfloat32_t v1046 = svdup_n_f32(v619);
    svfloat32_t v1047 = svdup_n_f32(v626);
    svfloat32_t v1048 = svdup_n_f32(v633);
    svfloat32_t v1049 = svdup_n_f32(v640);
    svfloat32_t v1050 = svdup_n_f32(v647);
    float32x2_t *v1076 = &v6[v741];
    float32x2_t *v1085 = &v6[v749];
    float32x2_t *v1094 = &v6[v757];
    float32x2_t *v1103 = &v6[v765];
    float32x2_t *v1112 = &v6[v773];
    float32x2_t *v1121 = &v6[v781];
    float32x2_t *v1130 = &v6[v789];
    float32x2_t *v1139 = &v6[v797];
    float32x2_t *v1148 = &v6[v805];
    float32x2_t *v1157 = &v6[v813];
    float32x2_t *v1166 = &v6[v821];
    float32x2_t *v1175 = &v6[v829];
    float32x2_t *v1184 = &v6[v837];
    float32x2_t *v1193 = &v6[v845];
    float32x2_t *v1202 = &v6[v853];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v867, v51, 0),
                     v867, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v135 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v134]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v177 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v176]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v219 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v218]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v303 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v302]));
    svfloat32_t v310 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v309]));
    svfloat32_t v345 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v344]));
    svfloat32_t v352 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v351]));
    svfloat32_t v877 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v875), v1013));
    svfloat32_t v887 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v885), v1013));
    svfloat32_t v896 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v894), v1013));
    svfloat32_t v905 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v903), v1013));
    svfloat32_t v914 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v912), v1013));
    svfloat32_t v923 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v921), v1013));
    svfloat32_t v932 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v930), v1013));
    svfloat32_t v941 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v939), v1013));
    svfloat32_t v950 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v948), v1013));
    svfloat32_t v959 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v957), v1013));
    svfloat32_t v968 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v966), v1013));
    svfloat32_t v977 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v975), v1013));
    svfloat32_t v986 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v984), v1013));
    svfloat32_t v995 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v993), v1013));
    svfloat32_t v1004 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1002), v1013));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v877, v58, 0),
                     v877, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v887, v93, 0),
                     v887, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v896, v100, 0),
                     v896, v100, 90);
    svfloat32_t zero136 = svdup_n_f32(0);
    svfloat32_t v136 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero136, v905, v135, 0),
                     v905, v135, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero143, v914, v142, 0),
                     v914, v142, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero178, v923, v177, 0),
                     v923, v177, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero185, v932, v184, 0),
                     v932, v184, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero220, v941, v219, 0),
                     v941, v219, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero227, v950, v226, 0),
                     v950, v226, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero262, v959, v261, 0),
                     v959, v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero269, v968, v268, 0),
                     v968, v268, 90);
    svfloat32_t zero304 = svdup_n_f32(0);
    svfloat32_t v304 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero304, v977, v303, 0),
                     v977, v303, 90);
    svfloat32_t zero311 = svdup_n_f32(0);
    svfloat32_t v311 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero311, v986, v310, 0),
                     v986, v310, 90);
    svfloat32_t zero346 = svdup_n_f32(0);
    svfloat32_t v346 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero346, v995, v345, 0),
                     v995, v345, 90);
    svfloat32_t zero353 = svdup_n_f32(0);
    svfloat32_t v353 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero353, v1004, v352, 0), v1004,
        v352, 90);
    svfloat32_t v354 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v355 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v356 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v357 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v358 = svadd_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v359 = svsub_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v360 = svadd_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v361 = svsub_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v362 = svadd_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v363 = svsub_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v364 = svadd_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v365 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v366 = svadd_f32_x(svptrue_b32(), v304, v311);
    svfloat32_t v367 = svsub_f32_x(svptrue_b32(), v304, v311);
    svfloat32_t v368 = svadd_f32_x(svptrue_b32(), v346, v353);
    svfloat32_t v369 = svsub_f32_x(svptrue_b32(), v346, v353);
    svfloat32_t v370 = svadd_f32_x(svptrue_b32(), v354, v362);
    svfloat32_t v371 = svadd_f32_x(svptrue_b32(), v356, v364);
    svfloat32_t v372 = svadd_f32_x(svptrue_b32(), v358, v366);
    svfloat32_t v373 = svadd_f32_x(svptrue_b32(), v360, v368);
    svfloat32_t v376 = svsub_f32_x(svptrue_b32(), v354, v362);
    svfloat32_t v377 = svsub_f32_x(svptrue_b32(), v356, v364);
    svfloat32_t v378 = svsub_f32_x(svptrue_b32(), v358, v366);
    svfloat32_t v379 = svsub_f32_x(svptrue_b32(), v360, v368);
    svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v355, v359);
    svfloat32_t v391 = svadd_f32_x(svptrue_b32(), v357, v361);
    svfloat32_t v392 = svsub_f32_x(svptrue_b32(), v355, v359);
    svfloat32_t v393 = svsub_f32_x(svptrue_b32(), v369, v365);
    svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v363, v367);
    svfloat32_t v395 = svadd_f32_x(svptrue_b32(), v365, v369);
    svfloat32_t v396 = svsub_f32_x(svptrue_b32(), v363, v367);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v357, v361);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v355, v363);
    svfloat32_t v411 = svadd_f32_x(svptrue_b32(), v361, v369);
    svfloat32_t v374 = svadd_f32_x(svptrue_b32(), v370, v372);
    svfloat32_t v375 = svadd_f32_x(svptrue_b32(), v371, v373);
    svfloat32_t v380 = svsub_f32_x(svptrue_b32(), v370, v372);
    svfloat32_t v381 = svsub_f32_x(svptrue_b32(), v371, v373);
    svfloat32_t v384 = svadd_f32_x(svptrue_b32(), v377, v379);
    svfloat32_t v385 = svadd_f32_x(svptrue_b32(), v376, v378);
    svfloat32_t v387 = svsub_f32_x(svptrue_b32(), v378, v379);
    svfloat32_t v388 = svsub_f32_x(svptrue_b32(), v376, v377);
    svfloat32_t v398 = svadd_f32_x(svptrue_b32(), v390, v391);
    svfloat32_t v399 = svadd_f32_x(svptrue_b32(), v394, v395);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v390, v391);
    svfloat32_t v402 = svsub_f32_x(svptrue_b32(), v394, v395);
    svfloat32_t v404 = svadd_f32_x(svptrue_b32(), v392, v393);
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v396, v397);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v392, v393);
    svfloat32_t v408 = svsub_f32_x(svptrue_b32(), v396, v397);
    svfloat32_t v447 = svmul_f32_x(svptrue_b32(), v378, v1018);
    svfloat32_t zero614 = svdup_n_f32(0);
    svfloat32_t v614 = svcmla_f32_x(pred_full, zero614, v1045, v411, 90);
    svfloat32_t v382 = svadd_f32_x(svptrue_b32(), v374, v375);
    svfloat32_t v383 = svsub_f32_x(svptrue_b32(), v374, v375);
    svfloat32_t v386 = svsub_f32_x(svptrue_b32(), v385, v384);
    svfloat32_t v389 = svadd_f32_x(svptrue_b32(), v380, v381);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v398, v399);
    svfloat32_t v403 = svadd_f32_x(svptrue_b32(), v401, v402);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v404, v405);
    svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v407, v408);
    svfloat32_t v412 = svsub_f32_x(svptrue_b32(), v405, v399);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v398, v404);
    svfloat32_t v457 = svmul_f32_x(svptrue_b32(), v380, v1020);
    svfloat32_t v462 = svmul_f32_x(svptrue_b32(), v381, v1021);
    svfloat32_t v492 = svmul_f32_x(svptrue_b32(), v387, v1027);
    svfloat32_t v497 = svmul_f32_x(svptrue_b32(), v388, v1028);
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v412, v355);
    svfloat32_t v416 = svadd_f32_x(svptrue_b32(), v415, v361);
    svfloat32_t v427 = svadd_f32_x(svptrue_b32(), v1014, v382);
    svfloat32_t v487 = svmul_f32_x(svptrue_b32(), v386, v1026);
    svfloat32_t zero523 = svdup_n_f32(0);
    svfloat32_t v523 = svcmla_f32_x(pred_full, zero523, v1032, v400, 90);
    svfloat32_t zero544 = svdup_n_f32(0);
    svfloat32_t v544 = svcmla_f32_x(pred_full, zero544, v1035, v403, 90);
    svfloat32_t zero565 = svdup_n_f32(0);
    svfloat32_t v565 = svcmla_f32_x(pred_full, zero565, v1038, v406, 90);
    svfloat32_t zero586 = svdup_n_f32(0);
    svfloat32_t v586 = svcmla_f32_x(pred_full, zero586, v1041, v409, 90);
    svfloat32_t v652 = svmla_f32_x(pred_full, v492, v379, v1019);
    svfloat32_t v653 = svnmls_f32_x(pred_full, v447, v387, v1027);
    svfloat32_t v654 = svmla_f32_x(pred_full, v497, v377, v1017);
    svfloat32_t v655 = svnmls_f32_x(pred_full, v497, v376, v1016);
    svfloat32_t v414 = svsub_f32_x(svptrue_b32(), v413, v411);
    svfloat32_t v417 = svadd_f32_x(svptrue_b32(), v416, v363);
    svfloat32_t v650 = svmla_f32_x(pred_full, v487, v384, v1024);
    svfloat32_t v651 = svnmls_f32_x(pred_full, v487, v385, v1025);
    svfloat32_t v656 = svnmls_f32_x(pred_full, v462, v389, v1029);
    svfloat32_t v657 = svmla_f32_x(pred_full, v457, v389, v1029);
    svfloat32_t v658 = svmla_f32_x(pred_full, v427, v382, v1022);
    svfloat32_t v677 = svcmla_f32_x(pred_full, v523, v1030, v398, 90);
    svfloat32_t v678 = svcmla_f32_x(pred_full, v523, v1031, v399, 90);
    svfloat32_t v679 = svcmla_f32_x(pred_full, v544, v1033, v401, 90);
    svfloat32_t v680 = svcmla_f32_x(pred_full, v544, v1034, v402, 90);
    svfloat32_t v681 = svcmla_f32_x(pred_full, v565, v1036, v404, 90);
    svfloat32_t v682 = svcmla_f32_x(pred_full, v565, v1037, v405, 90);
    svfloat32_t v683 = svcmla_f32_x(pred_full, v586, v1039, v407, 90);
    svfloat32_t v684 = svcmla_f32_x(pred_full, v586, v1040, v408, 90);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1058), v1203,
                               svreinterpret_f64_f32(v427));
    svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v417, v369);
    svfloat32_t zero635 = svdup_n_f32(0);
    svfloat32_t v635 = svcmla_f32_x(pred_full, zero635, v1048, v414, 90);
    svfloat32_t v659 = svmla_f32_x(pred_full, v658, v383, v1023);
    svfloat32_t v660 = svmls_f32_x(pred_full, v658, v383, v1023);
    svfloat32_t v661 = svsub_f32_x(svptrue_b32(), v650, v652);
    svfloat32_t v663 = svadd_f32_x(svptrue_b32(), v651, v653);
    svfloat32_t v665 = svadd_f32_x(svptrue_b32(), v650, v654);
    svfloat32_t v667 = svadd_f32_x(svptrue_b32(), v651, v655);
    svfloat32_t v688 = svadd_f32_x(svptrue_b32(), v677, v679);
    svfloat32_t v689 = svsub_f32_x(svptrue_b32(), v677, v679);
    svfloat32_t v690 = svadd_f32_x(svptrue_b32(), v678, v680);
    svfloat32_t v691 = svsub_f32_x(svptrue_b32(), v678, v680);
    svfloat32_t v692 = svadd_f32_x(svptrue_b32(), v681, v683);
    svfloat32_t v693 = svsub_f32_x(svptrue_b32(), v683, v681);
    svfloat32_t v694 = svadd_f32_x(svptrue_b32(), v682, v684);
    svfloat32_t v695 = svsub_f32_x(svptrue_b32(), v684, v682);
    svfloat32_t v419 = svadd_f32_x(svptrue_b32(), v414, v418);
    svfloat32_t zero642 = svdup_n_f32(0);
    svfloat32_t v642 = svcmla_f32_x(pred_full, zero642, v1049, v418, 90);
    svfloat32_t v662 = svadd_f32_x(svptrue_b32(), v656, v659);
    svfloat32_t v664 = svadd_f32_x(svptrue_b32(), v657, v660);
    svfloat32_t v666 = svsub_f32_x(svptrue_b32(), v659, v656);
    svfloat32_t v668 = svsub_f32_x(svptrue_b32(), v660, v657);
    svfloat32_t v705 = svadd_f32_x(svptrue_b32(), v690, v694);
    svfloat32_t v707 = svadd_f32_x(svptrue_b32(), v689, v695);
    svfloat32_t v709 = svsub_f32_x(svptrue_b32(), v688, v692);
    svfloat32_t v711 = svsub_f32_x(svptrue_b32(), v695, v689);
    svfloat32_t v713 = svadd_f32_x(svptrue_b32(), v688, v692);
    svfloat32_t v716 = svsub_f32_x(svptrue_b32(), v693, v691);
    svfloat32_t v719 = svsub_f32_x(svptrue_b32(), v694, v690);
    svfloat32_t v722 = svadd_f32_x(svptrue_b32(), v691, v693);
    svfloat32_t v669 = svadd_f32_x(svptrue_b32(), v661, v662);
    svfloat32_t v670 = svadd_f32_x(svptrue_b32(), v663, v664);
    svfloat32_t v671 = svadd_f32_x(svptrue_b32(), v665, v666);
    svfloat32_t v672 = svadd_f32_x(svptrue_b32(), v667, v668);
    svfloat32_t v673 = svsub_f32_x(svptrue_b32(), v662, v661);
    svfloat32_t v674 = svsub_f32_x(svptrue_b32(), v664, v663);
    svfloat32_t v675 = svsub_f32_x(svptrue_b32(), v666, v665);
    svfloat32_t v676 = svsub_f32_x(svptrue_b32(), v668, v667);
    svfloat32_t v696 = svsub_f32_x(svptrue_b32(), v635, v642);
    svfloat32_t v685 = svcmla_f32_x(pred_full, v642, v1050, v419, 90);
    svfloat32_t v698 = svadd_f32_x(svptrue_b32(), v696, v696);
    svfloat32_t v723 = svsub_f32_x(svptrue_b32(), v722, v696);
    svfloat32_t v686 = svcmla_f32_x(pred_full, v685, v1042, v410, 90);
    svfloat32_t v699 = svsub_f32_x(svptrue_b32(), v614, v698);
    svfloat32_t v702 = svadd_f32_x(svptrue_b32(), v685, v685);
    svfloat32_t v720 = svadd_f32_x(svptrue_b32(), v719, v698);
    svfloat32_t v763 = svadd_f32_x(svptrue_b32(), v676, v723);
    svfloat32_t v771 = svsub_f32_x(svptrue_b32(), v676, v723);
    svfloat32_t v687 = svcmla_f32_x(pred_full, v686, v1043, v355, 90);
    svfloat32_t v697 = svcmla_f32_x(pred_full, v686, v1044, v363, 90);
    svfloat32_t v700 = svcmla_f32_x(pred_full, v699, v1046, v361, 90);
    svfloat32_t v701 = svcmla_f32_x(pred_full, v699, v1047, v369, 90);
    svfloat32_t v703 = svadd_f32_x(svptrue_b32(), v702, v702);
    svfloat32_t v704 = svadd_f32_x(svptrue_b32(), v696, v702);
    svfloat32_t v710 = svadd_f32_x(svptrue_b32(), v709, v702);
    svfloat32_t v721 = svadd_f32_x(svptrue_b32(), v720, v702);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1103), v1203,
                               svreinterpret_f64_f32(v763));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1112), v1203,
                               svreinterpret_f64_f32(v771));
    svfloat32_t v706 = svadd_f32_x(svptrue_b32(), v705, v697);
    svfloat32_t v708 = svadd_f32_x(svptrue_b32(), v707, v700);
    svfloat32_t v712 = svsub_f32_x(svptrue_b32(), v711, v704);
    svfloat32_t v714 = svadd_f32_x(svptrue_b32(), v713, v687);
    svfloat32_t v717 = svsub_f32_x(svptrue_b32(), v716, v701);
    svfloat32_t v747 = svadd_f32_x(svptrue_b32(), v671, v710);
    svfloat32_t v755 = svsub_f32_x(svptrue_b32(), v671, v710);
    svfloat32_t v843 = svadd_f32_x(svptrue_b32(), v675, v721);
    svfloat32_t v851 = svsub_f32_x(svptrue_b32(), v675, v721);
    svfloat32_t v715 = svadd_f32_x(svptrue_b32(), v714, v696);
    svfloat32_t v718 = svadd_f32_x(svptrue_b32(), v717, v703);
    svfloat32_t v731 = svadd_f32_x(svptrue_b32(), v669, v706);
    svfloat32_t v739 = svsub_f32_x(svptrue_b32(), v669, v706);
    svfloat32_t v795 = svadd_f32_x(svptrue_b32(), v672, v712);
    svfloat32_t v803 = svsub_f32_x(svptrue_b32(), v672, v712);
    svfloat32_t v811 = svadd_f32_x(svptrue_b32(), v670, v708);
    svfloat32_t v819 = svsub_f32_x(svptrue_b32(), v670, v708);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1085), v1203,
                               svreinterpret_f64_f32(v747));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1094), v1203,
                               svreinterpret_f64_f32(v755));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1193), v1203,
                               svreinterpret_f64_f32(v843));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1202), v1203,
                               svreinterpret_f64_f32(v851));
    svfloat32_t v779 = svadd_f32_x(svptrue_b32(), v673, v715);
    svfloat32_t v787 = svsub_f32_x(svptrue_b32(), v673, v715);
    svfloat32_t v827 = svadd_f32_x(svptrue_b32(), v674, v718);
    svfloat32_t v835 = svsub_f32_x(svptrue_b32(), v674, v718);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1067), v1203,
                               svreinterpret_f64_f32(v731));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1076), v1203,
                               svreinterpret_f64_f32(v739));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1139), v1203,
                               svreinterpret_f64_f32(v795));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1148), v1203,
                               svreinterpret_f64_f32(v803));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1157), v1203,
                               svreinterpret_f64_f32(v811));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1166), v1203,
                               svreinterpret_f64_f32(v819));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1121), v1203,
                               svreinterpret_f64_f32(v779));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1130), v1203,
                               svreinterpret_f64_f32(v787));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1175), v1203,
                               svreinterpret_f64_f32(v827));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1184), v1203,
                               svreinterpret_f64_f32(v835));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs18(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v317 = v5[istride];
    float v695 = -5.0000000000000000e-01F;
    float v706 = -1.4999999999999998e+00F;
    float v709 = 8.6602540378443871e-01F;
    float v710 = -8.6602540378443871e-01F;
    float v717 = 7.6604444311897801e-01F;
    float v721 = 9.3969262078590832e-01F;
    float v725 = -1.7364817766693039e-01F;
    float v728 = 6.4278760968653925e-01F;
    float v729 = -6.4278760968653925e-01F;
    float v735 = -3.4202014332566888e-01F;
    float v736 = 3.4202014332566888e-01F;
    float v742 = 9.8480775301220802e-01F;
    float v743 = -9.8480775301220802e-01F;
    float32x2_t v745 = (float32x2_t){v4, v4};
    float32x2_t v349 = vtrn1_f32(v317, v317);
    float32x2_t v350 = vtrn2_f32(v317, v317);
    float32x2_t v547 = v5[0];
    float32x2_t v696 = (float32x2_t){v695, v695};
    float32x2_t v707 = (float32x2_t){v706, v706};
    float32x2_t v711 = (float32x2_t){v709, v710};
    float32x2_t v718 = (float32x2_t){v717, v717};
    float32x2_t v722 = (float32x2_t){v721, v721};
    float32x2_t v726 = (float32x2_t){v725, v725};
    float32x2_t v730 = (float32x2_t){v728, v729};
    float32x2_t v737 = (float32x2_t){v735, v736};
    float32x2_t v744 = (float32x2_t){v742, v743};
    float32x2_t v20 = v5[istride * 9];
    int64_t v37 = 16 + j * 34;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 11];
    int64_t v86 = 2 + j * 34;
    int64_t v99 = 20 + j * 34;
    float32x2_t v113 = v5[istride * 4];
    float32x2_t v131 = v5[istride * 13];
    int64_t v148 = 6 + j * 34;
    int64_t v161 = 24 + j * 34;
    float32x2_t v175 = v5[istride * 6];
    float32x2_t v193 = v5[istride * 15];
    int64_t v210 = 10 + j * 34;
    int64_t v223 = 28 + j * 34;
    float32x2_t v237 = v5[istride * 8];
    float32x2_t v255 = v5[istride * 17];
    int64_t v272 = 14 + j * 34;
    int64_t v285 = 32 + j * 34;
    float32x2_t v299 = v5[istride * 10];
    int64_t v334 = 18 + j * 34;
    float32x2_t v348 = v7[j * 34];
    int64_t v352 = j * 34 + 1;
    float32x2_t v361 = v5[istride * 12];
    float32x2_t v379 = v5[istride * 3];
    int64_t v396 = 22 + j * 34;
    int64_t v409 = 4 + j * 34;
    float32x2_t v423 = v5[istride * 14];
    float32x2_t v441 = v5[istride * 5];
    int64_t v458 = 26 + j * 34;
    int64_t v471 = 8 + j * 34;
    float32x2_t v485 = v5[istride * 16];
    float32x2_t v503 = v5[istride * 7];
    int64_t v520 = 30 + j * 34;
    int64_t v533 = 12 + j * 34;
    float32x2_t v713 = vmul_f32(v745, v711);
    float32x2_t v732 = vmul_f32(v745, v730);
    float32x2_t v739 = vmul_f32(v745, v737);
    float32x2_t v746 = vmul_f32(v745, v744);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v273 = v7[v272];
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    int64_t v277 = v272 + 1;
    float32x2_t v286 = v7[v285];
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    int64_t v290 = v285 + 1;
    float32x2_t v335 = v7[v334];
    float32x2_t v336 = vtrn1_f32(v299, v299);
    float32x2_t v337 = vtrn2_f32(v299, v299);
    int64_t v339 = v334 + 1;
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vmul_f32(v349, v348);
    float32x2_t v397 = v7[v396];
    float32x2_t v398 = vtrn1_f32(v361, v361);
    float32x2_t v399 = vtrn2_f32(v361, v361);
    int64_t v401 = v396 + 1;
    float32x2_t v410 = v7[v409];
    float32x2_t v411 = vtrn1_f32(v379, v379);
    float32x2_t v412 = vtrn2_f32(v379, v379);
    int64_t v414 = v409 + 1;
    float32x2_t v459 = v7[v458];
    float32x2_t v460 = vtrn1_f32(v423, v423);
    float32x2_t v461 = vtrn2_f32(v423, v423);
    int64_t v463 = v458 + 1;
    float32x2_t v472 = v7[v471];
    float32x2_t v473 = vtrn1_f32(v441, v441);
    float32x2_t v474 = vtrn2_f32(v441, v441);
    int64_t v476 = v471 + 1;
    float32x2_t v521 = v7[v520];
    float32x2_t v522 = vtrn1_f32(v485, v485);
    float32x2_t v523 = vtrn2_f32(v485, v485);
    int64_t v525 = v520 + 1;
    float32x2_t v534 = v7[v533];
    float32x2_t v535 = vtrn1_f32(v503, v503);
    float32x2_t v536 = vtrn2_f32(v503, v503);
    int64_t v538 = v533 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vmul_f32(v336, v335);
    float32x2_t v402 = v7[v401];
    float32x2_t v403 = vmul_f32(v398, v397);
    float32x2_t v415 = v7[v414];
    float32x2_t v416 = vmul_f32(v411, v410);
    float32x2_t v464 = v7[v463];
    float32x2_t v465 = vmul_f32(v460, v459);
    float32x2_t v477 = v7[v476];
    float32x2_t v478 = vmul_f32(v473, v472);
    float32x2_t v526 = v7[v525];
    float32x2_t v527 = vmul_f32(v522, v521);
    float32x2_t v539 = v7[v538];
    float32x2_t v540 = vmul_f32(v535, v534);
    float32x2_t v356 = vfma_f32(v354, v350, v353);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v343 = vfma_f32(v341, v337, v340);
    float32x2_t v405 = vfma_f32(v403, v399, v402);
    float32x2_t v418 = vfma_f32(v416, v412, v415);
    float32x2_t v467 = vfma_f32(v465, v461, v464);
    float32x2_t v480 = vfma_f32(v478, v474, v477);
    float32x2_t v529 = vfma_f32(v527, v523, v526);
    float32x2_t v542 = vfma_f32(v540, v536, v539);
    float32x2_t v548 = vadd_f32(v547, v46);
    float32x2_t v549 = vsub_f32(v547, v46);
    float32x2_t v550 = vadd_f32(v95, v108);
    float32x2_t v551 = vsub_f32(v95, v108);
    float32x2_t v552 = vadd_f32(v157, v170);
    float32x2_t v553 = vsub_f32(v157, v170);
    float32x2_t v554 = vadd_f32(v219, v232);
    float32x2_t v555 = vsub_f32(v219, v232);
    float32x2_t v556 = vadd_f32(v281, v294);
    float32x2_t v557 = vsub_f32(v281, v294);
    float32x2_t v558 = vadd_f32(v343, v356);
    float32x2_t v559 = vsub_f32(v343, v356);
    float32x2_t v560 = vadd_f32(v405, v418);
    float32x2_t v561 = vsub_f32(v405, v418);
    float32x2_t v562 = vadd_f32(v467, v480);
    float32x2_t v563 = vsub_f32(v467, v480);
    float32x2_t v564 = vadd_f32(v529, v542);
    float32x2_t v565 = vsub_f32(v529, v542);
    float32x2_t v566 = vadd_f32(v550, v564);
    float32x2_t v567 = vsub_f32(v550, v564);
    float32x2_t v568 = vadd_f32(v562, v552);
    float32x2_t v569 = vsub_f32(v562, v552);
    float32x2_t v570 = vadd_f32(v554, v560);
    float32x2_t v571 = vsub_f32(v554, v560);
    float32x2_t v572 = vadd_f32(v556, v558);
    float32x2_t v573 = vsub_f32(v556, v558);
    float32x2_t v670 = vadd_f32(v551, v565);
    float32x2_t v671 = vsub_f32(v551, v565);
    float32x2_t v672 = vadd_f32(v563, v553);
    float32x2_t v673 = vsub_f32(v563, v553);
    float32x2_t v674 = vadd_f32(v555, v561);
    float32x2_t v675 = vsub_f32(v555, v561);
    float32x2_t v676 = vadd_f32(v557, v559);
    float32x2_t v677 = vsub_f32(v557, v559);
    float32x2_t v574 = vadd_f32(v566, v568);
    float32x2_t v578 = vadd_f32(v567, v569);
    float32x2_t v580 = vsub_f32(v566, v568);
    float32x2_t v581 = vsub_f32(v568, v572);
    float32x2_t v582 = vsub_f32(v572, v566);
    float32x2_t v583 = vsub_f32(v567, v569);
    float32x2_t v584 = vsub_f32(v569, v573);
    float32x2_t v585 = vsub_f32(v573, v567);
    float32x2_t v604 = vmul_f32(v570, v707);
    float32x2_t v610 = vrev64_f32(v571);
    float32x2_t v678 = vadd_f32(v670, v672);
    float32x2_t v682 = vadd_f32(v671, v673);
    float32x2_t v684 = vsub_f32(v670, v672);
    float32x2_t v685 = vsub_f32(v672, v676);
    float32x2_t v686 = vsub_f32(v676, v670);
    float32x2_t v687 = vsub_f32(v671, v673);
    float32x2_t v688 = vsub_f32(v673, v677);
    float32x2_t v689 = vsub_f32(v677, v671);
    float32x2_t v708 = vmul_f32(v674, v707);
    float32x2_t v714 = vrev64_f32(v675);
    float32x2_t v575 = vadd_f32(v574, v572);
    float32x2_t v579 = vadd_f32(v578, v573);
    float32x2_t v611 = vmul_f32(v610, v713);
    float32x2_t v615 = vmul_f32(v580, v718);
    float32x2_t v619 = vmul_f32(v581, v722);
    float32x2_t v623 = vmul_f32(v582, v726);
    float32x2_t v629 = vrev64_f32(v583);
    float32x2_t v636 = vrev64_f32(v584);
    float32x2_t v643 = vrev64_f32(v585);
    float32x2_t v679 = vadd_f32(v678, v676);
    float32x2_t v683 = vadd_f32(v682, v677);
    float32x2_t v715 = vmul_f32(v714, v713);
    float32x2_t v719 = vmul_f32(v684, v718);
    float32x2_t v723 = vmul_f32(v685, v722);
    float32x2_t v727 = vmul_f32(v686, v726);
    float32x2_t v733 = vrev64_f32(v687);
    float32x2_t v740 = vrev64_f32(v688);
    float32x2_t v747 = vrev64_f32(v689);
    float32x2_t v576 = vadd_f32(v575, v570);
    float32x2_t v593 = vmul_f32(v575, v696);
    float32x2_t v599 = vrev64_f32(v579);
    float32x2_t v630 = vmul_f32(v629, v732);
    float32x2_t v637 = vmul_f32(v636, v739);
    float32x2_t v644 = vmul_f32(v643, v746);
    float32x2_t v680 = vadd_f32(v679, v674);
    float32x2_t v697 = vmul_f32(v679, v696);
    float32x2_t v703 = vrev64_f32(v683);
    float32x2_t v734 = vmul_f32(v733, v732);
    float32x2_t v741 = vmul_f32(v740, v739);
    float32x2_t v748 = vmul_f32(v747, v746);
    float32x2_t v577 = vadd_f32(v576, v548);
    float32x2_t v600 = vmul_f32(v599, v713);
    float32x2_t v645 = vadd_f32(v593, v593);
    float32x2_t v658 = vadd_f32(v611, v630);
    float32x2_t v660 = vsub_f32(v611, v637);
    float32x2_t v662 = vsub_f32(v611, v630);
    float32x2_t v681 = vadd_f32(v680, v549);
    float32x2_t v704 = vmul_f32(v703, v713);
    float32x2_t v749 = vadd_f32(v697, v697);
    float32x2_t v762 = vadd_f32(v715, v734);
    float32x2_t v764 = vsub_f32(v715, v741);
    float32x2_t v766 = vsub_f32(v715, v734);
    float32x2_t v646 = vadd_f32(v645, v593);
    float32x2_t v650 = vadd_f32(v577, v604);
    float32x2_t v659 = vadd_f32(v658, v637);
    float32x2_t v661 = vadd_f32(v660, v644);
    float32x2_t v663 = vsub_f32(v662, v644);
    float32x2_t v750 = vadd_f32(v749, v697);
    float32x2_t v754 = vadd_f32(v681, v708);
    float32x2_t v763 = vadd_f32(v762, v741);
    float32x2_t v765 = vadd_f32(v764, v748);
    float32x2_t v767 = vsub_f32(v766, v748);
    v6[0] = v577;
    v6[ostride * 9] = v681;
    float32x2_t v647 = vadd_f32(v577, v646);
    float32x2_t v651 = vadd_f32(v650, v645);
    float32x2_t v751 = vadd_f32(v681, v750);
    float32x2_t v755 = vadd_f32(v754, v749);
    float32x2_t v648 = vadd_f32(v647, v600);
    float32x2_t v649 = vsub_f32(v647, v600);
    float32x2_t v652 = vadd_f32(v651, v615);
    float32x2_t v654 = vsub_f32(v651, v619);
    float32x2_t v656 = vsub_f32(v651, v615);
    float32x2_t v752 = vadd_f32(v751, v704);
    float32x2_t v753 = vsub_f32(v751, v704);
    float32x2_t v756 = vadd_f32(v755, v719);
    float32x2_t v758 = vsub_f32(v755, v723);
    float32x2_t v760 = vsub_f32(v755, v719);
    float32x2_t v653 = vadd_f32(v652, v619);
    float32x2_t v655 = vadd_f32(v654, v623);
    float32x2_t v657 = vsub_f32(v656, v623);
    float32x2_t v757 = vadd_f32(v756, v723);
    float32x2_t v759 = vadd_f32(v758, v727);
    float32x2_t v761 = vsub_f32(v760, v727);
    v6[ostride * 12] = v649;
    v6[ostride * 3] = v753;
    v6[ostride * 6] = v648;
    v6[ostride * 15] = v752;
    float32x2_t v664 = vadd_f32(v653, v659);
    float32x2_t v665 = vsub_f32(v653, v659);
    float32x2_t v666 = vadd_f32(v655, v661);
    float32x2_t v667 = vsub_f32(v655, v661);
    float32x2_t v668 = vadd_f32(v657, v663);
    float32x2_t v669 = vsub_f32(v657, v663);
    float32x2_t v768 = vadd_f32(v757, v763);
    float32x2_t v769 = vsub_f32(v757, v763);
    float32x2_t v770 = vadd_f32(v759, v765);
    float32x2_t v771 = vsub_f32(v759, v765);
    float32x2_t v772 = vadd_f32(v761, v767);
    float32x2_t v773 = vsub_f32(v761, v767);
    v6[ostride * 10] = v665;
    v6[ostride] = v769;
    v6[ostride * 2] = v666;
    v6[ostride * 11] = v770;
    v6[ostride * 4] = v669;
    v6[ostride * 13] = v773;
    v6[ostride * 14] = v668;
    v6[ostride * 5] = v772;
    v6[ostride * 16] = v667;
    v6[ostride * 7] = v771;
    v6[ostride * 8] = v664;
    v6[ostride * 17] = v768;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs18(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v536 = -5.0000000000000000e-01F;
    float v548 = -1.4999999999999998e+00F;
    float v553 = -8.6602540378443871e-01F;
    float v560 = 7.6604444311897801e-01F;
    float v565 = 9.3969262078590832e-01F;
    float v570 = -1.7364817766693039e-01F;
    float v575 = -6.4278760968653925e-01F;
    float v582 = 3.4202014332566888e-01F;
    float v589 = -9.8480775301220802e-01F;
    const float32x2_t *v842 = &v5[v0];
    float32x2_t *v967 = &v6[v2];
    int64_t v19 = v0 * 9;
    int64_t v34 = v10 * 8;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 11;
    int64_t v76 = v10 * 10;
    int64_t v82 = v0 * 4;
    int64_t v96 = v0 * 13;
    int64_t v111 = v10 * 3;
    int64_t v118 = v10 * 12;
    int64_t v124 = v0 * 6;
    int64_t v138 = v0 * 15;
    int64_t v153 = v10 * 5;
    int64_t v160 = v10 * 14;
    int64_t v166 = v0 * 8;
    int64_t v180 = v0 * 17;
    int64_t v195 = v10 * 7;
    int64_t v202 = v10 * 16;
    int64_t v208 = v0 * 10;
    int64_t v237 = v10 * 9;
    int64_t v250 = v0 * 12;
    int64_t v264 = v0 * 3;
    int64_t v279 = v10 * 11;
    int64_t v286 = v10 * 2;
    int64_t v292 = v0 * 14;
    int64_t v306 = v0 * 5;
    int64_t v321 = v10 * 13;
    int64_t v328 = v10 * 4;
    int64_t v334 = v0 * 16;
    int64_t v348 = v0 * 7;
    int64_t v363 = v10 * 15;
    int64_t v370 = v10 * 6;
    int64_t v371 = v13 * 17;
    float v556 = v4 * v553;
    float v578 = v4 * v575;
    float v585 = v4 * v582;
    float v592 = v4 * v589;
    int64_t v628 = v2 * 9;
    int64_t v635 = v2 * 10;
    int64_t v649 = v2 * 2;
    int64_t v656 = v2 * 11;
    int64_t v663 = v2 * 12;
    int64_t v670 = v2 * 3;
    int64_t v677 = v2 * 4;
    int64_t v684 = v2 * 13;
    int64_t v691 = v2 * 14;
    int64_t v698 = v2 * 5;
    int64_t v705 = v2 * 6;
    int64_t v712 = v2 * 15;
    int64_t v719 = v2 * 16;
    int64_t v726 = v2 * 7;
    int64_t v733 = v2 * 8;
    int64_t v740 = v2 * 17;
    const float32x2_t *v908 = &v5[0];
    svint64_t v909 = svindex_s64(0, v1);
    svfloat32_t v923 = svdup_n_f32(v536);
    svfloat32_t v925 = svdup_n_f32(v548);
    svfloat32_t v927 = svdup_n_f32(v560);
    svfloat32_t v928 = svdup_n_f32(v565);
    svfloat32_t v929 = svdup_n_f32(v570);
    float32x2_t *v940 = &v6[0];
    svint64_t v1094 = svindex_s64(0, v3);
    int64_t v36 = v34 + v371;
    int64_t v71 = v10 + v371;
    int64_t v78 = v76 + v371;
    int64_t v113 = v111 + v371;
    int64_t v120 = v118 + v371;
    int64_t v155 = v153 + v371;
    int64_t v162 = v160 + v371;
    int64_t v197 = v195 + v371;
    int64_t v204 = v202 + v371;
    int64_t v239 = v237 + v371;
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v371]));
    int64_t v281 = v279 + v371;
    int64_t v288 = v286 + v371;
    int64_t v323 = v321 + v371;
    int64_t v330 = v328 + v371;
    int64_t v365 = v363 + v371;
    int64_t v372 = v370 + v371;
    const float32x2_t *v752 = &v5[v19];
    const float32x2_t *v761 = &v5[v40];
    const float32x2_t *v770 = &v5[v54];
    const float32x2_t *v779 = &v5[v82];
    const float32x2_t *v788 = &v5[v96];
    const float32x2_t *v797 = &v5[v124];
    const float32x2_t *v806 = &v5[v138];
    const float32x2_t *v815 = &v5[v166];
    const float32x2_t *v824 = &v5[v180];
    const float32x2_t *v833 = &v5[v208];
    svfloat32_t v844 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v842), v909));
    const float32x2_t *v853 = &v5[v250];
    const float32x2_t *v862 = &v5[v264];
    const float32x2_t *v871 = &v5[v292];
    const float32x2_t *v880 = &v5[v306];
    const float32x2_t *v889 = &v5[v334];
    const float32x2_t *v898 = &v5[v348];
    svfloat32_t v910 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v908), v909));
    svfloat32_t v926 = svdup_n_f32(v556);
    svfloat32_t v930 = svdup_n_f32(v578);
    svfloat32_t v931 = svdup_n_f32(v585);
    svfloat32_t v932 = svdup_n_f32(v592);
    float32x2_t *v949 = &v6[v628];
    float32x2_t *v958 = &v6[v635];
    float32x2_t *v976 = &v6[v649];
    float32x2_t *v985 = &v6[v656];
    float32x2_t *v994 = &v6[v663];
    float32x2_t *v1003 = &v6[v670];
    float32x2_t *v1012 = &v6[v677];
    float32x2_t *v1021 = &v6[v684];
    float32x2_t *v1030 = &v6[v691];
    float32x2_t *v1039 = &v6[v698];
    float32x2_t *v1048 = &v6[v705];
    float32x2_t *v1057 = &v6[v712];
    float32x2_t *v1066 = &v6[v719];
    float32x2_t *v1075 = &v6[v726];
    float32x2_t *v1084 = &v6[v733];
    float32x2_t *v1093 = &v6[v740];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero248, v844, v247, 0),
                     v844, v247, 90);
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v288]));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v331 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v330]));
    svfloat32_t v366 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v365]));
    svfloat32_t v373 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v372]));
    svfloat32_t v754 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v752), v909));
    svfloat32_t v763 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v761), v909));
    svfloat32_t v772 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v770), v909));
    svfloat32_t v781 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v779), v909));
    svfloat32_t v790 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v788), v909));
    svfloat32_t v799 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v797), v909));
    svfloat32_t v808 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v806), v909));
    svfloat32_t v817 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v815), v909));
    svfloat32_t v826 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v824), v909));
    svfloat32_t v835 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v833), v909));
    svfloat32_t v855 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v853), v909));
    svfloat32_t v864 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v862), v909));
    svfloat32_t v873 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v871), v909));
    svfloat32_t v882 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v880), v909));
    svfloat32_t v891 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v889), v909));
    svfloat32_t v900 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v898), v909));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v754, v37, 0),
                     v754, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v763, v72, 0),
                     v763, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v772, v79, 0),
                     v772, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v781, v114, 0),
                     v781, v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v790, v121, 0),
                     v790, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v799, v156, 0),
                     v799, v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v808, v163, 0),
                     v808, v163, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v817, v198, 0),
                     v817, v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v826, v205, 0),
                     v826, v205, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero241, v835, v240, 0),
                     v835, v240, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero283, v855, v282, 0),
                     v855, v282, 90);
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero290, v864, v289, 0),
                     v864, v289, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero325, v873, v324, 0),
                     v873, v324, 90);
    svfloat32_t zero332 = svdup_n_f32(0);
    svfloat32_t v332 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero332, v882, v331, 0),
                     v882, v331, 90);
    svfloat32_t zero367 = svdup_n_f32(0);
    svfloat32_t v367 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero367, v891, v366, 0),
                     v891, v366, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero374, v900, v373, 0),
                     v900, v373, 90);
    svfloat32_t v382 = svadd_f32_x(svptrue_b32(), v910, v38);
    svfloat32_t v383 = svsub_f32_x(svptrue_b32(), v910, v38);
    svfloat32_t v384 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v385 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v386 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v387 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v388 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v389 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v390 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v391 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v392 = svadd_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v393 = svsub_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v394 = svadd_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v395 = svsub_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v398 = svadd_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v399 = svsub_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v384, v398);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v384, v398);
    svfloat32_t v402 = svadd_f32_x(svptrue_b32(), v396, v386);
    svfloat32_t v403 = svsub_f32_x(svptrue_b32(), v396, v386);
    svfloat32_t v404 = svadd_f32_x(svptrue_b32(), v388, v394);
    svfloat32_t v405 = svsub_f32_x(svptrue_b32(), v388, v394);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v390, v392);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v390, v392);
    svfloat32_t v510 = svadd_f32_x(svptrue_b32(), v385, v399);
    svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v385, v399);
    svfloat32_t v512 = svadd_f32_x(svptrue_b32(), v397, v387);
    svfloat32_t v513 = svsub_f32_x(svptrue_b32(), v397, v387);
    svfloat32_t v514 = svadd_f32_x(svptrue_b32(), v389, v395);
    svfloat32_t v515 = svsub_f32_x(svptrue_b32(), v389, v395);
    svfloat32_t v516 = svadd_f32_x(svptrue_b32(), v391, v393);
    svfloat32_t v517 = svsub_f32_x(svptrue_b32(), v391, v393);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v400, v402);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v401, v403);
    svfloat32_t v414 = svsub_f32_x(svptrue_b32(), v400, v402);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v402, v406);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v406, v400);
    svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v401, v403);
    svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v403, v407);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v407, v401);
    svfloat32_t zero448 = svdup_n_f32(0);
    svfloat32_t v448 = svcmla_f32_x(pred_full, zero448, v926, v405, 90);
    svfloat32_t v518 = svadd_f32_x(svptrue_b32(), v510, v512);
    svfloat32_t v522 = svadd_f32_x(svptrue_b32(), v511, v513);
    svfloat32_t v524 = svsub_f32_x(svptrue_b32(), v510, v512);
    svfloat32_t v525 = svsub_f32_x(svptrue_b32(), v512, v516);
    svfloat32_t v526 = svsub_f32_x(svptrue_b32(), v516, v510);
    svfloat32_t v527 = svsub_f32_x(svptrue_b32(), v511, v513);
    svfloat32_t v528 = svsub_f32_x(svptrue_b32(), v513, v517);
    svfloat32_t v529 = svsub_f32_x(svptrue_b32(), v517, v511);
    svfloat32_t zero558 = svdup_n_f32(0);
    svfloat32_t v558 = svcmla_f32_x(pred_full, zero558, v926, v515, 90);
    svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v408, v406);
    svfloat32_t v413 = svadd_f32_x(svptrue_b32(), v412, v407);
    svfloat32_t zero470 = svdup_n_f32(0);
    svfloat32_t v470 = svcmla_f32_x(pred_full, zero470, v930, v417, 90);
    svfloat32_t zero477 = svdup_n_f32(0);
    svfloat32_t v477 = svcmla_f32_x(pred_full, zero477, v931, v418, 90);
    svfloat32_t zero484 = svdup_n_f32(0);
    svfloat32_t v484 = svcmla_f32_x(pred_full, zero484, v932, v419, 90);
    svfloat32_t v519 = svadd_f32_x(svptrue_b32(), v518, v516);
    svfloat32_t v523 = svadd_f32_x(svptrue_b32(), v522, v517);
    svfloat32_t zero580 = svdup_n_f32(0);
    svfloat32_t v580 = svcmla_f32_x(pred_full, zero580, v930, v527, 90);
    svfloat32_t zero587 = svdup_n_f32(0);
    svfloat32_t v587 = svcmla_f32_x(pred_full, zero587, v931, v528, 90);
    svfloat32_t zero594 = svdup_n_f32(0);
    svfloat32_t v594 = svcmla_f32_x(pred_full, zero594, v932, v529, 90);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v409, v404);
    svfloat32_t v429 = svmul_f32_x(svptrue_b32(), v409, v923);
    svfloat32_t zero436 = svdup_n_f32(0);
    svfloat32_t v436 = svcmla_f32_x(pred_full, zero436, v926, v413, 90);
    svfloat32_t v498 = svadd_f32_x(svptrue_b32(), v448, v470);
    svfloat32_t v500 = svsub_f32_x(svptrue_b32(), v448, v477);
    svfloat32_t v502 = svsub_f32_x(svptrue_b32(), v448, v470);
    svfloat32_t v520 = svadd_f32_x(svptrue_b32(), v519, v514);
    svfloat32_t v539 = svmul_f32_x(svptrue_b32(), v519, v923);
    svfloat32_t zero546 = svdup_n_f32(0);
    svfloat32_t v546 = svcmla_f32_x(pred_full, zero546, v926, v523, 90);
    svfloat32_t v608 = svadd_f32_x(svptrue_b32(), v558, v580);
    svfloat32_t v610 = svsub_f32_x(svptrue_b32(), v558, v587);
    svfloat32_t v612 = svsub_f32_x(svptrue_b32(), v558, v580);
    svfloat32_t v411 = svadd_f32_x(svptrue_b32(), v410, v382);
    svfloat32_t v485 = svadd_f32_x(svptrue_b32(), v429, v429);
    svfloat32_t v499 = svadd_f32_x(svptrue_b32(), v498, v477);
    svfloat32_t v501 = svadd_f32_x(svptrue_b32(), v500, v484);
    svfloat32_t v503 = svsub_f32_x(svptrue_b32(), v502, v484);
    svfloat32_t v521 = svadd_f32_x(svptrue_b32(), v520, v383);
    svfloat32_t v595 = svadd_f32_x(svptrue_b32(), v539, v539);
    svfloat32_t v609 = svadd_f32_x(svptrue_b32(), v608, v587);
    svfloat32_t v611 = svadd_f32_x(svptrue_b32(), v610, v594);
    svfloat32_t v613 = svsub_f32_x(svptrue_b32(), v612, v594);
    svfloat32_t v486 = svmla_f32_x(pred_full, v485, v409, v923);
    svfloat32_t v490 = svmla_f32_x(pred_full, v411, v404, v925);
    svfloat32_t v596 = svmla_f32_x(pred_full, v595, v519, v923);
    svfloat32_t v600 = svmla_f32_x(pred_full, v521, v514, v925);
    svst1_scatter_s64index_f64(pred_full, (double *)(v940), v1094,
                               svreinterpret_f64_f32(v411));
    svst1_scatter_s64index_f64(pred_full, (double *)(v949), v1094,
                               svreinterpret_f64_f32(v521));
    svfloat32_t v487 = svadd_f32_x(svptrue_b32(), v411, v486);
    svfloat32_t v491 = svadd_f32_x(svptrue_b32(), v490, v485);
    svfloat32_t v597 = svadd_f32_x(svptrue_b32(), v521, v596);
    svfloat32_t v601 = svadd_f32_x(svptrue_b32(), v600, v595);
    svfloat32_t v488 = svadd_f32_x(svptrue_b32(), v487, v436);
    svfloat32_t v489 = svsub_f32_x(svptrue_b32(), v487, v436);
    svfloat32_t v492 = svmla_f32_x(pred_full, v491, v414, v927);
    svfloat32_t v494 = svmls_f32_x(pred_full, v491, v415, v928);
    svfloat32_t v496 = svmls_f32_x(pred_full, v491, v414, v927);
    svfloat32_t v598 = svadd_f32_x(svptrue_b32(), v597, v546);
    svfloat32_t v599 = svsub_f32_x(svptrue_b32(), v597, v546);
    svfloat32_t v602 = svmla_f32_x(pred_full, v601, v524, v927);
    svfloat32_t v604 = svmls_f32_x(pred_full, v601, v525, v928);
    svfloat32_t v606 = svmls_f32_x(pred_full, v601, v524, v927);
    svfloat32_t v493 = svmla_f32_x(pred_full, v492, v415, v928);
    svfloat32_t v495 = svmla_f32_x(pred_full, v494, v416, v929);
    svfloat32_t v497 = svmls_f32_x(pred_full, v496, v416, v929);
    svfloat32_t v603 = svmla_f32_x(pred_full, v602, v525, v928);
    svfloat32_t v605 = svmla_f32_x(pred_full, v604, v526, v929);
    svfloat32_t v607 = svmls_f32_x(pred_full, v606, v526, v929);
    svst1_scatter_s64index_f64(pred_full, (double *)(v994), v1094,
                               svreinterpret_f64_f32(v489));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1003), v1094,
                               svreinterpret_f64_f32(v599));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1048), v1094,
                               svreinterpret_f64_f32(v488));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1057), v1094,
                               svreinterpret_f64_f32(v598));
    svfloat32_t v504 = svadd_f32_x(svptrue_b32(), v493, v499);
    svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v493, v499);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v495, v501);
    svfloat32_t v507 = svsub_f32_x(svptrue_b32(), v495, v501);
    svfloat32_t v508 = svadd_f32_x(svptrue_b32(), v497, v503);
    svfloat32_t v509 = svsub_f32_x(svptrue_b32(), v497, v503);
    svfloat32_t v614 = svadd_f32_x(svptrue_b32(), v603, v609);
    svfloat32_t v615 = svsub_f32_x(svptrue_b32(), v603, v609);
    svfloat32_t v616 = svadd_f32_x(svptrue_b32(), v605, v611);
    svfloat32_t v617 = svsub_f32_x(svptrue_b32(), v605, v611);
    svfloat32_t v618 = svadd_f32_x(svptrue_b32(), v607, v613);
    svfloat32_t v619 = svsub_f32_x(svptrue_b32(), v607, v613);
    svst1_scatter_s64index_f64(pred_full, (double *)(v958), v1094,
                               svreinterpret_f64_f32(v505));
    svst1_scatter_s64index_f64(pred_full, (double *)(v967), v1094,
                               svreinterpret_f64_f32(v615));
    svst1_scatter_s64index_f64(pred_full, (double *)(v976), v1094,
                               svreinterpret_f64_f32(v506));
    svst1_scatter_s64index_f64(pred_full, (double *)(v985), v1094,
                               svreinterpret_f64_f32(v616));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1012), v1094,
                               svreinterpret_f64_f32(v509));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1021), v1094,
                               svreinterpret_f64_f32(v619));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1030), v1094,
                               svreinterpret_f64_f32(v508));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1039), v1094,
                               svreinterpret_f64_f32(v618));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1066), v1094,
                               svreinterpret_f64_f32(v507));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1075), v1094,
                               svreinterpret_f64_f32(v617));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1084), v1094,
                               svreinterpret_f64_f32(v504));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1093), v1094,
                               svreinterpret_f64_f32(v614));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs19(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v20 = v5[istride];
    float v667 = -1.0555555555555556e+00F;
    float v671 = 1.7752228513927079e-01F;
    float v675 = -1.2820077502191529e-01F;
    float v679 = 4.9321510117355499e-02F;
    float v683 = 5.7611011491005903e-01F;
    float v687 = -7.4996449655536279e-01F;
    float v691 = -1.7385438164530381e-01F;
    float v695 = -2.1729997561977314e+00F;
    float v699 = -1.7021211726914738e+00F;
    float v703 = 4.7087858350625778e-01F;
    float v707 = -2.0239400846888440e+00F;
    float v711 = 1.0551641201664090e-01F;
    float v715 = 2.1294564967054850e+00F;
    float v719 = -7.5087543897371167e-01F;
    float v723 = 1.4812817695157160e-01F;
    float v727 = 8.9900361592528333e-01F;
    float v731 = -6.2148246772602778e-01F;
    float v735 = -7.9869352098712687e-01F;
    float v739 = -4.7339199623771833e-01F;
    float v742 = -2.4216105241892630e-01F;
    float v743 = 2.4216105241892630e-01F;
    float v749 = -5.9368607967505101e-02F;
    float v750 = 5.9368607967505101e-02F;
    float v756 = 1.2578688255176201e-02F;
    float v757 = -1.2578688255176201e-02F;
    float v763 = -4.6789919712328903e-02F;
    float v764 = 4.6789919712328903e-02F;
    float v770 = -9.3750121913782358e-01F;
    float v771 = 9.3750121913782358e-01F;
    float v777 = -5.0111537043352902e-02F;
    float v778 = 5.0111537043352902e-02F;
    float v784 = -9.8761275618117661e-01F;
    float v785 = 9.8761275618117661e-01F;
    float v791 = -1.1745786501205959e+00F;
    float v792 = 1.1745786501205959e+00F;
    float v798 = 1.1114482296234993e+00F;
    float v799 = -1.1114482296234993e+00F;
    float v805 = 2.2860268797440955e+00F;
    float v806 = -2.2860268797440955e+00F;
    float v812 = 2.6420523257930939e-01F;
    float v813 = -2.6420523257930939e-01F;
    float v819 = 2.1981792779352136e+00F;
    float v820 = -2.1981792779352136e+00F;
    float v826 = 1.9339740453559042e+00F;
    float v827 = -1.9339740453559042e+00F;
    float v833 = -7.4825847091254893e-01F;
    float v834 = 7.4825847091254893e-01F;
    float v840 = -4.7820835642768872e-01F;
    float v841 = 4.7820835642768872e-01F;
    float v847 = 2.7005011448486022e-01F;
    float v848 = -2.7005011448486022e-01F;
    float v854 = -3.4642356159542270e-01F;
    float v855 = 3.4642356159542270e-01F;
    float v861 = -8.3485429360688279e-01F;
    float v862 = 8.3485429360688279e-01F;
    float v868 = -3.9375928506743518e-01F;
    float v869 = 3.9375928506743518e-01F;
    float32x2_t v871 = (float32x2_t){v4, v4};
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    float32x2_t v612 = v5[0];
    float32x2_t v668 = (float32x2_t){v667, v667};
    float32x2_t v672 = (float32x2_t){v671, v671};
    float32x2_t v676 = (float32x2_t){v675, v675};
    float32x2_t v680 = (float32x2_t){v679, v679};
    float32x2_t v684 = (float32x2_t){v683, v683};
    float32x2_t v688 = (float32x2_t){v687, v687};
    float32x2_t v692 = (float32x2_t){v691, v691};
    float32x2_t v696 = (float32x2_t){v695, v695};
    float32x2_t v700 = (float32x2_t){v699, v699};
    float32x2_t v704 = (float32x2_t){v703, v703};
    float32x2_t v708 = (float32x2_t){v707, v707};
    float32x2_t v712 = (float32x2_t){v711, v711};
    float32x2_t v716 = (float32x2_t){v715, v715};
    float32x2_t v720 = (float32x2_t){v719, v719};
    float32x2_t v724 = (float32x2_t){v723, v723};
    float32x2_t v728 = (float32x2_t){v727, v727};
    float32x2_t v732 = (float32x2_t){v731, v731};
    float32x2_t v736 = (float32x2_t){v735, v735};
    float32x2_t v740 = (float32x2_t){v739, v739};
    float32x2_t v744 = (float32x2_t){v742, v743};
    float32x2_t v751 = (float32x2_t){v749, v750};
    float32x2_t v758 = (float32x2_t){v756, v757};
    float32x2_t v765 = (float32x2_t){v763, v764};
    float32x2_t v772 = (float32x2_t){v770, v771};
    float32x2_t v779 = (float32x2_t){v777, v778};
    float32x2_t v786 = (float32x2_t){v784, v785};
    float32x2_t v793 = (float32x2_t){v791, v792};
    float32x2_t v800 = (float32x2_t){v798, v799};
    float32x2_t v807 = (float32x2_t){v805, v806};
    float32x2_t v814 = (float32x2_t){v812, v813};
    float32x2_t v821 = (float32x2_t){v819, v820};
    float32x2_t v828 = (float32x2_t){v826, v827};
    float32x2_t v835 = (float32x2_t){v833, v834};
    float32x2_t v842 = (float32x2_t){v840, v841};
    float32x2_t v849 = (float32x2_t){v847, v848};
    float32x2_t v856 = (float32x2_t){v854, v855};
    float32x2_t v863 = (float32x2_t){v861, v862};
    float32x2_t v870 = (float32x2_t){v868, v869};
    float32x2_t v38 = v5[istride * 18];
    float32x2_t v56 = v7[j * 36];
    int64_t v60 = j * 36 + 1;
    int64_t v68 = 34 + j * 36;
    float32x2_t v82 = v5[istride * 2];
    float32x2_t v100 = v5[istride * 17];
    int64_t v117 = 32 + j * 36;
    int64_t v130 = 2 + j * 36;
    float32x2_t v144 = v5[istride * 4];
    float32x2_t v162 = v5[istride * 15];
    int64_t v179 = 6 + j * 36;
    int64_t v192 = 28 + j * 36;
    float32x2_t v206 = v5[istride * 8];
    float32x2_t v224 = v5[istride * 11];
    int64_t v241 = 20 + j * 36;
    int64_t v254 = 14 + j * 36;
    float32x2_t v268 = v5[istride * 16];
    float32x2_t v286 = v5[istride * 3];
    int64_t v303 = 30 + j * 36;
    int64_t v316 = 4 + j * 36;
    float32x2_t v330 = v5[istride * 13];
    float32x2_t v348 = v5[istride * 6];
    int64_t v365 = 10 + j * 36;
    int64_t v378 = 24 + j * 36;
    float32x2_t v392 = v5[istride * 7];
    float32x2_t v410 = v5[istride * 12];
    int64_t v427 = 12 + j * 36;
    int64_t v440 = 22 + j * 36;
    float32x2_t v454 = v5[istride * 14];
    float32x2_t v472 = v5[istride * 5];
    int64_t v489 = 8 + j * 36;
    int64_t v502 = 26 + j * 36;
    float32x2_t v516 = v5[istride * 9];
    float32x2_t v534 = v5[istride * 10];
    int64_t v551 = 16 + j * 36;
    int64_t v564 = 18 + j * 36;
    float32x2_t v746 = vmul_f32(v871, v744);
    float32x2_t v753 = vmul_f32(v871, v751);
    float32x2_t v760 = vmul_f32(v871, v758);
    float32x2_t v767 = vmul_f32(v871, v765);
    float32x2_t v774 = vmul_f32(v871, v772);
    float32x2_t v781 = vmul_f32(v871, v779);
    float32x2_t v788 = vmul_f32(v871, v786);
    float32x2_t v795 = vmul_f32(v871, v793);
    float32x2_t v802 = vmul_f32(v871, v800);
    float32x2_t v809 = vmul_f32(v871, v807);
    float32x2_t v816 = vmul_f32(v871, v814);
    float32x2_t v823 = vmul_f32(v871, v821);
    float32x2_t v830 = vmul_f32(v871, v828);
    float32x2_t v837 = vmul_f32(v871, v835);
    float32x2_t v844 = vmul_f32(v871, v842);
    float32x2_t v851 = vmul_f32(v871, v849);
    float32x2_t v858 = vmul_f32(v871, v856);
    float32x2_t v865 = vmul_f32(v871, v863);
    float32x2_t v872 = vmul_f32(v871, v870);
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v100, v100);
    float32x2_t v120 = vtrn2_f32(v100, v100);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v82, v82);
    float32x2_t v133 = vtrn2_f32(v82, v82);
    int64_t v135 = v130 + 1;
    float32x2_t v180 = v7[v179];
    float32x2_t v181 = vtrn1_f32(v144, v144);
    float32x2_t v182 = vtrn2_f32(v144, v144);
    int64_t v184 = v179 + 1;
    float32x2_t v193 = v7[v192];
    float32x2_t v194 = vtrn1_f32(v162, v162);
    float32x2_t v195 = vtrn2_f32(v162, v162);
    int64_t v197 = v192 + 1;
    float32x2_t v242 = v7[v241];
    float32x2_t v243 = vtrn1_f32(v224, v224);
    float32x2_t v244 = vtrn2_f32(v224, v224);
    int64_t v246 = v241 + 1;
    float32x2_t v255 = v7[v254];
    float32x2_t v256 = vtrn1_f32(v206, v206);
    float32x2_t v257 = vtrn2_f32(v206, v206);
    int64_t v259 = v254 + 1;
    float32x2_t v304 = v7[v303];
    float32x2_t v305 = vtrn1_f32(v268, v268);
    float32x2_t v306 = vtrn2_f32(v268, v268);
    int64_t v308 = v303 + 1;
    float32x2_t v317 = v7[v316];
    float32x2_t v318 = vtrn1_f32(v286, v286);
    float32x2_t v319 = vtrn2_f32(v286, v286);
    int64_t v321 = v316 + 1;
    float32x2_t v366 = v7[v365];
    float32x2_t v367 = vtrn1_f32(v348, v348);
    float32x2_t v368 = vtrn2_f32(v348, v348);
    int64_t v370 = v365 + 1;
    float32x2_t v379 = v7[v378];
    float32x2_t v380 = vtrn1_f32(v330, v330);
    float32x2_t v381 = vtrn2_f32(v330, v330);
    int64_t v383 = v378 + 1;
    float32x2_t v428 = v7[v427];
    float32x2_t v429 = vtrn1_f32(v392, v392);
    float32x2_t v430 = vtrn2_f32(v392, v392);
    int64_t v432 = v427 + 1;
    float32x2_t v441 = v7[v440];
    float32x2_t v442 = vtrn1_f32(v410, v410);
    float32x2_t v443 = vtrn2_f32(v410, v410);
    int64_t v445 = v440 + 1;
    float32x2_t v490 = v7[v489];
    float32x2_t v491 = vtrn1_f32(v472, v472);
    float32x2_t v492 = vtrn2_f32(v472, v472);
    int64_t v494 = v489 + 1;
    float32x2_t v503 = v7[v502];
    float32x2_t v504 = vtrn1_f32(v454, v454);
    float32x2_t v505 = vtrn2_f32(v454, v454);
    int64_t v507 = v502 + 1;
    float32x2_t v552 = v7[v551];
    float32x2_t v553 = vtrn1_f32(v516, v516);
    float32x2_t v554 = vtrn2_f32(v516, v516);
    int64_t v556 = v551 + 1;
    float32x2_t v565 = v7[v564];
    float32x2_t v566 = vtrn1_f32(v534, v534);
    float32x2_t v567 = vtrn2_f32(v534, v534);
    int64_t v569 = v564 + 1;
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v185 = v7[v184];
    float32x2_t v186 = vmul_f32(v181, v180);
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vmul_f32(v194, v193);
    float32x2_t v247 = v7[v246];
    float32x2_t v248 = vmul_f32(v243, v242);
    float32x2_t v260 = v7[v259];
    float32x2_t v261 = vmul_f32(v256, v255);
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vmul_f32(v305, v304);
    float32x2_t v322 = v7[v321];
    float32x2_t v323 = vmul_f32(v318, v317);
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vmul_f32(v367, v366);
    float32x2_t v384 = v7[v383];
    float32x2_t v385 = vmul_f32(v380, v379);
    float32x2_t v433 = v7[v432];
    float32x2_t v434 = vmul_f32(v429, v428);
    float32x2_t v446 = v7[v445];
    float32x2_t v447 = vmul_f32(v442, v441);
    float32x2_t v495 = v7[v494];
    float32x2_t v496 = vmul_f32(v491, v490);
    float32x2_t v508 = v7[v507];
    float32x2_t v509 = vmul_f32(v504, v503);
    float32x2_t v557 = v7[v556];
    float32x2_t v558 = vmul_f32(v553, v552);
    float32x2_t v570 = v7[v569];
    float32x2_t v571 = vmul_f32(v566, v565);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v188 = vfma_f32(v186, v182, v185);
    float32x2_t v201 = vfma_f32(v199, v195, v198);
    float32x2_t v250 = vfma_f32(v248, v244, v247);
    float32x2_t v263 = vfma_f32(v261, v257, v260);
    float32x2_t v312 = vfma_f32(v310, v306, v309);
    float32x2_t v325 = vfma_f32(v323, v319, v322);
    float32x2_t v374 = vfma_f32(v372, v368, v371);
    float32x2_t v387 = vfma_f32(v385, v381, v384);
    float32x2_t v436 = vfma_f32(v434, v430, v433);
    float32x2_t v449 = vfma_f32(v447, v443, v446);
    float32x2_t v498 = vfma_f32(v496, v492, v495);
    float32x2_t v511 = vfma_f32(v509, v505, v508);
    float32x2_t v560 = vfma_f32(v558, v554, v557);
    float32x2_t v573 = vfma_f32(v571, v567, v570);
    float32x2_t v574 = vadd_f32(v64, v77);
    float32x2_t v575 = vsub_f32(v64, v77);
    float32x2_t v576 = vadd_f32(v139, v126);
    float32x2_t v577 = vsub_f32(v126, v139);
    float32x2_t v578 = vadd_f32(v188, v201);
    float32x2_t v579 = vsub_f32(v188, v201);
    float32x2_t v580 = vadd_f32(v263, v250);
    float32x2_t v581 = vsub_f32(v250, v263);
    float32x2_t v582 = vadd_f32(v312, v325);
    float32x2_t v583 = vsub_f32(v312, v325);
    float32x2_t v584 = vadd_f32(v387, v374);
    float32x2_t v585 = vsub_f32(v374, v387);
    float32x2_t v586 = vadd_f32(v436, v449);
    float32x2_t v587 = vsub_f32(v436, v449);
    float32x2_t v588 = vadd_f32(v511, v498);
    float32x2_t v589 = vsub_f32(v498, v511);
    float32x2_t v590 = vadd_f32(v560, v573);
    float32x2_t v591 = vsub_f32(v560, v573);
    float32x2_t v592 = vsub_f32(v574, v586);
    float32x2_t v593 = vsub_f32(v576, v588);
    float32x2_t v594 = vsub_f32(v578, v590);
    float32x2_t v595 = vsub_f32(v580, v586);
    float32x2_t v596 = vsub_f32(v582, v588);
    float32x2_t v597 = vsub_f32(v584, v590);
    float32x2_t v598 = vadd_f32(v574, v580);
    float32x2_t v600 = vadd_f32(v576, v582);
    float32x2_t v602 = vadd_f32(v578, v584);
    float32x2_t v630 = vsub_f32(v575, v587);
    float32x2_t v631 = vsub_f32(v577, v589);
    float32x2_t v632 = vsub_f32(v579, v591);
    float32x2_t v633 = vsub_f32(v581, v587);
    float32x2_t v634 = vsub_f32(v583, v589);
    float32x2_t v635 = vsub_f32(v585, v591);
    float32x2_t v636 = vadd_f32(v575, v581);
    float32x2_t v638 = vadd_f32(v577, v583);
    float32x2_t v640 = vadd_f32(v579, v585);
    float32x2_t v599 = vadd_f32(v598, v586);
    float32x2_t v601 = vadd_f32(v600, v588);
    float32x2_t v603 = vadd_f32(v602, v590);
    float32x2_t v604 = vadd_f32(v592, v594);
    float32x2_t v605 = vadd_f32(v595, v597);
    float32x2_t v620 = vsub_f32(v592, v595);
    float32x2_t v621 = vsub_f32(v594, v597);
    float32x2_t v637 = vadd_f32(v636, v587);
    float32x2_t v639 = vadd_f32(v638, v589);
    float32x2_t v641 = vadd_f32(v640, v591);
    float32x2_t v642 = vadd_f32(v630, v632);
    float32x2_t v643 = vadd_f32(v633, v635);
    float32x2_t v652 = vsub_f32(v630, v633);
    float32x2_t v653 = vsub_f32(v632, v635);
    float32x2_t v697 = vmul_f32(v595, v696);
    float32x2_t v709 = vmul_f32(v597, v708);
    float32x2_t v717 = vmul_f32(v594, v716);
    float32x2_t v796 = vrev64_f32(v633);
    float32x2_t v810 = vrev64_f32(v630);
    float32x2_t v817 = vrev64_f32(v635);
    float32x2_t v831 = vrev64_f32(v632);
    float32x2_t v606 = vadd_f32(v599, v601);
    float32x2_t v614 = vadd_f32(v605, v596);
    float32x2_t v615 = vadd_f32(v604, v593);
    float32x2_t v617 = vsub_f32(v605, v596);
    float32x2_t v618 = vsub_f32(v604, v593);
    float32x2_t v622 = vsub_f32(v592, v621);
    float32x2_t v624 = vadd_f32(v620, v597);
    float32x2_t v627 = vsub_f32(v599, v603);
    float32x2_t v628 = vsub_f32(v601, v603);
    float32x2_t v644 = vadd_f32(v637, v639);
    float32x2_t v646 = vadd_f32(v643, v634);
    float32x2_t v647 = vadd_f32(v642, v631);
    float32x2_t v649 = vsub_f32(v643, v634);
    float32x2_t v650 = vsub_f32(v642, v631);
    float32x2_t v654 = vsub_f32(v630, v653);
    float32x2_t v656 = vadd_f32(v652, v635);
    float32x2_t v659 = vsub_f32(v637, v641);
    float32x2_t v660 = vsub_f32(v639, v641);
    float32x2_t v701 = vmul_f32(v620, v700);
    float32x2_t v713 = vmul_f32(v621, v712);
    float32x2_t v797 = vmul_f32(v796, v795);
    float32x2_t v803 = vrev64_f32(v652);
    float32x2_t v818 = vmul_f32(v817, v816);
    float32x2_t v824 = vrev64_f32(v653);
    float32x2_t v832 = vmul_f32(v831, v830);
    float32x2_t v607 = vadd_f32(v606, v603);
    float32x2_t v616 = vsub_f32(v615, v614);
    float32x2_t v619 = vsub_f32(v618, v617);
    float32x2_t v623 = vsub_f32(v622, v596);
    float32x2_t v625 = vsub_f32(v624, v593);
    float32x2_t v629 = vadd_f32(v627, v628);
    float32x2_t v645 = vadd_f32(v644, v641);
    float32x2_t v648 = vsub_f32(v647, v646);
    float32x2_t v651 = vsub_f32(v650, v649);
    float32x2_t v655 = vsub_f32(v654, v634);
    float32x2_t v657 = vsub_f32(v656, v631);
    float32x2_t v661 = vadd_f32(v659, v660);
    float32x2_t v673 = vmul_f32(v614, v672);
    float32x2_t v677 = vmul_f32(v615, v676);
    float32x2_t v685 = vmul_f32(v617, v684);
    float32x2_t v689 = vmul_f32(v618, v688);
    float32x2_t v733 = vmul_f32(v627, v732);
    float32x2_t v737 = vmul_f32(v628, v736);
    float32x2_t v754 = vrev64_f32(v646);
    float32x2_t v761 = vrev64_f32(v647);
    float32x2_t v775 = vrev64_f32(v649);
    float32x2_t v782 = vrev64_f32(v650);
    float32x2_t v804 = vmul_f32(v803, v802);
    float32x2_t v825 = vmul_f32(v824, v823);
    float32x2_t v859 = vrev64_f32(v659);
    float32x2_t v866 = vrev64_f32(v660);
    float32x2_t v613 = vadd_f32(v612, v607);
    float32x2_t v626 = vsub_f32(v623, v625);
    float32x2_t v658 = vsub_f32(v655, v657);
    float32x2_t v669 = vmul_f32(v607, v668);
    float32x2_t v681 = vmul_f32(v616, v680);
    float32x2_t v693 = vmul_f32(v619, v692);
    float32x2_t v721 = vmul_f32(v623, v720);
    float32x2_t v725 = vmul_f32(v625, v724);
    float32x2_t v741 = vmul_f32(v629, v740);
    float32x2_t v747 = vrev64_f32(v645);
    float32x2_t v755 = vmul_f32(v754, v753);
    float32x2_t v762 = vmul_f32(v761, v760);
    float32x2_t v768 = vrev64_f32(v648);
    float32x2_t v776 = vmul_f32(v775, v774);
    float32x2_t v783 = vmul_f32(v782, v781);
    float32x2_t v789 = vrev64_f32(v651);
    float32x2_t v838 = vrev64_f32(v655);
    float32x2_t v845 = vrev64_f32(v657);
    float32x2_t v860 = vmul_f32(v859, v858);
    float32x2_t v867 = vmul_f32(v866, v865);
    float32x2_t v873 = vrev64_f32(v661);
    float32x2_t v875 = vadd_f32(v673, v677);
    float32x2_t v876 = vadd_f32(v685, v689);
    float32x2_t v729 = vmul_f32(v626, v728);
    float32x2_t v748 = vmul_f32(v747, v746);
    float32x2_t v769 = vmul_f32(v768, v767);
    float32x2_t v790 = vmul_f32(v789, v788);
    float32x2_t v839 = vmul_f32(v838, v837);
    float32x2_t v846 = vmul_f32(v845, v844);
    float32x2_t v852 = vrev64_f32(v658);
    float32x2_t v874 = vmul_f32(v873, v872);
    float32x2_t v878 = vadd_f32(v875, v876);
    float32x2_t v879 = vadd_f32(v673, v681);
    float32x2_t v880 = vadd_f32(v685, v693);
    float32x2_t v897 = vsub_f32(v875, v876);
    float32x2_t v899 = vsub_f32(v733, v741);
    float32x2_t v900 = vsub_f32(v737, v741);
    float32x2_t v901 = vadd_f32(v669, v613);
    float32x2_t v906 = vadd_f32(v755, v762);
    float32x2_t v907 = vadd_f32(v776, v783);
    v6[0] = v613;
    float32x2_t v853 = vmul_f32(v852, v851);
    float32x2_t v877 = vadd_f32(v725, v729);
    float32x2_t v881 = vadd_f32(v721, v729);
    float32x2_t v882 = vsub_f32(v697, v878);
    float32x2_t v883 = vadd_f32(v879, v880);
    float32x2_t v889 = vsub_f32(v879, v880);
    float32x2_t v894 = vadd_f32(v878, v717);
    float32x2_t v902 = vadd_f32(v901, v899);
    float32x2_t v903 = vsub_f32(v901, v899);
    float32x2_t v905 = vadd_f32(v901, v900);
    float32x2_t v909 = vadd_f32(v906, v907);
    float32x2_t v910 = vadd_f32(v755, v769);
    float32x2_t v911 = vadd_f32(v776, v790);
    float32x2_t v928 = vsub_f32(v906, v907);
    float32x2_t v930 = vsub_f32(v860, v874);
    float32x2_t v931 = vsub_f32(v867, v874);
    float32x2_t v884 = vsub_f32(v709, v881);
    float32x2_t v885 = vadd_f32(v701, v877);
    float32x2_t v887 = vadd_f32(v883, v713);
    float32x2_t v890 = vadd_f32(v889, v877);
    float32x2_t v891 = vadd_f32(v882, v883);
    float32x2_t v898 = vadd_f32(v897, v881);
    float32x2_t v904 = vsub_f32(v903, v900);
    float32x2_t v908 = vadd_f32(v846, v853);
    float32x2_t v912 = vadd_f32(v839, v853);
    float32x2_t v913 = vsub_f32(v797, v909);
    float32x2_t v914 = vadd_f32(v910, v911);
    float32x2_t v920 = vsub_f32(v910, v911);
    float32x2_t v925 = vadd_f32(v909, v832);
    float32x2_t v932 = vadd_f32(v748, v930);
    float32x2_t v933 = vsub_f32(v748, v930);
    float32x2_t v935 = vadd_f32(v748, v931);
    float32x2_t v886 = vadd_f32(v885, v882);
    float32x2_t v888 = vadd_f32(v887, v884);
    float32x2_t v892 = vfma_f32(v891, v592, v704);
    float32x2_t v895 = vadd_f32(v894, v884);
    float32x2_t v915 = vsub_f32(v818, v912);
    float32x2_t v916 = vadd_f32(v804, v908);
    float32x2_t v918 = vadd_f32(v914, v825);
    float32x2_t v921 = vadd_f32(v920, v908);
    float32x2_t v922 = vadd_f32(v913, v914);
    float32x2_t v929 = vadd_f32(v928, v912);
    float32x2_t v934 = vsub_f32(v933, v931);
    float32x2_t v940 = vsub_f32(v898, v890);
    float32x2_t v944 = vsub_f32(v905, v898);
    float32x2_t v947 = vadd_f32(v890, v905);
    float32x2_t v893 = vadd_f32(v892, v881);
    float32x2_t v896 = vadd_f32(v895, v877);
    float32x2_t v917 = vadd_f32(v916, v913);
    float32x2_t v919 = vadd_f32(v918, v915);
    float32x2_t v923 = vfma_f32(v922, v810, v809);
    float32x2_t v926 = vadd_f32(v925, v915);
    float32x2_t v941 = vadd_f32(v940, v905);
    float32x2_t v945 = vadd_f32(v886, v902);
    float32x2_t v946 = vadd_f32(v888, v904);
    float32x2_t v952 = vsub_f32(v929, v921);
    float32x2_t v956 = vsub_f32(v929, v935);
    float32x2_t v959 = vadd_f32(v921, v935);
    float32x2_t v924 = vadd_f32(v923, v912);
    float32x2_t v927 = vadd_f32(v926, v908);
    float32x2_t v936 = vsub_f32(v893, v886);
    float32x2_t v938 = vsub_f32(v896, v888);
    float32x2_t v942 = vsub_f32(v902, v893);
    float32x2_t v943 = vsub_f32(v904, v896);
    float32x2_t v953 = vadd_f32(v952, v935);
    float32x2_t v957 = vadd_f32(v917, v932);
    float32x2_t v958 = vadd_f32(v919, v934);
    float32x2_t v977 = vsub_f32(v947, v959);
    float32x2_t v983 = vadd_f32(v947, v959);
    float32x2_t v989 = vadd_f32(v944, v956);
    float32x2_t v995 = vsub_f32(v944, v956);
    float32x2_t v937 = vadd_f32(v936, v902);
    float32x2_t v939 = vadd_f32(v938, v904);
    float32x2_t v948 = vsub_f32(v924, v917);
    float32x2_t v950 = vsub_f32(v927, v919);
    float32x2_t v954 = vsub_f32(v932, v924);
    float32x2_t v955 = vsub_f32(v934, v927);
    v6[ostride * 2] = v977;
    v6[ostride * 17] = v983;
    v6[ostride * 3] = v989;
    v6[ostride * 16] = v995;
    float32x2_t v1001 = vadd_f32(v946, v958);
    float32x2_t v1007 = vsub_f32(v946, v958);
    float32x2_t v1013 = vadd_f32(v941, v953);
    float32x2_t v1019 = vsub_f32(v941, v953);
    float32x2_t v1049 = vsub_f32(v945, v957);
    float32x2_t v1055 = vadd_f32(v945, v957);
    float32x2_t v949 = vadd_f32(v948, v932);
    float32x2_t v951 = vadd_f32(v950, v934);
    v6[ostride * 4] = v1001;
    v6[ostride * 15] = v1007;
    v6[ostride * 5] = v1013;
    v6[ostride * 14] = v1019;
    float32x2_t v1025 = vadd_f32(v943, v955);
    float32x2_t v1031 = vsub_f32(v943, v955);
    float32x2_t v1037 = vadd_f32(v942, v954);
    float32x2_t v1043 = vsub_f32(v942, v954);
    v6[ostride * 8] = v1049;
    v6[ostride * 11] = v1055;
    float32x2_t v965 = vadd_f32(v937, v949);
    float32x2_t v971 = vsub_f32(v937, v949);
    v6[ostride * 6] = v1025;
    v6[ostride * 13] = v1031;
    v6[ostride * 7] = v1037;
    v6[ostride * 12] = v1043;
    float32x2_t v1061 = vadd_f32(v939, v951);
    float32x2_t v1067 = vsub_f32(v939, v951);
    v6[ostride] = v965;
    v6[ostride * 18] = v971;
    v6[ostride * 9] = v1061;
    v6[ostride * 10] = v1067;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs19(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v492 = -1.0555555555555556e+00F;
    float v497 = 1.7752228513927079e-01F;
    float v502 = -1.2820077502191529e-01F;
    float v507 = 4.9321510117355499e-02F;
    float v512 = 5.7611011491005903e-01F;
    float v517 = -7.4996449655536279e-01F;
    float v522 = -1.7385438164530381e-01F;
    float v527 = -2.1729997561977314e+00F;
    float v532 = -1.7021211726914738e+00F;
    float v537 = 4.7087858350625778e-01F;
    float v542 = -2.0239400846888440e+00F;
    float v547 = 1.0551641201664090e-01F;
    float v552 = 2.1294564967054850e+00F;
    float v557 = -7.5087543897371167e-01F;
    float v562 = 1.4812817695157160e-01F;
    float v567 = 8.9900361592528333e-01F;
    float v572 = -6.2148246772602778e-01F;
    float v577 = -7.9869352098712687e-01F;
    float v582 = -4.7339199623771833e-01F;
    float v587 = 2.4216105241892630e-01F;
    float v594 = 5.9368607967505101e-02F;
    float v601 = -1.2578688255176201e-02F;
    float v608 = 4.6789919712328903e-02F;
    float v615 = 9.3750121913782358e-01F;
    float v622 = 5.0111537043352902e-02F;
    float v629 = 9.8761275618117661e-01F;
    float v636 = 1.1745786501205959e+00F;
    float v643 = -1.1114482296234993e+00F;
    float v650 = -2.2860268797440955e+00F;
    float v657 = -2.6420523257930939e-01F;
    float v664 = -2.1981792779352136e+00F;
    float v671 = -1.9339740453559042e+00F;
    float v678 = 7.4825847091254893e-01F;
    float v685 = 4.7820835642768872e-01F;
    float v692 = -2.7005011448486022e-01F;
    float v699 = 3.4642356159542270e-01F;
    float v706 = 8.3485429360688279e-01F;
    float v713 = 3.9375928506743518e-01F;
    const float32x2_t *v961 = &v5[v0];
    float32x2_t *v1184 = &v6[v2];
    int64_t v33 = v0 * 18;
    int64_t v55 = v10 * 17;
    int64_t v61 = v0 * 2;
    int64_t v75 = v0 * 17;
    int64_t v90 = v10 * 16;
    int64_t v103 = v0 * 4;
    int64_t v117 = v0 * 15;
    int64_t v132 = v10 * 3;
    int64_t v139 = v10 * 14;
    int64_t v145 = v0 * 8;
    int64_t v159 = v0 * 11;
    int64_t v174 = v10 * 10;
    int64_t v181 = v10 * 7;
    int64_t v187 = v0 * 16;
    int64_t v201 = v0 * 3;
    int64_t v216 = v10 * 15;
    int64_t v223 = v10 * 2;
    int64_t v229 = v0 * 13;
    int64_t v243 = v0 * 6;
    int64_t v258 = v10 * 5;
    int64_t v265 = v10 * 12;
    int64_t v271 = v0 * 7;
    int64_t v285 = v0 * 12;
    int64_t v300 = v10 * 6;
    int64_t v307 = v10 * 11;
    int64_t v313 = v0 * 14;
    int64_t v327 = v0 * 5;
    int64_t v342 = v10 * 4;
    int64_t v349 = v10 * 13;
    int64_t v355 = v0 * 9;
    int64_t v369 = v0 * 10;
    int64_t v384 = v10 * 8;
    int64_t v391 = v10 * 9;
    int64_t v392 = v13 * 18;
    float v590 = v4 * v587;
    float v597 = v4 * v594;
    float v604 = v4 * v601;
    float v611 = v4 * v608;
    float v618 = v4 * v615;
    float v625 = v4 * v622;
    float v632 = v4 * v629;
    float v639 = v4 * v636;
    float v646 = v4 * v643;
    float v653 = v4 * v650;
    float v660 = v4 * v657;
    float v667 = v4 * v664;
    float v674 = v4 * v671;
    float v681 = v4 * v678;
    float v688 = v4 * v685;
    float v695 = v4 * v692;
    float v702 = v4 * v699;
    float v709 = v4 * v706;
    float v716 = v4 * v713;
    int64_t v821 = v2 * 18;
    int64_t v829 = v2 * 2;
    int64_t v837 = v2 * 17;
    int64_t v845 = v2 * 3;
    int64_t v853 = v2 * 16;
    int64_t v861 = v2 * 4;
    int64_t v869 = v2 * 15;
    int64_t v877 = v2 * 5;
    int64_t v885 = v2 * 14;
    int64_t v893 = v2 * 6;
    int64_t v901 = v2 * 13;
    int64_t v909 = v2 * 7;
    int64_t v917 = v2 * 12;
    int64_t v925 = v2 * 8;
    int64_t v933 = v2 * 11;
    int64_t v941 = v2 * 9;
    int64_t v949 = v2 * 10;
    const float32x2_t *v1126 = &v5[0];
    svint64_t v1127 = svindex_s64(0, v1);
    svfloat32_t v1130 = svdup_n_f32(v492);
    svfloat32_t v1131 = svdup_n_f32(v497);
    svfloat32_t v1132 = svdup_n_f32(v502);
    svfloat32_t v1133 = svdup_n_f32(v507);
    svfloat32_t v1134 = svdup_n_f32(v512);
    svfloat32_t v1135 = svdup_n_f32(v517);
    svfloat32_t v1136 = svdup_n_f32(v522);
    svfloat32_t v1137 = svdup_n_f32(v527);
    svfloat32_t v1138 = svdup_n_f32(v532);
    svfloat32_t v1139 = svdup_n_f32(v537);
    svfloat32_t v1140 = svdup_n_f32(v542);
    svfloat32_t v1141 = svdup_n_f32(v547);
    svfloat32_t v1142 = svdup_n_f32(v552);
    svfloat32_t v1143 = svdup_n_f32(v557);
    svfloat32_t v1144 = svdup_n_f32(v562);
    svfloat32_t v1145 = svdup_n_f32(v567);
    svfloat32_t v1146 = svdup_n_f32(v572);
    svfloat32_t v1147 = svdup_n_f32(v577);
    svfloat32_t v1148 = svdup_n_f32(v582);
    float32x2_t *v1175 = &v6[0];
    svint64_t v1338 = svindex_s64(0, v3);
    svfloat32_t v51 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v392]));
    int64_t v57 = v55 + v392;
    int64_t v92 = v90 + v392;
    int64_t v99 = v10 + v392;
    int64_t v134 = v132 + v392;
    int64_t v141 = v139 + v392;
    int64_t v176 = v174 + v392;
    int64_t v183 = v181 + v392;
    int64_t v218 = v216 + v392;
    int64_t v225 = v223 + v392;
    int64_t v260 = v258 + v392;
    int64_t v267 = v265 + v392;
    int64_t v302 = v300 + v392;
    int64_t v309 = v307 + v392;
    int64_t v344 = v342 + v392;
    int64_t v351 = v349 + v392;
    int64_t v386 = v384 + v392;
    int64_t v393 = v391 + v392;
    svfloat32_t v963 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v961), v1127));
    const float32x2_t *v971 = &v5[v33];
    const float32x2_t *v981 = &v5[v61];
    const float32x2_t *v990 = &v5[v75];
    const float32x2_t *v999 = &v5[v103];
    const float32x2_t *v1008 = &v5[v117];
    const float32x2_t *v1017 = &v5[v145];
    const float32x2_t *v1026 = &v5[v159];
    const float32x2_t *v1035 = &v5[v187];
    const float32x2_t *v1044 = &v5[v201];
    const float32x2_t *v1053 = &v5[v229];
    const float32x2_t *v1062 = &v5[v243];
    const float32x2_t *v1071 = &v5[v271];
    const float32x2_t *v1080 = &v5[v285];
    const float32x2_t *v1089 = &v5[v313];
    const float32x2_t *v1098 = &v5[v327];
    const float32x2_t *v1107 = &v5[v355];
    const float32x2_t *v1116 = &v5[v369];
    svfloat32_t v1128 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1126), v1127));
    svfloat32_t v1149 = svdup_n_f32(v590);
    svfloat32_t v1150 = svdup_n_f32(v597);
    svfloat32_t v1151 = svdup_n_f32(v604);
    svfloat32_t v1152 = svdup_n_f32(v611);
    svfloat32_t v1153 = svdup_n_f32(v618);
    svfloat32_t v1154 = svdup_n_f32(v625);
    svfloat32_t v1155 = svdup_n_f32(v632);
    svfloat32_t v1156 = svdup_n_f32(v639);
    svfloat32_t v1157 = svdup_n_f32(v646);
    svfloat32_t v1158 = svdup_n_f32(v653);
    svfloat32_t v1159 = svdup_n_f32(v660);
    svfloat32_t v1160 = svdup_n_f32(v667);
    svfloat32_t v1161 = svdup_n_f32(v674);
    svfloat32_t v1162 = svdup_n_f32(v681);
    svfloat32_t v1163 = svdup_n_f32(v688);
    svfloat32_t v1164 = svdup_n_f32(v695);
    svfloat32_t v1165 = svdup_n_f32(v702);
    svfloat32_t v1166 = svdup_n_f32(v709);
    svfloat32_t v1167 = svdup_n_f32(v716);
    float32x2_t *v1193 = &v6[v821];
    float32x2_t *v1202 = &v6[v829];
    float32x2_t *v1211 = &v6[v837];
    float32x2_t *v1220 = &v6[v845];
    float32x2_t *v1229 = &v6[v853];
    float32x2_t *v1238 = &v6[v861];
    float32x2_t *v1247 = &v6[v869];
    float32x2_t *v1256 = &v6[v877];
    float32x2_t *v1265 = &v6[v885];
    float32x2_t *v1274 = &v6[v893];
    float32x2_t *v1283 = &v6[v901];
    float32x2_t *v1292 = &v6[v909];
    float32x2_t *v1301 = &v6[v917];
    float32x2_t *v1310 = &v6[v925];
    float32x2_t *v1319 = &v6[v933];
    float32x2_t *v1328 = &v6[v941];
    float32x2_t *v1337 = &v6[v949];
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v963, v51, 0),
                     v963, v51, 90);
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v135 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v134]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v177 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v176]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v219 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v218]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v303 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v302]));
    svfloat32_t v310 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v309]));
    svfloat32_t v345 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v344]));
    svfloat32_t v352 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v351]));
    svfloat32_t v387 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v386]));
    svfloat32_t v394 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v393]));
    svfloat32_t v973 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v971), v1127));
    svfloat32_t v983 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v981), v1127));
    svfloat32_t v992 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v990), v1127));
    svfloat32_t v1001 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v999), v1127));
    svfloat32_t v1010 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1008), v1127));
    svfloat32_t v1019 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1017), v1127));
    svfloat32_t v1028 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1026), v1127));
    svfloat32_t v1037 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1035), v1127));
    svfloat32_t v1046 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1044), v1127));
    svfloat32_t v1055 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1053), v1127));
    svfloat32_t v1064 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1062), v1127));
    svfloat32_t v1073 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1071), v1127));
    svfloat32_t v1082 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1080), v1127));
    svfloat32_t v1091 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1089), v1127));
    svfloat32_t v1100 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1098), v1127));
    svfloat32_t v1109 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1107), v1127));
    svfloat32_t v1118 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1116), v1127));
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v973, v58, 0),
                     v973, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v992, v93, 0),
                     v992, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v983, v100, 0),
                     v983, v100, 90);
    svfloat32_t zero136 = svdup_n_f32(0);
    svfloat32_t v136 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero136, v1001, v135, 0), v1001,
        v135, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero143, v1010, v142, 0), v1010,
        v142, 90);
    svfloat32_t zero178 = svdup_n_f32(0);
    svfloat32_t v178 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero178, v1028, v177, 0), v1028,
        v177, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero185, v1019, v184, 0), v1019,
        v184, 90);
    svfloat32_t zero220 = svdup_n_f32(0);
    svfloat32_t v220 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero220, v1037, v219, 0), v1037,
        v219, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero227, v1046, v226, 0), v1046,
        v226, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero262, v1064, v261, 0), v1064,
        v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero269, v1055, v268, 0), v1055,
        v268, 90);
    svfloat32_t zero304 = svdup_n_f32(0);
    svfloat32_t v304 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero304, v1073, v303, 0), v1073,
        v303, 90);
    svfloat32_t zero311 = svdup_n_f32(0);
    svfloat32_t v311 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero311, v1082, v310, 0), v1082,
        v310, 90);
    svfloat32_t zero346 = svdup_n_f32(0);
    svfloat32_t v346 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero346, v1100, v345, 0), v1100,
        v345, 90);
    svfloat32_t zero353 = svdup_n_f32(0);
    svfloat32_t v353 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero353, v1091, v352, 0), v1091,
        v352, 90);
    svfloat32_t zero388 = svdup_n_f32(0);
    svfloat32_t v388 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero388, v1109, v387, 0), v1109,
        v387, 90);
    svfloat32_t zero395 = svdup_n_f32(0);
    svfloat32_t v395 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero395, v1118, v394, 0), v1118,
        v394, 90);
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v398 = svadd_f32_x(svptrue_b32(), v101, v94);
    svfloat32_t v399 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v400 = svadd_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v401 = svsub_f32_x(svptrue_b32(), v136, v143);
    svfloat32_t v402 = svadd_f32_x(svptrue_b32(), v185, v178);
    svfloat32_t v403 = svsub_f32_x(svptrue_b32(), v178, v185);
    svfloat32_t v404 = svadd_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v405 = svsub_f32_x(svptrue_b32(), v220, v227);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v269, v262);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v408 = svadd_f32_x(svptrue_b32(), v304, v311);
    svfloat32_t v409 = svsub_f32_x(svptrue_b32(), v304, v311);
    svfloat32_t v410 = svadd_f32_x(svptrue_b32(), v353, v346);
    svfloat32_t v411 = svsub_f32_x(svptrue_b32(), v346, v353);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v388, v395);
    svfloat32_t v413 = svsub_f32_x(svptrue_b32(), v388, v395);
    svfloat32_t v414 = svsub_f32_x(svptrue_b32(), v396, v408);
    svfloat32_t v415 = svsub_f32_x(svptrue_b32(), v398, v410);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v400, v412);
    svfloat32_t v417 = svsub_f32_x(svptrue_b32(), v402, v408);
    svfloat32_t v418 = svsub_f32_x(svptrue_b32(), v404, v410);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v406, v412);
    svfloat32_t v420 = svadd_f32_x(svptrue_b32(), v396, v402);
    svfloat32_t v422 = svadd_f32_x(svptrue_b32(), v398, v404);
    svfloat32_t v424 = svadd_f32_x(svptrue_b32(), v400, v406);
    svfloat32_t v454 = svsub_f32_x(svptrue_b32(), v397, v409);
    svfloat32_t v455 = svsub_f32_x(svptrue_b32(), v399, v411);
    svfloat32_t v456 = svsub_f32_x(svptrue_b32(), v401, v413);
    svfloat32_t v457 = svsub_f32_x(svptrue_b32(), v403, v409);
    svfloat32_t v458 = svsub_f32_x(svptrue_b32(), v405, v411);
    svfloat32_t v459 = svsub_f32_x(svptrue_b32(), v407, v413);
    svfloat32_t v460 = svadd_f32_x(svptrue_b32(), v397, v403);
    svfloat32_t v462 = svadd_f32_x(svptrue_b32(), v399, v405);
    svfloat32_t v464 = svadd_f32_x(svptrue_b32(), v401, v407);
    svfloat32_t v421 = svadd_f32_x(svptrue_b32(), v420, v408);
    svfloat32_t v423 = svadd_f32_x(svptrue_b32(), v422, v410);
    svfloat32_t v425 = svadd_f32_x(svptrue_b32(), v424, v412);
    svfloat32_t v426 = svadd_f32_x(svptrue_b32(), v414, v416);
    svfloat32_t v427 = svadd_f32_x(svptrue_b32(), v417, v419);
    svfloat32_t v444 = svsub_f32_x(svptrue_b32(), v414, v417);
    svfloat32_t v445 = svsub_f32_x(svptrue_b32(), v416, v419);
    svfloat32_t v461 = svadd_f32_x(svptrue_b32(), v460, v409);
    svfloat32_t v463 = svadd_f32_x(svptrue_b32(), v462, v411);
    svfloat32_t v465 = svadd_f32_x(svptrue_b32(), v464, v413);
    svfloat32_t v466 = svadd_f32_x(svptrue_b32(), v454, v456);
    svfloat32_t v467 = svadd_f32_x(svptrue_b32(), v457, v459);
    svfloat32_t v476 = svsub_f32_x(svptrue_b32(), v454, v457);
    svfloat32_t v477 = svsub_f32_x(svptrue_b32(), v456, v459);
    svfloat32_t zero641 = svdup_n_f32(0);
    svfloat32_t v641 = svcmla_f32_x(pred_full, zero641, v1156, v457, 90);
    svfloat32_t zero662 = svdup_n_f32(0);
    svfloat32_t v662 = svcmla_f32_x(pred_full, zero662, v1159, v459, 90);
    svfloat32_t v428 = svadd_f32_x(svptrue_b32(), v421, v423);
    svfloat32_t v438 = svadd_f32_x(svptrue_b32(), v427, v418);
    svfloat32_t v439 = svadd_f32_x(svptrue_b32(), v426, v415);
    svfloat32_t v441 = svsub_f32_x(svptrue_b32(), v427, v418);
    svfloat32_t v442 = svsub_f32_x(svptrue_b32(), v426, v415);
    svfloat32_t v446 = svsub_f32_x(svptrue_b32(), v414, v445);
    svfloat32_t v448 = svadd_f32_x(svptrue_b32(), v444, v419);
    svfloat32_t v451 = svsub_f32_x(svptrue_b32(), v421, v425);
    svfloat32_t v452 = svsub_f32_x(svptrue_b32(), v423, v425);
    svfloat32_t v468 = svadd_f32_x(svptrue_b32(), v461, v463);
    svfloat32_t v470 = svadd_f32_x(svptrue_b32(), v467, v458);
    svfloat32_t v471 = svadd_f32_x(svptrue_b32(), v466, v455);
    svfloat32_t v473 = svsub_f32_x(svptrue_b32(), v467, v458);
    svfloat32_t v474 = svsub_f32_x(svptrue_b32(), v466, v455);
    svfloat32_t v478 = svsub_f32_x(svptrue_b32(), v454, v477);
    svfloat32_t v480 = svadd_f32_x(svptrue_b32(), v476, v459);
    svfloat32_t v483 = svsub_f32_x(svptrue_b32(), v461, v465);
    svfloat32_t v484 = svsub_f32_x(svptrue_b32(), v463, v465);
    svfloat32_t v429 = svadd_f32_x(svptrue_b32(), v428, v425);
    svfloat32_t v440 = svsub_f32_x(svptrue_b32(), v439, v438);
    svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v442, v441);
    svfloat32_t v447 = svsub_f32_x(svptrue_b32(), v446, v418);
    svfloat32_t v449 = svsub_f32_x(svptrue_b32(), v448, v415);
    svfloat32_t v453 = svadd_f32_x(svptrue_b32(), v451, v452);
    svfloat32_t v469 = svadd_f32_x(svptrue_b32(), v468, v465);
    svfloat32_t v472 = svsub_f32_x(svptrue_b32(), v471, v470);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v474, v473);
    svfloat32_t v479 = svsub_f32_x(svptrue_b32(), v478, v458);
    svfloat32_t v481 = svsub_f32_x(svptrue_b32(), v480, v455);
    svfloat32_t v485 = svadd_f32_x(svptrue_b32(), v483, v484);
    svfloat32_t v505 = svmul_f32_x(svptrue_b32(), v439, v1132);
    svfloat32_t v520 = svmul_f32_x(svptrue_b32(), v442, v1135);
    svfloat32_t zero599 = svdup_n_f32(0);
    svfloat32_t v599 = svcmla_f32_x(pred_full, zero599, v1150, v470, 90);
    svfloat32_t zero620 = svdup_n_f32(0);
    svfloat32_t v620 = svcmla_f32_x(pred_full, zero620, v1153, v473, 90);
    svfloat32_t zero704 = svdup_n_f32(0);
    svfloat32_t v704 = svcmla_f32_x(pred_full, zero704, v1165, v483, 90);
    svfloat32_t zero711 = svdup_n_f32(0);
    svfloat32_t v711 = svcmla_f32_x(pred_full, zero711, v1166, v484, 90);
    svfloat32_t v437 = svadd_f32_x(svptrue_b32(), v1128, v429);
    svfloat32_t v450 = svsub_f32_x(svptrue_b32(), v447, v449);
    svfloat32_t v482 = svsub_f32_x(svptrue_b32(), v479, v481);
    svfloat32_t v510 = svmul_f32_x(svptrue_b32(), v440, v1133);
    svfloat32_t v525 = svmul_f32_x(svptrue_b32(), v443, v1136);
    svfloat32_t v585 = svmul_f32_x(svptrue_b32(), v453, v1148);
    svfloat32_t zero592 = svdup_n_f32(0);
    svfloat32_t v592 = svcmla_f32_x(pred_full, zero592, v1149, v469, 90);
    svfloat32_t zero718 = svdup_n_f32(0);
    svfloat32_t v718 = svcmla_f32_x(pred_full, zero718, v1167, v485, 90);
    svfloat32_t v719 = svmla_f32_x(pred_full, v505, v438, v1131);
    svfloat32_t v720 = svmla_f32_x(pred_full, v520, v441, v1134);
    svfloat32_t v750 = svcmla_f32_x(pred_full, v599, v1151, v471, 90);
    svfloat32_t v751 = svcmla_f32_x(pred_full, v620, v1154, v474, 90);
    svfloat32_t v570 = svmul_f32_x(svptrue_b32(), v450, v1145);
    svfloat32_t zero697 = svdup_n_f32(0);
    svfloat32_t v697 = svcmla_f32_x(pred_full, zero697, v1164, v482, 90);
    svfloat32_t v722 = svadd_f32_x(svptrue_b32(), v719, v720);
    svfloat32_t v723 = svmla_f32_x(pred_full, v510, v438, v1131);
    svfloat32_t v724 = svmla_f32_x(pred_full, v525, v441, v1134);
    svfloat32_t v741 = svsub_f32_x(svptrue_b32(), v719, v720);
    svfloat32_t v743 = svnmls_f32_x(pred_full, v585, v451, v1146);
    svfloat32_t v744 = svnmls_f32_x(pred_full, v585, v452, v1147);
    svfloat32_t v745 = svmla_f32_x(pred_full, v437, v429, v1130);
    svfloat32_t v753 = svadd_f32_x(svptrue_b32(), v750, v751);
    svfloat32_t v754 = svcmla_f32_x(pred_full, v599, v1152, v472, 90);
    svfloat32_t v755 = svcmla_f32_x(pred_full, v620, v1155, v475, 90);
    svfloat32_t v772 = svsub_f32_x(svptrue_b32(), v750, v751);
    svfloat32_t v774 = svsub_f32_x(svptrue_b32(), v704, v718);
    svfloat32_t v775 = svsub_f32_x(svptrue_b32(), v711, v718);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1175), v1338,
                               svreinterpret_f64_f32(v437));
    svfloat32_t v721 = svmla_f32_x(pred_full, v570, v449, v1144);
    svfloat32_t v725 = svmla_f32_x(pred_full, v570, v447, v1143);
    svfloat32_t v726 = svnmls_f32_x(pred_full, v722, v417, v1137);
    svfloat32_t v727 = svadd_f32_x(svptrue_b32(), v723, v724);
    svfloat32_t v733 = svsub_f32_x(svptrue_b32(), v723, v724);
    svfloat32_t v738 = svmla_f32_x(pred_full, v722, v416, v1142);
    svfloat32_t v746 = svadd_f32_x(svptrue_b32(), v745, v743);
    svfloat32_t v747 = svsub_f32_x(svptrue_b32(), v745, v743);
    svfloat32_t v749 = svadd_f32_x(svptrue_b32(), v745, v744);
    svfloat32_t v752 = svcmla_f32_x(pred_full, v697, v1163, v481, 90);
    svfloat32_t v756 = svcmla_f32_x(pred_full, v697, v1162, v479, 90);
    svfloat32_t v757 = svsub_f32_x(svptrue_b32(), v641, v753);
    svfloat32_t v758 = svadd_f32_x(svptrue_b32(), v754, v755);
    svfloat32_t v764 = svsub_f32_x(svptrue_b32(), v754, v755);
    svfloat32_t v769 = svcmla_f32_x(pred_full, v753, v1161, v456, 90);
    svfloat32_t v776 = svadd_f32_x(svptrue_b32(), v592, v774);
    svfloat32_t v777 = svsub_f32_x(svptrue_b32(), v592, v774);
    svfloat32_t v779 = svadd_f32_x(svptrue_b32(), v592, v775);
    svfloat32_t v728 = svnmls_f32_x(pred_full, v725, v419, v1140);
    svfloat32_t v729 = svmla_f32_x(pred_full, v721, v444, v1138);
    svfloat32_t v731 = svmla_f32_x(pred_full, v727, v445, v1141);
    svfloat32_t v734 = svadd_f32_x(svptrue_b32(), v733, v721);
    svfloat32_t v735 = svadd_f32_x(svptrue_b32(), v726, v727);
    svfloat32_t v742 = svadd_f32_x(svptrue_b32(), v741, v725);
    svfloat32_t v748 = svsub_f32_x(svptrue_b32(), v747, v744);
    svfloat32_t v759 = svsub_f32_x(svptrue_b32(), v662, v756);
    svfloat32_t v760 = svcmla_f32_x(pred_full, v752, v1157, v476, 90);
    svfloat32_t v762 = svcmla_f32_x(pred_full, v758, v1160, v477, 90);
    svfloat32_t v765 = svadd_f32_x(svptrue_b32(), v764, v752);
    svfloat32_t v766 = svadd_f32_x(svptrue_b32(), v757, v758);
    svfloat32_t v773 = svadd_f32_x(svptrue_b32(), v772, v756);
    svfloat32_t v778 = svsub_f32_x(svptrue_b32(), v777, v775);
    svfloat32_t v730 = svadd_f32_x(svptrue_b32(), v729, v726);
    svfloat32_t v732 = svadd_f32_x(svptrue_b32(), v731, v728);
    svfloat32_t v736 = svmla_f32_x(pred_full, v735, v414, v1139);
    svfloat32_t v739 = svadd_f32_x(svptrue_b32(), v738, v728);
    svfloat32_t v761 = svadd_f32_x(svptrue_b32(), v760, v757);
    svfloat32_t v763 = svadd_f32_x(svptrue_b32(), v762, v759);
    svfloat32_t v767 = svcmla_f32_x(pred_full, v766, v1158, v454, 90);
    svfloat32_t v770 = svadd_f32_x(svptrue_b32(), v769, v759);
    svfloat32_t v784 = svsub_f32_x(svptrue_b32(), v742, v734);
    svfloat32_t v788 = svsub_f32_x(svptrue_b32(), v749, v742);
    svfloat32_t v791 = svadd_f32_x(svptrue_b32(), v734, v749);
    svfloat32_t v796 = svsub_f32_x(svptrue_b32(), v773, v765);
    svfloat32_t v800 = svsub_f32_x(svptrue_b32(), v773, v779);
    svfloat32_t v803 = svadd_f32_x(svptrue_b32(), v765, v779);
    svfloat32_t v737 = svadd_f32_x(svptrue_b32(), v736, v725);
    svfloat32_t v740 = svadd_f32_x(svptrue_b32(), v739, v721);
    svfloat32_t v768 = svadd_f32_x(svptrue_b32(), v767, v756);
    svfloat32_t v771 = svadd_f32_x(svptrue_b32(), v770, v752);
    svfloat32_t v785 = svadd_f32_x(svptrue_b32(), v784, v749);
    svfloat32_t v789 = svadd_f32_x(svptrue_b32(), v730, v746);
    svfloat32_t v790 = svadd_f32_x(svptrue_b32(), v732, v748);
    svfloat32_t v797 = svadd_f32_x(svptrue_b32(), v796, v779);
    svfloat32_t v801 = svadd_f32_x(svptrue_b32(), v761, v776);
    svfloat32_t v802 = svadd_f32_x(svptrue_b32(), v763, v778);
    svfloat32_t v827 = svsub_f32_x(svptrue_b32(), v791, v803);
    svfloat32_t v835 = svadd_f32_x(svptrue_b32(), v791, v803);
    svfloat32_t v843 = svadd_f32_x(svptrue_b32(), v788, v800);
    svfloat32_t v851 = svsub_f32_x(svptrue_b32(), v788, v800);
    svfloat32_t v780 = svsub_f32_x(svptrue_b32(), v737, v730);
    svfloat32_t v782 = svsub_f32_x(svptrue_b32(), v740, v732);
    svfloat32_t v786 = svsub_f32_x(svptrue_b32(), v746, v737);
    svfloat32_t v787 = svsub_f32_x(svptrue_b32(), v748, v740);
    svfloat32_t v792 = svsub_f32_x(svptrue_b32(), v768, v761);
    svfloat32_t v794 = svsub_f32_x(svptrue_b32(), v771, v763);
    svfloat32_t v798 = svsub_f32_x(svptrue_b32(), v776, v768);
    svfloat32_t v799 = svsub_f32_x(svptrue_b32(), v778, v771);
    svfloat32_t v859 = svadd_f32_x(svptrue_b32(), v790, v802);
    svfloat32_t v867 = svsub_f32_x(svptrue_b32(), v790, v802);
    svfloat32_t v875 = svadd_f32_x(svptrue_b32(), v785, v797);
    svfloat32_t v883 = svsub_f32_x(svptrue_b32(), v785, v797);
    svfloat32_t v923 = svsub_f32_x(svptrue_b32(), v789, v801);
    svfloat32_t v931 = svadd_f32_x(svptrue_b32(), v789, v801);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1202), v1338,
                               svreinterpret_f64_f32(v827));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1211), v1338,
                               svreinterpret_f64_f32(v835));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1220), v1338,
                               svreinterpret_f64_f32(v843));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1229), v1338,
                               svreinterpret_f64_f32(v851));
    svfloat32_t v781 = svadd_f32_x(svptrue_b32(), v780, v746);
    svfloat32_t v783 = svadd_f32_x(svptrue_b32(), v782, v748);
    svfloat32_t v793 = svadd_f32_x(svptrue_b32(), v792, v776);
    svfloat32_t v795 = svadd_f32_x(svptrue_b32(), v794, v778);
    svfloat32_t v891 = svadd_f32_x(svptrue_b32(), v787, v799);
    svfloat32_t v899 = svsub_f32_x(svptrue_b32(), v787, v799);
    svfloat32_t v907 = svadd_f32_x(svptrue_b32(), v786, v798);
    svfloat32_t v915 = svsub_f32_x(svptrue_b32(), v786, v798);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1238), v1338,
                               svreinterpret_f64_f32(v859));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1247), v1338,
                               svreinterpret_f64_f32(v867));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1256), v1338,
                               svreinterpret_f64_f32(v875));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1265), v1338,
                               svreinterpret_f64_f32(v883));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1310), v1338,
                               svreinterpret_f64_f32(v923));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1319), v1338,
                               svreinterpret_f64_f32(v931));
    svfloat32_t v811 = svadd_f32_x(svptrue_b32(), v781, v793);
    svfloat32_t v819 = svsub_f32_x(svptrue_b32(), v781, v793);
    svfloat32_t v939 = svadd_f32_x(svptrue_b32(), v783, v795);
    svfloat32_t v947 = svsub_f32_x(svptrue_b32(), v783, v795);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1274), v1338,
                               svreinterpret_f64_f32(v891));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1283), v1338,
                               svreinterpret_f64_f32(v899));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1292), v1338,
                               svreinterpret_f64_f32(v907));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1301), v1338,
                               svreinterpret_f64_f32(v915));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1184), v1338,
                               svreinterpret_f64_f32(v811));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1193), v1338,
                               svreinterpret_f64_f32(v819));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1328), v1338,
                               svreinterpret_f64_f32(v939));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1337), v1338,
                               svreinterpret_f64_f32(v947));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs20(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v547 = v5[istride];
    float v760 = 1.5388417685876268e+00F;
    float v767 = 5.8778525229247325e-01F;
    float v774 = 3.6327126400268028e-01F;
    float v798 = 1.0000000000000000e+00F;
    float v799 = -1.0000000000000000e+00F;
    float v805 = -1.2500000000000000e+00F;
    float v806 = 1.2500000000000000e+00F;
    float v812 = 5.5901699437494745e-01F;
    float v813 = -5.5901699437494745e-01F;
    float32x2_t v815 = (float32x2_t){v4, v4};
    float v820 = -1.5388417685876268e+00F;
    float v824 = -5.8778525229247325e-01F;
    float v828 = -3.6327126400268028e-01F;
    float32x2_t v584 = vtrn1_f32(v547, v547);
    float32x2_t v585 = vtrn2_f32(v547, v547);
    float32x2_t v609 = v5[0];
    float32x2_t v754 = (float32x2_t){v805, v805};
    float32x2_t v758 = (float32x2_t){v812, v812};
    float32x2_t v762 = (float32x2_t){v760, v820};
    float32x2_t v769 = (float32x2_t){v767, v824};
    float32x2_t v776 = (float32x2_t){v774, v828};
    float32x2_t v800 = (float32x2_t){v798, v799};
    float32x2_t v807 = (float32x2_t){v805, v806};
    float32x2_t v814 = (float32x2_t){v812, v813};
    float32x2_t v821 = (float32x2_t){v820, v820};
    float32x2_t v825 = (float32x2_t){v824, v824};
    float32x2_t v829 = (float32x2_t){v828, v828};
    float32x2_t v20 = v5[istride * 10];
    int64_t v37 = 18 + j * 38;
    float32x2_t v51 = v5[istride * 5];
    float32x2_t v69 = v5[istride * 15];
    int64_t v86 = 8 + j * 38;
    int64_t v99 = 28 + j * 38;
    float32x2_t v113 = v5[istride * 4];
    float32x2_t v131 = v5[istride * 14];
    int64_t v148 = 6 + j * 38;
    int64_t v161 = 26 + j * 38;
    float32x2_t v175 = v5[istride * 9];
    float32x2_t v193 = v5[istride * 19];
    int64_t v210 = 16 + j * 38;
    int64_t v223 = 36 + j * 38;
    float32x2_t v237 = v5[istride * 8];
    float32x2_t v255 = v5[istride * 18];
    int64_t v272 = 14 + j * 38;
    int64_t v285 = 34 + j * 38;
    float32x2_t v299 = v5[istride * 13];
    float32x2_t v317 = v5[istride * 3];
    int64_t v334 = 24 + j * 38;
    int64_t v347 = 4 + j * 38;
    float32x2_t v361 = v5[istride * 12];
    float32x2_t v379 = v5[istride * 2];
    int64_t v396 = 22 + j * 38;
    int64_t v409 = 2 + j * 38;
    float32x2_t v423 = v5[istride * 17];
    float32x2_t v441 = v5[istride * 7];
    int64_t v458 = 32 + j * 38;
    int64_t v471 = 12 + j * 38;
    float32x2_t v485 = v5[istride * 16];
    float32x2_t v503 = v5[istride * 6];
    int64_t v520 = 30 + j * 38;
    int64_t v533 = 10 + j * 38;
    float32x2_t v565 = v5[istride * 11];
    float32x2_t v583 = v7[j * 38];
    int64_t v587 = j * 38 + 1;
    int64_t v595 = 20 + j * 38;
    float32x2_t v764 = vmul_f32(v815, v762);
    float32x2_t v771 = vmul_f32(v815, v769);
    float32x2_t v778 = vmul_f32(v815, v776);
    float32x2_t v802 = vmul_f32(v815, v800);
    float32x2_t v809 = vmul_f32(v815, v807);
    float32x2_t v816 = vmul_f32(v815, v814);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v273 = v7[v272];
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    int64_t v277 = v272 + 1;
    float32x2_t v286 = v7[v285];
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    int64_t v290 = v285 + 1;
    float32x2_t v335 = v7[v334];
    float32x2_t v336 = vtrn1_f32(v299, v299);
    float32x2_t v337 = vtrn2_f32(v299, v299);
    int64_t v339 = v334 + 1;
    float32x2_t v348 = v7[v347];
    float32x2_t v349 = vtrn1_f32(v317, v317);
    float32x2_t v350 = vtrn2_f32(v317, v317);
    int64_t v352 = v347 + 1;
    float32x2_t v397 = v7[v396];
    float32x2_t v398 = vtrn1_f32(v361, v361);
    float32x2_t v399 = vtrn2_f32(v361, v361);
    int64_t v401 = v396 + 1;
    float32x2_t v410 = v7[v409];
    float32x2_t v411 = vtrn1_f32(v379, v379);
    float32x2_t v412 = vtrn2_f32(v379, v379);
    int64_t v414 = v409 + 1;
    float32x2_t v459 = v7[v458];
    float32x2_t v460 = vtrn1_f32(v423, v423);
    float32x2_t v461 = vtrn2_f32(v423, v423);
    int64_t v463 = v458 + 1;
    float32x2_t v472 = v7[v471];
    float32x2_t v473 = vtrn1_f32(v441, v441);
    float32x2_t v474 = vtrn2_f32(v441, v441);
    int64_t v476 = v471 + 1;
    float32x2_t v521 = v7[v520];
    float32x2_t v522 = vtrn1_f32(v485, v485);
    float32x2_t v523 = vtrn2_f32(v485, v485);
    int64_t v525 = v520 + 1;
    float32x2_t v534 = v7[v533];
    float32x2_t v535 = vtrn1_f32(v503, v503);
    float32x2_t v536 = vtrn2_f32(v503, v503);
    int64_t v538 = v533 + 1;
    float32x2_t v588 = v7[v587];
    float32x2_t v589 = vmul_f32(v584, v583);
    float32x2_t v596 = v7[v595];
    float32x2_t v597 = vtrn1_f32(v565, v565);
    float32x2_t v598 = vtrn2_f32(v565, v565);
    int64_t v600 = v595 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vmul_f32(v336, v335);
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vmul_f32(v349, v348);
    float32x2_t v402 = v7[v401];
    float32x2_t v403 = vmul_f32(v398, v397);
    float32x2_t v415 = v7[v414];
    float32x2_t v416 = vmul_f32(v411, v410);
    float32x2_t v464 = v7[v463];
    float32x2_t v465 = vmul_f32(v460, v459);
    float32x2_t v477 = v7[v476];
    float32x2_t v478 = vmul_f32(v473, v472);
    float32x2_t v526 = v7[v525];
    float32x2_t v527 = vmul_f32(v522, v521);
    float32x2_t v539 = v7[v538];
    float32x2_t v540 = vmul_f32(v535, v534);
    float32x2_t v601 = v7[v600];
    float32x2_t v602 = vmul_f32(v597, v596);
    float32x2_t v591 = vfma_f32(v589, v585, v588);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v343 = vfma_f32(v341, v337, v340);
    float32x2_t v356 = vfma_f32(v354, v350, v353);
    float32x2_t v405 = vfma_f32(v403, v399, v402);
    float32x2_t v418 = vfma_f32(v416, v412, v415);
    float32x2_t v467 = vfma_f32(v465, v461, v464);
    float32x2_t v480 = vfma_f32(v478, v474, v477);
    float32x2_t v529 = vfma_f32(v527, v523, v526);
    float32x2_t v542 = vfma_f32(v540, v536, v539);
    float32x2_t v604 = vfma_f32(v602, v598, v601);
    float32x2_t v610 = vadd_f32(v609, v46);
    float32x2_t v611 = vsub_f32(v609, v46);
    float32x2_t v612 = vadd_f32(v95, v108);
    float32x2_t v613 = vsub_f32(v95, v108);
    float32x2_t v616 = vadd_f32(v157, v170);
    float32x2_t v617 = vsub_f32(v157, v170);
    float32x2_t v618 = vadd_f32(v219, v232);
    float32x2_t v619 = vsub_f32(v219, v232);
    float32x2_t v622 = vadd_f32(v281, v294);
    float32x2_t v623 = vsub_f32(v281, v294);
    float32x2_t v624 = vadd_f32(v343, v356);
    float32x2_t v625 = vsub_f32(v343, v356);
    float32x2_t v628 = vadd_f32(v405, v418);
    float32x2_t v629 = vsub_f32(v405, v418);
    float32x2_t v630 = vadd_f32(v467, v480);
    float32x2_t v631 = vsub_f32(v467, v480);
    float32x2_t v634 = vadd_f32(v529, v542);
    float32x2_t v635 = vsub_f32(v529, v542);
    float32x2_t v636 = vadd_f32(v591, v604);
    float32x2_t v637 = vsub_f32(v591, v604);
    float32x2_t v614 = vadd_f32(v610, v612);
    float32x2_t v615 = vsub_f32(v610, v612);
    float32x2_t v620 = vadd_f32(v616, v618);
    float32x2_t v621 = vsub_f32(v616, v618);
    float32x2_t v626 = vadd_f32(v622, v624);
    float32x2_t v627 = vsub_f32(v622, v624);
    float32x2_t v632 = vadd_f32(v628, v630);
    float32x2_t v633 = vsub_f32(v628, v630);
    float32x2_t v638 = vadd_f32(v634, v636);
    float32x2_t v639 = vsub_f32(v634, v636);
    float32x2_t v740 = vadd_f32(v617, v635);
    float32x2_t v741 = vsub_f32(v617, v635);
    float32x2_t v742 = vadd_f32(v629, v623);
    float32x2_t v743 = vsub_f32(v629, v623);
    float32x2_t v790 = vadd_f32(v619, v637);
    float32x2_t v791 = vsub_f32(v619, v637);
    float32x2_t v792 = vadd_f32(v631, v625);
    float32x2_t v793 = vsub_f32(v631, v625);
    float32x2_t v640 = vadd_f32(v620, v638);
    float32x2_t v641 = vsub_f32(v620, v638);
    float32x2_t v642 = vadd_f32(v632, v626);
    float32x2_t v643 = vsub_f32(v632, v626);
    float32x2_t v690 = vadd_f32(v621, v639);
    float32x2_t v691 = vsub_f32(v621, v639);
    float32x2_t v692 = vadd_f32(v633, v627);
    float32x2_t v693 = vsub_f32(v633, v627);
    float32x2_t v744 = vadd_f32(v740, v742);
    float32x2_t v745 = vsub_f32(v740, v742);
    float32x2_t v746 = vadd_f32(v741, v743);
    float32x2_t v765 = vrev64_f32(v741);
    float32x2_t v779 = vrev64_f32(v743);
    float32x2_t v794 = vadd_f32(v790, v792);
    float32x2_t v795 = vsub_f32(v790, v792);
    float32x2_t v796 = vadd_f32(v791, v793);
    float32x2_t v822 = vmul_f32(v791, v821);
    float32x2_t v830 = vmul_f32(v793, v829);
    float32x2_t v644 = vadd_f32(v640, v642);
    float32x2_t v645 = vsub_f32(v640, v642);
    float32x2_t v646 = vadd_f32(v641, v643);
    float32x2_t v665 = vrev64_f32(v641);
    float32x2_t v679 = vrev64_f32(v643);
    float32x2_t v694 = vadd_f32(v690, v692);
    float32x2_t v695 = vsub_f32(v690, v692);
    float32x2_t v696 = vadd_f32(v691, v693);
    float32x2_t v715 = vrev64_f32(v691);
    float32x2_t v729 = vrev64_f32(v693);
    float32x2_t v747 = vadd_f32(v744, v611);
    float32x2_t v755 = vmul_f32(v744, v754);
    float32x2_t v759 = vmul_f32(v745, v758);
    float32x2_t v766 = vmul_f32(v765, v764);
    float32x2_t v772 = vrev64_f32(v746);
    float32x2_t v780 = vmul_f32(v779, v778);
    float32x2_t v797 = vadd_f32(v794, v613);
    float32x2_t v810 = vrev64_f32(v794);
    float32x2_t v817 = vrev64_f32(v795);
    float32x2_t v826 = vmul_f32(v796, v825);
    float32x2_t v647 = vadd_f32(v644, v614);
    float32x2_t v655 = vmul_f32(v644, v754);
    float32x2_t v659 = vmul_f32(v645, v758);
    float32x2_t v666 = vmul_f32(v665, v764);
    float32x2_t v672 = vrev64_f32(v646);
    float32x2_t v680 = vmul_f32(v679, v778);
    float32x2_t v697 = vadd_f32(v694, v615);
    float32x2_t v705 = vmul_f32(v694, v754);
    float32x2_t v709 = vmul_f32(v695, v758);
    float32x2_t v716 = vmul_f32(v715, v764);
    float32x2_t v722 = vrev64_f32(v696);
    float32x2_t v730 = vmul_f32(v729, v778);
    float32x2_t v773 = vmul_f32(v772, v771);
    float32x2_t v781 = vadd_f32(v747, v755);
    float32x2_t v803 = vrev64_f32(v797);
    float32x2_t v811 = vmul_f32(v810, v809);
    float32x2_t v818 = vmul_f32(v817, v816);
    float32x2_t v834 = vsub_f32(v822, v826);
    float32x2_t v835 = vadd_f32(v826, v830);
    float32x2_t v673 = vmul_f32(v672, v771);
    float32x2_t v681 = vadd_f32(v647, v655);
    float32x2_t v723 = vmul_f32(v722, v771);
    float32x2_t v731 = vadd_f32(v697, v705);
    float32x2_t v782 = vadd_f32(v781, v759);
    float32x2_t v783 = vsub_f32(v781, v759);
    float32x2_t v784 = vsub_f32(v766, v773);
    float32x2_t v785 = vadd_f32(v773, v780);
    float32x2_t v804 = vmul_f32(v803, v802);
    v6[0] = v647;
    v6[ostride * 10] = v697;
    float32x2_t v682 = vadd_f32(v681, v659);
    float32x2_t v683 = vsub_f32(v681, v659);
    float32x2_t v684 = vsub_f32(v666, v673);
    float32x2_t v685 = vadd_f32(v673, v680);
    float32x2_t v732 = vadd_f32(v731, v709);
    float32x2_t v733 = vsub_f32(v731, v709);
    float32x2_t v734 = vsub_f32(v716, v723);
    float32x2_t v735 = vadd_f32(v723, v730);
    float32x2_t v786 = vadd_f32(v782, v784);
    float32x2_t v787 = vsub_f32(v782, v784);
    float32x2_t v788 = vadd_f32(v783, v785);
    float32x2_t v789 = vsub_f32(v783, v785);
    float32x2_t v831 = vadd_f32(v804, v811);
    float32x2_t v840 = vadd_f32(v747, v804);
    float32x2_t v841 = vsub_f32(v747, v804);
    float32x2_t v686 = vadd_f32(v682, v684);
    float32x2_t v687 = vsub_f32(v682, v684);
    float32x2_t v688 = vadd_f32(v683, v685);
    float32x2_t v689 = vsub_f32(v683, v685);
    float32x2_t v736 = vadd_f32(v732, v734);
    float32x2_t v737 = vsub_f32(v732, v734);
    float32x2_t v738 = vadd_f32(v733, v735);
    float32x2_t v739 = vsub_f32(v733, v735);
    float32x2_t v832 = vadd_f32(v831, v818);
    float32x2_t v833 = vsub_f32(v831, v818);
    v6[ostride * 5] = v841;
    v6[ostride * 15] = v840;
    float32x2_t v836 = vadd_f32(v832, v834);
    float32x2_t v837 = vsub_f32(v832, v834);
    float32x2_t v838 = vadd_f32(v833, v835);
    float32x2_t v839 = vsub_f32(v833, v835);
    v6[ostride * 16] = v687;
    v6[ostride * 6] = v737;
    v6[ostride * 12] = v689;
    v6[ostride * 2] = v739;
    v6[ostride * 8] = v688;
    v6[ostride * 18] = v738;
    v6[ostride * 4] = v686;
    v6[ostride * 14] = v736;
    float32x2_t v862 = vadd_f32(v787, v837);
    float32x2_t v863 = vsub_f32(v787, v837);
    float32x2_t v884 = vadd_f32(v789, v839);
    float32x2_t v885 = vsub_f32(v789, v839);
    float32x2_t v906 = vadd_f32(v788, v838);
    float32x2_t v907 = vsub_f32(v788, v838);
    float32x2_t v928 = vadd_f32(v786, v836);
    float32x2_t v929 = vsub_f32(v786, v836);
    v6[ostride] = v863;
    v6[ostride * 11] = v862;
    v6[ostride * 17] = v885;
    v6[ostride * 7] = v884;
    v6[ostride * 13] = v907;
    v6[ostride * 3] = v906;
    v6[ostride * 9] = v929;
    v6[ostride * 19] = v928;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs20(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v574 = -1.2500000000000000e+00F;
    float v579 = 5.5901699437494745e-01F;
    float v622 = -1.0000000000000000e+00F;
    float v629 = 1.2500000000000000e+00F;
    float v636 = -5.5901699437494745e-01F;
    float v643 = -1.5388417685876268e+00F;
    float v648 = -5.8778525229247325e-01F;
    float v653 = -3.6327126400268028e-01F;
    const float32x2_t *v975 = &v5[v0];
    float32x2_t *v1075 = &v6[v2];
    int64_t v19 = v0 * 10;
    int64_t v34 = v10 * 9;
    int64_t v40 = v0 * 5;
    int64_t v54 = v0 * 15;
    int64_t v69 = v10 * 4;
    int64_t v76 = v10 * 14;
    int64_t v82 = v0 * 4;
    int64_t v96 = v0 * 14;
    int64_t v111 = v10 * 3;
    int64_t v118 = v10 * 13;
    int64_t v124 = v0 * 9;
    int64_t v138 = v0 * 19;
    int64_t v153 = v10 * 8;
    int64_t v160 = v10 * 18;
    int64_t v166 = v0 * 8;
    int64_t v180 = v0 * 18;
    int64_t v195 = v10 * 7;
    int64_t v202 = v10 * 17;
    int64_t v208 = v0 * 13;
    int64_t v222 = v0 * 3;
    int64_t v237 = v10 * 12;
    int64_t v244 = v10 * 2;
    int64_t v250 = v0 * 12;
    int64_t v264 = v0 * 2;
    int64_t v279 = v10 * 11;
    int64_t v292 = v0 * 17;
    int64_t v306 = v0 * 7;
    int64_t v321 = v10 * 16;
    int64_t v328 = v10 * 6;
    int64_t v334 = v0 * 16;
    int64_t v348 = v0 * 6;
    int64_t v363 = v10 * 15;
    int64_t v370 = v10 * 5;
    int64_t v390 = v0 * 11;
    int64_t v412 = v10 * 10;
    int64_t v413 = v13 * 19;
    float v587 = v4 * v643;
    float v594 = v4 * v648;
    float v601 = v4 * v653;
    float v625 = v4 * v622;
    float v632 = v4 * v629;
    float v639 = v4 * v636;
    int64_t v676 = v2 * 5;
    int64_t v683 = v2 * 10;
    int64_t v690 = v2 * 15;
    int64_t v699 = v2 * 16;
    int64_t v713 = v2 * 6;
    int64_t v720 = v2 * 11;
    int64_t v729 = v2 * 12;
    int64_t v736 = v2 * 17;
    int64_t v743 = v2 * 2;
    int64_t v750 = v2 * 7;
    int64_t v759 = v2 * 8;
    int64_t v766 = v2 * 13;
    int64_t v773 = v2 * 18;
    int64_t v780 = v2 * 3;
    int64_t v789 = v2 * 4;
    int64_t v796 = v2 * 9;
    int64_t v803 = v2 * 14;
    int64_t v810 = v2 * 19;
    const float32x2_t *v996 = &v5[0];
    svint64_t v997 = svindex_s64(0, v1);
    svfloat32_t v1012 = svdup_n_f32(v574);
    svfloat32_t v1013 = svdup_n_f32(v579);
    svfloat32_t v1020 = svdup_n_f32(v643);
    svfloat32_t v1021 = svdup_n_f32(v648);
    svfloat32_t v1022 = svdup_n_f32(v653);
    float32x2_t *v1030 = &v6[0];
    svint64_t v1202 = svindex_s64(0, v3);
    int64_t v36 = v34 + v413;
    int64_t v71 = v69 + v413;
    int64_t v78 = v76 + v413;
    int64_t v113 = v111 + v413;
    int64_t v120 = v118 + v413;
    int64_t v155 = v153 + v413;
    int64_t v162 = v160 + v413;
    int64_t v197 = v195 + v413;
    int64_t v204 = v202 + v413;
    int64_t v239 = v237 + v413;
    int64_t v246 = v244 + v413;
    int64_t v281 = v279 + v413;
    int64_t v288 = v10 + v413;
    int64_t v323 = v321 + v413;
    int64_t v330 = v328 + v413;
    int64_t v365 = v363 + v413;
    int64_t v372 = v370 + v413;
    svfloat32_t v408 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v413]));
    int64_t v414 = v412 + v413;
    const float32x2_t *v822 = &v5[v19];
    const float32x2_t *v831 = &v5[v40];
    const float32x2_t *v840 = &v5[v54];
    const float32x2_t *v849 = &v5[v82];
    const float32x2_t *v858 = &v5[v96];
    const float32x2_t *v867 = &v5[v124];
    const float32x2_t *v876 = &v5[v138];
    const float32x2_t *v885 = &v5[v166];
    const float32x2_t *v894 = &v5[v180];
    const float32x2_t *v903 = &v5[v208];
    const float32x2_t *v912 = &v5[v222];
    const float32x2_t *v921 = &v5[v250];
    const float32x2_t *v930 = &v5[v264];
    const float32x2_t *v939 = &v5[v292];
    const float32x2_t *v948 = &v5[v306];
    const float32x2_t *v957 = &v5[v334];
    const float32x2_t *v966 = &v5[v348];
    svfloat32_t v977 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v975), v997));
    const float32x2_t *v985 = &v5[v390];
    svfloat32_t v998 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v996), v997));
    svfloat32_t v1014 = svdup_n_f32(v587);
    svfloat32_t v1015 = svdup_n_f32(v594);
    svfloat32_t v1016 = svdup_n_f32(v601);
    svfloat32_t v1017 = svdup_n_f32(v625);
    svfloat32_t v1018 = svdup_n_f32(v632);
    svfloat32_t v1019 = svdup_n_f32(v639);
    float32x2_t *v1039 = &v6[v676];
    float32x2_t *v1048 = &v6[v683];
    float32x2_t *v1057 = &v6[v690];
    float32x2_t *v1066 = &v6[v699];
    float32x2_t *v1084 = &v6[v713];
    float32x2_t *v1093 = &v6[v720];
    float32x2_t *v1102 = &v6[v729];
    float32x2_t *v1111 = &v6[v736];
    float32x2_t *v1120 = &v6[v743];
    float32x2_t *v1129 = &v6[v750];
    float32x2_t *v1138 = &v6[v759];
    float32x2_t *v1147 = &v6[v766];
    float32x2_t *v1156 = &v6[v773];
    float32x2_t *v1165 = &v6[v780];
    float32x2_t *v1174 = &v6[v789];
    float32x2_t *v1183 = &v6[v796];
    float32x2_t *v1192 = &v6[v803];
    float32x2_t *v1201 = &v6[v810];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v246]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v288]));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v331 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v330]));
    svfloat32_t v366 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v365]));
    svfloat32_t v373 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v372]));
    svfloat32_t zero409 = svdup_n_f32(0);
    svfloat32_t v409 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero409, v977, v408, 0),
                     v977, v408, 90);
    svfloat32_t v415 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v414]));
    svfloat32_t v824 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v822), v997));
    svfloat32_t v833 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v831), v997));
    svfloat32_t v842 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v840), v997));
    svfloat32_t v851 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v849), v997));
    svfloat32_t v860 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v858), v997));
    svfloat32_t v869 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v867), v997));
    svfloat32_t v878 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v876), v997));
    svfloat32_t v887 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v885), v997));
    svfloat32_t v896 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v894), v997));
    svfloat32_t v905 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v903), v997));
    svfloat32_t v914 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v912), v997));
    svfloat32_t v923 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v921), v997));
    svfloat32_t v932 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v930), v997));
    svfloat32_t v941 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v939), v997));
    svfloat32_t v950 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v948), v997));
    svfloat32_t v959 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v957), v997));
    svfloat32_t v968 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v966), v997));
    svfloat32_t v987 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v985), v997));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v824, v37, 0),
                     v824, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v833, v72, 0),
                     v833, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v842, v79, 0),
                     v842, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero115, v851, v114, 0),
                     v851, v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero122, v860, v121, 0),
                     v860, v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v869, v156, 0),
                     v869, v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero164, v878, v163, 0),
                     v878, v163, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero199, v887, v198, 0),
                     v887, v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v896, v205, 0),
                     v896, v205, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero241, v905, v240, 0),
                     v905, v240, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero248, v914, v247, 0),
                     v914, v247, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero283, v923, v282, 0),
                     v923, v282, 90);
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero290, v932, v289, 0),
                     v932, v289, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero325, v941, v324, 0),
                     v941, v324, 90);
    svfloat32_t zero332 = svdup_n_f32(0);
    svfloat32_t v332 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero332, v950, v331, 0),
                     v950, v331, 90);
    svfloat32_t zero367 = svdup_n_f32(0);
    svfloat32_t v367 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero367, v959, v366, 0),
                     v959, v366, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero374, v968, v373, 0),
                     v968, v373, 90);
    svfloat32_t zero416 = svdup_n_f32(0);
    svfloat32_t v416 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero416, v987, v415, 0),
                     v987, v415, 90);
    svfloat32_t v424 = svadd_f32_x(svptrue_b32(), v998, v38);
    svfloat32_t v425 = svsub_f32_x(svptrue_b32(), v998, v38);
    svfloat32_t v426 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v430 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v431 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v432 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v433 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v437 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v438 = svadd_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v439 = svsub_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v442 = svadd_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v443 = svsub_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v444 = svadd_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v445 = svsub_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v448 = svadd_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v449 = svsub_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v450 = svadd_f32_x(svptrue_b32(), v409, v416);
    svfloat32_t v451 = svsub_f32_x(svptrue_b32(), v409, v416);
    svfloat32_t v428 = svadd_f32_x(svptrue_b32(), v424, v426);
    svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v424, v426);
    svfloat32_t v434 = svadd_f32_x(svptrue_b32(), v430, v432);
    svfloat32_t v435 = svsub_f32_x(svptrue_b32(), v430, v432);
    svfloat32_t v440 = svadd_f32_x(svptrue_b32(), v436, v438);
    svfloat32_t v441 = svsub_f32_x(svptrue_b32(), v436, v438);
    svfloat32_t v446 = svadd_f32_x(svptrue_b32(), v442, v444);
    svfloat32_t v447 = svsub_f32_x(svptrue_b32(), v442, v444);
    svfloat32_t v452 = svadd_f32_x(svptrue_b32(), v448, v450);
    svfloat32_t v453 = svsub_f32_x(svptrue_b32(), v448, v450);
    svfloat32_t v560 = svadd_f32_x(svptrue_b32(), v431, v449);
    svfloat32_t v561 = svsub_f32_x(svptrue_b32(), v431, v449);
    svfloat32_t v562 = svadd_f32_x(svptrue_b32(), v443, v437);
    svfloat32_t v563 = svsub_f32_x(svptrue_b32(), v443, v437);
    svfloat32_t v613 = svadd_f32_x(svptrue_b32(), v433, v451);
    svfloat32_t v614 = svsub_f32_x(svptrue_b32(), v433, v451);
    svfloat32_t v615 = svadd_f32_x(svptrue_b32(), v445, v439);
    svfloat32_t v616 = svsub_f32_x(svptrue_b32(), v445, v439);
    svfloat32_t v454 = svadd_f32_x(svptrue_b32(), v434, v452);
    svfloat32_t v455 = svsub_f32_x(svptrue_b32(), v434, v452);
    svfloat32_t v456 = svadd_f32_x(svptrue_b32(), v446, v440);
    svfloat32_t v457 = svsub_f32_x(svptrue_b32(), v446, v440);
    svfloat32_t v507 = svadd_f32_x(svptrue_b32(), v435, v453);
    svfloat32_t v508 = svsub_f32_x(svptrue_b32(), v435, v453);
    svfloat32_t v509 = svadd_f32_x(svptrue_b32(), v447, v441);
    svfloat32_t v510 = svsub_f32_x(svptrue_b32(), v447, v441);
    svfloat32_t v564 = svadd_f32_x(svptrue_b32(), v560, v562);
    svfloat32_t v565 = svsub_f32_x(svptrue_b32(), v560, v562);
    svfloat32_t v566 = svadd_f32_x(svptrue_b32(), v561, v563);
    svfloat32_t zero589 = svdup_n_f32(0);
    svfloat32_t v589 = svcmla_f32_x(pred_full, zero589, v1014, v561, 90);
    svfloat32_t v617 = svadd_f32_x(svptrue_b32(), v613, v615);
    svfloat32_t v618 = svsub_f32_x(svptrue_b32(), v613, v615);
    svfloat32_t v619 = svadd_f32_x(svptrue_b32(), v614, v616);
    svfloat32_t v656 = svmul_f32_x(svptrue_b32(), v616, v1022);
    svfloat32_t v458 = svadd_f32_x(svptrue_b32(), v454, v456);
    svfloat32_t v459 = svsub_f32_x(svptrue_b32(), v454, v456);
    svfloat32_t v460 = svadd_f32_x(svptrue_b32(), v455, v457);
    svfloat32_t zero483 = svdup_n_f32(0);
    svfloat32_t v483 = svcmla_f32_x(pred_full, zero483, v1014, v455, 90);
    svfloat32_t v511 = svadd_f32_x(svptrue_b32(), v507, v509);
    svfloat32_t v512 = svsub_f32_x(svptrue_b32(), v507, v509);
    svfloat32_t v513 = svadd_f32_x(svptrue_b32(), v508, v510);
    svfloat32_t zero536 = svdup_n_f32(0);
    svfloat32_t v536 = svcmla_f32_x(pred_full, zero536, v1014, v508, 90);
    svfloat32_t v567 = svadd_f32_x(svptrue_b32(), v564, v425);
    svfloat32_t zero596 = svdup_n_f32(0);
    svfloat32_t v596 = svcmla_f32_x(pred_full, zero596, v1015, v566, 90);
    svfloat32_t v620 = svadd_f32_x(svptrue_b32(), v617, v427);
    svfloat32_t zero641 = svdup_n_f32(0);
    svfloat32_t v641 = svcmla_f32_x(pred_full, zero641, v1019, v618, 90);
    svfloat32_t v651 = svmul_f32_x(svptrue_b32(), v619, v1021);
    svfloat32_t v461 = svadd_f32_x(svptrue_b32(), v458, v428);
    svfloat32_t zero490 = svdup_n_f32(0);
    svfloat32_t v490 = svcmla_f32_x(pred_full, zero490, v1015, v460, 90);
    svfloat32_t v514 = svadd_f32_x(svptrue_b32(), v511, v429);
    svfloat32_t zero543 = svdup_n_f32(0);
    svfloat32_t v543 = svcmla_f32_x(pred_full, zero543, v1015, v513, 90);
    svfloat32_t v604 = svmla_f32_x(pred_full, v567, v564, v1012);
    svfloat32_t v607 = svsub_f32_x(svptrue_b32(), v589, v596);
    svfloat32_t v608 = svcmla_f32_x(pred_full, v596, v1016, v563, 90);
    svfloat32_t zero627 = svdup_n_f32(0);
    svfloat32_t v627 = svcmla_f32_x(pred_full, zero627, v1017, v620, 90);
    svfloat32_t v660 = svnmls_f32_x(pred_full, v651, v614, v1020);
    svfloat32_t v661 = svmla_f32_x(pred_full, v656, v619, v1021);
    svfloat32_t v498 = svmla_f32_x(pred_full, v461, v458, v1012);
    svfloat32_t v501 = svsub_f32_x(svptrue_b32(), v483, v490);
    svfloat32_t v502 = svcmla_f32_x(pred_full, v490, v1016, v457, 90);
    svfloat32_t v551 = svmla_f32_x(pred_full, v514, v511, v1012);
    svfloat32_t v554 = svsub_f32_x(svptrue_b32(), v536, v543);
    svfloat32_t v555 = svcmla_f32_x(pred_full, v543, v1016, v510, 90);
    svfloat32_t v605 = svmla_f32_x(pred_full, v604, v565, v1013);
    svfloat32_t v606 = svmls_f32_x(pred_full, v604, v565, v1013);
    svfloat32_t v657 = svcmla_f32_x(pred_full, v627, v1018, v617, 90);
    svfloat32_t v666 = svadd_f32_x(svptrue_b32(), v567, v627);
    svfloat32_t v667 = svsub_f32_x(svptrue_b32(), v567, v627);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1030), v1202,
                               svreinterpret_f64_f32(v461));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1048), v1202,
                               svreinterpret_f64_f32(v514));
    svfloat32_t v499 = svmla_f32_x(pred_full, v498, v459, v1013);
    svfloat32_t v500 = svmls_f32_x(pred_full, v498, v459, v1013);
    svfloat32_t v552 = svmla_f32_x(pred_full, v551, v512, v1013);
    svfloat32_t v553 = svmls_f32_x(pred_full, v551, v512, v1013);
    svfloat32_t v609 = svadd_f32_x(svptrue_b32(), v605, v607);
    svfloat32_t v610 = svsub_f32_x(svptrue_b32(), v605, v607);
    svfloat32_t v611 = svadd_f32_x(svptrue_b32(), v606, v608);
    svfloat32_t v612 = svsub_f32_x(svptrue_b32(), v606, v608);
    svfloat32_t v658 = svadd_f32_x(svptrue_b32(), v657, v641);
    svfloat32_t v659 = svsub_f32_x(svptrue_b32(), v657, v641);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1039), v1202,
                               svreinterpret_f64_f32(v667));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1057), v1202,
                               svreinterpret_f64_f32(v666));
    svfloat32_t v503 = svadd_f32_x(svptrue_b32(), v499, v501);
    svfloat32_t v504 = svsub_f32_x(svptrue_b32(), v499, v501);
    svfloat32_t v505 = svadd_f32_x(svptrue_b32(), v500, v502);
    svfloat32_t v506 = svsub_f32_x(svptrue_b32(), v500, v502);
    svfloat32_t v556 = svadd_f32_x(svptrue_b32(), v552, v554);
    svfloat32_t v557 = svsub_f32_x(svptrue_b32(), v552, v554);
    svfloat32_t v558 = svadd_f32_x(svptrue_b32(), v553, v555);
    svfloat32_t v559 = svsub_f32_x(svptrue_b32(), v553, v555);
    svfloat32_t v662 = svadd_f32_x(svptrue_b32(), v658, v660);
    svfloat32_t v663 = svsub_f32_x(svptrue_b32(), v658, v660);
    svfloat32_t v664 = svadd_f32_x(svptrue_b32(), v659, v661);
    svfloat32_t v665 = svsub_f32_x(svptrue_b32(), v659, v661);
    svfloat32_t v696 = svadd_f32_x(svptrue_b32(), v610, v663);
    svfloat32_t v697 = svsub_f32_x(svptrue_b32(), v610, v663);
    svfloat32_t v726 = svadd_f32_x(svptrue_b32(), v612, v665);
    svfloat32_t v727 = svsub_f32_x(svptrue_b32(), v612, v665);
    svfloat32_t v756 = svadd_f32_x(svptrue_b32(), v611, v664);
    svfloat32_t v757 = svsub_f32_x(svptrue_b32(), v611, v664);
    svfloat32_t v786 = svadd_f32_x(svptrue_b32(), v609, v662);
    svfloat32_t v787 = svsub_f32_x(svptrue_b32(), v609, v662);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1066), v1202,
                               svreinterpret_f64_f32(v504));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1084), v1202,
                               svreinterpret_f64_f32(v557));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1102), v1202,
                               svreinterpret_f64_f32(v506));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1120), v1202,
                               svreinterpret_f64_f32(v559));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1138), v1202,
                               svreinterpret_f64_f32(v505));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1156), v1202,
                               svreinterpret_f64_f32(v558));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1174), v1202,
                               svreinterpret_f64_f32(v503));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1192), v1202,
                               svreinterpret_f64_f32(v556));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1075), v1202,
                               svreinterpret_f64_f32(v697));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1093), v1202,
                               svreinterpret_f64_f32(v696));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1111), v1202,
                               svreinterpret_f64_f32(v727));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1129), v1202,
                               svreinterpret_f64_f32(v726));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1147), v1202,
                               svreinterpret_f64_f32(v757));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1165), v1202,
                               svreinterpret_f64_f32(v756));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1183), v1202,
                               svreinterpret_f64_f32(v787));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1201), v1202,
                               svreinterpret_f64_f32(v786));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs21(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v402 = v5[istride];
    float v606 = -1.1666666666666665e+00F;
    float v610 = 7.9015646852540022e-01F;
    float v614 = 5.5854267289647742e-02F;
    float v618 = 7.3430220123575241e-01F;
    float v621 = 4.4095855184409838e-01F;
    float v622 = -4.4095855184409838e-01F;
    float v628 = 3.4087293062393137e-01F;
    float v629 = -3.4087293062393137e-01F;
    float v635 = -5.3396936033772524e-01F;
    float v636 = 5.3396936033772524e-01F;
    float v642 = 8.7484229096165667e-01F;
    float v643 = -8.7484229096165667e-01F;
    float v686 = -1.4999999999999998e+00F;
    float v690 = 1.7499999999999996e+00F;
    float v694 = -1.1852347027881001e+00F;
    float v698 = -8.3781400934471603e-02F;
    float v702 = -1.1014533018536286e+00F;
    float v705 = -6.6143782776614746e-01F;
    float v706 = 6.6143782776614746e-01F;
    float v712 = -5.1130939593589697e-01F;
    float v713 = 5.1130939593589697e-01F;
    float v719 = 8.0095404050658769e-01F;
    float v720 = -8.0095404050658769e-01F;
    float v726 = -1.3122634364424848e+00F;
    float v727 = 1.3122634364424848e+00F;
    float v769 = 8.6602540378443871e-01F;
    float v770 = -8.6602540378443871e-01F;
    float v776 = -1.0103629710818451e+00F;
    float v777 = 1.0103629710818451e+00F;
    float v783 = 6.8429557470759583e-01F;
    float v784 = -6.8429557470759583e-01F;
    float v790 = 4.8371214382601155e-02F;
    float v791 = -4.8371214382601155e-02F;
    float v797 = 6.3592436032499466e-01F;
    float v798 = -6.3592436032499466e-01F;
    float32x2_t v800 = (float32x2_t){v4, v4};
    float v805 = -3.8188130791298663e-01F;
    float v809 = -2.9520461738277515e-01F;
    float v813 = 4.6243103089499693e-01F;
    float v817 = -7.5763564827777208e-01F;
    float32x2_t v439 = vtrn1_f32(v402, v402);
    float32x2_t v440 = vtrn2_f32(v402, v402);
    float32x2_t v564 = v5[0];
    float32x2_t v607 = (float32x2_t){v606, v606};
    float32x2_t v611 = (float32x2_t){v610, v610};
    float32x2_t v615 = (float32x2_t){v614, v614};
    float32x2_t v619 = (float32x2_t){v618, v618};
    float32x2_t v623 = (float32x2_t){v621, v622};
    float32x2_t v630 = (float32x2_t){v628, v629};
    float32x2_t v637 = (float32x2_t){v635, v636};
    float32x2_t v644 = (float32x2_t){v642, v643};
    float32x2_t v687 = (float32x2_t){v686, v686};
    float32x2_t v691 = (float32x2_t){v690, v690};
    float32x2_t v695 = (float32x2_t){v694, v694};
    float32x2_t v699 = (float32x2_t){v698, v698};
    float32x2_t v703 = (float32x2_t){v702, v702};
    float32x2_t v707 = (float32x2_t){v705, v706};
    float32x2_t v714 = (float32x2_t){v712, v713};
    float32x2_t v721 = (float32x2_t){v719, v720};
    float32x2_t v728 = (float32x2_t){v726, v727};
    float32x2_t v771 = (float32x2_t){v769, v770};
    float32x2_t v778 = (float32x2_t){v776, v777};
    float32x2_t v785 = (float32x2_t){v783, v784};
    float32x2_t v792 = (float32x2_t){v790, v791};
    float32x2_t v799 = (float32x2_t){v797, v798};
    float32x2_t v806 = (float32x2_t){v805, v805};
    float32x2_t v810 = (float32x2_t){v809, v809};
    float32x2_t v814 = (float32x2_t){v813, v813};
    float32x2_t v818 = (float32x2_t){v817, v817};
    float32x2_t v20 = v5[istride * 7];
    float32x2_t v38 = v5[istride * 14];
    int64_t v55 = 12 + j * 40;
    int64_t v68 = 26 + j * 40;
    float32x2_t v82 = v5[istride * 10];
    float32x2_t v100 = v5[istride * 17];
    int64_t v117 = 18 + j * 40;
    int64_t v130 = 32 + j * 40;
    float32x2_t v144 = v5[istride * 3];
    int64_t v148 = 4 + j * 40;
    float32x2_t v162 = v5[istride * 13];
    float32x2_t v180 = v5[istride * 20];
    int64_t v197 = 24 + j * 40;
    int64_t v210 = 38 + j * 40;
    float32x2_t v224 = v5[istride * 6];
    int64_t v228 = 10 + j * 40;
    float32x2_t v242 = v5[istride * 16];
    float32x2_t v260 = v5[istride * 2];
    int64_t v277 = 30 + j * 40;
    int64_t v290 = 2 + j * 40;
    float32x2_t v304 = v5[istride * 9];
    int64_t v308 = 16 + j * 40;
    float32x2_t v322 = v5[istride * 19];
    float32x2_t v340 = v5[istride * 5];
    int64_t v357 = 36 + j * 40;
    int64_t v370 = 8 + j * 40;
    float32x2_t v384 = v5[istride * 12];
    int64_t v388 = 22 + j * 40;
    float32x2_t v420 = v5[istride * 8];
    float32x2_t v438 = v7[j * 40];
    int64_t v442 = j * 40 + 1;
    int64_t v450 = 14 + j * 40;
    float32x2_t v464 = v5[istride * 15];
    int64_t v468 = 28 + j * 40;
    float32x2_t v482 = v5[istride * 4];
    float32x2_t v500 = v5[istride * 11];
    int64_t v517 = 6 + j * 40;
    int64_t v530 = 20 + j * 40;
    float32x2_t v544 = v5[istride * 18];
    int64_t v548 = 34 + j * 40;
    float32x2_t v625 = vmul_f32(v800, v623);
    float32x2_t v632 = vmul_f32(v800, v630);
    float32x2_t v639 = vmul_f32(v800, v637);
    float32x2_t v646 = vmul_f32(v800, v644);
    float32x2_t v709 = vmul_f32(v800, v707);
    float32x2_t v716 = vmul_f32(v800, v714);
    float32x2_t v723 = vmul_f32(v800, v721);
    float32x2_t v730 = vmul_f32(v800, v728);
    float32x2_t v773 = vmul_f32(v800, v771);
    float32x2_t v780 = vmul_f32(v800, v778);
    float32x2_t v787 = vmul_f32(v800, v785);
    float32x2_t v794 = vmul_f32(v800, v792);
    float32x2_t v801 = vmul_f32(v800, v799);
    float32x2_t v56 = v7[v55];
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    int64_t v60 = v55 + 1;
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v144, v144);
    float32x2_t v151 = vtrn2_f32(v144, v144);
    int64_t v153 = v148 + 1;
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vtrn1_f32(v162, v162);
    float32x2_t v200 = vtrn2_f32(v162, v162);
    int64_t v202 = v197 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v180, v180);
    float32x2_t v213 = vtrn2_f32(v180, v180);
    int64_t v215 = v210 + 1;
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vtrn1_f32(v224, v224);
    float32x2_t v231 = vtrn2_f32(v224, v224);
    int64_t v233 = v228 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vtrn1_f32(v242, v242);
    float32x2_t v280 = vtrn2_f32(v242, v242);
    int64_t v282 = v277 + 1;
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vtrn1_f32(v260, v260);
    float32x2_t v293 = vtrn2_f32(v260, v260);
    int64_t v295 = v290 + 1;
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vtrn1_f32(v304, v304);
    float32x2_t v311 = vtrn2_f32(v304, v304);
    int64_t v313 = v308 + 1;
    float32x2_t v358 = v7[v357];
    float32x2_t v359 = vtrn1_f32(v322, v322);
    float32x2_t v360 = vtrn2_f32(v322, v322);
    int64_t v362 = v357 + 1;
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vtrn1_f32(v340, v340);
    float32x2_t v373 = vtrn2_f32(v340, v340);
    int64_t v375 = v370 + 1;
    float32x2_t v389 = v7[v388];
    float32x2_t v390 = vtrn1_f32(v384, v384);
    float32x2_t v391 = vtrn2_f32(v384, v384);
    int64_t v393 = v388 + 1;
    float32x2_t v443 = v7[v442];
    float32x2_t v444 = vmul_f32(v439, v438);
    float32x2_t v451 = v7[v450];
    float32x2_t v452 = vtrn1_f32(v420, v420);
    float32x2_t v453 = vtrn2_f32(v420, v420);
    int64_t v455 = v450 + 1;
    float32x2_t v469 = v7[v468];
    float32x2_t v470 = vtrn1_f32(v464, v464);
    float32x2_t v471 = vtrn2_f32(v464, v464);
    int64_t v473 = v468 + 1;
    float32x2_t v518 = v7[v517];
    float32x2_t v519 = vtrn1_f32(v482, v482);
    float32x2_t v520 = vtrn2_f32(v482, v482);
    int64_t v522 = v517 + 1;
    float32x2_t v531 = v7[v530];
    float32x2_t v532 = vtrn1_f32(v500, v500);
    float32x2_t v533 = vtrn2_f32(v500, v500);
    int64_t v535 = v530 + 1;
    float32x2_t v549 = v7[v548];
    float32x2_t v550 = vtrn1_f32(v544, v544);
    float32x2_t v551 = vtrn2_f32(v544, v544);
    int64_t v553 = v548 + 1;
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v203 = v7[v202];
    float32x2_t v204 = vmul_f32(v199, v198);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v234 = v7[v233];
    float32x2_t v235 = vmul_f32(v230, v229);
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vmul_f32(v292, v291);
    float32x2_t v314 = v7[v313];
    float32x2_t v315 = vmul_f32(v310, v309);
    float32x2_t v363 = v7[v362];
    float32x2_t v364 = vmul_f32(v359, v358);
    float32x2_t v376 = v7[v375];
    float32x2_t v377 = vmul_f32(v372, v371);
    float32x2_t v394 = v7[v393];
    float32x2_t v395 = vmul_f32(v390, v389);
    float32x2_t v456 = v7[v455];
    float32x2_t v457 = vmul_f32(v452, v451);
    float32x2_t v474 = v7[v473];
    float32x2_t v475 = vmul_f32(v470, v469);
    float32x2_t v523 = v7[v522];
    float32x2_t v524 = vmul_f32(v519, v518);
    float32x2_t v536 = v7[v535];
    float32x2_t v537 = vmul_f32(v532, v531);
    float32x2_t v554 = v7[v553];
    float32x2_t v555 = vmul_f32(v550, v549);
    float32x2_t v446 = vfma_f32(v444, v440, v443);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v206 = vfma_f32(v204, v200, v203);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v237 = vfma_f32(v235, v231, v234);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v299 = vfma_f32(v297, v293, v296);
    float32x2_t v317 = vfma_f32(v315, v311, v314);
    float32x2_t v366 = vfma_f32(v364, v360, v363);
    float32x2_t v379 = vfma_f32(v377, v373, v376);
    float32x2_t v397 = vfma_f32(v395, v391, v394);
    float32x2_t v459 = vfma_f32(v457, v453, v456);
    float32x2_t v477 = vfma_f32(v475, v471, v474);
    float32x2_t v526 = vfma_f32(v524, v520, v523);
    float32x2_t v539 = vfma_f32(v537, v533, v536);
    float32x2_t v557 = vfma_f32(v555, v551, v554);
    float32x2_t v558 = vadd_f32(v64, v77);
    float32x2_t v559 = vsub_f32(v64, v77);
    float32x2_t v566 = vadd_f32(v126, v139);
    float32x2_t v567 = vsub_f32(v126, v139);
    float32x2_t v569 = vadd_f32(v206, v219);
    float32x2_t v570 = vsub_f32(v206, v219);
    float32x2_t v572 = vadd_f32(v286, v299);
    float32x2_t v573 = vsub_f32(v286, v299);
    float32x2_t v575 = vadd_f32(v366, v379);
    float32x2_t v576 = vsub_f32(v366, v379);
    float32x2_t v578 = vadd_f32(v446, v459);
    float32x2_t v579 = vsub_f32(v446, v459);
    float32x2_t v581 = vadd_f32(v526, v539);
    float32x2_t v582 = vsub_f32(v526, v539);
    float32x2_t v565 = vadd_f32(v558, v564);
    float32x2_t v568 = vadd_f32(v566, v157);
    float32x2_t v571 = vadd_f32(v569, v237);
    float32x2_t v574 = vadd_f32(v572, v317);
    float32x2_t v577 = vadd_f32(v575, v397);
    float32x2_t v580 = vadd_f32(v578, v477);
    float32x2_t v583 = vadd_f32(v581, v557);
    float32x2_t v668 = vadd_f32(v566, v581);
    float32x2_t v669 = vsub_f32(v566, v581);
    float32x2_t v670 = vadd_f32(v575, v572);
    float32x2_t v671 = vsub_f32(v575, v572);
    float32x2_t v672 = vadd_f32(v569, v578);
    float32x2_t v673 = vsub_f32(v569, v578);
    float32x2_t v752 = vadd_f32(v567, v582);
    float32x2_t v753 = vsub_f32(v567, v582);
    float32x2_t v754 = vadd_f32(v576, v573);
    float32x2_t v755 = vsub_f32(v576, v573);
    float32x2_t v756 = vadd_f32(v570, v579);
    float32x2_t v757 = vsub_f32(v570, v579);
    float32x2_t v584 = vadd_f32(v568, v583);
    float32x2_t v585 = vsub_f32(v568, v583);
    float32x2_t v586 = vadd_f32(v577, v574);
    float32x2_t v587 = vsub_f32(v577, v574);
    float32x2_t v588 = vadd_f32(v571, v580);
    float32x2_t v589 = vsub_f32(v571, v580);
    float32x2_t v674 = vadd_f32(v668, v670);
    float32x2_t v677 = vsub_f32(v668, v670);
    float32x2_t v678 = vsub_f32(v670, v672);
    float32x2_t v679 = vsub_f32(v672, v668);
    float32x2_t v680 = vadd_f32(v669, v671);
    float32x2_t v682 = vsub_f32(v669, v671);
    float32x2_t v683 = vsub_f32(v671, v673);
    float32x2_t v684 = vsub_f32(v673, v669);
    float32x2_t v758 = vadd_f32(v752, v754);
    float32x2_t v761 = vsub_f32(v752, v754);
    float32x2_t v762 = vsub_f32(v754, v756);
    float32x2_t v763 = vsub_f32(v756, v752);
    float32x2_t v764 = vadd_f32(v753, v755);
    float32x2_t v766 = vsub_f32(v753, v755);
    float32x2_t v767 = vsub_f32(v755, v757);
    float32x2_t v768 = vsub_f32(v757, v753);
    float32x2_t v590 = vadd_f32(v584, v586);
    float32x2_t v593 = vsub_f32(v584, v586);
    float32x2_t v594 = vsub_f32(v586, v588);
    float32x2_t v595 = vsub_f32(v588, v584);
    float32x2_t v596 = vadd_f32(v585, v587);
    float32x2_t v598 = vsub_f32(v585, v587);
    float32x2_t v599 = vsub_f32(v587, v589);
    float32x2_t v600 = vsub_f32(v589, v585);
    float32x2_t v675 = vadd_f32(v674, v672);
    float32x2_t v681 = vadd_f32(v680, v673);
    float32x2_t v696 = vmul_f32(v677, v695);
    float32x2_t v700 = vmul_f32(v678, v699);
    float32x2_t v704 = vmul_f32(v679, v703);
    float32x2_t v717 = vrev64_f32(v682);
    float32x2_t v724 = vrev64_f32(v683);
    float32x2_t v731 = vrev64_f32(v684);
    float32x2_t v759 = vadd_f32(v758, v756);
    float32x2_t v765 = vadd_f32(v764, v757);
    float32x2_t v788 = vrev64_f32(v761);
    float32x2_t v795 = vrev64_f32(v762);
    float32x2_t v802 = vrev64_f32(v763);
    float32x2_t v811 = vmul_f32(v766, v810);
    float32x2_t v815 = vmul_f32(v767, v814);
    float32x2_t v819 = vmul_f32(v768, v818);
    float32x2_t v591 = vadd_f32(v590, v588);
    float32x2_t v597 = vadd_f32(v596, v589);
    float32x2_t v612 = vmul_f32(v593, v611);
    float32x2_t v616 = vmul_f32(v594, v615);
    float32x2_t v620 = vmul_f32(v595, v619);
    float32x2_t v633 = vrev64_f32(v598);
    float32x2_t v640 = vrev64_f32(v599);
    float32x2_t v647 = vrev64_f32(v600);
    float32x2_t v676 = vadd_f32(v675, v558);
    float32x2_t v692 = vmul_f32(v675, v691);
    float32x2_t v710 = vrev64_f32(v681);
    float32x2_t v718 = vmul_f32(v717, v716);
    float32x2_t v725 = vmul_f32(v724, v723);
    float32x2_t v732 = vmul_f32(v731, v730);
    float32x2_t v760 = vadd_f32(v759, v559);
    float32x2_t v781 = vrev64_f32(v759);
    float32x2_t v789 = vmul_f32(v788, v787);
    float32x2_t v796 = vmul_f32(v795, v794);
    float32x2_t v803 = vmul_f32(v802, v801);
    float32x2_t v807 = vmul_f32(v765, v806);
    float32x2_t v592 = vadd_f32(v591, v565);
    float32x2_t v608 = vmul_f32(v591, v607);
    float32x2_t v626 = vrev64_f32(v597);
    float32x2_t v634 = vmul_f32(v633, v632);
    float32x2_t v641 = vmul_f32(v640, v639);
    float32x2_t v648 = vmul_f32(v647, v646);
    float32x2_t v688 = vmul_f32(v676, v687);
    float32x2_t v711 = vmul_f32(v710, v709);
    float32x2_t v774 = vrev64_f32(v760);
    float32x2_t v782 = vmul_f32(v781, v780);
    float32x2_t v827 = vadd_f32(v807, v811);
    float32x2_t v829 = vsub_f32(v807, v811);
    float32x2_t v831 = vsub_f32(v807, v815);
    float32x2_t v627 = vmul_f32(v626, v625);
    float32x2_t v649 = vadd_f32(v592, v608);
    float32x2_t v733 = vadd_f32(v688, v692);
    float32x2_t v740 = vadd_f32(v711, v718);
    float32x2_t v742 = vsub_f32(v711, v718);
    float32x2_t v744 = vsub_f32(v711, v725);
    float32x2_t v775 = vmul_f32(v774, v773);
    float32x2_t v828 = vadd_f32(v827, v815);
    float32x2_t v830 = vsub_f32(v829, v819);
    float32x2_t v832 = vadd_f32(v831, v819);
    float32x2_t v839 = vadd_f32(v592, v688);
    v6[0] = v592;
    float32x2_t v650 = vadd_f32(v649, v612);
    float32x2_t v652 = vsub_f32(v649, v612);
    float32x2_t v654 = vsub_f32(v649, v616);
    float32x2_t v656 = vadd_f32(v627, v634);
    float32x2_t v658 = vsub_f32(v627, v634);
    float32x2_t v660 = vsub_f32(v627, v641);
    float32x2_t v734 = vadd_f32(v733, v696);
    float32x2_t v736 = vsub_f32(v733, v696);
    float32x2_t v738 = vsub_f32(v733, v700);
    float32x2_t v741 = vadd_f32(v740, v725);
    float32x2_t v743 = vsub_f32(v742, v732);
    float32x2_t v745 = vadd_f32(v744, v732);
    float32x2_t v820 = vadd_f32(v775, v782);
    float32x2_t v840 = vadd_f32(v839, v775);
    float32x2_t v841 = vsub_f32(v839, v775);
    float32x2_t v651 = vadd_f32(v650, v616);
    float32x2_t v653 = vsub_f32(v652, v620);
    float32x2_t v655 = vadd_f32(v654, v620);
    float32x2_t v657 = vadd_f32(v656, v641);
    float32x2_t v659 = vsub_f32(v658, v648);
    float32x2_t v661 = vadd_f32(v660, v648);
    float32x2_t v735 = vadd_f32(v734, v700);
    float32x2_t v737 = vsub_f32(v736, v704);
    float32x2_t v739 = vadd_f32(v738, v704);
    float32x2_t v821 = vadd_f32(v820, v789);
    float32x2_t v823 = vsub_f32(v820, v789);
    float32x2_t v825 = vsub_f32(v820, v796);
    v6[ostride * 7] = v841;
    v6[ostride * 14] = v840;
    float32x2_t v662 = vadd_f32(v651, v657);
    float32x2_t v663 = vsub_f32(v651, v657);
    float32x2_t v664 = vadd_f32(v653, v659);
    float32x2_t v665 = vsub_f32(v653, v659);
    float32x2_t v666 = vadd_f32(v655, v661);
    float32x2_t v667 = vsub_f32(v655, v661);
    float32x2_t v746 = vadd_f32(v735, v741);
    float32x2_t v747 = vsub_f32(v735, v741);
    float32x2_t v748 = vadd_f32(v737, v743);
    float32x2_t v749 = vsub_f32(v737, v743);
    float32x2_t v750 = vadd_f32(v739, v745);
    float32x2_t v751 = vsub_f32(v739, v745);
    float32x2_t v822 = vadd_f32(v821, v796);
    float32x2_t v824 = vsub_f32(v823, v803);
    float32x2_t v826 = vadd_f32(v825, v803);
    float32x2_t v833 = vadd_f32(v822, v828);
    float32x2_t v834 = vsub_f32(v822, v828);
    float32x2_t v835 = vadd_f32(v824, v830);
    float32x2_t v836 = vsub_f32(v824, v830);
    float32x2_t v837 = vadd_f32(v826, v832);
    float32x2_t v838 = vsub_f32(v826, v832);
    float32x2_t v857 = vadd_f32(v663, v747);
    v6[ostride * 15] = v663;
    float32x2_t v875 = vadd_f32(v665, v749);
    v6[ostride * 9] = v665;
    float32x2_t v893 = vadd_f32(v666, v750);
    v6[ostride * 3] = v666;
    float32x2_t v911 = vadd_f32(v667, v751);
    v6[ostride * 18] = v667;
    float32x2_t v929 = vadd_f32(v664, v748);
    v6[ostride * 12] = v664;
    float32x2_t v947 = vadd_f32(v662, v746);
    v6[ostride * 6] = v662;
    float32x2_t v858 = vadd_f32(v857, v834);
    float32x2_t v859 = vsub_f32(v857, v834);
    float32x2_t v876 = vadd_f32(v875, v836);
    float32x2_t v877 = vsub_f32(v875, v836);
    float32x2_t v894 = vadd_f32(v893, v837);
    float32x2_t v895 = vsub_f32(v893, v837);
    float32x2_t v912 = vadd_f32(v911, v838);
    float32x2_t v913 = vsub_f32(v911, v838);
    float32x2_t v930 = vadd_f32(v929, v835);
    float32x2_t v931 = vsub_f32(v929, v835);
    float32x2_t v948 = vadd_f32(v947, v833);
    float32x2_t v949 = vsub_f32(v947, v833);
    v6[ostride] = v859;
    v6[ostride * 8] = v858;
    v6[ostride * 16] = v877;
    v6[ostride * 2] = v876;
    v6[ostride * 10] = v895;
    v6[ostride * 17] = v894;
    v6[ostride * 4] = v913;
    v6[ostride * 11] = v912;
    v6[ostride * 19] = v931;
    v6[ostride * 5] = v930;
    v6[ostride * 13] = v949;
    v6[ostride * 20] = v948;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs21(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v447 = -1.1666666666666665e+00F;
    float v452 = 7.9015646852540022e-01F;
    float v457 = 5.5854267289647742e-02F;
    float v462 = 7.3430220123575241e-01F;
    float v467 = -4.4095855184409838e-01F;
    float v474 = -3.4087293062393137e-01F;
    float v481 = 5.3396936033772524e-01F;
    float v488 = -8.7484229096165667e-01F;
    float v531 = -1.4999999999999998e+00F;
    float v536 = 1.7499999999999996e+00F;
    float v541 = -1.1852347027881001e+00F;
    float v546 = -8.3781400934471603e-02F;
    float v551 = -1.1014533018536286e+00F;
    float v556 = 6.6143782776614746e-01F;
    float v563 = 5.1130939593589697e-01F;
    float v570 = -8.0095404050658769e-01F;
    float v577 = 1.3122634364424848e+00F;
    float v620 = -8.6602540378443871e-01F;
    float v627 = 1.0103629710818451e+00F;
    float v634 = -6.8429557470759583e-01F;
    float v641 = -4.8371214382601155e-02F;
    float v648 = -6.3592436032499466e-01F;
    float v655 = -3.8188130791298663e-01F;
    float v660 = -2.9520461738277515e-01F;
    float v665 = 4.6243103089499693e-01F;
    float v670 = -7.5763564827777208e-01F;
    const float32x2_t *v993 = &v5[v0];
    float32x2_t *v1123 = &v6[v2];
    int64_t v19 = v0 * 7;
    int64_t v33 = v0 * 14;
    int64_t v48 = v10 * 6;
    int64_t v55 = v10 * 13;
    int64_t v61 = v0 * 10;
    int64_t v75 = v0 * 17;
    int64_t v90 = v10 * 9;
    int64_t v97 = v10 * 16;
    int64_t v103 = v0 * 3;
    int64_t v111 = v10 * 2;
    int64_t v117 = v0 * 13;
    int64_t v131 = v0 * 20;
    int64_t v146 = v10 * 12;
    int64_t v153 = v10 * 19;
    int64_t v159 = v0 * 6;
    int64_t v167 = v10 * 5;
    int64_t v173 = v0 * 16;
    int64_t v187 = v0 * 2;
    int64_t v202 = v10 * 15;
    int64_t v215 = v0 * 9;
    int64_t v223 = v10 * 8;
    int64_t v229 = v0 * 19;
    int64_t v243 = v0 * 5;
    int64_t v258 = v10 * 18;
    int64_t v265 = v10 * 4;
    int64_t v271 = v0 * 12;
    int64_t v279 = v10 * 11;
    int64_t v299 = v0 * 8;
    int64_t v321 = v10 * 7;
    int64_t v327 = v0 * 15;
    int64_t v335 = v10 * 14;
    int64_t v341 = v0 * 4;
    int64_t v355 = v0 * 11;
    int64_t v370 = v10 * 3;
    int64_t v377 = v10 * 10;
    int64_t v383 = v0 * 18;
    int64_t v391 = v10 * 17;
    int64_t v392 = v13 * 20;
    float v470 = v4 * v467;
    float v477 = v4 * v474;
    float v484 = v4 * v481;
    float v491 = v4 * v488;
    float v559 = v4 * v556;
    float v566 = v4 * v563;
    float v573 = v4 * v570;
    float v580 = v4 * v577;
    float v623 = v4 * v620;
    float v630 = v4 * v627;
    float v637 = v4 * v634;
    float v644 = v4 * v641;
    float v651 = v4 * v648;
    int64_t v704 = v2 * 7;
    int64_t v711 = v2 * 14;
    int64_t v721 = v2 * 15;
    int64_t v735 = v2 * 8;
    int64_t v745 = v2 * 9;
    int64_t v752 = v2 * 16;
    int64_t v759 = v2 * 2;
    int64_t v769 = v2 * 3;
    int64_t v776 = v2 * 10;
    int64_t v783 = v2 * 17;
    int64_t v793 = v2 * 18;
    int64_t v800 = v2 * 4;
    int64_t v807 = v2 * 11;
    int64_t v817 = v2 * 12;
    int64_t v824 = v2 * 19;
    int64_t v831 = v2 * 5;
    int64_t v841 = v2 * 6;
    int64_t v848 = v2 * 13;
    int64_t v855 = v2 * 20;
    const float32x2_t *v1050 = &v5[0];
    svint64_t v1051 = svindex_s64(0, v1);
    svfloat32_t v1054 = svdup_n_f32(v447);
    svfloat32_t v1055 = svdup_n_f32(v452);
    svfloat32_t v1056 = svdup_n_f32(v457);
    svfloat32_t v1057 = svdup_n_f32(v462);
    svfloat32_t v1062 = svdup_n_f32(v531);
    svfloat32_t v1063 = svdup_n_f32(v536);
    svfloat32_t v1064 = svdup_n_f32(v541);
    svfloat32_t v1065 = svdup_n_f32(v546);
    svfloat32_t v1066 = svdup_n_f32(v551);
    svfloat32_t v1076 = svdup_n_f32(v655);
    svfloat32_t v1077 = svdup_n_f32(v660);
    svfloat32_t v1078 = svdup_n_f32(v665);
    svfloat32_t v1079 = svdup_n_f32(v670);
    float32x2_t *v1087 = &v6[0];
    svint64_t v1268 = svindex_s64(0, v3);
    int64_t v50 = v48 + v392;
    int64_t v57 = v55 + v392;
    int64_t v92 = v90 + v392;
    int64_t v99 = v97 + v392;
    int64_t v113 = v111 + v392;
    int64_t v148 = v146 + v392;
    int64_t v155 = v153 + v392;
    int64_t v169 = v167 + v392;
    int64_t v204 = v202 + v392;
    int64_t v211 = v10 + v392;
    int64_t v225 = v223 + v392;
    int64_t v260 = v258 + v392;
    int64_t v267 = v265 + v392;
    int64_t v281 = v279 + v392;
    svfloat32_t v317 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v392]));
    int64_t v323 = v321 + v392;
    int64_t v337 = v335 + v392;
    int64_t v372 = v370 + v392;
    int64_t v379 = v377 + v392;
    int64_t v393 = v391 + v392;
    const float32x2_t *v867 = &v5[v19];
    const float32x2_t *v876 = &v5[v33];
    const float32x2_t *v885 = &v5[v61];
    const float32x2_t *v894 = &v5[v75];
    const float32x2_t *v903 = &v5[v103];
    const float32x2_t *v912 = &v5[v117];
    const float32x2_t *v921 = &v5[v131];
    const float32x2_t *v930 = &v5[v159];
    const float32x2_t *v939 = &v5[v173];
    const float32x2_t *v948 = &v5[v187];
    const float32x2_t *v957 = &v5[v215];
    const float32x2_t *v966 = &v5[v229];
    const float32x2_t *v975 = &v5[v243];
    const float32x2_t *v984 = &v5[v271];
    svfloat32_t v995 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v993), v1051));
    const float32x2_t *v1003 = &v5[v299];
    const float32x2_t *v1013 = &v5[v327];
    const float32x2_t *v1022 = &v5[v341];
    const float32x2_t *v1031 = &v5[v355];
    const float32x2_t *v1040 = &v5[v383];
    svfloat32_t v1052 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1050), v1051));
    svfloat32_t v1058 = svdup_n_f32(v470);
    svfloat32_t v1059 = svdup_n_f32(v477);
    svfloat32_t v1060 = svdup_n_f32(v484);
    svfloat32_t v1061 = svdup_n_f32(v491);
    svfloat32_t v1067 = svdup_n_f32(v559);
    svfloat32_t v1068 = svdup_n_f32(v566);
    svfloat32_t v1069 = svdup_n_f32(v573);
    svfloat32_t v1070 = svdup_n_f32(v580);
    svfloat32_t v1071 = svdup_n_f32(v623);
    svfloat32_t v1072 = svdup_n_f32(v630);
    svfloat32_t v1073 = svdup_n_f32(v637);
    svfloat32_t v1074 = svdup_n_f32(v644);
    svfloat32_t v1075 = svdup_n_f32(v651);
    float32x2_t *v1096 = &v6[v704];
    float32x2_t *v1105 = &v6[v711];
    float32x2_t *v1114 = &v6[v721];
    float32x2_t *v1132 = &v6[v735];
    float32x2_t *v1141 = &v6[v745];
    float32x2_t *v1150 = &v6[v752];
    float32x2_t *v1159 = &v6[v759];
    float32x2_t *v1168 = &v6[v769];
    float32x2_t *v1177 = &v6[v776];
    float32x2_t *v1186 = &v6[v783];
    float32x2_t *v1195 = &v6[v793];
    float32x2_t *v1204 = &v6[v800];
    float32x2_t *v1213 = &v6[v807];
    float32x2_t *v1222 = &v6[v817];
    float32x2_t *v1231 = &v6[v824];
    float32x2_t *v1240 = &v6[v831];
    float32x2_t *v1249 = &v6[v841];
    float32x2_t *v1258 = &v6[v848];
    float32x2_t *v1267 = &v6[v855];
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v50]));
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v149 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v148]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t zero318 = svdup_n_f32(0);
    svfloat32_t v318 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero318, v995, v317, 0),
                     v995, v317, 90);
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v338 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v337]));
    svfloat32_t v373 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v372]));
    svfloat32_t v380 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v379]));
    svfloat32_t v394 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v393]));
    svfloat32_t v869 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v867), v1051));
    svfloat32_t v878 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v876), v1051));
    svfloat32_t v887 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v885), v1051));
    svfloat32_t v896 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v894), v1051));
    svfloat32_t v905 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v903), v1051));
    svfloat32_t v914 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v912), v1051));
    svfloat32_t v923 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v921), v1051));
    svfloat32_t v932 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v930), v1051));
    svfloat32_t v941 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v939), v1051));
    svfloat32_t v950 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v948), v1051));
    svfloat32_t v959 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v957), v1051));
    svfloat32_t v968 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v966), v1051));
    svfloat32_t v977 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v975), v1051));
    svfloat32_t v986 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v984), v1051));
    svfloat32_t v1005 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1003), v1051));
    svfloat32_t v1015 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1013), v1051));
    svfloat32_t v1024 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1022), v1051));
    svfloat32_t v1033 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1031), v1051));
    svfloat32_t v1042 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1040), v1051));
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v869, v51, 0),
                     v869, v51, 90);
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v878, v58, 0),
                     v878, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v887, v93, 0),
                     v887, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v896, v100, 0),
                     v896, v100, 90);
    svfloat32_t zero150 = svdup_n_f32(0);
    svfloat32_t v150 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero150, v914, v149, 0),
                     v914, v149, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v923, v156, 0),
                     v923, v156, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v941, v205, 0),
                     v941, v205, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v950, v212, 0),
                     v950, v212, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero262, v968, v261, 0),
                     v968, v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero269, v977, v268, 0),
                     v977, v268, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero325, v1005, v324, 0), v1005,
        v324, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero374, v1024, v373, 0), v1024,
        v373, 90);
    svfloat32_t zero381 = svdup_n_f32(0);
    svfloat32_t v381 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero381, v1033, v380, 0), v1033,
        v380, 90);
    svfloat32_t v396 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v397 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v406 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v407 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v409 = svadd_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v410 = svsub_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v412 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v413 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v415 = svadd_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v416 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v418 = svadd_f32_x(svptrue_b32(), v318, v325);
    svfloat32_t v419 = svsub_f32_x(svptrue_b32(), v318, v325);
    svfloat32_t v421 = svadd_f32_x(svptrue_b32(), v374, v381);
    svfloat32_t v422 = svsub_f32_x(svptrue_b32(), v374, v381);
    svfloat32_t v405 = svadd_f32_x(svptrue_b32(), v396, v1052);
    svfloat32_t v408 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v406, v905, v114, 0),
                     v905, v114, 90);
    svfloat32_t v411 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v409, v932, v170, 0),
                     v932, v170, 90);
    svfloat32_t v414 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v412, v959, v226, 0),
                     v959, v226, 90);
    svfloat32_t v417 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v415, v986, v282, 0),
                     v986, v282, 90);
    svfloat32_t v420 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v418, v1015, v338, 0),
                     v1015, v338, 90);
    svfloat32_t v423 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v421, v1042, v394, 0),
                     v1042, v394, 90);
    svfloat32_t v513 = svadd_f32_x(svptrue_b32(), v406, v421);
    svfloat32_t v514 = svsub_f32_x(svptrue_b32(), v406, v421);
    svfloat32_t v515 = svadd_f32_x(svptrue_b32(), v415, v412);
    svfloat32_t v516 = svsub_f32_x(svptrue_b32(), v415, v412);
    svfloat32_t v517 = svadd_f32_x(svptrue_b32(), v409, v418);
    svfloat32_t v518 = svsub_f32_x(svptrue_b32(), v409, v418);
    svfloat32_t v602 = svadd_f32_x(svptrue_b32(), v407, v422);
    svfloat32_t v603 = svsub_f32_x(svptrue_b32(), v407, v422);
    svfloat32_t v604 = svadd_f32_x(svptrue_b32(), v416, v413);
    svfloat32_t v605 = svsub_f32_x(svptrue_b32(), v416, v413);
    svfloat32_t v606 = svadd_f32_x(svptrue_b32(), v410, v419);
    svfloat32_t v607 = svsub_f32_x(svptrue_b32(), v410, v419);
    svfloat32_t v424 = svadd_f32_x(svptrue_b32(), v408, v423);
    svfloat32_t v425 = svsub_f32_x(svptrue_b32(), v408, v423);
    svfloat32_t v426 = svadd_f32_x(svptrue_b32(), v417, v414);
    svfloat32_t v427 = svsub_f32_x(svptrue_b32(), v417, v414);
    svfloat32_t v428 = svadd_f32_x(svptrue_b32(), v411, v420);
    svfloat32_t v429 = svsub_f32_x(svptrue_b32(), v411, v420);
    svfloat32_t v519 = svadd_f32_x(svptrue_b32(), v513, v515);
    svfloat32_t v522 = svsub_f32_x(svptrue_b32(), v513, v515);
    svfloat32_t v523 = svsub_f32_x(svptrue_b32(), v515, v517);
    svfloat32_t v524 = svsub_f32_x(svptrue_b32(), v517, v513);
    svfloat32_t v525 = svadd_f32_x(svptrue_b32(), v514, v516);
    svfloat32_t v527 = svsub_f32_x(svptrue_b32(), v514, v516);
    svfloat32_t v528 = svsub_f32_x(svptrue_b32(), v516, v518);
    svfloat32_t v529 = svsub_f32_x(svptrue_b32(), v518, v514);
    svfloat32_t v608 = svadd_f32_x(svptrue_b32(), v602, v604);
    svfloat32_t v611 = svsub_f32_x(svptrue_b32(), v602, v604);
    svfloat32_t v612 = svsub_f32_x(svptrue_b32(), v604, v606);
    svfloat32_t v613 = svsub_f32_x(svptrue_b32(), v606, v602);
    svfloat32_t v614 = svadd_f32_x(svptrue_b32(), v603, v605);
    svfloat32_t v616 = svsub_f32_x(svptrue_b32(), v603, v605);
    svfloat32_t v617 = svsub_f32_x(svptrue_b32(), v605, v607);
    svfloat32_t v618 = svsub_f32_x(svptrue_b32(), v607, v603);
    svfloat32_t v430 = svadd_f32_x(svptrue_b32(), v424, v426);
    svfloat32_t v433 = svsub_f32_x(svptrue_b32(), v424, v426);
    svfloat32_t v434 = svsub_f32_x(svptrue_b32(), v426, v428);
    svfloat32_t v435 = svsub_f32_x(svptrue_b32(), v428, v424);
    svfloat32_t v436 = svadd_f32_x(svptrue_b32(), v425, v427);
    svfloat32_t v438 = svsub_f32_x(svptrue_b32(), v425, v427);
    svfloat32_t v439 = svsub_f32_x(svptrue_b32(), v427, v429);
    svfloat32_t v440 = svsub_f32_x(svptrue_b32(), v429, v425);
    svfloat32_t v520 = svadd_f32_x(svptrue_b32(), v519, v517);
    svfloat32_t v526 = svadd_f32_x(svptrue_b32(), v525, v518);
    svfloat32_t zero568 = svdup_n_f32(0);
    svfloat32_t v568 = svcmla_f32_x(pred_full, zero568, v1068, v527, 90);
    svfloat32_t zero575 = svdup_n_f32(0);
    svfloat32_t v575 = svcmla_f32_x(pred_full, zero575, v1069, v528, 90);
    svfloat32_t zero582 = svdup_n_f32(0);
    svfloat32_t v582 = svcmla_f32_x(pred_full, zero582, v1070, v529, 90);
    svfloat32_t v609 = svadd_f32_x(svptrue_b32(), v608, v606);
    svfloat32_t v615 = svadd_f32_x(svptrue_b32(), v614, v607);
    svfloat32_t zero639 = svdup_n_f32(0);
    svfloat32_t v639 = svcmla_f32_x(pred_full, zero639, v1073, v611, 90);
    svfloat32_t zero646 = svdup_n_f32(0);
    svfloat32_t v646 = svcmla_f32_x(pred_full, zero646, v1074, v612, 90);
    svfloat32_t zero653 = svdup_n_f32(0);
    svfloat32_t v653 = svcmla_f32_x(pred_full, zero653, v1075, v613, 90);
    svfloat32_t v663 = svmul_f32_x(svptrue_b32(), v616, v1077);
    svfloat32_t v668 = svmul_f32_x(svptrue_b32(), v617, v1078);
    svfloat32_t v431 = svadd_f32_x(svptrue_b32(), v430, v428);
    svfloat32_t v437 = svadd_f32_x(svptrue_b32(), v436, v429);
    svfloat32_t zero479 = svdup_n_f32(0);
    svfloat32_t v479 = svcmla_f32_x(pred_full, zero479, v1059, v438, 90);
    svfloat32_t zero486 = svdup_n_f32(0);
    svfloat32_t v486 = svcmla_f32_x(pred_full, zero486, v1060, v439, 90);
    svfloat32_t zero493 = svdup_n_f32(0);
    svfloat32_t v493 = svcmla_f32_x(pred_full, zero493, v1061, v440, 90);
    svfloat32_t v521 = svadd_f32_x(svptrue_b32(), v520, v396);
    svfloat32_t v539 = svmul_f32_x(svptrue_b32(), v520, v1063);
    svfloat32_t zero561 = svdup_n_f32(0);
    svfloat32_t v561 = svcmla_f32_x(pred_full, zero561, v1067, v526, 90);
    svfloat32_t v610 = svadd_f32_x(svptrue_b32(), v609, v397);
    svfloat32_t v432 = svadd_f32_x(svptrue_b32(), v431, v405);
    svfloat32_t zero472 = svdup_n_f32(0);
    svfloat32_t v472 = svcmla_f32_x(pred_full, zero472, v1058, v437, 90);
    svfloat32_t v590 = svadd_f32_x(svptrue_b32(), v561, v568);
    svfloat32_t v592 = svsub_f32_x(svptrue_b32(), v561, v568);
    svfloat32_t v594 = svsub_f32_x(svptrue_b32(), v561, v575);
    svfloat32_t zero625 = svdup_n_f32(0);
    svfloat32_t v625 = svcmla_f32_x(pred_full, zero625, v1071, v610, 90);
    svfloat32_t v681 = svmla_f32_x(pred_full, v663, v615, v1076);
    svfloat32_t v683 = svnmls_f32_x(pred_full, v663, v615, v1076);
    svfloat32_t v685 = svnmls_f32_x(pred_full, v668, v615, v1076);
    svfloat32_t v494 = svmla_f32_x(pred_full, v432, v431, v1054);
    svfloat32_t v501 = svadd_f32_x(svptrue_b32(), v472, v479);
    svfloat32_t v503 = svsub_f32_x(svptrue_b32(), v472, v479);
    svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v472, v486);
    svfloat32_t v583 = svmla_f32_x(pred_full, v539, v521, v1062);
    svfloat32_t v591 = svadd_f32_x(svptrue_b32(), v590, v575);
    svfloat32_t v593 = svsub_f32_x(svptrue_b32(), v592, v582);
    svfloat32_t v595 = svadd_f32_x(svptrue_b32(), v594, v582);
    svfloat32_t v674 = svcmla_f32_x(pred_full, v625, v1072, v609, 90);
    svfloat32_t v682 = svmla_f32_x(pred_full, v681, v617, v1078);
    svfloat32_t v684 = svmls_f32_x(pred_full, v683, v618, v1079);
    svfloat32_t v686 = svmla_f32_x(pred_full, v685, v618, v1079);
    svfloat32_t v693 = svmla_f32_x(pred_full, v432, v521, v1062);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1087), v1268,
                               svreinterpret_f64_f32(v432));
    svfloat32_t v495 = svmla_f32_x(pred_full, v494, v433, v1055);
    svfloat32_t v497 = svmls_f32_x(pred_full, v494, v433, v1055);
    svfloat32_t v499 = svmls_f32_x(pred_full, v494, v434, v1056);
    svfloat32_t v502 = svadd_f32_x(svptrue_b32(), v501, v486);
    svfloat32_t v504 = svsub_f32_x(svptrue_b32(), v503, v493);
    svfloat32_t v506 = svadd_f32_x(svptrue_b32(), v505, v493);
    svfloat32_t v584 = svmla_f32_x(pred_full, v583, v522, v1064);
    svfloat32_t v586 = svmls_f32_x(pred_full, v583, v522, v1064);
    svfloat32_t v588 = svmls_f32_x(pred_full, v583, v523, v1065);
    svfloat32_t v675 = svadd_f32_x(svptrue_b32(), v674, v639);
    svfloat32_t v677 = svsub_f32_x(svptrue_b32(), v674, v639);
    svfloat32_t v679 = svsub_f32_x(svptrue_b32(), v674, v646);
    svfloat32_t v694 = svadd_f32_x(svptrue_b32(), v693, v625);
    svfloat32_t v695 = svsub_f32_x(svptrue_b32(), v693, v625);
    svfloat32_t v496 = svmla_f32_x(pred_full, v495, v434, v1056);
    svfloat32_t v498 = svmls_f32_x(pred_full, v497, v435, v1057);
    svfloat32_t v500 = svmla_f32_x(pred_full, v499, v435, v1057);
    svfloat32_t v585 = svmla_f32_x(pred_full, v584, v523, v1065);
    svfloat32_t v587 = svmls_f32_x(pred_full, v586, v524, v1066);
    svfloat32_t v589 = svmla_f32_x(pred_full, v588, v524, v1066);
    svfloat32_t v676 = svadd_f32_x(svptrue_b32(), v675, v646);
    svfloat32_t v678 = svsub_f32_x(svptrue_b32(), v677, v653);
    svfloat32_t v680 = svadd_f32_x(svptrue_b32(), v679, v653);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1096), v1268,
                               svreinterpret_f64_f32(v695));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1105), v1268,
                               svreinterpret_f64_f32(v694));
    svfloat32_t v507 = svadd_f32_x(svptrue_b32(), v496, v502);
    svfloat32_t v508 = svsub_f32_x(svptrue_b32(), v496, v502);
    svfloat32_t v509 = svadd_f32_x(svptrue_b32(), v498, v504);
    svfloat32_t v510 = svsub_f32_x(svptrue_b32(), v498, v504);
    svfloat32_t v511 = svadd_f32_x(svptrue_b32(), v500, v506);
    svfloat32_t v512 = svsub_f32_x(svptrue_b32(), v500, v506);
    svfloat32_t v596 = svadd_f32_x(svptrue_b32(), v585, v591);
    svfloat32_t v597 = svsub_f32_x(svptrue_b32(), v585, v591);
    svfloat32_t v598 = svadd_f32_x(svptrue_b32(), v587, v593);
    svfloat32_t v599 = svsub_f32_x(svptrue_b32(), v587, v593);
    svfloat32_t v600 = svadd_f32_x(svptrue_b32(), v589, v595);
    svfloat32_t v601 = svsub_f32_x(svptrue_b32(), v589, v595);
    svfloat32_t v687 = svadd_f32_x(svptrue_b32(), v676, v682);
    svfloat32_t v688 = svsub_f32_x(svptrue_b32(), v676, v682);
    svfloat32_t v689 = svadd_f32_x(svptrue_b32(), v678, v684);
    svfloat32_t v690 = svsub_f32_x(svptrue_b32(), v678, v684);
    svfloat32_t v691 = svadd_f32_x(svptrue_b32(), v680, v686);
    svfloat32_t v692 = svsub_f32_x(svptrue_b32(), v680, v686);
    svfloat32_t v717 = svadd_f32_x(svptrue_b32(), v508, v597);
    svfloat32_t v741 = svadd_f32_x(svptrue_b32(), v510, v599);
    svfloat32_t v765 = svadd_f32_x(svptrue_b32(), v511, v600);
    svfloat32_t v789 = svadd_f32_x(svptrue_b32(), v512, v601);
    svfloat32_t v813 = svadd_f32_x(svptrue_b32(), v509, v598);
    svfloat32_t v837 = svadd_f32_x(svptrue_b32(), v507, v596);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1114), v1268,
                               svreinterpret_f64_f32(v508));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1141), v1268,
                               svreinterpret_f64_f32(v510));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1168), v1268,
                               svreinterpret_f64_f32(v511));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1195), v1268,
                               svreinterpret_f64_f32(v512));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1222), v1268,
                               svreinterpret_f64_f32(v509));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1249), v1268,
                               svreinterpret_f64_f32(v507));
    svfloat32_t v718 = svadd_f32_x(svptrue_b32(), v717, v688);
    svfloat32_t v719 = svsub_f32_x(svptrue_b32(), v717, v688);
    svfloat32_t v742 = svadd_f32_x(svptrue_b32(), v741, v690);
    svfloat32_t v743 = svsub_f32_x(svptrue_b32(), v741, v690);
    svfloat32_t v766 = svadd_f32_x(svptrue_b32(), v765, v691);
    svfloat32_t v767 = svsub_f32_x(svptrue_b32(), v765, v691);
    svfloat32_t v790 = svadd_f32_x(svptrue_b32(), v789, v692);
    svfloat32_t v791 = svsub_f32_x(svptrue_b32(), v789, v692);
    svfloat32_t v814 = svadd_f32_x(svptrue_b32(), v813, v689);
    svfloat32_t v815 = svsub_f32_x(svptrue_b32(), v813, v689);
    svfloat32_t v838 = svadd_f32_x(svptrue_b32(), v837, v687);
    svfloat32_t v839 = svsub_f32_x(svptrue_b32(), v837, v687);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1123), v1268,
                               svreinterpret_f64_f32(v719));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1132), v1268,
                               svreinterpret_f64_f32(v718));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1150), v1268,
                               svreinterpret_f64_f32(v743));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1159), v1268,
                               svreinterpret_f64_f32(v742));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1177), v1268,
                               svreinterpret_f64_f32(v767));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1186), v1268,
                               svreinterpret_f64_f32(v766));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1204), v1268,
                               svreinterpret_f64_f32(v791));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1213), v1268,
                               svreinterpret_f64_f32(v790));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1231), v1268,
                               svreinterpret_f64_f32(v815));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1240), v1268,
                               svreinterpret_f64_f32(v814));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1258), v1268,
                               svreinterpret_f64_f32(v839));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1267), v1268,
                               svreinterpret_f64_f32(v838));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs22(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v379 = v5[istride];
    float v934 = 1.1000000000000001e+00F;
    float v937 = 3.3166247903554003e-01F;
    float v938 = -3.3166247903554003e-01F;
    float v945 = 5.1541501300188641e-01F;
    float v949 = 9.4125353283118118e-01F;
    float v953 = 1.4143537075597825e+00F;
    float v957 = 8.5949297361449750e-01F;
    float v961 = 4.2314838273285138e-02F;
    float v965 = 3.8639279888589606e-01F;
    float v969 = 5.1254589567200015e-01F;
    float v973 = 1.0702757469471715e+00F;
    float v977 = 5.5486073394528512e-01F;
    float v980 = 1.2412944743900585e+00F;
    float v981 = -1.2412944743900585e+00F;
    float v987 = 2.0897833842005756e-01F;
    float v988 = -2.0897833842005756e-01F;
    float v994 = 3.7415717312460811e-01F;
    float v995 = -3.7415717312460811e-01F;
    float v1001 = 4.9929922194110327e-02F;
    float v1002 = -4.9929922194110327e-02F;
    float v1008 = 6.5815896284539266e-01F;
    float v1009 = -6.5815896284539266e-01F;
    float v1015 = 6.3306543373877577e-01F;
    float v1016 = -6.3306543373877577e-01F;
    float v1022 = 1.0822460581641109e+00F;
    float v1023 = -1.0822460581641109e+00F;
    float v1029 = 8.1720737907134022e-01F;
    float v1030 = -8.1720737907134022e-01F;
    float v1036 = 4.2408709531871824e-01F;
    float v1037 = -4.2408709531871824e-01F;
    float32x2_t v1039 = (float32x2_t){v4, v4};
    float32x2_t v411 = vtrn1_f32(v379, v379);
    float32x2_t v412 = vtrn2_f32(v379, v379);
    float32x2_t v671 = v5[0];
    float32x2_t v935 = (float32x2_t){v934, v934};
    float32x2_t v939 = (float32x2_t){v937, v938};
    float32x2_t v946 = (float32x2_t){v945, v945};
    float32x2_t v950 = (float32x2_t){v949, v949};
    float32x2_t v954 = (float32x2_t){v953, v953};
    float32x2_t v958 = (float32x2_t){v957, v957};
    float32x2_t v962 = (float32x2_t){v961, v961};
    float32x2_t v966 = (float32x2_t){v965, v965};
    float32x2_t v970 = (float32x2_t){v969, v969};
    float32x2_t v974 = (float32x2_t){v973, v973};
    float32x2_t v978 = (float32x2_t){v977, v977};
    float32x2_t v982 = (float32x2_t){v980, v981};
    float32x2_t v989 = (float32x2_t){v987, v988};
    float32x2_t v996 = (float32x2_t){v994, v995};
    float32x2_t v1003 = (float32x2_t){v1001, v1002};
    float32x2_t v1010 = (float32x2_t){v1008, v1009};
    float32x2_t v1017 = (float32x2_t){v1015, v1016};
    float32x2_t v1024 = (float32x2_t){v1022, v1023};
    float32x2_t v1031 = (float32x2_t){v1029, v1030};
    float32x2_t v1038 = (float32x2_t){v1036, v1037};
    float32x2_t v20 = v5[istride * 11];
    int64_t v37 = 20 + j * 42;
    float32x2_t v51 = v5[istride * 2];
    float32x2_t v69 = v5[istride * 13];
    int64_t v86 = 2 + j * 42;
    int64_t v99 = 24 + j * 42;
    float32x2_t v113 = v5[istride * 4];
    float32x2_t v131 = v5[istride * 15];
    int64_t v148 = 6 + j * 42;
    int64_t v161 = 28 + j * 42;
    float32x2_t v175 = v5[istride * 6];
    float32x2_t v193 = v5[istride * 17];
    int64_t v210 = 10 + j * 42;
    int64_t v223 = 32 + j * 42;
    float32x2_t v237 = v5[istride * 8];
    float32x2_t v255 = v5[istride * 19];
    int64_t v272 = 14 + j * 42;
    int64_t v285 = 36 + j * 42;
    float32x2_t v299 = v5[istride * 10];
    float32x2_t v317 = v5[istride * 21];
    int64_t v334 = 18 + j * 42;
    int64_t v347 = 40 + j * 42;
    float32x2_t v361 = v5[istride * 12];
    int64_t v396 = 22 + j * 42;
    float32x2_t v410 = v7[j * 42];
    int64_t v414 = j * 42 + 1;
    float32x2_t v423 = v5[istride * 14];
    float32x2_t v441 = v5[istride * 3];
    int64_t v458 = 26 + j * 42;
    int64_t v471 = 4 + j * 42;
    float32x2_t v485 = v5[istride * 16];
    float32x2_t v503 = v5[istride * 5];
    int64_t v520 = 30 + j * 42;
    int64_t v533 = 8 + j * 42;
    float32x2_t v547 = v5[istride * 18];
    float32x2_t v565 = v5[istride * 7];
    int64_t v582 = 34 + j * 42;
    int64_t v595 = 12 + j * 42;
    float32x2_t v609 = v5[istride * 20];
    float32x2_t v627 = v5[istride * 9];
    int64_t v644 = 38 + j * 42;
    int64_t v657 = 16 + j * 42;
    float32x2_t v941 = vmul_f32(v1039, v939);
    float32x2_t v984 = vmul_f32(v1039, v982);
    float32x2_t v991 = vmul_f32(v1039, v989);
    float32x2_t v998 = vmul_f32(v1039, v996);
    float32x2_t v1005 = vmul_f32(v1039, v1003);
    float32x2_t v1012 = vmul_f32(v1039, v1010);
    float32x2_t v1019 = vmul_f32(v1039, v1017);
    float32x2_t v1026 = vmul_f32(v1039, v1024);
    float32x2_t v1033 = vmul_f32(v1039, v1031);
    float32x2_t v1040 = vmul_f32(v1039, v1038);
    float32x2_t v38 = v7[v37];
    float32x2_t v39 = vtrn1_f32(v20, v20);
    float32x2_t v40 = vtrn2_f32(v20, v20);
    int64_t v42 = v37 + 1;
    float32x2_t v87 = v7[v86];
    float32x2_t v88 = vtrn1_f32(v51, v51);
    float32x2_t v89 = vtrn2_f32(v51, v51);
    int64_t v91 = v86 + 1;
    float32x2_t v100 = v7[v99];
    float32x2_t v101 = vtrn1_f32(v69, v69);
    float32x2_t v102 = vtrn2_f32(v69, v69);
    int64_t v104 = v99 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v113, v113);
    float32x2_t v151 = vtrn2_f32(v113, v113);
    int64_t v153 = v148 + 1;
    float32x2_t v162 = v7[v161];
    float32x2_t v163 = vtrn1_f32(v131, v131);
    float32x2_t v164 = vtrn2_f32(v131, v131);
    int64_t v166 = v161 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v175, v175);
    float32x2_t v213 = vtrn2_f32(v175, v175);
    int64_t v215 = v210 + 1;
    float32x2_t v224 = v7[v223];
    float32x2_t v225 = vtrn1_f32(v193, v193);
    float32x2_t v226 = vtrn2_f32(v193, v193);
    int64_t v228 = v223 + 1;
    float32x2_t v273 = v7[v272];
    float32x2_t v274 = vtrn1_f32(v237, v237);
    float32x2_t v275 = vtrn2_f32(v237, v237);
    int64_t v277 = v272 + 1;
    float32x2_t v286 = v7[v285];
    float32x2_t v287 = vtrn1_f32(v255, v255);
    float32x2_t v288 = vtrn2_f32(v255, v255);
    int64_t v290 = v285 + 1;
    float32x2_t v335 = v7[v334];
    float32x2_t v336 = vtrn1_f32(v299, v299);
    float32x2_t v337 = vtrn2_f32(v299, v299);
    int64_t v339 = v334 + 1;
    float32x2_t v348 = v7[v347];
    float32x2_t v349 = vtrn1_f32(v317, v317);
    float32x2_t v350 = vtrn2_f32(v317, v317);
    int64_t v352 = v347 + 1;
    float32x2_t v397 = v7[v396];
    float32x2_t v398 = vtrn1_f32(v361, v361);
    float32x2_t v399 = vtrn2_f32(v361, v361);
    int64_t v401 = v396 + 1;
    float32x2_t v415 = v7[v414];
    float32x2_t v416 = vmul_f32(v411, v410);
    float32x2_t v459 = v7[v458];
    float32x2_t v460 = vtrn1_f32(v423, v423);
    float32x2_t v461 = vtrn2_f32(v423, v423);
    int64_t v463 = v458 + 1;
    float32x2_t v472 = v7[v471];
    float32x2_t v473 = vtrn1_f32(v441, v441);
    float32x2_t v474 = vtrn2_f32(v441, v441);
    int64_t v476 = v471 + 1;
    float32x2_t v521 = v7[v520];
    float32x2_t v522 = vtrn1_f32(v485, v485);
    float32x2_t v523 = vtrn2_f32(v485, v485);
    int64_t v525 = v520 + 1;
    float32x2_t v534 = v7[v533];
    float32x2_t v535 = vtrn1_f32(v503, v503);
    float32x2_t v536 = vtrn2_f32(v503, v503);
    int64_t v538 = v533 + 1;
    float32x2_t v583 = v7[v582];
    float32x2_t v584 = vtrn1_f32(v547, v547);
    float32x2_t v585 = vtrn2_f32(v547, v547);
    int64_t v587 = v582 + 1;
    float32x2_t v596 = v7[v595];
    float32x2_t v597 = vtrn1_f32(v565, v565);
    float32x2_t v598 = vtrn2_f32(v565, v565);
    int64_t v600 = v595 + 1;
    float32x2_t v645 = v7[v644];
    float32x2_t v646 = vtrn1_f32(v609, v609);
    float32x2_t v647 = vtrn2_f32(v609, v609);
    int64_t v649 = v644 + 1;
    float32x2_t v658 = v7[v657];
    float32x2_t v659 = vtrn1_f32(v627, v627);
    float32x2_t v660 = vtrn2_f32(v627, v627);
    int64_t v662 = v657 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vmul_f32(v39, v38);
    float32x2_t v92 = v7[v91];
    float32x2_t v93 = vmul_f32(v88, v87);
    float32x2_t v105 = v7[v104];
    float32x2_t v106 = vmul_f32(v101, v100);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v167 = v7[v166];
    float32x2_t v168 = vmul_f32(v163, v162);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vmul_f32(v225, v224);
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vmul_f32(v274, v273);
    float32x2_t v291 = v7[v290];
    float32x2_t v292 = vmul_f32(v287, v286);
    float32x2_t v340 = v7[v339];
    float32x2_t v341 = vmul_f32(v336, v335);
    float32x2_t v353 = v7[v352];
    float32x2_t v354 = vmul_f32(v349, v348);
    float32x2_t v402 = v7[v401];
    float32x2_t v403 = vmul_f32(v398, v397);
    float32x2_t v464 = v7[v463];
    float32x2_t v465 = vmul_f32(v460, v459);
    float32x2_t v477 = v7[v476];
    float32x2_t v478 = vmul_f32(v473, v472);
    float32x2_t v526 = v7[v525];
    float32x2_t v527 = vmul_f32(v522, v521);
    float32x2_t v539 = v7[v538];
    float32x2_t v540 = vmul_f32(v535, v534);
    float32x2_t v588 = v7[v587];
    float32x2_t v589 = vmul_f32(v584, v583);
    float32x2_t v601 = v7[v600];
    float32x2_t v602 = vmul_f32(v597, v596);
    float32x2_t v650 = v7[v649];
    float32x2_t v651 = vmul_f32(v646, v645);
    float32x2_t v663 = v7[v662];
    float32x2_t v664 = vmul_f32(v659, v658);
    float32x2_t v418 = vfma_f32(v416, v412, v415);
    float32x2_t v46 = vfma_f32(v44, v40, v43);
    float32x2_t v95 = vfma_f32(v93, v89, v92);
    float32x2_t v108 = vfma_f32(v106, v102, v105);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v170 = vfma_f32(v168, v164, v167);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v232 = vfma_f32(v230, v226, v229);
    float32x2_t v281 = vfma_f32(v279, v275, v278);
    float32x2_t v294 = vfma_f32(v292, v288, v291);
    float32x2_t v343 = vfma_f32(v341, v337, v340);
    float32x2_t v356 = vfma_f32(v354, v350, v353);
    float32x2_t v405 = vfma_f32(v403, v399, v402);
    float32x2_t v467 = vfma_f32(v465, v461, v464);
    float32x2_t v480 = vfma_f32(v478, v474, v477);
    float32x2_t v529 = vfma_f32(v527, v523, v526);
    float32x2_t v542 = vfma_f32(v540, v536, v539);
    float32x2_t v591 = vfma_f32(v589, v585, v588);
    float32x2_t v604 = vfma_f32(v602, v598, v601);
    float32x2_t v653 = vfma_f32(v651, v647, v650);
    float32x2_t v666 = vfma_f32(v664, v660, v663);
    float32x2_t v672 = vadd_f32(v671, v46);
    float32x2_t v673 = vsub_f32(v671, v46);
    float32x2_t v674 = vadd_f32(v95, v108);
    float32x2_t v675 = vsub_f32(v95, v108);
    float32x2_t v676 = vadd_f32(v157, v170);
    float32x2_t v677 = vsub_f32(v157, v170);
    float32x2_t v678 = vadd_f32(v219, v232);
    float32x2_t v679 = vsub_f32(v219, v232);
    float32x2_t v680 = vadd_f32(v281, v294);
    float32x2_t v681 = vsub_f32(v281, v294);
    float32x2_t v682 = vadd_f32(v343, v356);
    float32x2_t v683 = vsub_f32(v343, v356);
    float32x2_t v684 = vadd_f32(v405, v418);
    float32x2_t v685 = vsub_f32(v405, v418);
    float32x2_t v686 = vadd_f32(v467, v480);
    float32x2_t v687 = vsub_f32(v467, v480);
    float32x2_t v688 = vadd_f32(v529, v542);
    float32x2_t v689 = vsub_f32(v529, v542);
    float32x2_t v690 = vadd_f32(v591, v604);
    float32x2_t v691 = vsub_f32(v591, v604);
    float32x2_t v692 = vadd_f32(v653, v666);
    float32x2_t v693 = vsub_f32(v653, v666);
    float32x2_t v694 = vadd_f32(v674, v692);
    float32x2_t v695 = vadd_f32(v676, v690);
    float32x2_t v696 = vadd_f32(v678, v688);
    float32x2_t v697 = vadd_f32(v680, v686);
    float32x2_t v698 = vadd_f32(v682, v684);
    float32x2_t v699 = vsub_f32(v674, v692);
    float32x2_t v700 = vsub_f32(v676, v690);
    float32x2_t v701 = vsub_f32(v678, v688);
    float32x2_t v702 = vsub_f32(v680, v686);
    float32x2_t v703 = vsub_f32(v682, v684);
    float32x2_t v892 = vadd_f32(v675, v693);
    float32x2_t v893 = vadd_f32(v677, v691);
    float32x2_t v894 = vadd_f32(v679, v689);
    float32x2_t v895 = vadd_f32(v681, v687);
    float32x2_t v896 = vadd_f32(v683, v685);
    float32x2_t v897 = vsub_f32(v675, v693);
    float32x2_t v898 = vsub_f32(v677, v691);
    float32x2_t v899 = vsub_f32(v679, v689);
    float32x2_t v900 = vsub_f32(v681, v687);
    float32x2_t v901 = vsub_f32(v683, v685);
    float32x2_t v704 = vadd_f32(v694, v695);
    float32x2_t v705 = vadd_f32(v696, v698);
    float32x2_t v707 = vsub_f32(v700, v701);
    float32x2_t v708 = vadd_f32(v699, v703);
    float32x2_t v713 = vsub_f32(v695, v697);
    float32x2_t v714 = vsub_f32(v694, v697);
    float32x2_t v715 = vsub_f32(v695, v694);
    float32x2_t v716 = vsub_f32(v698, v697);
    float32x2_t v717 = vsub_f32(v696, v697);
    float32x2_t v718 = vsub_f32(v698, v696);
    float32x2_t v719 = vsub_f32(v695, v698);
    float32x2_t v720 = vsub_f32(v694, v696);
    float32x2_t v722 = vadd_f32(v700, v702);
    float32x2_t v723 = vsub_f32(v699, v702);
    float32x2_t v724 = vadd_f32(v699, v700);
    float32x2_t v725 = vsub_f32(v702, v703);
    float32x2_t v726 = vsub_f32(v701, v702);
    float32x2_t v727 = vsub_f32(v701, v703);
    float32x2_t v728 = vadd_f32(v700, v703);
    float32x2_t v729 = vsub_f32(v699, v701);
    float32x2_t v902 = vadd_f32(v892, v893);
    float32x2_t v903 = vadd_f32(v894, v896);
    float32x2_t v905 = vsub_f32(v898, v899);
    float32x2_t v906 = vadd_f32(v897, v901);
    float32x2_t v911 = vsub_f32(v893, v895);
    float32x2_t v912 = vsub_f32(v892, v895);
    float32x2_t v913 = vsub_f32(v893, v892);
    float32x2_t v914 = vsub_f32(v896, v895);
    float32x2_t v915 = vsub_f32(v894, v895);
    float32x2_t v916 = vsub_f32(v896, v894);
    float32x2_t v917 = vsub_f32(v893, v896);
    float32x2_t v918 = vsub_f32(v892, v894);
    float32x2_t v920 = vadd_f32(v898, v900);
    float32x2_t v921 = vsub_f32(v897, v900);
    float32x2_t v922 = vadd_f32(v897, v898);
    float32x2_t v923 = vsub_f32(v900, v901);
    float32x2_t v924 = vsub_f32(v899, v900);
    float32x2_t v925 = vsub_f32(v899, v901);
    float32x2_t v926 = vadd_f32(v898, v901);
    float32x2_t v927 = vsub_f32(v897, v899);
    float32x2_t v706 = vadd_f32(v697, v704);
    float32x2_t v711 = vsub_f32(v707, v708);
    float32x2_t v721 = vsub_f32(v705, v704);
    float32x2_t v730 = vadd_f32(v707, v708);
    float32x2_t v749 = vmul_f32(v713, v946);
    float32x2_t v753 = vmul_f32(v714, v950);
    float32x2_t v757 = vmul_f32(v715, v954);
    float32x2_t v761 = vmul_f32(v716, v958);
    float32x2_t v765 = vmul_f32(v717, v962);
    float32x2_t v769 = vmul_f32(v718, v966);
    float32x2_t v773 = vmul_f32(v719, v970);
    float32x2_t v777 = vmul_f32(v720, v974);
    float32x2_t v787 = vrev64_f32(v722);
    float32x2_t v794 = vrev64_f32(v723);
    float32x2_t v801 = vrev64_f32(v724);
    float32x2_t v808 = vrev64_f32(v725);
    float32x2_t v815 = vrev64_f32(v726);
    float32x2_t v822 = vrev64_f32(v727);
    float32x2_t v829 = vrev64_f32(v728);
    float32x2_t v836 = vrev64_f32(v729);
    float32x2_t v904 = vadd_f32(v895, v902);
    float32x2_t v909 = vsub_f32(v905, v906);
    float32x2_t v919 = vsub_f32(v903, v902);
    float32x2_t v928 = vadd_f32(v905, v906);
    float32x2_t v947 = vmul_f32(v911, v946);
    float32x2_t v951 = vmul_f32(v912, v950);
    float32x2_t v955 = vmul_f32(v913, v954);
    float32x2_t v959 = vmul_f32(v914, v958);
    float32x2_t v963 = vmul_f32(v915, v962);
    float32x2_t v967 = vmul_f32(v916, v966);
    float32x2_t v971 = vmul_f32(v917, v970);
    float32x2_t v975 = vmul_f32(v918, v974);
    float32x2_t v985 = vrev64_f32(v920);
    float32x2_t v992 = vrev64_f32(v921);
    float32x2_t v999 = vrev64_f32(v922);
    float32x2_t v1006 = vrev64_f32(v923);
    float32x2_t v1013 = vrev64_f32(v924);
    float32x2_t v1020 = vrev64_f32(v925);
    float32x2_t v1027 = vrev64_f32(v926);
    float32x2_t v1034 = vrev64_f32(v927);
    float32x2_t v709 = vadd_f32(v706, v705);
    float32x2_t v712 = vsub_f32(v711, v702);
    float32x2_t v781 = vmul_f32(v721, v978);
    float32x2_t v788 = vmul_f32(v787, v984);
    float32x2_t v795 = vmul_f32(v794, v991);
    float32x2_t v802 = vmul_f32(v801, v998);
    float32x2_t v809 = vmul_f32(v808, v1005);
    float32x2_t v816 = vmul_f32(v815, v1012);
    float32x2_t v823 = vmul_f32(v822, v1019);
    float32x2_t v830 = vmul_f32(v829, v1026);
    float32x2_t v837 = vmul_f32(v836, v1033);
    float32x2_t v843 = vrev64_f32(v730);
    float32x2_t v846 = vadd_f32(v749, v753);
    float32x2_t v847 = vadd_f32(v753, v757);
    float32x2_t v848 = vsub_f32(v749, v757);
    float32x2_t v849 = vadd_f32(v761, v765);
    float32x2_t v850 = vadd_f32(v765, v769);
    float32x2_t v851 = vsub_f32(v761, v769);
    float32x2_t v907 = vadd_f32(v904, v903);
    float32x2_t v910 = vsub_f32(v909, v900);
    float32x2_t v979 = vmul_f32(v919, v978);
    float32x2_t v986 = vmul_f32(v985, v984);
    float32x2_t v993 = vmul_f32(v992, v991);
    float32x2_t v1000 = vmul_f32(v999, v998);
    float32x2_t v1007 = vmul_f32(v1006, v1005);
    float32x2_t v1014 = vmul_f32(v1013, v1012);
    float32x2_t v1021 = vmul_f32(v1020, v1019);
    float32x2_t v1028 = vmul_f32(v1027, v1026);
    float32x2_t v1035 = vmul_f32(v1034, v1033);
    float32x2_t v1041 = vrev64_f32(v928);
    float32x2_t v1044 = vadd_f32(v947, v951);
    float32x2_t v1045 = vadd_f32(v951, v955);
    float32x2_t v1046 = vsub_f32(v947, v955);
    float32x2_t v1047 = vadd_f32(v959, v963);
    float32x2_t v1048 = vadd_f32(v963, v967);
    float32x2_t v1049 = vsub_f32(v959, v967);
    float32x2_t v710 = vadd_f32(v672, v709);
    float32x2_t v738 = vmul_f32(v709, v935);
    float32x2_t v744 = vrev64_f32(v712);
    float32x2_t v844 = vmul_f32(v843, v1040);
    float32x2_t v852 = vadd_f32(v777, v781);
    float32x2_t v853 = vadd_f32(v773, v781);
    float32x2_t v854 = vadd_f32(v795, v802);
    float32x2_t v855 = vsub_f32(v788, v802);
    float32x2_t v856 = vadd_f32(v816, v823);
    float32x2_t v857 = vsub_f32(v809, v823);
    float32x2_t v908 = vadd_f32(v673, v907);
    float32x2_t v936 = vmul_f32(v907, v935);
    float32x2_t v942 = vrev64_f32(v910);
    float32x2_t v1042 = vmul_f32(v1041, v1040);
    float32x2_t v1050 = vadd_f32(v975, v979);
    float32x2_t v1051 = vadd_f32(v971, v979);
    float32x2_t v1052 = vadd_f32(v993, v1000);
    float32x2_t v1053 = vsub_f32(v986, v1000);
    float32x2_t v1054 = vadd_f32(v1014, v1021);
    float32x2_t v1055 = vsub_f32(v1007, v1021);
    float32x2_t v745 = vmul_f32(v744, v941);
    float32x2_t v845 = vsub_f32(v710, v738);
    float32x2_t v858 = vadd_f32(v837, v844);
    float32x2_t v859 = vsub_f32(v830, v844);
    float32x2_t v860 = vadd_f32(v850, v852);
    float32x2_t v878 = vadd_f32(v854, v855);
    float32x2_t v943 = vmul_f32(v942, v941);
    float32x2_t v1043 = vsub_f32(v908, v936);
    float32x2_t v1056 = vadd_f32(v1035, v1042);
    float32x2_t v1057 = vsub_f32(v1028, v1042);
    float32x2_t v1058 = vadd_f32(v1048, v1050);
    float32x2_t v1076 = vadd_f32(v1052, v1053);
    v6[0] = v710;
    v6[ostride * 11] = v908;
    float32x2_t v861 = vadd_f32(v860, v845);
    float32x2_t v862 = vsub_f32(v845, v847);
    float32x2_t v864 = vadd_f32(v845, v851);
    float32x2_t v866 = vsub_f32(v845, v848);
    float32x2_t v868 = vadd_f32(v845, v846);
    float32x2_t v870 = vadd_f32(v745, v856);
    float32x2_t v872 = vsub_f32(v858, v854);
    float32x2_t v874 = vadd_f32(v745, v859);
    float32x2_t v876 = vsub_f32(v859, v855);
    float32x2_t v879 = vadd_f32(v878, v856);
    float32x2_t v1059 = vadd_f32(v1058, v1043);
    float32x2_t v1060 = vsub_f32(v1043, v1045);
    float32x2_t v1062 = vadd_f32(v1043, v1049);
    float32x2_t v1064 = vsub_f32(v1043, v1046);
    float32x2_t v1066 = vadd_f32(v1043, v1044);
    float32x2_t v1068 = vadd_f32(v943, v1054);
    float32x2_t v1070 = vsub_f32(v1056, v1052);
    float32x2_t v1072 = vadd_f32(v943, v1057);
    float32x2_t v1074 = vsub_f32(v1057, v1053);
    float32x2_t v1077 = vadd_f32(v1076, v1054);
    float32x2_t v863 = vsub_f32(v862, v852);
    float32x2_t v865 = vadd_f32(v864, v853);
    float32x2_t v867 = vsub_f32(v866, v853);
    float32x2_t v869 = vsub_f32(v868, v849);
    float32x2_t v871 = vadd_f32(v870, v858);
    float32x2_t v873 = vsub_f32(v872, v745);
    float32x2_t v875 = vadd_f32(v874, v857);
    float32x2_t v877 = vsub_f32(v876, v745);
    float32x2_t v880 = vadd_f32(v879, v857);
    float32x2_t v1061 = vsub_f32(v1060, v1050);
    float32x2_t v1063 = vadd_f32(v1062, v1051);
    float32x2_t v1065 = vsub_f32(v1064, v1051);
    float32x2_t v1067 = vsub_f32(v1066, v1047);
    float32x2_t v1069 = vadd_f32(v1068, v1056);
    float32x2_t v1071 = vsub_f32(v1070, v943);
    float32x2_t v1073 = vadd_f32(v1072, v1055);
    float32x2_t v1075 = vsub_f32(v1074, v943);
    float32x2_t v1078 = vadd_f32(v1077, v1055);
    float32x2_t v881 = vsub_f32(v880, v745);
    float32x2_t v883 = vadd_f32(v861, v871);
    float32x2_t v884 = vadd_f32(v863, v873);
    float32x2_t v885 = vsub_f32(v865, v875);
    float32x2_t v886 = vadd_f32(v867, v877);
    float32x2_t v887 = vsub_f32(v867, v877);
    float32x2_t v888 = vadd_f32(v865, v875);
    float32x2_t v889 = vsub_f32(v863, v873);
    float32x2_t v890 = vsub_f32(v861, v871);
    float32x2_t v1079 = vsub_f32(v1078, v943);
    float32x2_t v1081 = vadd_f32(v1059, v1069);
    float32x2_t v1082 = vadd_f32(v1061, v1071);
    float32x2_t v1083 = vsub_f32(v1063, v1073);
    float32x2_t v1084 = vadd_f32(v1065, v1075);
    float32x2_t v1085 = vsub_f32(v1065, v1075);
    float32x2_t v1086 = vadd_f32(v1063, v1073);
    float32x2_t v1087 = vsub_f32(v1061, v1071);
    float32x2_t v1088 = vsub_f32(v1059, v1069);
    float32x2_t v882 = vadd_f32(v869, v881);
    float32x2_t v891 = vsub_f32(v869, v881);
    float32x2_t v1080 = vadd_f32(v1067, v1079);
    float32x2_t v1089 = vsub_f32(v1067, v1079);
    v6[ostride * 2] = v890;
    v6[ostride * 13] = v1088;
    v6[ostride * 14] = v889;
    v6[ostride * 3] = v1087;
    v6[ostride * 4] = v888;
    v6[ostride * 15] = v1086;
    v6[ostride * 16] = v887;
    v6[ostride * 5] = v1085;
    v6[ostride * 6] = v886;
    v6[ostride * 17] = v1084;
    v6[ostride * 18] = v885;
    v6[ostride * 7] = v1083;
    v6[ostride * 8] = v884;
    v6[ostride * 19] = v1082;
    v6[ostride * 20] = v883;
    v6[ostride * 9] = v1081;
    v6[ostride * 12] = v891;
    v6[ostride] = v1089;
    v6[ostride * 10] = v882;
    v6[ostride * 21] = v1080;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs22(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v740 = 1.1000000000000001e+00F;
    float v745 = -3.3166247903554003e-01F;
    float v752 = 5.1541501300188641e-01F;
    float v757 = 9.4125353283118118e-01F;
    float v762 = 1.4143537075597825e+00F;
    float v767 = 8.5949297361449750e-01F;
    float v772 = 4.2314838273285138e-02F;
    float v777 = 3.8639279888589606e-01F;
    float v782 = 5.1254589567200015e-01F;
    float v787 = 1.0702757469471715e+00F;
    float v792 = 5.5486073394528512e-01F;
    float v797 = -1.2412944743900585e+00F;
    float v804 = -2.0897833842005756e-01F;
    float v811 = -3.7415717312460811e-01F;
    float v818 = -4.9929922194110327e-02F;
    float v825 = -6.5815896284539266e-01F;
    float v832 = -6.3306543373877577e-01F;
    float v839 = -1.0822460581641109e+00F;
    float v846 = -8.1720737907134022e-01F;
    float v853 = -4.2408709531871824e-01F;
    const float32x2_t *v1174 = &v5[v0];
    float32x2_t *v1337 = &v6[v2];
    int64_t v19 = v0 * 11;
    int64_t v34 = v10 * 10;
    int64_t v40 = v0 * 2;
    int64_t v54 = v0 * 13;
    int64_t v76 = v10 * 12;
    int64_t v82 = v0 * 4;
    int64_t v96 = v0 * 15;
    int64_t v111 = v10 * 3;
    int64_t v118 = v10 * 14;
    int64_t v124 = v0 * 6;
    int64_t v138 = v0 * 17;
    int64_t v153 = v10 * 5;
    int64_t v160 = v10 * 16;
    int64_t v166 = v0 * 8;
    int64_t v180 = v0 * 19;
    int64_t v195 = v10 * 7;
    int64_t v202 = v10 * 18;
    int64_t v208 = v0 * 10;
    int64_t v222 = v0 * 21;
    int64_t v237 = v10 * 9;
    int64_t v244 = v10 * 20;
    int64_t v250 = v0 * 12;
    int64_t v279 = v10 * 11;
    int64_t v292 = v0 * 14;
    int64_t v306 = v0 * 3;
    int64_t v321 = v10 * 13;
    int64_t v328 = v10 * 2;
    int64_t v334 = v0 * 16;
    int64_t v348 = v0 * 5;
    int64_t v363 = v10 * 15;
    int64_t v370 = v10 * 4;
    int64_t v376 = v0 * 18;
    int64_t v390 = v0 * 7;
    int64_t v405 = v10 * 17;
    int64_t v412 = v10 * 6;
    int64_t v418 = v0 * 20;
    int64_t v432 = v0 * 9;
    int64_t v447 = v10 * 19;
    int64_t v454 = v10 * 8;
    int64_t v455 = v13 * 21;
    float v748 = v4 * v745;
    float v800 = v4 * v797;
    float v807 = v4 * v804;
    float v814 = v4 * v811;
    float v821 = v4 * v818;
    float v828 = v4 * v825;
    float v835 = v4 * v832;
    float v842 = v4 * v839;
    float v849 = v4 * v846;
    float v856 = v4 * v853;
    int64_t v914 = v2 * 11;
    int64_t v921 = v2 * 12;
    int64_t v935 = v2 * 2;
    int64_t v942 = v2 * 13;
    int64_t v949 = v2 * 14;
    int64_t v956 = v2 * 3;
    int64_t v963 = v2 * 4;
    int64_t v970 = v2 * 15;
    int64_t v977 = v2 * 16;
    int64_t v984 = v2 * 5;
    int64_t v991 = v2 * 6;
    int64_t v998 = v2 * 17;
    int64_t v1005 = v2 * 18;
    int64_t v1012 = v2 * 7;
    int64_t v1019 = v2 * 8;
    int64_t v1026 = v2 * 19;
    int64_t v1033 = v2 * 20;
    int64_t v1040 = v2 * 9;
    int64_t v1047 = v2 * 10;
    int64_t v1054 = v2 * 21;
    const float32x2_t *v1258 = &v5[0];
    svint64_t v1259 = svindex_s64(0, v1);
    svfloat32_t v1283 = svdup_n_f32(v740);
    svfloat32_t v1285 = svdup_n_f32(v752);
    svfloat32_t v1286 = svdup_n_f32(v757);
    svfloat32_t v1287 = svdup_n_f32(v762);
    svfloat32_t v1288 = svdup_n_f32(v767);
    svfloat32_t v1289 = svdup_n_f32(v772);
    svfloat32_t v1290 = svdup_n_f32(v777);
    svfloat32_t v1291 = svdup_n_f32(v782);
    svfloat32_t v1292 = svdup_n_f32(v787);
    svfloat32_t v1293 = svdup_n_f32(v792);
    float32x2_t *v1310 = &v6[0];
    svint64_t v1500 = svindex_s64(0, v3);
    int64_t v36 = v34 + v455;
    int64_t v71 = v10 + v455;
    int64_t v78 = v76 + v455;
    int64_t v113 = v111 + v455;
    int64_t v120 = v118 + v455;
    int64_t v155 = v153 + v455;
    int64_t v162 = v160 + v455;
    int64_t v197 = v195 + v455;
    int64_t v204 = v202 + v455;
    int64_t v239 = v237 + v455;
    int64_t v246 = v244 + v455;
    int64_t v281 = v279 + v455;
    svfloat32_t v289 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v455]));
    int64_t v323 = v321 + v455;
    int64_t v330 = v328 + v455;
    int64_t v365 = v363 + v455;
    int64_t v372 = v370 + v455;
    int64_t v407 = v405 + v455;
    int64_t v414 = v412 + v455;
    int64_t v449 = v447 + v455;
    int64_t v456 = v454 + v455;
    const float32x2_t *v1066 = &v5[v19];
    const float32x2_t *v1075 = &v5[v40];
    const float32x2_t *v1084 = &v5[v54];
    const float32x2_t *v1093 = &v5[v82];
    const float32x2_t *v1102 = &v5[v96];
    const float32x2_t *v1111 = &v5[v124];
    const float32x2_t *v1120 = &v5[v138];
    const float32x2_t *v1129 = &v5[v166];
    const float32x2_t *v1138 = &v5[v180];
    const float32x2_t *v1147 = &v5[v208];
    const float32x2_t *v1156 = &v5[v222];
    const float32x2_t *v1165 = &v5[v250];
    svfloat32_t v1176 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1174), v1259));
    const float32x2_t *v1185 = &v5[v292];
    const float32x2_t *v1194 = &v5[v306];
    const float32x2_t *v1203 = &v5[v334];
    const float32x2_t *v1212 = &v5[v348];
    const float32x2_t *v1221 = &v5[v376];
    const float32x2_t *v1230 = &v5[v390];
    const float32x2_t *v1239 = &v5[v418];
    const float32x2_t *v1248 = &v5[v432];
    svfloat32_t v1260 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1258), v1259));
    svfloat32_t v1284 = svdup_n_f32(v748);
    svfloat32_t v1294 = svdup_n_f32(v800);
    svfloat32_t v1295 = svdup_n_f32(v807);
    svfloat32_t v1296 = svdup_n_f32(v814);
    svfloat32_t v1297 = svdup_n_f32(v821);
    svfloat32_t v1298 = svdup_n_f32(v828);
    svfloat32_t v1299 = svdup_n_f32(v835);
    svfloat32_t v1300 = svdup_n_f32(v842);
    svfloat32_t v1301 = svdup_n_f32(v849);
    svfloat32_t v1302 = svdup_n_f32(v856);
    float32x2_t *v1319 = &v6[v914];
    float32x2_t *v1328 = &v6[v921];
    float32x2_t *v1346 = &v6[v935];
    float32x2_t *v1355 = &v6[v942];
    float32x2_t *v1364 = &v6[v949];
    float32x2_t *v1373 = &v6[v956];
    float32x2_t *v1382 = &v6[v963];
    float32x2_t *v1391 = &v6[v970];
    float32x2_t *v1400 = &v6[v977];
    float32x2_t *v1409 = &v6[v984];
    float32x2_t *v1418 = &v6[v991];
    float32x2_t *v1427 = &v6[v998];
    float32x2_t *v1436 = &v6[v1005];
    float32x2_t *v1445 = &v6[v1012];
    float32x2_t *v1454 = &v6[v1019];
    float32x2_t *v1463 = &v6[v1026];
    float32x2_t *v1472 = &v6[v1033];
    float32x2_t *v1481 = &v6[v1040];
    float32x2_t *v1490 = &v6[v1047];
    float32x2_t *v1499 = &v6[v1054];
    svfloat32_t v37 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v36]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t v79 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v78]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v121 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v120]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v163 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v162]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v247 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v246]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t zero290 = svdup_n_f32(0);
    svfloat32_t v290 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero290, v1176, v289, 0), v1176,
        v289, 90);
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v331 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v330]));
    svfloat32_t v366 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v365]));
    svfloat32_t v373 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v372]));
    svfloat32_t v408 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v407]));
    svfloat32_t v415 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v414]));
    svfloat32_t v450 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v449]));
    svfloat32_t v457 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v456]));
    svfloat32_t v1068 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1066), v1259));
    svfloat32_t v1077 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1075), v1259));
    svfloat32_t v1086 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1084), v1259));
    svfloat32_t v1095 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1093), v1259));
    svfloat32_t v1104 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1102), v1259));
    svfloat32_t v1113 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1111), v1259));
    svfloat32_t v1122 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1120), v1259));
    svfloat32_t v1131 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1129), v1259));
    svfloat32_t v1140 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1138), v1259));
    svfloat32_t v1149 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1147), v1259));
    svfloat32_t v1158 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1156), v1259));
    svfloat32_t v1167 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1165), v1259));
    svfloat32_t v1187 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1185), v1259));
    svfloat32_t v1196 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1194), v1259));
    svfloat32_t v1205 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1203), v1259));
    svfloat32_t v1214 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1212), v1259));
    svfloat32_t v1223 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1221), v1259));
    svfloat32_t v1232 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1230), v1259));
    svfloat32_t v1241 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1239), v1259));
    svfloat32_t v1250 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1248), v1259));
    svfloat32_t zero38 = svdup_n_f32(0);
    svfloat32_t v38 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero38, v1068, v37, 0),
                     v1068, v37, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v1077, v72, 0),
                     v1077, v72, 90);
    svfloat32_t zero80 = svdup_n_f32(0);
    svfloat32_t v80 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero80, v1086, v79, 0),
                     v1086, v79, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero115, v1095, v114, 0), v1095,
        v114, 90);
    svfloat32_t zero122 = svdup_n_f32(0);
    svfloat32_t v122 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero122, v1104, v121, 0), v1104,
        v121, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero157, v1113, v156, 0), v1113,
        v156, 90);
    svfloat32_t zero164 = svdup_n_f32(0);
    svfloat32_t v164 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero164, v1122, v163, 0), v1122,
        v163, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero199, v1131, v198, 0), v1131,
        v198, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero206, v1140, v205, 0), v1140,
        v205, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero241, v1149, v240, 0), v1149,
        v240, 90);
    svfloat32_t zero248 = svdup_n_f32(0);
    svfloat32_t v248 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero248, v1158, v247, 0), v1158,
        v247, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero283, v1167, v282, 0), v1167,
        v282, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero325, v1187, v324, 0), v1187,
        v324, 90);
    svfloat32_t zero332 = svdup_n_f32(0);
    svfloat32_t v332 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero332, v1196, v331, 0), v1196,
        v331, 90);
    svfloat32_t zero367 = svdup_n_f32(0);
    svfloat32_t v367 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero367, v1205, v366, 0), v1205,
        v366, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero374, v1214, v373, 0), v1214,
        v373, 90);
    svfloat32_t zero409 = svdup_n_f32(0);
    svfloat32_t v409 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero409, v1223, v408, 0), v1223,
        v408, 90);
    svfloat32_t zero416 = svdup_n_f32(0);
    svfloat32_t v416 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero416, v1232, v415, 0), v1232,
        v415, 90);
    svfloat32_t zero451 = svdup_n_f32(0);
    svfloat32_t v451 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero451, v1241, v450, 0), v1241,
        v450, 90);
    svfloat32_t zero458 = svdup_n_f32(0);
    svfloat32_t v458 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero458, v1250, v457, 0), v1250,
        v457, 90);
    svfloat32_t v466 = svadd_f32_x(svptrue_b32(), v1260, v38);
    svfloat32_t v467 = svsub_f32_x(svptrue_b32(), v1260, v38);
    svfloat32_t v468 = svadd_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v469 = svsub_f32_x(svptrue_b32(), v73, v80);
    svfloat32_t v470 = svadd_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v471 = svsub_f32_x(svptrue_b32(), v115, v122);
    svfloat32_t v472 = svadd_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v473 = svsub_f32_x(svptrue_b32(), v157, v164);
    svfloat32_t v474 = svadd_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v199, v206);
    svfloat32_t v476 = svadd_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v477 = svsub_f32_x(svptrue_b32(), v241, v248);
    svfloat32_t v478 = svadd_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v479 = svsub_f32_x(svptrue_b32(), v283, v290);
    svfloat32_t v480 = svadd_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v481 = svsub_f32_x(svptrue_b32(), v325, v332);
    svfloat32_t v482 = svadd_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v483 = svsub_f32_x(svptrue_b32(), v367, v374);
    svfloat32_t v484 = svadd_f32_x(svptrue_b32(), v409, v416);
    svfloat32_t v485 = svsub_f32_x(svptrue_b32(), v409, v416);
    svfloat32_t v486 = svadd_f32_x(svptrue_b32(), v451, v458);
    svfloat32_t v487 = svsub_f32_x(svptrue_b32(), v451, v458);
    svfloat32_t v488 = svadd_f32_x(svptrue_b32(), v468, v486);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v470, v484);
    svfloat32_t v490 = svadd_f32_x(svptrue_b32(), v472, v482);
    svfloat32_t v491 = svadd_f32_x(svptrue_b32(), v474, v480);
    svfloat32_t v492 = svadd_f32_x(svptrue_b32(), v476, v478);
    svfloat32_t v493 = svsub_f32_x(svptrue_b32(), v468, v486);
    svfloat32_t v494 = svsub_f32_x(svptrue_b32(), v470, v484);
    svfloat32_t v495 = svsub_f32_x(svptrue_b32(), v472, v482);
    svfloat32_t v496 = svsub_f32_x(svptrue_b32(), v474, v480);
    svfloat32_t v497 = svsub_f32_x(svptrue_b32(), v476, v478);
    svfloat32_t v697 = svadd_f32_x(svptrue_b32(), v469, v487);
    svfloat32_t v698 = svadd_f32_x(svptrue_b32(), v471, v485);
    svfloat32_t v699 = svadd_f32_x(svptrue_b32(), v473, v483);
    svfloat32_t v700 = svadd_f32_x(svptrue_b32(), v475, v481);
    svfloat32_t v701 = svadd_f32_x(svptrue_b32(), v477, v479);
    svfloat32_t v702 = svsub_f32_x(svptrue_b32(), v469, v487);
    svfloat32_t v703 = svsub_f32_x(svptrue_b32(), v471, v485);
    svfloat32_t v704 = svsub_f32_x(svptrue_b32(), v473, v483);
    svfloat32_t v705 = svsub_f32_x(svptrue_b32(), v475, v481);
    svfloat32_t v706 = svsub_f32_x(svptrue_b32(), v477, v479);
    svfloat32_t v498 = svadd_f32_x(svptrue_b32(), v488, v489);
    svfloat32_t v499 = svadd_f32_x(svptrue_b32(), v490, v492);
    svfloat32_t v501 = svsub_f32_x(svptrue_b32(), v494, v495);
    svfloat32_t v502 = svadd_f32_x(svptrue_b32(), v493, v497);
    svfloat32_t v507 = svsub_f32_x(svptrue_b32(), v489, v491);
    svfloat32_t v508 = svsub_f32_x(svptrue_b32(), v488, v491);
    svfloat32_t v509 = svsub_f32_x(svptrue_b32(), v489, v488);
    svfloat32_t v510 = svsub_f32_x(svptrue_b32(), v492, v491);
    svfloat32_t v511 = svsub_f32_x(svptrue_b32(), v490, v491);
    svfloat32_t v512 = svsub_f32_x(svptrue_b32(), v492, v490);
    svfloat32_t v513 = svsub_f32_x(svptrue_b32(), v489, v492);
    svfloat32_t v514 = svsub_f32_x(svptrue_b32(), v488, v490);
    svfloat32_t v516 = svadd_f32_x(svptrue_b32(), v494, v496);
    svfloat32_t v517 = svsub_f32_x(svptrue_b32(), v493, v496);
    svfloat32_t v518 = svadd_f32_x(svptrue_b32(), v493, v494);
    svfloat32_t v519 = svsub_f32_x(svptrue_b32(), v496, v497);
    svfloat32_t v520 = svsub_f32_x(svptrue_b32(), v495, v496);
    svfloat32_t v521 = svsub_f32_x(svptrue_b32(), v495, v497);
    svfloat32_t v522 = svadd_f32_x(svptrue_b32(), v494, v497);
    svfloat32_t v523 = svsub_f32_x(svptrue_b32(), v493, v495);
    svfloat32_t v707 = svadd_f32_x(svptrue_b32(), v697, v698);
    svfloat32_t v708 = svadd_f32_x(svptrue_b32(), v699, v701);
    svfloat32_t v710 = svsub_f32_x(svptrue_b32(), v703, v704);
    svfloat32_t v711 = svadd_f32_x(svptrue_b32(), v702, v706);
    svfloat32_t v716 = svsub_f32_x(svptrue_b32(), v698, v700);
    svfloat32_t v717 = svsub_f32_x(svptrue_b32(), v697, v700);
    svfloat32_t v718 = svsub_f32_x(svptrue_b32(), v698, v697);
    svfloat32_t v719 = svsub_f32_x(svptrue_b32(), v701, v700);
    svfloat32_t v720 = svsub_f32_x(svptrue_b32(), v699, v700);
    svfloat32_t v721 = svsub_f32_x(svptrue_b32(), v701, v699);
    svfloat32_t v722 = svsub_f32_x(svptrue_b32(), v698, v701);
    svfloat32_t v723 = svsub_f32_x(svptrue_b32(), v697, v699);
    svfloat32_t v725 = svadd_f32_x(svptrue_b32(), v703, v705);
    svfloat32_t v726 = svsub_f32_x(svptrue_b32(), v702, v705);
    svfloat32_t v727 = svadd_f32_x(svptrue_b32(), v702, v703);
    svfloat32_t v728 = svsub_f32_x(svptrue_b32(), v705, v706);
    svfloat32_t v729 = svsub_f32_x(svptrue_b32(), v704, v705);
    svfloat32_t v730 = svsub_f32_x(svptrue_b32(), v704, v706);
    svfloat32_t v731 = svadd_f32_x(svptrue_b32(), v703, v706);
    svfloat32_t v732 = svsub_f32_x(svptrue_b32(), v702, v704);
    svfloat32_t v500 = svadd_f32_x(svptrue_b32(), v491, v498);
    svfloat32_t v505 = svsub_f32_x(svptrue_b32(), v501, v502);
    svfloat32_t v515 = svsub_f32_x(svptrue_b32(), v499, v498);
    svfloat32_t v524 = svadd_f32_x(svptrue_b32(), v501, v502);
    svfloat32_t v551 = svmul_f32_x(svptrue_b32(), v508, v1286);
    svfloat32_t v556 = svmul_f32_x(svptrue_b32(), v509, v1287);
    svfloat32_t v566 = svmul_f32_x(svptrue_b32(), v511, v1289);
    svfloat32_t v571 = svmul_f32_x(svptrue_b32(), v512, v1290);
    svfloat32_t zero593 = svdup_n_f32(0);
    svfloat32_t v593 = svcmla_f32_x(pred_full, zero593, v1294, v516, 90);
    svfloat32_t zero607 = svdup_n_f32(0);
    svfloat32_t v607 = svcmla_f32_x(pred_full, zero607, v1296, v518, 90);
    svfloat32_t zero614 = svdup_n_f32(0);
    svfloat32_t v614 = svcmla_f32_x(pred_full, zero614, v1297, v519, 90);
    svfloat32_t zero628 = svdup_n_f32(0);
    svfloat32_t v628 = svcmla_f32_x(pred_full, zero628, v1299, v521, 90);
    svfloat32_t zero635 = svdup_n_f32(0);
    svfloat32_t v635 = svcmla_f32_x(pred_full, zero635, v1300, v522, 90);
    svfloat32_t v709 = svadd_f32_x(svptrue_b32(), v700, v707);
    svfloat32_t v714 = svsub_f32_x(svptrue_b32(), v710, v711);
    svfloat32_t v724 = svsub_f32_x(svptrue_b32(), v708, v707);
    svfloat32_t v733 = svadd_f32_x(svptrue_b32(), v710, v711);
    svfloat32_t v760 = svmul_f32_x(svptrue_b32(), v717, v1286);
    svfloat32_t v765 = svmul_f32_x(svptrue_b32(), v718, v1287);
    svfloat32_t v775 = svmul_f32_x(svptrue_b32(), v720, v1289);
    svfloat32_t v780 = svmul_f32_x(svptrue_b32(), v721, v1290);
    svfloat32_t zero802 = svdup_n_f32(0);
    svfloat32_t v802 = svcmla_f32_x(pred_full, zero802, v1294, v725, 90);
    svfloat32_t zero816 = svdup_n_f32(0);
    svfloat32_t v816 = svcmla_f32_x(pred_full, zero816, v1296, v727, 90);
    svfloat32_t zero823 = svdup_n_f32(0);
    svfloat32_t v823 = svcmla_f32_x(pred_full, zero823, v1297, v728, 90);
    svfloat32_t zero837 = svdup_n_f32(0);
    svfloat32_t v837 = svcmla_f32_x(pred_full, zero837, v1299, v730, 90);
    svfloat32_t zero844 = svdup_n_f32(0);
    svfloat32_t v844 = svcmla_f32_x(pred_full, zero844, v1300, v731, 90);
    svfloat32_t v503 = svadd_f32_x(svptrue_b32(), v500, v499);
    svfloat32_t v506 = svsub_f32_x(svptrue_b32(), v505, v496);
    svfloat32_t v586 = svmul_f32_x(svptrue_b32(), v515, v1293);
    svfloat32_t zero649 = svdup_n_f32(0);
    svfloat32_t v649 = svcmla_f32_x(pred_full, zero649, v1302, v524, 90);
    svfloat32_t v651 = svmla_f32_x(pred_full, v551, v507, v1285);
    svfloat32_t v652 = svmla_f32_x(pred_full, v556, v508, v1286);
    svfloat32_t v653 = svnmls_f32_x(pred_full, v556, v507, v1285);
    svfloat32_t v654 = svmla_f32_x(pred_full, v566, v510, v1288);
    svfloat32_t v655 = svmla_f32_x(pred_full, v571, v511, v1289);
    svfloat32_t v656 = svnmls_f32_x(pred_full, v571, v510, v1288);
    svfloat32_t v659 = svcmla_f32_x(pred_full, v607, v1295, v517, 90);
    svfloat32_t v660 = svsub_f32_x(svptrue_b32(), v593, v607);
    svfloat32_t v661 = svcmla_f32_x(pred_full, v628, v1298, v520, 90);
    svfloat32_t v662 = svsub_f32_x(svptrue_b32(), v614, v628);
    svfloat32_t v712 = svadd_f32_x(svptrue_b32(), v709, v708);
    svfloat32_t v715 = svsub_f32_x(svptrue_b32(), v714, v705);
    svfloat32_t v795 = svmul_f32_x(svptrue_b32(), v724, v1293);
    svfloat32_t zero858 = svdup_n_f32(0);
    svfloat32_t v858 = svcmla_f32_x(pred_full, zero858, v1302, v733, 90);
    svfloat32_t v860 = svmla_f32_x(pred_full, v760, v716, v1285);
    svfloat32_t v861 = svmla_f32_x(pred_full, v765, v717, v1286);
    svfloat32_t v862 = svnmls_f32_x(pred_full, v765, v716, v1285);
    svfloat32_t v863 = svmla_f32_x(pred_full, v775, v719, v1288);
    svfloat32_t v864 = svmla_f32_x(pred_full, v780, v720, v1289);
    svfloat32_t v865 = svnmls_f32_x(pred_full, v780, v719, v1288);
    svfloat32_t v868 = svcmla_f32_x(pred_full, v816, v1295, v726, 90);
    svfloat32_t v869 = svsub_f32_x(svptrue_b32(), v802, v816);
    svfloat32_t v870 = svcmla_f32_x(pred_full, v837, v1298, v729, 90);
    svfloat32_t v871 = svsub_f32_x(svptrue_b32(), v823, v837);
    svfloat32_t v504 = svadd_f32_x(svptrue_b32(), v466, v503);
    svfloat32_t zero541 = svdup_n_f32(0);
    svfloat32_t v541 = svcmla_f32_x(pred_full, zero541, v1284, v506, 90);
    svfloat32_t v657 = svmla_f32_x(pred_full, v586, v514, v1292);
    svfloat32_t v658 = svmla_f32_x(pred_full, v586, v513, v1291);
    svfloat32_t v663 = svcmla_f32_x(pred_full, v649, v1301, v523, 90);
    svfloat32_t v664 = svsub_f32_x(svptrue_b32(), v635, v649);
    svfloat32_t v683 = svadd_f32_x(svptrue_b32(), v659, v660);
    svfloat32_t v713 = svadd_f32_x(svptrue_b32(), v467, v712);
    svfloat32_t zero750 = svdup_n_f32(0);
    svfloat32_t v750 = svcmla_f32_x(pred_full, zero750, v1284, v715, 90);
    svfloat32_t v866 = svmla_f32_x(pred_full, v795, v723, v1292);
    svfloat32_t v867 = svmla_f32_x(pred_full, v795, v722, v1291);
    svfloat32_t v872 = svcmla_f32_x(pred_full, v858, v1301, v732, 90);
    svfloat32_t v873 = svsub_f32_x(svptrue_b32(), v844, v858);
    svfloat32_t v892 = svadd_f32_x(svptrue_b32(), v868, v869);
    svfloat32_t v650 = svmls_f32_x(pred_full, v504, v503, v1283);
    svfloat32_t v665 = svadd_f32_x(svptrue_b32(), v655, v657);
    svfloat32_t v675 = svadd_f32_x(svptrue_b32(), v541, v661);
    svfloat32_t v677 = svsub_f32_x(svptrue_b32(), v663, v659);
    svfloat32_t v679 = svadd_f32_x(svptrue_b32(), v541, v664);
    svfloat32_t v681 = svsub_f32_x(svptrue_b32(), v664, v660);
    svfloat32_t v684 = svadd_f32_x(svptrue_b32(), v683, v661);
    svfloat32_t v859 = svmls_f32_x(pred_full, v713, v712, v1283);
    svfloat32_t v874 = svadd_f32_x(svptrue_b32(), v864, v866);
    svfloat32_t v884 = svadd_f32_x(svptrue_b32(), v750, v870);
    svfloat32_t v886 = svsub_f32_x(svptrue_b32(), v872, v868);
    svfloat32_t v888 = svadd_f32_x(svptrue_b32(), v750, v873);
    svfloat32_t v890 = svsub_f32_x(svptrue_b32(), v873, v869);
    svfloat32_t v893 = svadd_f32_x(svptrue_b32(), v892, v870);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1310), v1500,
                               svreinterpret_f64_f32(v504));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1319), v1500,
                               svreinterpret_f64_f32(v713));
    svfloat32_t v666 = svadd_f32_x(svptrue_b32(), v665, v650);
    svfloat32_t v667 = svsub_f32_x(svptrue_b32(), v650, v652);
    svfloat32_t v669 = svadd_f32_x(svptrue_b32(), v650, v656);
    svfloat32_t v671 = svsub_f32_x(svptrue_b32(), v650, v653);
    svfloat32_t v673 = svadd_f32_x(svptrue_b32(), v650, v651);
    svfloat32_t v676 = svadd_f32_x(svptrue_b32(), v675, v663);
    svfloat32_t v678 = svsub_f32_x(svptrue_b32(), v677, v541);
    svfloat32_t v680 = svadd_f32_x(svptrue_b32(), v679, v662);
    svfloat32_t v682 = svsub_f32_x(svptrue_b32(), v681, v541);
    svfloat32_t v685 = svadd_f32_x(svptrue_b32(), v684, v662);
    svfloat32_t v875 = svadd_f32_x(svptrue_b32(), v874, v859);
    svfloat32_t v876 = svsub_f32_x(svptrue_b32(), v859, v861);
    svfloat32_t v878 = svadd_f32_x(svptrue_b32(), v859, v865);
    svfloat32_t v880 = svsub_f32_x(svptrue_b32(), v859, v862);
    svfloat32_t v882 = svadd_f32_x(svptrue_b32(), v859, v860);
    svfloat32_t v885 = svadd_f32_x(svptrue_b32(), v884, v872);
    svfloat32_t v887 = svsub_f32_x(svptrue_b32(), v886, v750);
    svfloat32_t v889 = svadd_f32_x(svptrue_b32(), v888, v871);
    svfloat32_t v891 = svsub_f32_x(svptrue_b32(), v890, v750);
    svfloat32_t v894 = svadd_f32_x(svptrue_b32(), v893, v871);
    svfloat32_t v668 = svsub_f32_x(svptrue_b32(), v667, v657);
    svfloat32_t v670 = svadd_f32_x(svptrue_b32(), v669, v658);
    svfloat32_t v672 = svsub_f32_x(svptrue_b32(), v671, v658);
    svfloat32_t v674 = svsub_f32_x(svptrue_b32(), v673, v654);
    svfloat32_t v686 = svsub_f32_x(svptrue_b32(), v685, v541);
    svfloat32_t v688 = svadd_f32_x(svptrue_b32(), v666, v676);
    svfloat32_t v695 = svsub_f32_x(svptrue_b32(), v666, v676);
    svfloat32_t v877 = svsub_f32_x(svptrue_b32(), v876, v866);
    svfloat32_t v879 = svadd_f32_x(svptrue_b32(), v878, v867);
    svfloat32_t v881 = svsub_f32_x(svptrue_b32(), v880, v867);
    svfloat32_t v883 = svsub_f32_x(svptrue_b32(), v882, v863);
    svfloat32_t v895 = svsub_f32_x(svptrue_b32(), v894, v750);
    svfloat32_t v897 = svadd_f32_x(svptrue_b32(), v875, v885);
    svfloat32_t v904 = svsub_f32_x(svptrue_b32(), v875, v885);
    svfloat32_t v687 = svadd_f32_x(svptrue_b32(), v674, v686);
    svfloat32_t v689 = svadd_f32_x(svptrue_b32(), v668, v678);
    svfloat32_t v690 = svsub_f32_x(svptrue_b32(), v670, v680);
    svfloat32_t v691 = svadd_f32_x(svptrue_b32(), v672, v682);
    svfloat32_t v692 = svsub_f32_x(svptrue_b32(), v672, v682);
    svfloat32_t v693 = svadd_f32_x(svptrue_b32(), v670, v680);
    svfloat32_t v694 = svsub_f32_x(svptrue_b32(), v668, v678);
    svfloat32_t v696 = svsub_f32_x(svptrue_b32(), v674, v686);
    svfloat32_t v896 = svadd_f32_x(svptrue_b32(), v883, v895);
    svfloat32_t v898 = svadd_f32_x(svptrue_b32(), v877, v887);
    svfloat32_t v899 = svsub_f32_x(svptrue_b32(), v879, v889);
    svfloat32_t v900 = svadd_f32_x(svptrue_b32(), v881, v891);
    svfloat32_t v901 = svsub_f32_x(svptrue_b32(), v881, v891);
    svfloat32_t v902 = svadd_f32_x(svptrue_b32(), v879, v889);
    svfloat32_t v903 = svsub_f32_x(svptrue_b32(), v877, v887);
    svfloat32_t v905 = svsub_f32_x(svptrue_b32(), v883, v895);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1346), v1500,
                               svreinterpret_f64_f32(v695));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1355), v1500,
                               svreinterpret_f64_f32(v904));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1472), v1500,
                               svreinterpret_f64_f32(v688));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1481), v1500,
                               svreinterpret_f64_f32(v897));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1328), v1500,
                               svreinterpret_f64_f32(v696));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1337), v1500,
                               svreinterpret_f64_f32(v905));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1364), v1500,
                               svreinterpret_f64_f32(v694));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1373), v1500,
                               svreinterpret_f64_f32(v903));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1382), v1500,
                               svreinterpret_f64_f32(v693));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1391), v1500,
                               svreinterpret_f64_f32(v902));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1400), v1500,
                               svreinterpret_f64_f32(v692));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1409), v1500,
                               svreinterpret_f64_f32(v901));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1418), v1500,
                               svreinterpret_f64_f32(v691));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1427), v1500,
                               svreinterpret_f64_f32(v900));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1436), v1500,
                               svreinterpret_f64_f32(v690));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1445), v1500,
                               svreinterpret_f64_f32(v899));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1454), v1500,
                               svreinterpret_f64_f32(v689));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1463), v1500,
                               svreinterpret_f64_f32(v898));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1490), v1500,
                               svreinterpret_f64_f32(v687));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1499), v1500,
                               svreinterpret_f64_f32(v896));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs24(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v260 = v5[istride];
    float v706 = 1.0000000000000000e+00F;
    float v707 = -1.0000000000000000e+00F;
    float v714 = -7.0710678118654746e-01F;
    float v721 = 7.0710678118654757e-01F;
    float v773 = -1.4999999999999998e+00F;
    float v774 = 1.4999999999999998e+00F;
    float v781 = 1.0606601717798210e+00F;
    float v788 = -1.0606601717798212e+00F;
    float v842 = 8.6602540378443871e-01F;
    float v850 = -8.6602540378443871e-01F;
    float v857 = 6.1237243569579458e-01F;
    float v858 = -6.1237243569579458e-01F;
    float32x2_t v860 = (float32x2_t){v4, v4};
    float32x2_t v292 = vtrn1_f32(v260, v260);
    float32x2_t v293 = vtrn2_f32(v260, v260);
    float32x2_t v644 = v5[0];
    float32x2_t v708 = (float32x2_t){v706, v707};
    float32x2_t v715 = (float32x2_t){v721, v714};
    float32x2_t v722 = (float32x2_t){v721, v721};
    float32x2_t v771 = (float32x2_t){v773, v773};
    float32x2_t v775 = (float32x2_t){v773, v774};
    float32x2_t v782 = (float32x2_t){v788, v781};
    float32x2_t v789 = (float32x2_t){v788, v788};
    float32x2_t v844 = (float32x2_t){v842, v850};
    float32x2_t v851 = (float32x2_t){v850, v850};
    float32x2_t v855 = (float32x2_t){v858, v858};
    float32x2_t v859 = (float32x2_t){v857, v858};
    float32x2_t v20 = v5[istride * 8];
    float32x2_t v38 = v5[istride * 16];
    int64_t v55 = 14 + j * 46;
    int64_t v68 = 30 + j * 46;
    float32x2_t v82 = v5[istride * 11];
    float32x2_t v100 = v5[istride * 19];
    int64_t v117 = 20 + j * 46;
    int64_t v130 = 36 + j * 46;
    float32x2_t v144 = v5[istride * 3];
    int64_t v148 = 4 + j * 46;
    float32x2_t v162 = v5[istride * 14];
    float32x2_t v180 = v5[istride * 22];
    int64_t v197 = 26 + j * 46;
    int64_t v210 = 42 + j * 46;
    float32x2_t v224 = v5[istride * 6];
    int64_t v228 = 10 + j * 46;
    float32x2_t v242 = v5[istride * 17];
    int64_t v277 = 32 + j * 46;
    float32x2_t v291 = v7[j * 46];
    int64_t v295 = j * 46 + 1;
    float32x2_t v304 = v5[istride * 9];
    int64_t v308 = 16 + j * 46;
    float32x2_t v322 = v5[istride * 20];
    float32x2_t v340 = v5[istride * 4];
    int64_t v357 = 38 + j * 46;
    int64_t v370 = 6 + j * 46;
    float32x2_t v384 = v5[istride * 12];
    int64_t v388 = 22 + j * 46;
    float32x2_t v402 = v5[istride * 23];
    float32x2_t v420 = v5[istride * 7];
    int64_t v437 = 44 + j * 46;
    int64_t v450 = 12 + j * 46;
    float32x2_t v464 = v5[istride * 15];
    int64_t v468 = 28 + j * 46;
    float32x2_t v482 = v5[istride * 2];
    float32x2_t v500 = v5[istride * 10];
    int64_t v517 = 2 + j * 46;
    int64_t v530 = 18 + j * 46;
    float32x2_t v544 = v5[istride * 18];
    int64_t v548 = 34 + j * 46;
    float32x2_t v562 = v5[istride * 5];
    float32x2_t v580 = v5[istride * 13];
    int64_t v597 = 8 + j * 46;
    int64_t v610 = 24 + j * 46;
    float32x2_t v624 = v5[istride * 21];
    int64_t v628 = 40 + j * 46;
    float32x2_t v710 = vmul_f32(v860, v708);
    float32x2_t v717 = vmul_f32(v860, v715);
    float32x2_t v777 = vmul_f32(v860, v775);
    float32x2_t v784 = vmul_f32(v860, v782);
    float32x2_t v846 = vmul_f32(v860, v844);
    float32x2_t v861 = vmul_f32(v860, v859);
    float32x2_t v56 = v7[v55];
    float32x2_t v57 = vtrn1_f32(v20, v20);
    float32x2_t v58 = vtrn2_f32(v20, v20);
    int64_t v60 = v55 + 1;
    float32x2_t v69 = v7[v68];
    float32x2_t v70 = vtrn1_f32(v38, v38);
    float32x2_t v71 = vtrn2_f32(v38, v38);
    int64_t v73 = v68 + 1;
    float32x2_t v118 = v7[v117];
    float32x2_t v119 = vtrn1_f32(v82, v82);
    float32x2_t v120 = vtrn2_f32(v82, v82);
    int64_t v122 = v117 + 1;
    float32x2_t v131 = v7[v130];
    float32x2_t v132 = vtrn1_f32(v100, v100);
    float32x2_t v133 = vtrn2_f32(v100, v100);
    int64_t v135 = v130 + 1;
    float32x2_t v149 = v7[v148];
    float32x2_t v150 = vtrn1_f32(v144, v144);
    float32x2_t v151 = vtrn2_f32(v144, v144);
    int64_t v153 = v148 + 1;
    float32x2_t v198 = v7[v197];
    float32x2_t v199 = vtrn1_f32(v162, v162);
    float32x2_t v200 = vtrn2_f32(v162, v162);
    int64_t v202 = v197 + 1;
    float32x2_t v211 = v7[v210];
    float32x2_t v212 = vtrn1_f32(v180, v180);
    float32x2_t v213 = vtrn2_f32(v180, v180);
    int64_t v215 = v210 + 1;
    float32x2_t v229 = v7[v228];
    float32x2_t v230 = vtrn1_f32(v224, v224);
    float32x2_t v231 = vtrn2_f32(v224, v224);
    int64_t v233 = v228 + 1;
    float32x2_t v278 = v7[v277];
    float32x2_t v279 = vtrn1_f32(v242, v242);
    float32x2_t v280 = vtrn2_f32(v242, v242);
    int64_t v282 = v277 + 1;
    float32x2_t v296 = v7[v295];
    float32x2_t v297 = vmul_f32(v292, v291);
    float32x2_t v309 = v7[v308];
    float32x2_t v310 = vtrn1_f32(v304, v304);
    float32x2_t v311 = vtrn2_f32(v304, v304);
    int64_t v313 = v308 + 1;
    float32x2_t v358 = v7[v357];
    float32x2_t v359 = vtrn1_f32(v322, v322);
    float32x2_t v360 = vtrn2_f32(v322, v322);
    int64_t v362 = v357 + 1;
    float32x2_t v371 = v7[v370];
    float32x2_t v372 = vtrn1_f32(v340, v340);
    float32x2_t v373 = vtrn2_f32(v340, v340);
    int64_t v375 = v370 + 1;
    float32x2_t v389 = v7[v388];
    float32x2_t v390 = vtrn1_f32(v384, v384);
    float32x2_t v391 = vtrn2_f32(v384, v384);
    int64_t v393 = v388 + 1;
    float32x2_t v438 = v7[v437];
    float32x2_t v439 = vtrn1_f32(v402, v402);
    float32x2_t v440 = vtrn2_f32(v402, v402);
    int64_t v442 = v437 + 1;
    float32x2_t v451 = v7[v450];
    float32x2_t v452 = vtrn1_f32(v420, v420);
    float32x2_t v453 = vtrn2_f32(v420, v420);
    int64_t v455 = v450 + 1;
    float32x2_t v469 = v7[v468];
    float32x2_t v470 = vtrn1_f32(v464, v464);
    float32x2_t v471 = vtrn2_f32(v464, v464);
    int64_t v473 = v468 + 1;
    float32x2_t v518 = v7[v517];
    float32x2_t v519 = vtrn1_f32(v482, v482);
    float32x2_t v520 = vtrn2_f32(v482, v482);
    int64_t v522 = v517 + 1;
    float32x2_t v531 = v7[v530];
    float32x2_t v532 = vtrn1_f32(v500, v500);
    float32x2_t v533 = vtrn2_f32(v500, v500);
    int64_t v535 = v530 + 1;
    float32x2_t v549 = v7[v548];
    float32x2_t v550 = vtrn1_f32(v544, v544);
    float32x2_t v551 = vtrn2_f32(v544, v544);
    int64_t v553 = v548 + 1;
    float32x2_t v598 = v7[v597];
    float32x2_t v599 = vtrn1_f32(v562, v562);
    float32x2_t v600 = vtrn2_f32(v562, v562);
    int64_t v602 = v597 + 1;
    float32x2_t v611 = v7[v610];
    float32x2_t v612 = vtrn1_f32(v580, v580);
    float32x2_t v613 = vtrn2_f32(v580, v580);
    int64_t v615 = v610 + 1;
    float32x2_t v629 = v7[v628];
    float32x2_t v630 = vtrn1_f32(v624, v624);
    float32x2_t v631 = vtrn2_f32(v624, v624);
    int64_t v633 = v628 + 1;
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vmul_f32(v57, v56);
    float32x2_t v74 = v7[v73];
    float32x2_t v75 = vmul_f32(v70, v69);
    float32x2_t v123 = v7[v122];
    float32x2_t v124 = vmul_f32(v119, v118);
    float32x2_t v136 = v7[v135];
    float32x2_t v137 = vmul_f32(v132, v131);
    float32x2_t v154 = v7[v153];
    float32x2_t v155 = vmul_f32(v150, v149);
    float32x2_t v203 = v7[v202];
    float32x2_t v204 = vmul_f32(v199, v198);
    float32x2_t v216 = v7[v215];
    float32x2_t v217 = vmul_f32(v212, v211);
    float32x2_t v234 = v7[v233];
    float32x2_t v235 = vmul_f32(v230, v229);
    float32x2_t v283 = v7[v282];
    float32x2_t v284 = vmul_f32(v279, v278);
    float32x2_t v314 = v7[v313];
    float32x2_t v315 = vmul_f32(v310, v309);
    float32x2_t v363 = v7[v362];
    float32x2_t v364 = vmul_f32(v359, v358);
    float32x2_t v376 = v7[v375];
    float32x2_t v377 = vmul_f32(v372, v371);
    float32x2_t v394 = v7[v393];
    float32x2_t v395 = vmul_f32(v390, v389);
    float32x2_t v443 = v7[v442];
    float32x2_t v444 = vmul_f32(v439, v438);
    float32x2_t v456 = v7[v455];
    float32x2_t v457 = vmul_f32(v452, v451);
    float32x2_t v474 = v7[v473];
    float32x2_t v475 = vmul_f32(v470, v469);
    float32x2_t v523 = v7[v522];
    float32x2_t v524 = vmul_f32(v519, v518);
    float32x2_t v536 = v7[v535];
    float32x2_t v537 = vmul_f32(v532, v531);
    float32x2_t v554 = v7[v553];
    float32x2_t v555 = vmul_f32(v550, v549);
    float32x2_t v603 = v7[v602];
    float32x2_t v604 = vmul_f32(v599, v598);
    float32x2_t v616 = v7[v615];
    float32x2_t v617 = vmul_f32(v612, v611);
    float32x2_t v634 = v7[v633];
    float32x2_t v635 = vmul_f32(v630, v629);
    float32x2_t v299 = vfma_f32(v297, v293, v296);
    float32x2_t v64 = vfma_f32(v62, v58, v61);
    float32x2_t v77 = vfma_f32(v75, v71, v74);
    float32x2_t v126 = vfma_f32(v124, v120, v123);
    float32x2_t v139 = vfma_f32(v137, v133, v136);
    float32x2_t v157 = vfma_f32(v155, v151, v154);
    float32x2_t v206 = vfma_f32(v204, v200, v203);
    float32x2_t v219 = vfma_f32(v217, v213, v216);
    float32x2_t v237 = vfma_f32(v235, v231, v234);
    float32x2_t v286 = vfma_f32(v284, v280, v283);
    float32x2_t v317 = vfma_f32(v315, v311, v314);
    float32x2_t v366 = vfma_f32(v364, v360, v363);
    float32x2_t v379 = vfma_f32(v377, v373, v376);
    float32x2_t v397 = vfma_f32(v395, v391, v394);
    float32x2_t v446 = vfma_f32(v444, v440, v443);
    float32x2_t v459 = vfma_f32(v457, v453, v456);
    float32x2_t v477 = vfma_f32(v475, v471, v474);
    float32x2_t v526 = vfma_f32(v524, v520, v523);
    float32x2_t v539 = vfma_f32(v537, v533, v536);
    float32x2_t v557 = vfma_f32(v555, v551, v554);
    float32x2_t v606 = vfma_f32(v604, v600, v603);
    float32x2_t v619 = vfma_f32(v617, v613, v616);
    float32x2_t v637 = vfma_f32(v635, v631, v634);
    float32x2_t v638 = vadd_f32(v64, v77);
    float32x2_t v639 = vsub_f32(v64, v77);
    float32x2_t v646 = vadd_f32(v126, v139);
    float32x2_t v647 = vsub_f32(v126, v139);
    float32x2_t v649 = vadd_f32(v206, v219);
    float32x2_t v650 = vsub_f32(v206, v219);
    float32x2_t v652 = vadd_f32(v286, v299);
    float32x2_t v653 = vsub_f32(v286, v299);
    float32x2_t v655 = vadd_f32(v366, v379);
    float32x2_t v656 = vsub_f32(v366, v379);
    float32x2_t v658 = vadd_f32(v446, v459);
    float32x2_t v659 = vsub_f32(v446, v459);
    float32x2_t v661 = vadd_f32(v526, v539);
    float32x2_t v662 = vsub_f32(v526, v539);
    float32x2_t v664 = vadd_f32(v606, v619);
    float32x2_t v665 = vsub_f32(v606, v619);
    float32x2_t v645 = vadd_f32(v638, v644);
    float32x2_t v648 = vadd_f32(v646, v157);
    float32x2_t v651 = vadd_f32(v649, v237);
    float32x2_t v654 = vadd_f32(v652, v317);
    float32x2_t v657 = vadd_f32(v655, v397);
    float32x2_t v660 = vadd_f32(v658, v477);
    float32x2_t v663 = vadd_f32(v661, v557);
    float32x2_t v666 = vadd_f32(v664, v637);
    float32x2_t v734 = vadd_f32(v638, v655);
    float32x2_t v735 = vsub_f32(v638, v655);
    float32x2_t v736 = vadd_f32(v649, v661);
    float32x2_t v737 = vsub_f32(v649, v661);
    float32x2_t v738 = vadd_f32(v646, v658);
    float32x2_t v739 = vsub_f32(v646, v658);
    float32x2_t v740 = vadd_f32(v652, v664);
    float32x2_t v741 = vsub_f32(v652, v664);
    float32x2_t v801 = vadd_f32(v639, v656);
    float32x2_t v802 = vsub_f32(v639, v656);
    float32x2_t v803 = vadd_f32(v650, v662);
    float32x2_t v804 = vsub_f32(v650, v662);
    float32x2_t v805 = vadd_f32(v647, v659);
    float32x2_t v806 = vsub_f32(v647, v659);
    float32x2_t v807 = vadd_f32(v653, v665);
    float32x2_t v808 = vsub_f32(v653, v665);
    float32x2_t v667 = vadd_f32(v645, v657);
    float32x2_t v668 = vsub_f32(v645, v657);
    float32x2_t v669 = vadd_f32(v651, v663);
    float32x2_t v670 = vsub_f32(v651, v663);
    float32x2_t v671 = vadd_f32(v648, v660);
    float32x2_t v672 = vsub_f32(v648, v660);
    float32x2_t v673 = vadd_f32(v654, v666);
    float32x2_t v674 = vsub_f32(v654, v666);
    float32x2_t v742 = vadd_f32(v734, v736);
    float32x2_t v743 = vsub_f32(v734, v736);
    float32x2_t v744 = vadd_f32(v738, v740);
    float32x2_t v745 = vsub_f32(v738, v740);
    float32x2_t v748 = vadd_f32(v739, v741);
    float32x2_t v749 = vsub_f32(v739, v741);
    float32x2_t v772 = vmul_f32(v735, v771);
    float32x2_t v778 = vrev64_f32(v737);
    float32x2_t v809 = vadd_f32(v801, v803);
    float32x2_t v810 = vsub_f32(v801, v803);
    float32x2_t v811 = vadd_f32(v805, v807);
    float32x2_t v812 = vsub_f32(v805, v807);
    float32x2_t v815 = vadd_f32(v806, v808);
    float32x2_t v816 = vsub_f32(v806, v808);
    float32x2_t v847 = vrev64_f32(v802);
    float32x2_t v852 = vmul_f32(v804, v851);
    float32x2_t v675 = vadd_f32(v667, v669);
    float32x2_t v676 = vsub_f32(v667, v669);
    float32x2_t v677 = vadd_f32(v671, v673);
    float32x2_t v678 = vsub_f32(v671, v673);
    float32x2_t v681 = vadd_f32(v672, v674);
    float32x2_t v682 = vsub_f32(v672, v674);
    float32x2_t v711 = vrev64_f32(v670);
    float32x2_t v746 = vadd_f32(v742, v744);
    float32x2_t v747 = vsub_f32(v742, v744);
    float32x2_t v761 = vmul_f32(v743, v771);
    float32x2_t v767 = vrev64_f32(v745);
    float32x2_t v779 = vmul_f32(v778, v777);
    float32x2_t v785 = vrev64_f32(v748);
    float32x2_t v790 = vmul_f32(v749, v789);
    float32x2_t v813 = vadd_f32(v809, v811);
    float32x2_t v814 = vsub_f32(v809, v811);
    float32x2_t v836 = vrev64_f32(v810);
    float32x2_t v841 = vmul_f32(v812, v851);
    float32x2_t v848 = vmul_f32(v847, v846);
    float32x2_t v856 = vmul_f32(v815, v855);
    float32x2_t v862 = vrev64_f32(v816);
    float32x2_t v679 = vadd_f32(v675, v677);
    float32x2_t v680 = vsub_f32(v675, v677);
    float32x2_t v700 = vrev64_f32(v678);
    float32x2_t v712 = vmul_f32(v711, v710);
    float32x2_t v718 = vrev64_f32(v681);
    float32x2_t v723 = vmul_f32(v682, v722);
    float32x2_t v753 = vmul_f32(v746, v771);
    float32x2_t v757 = vmul_f32(v747, v771);
    float32x2_t v768 = vmul_f32(v767, v777);
    float32x2_t v786 = vmul_f32(v785, v784);
    float32x2_t v793 = vadd_f32(v772, v790);
    float32x2_t v794 = vsub_f32(v772, v790);
    float32x2_t v822 = vrev64_f32(v813);
    float32x2_t v829 = vrev64_f32(v814);
    float32x2_t v837 = vmul_f32(v836, v846);
    float32x2_t v863 = vmul_f32(v862, v861);
    float32x2_t v868 = vadd_f32(v852, v856);
    float32x2_t v869 = vsub_f32(v852, v856);
    float32x2_t v701 = vmul_f32(v700, v710);
    float32x2_t v719 = vmul_f32(v718, v717);
    float32x2_t v726 = vadd_f32(v668, v723);
    float32x2_t v727 = vsub_f32(v668, v723);
    float32x2_t v791 = vadd_f32(v761, v768);
    float32x2_t v792 = vsub_f32(v761, v768);
    float32x2_t v795 = vadd_f32(v779, v786);
    float32x2_t v796 = vsub_f32(v779, v786);
    float32x2_t v823 = vmul_f32(v822, v846);
    float32x2_t v830 = vmul_f32(v829, v846);
    float32x2_t v864 = vadd_f32(v837, v841);
    float32x2_t v865 = vsub_f32(v837, v841);
    float32x2_t v866 = vadd_f32(v848, v863);
    float32x2_t v867 = vsub_f32(v848, v863);
    float32x2_t v874 = vadd_f32(v679, v753);
    v6[0] = v679;
    float32x2_t v946 = vadd_f32(v680, v757);
    v6[ostride * 12] = v680;
    float32x2_t v724 = vadd_f32(v676, v701);
    float32x2_t v725 = vsub_f32(v676, v701);
    float32x2_t v728 = vadd_f32(v712, v719);
    float32x2_t v729 = vsub_f32(v712, v719);
    float32x2_t v797 = vadd_f32(v793, v795);
    float32x2_t v798 = vsub_f32(v793, v795);
    float32x2_t v799 = vadd_f32(v794, v796);
    float32x2_t v800 = vsub_f32(v794, v796);
    float32x2_t v870 = vadd_f32(v866, v868);
    float32x2_t v871 = vsub_f32(v866, v868);
    float32x2_t v872 = vadd_f32(v867, v869);
    float32x2_t v873 = vsub_f32(v867, v869);
    float32x2_t v875 = vadd_f32(v874, v823);
    float32x2_t v876 = vsub_f32(v874, v823);
    float32x2_t v947 = vadd_f32(v946, v830);
    float32x2_t v948 = vsub_f32(v946, v830);
    float32x2_t v730 = vadd_f32(v726, v728);
    float32x2_t v731 = vsub_f32(v726, v728);
    float32x2_t v732 = vadd_f32(v727, v729);
    float32x2_t v733 = vsub_f32(v727, v729);
    v6[ostride * 16] = v876;
    v6[ostride * 8] = v875;
    float32x2_t v910 = vadd_f32(v725, v792);
    v6[ostride * 18] = v725;
    v6[ostride * 4] = v948;
    v6[ostride * 20] = v947;
    float32x2_t v982 = vadd_f32(v724, v791);
    v6[ostride * 6] = v724;
    float32x2_t v892 = vadd_f32(v731, v798);
    v6[ostride * 9] = v731;
    float32x2_t v911 = vadd_f32(v910, v865);
    float32x2_t v912 = vsub_f32(v910, v865);
    float32x2_t v928 = vadd_f32(v732, v799);
    v6[ostride * 3] = v732;
    float32x2_t v964 = vadd_f32(v733, v800);
    v6[ostride * 21] = v733;
    float32x2_t v983 = vadd_f32(v982, v864);
    float32x2_t v984 = vsub_f32(v982, v864);
    float32x2_t v1000 = vadd_f32(v730, v797);
    v6[ostride * 15] = v730;
    float32x2_t v893 = vadd_f32(v892, v871);
    float32x2_t v894 = vsub_f32(v892, v871);
    v6[ostride * 10] = v912;
    v6[ostride * 2] = v911;
    float32x2_t v929 = vadd_f32(v928, v872);
    float32x2_t v930 = vsub_f32(v928, v872);
    float32x2_t v965 = vadd_f32(v964, v873);
    float32x2_t v966 = vsub_f32(v964, v873);
    v6[ostride * 22] = v984;
    v6[ostride * 14] = v983;
    float32x2_t v1001 = vadd_f32(v1000, v870);
    float32x2_t v1002 = vsub_f32(v1000, v870);
    v6[ostride] = v894;
    v6[ostride * 17] = v893;
    v6[ostride * 19] = v930;
    v6[ostride * 11] = v929;
    v6[ostride * 13] = v966;
    v6[ostride * 5] = v965;
    v6[ostride * 7] = v1002;
    v6[ostride * 23] = v1001;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs24(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v527 = -1.0000000000000000e+00F;
    float v534 = -7.0710678118654746e-01F;
    float v541 = 7.0710678118654757e-01F;
    float v594 = -1.4999999999999998e+00F;
    float v599 = 1.4999999999999998e+00F;
    float v606 = 1.0606601717798210e+00F;
    float v613 = -1.0606601717798212e+00F;
    float v677 = -8.6602540378443871e-01F;
    float v687 = -6.1237243569579458e-01F;
    const float32x2_t *v982 = &v5[v0];
    float32x2_t *v1181 = &v6[v2];
    int64_t v19 = v0 * 8;
    int64_t v33 = v0 * 16;
    int64_t v48 = v10 * 7;
    int64_t v55 = v10 * 15;
    int64_t v61 = v0 * 11;
    int64_t v75 = v0 * 19;
    int64_t v90 = v10 * 10;
    int64_t v97 = v10 * 18;
    int64_t v103 = v0 * 3;
    int64_t v111 = v10 * 2;
    int64_t v117 = v0 * 14;
    int64_t v131 = v0 * 22;
    int64_t v146 = v10 * 13;
    int64_t v153 = v10 * 21;
    int64_t v159 = v0 * 6;
    int64_t v167 = v10 * 5;
    int64_t v173 = v0 * 17;
    int64_t v202 = v10 * 16;
    int64_t v215 = v0 * 9;
    int64_t v223 = v10 * 8;
    int64_t v229 = v0 * 20;
    int64_t v243 = v0 * 4;
    int64_t v258 = v10 * 19;
    int64_t v265 = v10 * 3;
    int64_t v271 = v0 * 12;
    int64_t v279 = v10 * 11;
    int64_t v285 = v0 * 23;
    int64_t v299 = v0 * 7;
    int64_t v314 = v10 * 22;
    int64_t v321 = v10 * 6;
    int64_t v327 = v0 * 15;
    int64_t v335 = v10 * 14;
    int64_t v341 = v0 * 2;
    int64_t v355 = v0 * 10;
    int64_t v377 = v10 * 9;
    int64_t v383 = v0 * 18;
    int64_t v391 = v10 * 17;
    int64_t v397 = v0 * 5;
    int64_t v411 = v0 * 13;
    int64_t v426 = v10 * 4;
    int64_t v433 = v10 * 12;
    int64_t v439 = v0 * 21;
    int64_t v447 = v10 * 20;
    int64_t v448 = v13 * 23;
    float v530 = v4 * v527;
    float v537 = v4 * v534;
    float v602 = v4 * v599;
    float v609 = v4 * v606;
    float v673 = v4 * v677;
    float v690 = v4 * v687;
    int64_t v714 = v2 * 16;
    int64_t v721 = v2 * 8;
    int64_t v731 = v2 * 9;
    int64_t v745 = v2 * 17;
    int64_t v755 = v2 * 18;
    int64_t v762 = v2 * 10;
    int64_t v769 = v2 * 2;
    int64_t v779 = v2 * 3;
    int64_t v786 = v2 * 19;
    int64_t v793 = v2 * 11;
    int64_t v803 = v2 * 12;
    int64_t v810 = v2 * 4;
    int64_t v817 = v2 * 20;
    int64_t v827 = v2 * 21;
    int64_t v834 = v2 * 13;
    int64_t v841 = v2 * 5;
    int64_t v851 = v2 * 6;
    int64_t v858 = v2 * 22;
    int64_t v865 = v2 * 14;
    int64_t v875 = v2 * 15;
    int64_t v882 = v2 * 7;
    int64_t v889 = v2 * 23;
    const float32x2_t *v1111 = &v5[0];
    svint64_t v1112 = svindex_s64(0, v1);
    svfloat32_t v1121 = svdup_n_f32(v541);
    svfloat32_t v1126 = svdup_n_f32(v594);
    svfloat32_t v1129 = svdup_n_f32(v613);
    svfloat32_t v1135 = svdup_n_f32(v677);
    svfloat32_t v1136 = svdup_n_f32(v687);
    float32x2_t *v1145 = &v6[0];
    svint64_t v1353 = svindex_s64(0, v3);
    int64_t v50 = v48 + v448;
    int64_t v57 = v55 + v448;
    int64_t v92 = v90 + v448;
    int64_t v99 = v97 + v448;
    int64_t v113 = v111 + v448;
    int64_t v148 = v146 + v448;
    int64_t v155 = v153 + v448;
    int64_t v169 = v167 + v448;
    int64_t v204 = v202 + v448;
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v448]));
    int64_t v225 = v223 + v448;
    int64_t v260 = v258 + v448;
    int64_t v267 = v265 + v448;
    int64_t v281 = v279 + v448;
    int64_t v316 = v314 + v448;
    int64_t v323 = v321 + v448;
    int64_t v337 = v335 + v448;
    int64_t v372 = v10 + v448;
    int64_t v379 = v377 + v448;
    int64_t v393 = v391 + v448;
    int64_t v428 = v426 + v448;
    int64_t v435 = v433 + v448;
    int64_t v449 = v447 + v448;
    const float32x2_t *v901 = &v5[v19];
    const float32x2_t *v910 = &v5[v33];
    const float32x2_t *v919 = &v5[v61];
    const float32x2_t *v928 = &v5[v75];
    const float32x2_t *v937 = &v5[v103];
    const float32x2_t *v946 = &v5[v117];
    const float32x2_t *v955 = &v5[v131];
    const float32x2_t *v964 = &v5[v159];
    const float32x2_t *v973 = &v5[v173];
    svfloat32_t v984 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v982), v1112));
    const float32x2_t *v993 = &v5[v215];
    const float32x2_t *v1002 = &v5[v229];
    const float32x2_t *v1011 = &v5[v243];
    const float32x2_t *v1020 = &v5[v271];
    const float32x2_t *v1029 = &v5[v285];
    const float32x2_t *v1038 = &v5[v299];
    const float32x2_t *v1047 = &v5[v327];
    const float32x2_t *v1056 = &v5[v341];
    const float32x2_t *v1065 = &v5[v355];
    const float32x2_t *v1074 = &v5[v383];
    const float32x2_t *v1083 = &v5[v397];
    const float32x2_t *v1092 = &v5[v411];
    const float32x2_t *v1101 = &v5[v439];
    svfloat32_t v1113 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1111), v1112));
    svfloat32_t v1119 = svdup_n_f32(v530);
    svfloat32_t v1120 = svdup_n_f32(v537);
    svfloat32_t v1127 = svdup_n_f32(v602);
    svfloat32_t v1128 = svdup_n_f32(v609);
    svfloat32_t v1134 = svdup_n_f32(v673);
    svfloat32_t v1137 = svdup_n_f32(v690);
    float32x2_t *v1154 = &v6[v714];
    float32x2_t *v1163 = &v6[v721];
    float32x2_t *v1172 = &v6[v731];
    float32x2_t *v1190 = &v6[v745];
    float32x2_t *v1199 = &v6[v755];
    float32x2_t *v1208 = &v6[v762];
    float32x2_t *v1217 = &v6[v769];
    float32x2_t *v1226 = &v6[v779];
    float32x2_t *v1235 = &v6[v786];
    float32x2_t *v1244 = &v6[v793];
    float32x2_t *v1253 = &v6[v803];
    float32x2_t *v1262 = &v6[v810];
    float32x2_t *v1271 = &v6[v817];
    float32x2_t *v1280 = &v6[v827];
    float32x2_t *v1289 = &v6[v834];
    float32x2_t *v1298 = &v6[v841];
    float32x2_t *v1307 = &v6[v851];
    float32x2_t *v1316 = &v6[v858];
    float32x2_t *v1325 = &v6[v865];
    float32x2_t *v1334 = &v6[v875];
    float32x2_t *v1343 = &v6[v882];
    float32x2_t *v1352 = &v6[v889];
    svfloat32_t v51 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v50]));
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v93 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v92]));
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v149 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v148]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t v205 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v204]));
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero213, v984, v212, 0),
                     v984, v212, 90);
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v261 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v260]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v317 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v316]));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v338 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v337]));
    svfloat32_t v373 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v372]));
    svfloat32_t v380 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v379]));
    svfloat32_t v394 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v393]));
    svfloat32_t v429 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v428]));
    svfloat32_t v436 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v435]));
    svfloat32_t v450 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v449]));
    svfloat32_t v903 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v901), v1112));
    svfloat32_t v912 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v910), v1112));
    svfloat32_t v921 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v919), v1112));
    svfloat32_t v930 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v928), v1112));
    svfloat32_t v939 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v937), v1112));
    svfloat32_t v948 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v946), v1112));
    svfloat32_t v957 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v955), v1112));
    svfloat32_t v966 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v964), v1112));
    svfloat32_t v975 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v973), v1112));
    svfloat32_t v995 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v993), v1112));
    svfloat32_t v1004 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1002), v1112));
    svfloat32_t v1013 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1011), v1112));
    svfloat32_t v1022 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1020), v1112));
    svfloat32_t v1031 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1029), v1112));
    svfloat32_t v1040 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1038), v1112));
    svfloat32_t v1049 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1047), v1112));
    svfloat32_t v1058 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1056), v1112));
    svfloat32_t v1067 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1065), v1112));
    svfloat32_t v1076 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1074), v1112));
    svfloat32_t v1085 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1083), v1112));
    svfloat32_t v1094 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1092), v1112));
    svfloat32_t v1103 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1101), v1112));
    svfloat32_t zero52 = svdup_n_f32(0);
    svfloat32_t v52 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero52, v903, v51, 0),
                     v903, v51, 90);
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v912, v58, 0),
                     v912, v58, 90);
    svfloat32_t zero94 = svdup_n_f32(0);
    svfloat32_t v94 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero94, v921, v93, 0),
                     v921, v93, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero101, v930, v100, 0),
                     v930, v100, 90);
    svfloat32_t zero150 = svdup_n_f32(0);
    svfloat32_t v150 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero150, v948, v149, 0),
                     v948, v149, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero157, v957, v156, 0),
                     v957, v156, 90);
    svfloat32_t zero206 = svdup_n_f32(0);
    svfloat32_t v206 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero206, v975, v205, 0),
                     v975, v205, 90);
    svfloat32_t zero262 = svdup_n_f32(0);
    svfloat32_t v262 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero262, v1004, v261, 0), v1004,
        v261, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero269, v1013, v268, 0), v1013,
        v268, 90);
    svfloat32_t zero318 = svdup_n_f32(0);
    svfloat32_t v318 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero318, v1031, v317, 0), v1031,
        v317, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero325, v1040, v324, 0), v1040,
        v324, 90);
    svfloat32_t zero374 = svdup_n_f32(0);
    svfloat32_t v374 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero374, v1058, v373, 0), v1058,
        v373, 90);
    svfloat32_t zero381 = svdup_n_f32(0);
    svfloat32_t v381 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero381, v1067, v380, 0), v1067,
        v380, 90);
    svfloat32_t zero430 = svdup_n_f32(0);
    svfloat32_t v430 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero430, v1085, v429, 0), v1085,
        v429, 90);
    svfloat32_t zero437 = svdup_n_f32(0);
    svfloat32_t v437 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero437, v1094, v436, 0), v1094,
        v436, 90);
    svfloat32_t v452 = svadd_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v453 = svsub_f32_x(svptrue_b32(), v52, v59);
    svfloat32_t v462 = svadd_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v463 = svsub_f32_x(svptrue_b32(), v94, v101);
    svfloat32_t v465 = svadd_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v466 = svsub_f32_x(svptrue_b32(), v150, v157);
    svfloat32_t v468 = svadd_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v469 = svsub_f32_x(svptrue_b32(), v206, v213);
    svfloat32_t v471 = svadd_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v472 = svsub_f32_x(svptrue_b32(), v262, v269);
    svfloat32_t v474 = svadd_f32_x(svptrue_b32(), v318, v325);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v318, v325);
    svfloat32_t v477 = svadd_f32_x(svptrue_b32(), v374, v381);
    svfloat32_t v478 = svsub_f32_x(svptrue_b32(), v374, v381);
    svfloat32_t v480 = svadd_f32_x(svptrue_b32(), v430, v437);
    svfloat32_t v481 = svsub_f32_x(svptrue_b32(), v430, v437);
    svfloat32_t v461 = svadd_f32_x(svptrue_b32(), v452, v1113);
    svfloat32_t v464 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v462, v939, v114, 0),
                     v939, v114, 90);
    svfloat32_t v467 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v465, v966, v170, 0),
                     v966, v170, 90);
    svfloat32_t v470 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v468, v995, v226, 0),
                     v995, v226, 90);
    svfloat32_t v473 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v471, v1022, v282, 0),
                     v1022, v282, 90);
    svfloat32_t v476 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v474, v1049, v338, 0),
                     v1049, v338, 90);
    svfloat32_t v479 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v477, v1076, v394, 0),
                     v1076, v394, 90);
    svfloat32_t v482 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, v480, v1103, v450, 0),
                     v1103, v450, 90);
    svfloat32_t v555 = svadd_f32_x(svptrue_b32(), v452, v471);
    svfloat32_t v556 = svsub_f32_x(svptrue_b32(), v452, v471);
    svfloat32_t v557 = svadd_f32_x(svptrue_b32(), v465, v477);
    svfloat32_t v558 = svsub_f32_x(svptrue_b32(), v465, v477);
    svfloat32_t v559 = svadd_f32_x(svptrue_b32(), v462, v474);
    svfloat32_t v560 = svsub_f32_x(svptrue_b32(), v462, v474);
    svfloat32_t v561 = svadd_f32_x(svptrue_b32(), v468, v480);
    svfloat32_t v562 = svsub_f32_x(svptrue_b32(), v468, v480);
    svfloat32_t v627 = svadd_f32_x(svptrue_b32(), v453, v472);
    svfloat32_t v628 = svsub_f32_x(svptrue_b32(), v453, v472);
    svfloat32_t v629 = svadd_f32_x(svptrue_b32(), v466, v478);
    svfloat32_t v630 = svsub_f32_x(svptrue_b32(), v466, v478);
    svfloat32_t v631 = svadd_f32_x(svptrue_b32(), v463, v475);
    svfloat32_t v632 = svsub_f32_x(svptrue_b32(), v463, v475);
    svfloat32_t v633 = svadd_f32_x(svptrue_b32(), v469, v481);
    svfloat32_t v634 = svsub_f32_x(svptrue_b32(), v469, v481);
    svfloat32_t v483 = svadd_f32_x(svptrue_b32(), v461, v473);
    svfloat32_t v484 = svsub_f32_x(svptrue_b32(), v461, v473);
    svfloat32_t v485 = svadd_f32_x(svptrue_b32(), v467, v479);
    svfloat32_t v486 = svsub_f32_x(svptrue_b32(), v467, v479);
    svfloat32_t v487 = svadd_f32_x(svptrue_b32(), v464, v476);
    svfloat32_t v488 = svsub_f32_x(svptrue_b32(), v464, v476);
    svfloat32_t v489 = svadd_f32_x(svptrue_b32(), v470, v482);
    svfloat32_t v490 = svsub_f32_x(svptrue_b32(), v470, v482);
    svfloat32_t v563 = svadd_f32_x(svptrue_b32(), v555, v557);
    svfloat32_t v564 = svsub_f32_x(svptrue_b32(), v555, v557);
    svfloat32_t v565 = svadd_f32_x(svptrue_b32(), v559, v561);
    svfloat32_t v566 = svsub_f32_x(svptrue_b32(), v559, v561);
    svfloat32_t v569 = svadd_f32_x(svptrue_b32(), v560, v562);
    svfloat32_t v570 = svsub_f32_x(svptrue_b32(), v560, v562);
    svfloat32_t zero604 = svdup_n_f32(0);
    svfloat32_t v604 = svcmla_f32_x(pred_full, zero604, v1127, v558, 90);
    svfloat32_t v635 = svadd_f32_x(svptrue_b32(), v627, v629);
    svfloat32_t v636 = svsub_f32_x(svptrue_b32(), v627, v629);
    svfloat32_t v637 = svadd_f32_x(svptrue_b32(), v631, v633);
    svfloat32_t v638 = svsub_f32_x(svptrue_b32(), v631, v633);
    svfloat32_t v641 = svadd_f32_x(svptrue_b32(), v632, v634);
    svfloat32_t v642 = svsub_f32_x(svptrue_b32(), v632, v634);
    svfloat32_t zero675 = svdup_n_f32(0);
    svfloat32_t v675 = svcmla_f32_x(pred_full, zero675, v1134, v628, 90);
    svfloat32_t v491 = svadd_f32_x(svptrue_b32(), v483, v485);
    svfloat32_t v492 = svsub_f32_x(svptrue_b32(), v483, v485);
    svfloat32_t v493 = svadd_f32_x(svptrue_b32(), v487, v489);
    svfloat32_t v494 = svsub_f32_x(svptrue_b32(), v487, v489);
    svfloat32_t v497 = svadd_f32_x(svptrue_b32(), v488, v490);
    svfloat32_t v498 = svsub_f32_x(svptrue_b32(), v488, v490);
    svfloat32_t zero532 = svdup_n_f32(0);
    svfloat32_t v532 = svcmla_f32_x(pred_full, zero532, v1119, v486, 90);
    svfloat32_t v567 = svadd_f32_x(svptrue_b32(), v563, v565);
    svfloat32_t v568 = svsub_f32_x(svptrue_b32(), v563, v565);
    svfloat32_t zero592 = svdup_n_f32(0);
    svfloat32_t v592 = svcmla_f32_x(pred_full, zero592, v1127, v566, 90);
    svfloat32_t zero611 = svdup_n_f32(0);
    svfloat32_t v611 = svcmla_f32_x(pred_full, zero611, v1128, v569, 90);
    svfloat32_t v616 = svmul_f32_x(svptrue_b32(), v570, v1129);
    svfloat32_t v639 = svadd_f32_x(svptrue_b32(), v635, v637);
    svfloat32_t v640 = svsub_f32_x(svptrue_b32(), v635, v637);
    svfloat32_t zero663 = svdup_n_f32(0);
    svfloat32_t v663 = svcmla_f32_x(pred_full, zero663, v1134, v636, 90);
    svfloat32_t v685 = svmul_f32_x(svptrue_b32(), v641, v1136);
    svfloat32_t zero692 = svdup_n_f32(0);
    svfloat32_t v692 = svcmla_f32_x(pred_full, zero692, v1137, v642, 90);
    svfloat32_t v495 = svadd_f32_x(svptrue_b32(), v491, v493);
    svfloat32_t v496 = svsub_f32_x(svptrue_b32(), v491, v493);
    svfloat32_t zero520 = svdup_n_f32(0);
    svfloat32_t v520 = svcmla_f32_x(pred_full, zero520, v1119, v494, 90);
    svfloat32_t zero539 = svdup_n_f32(0);
    svfloat32_t v539 = svcmla_f32_x(pred_full, zero539, v1120, v497, 90);
    svfloat32_t v617 = svmla_f32_x(pred_full, v592, v564, v1126);
    svfloat32_t v618 = svnmls_f32_x(pred_full, v592, v564, v1126);
    svfloat32_t v619 = svmla_f32_x(pred_full, v616, v556, v1126);
    svfloat32_t v620 = svnmls_f32_x(pred_full, v616, v556, v1126);
    svfloat32_t v621 = svadd_f32_x(svptrue_b32(), v604, v611);
    svfloat32_t v622 = svsub_f32_x(svptrue_b32(), v604, v611);
    svfloat32_t zero649 = svdup_n_f32(0);
    svfloat32_t v649 = svcmla_f32_x(pred_full, zero649, v1134, v639, 90);
    svfloat32_t zero656 = svdup_n_f32(0);
    svfloat32_t v656 = svcmla_f32_x(pred_full, zero656, v1134, v640, 90);
    svfloat32_t v693 = svmla_f32_x(pred_full, v663, v638, v1135);
    svfloat32_t v694 = svmls_f32_x(pred_full, v663, v638, v1135);
    svfloat32_t v695 = svadd_f32_x(svptrue_b32(), v675, v692);
    svfloat32_t v696 = svsub_f32_x(svptrue_b32(), v675, v692);
    svfloat32_t v697 = svmla_f32_x(pred_full, v685, v630, v1135);
    svfloat32_t v698 = svnmls_f32_x(pred_full, v685, v630, v1135);
    svfloat32_t v545 = svadd_f32_x(svptrue_b32(), v492, v520);
    svfloat32_t v546 = svsub_f32_x(svptrue_b32(), v492, v520);
    svfloat32_t v547 = svmla_f32_x(pred_full, v484, v498, v1121);
    svfloat32_t v548 = svmls_f32_x(pred_full, v484, v498, v1121);
    svfloat32_t v549 = svadd_f32_x(svptrue_b32(), v532, v539);
    svfloat32_t v550 = svsub_f32_x(svptrue_b32(), v532, v539);
    svfloat32_t v623 = svadd_f32_x(svptrue_b32(), v619, v621);
    svfloat32_t v624 = svsub_f32_x(svptrue_b32(), v619, v621);
    svfloat32_t v625 = svadd_f32_x(svptrue_b32(), v620, v622);
    svfloat32_t v626 = svsub_f32_x(svptrue_b32(), v620, v622);
    svfloat32_t v699 = svadd_f32_x(svptrue_b32(), v695, v697);
    svfloat32_t v700 = svsub_f32_x(svptrue_b32(), v695, v697);
    svfloat32_t v701 = svadd_f32_x(svptrue_b32(), v696, v698);
    svfloat32_t v702 = svsub_f32_x(svptrue_b32(), v696, v698);
    svfloat32_t v703 = svmla_f32_x(pred_full, v495, v567, v1126);
    svfloat32_t v799 = svmla_f32_x(pred_full, v496, v568, v1126);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1145), v1353,
                               svreinterpret_f64_f32(v495));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1253), v1353,
                               svreinterpret_f64_f32(v496));
    svfloat32_t v551 = svadd_f32_x(svptrue_b32(), v547, v549);
    svfloat32_t v552 = svsub_f32_x(svptrue_b32(), v547, v549);
    svfloat32_t v553 = svadd_f32_x(svptrue_b32(), v548, v550);
    svfloat32_t v554 = svsub_f32_x(svptrue_b32(), v548, v550);
    svfloat32_t v704 = svadd_f32_x(svptrue_b32(), v703, v649);
    svfloat32_t v705 = svsub_f32_x(svptrue_b32(), v703, v649);
    svfloat32_t v751 = svadd_f32_x(svptrue_b32(), v546, v618);
    svfloat32_t v800 = svadd_f32_x(svptrue_b32(), v799, v656);
    svfloat32_t v801 = svsub_f32_x(svptrue_b32(), v799, v656);
    svfloat32_t v847 = svadd_f32_x(svptrue_b32(), v545, v617);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1199), v1353,
                               svreinterpret_f64_f32(v546));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1307), v1353,
                               svreinterpret_f64_f32(v545));
    svfloat32_t v727 = svadd_f32_x(svptrue_b32(), v552, v624);
    svfloat32_t v752 = svadd_f32_x(svptrue_b32(), v751, v694);
    svfloat32_t v753 = svsub_f32_x(svptrue_b32(), v751, v694);
    svfloat32_t v775 = svadd_f32_x(svptrue_b32(), v553, v625);
    svfloat32_t v823 = svadd_f32_x(svptrue_b32(), v554, v626);
    svfloat32_t v848 = svadd_f32_x(svptrue_b32(), v847, v693);
    svfloat32_t v849 = svsub_f32_x(svptrue_b32(), v847, v693);
    svfloat32_t v871 = svadd_f32_x(svptrue_b32(), v551, v623);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1154), v1353,
                               svreinterpret_f64_f32(v705));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1163), v1353,
                               svreinterpret_f64_f32(v704));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1172), v1353,
                               svreinterpret_f64_f32(v552));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1226), v1353,
                               svreinterpret_f64_f32(v553));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1262), v1353,
                               svreinterpret_f64_f32(v801));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1271), v1353,
                               svreinterpret_f64_f32(v800));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1280), v1353,
                               svreinterpret_f64_f32(v554));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1334), v1353,
                               svreinterpret_f64_f32(v551));
    svfloat32_t v728 = svadd_f32_x(svptrue_b32(), v727, v700);
    svfloat32_t v729 = svsub_f32_x(svptrue_b32(), v727, v700);
    svfloat32_t v776 = svadd_f32_x(svptrue_b32(), v775, v701);
    svfloat32_t v777 = svsub_f32_x(svptrue_b32(), v775, v701);
    svfloat32_t v824 = svadd_f32_x(svptrue_b32(), v823, v702);
    svfloat32_t v825 = svsub_f32_x(svptrue_b32(), v823, v702);
    svfloat32_t v872 = svadd_f32_x(svptrue_b32(), v871, v699);
    svfloat32_t v873 = svsub_f32_x(svptrue_b32(), v871, v699);
    svst1_scatter_s64index_f64(pred_full, (double *)(v1208), v1353,
                               svreinterpret_f64_f32(v753));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1217), v1353,
                               svreinterpret_f64_f32(v752));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1316), v1353,
                               svreinterpret_f64_f32(v849));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1325), v1353,
                               svreinterpret_f64_f32(v848));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1181), v1353,
                               svreinterpret_f64_f32(v729));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1190), v1353,
                               svreinterpret_f64_f32(v728));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1235), v1353,
                               svreinterpret_f64_f32(v777));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1244), v1353,
                               svreinterpret_f64_f32(v776));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1289), v1353,
                               svreinterpret_f64_f32(v825));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1298), v1353,
                               svreinterpret_f64_f32(v824));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1343), v1353,
                               svreinterpret_f64_f32(v873));
    svst1_scatter_s64index_f64(pred_full, (double *)(v1352), v1353,
                               svreinterpret_f64_f32(v872));
    v5 += v11;
    v6 += v12;
  }
}
#endif

#ifndef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs25(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  for (int j = 0; j < howmany; j += 1) {
    float32x2_t v92 = v5[istride];
    float v1163 = 9.6858316112863108e-01F;
    float v1166 = -2.4868988716485479e-01F;
    float v1167 = 2.4868988716485479e-01F;
    float v1302 = 8.7630668004386358e-01F;
    float v1305 = -4.8175367410171532e-01F;
    float v1306 = 4.8175367410171532e-01F;
    float v1441 = 7.2896862742141155e-01F;
    float v1444 = -6.8454710592868862e-01F;
    float v1445 = 6.8454710592868862e-01F;
    float v1453 = 6.2790519529313527e-02F;
    float v1456 = -9.9802672842827156e-01F;
    float v1457 = 9.9802672842827156e-01F;
    float v1580 = 5.3582679497899655e-01F;
    float v1583 = -8.4432792550201508e-01F;
    float v1584 = 8.4432792550201508e-01F;
    float v1592 = -4.2577929156507272e-01F;
    float v1595 = -9.0482705246601947e-01F;
    float v1596 = 9.0482705246601947e-01F;
    float v1604 = -6.3742398974868952e-01F;
    float v1607 = 7.7051324277578936e-01F;
    float v1608 = -7.7051324277578936e-01F;
    float v1622 = -9.9211470131447776e-01F;
    float v1625 = -1.2533323356430454e-01F;
    float v1626 = 1.2533323356430454e-01F;
    float v1642 = 2.5000000000000000e-01F;
    float v1652 = 5.5901699437494745e-01F;
    float v1662 = 6.1803398874989490e-01F;
    float v1685 = 9.5105651629515353e-01F;
    float v1686 = -9.5105651629515353e-01F;
    float32x2_t v1688 = (float32x2_t){v4, v4};
    float v1709 = 2.0000000000000000e+00F;
    float32x2_t v98 = vtrn1_f32(v92, v92);
    float32x2_t v99 = vtrn2_f32(v92, v92);
    float32x2_t v452 = v5[0];
    float32x2_t v1164 = (float32x2_t){v1163, v1163};
    float32x2_t v1168 = (float32x2_t){v1166, v1167};
    float32x2_t v1303 = (float32x2_t){v1302, v1302};
    float32x2_t v1307 = (float32x2_t){v1305, v1306};
    float32x2_t v1442 = (float32x2_t){v1441, v1441};
    float32x2_t v1446 = (float32x2_t){v1444, v1445};
    float32x2_t v1454 = (float32x2_t){v1453, v1453};
    float32x2_t v1458 = (float32x2_t){v1456, v1457};
    float32x2_t v1488 = (float32x2_t){v1608, v1607};
    float32x2_t v1581 = (float32x2_t){v1580, v1580};
    float32x2_t v1585 = (float32x2_t){v1583, v1584};
    float32x2_t v1593 = (float32x2_t){v1592, v1592};
    float32x2_t v1597 = (float32x2_t){v1595, v1596};
    float32x2_t v1605 = (float32x2_t){v1604, v1604};
    float32x2_t v1609 = (float32x2_t){v1607, v1608};
    float32x2_t v1623 = (float32x2_t){v1622, v1622};
    float32x2_t v1627 = (float32x2_t){v1625, v1626};
    float32x2_t v1643 = (float32x2_t){v1642, v1642};
    float32x2_t v1653 = (float32x2_t){v1652, v1652};
    float32x2_t v1663 = (float32x2_t){v1662, v1662};
    float32x2_t v1687 = (float32x2_t){v1685, v1686};
    float32x2_t v1710 = (float32x2_t){v1709, v1709};
    float32x2_t v20 = v5[istride * 5];
    int64_t v24 = 8 + j * 48;
    float32x2_t v38 = v5[istride * 10];
    int64_t v42 = 18 + j * 48;
    float32x2_t v56 = v5[istride * 15];
    int64_t v60 = 28 + j * 48;
    float32x2_t v74 = v5[istride * 20];
    int64_t v78 = 38 + j * 48;
    float32x2_t v97 = v7[j * 48];
    int64_t v101 = j * 48 + 1;
    float32x2_t v110 = v5[istride * 6];
    int64_t v114 = 10 + j * 48;
    float32x2_t v128 = v5[istride * 11];
    int64_t v132 = 20 + j * 48;
    float32x2_t v146 = v5[istride * 16];
    int64_t v150 = 30 + j * 48;
    float32x2_t v164 = v5[istride * 21];
    int64_t v168 = 40 + j * 48;
    float32x2_t v182 = v5[istride * 2];
    int64_t v186 = 2 + j * 48;
    float32x2_t v200 = v5[istride * 7];
    int64_t v204 = 12 + j * 48;
    float32x2_t v218 = v5[istride * 12];
    int64_t v222 = 22 + j * 48;
    float32x2_t v236 = v5[istride * 17];
    int64_t v240 = 32 + j * 48;
    float32x2_t v254 = v5[istride * 22];
    int64_t v258 = 42 + j * 48;
    float32x2_t v272 = v5[istride * 3];
    int64_t v276 = 4 + j * 48;
    float32x2_t v290 = v5[istride * 8];
    int64_t v294 = 14 + j * 48;
    float32x2_t v308 = v5[istride * 13];
    int64_t v312 = 24 + j * 48;
    float32x2_t v326 = v5[istride * 18];
    int64_t v330 = 34 + j * 48;
    float32x2_t v344 = v5[istride * 23];
    int64_t v348 = 44 + j * 48;
    float32x2_t v362 = v5[istride * 4];
    int64_t v366 = 6 + j * 48;
    float32x2_t v380 = v5[istride * 9];
    int64_t v384 = 16 + j * 48;
    float32x2_t v398 = v5[istride * 14];
    int64_t v402 = 26 + j * 48;
    float32x2_t v416 = v5[istride * 19];
    int64_t v420 = 36 + j * 48;
    float32x2_t v434 = v5[istride * 24];
    int64_t v438 = 46 + j * 48;
    float32x2_t v1170 = vmul_f32(v1688, v1168);
    float32x2_t v1309 = vmul_f32(v1688, v1307);
    float32x2_t v1448 = vmul_f32(v1688, v1446);
    float32x2_t v1460 = vmul_f32(v1688, v1458);
    float32x2_t v1490 = vmul_f32(v1688, v1488);
    float32x2_t v1587 = vmul_f32(v1688, v1585);
    float32x2_t v1599 = vmul_f32(v1688, v1597);
    float32x2_t v1611 = vmul_f32(v1688, v1609);
    float32x2_t v1629 = vmul_f32(v1688, v1627);
    float32x2_t v1689 = vmul_f32(v1688, v1687);
    float32x2_t v25 = v7[v24];
    float32x2_t v26 = vtrn1_f32(v20, v20);
    float32x2_t v27 = vtrn2_f32(v20, v20);
    int64_t v29 = v24 + 1;
    float32x2_t v43 = v7[v42];
    float32x2_t v44 = vtrn1_f32(v38, v38);
    float32x2_t v45 = vtrn2_f32(v38, v38);
    int64_t v47 = v42 + 1;
    float32x2_t v61 = v7[v60];
    float32x2_t v62 = vtrn1_f32(v56, v56);
    float32x2_t v63 = vtrn2_f32(v56, v56);
    int64_t v65 = v60 + 1;
    float32x2_t v79 = v7[v78];
    float32x2_t v80 = vtrn1_f32(v74, v74);
    float32x2_t v81 = vtrn2_f32(v74, v74);
    int64_t v83 = v78 + 1;
    float32x2_t v102 = v7[v101];
    float32x2_t v103 = vmul_f32(v98, v97);
    float32x2_t v115 = v7[v114];
    float32x2_t v116 = vtrn1_f32(v110, v110);
    float32x2_t v117 = vtrn2_f32(v110, v110);
    int64_t v119 = v114 + 1;
    float32x2_t v133 = v7[v132];
    float32x2_t v134 = vtrn1_f32(v128, v128);
    float32x2_t v135 = vtrn2_f32(v128, v128);
    int64_t v137 = v132 + 1;
    float32x2_t v151 = v7[v150];
    float32x2_t v152 = vtrn1_f32(v146, v146);
    float32x2_t v153 = vtrn2_f32(v146, v146);
    int64_t v155 = v150 + 1;
    float32x2_t v169 = v7[v168];
    float32x2_t v170 = vtrn1_f32(v164, v164);
    float32x2_t v171 = vtrn2_f32(v164, v164);
    int64_t v173 = v168 + 1;
    float32x2_t v187 = v7[v186];
    float32x2_t v188 = vtrn1_f32(v182, v182);
    float32x2_t v189 = vtrn2_f32(v182, v182);
    int64_t v191 = v186 + 1;
    float32x2_t v205 = v7[v204];
    float32x2_t v206 = vtrn1_f32(v200, v200);
    float32x2_t v207 = vtrn2_f32(v200, v200);
    int64_t v209 = v204 + 1;
    float32x2_t v223 = v7[v222];
    float32x2_t v224 = vtrn1_f32(v218, v218);
    float32x2_t v225 = vtrn2_f32(v218, v218);
    int64_t v227 = v222 + 1;
    float32x2_t v241 = v7[v240];
    float32x2_t v242 = vtrn1_f32(v236, v236);
    float32x2_t v243 = vtrn2_f32(v236, v236);
    int64_t v245 = v240 + 1;
    float32x2_t v259 = v7[v258];
    float32x2_t v260 = vtrn1_f32(v254, v254);
    float32x2_t v261 = vtrn2_f32(v254, v254);
    int64_t v263 = v258 + 1;
    float32x2_t v277 = v7[v276];
    float32x2_t v278 = vtrn1_f32(v272, v272);
    float32x2_t v279 = vtrn2_f32(v272, v272);
    int64_t v281 = v276 + 1;
    float32x2_t v295 = v7[v294];
    float32x2_t v296 = vtrn1_f32(v290, v290);
    float32x2_t v297 = vtrn2_f32(v290, v290);
    int64_t v299 = v294 + 1;
    float32x2_t v313 = v7[v312];
    float32x2_t v314 = vtrn1_f32(v308, v308);
    float32x2_t v315 = vtrn2_f32(v308, v308);
    int64_t v317 = v312 + 1;
    float32x2_t v331 = v7[v330];
    float32x2_t v332 = vtrn1_f32(v326, v326);
    float32x2_t v333 = vtrn2_f32(v326, v326);
    int64_t v335 = v330 + 1;
    float32x2_t v349 = v7[v348];
    float32x2_t v350 = vtrn1_f32(v344, v344);
    float32x2_t v351 = vtrn2_f32(v344, v344);
    int64_t v353 = v348 + 1;
    float32x2_t v367 = v7[v366];
    float32x2_t v368 = vtrn1_f32(v362, v362);
    float32x2_t v369 = vtrn2_f32(v362, v362);
    int64_t v371 = v366 + 1;
    float32x2_t v385 = v7[v384];
    float32x2_t v386 = vtrn1_f32(v380, v380);
    float32x2_t v387 = vtrn2_f32(v380, v380);
    int64_t v389 = v384 + 1;
    float32x2_t v403 = v7[v402];
    float32x2_t v404 = vtrn1_f32(v398, v398);
    float32x2_t v405 = vtrn2_f32(v398, v398);
    int64_t v407 = v402 + 1;
    float32x2_t v421 = v7[v420];
    float32x2_t v422 = vtrn1_f32(v416, v416);
    float32x2_t v423 = vtrn2_f32(v416, v416);
    int64_t v425 = v420 + 1;
    float32x2_t v439 = v7[v438];
    float32x2_t v440 = vtrn1_f32(v434, v434);
    float32x2_t v441 = vtrn2_f32(v434, v434);
    int64_t v443 = v438 + 1;
    float32x2_t v30 = v7[v29];
    float32x2_t v31 = vmul_f32(v26, v25);
    float32x2_t v48 = v7[v47];
    float32x2_t v49 = vmul_f32(v44, v43);
    float32x2_t v66 = v7[v65];
    float32x2_t v67 = vmul_f32(v62, v61);
    float32x2_t v84 = v7[v83];
    float32x2_t v85 = vmul_f32(v80, v79);
    float32x2_t v120 = v7[v119];
    float32x2_t v121 = vmul_f32(v116, v115);
    float32x2_t v138 = v7[v137];
    float32x2_t v139 = vmul_f32(v134, v133);
    float32x2_t v156 = v7[v155];
    float32x2_t v157 = vmul_f32(v152, v151);
    float32x2_t v174 = v7[v173];
    float32x2_t v175 = vmul_f32(v170, v169);
    float32x2_t v192 = v7[v191];
    float32x2_t v193 = vmul_f32(v188, v187);
    float32x2_t v210 = v7[v209];
    float32x2_t v211 = vmul_f32(v206, v205);
    float32x2_t v228 = v7[v227];
    float32x2_t v229 = vmul_f32(v224, v223);
    float32x2_t v246 = v7[v245];
    float32x2_t v247 = vmul_f32(v242, v241);
    float32x2_t v264 = v7[v263];
    float32x2_t v265 = vmul_f32(v260, v259);
    float32x2_t v282 = v7[v281];
    float32x2_t v283 = vmul_f32(v278, v277);
    float32x2_t v300 = v7[v299];
    float32x2_t v301 = vmul_f32(v296, v295);
    float32x2_t v318 = v7[v317];
    float32x2_t v319 = vmul_f32(v314, v313);
    float32x2_t v336 = v7[v335];
    float32x2_t v337 = vmul_f32(v332, v331);
    float32x2_t v354 = v7[v353];
    float32x2_t v355 = vmul_f32(v350, v349);
    float32x2_t v372 = v7[v371];
    float32x2_t v373 = vmul_f32(v368, v367);
    float32x2_t v390 = v7[v389];
    float32x2_t v391 = vmul_f32(v386, v385);
    float32x2_t v408 = v7[v407];
    float32x2_t v409 = vmul_f32(v404, v403);
    float32x2_t v426 = v7[v425];
    float32x2_t v427 = vmul_f32(v422, v421);
    float32x2_t v444 = v7[v443];
    float32x2_t v445 = vmul_f32(v440, v439);
    float32x2_t v105 = vfma_f32(v103, v99, v102);
    float32x2_t v33 = vfma_f32(v31, v27, v30);
    float32x2_t v51 = vfma_f32(v49, v45, v48);
    float32x2_t v69 = vfma_f32(v67, v63, v66);
    float32x2_t v87 = vfma_f32(v85, v81, v84);
    float32x2_t v123 = vfma_f32(v121, v117, v120);
    float32x2_t v141 = vfma_f32(v139, v135, v138);
    float32x2_t v159 = vfma_f32(v157, v153, v156);
    float32x2_t v177 = vfma_f32(v175, v171, v174);
    float32x2_t v195 = vfma_f32(v193, v189, v192);
    float32x2_t v213 = vfma_f32(v211, v207, v210);
    float32x2_t v231 = vfma_f32(v229, v225, v228);
    float32x2_t v249 = vfma_f32(v247, v243, v246);
    float32x2_t v267 = vfma_f32(v265, v261, v264);
    float32x2_t v285 = vfma_f32(v283, v279, v282);
    float32x2_t v303 = vfma_f32(v301, v297, v300);
    float32x2_t v321 = vfma_f32(v319, v315, v318);
    float32x2_t v339 = vfma_f32(v337, v333, v336);
    float32x2_t v357 = vfma_f32(v355, v351, v354);
    float32x2_t v375 = vfma_f32(v373, v369, v372);
    float32x2_t v393 = vfma_f32(v391, v387, v390);
    float32x2_t v411 = vfma_f32(v409, v405, v408);
    float32x2_t v429 = vfma_f32(v427, v423, v426);
    float32x2_t v447 = vfma_f32(v445, v441, v444);
    float32x2_t v489 = vsub_f32(v33, v87);
    float32x2_t v493 = vmul_f32(v33, v1710);
    float32x2_t v507 = vsub_f32(v51, v69);
    float32x2_t v511 = vmul_f32(v51, v1710);
    float32x2_t v603 = vsub_f32(v123, v177);
    float32x2_t v607 = vmul_f32(v123, v1710);
    float32x2_t v621 = vsub_f32(v141, v159);
    float32x2_t v625 = vmul_f32(v141, v1710);
    float32x2_t v717 = vsub_f32(v213, v267);
    float32x2_t v721 = vmul_f32(v213, v1710);
    float32x2_t v735 = vsub_f32(v231, v249);
    float32x2_t v739 = vmul_f32(v231, v1710);
    float32x2_t v831 = vsub_f32(v303, v357);
    float32x2_t v835 = vmul_f32(v303, v1710);
    float32x2_t v849 = vsub_f32(v321, v339);
    float32x2_t v853 = vmul_f32(v321, v1710);
    float32x2_t v945 = vsub_f32(v393, v447);
    float32x2_t v949 = vmul_f32(v393, v1710);
    float32x2_t v963 = vsub_f32(v411, v429);
    float32x2_t v967 = vmul_f32(v411, v1710);
    float32x2_t v494 = vsub_f32(v493, v489);
    float32x2_t v512 = vsub_f32(v511, v507);
    float32x2_t v523 = vmul_f32(v507, v1663);
    float32x2_t v538 = vmul_f32(v489, v1663);
    float32x2_t v608 = vsub_f32(v607, v603);
    float32x2_t v626 = vsub_f32(v625, v621);
    float32x2_t v637 = vmul_f32(v621, v1663);
    float32x2_t v652 = vmul_f32(v603, v1663);
    float32x2_t v722 = vsub_f32(v721, v717);
    float32x2_t v740 = vsub_f32(v739, v735);
    float32x2_t v751 = vmul_f32(v735, v1663);
    float32x2_t v766 = vmul_f32(v717, v1663);
    float32x2_t v836 = vsub_f32(v835, v831);
    float32x2_t v854 = vsub_f32(v853, v849);
    float32x2_t v865 = vmul_f32(v849, v1663);
    float32x2_t v880 = vmul_f32(v831, v1663);
    float32x2_t v950 = vsub_f32(v949, v945);
    float32x2_t v968 = vsub_f32(v967, v963);
    float32x2_t v979 = vmul_f32(v963, v1663);
    float32x2_t v994 = vmul_f32(v945, v1663);
    float32x2_t v513 = vadd_f32(v494, v512);
    float32x2_t v514 = vsub_f32(v494, v512);
    float32x2_t v524 = vadd_f32(v489, v523);
    float32x2_t v539 = vsub_f32(v538, v507);
    float32x2_t v627 = vadd_f32(v608, v626);
    float32x2_t v628 = vsub_f32(v608, v626);
    float32x2_t v638 = vadd_f32(v603, v637);
    float32x2_t v653 = vsub_f32(v652, v621);
    float32x2_t v741 = vadd_f32(v722, v740);
    float32x2_t v742 = vsub_f32(v722, v740);
    float32x2_t v752 = vadd_f32(v717, v751);
    float32x2_t v767 = vsub_f32(v766, v735);
    float32x2_t v855 = vadd_f32(v836, v854);
    float32x2_t v856 = vsub_f32(v836, v854);
    float32x2_t v866 = vadd_f32(v831, v865);
    float32x2_t v881 = vsub_f32(v880, v849);
    float32x2_t v969 = vadd_f32(v950, v968);
    float32x2_t v970 = vsub_f32(v950, v968);
    float32x2_t v980 = vadd_f32(v945, v979);
    float32x2_t v995 = vsub_f32(v994, v963);
    float32x2_t v518 = vmul_f32(v513, v1643);
    float32x2_t v528 = vmul_f32(v514, v1653);
    float32x2_t v540 = vadd_f32(v452, v513);
    float32x2_t v546 = vrev64_f32(v524);
    float32x2_t v554 = vrev64_f32(v539);
    float32x2_t v632 = vmul_f32(v627, v1643);
    float32x2_t v642 = vmul_f32(v628, v1653);
    float32x2_t v654 = vadd_f32(v105, v627);
    float32x2_t v660 = vrev64_f32(v638);
    float32x2_t v668 = vrev64_f32(v653);
    float32x2_t v746 = vmul_f32(v741, v1643);
    float32x2_t v756 = vmul_f32(v742, v1653);
    float32x2_t v768 = vadd_f32(v195, v741);
    float32x2_t v774 = vrev64_f32(v752);
    float32x2_t v782 = vrev64_f32(v767);
    float32x2_t v860 = vmul_f32(v855, v1643);
    float32x2_t v870 = vmul_f32(v856, v1653);
    float32x2_t v882 = vadd_f32(v285, v855);
    float32x2_t v888 = vrev64_f32(v866);
    float32x2_t v896 = vrev64_f32(v881);
    float32x2_t v974 = vmul_f32(v969, v1643);
    float32x2_t v984 = vmul_f32(v970, v1653);
    float32x2_t v996 = vadd_f32(v375, v969);
    float32x2_t v1002 = vrev64_f32(v980);
    float32x2_t v1010 = vrev64_f32(v995);
    float32x2_t v519 = vsub_f32(v452, v518);
    float32x2_t v547 = vmul_f32(v546, v1689);
    float32x2_t v555 = vmul_f32(v554, v1689);
    float32x2_t v633 = vsub_f32(v105, v632);
    float32x2_t v661 = vmul_f32(v660, v1689);
    float32x2_t v669 = vmul_f32(v668, v1689);
    float32x2_t v747 = vsub_f32(v195, v746);
    float32x2_t v775 = vmul_f32(v774, v1689);
    float32x2_t v783 = vmul_f32(v782, v1689);
    float32x2_t v861 = vsub_f32(v285, v860);
    float32x2_t v889 = vmul_f32(v888, v1689);
    float32x2_t v897 = vmul_f32(v896, v1689);
    float32x2_t v975 = vsub_f32(v375, v974);
    float32x2_t v1003 = vmul_f32(v1002, v1689);
    float32x2_t v1011 = vmul_f32(v1010, v1689);
    float32x2_t v1059 = vsub_f32(v654, v996);
    float32x2_t v1063 = vmul_f32(v654, v1710);
    float32x2_t v1077 = vsub_f32(v768, v882);
    float32x2_t v1081 = vmul_f32(v768, v1710);
    float32x2_t v529 = vsub_f32(v519, v528);
    float32x2_t v533 = vmul_f32(v519, v1710);
    float32x2_t v643 = vsub_f32(v633, v642);
    float32x2_t v647 = vmul_f32(v633, v1710);
    float32x2_t v757 = vsub_f32(v747, v756);
    float32x2_t v761 = vmul_f32(v747, v1710);
    float32x2_t v871 = vsub_f32(v861, v870);
    float32x2_t v875 = vmul_f32(v861, v1710);
    float32x2_t v985 = vsub_f32(v975, v984);
    float32x2_t v989 = vmul_f32(v975, v1710);
    float32x2_t v1064 = vsub_f32(v1063, v1059);
    float32x2_t v1082 = vsub_f32(v1081, v1077);
    float32x2_t v1093 = vmul_f32(v1077, v1663);
    float32x2_t v1108 = vmul_f32(v1059, v1663);
    float32x2_t v534 = vsub_f32(v533, v529);
    float32x2_t v556 = vsub_f32(v529, v555);
    float32x2_t v560 = vmul_f32(v529, v1710);
    float32x2_t v648 = vsub_f32(v647, v643);
    float32x2_t v670 = vsub_f32(v643, v669);
    float32x2_t v674 = vmul_f32(v643, v1710);
    float32x2_t v762 = vsub_f32(v761, v757);
    float32x2_t v784 = vsub_f32(v757, v783);
    float32x2_t v788 = vmul_f32(v757, v1710);
    float32x2_t v876 = vsub_f32(v875, v871);
    float32x2_t v898 = vsub_f32(v871, v897);
    float32x2_t v902 = vmul_f32(v871, v1710);
    float32x2_t v990 = vsub_f32(v989, v985);
    float32x2_t v1012 = vsub_f32(v985, v1011);
    float32x2_t v1016 = vmul_f32(v985, v1710);
    float32x2_t v1083 = vadd_f32(v1064, v1082);
    float32x2_t v1084 = vsub_f32(v1064, v1082);
    float32x2_t v1094 = vadd_f32(v1059, v1093);
    float32x2_t v1109 = vsub_f32(v1108, v1077);
    float32x2_t v548 = vsub_f32(v534, v547);
    float32x2_t v561 = vsub_f32(v560, v556);
    float32x2_t v565 = vmul_f32(v534, v1710);
    float32x2_t v662 = vsub_f32(v648, v661);
    float32x2_t v675 = vsub_f32(v674, v670);
    float32x2_t v679 = vmul_f32(v648, v1710);
    float32x2_t v776 = vsub_f32(v762, v775);
    float32x2_t v789 = vsub_f32(v788, v784);
    float32x2_t v793 = vmul_f32(v762, v1710);
    float32x2_t v890 = vsub_f32(v876, v889);
    float32x2_t v903 = vsub_f32(v902, v898);
    float32x2_t v907 = vmul_f32(v876, v1710);
    float32x2_t v1004 = vsub_f32(v990, v1003);
    float32x2_t v1017 = vsub_f32(v1016, v1012);
    float32x2_t v1021 = vmul_f32(v990, v1710);
    float32x2_t v1088 = vmul_f32(v1083, v1643);
    float32x2_t v1098 = vmul_f32(v1084, v1653);
    float32x2_t v1110 = vadd_f32(v540, v1083);
    float32x2_t v1121 = vrev64_f32(v1094);
    float32x2_t v1134 = vrev64_f32(v1109);
    float32x2_t v1310 = vrev64_f32(v670);
    float32x2_t v1322 = vrev64_f32(v784);
    float32x2_t v1334 = vrev64_f32(v1012);
    float32x2_t v1352 = vrev64_f32(v898);
    float32x2_t v566 = vsub_f32(v565, v548);
    float32x2_t v680 = vsub_f32(v679, v662);
    float32x2_t v794 = vsub_f32(v793, v776);
    float32x2_t v908 = vsub_f32(v907, v890);
    float32x2_t v1022 = vsub_f32(v1021, v1004);
    float32x2_t v1089 = vsub_f32(v540, v1088);
    v6[0] = v1110;
    float32x2_t v1122 = vmul_f32(v1121, v1689);
    float32x2_t v1135 = vmul_f32(v1134, v1689);
    float32x2_t v1171 = vrev64_f32(v662);
    float32x2_t v1183 = vrev64_f32(v776);
    float32x2_t v1195 = vrev64_f32(v1004);
    float32x2_t v1213 = vrev64_f32(v890);
    float32x2_t v1311 = vmul_f32(v1310, v1309);
    float32x2_t v1323 = vmul_f32(v1322, v1587);
    float32x2_t v1335 = vmul_f32(v1334, v1599);
    float32x2_t v1353 = vmul_f32(v1352, v1460);
    float32x2_t v1449 = vrev64_f32(v675);
    float32x2_t v1461 = vrev64_f32(v789);
    float32x2_t v1473 = vrev64_f32(v1017);
    float32x2_t v1491 = vrev64_f32(v903);
    float32x2_t v1099 = vsub_f32(v1089, v1098);
    float32x2_t v1103 = vmul_f32(v1089, v1710);
    float32x2_t v1172 = vmul_f32(v1171, v1170);
    float32x2_t v1184 = vmul_f32(v1183, v1309);
    float32x2_t v1196 = vmul_f32(v1195, v1587);
    float32x2_t v1214 = vmul_f32(v1213, v1448);
    float32x2_t v1312 = vfma_f32(v1311, v670, v1303);
    float32x2_t v1324 = vfma_f32(v1323, v784, v1581);
    float32x2_t v1336 = vfma_f32(v1335, v1012, v1593);
    float32x2_t v1354 = vfma_f32(v1353, v898, v1454);
    float32x2_t v1450 = vmul_f32(v1449, v1448);
    float32x2_t v1462 = vmul_f32(v1461, v1460);
    float32x2_t v1474 = vmul_f32(v1473, v1629);
    float32x2_t v1492 = vmul_f32(v1491, v1490);
    float32x2_t v1588 = vrev64_f32(v680);
    float32x2_t v1600 = vrev64_f32(v794);
    float32x2_t v1612 = vrev64_f32(v1022);
    float32x2_t v1630 = vrev64_f32(v908);
    float32x2_t v1104 = vsub_f32(v1103, v1099);
    float32x2_t v1136 = vsub_f32(v1099, v1135);
    float32x2_t v1145 = vmul_f32(v1099, v1710);
    float32x2_t v1173 = vfma_f32(v1172, v662, v1164);
    float32x2_t v1185 = vfma_f32(v1184, v776, v1303);
    float32x2_t v1197 = vfma_f32(v1196, v1004, v1581);
    float32x2_t v1215 = vfma_f32(v1214, v890, v1442);
    float32x2_t v1337 = vsub_f32(v1312, v1336);
    float32x2_t v1341 = vmul_f32(v1312, v1710);
    float32x2_t v1355 = vsub_f32(v1324, v1354);
    float32x2_t v1359 = vmul_f32(v1324, v1710);
    float32x2_t v1451 = vfma_f32(v1450, v675, v1442);
    float32x2_t v1463 = vfma_f32(v1462, v789, v1454);
    float32x2_t v1475 = vfma_f32(v1474, v1017, v1623);
    float32x2_t v1493 = vfma_f32(v1492, v903, v1605);
    float32x2_t v1589 = vmul_f32(v1588, v1587);
    float32x2_t v1601 = vmul_f32(v1600, v1599);
    float32x2_t v1613 = vmul_f32(v1612, v1611);
    float32x2_t v1631 = vmul_f32(v1630, v1629);
    float32x2_t v1123 = vsub_f32(v1104, v1122);
    v6[ostride * 10] = v1136;
    float32x2_t v1146 = vsub_f32(v1145, v1136);
    float32x2_t v1155 = vmul_f32(v1104, v1710);
    float32x2_t v1198 = vsub_f32(v1173, v1197);
    float32x2_t v1202 = vmul_f32(v1173, v1710);
    float32x2_t v1216 = vsub_f32(v1185, v1215);
    float32x2_t v1220 = vmul_f32(v1185, v1710);
    float32x2_t v1342 = vsub_f32(v1341, v1337);
    float32x2_t v1360 = vsub_f32(v1359, v1355);
    float32x2_t v1371 = vmul_f32(v1355, v1663);
    float32x2_t v1386 = vmul_f32(v1337, v1663);
    float32x2_t v1476 = vsub_f32(v1451, v1475);
    float32x2_t v1480 = vmul_f32(v1451, v1710);
    float32x2_t v1494 = vsub_f32(v1463, v1493);
    float32x2_t v1498 = vmul_f32(v1463, v1710);
    float32x2_t v1590 = vfma_f32(v1589, v680, v1581);
    float32x2_t v1602 = vfma_f32(v1601, v794, v1593);
    float32x2_t v1614 = vfma_f32(v1613, v1022, v1605);
    float32x2_t v1632 = vfma_f32(v1631, v908, v1623);
    v6[ostride * 5] = v1123;
    v6[ostride * 15] = v1146;
    float32x2_t v1156 = vsub_f32(v1155, v1123);
    float32x2_t v1203 = vsub_f32(v1202, v1198);
    float32x2_t v1221 = vsub_f32(v1220, v1216);
    float32x2_t v1232 = vmul_f32(v1216, v1663);
    float32x2_t v1247 = vmul_f32(v1198, v1663);
    float32x2_t v1361 = vadd_f32(v1342, v1360);
    float32x2_t v1362 = vsub_f32(v1342, v1360);
    float32x2_t v1372 = vadd_f32(v1337, v1371);
    float32x2_t v1387 = vsub_f32(v1386, v1355);
    float32x2_t v1481 = vsub_f32(v1480, v1476);
    float32x2_t v1499 = vsub_f32(v1498, v1494);
    float32x2_t v1510 = vmul_f32(v1494, v1663);
    float32x2_t v1525 = vmul_f32(v1476, v1663);
    float32x2_t v1615 = vsub_f32(v1590, v1614);
    float32x2_t v1619 = vmul_f32(v1590, v1710);
    float32x2_t v1633 = vsub_f32(v1602, v1632);
    float32x2_t v1637 = vmul_f32(v1602, v1710);
    v6[ostride * 20] = v1156;
    float32x2_t v1222 = vadd_f32(v1203, v1221);
    float32x2_t v1223 = vsub_f32(v1203, v1221);
    float32x2_t v1233 = vadd_f32(v1198, v1232);
    float32x2_t v1248 = vsub_f32(v1247, v1216);
    float32x2_t v1366 = vmul_f32(v1361, v1643);
    float32x2_t v1376 = vmul_f32(v1362, v1653);
    float32x2_t v1388 = vadd_f32(v556, v1361);
    float32x2_t v1399 = vrev64_f32(v1372);
    float32x2_t v1412 = vrev64_f32(v1387);
    float32x2_t v1500 = vadd_f32(v1481, v1499);
    float32x2_t v1501 = vsub_f32(v1481, v1499);
    float32x2_t v1511 = vadd_f32(v1476, v1510);
    float32x2_t v1526 = vsub_f32(v1525, v1494);
    float32x2_t v1620 = vsub_f32(v1619, v1615);
    float32x2_t v1638 = vsub_f32(v1637, v1633);
    float32x2_t v1649 = vmul_f32(v1633, v1663);
    float32x2_t v1664 = vmul_f32(v1615, v1663);
    float32x2_t v1227 = vmul_f32(v1222, v1643);
    float32x2_t v1237 = vmul_f32(v1223, v1653);
    float32x2_t v1249 = vadd_f32(v548, v1222);
    float32x2_t v1260 = vrev64_f32(v1233);
    float32x2_t v1273 = vrev64_f32(v1248);
    float32x2_t v1367 = vsub_f32(v556, v1366);
    v6[ostride * 2] = v1388;
    float32x2_t v1400 = vmul_f32(v1399, v1689);
    float32x2_t v1413 = vmul_f32(v1412, v1689);
    float32x2_t v1505 = vmul_f32(v1500, v1643);
    float32x2_t v1515 = vmul_f32(v1501, v1653);
    float32x2_t v1527 = vadd_f32(v561, v1500);
    float32x2_t v1538 = vrev64_f32(v1511);
    float32x2_t v1551 = vrev64_f32(v1526);
    float32x2_t v1639 = vadd_f32(v1620, v1638);
    float32x2_t v1640 = vsub_f32(v1620, v1638);
    float32x2_t v1650 = vadd_f32(v1615, v1649);
    float32x2_t v1665 = vsub_f32(v1664, v1633);
    float32x2_t v1228 = vsub_f32(v548, v1227);
    v6[ostride] = v1249;
    float32x2_t v1261 = vmul_f32(v1260, v1689);
    float32x2_t v1274 = vmul_f32(v1273, v1689);
    float32x2_t v1377 = vsub_f32(v1367, v1376);
    float32x2_t v1381 = vmul_f32(v1367, v1710);
    float32x2_t v1506 = vsub_f32(v561, v1505);
    v6[ostride * 3] = v1527;
    float32x2_t v1539 = vmul_f32(v1538, v1689);
    float32x2_t v1552 = vmul_f32(v1551, v1689);
    float32x2_t v1644 = vmul_f32(v1639, v1643);
    float32x2_t v1654 = vmul_f32(v1640, v1653);
    float32x2_t v1666 = vadd_f32(v566, v1639);
    float32x2_t v1677 = vrev64_f32(v1650);
    float32x2_t v1690 = vrev64_f32(v1665);
    float32x2_t v1238 = vsub_f32(v1228, v1237);
    float32x2_t v1242 = vmul_f32(v1228, v1710);
    float32x2_t v1382 = vsub_f32(v1381, v1377);
    float32x2_t v1414 = vsub_f32(v1377, v1413);
    float32x2_t v1423 = vmul_f32(v1377, v1710);
    float32x2_t v1516 = vsub_f32(v1506, v1515);
    float32x2_t v1520 = vmul_f32(v1506, v1710);
    float32x2_t v1645 = vsub_f32(v566, v1644);
    v6[ostride * 4] = v1666;
    float32x2_t v1678 = vmul_f32(v1677, v1689);
    float32x2_t v1691 = vmul_f32(v1690, v1689);
    float32x2_t v1243 = vsub_f32(v1242, v1238);
    float32x2_t v1275 = vsub_f32(v1238, v1274);
    float32x2_t v1284 = vmul_f32(v1238, v1710);
    float32x2_t v1401 = vsub_f32(v1382, v1400);
    v6[ostride * 12] = v1414;
    float32x2_t v1424 = vsub_f32(v1423, v1414);
    float32x2_t v1433 = vmul_f32(v1382, v1710);
    float32x2_t v1521 = vsub_f32(v1520, v1516);
    float32x2_t v1553 = vsub_f32(v1516, v1552);
    float32x2_t v1562 = vmul_f32(v1516, v1710);
    float32x2_t v1655 = vsub_f32(v1645, v1654);
    float32x2_t v1659 = vmul_f32(v1645, v1710);
    float32x2_t v1262 = vsub_f32(v1243, v1261);
    v6[ostride * 11] = v1275;
    float32x2_t v1285 = vsub_f32(v1284, v1275);
    float32x2_t v1294 = vmul_f32(v1243, v1710);
    v6[ostride * 7] = v1401;
    v6[ostride * 17] = v1424;
    float32x2_t v1434 = vsub_f32(v1433, v1401);
    float32x2_t v1540 = vsub_f32(v1521, v1539);
    v6[ostride * 13] = v1553;
    float32x2_t v1563 = vsub_f32(v1562, v1553);
    float32x2_t v1572 = vmul_f32(v1521, v1710);
    float32x2_t v1660 = vsub_f32(v1659, v1655);
    float32x2_t v1692 = vsub_f32(v1655, v1691);
    float32x2_t v1701 = vmul_f32(v1655, v1710);
    v6[ostride * 6] = v1262;
    v6[ostride * 16] = v1285;
    float32x2_t v1295 = vsub_f32(v1294, v1262);
    v6[ostride * 22] = v1434;
    v6[ostride * 8] = v1540;
    v6[ostride * 18] = v1563;
    float32x2_t v1573 = vsub_f32(v1572, v1540);
    float32x2_t v1679 = vsub_f32(v1660, v1678);
    v6[ostride * 14] = v1692;
    float32x2_t v1702 = vsub_f32(v1701, v1692);
    float32x2_t v1711 = vmul_f32(v1660, v1710);
    v6[ostride * 21] = v1295;
    v6[ostride * 23] = v1573;
    v6[ostride * 9] = v1679;
    v6[ostride * 19] = v1702;
    float32x2_t v1712 = vsub_f32(v1711, v1679);
    v6[ostride * 24] = v1712;
    v5 += 1 * idist;
    v6 += 1 * odist;
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
void armral_fft_cf32_cf32_cf32_ab_t_gs25(const armral_cmplx_f32_t *restrict x,
                                         armral_cmplx_f32_t *restrict y,
                                         int istride, int ostride,
                                         const armral_cmplx_f32_t *restrict w,
                                         int howmany, int idist, int odist,
                                         float dir) {
  int64_t v0 = istride;
  int64_t v1 = idist;
  int64_t v2 = ostride;
  int64_t v3 = odist;
  float v4 = dir;
  const float32x2_t *v5 = (const float32x2_t *)x;
  float32x2_t *v6 = (float32x2_t *)y;
  const float32x2_t *v7 = (const float32x2_t *)w;
  int64_t v8 = howmany;
  int64_t v10 = svcntd();
  int64_t v11 = v10 * v1;
  int64_t v12 = v10 * v3;
  for (int j = 0; j < v8; j += v10) {
    svbool_t pred_full = svwhilelt_b32(j * 2, howmany * 2);
    int64_t v13 = j;
    float v1159 = 9.6858316112863108e-01F;
    float v1164 = 2.4868988716485479e-01F;
    float v1321 = 8.7630668004386358e-01F;
    float v1326 = 4.8175367410171532e-01F;
    float v1483 = 7.2896862742141155e-01F;
    float v1488 = 6.8454710592868862e-01F;
    float v1496 = 6.2790519529313527e-02F;
    float v1501 = 9.9802672842827156e-01F;
    float v1534 = 7.7051324277578925e-01F;
    float v1645 = 5.3582679497899655e-01F;
    float v1650 = 8.4432792550201508e-01F;
    float v1658 = -4.2577929156507272e-01F;
    float v1663 = 9.0482705246601947e-01F;
    float v1671 = -6.3742398974868952e-01F;
    float v1676 = -7.7051324277578936e-01F;
    float v1691 = -9.9211470131447776e-01F;
    float v1696 = 1.2533323356430454e-01F;
    float v1713 = 2.5000000000000000e-01F;
    float v1725 = 5.5901699437494745e-01F;
    float v1737 = 6.1803398874989490e-01F;
    float v1766 = -9.5105651629515353e-01F;
    float v1794 = 2.0000000000000000e+00F;
    const float32x2_t *v1848 = &v5[v0];
    float32x2_t *v2214 = &v6[v2];
    int64_t v19 = v0 * 5;
    int64_t v27 = v10 * 4;
    int64_t v33 = v0 * 10;
    int64_t v41 = v10 * 9;
    int64_t v47 = v0 * 15;
    int64_t v55 = v10 * 14;
    int64_t v61 = v0 * 20;
    int64_t v69 = v10 * 19;
    int64_t v89 = v0 * 6;
    int64_t v97 = v10 * 5;
    int64_t v103 = v0 * 11;
    int64_t v111 = v10 * 10;
    int64_t v117 = v0 * 16;
    int64_t v125 = v10 * 15;
    int64_t v131 = v0 * 21;
    int64_t v139 = v10 * 20;
    int64_t v145 = v0 * 2;
    int64_t v159 = v0 * 7;
    int64_t v167 = v10 * 6;
    int64_t v173 = v0 * 12;
    int64_t v181 = v10 * 11;
    int64_t v187 = v0 * 17;
    int64_t v195 = v10 * 16;
    int64_t v201 = v0 * 22;
    int64_t v209 = v10 * 21;
    int64_t v215 = v0 * 3;
    int64_t v223 = v10 * 2;
    int64_t v229 = v0 * 8;
    int64_t v237 = v10 * 7;
    int64_t v243 = v0 * 13;
    int64_t v251 = v10 * 12;
    int64_t v257 = v0 * 18;
    int64_t v265 = v10 * 17;
    int64_t v271 = v0 * 23;
    int64_t v279 = v10 * 22;
    int64_t v285 = v0 * 4;
    int64_t v293 = v10 * 3;
    int64_t v299 = v0 * 9;
    int64_t v307 = v10 * 8;
    int64_t v313 = v0 * 14;
    int64_t v321 = v10 * 13;
    int64_t v327 = v0 * 19;
    int64_t v335 = v10 * 18;
    int64_t v341 = v0 * 24;
    int64_t v349 = v10 * 23;
    int64_t v350 = v13 * 24;
    int64_t v1111 = v2 * 5;
    int64_t v1126 = v2 * 10;
    int64_t v1139 = v2 * 15;
    int64_t v1152 = v2 * 20;
    float v1167 = v4 * v1164;
    int64_t v1273 = v2 * 6;
    int64_t v1288 = v2 * 11;
    int64_t v1301 = v2 * 16;
    int64_t v1314 = v2 * 21;
    float v1329 = v4 * v1326;
    int64_t v1420 = v2 * 2;
    int64_t v1435 = v2 * 7;
    int64_t v1450 = v2 * 12;
    int64_t v1463 = v2 * 17;
    int64_t v1476 = v2 * 22;
    float v1491 = v4 * v1488;
    float v1504 = v4 * v1501;
    float v1537 = v4 * v1534;
    int64_t v1582 = v2 * 3;
    int64_t v1597 = v2 * 8;
    int64_t v1612 = v2 * 13;
    int64_t v1625 = v2 * 18;
    int64_t v1638 = v2 * 23;
    float v1653 = v4 * v1650;
    float v1666 = v4 * v1663;
    float v1679 = v4 * v1676;
    float v1699 = v4 * v1696;
    int64_t v1744 = v2 * 4;
    int64_t v1759 = v2 * 9;
    float v1769 = v4 * v1766;
    int64_t v1774 = v2 * 14;
    int64_t v1787 = v2 * 19;
    int64_t v1800 = v2 * 24;
    const float32x2_t *v2030 = &v5[0];
    svint64_t v2031 = svindex_s64(0, v1);
    svfloat32_t v2136 = svdup_n_f32(0);
    float32x2_t *v2150 = &v6[0];
    svfloat32_t v2193 = svdup_n_f32(v1159);
    svfloat32_t v2257 = svdup_n_f32(v1321);
    svfloat32_t v2321 = svdup_n_f32(v1483);
    svfloat32_t v2323 = svdup_n_f32(v1496);
    svfloat32_t v2385 = svdup_n_f32(v1645);
    svfloat32_t v2387 = svdup_n_f32(v1658);
    svfloat32_t v2389 = svdup_n_f32(v1671);
    svfloat32_t v2392 = svdup_n_f32(v1691);
    svfloat32_t v2395 = svdup_n_f32(v1713);
    svfloat32_t v2397 = svdup_n_f32(v1725);
    svfloat32_t v2399 = svdup_n_f32(v1737);
    svfloat32_t v2439 = svdup_n_f32(v1794);
    svint64_t v2447 = svindex_s64(0, v3);
    int64_t v29 = v27 + v350;
    int64_t v43 = v41 + v350;
    int64_t v57 = v55 + v350;
    int64_t v71 = v69 + v350;
    svfloat32_t v86 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v350]));
    int64_t v99 = v97 + v350;
    int64_t v113 = v111 + v350;
    int64_t v127 = v125 + v350;
    int64_t v141 = v139 + v350;
    int64_t v155 = v10 + v350;
    int64_t v169 = v167 + v350;
    int64_t v183 = v181 + v350;
    int64_t v197 = v195 + v350;
    int64_t v211 = v209 + v350;
    int64_t v225 = v223 + v350;
    int64_t v239 = v237 + v350;
    int64_t v253 = v251 + v350;
    int64_t v267 = v265 + v350;
    int64_t v281 = v279 + v350;
    int64_t v295 = v293 + v350;
    int64_t v309 = v307 + v350;
    int64_t v323 = v321 + v350;
    int64_t v337 = v335 + v350;
    int64_t v351 = v349 + v350;
    const float32x2_t *v1812 = &v5[v19];
    const float32x2_t *v1821 = &v5[v33];
    const float32x2_t *v1830 = &v5[v47];
    const float32x2_t *v1839 = &v5[v61];
    svfloat32_t v1850 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1848), v2031));
    const float32x2_t *v1858 = &v5[v89];
    const float32x2_t *v1867 = &v5[v103];
    const float32x2_t *v1876 = &v5[v117];
    const float32x2_t *v1885 = &v5[v131];
    const float32x2_t *v1894 = &v5[v145];
    const float32x2_t *v1903 = &v5[v159];
    const float32x2_t *v1912 = &v5[v173];
    const float32x2_t *v1921 = &v5[v187];
    const float32x2_t *v1930 = &v5[v201];
    const float32x2_t *v1939 = &v5[v215];
    const float32x2_t *v1948 = &v5[v229];
    const float32x2_t *v1957 = &v5[v243];
    const float32x2_t *v1966 = &v5[v257];
    const float32x2_t *v1975 = &v5[v271];
    const float32x2_t *v1984 = &v5[v285];
    const float32x2_t *v1993 = &v5[v299];
    const float32x2_t *v2002 = &v5[v313];
    const float32x2_t *v2011 = &v5[v327];
    const float32x2_t *v2020 = &v5[v341];
    svfloat32_t v2032 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2030), v2031));
    float32x2_t *v2160 = &v6[v1111];
    float32x2_t *v2170 = &v6[v1126];
    float32x2_t *v2180 = &v6[v1139];
    float32x2_t *v2190 = &v6[v1152];
    svfloat32_t v2194 = svdup_n_f32(v1167);
    float32x2_t *v2224 = &v6[v1273];
    float32x2_t *v2234 = &v6[v1288];
    float32x2_t *v2244 = &v6[v1301];
    float32x2_t *v2254 = &v6[v1314];
    svfloat32_t v2258 = svdup_n_f32(v1329);
    float32x2_t *v2278 = &v6[v1420];
    float32x2_t *v2288 = &v6[v1435];
    float32x2_t *v2298 = &v6[v1450];
    float32x2_t *v2308 = &v6[v1463];
    float32x2_t *v2318 = &v6[v1476];
    svfloat32_t v2322 = svdup_n_f32(v1491);
    svfloat32_t v2324 = svdup_n_f32(v1504);
    svfloat32_t v2329 = svdup_n_f32(v1537);
    float32x2_t *v2342 = &v6[v1582];
    float32x2_t *v2352 = &v6[v1597];
    float32x2_t *v2362 = &v6[v1612];
    float32x2_t *v2372 = &v6[v1625];
    float32x2_t *v2382 = &v6[v1638];
    svfloat32_t v2386 = svdup_n_f32(v1653);
    svfloat32_t v2388 = svdup_n_f32(v1666);
    svfloat32_t v2390 = svdup_n_f32(v1679);
    svfloat32_t v2393 = svdup_n_f32(v1699);
    float32x2_t *v2406 = &v6[v1744];
    float32x2_t *v2416 = &v6[v1759];
    svfloat32_t v2419 = svdup_n_f32(v1769);
    float32x2_t *v2426 = &v6[v1774];
    float32x2_t *v2436 = &v6[v1787];
    float32x2_t *v2446 = &v6[v1800];
    svfloat32_t v30 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v29]));
    svfloat32_t v44 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v43]));
    svfloat32_t v58 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v57]));
    svfloat32_t v72 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v71]));
    svfloat32_t zero87 = svdup_n_f32(0);
    svfloat32_t v87 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero87, v1850, v86, 0),
                     v1850, v86, 90);
    svfloat32_t v100 =
        svreinterpret_f32_f64(svld1_f64(pred_full, &((const double *)v7)[v99]));
    svfloat32_t v114 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v113]));
    svfloat32_t v128 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v127]));
    svfloat32_t v142 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v141]));
    svfloat32_t v156 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v155]));
    svfloat32_t v170 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v169]));
    svfloat32_t v184 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v183]));
    svfloat32_t v198 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v197]));
    svfloat32_t v212 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v211]));
    svfloat32_t v226 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v225]));
    svfloat32_t v240 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v239]));
    svfloat32_t v254 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v253]));
    svfloat32_t v268 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v267]));
    svfloat32_t v282 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v281]));
    svfloat32_t v296 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v295]));
    svfloat32_t v310 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v309]));
    svfloat32_t v324 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v323]));
    svfloat32_t v338 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v337]));
    svfloat32_t v352 = svreinterpret_f32_f64(
        svld1_f64(pred_full, &((const double *)v7)[v351]));
    svfloat32_t v1814 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1812), v2031));
    svfloat32_t v1823 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1821), v2031));
    svfloat32_t v1832 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1830), v2031));
    svfloat32_t v1841 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1839), v2031));
    svfloat32_t v1860 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1858), v2031));
    svfloat32_t v1869 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1867), v2031));
    svfloat32_t v1878 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1876), v2031));
    svfloat32_t v1887 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1885), v2031));
    svfloat32_t v1896 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1894), v2031));
    svfloat32_t v1905 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1903), v2031));
    svfloat32_t v1914 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1912), v2031));
    svfloat32_t v1923 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1921), v2031));
    svfloat32_t v1932 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1930), v2031));
    svfloat32_t v1941 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1939), v2031));
    svfloat32_t v1950 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1948), v2031));
    svfloat32_t v1959 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1957), v2031));
    svfloat32_t v1968 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1966), v2031));
    svfloat32_t v1977 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1975), v2031));
    svfloat32_t v1986 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1984), v2031));
    svfloat32_t v1995 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v1993), v2031));
    svfloat32_t v2004 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2002), v2031));
    svfloat32_t v2013 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2011), v2031));
    svfloat32_t v2022 = svreinterpret_f32_f64(
        svld1_gather_s64index_f64(pred_full, (const double *)(v2020), v2031));
    svfloat32_t zero31 = svdup_n_f32(0);
    svfloat32_t v31 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero31, v1814, v30, 0),
                     v1814, v30, 90);
    svfloat32_t zero45 = svdup_n_f32(0);
    svfloat32_t v45 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero45, v1823, v44, 0),
                     v1823, v44, 90);
    svfloat32_t zero59 = svdup_n_f32(0);
    svfloat32_t v59 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero59, v1832, v58, 0),
                     v1832, v58, 90);
    svfloat32_t zero73 = svdup_n_f32(0);
    svfloat32_t v73 =
        svcmla_f32_x(pred_full, svcmla_f32_x(pred_full, zero73, v1841, v72, 0),
                     v1841, v72, 90);
    svfloat32_t zero101 = svdup_n_f32(0);
    svfloat32_t v101 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero101, v1860, v100, 0), v1860,
        v100, 90);
    svfloat32_t zero115 = svdup_n_f32(0);
    svfloat32_t v115 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero115, v1869, v114, 0), v1869,
        v114, 90);
    svfloat32_t zero129 = svdup_n_f32(0);
    svfloat32_t v129 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero129, v1878, v128, 0), v1878,
        v128, 90);
    svfloat32_t zero143 = svdup_n_f32(0);
    svfloat32_t v143 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero143, v1887, v142, 0), v1887,
        v142, 90);
    svfloat32_t zero157 = svdup_n_f32(0);
    svfloat32_t v157 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero157, v1896, v156, 0), v1896,
        v156, 90);
    svfloat32_t zero171 = svdup_n_f32(0);
    svfloat32_t v171 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero171, v1905, v170, 0), v1905,
        v170, 90);
    svfloat32_t zero185 = svdup_n_f32(0);
    svfloat32_t v185 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero185, v1914, v184, 0), v1914,
        v184, 90);
    svfloat32_t zero199 = svdup_n_f32(0);
    svfloat32_t v199 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero199, v1923, v198, 0), v1923,
        v198, 90);
    svfloat32_t zero213 = svdup_n_f32(0);
    svfloat32_t v213 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero213, v1932, v212, 0), v1932,
        v212, 90);
    svfloat32_t zero227 = svdup_n_f32(0);
    svfloat32_t v227 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero227, v1941, v226, 0), v1941,
        v226, 90);
    svfloat32_t zero241 = svdup_n_f32(0);
    svfloat32_t v241 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero241, v1950, v240, 0), v1950,
        v240, 90);
    svfloat32_t zero255 = svdup_n_f32(0);
    svfloat32_t v255 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero255, v1959, v254, 0), v1959,
        v254, 90);
    svfloat32_t zero269 = svdup_n_f32(0);
    svfloat32_t v269 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero269, v1968, v268, 0), v1968,
        v268, 90);
    svfloat32_t zero283 = svdup_n_f32(0);
    svfloat32_t v283 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero283, v1977, v282, 0), v1977,
        v282, 90);
    svfloat32_t zero297 = svdup_n_f32(0);
    svfloat32_t v297 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero297, v1986, v296, 0), v1986,
        v296, 90);
    svfloat32_t zero311 = svdup_n_f32(0);
    svfloat32_t v311 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero311, v1995, v310, 0), v1995,
        v310, 90);
    svfloat32_t zero325 = svdup_n_f32(0);
    svfloat32_t v325 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero325, v2004, v324, 0), v2004,
        v324, 90);
    svfloat32_t zero339 = svdup_n_f32(0);
    svfloat32_t v339 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero339, v2013, v338, 0), v2013,
        v338, 90);
    svfloat32_t zero353 = svdup_n_f32(0);
    svfloat32_t v353 = svcmla_f32_x(
        pred_full, svcmla_f32_x(pred_full, zero353, v2022, v352, 0), v2022,
        v352, 90);
    svfloat32_t v373 = svcmla_f32_x(pred_full, v31, v2136, v31, 90);
    svfloat32_t v386 = svcmla_f32_x(pred_full, v45, v2136, v45, 90);
    svfloat32_t v399 = svcmla_f32_x(pred_full, v73, v2136, v73, 90);
    svfloat32_t v419 = svcmla_f32_x(pred_full, v59, v2136, v59, 90);
    svfloat32_t v500 = svcmla_f32_x(pred_full, v101, v2136, v101, 90);
    svfloat32_t v513 = svcmla_f32_x(pred_full, v115, v2136, v115, 90);
    svfloat32_t v526 = svcmla_f32_x(pred_full, v143, v2136, v143, 90);
    svfloat32_t v546 = svcmla_f32_x(pred_full, v129, v2136, v129, 90);
    svfloat32_t v627 = svcmla_f32_x(pred_full, v171, v2136, v171, 90);
    svfloat32_t v640 = svcmla_f32_x(pred_full, v185, v2136, v185, 90);
    svfloat32_t v653 = svcmla_f32_x(pred_full, v213, v2136, v213, 90);
    svfloat32_t v673 = svcmla_f32_x(pred_full, v199, v2136, v199, 90);
    svfloat32_t v754 = svcmla_f32_x(pred_full, v241, v2136, v241, 90);
    svfloat32_t v767 = svcmla_f32_x(pred_full, v255, v2136, v255, 90);
    svfloat32_t v780 = svcmla_f32_x(pred_full, v283, v2136, v283, 90);
    svfloat32_t v800 = svcmla_f32_x(pred_full, v269, v2136, v269, 90);
    svfloat32_t v881 = svcmla_f32_x(pred_full, v311, v2136, v311, 90);
    svfloat32_t v894 = svcmla_f32_x(pred_full, v325, v2136, v325, 90);
    svfloat32_t v907 = svcmla_f32_x(pred_full, v353, v2136, v353, 90);
    svfloat32_t v927 = svcmla_f32_x(pred_full, v339, v2136, v339, 90);
    svfloat32_t v400 = svsub_f32_x(svptrue_b32(), v373, v399);
    svfloat32_t v420 = svsub_f32_x(svptrue_b32(), v386, v419);
    svfloat32_t v527 = svsub_f32_x(svptrue_b32(), v500, v526);
    svfloat32_t v547 = svsub_f32_x(svptrue_b32(), v513, v546);
    svfloat32_t v654 = svsub_f32_x(svptrue_b32(), v627, v653);
    svfloat32_t v674 = svsub_f32_x(svptrue_b32(), v640, v673);
    svfloat32_t v781 = svsub_f32_x(svptrue_b32(), v754, v780);
    svfloat32_t v801 = svsub_f32_x(svptrue_b32(), v767, v800);
    svfloat32_t v908 = svsub_f32_x(svptrue_b32(), v881, v907);
    svfloat32_t v928 = svsub_f32_x(svptrue_b32(), v894, v927);
    svfloat32_t v406 = svnmls_f32_x(pred_full, v400, v373, v2439);
    svfloat32_t v426 = svnmls_f32_x(pred_full, v420, v386, v2439);
    svfloat32_t v533 = svnmls_f32_x(pred_full, v527, v500, v2439);
    svfloat32_t v553 = svnmls_f32_x(pred_full, v547, v513, v2439);
    svfloat32_t v660 = svnmls_f32_x(pred_full, v654, v627, v2439);
    svfloat32_t v680 = svnmls_f32_x(pred_full, v674, v640, v2439);
    svfloat32_t v787 = svnmls_f32_x(pred_full, v781, v754, v2439);
    svfloat32_t v807 = svnmls_f32_x(pred_full, v801, v767, v2439);
    svfloat32_t v914 = svnmls_f32_x(pred_full, v908, v881, v2439);
    svfloat32_t v934 = svnmls_f32_x(pred_full, v928, v894, v2439);
    svfloat32_t v427 = svadd_f32_x(svptrue_b32(), v406, v426);
    svfloat32_t v428 = svsub_f32_x(svptrue_b32(), v406, v426);
    svfloat32_t v440 = svmla_f32_x(pred_full, v400, v420, v2399);
    svfloat32_t v458 = svnmls_f32_x(pred_full, v420, v400, v2399);
    svfloat32_t v554 = svadd_f32_x(svptrue_b32(), v533, v553);
    svfloat32_t v555 = svsub_f32_x(svptrue_b32(), v533, v553);
    svfloat32_t v567 = svmla_f32_x(pred_full, v527, v547, v2399);
    svfloat32_t v585 = svnmls_f32_x(pred_full, v547, v527, v2399);
    svfloat32_t v681 = svadd_f32_x(svptrue_b32(), v660, v680);
    svfloat32_t v682 = svsub_f32_x(svptrue_b32(), v660, v680);
    svfloat32_t v694 = svmla_f32_x(pred_full, v654, v674, v2399);
    svfloat32_t v712 = svnmls_f32_x(pred_full, v674, v654, v2399);
    svfloat32_t v808 = svadd_f32_x(svptrue_b32(), v787, v807);
    svfloat32_t v809 = svsub_f32_x(svptrue_b32(), v787, v807);
    svfloat32_t v821 = svmla_f32_x(pred_full, v781, v801, v2399);
    svfloat32_t v839 = svnmls_f32_x(pred_full, v801, v781, v2399);
    svfloat32_t v935 = svadd_f32_x(svptrue_b32(), v914, v934);
    svfloat32_t v936 = svsub_f32_x(svptrue_b32(), v914, v934);
    svfloat32_t v948 = svmla_f32_x(pred_full, v908, v928, v2399);
    svfloat32_t v966 = svnmls_f32_x(pred_full, v928, v908, v2399);
    svfloat32_t v459 = svadd_f32_x(svptrue_b32(), v2032, v427);
    svfloat32_t zero466 = svdup_n_f32(0);
    svfloat32_t v466 = svcmla_f32_x(pred_full, zero466, v2419, v440, 90);
    svfloat32_t zero474 = svdup_n_f32(0);
    svfloat32_t v474 = svcmla_f32_x(pred_full, zero474, v2419, v458, 90);
    svfloat32_t v586 = svadd_f32_x(svptrue_b32(), v87, v554);
    svfloat32_t zero593 = svdup_n_f32(0);
    svfloat32_t v593 = svcmla_f32_x(pred_full, zero593, v2419, v567, 90);
    svfloat32_t zero601 = svdup_n_f32(0);
    svfloat32_t v601 = svcmla_f32_x(pred_full, zero601, v2419, v585, 90);
    svfloat32_t v713 = svadd_f32_x(svptrue_b32(), v157, v681);
    svfloat32_t zero720 = svdup_n_f32(0);
    svfloat32_t v720 = svcmla_f32_x(pred_full, zero720, v2419, v694, 90);
    svfloat32_t zero728 = svdup_n_f32(0);
    svfloat32_t v728 = svcmla_f32_x(pred_full, zero728, v2419, v712, 90);
    svfloat32_t v840 = svadd_f32_x(svptrue_b32(), v227, v808);
    svfloat32_t zero847 = svdup_n_f32(0);
    svfloat32_t v847 = svcmla_f32_x(pred_full, zero847, v2419, v821, 90);
    svfloat32_t zero855 = svdup_n_f32(0);
    svfloat32_t v855 = svcmla_f32_x(pred_full, zero855, v2419, v839, 90);
    svfloat32_t v967 = svadd_f32_x(svptrue_b32(), v297, v935);
    svfloat32_t zero974 = svdup_n_f32(0);
    svfloat32_t v974 = svcmla_f32_x(pred_full, zero974, v2419, v948, 90);
    svfloat32_t zero982 = svdup_n_f32(0);
    svfloat32_t v982 = svcmla_f32_x(pred_full, zero982, v2419, v966, 90);
    svfloat32_t v434 = svmls_f32_x(pred_full, v2032, v427, v2395);
    svfloat32_t v561 = svmls_f32_x(pred_full, v87, v554, v2395);
    svfloat32_t v688 = svmls_f32_x(pred_full, v157, v681, v2395);
    svfloat32_t v815 = svmls_f32_x(pred_full, v227, v808, v2395);
    svfloat32_t v942 = svmls_f32_x(pred_full, v297, v935, v2395);
    svfloat32_t v446 = svmls_f32_x(pred_full, v434, v428, v2397);
    svfloat32_t v573 = svmls_f32_x(pred_full, v561, v555, v2397);
    svfloat32_t v700 = svmls_f32_x(pred_full, v688, v682, v2397);
    svfloat32_t v827 = svmls_f32_x(pred_full, v815, v809, v2397);
    svfloat32_t v954 = svmls_f32_x(pred_full, v942, v936, v2397);
    svfloat32_t v1008 = svcmla_f32_x(pred_full, v586, v2136, v586, 90);
    svfloat32_t v1021 = svcmla_f32_x(pred_full, v713, v2136, v713, 90);
    svfloat32_t v1034 = svcmla_f32_x(pred_full, v967, v2136, v967, 90);
    svfloat32_t v1054 = svcmla_f32_x(pred_full, v840, v2136, v840, 90);
    svfloat32_t v452 = svnmls_f32_x(pred_full, v446, v434, v2439);
    svfloat32_t v475 = svsub_f32_x(svptrue_b32(), v446, v474);
    svfloat32_t v579 = svnmls_f32_x(pred_full, v573, v561, v2439);
    svfloat32_t v602 = svsub_f32_x(svptrue_b32(), v573, v601);
    svfloat32_t v706 = svnmls_f32_x(pred_full, v700, v688, v2439);
    svfloat32_t v729 = svsub_f32_x(svptrue_b32(), v700, v728);
    svfloat32_t v833 = svnmls_f32_x(pred_full, v827, v815, v2439);
    svfloat32_t v856 = svsub_f32_x(svptrue_b32(), v827, v855);
    svfloat32_t v960 = svnmls_f32_x(pred_full, v954, v942, v2439);
    svfloat32_t v983 = svsub_f32_x(svptrue_b32(), v954, v982);
    svfloat32_t v1035 = svsub_f32_x(svptrue_b32(), v1008, v1034);
    svfloat32_t v1055 = svsub_f32_x(svptrue_b32(), v1021, v1054);
    svfloat32_t v467 = svsub_f32_x(svptrue_b32(), v452, v466);
    svfloat32_t v481 = svnmls_f32_x(pred_full, v475, v446, v2439);
    svfloat32_t v594 = svsub_f32_x(svptrue_b32(), v579, v593);
    svfloat32_t v608 = svnmls_f32_x(pred_full, v602, v573, v2439);
    svfloat32_t v721 = svsub_f32_x(svptrue_b32(), v706, v720);
    svfloat32_t v735 = svnmls_f32_x(pred_full, v729, v700, v2439);
    svfloat32_t v848 = svsub_f32_x(svptrue_b32(), v833, v847);
    svfloat32_t v862 = svnmls_f32_x(pred_full, v856, v827, v2439);
    svfloat32_t v975 = svsub_f32_x(svptrue_b32(), v960, v974);
    svfloat32_t v989 = svnmls_f32_x(pred_full, v983, v954, v2439);
    svfloat32_t v1041 = svnmls_f32_x(pred_full, v1035, v1008, v2439);
    svfloat32_t v1061 = svnmls_f32_x(pred_full, v1055, v1021, v2439);
    svfloat32_t v1324 = svmul_f32_x(svptrue_b32(), v602, v2257);
    svfloat32_t v1337 = svmul_f32_x(svptrue_b32(), v729, v2385);
    svfloat32_t v1350 = svmul_f32_x(svptrue_b32(), v983, v2387);
    svfloat32_t v1370 = svmul_f32_x(svptrue_b32(), v856, v2323);
    svfloat32_t v487 = svnmls_f32_x(pred_full, v467, v452, v2439);
    svfloat32_t v614 = svnmls_f32_x(pred_full, v594, v579, v2439);
    svfloat32_t v741 = svnmls_f32_x(pred_full, v721, v706, v2439);
    svfloat32_t v868 = svnmls_f32_x(pred_full, v848, v833, v2439);
    svfloat32_t v995 = svnmls_f32_x(pred_full, v975, v960, v2439);
    svfloat32_t v1062 = svadd_f32_x(svptrue_b32(), v1041, v1061);
    svfloat32_t v1063 = svsub_f32_x(svptrue_b32(), v1041, v1061);
    svfloat32_t v1075 = svmla_f32_x(pred_full, v1035, v1055, v2399);
    svfloat32_t v1093 = svnmls_f32_x(pred_full, v1055, v1035, v2399);
    svfloat32_t v1162 = svmul_f32_x(svptrue_b32(), v594, v2193);
    svfloat32_t v1175 = svmul_f32_x(svptrue_b32(), v721, v2257);
    svfloat32_t v1188 = svmul_f32_x(svptrue_b32(), v975, v2385);
    svfloat32_t v1208 = svmul_f32_x(svptrue_b32(), v848, v2321);
    svfloat32_t v1332 = svcmla_f32_x(pred_full, v1324, v2258, v602, 90);
    svfloat32_t v1345 = svcmla_f32_x(pred_full, v1337, v2386, v729, 90);
    svfloat32_t v1358 = svcmla_f32_x(pred_full, v1350, v2388, v983, 90);
    svfloat32_t v1378 = svcmla_f32_x(pred_full, v1370, v2324, v856, 90);
    svfloat32_t v1486 = svmul_f32_x(svptrue_b32(), v608, v2321);
    svfloat32_t v1499 = svmul_f32_x(svptrue_b32(), v735, v2323);
    svfloat32_t v1512 = svmul_f32_x(svptrue_b32(), v989, v2392);
    svfloat32_t v1532 = svmul_f32_x(svptrue_b32(), v862, v2389);
    svfloat32_t v1094 = svadd_f32_x(svptrue_b32(), v459, v1062);
    svfloat32_t zero1108 = svdup_n_f32(0);
    svfloat32_t v1108 = svcmla_f32_x(pred_full, zero1108, v2419, v1075, 90);
    svfloat32_t zero1123 = svdup_n_f32(0);
    svfloat32_t v1123 = svcmla_f32_x(pred_full, zero1123, v2419, v1093, 90);
    svfloat32_t v1170 = svcmla_f32_x(pred_full, v1162, v2194, v594, 90);
    svfloat32_t v1183 = svcmla_f32_x(pred_full, v1175, v2258, v721, 90);
    svfloat32_t v1196 = svcmla_f32_x(pred_full, v1188, v2386, v975, 90);
    svfloat32_t v1216 = svcmla_f32_x(pred_full, v1208, v2322, v848, 90);
    svfloat32_t v1359 = svsub_f32_x(svptrue_b32(), v1332, v1358);
    svfloat32_t v1379 = svsub_f32_x(svptrue_b32(), v1345, v1378);
    svfloat32_t v1494 = svcmla_f32_x(pred_full, v1486, v2322, v608, 90);
    svfloat32_t v1507 = svcmla_f32_x(pred_full, v1499, v2324, v735, 90);
    svfloat32_t v1520 = svcmla_f32_x(pred_full, v1512, v2393, v989, 90);
    svfloat32_t v1540 = svcmla_f32_x(pred_full, v1532, v2329, v862, 90);
    svfloat32_t v1648 = svmul_f32_x(svptrue_b32(), v614, v2385);
    svfloat32_t v1661 = svmul_f32_x(svptrue_b32(), v741, v2387);
    svfloat32_t v1674 = svmul_f32_x(svptrue_b32(), v995, v2389);
    svfloat32_t v1694 = svmul_f32_x(svptrue_b32(), v868, v2392);
    svfloat32_t v1069 = svmls_f32_x(pred_full, v459, v1062, v2395);
    svfloat32_t v1197 = svsub_f32_x(svptrue_b32(), v1170, v1196);
    svfloat32_t v1217 = svsub_f32_x(svptrue_b32(), v1183, v1216);
    svfloat32_t v1365 = svnmls_f32_x(pred_full, v1359, v1332, v2439);
    svfloat32_t v1385 = svnmls_f32_x(pred_full, v1379, v1345, v2439);
    svfloat32_t v1521 = svsub_f32_x(svptrue_b32(), v1494, v1520);
    svfloat32_t v1541 = svsub_f32_x(svptrue_b32(), v1507, v1540);
    svfloat32_t v1656 = svcmla_f32_x(pred_full, v1648, v2386, v614, 90);
    svfloat32_t v1669 = svcmla_f32_x(pred_full, v1661, v2388, v741, 90);
    svfloat32_t v1682 = svcmla_f32_x(pred_full, v1674, v2390, v995, 90);
    svfloat32_t v1702 = svcmla_f32_x(pred_full, v1694, v2393, v868, 90);
    svst1_scatter_s64index_f64(pred_full, (double *)(v2150), v2447,
                               svreinterpret_f64_f32(v1094));
    svfloat32_t v1081 = svmls_f32_x(pred_full, v1069, v1063, v2397);
    svfloat32_t v1203 = svnmls_f32_x(pred_full, v1197, v1170, v2439);
    svfloat32_t v1223 = svnmls_f32_x(pred_full, v1217, v1183, v2439);
    svfloat32_t v1386 = svadd_f32_x(svptrue_b32(), v1365, v1385);
    svfloat32_t v1387 = svsub_f32_x(svptrue_b32(), v1365, v1385);
    svfloat32_t v1399 = svmla_f32_x(pred_full, v1359, v1379, v2399);
    svfloat32_t v1417 = svnmls_f32_x(pred_full, v1379, v1359, v2399);
    svfloat32_t v1527 = svnmls_f32_x(pred_full, v1521, v1494, v2439);
    svfloat32_t v1547 = svnmls_f32_x(pred_full, v1541, v1507, v2439);
    svfloat32_t v1683 = svsub_f32_x(svptrue_b32(), v1656, v1682);
    svfloat32_t v1703 = svsub_f32_x(svptrue_b32(), v1669, v1702);
    svfloat32_t v1087 = svnmls_f32_x(pred_full, v1081, v1069, v2439);
    svfloat32_t v1124 = svsub_f32_x(svptrue_b32(), v1081, v1123);
    svfloat32_t v1224 = svadd_f32_x(svptrue_b32(), v1203, v1223);
    svfloat32_t v1225 = svsub_f32_x(svptrue_b32(), v1203, v1223);
    svfloat32_t v1237 = svmla_f32_x(pred_full, v1197, v1217, v2399);
    svfloat32_t v1255 = svnmls_f32_x(pred_full, v1217, v1197, v2399);
    svfloat32_t v1418 = svadd_f32_x(svptrue_b32(), v475, v1386);
    svfloat32_t zero1432 = svdup_n_f32(0);
    svfloat32_t v1432 = svcmla_f32_x(pred_full, zero1432, v2419, v1399, 90);
    svfloat32_t zero1447 = svdup_n_f32(0);
    svfloat32_t v1447 = svcmla_f32_x(pred_full, zero1447, v2419, v1417, 90);
    svfloat32_t v1548 = svadd_f32_x(svptrue_b32(), v1527, v1547);
    svfloat32_t v1549 = svsub_f32_x(svptrue_b32(), v1527, v1547);
    svfloat32_t v1561 = svmla_f32_x(pred_full, v1521, v1541, v2399);
    svfloat32_t v1579 = svnmls_f32_x(pred_full, v1541, v1521, v2399);
    svfloat32_t v1689 = svnmls_f32_x(pred_full, v1683, v1656, v2439);
    svfloat32_t v1709 = svnmls_f32_x(pred_full, v1703, v1669, v2439);
    svfloat32_t v1109 = svsub_f32_x(svptrue_b32(), v1087, v1108);
    svfloat32_t v1137 = svnmls_f32_x(pred_full, v1124, v1081, v2439);
    svfloat32_t v1256 = svadd_f32_x(svptrue_b32(), v467, v1224);
    svfloat32_t zero1270 = svdup_n_f32(0);
    svfloat32_t v1270 = svcmla_f32_x(pred_full, zero1270, v2419, v1237, 90);
    svfloat32_t zero1285 = svdup_n_f32(0);
    svfloat32_t v1285 = svcmla_f32_x(pred_full, zero1285, v2419, v1255, 90);
    svfloat32_t v1393 = svmls_f32_x(pred_full, v475, v1386, v2395);
    svfloat32_t v1580 = svadd_f32_x(svptrue_b32(), v481, v1548);
    svfloat32_t zero1594 = svdup_n_f32(0);
    svfloat32_t v1594 = svcmla_f32_x(pred_full, zero1594, v2419, v1561, 90);
    svfloat32_t zero1609 = svdup_n_f32(0);
    svfloat32_t v1609 = svcmla_f32_x(pred_full, zero1609, v2419, v1579, 90);
    svfloat32_t v1710 = svadd_f32_x(svptrue_b32(), v1689, v1709);
    svfloat32_t v1711 = svsub_f32_x(svptrue_b32(), v1689, v1709);
    svfloat32_t v1723 = svmla_f32_x(pred_full, v1683, v1703, v2399);
    svfloat32_t v1741 = svnmls_f32_x(pred_full, v1703, v1683, v2399);
    svst1_scatter_s64index_f64(pred_full, (double *)(v2170), v2447,
                               svreinterpret_f64_f32(v1124));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2278), v2447,
                               svreinterpret_f64_f32(v1418));
    svfloat32_t v1150 = svnmls_f32_x(pred_full, v1109, v1087, v2439);
    svfloat32_t v1231 = svmls_f32_x(pred_full, v467, v1224, v2395);
    svfloat32_t v1405 = svmls_f32_x(pred_full, v1393, v1387, v2397);
    svfloat32_t v1555 = svmls_f32_x(pred_full, v481, v1548, v2395);
    svfloat32_t v1742 = svadd_f32_x(svptrue_b32(), v487, v1710);
    svfloat32_t zero1756 = svdup_n_f32(0);
    svfloat32_t v1756 = svcmla_f32_x(pred_full, zero1756, v2419, v1723, 90);
    svfloat32_t zero1771 = svdup_n_f32(0);
    svfloat32_t v1771 = svcmla_f32_x(pred_full, zero1771, v2419, v1741, 90);
    svst1_scatter_s64index_f64(pred_full, (double *)(v2160), v2447,
                               svreinterpret_f64_f32(v1109));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2180), v2447,
                               svreinterpret_f64_f32(v1137));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2214), v2447,
                               svreinterpret_f64_f32(v1256));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2342), v2447,
                               svreinterpret_f64_f32(v1580));
    svfloat32_t v1243 = svmls_f32_x(pred_full, v1231, v1225, v2397);
    svfloat32_t v1411 = svnmls_f32_x(pred_full, v1405, v1393, v2439);
    svfloat32_t v1448 = svsub_f32_x(svptrue_b32(), v1405, v1447);
    svfloat32_t v1567 = svmls_f32_x(pred_full, v1555, v1549, v2397);
    svfloat32_t v1717 = svmls_f32_x(pred_full, v487, v1710, v2395);
    svst1_scatter_s64index_f64(pred_full, (double *)(v2190), v2447,
                               svreinterpret_f64_f32(v1150));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2406), v2447,
                               svreinterpret_f64_f32(v1742));
    svfloat32_t v1249 = svnmls_f32_x(pred_full, v1243, v1231, v2439);
    svfloat32_t v1286 = svsub_f32_x(svptrue_b32(), v1243, v1285);
    svfloat32_t v1433 = svsub_f32_x(svptrue_b32(), v1411, v1432);
    svfloat32_t v1461 = svnmls_f32_x(pred_full, v1448, v1405, v2439);
    svfloat32_t v1573 = svnmls_f32_x(pred_full, v1567, v1555, v2439);
    svfloat32_t v1610 = svsub_f32_x(svptrue_b32(), v1567, v1609);
    svfloat32_t v1729 = svmls_f32_x(pred_full, v1717, v1711, v2397);
    svst1_scatter_s64index_f64(pred_full, (double *)(v2298), v2447,
                               svreinterpret_f64_f32(v1448));
    svfloat32_t v1271 = svsub_f32_x(svptrue_b32(), v1249, v1270);
    svfloat32_t v1299 = svnmls_f32_x(pred_full, v1286, v1243, v2439);
    svfloat32_t v1474 = svnmls_f32_x(pred_full, v1433, v1411, v2439);
    svfloat32_t v1595 = svsub_f32_x(svptrue_b32(), v1573, v1594);
    svfloat32_t v1623 = svnmls_f32_x(pred_full, v1610, v1567, v2439);
    svfloat32_t v1735 = svnmls_f32_x(pred_full, v1729, v1717, v2439);
    svfloat32_t v1772 = svsub_f32_x(svptrue_b32(), v1729, v1771);
    svst1_scatter_s64index_f64(pred_full, (double *)(v2234), v2447,
                               svreinterpret_f64_f32(v1286));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2288), v2447,
                               svreinterpret_f64_f32(v1433));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2308), v2447,
                               svreinterpret_f64_f32(v1461));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2362), v2447,
                               svreinterpret_f64_f32(v1610));
    svfloat32_t v1312 = svnmls_f32_x(pred_full, v1271, v1249, v2439);
    svfloat32_t v1636 = svnmls_f32_x(pred_full, v1595, v1573, v2439);
    svfloat32_t v1757 = svsub_f32_x(svptrue_b32(), v1735, v1756);
    svfloat32_t v1785 = svnmls_f32_x(pred_full, v1772, v1729, v2439);
    svst1_scatter_s64index_f64(pred_full, (double *)(v2224), v2447,
                               svreinterpret_f64_f32(v1271));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2244), v2447,
                               svreinterpret_f64_f32(v1299));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2318), v2447,
                               svreinterpret_f64_f32(v1474));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2352), v2447,
                               svreinterpret_f64_f32(v1595));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2372), v2447,
                               svreinterpret_f64_f32(v1623));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2426), v2447,
                               svreinterpret_f64_f32(v1772));
    svfloat32_t v1798 = svnmls_f32_x(pred_full, v1757, v1735, v2439);
    svst1_scatter_s64index_f64(pred_full, (double *)(v2254), v2447,
                               svreinterpret_f64_f32(v1312));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2382), v2447,
                               svreinterpret_f64_f32(v1636));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2416), v2447,
                               svreinterpret_f64_f32(v1757));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2436), v2447,
                               svreinterpret_f64_f32(v1785));
    svst1_scatter_s64index_f64(pred_full, (double *)(v2446), v2447,
                               svreinterpret_f64_f32(v1798));
    v5 += v11;
    v6 += v12;
  }
}
#endif
