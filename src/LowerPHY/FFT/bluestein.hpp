/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2024-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#pragma once

#include "fft_plan.hpp"

#include "optional"

namespace armral::fft {

/// Class to support using Bluestein's algorithm for prime n.
template<typename Tx, typename Ty, typename Tw>
struct bluestein {
  int n;
  int n_pad;
  armral_fft_direction_t dir;

  armral_fft_plan_t *pf;
  armral_fft_plan_t *pb;

  const Tw *a;
  const Tw *b;

  bluestein()
    : n(0), n_pad(0), dir((armral_fft_direction_t)0), pf(nullptr), pb(nullptr),
      a(nullptr), b(nullptr) {}

  bluestein(const bluestein &) = delete;

  bluestein(bluestein &&other)
    : n(other.n), n_pad(other.n_pad), dir(other.dir), pf(other.pf),
      pb(other.pb), a(other.a), b(other.b) {
    other.a = nullptr;
    other.b = nullptr;
    other.pf = nullptr;
    other.pb = nullptr;
  }

  bluestein &operator=(const bluestein &) = delete;
  bluestein &operator=(bluestein &&) = delete;

  bluestein(int n_in, int n_pad_in, armral_fft_direction_t dir_in,
            armral_fft_plan_t *pf_in, armral_fft_plan_t *pb_in, Tw *a_in,
            Tw *b_in)
    : n(n_in), n_pad(n_pad_in), dir(dir_in), pf(pf_in), pb(pb_in), a(a_in),
      b(b_in) {}

  ~bluestein();

  operator bool() const {
    return n != 0;
  }
};

template<typename Tx, typename Ty, typename Tw>
std::optional<bluestein<Tx, Ty, Tw>>
make_bluestein(int n, armral_fft_direction_t dir, const int *base_kernels,
               int len_base_kernels);

template<typename Tx, typename Ty, typename Tw>
void execute_bluestein(const bluestein<Tx, Ty, Tw> &bs, const Tx *x, Ty *y,
                       int istride, int ostride, const Tw *w, int howmany,
                       int idist, int odist);

} // namespace armral::fft
