/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

template<unsigned int N>
static inline void generate_seq_128(uint64_t *x) {
  static_assert(N == 2);

  poly64_t pmask[3] = {0x303, 0x1111, 0xffff000};
  uint64x2_t mask_low_20 = vdupq_n_u64(0xfffff);
  uint64x2_t mask_high_16 = vdupq_n_u64(0xffff000000000000);

  uint64x2_t low_20 =
      vshrq_n_u64(vreinterpretq_u64_p128(vmull_p64(*x, pmask[0])), 44);
  uint64x2_t mid_28 =
      vshrq_n_u64(vreinterpretq_u64_p128(vmull_p64(*x, pmask[1])), 16);
  uint64x2_t high_16 = vreinterpretq_u64_p128(vmull_p64(*x, pmask[2]));
  *x = vbslq_u64(mask_high_16, high_16,
                 vbslq_u64(mask_low_20, low_20, mid_28))[0];
}

template<unsigned int N>
static inline void generate_seq_64(uint64_t *x) {
  static_assert((N == 1) || (N == 2));

  poly64_t pmask[3];
  if (N == 1) {
    pmask[0] = 0x9;
    pmask[1] = 0x41;
    pmask[2] = 0x24900000;
  } else {
    pmask[0] = 0xf;
    pmask[1] = 0x55;
    pmask[2] = 0x30300000;
  }
  uint64x2_t mask_low_28 = vdupq_n_u64(0xfffffff);
  uint64x2_t mask_high_8 = vdupq_n_u64(0xff00000000000000);

  uint64x2_t low_28 =
      vshrq_n_u64(vreinterpretq_u64_p128(vmull_p64(*x, pmask[0])), 36);
  uint64x2_t mid_28 =
      vshrq_n_u64(vreinterpretq_u64_p128(vmull_p64(*x, pmask[1])), 8);
  uint64x2_t high_8 = vreinterpretq_u64_p128(vmull_p64(*x, pmask[2]));
  *x =
      vbslq_u64(mask_high_8, high_8, vbslq_u64(mask_low_28, low_28, mid_28))[0];
}

armral_status armral_seq_generator(uint32_t sequence_len, uint32_t seed,
                                   uint8_t *p_dst) {

  uint64_t *p_out = (uint64_t *)p_dst;

  // Set the required masks.
  uint64x2_t mask_28 = vdupq_n_u64(0x7ffffff80000000);
  uint64x2_t mask_5 = vdupq_n_u64(0xf800000000000000);

  // Set the first 64 bits x2.
  uint64_t cinit = seed & 0x7fffffff;
  uint64_t x2 = cinit;
  x2 |=
      vandq_u64(vreinterpretq_u64_p128(vmull_p64(x2, 0xf0000000)), mask_28)[0];
  x2 |= vandq_u64(vreinterpretq_u64_p128(vmull_p64(x2, 0xf0000000)), mask_5)[0];

  // The sequence x1 is determined according to x1(n+31) = x1(n+3) ^ x1(n)
  // The initial conditions for x1 are x1(0) = 1, x1(n) = 0, n = 1, 2, ..., 30.
  // We don't need to calculate the first 1664 bits of x1 (like we do for x2) as
  // they don't depend on the seed so are always the same. x1 contains bits 1600
  // to 1663 of the sequence.
  uint64_t x1 = 0x6ac0a9a45e485840;

  // Generate x2 processing 128 bits at a time. After the loop, x2 will contain
  // bits 1536 to 1599.
  //    x2(n+31) = x2(n) + x2(n+1) + x2(n+2) + x2(n+3)
  for (uint32_t n = 0; n < 12; n++) {
    generate_seq_128<2>(&x2);
  }
  // Generate bits 1600 to 1663 for x2
  generate_seq_64<2>(&x2);

  uint32_t length = sequence_len / 64;
  // Generate c(n) = (x1(n+Nc) + x2(n+Nc)), Nc = 1600
  for (uint32_t n = 0; n < length; n++) {
    *p_out = x1 ^ x2;
    p_out++;

    // Next 64 bits of x1 and x2.
    generate_seq_64<1>(&x1);
    generate_seq_64<2>(&x2);
  }

  // Tail
  if ((sequence_len % 64) != 0) {
    uint8_t tail_length = ((sequence_len % 64) + 7) >> 3;
    uint64_t ptemp_res = x1 ^ x2;
#ifdef ARMRAL_ARCH_SVE
    svbool_t pg = svwhilelt_b8(0, tail_length);
    svuint64_t splat_val = svdup_u64(ptemp_res);
    svuint8_t splat_val8 = svreinterpret_u8_u64(splat_val);
    svst1_u8(pg, (uint8_t *)p_out, splat_val8);
#else
    uint8_t *p_out_tail = (uint8_t *)p_out;
    for (uint32_t i = 0; i < tail_length; i++) {
      (*p_out_tail) = (uint8_t)(ptemp_res >> (i * 8));
      p_out_tail++;
    }
#endif
  }

  return ARMRAL_SUCCESS;
}
