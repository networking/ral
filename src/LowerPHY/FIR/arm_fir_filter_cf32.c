/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

#ifdef ARMRAL_ARCH_SVE

static inline svfloat32x4_t fir_sve_blk_4(svbool_t pg, const float32_t *in,
                                          const float32_t *coeffs,
                                          uint32_t n_taps) {
  // Compute FIR for four vector-lengths of data. Coeffs array is
  // unrolled by 2 and we have 2 accumulators per vector length, as
  // explained in fir_sve_blk_2. In addition, loads and mlas are
  // hand-interleaved in order to minimize latency.

  svfloat32_t y1_1 = svdup_f32(0);
  svfloat32_t y2_1 = svdup_f32(0);
  svfloat32_t y3_1 = svdup_f32(0);
  svfloat32_t y4_1 = svdup_f32(0);
  svfloat32_t y1_2 = svdup_f32(0);
  svfloat32_t y2_2 = svdup_f32(0);
  svfloat32_t y3_2 = svdup_f32(0);
  svfloat32_t y4_2 = svdup_f32(0);

  for (uint32_t t = 0; t + 2 <= n_taps; t += 2) {

    svfloat32_t c = svld1rq_f32(pg, coeffs + t * 2);
    svfloat32_t x1_1 = svld1_f32(pg, in + t * 2);

    asm volatile("");

    svfloat32_t x1_2 = svld1_vnum_f32(pg, in + t * 2, 1);
    svfloat32_t x1_3 = svld1_vnum_f32(pg, in + t * 2, 2);

    asm volatile("");

    svfloat32_t x1_4 = svld1_vnum_f32(pg, in + t * 2, 3);
    svfloat32_t x2_1 = svld1_f32(pg, in + (t + 1) * 2);

    asm volatile("");

    svfloat32_t x2_2 = svld1_vnum_f32(pg, in + (t + 1) * 2, 1);
    y1_1 = svcmla_lane_f32(y1_1, x1_1, c, 0, 0);
    y1_2 = svcmla_lane_f32(y1_2, x1_1, c, 0, 90);

    asm volatile("");

    svfloat32_t x2_3 = svld1_vnum_f32(pg, in + (t + 1) * 2, 2);
    y2_1 = svcmla_lane_f32(y2_1, x1_2, c, 0, 0);
    y2_2 = svcmla_lane_f32(y2_2, x1_2, c, 0, 90);

    asm volatile("");

    svfloat32_t x2_4 = svld1_vnum_f32(pg, in + (t + 1) * 2, 3);
    y3_1 = svcmla_lane_f32(y3_1, x1_3, c, 0, 0);
    y3_2 = svcmla_lane_f32(y3_2, x1_3, c, 0, 90);

    asm volatile("");

    y4_1 = svcmla_lane_f32(y4_1, x1_4, c, 0, 0);
    y4_2 = svcmla_lane_f32(y4_2, x1_4, c, 0, 90);

    asm volatile("");

    y1_1 = svcmla_lane_f32(y1_1, x2_1, c, 1, 0);
    y1_2 = svcmla_lane_f32(y1_2, x2_1, c, 1, 90);

    asm volatile("");

    y2_1 = svcmla_lane_f32(y2_1, x2_2, c, 1, 0);
    y2_2 = svcmla_lane_f32(y2_2, x2_2, c, 1, 90);

    asm volatile("");

    y3_1 = svcmla_lane_f32(y3_1, x2_3, c, 1, 0);
    y3_2 = svcmla_lane_f32(y3_2, x2_3, c, 1, 90);

    asm volatile("");

    y4_1 = svcmla_lane_f32(y4_1, x2_4, c, 1, 0);
    y4_2 = svcmla_lane_f32(y4_2, x2_4, c, 1, 90);
  }

  svfloat32_t y1 = svadd_x(pg, y1_1, y1_2);
  svfloat32_t y2 = svadd_x(pg, y2_1, y2_2);
  svfloat32_t y3 = svadd_x(pg, y3_1, y3_2);
  svfloat32_t y4 = svadd_x(pg, y4_1, y4_2);

  if (n_taps % 2) {
    svfloat32_t c = svreinterpret_f32_u64(
        svdup_u64(((const uint64_t *)coeffs)[n_taps - 1]));
    svfloat32_t x1 = svld1_f32(pg, in + (n_taps - 1) * 2);
    svfloat32_t x2 = svld1_vnum_f32(pg, in + (n_taps - 1) * 2, 1);
    svfloat32_t x3 = svld1_vnum_f32(pg, in + (n_taps - 1) * 2, 2);
    svfloat32_t x4 = svld1_vnum_f32(pg, in + (n_taps - 1) * 2, 3);
    y1 = svcmla_f32_x(pg, y1, c, x1, 0);
    y1 = svcmla_f32_x(pg, y1, c, x1, 90);
    y2 = svcmla_f32_x(pg, y2, c, x2, 0);
    y2 = svcmla_f32_x(pg, y2, c, x2, 90);
    y3 = svcmla_f32_x(pg, y3, c, x3, 0);
    y3 = svcmla_f32_x(pg, y3, c, x3, 90);
    y4 = svcmla_f32_x(pg, y4, c, x4, 0);
    y4 = svcmla_f32_x(pg, y4, c, x4, 90);
  }
  return svcreate4(y1, y2, y3, y4);
}

static inline svfloat32x2_t fir_sve_blk_2(svbool_t pg, const float32_t *in,
                                          const float32_t *coeffs,
                                          uint32_t n_taps) {
  // Compute FIR for 2 vector-lengths of data. Lightly optimized - this
  // function will be called at most once per call of
  // arm_fir_filter_cf32. Coefficient array is unrolled by factor 2, as
  // for fir_sve_blk, with the difference that we have two accumulators
  // for each vector-length.

  // For each complex multiply-accumulate, one accumulator stores the result of
  // multiplication rotated by 0, and the other the result of multiplication
  // rotated by 90 degrees. This means that the dependency chains are relaxed,
  // and we don't have to wait for one cmla operation to finish before the next
  // one can start. The final result is calculated at the end by adding the
  // pairs of accumulators together.

  svfloat32_t y1_1 = svdup_f32(0);
  svfloat32_t y2_1 = svdup_f32(0);
  svfloat32_t y1_2 = svdup_f32(0);
  svfloat32_t y2_2 = svdup_f32(0);

  for (uint32_t t = 0; t < n_taps - 1; t += 2) {
    svfloat32_t c = svld1rq_f32(pg, coeffs + t * 2);

    svfloat32_t x1_1 = svld1_f32(pg, in + t * 2);
    svfloat32_t x1_2 = svld1_vnum_f32(pg, in + t * 2, 1);

    y1_1 = svcmla_lane_f32(y1_1, x1_1, c, 0, 0);
    y2_1 = svcmla_lane_f32(y2_1, x1_2, c, 0, 0);
    y1_2 = svcmla_lane_f32(y1_2, x1_1, c, 0, 90);
    y2_2 = svcmla_lane_f32(y2_2, x1_2, c, 0, 90);

    svfloat32_t x2_1 = svld1_f32(pg, in + (t + 1) * 2);
    svfloat32_t x2_2 = svld1_vnum_f32(pg, in + (t + 1) * 2, 1);

    y1_1 = svcmla_lane_f32(y1_1, x2_1, c, 1, 0);
    y2_1 = svcmla_lane_f32(y2_1, x2_2, c, 1, 0);
    y1_2 = svcmla_lane_f32(y1_2, x2_1, c, 1, 90);
    y2_2 = svcmla_lane_f32(y2_2, x2_2, c, 1, 90);
  }

  if (n_taps % 2) {
    svfloat32_t c = svreinterpret_f32_u64(
        svdup_u64(((const uint64_t *)coeffs)[n_taps - 1]));
    svfloat32_t x1 = svld1_f32(pg, in + (n_taps - 1) * 2);
    svfloat32_t x2 = svld1_vnum_f32(pg, in + (n_taps - 1) * 2, 1);
    y1_1 = svcmla_f32_x(pg, y1_1, c, x1, 0);
    y2_1 = svcmla_f32_x(pg, y2_1, c, x2, 0);
    y1_2 = svcmla_f32_x(pg, y1_2, c, x1, 90);
    y2_2 = svcmla_f32_x(pg, y2_2, c, x2, 90);
  }

  svfloat32_t y1 = svadd_x(pg, y1_1, y1_2);
  svfloat32_t y2 = svadd_x(pg, y2_1, y2_2);
  return svcreate2(y1, y2);
}

static inline svfloat32_t fir_sve_blk(svbool_t pg, const float32_t *in,
                                      const float32_t *coeffs,
                                      uint32_t n_taps) {
  // Compute FIR for one vector-length of data. This version is not
  // really optimized, as it is only ever used as the tail of the more
  // heavily unrolled versions above. The loop over the coeffs array is
  // unrolled by factor 2, since we can fit 2 complex values in a
  // quad-word.
  svfloat32_t y = svdup_f32(0);
  uint32_t t = 0;

  for (; t < n_taps - 1; t += 2) {
    svfloat32_t c = svld1rq_f32(pg, coeffs + t * 2);

    svfloat32_t x1 = svld1_f32(pg, in + t * 2);
    y = svcmla_lane_f32(y, x1, c, 0, 0);  // accumulate real(x1) * c1
    y = svcmla_lane_f32(y, x1, c, 0, 90); // accumulate imag(x1) * rot90(c1)

    svfloat32_t x2 = svld1_f32(pg, in + (t + 1) * 2);
    y = svcmla_lane_f32(y, x2, c, 1, 0);  // accumulate real(x2) * c2
    y = svcmla_lane_f32(y, x2, c, 1, 90); // accumulate imag(x2) * rot90(c2)
  }

  if (n_taps % 2) {
    svfloat32_t c =
        svreinterpret_f32_u64(svdup_u64(((const uint64_t *)coeffs)[t]));
    svfloat32_t x = svld1_f32(pg, in + t * 2);
    y = svcmla_f32_x(pg, y, c, x, 0);  // accumulate real(x) * c
    y = svcmla_f32_x(pg, y, c, x, 90); // accumulate imag(x) * c
  }

  return y;
}

#endif // ARMRAL_ARCH_SVE

armral_status armral_fir_filter_cf32(uint32_t size, uint32_t taps,
                                     const armral_cmplx_f32_t *restrict input,
                                     const armral_cmplx_f32_t *restrict coeffs,
                                     armral_cmplx_f32_t *output) {
#ifdef ARMRAL_ARCH_SVE
  svbool_t ptrue_b32 = svptrue_b32();
  uint32_t x_blk_idx = 0;
  uint32_t xinc = svcntw() * 2;
  const float32_t *c = (const float32_t *)coeffs;
  const float32_t *in = (const float32_t *)input;
  float32_t *out = (float32_t *)output;

  for (; x_blk_idx + xinc * 2 < size * 2; x_blk_idx += xinc * 2) {
    svfloat32x4_t y = fir_sve_blk_4(ptrue_b32, in + x_blk_idx, c, taps);
    svst1_f32(ptrue_b32, out + x_blk_idx, svget4(y, 0));
    svst1_vnum_f32(ptrue_b32, out + x_blk_idx, 1, svget4(y, 1));
    svst1_vnum_f32(ptrue_b32, out + x_blk_idx, 2, svget4(y, 2));
    svst1_vnum_f32(ptrue_b32, out + x_blk_idx, 3, svget4(y, 3));
  }
  for (; x_blk_idx + xinc < size * 2; x_blk_idx += xinc) {
    svfloat32x2_t y = fir_sve_blk_2(ptrue_b32, in + x_blk_idx, c, taps);
    svst1_f32(ptrue_b32, out + x_blk_idx, svget2(y, 0));
    svst1_vnum_f32(ptrue_b32, out + x_blk_idx, 1, svget2(y, 1));
  }
  if (x_blk_idx + svcntw() < size * 2) {
    svst1_f32(ptrue_b32, out + x_blk_idx,
              fir_sve_blk(ptrue_b32, in + x_blk_idx, c, taps));
    x_blk_idx += svcntw();
  }
  if (x_blk_idx < size * 2) {
    svbool_t pg = svwhilelt_b32(x_blk_idx, size * 2);
    svst1_f32(pg, out + x_blk_idx, fir_sve_blk(pg, in + x_blk_idx, c, taps));
  }
#else
  /* Compute number of blocks of 4 input data and a possible tail */
  uint32_t blck_data = size >> 2U;

  /* Compute number of blocks of 4 taps and a possible tail */
  uint32_t total_taps = taps >> 2U;
  uint32_t tailtaps = taps & 0x3;
  uint32_t blck_taps;

  float32x4x2_t s;
  float32x4x2_t h;
  float32x4x2_t old_s;
  float32x4x2_t shifted_s;
  const float32_t *fir_p_input;
  const float32_t *p_input = (const float32_t *)input;
  float32_t *p_output = (float32_t *)output;
  const float32_t *p_coeff;

  float32x4_t zero_32x4;
  float32x4x2_t accum;
  zero_32x4 = vdupq_n_f32(0.0);

  uint32x4_t fir_mask[4];

  fir_mask[0][0] = 0x00000000;
  fir_mask[0][1] = 0x00000000;
  fir_mask[0][2] = 0x00000000;
  fir_mask[0][3] = 0x00000000;
  fir_mask[1][0] = 0xFFFFFFFF;
  fir_mask[1][1] = 0x00000000;
  fir_mask[1][2] = 0x00000000;
  fir_mask[1][3] = 0x00000000;
  fir_mask[2][0] = 0xFFFFFFFF;
  fir_mask[2][1] = 0xFFFFFFFF;
  fir_mask[2][2] = 0x00000000;
  fir_mask[2][3] = 0x00000000;
  fir_mask[3][0] = 0xFFFFFFFF;
  fir_mask[3][1] = 0xFFFFFFFF;
  fir_mask[3][2] = 0xFFFFFFFF;
  fir_mask[3][3] = 0x00000000;

  /* Input data loop unrolling */
  while (blck_data > 0U) {
    /* At every iteration of the outer loop the pointer of the coefficient must
     * be reset */
    p_coeff = (const float32_t *)coeffs;
    /* Reset the accumulators */
    accum.val[0] = vdupq_n_f32(0.0);
    accum.val[1] = vdupq_n_f32(0.0);

    /* Load data and FIR coefficients */
    s = vld2q_f32(p_input);
    p_input += 8; /* 4 samples, Re + Im */
    fir_p_input = p_input;

    blck_taps = total_taps;
    while (blck_taps > 0U) {
      h = vld2q_f32(p_coeff);
      p_coeff += 8; /* 4 coefficients, Re + Im */

      float32x4_t h_re = h.val[0];
      float32x4_t h_im = h.val[1];

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], s.val[0], h_re, 0);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], s.val[1], h_im, 0);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[1], h_re, 0);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[0], h_im, 0);

      /* Save the current samples and load new ones */
      old_s = s;
      s = vld2q_f32(fir_p_input);
      fir_p_input += 8; /* 4 samples, Re + Im */

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 1);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 1);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 1);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 1);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 1);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 1);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 2);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 2);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 2);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 2);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 2);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 3);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 3);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 3);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 3);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 3);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 3);

      blck_taps--;
    }

    /* FIR tail management */
    if (tailtaps) {
      /* Load the last tailTaps coefficient */
      h = vld2q_f32(p_coeff);
      h.val[0] = vbslq_f32(fir_mask[tailtaps], h.val[0], zero_32x4);
      h.val[1] = vbslq_f32(fir_mask[tailtaps], h.val[1], zero_32x4);

      float32x4_t h_re = h.val[0];
      float32x4_t h_im = h.val[1];

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], s.val[0], h_re, 0);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], s.val[1], h_im, 0);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[1], h_re, 0);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[0], h_im, 0);

      /* Save the current samples and load new ones */
      old_s = s;
      s = vld2q_f32(fir_p_input); /* 4 samples, Re + Im */

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 1);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 1);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 1);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 1);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 1);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 1);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 2);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 2);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 2);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 2);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 2);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 3);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 3);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 3);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 3);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 3);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 3);
    }

    /* Store the results */
    vst2q_f32(p_output, accum);
    p_output += 8;

    blck_data--;
  }
#endif
  return ARMRAL_SUCCESS;
}
