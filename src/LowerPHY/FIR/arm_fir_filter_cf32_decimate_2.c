/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#ifdef ARMRAL_ARCH_SVE

#include <arm_sve.h>

static inline svfloat32_t svld1_vnum_evens(svbool_t pg, const uint64_t *addr,
                                           uint64_t vnum) {
  svuint64x2_t ld2_res = svld2_vnum_u64(pg, addr, vnum);
  return svreinterpret_f32_u64(svget2(ld2_res, 0));
}

static inline svfloat32_t sv_full_cmla(svbool_t pg, svfloat32_t acc,
                                       svfloat32_t x, svfloat32_t y) {
  acc = svcmla_f32_x(pg, acc, x, y, 0);
  acc = svcmla_f32_x(pg, acc, x, y, 90);
  return acc;
}

static inline void sv_fir_block(svbool_t pg,
                                const armral_cmplx_f32_t *restrict input,
                                const armral_cmplx_f32_t *restrict coeffs,
                                armral_cmplx_f32_t *out, uint32_t taps) {
  // Compute FIR for one vector-length of data (read 2 vector lengths, write 1)
  const uint64_t *in = (const uint64_t *)input;
  svfloat32_t y = svdup_f32(0);

  uint32_t i = 0;
  for (; i + 2 <= taps; i += 2) {
    svfloat32_t c = svld1rq_f32(pg, (const float32_t *)&coeffs[i]);

    // ld2_u64 allows us to separate even and odd complex elements, for example
    // if i == 0 then:
    // x1 = [r0, i0, r2, i2, r4, i4, ...]
    // x2 = [r1, i1, r3, i3, r5, i5, ...]
    // Elements of x1 correspond to the first complex coeff in c, elements of x2
    // correspond to the second

    svuint64x2_t x = svld2_u64(pg, &in[i]);
    svfloat32_t x1 = svreinterpret_f32_u64(svget2(x, 0));
    svfloat32_t x2 = svreinterpret_f32_u64(svget2(x, 1));

    y = svcmla_lane_f32(y, x1, c, 0, 0);
    y = svcmla_lane_f32(y, x1, c, 0, 90);
    y = svcmla_lane_f32(y, x2, c, 1, 0);
    y = svcmla_lane_f32(y, x2, c, 1, 90);
  }

  if (taps % 2) {
    svfloat32_t c =
        svreinterpret_f32_u64(svdup_u64(((const uint64_t *)coeffs)[i]));
    svfloat32_t x = svld1_vnum_evens(pg, &in[i], 0);
    y = sv_full_cmla(pg, y, c, x);
  }

  svst1_f32(pg, (float32_t *)out, y);
}

static inline void sv_fir_block_2(svbool_t pg,
                                  const armral_cmplx_f32_t *restrict input,
                                  const armral_cmplx_f32_t *restrict coeffs,
                                  armral_cmplx_f32_t *out, uint32_t taps) {
  // Compute FIR for 2 vector-lengths of data (read 4 vector-lengths, write 2)

  // For each complex multiply-accumulate, one accumulator stores the result of
  // multiplication rotated by 0, and the other the result of multiplication
  // rotated by 90 degrees. This means that the dependency chains are relaxed,
  // and we don't have to wait for one cmla operation to finish before the next
  // one can start. The final result is calculated at the end by adding the
  // pairs of accumulators together.

  // The loop over the coeffs array is again unrolled by two, for an explanation
  // of the use of ld2_u64 see sv_fir_block
  const uint64_t *in = (const uint64_t *)input;

  svfloat32_t y_0_0 = svdup_f32(0);
  svfloat32_t y_1_0 = svdup_f32(0);
  svfloat32_t y_0_1 = svdup_f32(0);
  svfloat32_t y_1_1 = svdup_f32(0);

  uint32_t i = 0;
  for (; i + 2 <= taps; i += 2) {
    svfloat32_t c = svld1rq_f32(pg, (const float32_t *)&coeffs[i]);

    svuint64x2_t x0 = svld2_u64(pg, &in[i]);
    svuint64x2_t x1 = svld2_vnum_u64(pg, &in[i], 2);
    svfloat32_t x0_0 = svreinterpret_f32_u64(svget2(x0, 0));
    svfloat32_t x1_0 = svreinterpret_f32_u64(svget2(x0, 1));
    svfloat32_t x0_1 = svreinterpret_f32_u64(svget2(x1, 0));
    svfloat32_t x1_1 = svreinterpret_f32_u64(svget2(x1, 1));

    y_0_0 = svcmla_lane_f32(y_0_0, x0_0, c, 0, 0);
    y_0_1 = svcmla_lane_f32(y_0_1, x0_0, c, 0, 90);
    y_0_0 = svcmla_lane_f32(y_0_0, x1_0, c, 1, 0);
    y_0_1 = svcmla_lane_f32(y_0_1, x1_0, c, 1, 90);

    y_1_0 = svcmla_lane_f32(y_1_0, x0_1, c, 0, 0);
    y_1_1 = svcmla_lane_f32(y_1_1, x0_1, c, 0, 90);
    y_1_0 = svcmla_lane_f32(y_1_0, x1_1, c, 1, 0);
    y_1_1 = svcmla_lane_f32(y_1_1, x1_1, c, 1, 90);
  }

  svfloat32_t y_0 = svadd_x(pg, y_0_0, y_0_1);
  svfloat32_t y_1 = svadd_x(pg, y_1_0, y_1_1);

  if (taps % 2) {
    svfloat32_t c =
        svreinterpret_f32_u64(svdup_u64(((const uint64_t *)coeffs)[i]));
    svfloat32_t x_0 = svld1_vnum_evens(pg, &in[i], 0);
    y_0 = sv_full_cmla(pg, y_0, c, x_0);
    svfloat32_t x_1 = svld1_vnum_evens(pg, &in[i], 2);
    y_1 = sv_full_cmla(pg, y_1, c, x_1);
  }

  svst1_f32(pg, (float32_t *)out, y_0);
  svst1_vnum_f32(pg, (float32_t *)out, 1, y_1);
}

static inline void sv_fir_block_4(svbool_t pg,
                                  const armral_cmplx_f32_t *restrict input,
                                  const armral_cmplx_f32_t *restrict coeffs,
                                  armral_cmplx_f32_t *out, uint32_t taps) {
  // Compute FIR for 4 vector-lengths of data (read 8 vector-lengths, write 4).
  // Coeffs array is unrolled by 2 and we have 2 accumulators per vector length,
  // as explained in sv_fir_block_2. In addition, loads and mlas are
  // hand-interleaved in order to minimize latency.

  const uint64_t *in = (const uint64_t *)input;
  svfloat32_t y_0_0 = svdup_f32(0);
  svfloat32_t y_0_1 = svdup_f32(0);
  svfloat32_t y_1_0 = svdup_f32(0);
  svfloat32_t y_1_1 = svdup_f32(0);
  svfloat32_t y_2_0 = svdup_f32(0);
  svfloat32_t y_2_1 = svdup_f32(0);
  svfloat32_t y_3_0 = svdup_f32(0);
  svfloat32_t y_3_1 = svdup_f32(0);

  uint32_t i = 0;
  for (; i + 2 <= taps; i += 2) {

    svfloat32_t c = svld1rq_f32(pg, (const float32_t *)&coeffs[i]);
    svuint64x2_t x0 = svld2_u64(pg, &in[i]);
    svfloat32_t x0_0 = svreinterpret_f32_u64(svget2(x0, 0));
    svfloat32_t x1_0 = svreinterpret_f32_u64(svget2(x0, 1));

    asm volatile("");

    svuint64x2_t x1 = svld2_vnum_u64(pg, &in[i], 2);
    svuint64x2_t x2 = svld2_vnum_u64(pg, &in[i], 4);
    svfloat32_t x0_1 = svreinterpret_f32_u64(svget2(x1, 0));
    svfloat32_t x1_1 = svreinterpret_f32_u64(svget2(x1, 1));
    svfloat32_t x0_2 = svreinterpret_f32_u64(svget2(x2, 0));
    svfloat32_t x1_2 = svreinterpret_f32_u64(svget2(x2, 1));

    asm volatile("");

    svuint64x2_t x3 = svld2_vnum_u64(pg, &in[i], 6);
    svfloat32_t x0_3 = svreinterpret_f32_u64(svget2(x3, 0));
    svfloat32_t x1_3 = svreinterpret_f32_u64(svget2(x3, 1));

    asm volatile("");

    y_0_0 = svcmla_lane_f32(y_0_0, x0_0, c, 0, 0);
    y_0_1 = svcmla_lane_f32(y_0_1, x0_0, c, 0, 90);

    asm volatile("");

    y_1_0 = svcmla_lane_f32(y_1_0, x0_1, c, 0, 0);
    y_1_1 = svcmla_lane_f32(y_1_1, x0_1, c, 0, 90);

    asm volatile("");

    y_2_0 = svcmla_lane_f32(y_2_0, x0_2, c, 0, 0);
    y_2_1 = svcmla_lane_f32(y_2_1, x0_2, c, 0, 90);

    asm volatile("");

    y_3_0 = svcmla_lane_f32(y_3_0, x0_3, c, 0, 0);
    y_3_1 = svcmla_lane_f32(y_3_1, x0_3, c, 0, 90);

    asm volatile("");

    y_0_0 = svcmla_lane_f32(y_0_0, x1_0, c, 1, 0);
    y_0_1 = svcmla_lane_f32(y_0_1, x1_0, c, 1, 90);

    asm volatile("");

    y_1_0 = svcmla_lane_f32(y_1_0, x1_1, c, 1, 0);
    y_1_1 = svcmla_lane_f32(y_1_1, x1_1, c, 1, 90);

    asm volatile("");

    y_2_0 = svcmla_lane_f32(y_2_0, x1_2, c, 1, 0);
    y_2_1 = svcmla_lane_f32(y_2_1, x1_2, c, 1, 90);

    asm volatile("");

    y_3_0 = svcmla_lane_f32(y_3_0, x1_3, c, 1, 0);
    y_3_1 = svcmla_lane_f32(y_3_1, x1_3, c, 1, 90);
  }

  svfloat32_t y_0 = svadd_x(pg, y_0_0, y_0_1);
  svfloat32_t y_1 = svadd_x(pg, y_1_0, y_1_1);
  svfloat32_t y_2 = svadd_x(pg, y_2_0, y_2_1);
  svfloat32_t y_3 = svadd_x(pg, y_3_0, y_3_1);

  if (taps % 2) {
    svfloat32_t c =
        svreinterpret_f32_u64(svdup_u64(((const uint64_t *)coeffs)[i]));
    svfloat32_t x_0 = svld1_vnum_evens(pg, &in[i], 0);
    y_0 = sv_full_cmla(pg, y_0, c, x_0);
    svfloat32_t x_1 = svld1_vnum_evens(pg, &in[i], 2);
    y_1 = sv_full_cmla(pg, y_1, c, x_1);
    svfloat32_t x_2 = svld1_vnum_evens(pg, &in[i], 4);
    y_2 = sv_full_cmla(pg, y_2, c, x_2);
    svfloat32_t x_3 = svld1_vnum_evens(pg, &in[i], 6);
    y_3 = sv_full_cmla(pg, y_3, c, x_3);
  }

  svst1_f32(pg, (float32_t *)out, y_0);
  svst1_vnum_f32(pg, (float32_t *)out, 1, y_1);
  svst1_vnum_f32(pg, (float32_t *)out, 2, y_2);
  svst1_vnum_f32(pg, (float32_t *)out, 3, y_3);
}

static inline void sv_fir_block_8(svbool_t pg,
                                  const armral_cmplx_f32_t *restrict input,
                                  const armral_cmplx_f32_t *restrict coeffs,
                                  armral_cmplx_f32_t *out, uint32_t taps) {
  // Compute FIR for 8 vector-lengths of data (read 8 vector-lengths, write 16).
  // Unlike the previous 2 versions, we only need 1 accumulator per vector
  // length, as we have enough accumulators to hide the latency of ld2 and cmla
  // without needing to split them in half. Again, loads and mlas are
  // hand-interleaved in order to minimize latency.
  const uint64_t *in = (const uint64_t *)input;
  svfloat32_t y_0 = svdup_f32(0);
  svfloat32_t y_1 = svdup_f32(0);
  svfloat32_t y_2 = svdup_f32(0);
  svfloat32_t y_3 = svdup_f32(0);
  svfloat32_t y_4 = svdup_f32(0);
  svfloat32_t y_5 = svdup_f32(0);
  svfloat32_t y_6 = svdup_f32(0);
  svfloat32_t y_7 = svdup_f32(0);

  uint32_t i = 0;
  for (; i + 2 <= taps; i += 2) {
    svfloat32_t c = svld1rq_f32(pg, (const float32_t *)&coeffs[i]);
    svuint64x2_t x0 = svld2_u64(pg, &in[i]);
    svfloat32_t x0_0 = svreinterpret_f32_u64(svget2(x0, 0));
    svfloat32_t x0_1 = svreinterpret_f32_u64(svget2(x0, 1));

    asm volatile("");

    svuint64x2_t x1 = svld2_vnum_u64(pg, &in[i], 2);
    svuint64x2_t x2 = svld2_vnum_u64(pg, &in[i], 4);
    svfloat32_t x1_0 = svreinterpret_f32_u64(svget2(x1, 0));
    svfloat32_t x1_1 = svreinterpret_f32_u64(svget2(x1, 1));
    svfloat32_t x2_0 = svreinterpret_f32_u64(svget2(x2, 0));
    svfloat32_t x2_1 = svreinterpret_f32_u64(svget2(x2, 1));

    asm volatile("");

    svuint64x2_t x3 = svld2_vnum_u64(pg, &in[i], 6);
    svuint64x2_t x4 = svld2_vnum_u64(pg, &in[i], 8);
    svfloat32_t x3_0 = svreinterpret_f32_u64(svget2(x3, 0));
    svfloat32_t x3_1 = svreinterpret_f32_u64(svget2(x3, 1));
    svfloat32_t x4_0 = svreinterpret_f32_u64(svget2(x4, 0));
    svfloat32_t x4_1 = svreinterpret_f32_u64(svget2(x4, 1));

    asm volatile("");

    svuint64x2_t x5 = svld2_vnum_u64(pg, &in[i], 10);
    svuint64x2_t x6 = svld2_vnum_u64(pg, &in[i], 12);
    svfloat32_t x5_0 = svreinterpret_f32_u64(svget2(x5, 0));
    svfloat32_t x5_1 = svreinterpret_f32_u64(svget2(x5, 1));
    svfloat32_t x6_0 = svreinterpret_f32_u64(svget2(x6, 0));
    svfloat32_t x6_1 = svreinterpret_f32_u64(svget2(x6, 1));

    asm volatile("");

    svuint64x2_t x7 = svld2_vnum_u64(pg, &in[i], 14);
    svfloat32_t x7_0 = svreinterpret_f32_u64(svget2(x7, 0));
    svfloat32_t x7_1 = svreinterpret_f32_u64(svget2(x7, 1));
    y_0 = svcmla_lane_f32(y_0, x0_0, c, 0, 0);
    y_1 = svcmla_lane_f32(y_1, x1_0, c, 0, 0);
    asm volatile("");
    y_2 = svcmla_lane_f32(y_2, x2_0, c, 0, 0);
    y_3 = svcmla_lane_f32(y_3, x3_0, c, 0, 0);
    asm volatile("");
    y_4 = svcmla_lane_f32(y_4, x4_0, c, 0, 0);
    y_5 = svcmla_lane_f32(y_5, x5_0, c, 0, 0);
    asm volatile("");
    y_6 = svcmla_lane_f32(y_6, x6_0, c, 0, 0);
    y_7 = svcmla_lane_f32(y_7, x7_0, c, 0, 0);

    asm volatile("");

    y_0 = svcmla_lane_f32(y_0, x0_1, c, 1, 0);
    y_1 = svcmla_lane_f32(y_1, x1_1, c, 1, 0);
    asm volatile("");
    y_2 = svcmla_lane_f32(y_2, x2_1, c, 1, 0);
    y_3 = svcmla_lane_f32(y_3, x3_1, c, 1, 0);
    asm volatile("");
    y_4 = svcmla_lane_f32(y_4, x4_1, c, 1, 0);
    y_5 = svcmla_lane_f32(y_5, x5_1, c, 1, 0);
    asm volatile("");
    y_6 = svcmla_lane_f32(y_6, x6_1, c, 1, 0);
    y_7 = svcmla_lane_f32(y_7, x7_1, c, 1, 0);

    asm volatile("");

    y_0 = svcmla_lane_f32(y_0, x0_0, c, 0, 90);
    y_1 = svcmla_lane_f32(y_1, x1_0, c, 0, 90);
    asm volatile("");
    y_2 = svcmla_lane_f32(y_2, x2_0, c, 0, 90);
    y_3 = svcmla_lane_f32(y_3, x3_0, c, 0, 90);
    asm volatile("");
    y_4 = svcmla_lane_f32(y_4, x4_0, c, 0, 90);
    y_5 = svcmla_lane_f32(y_5, x5_0, c, 0, 90);
    asm volatile("");
    y_6 = svcmla_lane_f32(y_6, x6_0, c, 0, 90);
    y_7 = svcmla_lane_f32(y_7, x7_0, c, 0, 90);

    asm volatile("");

    y_0 = svcmla_lane_f32(y_0, x0_1, c, 1, 90);
    y_1 = svcmla_lane_f32(y_1, x1_1, c, 1, 90);
    asm volatile("");
    y_2 = svcmla_lane_f32(y_2, x2_1, c, 1, 90);
    y_3 = svcmla_lane_f32(y_3, x3_1, c, 1, 90);
    asm volatile("");
    y_4 = svcmla_lane_f32(y_4, x4_1, c, 1, 90);
    y_5 = svcmla_lane_f32(y_5, x5_1, c, 1, 90);
    asm volatile("");
    y_6 = svcmla_lane_f32(y_6, x6_1, c, 1, 90);
    y_7 = svcmla_lane_f32(y_7, x7_1, c, 1, 90);
  }

  if (taps % 2) {
    svfloat32_t c =
        svreinterpret_f32_u64(svdup_u64(((const uint64_t *)coeffs)[i]));
    svfloat32_t x_0 = svld1_vnum_evens(pg, &in[i], 0);
    y_0 = sv_full_cmla(pg, y_0, c, x_0);
    svfloat32_t x_1 = svld1_vnum_evens(pg, &in[i], 2);
    y_1 = sv_full_cmla(pg, y_1, c, x_1);
    svfloat32_t x_2 = svld1_vnum_evens(pg, &in[i], 4);
    y_2 = sv_full_cmla(pg, y_2, c, x_2);
    svfloat32_t x_3 = svld1_vnum_evens(pg, &in[i], 6);
    y_3 = sv_full_cmla(pg, y_3, c, x_3);
    svfloat32_t x_4 = svld1_vnum_evens(pg, &in[i], 8);
    y_4 = sv_full_cmla(pg, y_4, c, x_4);
    svfloat32_t x_5 = svld1_vnum_evens(pg, &in[i], 10);
    y_5 = sv_full_cmla(pg, y_5, c, x_5);
    svfloat32_t x_6 = svld1_vnum_evens(pg, &in[i], 12);
    y_6 = sv_full_cmla(pg, y_6, c, x_6);
    svfloat32_t x_7 = svld1_vnum_evens(pg, &in[i], 14);
    y_7 = sv_full_cmla(pg, y_7, c, x_7);
  }

  svst1_f32(pg, (float32_t *)out, y_0);
  svst1_vnum_f32(pg, (float32_t *)out, 1, y_1);
  svst1_vnum_f32(pg, (float32_t *)out, 2, y_2);
  svst1_vnum_f32(pg, (float32_t *)out, 3, y_3);
  svst1_vnum_f32(pg, (float32_t *)out, 4, y_4);
  svst1_vnum_f32(pg, (float32_t *)out, 5, y_5);
  svst1_vnum_f32(pg, (float32_t *)out, 6, y_6);
  svst1_vnum_f32(pg, (float32_t *)out, 7, y_7);
}

#endif

armral_status armral_fir_filter_cf32_decimate_2(
    uint32_t size, uint32_t taps, const armral_cmplx_f32_t *restrict input,
    const armral_cmplx_f32_t *restrict coeffs, armral_cmplx_f32_t *output) {
#ifdef ARMRAL_ARCH_SVE
  svbool_t ptrue = svptrue_b32();
  uint32_t i = 0;
  for (; i + svcntd() * 16 <= size; i += svcntd() * 16) {
    sv_fir_block_8(ptrue, input + i, coeffs, output + (i >> 1), taps);
  }
  for (; i + svcntd() * 8 <= size; i += svcntd() * 8) {
    sv_fir_block_4(ptrue, input + i, coeffs, output + (i >> 1), taps);
  }
  for (; i + svcntd() * 4 <= size; i += svcntd() * 4) {
    sv_fir_block_2(ptrue, input + i, coeffs, output + (i >> 1), taps);
  }
  for (; i + svcntd() * 2 <= size; i += svcntd() * 2) {
    sv_fir_block(ptrue, input + i, coeffs, output + (i >> 1), taps);
  }
  svbool_t pg = svptrue_pat_b32(SV_VL8);
  for (; i + 8 <= size; i += 8) {
    sv_fir_block(pg, input + i, coeffs, output + (i >> 1), taps);
  }
#else
  /* Compute number of blocks of 8 input data and a possible tail */
  uint32_t blck_data = size >> 3U;

  /* Compute number of blocks of 4 taps and a possible tail */
  uint32_t tailtaps = taps & 0x3;
  uint32_t blck_taps;

  uint32_t blk8_taps = taps >> 3U;
  uint32_t tail8_taps = taps & 0x7;

  uint32_t tail_taps = tail8_taps & 0x3;
  uint32_t tail4_taps = tail8_taps >> 2U;

  float32x4x4_t s;
  float32x4x4_t old_s;
  float32x4x4_t shifted_s;
  float32x4x2_t h;
  const float32_t *fir_p_input;
  const float32_t *p_input = (const float32_t *)input;
  float32_t *p_output = (float32_t *)output;
  const float32_t *p_coeff;

  float32x4_t zero_32x4;
  float32x4x2_t accum;
  zero_32x4 = vdupq_n_f32(0.0);

  uint32x4_t fir_mask[4];

  fir_mask[0][0] = 0x00000000;
  fir_mask[0][1] = 0x00000000;
  fir_mask[0][2] = 0x00000000;
  fir_mask[0][3] = 0x00000000;
  fir_mask[1][0] = 0xFFFFFFFF;
  fir_mask[1][1] = 0x00000000;
  fir_mask[1][2] = 0x00000000;
  fir_mask[1][3] = 0x00000000;
  fir_mask[2][0] = 0xFFFFFFFF;
  fir_mask[2][1] = 0xFFFFFFFF;
  fir_mask[2][2] = 0x00000000;
  fir_mask[2][3] = 0x00000000;
  fir_mask[3][0] = 0xFFFFFFFF;
  fir_mask[3][1] = 0xFFFFFFFF;
  fir_mask[3][2] = 0xFFFFFFFF;
  fir_mask[3][3] = 0x00000000;

  /* Input data loop unrolling */
  while (blck_data > 0U) {
    /* At every iteration of the outer loop the pointer of the coefficient must
     * be reset */
    p_coeff = (const float32_t *)coeffs;

    /* Reset the accumulators */
    accum.val[0] = vdupq_n_f32(0.0);
    accum.val[1] = vdupq_n_f32(0.0);

    /* Load data */
    s = vld4q_f32(p_input);
    p_input += 16; /* 8 samples, Re + Im */
    fir_p_input = p_input;

    blck_taps = blk8_taps;
    while (blck_taps > 0U) {
      h = vld2q_f32(p_coeff);
      p_coeff += 8; /* 4 coefficients, Re + Im */

      float32x4_t h_re = h.val[0];
      float32x4_t h_im = h.val[1];
      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], s.val[0], h_re, 0);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], s.val[1], h_im, 0);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[1], h_re, 0);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[0], h_im, 0);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], s.val[2], h_re, 1);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], s.val[3], h_im, 1);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[3], h_re, 1);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[2], h_im, 1);

      /* Save the current samples and load new ones */
      old_s = s;
      s = vld4q_f32(fir_p_input);
      fir_p_input += 16; /* 8 samples, Re + Im */

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 1);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 1);
      shifted_s.val[2] = vextq_f32(old_s.val[2], s.val[2], 1);
      shifted_s.val[3] = vextq_f32(old_s.val[3], s.val[3], 1);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 2);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 2);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 2);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[2], h_re, 3);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[3], h_im, 3);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[3], h_re, 3);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[2], h_im, 3);

      h = vld2q_f32(p_coeff);
      p_coeff += 8; /* 4 coefficients, Re + Im */

      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 2);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 2);
      shifted_s.val[2] = vextq_f32(old_s.val[2], s.val[2], 2);
      shifted_s.val[3] = vextq_f32(old_s.val[3], s.val[3], 2);

      h_re = h.val[0];
      h_im = h.val[1];

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 0);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 0);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 0);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 0);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[2], h_re, 1);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[3], h_im, 1);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[3], h_re, 1);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[2], h_im, 1);

      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 3);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 3);
      shifted_s.val[2] = vextq_f32(old_s.val[2], s.val[2], 3);
      shifted_s.val[3] = vextq_f32(old_s.val[3], s.val[3], 3);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 2);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 2);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 2);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[2], h_re, 3);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[3], h_im, 3);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[3], h_re, 3);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[2], h_im, 3);

      blck_taps--;
    }

    /* Process number of taps between 4 and 7 included */
    if (tail4_taps > 0U) {
      h = vld2q_f32(p_coeff);
      p_coeff += 8; /* 4 coefficients, Re + Im */

      float32x4_t h_re = h.val[0];
      float32x4_t h_im = h.val[1];

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], s.val[0], h_re, 0);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], s.val[1], h_im, 0);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[1], h_re, 0);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[0], h_im, 0);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], s.val[2], h_re, 1);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], s.val[3], h_im, 1);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[3], h_re, 1);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[2], h_im, 1);

      /* Save the current samples and load new ones */
      old_s = s;
      s = vld4q_f32(fir_p_input);
      fir_p_input += 16; /* 8 samples, Re + Im */

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 1);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 1);
      shifted_s.val[2] = vextq_f32(old_s.val[2], s.val[2], 1);
      shifted_s.val[3] = vextq_f32(old_s.val[3], s.val[3], 1);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 2);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 2);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 2);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[2], h_re, 3);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[3], h_im, 3);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[3], h_re, 3);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[2], h_im, 3);

      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 2);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 2);
      shifted_s.val[2] = vextq_f32(old_s.val[2], s.val[2], 2);
      shifted_s.val[3] = vextq_f32(old_s.val[3], s.val[3], 2);

      /* Process a possible tail as another block of 4 */
      if (tail_taps) {
        h = vld2q_f32(p_coeff);
        p_coeff += 8; /* 4 coefficients, Re + Im */
        h.val[0] = vbslq_f32(fir_mask[tailtaps], h.val[0], zero_32x4);
        h.val[1] = vbslq_f32(fir_mask[tailtaps], h.val[1], zero_32x4);

        h_re = h.val[0];
        h_im = h.val[1];

        accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 0);
        accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 0);
        /* Compute y.im */
        accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 0);
        accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 0);

        /* Compute y.re */
        accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[2], h_re, 1);
        accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[3], h_im, 1);
        /* Compute y.im */
        accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[3], h_re, 1);
        accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[2], h_im, 1);

        shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 3);
        shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 3);
        shifted_s.val[2] = vextq_f32(old_s.val[2], s.val[2], 3);
        shifted_s.val[3] = vextq_f32(old_s.val[3], s.val[3], 3);

        /* Compute y.re */
        accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 2);
        accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 2);
        /* Compute y.im */
        accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 2);
        accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 2);

        /* Compute y.re */
        accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[2], h_re, 3);
        accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[3], h_im, 3);
        /* Compute y.im */
        accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[3], h_re, 3);
        accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[2], h_im, 3);
      }
    }

    /* FIR tail management */
    if ((tail4_taps == 0U) && (tail_taps != 0U)) {
      /* Load the last tailTaps coefficient */
      h = vld2q_f32(p_coeff);
      h.val[0] = vbslq_f32(fir_mask[tailtaps], h.val[0], zero_32x4);
      h.val[1] = vbslq_f32(fir_mask[tailtaps], h.val[1], zero_32x4);

      float32x4_t h_re = h.val[0];
      float32x4_t h_im = h.val[1];

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], s.val[0], h_re, 0);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], s.val[1], h_im, 0);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[1], h_re, 0);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[0], h_im, 0);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], s.val[2], h_re, 1);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], s.val[3], h_im, 1);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[3], h_re, 1);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], s.val[2], h_im, 1);

      /* Save the current samples and load new ones */
      old_s = s;
      s = vld4q_f32(fir_p_input);
      fir_p_input += 16; /* 8 samples, Re + Im */

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_f32(old_s.val[0], s.val[0], 1);
      shifted_s.val[1] = vextq_f32(old_s.val[1], s.val[1], 1);
      shifted_s.val[2] = vextq_f32(old_s.val[2], s.val[2], 1);
      shifted_s.val[3] = vextq_f32(old_s.val[3], s.val[3], 1);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[0], h_re, 2);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[1], h_re, 2);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[0], h_im, 2);

      /* Compute y.re */
      accum.val[0] = vfmaq_laneq_f32(accum.val[0], shifted_s.val[2], h_re, 3);
      accum.val[0] = vfmsq_laneq_f32(accum.val[0], shifted_s.val[3], h_im, 3);
      /* Compute y.im */
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[3], h_re, 3);
      accum.val[1] = vfmaq_laneq_f32(accum.val[1], shifted_s.val[2], h_im, 3);
    }

    /* Store the results */
    vst2q_f32(p_output, accum);
    p_output += 8;

    blck_data--;
  }
#endif // ARMRAL_ARCH_SVE
  return ARMRAL_SUCCESS;
}
