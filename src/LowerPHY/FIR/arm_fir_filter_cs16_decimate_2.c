/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#if ARMRAL_ARCH_SVE >= 2

#include <arm_sve.h>

static inline svint16_t shr16_interleave(svint32_t re, svint32_t im) {
  svint16_t y = svqshrnb_n_s32(re, 16);
  y = svqshrnt_n_s32(y, im, 16);
  return y;
}

static inline svint16_t svld_even_cmplx(svbool_t pg, const uint32_t *addr,
                                        uint32_t vnum) {
  svuint32x2_t ld2_res = svld2_vnum_u32(pg, addr, vnum);
  return svreinterpret_s16_u32(svget2(ld2_res, 0));
}

static inline svint32_t real_cmla(svint32_t acc, svint16_t in, svint16_t c) {
  acc = svmlalb_s32(acc, in, c);
  acc = svmlslt_s32(acc, in, c);
  return acc;
}

static inline svint32_t imag_cmla(svint32_t acc, svint16_t in, svint16_t c) {
  acc = svmlalb_s32(acc, in, c);
  acc = svmlalt_s32(acc, in, c);
  return acc;
}

static inline void sv_fir_4_block(svbool_t pg,
                                  const armral_cmplx_int16_t *restrict input,
                                  const armral_cmplx_int16_t *restrict coeffs,
                                  armral_cmplx_int16_t *output, uint32_t taps) {
  // Compute FIR on 4 vector-lengths of data (read 8 vector-lengths, write 4).
  // Unlike the other two versions we only have one accumulator each for real
  // and imag parts of each output vector, as there are enough independent
  // computations going on to hide the latency of MLAs. The MLAs have been
  // hand-interleaved with the LD2s to try and hide latency as much as possible.

  // The resulting code is quite dense. Input vectors have been named
  // vN_cM in order to try and clarify what's going on. For each
  // iteration of the main loop there are 4 complex coefficients:
  // [u0, v0, u1, v1, u2, v2, u3, v3].

  // We do two LD2s for each output vector N (0 to 3):
  // vN_0 = svld2(ptrue, &in[0])
  // vN_1 = svld2(ptrue, &in[2])
  // vN_c0 = svget2(vN_0, 0) = [x0, y0, x2, y2, x4, y4, ...]
  // vN_c1 = svget2(vN_0, 1) = [x1, y1, x3, y3, x5, y5, ...]
  // vN_c2 = svget2(vN_1, 0) = [x2, y2, x4, y4, x6, y6, ...]
  // vN_c3 = svget2(vN_1, 1) = [x3, y3, x5, y5, x7, y7, ...]
  // We get the right result by multiplying vN_cM by coefficient M (0 to 3, see
  // sv_fir_block for an explanation of how the widening MLAs implement CMLA)

  const uint32_t *in = (const uint32_t *)input;
  svint32_t acc_re_0 = svdup_s32(0);
  svint32_t acc_im_0 = svdup_s32(0);
  svint32_t acc_re_1 = svdup_s32(0);
  svint32_t acc_im_1 = svdup_s32(0);
  svint32_t acc_re_2 = svdup_s32(0);
  svint32_t acc_im_2 = svdup_s32(0);
  svint32_t acc_re_3 = svdup_s32(0);
  svint32_t acc_im_3 = svdup_s32(0);

  svbool_t ptrue = svptrue_b16();
  uint32_t i = 0;
  for (; i + 4 <= taps; i += 4) {
    svuint32x2_t v0 = svld2(ptrue, &in[i]);
    svint16_t v0_c0 = svreinterpret_s16_u32(svget2(v0, 0));
    svint16_t v0_c1 = svreinterpret_s16_u32(svget2(v0, 1));

    asm volatile("");

    svint16_t c = svld1rq(ptrue, (const int16_t *)&coeffs[i]);

    asm volatile("");

    svuint32x2_t v1_0 = svld2_vnum(ptrue, &in[i], 2);
    svint16_t v1_c0 = svreinterpret_s16_u32(svget2(v1_0, 0));
    svint16_t v1_c1 = svreinterpret_s16_u32(svget2(v1_0, 1));

    asm volatile("");

    acc_re_0 = svmlalb_lane_s32(acc_re_0, v0_c0, c, 0);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, v0_c0, c, 1);
    asm volatile("");
    svuint32x2_t v2_0 = svld2_vnum(ptrue, &in[i], 4);
    svint16_t v2_c0 = svreinterpret_s16_u32(svget2(v2_0, 0));
    svint16_t v2_c1 = svreinterpret_s16_u32(svget2(v2_0, 1));
    acc_re_0 = svmlslt_lane_s32(acc_re_0, v0_c0, c, 1);
    acc_im_0 = svmlalt_lane_s32(acc_im_0, v0_c0, c, 0);
    asm volatile("");
    acc_re_0 = svmlalb_lane_s32(acc_re_0, v0_c1, c, 2);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, v0_c1, c, 3);
    svuint32x2_t v3_0 = svld2_vnum(ptrue, &in[i], 6);
    svint16_t v3_c0 = svreinterpret_s16_u32(svget2(v3_0, 0));
    svint16_t v3_c1 = svreinterpret_s16_u32(svget2(v3_0, 1));
    asm volatile("");
    acc_re_1 = svmlalb_lane_s32(acc_re_1, v1_c0, c, 0);
    acc_im_1 = svmlalb_lane_s32(acc_im_1, v1_c0, c, 1);

    asm volatile("");

    svuint32x2_t v0_1 = svld2(ptrue, &in[i + 2]);
    svint16_t v0_c2 = svreinterpret_s16_u32(svget2(v0_1, 0));
    svint16_t v0_c3 = svreinterpret_s16_u32(svget2(v0_1, 1));
    acc_re_2 = svmlalb_lane_s32(acc_re_2, v2_c0, c, 0);
    acc_im_2 = svmlalb_lane_s32(acc_im_2, v2_c0, c, 1);
    asm volatile("");
    acc_re_3 = svmlalb_lane_s32(acc_re_3, v3_c0, c, 0);
    acc_im_3 = svmlalb_lane_s32(acc_im_3, v3_c0, c, 1);
    asm volatile("");
    svuint32x2_t v1_1 = svld2_vnum(ptrue, &in[i + 2], 2);
    svint16_t v1_c2 = svreinterpret_s16_u32(svget2(v1_1, 0));
    svint16_t v1_c3 = svreinterpret_s16_u32(svget2(v1_1, 1));
    acc_re_0 = svmlslt_lane_s32(acc_re_0, v0_c1, c, 3);
    acc_im_0 = svmlalt_lane_s32(acc_im_0, v0_c1, c, 2);
    asm volatile("");
    acc_re_1 = svmlslt_lane_s32(acc_re_1, v1_c0, c, 1);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, v1_c0, c, 0);

    asm volatile("");

    svuint32x2_t v2_1 = svld2_vnum(ptrue, &in[i + 2], 4);
    svint16_t v2_c2 = svreinterpret_s16_u32(svget2(v2_1, 0));
    svint16_t v2_c3 = svreinterpret_s16_u32(svget2(v2_1, 1));
    acc_re_2 = svmlslt_lane_s32(acc_re_2, v2_c0, c, 1);
    acc_im_2 = svmlalt_lane_s32(acc_im_2, v2_c0, c, 0);
    asm volatile("");
    acc_re_3 = svmlslt_lane_s32(acc_re_3, v3_c0, c, 1);
    acc_im_3 = svmlalt_lane_s32(acc_im_3, v3_c0, c, 0);
    asm volatile("");
    svuint32x2_t v3_1 = svld2_vnum(ptrue, &in[i + 2], 6);
    svint16_t v3_c2 = svreinterpret_s16_u32(svget2(v3_1, 0));
    svint16_t v3_c3 = svreinterpret_s16_u32(svget2(v3_1, 1));
    acc_re_0 = svmlalb_lane_s32(acc_re_0, v0_c2, c, 4);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, v0_c2, c, 5);
    asm volatile("");
    acc_re_1 = svmlalb_lane_s32(acc_re_1, v1_c1, c, 2);
    acc_im_1 = svmlalb_lane_s32(acc_im_1, v1_c1, c, 3);

    asm volatile("");
    acc_im_2 = svmlalb_lane_s32(acc_im_2, v2_c1, c, 3);
    acc_im_2 = svmlalt_lane_s32(acc_im_2, v2_c1, c, 2);
    asm volatile("");
    acc_re_3 = svmlalb_lane_s32(acc_re_3, v3_c1, c, 2);
    acc_im_3 = svmlalb_lane_s32(acc_im_3, v3_c1, c, 3);

    asm volatile("");

    acc_re_0 = svmlslt_lane_s32(acc_re_0, v0_c2, c, 5);
    acc_im_0 = svmlalt_lane_s32(acc_im_0, v0_c2, c, 4);
    asm volatile("");
    acc_re_1 = svmlslt_lane_s32(acc_re_1, v1_c1, c, 3);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, v1_c1, c, 2);
    asm volatile("");
    acc_re_2 = svmlslt_lane_s32(acc_re_2, v2_c1, c, 3);
    acc_re_2 = svmlalb_lane_s32(acc_re_2, v2_c1, c, 2);
    asm volatile("");
    acc_re_3 = svmlslt_lane_s32(acc_re_3, v3_c1, c, 3);
    acc_im_3 = svmlalt_lane_s32(acc_im_3, v3_c1, c, 2);

    asm volatile("");

    acc_re_0 = svmlalb_lane_s32(acc_re_0, v0_c3, c, 6);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, v0_c3, c, 7);
    asm volatile("");
    acc_re_1 = svmlalb_lane_s32(acc_re_1, v1_c2, c, 4);
    acc_im_1 = svmlalb_lane_s32(acc_im_1, v1_c2, c, 5);
    asm volatile("");
    acc_re_2 = svmlalb_lane_s32(acc_re_2, v2_c2, c, 4);
    acc_im_2 = svmlalb_lane_s32(acc_im_2, v2_c2, c, 5);
    asm volatile("");
    acc_re_3 = svmlalb_lane_s32(acc_re_3, v3_c2, c, 4);
    acc_im_3 = svmlalb_lane_s32(acc_im_3, v3_c2, c, 5);

    asm volatile("");

    acc_re_0 = svmlslt_lane_s32(acc_re_0, v0_c3, c, 7);
    acc_im_0 = svmlalt_lane_s32(acc_im_0, v0_c3, c, 6);
    asm volatile("");
    acc_re_1 = svmlslt_lane_s32(acc_re_1, v1_c2, c, 5);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, v1_c2, c, 4);
    asm volatile("");
    acc_re_2 = svmlslt_lane_s32(acc_re_2, v2_c2, c, 5);
    acc_im_2 = svmlalt_lane_s32(acc_im_2, v2_c2, c, 4);
    asm volatile("");
    acc_re_3 = svmlslt_lane_s32(acc_re_3, v3_c2, c, 5);
    acc_im_3 = svmlalt_lane_s32(acc_im_3, v3_c2, c, 4);

    asm volatile("");

    acc_re_1 = svmlalb_lane_s32(acc_re_1, v1_c3, c, 6);
    acc_im_1 = svmlalb_lane_s32(acc_im_1, v1_c3, c, 7);
    asm volatile("");
    acc_re_2 = svmlalb_lane_s32(acc_re_2, v2_c3, c, 6);
    acc_im_2 = svmlalb_lane_s32(acc_im_2, v2_c3, c, 7);
    asm volatile("");
    acc_re_3 = svmlalb_lane_s32(acc_re_3, v3_c3, c, 6);
    acc_im_3 = svmlalb_lane_s32(acc_im_3, v3_c3, c, 7);

    asm volatile("");

    acc_re_1 = svmlslt_lane_s32(acc_re_1, v1_c3, c, 7);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, v1_c3, c, 6);
    asm volatile("");
    acc_re_2 = svmlslt_lane_s32(acc_re_2, v2_c3, c, 7);
    acc_im_2 = svmlalt_lane_s32(acc_im_2, v2_c3, c, 6);
    asm volatile("");
    acc_re_3 = svmlslt_lane_s32(acc_re_3, v3_c3, c, 7);
    acc_im_3 = svmlalt_lane_s32(acc_im_3, v3_c3, c, 6);
  }

  for (; i < taps; i++) {
    svint16_t v0 = svld_even_cmplx(ptrue, &in[i], 0);
    svint16_t v1 = svld_even_cmplx(ptrue, &in[i], 2);
    svint16_t v2 = svld_even_cmplx(ptrue, &in[i], 4);
    svint16_t v3 = svld_even_cmplx(ptrue, &in[i], 6);
    svint16_t c =
        svreinterpret_s16_u32(svdup_n_u32(((const uint32_t *)coeffs)[i]));
    svint16_t c_rev = svrev_s16(c);
    acc_re_0 = real_cmla(acc_re_0, v0, c);
    acc_re_1 = real_cmla(acc_re_1, v1, c);
    acc_re_2 = real_cmla(acc_re_2, v2, c);
    acc_re_3 = real_cmla(acc_re_3, v3, c);
    acc_im_0 = imag_cmla(acc_im_0, v0, c_rev);
    acc_im_1 = imag_cmla(acc_im_1, v1, c_rev);
    acc_im_2 = imag_cmla(acc_im_2, v2, c_rev);
    acc_im_3 = imag_cmla(acc_im_3, v3, c_rev);
  }

  int16_t *out = (int16_t *)output;
  svst1_s16(pg, out, shr16_interleave(acc_re_0, acc_im_0));
  svst1_vnum_s16(pg, out, 1, shr16_interleave(acc_re_1, acc_im_1));
  svst1_vnum_s16(pg, out, 2, shr16_interleave(acc_re_2, acc_im_2));
  svst1_vnum_s16(pg, out, 3, shr16_interleave(acc_re_3, acc_im_3));
}

static inline void sv_fir_2_block(svbool_t pg,
                                  const armral_cmplx_int16_t *restrict input,
                                  const armral_cmplx_int16_t *restrict coeffs,
                                  armral_cmplx_int16_t *out, uint32_t taps) {
  // Compute FIR for two vector-lengths of data (read 4 vector-lengths, write
  // 2). This is simply an unrolled version of sv_fir_block - for an explanation
  // of the complex MLAs and halved accumulators see sv_fir_block. This version
  // is only used as a tail for when there is not enough data left for
  // sv_fir_4_block
  const uint32_t *in = (const uint32_t *)input;

  svint32_t acc_re_0_0 = svdup_s32(0);
  svint32_t acc_im_0_0 = svdup_s32(0);
  svint32_t acc_re_0_1 = svdup_s32(0);
  svint32_t acc_im_0_1 = svdup_s32(0);
  svint32_t acc_re_1_0 = svdup_s32(0);
  svint32_t acc_im_1_0 = svdup_s32(0);
  svint32_t acc_re_1_1 = svdup_s32(0);
  svint32_t acc_im_1_1 = svdup_s32(0);

  svbool_t ptrue = svptrue_b16();
  uint32_t i = 0;
  for (; i + 4 <= taps; i += 4) {
    svint16_t c = svld1rq(ptrue, (const int16_t *)&coeffs[i]);

    svuint32x2_t v12 = svld2(ptrue, &in[i]);
    svint16_t v1 = svreinterpret_s16_u32(svget2(v12, 0));
    svint16_t v2 = svreinterpret_s16_u32(svget2(v12, 1));

    svuint32x2_t v34 = svld2(ptrue, &in[i + 2]);
    svint16_t v3 = svreinterpret_s16_u32(svget2(v34, 0));
    svint16_t v4 = svreinterpret_s16_u32(svget2(v34, 1));

    svuint32x2_t v56 = svld2_vnum(ptrue, &in[i], 2);
    svint16_t v5 = svreinterpret_s16_u32(svget2(v56, 0));
    svint16_t v6 = svreinterpret_s16_u32(svget2(v56, 1));

    svuint32x2_t v78 = svld2_vnum(ptrue, &in[i + 2], 2);
    svint16_t v7 = svreinterpret_s16_u32(svget2(v78, 0));
    svint16_t v8 = svreinterpret_s16_u32(svget2(v78, 1));

    acc_re_0_0 = svmlalb_lane_s32(acc_re_0_0, v1, c, 0);
    acc_re_0_1 = svmlslt_lane_s32(acc_re_0_1, v1, c, 1);
    acc_im_0_0 = svmlalb_lane_s32(acc_im_0_0, v1, c, 1);
    acc_im_0_1 = svmlalt_lane_s32(acc_im_0_1, v1, c, 0);

    acc_re_0_0 = svmlalb_lane_s32(acc_re_0_0, v2, c, 2);
    acc_re_0_1 = svmlslt_lane_s32(acc_re_0_1, v2, c, 3);
    acc_im_0_0 = svmlalb_lane_s32(acc_im_0_0, v2, c, 3);
    acc_im_0_1 = svmlalt_lane_s32(acc_im_0_1, v2, c, 2);

    acc_re_0_0 = svmlalb_lane_s32(acc_re_0_0, v3, c, 4);
    acc_re_0_1 = svmlslt_lane_s32(acc_re_0_1, v3, c, 5);
    acc_im_0_0 = svmlalb_lane_s32(acc_im_0_0, v3, c, 5);
    acc_im_0_1 = svmlalt_lane_s32(acc_im_0_1, v3, c, 4);

    acc_re_0_0 = svmlalb_lane_s32(acc_re_0_0, v4, c, 6);
    acc_re_0_1 = svmlslt_lane_s32(acc_re_0_1, v4, c, 7);
    acc_im_0_0 = svmlalb_lane_s32(acc_im_0_0, v4, c, 7);
    acc_im_0_1 = svmlalt_lane_s32(acc_im_0_1, v4, c, 6);

    acc_re_1_0 = svmlalb_lane_s32(acc_re_1_0, v5, c, 0);
    acc_re_1_1 = svmlslt_lane_s32(acc_re_1_1, v5, c, 1);
    acc_im_1_0 = svmlalb_lane_s32(acc_im_1_0, v5, c, 1);
    acc_im_1_1 = svmlalt_lane_s32(acc_im_1_1, v5, c, 0);

    acc_re_1_0 = svmlalb_lane_s32(acc_re_1_0, v6, c, 2);
    acc_re_1_1 = svmlslt_lane_s32(acc_re_1_1, v6, c, 3);
    acc_im_1_0 = svmlalb_lane_s32(acc_im_1_0, v6, c, 3);
    acc_im_1_1 = svmlalt_lane_s32(acc_im_1_1, v6, c, 2);

    acc_re_1_0 = svmlalb_lane_s32(acc_re_1_0, v7, c, 4);
    acc_re_1_1 = svmlslt_lane_s32(acc_re_1_1, v7, c, 5);
    acc_im_1_0 = svmlalb_lane_s32(acc_im_1_0, v7, c, 5);
    acc_im_1_1 = svmlalt_lane_s32(acc_im_1_1, v7, c, 4);

    acc_re_1_0 = svmlalb_lane_s32(acc_re_1_0, v8, c, 6);
    acc_re_1_1 = svmlslt_lane_s32(acc_re_1_1, v8, c, 7);
    acc_im_1_0 = svmlalb_lane_s32(acc_im_1_0, v8, c, 7);
    acc_im_1_1 = svmlalt_lane_s32(acc_im_1_1, v8, c, 6);
  }

  svint32_t acc_re_0 = svadd_x(ptrue, acc_re_0_0, acc_re_0_1);
  svint32_t acc_im_0 = svadd_x(ptrue, acc_im_0_0, acc_im_0_1);
  svint32_t acc_re_1 = svadd_x(ptrue, acc_re_1_0, acc_re_1_1);
  svint32_t acc_im_1 = svadd_x(ptrue, acc_im_1_0, acc_im_1_1);

  for (; i < taps; i++) {
    svint16_t v0 = svld_even_cmplx(ptrue, &in[i], 0);
    svint16_t v1 = svld_even_cmplx(ptrue, &in[i], 2);
    svint16_t c =
        svreinterpret_s16_u32(svdup_n_u32(((const uint32_t *)coeffs)[i]));
    svint16_t c_rev = svrev_s16(c);

    acc_re_0 = real_cmla(acc_re_0, v0, c);
    acc_re_1 = real_cmla(acc_re_1, v1, c);
    acc_im_0 = imag_cmla(acc_im_0, v0, c_rev);
    acc_im_1 = imag_cmla(acc_im_1, v1, c_rev);
  }

  svst1_s16(pg, (int16_t *)out, shr16_interleave(acc_re_0, acc_im_0));
  svst1_vnum_s16(pg, (int16_t *)out, 1, shr16_interleave(acc_re_1, acc_im_1));
}

static inline void sv_fir_block(svbool_t pg,
                                const armral_cmplx_int16_t *restrict input,
                                const armral_cmplx_int16_t *restrict coeffs,
                                armral_cmplx_int16_t *out, uint32_t taps) {
  // Compute FIR on one vector-length of data (read 2 vector-lengths, write 1).
  // This version is only used as a tail for the more heavily optimized,
  // unrolled versions above.
  const uint32_t *in = (const uint32_t *)input;

  svint32_t acc_re_0 = svdup_s32(0);
  svint32_t acc_im_0 = svdup_s32(0);
  svint32_t acc_re_1 = svdup_s32(0);
  svint32_t acc_im_1 = svdup_s32(0);

  svbool_t ptrue = svptrue_b16();
  uint32_t i = 0;
  for (; i + 4 <= taps; i += 4) {
    // Loop over the coeffs array, unrolled by 4 as we can fit 4 complex
    // coefficient in one quad-word. Instead of CMLAs, we use SVE2 widening
    // MLAs, as accumulators may overflow in 16 bits.

    // For complex multiplication expressed as:
    // (x + yi)(u + vi) = (xu - yv) + (xv + yu)

    // One complete complex widening MLA of a vector V by coefficient N of the
    // coeff vector C:

    // V = [x0, y0, x1, y1, x2, y2, ...]
    // C = [u0, v0, u1, v1, u2, v2, ...]

    // accumulate x*uN to real accumulator:
    // acc_re = svmlalb_lane_s32(acc_re, V, C, 2N);

    // accumulate -y*vN to real accumulator:
    // acc_re = svmlslt_lane_s32(acc_re, V, C, 2N + 1);

    // accumulate x*vN to imag accumulator:
    // acc_im = svmlalb_lane_s32(acc_im, V, C, 2N + 1);

    // accumulate y*uN to imag accumulator:
    // acc_im = svmlalb_lane_s32(acc_im, V, C, 2N);

    // There is one small complication, which is that we have two accumulator
    // vectors per vector-lenth of output. This allows dependency chains to be
    // relaxed in the MLAs, meaning we don't have to wait for one MLA to
    // complete before starting another. For instance acc_re_0 accumulates all
    // the SVMLALBs of real input, and acc_re_1 accumulates all the SVMLSLTs of
    // real input

    svint16_t c = svld1rq(ptrue, (const int16_t *)&coeffs[i]);

    svuint32x2_t v12 = svld2(ptrue, &in[i]);
    svuint32x2_t v34 = svld2(ptrue, &in[i + 2]);
    svint16_t v1 = svreinterpret_s16_u32(svget2(v12, 0));
    svint16_t v2 = svreinterpret_s16_u32(svget2(v12, 1));
    svint16_t v3 = svreinterpret_s16_u32(svget2(v34, 0));
    svint16_t v4 = svreinterpret_s16_u32(svget2(v34, 1));

    // v1 = [x0, y0, x2, y2, x4, y4, ...]
    // v2 = [x1, y1, x3, y3, x5, y5, ...]
    // v3 = [x2, y2, x4, y4, x6, y6, ...]
    // v4 = [x3, y3, x5, y5, x7, y7, ...]

    acc_re_0 = svmlalb_lane_s32(acc_re_0, v1, c, 0);
    acc_re_1 = svmlslt_lane_s32(acc_re_1, v1, c, 1);
    acc_re_0 = svmlalb_lane_s32(acc_re_0, v2, c, 2);
    acc_re_1 = svmlslt_lane_s32(acc_re_1, v2, c, 3);
    acc_re_0 = svmlalb_lane_s32(acc_re_0, v3, c, 4);
    acc_re_1 = svmlslt_lane_s32(acc_re_1, v3, c, 5);
    acc_re_0 = svmlalb_lane_s32(acc_re_0, v4, c, 6);
    acc_re_1 = svmlslt_lane_s32(acc_re_1, v4, c, 7);

    acc_im_0 = svmlalb_lane_s32(acc_im_0, v1, c, 1);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, v1, c, 0);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, v2, c, 3);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, v2, c, 2);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, v3, c, 5);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, v3, c, 4);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, v4, c, 7);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, v4, c, 6);
  }

  // Sum the accumulators before the tail loop
  svint32_t acc_re = svadd_x(ptrue, acc_re_0, acc_re_1);
  svint32_t acc_im = svadd_x(ptrue, acc_im_0, acc_im_1);

  for (; i < taps; i++) {
    svint16_t v = svld_even_cmplx(ptrue, &in[i], 0);
    svint16_t c =
        svreinterpret_s16_u32(svdup_n_u32(((const uint32_t *)coeffs)[i]));
    svint16_t c_rev = svrev_s16(c);
    acc_re = real_cmla(acc_re, v, c);
    acc_im = imag_cmla(acc_im, v, c_rev);
  }

  // Re-interleave real and imag parts, saturating if needs be, and store
  svst1_s16(pg, (int16_t *)out, shr16_interleave(acc_re, acc_im));
}

#endif // ARMRAL_ARCH_SVE >= 2

armral_status armral_fir_filter_cs16_decimate_2(
    uint32_t size, uint32_t taps, const armral_cmplx_int16_t *restrict input,
    const armral_cmplx_int16_t *restrict coeffs, armral_cmplx_int16_t *output) {
#if ARMRAL_ARCH_SVE >= 2
  uint32_t i = 0;
  for (; i + svcntw() * 8 <= size; i += svcntw() * 8) {
    sv_fir_4_block(svptrue_b16(), input + i, coeffs, output + i / 2, taps);
  }
  for (; i + svcntw() * 4 <= size; i += svcntw() * 4) {
    sv_fir_2_block(svptrue_b16(), input + i, coeffs, output + i / 2, taps);
  }
  for (; i + svcntw() * 2 <= size; i += svcntw() * 2) {
    sv_fir_block(svptrue_b16(), input + i, coeffs, output + i / 2, taps);
  }
  for (; i + 8 <= size; i += 8) {
    sv_fir_block(svwhilelt_b16_u32(i, size), input + i, coeffs, output + i / 2,
                 taps);
  }
#else  // ARMRAL_ARCH_SVE < 2
  /* Compute number of blocks of 8 input data and a possible tail */
  uint32_t blck_data = size >> 3U;

  /* Compute number of blocks of 4 taps and a possible tail */
  uint32_t tailtaps = taps & 0x3;
  uint32_t blck_taps;

  uint32_t blk8_taps = taps >> 3U;
  uint32_t tail8_taps = taps & 0x7;

  uint32_t tail_taps = tail8_taps & 0x3;
  uint32_t tail4_taps = tail8_taps >> 2U;

  int16x4x4_t s;
  int16x4x4_t old_s;
  int16x4x4_t shifted_s;
  int16x4x2_t h;
  const int16_t *fir_p_input;
  const int16_t *p_input = (const int16_t *)input;
  int16_t *p_output = (int16_t *)output;
  const int16_t *p_coeff;

  int16x4_t zero_16x4;
  int32x4x2_t accum;
  int16x4x2_t data_filtered;
  zero_16x4 = vdup_n_s16(0);

  uint16x4_t fir_mask[4];

  fir_mask[0][0] = 0x0000;
  fir_mask[0][1] = 0x0000;
  fir_mask[0][2] = 0x0000;
  fir_mask[0][3] = 0x0000;
  fir_mask[1][0] = 0xFFFF;
  fir_mask[1][1] = 0x0000;
  fir_mask[1][2] = 0x0000;
  fir_mask[1][3] = 0x0000;
  fir_mask[2][0] = 0xFFFF;
  fir_mask[2][1] = 0xFFFF;
  fir_mask[2][2] = 0x0000;
  fir_mask[2][3] = 0x0000;
  fir_mask[3][0] = 0xFFFF;
  fir_mask[3][1] = 0xFFFF;
  fir_mask[3][2] = 0xFFFF;
  fir_mask[3][3] = 0x0000;

  /* Input data loop unrolling */
  while (blck_data > 0U) {
    /* At every iteration of the outer loop the pointer of the coefficient must
     * be reset */
    p_coeff = (const int16_t *)coeffs;

    /* Reset the accumulators */
    accum.val[0] = vdupq_n_s32(0);
    accum.val[1] = vdupq_n_s32(0);

    /* Load data */
    s = vld4_s16(p_input);
    p_input += 16; /* 8 samples, Re + Im */
    fir_p_input = p_input;

    blck_taps = blk8_taps;
    while (blck_taps > 0U) {
      h = vld2_s16(p_coeff);
      p_coeff += 8; /* 4 coefficients, Re + Im */

      int16x4_t h_re = h.val[0];
      int16x4_t h_im = h.val[1];

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], s.val[0], h_re, 0);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], s.val[1], h_im, 0);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[0], h_im, 0);
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[1], h_re, 0);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], s.val[2], h_re, 1);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], s.val[3], h_im, 1);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[2], h_im, 1);
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[3], h_re, 1);

      /* Save the current samples and load new ones */
      old_s = s;
      s = vld4_s16(fir_p_input);
      fir_p_input += 16; /* 8 samples, Re + Im */

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vext_s16(old_s.val[0], s.val[0], 1);
      shifted_s.val[1] = vext_s16(old_s.val[1], s.val[1], 1);
      shifted_s.val[2] = vext_s16(old_s.val[2], s.val[2], 1);
      shifted_s.val[3] = vext_s16(old_s.val[3], s.val[3], 1);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[0], h_re, 2);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[0], h_im, 2);
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[1], h_re, 2);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[2], h_re, 3);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[3], h_im, 3);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[2], h_im, 3);
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[3], h_re, 3);

      h = vld2_s16(p_coeff);
      p_coeff += 8; /* 4 coefficients, Re + Im */

      h_re = h.val[0];
      h_im = h.val[1];

      shifted_s.val[0] = vext_s16(old_s.val[0], s.val[0], 2);
      shifted_s.val[1] = vext_s16(old_s.val[1], s.val[1], 2);
      shifted_s.val[2] = vext_s16(old_s.val[2], s.val[2], 2);
      shifted_s.val[3] = vext_s16(old_s.val[3], s.val[3], 2);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[0], h_re, 0);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[1], h_im, 0);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[0], h_im, 0);
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[1], h_re, 0);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[2], h_re, 1);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[3], h_im, 1);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[2], h_im, 1);
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[3], h_re, 1);

      shifted_s.val[0] = vext_s16(old_s.val[0], s.val[0], 3);
      shifted_s.val[1] = vext_s16(old_s.val[1], s.val[1], 3);
      shifted_s.val[2] = vext_s16(old_s.val[2], s.val[2], 3);
      shifted_s.val[3] = vext_s16(old_s.val[3], s.val[3], 3);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[0], h_re, 2);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[0], h_im, 2);
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[1], h_re, 2);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[2], h_re, 3);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[3], h_im, 3);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[2], h_im, 3);
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[3], h_re, 3);

      blck_taps--;
    }

    /* Process number of taps between 4 and 7 included */
    if (tail4_taps > 0U) {
      h = vld2_s16(p_coeff);
      p_coeff += 8; /* 4 coefficients, Re + Im */

      int16x4_t h_re = h.val[0];
      int16x4_t h_im = h.val[1];

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], s.val[0], h_re, 0);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], s.val[1], h_im, 0);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[0], h_im, 0);
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[1], h_re, 0);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], s.val[2], h_re, 1);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], s.val[3], h_im, 1);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[2], h_im, 1);
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[3], h_re, 1);

      /* Save the current samples and load new ones */
      old_s = s;
      s = vld4_s16(fir_p_input);
      fir_p_input += 16; /* 8 samples, Re + Im */

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vext_s16(old_s.val[0], s.val[0], 1);
      shifted_s.val[1] = vext_s16(old_s.val[1], s.val[1], 1);
      shifted_s.val[2] = vext_s16(old_s.val[2], s.val[2], 1);
      shifted_s.val[3] = vext_s16(old_s.val[3], s.val[3], 1);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[0], h_re, 2);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[0], h_im, 2);
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[1], h_re, 2);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[2], h_re, 3);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[3], h_im, 3);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[2], h_im, 3);
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[3], h_re, 3);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vext_s16(old_s.val[0], s.val[0], 2);
      shifted_s.val[1] = vext_s16(old_s.val[1], s.val[1], 2);
      shifted_s.val[2] = vext_s16(old_s.val[2], s.val[2], 2);
      shifted_s.val[3] = vext_s16(old_s.val[3], s.val[3], 2);

      /* Process a possible tail as another block of 4 */
      if (tail_taps) {
        h = vld2_s16(p_coeff); /* 4 coefficients, Re + Im */

        h.val[0] = vbsl_s16(fir_mask[tailtaps], h.val[0], zero_16x4);
        h.val[1] = vbsl_s16(fir_mask[tailtaps], h.val[1], zero_16x4);

        h_re = h.val[0];
        h_im = h.val[1];

        /* Compute y.re */
        accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[0], h_re, 0);
        accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[1], h_im, 0);
        /* Compute y.im */
        accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[0], h_im, 0);
        accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[1], h_re, 0);

        /* Compute y.re */
        accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[2], h_re, 1);
        accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[3], h_im, 1);
        /* Compute y.im */
        accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[2], h_im, 1);
        accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[3], h_re, 1);

        shifted_s.val[0] = vext_s16(old_s.val[0], s.val[0], 3);
        shifted_s.val[1] = vext_s16(old_s.val[1], s.val[1], 3);
        shifted_s.val[2] = vext_s16(old_s.val[2], s.val[2], 3);
        shifted_s.val[3] = vext_s16(old_s.val[3], s.val[3], 3);

        /* Compute y.re */
        accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[0], h_re, 2);
        accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[1], h_im, 2);
        /* Compute y.im */
        accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[0], h_im, 2);
        accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[1], h_re, 2);

        /* Compute y.re */
        accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[2], h_re, 3);
        accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[3], h_im, 3);
        /* Compute y.im */
        accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[2], h_im, 3);
        accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[3], h_re, 3);
      }
    }

    /* FIR tail management */
    if ((tail4_taps == 0U) && (tail_taps != 0U)) {
      h = vld2_s16(p_coeff);
      h.val[0] = vbsl_s16(fir_mask[tailtaps], h.val[0], zero_16x4);
      h.val[1] = vbsl_s16(fir_mask[tailtaps], h.val[1], zero_16x4);

      int16x4_t h_re = h.val[0];
      int16x4_t h_im = h.val[1];

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], s.val[0], h_re, 0);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], s.val[1], h_im, 0);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[0], h_im, 0);
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[1], h_re, 0);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], s.val[2], h_re, 1);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], s.val[3], h_im, 1);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[2], h_im, 1);
      accum.val[1] = vmlal_lane_s16(accum.val[1], s.val[3], h_re, 1);

      /* Save the current samples and load new ones */
      old_s = s;
      s = vld4_s16(fir_p_input);
      fir_p_input += 16;

      shifted_s.val[0] = vext_s16(old_s.val[0], s.val[0], 1);
      shifted_s.val[1] = vext_s16(old_s.val[1], s.val[1], 1);
      shifted_s.val[2] = vext_s16(old_s.val[2], s.val[2], 1);
      shifted_s.val[3] = vext_s16(old_s.val[3], s.val[3], 1);

      /* Compute y.re */
      accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[0], h_re, 2);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im */
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[0], h_im, 2);
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[1], h_re, 2);

      /* Compute y.re for lower int16x4_t */
      accum.val[0] = vmlal_lane_s16(accum.val[0], shifted_s.val[2], h_re, 3);
      accum.val[0] = vmlsl_lane_s16(accum.val[0], shifted_s.val[3], h_im, 3);
      /* Compute y.im for lower int16x4_t */
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[2], h_im, 3);
      accum.val[1] = vmlal_lane_s16(accum.val[1], shifted_s.val[3], h_re, 3);
    }

    data_filtered.val[0] = vqshrn_n_s32(accum.val[0], 16);
    data_filtered.val[1] = vqshrn_n_s32(accum.val[1], 16);

    /* Store the results */
    vst2_s16(p_output, data_filtered);
    p_output += 8;

    blck_data--;
  }
#endif // ARMRAL_ARCH_SVE >= 2
  return ARMRAL_SUCCESS;
}
