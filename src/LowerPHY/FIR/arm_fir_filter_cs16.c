/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"

#if ARMRAL_ARCH_SVE >= 2

#include <arm_sve.h>

static inline svint32_t acc_fir_re(svint32_t acc, svint16_t in, svint16_t c) {
  acc = svmlalb_s32(acc, in, c);
  acc = svmlslt_s32(acc, in, c);
  return acc;
}

static inline svint32_t acc_fir_im(svint32_t acc, svint16_t in, svint16_t c) {
  acc = svmlalb_s32(acc, in, c);
  acc = svmlalt_s32(acc, in, c);
  return acc;
}

static inline svint16_t shr16_interleave(svint32_t re, svint32_t im) {
  svint16_t y = svqshrnb_n_s32(re, 16);
  y = svqshrnt_n_s32(y, im, 16);
  return y;
}

static inline void sv_fir_block(const armral_cmplx_int16_t *restrict input,
                                const armral_cmplx_int16_t *restrict coeffs,
                                armral_cmplx_int16_t *output, svbool_t pg,
                                uint32_t taps) {
  svint32_t acc_re = svdup_n_s32(0);
  svint32_t acc_im = svdup_n_s32(0);
  uint32_t t = 0;

  for (; t + 4 <= taps; t += 4) {
    svint16_t c = svld1rq_s16(pg, (const int16_t *)&coeffs[t]);

    svint16_t in_0 = svld1_s16(pg, (const int16_t *)&input[t]);
    svint16_t in_1 = svld1_s16(pg, (const int16_t *)&input[t + 1]);
    svint16_t in_2 = svld1_s16(pg, (const int16_t *)&input[t + 2]);
    svint16_t in_3 = svld1_s16(pg, (const int16_t *)&input[t + 3]);

    acc_re = svmlalb_lane_s32(acc_re, in_0, c, 0);
    acc_re = svmlslt_lane_s32(acc_re, in_0, c, 1);
    acc_re = svmlalb_lane_s32(acc_re, in_1, c, 2);
    acc_re = svmlslt_lane_s32(acc_re, in_1, c, 3);
    acc_re = svmlalb_lane_s32(acc_re, in_2, c, 4);
    acc_re = svmlslt_lane_s32(acc_re, in_2, c, 5);
    acc_re = svmlalb_lane_s32(acc_re, in_3, c, 6);
    acc_re = svmlslt_lane_s32(acc_re, in_3, c, 7);

    acc_im = svmlalb_lane_s32(acc_im, in_0, c, 1);
    acc_im = svmlalt_lane_s32(acc_im, in_0, c, 0);
    acc_im = svmlalb_lane_s32(acc_im, in_1, c, 3);
    acc_im = svmlalt_lane_s32(acc_im, in_1, c, 2);
    acc_im = svmlalb_lane_s32(acc_im, in_2, c, 5);
    acc_im = svmlalt_lane_s32(acc_im, in_2, c, 4);
    acc_im = svmlalb_lane_s32(acc_im, in_3, c, 7);
    acc_im = svmlalt_lane_s32(acc_im, in_3, c, 6);
  }

  for (; t < taps; t++) {
    svint16_t in = svld1_s16(pg, (const int16_t *)&input[t]);
    svint16_t c =
        svreinterpret_s16_u32(svdup_n_u32(((const uint32_t *)coeffs)[t]));
    svint16_t c_rev = svrev_s16(c);
    acc_re = acc_fir_re(acc_re, in, c);
    acc_im = acc_fir_im(acc_im, in, c_rev);
  }

  svint16_t y = shr16_interleave(acc_re, acc_im);
  svst1_s16(pg, (int16_t *)output, y);
}

static inline void sv_fir_2_block(const armral_cmplx_int16_t *restrict input,
                                  const armral_cmplx_int16_t *restrict coeffs,
                                  armral_cmplx_int16_t *output, svbool_t pg,
                                  uint32_t taps) {
  /*
    Compute FIR on 2 vector-lengths of input. Maintain two separate
    accumulators EACH for real and imaginary parts of each
    vector. This is so that the latency of MLAs can be hidden
    sufficiently - each pair of accumulators is added after the
    unrolled loop.
  */
  svint32_t acc_re_0_0 = svdup_n_s32(0);
  svint32_t acc_im_0_0 = svdup_n_s32(0);
  svint32_t acc_re_1_0 = svdup_n_s32(0);
  svint32_t acc_im_1_0 = svdup_n_s32(0);
  svint32_t acc_re_0_1 = svdup_n_s32(0);
  svint32_t acc_im_0_1 = svdup_n_s32(0);
  svint32_t acc_re_1_1 = svdup_n_s32(0);
  svint32_t acc_im_1_1 = svdup_n_s32(0);

  uint32_t t = 0;
  for (; t + 4 <= taps; t += 4) {
    /*
      Manually unrolled loop over coefficient array. Go 4 coefficients
      at a time. See sv_fir_4_block for explanation of the widening
      complex MLAs below.
    */
    svint16_t c = svld1rq_s16(pg, (const int16_t *)&coeffs[t]);
    svint16_t in_0 = svld1_s16(pg, (const int16_t *)&input[t]);
    svint16_t in_4 = svld1_vnum_s16(pg, (const int16_t *)&input[t], 1);

    asm volatile("");

    svint16_t in_1 = svld1_s16(pg, (const int16_t *)&input[t + 1]);
    acc_re_0_0 = svmlalb_lane_s32(acc_re_0_0, in_0, c, 0);
    acc_im_0_0 = svmlalb_lane_s32(acc_im_0_0, in_0, c, 1);
    asm volatile("");
    svint16_t in_5 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 1], 1);
    acc_re_1_0 = svmlalb_lane_s32(acc_re_1_0, in_4, c, 0);
    acc_im_1_0 = svmlalb_lane_s32(acc_im_1_0, in_4, c, 1);

    asm volatile("");

    acc_re_0_1 = svmlslt_lane_s32(acc_re_0_1, in_0, c, 1);
    acc_im_0_1 = svmlalt_lane_s32(acc_im_0_1, in_0, c, 0);
    asm volatile("");
    acc_re_1_1 = svmlslt_lane_s32(acc_re_1_1, in_4, c, 1);
    acc_im_1_1 = svmlalt_lane_s32(acc_im_1_1, in_4, c, 0);

    asm volatile("");

    svint16_t in_2 = svld1_s16(pg, (const int16_t *)&input[t + 2]);
    acc_re_0_0 = svmlalb_lane_s32(acc_re_0_0, in_1, c, 2);
    acc_im_0_0 = svmlalb_lane_s32(acc_im_0_0, in_1, c, 3);
    asm volatile("");
    svint16_t in_6 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 2], 1);
    acc_re_1_0 = svmlalb_lane_s32(acc_re_1_0, in_5, c, 2);
    acc_im_1_0 = svmlalb_lane_s32(acc_im_1_0, in_5, c, 3);

    asm volatile("");

    acc_re_0_1 = svmlslt_lane_s32(acc_re_0_1, in_1, c, 3);
    acc_im_0_1 = svmlalt_lane_s32(acc_im_0_1, in_1, c, 2);
    asm volatile("");
    acc_re_1_1 = svmlslt_lane_s32(acc_re_1_1, in_5, c, 3);
    acc_im_1_1 = svmlalt_lane_s32(acc_im_1_1, in_5, c, 2);

    asm volatile("");

    svint16_t in_3 = svld1_s16(pg, (const int16_t *)&input[t + 3]);
    acc_re_0_0 = svmlalb_lane_s32(acc_re_0_0, in_2, c, 4);
    acc_im_0_0 = svmlalb_lane_s32(acc_im_0_0, in_2, c, 5);
    asm volatile("");
    svint16_t in_7 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 3], 1);
    acc_re_1_0 = svmlalb_lane_s32(acc_re_1_0, in_6, c, 4);
    acc_im_1_0 = svmlalb_lane_s32(acc_im_1_0, in_6, c, 5);

    asm volatile("");

    acc_re_0_1 = svmlslt_lane_s32(acc_re_0_1, in_2, c, 5);
    acc_im_0_1 = svmlalt_lane_s32(acc_im_0_1, in_2, c, 4);
    asm volatile("");
    acc_re_1_1 = svmlslt_lane_s32(acc_re_1_1, in_6, c, 5);
    acc_im_1_1 = svmlalt_lane_s32(acc_im_1_1, in_6, c, 4);

    asm volatile("");

    acc_re_0_0 = svmlalb_lane_s32(acc_re_0_0, in_3, c, 6);
    acc_im_0_0 = svmlalb_lane_s32(acc_im_0_0, in_3, c, 7);
    asm volatile("");
    acc_re_1_0 = svmlalb_lane_s32(acc_re_1_0, in_7, c, 6);
    acc_im_1_0 = svmlalb_lane_s32(acc_im_1_0, in_7, c, 7);
    asm volatile("");
    acc_re_0_1 = svmlslt_lane_s32(acc_re_0_1, in_3, c, 7);
    acc_im_0_1 = svmlalt_lane_s32(acc_im_0_1, in_3, c, 6);
    asm volatile("");
    acc_re_1_1 = svmlslt_lane_s32(acc_re_1_1, in_7, c, 7);
    acc_im_1_1 = svmlalt_lane_s32(acc_im_1_1, in_7, c, 6);
  }

  svint32_t acc_re_0 = svadd_x(pg, acc_re_0_0, acc_re_0_1);
  svint32_t acc_im_0 = svadd_x(pg, acc_im_0_0, acc_im_0_1);
  svint32_t acc_re_1 = svadd_x(pg, acc_re_1_0, acc_re_1_1);
  svint32_t acc_im_1 = svadd_x(pg, acc_im_1_0, acc_im_1_1);

  for (; t < taps; t++) {
    svint16_t in_0 = svld1_s16(pg, (const int16_t *)&input[t]);
    svint16_t in_1 = svld1_vnum_s16(pg, (const int16_t *)&input[t], 1);
    svint16_t c =
        svreinterpret_s16_u32(svdup_n_u32(((const uint32_t *)coeffs)[t]));
    svint16_t c_rev = svrev_s16(c);

    acc_re_0 = acc_fir_re(acc_re_0, in_0, c);
    acc_im_0 = acc_fir_im(acc_im_0, in_0, c_rev);
    acc_re_1 = acc_fir_re(acc_re_1, in_1, c);
    acc_im_1 = acc_fir_im(acc_im_1, in_1, c_rev);
  }

  svint16_t y_0 = shr16_interleave(acc_re_0, acc_im_0);
  svst1_s16(pg, (int16_t *)output, y_0);

  svint16_t y_1 = shr16_interleave(acc_re_1, acc_im_1);
  svst1_vnum_s16(pg, (int16_t *)output, 1, y_1);
}

static inline void sv_fir_4_block(const armral_cmplx_int16_t *restrict input,
                                  const armral_cmplx_int16_t *restrict coeffs,
                                  armral_cmplx_int16_t *output, svbool_t pg,
                                  uint32_t taps) {
  /*
    Compute FIR on 4 vector-lengths of input. Maintain a separate
    accumulator for real and imaginary parts of each vector
  */

  svint32_t acc_re_0 = svdup_n_s32(0);
  svint32_t acc_im_0 = svdup_n_s32(0);
  svint32_t acc_re_1 = svdup_n_s32(0);
  svint32_t acc_im_1 = svdup_n_s32(0);
  svint32_t acc_re_2 = svdup_n_s32(0);
  svint32_t acc_im_2 = svdup_n_s32(0);
  svint32_t acc_re_3 = svdup_n_s32(0);
  svint32_t acc_im_3 = svdup_n_s32(0);

  uint32_t t = 0;
  for (; t + 4 <= taps; t += 4) {
    /*
      Manually unrolled loop over coefficient array. Go 4 coefficients
      at a time.

      Each quad-word of c contains 4 complex coefficients, where the
      components are 16-bit.  For each coeff N (0 to 3) and each input
      vector M (0 to 3), the complex multiply-accumulate looks like:

      Complex multiplication: (x + yi)(u + vi) = (xu - yv) + (xv + yu)i

      // multiply even (real) elements of in_M by element 2N of c
      // (i.e. real component of coeff N), widen and accumulate to
      // acc_re_M
      // real_accumulator += xu
      acc_re_M = svmlalb_lane_s32(acc_re_M, in_M, c, 2N);

      // multiply odd (imag) elements of in_M by element 2N + 1 of c
      // (i.e. imag component of coeff N), widen and subtract from
      // acc_re_M
      // real_accumulator -= yv
      acc_re_M = svmlslt_lane_s32(acc_re_M, in_M, c, 2N + 1);

      // multiply even (real) elements of in_M by element 2N of c
      // (i.e. real component of coeff N), widen and accumulate to
      // acc_im_N
      // imag_accumulator += xv
      acc_im_M = svmlalb_lane_s32(acc_im_M, in_M, c, 2N);

      // multiply odd (imag) elements of in_M by elements 2N + 1 of c
      // (i.e. imag component of coeff N), widen and accumulate to
      // acc_im_N
      // imag_accumulator += yu
      acc_im_M = svmlalt_lane_s32(acc_im_M, in_M, c, 2N + 1);

      All multiply-accumulates are widened to 32-bit to prevent
      overflow.  These operations are interleaved with the loads for
      the next batch of inputs in order to hide load latency.
    */

    svint16_t c = svld1rq_s16(pg, (const int16_t *)&coeffs[t]);
    svint16_t in_0 = svld1_s16(pg, (const int16_t *)&input[t]);
    svint16_t in_4 = svld1_vnum_s16(pg, (const int16_t *)&input[t], 1);
    svint16_t in_8 = svld1_vnum_s16(pg, (const int16_t *)&input[t], 2);
    svint16_t in_12 = svld1_vnum_s16(pg, (const int16_t *)&input[t], 3);

    asm volatile("");

    svint16_t in_1 = svld1_s16(pg, (const int16_t *)&input[t + 1]);
    acc_re_0 = svmlalb_lane_s32(acc_re_0, in_0, c, 0);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, in_0, c, 1);
    asm volatile("");
    svint16_t in_5 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 1], 1);
    acc_re_1 = svmlalb_lane_s32(acc_re_1, in_4, c, 0);
    acc_im_1 = svmlalb_lane_s32(acc_im_1, in_4, c, 1);
    asm volatile("");
    svint16_t in_9 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 1], 2);
    acc_re_2 = svmlalb_lane_s32(acc_re_2, in_8, c, 0);
    acc_im_2 = svmlalb_lane_s32(acc_im_2, in_8, c, 1);
    asm volatile("");
    svint16_t in_13 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 1], 3);
    acc_re_3 = svmlalb_lane_s32(acc_re_3, in_12, c, 0);
    acc_im_3 = svmlalb_lane_s32(acc_im_3, in_12, c, 1);

    asm volatile("");

    acc_re_0 = svmlslt_lane_s32(acc_re_0, in_0, c, 1);
    acc_im_0 = svmlalt_lane_s32(acc_im_0, in_0, c, 0);
    asm volatile("");
    acc_re_1 = svmlslt_lane_s32(acc_re_1, in_4, c, 1);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, in_4, c, 0);
    asm volatile("");
    acc_re_2 = svmlslt_lane_s32(acc_re_2, in_8, c, 1);
    acc_im_2 = svmlalt_lane_s32(acc_im_2, in_8, c, 0);
    asm volatile("");
    acc_re_3 = svmlslt_lane_s32(acc_re_3, in_12, c, 1);
    acc_im_3 = svmlalt_lane_s32(acc_im_3, in_12, c, 0);

    asm volatile("");

    svint16_t in_2 = svld1_s16(pg, (const int16_t *)&input[t + 2]);
    acc_re_0 = svmlalb_lane_s32(acc_re_0, in_1, c, 2);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, in_1, c, 3);
    asm volatile("");
    svint16_t in_6 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 2], 1);
    acc_re_1 = svmlalb_lane_s32(acc_re_1, in_5, c, 2);
    acc_im_1 = svmlalb_lane_s32(acc_im_1, in_5, c, 3);
    asm volatile("");
    svint16_t in_10 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 2], 2);
    acc_re_2 = svmlalb_lane_s32(acc_re_2, in_9, c, 2);
    acc_im_2 = svmlalb_lane_s32(acc_im_2, in_9, c, 3);
    asm volatile("");
    svint16_t in_14 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 2], 3);
    acc_re_3 = svmlalb_lane_s32(acc_re_3, in_13, c, 2);
    acc_im_3 = svmlalb_lane_s32(acc_im_3, in_13, c, 3);

    asm volatile("");

    acc_re_0 = svmlslt_lane_s32(acc_re_0, in_1, c, 3);
    acc_im_0 = svmlalt_lane_s32(acc_im_0, in_1, c, 2);
    asm volatile("");
    acc_re_1 = svmlslt_lane_s32(acc_re_1, in_5, c, 3);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, in_5, c, 2);
    asm volatile("");
    acc_re_2 = svmlslt_lane_s32(acc_re_2, in_9, c, 3);
    acc_im_2 = svmlalt_lane_s32(acc_im_2, in_9, c, 2);
    asm volatile("");
    acc_re_3 = svmlslt_lane_s32(acc_re_3, in_13, c, 3);
    acc_im_3 = svmlalt_lane_s32(acc_im_3, in_13, c, 2);

    asm volatile("");

    svint16_t in_3 = svld1_s16(pg, (const int16_t *)&input[t + 3]);
    acc_re_0 = svmlalb_lane_s32(acc_re_0, in_2, c, 4);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, in_2, c, 5);
    asm volatile("");
    svint16_t in_7 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 3], 1);
    acc_re_1 = svmlalb_lane_s32(acc_re_1, in_6, c, 4);
    acc_im_1 = svmlalb_lane_s32(acc_im_1, in_6, c, 5);
    asm volatile("");
    svint16_t in_11 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 3], 2);
    acc_re_2 = svmlalb_lane_s32(acc_re_2, in_10, c, 4);
    acc_im_2 = svmlalb_lane_s32(acc_im_2, in_10, c, 5);
    asm volatile("");
    svint16_t in_15 = svld1_vnum_s16(pg, (const int16_t *)&input[t + 3], 3);
    acc_re_3 = svmlalb_lane_s32(acc_re_3, in_14, c, 4);
    acc_im_3 = svmlalb_lane_s32(acc_im_3, in_14, c, 5);

    asm volatile("");

    acc_re_0 = svmlslt_lane_s32(acc_re_0, in_2, c, 5);
    acc_im_0 = svmlalt_lane_s32(acc_im_0, in_2, c, 4);
    asm volatile("");
    acc_re_1 = svmlslt_lane_s32(acc_re_1, in_6, c, 5);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, in_6, c, 4);
    asm volatile("");
    acc_re_2 = svmlslt_lane_s32(acc_re_2, in_10, c, 5);
    acc_im_2 = svmlalt_lane_s32(acc_im_2, in_10, c, 4);
    asm volatile("");
    acc_re_3 = svmlslt_lane_s32(acc_re_3, in_14, c, 5);
    acc_im_3 = svmlalt_lane_s32(acc_im_3, in_14, c, 4);

    asm volatile("");

    acc_re_0 = svmlalb_lane_s32(acc_re_0, in_3, c, 6);
    acc_im_0 = svmlalb_lane_s32(acc_im_0, in_3, c, 7);
    asm volatile("");
    acc_re_1 = svmlalb_lane_s32(acc_re_1, in_7, c, 6);
    acc_im_1 = svmlalb_lane_s32(acc_im_1, in_7, c, 7);
    asm volatile("");
    acc_re_2 = svmlalb_lane_s32(acc_re_2, in_11, c, 6);
    acc_im_2 = svmlalb_lane_s32(acc_im_2, in_11, c, 7);
    asm volatile("");
    acc_re_3 = svmlalb_lane_s32(acc_re_3, in_15, c, 6);
    acc_im_3 = svmlalb_lane_s32(acc_im_3, in_15, c, 7);
    asm volatile("");

    acc_re_0 = svmlslt_lane_s32(acc_re_0, in_3, c, 7);
    acc_im_0 = svmlalt_lane_s32(acc_im_0, in_3, c, 6);
    asm volatile("");
    acc_re_1 = svmlslt_lane_s32(acc_re_1, in_7, c, 7);
    acc_im_1 = svmlalt_lane_s32(acc_im_1, in_7, c, 6);
    asm volatile("");
    acc_re_2 = svmlslt_lane_s32(acc_re_2, in_11, c, 7);
    acc_im_2 = svmlalt_lane_s32(acc_im_2, in_11, c, 6);
    asm volatile("");
    acc_re_3 = svmlslt_lane_s32(acc_re_3, in_15, c, 7);
    acc_im_3 = svmlalt_lane_s32(acc_im_3, in_15, c, 6);
  }

  // Tail loop for taps % 4 != 0
  for (; __builtin_expect(!!(t < taps), 0); t++) {
    svint16_t in_0 = svld1_s16(pg, (const int16_t *)&input[t]);
    svint16_t in_1 = svld1_vnum_s16(pg, (const int16_t *)&input[t], 1);
    svint16_t in_2 = svld1_vnum_s16(pg, (const int16_t *)&input[t], 2);
    svint16_t in_3 = svld1_vnum_s16(pg, (const int16_t *)&input[t], 3);
    svint16_t c =
        svreinterpret_s16_u32(svdup_n_u32(((const uint32_t *)coeffs)[t]));
    svint16_t c_rev = svrev_s16(c);

    acc_re_0 = acc_fir_re(acc_re_0, in_0, c);
    acc_im_0 = acc_fir_im(acc_im_0, in_0, c_rev);
    acc_re_1 = acc_fir_re(acc_re_1, in_1, c);
    acc_im_1 = acc_fir_im(acc_im_1, in_1, c_rev);
    acc_re_2 = acc_fir_re(acc_re_2, in_2, c);
    acc_im_2 = acc_fir_im(acc_im_2, in_2, c_rev);
    acc_re_3 = acc_fir_re(acc_re_3, in_3, c);
    acc_im_3 = acc_fir_im(acc_im_3, in_3, c_rev);
  }

  svint16_t y_0 = shr16_interleave(acc_re_0, acc_im_0);
  svst1_s16(pg, (int16_t *)output, y_0);

  svint16_t y_1 = shr16_interleave(acc_re_1, acc_im_1);
  svst1_vnum_s16(pg, (int16_t *)output, 1, y_1);

  svint16_t y_2 = shr16_interleave(acc_re_2, acc_im_2);
  svst1_vnum_s16(pg, (int16_t *)output, 2, y_2);

  svint16_t y_3 = shr16_interleave(acc_re_3, acc_im_3);
  svst1_vnum_s16(pg, (int16_t *)output, 3, y_3);
}
#endif // ARMRAL_ARCH_SVE >= 2

armral_status armral_fir_filter_cs16(
    uint32_t size, uint32_t taps, const armral_cmplx_int16_t *restrict input,
    const armral_cmplx_int16_t *restrict coeffs, armral_cmplx_int16_t *output) {
#if ARMRAL_ARCH_SVE >= 2
  svbool_t ptrue = svptrue_b16();
  uint32_t i = 0;
  for (; i + svcntw() * 4 <= size; i += svcntw() * 4) {
    sv_fir_4_block(input + i, coeffs, output + i, ptrue, taps);
  }
  for (; i + svcntw() * 2 <= size; i += svcntw() * 2) {
    sv_fir_2_block(input + i, coeffs, output + i, ptrue, taps);
  }
  for (; i + svcntw() <= size; i += svcntw()) {
    sv_fir_block(input + i, coeffs, output + i, ptrue, taps);
  }
  // Input array is not long enough to load svcntw() elements any more, so
  // process the remaining elements with predication
  if (i < size) {
    svbool_t pg = svwhilelt_b16(i, size);
    sv_fir_block(input + i, coeffs, output + i, pg, taps);
  }
#else
  /* Compute number of blocks of 8 input data and a possible tail */
  uint32_t blck_data = size >> 3U;

  /* Compute number of blocks of 8 taps and a possible tail */
  uint32_t total_taps = taps >> 3U;
  uint32_t tailtaps = taps & 0x7;
  uint32_t blck_taps;

  int16x8x2_t s;
  int16x8x2_t h;
  int16x8x2_t old_s;
  int16x8x2_t shifted_s;
  const int16_t *fir_p_input;
  const int16_t *p_input = (const int16_t *)input;
  int16_t *p_output = (int16_t *)output;
  const int16_t *p_coeff;

  int16x8_t zero_16x8;
  int32x4x2_t acc_low;
  int32x4x2_t acc_high;
  int16x4x2_t data_filtered_low;
  int16x4x2_t data_filtered_high;
  zero_16x8 = vdupq_n_s16(0);

  uint16x8_t fir_mask[8];

  fir_mask[0][0] = 0x0000;
  fir_mask[0][1] = 0x0000;
  fir_mask[0][2] = 0x0000;
  fir_mask[0][3] = 0x0000;
  fir_mask[0][4] = 0x0000;
  fir_mask[0][5] = 0x0000;
  fir_mask[0][6] = 0x0000;
  fir_mask[0][7] = 0x0000;

  fir_mask[1][0] = 0xFFFF;
  fir_mask[1][1] = 0x0000;
  fir_mask[1][2] = 0x0000;
  fir_mask[1][3] = 0x0000;
  fir_mask[1][4] = 0x0000;
  fir_mask[1][5] = 0x0000;
  fir_mask[1][6] = 0x0000;
  fir_mask[1][7] = 0x0000;

  fir_mask[2][0] = 0xFFFF;
  fir_mask[2][1] = 0xFFFF;
  fir_mask[2][2] = 0x0000;
  fir_mask[2][3] = 0x0000;
  fir_mask[2][4] = 0x0000;
  fir_mask[2][5] = 0x0000;
  fir_mask[2][6] = 0x0000;
  fir_mask[2][7] = 0x0000;

  fir_mask[3][0] = 0xFFFF;
  fir_mask[3][1] = 0xFFFF;
  fir_mask[3][2] = 0xFFFF;
  fir_mask[3][3] = 0x0000;
  fir_mask[3][4] = 0x0000;
  fir_mask[3][5] = 0x0000;
  fir_mask[3][6] = 0x0000;
  fir_mask[3][7] = 0x0000;

  fir_mask[4][0] = 0xFFFF;
  fir_mask[4][1] = 0xFFFF;
  fir_mask[4][2] = 0xFFFF;
  fir_mask[4][3] = 0xFFFF;
  fir_mask[4][4] = 0x0000;
  fir_mask[4][5] = 0x0000;
  fir_mask[4][6] = 0x0000;
  fir_mask[4][7] = 0x0000;

  fir_mask[5][0] = 0xFFFF;
  fir_mask[5][1] = 0xFFFF;
  fir_mask[5][2] = 0xFFFF;
  fir_mask[5][3] = 0xFFFF;
  fir_mask[5][4] = 0xFFFF;
  fir_mask[5][5] = 0x0000;
  fir_mask[5][6] = 0x0000;
  fir_mask[5][7] = 0x0000;

  fir_mask[6][0] = 0xFFFF;
  fir_mask[6][1] = 0xFFFF;
  fir_mask[6][2] = 0xFFFF;
  fir_mask[6][3] = 0xFFFF;
  fir_mask[6][4] = 0xFFFF;
  fir_mask[6][5] = 0xFFFF;
  fir_mask[6][6] = 0x0000;
  fir_mask[6][7] = 0x0000;

  fir_mask[7][0] = 0xFFFF;
  fir_mask[7][1] = 0xFFFF;
  fir_mask[7][2] = 0xFFFF;
  fir_mask[7][3] = 0xFFFF;
  fir_mask[7][4] = 0xFFFF;
  fir_mask[7][5] = 0xFFFF;
  fir_mask[7][6] = 0xFFFF;
  fir_mask[7][7] = 0x0000;

  /* Input data loop unrolling */
  while (blck_data > 0U) {
    /* At every iteration of the outer loop the pointer of the coefficient must
     * be reset */
    p_coeff = (const int16_t *)coeffs;

    /* Reset the accumulators */
    acc_low.val[0] = vdupq_n_s32(0);
    acc_low.val[1] = vdupq_n_s32(0);

    acc_high.val[0] = vdupq_n_s32(0);
    acc_high.val[1] = vdupq_n_s32(0);

    /* Load data */
    s = vld2q_s16(p_input);
    p_input += 16; /* 8 samples, Re + Im */
    fir_p_input = p_input;

    blck_taps = total_taps;
    while (blck_taps > 0U) {
      h = vld2q_s16(p_coeff);
      p_coeff += 16; /* 8 coefficients, Re + Im */
      int16x8_t h_re = h.val[0];
      int16x8_t h_im = h.val[1];

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] = vmlal_low_n_s16(acc_low.val[0], s.val[0], h_re[0]);
      acc_low.val[0] = vmlsl_low_n_s16(acc_low.val[0], s.val[1], h_im[0]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] = vmlal_low_n_s16(acc_low.val[1], s.val[0], h_im[0]);
      acc_low.val[1] = vmlal_low_n_s16(acc_low.val[1], s.val[1], h_re[0]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], s.val[0], h_re, 0);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], s.val[1], h_im, 0);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], s.val[0], h_im, 0);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], s.val[1], h_re, 0);

      /* Save the current samples and load new ones */
      old_s = s;
      s = vld2q_s16(fir_p_input);
      fir_p_input += 16; /* 8 samples, Re + Im */

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 1);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 1);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[1]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[1]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[1]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[1]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 1);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 1);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 1);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 1);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 2);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 2);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[2]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[2]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[2]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[2]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 2);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 2);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 2);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 3);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 3);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[3]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[3]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[3]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[3]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 3);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 3);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 3);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 3);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 4);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 4);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[4]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[4]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[4]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[4]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 4);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 4);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 4);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 4);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 5);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 5);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[5]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[5]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[5]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[5]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 5);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 5);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 5);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 5);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 6);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 6);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[6]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[6]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[6]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[6]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 6);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 6);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 6);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 6);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 7);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 7);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[7]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[7]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[7]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[7]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 7);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 7);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 7);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 7);

      blck_taps--;
    }

    /* FIR tail management */
    if (tailtaps) {
      h = vld2q_s16(p_coeff);
      h.val[0] = vbslq_s16(fir_mask[tailtaps], h.val[0], zero_16x8);
      h.val[1] = vbslq_s16(fir_mask[tailtaps], h.val[1], zero_16x8);

      int16x8_t h_re = h.val[0];
      int16x8_t h_im = h.val[1];

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] = vmlal_low_n_s16(acc_low.val[0], s.val[0], h_re[0]);
      acc_low.val[0] = vmlsl_low_n_s16(acc_low.val[0], s.val[1], h_im[0]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] = vmlal_low_n_s16(acc_low.val[1], s.val[0], h_im[0]);
      acc_low.val[1] = vmlal_low_n_s16(acc_low.val[1], s.val[1], h_re[0]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], s.val[0], h_re, 0);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], s.val[1], h_im, 0);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], s.val[0], h_im, 0);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], s.val[1], h_re, 0);

      /* Save the current samples and load new ones */
      old_s = s;
      s = vld2q_s16(fir_p_input); /* 8 samples, Re + Im */

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 1);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 1);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[1]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[1]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[1]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[1]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 1);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 1);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 1);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 1);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 2);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 2);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[2]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[2]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[2]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[2]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 2);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 2);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 2);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 2);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 3);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 3);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[3]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[3]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[3]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[3]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 3);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 3);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 3);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 3);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 4);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 4);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[4]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[4]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[4]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[4]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 4);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 4);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 4);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 4);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 5);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 5);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[5]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[5]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[5]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[5]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 5);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 5);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 5);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 5);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 6);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 6);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[6]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[6]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[6]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[6]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 6);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 6);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 6);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 6);

      /* Extract Re & Im to shift */
      shifted_s.val[0] = vextq_s16(old_s.val[0], s.val[0], 7);
      shifted_s.val[1] = vextq_s16(old_s.val[1], s.val[1], 7);

      /* Compute y.re for lower int16x4_t */
      acc_low.val[0] =
          vmlal_low_n_s16(acc_low.val[0], shifted_s.val[0], h_re[7]);
      acc_low.val[0] =
          vmlsl_low_n_s16(acc_low.val[0], shifted_s.val[1], h_im[7]);
      /* Compute y.im for lower int16x4_t */
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[0], h_im[7]);
      acc_low.val[1] =
          vmlal_low_n_s16(acc_low.val[1], shifted_s.val[1], h_re[7]);

      /* Compute y.re for higher int16x4_t */
      acc_high.val[0] =
          vmlal_high_laneq_s16(acc_high.val[0], shifted_s.val[0], h_re, 7);
      acc_high.val[0] =
          vmlsl_high_laneq_s16(acc_high.val[0], shifted_s.val[1], h_im, 7);
      /* Compute y.im for higher int16x4_t */
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[0], h_im, 7);
      acc_high.val[1] =
          vmlal_high_laneq_s16(acc_high.val[1], shifted_s.val[1], h_re, 7);
    }

    data_filtered_low.val[0] = vqshrn_n_s32(acc_low.val[0], 16);
    data_filtered_low.val[1] = vqshrn_n_s32(acc_low.val[1], 16);

    data_filtered_high.val[0] = vqshrn_n_s32(acc_high.val[0], 16);
    data_filtered_high.val[1] = vqshrn_n_s32(acc_high.val[1], 16);

    /* Store the results */
    vst2_s16(p_output, data_filtered_low);
    p_output += 8;
    vst2_s16(p_output, data_filtered_high);
    p_output += 8;

    blck_data--;
  }
#endif
  return ARMRAL_SUCCESS;
}
