/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

template<unsigned int N>
static inline void xor_u8(const uint8_t *__restrict &input,
                          const uint8_t *__restrict &sequence, uint8_t *&out) {
  static_assert(N == 16 || N == 8);
  if constexpr (N == 16) {
    vst1q_u8(out, veorq_u8(vld1q_u8(input), vld1q_u8(sequence)));
  } else {
    vst1_u8(out, veor_u8(vld1_u8(input), vld1_u8(sequence)));
  }
  input += N;
  sequence += N;
  out += N;
}

static inline void xor_scalar(const uint8_t *__restrict input,
                              const uint8_t *__restrict sequence, uint8_t *out,
                              uint32_t len) {
  for (uint32_t i = 0; i < len; ++i) {
    out[i] = input[i] ^ sequence[i];
  }
}

armral_status armral_scramble_code_block(const uint8_t *__restrict src,
                                         const uint8_t *__restrict seq,
                                         uint32_t num_bits, uint8_t *dst) {
  uint32_t bytes = (num_bits + 7) >> 3;

  // No vectorization for less than 8 bytes.
  if (bytes < 8) {
    xor_scalar(src, seq, dst, bytes);
    return ARMRAL_SUCCESS;
  }

  uint32_t rem_bytes = bytes;

  // Use unrolled vectorized loop if there are at least 32 bytes.
  if (bytes > 31) {
    uint32_t len = rem_bytes >> 5;
    while (len > 0) {
      xor_u8<16>(src, seq, dst);
      xor_u8<16>(src, seq, dst);
      len--;
    }
    rem_bytes = rem_bytes % 32;
  }
  // Process 16 of the remaining bytes.
  if (rem_bytes > 15) {
    xor_u8<16>(src, seq, dst);
    rem_bytes = rem_bytes % 16;
  }
  // Process 8 of the remaining bytes.
  if (rem_bytes > 7) {
    xor_u8<8>(src, seq, dst);
    rem_bytes = rem_bytes % 8;
  }
  // Process any leftover bytes.
  if (rem_bytes != 0) {
    xor_scalar(src, seq, dst, rem_bytes);
  }

  return ARMRAL_SUCCESS;
}
