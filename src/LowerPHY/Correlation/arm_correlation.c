/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"

#include <math.h>

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

/*
 * Calculation of the square root as a positive Q15 number
 *
 * @param[in] in  a fixed-point number in Q33.30 format
 * @return    square root in Q15 format
 */
static inline int16_t __attribute__((always_inline)) sqrt_q15(int64_t in) {
  double q33_30_to_fp = 1.0 / (32768.0 * 32768.0);
  double fp_to_q15 = 32768.0;
  return sqrt(in * q33_30_to_fp) * fp_to_q15 + 0.5;
}

#if !(ARMRAL_ARCH_SVE >= 2)
static inline void __attribute__((always_inline))
armral_cmplx_vecdot_i16_n8_conj_32bit(int16x8_t vec_a_re, int16x8_t vec_a_im,
                                      int16x8_t vec_b_re, int16x8_t vec_b_im,
                                      int64x2_t *restrict p_src_c_re,
                                      int64x2_t *restrict p_src_c_im) {
  // do a dot product of exactly 8 pairs of elements (hence n8),
  // b is conjugated, so negate the imaginary component.

  int32x4_t acc_r;
  int32x4_t acc_i;

  /* Re{C} = Re{A}*Re{B} - Im{A}*(-Im{B}) */
  acc_r = vqdmull_high_s16(vec_a_re, vec_b_re);
  acc_r = vqdmlal_low_s16(acc_r, vec_a_re, vec_b_re);
  acc_r = vqdmlal_high_s16(acc_r, vec_a_im, vec_b_im);
  acc_r = vqdmlal_low_s16(acc_r, vec_a_im, vec_b_im);

  /* Im{C} = Re{A}*(-Im{B}) + Im{A}*Re{B} */
  acc_i = vqdmull_high_s16(vec_a_im, vec_b_re);
  acc_i = vqdmlal_low_s16(acc_i, vec_a_im, vec_b_re);
  acc_i = vqdmlsl_high_s16(acc_i, vec_a_re, vec_b_im);
  acc_i = vqdmlsl_low_s16(acc_i, vec_a_re, vec_b_im);

  *p_src_c_re = vpadalq_s32(*p_src_c_re, acc_r);
  *p_src_c_im = vpadalq_s32(*p_src_c_im, acc_i);
}
#endif

armral_status
armral_corr_coeff_i16(uint32_t n, const armral_cmplx_int16_t *restrict p_src_a,
                      const armral_cmplx_int16_t *restrict p_src_b,
                      armral_cmplx_int16_t *c) {
  /* We have to cast uint32_t n to an int32_t, but if it is too large to */
  /* be represented by the new datatype we return an argument error.     */
  if (n > INT32_MAX) {
    return ARMRAL_ARGUMENT_ERROR;
  }

  const int16_t *p_ini_a = (const int16_t *)p_src_a;
  const int16_t *p_ini_b = (const int16_t *)p_src_b;

#if ARMRAL_ARCH_SVE >= 2
  svbool_t ptrue = svptrue_b8();
  svint32_t xmod_quad = svdup_n_s32(0);
  svint32_t ymod_quad = svdup_n_s32(0);
  svint32_t xavg_re_vec = svdup_n_s32(0);
  svint32_t xavg_im_vec = svdup_n_s32(0);
  svint32_t yavg_re_vec = svdup_n_s32(0);
  svint32_t yavg_im_vec = svdup_n_s32(0);
  svint64_t xyconj_re_vec = svdup_n_s64(0);
  svint64_t xyconj_im_vec = svdup_n_s64(0);

  /* Compute 8 outputs at a time */
  for (uint32_t i = 0; i < n; i += svcnth()) {
    svbool_t pg = svwhilelt_b16(i, n);
    /* Load samples */
    svint16x2_t x = svld2_s16(pg, p_ini_a);
    svint16x2_t y = svld2_s16(pg, p_ini_b);
    p_ini_a += svcntb();
    p_ini_b += svcntb();

    svint16_t x_re = svget2_s16(x, 0);
    svint16_t x_im = svget2_s16(x, 1);
    svint16_t y_re = svget2_s16(y, 0);
    svint16_t y_im = svget2_s16(y, 1);

    /*Sum all x/y members*/
    xavg_re_vec = svadalp_s32_m(pg, xavg_re_vec, x_re); /*16.15 format*/
    xavg_im_vec = svadalp_s32_m(pg, xavg_im_vec, x_im); /*16.15 format*/
    yavg_re_vec = svadalp_s32_m(pg, yavg_re_vec, y_re); /*16.15 format*/
    yavg_im_vec = svadalp_s32_m(pg, yavg_im_vec, y_im); /*16.15 format*/

    /* Re{C} = Re{A}*Re{B} - Im{A}*(-Im{B}) */
    svint32_t acc_r;
    acc_r = svqdmullb_s32(x_re, y_re);
    acc_r = svqdmlalt_s32(acc_r, x_re, y_re);
    acc_r = svqdmlalb_s32(acc_r, x_im, y_im);
    acc_r = svqdmlalt_s32(acc_r, x_im, y_im);

    /* Im{C} = Re{A}*(-Im{B}) + Im{A}*Re{B} */
    svint32_t acc_i;
    acc_i = svqdmullb_s32(x_im, y_re);
    acc_i = svqdmlalt_s32(acc_i, x_im, y_re);
    acc_i = svqdmlslb_s32(acc_i, x_re, y_im);
    acc_i = svqdmlslt_s32(acc_i, x_re, y_im);

    xyconj_re_vec = svadalp_s64_m(pg, xyconj_re_vec, acc_r);
    xyconj_im_vec = svadalp_s64_m(pg, xyconj_im_vec, acc_i);

    /*Re(x)^2 + Im(x)^2 */
    xmod_quad = svqdmlalb_s32(xmod_quad, x_re, x_re); /*0.31*/
    xmod_quad = svqdmlalt_s32(xmod_quad, x_re, x_re); /*0.31*/
    xmod_quad = svqdmlalb_s32(xmod_quad, x_im, x_im); /*0.31*/
    xmod_quad = svqdmlalt_s32(xmod_quad, x_im, x_im); /*0.31*/

    /*Re(y)^2 + Im(y)^2 */
    ymod_quad = svqdmlalb_s32(ymod_quad, y_re, y_re); /*0.31*/
    ymod_quad = svqdmlalt_s32(ymod_quad, y_re, y_re); /*0.31*/
    ymod_quad = svqdmlalb_s32(ymod_quad, y_im, y_im); /*0.31*/
    ymod_quad = svqdmlalt_s32(ymod_quad, y_im, y_im); /*0.31*/
  }

  int32_t xavg_re = svaddv_s32(ptrue, xavg_re_vec);
  int32_t xavg_im = svaddv_s32(ptrue, xavg_im_vec);
  int32_t yavg_re = svaddv_s32(ptrue, yavg_re_vec);
  int32_t yavg_im = svaddv_s32(ptrue, yavg_im_vec);
  int64_t xyconj_re = svaddv_s64(ptrue, xyconj_re_vec);
  int64_t xyconj_im = svaddv_s64(ptrue, xyconj_im_vec);

  xyconj_re >>= 1;
  xyconj_im >>= 1;

  /* 0.31 -> 33.30 format */
  int64_t xsum_mod = svaddv_s64(ptrue, svaddlbt_s64(xmod_quad, xmod_quad)) >> 1;
  int64_t ysum_mod = svaddv_s64(ptrue, svaddlbt_s64(ymod_quad, ymod_quad)) >> 1;
#else
  int32x4_t xmod_quad = vdupq_n_s32(0);
  int32x4_t ymod_quad = vdupq_n_s32(0);
  int32x4_t xavg_re_vec = vdupq_n_s32(0);
  int32x4_t xavg_im_vec = vdupq_n_s32(0);
  int32x4_t yavg_re_vec = vdupq_n_s32(0);
  int32x4_t yavg_im_vec = vdupq_n_s32(0);
  int64x2_t xyconj_re_vec = vdupq_n_s64(0);
  int64x2_t xyconj_im_vec = vdupq_n_s64(0);

  /* Compute 8 outputs at a time */
  uint32_t blk_cnt = n >> 3U;
  while (blk_cnt > 0U) {
    /* Load samples */
    int16x8x2_t x = vld2q_s16(p_ini_a);
    int16x8x2_t y = vld2q_s16(p_ini_b);
    p_ini_a = p_ini_a + 16;
    p_ini_b = p_ini_b + 16;

    /*Sum all x/y members*/
    xavg_re_vec = vpadalq_s16(xavg_re_vec, x.val[0]); /*16.15 format*/
    xavg_im_vec = vpadalq_s16(xavg_im_vec, x.val[1]); /*16.15 format*/
    yavg_re_vec = vpadalq_s16(yavg_re_vec, y.val[0]); /*16.15 format*/
    yavg_im_vec = vpadalq_s16(yavg_im_vec, y.val[1]); /*16.15 format*/

    /* SUM(xy*), result in Q15 format */
    armral_cmplx_vecdot_i16_n8_conj_32bit(
        x.val[0], x.val[1], y.val[0], y.val[1], &xyconj_re_vec, &xyconj_im_vec);

    /*Re(x)^2 + Im(x)^2 */
    xmod_quad = vqdmlal_high_s16(xmod_quad, x.val[0], x.val[0]); /*0.31 format*/
    xmod_quad = vqdmlal_low_s16(xmod_quad, x.val[0], x.val[0]);  /*0.31 format*/
    xmod_quad = vqdmlal_high_s16(xmod_quad, x.val[1], x.val[1]); /*0.31 format*/
    xmod_quad = vqdmlal_low_s16(xmod_quad, x.val[1], x.val[1]);  /*0.31 format*/

    /*Re(y)^2 + Im(y)^2 */
    ymod_quad = vqdmlal_high_s16(ymod_quad, y.val[0], y.val[0]); /*0.31 format*/
    ymod_quad = vqdmlal_low_s16(ymod_quad, y.val[0], y.val[0]);  /*0.31 format*/
    ymod_quad = vqdmlal_high_s16(ymod_quad, y.val[1], y.val[1]); /*0.31 format*/
    ymod_quad = vqdmlal_low_s16(ymod_quad, y.val[1], y.val[1]);  /*0.31 format*/
    blk_cnt--;
  }

  int32_t xavg_re = vaddvq_s32(xavg_re_vec);
  int32_t xavg_im = vaddvq_s32(xavg_im_vec);
  int32_t yavg_re = vaddvq_s32(yavg_re_vec);
  int32_t yavg_im = vaddvq_s32(yavg_im_vec);
  int64_t xyconj_re = vaddvq_s64(xyconj_re_vec);
  int64_t xyconj_im = vaddvq_s64(xyconj_im_vec);

  xyconj_re >>= 1;
  xyconj_im >>= 1;

  int64_t xsum_mod = vaddlvq_s32(xmod_quad) >> 1; /* 0.31 -> 33.30 format */
  int64_t ysum_mod = vaddlvq_s32(ymod_quad) >> 1; /* 0.31 -> 33.30 format */

  /* Tail */
  blk_cnt = n & 0x7;

  while (blk_cnt > 0U) {
    int16_t xi_re = p_ini_a[0];
    int16_t xi_im = p_ini_a[1];
    int16_t yi_re = p_ini_b[0];
    int16_t yi_im = p_ini_b[1];
    p_ini_a += 2;
    p_ini_b += 2;

    xavg_re += xi_re;
    xavg_im += xi_im;
    yavg_re += yi_re;
    yavg_im += yi_im;

    xyconj_re = vqaddd_s64(xyconj_re, xi_re * yi_re);
    xyconj_im = vqsubd_s64(xyconj_im, xi_re * yi_im);
    xyconj_re = vqaddd_s64(xyconj_re, xi_im * yi_im);
    xyconj_im = vqaddd_s64(xyconj_im, xi_im * yi_re);

    xsum_mod += xi_re * xi_re;
    xsum_mod += xi_im * xi_im;
    ysum_mod += yi_re * yi_re;
    ysum_mod += yi_im * yi_im;

    blk_cnt--;
  }
#endif

  xavg_re = xavg_re / (int32_t)n; /*16.15 format*/
  xavg_im = xavg_im / (int32_t)n;
  yavg_re = yavg_re / (int32_t)n;
  yavg_im = yavg_im / (int32_t)n;

  /* (n*xavg*yavg), 33.30 format */
  int64_t temp_re =
      (int64_t)n * (int64_t)((xavg_re * yavg_re) - (xavg_im * yavg_im));
  int64_t temp_im =
      (int64_t)n * (int64_t)((xavg_im * yavg_re) + (xavg_re * yavg_im));

  int64_t num_re = xyconj_re - temp_re; /* 33.30 format*/
  int64_t num_im = xyconj_im - temp_im;

  int32_t xavg_mod;
  int32_t yavg_mod;
  xavg_mod = (xavg_re * xavg_re) + (xavg_im * xavg_im); /*1.30 format*/
  yavg_mod = (yavg_re * yavg_re) + (yavg_im * yavg_im); /*1.30 format*/

  temp_re = xsum_mod - (int64_t)n * xavg_mod; /*33.30 format*/
  temp_im = ysum_mod - (int64_t)n * yavg_mod; /*33.30 format*/

  temp_re = temp_re >> 15;
  temp_im = temp_im >> 15;
  int64_t temp = temp_re * temp_im; /* 48.15 * 48.15 -> 33.30 format*/
  int16_t den = sqrt_q15(temp);     /* 33.30 -> 0.15 format*/

  (*c).re = sat(num_re / den);
  (*c).im = sat(num_im / den);
  return ARMRAL_SUCCESS;
}
