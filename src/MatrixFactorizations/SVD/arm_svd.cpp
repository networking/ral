/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "armral.h"
#include "matrix_view.hpp"
#include "utils/allocators.hpp"

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstring>
#include <optional>

namespace {

// Compute dot product c = a . conj(b) with a, b and c complex vectors
// of length n.
inline void cmplx_vecdot_conj_f32(uint32_t n, const armral_cmplx_f32_t *p_src_a,
                                  const armral_cmplx_f32_t *p_src_b,
                                  armral_cmplx_f32_t *p_src_c) {

#ifdef ARMRAL_ARCH_SVE
  uint32_t num_lanes = svcntd();
  svbool_t ptrue = svptrue_b32();
  svfloat32_t acc0 = svdup_n_f32(0);
  svfloat32_t acc1 = svdup_n_f32(0);

  uint32_t i = 0;
  for (; (i + 2) * num_lanes <= n; i += 2) {
    svbool_t pg = svptrue_b32();
    svfloat32_t vec_a0 = svld1_vnum_f32(pg, (const float32_t *)p_src_a, i);
    svfloat32_t vec_b0 = svld1_vnum_f32(pg, (const float32_t *)p_src_b, i);
    svfloat32_t vec_a1 = svld1_vnum_f32(pg, (const float32_t *)p_src_a, i + 1);
    svfloat32_t vec_b1 = svld1_vnum_f32(pg, (const float32_t *)p_src_b, i + 1);

    acc0 = svcmla_f32_m(pg, acc0, vec_b0, vec_a0, 0);
    acc0 = svcmla_f32_m(pg, acc0, vec_b0, vec_a0, 270);
    acc1 = svcmla_f32_m(pg, acc1, vec_b1, vec_a1, 0);
    acc1 = svcmla_f32_m(pg, acc1, vec_b1, vec_a1, 270);
  }

  for (; i * num_lanes < n; ++i) {
    svbool_t pg = svwhilelt_b32(2 * i * num_lanes, 2 * n);
    svfloat32_t vec_a = svld1_vnum_f32(pg, (const float32_t *)p_src_a, i);
    svfloat32_t vec_b = svld1_vnum_f32(pg, (const float32_t *)p_src_b, i);

    acc0 = svcmla_f32_m(pg, acc0, vec_b, vec_a, 0);
    acc0 = svcmla_f32_m(pg, acc0, vec_b, vec_a, 270);
  }

  p_src_c->re = svaddv_f32(ptrue, svtrn1_f32(acc0, acc1));
  p_src_c->im = svaddv_f32(ptrue, svtrn2_f32(acc0, acc1));

#else
  float32_t real_sum = 0;
  float32_t imag_sum = 0;

  float32x4_t acc_r = vdupq_n_f32(0.0);
  float32x4_t acc_i = vdupq_n_f32(0.0);

  // Loop unrolling: Compute 8 outputs at a time
  uint32_t blk_cnt = n >> 3U;

  if (blk_cnt > 0U) {
    while (blk_cnt > 0U) {
      // C = (A[0] + j * A[1]) * (B[0] - j * B[1]) + ...
      // Calculate dot product and then store the result in a temporary buffer.

      float32x4x2_t vec1 = vld2q_f32((const float32_t *)p_src_a);
      float32x4x2_t vec2 = vld2q_f32((const float32_t *)p_src_b);

      // Increment pointers
      p_src_a += 4;
      p_src_b += 4;

      // Re{C} = Re{A}*Re{B} + Im{A}*Im{B}
      acc_r = vfmaq_f32(acc_r, vec1.val[0], vec2.val[0]);
      acc_r = vfmaq_f32(acc_r, vec1.val[1], vec2.val[1]);
      // Im{C} = - Re{A}*Im{B} + Im{A}*Re{B}
      acc_i = vfmaq_f32(acc_i, vec1.val[1], vec2.val[0]);
      acc_i = vfmsq_f32(acc_i, vec1.val[0], vec2.val[1]);

      float32x4x2_t vec3 = vld2q_f32((const float32_t *)p_src_a);
      float32x4x2_t vec4 = vld2q_f32((const float32_t *)p_src_b);

      // Increment pointers
      p_src_a += 4;
      p_src_b += 4;

      // Re{C} = Re{A}*Re{B} + Im{A}*Im{B}
      acc_r = vfmaq_f32(acc_r, vec3.val[0], vec4.val[0]);
      acc_r = vfmaq_f32(acc_r, vec3.val[1], vec4.val[1]);

      // Im{C} = - Re{A}*Im{B} + Im{A}*Re{B}
      acc_i = vfmaq_f32(acc_i, vec3.val[1], vec4.val[0]);
      acc_i = vfmsq_f32(acc_i, vec3.val[0], vec4.val[1]);

      // Decrement the loop counter
      blk_cnt--;
    }

    // This is marginally faster than using a single float32x4_t accumulator
    float32x2_t accum;
    accum = vpadd_f32(vget_low_f32(acc_r), vget_high_f32(acc_r));
    real_sum = accum[0] + accum[1];
    accum = vpadd_f32(vget_low_f32(acc_i), vget_high_f32(acc_i));
    imag_sum = accum[0] + accum[1];
  }

  // Tail
  blk_cnt = n & 0x7;

  while (blk_cnt > 0U) {
    float32_t re_a = p_src_a->re;
    float32_t im_a = p_src_a->im;
    float32_t re_b = p_src_b->re;
    float32_t im_b = p_src_b->im;

    real_sum += re_a * re_b;
    real_sum += im_a * im_b;
    imag_sum += im_a * re_b;
    imag_sum -= im_b * re_a;

    p_src_a++;
    p_src_b++;

    // Decrement loop counter
    blk_cnt--;
  }

  // Store real and imaginary result in destination buffer.
  p_src_c->re = real_sum;
  p_src_c->im = imag_sum;

#endif
}

// Compute c -= a * b with a and c complex vectors of length n and b a complex
// constant value.
inline void cmplx_axmy_f32(uint32_t n, const armral_cmplx_f32_t *p_src_a,
                           const armral_cmplx_f32_t *p_src_b,
                           armral_cmplx_f32_t *p_src_c) {
  float32x4_t re_cte = vdupq_n_f32(p_src_b->re);
  float32x4_t im_cte = vdupq_n_f32(p_src_b->im);

  // im_cte = [im, im, im, im]
  // im_neg_cte = [-im, im, -im, im]
  float32x4_t im_neg_cte = vrev64q_f32(
      vreinterpretq_f32_f64(vnegq_f64(vreinterpretq_f64_f32(im_cte))));

  // Loop unrolling: Compute 2 cmplx outputs at a time
  uint32_t blk_cnt = n >> 1U;

  while (blk_cnt > 0U) {
    // C[2i] + jC[2i+1] = (A[2i] + jA[2i+1]) * (B[0] + jB[1])

    float32x4_t vec1 = vld1q_f32((const float32_t *)p_src_a);
    float32x4_t vec1_rev = vrev64q_f32(vec1);
    float32x4_t vec2 = vld1q_f32((const float32_t *)p_src_c);

    // re_c = re_c - re_a * re_b
    // im_c = im_c - im_a * re_b
    vec2 = vfmsq_f32(vec2, vec1, re_cte);
    // re_c = re_c - im_a * -im_b
    // im_c = im_c - re_a *  im_b
    vec2 = vfmsq_f32(vec2, vec1_rev, im_neg_cte);

    // Store real and imaginary result in destination pointer.
    vst1q_f32((float32_t *)p_src_c, vec2);

    // Increment pointers
    p_src_a += 2;
    p_src_c += 2;

    // Decrement the loop counter
    blk_cnt--;
  }

  // Tail
  blk_cnt = n & 0x1;

  if (blk_cnt > 0U) {
    float32_t re_b = p_src_b->re;
    float32_t im_b = p_src_b->im;
    float32_t re_a = p_src_a->re;
    float32_t im_a = p_src_a->im;

    float32_t real_sum = p_src_c->re;
    float32_t imag_sum = p_src_c->im;
    real_sum -= re_a * re_b;
    real_sum += im_a * im_b;
    imag_sum -= im_a * re_b;
    imag_sum -= im_b * re_a;

    // Store real and imaginary result in destination buffer.
    p_src_c->re = real_sum;
    p_src_c->im = imag_sum;
  }
}

// safemin is the smallest floating point
// number such as 1/safemin does not overflow.
// It is used to check and guard against overflow
// in division by a small floating point number.
// Epsilon is taken to be 2^{-(p-1)}/2, p=24 for float.
// We use the same value of epsilon used in LAPACK.
const float32_t eps = 5.96046E-08;
const float32_t safemin = 1.17549E-38 / eps;

// Compute a * b
inline armral_cmplx_f32_t mult_cf32(armral_cmplx_f32_t a,
                                    armral_cmplx_f32_t b) {
  armral_cmplx_f32_t ret;
  ret.re = a.re * b.re - a.im * b.im;
  ret.im = a.re * b.im + a.im * b.re;
  return ret;
}

// Compute c -= a * b
inline armral_cmplx_f32_t mult_sub_cf32(armral_cmplx_f32_t a,
                                        armral_cmplx_f32_t b,
                                        armral_cmplx_f32_t c) {
  armral_cmplx_f32_t tmp;
  tmp.re = a.re * b.re - a.im * b.im;
  tmp.im = a.re * b.im + a.im * b.re;
  return {c.re - tmp.re, c.im - tmp.im};
}

// Compute a * conj(b)
inline armral_cmplx_f32_t mult_conj_cf32(armral_cmplx_f32_t a,
                                         armral_cmplx_f32_t b) {
  armral_cmplx_f32_t ret;
  ret.re = a.re * b.re + a.im * b.im;
  ret.im = a.im * b.re - a.re * b.im;
  return ret;
}

// Compute c +=  a * conj(b)
inline armral_cmplx_f32_t mult_conj_add_cf32(armral_cmplx_f32_t a,
                                             armral_cmplx_f32_t b,
                                             armral_cmplx_f32_t c) {
  armral_cmplx_f32_t tmp;
  tmp.re = a.re * b.re + a.im * b.im;
  tmp.im = a.im * b.re - a.re * b.im;
  return {tmp.re + c.re, tmp.im + c.im};
}

// Compute c -=  a * conj(b)
inline armral_cmplx_f32_t mult_conj_sub_cf32(armral_cmplx_f32_t a,
                                             armral_cmplx_f32_t b,
                                             armral_cmplx_f32_t c) {
  armral_cmplx_f32_t tmp;
  tmp.re = a.re * b.re + a.im * b.im;
  tmp.im = a.im * b.re - a.re * b.im;
  return {c.re - tmp.re, c.im - tmp.im};
}

// Compute c +=  a * b
inline armral_cmplx_f32_t mult_add_cf32(armral_cmplx_f32_t a,
                                        armral_cmplx_f32_t b,
                                        armral_cmplx_f32_t c) {
  armral_cmplx_f32_t tmp;
  tmp.re = a.re * b.re - a.im * b.im;
  tmp.im = a.im * b.re + a.re * b.im;
  return {tmp.re + c.re, tmp.im + c.im};
}

// Compute a * conj(a)
inline float32_t square_conj_cf32(armral_cmplx_f32_t a) {
  return a.re * a.re + a.im * a.im;
}

inline armral_cmplx_f32_t inv_cf32(armral_cmplx_f32_t a) {
  float32_t tmp = a.re * a.re + a.im * a.im;
  return {a.re / tmp, -a.im / tmp};
}

// clarfg computes the householder reflectors of
// a given vector to annihilate all the entries
// except the first. The second entry to the
// last are overwritten by the reflectors.
inline armral_cmplx_f32_t clarfg(uint32_t n, armral_cmplx_f32_t &aii,
                                 armral_cmplx_f32_t *x, uint32_t incx) {

  armral_cmplx_f32_t alpha = aii;
  // Sum of x[i] * conj(x[i])
  float32_t sum = 0.0F;
  for (uint32_t i = 0; i < n * incx; i += incx) {
    sum += square_conj_cf32(x[i]);
  }

  // Check if the elts to annihilate are all zero
  if (sum == 0 && alpha.im == 0) {
    return {0.0F, 0.0F};
  }
  // Add alpha * conj(alpha) to sum
  // to compute the 2 norm of the full vector
  sum += square_conj_cf32(alpha);
  float32_t beta = -copysign(sqrt(sum), alpha.re);
  float32_t rsafemin = 1.0F / safemin;
  uint32_t cnt = 0;
  uint32_t max_attempt = 10;
  float32_t scale = 1.0F;
  // Check if beta is small enough to induce
  // overflow when taking the inverse, and
  // if it is the case, scale to avoid overflow
  while ((std::abs(beta) < safemin) && (cnt < max_attempt)) {
    beta *= rsafemin;
    scale *= rsafemin;
    cnt++;
  }
  // scale alpha and beta
  // only if there is a risk of overflow
  if (cnt > 0) {
    alpha.re *= scale;
    alpha.im *= scale;
    for (uint32_t i = 0; i < n * incx; i += incx) {
      x[i].re *= scale;
      x[i].im *= scale;
    }
    // The new beta is at most 1, at least safmin,
    sum = square_conj_cf32(alpha);
    for (uint32_t i = 0; i < n * incx; i += incx) {
      sum += square_conj_cf32(x[i]);
    }
    beta = -copysign(sqrt(sum), alpha.re);
  }
  // Compute tau and update aii
  armral_cmplx_f32_t tau;
  tau.re = (beta - alpha.re) / beta;
  tau.im = -alpha.im / beta;
  armral_cmplx_f32_t normalization_factor =
      inv_cf32({alpha.re - beta, alpha.im});
  for (uint32_t i = 0; i < n * incx; i += incx) {
    x[i] = mult_cf32(normalization_factor, x[i]);
  }
  beta /= scale;
  aii = {beta, 0.0F};
  return tau;
}

// Computation of Givens rotation components.
inline void rotg(float32_t f, float32_t g, float32_t &cs, float32_t &sn,
                 float32_t &r) {
  if (f == 0) {
    cs = 0.0F;
    sn = 1.0F;
    r = g;
    return;
  }
  if (std::abs(f) > std::abs(g)) {
    float32_t t = g / f;
    float32_t tt = sqrt(1 + t * t);
    cs = 1 / tt;
    sn = t / tt;
    r = f * tt;
    return;
  }
  float32_t t = f / g;
  float32_t tt = sqrt(1 + t * t);
  sn = 1 / tt;
  cs = t / tt;
  r = g * tt;
}

// This routine updates singular vectors
// by applying the Givens rotations
// used to update the bidiagonal matrix
inline void update_sigvect(uint32_t m, float32_t cs, float32_t sn,
                           armral_cmplx_f32_t *v1, armral_cmplx_f32_t *v2,
                           uint32_t incv) {
  for (uint32_t i = 0; i < m * incv; i += incv) {
    auto t = v1[i];
    v1[i].re = cs * t.re + sn * v2[i].re;
    v1[i].im = cs * t.im + sn * v2[i].im;
    v2[i].re = -sn * t.re + cs * v2[i].re;
    v2[i].im = -sn * t.im + cs * v2[i].im;
  }
}

// householder_qr computes the QR factorization A = QR.
// On exit, the elements on and above the diagonal
// of the A contain the upper triangular matrix R.
// The elements below the diagonal, with the array tau,
// represent the orthogonal matrx Q. Note that Q is not
// stored explicitly, but can be formed using
// the armral_assemble_q routine, or applied,
// using armral_apply_q.
armral_status householder_qr(uint32_t m, uint32_t n, armral_cmplx_f32_t *a,
                             armral_cmplx_f32_t *tau) {
  if (m < n) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  column_major_matrix_view a_mat{a, m};
  for (uint32_t i = 0; i < n; i++) {
    uint32_t k = std::min(i + 1, m - 1);
    tau[i] =
        clarfg(m - i - 1, a_mat(i, i), &a_mat(k, i), a_mat.row_increment());
    auto tmp = a_mat(i, i);
    a_mat(i, i) = {1.0F, 0.0F};
    if (i < n - 1) {
      for (uint32_t col = i + 1; col < n; col++) {
        // w = A(row, col) * conj(A(row, i))
        armral_cmplx_f32_t w = {0.0F, 0.0F};
        if (m > i) {
          cmplx_vecdot_conj_f32(m - i, &a_mat(i, col), &a_mat(i, i), &w);
          auto tmp2 = mult_conj_cf32(w, tau[i]);
          cmplx_axmy_f32(m - i, &a_mat(i, i), &tmp2, &a_mat(i, col));
        }
      }
    }
    a_mat(i, i) = tmp;
  }
  return ARMRAL_SUCCESS;
}

// Generate explicitly Q from QR factorization or from
// the bidiagonalization A = Q * B * P^H
armral_status assemble_q(uint32_t m, uint32_t n, const armral_cmplx_f32_t *a,
                         const armral_cmplx_f32_t *tau, armral_cmplx_f32_t *q) {
  if (m < n) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  // make a copy of a into q
  memcpy(q, a, m * n * sizeof(armral_cmplx_f32_t));

  // Accumulate reflectors from right to left
  // Q = H1 * H2....* Hn. They are applied to identity.
  column_major_matrix_view q_mat{q, m};

  // n will always be >=1 because of the fast
  // return in the top-level function
  uint32_t i = n;
  do {
    i--;
    if (i < n - 1) {
      q_mat(i, i) = {1.0F, 0.0F};
      // Apply reflector from the left
      for (uint32_t col = i + 1; col < n; col++) {
        armral_cmplx_f32_t w = {0.0F, 0.0F};
        if (m > i) {
          cmplx_vecdot_conj_f32(m - i, &q_mat(i, col), &q_mat(i, i), &w);
          auto tmp = mult_cf32(tau[i], w);
          cmplx_axmy_f32(m - i, &q_mat(i, i), &tmp, &q_mat(i, col));
        }
      }
    }
    if (i < m - 1) {
      // Scale entries i+1 to m-1 of the i-th column
      for (uint32_t r = i + 1; r < m; r++) {
        q_mat(r, i) = mult_cf32(q_mat(r, i), {-tau[i].re, -tau[i].im});
      }
    }
    q_mat(i, i) = {1.0F - tau[i].re, -tau[i].im};
    // Set the entries 0 to i-1 of the i-th column to zero
    for (uint32_t r = 0; r < i; r++) {
      q_mat(r, i) = {0.0F, 0.0F};
    }
  } while (i != 0);
  return ARMRAL_SUCCESS;
}

// Generate the orthogonal matrix P from
// the bidiagonalization  A = Q * B * P^H,
// note that P^H is generated directly
// instead of P
void assemble_p(uint32_t m, uint32_t n, const armral_cmplx_f32_t *a,
                const armral_cmplx_f32_t *tau, armral_cmplx_f32_t *p) {
  // Shifted copy of A to P with first
  // column and first row set to zero
  // Set first column to zero
  // except p[0][0] = 1
  p[0] = {1.0F, 0.0F};
  memset(&p[1], 0, n * sizeof(armral_cmplx_f32_t));
  column_major_matrix_view p_mat{p, n};
  column_major_matrix_view a_mat{a, m};
  for (uint32_t j = 1; j < n; j++) {
    // Set for row to zero
    p_mat(0, j) = {0.0F, 0.0F};
    for (uint32_t i = 1; i <= j; i++) {
      p_mat(i, j) = a_mat(i - 1, j);
    }
  }
  // Work on shifted matrix with reflector
  // just above the diagonal to fall back a
  // case similar to QR

  // n will always be >=1 because of the fast
  // return in the top-level function
  uint32_t n1 = n - 1;
  auto *p1 = &p[p_mat.stride() + 1];

  // Apply householder reflectors from the right
  column_major_matrix_view p1_mat{p1, n};
  uint32_t i = n1;
  do {
    i--;
    if (i < n1 - 1) {
      p1_mat(i, i) = {1.0F, 0.0F};
      for (uint32_t row = i + 1; row < n1; row++) {
        armral_cmplx_f32_t w = {0.0F, 0.0F};
        for (uint32_t col = i; col < n1; col++) {
          w = mult_conj_add_cf32(p1_mat(row, col), p1_mat(i, col), w);
        }
        auto tmp = mult_conj_cf32(w, tau[i]);
        for (uint32_t col = i; col < n1; col++) {
          p1_mat(row, col) =
              mult_sub_cf32(p1_mat(i, col), tmp, p1_mat(row, col));
        }
      }
      // Scale
      for (uint32_t col = i + 1; col < n1; col++) {
        p1_mat(i, col) = mult_cf32(p1_mat(i, col), {-tau[i].re, tau[i].im});
      }
    }
    p1_mat(i, i) = {1.0F - tau[i].re, tau[i].im};
    // Set entries  0 to i-1 of the i-th row to zero
    for (uint32_t col = 0; col < i; col++) {
      p1_mat(i, col) = {0.0F, 0.0F};
    }
  } while (i != 0);
}

// This routine reduces a general complex m-by-n matrix A
// to upper bidiagonal form B by a unitary transformation:
// Q**H * A * P = B. On exit, the diagonal and the first
// superdiagonal are overwritten with the upper bidiagonal
// matrix B. The elements below the diagonal, with
// the array tauq, represent the unitary matrix Q as a
// product of elementary reflectors, and the elements
// above the first superdiagonal, with the array taup,
// represent the unitary matrix P as a product of
// elementary reflectors. On exit, D contains the
// diagonal elements of the bidiagonal matrix B,
// and E contains the off-diagonal elements of
// the bidiagonal matrix B. Note that this routine
// returns directly the conjugate transpose of the
// left orthogonal matrix.
armral_status bidiagonalization(uint32_t m, uint32_t n, armral_cmplx_f32_t *a,
                                float32_t *d, float32_t *e,
                                armral_cmplx_f32_t *tauq,
                                armral_cmplx_f32_t *taup) {
  if (m < n) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  column_major_matrix_view a_mat{a, m};
  for (uint32_t i = 0; i < n; i++) {
    // QR steps, generate elementary reflector H(i) to annihilate
    // the entries i+1 to the last of the i-th column
    uint32_t k = std::min(i + 1, m - 1);
    tauq[i] =
        clarfg(m - i - 1, a_mat(i, i), &a_mat(k, i), a_mat.row_increment());
    d[i] = a_mat(i, i).re;
    a_mat(i, i) = {1.0F, 0.0F};
    // Apply householder reflectors from left
    if (i < n - 1) {
      for (uint32_t col = i + 1; col < n; col++) {
        // w = A(i:m-1, col) * conj(A(i:m-1, i))
        armral_cmplx_f32_t w = {0.0F, 0.0F};
        if (m > i) {
          cmplx_vecdot_conj_f32(m - i, &a_mat(i, col), &a_mat(i, i), &w);
          auto tmp = mult_conj_cf32(w, tauq[i]);
          cmplx_axmy_f32(m - i, &a_mat(i, i), &tmp, &a_mat(i, col));
        }
      }
    }
    a_mat(i, i) = {d[i], 0.0F};

    // LQ steps
    if (i == n - 1) {
      taup[i] = {0.0F, 0.0F};
      continue;
    }

    // Transpose conjugate entries i+1 to the last
    // of the i-th row
    for (uint32_t col = i + 1; col < n; col++) {
      a_mat(i, col).im = -a_mat(i, col).im;
    }

    // Generate reflectors to annihilate the entries i+2
    // to the last of the i-th row.
    uint32_t j = std::min(i + 2, n - 1);
    taup[i] = clarfg(n - i - 2, a_mat(i, i + 1), &a_mat(i, j),
                     a_mat.column_increment());
    e[i] = a_mat(i, i + 1).re;
    a_mat(i, i + 1) = {1.0F, 0.0F};
    // Apply the reflectors
    for (uint32_t row = i + 1; row < m; row++) {
      armral_cmplx_f32_t w = {0.0F, 0.0F};
      for (uint32_t col = i + 1; col < n; col++) {
        w = mult_add_cf32(a_mat(row, col), a_mat(i, col), w);
      }
      auto tmp = mult_cf32(taup[i], w);
      for (uint32_t col = i + 1; col < n; col++) {
        a_mat(row, col) =
            mult_conj_sub_cf32(tmp, a_mat(i, col), a_mat(row, col));
      }
    }
    // Conjugate transpose the reflectors
    for (uint32_t col = i + 1; col < n; col++) {
      a_mat(i, col).im = -a_mat(i, col).im;
    }
    a_mat(i, i + 1) = {e[i], 0.0F};
  }
  return ARMRAL_SUCCESS;
}

// svd_bidiagonal computes the singular values and,
// optionally, the right and left singular vectors
// from the SVD of a real n-by-n upper bidiagonal
//  matrix B using the Wilkinson shift QR algorithm.
// The SVD of B has the form  B = Q * S * P**H.
//  On entry, D and E form the bidiagonal matrix, and
//  on exit, D contains the singular values. The
//  left and right singular vectors are updated if required.
//  This algorithm is developed by G. H. Golub  and C. Reinsch.
//  For more detail, the algorithm is well explained in
//  "Singular Value Decomposition and Least Squares Solutions"
//  published in Numer. Math. 14, 403--420 (1970).
armral_status svd_bidiagonal(bool gen_singular_vectors, uint32_t m, uint32_t n,
                             float32_t *d, float32_t *e, armral_cmplx_f32_t *u,
                             armral_cmplx_f32_t *vt, uint32_t u_stride) {

  if (m < n) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  // Shift the off-diagonal elements down by 1
  // This helps to have D[i] and E[i] as the i-th
  // column of the bidiagonal matrix B.
  for (uint32_t i = n - 1; i > 0; i--) {
    e[i] = e[i - 1];
  }
  e[0] = 0.0F;

  // Compute the 1-norm of the bidiagonal matrix
  // for the computation of the stopping criteria.
  float32_t anorm = 0;
  for (uint32_t i = 0; i < n; i++) {
    float32_t tmp = std::abs(d[i]) + std::abs(e[i]);
    if (anorm < tmp) {
      anorm = tmp;
    }
  }
  float32_t tol = anorm * eps;

  uint32_t maxiter = n * n;
  // Loop over the columns
  column_major_matrix_view u_mat{u, u_stride};
  column_major_matrix_view vt_mat{vt, n};

  // n will always be >=1 because of the fast
  // return in the top-level function
  uint32_t curr_col = n;
  do {
    curr_col--;
    // iteration to annihilate the off-diagonal E[curr_col].
    uint32_t iter = 0;
    for (; iter <= maxiter; iter++) {
      bool diag_is_zero = false;
      uint32_t next_col = curr_col + 1;

      // Check if an off-diagonal is zero.
      do {
        next_col--;
        if (std::abs(e[next_col]) < tol) {
          break;
        }
        // Check if a diagonal is zero.
        if (std::abs(d[next_col - 1]) < tol) {
          diag_is_zero = true;
          break;
        }
      } while (next_col != 0);
      // If the diagonal D[next_col] = 0; then at least one singular
      // value must be equal to zero. In the absence of roundoff error,
      // the matrix will break if a shift of zero is performed.
      // In this case, an extra sequence of Givens rotations is
      // applied from the left to annihilate the off-diagonal E[next_col].
      if (diag_is_zero) {
        float32_t cs = 0.0;
        float32_t sn = 1.0;
        for (uint32_t i = next_col; i < curr_col; i++) {
          float32_t f = sn * e[i];
          e[i] *= cs;
          if (std::abs(f) <= tol) {
            break;
          }
          float32_t g = d[i];
          float32_t h;
          rotg(f, g, cs, sn, h);
          d[i] = h;
          // Update left singular vectors.
          if (gen_singular_vectors) {
            update_sigvect(m, cs, -sn, &u_mat(0, next_col - 1), &u_mat(0, i),
                           u_mat.row_increment());
          }
        }
      }
      float32_t z = d[curr_col];
      if (next_col == curr_col) {
        // Make singular value nonnegative and update
        // the corresponding right singular vectors.
        if (z < 0.0) {
          d[curr_col] = -z;
          if (gen_singular_vectors) {
            // For the sake of performance we copy data that is contiguous in
            // memory
            for (uint32_t row = 0; row < m; row++) {
              u_mat(row, curr_col).re = -u_mat(row, curr_col).re;
              u_mat(row, curr_col).im = -u_mat(row, curr_col).im;
            }
          }
        }
        break;
      }
      // Shifted QR iteration with wilkinson shift
      // that is determined by the eigenvalue of the bottom
      // right 2 × 2 block of the unreduced matrix.
      // As the algorithm works on a bidiagonal matrix,
      // the entries of the corresponding symmetric
      // tridigonal matrix B^H * B of interest are computed implicitly.
      // Note that for a 2-by-2 symmetric tridiagonal matrix
      // with d1 and d2 the diagonals and e1 the off diagonal,
      // the 2 eigenvalues are (d1 + d2)/2 +/- sqrt(((d1 - d2)/2)^2 + e1^2).
      // The choice of this shift accelerates the convergence of the
      // most bottom off-diagonal E[curr_col] to zero.
      float32_t x = d[next_col];
      float32_t y = d[curr_col - 1];
      float32_t g = e[curr_col - 1];
      float32_t h = e[curr_col];
      // a^2 - b^2 operations are computed as
      // (a - b)* (a + b) to avoid overflow.
      float32_t f = ((y - z) * (y + z) + (g - h) * (g + h)) / (2.0 * h * y);
      g = sqrt(f * f + 1);
      f = ((x - z) * (x + z) + h * (y / (f + copysign(g, f)) - h)) / x;

      // Shifted QR iteration, bulge chasing, applying
      // successive Givens rotations from right then from left.
      float32_t c = 1.0F;
      float32_t s = 1.0F;
      for (uint32_t i = next_col + 1; i <= curr_col; i++) {
        g = e[i];
        y = d[i];
        h = s * g;
        g = c * g;
        // Apply Givens rotation from right.
        rotg(f, h, c, s, z);
        f = x * c + g * s;
        g = g * c - x * s;
        h = y * s;
        y = y * c;
        e[i - 1] = z;
        // Update right singular vector.
        if (gen_singular_vectors) {
          update_sigvect(n, c, s, &vt_mat(i - 1, 0), &vt_mat(i, 0),
                         vt_mat.column_increment());
        }
        // Apply Givens rotation from left.
        rotg(f, h, c, s, z);
        d[i - 1] = z;
        f = (c * g) + (s * y);
        x = (c * y) - (s * g);
        // Update left singular vectors.
        if (gen_singular_vectors) {
          update_sigvect(m, c, s, &u_mat(0, i - 1), &u_mat(0, i),
                         u_mat.row_increment());
        }
      }
      e[next_col] = 0.0;
      e[curr_col] = f;
      d[curr_col] = x;
    }
  } while (curr_col != 0);
  // Sort the singular values in decreasing order
  // and the singular vectors if required.
  for (uint32_t i = 0; i < n - 1; i++) {
    uint32_t max_pos = i;
    for (uint32_t j = i + 1; j < n; j++) {
      if (d[j] > d[max_pos]) {
        max_pos = j;
      }
    }
    if (max_pos != i) {
      std::swap(d[i], d[max_pos]);
      if (gen_singular_vectors) {
        // Swap corresponding columns in left singular vectors.
        for (uint32_t row = 0; row < m; row++) {
          std::swap(u_mat(row, i).re, u_mat(row, max_pos).re);
          std::swap(u_mat(row, i).im, u_mat(row, max_pos).im);
        }
        // Swap corresponding rows in right singular vectors.
        for (uint32_t col = 0; col < n; col++) {
          std::swap(vt_mat(i, col).re, vt_mat(max_pos, col).re);
          std::swap(vt_mat(i, col).im, vt_mat(max_pos, col).im);
        }
      }
    }
  }
  return ARMRAL_SUCCESS;
}

// Apply implicitly Q to an input matrix C of the same dimension
// as the matrix A that has been factorized into QR or bidiagonalization.
struct apply_q_work_buffers {
  armral_cmplx_f32_t *q;
};

inline armral_status apply_q(uint32_t m, uint32_t n,
                             const armral_cmplx_f32_t *a,
                             const armral_cmplx_f32_t *tau,
                             armral_cmplx_f32_t *c,
                             apply_q_work_buffers work_buffers) {
  if (m < n) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  // make a copy of a into q
  memcpy(work_buffers.q, a, (size_t)m * n * sizeof(armral_cmplx_f32_t));
  column_major_matrix_view q_mat{work_buffers.q, m};
  column_major_matrix_view c_mat{c, m};

  // n will always be >=1 because of the fast
  // return in the top-level function
  uint32_t i = n;
  do {
    i--;
    q_mat(i, i) = {1.0F, 0.0F};
    // Apply reflector from the left to all columns
    //  of C from row index i, to the end.
    for (uint32_t col = 0; col < n; col++) {
      armral_cmplx_f32_t w = {0.0F, 0.0F};
      if (m > i) {
        cmplx_vecdot_conj_f32(m - i, &c_mat(i, col), &q_mat(i, i), &w);
        auto tmp = mult_cf32(tau[i], w);
        cmplx_axmy_f32(m - i, &q_mat(i, i), &tmp, &c_mat(i, col));
      }
    }
  } while (i != 0);

  return ARMRAL_SUCCESS;
}

// Compute the crossover point for the SVD.
// When reducing an m-by-n (m >= n) matrix  to
// bidiagonal form, if m / n exceeds this value,
// a QR factorization is used first to reduce the
// matrix to a triangular form.
inline uint32_t threshold_svd_qr(bool vector_needed, uint32_t m, uint32_t n) {

  float32_t crossover_point;
  if (vector_needed) {
    // In this case, the computational complexities are:
    // 14 * m * n^2 + 8 * n^3 for direct svd,
    // 6 * m * n^2 + 20 * n^3 for qr_svd.
    // qr_svd becomes efficient when m >= 1.5 * n
    crossover_point = n * 1.5;
  } else {
    // When only the singular values are required,
    // the computational complexities are:
    // 4 * m * n^2 - 4/3 * n^3 for direct svd,
    // 2 * m * n^2 + 2 * n^3 for qr_svd
    // qr_svd becomes efficient when m >= 1.6 * n
    crossover_point = n * 1.6;
  }
  // return a crossover_point rounded toward zero
  return (uint32_t)crossover_point;
}

// qr_svd computes the SVD decomposition
// of an m-by-n matrix A in 4 steps.
// 1- QR factorization of A.
// 2- Bidiagonalization of R.
// 3- SVD of the bidiagonal matrix from R.
// 4- Update of the left singular vectors
// with the orthogonal matrix from QR.
template<typename Allocator>
armral_status qr_svd(bool gen_singular_vect, uint32_t m, uint32_t n,
                     armral_cmplx_f32_t *a, float32_t *s, armral_cmplx_f32_t *u,
                     armral_cmplx_f32_t *vt, Allocator &allocator) {

  assert(m >= n && "Invalid arguments: m < n is not supported");

  auto tau = allocate_uninitialized<armral_cmplx_f32_t>(allocator, n);
  auto r = allocate_zeroed<armral_cmplx_f32_t>(allocator, n * n);
  auto tauq = allocate_uninitialized<armral_cmplx_f32_t>(allocator, n);
  auto taup = allocate_uninitialized<armral_cmplx_f32_t>(allocator, n);
  auto e = allocate_uninitialized<float32_t>(allocator, n);

  // u1 and q have the same type as r, so we can reuse that pointer type.
  using cmplx_ptr = decltype(r);

  // Optionally allocate buffers needed to generate singular vectors.
  std::optional<cmplx_ptr> maybe_u1;
  std::optional<cmplx_ptr> maybe_q;
  if (gen_singular_vect) {
    maybe_u1 = allocate_uninitialized<armral_cmplx_f32_t>(allocator, n * n);
    maybe_q = allocate_uninitialized<armral_cmplx_f32_t>(allocator, m * n);
  }

  if constexpr (Allocator::is_counting) {
    return ARMRAL_SUCCESS;
  }

  // Perform the QR factorization of A.
  householder_qr(m, n, a, tau.get());

  // Extract the R.
  column_major_matrix_view r_mat{r.get(), n};
  column_major_matrix_view a_mat{a, m};
  for (uint32_t j = 0; j < n; j++) {
    for (uint32_t i = 0; i <= j; i++) {
      r_mat(i, j) = a_mat(i, j);
    }
  }
  // Bidiagonalization of R.
  bidiagonalization(n, n, r.get(), s, e.get(), tauq.get(), taup.get());

  // Generate left and right orthogonal vectors.
  if (maybe_u1.has_value()) {
    auto *u1 = maybe_u1.value().get();
    // Generate Q, and store it in u1.
    assemble_q(n, n, r.get(), tauq.get(), u1);
    // Copy u1 in u
    column_major_matrix_view u_mat{u, m};
    column_major_matrix_view u1_mat{u1, n};
    for (uint32_t j = 0; j < n; j++) {
      for (uint32_t i = 0; i < n; i++) {
        u_mat(i, j) = u1_mat(i, j);
      }
    }

    // Initialize  last n*(m-n) elements of u
    // to zero in case it is not.
    // m >=n is a prerequisite of this function so this
    // will never be negative
    uint32_t remainder = m - n;
    for (uint32_t j = 0; j < n; j++) {
      memset(&u[n + j * u_mat.stride()], 0,
             remainder * sizeof(armral_cmplx_f32_t));
    }
    // Generate P and store it in vt.
    assemble_p(n, n, r.get(), taup.get(), vt);
  }
  // Compute the singular values
  // and singular vectors if required.
  // Note: U is treated as N-by-N, but still stored in an M-by-N matrix.
  svd_bidiagonal(gen_singular_vect, n, n, s, e.get(), u, vt, m);

  // Apply Q to U
  if (maybe_q.has_value()) {
    apply_q(m, n, a, tau.get(), u, {maybe_q.value().get()});
  }

  return ARMRAL_SUCCESS;
}

template<typename Allocator>
armral_status svd(bool gen_singular_vect, uint32_t m, uint32_t n,
                  armral_cmplx_f32_t *a, float32_t *s, armral_cmplx_f32_t *u,
                  armral_cmplx_f32_t *vt, Allocator &allocator) {
  // Bidiagonalization: A = Q * B * P^H.
  auto tauq = allocate_uninitialized<armral_cmplx_f32_t>(allocator, n);
  auto taup = allocate_uninitialized<armral_cmplx_f32_t>(allocator, n);
  auto e = allocate_uninitialized<float32_t>(allocator, n);

  if constexpr (Allocator::is_counting) {
    return ARMRAL_SUCCESS;
  }

  bidiagonalization(m, n, a, s, e.get(), tauq.get(), taup.get());

  // Generate left and right orthogonal vectors if required.
  if (gen_singular_vect) {
    // Generate Q and store it in u.
    assemble_q(m, n, a, tauq.get(), u);
    // Generate P and store it in vt.
    assemble_p(m, n, a, taup.get(), vt);
  }
  // Compute the singular values and singular vectors
  // if required.
  svd_bidiagonal(gen_singular_vect, m, n, s, e.get(), u, vt, m);

  return ARMRAL_SUCCESS;
}

// svd_cf32 computes the SVD decomposition
// of an m-by-n matrix. It either performs
// a direct SVD decomposition of the input matrix,
// or performs QR factorization first followed
// by the SVD of R depending on the ratio m/n.
template<typename Allocator>
armral_status svd_cf32(bool gen_singular_vect, uint32_t m, uint32_t n,
                       armral_cmplx_f32_t *a, float32_t *s,
                       armral_cmplx_f32_t *u, armral_cmplx_f32_t *vt,
                       Allocator &allocator) {
  // Call arm_qr_svd if m is much larger than n
  uint32_t crossover_point = threshold_svd_qr(gen_singular_vect, m, n);
  if (m > crossover_point) {
    return qr_svd(gen_singular_vect, m, n, a, s, u, vt, allocator);
  }
  return svd(gen_singular_vect, m, n, a, s, u, vt, allocator);
}

} // anonymous namespace

armral_status armral_svd_cf32(bool gen_singular_vect, uint32_t m, uint32_t n,
                              armral_cmplx_f32_t *a, float32_t *s,
                              armral_cmplx_f32_t *u, armral_cmplx_f32_t *vt) {
  // This function is only implemented for m >= n
  if (m < n) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  // Trivial case: no work to do
  if (m == 0 || n == 0) {
    return ARMRAL_SUCCESS;
  }
  heap_allocator allocator{};
  return svd_cf32(gen_singular_vect, m, n, a, s, u, vt, allocator);
}

armral_status armral_svd_cf32_noalloc(bool gen_singular_vect, uint32_t m,
                                      uint32_t n, armral_cmplx_f32_t *a,
                                      float32_t *s, armral_cmplx_f32_t *u,
                                      armral_cmplx_f32_t *vt, void *buffer) {
  // This function is only implemented for m >= n
  if (m < n) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  // Trivial case: no work to do
  if (m == 0 || n == 0) {
    return ARMRAL_SUCCESS;
  }
  buffer_bump_allocator allocator{buffer};
  return svd_cf32(gen_singular_vect, m, n, a, s, u, vt, allocator);
}

uint32_t armral_svd_cf32_noalloc_buffer_size(bool gen_singular_vect, uint32_t m,
                                             uint32_t n) {
  counting_allocator allocator{};
  (void)svd_cf32(gen_singular_vect, m, n, nullptr, nullptr, nullptr, nullptr,
                 allocator);
  return allocator.required_bytes();
}
