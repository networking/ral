/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#pragma once

#include <cstdint>

/*
  A non-owning column major view of a matrix to provide more convenient
  indexing.
*/
template<typename T>
struct column_major_matrix_view {
  column_major_matrix_view(T *data, uint32_t stride)
    : m_data(data), m_stride(stride) {}

  T &operator()(uint32_t i, uint32_t j) {
    return m_data[i + stride() * j];
  }

  uint32_t stride() const {
    return m_stride;
  }

  uint32_t row_increment() const {
    return 1;
  }

  uint32_t column_increment() const {
    return stride();
  }

private:
  T *const m_data;
  const uint32_t m_stride;
};
