/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

armral_status armral_cmplx_vecmul_i16_2(uint32_t n,
                                        const int16_t *restrict a_re,
                                        const int16_t *restrict a_im,
                                        const int16_t *restrict b_re,
                                        const int16_t *restrict b_im,
                                        int16_t *c_re, int16_t *c_im) {
#if ARMRAL_ARCH_SVE >= 2
  uint32_t num_lanes = svcnth();
  uint32_t full_vectors = n / num_lanes;
  svbool_t pg = svptrue_b16();

  for (uint32_t i = 0; i < full_vectors; ++i) {
    svint16_t vec_a_real = svld1(pg, (const int16_t *)a_re);
    svint16_t vec_a_imag = svld1(pg, (const int16_t *)a_im);
    svint16_t vec_b_real = svld1(pg, (const int16_t *)b_re);
    svint16_t vec_b_imag = svld1(pg, (const int16_t *)b_im);

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    svint32_t vec_c_real_low = svdup_n_s32(0);
    svint32_t vec_c_real_high = svdup_n_s32(0);

    vec_c_real_low = svmullb_s32(vec_a_real, vec_b_real);
    vec_c_real_high = svmullt_s32(vec_a_real, vec_b_real);
    vec_c_real_low = svmlslb_s32(vec_c_real_low, vec_a_imag, vec_b_imag);
    vec_c_real_high = svmlslt_s32(vec_c_real_high, vec_a_imag, vec_b_imag);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    svint32_t vec_c_imag_low = svdup_n_s32(0);
    svint32_t vec_c_imag_high = svdup_n_s32(0);

    vec_c_imag_low = svqdmullb_s32(vec_a_real, vec_b_imag);
    vec_c_imag_high = svqdmullt_s32(vec_a_real, vec_b_imag);
    vec_c_imag_low = svqdmlalb_s32(vec_c_imag_low, vec_a_imag, vec_b_real);
    vec_c_imag_high = svqdmlalt_s32(vec_c_imag_high, vec_a_imag, vec_b_real);

    // Shift >> 15, results in Q0.15 format
    svint16_t vec_c_real = svdup_n_s16(0);
    vec_c_real = svqrshrnb_n_s32(vec_c_real_low, 15);
    vec_c_real = svqrshrnt_n_s32(vec_c_real, vec_c_real_high, 15);

    // Shift >> 16 as we use doubling multiplication and accumulation to get
    // saturation for the imaginary component
    svint16_t vec_c_imag = svdup_n_s16(0);
    vec_c_imag = svqrshrnb_n_s32(vec_c_imag_low, 16);
    vec_c_imag = svqrshrnt_n_s32(vec_c_imag, vec_c_imag_high, 16);

    svst1_s16(pg, (int16_t *)c_re, vec_c_real);
    svst1_s16(pg, (int16_t *)c_im, vec_c_imag);

    a_re += num_lanes;
    a_im += num_lanes;
    b_re += num_lanes;
    b_im += num_lanes;
    c_re += num_lanes;
    c_im += num_lanes;
  }

  uint32_t tail_size = n % num_lanes;
  if (tail_size) {
    pg = svwhilelt_b16(0U, tail_size);

    svint16_t vec_a_real = svld1(pg, (const int16_t *)a_re);
    svint16_t vec_a_imag = svld1(pg, (const int16_t *)a_im);
    svint16_t vec_b_real = svld1(pg, (const int16_t *)b_re);
    svint16_t vec_b_imag = svld1(pg, (const int16_t *)b_im);

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    svint32_t vec_c_real_low = svdup_n_s32(0);
    svint32_t vec_c_real_high = svdup_n_s32(0);

    vec_c_real_low = svmullb_s32(vec_a_real, vec_b_real);
    vec_c_real_high = svmullt_s32(vec_a_real, vec_b_real);
    vec_c_real_low = svmlslb_s32(vec_c_real_low, vec_a_imag, vec_b_imag);
    vec_c_real_high = svmlslt_s32(vec_c_real_high, vec_a_imag, vec_b_imag);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    svint32_t vec_c_imag_low = svdup_n_s32(0);
    svint32_t vec_c_imag_high = svdup_n_s32(0);

    vec_c_imag_low = svqdmullb_s32(vec_a_real, vec_b_imag);
    vec_c_imag_high = svqdmullt_s32(vec_a_real, vec_b_imag);
    vec_c_imag_low = svqdmlalb_s32(vec_c_imag_low, vec_a_imag, vec_b_real);
    vec_c_imag_high = svqdmlalt_s32(vec_c_imag_high, vec_a_imag, vec_b_real);

    // Shift >> 15, results in Q0.15 format
    svint16_t vec_c_real = svdup_n_s16(0);
    vec_c_real = svqrshrnb_n_s32(vec_c_real_low, 15);
    vec_c_real = svqrshrnt_n_s32(vec_c_real, vec_c_real_high, 15);

    // Shift >> 16 as we use doubling multiplication and accumulation to get
    // saturation for the imaginary component
    svint16_t vec_c_imag = svdup_n_s16(0);
    vec_c_imag = svqrshrnb_n_s32(vec_c_imag_low, 16);
    vec_c_imag = svqrshrnt_n_s32(vec_c_imag, vec_c_imag_high, 16);

    svst1_s16(pg, (int16_t *)c_re, vec_c_real);
    svst1_s16(pg, (int16_t *)c_im, vec_c_imag);
  }

  return ARMRAL_SUCCESS;
#else
  uint32_t blk_cnt; /* Loop counter */
  int16_t re_a;
  int16_t im_a;
  int16_t re_b;
  int16_t im_b; /* Temporary variables to store real and imaginary values */

  int16x8_t va_re[2];
  int16x8_t va_im[2];
  int16x8_t vb_re[2];
  int16x8_t vb_im[2];
  int32x4x4_t vc_re;
  int32x4x4_t vc_im;
  int16x4x2_t narrow_re;
  int16x4x2_t narrow_im;
  int16x8x2_t res_re;
  int16x8x2_t res_im;

  /* Compute 16 outputs at a time */
  blk_cnt = n >> 4U;

  while (blk_cnt > 0U) {
    va_re[0] = vld1q_s16(a_re);
    va_re[1] = vld1q_s16(a_re + 8);
    va_im[0] = vld1q_s16(a_im);
    va_im[1] = vld1q_s16(a_im + 8);
    vb_re[0] = vld1q_s16(b_re);
    vb_re[1] = vld1q_s16(b_re + 8);
    vb_im[0] = vld1q_s16(b_im);
    vb_im[1] = vld1q_s16(b_im + 8);

    /* Increment pointers */
    a_re += 16;
    a_im += 16;
    b_re += 16;
    b_im += 16;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    // As we extend to 32 bits, there is no possibility for saturation in the
    // 32-bit multiplication and addition for the real component. Don't use
    // saturating logic for the real part.
    vc_re.val[0] = vmull_low_s16(va_re[0], vb_re[0]);
    vc_re.val[1] = vmull_high_s16(va_re[0], vb_re[0]);
    vc_re.val[2] = vmull_low_s16(va_re[1], vb_re[1]);
    vc_re.val[3] = vmull_high_s16(va_re[1], vb_re[1]);
    vc_re.val[0] = vmlsl_low_s16(vc_re.val[0], va_im[0], vb_im[0]);
    vc_re.val[1] = vmlsl_high_s16(vc_re.val[1], va_im[0], vb_im[0]);
    vc_re.val[2] = vmlsl_low_s16(vc_re.val[2], va_im[1], vb_im[1]);
    vc_re.val[3] = vmlsl_high_s16(vc_re.val[3], va_im[1], vb_im[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    vc_im.val[0] = vqdmull_low_s16(va_re[0], vb_im[0]);
    vc_im.val[1] = vqdmull_high_s16(va_re[0], vb_im[0]);
    vc_im.val[2] = vqdmull_low_s16(va_re[1], vb_im[1]);
    vc_im.val[3] = vqdmull_high_s16(va_re[1], vb_im[1]);
    vc_im.val[0] = vqdmlal_low_s16(vc_im.val[0], va_im[0], vb_re[0]);
    vc_im.val[1] = vqdmlal_high_s16(vc_im.val[1], va_im[0], vb_re[0]);
    vc_im.val[2] = vqdmlal_low_s16(vc_im.val[2], va_im[1], vb_re[1]);
    vc_im.val[3] = vqdmlal_high_s16(vc_im.val[3], va_im[1], vb_re[1]);

    // Shift >> 15, results in Q0.15 format
    narrow_re.val[0] = vqrshrn_n_s32(vc_re.val[0], 15);
    res_re.val[0] = vqrshrn_high_n_s32(narrow_re.val[0], vc_re.val[1], 15);
    narrow_re.val[1] = vqrshrn_n_s32(vc_re.val[2], 15);
    res_re.val[1] = vqrshrn_high_n_s32(narrow_re.val[1], vc_re.val[3], 15);

    // Shift >> 16 as we use doubling multiplication and accumulation to get
    // saturation for the imaginary component
    narrow_im.val[0] = vqrshrn_n_s32(vc_im.val[0], 16);
    res_im.val[0] = vqrshrn_high_n_s32(narrow_im.val[0], vc_im.val[1], 16);
    narrow_im.val[1] = vqrshrn_n_s32(vc_im.val[2], 16);
    res_im.val[1] = vqrshrn_high_n_s32(narrow_im.val[1], vc_im.val[3], 16);

    vst1q_s16(c_re, res_re.val[0]);
    c_re += 8;
    vst1q_s16(c_re, res_re.val[1]);
    c_re += 8;
    vst1q_s16(c_im, res_im.val[0]);
    c_im += 8;
    vst1q_s16(c_im, res_im.val[1]);
    c_im += 8;

    /* Decrement the loop counter */
    blk_cnt--;
  }

  /* 4 samples groups - Tail */
  blk_cnt = n & 0xF;

  int16x4x2_t va;
  int16x4x2_t vb;
  int32x4x2_t vc;
  int16x4x2_t vc_int16;

  while (blk_cnt > 3U) {
    va.val[0] = vld1_s16(a_re); // va_re
    va.val[1] = vld1_s16(a_im); // va_im
    vb.val[0] = vld1_s16(b_re); // vb_re
    vb.val[1] = vld1_s16(b_im); // vb_im

    /* Increment pointers */
    a_re += 4;
    a_im += 4;
    b_re += 4;
    b_im += 4;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    // As we extend to 32 bits, there is no possibility for saturation in the
    // 32-bit multiplication and addition for the real component. Don't use
    // saturating logic for the real part.
    vc.val[0] = vmull_s16(va.val[0], vb.val[0]);
    vc.val[0] = vmlsl_s16(vc.val[0], va.val[1], vb.val[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    vc.val[1] = vqdmull_s16(va.val[0], vb.val[1]);
    vc.val[1] = vqdmlal_s16(vc.val[1], va.val[1], vb.val[0]);

    // Shift >> 15, results in Q0.15 format
    vc_int16.val[0] = vqrshrn_n_s32(vc.val[0], 15);
    // Shift >> 16 as we use doubling multiplication and accumulation to get
    // saturation for the imaginary component
    vc_int16.val[1] = vqrshrn_n_s32(vc.val[1], 16);

    vst1_s16((int16_t *)c_re, vc_int16.val[0]);
    vst1_s16((int16_t *)c_im, vc_int16.val[1]);

    /* Increment pointer */
    c_re += 4;
    c_im += 4;

    /* Decrement the loop counter */
    blk_cnt -= 4;
  }

  /* Tail */
  while (blk_cnt > 0U) {
    /* C[2 * i    ] = A[2 * i] * B[2 * i    ] - A[2 * i + 1] * B[2 * i + 1]. */
    /* C[2 * i + 1] = A[2 * i] * B[2 * i + 1] + A[2 * i + 1] * B[2 * i    ]. */

    re_a = *a_re++;
    im_a = *a_im++;
    re_b = *b_re++;
    im_b = *b_im++;

    va.val[0] = (int16x4_t){re_a, 0, 0, 0};
    va.val[1] = (int16x4_t){im_a, 0, 0, 0};
    vb.val[0] = (int16x4_t){re_b, 0, 0, 0};
    vb.val[1] = (int16x4_t){im_b, 0, 0, 0};

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    // As we extend to 32 bits, there is no possibility for saturation in the
    // 32-bit multiplication and addition for the real component. Don't use
    // saturating logic for the real part.
    vc.val[0] = vmull_s16(va.val[0], vb.val[0]);
    vc.val[0] = vmlsl_s16(vc.val[0], va.val[1], vb.val[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    vc.val[1] = vqdmull_s16(va.val[0], vb.val[1]);
    vc.val[1] = vqdmlal_s16(vc.val[1], va.val[1], vb.val[0]);

    // Shift >> 15, results in Q0.15 format
    *c_re++ = vqrshrn_n_s32(vc.val[0], 15)[0];
    // Shift >> 16 as we use doubling multiplication and accumulation to get
    // saturation for the imaginary component
    *c_im++ = vqrshrn_n_s32(vc.val[1], 16)[0];

    /* Decrement loop counter */
    blk_cnt--;
  }
  return ARMRAL_SUCCESS;
#endif
}
