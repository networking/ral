/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

armral_status armral_cmplx_vecmul_f32(uint32_t n,
                                      const armral_cmplx_f32_t *restrict a,
                                      const armral_cmplx_f32_t *restrict b,
                                      armral_cmplx_f32_t *c) {
#ifdef ARMRAL_ARCH_SVE
  uint32_t num_64bit_lanes = svcntd();
  uint32_t full_vectors = n / num_64bit_lanes;
  svbool_t pg = svptrue_b32();

  for (uint32_t i = 0; i < full_vectors; i++) {
    svfloat32_t vec_a = svld1_f32(pg, (const float32_t *)a);
    svfloat32_t vec_b = svld1_f32(pg, (const float32_t *)b);
    svfloat32_t vec_c = svdup_n_f32(0);

    vec_c = svcmla_f32_x(pg, vec_c, vec_a, vec_b, 0);
    vec_c = svcmla_f32_x(pg, vec_c, vec_a, vec_b, 90);

    svst1_f32(pg, (float32_t *)c, vec_c);

    a += num_64bit_lanes;
    b += num_64bit_lanes;
    c += num_64bit_lanes;
  }

  uint32_t tail_size = n % num_64bit_lanes;
  if (tail_size) {
    pg = svwhilelt_b32(0U, 2 * tail_size);

    svfloat32_t vec_a = svld1_f32(pg, (const float32_t *)a);
    svfloat32_t vec_b = svld1_f32(pg, (const float32_t *)b);
    svfloat32_t vec_c = svdup_n_f32(0);

    vec_c = svcmla_f32_x(pg, vec_c, vec_a, vec_b, 0);
    vec_c = svcmla_f32_x(pg, vec_c, vec_a, vec_b, 90);

    svst1_f32(pg, (float32_t *)c, vec_c);
  }

  return ARMRAL_SUCCESS;
#else
  uint32_t blk_cnt; /* Loop counter */
  float32_t re_a;
  float32_t im_a;
  float32_t re_b;
  float32_t im_b; /* Temporary variables to store real and imaginary values */

  float32x4x2_t va;
  float32x4x2_t vb;
  float32x4x2_t out_cplx;

  /* Compute 4 complex samples at a time */
  blk_cnt = n >> 2U;

  while (blk_cnt > 0U) {
    // load & separate real/imag (de-interleave 2)
    va = vld2q_f32((const float32_t *)a);
    vb = vld2q_f32((const float32_t *)b);

    /* Increment pointers */
    a += 4;
    b += 4;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    out_cplx.val[0] = vmulq_f32(va.val[0], vb.val[0]);
    out_cplx.val[0] = vfmsq_f32(out_cplx.val[0], va.val[1], vb.val[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    out_cplx.val[1] = vmulq_f32(va.val[0], vb.val[1]);
    out_cplx.val[1] = vfmaq_f32(out_cplx.val[1], va.val[1], vb.val[0]);

    vst2q_f32((float32_t *)c, out_cplx);

    /* Increment pointer */
    c += 4;

    /* Decrement the loop counter */
    blk_cnt--;
  }

  /* Tail */
  blk_cnt = n & 3;

  while (blk_cnt > 0U) {
    /* C[2 * i    ] = A[2 * i] * B[2 * i    ] - A[2 * i + 1] * B[2 * i + 1]. */
    /* C[2 * i + 1] = A[2 * i] * B[2 * i + 1] + A[2 * i + 1] * B[2 * i    ]. */

    re_a = a->re;
    im_a = a->im;
    re_b = b->re;
    im_b = b->im;

    /* store result in destination buffer. */
    c->re = (re_a * re_b) - (im_a * im_b);
    c->im = (re_a * im_b) + (im_a * re_b);

    a++;
    b++;
    c++;

    /* Decrement loop counter */
    blk_cnt--;
  }

  return ARMRAL_SUCCESS;
#endif
}
