/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"
#include "utils/vec_mul.hpp"

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

armral_status armral_cmplx_vecmul_i16(uint32_t n, const armral_cmplx_int16_t *a,
                                      const armral_cmplx_int16_t *b,
                                      armral_cmplx_int16_t *c) {
#if ARMRAL_ARCH_SVE >= 2
  uint32_t num_lanes = svcnth();
  uint32_t full_vectors = n / num_lanes;
  svbool_t pg = svptrue_b16();

  for (uint32_t i = 0; i < full_vectors; ++i) {
    svint16x2_t vec_a = svld2_s16(pg, (const int16_t *)a);
    svint16x2_t vec_b = svld2_s16(pg, (const int16_t *)b);

    svint16x2_t vec_c = sv_cmplx_mul_split_re_im(vec_a, vec_b);
    svst2_s16(pg, (int16_t *)c, vec_c);

    a += num_lanes;
    b += num_lanes;
    c += num_lanes;
  }

  uint32_t tail_size = n % num_lanes;
  if (tail_size != 0) {
    pg = svwhilelt_b16(0U, tail_size);

    svint16x2_t vec_a = svld2_s16(pg, (const int16_t *)a);
    svint16x2_t vec_b = svld2_s16(pg, (const int16_t *)b);

    /* re{c} = re{a}*re{b} - im{a}*im{b} */
    svint32_t vec_c_real_low = svdup_n_s32(0);
    svint32_t vec_c_real_high = svdup_n_s32(0);

    vec_c_real_low = svmullb_s32(svget2_s16(vec_a, 0), svget2_s16(vec_b, 0));
    vec_c_real_high = svmullt_s32(svget2_s16(vec_a, 0), svget2_s16(vec_b, 0));
    vec_c_real_low =
        svmlslb_s32(vec_c_real_low, svget2_s16(vec_a, 1), svget2_s16(vec_b, 1));
    vec_c_real_high = svmlslt_s32(vec_c_real_high, svget2_s16(vec_a, 1),
                                  svget2_s16(vec_b, 1));

    /* im{c} = re{a}*im{b} + im{a}*re{b} */
    svint32_t vec_c_imag_low = svdup_n_s32(0);
    svint32_t vec_c_imag_high = svdup_n_s32(0);

    vec_c_imag_low = svqdmullb_s32(svget2_s16(vec_a, 0), svget2_s16(vec_b, 1));
    vec_c_imag_high = svqdmullt_s32(svget2_s16(vec_a, 0), svget2_s16(vec_b, 1));
    vec_c_imag_low = svqdmlalb_s32(vec_c_imag_low, svget2_s16(vec_a, 1),
                                   svget2_s16(vec_b, 0));
    vec_c_imag_high = svqdmlalt_s32(vec_c_imag_high, svget2_s16(vec_a, 1),
                                    svget2_s16(vec_b, 0));

    svint16x2_t vec_c = svcreate2_s16(svdup_n_s16(0), svdup_n_s16(0));
    /* shift >> 15, results in q15 format */
    vec_c = svset2_s16(vec_c, 0, svqrshrnb_n_s32(vec_c_real_low, 15));
    vec_c = svset2_s16(
        vec_c, 0, svqrshrnt_n_s32(svget2_s16(vec_c, 0), vec_c_real_high, 15));
    // shift >> 16 for imaginary component, as we use doubling multiplication
    // and addition to be able to make use of the saturating intrinsics
    vec_c = svset2_s16(vec_c, 1, svqrshrnb_n_s32(vec_c_imag_low, 16));
    vec_c = svset2_s16(
        vec_c, 1, svqrshrnt_n_s32(svget2_s16(vec_c, 1), vec_c_imag_high, 16));

    svst2_s16(pg, (int16_t *)c, vec_c);
  }

  return ARMRAL_SUCCESS;
#else
  uint32_t blk_cnt; /* Loop counter */
  int16x8x2_t va;
  int16x8x2_t vb;

  /* Compute 8 complex samples at a time */
  blk_cnt = n >> 3U;

  while (blk_cnt > 0U) {
    // load & separate real/imag (de-interleave 2)
    va = vld2q_s16((const int16_t *)a);
    vb = vld2q_s16((const int16_t *)b);

    /* Increment pointers */
    a += 8;
    b += 8;

    int16x8x2_t res_cplx_i16 = cmplx_mul_split_re_im(va, vb);
    vst2q_s16((int16_t *)c, res_cplx_i16);

    /* Increment pointer */
    c += 8;

    /* Decrement the loop counter */
    blk_cnt--;
  }

  /* Tail */
  blk_cnt = n & 0x7;

  while (blk_cnt > 0U) {
    /* C[2 * i    ] = A[2 * i] * B[2 * i    ] - A[2 * i + 1] * B[2 * i + 1]. */
    /* C[2 * i + 1] = A[2 * i] * B[2 * i + 1] + A[2 * i + 1] * B[2 * i    ]. */

    int16x4x2_t zeros = {{{0, 0, 0, 0}, {0, 0, 0, 0}}};
    int16x4x2_t la = vld2_lane_s16((const int16_t *)a, zeros, 0);
    int16x4x2_t lb = vld2_lane_s16((const int16_t *)b, zeros, 0);
    int32x4x2_t l_out;
    int16x4x2_t l_out_int16;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    // Real component multiplication can't saturate int32, so we don't need
    // saturating logic for this
    l_out.val[0] = vmull_s16(la.val[0], lb.val[0]);
    l_out.val[0] = vmlsl_s16(l_out.val[0], la.val[1], lb.val[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    l_out.val[1] = vqdmull_s16(la.val[0], lb.val[1]);
    l_out.val[1] = vqdmlal_s16(l_out.val[1], la.val[1], lb.val[0]);

    /* Shift >> 15, results in Q15 format */
    l_out_int16.val[0] = vqrshrn_n_s32(l_out.val[0], 15);

    // Shift >> 16 for imaginary component, as we use doubling multiplication
    // and addition to be able to make use of the saturating intrinsics
    l_out_int16.val[1] = vqrshrn_n_s32(l_out.val[1], 16);

    vst2_lane_s16((int16_t *)c, l_out_int16, 0);

    a++;
    b++;
    c++;

    /* Decrement loop counter */
    blk_cnt--;
  }

  return ARMRAL_SUCCESS;
#endif
}
