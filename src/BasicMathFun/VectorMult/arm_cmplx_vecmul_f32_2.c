/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

armral_status armral_cmplx_vecmul_f32_2(uint32_t n,
                                        const float32_t *restrict a_re,
                                        const float32_t *restrict a_im,
                                        const float32_t *restrict b_re,
                                        const float32_t *restrict b_im,
                                        float32_t *c_re, float32_t *c_im) {
#ifdef ARMRAL_ARCH_SVE
  int64_t num_lanes = svcntw();
  svbool_t pg = svptrue_b32();

  int64_t i = 0;
  for (; (i + 4) * num_lanes <= (int64_t)n; i += 4) {
    svfloat32_t vec_a_0_re = svld1_vnum_f32(pg, a_re, i);
    svfloat32_t vec_a_0_im = svld1_vnum_f32(pg, a_im, i);
    svfloat32_t vec_b_0_re = svld1_vnum_f32(pg, b_re, i);
    svfloat32_t vec_b_0_im = svld1_vnum_f32(pg, b_im, i);
    svfloat32_t vec_a_1_re = svld1_vnum_f32(pg, a_re, i + 1);
    svfloat32_t vec_a_1_im = svld1_vnum_f32(pg, a_im, i + 1);
    svfloat32_t vec_b_1_re = svld1_vnum_f32(pg, b_re, i + 1);
    svfloat32_t vec_b_1_im = svld1_vnum_f32(pg, b_im, i + 1);
    svfloat32_t vec_a_2_re = svld1_vnum_f32(pg, a_re, i + 2);
    svfloat32_t vec_a_2_im = svld1_vnum_f32(pg, a_im, i + 2);
    svfloat32_t vec_b_2_re = svld1_vnum_f32(pg, b_re, i + 2);
    svfloat32_t vec_b_2_im = svld1_vnum_f32(pg, b_im, i + 2);
    svfloat32_t vec_a_3_re = svld1_vnum_f32(pg, a_re, i + 3);
    svfloat32_t vec_a_3_im = svld1_vnum_f32(pg, a_im, i + 3);
    svfloat32_t vec_b_3_re = svld1_vnum_f32(pg, b_re, i + 3);
    svfloat32_t vec_b_3_im = svld1_vnum_f32(pg, b_im, i + 3);

    /*Re{C} = Re{A}*Re{B} - Im{A}*Im{B}*/
    /*Im{C} = Re{A}*Im{B} + Im{A}*Re{B}*/
    svfloat32_t vec_c_0_re = svmul_f32_x(pg, vec_a_0_re, vec_b_0_re);
    svfloat32_t vec_c_0_im = svmul_f32_x(pg, vec_a_0_re, vec_b_0_im);
    svfloat32_t vec_c_1_re = svmul_f32_x(pg, vec_a_1_re, vec_b_1_re);
    svfloat32_t vec_c_1_im = svmul_f32_x(pg, vec_a_1_re, vec_b_1_im);
    svfloat32_t vec_c_2_re = svmul_f32_x(pg, vec_a_2_re, vec_b_2_re);
    svfloat32_t vec_c_2_im = svmul_f32_x(pg, vec_a_2_re, vec_b_2_im);
    svfloat32_t vec_c_3_re = svmul_f32_x(pg, vec_a_3_re, vec_b_3_re);
    svfloat32_t vec_c_3_im = svmul_f32_x(pg, vec_a_3_re, vec_b_3_im);

    vec_c_0_re = svmls_f32_x(pg, vec_c_0_re, vec_a_0_im, vec_b_0_im);
    vec_c_0_im = svmla_f32_x(pg, vec_c_0_im, vec_a_0_im, vec_b_0_re);
    vec_c_1_re = svmls_f32_x(pg, vec_c_1_re, vec_a_1_im, vec_b_1_im);
    vec_c_1_im = svmla_f32_x(pg, vec_c_1_im, vec_a_1_im, vec_b_1_re);
    vec_c_2_re = svmls_f32_x(pg, vec_c_2_re, vec_a_2_im, vec_b_2_im);
    vec_c_2_im = svmla_f32_x(pg, vec_c_2_im, vec_a_2_im, vec_b_2_re);
    vec_c_3_re = svmls_f32_x(pg, vec_c_3_re, vec_a_3_im, vec_b_3_im);
    vec_c_3_im = svmla_f32_x(pg, vec_c_3_im, vec_a_3_im, vec_b_3_re);

    svst1_vnum_f32(pg, c_re, i + 0, vec_c_0_re);
    svst1_vnum_f32(pg, c_im, i + 0, vec_c_0_im);
    svst1_vnum_f32(pg, c_re, i + 1, vec_c_1_re);
    svst1_vnum_f32(pg, c_im, i + 1, vec_c_1_im);
    svst1_vnum_f32(pg, c_re, i + 2, vec_c_2_re);
    svst1_vnum_f32(pg, c_im, i + 2, vec_c_2_im);
    svst1_vnum_f32(pg, c_re, i + 3, vec_c_3_re);
    svst1_vnum_f32(pg, c_im, i + 3, vec_c_3_im);
  }

  for (; i * num_lanes < (int64_t)n; i++) {
    pg = svwhilelt_b32(i * num_lanes, (int64_t)n);
    svfloat32_t vec_a_re = svld1_vnum_f32(pg, a_re, i);
    svfloat32_t vec_a_im = svld1_vnum_f32(pg, a_im, i);
    svfloat32_t vec_b_re = svld1_vnum_f32(pg, b_re, i);
    svfloat32_t vec_b_im = svld1_vnum_f32(pg, b_im, i);

    /*Re{C} = Re{A}*Re{B} - Im{A}*Im{B}*/
    svfloat32_t vec_c_re = svmul_f32_x(pg, vec_a_re, vec_b_re);
    vec_c_re = svmls_f32_x(pg, vec_c_re, vec_a_im, vec_b_im);

    /*Im{C} = Re{A}*Im{B} + Im{A}*Re{B}*/
    svfloat32_t vec_c_im = svmul_f32_x(pg, vec_a_re, vec_b_im);
    vec_c_im = svmla_f32_x(pg, vec_c_im, vec_a_im, vec_b_re);

    svst1_vnum_f32(pg, c_re, i, vec_c_re);
    svst1_vnum_f32(pg, c_im, i, vec_c_im);
  }

  return ARMRAL_SUCCESS;
#else
  uint32_t blk_cnt; /* Loop counter */
  float32_t re_a;
  float32_t im_a;
  float32_t re_b;
  float32_t im_b; /* Temporary variables to store real and imaginary values */

  float32x4x2_t vc_re;
  float32x4x2_t vc_im;
  float32x4_t va_re[2];
  float32x4_t va_im[2];
  float32x4_t vb_re[2];
  float32x4_t vb_im[2];

  /* Compute 8 outputs at a time */
  blk_cnt = n >> 3U;

  while (blk_cnt > 0U) {
    va_re[0] = vld1q_f32(a_re);
    va_re[1] = vld1q_f32(a_re + 4);
    va_im[0] = vld1q_f32(a_im);
    va_im[1] = vld1q_f32(a_im + 4);

    vb_re[0] = vld1q_f32(b_re);
    vb_re[1] = vld1q_f32(b_re + 4);
    vb_im[0] = vld1q_f32(b_im);
    vb_im[1] = vld1q_f32(b_im + 4);

    /* Increment pointers */
    a_re += 8;
    a_im += 8;
    b_re += 8;
    b_im += 8;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    vc_re.val[0] = vmulq_f32(va_re[0], vb_re[0]);
    vc_re.val[1] = vmulq_f32(va_re[1], vb_re[1]);

    vc_re.val[0] = vfmsq_f32(vc_re.val[0], va_im[0], vb_im[0]);
    vc_re.val[1] = vfmsq_f32(vc_re.val[1], va_im[1], vb_im[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    vc_im.val[0] = vmulq_f32(va_re[0], vb_im[0]);
    vc_im.val[1] = vmulq_f32(va_re[1], vb_im[1]);

    vc_im.val[0] = vfmaq_f32(vc_im.val[0], va_im[0], vb_re[0]);
    vc_im.val[1] = vfmaq_f32(vc_im.val[1], va_im[1], vb_re[1]);

    vst1q_f32(c_re, vc_re.val[0]);
    c_re += 4;
    vst1q_f32(c_re, vc_re.val[1]);
    c_re += 4;
    vst1q_f32(c_im, vc_im.val[0]);
    c_im += 4;
    vst1q_f32(c_im, vc_im.val[1]);
    c_im += 4;

    /* Decrement the loop counter */
    blk_cnt--;
  }

  /* Tail */
  blk_cnt = n & 7;

  while (blk_cnt > 0U) {
    /* C[2 * i    ] = A[2 * i] * B[2 * i    ] - A[2 * i + 1] * B[2 * i + 1]. */
    /* C[2 * i + 1] = A[2 * i] * B[2 * i + 1] + A[2 * i + 1] * B[2 * i    ]. */
    re_a = *a_re++;
    im_a = *a_im++;
    re_b = *b_re++;
    im_b = *b_im++;

    /* store result in destination buffer. */
    *c_re++ = (re_a * re_b) - (im_a * im_b);
    *c_im++ = (re_a * im_b) + (im_a * re_b);

    /* Decrement loop counter */
    blk_cnt--;
  }
  return ARMRAL_SUCCESS;
#endif
}
