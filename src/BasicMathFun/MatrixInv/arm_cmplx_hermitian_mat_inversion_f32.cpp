/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include "cmplx_hermitian_mat_inversion_f32.hpp"
#include "utils/cmplx_arith_f32.hpp"

#include <assert.h>

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

namespace armral::cmplx_herm_mat_inv {

#ifdef ARMRAL_ARCH_SVE
static void sve_invert_hermitian_matrix2x2(svfloat32_t row1, svfloat32_t row2,
                                           svfloat32_t *out_row1,
                                           svfloat32_t *out_row2) {
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svfloat64_t row1_64 = svreinterpret_f64_f32(svneg_f32_x(p4, row1));
  svfloat64_t row2_64 = svreinterpret_f64_f32(row2);

  // Determine adjugate
  svfloat32_t tmp = svreinterpret_f32_f64(svtrn1_f64(row2_64, row1_64));
  svfloat32_t adj2 = svneg_f32_x(p4, tmp);
  svfloat32_t adj1 = svreinterpret_f32_f64(svtrn2_f64(row2_64, row1_64));

  // For the determinant, we take advantage of the diagonals being
  // real and the upper triangular elements being conjugate to the lower
  // diagonals. So if you multiply the first row of the input matrix by the 1st
  // row of its adjugate, you get
  //
  //   {a_re * d_re, 0*0, -b_re * b_re, -b_im * b_im}.
  //
  // Which accumulated, just so happens to be the determinant of a 2x2 hermitian
  // matrix
  //
  //   a_re * d_re - |b|^2.
  //
  svfloat32_t det_vec = svmul_f32_x(p4, row1, adj1);
  float32_t inv_det = 1 / svaddv_f32(p4, det_vec);

  // 1/det * adj(A)
  *out_row1 = svmul_n_f32_x(p4, adj1, inv_det);
  *out_row2 = svmul_n_f32_x(p4, adj2, inv_det);
}
#endif

template<>
void invert_hermitian_matrix<2>(const armral_cmplx_f32_t *__restrict p_src,
                                armral_cmplx_f32_t *p_dst) {
#ifdef ARMRAL_ARCH_SVE
  // Setup some convenient variables
  const float32_t *src = (const float32_t *)p_src;
  float32_t *dst = (float32_t *)p_dst;
  svbool_t plower4 = svptrue_pat_b32(SV_VL4);

  // Load in the matrix
  svfloat32_t row1 = svld1_f32(plower4, src);
  svfloat32_t row2 = svld1_f32(plower4, src + 4);

  svfloat32_t out_row1;
  svfloat32_t out_row2;
  sve_invert_hermitian_matrix2x2(row1, row2, &out_row1, &out_row2);

  svst1_f32(plower4, dst, out_row1);
  svst1_f32(plower4, dst + 4, out_row2);

#else
  float32x2_t mat[4];
  mat[0] = vld1_f32((const float32_t *)p_src);
  mat[1] = vld1_f32((const float32_t *)(p_src + 1));
  mat[2] = vld1_f32((const float32_t *)(p_src + 2));
  mat[3] = vld1_f32((const float32_t *)(p_src + 3));

  /* Real Determinant of matrix A*/
  float32x2_t tmp = vmul_f32(mat[0], mat[3]);
  const float32_t det = vaddv_f32(vfms_f32(tmp, mat[1], mat[1]));
  const float32_t inv_det = 1 / det;

  vst1_f32((float32_t *)p_dst++, vmul_n_f32(mat[3], inv_det));
  vst1_f32((float32_t *)p_dst++, vmul_n_f32(mat[1], -inv_det));
  vst1_f32((float32_t *)p_dst++, vmul_n_f32(mat[2], -inv_det));
  vst1_f32((float32_t *)p_dst, vmul_n_f32(mat[0], inv_det));
#endif
}

#if ARMRAL_ARCH_SVE >= 2
static svfloat32_t sve_conjugate_f32(const svfloat32_t in) {
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svfloat64_t in_f64 = svreinterpret_f64_f32(in);
  return svreinterpret_f32_f64(svneg_f64_x(p4, in_f64));
}
#endif

#if ARMRAL_ARCH_SVE >= 2
static void sve_invert_batch_hermitian_matrix_4x_2x2(
    svfloat32_t a0, svfloat32_t a1, svfloat32_t b0, svfloat32_t b1,
    svfloat32_t c0, svfloat32_t c1, svfloat32_t d0, svfloat32_t d1,
    svfloat32_t *out_a0, svfloat32_t *out_a1, svfloat32_t *out_b0,
    svfloat32_t *out_b1, svfloat32_t *out_c0, svfloat32_t *out_c1,
    svfloat32_t *out_d0, svfloat32_t *out_d1) {

  svbool_t plower4 = svptrue_pat_b32(SV_VL4);

  // Create the vector
  //
  //   {a0*d0 - b0_re*b0_re, -b0_im*b0_im,
  //    a1*d1 - b1_re*b1_re, -b1_im*b1_im}
  //
  svfloat32_t ad0 = svmul_f32_x(plower4, a0, d0);
  svfloat32_t ad1 = svmul_f32_x(plower4, a1, d1);
  svfloat32_t ad_b20 = svmls_f32_x(plower4, ad0, b0, b0);
  svfloat32_t ad_b21 = svmls_f32_x(plower4, ad1, b1, b1);

  // This calculates the determinant twice for both matrices, storing the
  // determinant for the 1st matrix in the 2 leftmost buckets, and the
  // determinant for the 2nd matrix in the two rightmost.
  svfloat32_t det0 = svaddp_f32_x(plower4, ad_b20, ad_b20);
  svfloat32_t det1 = svaddp_f32_x(plower4, ad_b21, ad_b21);

  // Reciprocal of the determinant. Uses Newton-Raphson which gives better
  // throughput on some microarchitectures.
  //
  //    estimate   = recipe(val);
  //    reciprocal = recips(val, estimate) * estimate;
  //
  svfloat32_t inv_det0_e = svrecpe_f32(det0);
  svfloat32_t inv_det0_s = svrecps_f32(det0, inv_det0_e);
  svfloat32_t inv_det0 = svmul_f32_x(plower4, inv_det0_s, inv_det0_e);
  svfloat32_t inv_det1_e = svrecpe_f32(det1);
  svfloat32_t inv_det1_s = svrecps_f32(det1, inv_det1_e);
  svfloat32_t inv_det1 = svmul_f32_x(plower4, inv_det1_s, inv_det1_e);

  svfloat32_t zeroes = svdup_f32(0);
  *out_a0 = svmul_f32_x(plower4, inv_det0, d0);
  *out_b0 = svmls_f32_x(plower4, zeroes, inv_det0, b0);
  *out_c0 = svmls_f32_x(plower4, zeroes, inv_det0, c0);
  *out_d0 = svmul_f32_x(plower4, inv_det0, a0);
  *out_a1 = svmul_f32_x(plower4, inv_det1, d1);
  *out_b1 = svmls_f32_x(plower4, zeroes, inv_det1, b1);
  *out_c1 = svmls_f32_x(plower4, zeroes, inv_det1, c1);
  *out_d1 = svmul_f32_x(plower4, inv_det1, a1);
}
#endif

#if ARMRAL_ARCH_SVE >= 2
static void invert_batch_hermitian_matrix_2x2_impl(svfloat32_t a, svfloat32_t b,
                                                   svfloat32_t c, svfloat32_t d,
                                                   svfloat32_t *out_a,
                                                   svfloat32_t *out_b,
                                                   svfloat32_t *out_c,
                                                   svfloat32_t *out_d) {
  svbool_t plower4 = svptrue_pat_b32(SV_VL4);

  // Create the vector a*d - b*b.
  // The c vector can be safely ignored since the matrix is hermitian.
  svfloat32_t ad = svmul_f32_x(plower4, a, d);
  svfloat32_t ad_b2 = svmls_f32_x(plower4, ad, b, b);

  // This calculates the determinant twice for both matrices, storing the
  // determinant for the 1st matrix in the 2 leftmost buckets, and the
  // determinant for the 2nd matrix in the two rightmost.
  svfloat32_t det = svaddp_f32_x(plower4, ad_b2, ad_b2);

  svfloat32_t ones = svdup_f32(1);
  svfloat32_t zeroes = svdup_f32(0);
  svfloat32_t inv_det = svdiv_f32_x(plower4, ones, det);

  *out_a = svmul_f32_x(plower4, inv_det, d);
  *out_b = svmls_f32_x(plower4, zeroes, inv_det, b);
  *out_c = svmls_f32_x(plower4, zeroes, inv_det, c);
  *out_d = svmul_f32_x(plower4, inv_det, a);
}
#else
static void invert_batch_hermitian_matrix_2x2_impl(float32x4_t a, float32x4_t b,
                                                   float32x4_t c, float32x4_t d,
                                                   float32x4_t *out_a,
                                                   float32x4_t *out_b,
                                                   float32x4_t *out_c,
                                                   float32x4_t *out_d) {
  float32x4_t det = vmulq_f32(a, d);
  det = vfmsq_f32(det, b, b);
  det = vpaddq_f32(det, det);
  det = 1 / det;

  float32x4_t inv_det = vzip1q_f32(det, det);
  float32x4_t inv01 = vmulq_f32(b, -inv_det);

  *out_a = vmulq_f32(d, inv_det);
  *out_b = inv01;
  *out_c = vreinterpretq_f32_f64(vnegq_f64(vreinterpretq_f64_f32(inv01)));
  *out_d = vmulq_f32(a, inv_det);
}
#endif

static armral_status __attribute__((noinline, flatten))
invert_batch_hermitian_matrix_2x2(uint32_t num_mats,
                                  const armral_cmplx_f32_t *__restrict p_src,
                                  armral_cmplx_f32_t *p_dst) {

  const float32_t *src = (const float32_t *)p_src;
  float32_t *dst = (float32_t *)p_dst;
  uint32_t stride = num_mats * 2;

#if ARMRAL_ARCH_SVE >= 2
  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i += 2) {
    svbool_t plower4 = svptrue_pat_b32(SV_VL4);
    svfloat32_t a = svld1_f32(plower4, src);
    svfloat32_t b = svld1_f32(plower4, src + 1 * stride);
    svfloat32_t c = svld1_f32(plower4, src + 2 * stride);
    svfloat32_t d = svld1_f32(plower4, src + 3 * stride);

    svfloat32_t e;
    svfloat32_t f;
    svfloat32_t g;
    svfloat32_t h;
    invert_batch_hermitian_matrix_2x2_impl(a, b, c, d, &e, &f, &g, &h);

    svst1_f32(plower4, dst, e);
    svst1_f32(plower4, dst + 1 * stride, f);
    svst1_f32(plower4, dst + 2 * stride, g);
    svst1_f32(plower4, dst + 3 * stride, h);

    src += 4;
    dst += 4;
  }
#else
  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i += 2) {
    float32x4_t a = vld1q_f32(src);
    float32x4_t b = vld1q_f32(src + 1 * stride);
    float32x4_t c = vld1q_f32(src + 2 * stride);
    float32x4_t d = vld1q_f32(src + 3 * stride);

    float32x4_t e;
    float32x4_t f;
    float32x4_t g;
    float32x4_t h;
    invert_batch_hermitian_matrix_2x2_impl(a, b, c, d, &e, &f, &g, &h);

    vst1q_f32(dst, e);
    vst1q_f32(dst + 1 * stride, f);
    vst1q_f32(dst + 2 * stride, g);
    vst1q_f32(dst + 3 * stride, h);

    src += 4;
    dst += 4;
  }
#endif
  return ARMRAL_SUCCESS;
}

static armral_status __attribute__((noinline, flatten))
invert_batch_hermitian_matrix_2x2_pa(
    uint32_t num_mats, const armral_cmplx_f32_t **__restrict p_srcs,
    armral_cmplx_f32_t **__restrict p_dsts) {

  const float32_t *__restrict src_00 = (const float32_t *)p_srcs[0];
  const float32_t *__restrict src_01 = (const float32_t *)p_srcs[1];
  const float32_t *__restrict src_10 = (const float32_t *)p_srcs[2];
  const float32_t *__restrict src_11 = (const float32_t *)p_srcs[3];

  float32_t *__restrict dst_00 = (float32_t *)p_dsts[0];
  float32_t *__restrict dst_01 = (float32_t *)p_dsts[1];
  float32_t *__restrict dst_10 = (float32_t *)p_dsts[2];
  float32_t *__restrict dst_11 = (float32_t *)p_dsts[3];

#if ARMRAL_ARCH_SVE >= 2
  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i += 2) {
    svbool_t plower4 = svptrue_pat_b32(SV_VL4);
    svfloat32_t a = svld1_f32(plower4, src_00);
    svfloat32_t b = svld1_f32(plower4, src_01);
    svfloat32_t c = svld1_f32(plower4, src_10);
    svfloat32_t d = svld1_f32(plower4, src_11);

    svfloat32_t e;
    svfloat32_t f;
    svfloat32_t g;
    svfloat32_t h;
    invert_batch_hermitian_matrix_2x2_impl(a, b, c, d, &e, &f, &g, &h);

    svst1_f32(plower4, dst_00, e);
    svst1_f32(plower4, dst_01, f);
    svst1_f32(plower4, dst_10, g);
    svst1_f32(plower4, dst_11, h);

    src_00 += 4;
    src_01 += 4;
    src_10 += 4;
    src_11 += 4;

    dst_00 += 4;
    dst_01 += 4;
    dst_10 += 4;
    dst_11 += 4;
  }
#else
  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i += 2) {
    float32x4_t a = vld1q_f32(src_00);
    float32x4_t b = vld1q_f32(src_01);
    float32x4_t c = vld1q_f32(src_10);
    float32x4_t d = vld1q_f32(src_11);

    float32x4_t e;
    float32x4_t f;
    float32x4_t g;
    float32x4_t h;
    invert_batch_hermitian_matrix_2x2_impl(a, b, c, d, &e, &f, &g, &h);

    vst1q_f32(dst_00, e);
    vst1q_f32(dst_01, f);
    vst1q_f32(dst_10, g);
    vst1q_f32(dst_11, h);

    src_00 += 4;
    src_01 += 4;
    src_10 += 4;
    src_11 += 4;

    dst_00 += 4;
    dst_01 += 4;
    dst_10 += 4;
    dst_11 += 4;
  }
#endif
  return ARMRAL_SUCCESS;
}

#ifndef ARMRAL_ARCH_SVE
static void invert_hermitian_matrix2x2_packed(const float32x4_t adb,
                                              float32x4_t *ehf) {
  float32x4_t packed_mat = vnegq_f32(adb);
  packed_mat[0] = adb[1];
  packed_mat[1] = adb[0];

  const float32x4_t v18 = vmulq_f32(packed_mat, adb);
  float32x4_t v17 = vpaddq_f32(v18, v18);
  const float32_t det = v17[1] + v18[0];

  v17 = vdupq_n_f32(1 / det);
  *ehf = vmulq_f32(packed_mat, v17);
}
#endif

#ifdef ARMRAL_ARCH_SVE
static void sve_mat_mla2x2(svfloat32_t a0, svfloat32_t a1, svfloat32_t b0,
                           svfloat32_t b1, svfloat32_t c0, svfloat32_t c1,
                           svfloat32_t *out0, svfloat32_t *out1) {

  a0 = svcmla_lane_f32(a0, c0, b0, 0, 0);
  a0 = svcmla_lane_f32(a0, c0, b0, 0, 90);
  a0 = svcmla_lane_f32(a0, c1, b0, 1, 0);
  a0 = svcmla_lane_f32(a0, c1, b0, 1, 90);
  a1 = svcmla_lane_f32(a1, c0, b1, 0, 0);
  a1 = svcmla_lane_f32(a1, c0, b1, 0, 90);
  a1 = svcmla_lane_f32(a1, c1, b1, 1, 0);
  a1 = svcmla_lane_f32(a1, c1, b1, 1, 90);

  *out0 = a0;
  *out1 = a1;
}
#endif

// Helper functions for scalar implementation of 3x3 matrix inversion
static inline armral_cmplx_f32_t
scal_3x3_minor_cmplx_f32(const armral_cmplx_f32_t a, const armral_cmplx_f32_t b,
                         const armral_cmplx_f32_t c,
                         const armral_cmplx_f32_t d) {
  return scal_sub_cmplx_f32(scal_mul_cmplx_f32(a, d), scal_mul_cmplx_f32(b, c));
}

static inline armral_cmplx_f32_t
scal_3x3_minor_diag_cmplx_f32(const armral_cmplx_f32_t a,
                              const armral_cmplx_f32_t b,
                              const armral_cmplx_f32_t d) {
  return {(a.re * d.re) - scal_mod2_cmplx_f32(b).re, 0.0F};
}

template<>
void invert_hermitian_matrix<3>(const armral_cmplx_f32_t *__restrict p_src,
                                armral_cmplx_f32_t *p_dst) {
  // A scalar implementation
  // Can Neon or SVE really do better than that for a single matrix?
  armral_cmplx_f32_t a00 = p_src[0];
  armral_cmplx_f32_t a01 = p_src[1];
  armral_cmplx_f32_t a02 = p_src[2];
  armral_cmplx_f32_t a11 = p_src[4];
  armral_cmplx_f32_t a12 = p_src[5];
  armral_cmplx_f32_t a22 = p_src[8];

  // Compute cofactors
  armral_cmplx_f32_t adj00 = scal_3x3_minor_diag_cmplx_f32(a11, a12, a22);
  armral_cmplx_f32_t adj11 = scal_3x3_minor_diag_cmplx_f32(a00, a02, a22);
  armral_cmplx_f32_t adj22 = scal_3x3_minor_diag_cmplx_f32(a00, a01, a11);

  armral_cmplx_f32_t c01 = {a01.re, -a01.im};
  armral_cmplx_f32_t c12 = {a12.re, -a12.im};

  armral_cmplx_f32_t adj01 = scal_3x3_minor_cmplx_f32(a02, a01, a22, c12);
  armral_cmplx_f32_t adj02 = scal_3x3_minor_cmplx_f32(a01, a02, a11, a12);
  armral_cmplx_f32_t adj12 = scal_3x3_minor_cmplx_f32(a02, a00, a12, c01);
  armral_cmplx_f32_t adj10 = {adj01.re, -adj01.im};
  armral_cmplx_f32_t adj20 = {adj02.re, -adj02.im};
  armral_cmplx_f32_t adj21 = {adj12.re, -adj12.im};

  // Determinant (real): A_{0:} * adj(A)_{:0}
  float32_t det00 = a00.re * adj00.re;
  float32_t det11 = scal_mul_cmplx_f32(a01, adj10).re;
  float32_t det22 = scal_mul_cmplx_f32(a02, adj20).re;

  float32_t inv_det = 1.0F / (det00 + det11 + det22);

  // Write into output array
  p_dst[0] = {adj00.re * inv_det, adj00.im * inv_det};
  p_dst[1] = {adj01.re * inv_det, adj01.im * inv_det};
  p_dst[2] = {adj02.re * inv_det, adj02.im * inv_det};
  p_dst[3] = {adj10.re * inv_det, adj10.im * inv_det};
  p_dst[4] = {adj11.re * inv_det, adj11.im * inv_det};
  p_dst[5] = {adj12.re * inv_det, adj12.im * inv_det};
  p_dst[6] = {adj20.re * inv_det, adj20.im * inv_det};
  p_dst[7] = {adj21.re * inv_det, adj21.im * inv_det};
  p_dst[8] = {adj22.re * inv_det, adj22.im * inv_det};
}

#if ARMRAL_ARCH_SVE >= 2
// some helper functions for SVE implementation of 3x3 inversion
static inline svfloat32_t sve_mul_cmplx_f32(svfloat32_t x, svfloat32_t y) {
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svfloat32_t ret = svdup_n_f32(0.0F);
  ret = svcmla_f32_x(p4, ret, x, y, 0);
  ret = svcmla_f32_x(p4, ret, x, y, 90);
  return ret;
}

static inline svfloat32_t sve_minor_cmplx_f32(const svfloat32_t a,
                                              const svfloat32_t b,
                                              const svfloat32_t c,
                                              const svfloat32_t d) {
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svfloat32_t ret = svdup_n_f32(0.0F);
  // It could be beneficial to interleave computation of each minor to
  // make better use of pipelines...
  // ret = a * d - c * b
  ret = svcmla_f32_x(p4, ret, a, d, 0);
  ret = svcmla_f32_x(p4, ret, a, d, 90);
  ret = svcmla_f32_x(p4, ret, c, b, 180);
  ret = svcmla_f32_x(p4, ret, c, b, 270);
  return ret;
}

static inline svfloat32_t sve_recip_cmplx_f32(const svfloat32_t a) {
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svfloat32_t are = svtrn1_f32(a, a);
  svfloat32_t aim = svtrn2_f32(a, a);

  svfloat32_t abs2 = svdup_n_f32(0.0F);
  abs2 = svmla_f32_x(p4, abs2, are, are);
  abs2 = svmla_f32_x(p4, abs2, aim, aim);

  return svdiv_f32_x(p4, sve_conjugate_f32(a), abs2);
}
#endif

#if ARMRAL_ARCH_SVE >= 2
// 12 entries here for input and output since packed 3x3 hermitian
// has 6 entries, unrolled by two.
static void invert_batch_hermitian_matrix_3x3_impl(
    svfloat32_t a0_0, svfloat32_t a0_1, svfloat32_t a1_0, svfloat32_t a1_1,
    svfloat32_t a2_0, svfloat32_t a2_1, svfloat32_t a3_0, svfloat32_t a3_1,
    svfloat32_t a4_0, svfloat32_t a4_1, svfloat32_t a5_0, svfloat32_t a5_1,
    svfloat32_t *out_00_lo, svfloat32_t *out_00_hi, svfloat32_t *out_01_lo,
    svfloat32_t *out_01_hi, svfloat32_t *out_02_lo, svfloat32_t *out_02_hi,
    svfloat32_t *out_10_lo, svfloat32_t *out_10_hi, svfloat32_t *out_11_lo,
    svfloat32_t *out_11_hi, svfloat32_t *out_12_lo, svfloat32_t *out_12_hi,
    svfloat32_t *out_20_lo, svfloat32_t *out_20_hi, svfloat32_t *out_21_lo,
    svfloat32_t *out_21_hi, svfloat32_t *out_22_lo, svfloat32_t *out_22_hi) {

  // here a0, a1, ..., a5 denote consecutive entries in a (row-major)
  // packed 3x3 hermitian matrix (6 entries).
  // The _0 suffix represents the for first 2 matrices, _1 the last 2 matrices.

  // for the sake of simplicity we build off diagonal elements explicitly
  // (using conjugates) instead of adjusting the rotations in the cmlas.
  svfloat32_t c1_0 = sve_conjugate_f32(a1_0);
  svfloat32_t c2_0 = sve_conjugate_f32(a2_0);
  svfloat32_t c4_0 = sve_conjugate_f32(a4_0);
  svfloat32_t c1_1 = sve_conjugate_f32(a1_1);
  svfloat32_t c2_1 = sve_conjugate_f32(a2_1);
  svfloat32_t c4_1 = sve_conjugate_f32(a4_1);

  // low
  svfloat32_t adj00_0 = sve_minor_cmplx_f32(a3_0, a4_0, c4_0, a5_0);
  svfloat32_t adj11_0 = sve_minor_cmplx_f32(a0_0, a2_0, c2_0, a5_0);
  svfloat32_t adj22_0 = sve_minor_cmplx_f32(a0_0, a1_0, c1_0, a3_0);
  svfloat32_t adj10_0 = sve_minor_cmplx_f32(c1_0, a4_0, c2_0, a5_0);
  svfloat32_t adj20_0 = sve_minor_cmplx_f32(c1_0, a3_0, c2_0, c4_0);
  svfloat32_t adj01_0 = sve_minor_cmplx_f32(a1_0, a2_0, c4_0, a5_0);
  svfloat32_t adj21_0 = sve_minor_cmplx_f32(a0_0, a1_0, c2_0, c4_0);
  svfloat32_t adj02_0 = sve_minor_cmplx_f32(a1_0, a2_0, a3_0, a4_0);
  svfloat32_t adj12_0 = sve_minor_cmplx_f32(a0_0, a2_0, c1_0, a4_0);
  // high
  svfloat32_t adj00_1 = sve_minor_cmplx_f32(a3_1, a4_1, c4_1, a5_1);
  svfloat32_t adj11_1 = sve_minor_cmplx_f32(a0_1, a2_1, c2_1, a5_1);
  svfloat32_t adj22_1 = sve_minor_cmplx_f32(a0_1, a1_1, c1_1, a3_1);
  svfloat32_t adj10_1 = sve_minor_cmplx_f32(c1_1, a4_1, c2_1, a5_1);
  svfloat32_t adj20_1 = sve_minor_cmplx_f32(c1_1, a3_1, c2_1, c4_1);
  svfloat32_t adj01_1 = sve_minor_cmplx_f32(a1_1, a2_1, c4_1, a5_1);
  svfloat32_t adj21_1 = sve_minor_cmplx_f32(a0_1, a1_1, c2_1, c4_1);
  svfloat32_t adj02_1 = sve_minor_cmplx_f32(a1_1, a2_1, a3_1, a4_1);
  svfloat32_t adj12_1 = sve_minor_cmplx_f32(a0_1, a2_1, c1_1, a4_1);

  // Compute cofactors (apply negative signs)
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  adj01_0 = svneg_x(p4, adj01_0);
  adj10_0 = svneg_x(p4, adj10_0);
  adj12_0 = svneg_x(p4, adj12_0);
  adj21_0 = svneg_x(p4, adj21_0);
  adj01_1 = svneg_x(p4, adj01_1);
  adj10_1 = svneg_x(p4, adj10_1);
  adj12_1 = svneg_x(p4, adj12_1);
  adj21_1 = svneg_x(p4, adj21_1);

  // Determinant (real): A_{0:} * adj(A)_{:0}
  svfloat32_t det0_0 = sve_mul_cmplx_f32(a0_0, adj00_0);
  svfloat32_t det1_0 = sve_mul_cmplx_f32(a1_0, adj10_0);
  svfloat32_t det2_0 = sve_mul_cmplx_f32(a2_0, adj20_0);
  svfloat32_t det0_1 = sve_mul_cmplx_f32(a0_1, adj00_1);
  svfloat32_t det1_1 = sve_mul_cmplx_f32(a1_1, adj10_1);
  svfloat32_t det2_1 = sve_mul_cmplx_f32(a2_1, adj20_1);

  svfloat32_t det_0 = svadd_f32_x(p4, svadd_f32_x(p4, det0_0, det1_0), det2_0);
  svfloat32_t det_1 = svadd_f32_x(p4, svadd_f32_x(p4, det0_1, det1_1), det2_1);

  svfloat32_t inv_det_0 = sve_recip_cmplx_f32(det_0);
  svfloat32_t inv_det_1 = sve_recip_cmplx_f32(det_1);

  // 1/det * adj(A)
  *out_00_lo = sve_mul_cmplx_f32(adj00_0, inv_det_0);
  *out_00_hi = sve_mul_cmplx_f32(adj00_1, inv_det_1);
  *out_01_lo = sve_mul_cmplx_f32(adj01_0, inv_det_0);
  *out_01_hi = sve_mul_cmplx_f32(adj01_1, inv_det_1);
  *out_02_lo = sve_mul_cmplx_f32(adj02_0, inv_det_0);
  *out_02_hi = sve_mul_cmplx_f32(adj02_1, inv_det_1);
  *out_10_lo = sve_mul_cmplx_f32(adj10_0, inv_det_0);
  *out_10_hi = sve_mul_cmplx_f32(adj10_1, inv_det_1);
  *out_11_lo = sve_mul_cmplx_f32(adj11_0, inv_det_0);
  *out_11_hi = sve_mul_cmplx_f32(adj11_1, inv_det_1);
  *out_12_lo = sve_mul_cmplx_f32(adj12_0, inv_det_0);
  *out_12_hi = sve_mul_cmplx_f32(adj12_1, inv_det_1);
  *out_20_lo = sve_mul_cmplx_f32(adj20_0, inv_det_0);
  *out_20_hi = sve_mul_cmplx_f32(adj20_1, inv_det_1);
  *out_21_lo = sve_mul_cmplx_f32(adj21_0, inv_det_0);
  *out_21_hi = sve_mul_cmplx_f32(adj21_1, inv_det_1);
  *out_22_lo = sve_mul_cmplx_f32(adj22_0, inv_det_0);
  *out_22_hi = sve_mul_cmplx_f32(adj22_1, inv_det_1);
}
#else
static void invert_batch_hermitian_matrix_3x3_impl(
    float32x4_t a0_0, float32x4_t a0_1, float32x4_t a1_0, float32x4_t a1_1,
    float32x4_t a2_0, float32x4_t a2_1, float32x4_t a3_0, float32x4_t a3_1,
    float32x4_t a4_0, float32x4_t a4_1, float32x4_t a5_0, float32x4_t a5_1,
    float32x4_t *out_00_lo, float32x4_t *out_00_hi, float32x4_t *out_01_lo,
    float32x4_t *out_01_hi, float32x4_t *out_02_lo, float32x4_t *out_02_hi,
    float32x4_t *out_10_lo, float32x4_t *out_10_hi, float32x4_t *out_11_lo,
    float32x4_t *out_11_hi, float32x4_t *out_12_lo, float32x4_t *out_12_hi,
    float32x4_t *out_20_lo, float32x4_t *out_20_hi, float32x4_t *out_21_lo,
    float32x4_t *out_21_hi, float32x4_t *out_22_lo, float32x4_t *out_22_hi) {

  // Do not pack entries, we already pass the upper triangular part.

  // here a0, a1, ..., a5 denote consecutive entries in a (row-major)
  // packed 3x3 hermitian matrix (6 entries).
  // The _0 suffix represents the for first 2 matrices, _1 the last 2 matrices.

  // for the sake of simplicity we build off diagonal elements explicitly
  // (using conjugates) instead of adjusting the rotations in the cmlas.
  float32x4_t c1_0 = neon_conjugate_f32(a1_0);
  float32x4_t c2_0 = neon_conjugate_f32(a2_0);
  float32x4_t c4_0 = neon_conjugate_f32(a4_0);
  float32x4_t c1_1 = neon_conjugate_f32(a1_1);
  float32x4_t c2_1 = neon_conjugate_f32(a2_1);
  float32x4_t c4_1 = neon_conjugate_f32(a4_1);

  // Compute cofactors and apply negative signs
  // low
  float32x4_t adj00_0 = neon_minor_cmplx_f32<false>(a3_0, a4_0, c4_0, a5_0);
  float32x4_t adj11_0 = neon_minor_cmplx_f32<false>(a0_0, a2_0, c2_0, a5_0);
  float32x4_t adj22_0 = neon_minor_cmplx_f32<false>(a0_0, a1_0, c1_0, a3_0);
  float32x4_t adj10_0 = neon_minor_cmplx_f32<false>(c2_0, a5_0, c1_0, a4_0);
  float32x4_t adj20_0 = neon_minor_cmplx_f32<false>(c1_0, a3_0, c2_0, c4_0);
  float32x4_t adj01_0 = neon_minor_cmplx_f32<false>(c4_0, a5_0, a1_0, a2_0);
  float32x4_t adj21_0 = neon_minor_cmplx_f32<false>(c2_0, c4_0, a0_0, a1_0);
  float32x4_t adj02_0 = neon_minor_cmplx_f32<false>(a1_0, a2_0, a3_0, a4_0);
  float32x4_t adj12_0 = neon_minor_cmplx_f32<false>(c1_0, a4_0, a0_0, a2_0);
  // high
  float32x4_t adj00_1 = neon_minor_cmplx_f32<false>(a3_1, a4_1, c4_1, a5_1);
  float32x4_t adj11_1 = neon_minor_cmplx_f32<false>(a0_1, a2_1, c2_1, a5_1);
  float32x4_t adj22_1 = neon_minor_cmplx_f32<false>(a0_1, a1_1, c1_1, a3_1);
  float32x4_t adj10_1 = neon_minor_cmplx_f32<false>(c2_1, a5_1, c1_1, a4_1);
  float32x4_t adj20_1 = neon_minor_cmplx_f32<false>(c1_1, a3_1, c2_1, c4_1);
  float32x4_t adj01_1 = neon_minor_cmplx_f32<false>(c4_1, a5_1, a1_1, a2_1);
  float32x4_t adj21_1 = neon_minor_cmplx_f32<false>(c2_1, c4_1, a0_1, a1_1);
  float32x4_t adj02_1 = neon_minor_cmplx_f32<false>(a1_1, a2_1, a3_1, a4_1);
  float32x4_t adj12_1 = neon_minor_cmplx_f32<false>(c1_1, a4_1, a0_1, a2_1);

  // Determinant (real): A_{0:} * adj(A)_{:0}
  float32x4_t det0_0 = neon_mul_cmplx_f32<false>(a0_0, adj00_0);
  float32x4_t det1_0 = neon_mul_cmplx_f32<false>(a1_0, adj10_0);
  float32x4_t det2_0 = neon_mul_cmplx_f32<false>(a2_0, adj20_0);
  float32x4_t det0_1 = neon_mul_cmplx_f32<false>(a0_1, adj00_1);
  float32x4_t det1_1 = neon_mul_cmplx_f32<false>(a1_1, adj10_1);
  float32x4_t det2_1 = neon_mul_cmplx_f32<false>(a2_1, adj20_1);

  float32x4_t inv_det_0 = neon_recip_cmplx_f32(det0_0 + det1_0 + det2_0);
  float32x4_t inv_det_1 = neon_recip_cmplx_f32(det0_1 + det1_1 + det2_1);

  // 1/det * adj(A)
  *out_00_lo = neon_mul_cmplx_f32<false>(adj00_0, inv_det_0);
  *out_00_hi = neon_mul_cmplx_f32<false>(adj00_1, inv_det_1);
  *out_01_lo = neon_mul_cmplx_f32<false>(adj01_0, inv_det_0);
  *out_01_hi = neon_mul_cmplx_f32<false>(adj01_1, inv_det_1);
  *out_02_lo = neon_mul_cmplx_f32<false>(adj02_0, inv_det_0);
  *out_02_hi = neon_mul_cmplx_f32<false>(adj02_1, inv_det_1);
  *out_10_lo = neon_mul_cmplx_f32<false>(adj10_0, inv_det_0);
  *out_10_hi = neon_mul_cmplx_f32<false>(adj10_1, inv_det_1);
  *out_11_lo = neon_mul_cmplx_f32<false>(adj11_0, inv_det_0);
  *out_11_hi = neon_mul_cmplx_f32<false>(adj11_1, inv_det_1);
  *out_12_lo = neon_mul_cmplx_f32<false>(adj12_0, inv_det_0);
  *out_12_hi = neon_mul_cmplx_f32<false>(adj12_1, inv_det_1);
  *out_20_lo = neon_mul_cmplx_f32<false>(adj20_0, inv_det_0);
  *out_20_hi = neon_mul_cmplx_f32<false>(adj20_1, inv_det_1);
  *out_21_lo = neon_mul_cmplx_f32<false>(adj21_0, inv_det_0);
  *out_21_hi = neon_mul_cmplx_f32<false>(adj21_1, inv_det_1);
  *out_22_lo = neon_mul_cmplx_f32<false>(adj22_0, inv_det_0);
  *out_22_hi = neon_mul_cmplx_f32<false>(adj22_1, inv_det_1);
}
#endif

static armral_status __attribute__((noinline, flatten))
invert_batch_hermitian_matrix_3x3(uint32_t num_mats,
                                  const armral_cmplx_f32_t *__restrict p_src,
                                  armral_cmplx_f32_t *p_dst) {

  assert(num_mats % 4 == 0);

  const float32_t *src = (const float32_t *)p_src;
  float32_t *dst = (float32_t *)p_dst;
  uint32_t stride = num_mats * 2;

  for (uint32_t mat_i = 0; mat_i + 4 <= num_mats; mat_i += 4) {
#if ARMRAL_ARCH_SVE >= 2
    const svbool_t p4 = svptrue_pat_b32(SV_VL4);

    // [v0 v1] contains (re, im) of first entry of 4x matrices (re and im are
    // interleaved) As opposed to 4x4 inversion (using block inversion, without
    // consideration of symmetry), for 3x3 inversion we only use the upper
    // triangular part of the input matrix.
    svfloat32_t v0 = svld1_f32(p4, src);
    svfloat32_t v1 = svld1_f32(p4, src + 4);
    svfloat32_t v2 = svld1_f32(p4, src + 1 * stride);
    svfloat32_t v3 = svld1_f32(p4, src + 1 * stride + 4);
    svfloat32_t v4 = svld1_f32(p4, src + 2 * stride);
    svfloat32_t v5 = svld1_f32(p4, src + 2 * stride + 4);
    svfloat32_t v6 = svld1_f32(p4, src + 4 * stride);
    svfloat32_t v7 = svld1_f32(p4, src + 4 * stride + 4);
    svfloat32_t v8 = svld1_f32(p4, src + 5 * stride);
    svfloat32_t v9 = svld1_f32(p4, src + 5 * stride + 4);
    svfloat32_t v10 = svld1_f32(p4, src + 8 * stride);
    svfloat32_t v11 = svld1_f32(p4, src + 8 * stride + 4);

    svfloat32_t out_00_lo;
    svfloat32_t out_00_hi;
    svfloat32_t out_01_lo;
    svfloat32_t out_01_hi;
    svfloat32_t out_02_lo;
    svfloat32_t out_02_hi;
    svfloat32_t out_10_lo;
    svfloat32_t out_10_hi;
    svfloat32_t out_11_lo;
    svfloat32_t out_11_hi;
    svfloat32_t out_12_lo;
    svfloat32_t out_12_hi;
    svfloat32_t out_20_lo;
    svfloat32_t out_20_hi;
    svfloat32_t out_21_lo;
    svfloat32_t out_21_hi;
    svfloat32_t out_22_lo;
    svfloat32_t out_22_hi;
    invert_batch_hermitian_matrix_3x3_impl(
        v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, &out_00_lo,
        &out_00_hi, &out_01_lo, &out_01_hi, &out_02_lo, &out_02_hi, &out_10_lo,
        &out_10_hi, &out_11_lo, &out_11_hi, &out_12_lo, &out_12_hi, &out_20_lo,
        &out_20_hi, &out_21_lo, &out_21_hi, &out_22_lo, &out_22_hi);

    svst1_f32(p4, dst, out_00_lo);
    svst1_f32(p4, dst + 4, out_00_hi);
    svst1_f32(p4, dst + 1 * stride, out_01_lo);
    svst1_f32(p4, dst + 1 * stride + 4, out_01_hi);
    svst1_f32(p4, dst + 2 * stride, out_02_lo);
    svst1_f32(p4, dst + 2 * stride + 4, out_02_hi);
    svst1_f32(p4, dst + 3 * stride, out_10_lo);
    svst1_f32(p4, dst + 3 * stride + 4, out_10_hi);
    svst1_f32(p4, dst + 4 * stride, out_11_lo);
    svst1_f32(p4, dst + 4 * stride + 4, out_11_hi);
    svst1_f32(p4, dst + 5 * stride, out_12_lo);
    svst1_f32(p4, dst + 5 * stride + 4, out_12_hi);
    svst1_f32(p4, dst + 6 * stride, out_20_lo);
    svst1_f32(p4, dst + 6 * stride + 4, out_20_hi);
    svst1_f32(p4, dst + 7 * stride, out_21_lo);
    svst1_f32(p4, dst + 7 * stride + 4, out_21_hi);
    svst1_f32(p4, dst + 8 * stride, out_22_lo);
    svst1_f32(p4, dst + 8 * stride + 4, out_22_hi);
#else
    float32x4_t v0 = vld1q_f32(src);
    float32x4_t v1 = vld1q_f32(src + 4);
    float32x4_t v2 = vld1q_f32(src + 1 * stride);
    float32x4_t v3 = vld1q_f32(src + 1 * stride + 4);
    float32x4_t v4 = vld1q_f32(src + 2 * stride);
    float32x4_t v5 = vld1q_f32(src + 2 * stride + 4);
    float32x4_t v6 = vld1q_f32(src + 4 * stride);
    float32x4_t v7 = vld1q_f32(src + 4 * stride + 4);
    float32x4_t v8 = vld1q_f32(src + 5 * stride);
    float32x4_t v9 = vld1q_f32(src + 5 * stride + 4);
    float32x4_t v10 = vld1q_f32(src + 8 * stride);
    float32x4_t v11 = vld1q_f32(src + 8 * stride + 4);

    float32x4_t v0_out;
    float32x4_t v1_out;
    float32x4_t v2_out;
    float32x4_t v3_out;
    float32x4_t v4_out;
    float32x4_t v5_out;
    float32x4_t v6_out;
    float32x4_t v7_out;
    float32x4_t v8_out;
    float32x4_t v9_out;
    float32x4_t v10_out;
    float32x4_t v11_out;
    float32x4_t v12_out;
    float32x4_t v13_out;
    float32x4_t v14_out;
    float32x4_t v15_out;
    float32x4_t v16_out;
    float32x4_t v17_out;
    invert_batch_hermitian_matrix_3x3_impl(
        v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, &v0_out, &v1_out,
        &v2_out, &v3_out, &v4_out, &v5_out, &v6_out, &v7_out, &v8_out, &v9_out,
        &v10_out, &v11_out, &v12_out, &v13_out, &v14_out, &v15_out, &v16_out,
        &v17_out);

    vst1q_f32(dst, v0_out);
    vst1q_f32(dst + 4, v1_out);
    vst1q_f32(dst + 1 * stride, v2_out);
    vst1q_f32(dst + 1 * stride + 4, v3_out);
    vst1q_f32(dst + 2 * stride, v4_out);
    vst1q_f32(dst + 2 * stride + 4, v5_out);
    vst1q_f32(dst + 3 * stride, v6_out);
    vst1q_f32(dst + 3 * stride + 4, v7_out);
    vst1q_f32(dst + 4 * stride, v8_out);
    vst1q_f32(dst + 4 * stride + 4, v9_out);
    vst1q_f32(dst + 5 * stride, v10_out);
    vst1q_f32(dst + 5 * stride + 4, v11_out);
    vst1q_f32(dst + 6 * stride, v12_out);
    vst1q_f32(dst + 6 * stride + 4, v13_out);
    vst1q_f32(dst + 7 * stride, v14_out);
    vst1q_f32(dst + 7 * stride + 4, v15_out);
    vst1q_f32(dst + 8 * stride, v16_out);
    vst1q_f32(dst + 8 * stride + 4, v17_out);
#endif
    src += 8;
    dst += 8;
  }
  return ARMRAL_SUCCESS;
}

static armral_status __attribute__((noinline, flatten))
invert_batch_hermitian_matrix_3x3_pa(
    uint32_t num_mats, const armral_cmplx_f32_t *__restrict *__restrict p_srcs,
    armral_cmplx_f32_t *__restrict *__restrict p_dsts) {

  assert(num_mats % 4 == 0);

  const float32_t *src_00 = (const float32_t *)p_srcs[0];
  const float32_t *src_01 = (const float32_t *)p_srcs[1];
  const float32_t *src_02 = (const float32_t *)p_srcs[2];
  const float32_t *src_11 = (const float32_t *)p_srcs[4];
  const float32_t *src_12 = (const float32_t *)p_srcs[5];
  const float32_t *src_22 = (const float32_t *)p_srcs[8];

  float32_t *dst_00 = (float32_t *)p_dsts[0];
  float32_t *dst_01 = (float32_t *)p_dsts[1];
  float32_t *dst_02 = (float32_t *)p_dsts[2];
  float32_t *dst_10 = (float32_t *)p_dsts[3];
  float32_t *dst_11 = (float32_t *)p_dsts[4];
  float32_t *dst_12 = (float32_t *)p_dsts[5];
  float32_t *dst_20 = (float32_t *)p_dsts[6];
  float32_t *dst_21 = (float32_t *)p_dsts[7];
  float32_t *dst_22 = (float32_t *)p_dsts[8];

#if ARMRAL_ARCH_SVE >= 2
  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i += 4) {
    const svbool_t p4 = svptrue_pat_b32(SV_VL4);

    svfloat32_t v0 = svld1_f32(p4, src_00);
    svfloat32_t v1 = svld1_f32(p4, src_00 + 4);
    svfloat32_t v2 = svld1_f32(p4, src_01);
    svfloat32_t v3 = svld1_f32(p4, src_01 + 4);
    svfloat32_t v4 = svld1_f32(p4, src_02);
    svfloat32_t v5 = svld1_f32(p4, src_02 + 4);
    svfloat32_t v6 = svld1_f32(p4, src_11);
    svfloat32_t v7 = svld1_f32(p4, src_11 + 4);
    svfloat32_t v8 = svld1_f32(p4, src_12);
    svfloat32_t v9 = svld1_f32(p4, src_12 + 4);
    svfloat32_t v10 = svld1_f32(p4, src_22);
    svfloat32_t v11 = svld1_f32(p4, src_22 + 4);

    svfloat32_t out_00_lo;
    svfloat32_t out_00_hi;
    svfloat32_t out_01_lo;
    svfloat32_t out_01_hi;
    svfloat32_t out_02_lo;
    svfloat32_t out_02_hi;
    svfloat32_t out_10_lo;
    svfloat32_t out_10_hi;
    svfloat32_t out_11_lo;
    svfloat32_t out_11_hi;
    svfloat32_t out_12_lo;
    svfloat32_t out_12_hi;
    svfloat32_t out_20_lo;
    svfloat32_t out_20_hi;
    svfloat32_t out_21_lo;
    svfloat32_t out_21_hi;
    svfloat32_t out_22_lo;
    svfloat32_t out_22_hi;
    invert_batch_hermitian_matrix_3x3_impl(
        v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, &out_00_lo,
        &out_00_hi, &out_01_lo, &out_01_hi, &out_02_lo, &out_02_hi, &out_10_lo,
        &out_10_hi, &out_11_lo, &out_11_hi, &out_12_lo, &out_12_hi, &out_20_lo,
        &out_20_hi, &out_21_lo, &out_21_hi, &out_22_lo, &out_22_hi);

    svst1_f32(p4, dst_00, out_00_lo);
    svst1_f32(p4, dst_00 + 4, out_00_hi);
    svst1_f32(p4, dst_01, out_01_lo);
    svst1_f32(p4, dst_01 + 4, out_01_hi);
    svst1_f32(p4, dst_02, out_02_lo);
    svst1_f32(p4, dst_02 + 4, out_02_hi);
    svst1_f32(p4, dst_10, out_10_lo);
    svst1_f32(p4, dst_10 + 4, out_10_hi);
    svst1_f32(p4, dst_11, out_11_lo);
    svst1_f32(p4, dst_11 + 4, out_11_hi);
    svst1_f32(p4, dst_12, out_12_lo);
    svst1_f32(p4, dst_12 + 4, out_12_hi);
    svst1_f32(p4, dst_20, out_20_lo);
    svst1_f32(p4, dst_20 + 4, out_20_hi);
    svst1_f32(p4, dst_21, out_21_lo);
    svst1_f32(p4, dst_21 + 4, out_21_hi);
    svst1_f32(p4, dst_22, out_22_lo);
    svst1_f32(p4, dst_22 + 4, out_22_hi);

    src_00 += 8;
    src_01 += 8;
    src_02 += 8;
    src_11 += 8;
    src_12 += 8;
    src_22 += 8;

    dst_00 += 8;
    dst_01 += 8;
    dst_02 += 8;
    dst_10 += 8;
    dst_11 += 8;
    dst_12 += 8;
    dst_20 += 8;
    dst_21 += 8;
    dst_22 += 8;
  }
#else
  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i += 4) {
    float32x4_t v0 = vld1q_f32(src_00);
    float32x4_t v1 = vld1q_f32(src_00 + 4);
    float32x4_t v2 = vld1q_f32(src_01);
    float32x4_t v3 = vld1q_f32(src_01 + 4);
    float32x4_t v4 = vld1q_f32(src_02);
    float32x4_t v5 = vld1q_f32(src_02 + 4);
    float32x4_t v6 = vld1q_f32(src_11);
    float32x4_t v7 = vld1q_f32(src_11 + 4);
    float32x4_t v8 = vld1q_f32(src_12);
    float32x4_t v9 = vld1q_f32(src_12 + 4);
    float32x4_t v10 = vld1q_f32(src_22);
    float32x4_t v11 = vld1q_f32(src_22 + 4);

    float32x4_t v0_out;
    float32x4_t v1_out;
    float32x4_t v2_out;
    float32x4_t v3_out;
    float32x4_t v4_out;
    float32x4_t v5_out;
    float32x4_t v6_out;
    float32x4_t v7_out;
    float32x4_t v8_out;
    float32x4_t v9_out;
    float32x4_t v10_out;
    float32x4_t v11_out;
    float32x4_t v12_out;
    float32x4_t v13_out;
    float32x4_t v14_out;
    float32x4_t v15_out;
    float32x4_t v16_out;
    float32x4_t v17_out;
    invert_batch_hermitian_matrix_3x3_impl(
        v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, &v0_out, &v1_out,
        &v2_out, &v3_out, &v4_out, &v5_out, &v6_out, &v7_out, &v8_out, &v9_out,
        &v10_out, &v11_out, &v12_out, &v13_out, &v14_out, &v15_out, &v16_out,
        &v17_out);

    vst1q_f32(dst_00, v0_out);
    vst1q_f32(dst_00 + 4, v1_out);
    vst1q_f32(dst_01, v2_out);
    vst1q_f32(dst_01 + 4, v3_out);
    vst1q_f32(dst_02, v4_out);
    vst1q_f32(dst_02 + 4, v5_out);
    vst1q_f32(dst_10, v6_out);
    vst1q_f32(dst_10 + 4, v7_out);
    vst1q_f32(dst_11, v8_out);
    vst1q_f32(dst_11 + 4, v9_out);
    vst1q_f32(dst_12, v10_out);
    vst1q_f32(dst_12 + 4, v11_out);
    vst1q_f32(dst_20, v12_out);
    vst1q_f32(dst_20 + 4, v13_out);
    vst1q_f32(dst_21, v14_out);
    vst1q_f32(dst_21 + 4, v15_out);
    vst1q_f32(dst_22, v16_out);
    vst1q_f32(dst_22 + 4, v17_out);

    src_00 += 8;
    src_01 += 8;
    src_02 += 8;
    src_11 += 8;
    src_12 += 8;
    src_22 += 8;

    dst_00 += 8;
    dst_01 += 8;
    dst_02 += 8;
    dst_10 += 8;
    dst_11 += 8;
    dst_12 += 8;
    dst_20 += 8;
    dst_21 += 8;
    dst_22 += 8;
  }
#endif
  return ARMRAL_SUCCESS;
}

#ifdef ARMRAL_ARCH_SVE
//
//   Returns C - A.T * B.T
//
static void sve_mat_mls_4x4(const armral_cmplx_f32_t *__restrict c,
                            const armral_cmplx_f32_t *__restrict a,
                            const armral_cmplx_f32_t *__restrict b,
                            armral_cmplx_f32_t *dst) {
  float32_t *out = (float32_t *)dst;
  svbool_t p4 = svptrue_pat_b32(SV_VL4);

  svfloat32_t a00 = svld1_f32(p4, (const float32_t *)&a[0 * 2 + 0 * 4]);
  svfloat32_t a10 = svld1_f32(p4, (const float32_t *)&a[1 * 2 + 0 * 4]);
  svfloat32_t a01 = svld1_f32(p4, (const float32_t *)&a[0 * 2 + 1 * 4]);
  svfloat32_t a11 = svld1_f32(p4, (const float32_t *)&a[1 * 2 + 1 * 4]);
  svfloat32_t a02 = svld1_f32(p4, (const float32_t *)&a[0 * 2 + 2 * 4]);
  svfloat32_t a12 = svld1_f32(p4, (const float32_t *)&a[1 * 2 + 2 * 4]);
  svfloat32_t a03 = svld1_f32(p4, (const float32_t *)&a[0 * 2 + 3 * 4]);
  svfloat32_t a13 = svld1_f32(p4, (const float32_t *)&a[1 * 2 + 3 * 4]);

  for (int j = 0; j < 4; j++) {
    svfloat32_t cj0 = svld1_f32(p4, (const float32_t *)&c[0 * 2 + j * 4]);
    svfloat32_t cj1 = svld1_f32(p4, (const float32_t *)&c[1 * 2 + j * 4]);
    svfloat32_t b0j = svld1_f32(p4, (const float32_t *)&b[0 * 2 + j * 4]);
    svfloat32_t b1j = svld1_f32(p4, (const float32_t *)&b[1 * 2 + j * 4]);

    cj0 = svcmla_lane_f32(cj0, a00, b0j, 0, 180);
    cj0 = svcmla_lane_f32(cj0, a00, b0j, 0, 270);
    cj0 = svcmla_lane_f32(cj0, a01, b0j, 1, 180);
    cj0 = svcmla_lane_f32(cj0, a01, b0j, 1, 270);
    cj0 = svcmla_lane_f32(cj0, a02, b1j, 0, 180);
    cj0 = svcmla_lane_f32(cj0, a02, b1j, 0, 270);
    cj0 = svcmla_lane_f32(cj0, a03, b1j, 1, 180);
    cj0 = svcmla_lane_f32(cj0, a03, b1j, 1, 270);

    cj1 = svcmla_lane_f32(cj1, a10, b0j, 0, 180);
    cj1 = svcmla_lane_f32(cj1, a10, b0j, 0, 270);
    cj1 = svcmla_lane_f32(cj1, a11, b0j, 1, 180);
    cj1 = svcmla_lane_f32(cj1, a11, b0j, 1, 270);
    cj1 = svcmla_lane_f32(cj1, a12, b1j, 0, 180);
    cj1 = svcmla_lane_f32(cj1, a12, b1j, 0, 270);
    cj1 = svcmla_lane_f32(cj1, a13, b1j, 1, 180);
    cj1 = svcmla_lane_f32(cj1, a13, b1j, 1, 270);

    svst1_f32(p4, &out[0 * 4 + j * 8], cj0);
    svst1_f32(p4, &out[1 * 4 + j * 8], cj1);
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
//
//   Returns C - A.T * B.T
//
static void sve_mat_mls_8x8(const armral_cmplx_f32_t *__restrict c,
                            const armral_cmplx_f32_t *__restrict a,
                            const armral_cmplx_f32_t *__restrict b,
                            armral_cmplx_f32_t *dst) {
  // float32_t *out = (float32_t *)dst;
  svbool_t p4 = svptrue_pat_b32(SV_VL4);

  for (int i = 0; i < 8; i += 4) {
    int i0 = i;
    int i1 = i + 2;
    svfloat32_t a00 = svld1_f32(p4, (const float32_t *)&a[i0 + 0 * 8]);
    svfloat32_t a10 = svld1_f32(p4, (const float32_t *)&a[i1 + 0 * 8]);
    svfloat32_t a01 = svld1_f32(p4, (const float32_t *)&a[i0 + 1 * 8]);
    svfloat32_t a11 = svld1_f32(p4, (const float32_t *)&a[i1 + 1 * 8]);
    svfloat32_t a02 = svld1_f32(p4, (const float32_t *)&a[i0 + 2 * 8]);
    svfloat32_t a12 = svld1_f32(p4, (const float32_t *)&a[i1 + 2 * 8]);
    svfloat32_t a03 = svld1_f32(p4, (const float32_t *)&a[i0 + 3 * 8]);
    svfloat32_t a13 = svld1_f32(p4, (const float32_t *)&a[i1 + 3 * 8]);
    svfloat32_t a04 = svld1_f32(p4, (const float32_t *)&a[i0 + 4 * 8]);
    svfloat32_t a14 = svld1_f32(p4, (const float32_t *)&a[i1 + 4 * 8]);
    svfloat32_t a05 = svld1_f32(p4, (const float32_t *)&a[i0 + 5 * 8]);
    svfloat32_t a15 = svld1_f32(p4, (const float32_t *)&a[i1 + 5 * 8]);
    svfloat32_t a06 = svld1_f32(p4, (const float32_t *)&a[i0 + 6 * 8]);
    svfloat32_t a16 = svld1_f32(p4, (const float32_t *)&a[i1 + 6 * 8]);
    svfloat32_t a07 = svld1_f32(p4, (const float32_t *)&a[i0 + 7 * 8]);
    svfloat32_t a17 = svld1_f32(p4, (const float32_t *)&a[i1 + 7 * 8]);

    for (int j = 0; j < 8; j++) {
      svfloat32_t cj0 = svld1_f32(p4, (const float32_t *)&c[i0 + j * 8]);
      svfloat32_t cj1 = svld1_f32(p4, (const float32_t *)&c[i1 + j * 8]);
      svfloat32_t b0j = svld1_f32(p4, (const float32_t *)&b[0 * 2 + j * 8]);
      svfloat32_t b1j = svld1_f32(p4, (const float32_t *)&b[1 * 2 + j * 8]);
      svfloat32_t b2j = svld1_f32(p4, (const float32_t *)&b[2 * 2 + j * 8]);
      svfloat32_t b3j = svld1_f32(p4, (const float32_t *)&b[3 * 2 + j * 8]);

      cj0 = svcmla_lane_f32(cj0, a00, b0j, 0, 180);
      cj0 = svcmla_lane_f32(cj0, a00, b0j, 0, 270);
      cj0 = svcmla_lane_f32(cj0, a01, b0j, 1, 180);
      cj0 = svcmla_lane_f32(cj0, a01, b0j, 1, 270);
      cj0 = svcmla_lane_f32(cj0, a02, b1j, 0, 180);
      cj0 = svcmla_lane_f32(cj0, a02, b1j, 0, 270);
      cj0 = svcmla_lane_f32(cj0, a03, b1j, 1, 180);
      cj0 = svcmla_lane_f32(cj0, a03, b1j, 1, 270);
      cj0 = svcmla_lane_f32(cj0, a04, b2j, 0, 180);
      cj0 = svcmla_lane_f32(cj0, a04, b2j, 0, 270);
      cj0 = svcmla_lane_f32(cj0, a05, b2j, 1, 180);
      cj0 = svcmla_lane_f32(cj0, a05, b2j, 1, 270);
      cj0 = svcmla_lane_f32(cj0, a06, b3j, 0, 180);
      cj0 = svcmla_lane_f32(cj0, a06, b3j, 0, 270);
      cj0 = svcmla_lane_f32(cj0, a07, b3j, 1, 180);
      cj0 = svcmla_lane_f32(cj0, a07, b3j, 1, 270);

      cj1 = svcmla_lane_f32(cj1, a10, b0j, 0, 180);
      cj1 = svcmla_lane_f32(cj1, a10, b0j, 0, 270);
      cj1 = svcmla_lane_f32(cj1, a11, b0j, 1, 180);
      cj1 = svcmla_lane_f32(cj1, a11, b0j, 1, 270);
      cj1 = svcmla_lane_f32(cj1, a12, b1j, 0, 180);
      cj1 = svcmla_lane_f32(cj1, a12, b1j, 0, 270);
      cj1 = svcmla_lane_f32(cj1, a13, b1j, 1, 180);
      cj1 = svcmla_lane_f32(cj1, a13, b1j, 1, 270);
      cj1 = svcmla_lane_f32(cj1, a14, b2j, 0, 180);
      cj1 = svcmla_lane_f32(cj1, a14, b2j, 0, 270);
      cj1 = svcmla_lane_f32(cj1, a15, b2j, 1, 180);
      cj1 = svcmla_lane_f32(cj1, a15, b2j, 1, 270);
      cj1 = svcmla_lane_f32(cj1, a16, b3j, 0, 180);
      cj1 = svcmla_lane_f32(cj1, a16, b3j, 0, 270);
      cj1 = svcmla_lane_f32(cj1, a17, b3j, 1, 180);
      cj1 = svcmla_lane_f32(cj1, a17, b3j, 1, 270);

      svst1_f32(p4, (float32_t *)&dst[i0 + j * 8], cj0);
      svst1_f32(p4, (float32_t *)&dst[i1 + j * 8], cj1);
    }
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
static void sve_mat_mls2x2(svfloat32_t a0, svfloat32_t a1, svfloat32_t b0,
                           svfloat32_t b1, svfloat32_t c0, svfloat32_t c1,
                           svfloat32_t *out0, svfloat32_t *out1) {

  a0 = svcmla_lane_f32(a0, c0, b0, 0, 180);
  a0 = svcmla_lane_f32(a0, c0, b0, 0, 270);
  a0 = svcmla_lane_f32(a0, c1, b0, 1, 180);
  a0 = svcmla_lane_f32(a0, c1, b0, 1, 270);
  a1 = svcmla_lane_f32(a1, c0, b1, 0, 180);
  a1 = svcmla_lane_f32(a1, c0, b1, 0, 270);
  a1 = svcmla_lane_f32(a1, c1, b1, 1, 180);
  a1 = svcmla_lane_f32(a1, c1, b1, 1, 270);

  *out0 = a0;
  *out1 = a1;
}
#endif

#ifdef ARMRAL_ARCH_SVE
static void sve_mat_conj_tran_2x2(const svfloat32_t in0, const svfloat32_t in1,
                                  svfloat32_t *out0, svfloat32_t *out1) {
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svfloat64_t conj0 = svneg_f64_x(p4, svreinterpret_f64_f32(in0));
  svfloat64_t conj1 = svneg_f64_x(p4, svreinterpret_f64_f32(in1));
  *out0 = svreinterpret_f32_f64(svtrn1_f64(conj0, conj1));
  *out1 = svreinterpret_f32_f64(svtrn2_f64(conj0, conj1));
}
#endif

#if ARMRAL_ARCH_SVE >= 2
static void sve_mat_mla_2x_2x2(svfloat32_t a0, svfloat32_t a1, svfloat32_t a2,
                               svfloat32_t a3, svfloat32_t b0, svfloat32_t b1,
                               svfloat32_t b2, svfloat32_t b3, svfloat32_t c0,
                               svfloat32_t c1, svfloat32_t c2, svfloat32_t c3,
                               svfloat32_t *out0, svfloat32_t *out1,
                               svfloat32_t *out2, svfloat32_t *out3) {
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b0, c0, 0);
  a1 = svcmla_f32_x(p4, a1, b0, c1, 0);
  a2 = svcmla_f32_x(p4, a2, b2, c0, 0);
  a3 = svcmla_f32_x(p4, a3, b2, c1, 0);
  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b0, c0, 90);
  a1 = svcmla_f32_x(p4, a1, b0, c1, 90);
  a2 = svcmla_f32_x(p4, a2, b2, c0, 90);
  a3 = svcmla_f32_x(p4, a3, b2, c1, 90);
  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b1, c2, 0);
  a1 = svcmla_f32_x(p4, a1, b1, c3, 90);
  a2 = svcmla_f32_x(p4, a2, b3, c2, 0);
  a3 = svcmla_f32_x(p4, a3, b3, c3, 0);
  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b1, c2, 90);
  a1 = svcmla_f32_x(p4, a1, b1, c3, 0);
  a2 = svcmla_f32_x(p4, a2, b3, c2, 90);
  a3 = svcmla_f32_x(p4, a3, b3, c3, 90);
  asm volatile("");
  *out0 = a0;
  *out1 = a1;
  *out2 = a2;
  *out3 = a3;
}
#endif

#if ARMRAL_ARCH_SVE >= 2
static void sve_mat_mls_2x_2x2(svfloat32_t a0, svfloat32_t a1, svfloat32_t a2,
                               svfloat32_t a3, svfloat32_t b0, svfloat32_t b1,
                               svfloat32_t b2, svfloat32_t b3, svfloat32_t c0,
                               svfloat32_t c1, svfloat32_t c2, svfloat32_t c3,
                               svfloat32_t *out0, svfloat32_t *out1,
                               svfloat32_t *out2, svfloat32_t *out3) {

  svbool_t p4 = svptrue_pat_b32(SV_VL4);

  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b0, c0, 180);
  a1 = svcmla_f32_x(p4, a1, b0, c1, 180);
  a2 = svcmla_f32_x(p4, a2, b2, c0, 180);
  a3 = svcmla_f32_x(p4, a3, b2, c1, 180);
  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b0, c0, 270);
  a1 = svcmla_f32_x(p4, a1, b0, c1, 270);
  a2 = svcmla_f32_x(p4, a2, b2, c0, 270);
  a3 = svcmla_f32_x(p4, a3, b2, c1, 270);
  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b1, c2, 180);
  a1 = svcmla_f32_x(p4, a1, b1, c3, 180);
  a2 = svcmla_f32_x(p4, a2, b3, c2, 180);
  a3 = svcmla_f32_x(p4, a3, b3, c3, 180);
  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b1, c2, 270);
  a1 = svcmla_f32_x(p4, a1, b1, c3, 270);
  a2 = svcmla_f32_x(p4, a2, b3, c2, 270);
  a3 = svcmla_f32_x(p4, a3, b3, c3, 270);
  asm volatile("");
  *out0 = a0;
  *out1 = a1;
  *out2 = a2;
  *out3 = a3;
}
#endif

#if ARMRAL_ARCH_SVE >= 2
static void sve_mat_mls_hermitian_out_4x_2x2(
    svfloat32_t a0, svfloat32_t a1, svfloat32_t a2, svfloat32_t a3,
    svfloat32_t a4, svfloat32_t a5, svfloat32_t a6, svfloat32_t a7,
    svfloat32_t b0, svfloat32_t b1, svfloat32_t b2, svfloat32_t b3,
    svfloat32_t b4, svfloat32_t b5, svfloat32_t b6, svfloat32_t b7,
    svfloat32_t c0, svfloat32_t c1, svfloat32_t c2, svfloat32_t c3,
    svfloat32_t c4, svfloat32_t c5, svfloat32_t c6, svfloat32_t c7,
    svfloat32_t *out0, svfloat32_t *out1, svfloat32_t *out2, svfloat32_t *out3,
    svfloat32_t *out4, svfloat32_t *out5, svfloat32_t *out6,
    svfloat32_t *out7) {

  svbool_t p4 = svptrue_pat_b32(SV_VL4);

  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b0, c0, 180);
  a1 = svcmla_f32_x(p4, a1, b0, c1, 180);
  a3 = svcmla_f32_x(p4, a3, b2, c1, 180);
  a4 = svcmla_f32_x(p4, a4, b4, c4, 180);
  a5 = svcmla_f32_x(p4, a5, b4, c5, 180);
  a7 = svcmla_f32_x(p4, a7, b6, c5, 180);

  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b0, c0, 270);
  a1 = svcmla_f32_x(p4, a1, b0, c1, 270);
  a3 = svcmla_f32_x(p4, a3, b2, c1, 270);
  a4 = svcmla_f32_x(p4, a4, b4, c4, 270);
  a5 = svcmla_f32_x(p4, a5, b4, c5, 270);
  a7 = svcmla_f32_x(p4, a7, b6, c5, 270);

  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b1, c2, 180);
  a1 = svcmla_f32_x(p4, a1, b1, c3, 180);
  a3 = svcmla_f32_x(p4, a3, b3, c3, 180);
  a4 = svcmla_f32_x(p4, a4, b5, c6, 180);
  a5 = svcmla_f32_x(p4, a5, b5, c7, 180);
  a7 = svcmla_f32_x(p4, a7, b7, c7, 180);

  asm volatile("");
  a0 = svcmla_f32_x(p4, a0, b1, c2, 270);
  a1 = svcmla_f32_x(p4, a1, b1, c3, 270);
  a3 = svcmla_f32_x(p4, a3, b3, c3, 270);
  a4 = svcmla_f32_x(p4, a4, b5, c6, 270);
  a5 = svcmla_f32_x(p4, a5, b5, c7, 270);
  a7 = svcmla_f32_x(p4, a7, b7, c7, 270);

  asm volatile("");
  *out0 = a0;
  *out1 = a1;
  *out2 = sve_conjugate_f32(a1);
  *out3 = a3;
  *out4 = a4;
  *out5 = a5;
  *out6 = sve_conjugate_f32(a5);
  *out7 = a7;
}
#endif

#if !defined(ARMRAL_ARCH_SVE) || ARMRAL_ARCH_SVE < 2
// Matrix multiply where 2nd argument is hermitian.
static void mat_mul2x2_second_mat_hermitian(float32x4_t v8, float32x4_t v10,
                                            const float32x4_t ehf,
                                            float32x4_t *ij, float32x4_t *kl) {
  float32x4_t v12 = vmulq_n_f32(v8, ehf[0]);
  float32x4_t v14 = vmulq_n_f32(v10, ehf[1]);

  v12 = vfmaq_laneq_f32(v12, v10, ehf, 2);
  v14 = vfmaq_laneq_f32(v14, v8, ehf, 2);

  v8 = (float32x4_t)vnegq_f64((float64x2_t)v8);
  v8 = vrev64q_f32(v8);

  v10 = vrev64q_f32(v10);
  v10 = (float32x4_t)vnegq_f64((float64x2_t)v10);

  v12 = vfmaq_laneq_f32(v12, v10, ehf, 3);
  v14 = vfmaq_laneq_f32(v14, v8, ehf, 3);

  *ij = (float32x4_t)vzip1q_f64((float64x2_t)v12, (float64x2_t)v14);
  *kl = (float32x4_t)vzip2q_f64((float64x2_t)v12, (float64x2_t)v14);
}
#endif

#if !defined(ARMRAL_ARCH_SVE) || ARMRAL_ARCH_SVE < 2
/* C = A * B (hermitian) (first matrix not packed) */
static void mat_mul2x2_first_mat_conj_second_mat_hermitian(
    const float32x4_t ab, const float32x4_t cd, const float32x4_t ehf,
    float32x4_t *ij, float32x4_t *kl) {
  float32x4_t v8 = (float32x4_t)vnegq_f64((float64x2_t)ab);
  float32x4_t v10 = (float32x4_t)vnegq_f64((float64x2_t)cd);

  mat_mul2x2_second_mat_hermitian(v8, v10, ehf, ij, kl);
}
#endif

#if !defined(ARMRAL_ARCH_SVE) || ARMRAL_ARCH_SVE < 2
/* C = A * B (hermitian) (first matrix packed format) */
static void mat_mul2x2_first_mat_neg_conj_second_mat_hermitian(
    const float32x4_t ab, const float32x4_t cd, const float32x4_t ehf,
    float32x4_t *ij, float32x4_t *kl) {
  float32x4_t v8 = (float32x4_t)vnegq_f64((float64x2_t)ab);
  float32x4_t v10 = (float32x4_t)vnegq_f64((float64x2_t)cd);

  v8 = vnegq_f32(v8);
  v10 = vnegq_f32(v10);

  mat_mul2x2_second_mat_hermitian(v8, v10, ehf, ij, kl);
}
#endif

#if !defined(ARMRAL_ARCH_SVE) || ARMRAL_ARCH_SVE < 2
/* C (hermitian) = A * B (second matrix packed format) */
static void mat_mls2x2_second_matrix_packed(const float32x4_t ab,
                                            const float32x4_t cd,
                                            const float32x4_t ef,
                                            const float32x4_t gh,
                                            float32x4_t *ilj) {
  float32x4_t v8 = (float32x4_t)vtrn1q_f64((float64x2_t)ef, (float64x2_t)gh);
  float32x4_t v10 = (float32x4_t)vtrn2q_f64((float64x2_t)ef, (float64x2_t)gh);
  float32x4_t v12 = (float32x4_t)vnegq_f64((float64x2_t)ab);
  float32x4_t v14 = (float32x4_t)vnegq_f64((float64x2_t)cd);
  const float32x4_t v25 = vmulq_f32(v12, v8);
  const float32x4_t v27 = vmulq_f32(v12, v10);

  v8 = (float32x4_t)vnegq_f64((float64x2_t)v8);
  v8 = vrev64q_f32(v8);

  v12 = vmulq_f32(v14, v10);
  v14 = vmulq_f32(v14, v8);

  v8 = vpaddq_f32(v25, v12);
  v10 = vpaddq_f32(v27, v14);
  v8 = vpaddq_f32(v8, v10);

  *ilj = vsubq_f32(*ilj, v8);
}
#endif

#ifdef ARMRAL_ARCH_SVE
static void sve_invert_hermitian_matrix4x4(
    svfloat32_t a0, svfloat32_t b0, svfloat32_t a1, svfloat32_t b1,
    svfloat32_t c0, svfloat32_t d0, svfloat32_t c1, svfloat32_t d1,
    svfloat32_t *p_a_out0, svfloat32_t *p_b_out0, svfloat32_t *p_a_out1,
    svfloat32_t *p_b_out1, svfloat32_t *p_c_out0, svfloat32_t *p_d_out0,
    svfloat32_t *p_c_out1, svfloat32_t *p_d_out1) {
  /**
     ___________
    |     |     |  A, B, C, D are submatrices (2x2) of MAT
    |  A  |  B  |
    |_____|_____|
    |     |     |
    |  C  |  D  |
    |_____|_____|

    The Inverse of MAT is:
     _______________________________________________________________________
    |                                    |                                  |
    | A^-1*(I + B*(D-C*A^-1*B)^-1*C*A^-1)|      -A^-1*B*(D-C*A^-1*B)^-1     |
    |____________________________________|__________________________________|
    |                                    |                                  |
    |     -(D-C*A^-1*B)^-1 * C*A^-1      |       (D - C*A^-1 * B)^-1        |
    |____________________________________|__________________________________|

  */

  svfloat32_t z = svdup_n_f32(0);

  // Enable the compiler to optimize away loading c0 and c1.
  sve_mat_conj_tran_2x2(b0, b1, &c0, &c1);

  svfloat32_t a_inv0;
  svfloat32_t a_inv1;
  sve_invert_hermitian_matrix2x2(a0, a1, &a_inv0, &a_inv1);

  // E = C*A^-1
  svfloat32_t e0;
  svfloat32_t e1;
  sve_mat_mla2x2(z, z, c0, c1, a_inv0, a_inv1, &e0, &e1);

  // D_out = (D - C*A^-1 * B)^-1
  svfloat32_t d_out0;
  svfloat32_t d_out1;
  sve_mat_mls2x2(d0, d1, e0, e1, b0, b1, &d_out0, &d_out1);
  sve_invert_hermitian_matrix2x2(d_out0, d_out1, &d_out0, &d_out1);

  // C_out = -(D-C*A^-1*B)^-1 * C*A^-1
  svfloat32_t c_out0;
  svfloat32_t c_out1;
  sve_mat_mls2x2(z, z, d_out0, d_out1, e0, e1, &c_out0, &c_out1);

  // B_out = -A^-1*B*(D-C*A^-1*B)^-1 = C.H
  svfloat32_t b_out0;
  svfloat32_t b_out1;
  sve_mat_conj_tran_2x2(c_out0, c_out1, &b_out0, &b_out1);

  // A_out =  A^-1* + A^-1*B*(D-C*A^-1*B)^-1*C*A^-1
  svfloat32_t a_out0;
  svfloat32_t a_out1;
  sve_mat_mls2x2(a_inv0, a_inv1, b_out0, b_out1, e0, e1, &a_out0, &a_out1);

  *p_a_out0 = a_out0;
  *p_b_out0 = b_out0;
  *p_a_out1 = a_out1;
  *p_b_out1 = b_out1;
  *p_c_out0 = c_out0;
  *p_d_out0 = d_out0;
  *p_c_out1 = c_out1;
  *p_d_out1 = d_out1;
}
#endif

template<>
void invert_hermitian_matrix<4>(const armral_cmplx_f32_t *__restrict p_src,
                                armral_cmplx_f32_t *p_dst) {
#ifdef ARMRAL_ARCH_SVE
  svbool_t p4 = svptrue_pat_b32(SV_VL4);

  const float32_t *src = (const float32_t *)p_src;
  svfloat32_t a0 = svld1_f32(p4, &src[0 * 4]);
  svfloat32_t b0 = svld1_f32(p4, &src[1 * 4]);
  svfloat32_t a1 = svld1_f32(p4, &src[2 * 4]);
  svfloat32_t b1 = svld1_f32(p4, &src[3 * 4]);
  svfloat32_t c0 = svld1_f32(p4, &src[4 * 4]);
  svfloat32_t d0 = svld1_f32(p4, &src[5 * 4]);
  svfloat32_t c1 = svld1_f32(p4, &src[6 * 4]);
  svfloat32_t d1 = svld1_f32(p4, &src[7 * 4]);

  svfloat32_t d_out0;
  svfloat32_t d_out1;
  svfloat32_t c_out0;
  svfloat32_t c_out1;
  svfloat32_t b_out0;
  svfloat32_t b_out1;
  svfloat32_t a_out0;
  svfloat32_t a_out1;
  sve_invert_hermitian_matrix4x4(a0, b0, a1, b1, c0, d0, c1, d1, &a_out0,
                                 &b_out0, &a_out1, &b_out1, &c_out0, &d_out0,
                                 &c_out1, &d_out1);

  float32_t *dst = (float32_t *)p_dst;
  svst1_f32(p4, &dst[4 * 0], a_out0);
  svst1_f32(p4, &dst[4 * 1], b_out0);
  svst1_f32(p4, &dst[4 * 2], a_out1);
  svst1_f32(p4, &dst[4 * 3], b_out1);
  svst1_f32(p4, &dst[4 * 4], c_out0);
  svst1_f32(p4, &dst[4 * 5], d_out0);
  svst1_f32(p4, &dst[4 * 6], c_out1);
  svst1_f32(p4, &dst[4 * 7], d_out1);
#else

#define FIRST_ROW (0)
#define SECOND_ROW (1)

  // Matrix blocks for Block Matrix Inversion Algo. Matrix values are collected
  // in a 1-dim array.
  float32x4x2_t a;
  float32x4x2_t b;
  float32x4x2_t c;
  float32x4x2_t d;

  const float32_t *p_mat = (const float32_t *)p_src;

  // Fill sub-blocks matrix
  a.val[FIRST_ROW] = vld1q_f32(p_mat);
  b.val[FIRST_ROW] = vld1q_f32(&p_mat[4]);
  a.val[SECOND_ROW] = vld1q_f32(&p_mat[8]);
  b.val[SECOND_ROW] = vld1q_f32(&p_mat[12]);
  c.val[FIRST_ROW] = vld1q_f32(&p_mat[16]);
  d.val[FIRST_ROW] = vld1q_f32(&p_mat[20]);
  c.val[SECOND_ROW] = vld1q_f32(&p_mat[24]);
  d.val[SECOND_ROW] = vld1q_f32(&p_mat[28]);

  a.val[FIRST_ROW][1] = a.val[SECOND_ROW][2];
  d.val[FIRST_ROW][1] = d.val[SECOND_ROW][2];

  float32x4_t inv_a_packed;

  // 1. Calculation of Inv(A) = A^-1
  invert_hermitian_matrix2x2_packed(a.val[FIRST_ROW], &inv_a_packed);

  // 2. Calculation of L = C * A^-1
  mat_mul2x2_first_mat_conj_second_mat_hermitian(
      b.val[FIRST_ROW], b.val[SECOND_ROW], inv_a_packed, &c.val[FIRST_ROW],
      &c.val[SECOND_ROW]);

  // 3. Calculation of (D - C * (A^-1) * B)^-1
  float32x4_t block22_packed;
  mat_mls2x2_second_matrix_packed(c.val[FIRST_ROW], c.val[SECOND_ROW],
                                  b.val[FIRST_ROW], b.val[SECOND_ROW],
                                  &d.val[FIRST_ROW]);

  invert_hermitian_matrix2x2_packed(d.val[FIRST_ROW], &block22_packed);

  // 4. Calculation of -A^-1 * B * (D - C * (A^-1) * B)^-1, block 1,2 in INV
  // matrix
  mat_mul2x2_first_mat_neg_conj_second_mat_hermitian(
      c.val[FIRST_ROW], c.val[SECOND_ROW], block22_packed, &b.val[FIRST_ROW],
      &b.val[SECOND_ROW]);

  // 5. Calculation of A^-1 + A^-1 *  B * (D - C*(A^-1)*B)^-1 * C * A^-1
  mat_mls2x2_second_matrix_packed(b.val[FIRST_ROW], b.val[SECOND_ROW],
                                  c.val[FIRST_ROW], c.val[SECOND_ROW],
                                  &inv_a_packed);

  c.val[FIRST_ROW] = (float32x4_t)vtrn1q_f64((float64x2_t)b.val[FIRST_ROW],
                                             (float64x2_t)b.val[SECOND_ROW]);
  c.val[SECOND_ROW] = (float32x4_t)vtrn2q_f64((float64x2_t)b.val[FIRST_ROW],
                                              (float64x2_t)b.val[SECOND_ROW]);

  a.val[SECOND_ROW] = vdupq_n_f32(0);
  d.val[SECOND_ROW] = vdupq_n_f32(0);
  a.val[SECOND_ROW][0] = inv_a_packed[1];
  d.val[SECOND_ROW][0] = block22_packed[1];

  inv_a_packed[1] = 0;
  block22_packed[1] = 0;

  a.val[SECOND_ROW] = (float32x4_t)vextq_u8((uint8x16_t)inv_a_packed,
                                            (uint8x16_t)a.val[SECOND_ROW], 8);
  d.val[SECOND_ROW] = (float32x4_t)vextq_u8((uint8x16_t)block22_packed,
                                            (uint8x16_t)d.val[SECOND_ROW], 8);
  a.val[SECOND_ROW] = (float32x4_t)vnegq_f64((float64x2_t)a.val[SECOND_ROW]);
  d.val[SECOND_ROW] = (float32x4_t)vnegq_f64((float64x2_t)d.val[SECOND_ROW]);
  c.val[FIRST_ROW] = (float32x4_t)vnegq_f64((float64x2_t)c.val[FIRST_ROW]);
  c.val[SECOND_ROW] = (float32x4_t)vnegq_f64((float64x2_t)c.val[SECOND_ROW]);

  vst1q_f32((float32_t *)p_dst, inv_a_packed);
  vst1q_f32((float32_t *)&p_dst[2], b.val[FIRST_ROW]);
  vst1q_f32((float32_t *)&p_dst[4], a.val[SECOND_ROW]);
  vst1q_f32((float32_t *)&p_dst[6], b.val[SECOND_ROW]);
  vst1q_f32((float32_t *)&p_dst[8], c.val[FIRST_ROW]);
  vst1q_f32((float32_t *)&p_dst[10], block22_packed);
  vst1q_f32((float32_t *)&p_dst[12], c.val[SECOND_ROW]);
  vst1q_f32((float32_t *)&p_dst[14], d.val[SECOND_ROW]);

#endif
}

#if !defined(ARMRAL_ARCH_SVE) || ARMRAL_ARCH_SVE < 2
static void invert_four_packed_2x2_matrices(float32x4_t *adb1p,
                                            float32x4_t *adb2p,
                                            float32x4_t *adb3p,
                                            float32x4_t *adb4p) {
  float32x4_t adb1[2];
  float32x4_t adb3[2];
  adb1[0] = vld1q_f32((float32_t *)adb1p);
  adb1[1] = vld1q_f32((float32_t *)adb2p);
  adb3[0] = vld1q_f32((float32_t *)adb3p);
  adb3[1] = vld1q_f32((float32_t *)adb4p);

  float32x4_t v0 =
      (float32x4_t)vzip1q_f64((float64x2_t)adb1[0], (float64x2_t)adb3[0]);
  float32x4_t v1 =
      (float32x4_t)vzip1q_f64((float64x2_t)adb1[1], (float64x2_t)adb3[1]);
  float32x4_t v2 =
      (float32x4_t)vzip2q_f64((float64x2_t)adb1[0], (float64x2_t)adb3[0]);
  float32x4_t v3 =
      (float32x4_t)vzip2q_f64((float64x2_t)adb1[1], (float64x2_t)adb3[1]);

  float32x4_t v4 = vtrn1q_f32(v0, v1);
  float32x4_t v5 = vtrn2q_f32(v0, v1);
  float32x4_t v6 = vtrn1q_f32(v2, v3);
  float32x4_t v7 = vtrn2q_f32(v2, v3);

  v4 = vmulq_f32(v4, v5);
  v4 = vfmsq_f32(v4, v6, v6);
  v4 = vfmsq_f32(v4, v7, v7);

  v0 = vrev64q_f32(v0);
  v1 = vrev64q_f32(v1);

  v2 = vnegq_f32(v2);
  v3 = vnegq_f32(v3);

  const float32x4_t inverse_determinant = 1 / v4;

  adb1[0] = (float32x4_t)vzip1q_f64((float64x2_t)v0, (float64x2_t)v2);
  adb1[1] = (float32x4_t)vzip1q_f64((float64x2_t)v1, (float64x2_t)v3);
  adb3[0] = (float32x4_t)vzip2q_f64((float64x2_t)v0, (float64x2_t)v2);
  adb3[1] = (float32x4_t)vzip2q_f64((float64x2_t)v1, (float64x2_t)v3);

  v0 = vdupq_n_f32(inverse_determinant[0]);
  v1 = vdupq_n_f32(inverse_determinant[1]);
  v2 = vdupq_n_f32(inverse_determinant[2]);
  v3 = vdupq_n_f32(inverse_determinant[3]);

  vst1q_f32((float32_t *)adb1p, vmulq_f32(adb1[0], v0));
  vst1q_f32((float32_t *)adb2p, vmulq_f32(adb1[1], v1));
  vst1q_f32((float32_t *)adb3p, vmulq_f32(adb3[0], v2));
  vst1q_f32((float32_t *)adb4p, vmulq_f32(adb3[1], v3));
}
#endif

#if ARMRAL_ARCH_SVE >= 2
// 32 entries here for input and output since 4x4=16, unrolled by two.
static void invert_batch_hermitian_matrix_4x4_impl(
    svfloat32_t v0, svfloat32_t v1, svfloat32_t v2, svfloat32_t v3,
    svfloat32_t v4, svfloat32_t v5, svfloat32_t v6, svfloat32_t v7,
    svfloat32_t v8, svfloat32_t v9, svfloat32_t v10, svfloat32_t v11,
    svfloat32_t v12, svfloat32_t v13, svfloat32_t v14, svfloat32_t v15,
    svfloat32_t v16, svfloat32_t v17, svfloat32_t v18, svfloat32_t v19,
    svfloat32_t v20, svfloat32_t v21, svfloat32_t v22, svfloat32_t v23,
    svfloat32_t v24, svfloat32_t v25, svfloat32_t v26, svfloat32_t v27,
    svfloat32_t v28, svfloat32_t v29, svfloat32_t v30, svfloat32_t v31,
    svfloat32_t *out_00_lo, svfloat32_t *out_00_hi, svfloat32_t *out_01_lo,
    svfloat32_t *out_01_hi, svfloat32_t *out_02_lo, svfloat32_t *out_02_hi,
    svfloat32_t *out_03_lo, svfloat32_t *out_03_hi, svfloat32_t *out_10_lo,
    svfloat32_t *out_10_hi, svfloat32_t *out_11_lo, svfloat32_t *out_11_hi,
    svfloat32_t *out_12_lo, svfloat32_t *out_12_hi, svfloat32_t *out_13_lo,
    svfloat32_t *out_13_hi, svfloat32_t *out_20_lo, svfloat32_t *out_20_hi,
    svfloat32_t *out_21_lo, svfloat32_t *out_21_hi, svfloat32_t *out_22_lo,
    svfloat32_t *out_22_hi, svfloat32_t *out_23_lo, svfloat32_t *out_23_hi,
    svfloat32_t *out_30_lo, svfloat32_t *out_30_hi, svfloat32_t *out_31_lo,
    svfloat32_t *out_31_hi, svfloat32_t *out_32_lo, svfloat32_t *out_32_hi,
    svfloat32_t *out_33_lo, svfloat32_t *out_33_hi) {
  const svfloat32_t z = svdup_f32(0);

  svfloat32_t ai0;
  svfloat32_t ai1;
  svfloat32_t ai2;
  svfloat32_t ai3;
  svfloat32_t ai4;
  svfloat32_t ai5;
  svfloat32_t ai6;
  svfloat32_t ai7;
  sve_invert_batch_hermitian_matrix_4x_2x2(v0, v1, v2, v3, v8, v9, v10, v11,
                                           &ai0, &ai1, &ai2, &ai3, &ai4, &ai5,
                                           &ai6, &ai7);

  // E = C*A^-1
  svfloat32_t e0;
  svfloat32_t e1;
  svfloat32_t e2;
  svfloat32_t e3;
  svfloat32_t e4;
  svfloat32_t e5;
  svfloat32_t e6;
  svfloat32_t e7;
  sve_mat_mla_2x_2x2(z, z, z, z, v16, v18, v24, v26, ai0, ai2, ai4, ai6, &e0,
                     &e2, &e4, &e6);
  sve_mat_mla_2x_2x2(z, z, z, z, v17, v19, v25, v27, ai1, ai3, ai5, ai7, &e1,
                     &e3, &e5, &e7);

  // D_out = (D - C*A^-1 * B)^-1
  sve_mat_mls_2x_2x2(v20, v22, v28, v30, e0, e2, e4, e6, v4, v6, v12, v14,
                     out_22_lo, out_23_lo, out_32_lo, out_33_lo);
  sve_mat_mls_2x_2x2(v21, v23, v29, v31, e1, e3, e5, e7, v5, v7, v13, v15,
                     out_22_hi, out_23_hi, out_32_hi, out_33_hi);
  sve_invert_batch_hermitian_matrix_4x_2x2(
      *out_22_lo, *out_22_hi, *out_23_lo, *out_23_hi, *out_32_lo, *out_32_hi,
      *out_33_lo, *out_33_hi, out_22_lo, out_22_hi, out_23_lo, out_23_hi,
      out_32_lo, out_32_hi, out_33_lo, out_33_hi);

  // C_out = -(D-C*A^-1*B)^-1 * C*A^-1
  sve_mat_mls_2x_2x2(z, z, z, z, *out_22_lo, *out_23_lo, *out_32_lo, *out_33_lo,
                     e0, e2, e4, e6, out_20_lo, out_21_lo, out_30_lo,
                     out_31_lo);
  sve_mat_mls_2x_2x2(z, z, z, z, *out_22_hi, *out_23_hi, *out_32_hi, *out_33_hi,
                     e1, e3, e5, e7, out_20_hi, out_21_hi, out_30_hi,
                     out_31_hi);

  // B_out = -A^-1*B*(D-C*A^-1*B)^-1 = C_out.H
  *out_02_lo = sve_conjugate_f32(*out_20_lo);
  *out_02_hi = sve_conjugate_f32(*out_20_hi);
  *out_03_lo = sve_conjugate_f32(*out_30_lo);
  *out_03_hi = sve_conjugate_f32(*out_30_hi);
  *out_12_lo = sve_conjugate_f32(*out_21_lo);
  *out_12_hi = sve_conjugate_f32(*out_21_hi);
  *out_13_lo = sve_conjugate_f32(*out_31_lo);
  *out_13_hi = sve_conjugate_f32(*out_31_hi);

  // A_out =  A^-1* + A^-1*B*(D-C*A^-1*B)^-1*C*A^-1
  sve_mat_mls_hermitian_out_4x_2x2(
      ai0, ai2, ai4, ai6, ai1, ai3, ai5, ai7, *out_02_lo, *out_03_lo,
      *out_12_lo, *out_13_lo, *out_02_hi, *out_03_hi, *out_12_hi, *out_13_hi,
      e0, e2, e4, e6, e1, e3, e5, e7, out_00_lo, out_01_lo, out_10_lo,
      out_11_lo, out_00_hi, out_01_hi, out_10_hi, out_11_hi);
}
#else
static void invert_batch_hermitian_matrix_4x4_impl(
    float32x4_t v0, float32x4_t v1, float32x4_t v2, float32x4_t v3,
    float32x4_t v4, float32x4_t v5, float32x4_t v6, float32x4_t v7,
    float32x4_t v8, float32x4_t v9, float32x4_t v10, float32x4_t v11,
    float32x4_t v12, float32x4_t v13, float32x4_t v14, float32x4_t v15,
    float32x4_t v16, float32x4_t v17, float32x4_t v18, float32x4_t v19,
    float32x4_t v20, float32x4_t v21, float32x4_t v22, float32x4_t v23,
    float32x4_t v24, float32x4_t v25, float32x4_t v26, float32x4_t v27,
    float32x4_t v28, float32x4_t v29, float32x4_t v30, float32x4_t v31,
    float32x4_t *v0_out, float32x4_t *v1_out, float32x4_t *v2_out,
    float32x4_t *v3_out, float32x4_t *v4_out, float32x4_t *v5_out,
    float32x4_t *v6_out, float32x4_t *v7_out, float32x4_t *v8_out,
    float32x4_t *v9_out, float32x4_t *v10_out, float32x4_t *v11_out,
    float32x4_t *v12_out, float32x4_t *v13_out, float32x4_t *v14_out,
    float32x4_t *v15_out, float32x4_t *v16_out, float32x4_t *v17_out,
    float32x4_t *v18_out, float32x4_t *v19_out, float32x4_t *v20_out,
    float32x4_t *v21_out, float32x4_t *v22_out, float32x4_t *v23_out,
    float32x4_t *v24_out, float32x4_t *v25_out, float32x4_t *v26_out,
    float32x4_t *v27_out, float32x4_t *v28_out, float32x4_t *v29_out,
    float32x4_t *v30_out, float32x4_t *v31_out) {

  // Pack data
  v0 = vtrn1q_f32(v0, v10);
  v1 = vtrn1q_f32(v1, v11);
  v20 = vtrn1q_f32(v20, v30);
  v21 = vtrn1q_f32(v21, v31);

  v24 = (float32x4_t)vtrn1q_f64((float64x2_t)v4, (float64x2_t)v6);
  v25 = (float32x4_t)vtrn1q_f64((float64x2_t)v12, (float64x2_t)v14);
  v26 = (float32x4_t)vtrn2q_f64((float64x2_t)v4, (float64x2_t)v6);
  v27 = (float32x4_t)vtrn2q_f64((float64x2_t)v12, (float64x2_t)v14);
  v28 = (float32x4_t)vtrn1q_f64((float64x2_t)v5, (float64x2_t)v7);
  v29 = (float32x4_t)vtrn1q_f64((float64x2_t)v13, (float64x2_t)v15);
  v30 = (float32x4_t)vtrn2q_f64((float64x2_t)v5, (float64x2_t)v7);
  v31 = (float32x4_t)vtrn2q_f64((float64x2_t)v13, (float64x2_t)v15);

  v8 = (float32x4_t)vtrn1q_f64((float64x2_t)v0, (float64x2_t)v2);
  v9 = (float32x4_t)vtrn2q_f64((float64x2_t)v0, (float64x2_t)v2);
  v10 = (float32x4_t)vtrn1q_f64((float64x2_t)v1, (float64x2_t)v3);
  v11 = (float32x4_t)vtrn2q_f64((float64x2_t)v1, (float64x2_t)v3);

  v12 = (float32x4_t)vtrn1q_f64((float64x2_t)v20, (float64x2_t)v22);
  v13 = (float32x4_t)vtrn2q_f64((float64x2_t)v20, (float64x2_t)v22);
  v14 = (float32x4_t)vtrn1q_f64((float64x2_t)v21, (float64x2_t)v23);
  v15 = (float32x4_t)vtrn2q_f64((float64x2_t)v21, (float64x2_t)v23);

  // v8 = a1^-1, v9 = a2^-1, v10 = a3^-1, v11 = a4^-1
  invert_four_packed_2x2_matrices(&v8, &v9, &v10, &v11);

  // Performing b1'*a1^-1
  mat_mul2x2_first_mat_conj_second_mat_hermitian(v24, v25, v8, &v16, &v17);

  // Performing b2'*a2^-1
  mat_mul2x2_first_mat_conj_second_mat_hermitian(v26, v27, v9, &v18, &v19);

  // Performing b3'*a3^-1
  mat_mul2x2_first_mat_conj_second_mat_hermitian(v28, v29, v10, &v20, &v21);

  // Performing b4'*a4^-1
  mat_mul2x2_first_mat_conj_second_mat_hermitian(v30, v31, v11, &v22, &v23);
  // v12 = d1 - b1'*a1^-1*b1
  mat_mls2x2_second_matrix_packed(v16, v17, v24, v25, &v12);

  // v13 = d2 - b2'*a2^-1*b2
  mat_mls2x2_second_matrix_packed(v18, v19, v26, v27, &v13);

  // v14 = d3 - b3'*a3^-1*b3
  mat_mls2x2_second_matrix_packed(v20, v21, v28, v29, &v14);

  // v15 = d4 - b4'*a4^-1*b4
  mat_mls2x2_second_matrix_packed(v22, v23, v30, v31, &v15);

  // v12 = (d1 - b1'*a1^-1*b1)^-1;  v13 = (d2 - b2'*a2^-1*b2)^-1
  // v14 = (d3 - b3'*a3^-1*b3)^-1;  v15 = (d4 - b4'*a4^-1*b4)^-1
  invert_four_packed_2x2_matrices(&v12, &v13, &v14, &v15);

  // v24 = -a1^-1*b1 * (d1 - b1'*a1^-1*b1)^-1
  mat_mul2x2_first_mat_neg_conj_second_mat_hermitian(v16, v17, v12, &v24, &v25);

  // v26 = -a2^-1*b2 * (d2 - b2'*a2^-1*b2)^-1
  mat_mul2x2_first_mat_neg_conj_second_mat_hermitian(v18, v19, v13, &v26, &v27);

  // v28 = -a3^-1*b3 * (d3 - b3'*a3^-1*b3)^-1
  mat_mul2x2_first_mat_neg_conj_second_mat_hermitian(v20, v21, v14, &v28, &v29);

  // v30 = -a4^-1*b4 * (d4 - b4'*a4^-1*b4)^-1
  mat_mul2x2_first_mat_neg_conj_second_mat_hermitian(v22, v23, v15, &v30, &v31);

  // v8  = a1^-1 + a1^-1*b1 * (d1 - b1'* a1^-1*b1)^-1 * b1'*a1^-1
  mat_mls2x2_second_matrix_packed(v24, v25, v16, v17, &v8);

  // v9  = a2^-1 + a2^-1*b2 * (d2 - b2'* a2^-1*b2)^-1 * b2'*a2^-1
  mat_mls2x2_second_matrix_packed(v26, v27, v18, v19, &v9);

  // v10 = a3^-1 + a3^-1*b3 * (d3 - b3'* a3^-1*b3)^-1 * b3'*a3^-1
  mat_mls2x2_second_matrix_packed(v28, v29, v20, v21, &v10);

  // v11 = a4^-1 + a4^-1*b4 * (d4 - b4'* a4^-1*b4)^-1 * b4'*a4^-1
  mat_mls2x2_second_matrix_packed(v30, v31, v22, v23, &v11);

  // Unpack
  v16 = (float32x4_t)veorq_u16((uint16x8_t)v16, (uint16x8_t)v16);

  v0 = (float32x4_t)vtrn1q_f64((float64x2_t)v8, (float64x2_t)v9);
  v1 = (float32x4_t)vtrn1q_f64((float64x2_t)v10, (float64x2_t)v11);
  v20 = (float32x4_t)vtrn1q_f64((float64x2_t)v12, (float64x2_t)v13);
  v21 = (float32x4_t)vtrn1q_f64((float64x2_t)v14, (float64x2_t)v15);

  v2 = (float32x4_t)vtrn2q_f64((float64x2_t)v8, (float64x2_t)v9);
  v3 = (float32x4_t)vtrn2q_f64((float64x2_t)v10, (float64x2_t)v11);
  v22 = (float32x4_t)vtrn2q_f64((float64x2_t)v12, (float64x2_t)v13);
  v23 = (float32x4_t)vtrn2q_f64((float64x2_t)v14, (float64x2_t)v15);

  v4 = (float32x4_t)vtrn1q_f64((float64x2_t)v24, (float64x2_t)v26);
  v5 = (float32x4_t)vtrn1q_f64((float64x2_t)v28, (float64x2_t)v30);
  v6 = (float32x4_t)vtrn2q_f64((float64x2_t)v24, (float64x2_t)v26);
  v7 = (float32x4_t)vtrn2q_f64((float64x2_t)v28, (float64x2_t)v30);

  v12 = (float32x4_t)vtrn1q_f64((float64x2_t)v25, (float64x2_t)v27);
  v13 = (float32x4_t)vtrn1q_f64((float64x2_t)v29, (float64x2_t)v31);
  v14 = (float32x4_t)vtrn2q_f64((float64x2_t)v25, (float64x2_t)v27);
  v15 = (float32x4_t)vtrn2q_f64((float64x2_t)v29, (float64x2_t)v31);

  v10 = vtrn2q_f32(v0, v16);
  v11 = vtrn2q_f32(v1, v16);
  v30 = vtrn2q_f32(v20, v16);
  v31 = vtrn2q_f32(v21, v16);

  v0 = vtrn1q_f32(v0, v16);
  v1 = vtrn1q_f32(v1, v16);
  v20 = vtrn1q_f32(v20, v16);
  v21 = vtrn1q_f32(v21, v16);

  v8 = (float32x4_t)vnegq_f64((float64x2_t)v2);
  v9 = (float32x4_t)vnegq_f64((float64x2_t)v3);
  v28 = (float32x4_t)vnegq_f64((float64x2_t)v22);
  v29 = (float32x4_t)vnegq_f64((float64x2_t)v23);

  v16 = (float32x4_t)vnegq_f64((float64x2_t)v4);
  v17 = (float32x4_t)vnegq_f64((float64x2_t)v5);
  v24 = (float32x4_t)vnegq_f64((float64x2_t)v6);
  v25 = (float32x4_t)vnegq_f64((float64x2_t)v7);

  v18 = (float32x4_t)vnegq_f64((float64x2_t)v12);
  v19 = (float32x4_t)vnegq_f64((float64x2_t)v13);
  v26 = (float32x4_t)vnegq_f64((float64x2_t)v14);
  v27 = (float32x4_t)vnegq_f64((float64x2_t)v15);

  *v0_out = v0;
  *v1_out = v1;
  *v2_out = v2;
  *v3_out = v3;
  *v4_out = v4;
  *v5_out = v5;
  *v6_out = v6;
  *v7_out = v7;
  *v8_out = v8;
  *v9_out = v9;
  *v10_out = v10;
  *v11_out = v11;
  *v12_out = v12;
  *v13_out = v13;
  *v14_out = v14;
  *v15_out = v15;
  *v16_out = v16;
  *v17_out = v17;
  *v18_out = v18;
  *v19_out = v19;
  *v20_out = v20;
  *v21_out = v21;
  *v22_out = v22;
  *v23_out = v23;
  *v24_out = v24;
  *v25_out = v25;
  *v26_out = v26;
  *v27_out = v27;
  *v28_out = v28;
  *v29_out = v29;
  *v30_out = v30;
  *v31_out = v31;
}
#endif

static armral_status __attribute__((noinline, flatten))
invert_batch_hermitian_matrix_4x4(uint32_t num_mats,
                                  const armral_cmplx_f32_t *__restrict p_src,
                                  armral_cmplx_f32_t *p_dst) {

  const float32_t *src = (const float32_t *)p_src;
  float32_t *dst = (float32_t *)p_dst;
  uint32_t stride = num_mats * 2;

#if ARMRAL_ARCH_SVE >= 2
  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i += 4) {
    const svbool_t p4 = svptrue_pat_b32(SV_VL4);

    svfloat32_t v0 = svld1_f32(p4, src);
    svfloat32_t v1 = svld1_f32(p4, src + 4);
    svfloat32_t v2 = svld1_f32(p4, src + 1 * stride);
    svfloat32_t v3 = svld1_f32(p4, src + 1 * stride + 4);
    svfloat32_t v4 = svld1_f32(p4, src + 2 * stride);
    svfloat32_t v5 = svld1_f32(p4, src + 2 * stride + 4);
    svfloat32_t v6 = svld1_f32(p4, src + 3 * stride);
    svfloat32_t v7 = svld1_f32(p4, src + 3 * stride + 4);
    svfloat32_t v8 = svld1_f32(p4, src + 4 * stride);
    svfloat32_t v9 = svld1_f32(p4, src + 4 * stride + 4);
    svfloat32_t v10 = svld1_f32(p4, src + 5 * stride);
    svfloat32_t v11 = svld1_f32(p4, src + 5 * stride + 4);
    svfloat32_t v12 = svld1_f32(p4, src + 6 * stride);
    svfloat32_t v13 = svld1_f32(p4, src + 6 * stride + 4);
    svfloat32_t v14 = svld1_f32(p4, src + 7 * stride);
    svfloat32_t v15 = svld1_f32(p4, src + 7 * stride + 4);
    svfloat32_t v16 = svld1_f32(p4, src + 8 * stride);
    svfloat32_t v17 = svld1_f32(p4, src + 8 * stride + 4);
    svfloat32_t v18 = svld1_f32(p4, src + 9 * stride);
    svfloat32_t v19 = svld1_f32(p4, src + 9 * stride + 4);
    svfloat32_t v20 = svld1_f32(p4, src + 10 * stride);
    svfloat32_t v21 = svld1_f32(p4, src + 10 * stride + 4);
    svfloat32_t v22 = svld1_f32(p4, src + 11 * stride);
    svfloat32_t v23 = svld1_f32(p4, src + 11 * stride + 4);
    svfloat32_t v24 = svld1_f32(p4, src + 12 * stride);
    svfloat32_t v25 = svld1_f32(p4, src + 12 * stride + 4);
    svfloat32_t v26 = svld1_f32(p4, src + 13 * stride);
    svfloat32_t v27 = svld1_f32(p4, src + 13 * stride + 4);
    svfloat32_t v28 = svld1_f32(p4, src + 14 * stride);
    svfloat32_t v29 = svld1_f32(p4, src + 14 * stride + 4);
    svfloat32_t v30 = svld1_f32(p4, src + 15 * stride);
    svfloat32_t v31 = svld1_f32(p4, src + 15 * stride + 4);

    svfloat32_t out_00_lo;
    svfloat32_t out_00_hi;
    svfloat32_t out_01_lo;
    svfloat32_t out_01_hi;
    svfloat32_t out_02_lo;
    svfloat32_t out_02_hi;
    svfloat32_t out_03_lo;
    svfloat32_t out_03_hi;
    svfloat32_t out_10_lo;
    svfloat32_t out_10_hi;
    svfloat32_t out_11_lo;
    svfloat32_t out_11_hi;
    svfloat32_t out_12_lo;
    svfloat32_t out_12_hi;
    svfloat32_t out_13_lo;
    svfloat32_t out_13_hi;
    svfloat32_t out_20_lo;
    svfloat32_t out_20_hi;
    svfloat32_t out_21_lo;
    svfloat32_t out_21_hi;
    svfloat32_t out_22_lo;
    svfloat32_t out_22_hi;
    svfloat32_t out_23_lo;
    svfloat32_t out_23_hi;
    svfloat32_t out_30_lo;
    svfloat32_t out_30_hi;
    svfloat32_t out_31_lo;
    svfloat32_t out_31_hi;
    svfloat32_t out_32_lo;
    svfloat32_t out_32_hi;
    svfloat32_t out_33_lo;
    svfloat32_t out_33_hi;
    invert_batch_hermitian_matrix_4x4_impl(
        v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15,
        v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29,
        v30, v31, &out_00_lo, &out_00_hi, &out_01_lo, &out_01_hi, &out_02_lo,
        &out_02_hi, &out_03_lo, &out_03_hi, &out_10_lo, &out_10_hi, &out_11_lo,
        &out_11_hi, &out_12_lo, &out_12_hi, &out_13_lo, &out_13_hi, &out_20_lo,
        &out_20_hi, &out_21_lo, &out_21_hi, &out_22_lo, &out_22_hi, &out_23_lo,
        &out_23_hi, &out_30_lo, &out_30_hi, &out_31_lo, &out_31_hi, &out_32_lo,
        &out_32_hi, &out_33_lo, &out_33_hi);

    svst1_f32(p4, dst, out_00_lo);
    svst1_f32(p4, dst + 4, out_00_hi);
    svst1_f32(p4, dst + 1 * stride, out_01_lo);
    svst1_f32(p4, dst + 1 * stride + 4, out_01_hi);
    svst1_f32(p4, dst + 2 * stride, out_02_lo);
    svst1_f32(p4, dst + 2 * stride + 4, out_02_hi);
    svst1_f32(p4, dst + 3 * stride, out_03_lo);
    svst1_f32(p4, dst + 3 * stride + 4, out_03_hi);
    svst1_f32(p4, dst + 4 * stride, out_10_lo);
    svst1_f32(p4, dst + 4 * stride + 4, out_10_hi);
    svst1_f32(p4, dst + 5 * stride, out_11_lo);
    svst1_f32(p4, dst + 5 * stride + 4, out_11_hi);
    svst1_f32(p4, dst + 6 * stride, out_12_lo);
    svst1_f32(p4, dst + 6 * stride + 4, out_12_hi);
    svst1_f32(p4, dst + 7 * stride, out_13_lo);
    svst1_f32(p4, dst + 7 * stride + 4, out_13_hi);
    svst1_f32(p4, dst + 8 * stride, out_20_lo);
    svst1_f32(p4, dst + 8 * stride + 4, out_20_hi);
    svst1_f32(p4, dst + 9 * stride, out_21_lo);
    svst1_f32(p4, dst + 9 * stride + 4, out_21_hi);
    svst1_f32(p4, dst + 10 * stride, out_22_lo);
    svst1_f32(p4, dst + 10 * stride + 4, out_22_hi);
    svst1_f32(p4, dst + 11 * stride, out_23_lo);
    svst1_f32(p4, dst + 11 * stride + 4, out_23_hi);
    svst1_f32(p4, dst + 12 * stride, out_30_lo);
    svst1_f32(p4, dst + 12 * stride + 4, out_30_hi);
    svst1_f32(p4, dst + 13 * stride, out_31_lo);
    svst1_f32(p4, dst + 13 * stride + 4, out_31_hi);
    svst1_f32(p4, dst + 14 * stride, out_32_lo);
    svst1_f32(p4, dst + 14 * stride + 4, out_32_hi);
    svst1_f32(p4, dst + 15 * stride, out_33_lo);
    svst1_f32(p4, dst + 15 * stride + 4, out_33_hi);

    src += 8;
    dst += 8;
  }
#else
  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i += 4) {
    float32x4_t v0 = vld1q_f32(src);
    float32x4_t v1 = vld1q_f32(src + 4);
    float32x4_t v2 = vld1q_f32(src + 1 * stride);
    float32x4_t v3 = vld1q_f32(src + 1 * stride + 4);
    float32x4_t v4 = vld1q_f32(src + 2 * stride);
    float32x4_t v5 = vld1q_f32(src + 2 * stride + 4);
    float32x4_t v6 = vld1q_f32(src + 3 * stride);
    float32x4_t v7 = vld1q_f32(src + 3 * stride + 4);
    float32x4_t v8 = vld1q_f32(src + 4 * stride);
    float32x4_t v9 = vld1q_f32(src + 4 * stride + 4);
    float32x4_t v10 = vld1q_f32(src + 5 * stride);
    float32x4_t v11 = vld1q_f32(src + 5 * stride + 4);
    float32x4_t v12 = vld1q_f32(src + 6 * stride);
    float32x4_t v13 = vld1q_f32(src + 6 * stride + 4);
    float32x4_t v14 = vld1q_f32(src + 7 * stride);
    float32x4_t v15 = vld1q_f32(src + 7 * stride + 4);
    float32x4_t v16 = vld1q_f32(src + 8 * stride);
    float32x4_t v17 = vld1q_f32(src + 8 * stride + 4);
    float32x4_t v18 = vld1q_f32(src + 9 * stride);
    float32x4_t v19 = vld1q_f32(src + 9 * stride + 4);
    float32x4_t v20 = vld1q_f32(src + 10 * stride);
    float32x4_t v21 = vld1q_f32(src + 10 * stride + 4);
    float32x4_t v22 = vld1q_f32(src + 11 * stride);
    float32x4_t v23 = vld1q_f32(src + 11 * stride + 4);
    float32x4_t v24 = vld1q_f32(src + 12 * stride);
    float32x4_t v25 = vld1q_f32(src + 12 * stride + 4);
    float32x4_t v26 = vld1q_f32(src + 13 * stride);
    float32x4_t v27 = vld1q_f32(src + 13 * stride + 4);
    float32x4_t v28 = vld1q_f32(src + 14 * stride);
    float32x4_t v29 = vld1q_f32(src + 14 * stride + 4);
    float32x4_t v30 = vld1q_f32(src + 15 * stride);
    float32x4_t v31 = vld1q_f32(src + 15 * stride + 4);

    float32x4_t v0_out;
    float32x4_t v1_out;
    float32x4_t v2_out;
    float32x4_t v3_out;
    float32x4_t v4_out;
    float32x4_t v5_out;
    float32x4_t v6_out;
    float32x4_t v7_out;
    float32x4_t v8_out;
    float32x4_t v9_out;
    float32x4_t v10_out;
    float32x4_t v11_out;
    float32x4_t v12_out;
    float32x4_t v13_out;
    float32x4_t v14_out;
    float32x4_t v15_out;
    float32x4_t v16_out;
    float32x4_t v17_out;
    float32x4_t v18_out;
    float32x4_t v19_out;
    float32x4_t v20_out;
    float32x4_t v21_out;
    float32x4_t v22_out;
    float32x4_t v23_out;
    float32x4_t v24_out;
    float32x4_t v25_out;
    float32x4_t v26_out;
    float32x4_t v27_out;
    float32x4_t v28_out;
    float32x4_t v29_out;
    float32x4_t v30_out;
    float32x4_t v31_out;
    invert_batch_hermitian_matrix_4x4_impl(
        v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15,
        v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29,
        v30, v31, &v0_out, &v1_out, &v2_out, &v3_out, &v4_out, &v5_out, &v6_out,
        &v7_out, &v8_out, &v9_out, &v10_out, &v11_out, &v12_out, &v13_out,
        &v14_out, &v15_out, &v16_out, &v17_out, &v18_out, &v19_out, &v20_out,
        &v21_out, &v22_out, &v23_out, &v24_out, &v25_out, &v26_out, &v27_out,
        &v28_out, &v29_out, &v30_out, &v31_out);

    vst1q_f32(dst, v0_out);
    vst1q_f32(dst + 4, v1_out);
    vst1q_f32(dst + 1 * stride, v2_out);
    vst1q_f32(dst + 1 * stride + 4, v3_out);
    vst1q_f32(dst + 2 * stride, v4_out);
    vst1q_f32(dst + 2 * stride + 4, v5_out);
    vst1q_f32(dst + 3 * stride, v6_out);
    vst1q_f32(dst + 3 * stride + 4, v7_out);
    vst1q_f32(dst + 4 * stride, v8_out);
    vst1q_f32(dst + 4 * stride + 4, v9_out);
    vst1q_f32(dst + 5 * stride, v10_out);
    vst1q_f32(dst + 5 * stride + 4, v11_out);
    vst1q_f32(dst + 6 * stride, v12_out);
    vst1q_f32(dst + 6 * stride + 4, v13_out);
    vst1q_f32(dst + 7 * stride, v14_out);
    vst1q_f32(dst + 7 * stride + 4, v15_out);
    vst1q_f32(dst + 8 * stride, v16_out);
    vst1q_f32(dst + 8 * stride + 4, v17_out);
    vst1q_f32(dst + 9 * stride, v18_out);
    vst1q_f32(dst + 9 * stride + 4, v19_out);
    vst1q_f32(dst + 10 * stride, v20_out);
    vst1q_f32(dst + 10 * stride + 4, v21_out);
    vst1q_f32(dst + 11 * stride, v22_out);
    vst1q_f32(dst + 11 * stride + 4, v23_out);
    vst1q_f32(dst + 12 * stride, v24_out);
    vst1q_f32(dst + 12 * stride + 4, v25_out);
    vst1q_f32(dst + 13 * stride, v26_out);
    vst1q_f32(dst + 13 * stride + 4, v27_out);
    vst1q_f32(dst + 14 * stride, v28_out);
    vst1q_f32(dst + 14 * stride + 4, v29_out);
    vst1q_f32(dst + 15 * stride, v30_out);
    vst1q_f32(dst + 15 * stride + 4, v31_out);

    src += 8;
    dst += 8;
  }
#endif
  return ARMRAL_SUCCESS;
}

static armral_status __attribute__((noinline, flatten))
invert_batch_hermitian_matrix_4x4_pa(
    uint32_t num_mats, const armral_cmplx_f32_t *__restrict *__restrict p_srcs,
    armral_cmplx_f32_t *__restrict *__restrict p_dsts) {

  const float32_t *src_00 = (const float32_t *)p_srcs[0];
  const float32_t *src_01 = (const float32_t *)p_srcs[1];
  const float32_t *src_02 = (const float32_t *)p_srcs[2];
  const float32_t *src_03 = (const float32_t *)p_srcs[3];
  const float32_t *src_10 = (const float32_t *)p_srcs[4];
  const float32_t *src_11 = (const float32_t *)p_srcs[5];
  const float32_t *src_12 = (const float32_t *)p_srcs[6];
  const float32_t *src_13 = (const float32_t *)p_srcs[7];
  const float32_t *src_20 = (const float32_t *)p_srcs[8];
  const float32_t *src_21 = (const float32_t *)p_srcs[9];
  const float32_t *src_22 = (const float32_t *)p_srcs[10];
  const float32_t *src_23 = (const float32_t *)p_srcs[11];
  const float32_t *src_30 = (const float32_t *)p_srcs[12];
  const float32_t *src_31 = (const float32_t *)p_srcs[13];
  const float32_t *src_32 = (const float32_t *)p_srcs[14];
  const float32_t *src_33 = (const float32_t *)p_srcs[15];

  float32_t *dst_00 = (float32_t *)p_dsts[0];
  float32_t *dst_01 = (float32_t *)p_dsts[1];
  float32_t *dst_02 = (float32_t *)p_dsts[2];
  float32_t *dst_03 = (float32_t *)p_dsts[3];
  float32_t *dst_10 = (float32_t *)p_dsts[4];
  float32_t *dst_11 = (float32_t *)p_dsts[5];
  float32_t *dst_12 = (float32_t *)p_dsts[6];
  float32_t *dst_13 = (float32_t *)p_dsts[7];
  float32_t *dst_20 = (float32_t *)p_dsts[8];
  float32_t *dst_21 = (float32_t *)p_dsts[9];
  float32_t *dst_22 = (float32_t *)p_dsts[10];
  float32_t *dst_23 = (float32_t *)p_dsts[11];
  float32_t *dst_30 = (float32_t *)p_dsts[12];
  float32_t *dst_31 = (float32_t *)p_dsts[13];
  float32_t *dst_32 = (float32_t *)p_dsts[14];
  float32_t *dst_33 = (float32_t *)p_dsts[15];

#if ARMRAL_ARCH_SVE >= 2
  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i += 4) {
    const svbool_t p4 = svptrue_pat_b32(SV_VL4);

    svfloat32_t v0 = svld1_f32(p4, src_00);
    svfloat32_t v1 = svld1_f32(p4, src_00 + 4);
    svfloat32_t v2 = svld1_f32(p4, src_01);
    svfloat32_t v3 = svld1_f32(p4, src_01 + 4);
    svfloat32_t v4 = svld1_f32(p4, src_02);
    svfloat32_t v5 = svld1_f32(p4, src_02 + 4);
    svfloat32_t v6 = svld1_f32(p4, src_03);
    svfloat32_t v7 = svld1_f32(p4, src_03 + 4);
    svfloat32_t v8 = svld1_f32(p4, src_10);
    svfloat32_t v9 = svld1_f32(p4, src_10 + 4);
    svfloat32_t v10 = svld1_f32(p4, src_11);
    svfloat32_t v11 = svld1_f32(p4, src_11 + 4);
    svfloat32_t v12 = svld1_f32(p4, src_12);
    svfloat32_t v13 = svld1_f32(p4, src_12 + 4);
    svfloat32_t v14 = svld1_f32(p4, src_13);
    svfloat32_t v15 = svld1_f32(p4, src_13 + 4);
    svfloat32_t v16 = svld1_f32(p4, src_20);
    svfloat32_t v17 = svld1_f32(p4, src_20 + 4);
    svfloat32_t v18 = svld1_f32(p4, src_21);
    svfloat32_t v19 = svld1_f32(p4, src_21 + 4);
    svfloat32_t v20 = svld1_f32(p4, src_22);
    svfloat32_t v21 = svld1_f32(p4, src_22 + 4);
    svfloat32_t v22 = svld1_f32(p4, src_23);
    svfloat32_t v23 = svld1_f32(p4, src_23 + 4);
    svfloat32_t v24 = svld1_f32(p4, src_30);
    svfloat32_t v25 = svld1_f32(p4, src_30 + 4);
    svfloat32_t v26 = svld1_f32(p4, src_31);
    svfloat32_t v27 = svld1_f32(p4, src_31 + 4);
    svfloat32_t v28 = svld1_f32(p4, src_32);
    svfloat32_t v29 = svld1_f32(p4, src_32 + 4);
    svfloat32_t v30 = svld1_f32(p4, src_33);
    svfloat32_t v31 = svld1_f32(p4, src_33 + 4);

    svfloat32_t out_00_lo;
    svfloat32_t out_00_hi;
    svfloat32_t out_01_lo;
    svfloat32_t out_01_hi;
    svfloat32_t out_02_lo;
    svfloat32_t out_02_hi;
    svfloat32_t out_03_lo;
    svfloat32_t out_03_hi;
    svfloat32_t out_10_lo;
    svfloat32_t out_10_hi;
    svfloat32_t out_11_lo;
    svfloat32_t out_11_hi;
    svfloat32_t out_12_lo;
    svfloat32_t out_12_hi;
    svfloat32_t out_13_lo;
    svfloat32_t out_13_hi;
    svfloat32_t out_20_lo;
    svfloat32_t out_20_hi;
    svfloat32_t out_21_lo;
    svfloat32_t out_21_hi;
    svfloat32_t out_22_lo;
    svfloat32_t out_22_hi;
    svfloat32_t out_23_lo;
    svfloat32_t out_23_hi;
    svfloat32_t out_30_lo;
    svfloat32_t out_30_hi;
    svfloat32_t out_31_lo;
    svfloat32_t out_31_hi;
    svfloat32_t out_32_lo;
    svfloat32_t out_32_hi;
    svfloat32_t out_33_lo;
    svfloat32_t out_33_hi;
    invert_batch_hermitian_matrix_4x4_impl(
        v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15,
        v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29,
        v30, v31, &out_00_lo, &out_00_hi, &out_01_lo, &out_01_hi, &out_02_lo,
        &out_02_hi, &out_03_lo, &out_03_hi, &out_10_lo, &out_10_hi, &out_11_lo,
        &out_11_hi, &out_12_lo, &out_12_hi, &out_13_lo, &out_13_hi, &out_20_lo,
        &out_20_hi, &out_21_lo, &out_21_hi, &out_22_lo, &out_22_hi, &out_23_lo,
        &out_23_hi, &out_30_lo, &out_30_hi, &out_31_lo, &out_31_hi, &out_32_lo,
        &out_32_hi, &out_33_lo, &out_33_hi);

    svst1_f32(p4, dst_00, out_00_lo);
    svst1_f32(p4, dst_00 + 4, out_00_hi);
    svst1_f32(p4, dst_01, out_01_lo);
    svst1_f32(p4, dst_01 + 4, out_01_hi);
    svst1_f32(p4, dst_02, out_02_lo);
    svst1_f32(p4, dst_02 + 4, out_02_hi);
    svst1_f32(p4, dst_03, out_03_lo);
    svst1_f32(p4, dst_03 + 4, out_03_hi);
    svst1_f32(p4, dst_10, out_10_lo);
    svst1_f32(p4, dst_10 + 4, out_10_hi);
    svst1_f32(p4, dst_11, out_11_lo);
    svst1_f32(p4, dst_11 + 4, out_11_hi);
    svst1_f32(p4, dst_12, out_12_lo);
    svst1_f32(p4, dst_12 + 4, out_12_hi);
    svst1_f32(p4, dst_13, out_13_lo);
    svst1_f32(p4, dst_13 + 4, out_13_hi);
    svst1_f32(p4, dst_20, out_20_lo);
    svst1_f32(p4, dst_20 + 4, out_20_hi);
    svst1_f32(p4, dst_21, out_21_lo);
    svst1_f32(p4, dst_21 + 4, out_21_hi);
    svst1_f32(p4, dst_22, out_22_lo);
    svst1_f32(p4, dst_22 + 4, out_22_hi);
    svst1_f32(p4, dst_23, out_23_lo);
    svst1_f32(p4, dst_23 + 4, out_23_hi);
    svst1_f32(p4, dst_30, out_30_lo);
    svst1_f32(p4, dst_30 + 4, out_30_hi);
    svst1_f32(p4, dst_31, out_31_lo);
    svst1_f32(p4, dst_31 + 4, out_31_hi);
    svst1_f32(p4, dst_32, out_32_lo);
    svst1_f32(p4, dst_32 + 4, out_32_hi);
    svst1_f32(p4, dst_33, out_33_lo);
    svst1_f32(p4, dst_33 + 4, out_33_hi);

    src_00 += 8;
    src_01 += 8;
    src_02 += 8;
    src_03 += 8;
    src_10 += 8;
    src_11 += 8;
    src_12 += 8;
    src_13 += 8;
    src_20 += 8;
    src_21 += 8;
    src_22 += 8;
    src_23 += 8;
    src_30 += 8;
    src_31 += 8;
    src_32 += 8;
    src_33 += 8;

    dst_00 += 8;
    dst_01 += 8;
    dst_02 += 8;
    dst_03 += 8;
    dst_10 += 8;
    dst_11 += 8;
    dst_12 += 8;
    dst_13 += 8;
    dst_20 += 8;
    dst_21 += 8;
    dst_22 += 8;
    dst_23 += 8;
    dst_30 += 8;
    dst_31 += 8;
    dst_32 += 8;
    dst_33 += 8;
  }
#else
  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i += 4) {
    float32x4_t v0 = vld1q_f32(src_00);
    float32x4_t v1 = vld1q_f32(src_00 + 4);
    float32x4_t v2 = vld1q_f32(src_01);
    float32x4_t v3 = vld1q_f32(src_01 + 4);
    float32x4_t v4 = vld1q_f32(src_02);
    float32x4_t v5 = vld1q_f32(src_02 + 4);
    float32x4_t v6 = vld1q_f32(src_03);
    float32x4_t v7 = vld1q_f32(src_03 + 4);
    float32x4_t v8 = vld1q_f32(src_10);
    float32x4_t v9 = vld1q_f32(src_10 + 4);
    float32x4_t v10 = vld1q_f32(src_11);
    float32x4_t v11 = vld1q_f32(src_11 + 4);
    float32x4_t v12 = vld1q_f32(src_12);
    float32x4_t v13 = vld1q_f32(src_12 + 4);
    float32x4_t v14 = vld1q_f32(src_13);
    float32x4_t v15 = vld1q_f32(src_13 + 4);
    float32x4_t v16 = vld1q_f32(src_20);
    float32x4_t v17 = vld1q_f32(src_20 + 4);
    float32x4_t v18 = vld1q_f32(src_21);
    float32x4_t v19 = vld1q_f32(src_21 + 4);
    float32x4_t v20 = vld1q_f32(src_22);
    float32x4_t v21 = vld1q_f32(src_22 + 4);
    float32x4_t v22 = vld1q_f32(src_23);
    float32x4_t v23 = vld1q_f32(src_23 + 4);
    float32x4_t v24 = vld1q_f32(src_30);
    float32x4_t v25 = vld1q_f32(src_30 + 4);
    float32x4_t v26 = vld1q_f32(src_31);
    float32x4_t v27 = vld1q_f32(src_31 + 4);
    float32x4_t v28 = vld1q_f32(src_32);
    float32x4_t v29 = vld1q_f32(src_32 + 4);
    float32x4_t v30 = vld1q_f32(src_33);
    float32x4_t v31 = vld1q_f32(src_33 + 4);

    float32x4_t v0_out;
    float32x4_t v1_out;
    float32x4_t v2_out;
    float32x4_t v3_out;
    float32x4_t v4_out;
    float32x4_t v5_out;
    float32x4_t v6_out;
    float32x4_t v7_out;
    float32x4_t v8_out;
    float32x4_t v9_out;
    float32x4_t v10_out;
    float32x4_t v11_out;
    float32x4_t v12_out;
    float32x4_t v13_out;
    float32x4_t v14_out;
    float32x4_t v15_out;
    float32x4_t v16_out;
    float32x4_t v17_out;
    float32x4_t v18_out;
    float32x4_t v19_out;
    float32x4_t v20_out;
    float32x4_t v21_out;
    float32x4_t v22_out;
    float32x4_t v23_out;
    float32x4_t v24_out;
    float32x4_t v25_out;
    float32x4_t v26_out;
    float32x4_t v27_out;
    float32x4_t v28_out;
    float32x4_t v29_out;
    float32x4_t v30_out;
    float32x4_t v31_out;
    invert_batch_hermitian_matrix_4x4_impl(
        v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, v10, v11, v12, v13, v14, v15,
        v16, v17, v18, v19, v20, v21, v22, v23, v24, v25, v26, v27, v28, v29,
        v30, v31, &v0_out, &v1_out, &v2_out, &v3_out, &v4_out, &v5_out, &v6_out,
        &v7_out, &v8_out, &v9_out, &v10_out, &v11_out, &v12_out, &v13_out,
        &v14_out, &v15_out, &v16_out, &v17_out, &v18_out, &v19_out, &v20_out,
        &v21_out, &v22_out, &v23_out, &v24_out, &v25_out, &v26_out, &v27_out,
        &v28_out, &v29_out, &v30_out, &v31_out);

    vst1q_f32(dst_00, v0_out);
    vst1q_f32(dst_00 + 4, v1_out);
    vst1q_f32(dst_01, v2_out);
    vst1q_f32(dst_01 + 4, v3_out);
    vst1q_f32(dst_02, v4_out);
    vst1q_f32(dst_02 + 4, v5_out);
    vst1q_f32(dst_03, v6_out);
    vst1q_f32(dst_03 + 4, v7_out);
    vst1q_f32(dst_10, v8_out);
    vst1q_f32(dst_10 + 4, v9_out);
    vst1q_f32(dst_11, v10_out);
    vst1q_f32(dst_11 + 4, v11_out);
    vst1q_f32(dst_12, v12_out);
    vst1q_f32(dst_12 + 4, v13_out);
    vst1q_f32(dst_13, v14_out);
    vst1q_f32(dst_13 + 4, v15_out);
    vst1q_f32(dst_20, v16_out);
    vst1q_f32(dst_20 + 4, v17_out);
    vst1q_f32(dst_21, v18_out);
    vst1q_f32(dst_21 + 4, v19_out);
    vst1q_f32(dst_22, v20_out);
    vst1q_f32(dst_22 + 4, v21_out);
    vst1q_f32(dst_23, v22_out);
    vst1q_f32(dst_23 + 4, v23_out);
    vst1q_f32(dst_30, v24_out);
    vst1q_f32(dst_30 + 4, v25_out);
    vst1q_f32(dst_31, v26_out);
    vst1q_f32(dst_31 + 4, v27_out);
    vst1q_f32(dst_32, v28_out);
    vst1q_f32(dst_32 + 4, v29_out);
    vst1q_f32(dst_33, v30_out);
    vst1q_f32(dst_33 + 4, v31_out);

    src_00 += 8;
    src_01 += 8;
    src_02 += 8;
    src_03 += 8;
    src_10 += 8;
    src_11 += 8;
    src_12 += 8;
    src_13 += 8;
    src_20 += 8;
    src_21 += 8;
    src_22 += 8;
    src_23 += 8;
    src_30 += 8;
    src_31 += 8;
    src_32 += 8;
    src_33 += 8;

    dst_00 += 8;
    dst_01 += 8;
    dst_02 += 8;
    dst_03 += 8;
    dst_10 += 8;
    dst_11 += 8;
    dst_12 += 8;
    dst_13 += 8;
    dst_20 += 8;
    dst_21 += 8;
    dst_22 += 8;
    dst_23 += 8;
    dst_30 += 8;
    dst_31 += 8;
    dst_32 += 8;
    dst_33 += 8;
  }
#endif
  return ARMRAL_SUCCESS;
}

#ifdef ARMRAL_ARCH_SVE
// Loads one of 4 equally sized quadrants of a square matrix with dimension N.
// N must be a multiple of 4.
template<int N>
static void load_quadrant(const armral_cmplx_f32_t *__restrict p_src,
                          const int block_idx, const int block_idy,
                          armral_cmplx_f32_t *p_dst) {
  static_assert(N % 4 == 0, "N must be a multiple of 4");
  assert(block_idx == 0 || block_idx == 1);
  assert(block_idy == 0 || block_idy == 1);
  const int base_offset = (block_idx * N * N / 2) + (block_idy * N / 2);
  const svbool_t p4 = svptrue_pat_b32(SV_VL4);

  for (int i = 0; i < N / 2; i++) {
    for (int j = 0; j < N / 4; j++) {
      const armral_cmplx_f32_t *src = &p_src[base_offset];
      svfloat32_t a = svld1_f32(p4, (const float32_t *)&src[j * 2 + i * N]);
      svst1_f32(p4, (float32_t *)&p_dst[j * 2 + i * N / 2], a);
    }
  }
}
#endif

#ifdef ARMRAL_ARCH_SVE
// Stores one of 4 equally sized quadrants of a square matrix with dimension N
// N must be a multiple of 4.
template<int N>
static void store_quadrant(const armral_cmplx_f32_t *__restrict p_src,
                           const int block_idx, const int block_idy,
                           armral_cmplx_f32_t *p_dst) {
  static_assert(N % 4 == 0, "N must be a multiple of 4");
  assert(block_idx == 0 || block_idx == 1);
  assert(block_idy == 0 || block_idy == 1);
  const int base_offset = (block_idx * N * N / 2) + (block_idy * N / 2);
  const svbool_t p4 = svptrue_pat_b32(SV_VL4);

  for (int i = 0; i < N / 2; i++) {
    for (int j = 0; j < N / 4; j++) {
      armral_cmplx_f32_t *dst = &p_dst[base_offset];
      svfloat32_t a =
          svld1_f32(p4, (const float32_t *)&p_src[j * 2 + i * N / 2]);
      svst1_f32(p4, (float32_t *)&dst[j * 2 + i * N], a);
    }
  }
}
#endif

template<>
void invert_hermitian_matrix<8>(const armral_cmplx_f32_t *__restrict p_src,
                                armral_cmplx_f32_t *p_dst) {
  // See invert_hermitian_matrix4x4 for a description of the algorithm
#ifdef ARMRAL_ARCH_SVE
  armral_cmplx_f32_t a[4 * 4];
  armral_cmplx_f32_t b[4 * 4];
  armral_cmplx_f32_t c[4 * 4];
  armral_cmplx_f32_t d[4 * 4];
  load_quadrant<8>(p_src, 0, 0, a);
  load_quadrant<8>(p_src, 0, 1, b);
  load_quadrant<8>(p_src, 1, 0, c);
  load_quadrant<8>(p_src, 1, 1, d);

  armral_cmplx_f32_t a_inv[4 * 4];
  invert_hermitian_matrix<4>(a, a_inv);

  // E = C*A^-1 */
  armral_cmplx_f32_t e[4 * 4];
  armral_cmplx_mat_mult_4x4_f32(a_inv, c, e);

  // G = D - C*A^-1*B */
  armral_cmplx_f32_t g[4 * 4];
  sve_mat_mls_4x4(d, b, e, g);

  // D_out = (D - C*A^-1*B)^-1
  armral_cmplx_f32_t d_out[4 * 4];
  invert_hermitian_matrix<4>(g, d_out);

  // C_out = -(D-C*A^-1*B)^-1 * C*A^-1 */
  armral_cmplx_f32_t c_out[4 * 4] = {};
  sve_mat_mls_4x4(c_out, e, d_out, c_out);

  // B_out = -A^-1*B*(D-C*A^-1*B)^-1 = C_out.H
  armral_cmplx_f32_t b_out[4 * 4];
  for (int i = 0; i < 4; i++) {
    for (int j = 0; j < 4; j++) {
      b_out[i * 4 + j].re = c_out[j * 4 + i].re;
      b_out[i * 4 + j].im = -c_out[j * 4 + i].im;
    }
  }

  // A_out =  A^-1* + A^-1*B*(D-C*A^-1*B)^-1*C*A^-1
  armral_cmplx_f32_t a_out[4 * 4];
  sve_mat_mls_4x4(a_inv, e, b_out, a_out);

  store_quadrant<8>(a_out, 0, 0, p_dst);
  store_quadrant<8>(b_out, 0, 1, p_dst);
  store_quadrant<8>(c_out, 1, 0, p_dst);
  store_quadrant<8>(d_out, 1, 1, p_dst);
#else

  const float32_t *p_mat = (const float32_t *)p_src;
  float32x4_t mat_a[8];
  float32x4_t mat_b[8];
  float32x4_t mat_c[8];
  float32x4_t mat_d[8];

  /* Create sublocks 4x4 A and B */
  for (int i = 0; i < 8; i = i + 2) {
    mat_a[i] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_a[i + 1] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_b[i] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_b[i + 1] = vld1q_f32(p_mat);
    p_mat += 4;
  }

  /* Create sublocks 4x4 C and D */
  for (int i = 0; i < 8; i = i + 2) {
    mat_c[i] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_c[i + 1] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_d[i] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_d[i + 1] = vld1q_f32(p_mat);
    p_mat += 4;
  }

  float32_t *p_mat_a = (float32_t *)mat_a;
  float32_t *p_mat_b = (float32_t *)mat_b;
  float32_t *p_mat_c = (float32_t *)mat_c;

  /*Calculate inverse sublock A */
  float32x4_t inv_a[8];
  armral_cmplx_f32_t *p_inv_a = (armral_cmplx_f32_t *)inv_a;

  invert_hermitian_matrix<4>((armral_cmplx_f32_t *)p_mat_a, p_inv_a);

  /*Calculate E = (CA^-1) */
  armral_cmplx_f32_t mat_e[16];
  armral_cmplx_f32_t *p_mat_e = mat_e;
  armral_cmplx_mat_mult_4x4_f32((armral_cmplx_f32_t *)p_inv_a,
                                (armral_cmplx_f32_t *)p_mat_c, p_mat_e);

  /*Calculate F=(D-(CA^-1) * B) */
  float32x4_t temp_mat[8];
  armral_cmplx_f32_t *p_temp = (armral_cmplx_f32_t *)temp_mat;

  float32x4_t mat_f[8];
  float32x4_t inv_f[8];
  const armral_cmplx_f32_t *p_mat_f = (armral_cmplx_f32_t *)mat_f;
  armral_cmplx_f32_t *p_inv_f = (armral_cmplx_f32_t *)inv_f;
  armral_cmplx_mat_mult_4x4_f32((armral_cmplx_f32_t *)p_mat_b, mat_e, p_temp);

  for (int i = 0; i < 8; i++) {
    mat_f[i] = vsubq_f32(mat_d[i], temp_mat[i]);
  }

  /* Invert F */
  invert_hermitian_matrix<4>(p_mat_f, p_inv_f);

  /*calculate block 2-1 -F^-1 * E */
  armral_cmplx_mat_mult_4x4_f32(p_mat_e, p_inv_f, p_temp);

  float32x4_t block21[8];
  for (int i = 0; i < 8; i++) {
    block21[i] = vmulq_n_f32(temp_mat[i], (float32_t)-1);
  }

  /*Calculate G=(A^-1) * B) */
  armral_cmplx_f32_t mat_g[16];
  armral_cmplx_f32_t *p_mat_g = mat_g;
  armral_cmplx_mat_mult_4x4_f32((armral_cmplx_f32_t *)p_mat_b, p_inv_a,
                                p_mat_g);

  /*Calculate block 1-2 G*invF*/
  armral_cmplx_mat_mult_4x4_f32(p_inv_f, p_mat_g, p_temp);

  float32x4_t block12[8];
  for (int i = 0; i < 8; i++) {
    block12[i] = vmulq_n_f32(temp_mat[i], (float32_t)-1);
  }

  /*Calculate block 1-1 invA + G* invF * E*/
  float32x4_t temp_mat2[8];
  armral_cmplx_f32_t *p_temp2 = (armral_cmplx_f32_t *)temp_mat2;
  armral_cmplx_mat_mult_4x4_f32(p_mat_e, p_temp, p_temp2);

  float32x4_t block11[8];
  for (int i = 0; i < 8; i++) {
    block11[i] = vaddq_f32(inv_a[i], temp_mat2[i]);
  }

  float32_t *p_inv = (float32_t *)p_dst;

  vst1q_f32(p_inv, block11[0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[3]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[3]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[4]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[5]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[4]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[5]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[6]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[7]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[6]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[7]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[3]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[3]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[4]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[5]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[4]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[5]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[6]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[7]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[6]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[7]);
#endif
}

#ifdef ARMRAL_ARCH_SVE
static void sve_mat_conj_tran_8x8(const armral_cmplx_f32_t *__restrict src,
                                  armral_cmplx_f32_t *dst) {
  for (int i = 0; i < 8; i++) {
    for (int j = 0; j < 8; j++) {
      dst[i * 8 + j].re = src[j * 8 + i].re;
      dst[i * 8 + j].im = -src[j * 8 + i].im;
    }
  }
}
#endif

template<>
void invert_hermitian_matrix<16>(const armral_cmplx_f32_t *__restrict p_src,
                                 armral_cmplx_f32_t *p_dst) {
#ifdef ARMRAL_ARCH_SVE
  armral_cmplx_f32_t a[8 * 8];
  armral_cmplx_f32_t b[8 * 8];
  armral_cmplx_f32_t c[8 * 8];
  armral_cmplx_f32_t d[8 * 8];
  load_quadrant<16>(p_src, 0, 0, a);
  load_quadrant<16>(p_src, 0, 1, b);
  load_quadrant<16>(p_src, 1, 0, c);
  load_quadrant<16>(p_src, 1, 1, d);

  armral_cmplx_f32_t a_inv[8 * 8];
  invert_hermitian_matrix<8>(a, a_inv);

  // E = C*A^-1 */
  armral_cmplx_f32_t e[8 * 8];
  armral_cmplx_matmul_f32(8, 8, 8, c, a_inv, e);

  // G = D - C*A^-1*B */
  armral_cmplx_f32_t g[8 * 8];
  sve_mat_mls_8x8(d, b, e, g);

  // D_out = (D - C*A^-1*B)^-1
  armral_cmplx_f32_t d_out[8 * 8];
  invert_hermitian_matrix<8>(g, d_out);

  // C_out = -(D-C*A^-1*B)^-1 * C*A^-1 */
  armral_cmplx_f32_t c_out[8 * 8] = {};
  sve_mat_mls_8x8(c_out, e, d_out, c_out);

  // B_out = -A^-1*B*(D-C*A^-1*B)^-1 = C.H
  armral_cmplx_f32_t b_out[8 * 8];
  sve_mat_conj_tran_8x8(c_out, b_out);

  // A_out =  A^-1* + A^-1*B*(D-C*A^-1*B)^-1*C*A^-1
  armral_cmplx_f32_t a_out[8 * 8];
  sve_mat_mls_8x8(a_inv, e, b_out, a_out);

  store_quadrant<16>(a_out, 0, 0, p_dst);
  store_quadrant<16>(b_out, 0, 1, p_dst);
  store_quadrant<16>(c_out, 1, 0, p_dst);
  store_quadrant<16>(d_out, 1, 1, p_dst);
#else
  const float32_t *p_mat = (const float32_t *)p_src;
  float32x4_t mat_a[32];
  float32x4_t mat_b[32];
  float32x4_t mat_c[32];
  float32x4_t mat_d[32];

  /* Create sublocks 8x8 A and B */
  for (int i = 0; i < 32; i = i + 4) {
    mat_a[i] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_a[i + 1] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_a[i + 2] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_a[i + 3] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_b[i] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_b[i + 1] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_b[i + 2] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_b[i + 3] = vld1q_f32(p_mat);
    p_mat += 4;
  }

  /* Create sublocks 8x8 C and D */
  for (int i = 0; i < 32; i = i + 4) {
    mat_c[i] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_c[i + 1] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_c[i + 2] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_c[i + 3] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_d[i] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_d[i + 1] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_d[i + 2] = vld1q_f32(p_mat);
    p_mat += 4;
    mat_d[i + 3] = vld1q_f32(p_mat);
    p_mat += 4;
  }

  float32_t *p_mat_a = (float32_t *)mat_a;
  float32_t *p_mat_b = (float32_t *)mat_b;
  float32_t *p_mat_c = (float32_t *)mat_c;

  /*Calculate inverse sublock A */
  float32x4_t inv_a[32];
  armral_cmplx_f32_t *p_inv_a = (armral_cmplx_f32_t *)inv_a;

  invert_hermitian_matrix<8>((armral_cmplx_f32_t *)p_mat_a, p_inv_a);

  /*Calculate E = (CA^-1) */
  armral_cmplx_f32_t mat_e[64];
  armral_cmplx_f32_t *p_mat_e = mat_e;
  armral_cmplx_matmul_f32(8, 8, 8, (armral_cmplx_f32_t *)p_mat_c,
                          (armral_cmplx_f32_t *)p_inv_a, p_mat_e);

  /*Calculate F=(D-(CA^-1) * B) */
  float32x4_t temp_mat[32];
  armral_cmplx_f32_t *p_temp = (armral_cmplx_f32_t *)temp_mat;

  float32x4_t mat_f[32];
  float32x4_t inv_f[32];
  const armral_cmplx_f32_t *p_mat_f = (armral_cmplx_f32_t *)mat_f;
  armral_cmplx_f32_t *p_inv_f = (armral_cmplx_f32_t *)inv_f;

  armral_cmplx_matmul_f32(8, 8, 8, mat_e, (armral_cmplx_f32_t *)p_mat_b,
                          p_temp);

  for (int i = 0; i < 16; i++) {
    mat_f[2 * i] = vsubq_f32(mat_d[2 * i], temp_mat[2 * i]);
    mat_f[2 * i + 1] = vsubq_f32(mat_d[2 * i + 1], temp_mat[2 * i + 1]);
  }

  /* Invert F */
  invert_hermitian_matrix<8>(p_mat_f, p_inv_f);

  /*calculate block 2-1 -F^-1 * E */
  armral_cmplx_matmul_f32(8, 8, 8, p_inv_f, p_mat_e, p_temp);
  float32x4_t block21[32];

  for (int i = 0; i < 16; i++) {
    block21[2 * i] = vmulq_n_f32(temp_mat[2 * i], (float32_t)-1);
    block21[2 * i + 1] = vmulq_n_f32(temp_mat[2 * i + 1], (float32_t)-1);
  }

  /*Calculate G=(A^-1) * B) */
  armral_cmplx_f32_t mat_g[64];
  armral_cmplx_f32_t *p_mat_g = mat_g;

  armral_cmplx_matmul_f32(8, 8, 8, p_inv_a, (armral_cmplx_f32_t *)p_mat_b,
                          p_mat_g);

  /*Calculate block 1-2 G*invF*/
  armral_cmplx_matmul_f32(8, 8, 8, p_mat_g, p_inv_f, p_temp);

  float32x4_t block12[32];

  for (int i = 0; i < 16; i++) {
    block12[2 * i] = vmulq_n_f32(temp_mat[2 * i], (float32_t)-1);
    block12[2 * i + 1] = vmulq_n_f32(temp_mat[2 * i + 1], (float32_t)-1);
  }

  /*Calculate block 1-1 invA + G* invF * E*/
  float32x4_t temp_mat2[32];
  armral_cmplx_f32_t *p_temp2 = (armral_cmplx_f32_t *)temp_mat2;

  armral_cmplx_matmul_f32(8, 8, 8, p_temp, p_mat_e, p_temp2);

  float32x4_t block11[32];

  for (int i = 0; i < 16; i++) {
    block11[2 * i] = vaddq_f32(inv_a[2 * i], temp_mat2[2 * i]);
    block11[2 * i + 1] = vaddq_f32(inv_a[2 * i + 1], temp_mat2[2 * i + 1]);
  }

  float32_t *p_inv = (float32_t *)p_dst;

  vst1q_f32(p_inv, block11[2 * 0 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 0 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 1 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 1 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 0 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 0 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 1 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 1 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 2 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 2 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 3 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 3 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 2 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 2 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 3 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 3 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 4 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 4 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 5 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 5 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 4 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 4 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 5 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 5 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 6 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 6 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 7 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 7 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 6 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 6 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 7 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 7 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 8 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 8 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 9 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 9 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 8 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 8 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 9 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 9 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 10 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 10 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 11 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 11 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 10 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 10 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 11 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 11 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 12 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 12 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 13 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 13 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 12 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 12 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 13 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 13 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 14 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 14 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 15 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block11[2 * 15 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 14 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 14 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 15 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block12[2 * 15 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 0 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 0 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 1 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 1 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 0 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 0 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 1 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 1 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 2 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 2 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 3 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 3 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 2 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 2 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 3 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 3 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 4 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 4 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 5 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 5 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 4 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 4 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 5 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 5 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 6 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 6 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 7 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 7 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 6 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 6 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 7 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 7 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 8 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 8 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 9 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 9 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 8 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 8 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 9 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 9 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 10 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 10 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 11 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 11 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 10 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 10 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 11 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 11 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 12 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 12 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 13 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 13 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 12 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 12 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 13 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 13 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 14 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 14 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 15 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, block21[2 * 15 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 14 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 14 + 1]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 15 + 0]);
  p_inv += 4;
  vst1q_f32(p_inv, inv_f[2 * 15 + 1]);
#endif
}

} // namespace armral::cmplx_herm_mat_inv

armral_status armral_cmplx_hermitian_mat_inverse_f32(
    uint32_t size, const armral_cmplx_f32_t *__restrict p_src,
    armral_cmplx_f32_t *__restrict p_dst) {
  switch (size) {
  case 2:
    armral::cmplx_herm_mat_inv::invert_hermitian_matrix<2>(p_src, p_dst);
    break;
  case 3:
    armral::cmplx_herm_mat_inv::invert_hermitian_matrix<3>(p_src, p_dst);
    break;
  case 4:
    armral::cmplx_herm_mat_inv::invert_hermitian_matrix<4>(p_src, p_dst);
    break;
  case 8:
    armral::cmplx_herm_mat_inv::invert_hermitian_matrix<8>(p_src, p_dst);
    break;
  case 16:
    armral::cmplx_herm_mat_inv::invert_hermitian_matrix<16>(p_src, p_dst);
    break;
  default:
    return ARMRAL_ARGUMENT_ERROR;
  }
  return ARMRAL_SUCCESS;
}

armral_status armral_cmplx_hermitian_mat_inverse_batch_f32(
    uint32_t num_mats, uint32_t size,
    const armral_cmplx_f32_t *__restrict p_src,
    armral_cmplx_f32_t *__restrict p_dst) {
  if (size == 2) {
    return armral::cmplx_herm_mat_inv::invert_batch_hermitian_matrix_2x2(
        num_mats, p_src, p_dst);
  }
  if (size == 3) {
    return armral::cmplx_herm_mat_inv::invert_batch_hermitian_matrix_3x3(
        num_mats, p_src, p_dst);
  }
  if (size == 4) {
    return armral::cmplx_herm_mat_inv::invert_batch_hermitian_matrix_4x4(
        num_mats, p_src, p_dst);
  }
  return ARMRAL_ARGUMENT_ERROR;
}

armral_status armral_cmplx_hermitian_mat_inverse_batch_f32_pa(
    uint32_t num_mats, uint32_t size,
    const armral_cmplx_f32_t **__restrict p_srcs,
    armral_cmplx_f32_t **__restrict p_dsts) {
  if (size == 2) {
    return armral::cmplx_herm_mat_inv::invert_batch_hermitian_matrix_2x2_pa(
        num_mats, p_srcs, p_dsts);
  }
  if (size == 3) {
    return armral::cmplx_herm_mat_inv::invert_batch_hermitian_matrix_3x3_pa(
        num_mats, p_srcs, p_dsts);
  }
  if (size == 4) {
    return armral::cmplx_herm_mat_inv::invert_batch_hermitian_matrix_4x4_pa(
        num_mats, p_srcs, p_dsts);
  }
  return ARMRAL_ARGUMENT_ERROR;
}
