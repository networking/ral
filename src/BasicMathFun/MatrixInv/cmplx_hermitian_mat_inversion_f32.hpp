/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

namespace armral::cmplx_herm_mat_inv {

template<uint16_t m>
void invert_hermitian_matrix(const armral_cmplx_f32_t *__restrict p_src,
                             armral_cmplx_f32_t *p_dst);

} // namespace armral::cmplx_herm_mat_inv
