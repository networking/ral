/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include "intrinsics.h"
#include "utils/cmplx_arith_f32.hpp"

#include <assert.h>

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

namespace armral::cmplx_mat_inv {

// some helper functions for scalar implementation of matrix inversion
static inline armral_cmplx_f32_t
scal_minor_cmplx_f32(const armral_cmplx_f32_t a, const armral_cmplx_f32_t b,
                     const armral_cmplx_f32_t c, const armral_cmplx_f32_t d) {
  return scal_sub_cmplx_f32(scal_mul_cmplx_f32(a, d), scal_mul_cmplx_f32(b, c));
}

#ifdef ARMRAL_ARCH_SVE
template<uint64_t R1, uint64_t R2>
static inline svfloat32_t __attribute__((always_inline))
sve_mul_cmplx_f32(svbool_t pg, svfloat32_t op1, svfloat32_t op2) {
  svfloat32_t acc = svdup_n_f32(0.0F);
  acc = svcmla_f32_x(pg, acc, op1, op2, R1);
  acc = svcmla_f32_x(pg, acc, op1, op2, R2);
  return acc;
}

template<uint64_t R1, uint64_t R2, uint64_t L>
static inline svfloat32_t __attribute__((always_inline))
sve_mla_lane_cmplx_f32(svfloat32_t acc, svfloat32_t op1, svfloat32_t op2) {
  acc = svcmla_lane_f32(acc, op1, op2, L, R1);
  acc = svcmla_lane_f32(acc, op1, op2, L, R2);
  return acc;
}

template<uint64_t R1, uint64_t R2, uint64_t L>
static inline svfloat32_t __attribute__((always_inline))
sve_mul_lane_cmplx_f32(svfloat32_t op1, svfloat32_t op2) {
  return sve_mla_lane_cmplx_f32<R1, R2, L>(svdup_n_f32(0.0F), op1, op2);
}

template<bool accumulate>
static inline void sve_mat_mul_2x2_block_row_major(const svfloat32_t vec1_00_01,
                                                   const svfloat32_t vec1_10_11,
                                                   const svfloat32_t vec2_00_01,
                                                   const svfloat32_t vec2_10_11,
                                                   svfloat32_t *res_00_01,
                                                   svfloat32_t *res_10_11) {
  if constexpr (accumulate) {
    *res_00_01 =
        sve_mla_lane_cmplx_f32<0, 90, 0>(*res_00_01, vec2_00_01, vec1_00_01);
    *res_10_11 =
        sve_mla_lane_cmplx_f32<0, 90, 0>(*res_10_11, vec2_00_01, vec1_10_11);
  } else {
    *res_00_01 = sve_mul_lane_cmplx_f32<0, 90, 0>(vec2_00_01, vec1_00_01);
    *res_10_11 = sve_mul_lane_cmplx_f32<0, 90, 0>(vec2_00_01, vec1_10_11);
  }
  *res_00_01 =
      sve_mla_lane_cmplx_f32<0, 90, 1>(*res_00_01, vec2_10_11, vec1_00_01);
  *res_10_11 =
      sve_mla_lane_cmplx_f32<0, 90, 1>(*res_10_11, vec2_10_11, vec1_10_11);
}
#endif

template<bool accumulate>
static inline void mat_mul_2x2_block_row_major(const float32x4_t *a,
                                               const float32x4_t *b,
                                               float32x4_t *c) {
  float32x4_t b_rot90[2];
  b_rot90[0] = neon_conjugate_f32(b[0]);
  b_rot90[0] = vrev64q_f32(b_rot90[0]);
  b_rot90[1] = neon_conjugate_f32(b[1]);
  b_rot90[1] = vrev64q_f32(b_rot90[1]);

  if constexpr (accumulate) {
    c[0] = vmlaq_laneq_f32(c[0], b[0], a[0], 0);
  } else {
    c[0] = vmulq_laneq_f32(b[0], a[0], 0);
  }
  c[0] = vmlaq_laneq_f32(c[0], b_rot90[0], a[0], 1);
  c[0] = vmlaq_laneq_f32(c[0], b[1], a[0], 2);
  c[0] = vmlaq_laneq_f32(c[0], b_rot90[1], a[0], 3);

  if constexpr (accumulate) {
    c[1] = vmlaq_laneq_f32(c[1], b[0], a[1], 0);
  } else {
    c[1] = vmulq_laneq_f32(b[0], a[1], 0);
  }
  c[1] = vmlaq_laneq_f32(c[1], b_rot90[0], a[1], 1);
  c[1] = vmlaq_laneq_f32(c[1], b[1], a[1], 2);
  c[1] = vmlaq_laneq_f32(c[1], b_rot90[1], a[1], 3);
}

template<int N>
static inline void load_2x2_block_row_major(const float32_t *a,
                                            float32x4_t *a_block) {
  a_block[0] = vld1q_f32(a);
  a_block[1] = vld1q_f32(&a[2 * N]);
}

template<int N>
static inline void store_2x2_block_row_major(float32_t *a,
                                             float32x4_t *a_block) {
  vst1q_f32((float32_t *)a, a_block[0]);
  vst1q_f32((float32_t *)&a[2 * N], a_block[1]);
}

// This function performs D = C + A * B when accumulate == true or
// D = A * B when accumulate == false. Template parameter N denotes
// the number of row and column of the block size for multiplication,
// while N[A/B/C/D] denotes the number of row and column of the
// input/output matrices A/B/C/D, respectively. The algorithm loops
// over a sub-block size of 2x2, and thus assumes even number of rows
// and columns for the input/output matrices.
template<uint32_t N, uint32_t NA, uint32_t NB, uint32_t NC, uint32_t ND,
         bool accumulate>
static inline void mat_mult_row_major_impl(const armral_cmplx_f32_t *a,
                                           const armral_cmplx_f32_t *b,
                                           const armral_cmplx_f32_t *c,
                                           armral_cmplx_f32_t *d) {
  uint32_t constexpr n_half = N / 2;
  uint32_t constexpr na2 = NA * 2;
  uint32_t constexpr nb2 = NB * 2;
  uint32_t constexpr nc2 = NC * 2;
  uint32_t constexpr nd2 = ND * 2;

  float32x4_t a_block[2];
  float32x4_t b_block[2];
  float32x4_t c_block[2];
  for (uint32_t i = 0; i < n_half; ++i) {
    for (uint32_t j = 0; j < n_half; ++j) {
      load_2x2_block_row_major<NA>((const float32_t *)&a[i * na2], a_block);
      load_2x2_block_row_major<NB>((const float32_t *)&b[j * 2], b_block);
      if constexpr (accumulate) {
        load_2x2_block_row_major<NC>((const float32_t *)&c[j * 2 + i * nc2],
                                     c_block);
      }
      mat_mul_2x2_block_row_major<accumulate>(a_block, b_block, c_block);

      for (uint32_t k = 1; k < n_half; ++k) {
        load_2x2_block_row_major<NA>((const float32_t *)&a[k * 2 + i * na2],
                                     a_block);
        load_2x2_block_row_major<NB>((const float32_t *)&b[k * nb2 + j * 2],
                                     b_block);
        mat_mul_2x2_block_row_major<true>(a_block, b_block, c_block);
      }

      store_2x2_block_row_major<ND>((float32_t *)&d[j * 2 + i * nd2], c_block);
    }
  }
}

template<uint32_t N, uint32_t NA, uint32_t NB, uint32_t NC, uint32_t ND>
static inline void mat_mla(const armral_cmplx_f32_t *a,
                           const armral_cmplx_f32_t *b,
                           const armral_cmplx_f32_t *c, armral_cmplx_f32_t *d) {
  mat_mult_row_major_impl<N, NA, NB, NC, ND, true>(a, b, c, d);
}

template<uint32_t N, uint32_t NA, uint32_t NB, uint32_t NC>
static inline void mat_mul(const armral_cmplx_f32_t *a,
                           const armral_cmplx_f32_t *b, armral_cmplx_f32_t *c) {
  mat_mult_row_major_impl<N, NA, NB, 0, NC, false>(a, b, nullptr, c);
}

template<int N, int Nsrc, int Ndst>
struct mat_inverse {
  static inline void invert_matrix(const armral_cmplx_f32_t *p_src,
                                   armral_cmplx_f32_t *p_dst);

private:
  template<int NA, int NB, int NC>
  constexpr static inline auto mat_mul_block = mat_mul<N / 2, NA, NB, NC>;

  template<int NA, int NB, int NC, int ND>
  constexpr static inline auto mat_mla_block = mat_mla<N / 2, NA, NB, NC, ND>;

  template<int Nin, int Nout>
  constexpr static inline auto mat_inv_block =
      mat_inverse<N / 2, Nin, Nout>::invert_matrix;
};

template<int Nsrc, int Ndst>
struct mat_inverse<4, Nsrc, Ndst> {
  static inline void invert_matrix(const armral_cmplx_f32_t *p_src,
                                   armral_cmplx_f32_t *p_dst);
};

template<int N>
constexpr static inline auto invert_matrix =
    mat_inverse<N, N, N>::invert_matrix;

template<int N, int Nsrc, int Ndst>
inline void mat_inverse<N, Nsrc, Ndst>::invert_matrix(
    const armral_cmplx_f32_t *__restrict p_src, armral_cmplx_f32_t *p_dst) {
  constexpr int half_n = N / 2;
  constexpr int block_size = half_n * half_n;

  // p_src = | A  B |
  //         | C  D |
  // Matrix A
  const armral_cmplx_f32_t *a_ptr = &p_src[0];

  // 1. Calculate A^-1
  armral_cmplx_f32_t inv_a[block_size];
  mat_inv_block<Nsrc, half_n>(a_ptr, inv_a);

  armral_cmplx_f32_t neg_inv_a[block_size];
  for (int i = 0; i < block_size; ++i) {
    neg_inv_a[i] = {-inv_a[i].re, -inv_a[i].im};
  }

  // Matrix C
  const armral_cmplx_f32_t *c_ptr = &p_src[half_n * Nsrc];

  // 2. Calculation of -L = -C * A^-1
  armral_cmplx_f32_t neg_l[block_size];
  mat_mul_block<Nsrc, half_n, half_n>(c_ptr, neg_inv_a, neg_l);

  // Matrix B
  const armral_cmplx_f32_t *b_ptr = &p_src[half_n];

  // Matrix D
  const armral_cmplx_f32_t *d_ptr = &p_src[half_n * Nsrc + half_n];

  // 3. Calculation of (D - E)^-1 = inv_11 (i.e. inverse block in row 1, column
  // 1). This is the Schur complement of block A in p_src. We compute (D - E)^-1
  // by (D + (-L) * B)^-1. d now stores (D - E).
  armral_cmplx_f32_t de[block_size];
  mat_mla_block<half_n, Nsrc, Nsrc, half_n>(neg_l, b_ptr, d_ptr, de);

  armral_cmplx_f32_t *inv_11_ptr = &p_dst[half_n * Ndst + half_n];
  mat_inv_block<half_n, Ndst>(de, inv_11_ptr);

  // 4. Calculation of -A^-1 * B * (D - E)^-1 = inv_01
  armral_cmplx_f32_t b_inv_11[block_size];
  mat_mul_block<Nsrc, Ndst, half_n>(b_ptr, inv_11_ptr, b_inv_11);

  armral_cmplx_f32_t *inv_01_ptr = &p_dst[half_n];
  mat_mul_block<half_n, half_n, Ndst>(neg_inv_a, b_inv_11, inv_01_ptr);

  // 5. Calculation of -(D - E)^-1 * C * A^-1 = inv_10
  armral_cmplx_f32_t *inv_10_ptr = &p_dst[half_n * Ndst];
  mat_mul_block<Ndst, half_n, Ndst>(inv_11_ptr, neg_l, inv_10_ptr);

  // 6. Calculation of A^-1 + A^-1 * B * (D - E)^-1 * C * A^-1 = inv_00
  armral_cmplx_f32_t *inv_00_ptr = &p_dst[0];
  mat_mla_block<Ndst, half_n, half_n, Ndst>(inv_01_ptr, neg_l, inv_a,
                                            inv_00_ptr);
}

#ifdef ARMRAL_ARCH_SVE
static inline void sve_invert_matrix_2x2_impl(svfloat32_t ab, svfloat32_t cd,
                                              svfloat32_t *res0,
                                              svfloat32_t *res1) {
  // For a 2x2 matrix | a b |
  //                  | c d |
  // the inverse is
  //           |  d * conj(det)  -b * conj(det) | * 1 / (Re(det)^2 + Im(det)^2)
  //           | -c * conj(det)   a * conj(det) |

  svbool_t ptrue = svptrue_b32();

  // [ c a ]
  svfloat32_t ca = svtrn1_cmplx_f32(cd, ab);
  // [ d b ]
  svfloat32_t db = svtrn2_cmplx_f32(cd, ab);
  // [ d c ]
  svfloat32_t dc = svrev_cmplx_f32(cd);

  // [det -det]
  svfloat32_t det = sve_mul_cmplx_f32<0, 90>(ptrue, ab, dc);
  svfloat32_t det_rev = svrev_cmplx_f32(det);
  det = svsub_f32_x(ptrue, det, det_rev);

  // [1/(Re(det)^2 + Im(det)^2), 1/(Re(det)^2 + Im(det)^2), ...]
  svfloat32_t abs = svmul_f32_x(ptrue, det, det);
  abs = svadd_f32_x(ptrue, abs, svrev_f32(abs));
  svfloat32_t det_abs_recip = svdivr_n_f32_x(ptrue, abs, 1.0F);

  // [ d * conj(det)  b * conj(-det) ] * 1 / (Re(det)^2 + Im(det)^2)
  *res0 = sve_mul_cmplx_f32<0, 270>(ptrue, det, db);
  *res0 = svmul_f32_x(ptrue, *res0, det_abs_recip);

  // [ c * conj(-det)  a * conj(det) ] * 1 / (Re(det)^2 + Im(det)^2)
  *res1 = sve_mul_cmplx_f32<180, 90>(ptrue, det, ca);
  *res1 = svmul_f32_x(ptrue, *res1, det_abs_recip);
}
#else
static inline void invert_matrix_2x2_impl(const float32x4_t *a,
                                          float32x4_t *inv_a) {
  float32x4_t abcd_re = vuzp1q_f32(a[0], a[1]);
  float32x4_t abcd_im = vuzp2q_f32(a[0], a[1]);
  float32x4_t cdab_re = vuzp1q_f32(a[1], a[0]);
  float32x4_t cdab_im = vuzp2q_f32(a[1], a[0]);
  float32x4_t dcba_re = vrev64q_f32(cdab_re);
  float32x4_t dcba_im = vrev64q_f32(cdab_im);

  // det_re = [ Re(det), -Re(det), -Re(det), Re(det) ]
  float32x4_t det_re = vmulq_f32(abcd_re, dcba_re);
  det_re = vfmsq_f32(det_re, abcd_im, dcba_im);
  det_re = vsubq_f32(det_re, neon_rev_cmplx_f32(det_re));

  // det_im = [ Im(det), -Im(det), -Im(det), Im(det) ]
  float32x4_t det_im = vmulq_f32(abcd_re, dcba_im);
  det_im = vfmaq_f32(det_im, abcd_im, dcba_re);
  det_im = vsubq_f32(det_im, neon_rev_cmplx_f32(det_im));

  float32x4_t abs2 = vmulq_f32(det_re, det_re);
  abs2 = vfmaq_f32(abs2, det_im, det_im);
  float32x4_t inv_abs2 = vdivq_f32(vdupq_n_f32(1.0F), abs2);

  float32x4_t dbca_re = vuzp1q_f32(dcba_re, cdab_re);
  float32x4_t dbca_im = vuzp1q_f32(dcba_im, cdab_im);

  // out = [ d, b, c, a ] * conj(det)
  // The minus signs needed for b and c are taken into account through
  // the [ det_re, det_im ] vector.
  // We do not need to perform the conjugation of the determinant here.
  // We can simply flip the signs when multiplying with the dbca vector.
  float32x4_t out_re = vmulq_f32(dbca_re, det_re);
  out_re = vfmaq_f32(out_re, dbca_im, det_im);
  float32x4_t out_im = vmulq_f32(dbca_im, det_re);
  out_im = vfmsq_f32(out_im, dbca_re, det_im);

  inv_a[0] = vzip1q_f32(out_re, out_im);
  inv_a[1] = vzip2q_f32(out_re, out_im);
  inv_a[0] = vmulq_f32(inv_a[0], inv_abs2);
  inv_a[1] = vmulq_f32(inv_a[1], inv_abs2);
}
#endif

template<>
inline void
mat_inverse<2, 2, 2>::invert_matrix(const armral_cmplx_f32_t *__restrict p_src,
                                    armral_cmplx_f32_t *p_dst) {
  // Get the inverse of the matrix using the analytic expression
  // p_dst = |  p_src[3] -p_src[1] | * det^-1
  //         | -p_src[2]  p_src[0] |
  // where det = p_src[0] * p_src[3] - p_src[1] * p_src[2]
#ifdef ARMRAL_ARCH_SVE
  const auto *src = (const float32_t *)p_src;
  auto *dst = (float32_t *)p_dst;
  svbool_t p4 = svptrue_pat_b32(SV_VL4);

  // [ a b ]
  svfloat32_t ab = svld1rq_f32(p4, src);
  // [ c d ]
  svfloat32_t cd = svld1rq_f32(p4, src + 4);

  svfloat32_t res0 = svdup_n_f32(0.0F);
  svfloat32_t res1 = svdup_n_f32(0.0F);
  sve_invert_matrix_2x2_impl(ab, cd, &res0, &res1);

  svst1_f32(p4, dst, res0);
  svst1_f32(p4, dst + 4, res1);
#else
  // a[0] = [ a b ], a[1] = [ c d ]
  float32x4_t a[2];
  a[0] = vld1q_f32((const float32_t *)&p_src[0]);
  a[1] = vld1q_f32((const float32_t *)&p_src[2]);

  float32x4_t inv_a[2];

  invert_matrix_2x2_impl(a, inv_a);

  vst1q_f32((float32_t *)&p_dst[0], inv_a[0]);
  vst1q_f32((float32_t *)&p_dst[2], inv_a[1]);
#endif
}

template<>
inline void
mat_inverse<3, 3, 3>::invert_matrix(const armral_cmplx_f32_t *__restrict p_src,
                                    armral_cmplx_f32_t *p_dst) {

#ifdef ARMRAL_ARCH_SVE
  const float32_t *src = (const float32_t *)p_src;
  float32_t *dst = (float32_t *)p_dst;
  svbool_t ptrue = svptrue_b32();
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svbool_t p2 = svptrue_pat_b32(SV_VL2);

  // For an input matrix
  //   | a  b  c |
  //   | d  e  f |
  //   | g  h  i |

  // the inverse is
  //   | (ei - fh) * conj(det)  (ch - bi) * conj(det)  (bf - ce) * conj(det) |
  //   | (fg - di) * conj(det)  (ai - cg) * conj(det)  (cd - af) * conj(det) |
  //   | (dh - eg) * conj(det)  (bg - ah) * conj(det)  (ae - bd) * conj(det) |
  //   * 1/(Re(det)^2 + Im(det)^2)

  svfloat32_t ab = svld1rq_f32(ptrue, src);
  svfloat32_t cd = svld1rq_f32(ptrue, src + 4);
  svfloat32_t ee = svld1rq_f32(p2, src + 8);
  ee = svreinterpret_f32_f64(
      svuzp1_f64(svreinterpret_f64_f32(ee), svreinterpret_f64_f32(ee)));
  svfloat32_t fg = svld1rq_f32(ptrue, src + 10);
  svfloat32_t hi = svld1rq_f32(ptrue, src + 14);

  svfloat32_t ac = svtrn1_cmplx_f32(ab, cd);
  svfloat32_t ca = svtrn1_cmplx_f32(cd, ab);
  svfloat32_t ec = svtrn2_cmplx_f32(ee, ac);
  svfloat32_t gf = svrev_cmplx_f32(fg);
  svfloat32_t bf = svtrn2_cmplx_f32(ab, gf);
  svfloat32_t fb = svrev_cmplx_f32(bf);
  svfloat32_t ih = svrev_cmplx_f32(hi);
  svfloat32_t id = svtrn2_cmplx_f32(hi, cd);
  svfloat32_t db = svtrn2_cmplx_f32(cd, ab);
  svfloat32_t ei = svtrn1_cmplx_f32(ee, ih);
  svfloat32_t hg = svtrn1_cmplx_f32(hi, gf);
  svfloat32_t ea = svtrn1_cmplx_f32(ee, ab);
  svfloat32_t ed = svtrn2_cmplx_f32(ee, cd);
  svfloat32_t gh = svrev_cmplx_f32(hg);

  svfloat32_t ei_ch = sve_mul_cmplx_f32<0, 90>(ptrue, ec, ih);
  svfloat32_t bf_fg = sve_mul_cmplx_f32<0, 90>(ptrue, bf, fg);
  svfloat32_t ai_cd = sve_mul_cmplx_f32<0, 90>(ptrue, ac, id);
  svfloat32_t dh_bg = sve_mul_cmplx_f32<0, 90>(ptrue, db, hg);
  svfloat32_t fh_bi = sve_mul_cmplx_f32<0, 90>(ptrue, fb, hi);
  svfloat32_t ce_di = sve_mul_cmplx_f32<0, 90>(ptrue, cd, ei);
  svfloat32_t cg_af = sve_mul_cmplx_f32<0, 90>(ptrue, ca, gf);
  svfloat32_t eg_ah = sve_mul_cmplx_f32<0, 90>(ptrue, ea, gh);
  svfloat32_t ae_bd = sve_mul_cmplx_f32<0, 90>(ptrue, ab, ed);
  svfloat32_t bd_ae = svrev_cmplx_f32(ae_bd);

  // [ (ei - fh) (ch - bi) ]
  svfloat32_t vec_00_01 = svsub_f32_x(ptrue, ei_ch, fh_bi);
  // [ (bf - ce) (fg - di) ]
  svfloat32_t vec_02_10 = svsub_f32_x(ptrue, bf_fg, ce_di);
  // [ (ai - cg) (cd - af) ]
  svfloat32_t vec_11_12 = svsub_f32_x(ptrue, ai_cd, cg_af);
  // [ (dh - eg) (bg - ah) ]
  svfloat32_t vec_20_21 = svsub_f32_x(ptrue, dh_bg, eg_ah);
  // [ (ae - bd) (bd - ae) ]
  svfloat32_t vec_22 = svsub_f32_x(ptrue, ae_bd, bd_ae);

  // Calculate determinant [ det det ]
  // det = a * (ei - fh) + b * (fg - di) + c * (dh - eg)
  svfloat32_t cc = svtrn1_cmplx_f32(cd, cd);
  svfloat32_t vec_20_20 = svtrn1_cmplx_f32(vec_20_21, vec_20_21);
  svfloat32_t c_adj20 = sve_mul_cmplx_f32<0, 90>(ptrue, cc, vec_20_20);
  svfloat32_t adj00_adj10 =
      svtrn1_cmplx_f32(vec_00_01, svrev_cmplx_f32(vec_02_10));
  svfloat32_t det = sve_mul_cmplx_f32<0, 90>(ptrue, ab, adj00_adj10);
  svfloat32_t det_rev = svrev_cmplx_f32(det);
  det = svadd_f32_x(ptrue, det, det_rev);
  det = svadd_f32_x(ptrue, det, c_adj20);

  // [ 1 / (Re(det)^2 + Im(det)^2) 1 / (Re(det)^2 + Im(det)^2) ]
  svfloat32_t abs = svmul_f32_x(ptrue, det, det);
  abs = svadd_f32_x(ptrue, abs, svrev_f32(abs));
  svfloat32_t det_abs_recip = svdivr_n_f32_x(ptrue, abs, 1.0F);

  // [ (ei - fg) * conj(det),
  //   (ch - bi) * conj(det) ] * 1 / (Re(det)^2 + Im(det)^2)
  svfloat32_t res_00_01 = sve_mul_cmplx_f32<0, 270>(ptrue, det, vec_00_01);
  // [ (bf - ce) * conj(det),
  //   (fg - di) * conj(det) ] * (Re(det)^2 + Im(det)^2)
  svfloat32_t res_02_10 = sve_mul_cmplx_f32<0, 270>(ptrue, det, vec_02_10);
  // [ (ai - cg) * conj(det),
  //   (cd - af) * conj(det) ] * 1 / (Re(det)^2 + Im(det)^2)
  svfloat32_t res_11_12 = sve_mul_cmplx_f32<0, 270>(ptrue, det, vec_11_12);
  // [ (dh - eg) * conj(det),
  //   (bg - ah) * conj(det) ] * 1 / (Re(det)^2 + Im(det)^2)
  svfloat32_t res_20_21 = sve_mul_cmplx_f32<0, 270>(ptrue, det, vec_20_21);
  // [ (ae - db) * conj(det),
  //   (bd - ae) * conj(det) ] * 1 / (Re(det)^2 + Im(det)^2)
  svfloat32_t res_22 = sve_mul_cmplx_f32<0, 270>(ptrue, det, vec_22);

  svst1_f32(p4, dst, svmul_f32_x(ptrue, res_00_01, det_abs_recip));
  svst1_f32(p4, dst + 4, svmul_f32_x(ptrue, res_02_10, det_abs_recip));
  svst1_f32(p4, dst + 8, svmul_f32_x(ptrue, res_11_12, det_abs_recip));
  svst1_f32(p4, dst + 12, svmul_f32_x(ptrue, res_20_21, det_abs_recip));
  svst1_f32(p2, dst + 16, svmul_f32_x(ptrue, res_22, det_abs_recip));
#else
  float32x2_t a0 = vld1_f32((const float32_t *)&p_src[0]);
  float32x2_t a1 = vld1_f32((const float32_t *)&p_src[1]);
  float32x2_t a2 = vld1_f32((const float32_t *)&p_src[2]);
  float32x2_t a3 = vld1_f32((const float32_t *)&p_src[3]);
  float32x2_t a4 = vld1_f32((const float32_t *)&p_src[4]);
  float32x2_t a5 = vld1_f32((const float32_t *)&p_src[5]);
  float32x2_t a6 = vld1_f32((const float32_t *)&p_src[6]);
  float32x2_t a7 = vld1_f32((const float32_t *)&p_src[7]);
  float32x2_t a8 = vld1_f32((const float32_t *)&p_src[8]);

  // Compute minors, transpose, and apply negative signs
  float32x2_t adj00 = neon_minor_cmplx_f32<false>(a4, a5, a7, a8);
  float32x2_t adj01 = neon_minor_cmplx_f32<false>(a7, a8, a1, a2);
  float32x2_t adj02 = neon_minor_cmplx_f32<false>(a1, a2, a4, a5);
  float32x2_t adj10 = neon_minor_cmplx_f32<false>(a6, a8, a3, a5);
  float32x2_t adj11 = neon_minor_cmplx_f32<false>(a0, a2, a6, a8);
  float32x2_t adj12 = neon_minor_cmplx_f32<false>(a3, a5, a0, a2);
  float32x2_t adj20 = neon_minor_cmplx_f32<false>(a3, a4, a6, a7);
  float32x2_t adj21 = neon_minor_cmplx_f32<false>(a6, a7, a0, a1);
  float32x2_t adj22 = neon_minor_cmplx_f32<false>(a0, a1, a3, a4);

  // Determinant: A_{0:} * adj(A)_{:0}
  float32x2_t det = neon_mul_cmplx_f32<false>(a0, adj00);
  det = neon_mla_cmplx_f32<false>(det, a1, adj10);
  det = neon_mla_cmplx_f32<false>(det, a2, adj20);

  float32x2_t inv_det = neon_recip_cmplx_f32(det);

  vst1_f32((float32_t *)&p_dst[0], neon_mul_cmplx_f32<false>(inv_det, adj00));
  vst1_f32((float32_t *)&p_dst[1], neon_mul_cmplx_f32<false>(inv_det, adj01));
  vst1_f32((float32_t *)&p_dst[2], neon_mul_cmplx_f32<false>(inv_det, adj02));
  vst1_f32((float32_t *)&p_dst[3], neon_mul_cmplx_f32<false>(inv_det, adj10));
  vst1_f32((float32_t *)&p_dst[4], neon_mul_cmplx_f32<false>(inv_det, adj11));
  vst1_f32((float32_t *)&p_dst[5], neon_mul_cmplx_f32<false>(inv_det, adj12));
  vst1_f32((float32_t *)&p_dst[6], neon_mul_cmplx_f32<false>(inv_det, adj20));
  vst1_f32((float32_t *)&p_dst[7], neon_mul_cmplx_f32<false>(inv_det, adj21));
  vst1_f32((float32_t *)&p_dst[8], neon_mul_cmplx_f32<false>(inv_det, adj22));
#endif
}

template<int Nsrc, int Ndst>
inline void mat_inverse<4, Nsrc, Ndst>::invert_matrix(
    const armral_cmplx_f32_t *__restrict p_src, armral_cmplx_f32_t *p_dst) {
  // p_src = | A  B |
  //         | C  D |

#ifdef ARMRAL_ARCH_SVE
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svbool_t ptrue = svptrue_b32();

  const auto *src = (const float32_t *)p_src;
  auto *dst = (float32_t *)p_dst;

  // Calculate A^-1
  svfloat32_t a_00_01 = svld1rq_f32(p4, src);
  svfloat32_t a_10_11 = svld1rq_f32(p4, src + 2 * Nsrc);
  svfloat32_t inv_a_0 = svdup_n_f32(0.0F);
  svfloat32_t inv_a_1 = svdup_n_f32(0.0F);
  sve_invert_matrix_2x2_impl(a_00_01, a_10_11, &inv_a_0, &inv_a_1);

  // Calculation of -L = -C * A-1
  svfloat32_t neg_inv_a_00_01 = svneg_f32_x(ptrue, inv_a_0);
  svfloat32_t neg_inv_a_10_11 = svneg_f32_x(ptrue, inv_a_1);
  svfloat32_t c_00_01 = svld1rq_f32(p4, src + Nsrc * 4);
  svfloat32_t c_10_11 = svld1rq_f32(p4, src + Nsrc * 6);
  svfloat32_t neg_l_00_01 = svdup_n_f32(0.0F);
  svfloat32_t neg_l_10_11 = svdup_n_f32(0.0F);
  sve_mat_mul_2x2_block_row_major<false>(c_00_01, c_10_11, neg_inv_a_00_01,
                                         neg_inv_a_10_11, &neg_l_00_01,
                                         &neg_l_10_11);

  // Calculation of (D - E)^-1 = inv_11
  // (D - E)^-1 = (D + (-L) * B)^-1
  // d_00_01 and d_10_11 now hold (D - E)
  svfloat32_t b_00_01 = svld1rq_f32(p4, src + 4);
  svfloat32_t b_10_11 = svld1rq_f32(p4, src + (Nsrc + 2) * 2);
  svfloat32_t d_00_01 = svld1rq_f32(p4, src + Nsrc * 4 + 4);
  svfloat32_t d_10_11 = svld1rq_f32(p4, src + Nsrc * 6 + 4);
  sve_mat_mul_2x2_block_row_major<true>(neg_l_00_01, neg_l_10_11, b_00_01,
                                        b_10_11, &d_00_01, &d_10_11);
  svfloat32_t inv_11_0 = svdup_n_f32(0.0F);
  svfloat32_t inv_11_1 = svdup_n_f32(0.0F);
  sve_invert_matrix_2x2_impl(d_00_01, d_10_11, &inv_11_0, &inv_11_1);

  // Calculation of -A^-1 * B * (D - E)^-1 = inv_01
  svfloat32_t b_inv_11_0 = svdup_n_f32(0.0F);
  svfloat32_t b_inv_11_1 = svdup_n_f32(0.0F);
  sve_mat_mul_2x2_block_row_major<false>(b_00_01, b_10_11, inv_11_0, inv_11_1,
                                         &b_inv_11_0, &b_inv_11_1);
  svfloat32_t inv_01_0 = svdup_n_f32(0.0F);
  svfloat32_t inv_01_1 = svdup_n_f32(0.0F);
  sve_mat_mul_2x2_block_row_major<false>(neg_inv_a_00_01, neg_inv_a_10_11,
                                         b_inv_11_0, b_inv_11_1, &inv_01_0,
                                         &inv_01_1);

  // Calculation of -(D - E)^-1 * C * A^-1 = inv_10
  svfloat32_t inv_10_0 = svdup_n_f32(0.0F);
  svfloat32_t inv_10_1 = svdup_n_f32(0.0F);
  sve_mat_mul_2x2_block_row_major<false>(inv_11_0, inv_11_1, neg_l_00_01,
                                         neg_l_10_11, &inv_10_0, &inv_10_1);

  // Calculation of A^-1 + A^-1 * B * (D - E)^-1 * C * A^-1 = inv_00
  // inv_a_0 and inv_a_0 now hold inv_00
  sve_mat_mul_2x2_block_row_major<true>(inv_01_0, inv_01_1, neg_l_00_01,
                                        neg_l_10_11, &inv_a_0, &inv_a_1);

  svst1_f32(p4, dst, inv_a_0);
  svst1_f32(p4, dst + 4, inv_01_0);
  svst1_f32(p4, dst + Ndst * 2, inv_a_1);
  svst1_f32(p4, dst + Ndst * 2 + 4, inv_01_1);
  svst1_f32(p4, dst + Ndst * 4, inv_10_0);
  svst1_f32(p4, dst + Ndst * 4 + 4, inv_11_0);
  svst1_f32(p4, dst + Ndst * 6, inv_10_1);
  svst1_f32(p4, dst + Ndst * 6 + 4, inv_11_1);
#else
  float32x4_t a[2];
  float32x4_t b[2];
  float32x4_t c[2];
  float32x4_t d[2];

  // Matrix A
  a[0] = vld1q_f32((const float32_t *)&p_src[0]);
  a[1] = vld1q_f32((const float32_t *)&p_src[Nsrc]);

  // 1. Calculate A^-1
  float32x4_t inv_a[2];
  invert_matrix_2x2_impl(a, inv_a);

  float32x4_t neg_inv_a[2];
  neg_inv_a[0] = vnegq_f32(inv_a[0]);
  neg_inv_a[1] = vnegq_f32(inv_a[1]);

  // Matrix C
  c[0] = vld1q_f32((const float32_t *)&p_src[Nsrc * 2]);
  c[1] = vld1q_f32((const float32_t *)&p_src[Nsrc * 3]);

  // 2. Calculation of -L = -C * A^-1
  float32x4_t neg_l[2];
  mat_mul_2x2_block_row_major<false>(c, neg_inv_a, neg_l);

  // Matrix B
  b[0] = vld1q_f32((const float32_t *)&p_src[2]);
  b[1] = vld1q_f32((const float32_t *)&p_src[Nsrc + 2]);

  // Matrix D
  d[0] = vld1q_f32((const float32_t *)&p_src[Nsrc * 2 + 2]);
  d[1] = vld1q_f32((const float32_t *)&p_src[Nsrc * 3 + 2]);

  // 3. Calculation of (D - E)^-1 = inv_11 (i.e. inverse block in row 1, column
  // 1). This is the Schur complement of block A in p_src. We compute (D - E)^-1
  // by (D + (-L) * B)^-1. d now stores (D - E).
  mat_mul_2x2_block_row_major<true>(neg_l, b, d);

  float32x4_t inv_11[2];
  invert_matrix_2x2_impl(d, inv_11);

  // 4. Calculation of -A^-1 * B * (D - E)^-1 = inv_01
  float32x4_t b_inv_11[2];
  mat_mul_2x2_block_row_major<false>(b, inv_11, b_inv_11);

  float32x4_t inv_01[2];
  mat_mul_2x2_block_row_major<false>(neg_inv_a, b_inv_11, inv_01);

  // 5. Calculation of -(D - E)^-1 * C * A^-1 = inv_10
  float32x4_t inv_10[2];
  mat_mul_2x2_block_row_major<false>(inv_11, neg_l, inv_10);

  // 6. Calculation of A^-1 + A^-1 * B * (D - E)^-1 * C * A^-1 = inv_00
  // inv_a now stores inv_00.
  mat_mul_2x2_block_row_major<true>(inv_01, neg_l, inv_a);

  // We've got all of the components, so write them out. Matrix is in row-major
  // format
  vst1q_f32((float32_t *)&p_dst[0], inv_a[0]);
  vst1q_f32((float32_t *)&p_dst[2], inv_01[0]);
  vst1q_f32((float32_t *)&p_dst[Ndst], inv_a[1]);
  vst1q_f32((float32_t *)&p_dst[Ndst + 2], inv_01[1]);
  vst1q_f32((float32_t *)&p_dst[Ndst * 2], inv_10[0]);
  vst1q_f32((float32_t *)&p_dst[Ndst * 2 + 2], inv_11[0]);
  vst1q_f32((float32_t *)&p_dst[Ndst * 3], inv_10[1]);
  vst1q_f32((float32_t *)&p_dst[Ndst * 3 + 2], inv_11[1]);
#endif
}

static void invert_batch_matrix_2x2_impl(
    armral_cmplx_f32_t a, armral_cmplx_f32_t b, armral_cmplx_f32_t c,
    armral_cmplx_f32_t d, armral_cmplx_f32_t *out_a, armral_cmplx_f32_t *out_b,
    armral_cmplx_f32_t *out_c, armral_cmplx_f32_t *out_d) {
  // det = a * d - b * c
  armral_cmplx_f32_t det = scal_minor_cmplx_f32(a, b, c, d);
  // inv_det = 1 / det = conj(det) / abs(det)^2
  float32_t det_abs2_inv = 1.0F / scal_mod2_cmplx_f32(det).re;
  armral_cmplx_f32_t inv_det = {det.re * det_abs2_inv, -det.im * det_abs2_inv};
  armral_cmplx_f32_t minus_inv_det = {-inv_det.re, -inv_det.im};
  // p_dst = | d, -b | * inv_det
  //         | -c, a |
  *out_a = scal_mul_cmplx_f32(d, inv_det);
  *out_b = scal_mul_cmplx_f32(b, minus_inv_det);
  *out_c = scal_mul_cmplx_f32(c, minus_inv_det);
  *out_d = scal_mul_cmplx_f32(a, inv_det);
}

static void invert_batch_matrix_2x2_2mat_impl(float32x4_t va, float32x4_t vb,
                                              float32x4_t vc, float32x4_t vd,
                                              float32x4_t *ve, float32x4_t *vf,
                                              float32x4_t *vg,
                                              float32x4_t *vh) {
  // det = a * d - b * c
  // inv_det = 1 / det = conj(det) / abs(det)^2
  // We compute the conjugate of the determinant directly and can use it
  // to compute the inverse determinant because abs(det) = abs(conj(det)).
  float32x4_t conj_det = neon_minor_cmplx_f32<true>(va, vb, vc, vd);
  float32x4_t inv_det = conj_det / neon_abs2_cmplx_f32(conj_det);
  float32x4_t minus_inv_det = vnegq_f32(inv_det);

  // p_dst = | d, -b | * inv_det
  //         | -c, a |
  *ve = neon_mul_cmplx_f32<false>(vd, inv_det);
  *vf = neon_mul_cmplx_f32<false>(vb, minus_inv_det);
  *vg = neon_mul_cmplx_f32<false>(vc, minus_inv_det);
  *vh = neon_mul_cmplx_f32<false>(va, inv_det);
}

static void invert_batch_matrix_2x2_4mat_impl(
    float32x4_t va_re, float32x4_t va_im, float32x4_t vb_re, float32x4_t vb_im,
    float32x4_t vc_re, float32x4_t vc_im, float32x4_t vd_re, float32x4_t vd_im,
    float32x4_t *ve_re, float32x4_t *ve_im, float32x4_t *vf_re,
    float32x4_t *vf_im, float32x4_t *vg_re, float32x4_t *vg_im,
    float32x4_t *vh_re, float32x4_t *vh_im) {
  // det = a * d - b * c
  // inv_det = 1 / det = conj(det) / abs(det)^2
  // We compute the conjugate of the determinant directly and can use it
  // to compute the inverse determinant because abs(det) = abs(conj(det)).
  float32x4_t conj_det_re = vmulq_f32(va_re, vd_re);
  conj_det_re = vfmsq_f32(conj_det_re, va_im, vd_im);
  conj_det_re = vfmsq_f32(conj_det_re, vb_re, vc_re);
  conj_det_re = vfmaq_f32(conj_det_re, vb_im, vc_im);

  float32x4_t conj_det_im = vmulq_f32(vb_re, vc_im);
  conj_det_im = vfmaq_f32(conj_det_im, vb_im, vc_re);
  conj_det_im = vfmsq_f32(conj_det_im, va_re, vd_im);
  conj_det_im = vfmsq_f32(conj_det_im, va_im, vd_re);

  float32x4_t det_abs2 = vmulq_f32(conj_det_re, conj_det_re);
  det_abs2 = vfmaq_f32(det_abs2, conj_det_im, conj_det_im);

  float32x4_t inv_det_abs2 = vdivq_f32(vdupq_n_f32(1.0F), det_abs2);

  float32x4_t inv_det_re = vmulq_f32(conj_det_re, inv_det_abs2);
  float32x4_t inv_det_im = vmulq_f32(conj_det_im, inv_det_abs2);

  float32x4_t minus_inv_det_re = vnegq_f32(inv_det_re);
  float32x4_t minus_inv_det_im = vnegq_f32(inv_det_im);

  *ve_re = vmulq_f32(vd_re, inv_det_re);
  *ve_re = vfmsq_f32(*ve_re, vd_im, inv_det_im);
  *ve_im = vmulq_f32(vd_re, inv_det_im);
  *ve_im = vfmaq_f32(*ve_im, vd_im, inv_det_re);

  *vf_re = vmulq_f32(vb_re, minus_inv_det_re);
  *vf_re = vfmsq_f32(*vf_re, vb_im, minus_inv_det_im);
  *vf_im = vmulq_f32(vb_re, minus_inv_det_im);
  *vf_im = vfmaq_f32(*vf_im, vb_im, minus_inv_det_re);

  *vg_re = vmulq_f32(vc_re, minus_inv_det_re);
  *vg_re = vfmsq_f32(*vg_re, vc_im, minus_inv_det_im);
  *vg_im = vmulq_f32(vc_re, minus_inv_det_im);
  *vg_im = vfmaq_f32(*vg_im, vc_im, minus_inv_det_re);

  *vh_re = vmulq_f32(va_re, inv_det_re);
  *vh_re = vfmsq_f32(*vh_re, va_im, inv_det_im);
  *vh_im = vmulq_f32(va_re, inv_det_im);
  *vh_im = vfmaq_f32(*vh_im, va_im, inv_det_re);
}

static void __attribute__((noinline, flatten))
invert_batch_matrix_2x2(uint32_t num_mats,
                        const armral_cmplx_f32_t *__restrict p_src,
                        armral_cmplx_f32_t *p_dst) {

  uint32_t stride = num_mats;
  uint32_t mat_i = 0;
  for (; mat_i + 4 <= num_mats; mat_i += 4) {
    float32x4_t va0 = vld1q_f32((const float32_t *)&p_src[0]);
    float32x4_t vb0 = vld1q_f32((const float32_t *)&p_src[1 * stride]);
    float32x4_t vc0 = vld1q_f32((const float32_t *)&p_src[2 * stride]);
    float32x4_t vd0 = vld1q_f32((const float32_t *)&p_src[3 * stride]);

    p_src += 2;

    float32x4_t va1 = vld1q_f32((const float32_t *)&p_src[0]);
    float32x4_t vb1 = vld1q_f32((const float32_t *)&p_src[1 * stride]);
    float32x4_t vc1 = vld1q_f32((const float32_t *)&p_src[2 * stride]);
    float32x4_t vd1 = vld1q_f32((const float32_t *)&p_src[3 * stride]);

    p_src += 2;

    float32x4_t va_re = vuzp1q_f32(va0, va1);
    float32x4_t va_im = vuzp2q_f32(va0, va1);
    float32x4_t vb_re = vuzp1q_f32(vb0, vb1);
    float32x4_t vb_im = vuzp2q_f32(vb0, vb1);
    float32x4_t vc_re = vuzp1q_f32(vc0, vc1);
    float32x4_t vc_im = vuzp2q_f32(vc0, vc1);
    float32x4_t vd_re = vuzp1q_f32(vd0, vd1);
    float32x4_t vd_im = vuzp2q_f32(vd0, vd1);

    float32x4_t ve_re;
    float32x4_t ve_im;
    float32x4_t vf_re;
    float32x4_t vf_im;
    float32x4_t vg_re;
    float32x4_t vg_im;
    float32x4_t vh_re;
    float32x4_t vh_im;
    invert_batch_matrix_2x2_4mat_impl(va_re, va_im, vb_re, vb_im, vc_re, vc_im,
                                      vd_re, vd_im, &ve_re, &ve_im, &vf_re,
                                      &vf_im, &vg_re, &vg_im, &vh_re, &vh_im);

    float32x4_t ve0 = vzip1q_f32(ve_re, ve_im);
    float32x4_t ve1 = vzip2q_f32(ve_re, ve_im);
    float32x4_t vf0 = vzip1q_f32(vf_re, vf_im);
    float32x4_t vf1 = vzip2q_f32(vf_re, vf_im);
    float32x4_t vg0 = vzip1q_f32(vg_re, vg_im);
    float32x4_t vg1 = vzip2q_f32(vg_re, vg_im);
    float32x4_t vh0 = vzip1q_f32(vh_re, vh_im);
    float32x4_t vh1 = vzip2q_f32(vh_re, vh_im);

    vst1q_f32((float32_t *)&p_dst[0], ve0);
    vst1q_f32((float32_t *)&p_dst[1 * stride], vf0);
    vst1q_f32((float32_t *)&p_dst[2 * stride], vg0);
    vst1q_f32((float32_t *)&p_dst[3 * stride], vh0);

    p_dst += 2;

    vst1q_f32((float32_t *)&p_dst[0], ve1);
    vst1q_f32((float32_t *)&p_dst[1 * stride], vf1);
    vst1q_f32((float32_t *)&p_dst[2 * stride], vg1);
    vst1q_f32((float32_t *)&p_dst[3 * stride], vh1);

    p_dst += 2;
  }

  for (; mat_i + 2 <= num_mats; mat_i += 2) {
    float32x4_t va = vld1q_f32((const float32_t *)&p_src[0]);
    float32x4_t vb = vld1q_f32((const float32_t *)&p_src[1 * stride]);
    float32x4_t vc = vld1q_f32((const float32_t *)&p_src[2 * stride]);
    float32x4_t vd = vld1q_f32((const float32_t *)&p_src[3 * stride]);

    float32x4_t ve;
    float32x4_t vf;
    float32x4_t vg;
    float32x4_t vh;
    invert_batch_matrix_2x2_2mat_impl(va, vb, vc, vd, &ve, &vf, &vg, &vh);

    vst1q_f32((float32_t *)&p_dst[0], ve);
    vst1q_f32((float32_t *)&p_dst[1 * stride], vf);
    vst1q_f32((float32_t *)&p_dst[2 * stride], vg);
    vst1q_f32((float32_t *)&p_dst[3 * stride], vh);

    p_src += 2;
    p_dst += 2;
  }

  if (mat_i < num_mats) {
    armral_cmplx_f32_t a = p_src[0];
    armral_cmplx_f32_t b = p_src[1 * stride];
    armral_cmplx_f32_t c = p_src[2 * stride];
    armral_cmplx_f32_t d = p_src[3 * stride];

    armral_cmplx_f32_t e;
    armral_cmplx_f32_t f;
    armral_cmplx_f32_t g;
    armral_cmplx_f32_t h;
    invert_batch_matrix_2x2_impl(a, b, c, d, &e, &f, &g, &h);

    p_dst[0] = e;
    p_dst[1 * stride] = f;
    p_dst[2 * stride] = g;
    p_dst[3 * stride] = h;
  }
}

static void __attribute__((noinline, flatten))
invert_two_batch_matrix_2x2(const armral_cmplx_f32_t *__restrict p_src,
                            armral_cmplx_f32_t *p_dst) {

  float32x4_t va = vld1q_f32((const float32_t *)&p_src[0]);
  float32x4_t vb = vld1q_f32((const float32_t *)&p_src[2]);
  float32x4_t vc = vld1q_f32((const float32_t *)&p_src[4]);
  float32x4_t vd = vld1q_f32((const float32_t *)&p_src[6]);

  float32x4_t ve;
  float32x4_t vf;
  float32x4_t vg;
  float32x4_t vh;
  invert_batch_matrix_2x2_2mat_impl(va, vb, vc, vd, &ve, &vf, &vg, &vh);

  vst1q_f32((float32_t *)&p_dst[0], ve);
  vst1q_f32((float32_t *)&p_dst[2], vf);
  vst1q_f32((float32_t *)&p_dst[4], vg);
  vst1q_f32((float32_t *)&p_dst[6], vh);
}

static void __attribute__((noinline, flatten))
invert_batch_matrix_2x2_pa(uint32_t num_mats,
                           const armral_cmplx_f32_t **__restrict p_srcs,
                           armral_cmplx_f32_t **__restrict p_dsts) {

  const armral_cmplx_f32_t *__restrict src_00 = p_srcs[0];
  const armral_cmplx_f32_t *__restrict src_01 = p_srcs[1];
  const armral_cmplx_f32_t *__restrict src_10 = p_srcs[2];
  const armral_cmplx_f32_t *__restrict src_11 = p_srcs[3];

  armral_cmplx_f32_t *__restrict dst_00 = p_dsts[0];
  armral_cmplx_f32_t *__restrict dst_01 = p_dsts[1];
  armral_cmplx_f32_t *__restrict dst_10 = p_dsts[2];
  armral_cmplx_f32_t *__restrict dst_11 = p_dsts[3];

  uint32_t mat_i = 0;
  for (; mat_i + 4 <= num_mats; mat_i += 4) {
    float32x4_t va0 = vld1q_f32((const float32_t *)(src_00));
    float32x4_t va1 = vld1q_f32((const float32_t *)(src_00 + 2));
    float32x4_t vb0 = vld1q_f32((const float32_t *)(src_01));
    float32x4_t vb1 = vld1q_f32((const float32_t *)(src_01 + 2));
    float32x4_t vc0 = vld1q_f32((const float32_t *)(src_10));
    float32x4_t vc1 = vld1q_f32((const float32_t *)(src_10 + 2));
    float32x4_t vd0 = vld1q_f32((const float32_t *)(src_11));
    float32x4_t vd1 = vld1q_f32((const float32_t *)(src_11 + 2));

    float32x4_t va_re = vuzp1q_f32(va0, va1);
    float32x4_t va_im = vuzp2q_f32(va0, va1);
    float32x4_t vb_re = vuzp1q_f32(vb0, vb1);
    float32x4_t vb_im = vuzp2q_f32(vb0, vb1);
    float32x4_t vc_re = vuzp1q_f32(vc0, vc1);
    float32x4_t vc_im = vuzp2q_f32(vc0, vc1);
    float32x4_t vd_re = vuzp1q_f32(vd0, vd1);
    float32x4_t vd_im = vuzp2q_f32(vd0, vd1);

    float32x4_t ve_re;
    float32x4_t ve_im;
    float32x4_t vf_re;
    float32x4_t vf_im;
    float32x4_t vg_re;
    float32x4_t vg_im;
    float32x4_t vh_re;
    float32x4_t vh_im;
    invert_batch_matrix_2x2_4mat_impl(va_re, va_im, vb_re, vb_im, vc_re, vc_im,
                                      vd_re, vd_im, &ve_re, &ve_im, &vf_re,
                                      &vf_im, &vg_re, &vg_im, &vh_re, &vh_im);

    float32x4_t ve0 = vzip1q_f32(ve_re, ve_im);
    float32x4_t ve1 = vzip2q_f32(ve_re, ve_im);
    float32x4_t vf0 = vzip1q_f32(vf_re, vf_im);
    float32x4_t vf1 = vzip2q_f32(vf_re, vf_im);
    float32x4_t vg0 = vzip1q_f32(vg_re, vg_im);
    float32x4_t vg1 = vzip2q_f32(vg_re, vg_im);
    float32x4_t vh0 = vzip1q_f32(vh_re, vh_im);
    float32x4_t vh1 = vzip2q_f32(vh_re, vh_im);

    vst1q_f32((float32_t *)(dst_00), ve0);
    vst1q_f32((float32_t *)(dst_00 + 2), ve1);
    vst1q_f32((float32_t *)(dst_01), vf0);
    vst1q_f32((float32_t *)(dst_01 + 2), vf1);
    vst1q_f32((float32_t *)(dst_10), vg0);
    vst1q_f32((float32_t *)(dst_10 + 2), vg1);
    vst1q_f32((float32_t *)(dst_11), vh0);
    vst1q_f32((float32_t *)(dst_11 + 2), vh1);

    src_00 += 4;
    src_01 += 4;
    src_10 += 4;
    src_11 += 4;

    dst_00 += 4;
    dst_01 += 4;
    dst_10 += 4;
    dst_11 += 4;
  }

  for (; mat_i + 2 <= num_mats; mat_i += 2) {
    float32x4_t va = vld1q_f32((const float32_t *)(src_00));
    float32x4_t vb = vld1q_f32((const float32_t *)(src_01));
    float32x4_t vc = vld1q_f32((const float32_t *)(src_10));
    float32x4_t vd = vld1q_f32((const float32_t *)(src_11));

    float32x4_t ve;
    float32x4_t vf;
    float32x4_t vg;
    float32x4_t vh;
    invert_batch_matrix_2x2_2mat_impl(va, vb, vc, vd, &ve, &vf, &vg, &vh);

    vst1q_f32((float32_t *)(dst_00), ve);
    vst1q_f32((float32_t *)(dst_01), vf);
    vst1q_f32((float32_t *)(dst_10), vg);
    vst1q_f32((float32_t *)(dst_11), vh);

    src_00 += 2;
    src_01 += 2;
    src_10 += 2;
    src_11 += 2;

    dst_00 += 2;
    dst_01 += 2;
    dst_10 += 2;
    dst_11 += 2;
  }

  if (mat_i < num_mats) {
    armral_cmplx_f32_t a = *src_00;
    armral_cmplx_f32_t b = *src_01;
    armral_cmplx_f32_t c = *src_10;
    armral_cmplx_f32_t d = *src_11;

    armral_cmplx_f32_t e;
    armral_cmplx_f32_t f;
    armral_cmplx_f32_t g;
    armral_cmplx_f32_t h;
    invert_batch_matrix_2x2_impl(a, b, c, d, &e, &f, &g, &h);

    *dst_00 = e;
    *dst_01 = f;
    *dst_10 = g;
    *dst_11 = h;
  }
}

static void __attribute__((noinline, flatten))
invert_two_batch_matrix_2x2_pa(const armral_cmplx_f32_t **__restrict p_srcs,
                               armral_cmplx_f32_t **__restrict p_dsts) {

  const armral_cmplx_f32_t *__restrict src_00 = p_srcs[0];
  const armral_cmplx_f32_t *__restrict src_01 = p_srcs[1];
  const armral_cmplx_f32_t *__restrict src_10 = p_srcs[2];
  const armral_cmplx_f32_t *__restrict src_11 = p_srcs[3];

  armral_cmplx_f32_t *__restrict dst_00 = p_dsts[0];
  armral_cmplx_f32_t *__restrict dst_01 = p_dsts[1];
  armral_cmplx_f32_t *__restrict dst_10 = p_dsts[2];
  armral_cmplx_f32_t *__restrict dst_11 = p_dsts[3];

  float32x4_t va = vld1q_f32((const float32_t *)src_00);
  float32x4_t vb = vld1q_f32((const float32_t *)src_01);
  float32x4_t vc = vld1q_f32((const float32_t *)src_10);
  float32x4_t vd = vld1q_f32((const float32_t *)src_11);

  float32x4_t ve;
  float32x4_t vf;
  float32x4_t vg;
  float32x4_t vh;
  invert_batch_matrix_2x2_2mat_impl(va, vb, vc, vd, &ve, &vf, &vg, &vh);

  vst1q_f32((float32_t *)dst_00, ve);
  vst1q_f32((float32_t *)dst_01, vf);
  vst1q_f32((float32_t *)dst_10, vg);
  vst1q_f32((float32_t *)dst_11, vh);
}

static void invert_batch_matrix_3x3_impl(
    armral_cmplx_f32_t a00, armral_cmplx_f32_t a01, armral_cmplx_f32_t a02,
    armral_cmplx_f32_t a10, armral_cmplx_f32_t a11, armral_cmplx_f32_t a12,
    armral_cmplx_f32_t a20, armral_cmplx_f32_t a21, armral_cmplx_f32_t a22,
    armral_cmplx_f32_t *out_a00, armral_cmplx_f32_t *out_a01,
    armral_cmplx_f32_t *out_a02, armral_cmplx_f32_t *out_a10,
    armral_cmplx_f32_t *out_a11, armral_cmplx_f32_t *out_a12,
    armral_cmplx_f32_t *out_a20, armral_cmplx_f32_t *out_a21,
    armral_cmplx_f32_t *out_a22) {
  const armral_cmplx_f32_t p_src[9] = {a00, a01, a02, a10, a11,
                                       a12, a20, a21, a22};
  armral_cmplx_f32_t p_dst[9];

  invert_matrix<3>(p_src, p_dst);

  *out_a00 = p_dst[0];
  *out_a01 = p_dst[1];
  *out_a02 = p_dst[2];
  *out_a10 = p_dst[3];
  *out_a11 = p_dst[4];
  *out_a12 = p_dst[5];
  *out_a20 = p_dst[6];
  *out_a21 = p_dst[7];
  *out_a22 = p_dst[8];
}

static void invert_batch_matrix_3x3_vec_impl(
    float32x4_t a00, float32x4_t a01, float32x4_t a02, float32x4_t a10,
    float32x4_t a11, float32x4_t a12, float32x4_t a20, float32x4_t a21,
    float32x4_t a22, float32x4_t *out_a00, float32x4_t *out_a01,
    float32x4_t *out_a02, float32x4_t *out_a10, float32x4_t *out_a11,
    float32x4_t *out_a12, float32x4_t *out_a20, float32x4_t *out_a21,
    float32x4_t *out_a22) {

  // Compute minors, transpose, and apply negative signs
  float32x4_t adj00 = neon_minor_cmplx_f32<false>(a11, a12, a21, a22);
  float32x4_t adj11 = neon_minor_cmplx_f32<false>(a00, a02, a20, a22);
  float32x4_t adj22 = neon_minor_cmplx_f32<false>(a00, a01, a10, a11);
  float32x4_t adj10 = neon_minor_cmplx_f32<false>(a20, a22, a10, a12);
  float32x4_t adj20 = neon_minor_cmplx_f32<false>(a10, a11, a20, a21);
  float32x4_t adj01 = neon_minor_cmplx_f32<false>(a21, a22, a01, a02);
  float32x4_t adj21 = neon_minor_cmplx_f32<false>(a20, a21, a00, a01);
  float32x4_t adj02 = neon_minor_cmplx_f32<false>(a01, a02, a11, a12);
  float32x4_t adj12 = neon_minor_cmplx_f32<false>(a10, a12, a00, a02);

  // Determinant: A_{0:} * adj(A)_{:0}
  float32x4_t det0 = neon_mul_cmplx_f32<false>(a00, adj00);
  float32x4_t det1 = neon_mul_cmplx_f32<false>(a01, adj10);
  float32x4_t det2 = neon_mul_cmplx_f32<false>(a02, adj20);
  float32x4_t det;
  det = vaddq_f32(det0, det1);
  det = vaddq_f32(det, det2);

  // 1 / det = conj(det) / abs(det)^2
  float32x4_t inv_det = neon_recip_cmplx_f32(det);

  // Write into output array
  *out_a00 = neon_mul_cmplx_f32<false>(inv_det, adj00);
  *out_a01 = neon_mul_cmplx_f32<false>(inv_det, adj01);
  *out_a02 = neon_mul_cmplx_f32<false>(inv_det, adj02);
  *out_a10 = neon_mul_cmplx_f32<false>(inv_det, adj10);
  *out_a11 = neon_mul_cmplx_f32<false>(inv_det, adj11);
  *out_a12 = neon_mul_cmplx_f32<false>(inv_det, adj12);
  *out_a20 = neon_mul_cmplx_f32<false>(inv_det, adj20);
  *out_a21 = neon_mul_cmplx_f32<false>(inv_det, adj21);
  *out_a22 = neon_mul_cmplx_f32<false>(inv_det, adj22);
}

static void __attribute__((noinline, flatten))
invert_batch_matrix_3x3(uint32_t num_mats,
                        const armral_cmplx_f32_t *__restrict p_src,
                        armral_cmplx_f32_t *p_dst) {

  uint32_t stride = num_mats;

  for (uint32_t mat_i = 0; mat_i + 2 <= num_mats; mat_i += 2) {
    float32x4_t va00 = vld1q_f32((const float32_t *)&p_src[0]);
    float32x4_t va01 = vld1q_f32((const float32_t *)&p_src[1 * stride]);
    float32x4_t va02 = vld1q_f32((const float32_t *)&p_src[2 * stride]);
    float32x4_t va10 = vld1q_f32((const float32_t *)&p_src[3 * stride]);
    float32x4_t va11 = vld1q_f32((const float32_t *)&p_src[4 * stride]);
    float32x4_t va12 = vld1q_f32((const float32_t *)&p_src[5 * stride]);
    float32x4_t va20 = vld1q_f32((const float32_t *)&p_src[6 * stride]);
    float32x4_t va21 = vld1q_f32((const float32_t *)&p_src[7 * stride]);
    float32x4_t va22 = vld1q_f32((const float32_t *)&p_src[8 * stride]);

    float32x4_t vout_a00;
    float32x4_t vout_a01;
    float32x4_t vout_a02;
    float32x4_t vout_a10;
    float32x4_t vout_a11;
    float32x4_t vout_a12;
    float32x4_t vout_a20;
    float32x4_t vout_a21;
    float32x4_t vout_a22;
    invert_batch_matrix_3x3_vec_impl(va00, va01, va02, va10, va11, va12, va20,
                                     va21, va22, &vout_a00, &vout_a01,
                                     &vout_a02, &vout_a10, &vout_a11, &vout_a12,
                                     &vout_a20, &vout_a21, &vout_a22);

    vst1q_f32((float32_t *)&p_dst[0], vout_a00);
    vst1q_f32((float32_t *)&p_dst[1 * stride], vout_a01);
    vst1q_f32((float32_t *)&p_dst[2 * stride], vout_a02);
    vst1q_f32((float32_t *)&p_dst[3 * stride], vout_a10);
    vst1q_f32((float32_t *)&p_dst[4 * stride], vout_a11);
    vst1q_f32((float32_t *)&p_dst[5 * stride], vout_a12);
    vst1q_f32((float32_t *)&p_dst[6 * stride], vout_a20);
    vst1q_f32((float32_t *)&p_dst[7 * stride], vout_a21);
    vst1q_f32((float32_t *)&p_dst[8 * stride], vout_a22);

    p_src += 2;
    p_dst += 2;
  }

  if (num_mats % 2 == 1) {
    armral_cmplx_f32_t a00 = p_src[0];
    armral_cmplx_f32_t a01 = p_src[1 * stride];
    armral_cmplx_f32_t a02 = p_src[2 * stride];
    armral_cmplx_f32_t a10 = p_src[3 * stride];
    armral_cmplx_f32_t a11 = p_src[4 * stride];
    armral_cmplx_f32_t a12 = p_src[5 * stride];
    armral_cmplx_f32_t a20 = p_src[6 * stride];
    armral_cmplx_f32_t a21 = p_src[7 * stride];
    armral_cmplx_f32_t a22 = p_src[8 * stride];

    armral_cmplx_f32_t out_a00;
    armral_cmplx_f32_t out_a01;
    armral_cmplx_f32_t out_a02;
    armral_cmplx_f32_t out_a10;
    armral_cmplx_f32_t out_a11;
    armral_cmplx_f32_t out_a12;
    armral_cmplx_f32_t out_a20;
    armral_cmplx_f32_t out_a21;
    armral_cmplx_f32_t out_a22;

    invert_batch_matrix_3x3_impl(
        a00, a01, a02, a10, a11, a12, a20, a21, a22, &out_a00, &out_a01,
        &out_a02, &out_a10, &out_a11, &out_a12, &out_a20, &out_a21, &out_a22);

    p_dst[0 * stride] = out_a00;
    p_dst[1 * stride] = out_a01;
    p_dst[2 * stride] = out_a02;
    p_dst[3 * stride] = out_a10;
    p_dst[4 * stride] = out_a11;
    p_dst[5 * stride] = out_a12;
    p_dst[6 * stride] = out_a20;
    p_dst[7 * stride] = out_a21;
    p_dst[8 * stride] = out_a22;
  }
}

static void __attribute__((noinline, flatten))
invert_batch_matrix_3x3_pa(uint32_t num_mats,
                           const armral_cmplx_f32_t **__restrict p_srcs,
                           armral_cmplx_f32_t **__restrict p_dsts) {

  const armral_cmplx_f32_t *__restrict src_00 = p_srcs[0];
  const armral_cmplx_f32_t *__restrict src_01 = p_srcs[1];
  const armral_cmplx_f32_t *__restrict src_02 = p_srcs[2];
  const armral_cmplx_f32_t *__restrict src_10 = p_srcs[3];
  const armral_cmplx_f32_t *__restrict src_11 = p_srcs[4];
  const armral_cmplx_f32_t *__restrict src_12 = p_srcs[5];
  const armral_cmplx_f32_t *__restrict src_20 = p_srcs[6];
  const armral_cmplx_f32_t *__restrict src_21 = p_srcs[7];
  const armral_cmplx_f32_t *__restrict src_22 = p_srcs[8];

  armral_cmplx_f32_t *__restrict dst_00 = p_dsts[0];
  armral_cmplx_f32_t *__restrict dst_01 = p_dsts[1];
  armral_cmplx_f32_t *__restrict dst_02 = p_dsts[2];
  armral_cmplx_f32_t *__restrict dst_10 = p_dsts[3];
  armral_cmplx_f32_t *__restrict dst_11 = p_dsts[4];
  armral_cmplx_f32_t *__restrict dst_12 = p_dsts[5];
  armral_cmplx_f32_t *__restrict dst_20 = p_dsts[6];
  armral_cmplx_f32_t *__restrict dst_21 = p_dsts[7];
  armral_cmplx_f32_t *__restrict dst_22 = p_dsts[8];

  for (uint32_t mat_i = 0; mat_i + 2 <= num_mats; mat_i += 2) {
    float32x4_t va00 = vld1q_f32((const float32_t *)src_00);
    float32x4_t va01 = vld1q_f32((const float32_t *)src_01);
    float32x4_t va02 = vld1q_f32((const float32_t *)src_02);
    float32x4_t va10 = vld1q_f32((const float32_t *)src_10);
    float32x4_t va11 = vld1q_f32((const float32_t *)src_11);
    float32x4_t va12 = vld1q_f32((const float32_t *)src_12);
    float32x4_t va20 = vld1q_f32((const float32_t *)src_20);
    float32x4_t va21 = vld1q_f32((const float32_t *)src_21);
    float32x4_t va22 = vld1q_f32((const float32_t *)src_22);

    float32x4_t vout_a00;
    float32x4_t vout_a01;
    float32x4_t vout_a02;
    float32x4_t vout_a10;
    float32x4_t vout_a11;
    float32x4_t vout_a12;
    float32x4_t vout_a20;
    float32x4_t vout_a21;
    float32x4_t vout_a22;
    invert_batch_matrix_3x3_vec_impl(va00, va01, va02, va10, va11, va12, va20,
                                     va21, va22, &vout_a00, &vout_a01,
                                     &vout_a02, &vout_a10, &vout_a11, &vout_a12,
                                     &vout_a20, &vout_a21, &vout_a22);

    vst1q_f32((float32_t *)dst_00, vout_a00);
    vst1q_f32((float32_t *)dst_01, vout_a01);
    vst1q_f32((float32_t *)dst_02, vout_a02);
    vst1q_f32((float32_t *)dst_10, vout_a10);
    vst1q_f32((float32_t *)dst_11, vout_a11);
    vst1q_f32((float32_t *)dst_12, vout_a12);
    vst1q_f32((float32_t *)dst_20, vout_a20);
    vst1q_f32((float32_t *)dst_21, vout_a21);
    vst1q_f32((float32_t *)dst_22, vout_a22);

    src_00 += 2;
    src_01 += 2;
    src_02 += 2;
    src_10 += 2;
    src_11 += 2;
    src_12 += 2;
    src_20 += 2;
    src_21 += 2;
    src_22 += 2;

    dst_00 += 2;
    dst_01 += 2;
    dst_02 += 2;
    dst_10 += 2;
    dst_11 += 2;
    dst_12 += 2;
    dst_20 += 2;
    dst_21 += 2;
    dst_22 += 2;
  }

  if (num_mats % 2 == 1) {
    armral_cmplx_f32_t a00 = *src_00;
    armral_cmplx_f32_t a01 = *src_01;
    armral_cmplx_f32_t a02 = *src_02;
    armral_cmplx_f32_t a10 = *src_10;
    armral_cmplx_f32_t a11 = *src_11;
    armral_cmplx_f32_t a12 = *src_12;
    armral_cmplx_f32_t a20 = *src_20;
    armral_cmplx_f32_t a21 = *src_21;
    armral_cmplx_f32_t a22 = *src_22;

    armral_cmplx_f32_t out_a00;
    armral_cmplx_f32_t out_a01;
    armral_cmplx_f32_t out_a02;
    armral_cmplx_f32_t out_a10;
    armral_cmplx_f32_t out_a11;
    armral_cmplx_f32_t out_a12;
    armral_cmplx_f32_t out_a20;
    armral_cmplx_f32_t out_a21;
    armral_cmplx_f32_t out_a22;

    invert_batch_matrix_3x3_impl(
        a00, a01, a02, a10, a11, a12, a20, a21, a22, &out_a00, &out_a01,
        &out_a02, &out_a10, &out_a11, &out_a12, &out_a20, &out_a21, &out_a22);

    *dst_00 = out_a00;
    *dst_01 = out_a01;
    *dst_02 = out_a02;
    *dst_10 = out_a10;
    *dst_11 = out_a11;
    *dst_12 = out_a12;
    *dst_20 = out_a20;
    *dst_21 = out_a21;
    *dst_22 = out_a22;
  }
}

static void invert_batch_matrix_4x4_impl(
    armral_cmplx_f32_t a00, armral_cmplx_f32_t a01, armral_cmplx_f32_t a02,
    armral_cmplx_f32_t a03, armral_cmplx_f32_t a10, armral_cmplx_f32_t a11,
    armral_cmplx_f32_t a12, armral_cmplx_f32_t a13, armral_cmplx_f32_t a20,
    armral_cmplx_f32_t a21, armral_cmplx_f32_t a22, armral_cmplx_f32_t a23,
    armral_cmplx_f32_t a30, armral_cmplx_f32_t a31, armral_cmplx_f32_t a32,
    armral_cmplx_f32_t a33, armral_cmplx_f32_t *out_a00,
    armral_cmplx_f32_t *out_a01, armral_cmplx_f32_t *out_a02,
    armral_cmplx_f32_t *out_a03, armral_cmplx_f32_t *out_a10,
    armral_cmplx_f32_t *out_a11, armral_cmplx_f32_t *out_a12,
    armral_cmplx_f32_t *out_a13, armral_cmplx_f32_t *out_a20,
    armral_cmplx_f32_t *out_a21, armral_cmplx_f32_t *out_a22,
    armral_cmplx_f32_t *out_a23, armral_cmplx_f32_t *out_a30,
    armral_cmplx_f32_t *out_a31, armral_cmplx_f32_t *out_a32,
    armral_cmplx_f32_t *out_a33) {

  const armral_cmplx_f32_t p_src[16] = {a00, a01, a02, a03, a10, a11, a12, a13,
                                        a20, a21, a22, a23, a30, a31, a32, a33};
  armral_cmplx_f32_t p_dst[16];

  invert_matrix<4>(p_src, p_dst);

  *out_a00 = p_dst[0];
  *out_a01 = p_dst[1];
  *out_a02 = p_dst[2];
  *out_a03 = p_dst[3];
  *out_a10 = p_dst[4];
  *out_a11 = p_dst[5];
  *out_a12 = p_dst[6];
  *out_a13 = p_dst[7];
  *out_a20 = p_dst[8];
  *out_a21 = p_dst[9];
  *out_a22 = p_dst[10];
  *out_a23 = p_dst[11];
  *out_a30 = p_dst[12];
  *out_a31 = p_dst[13];
  *out_a32 = p_dst[14];
  *out_a33 = p_dst[15];
}

static void __attribute__((noinline, flatten))
invert_batch_matrix_4x4(uint32_t num_mats,
                        const armral_cmplx_f32_t *__restrict p_src,
                        armral_cmplx_f32_t *p_dst) {

  uint32_t stride = num_mats;

  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i++) {
    armral_cmplx_f32_t a00 = p_src[0];
    armral_cmplx_f32_t a01 = p_src[1 * stride];
    armral_cmplx_f32_t a02 = p_src[2 * stride];
    armral_cmplx_f32_t a03 = p_src[3 * stride];
    armral_cmplx_f32_t a10 = p_src[4 * stride];
    armral_cmplx_f32_t a11 = p_src[5 * stride];
    armral_cmplx_f32_t a12 = p_src[6 * stride];
    armral_cmplx_f32_t a13 = p_src[7 * stride];
    armral_cmplx_f32_t a20 = p_src[8 * stride];
    armral_cmplx_f32_t a21 = p_src[9 * stride];
    armral_cmplx_f32_t a22 = p_src[10 * stride];
    armral_cmplx_f32_t a23 = p_src[11 * stride];
    armral_cmplx_f32_t a30 = p_src[12 * stride];
    armral_cmplx_f32_t a31 = p_src[13 * stride];
    armral_cmplx_f32_t a32 = p_src[14 * stride];
    armral_cmplx_f32_t a33 = p_src[15 * stride];

    armral_cmplx_f32_t out_a00;
    armral_cmplx_f32_t out_a01;
    armral_cmplx_f32_t out_a02;
    armral_cmplx_f32_t out_a03;
    armral_cmplx_f32_t out_a10;
    armral_cmplx_f32_t out_a11;
    armral_cmplx_f32_t out_a12;
    armral_cmplx_f32_t out_a13;
    armral_cmplx_f32_t out_a20;
    armral_cmplx_f32_t out_a21;
    armral_cmplx_f32_t out_a22;
    armral_cmplx_f32_t out_a23;
    armral_cmplx_f32_t out_a30;
    armral_cmplx_f32_t out_a31;
    armral_cmplx_f32_t out_a32;
    armral_cmplx_f32_t out_a33;

    invert_batch_matrix_4x4_impl(
        a00, a01, a02, a03, a10, a11, a12, a13, a20, a21, a22, a23, a30, a31,
        a32, a33, &out_a00, &out_a01, &out_a02, &out_a03, &out_a10, &out_a11,
        &out_a12, &out_a13, &out_a20, &out_a21, &out_a22, &out_a23, &out_a30,
        &out_a31, &out_a32, &out_a33);

    p_dst[0 * stride] = out_a00;
    p_dst[1 * stride] = out_a01;
    p_dst[2 * stride] = out_a02;
    p_dst[3 * stride] = out_a03;
    p_dst[4 * stride] = out_a10;
    p_dst[5 * stride] = out_a11;
    p_dst[6 * stride] = out_a12;
    p_dst[7 * stride] = out_a13;
    p_dst[8 * stride] = out_a20;
    p_dst[9 * stride] = out_a21;
    p_dst[10 * stride] = out_a22;
    p_dst[11 * stride] = out_a23;
    p_dst[12 * stride] = out_a30;
    p_dst[13 * stride] = out_a31;
    p_dst[14 * stride] = out_a32;
    p_dst[15 * stride] = out_a33;

    p_src++;
    p_dst++;
  }
}

static void __attribute__((noinline, flatten)) invert_batch_matrix_4x4_pa(
    uint32_t num_mats, const armral_cmplx_f32_t *__restrict *__restrict p_srcs,
    armral_cmplx_f32_t *__restrict *__restrict p_dsts) {

  const armral_cmplx_f32_t *src_00 = p_srcs[0];
  const armral_cmplx_f32_t *src_01 = p_srcs[1];
  const armral_cmplx_f32_t *src_02 = p_srcs[2];
  const armral_cmplx_f32_t *src_03 = p_srcs[3];
  const armral_cmplx_f32_t *src_10 = p_srcs[4];
  const armral_cmplx_f32_t *src_11 = p_srcs[5];
  const armral_cmplx_f32_t *src_12 = p_srcs[6];
  const armral_cmplx_f32_t *src_13 = p_srcs[7];
  const armral_cmplx_f32_t *src_20 = p_srcs[8];
  const armral_cmplx_f32_t *src_21 = p_srcs[9];
  const armral_cmplx_f32_t *src_22 = p_srcs[10];
  const armral_cmplx_f32_t *src_23 = p_srcs[11];
  const armral_cmplx_f32_t *src_30 = p_srcs[12];
  const armral_cmplx_f32_t *src_31 = p_srcs[13];
  const armral_cmplx_f32_t *src_32 = p_srcs[14];
  const armral_cmplx_f32_t *src_33 = p_srcs[15];

  armral_cmplx_f32_t *dst_00 = p_dsts[0];
  armral_cmplx_f32_t *dst_01 = p_dsts[1];
  armral_cmplx_f32_t *dst_02 = p_dsts[2];
  armral_cmplx_f32_t *dst_03 = p_dsts[3];
  armral_cmplx_f32_t *dst_10 = p_dsts[4];
  armral_cmplx_f32_t *dst_11 = p_dsts[5];
  armral_cmplx_f32_t *dst_12 = p_dsts[6];
  armral_cmplx_f32_t *dst_13 = p_dsts[7];
  armral_cmplx_f32_t *dst_20 = p_dsts[8];
  armral_cmplx_f32_t *dst_21 = p_dsts[9];
  armral_cmplx_f32_t *dst_22 = p_dsts[10];
  armral_cmplx_f32_t *dst_23 = p_dsts[11];
  armral_cmplx_f32_t *dst_30 = p_dsts[12];
  armral_cmplx_f32_t *dst_31 = p_dsts[13];
  armral_cmplx_f32_t *dst_32 = p_dsts[14];
  armral_cmplx_f32_t *dst_33 = p_dsts[15];

  for (uint32_t mat_i = 0; mat_i < num_mats; mat_i++) {
    armral_cmplx_f32_t a00 = *src_00++;
    armral_cmplx_f32_t a01 = *src_01++;
    armral_cmplx_f32_t a02 = *src_02++;
    armral_cmplx_f32_t a03 = *src_03++;
    armral_cmplx_f32_t a10 = *src_10++;
    armral_cmplx_f32_t a11 = *src_11++;
    armral_cmplx_f32_t a12 = *src_12++;
    armral_cmplx_f32_t a13 = *src_13++;
    armral_cmplx_f32_t a20 = *src_20++;
    armral_cmplx_f32_t a21 = *src_21++;
    armral_cmplx_f32_t a22 = *src_22++;
    armral_cmplx_f32_t a23 = *src_23++;
    armral_cmplx_f32_t a30 = *src_30++;
    armral_cmplx_f32_t a31 = *src_31++;
    armral_cmplx_f32_t a32 = *src_32++;
    armral_cmplx_f32_t a33 = *src_33++;

    armral_cmplx_f32_t out_a00;
    armral_cmplx_f32_t out_a01;
    armral_cmplx_f32_t out_a02;
    armral_cmplx_f32_t out_a03;
    armral_cmplx_f32_t out_a10;
    armral_cmplx_f32_t out_a11;
    armral_cmplx_f32_t out_a12;
    armral_cmplx_f32_t out_a13;
    armral_cmplx_f32_t out_a20;
    armral_cmplx_f32_t out_a21;
    armral_cmplx_f32_t out_a22;
    armral_cmplx_f32_t out_a23;
    armral_cmplx_f32_t out_a30;
    armral_cmplx_f32_t out_a31;
    armral_cmplx_f32_t out_a32;
    armral_cmplx_f32_t out_a33;

    invert_batch_matrix_4x4_impl(
        a00, a01, a02, a03, a10, a11, a12, a13, a20, a21, a22, a23, a30, a31,
        a32, a33, &out_a00, &out_a01, &out_a02, &out_a03, &out_a10, &out_a11,
        &out_a12, &out_a13, &out_a20, &out_a21, &out_a22, &out_a23, &out_a30,
        &out_a31, &out_a32, &out_a33);

    *dst_00++ = out_a00;
    *dst_01++ = out_a01;
    *dst_02++ = out_a02;
    *dst_03++ = out_a03;
    *dst_10++ = out_a10;
    *dst_11++ = out_a11;
    *dst_12++ = out_a12;
    *dst_13++ = out_a13;
    *dst_20++ = out_a20;
    *dst_21++ = out_a21;
    *dst_22++ = out_a22;
    *dst_23++ = out_a23;
    *dst_30++ = out_a30;
    *dst_31++ = out_a31;
    *dst_32++ = out_a32;
    *dst_33++ = out_a33;
  }
}

} // namespace armral::cmplx_mat_inv

armral_status
armral_cmplx_mat_inverse_f32(uint32_t size,
                             const armral_cmplx_f32_t *__restrict p_src,
                             armral_cmplx_f32_t *__restrict p_dst) {
  switch (size) {
  case 2:
    armral::cmplx_mat_inv::invert_matrix<2>(p_src, p_dst);
    break;
  case 3:
    armral::cmplx_mat_inv::invert_matrix<3>(p_src, p_dst);
    break;
  case 4:
    armral::cmplx_mat_inv::invert_matrix<4>(p_src, p_dst);
    break;
  case 8:
    armral::cmplx_mat_inv::invert_matrix<8>(p_src, p_dst);
    break;
  case 16:
    armral::cmplx_mat_inv::invert_matrix<16>(p_src, p_dst);
    break;
  default:
    return ARMRAL_ARGUMENT_ERROR;
  }
  return ARMRAL_SUCCESS;
}

armral_status
armral_cmplx_mat_inverse_batch_f32(uint32_t num_mats, uint32_t size,
                                   const armral_cmplx_f32_t *__restrict p_src,
                                   armral_cmplx_f32_t *__restrict p_dst) {
  switch (size) {
  case 2:
    switch (num_mats) {
    case 2:
      armral::cmplx_mat_inv::invert_two_batch_matrix_2x2(p_src, p_dst);
      break;
    default:
      armral::cmplx_mat_inv::invert_batch_matrix_2x2(num_mats, p_src, p_dst);
    }
    break;
  case 3:
    armral::cmplx_mat_inv::invert_batch_matrix_3x3(num_mats, p_src, p_dst);
    break;
  case 4:
    armral::cmplx_mat_inv::invert_batch_matrix_4x4(num_mats, p_src, p_dst);
    break;
  default:
    return ARMRAL_ARGUMENT_ERROR;
  }
  return ARMRAL_SUCCESS;
}

armral_status armral_cmplx_mat_inverse_batch_f32_pa(
    uint32_t num_mats, uint32_t size,
    const armral_cmplx_f32_t **__restrict p_srcs,
    armral_cmplx_f32_t **__restrict p_dsts) {
  switch (size) {
  case 2:
    switch (num_mats) {
    case 2:
      armral::cmplx_mat_inv::invert_two_batch_matrix_2x2_pa(p_srcs, p_dsts);
      break;
    default:
      armral::cmplx_mat_inv::invert_batch_matrix_2x2_pa(num_mats, p_srcs,
                                                        p_dsts);
    }
    break;
  case 3:
    armral::cmplx_mat_inv::invert_batch_matrix_3x3_pa(num_mats, p_srcs, p_dsts);
    break;
  case 4:
    armral::cmplx_mat_inv::invert_batch_matrix_4x4_pa(num_mats, p_srcs, p_dsts);
    break;
  default:
    return ARMRAL_ARGUMENT_ERROR;
  }
  return ARMRAL_SUCCESS;
}
