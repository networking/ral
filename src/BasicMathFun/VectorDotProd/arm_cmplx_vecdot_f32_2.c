/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

armral_status armral_cmplx_vecdot_f32_2(uint32_t n,
                                        const float32_t *restrict p_src_a_re,
                                        const float32_t *restrict p_src_a_im,
                                        const float32_t *restrict p_src_b_re,
                                        const float32_t *restrict p_src_b_im,
                                        float32_t *p_src_c_re,
                                        float32_t *p_src_c_im) {
#ifdef ARMRAL_ARCH_SVE
  uint32_t num_lanes = svcntw();
  uint32_t full_vectors = n / num_lanes;
  svbool_t pg = svptrue_b32();
  svfloat32_t acc_real = svdup_f32(0);
  svfloat32_t acc_imag = svdup_f32(0);

  for (uint32_t i = 0; i < full_vectors; ++i) {
    svfloat32_t vec_a_real = svld1_f32(pg, p_src_a_re);
    svfloat32_t vec_a_imag = svld1_f32(pg, p_src_a_im);
    svfloat32_t vec_b_real = svld1_f32(pg, p_src_b_re);
    svfloat32_t vec_b_imag = svld1_f32(pg, p_src_b_im);

    p_src_a_re += num_lanes;
    p_src_a_im += num_lanes;
    p_src_b_re += num_lanes;
    p_src_b_im += num_lanes;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    acc_real = svmla_f32_x(pg, acc_real, vec_a_real, vec_b_real);
    acc_real = svmls_f32_x(pg, acc_real, vec_a_imag, vec_b_imag);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    acc_imag = svmla_f32_x(pg, acc_imag, vec_a_real, vec_b_imag);
    acc_imag = svmla_f32_x(pg, acc_imag, vec_a_imag, vec_b_real);
  }

  uint32_t tail_size = n % num_lanes;
  if (tail_size) {
    svbool_t tail_pg = svwhilelt_b32(0U, tail_size);

    svfloat32_t vec_a_real = svld1_f32(tail_pg, p_src_a_re);
    svfloat32_t vec_a_imag = svld1_f32(tail_pg, p_src_a_im);
    svfloat32_t vec_b_real = svld1_f32(tail_pg, p_src_b_re);
    svfloat32_t vec_b_imag = svld1_f32(tail_pg, p_src_b_im);

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    acc_real = svmla_f32_m(tail_pg, acc_real, vec_a_real, vec_b_real);
    acc_real = svmls_f32_m(tail_pg, acc_real, vec_a_imag, vec_b_imag);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    acc_imag = svmla_f32_m(tail_pg, acc_imag, vec_a_real, vec_b_imag);
    acc_imag = svmla_f32_m(tail_pg, acc_imag, vec_a_imag, vec_b_real);
  }

  *p_src_c_re = svaddv_f32(pg, acc_real);
  *p_src_c_im = svaddv_f32(pg, acc_imag);

  return ARMRAL_SUCCESS;
#else
  uint32_t blk_cnt; /* Loop counter */
  float32_t real_sum = 0.0F;
  float32_t imag_sum = 0.0F; /* Temporary result variables */
  float32_t re_a;
  float32_t im_a;
  float32_t re_b;
  float32_t im_b;

  float32x4_t vec_a_re[2];
  float32x4_t vec_a_im[2];
  float32x4_t vec_b_re[2];
  float32x4_t vec_b_im[2];

  float32x4_t acc_r = vdupq_n_f32(0.0);
  float32x4_t acc_i = vdupq_n_f32(0.0);

  /* Loop unrolling: Compute 8 outputs at a time */
  blk_cnt = n >> 3U;

  while (blk_cnt > 0U) {
    /* C = (A[0]+jA[1])*(B[0]+jB[1]) + ...  */
    /* Calculate dot product and then store the result in a temporary buffer. */
    vec_a_re[0] = vld1q_f32(p_src_a_re);
    vec_a_re[1] = vld1q_f32(p_src_a_re + 4);
    vec_a_im[0] = vld1q_f32(p_src_a_im);
    vec_a_im[1] = vld1q_f32(p_src_a_im + 4);

    vec_b_re[0] = vld1q_f32(p_src_b_re);
    vec_b_re[1] = vld1q_f32(p_src_b_re + 4);
    vec_b_im[0] = vld1q_f32(p_src_b_im);
    vec_b_im[1] = vld1q_f32(p_src_b_im + 4);

    /* Increment pointers */
    p_src_a_re += 8;
    p_src_a_im += 8;
    p_src_b_re += 8;
    p_src_b_im += 8;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    acc_r = vfmaq_f32(acc_r, vec_a_re[0], vec_b_re[0]);
    acc_r = vfmaq_f32(acc_r, vec_a_re[1], vec_b_re[1]);
    acc_r = vfmsq_f32(acc_r, vec_a_im[0], vec_b_im[0]);
    acc_r = vfmsq_f32(acc_r, vec_a_im[1], vec_b_im[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    acc_i = vfmaq_f32(acc_i, vec_a_im[0], vec_b_re[0]);
    acc_i = vfmaq_f32(acc_i, vec_a_im[1], vec_b_re[1]);
    acc_i = vfmaq_f32(acc_i, vec_a_re[0], vec_b_im[0]);
    acc_i = vfmaq_f32(acc_i, vec_a_re[1], vec_b_im[1]);

    /* Decrement the loop counter */
    blk_cnt--;
  }

  float32x2_t accum = vpadd_f32(vget_low_f32(acc_r), vget_high_f32(acc_r));
  real_sum += accum[0] + accum[1];

  accum = vpadd_f32(vget_low_f32(acc_i), vget_high_f32(acc_i));
  imag_sum += accum[0] + accum[1];

  /* Tail */
  blk_cnt = n & 0x7;

  while (blk_cnt > 0U) {
    re_a = *p_src_a_re;
    im_a = *p_src_a_im;
    re_b = *p_src_b_re;
    im_b = *p_src_b_im;

    real_sum += re_a * re_b;
    imag_sum += re_a * im_b;
    real_sum -= im_a * im_b;
    imag_sum += im_a * re_b;

    p_src_a_re++;
    p_src_a_im++;
    p_src_b_re++;
    p_src_b_im++;

    /* Decrement loop counter */
    blk_cnt--;
  }

  /* Store real and imaginary result in destination buffer. */
  *p_src_c_re = real_sum;
  *p_src_c_im = imag_sum;
  return ARMRAL_SUCCESS;
#endif
}
