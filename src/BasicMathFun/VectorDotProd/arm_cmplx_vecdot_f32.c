/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "armral.h"
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

armral_status
armral_cmplx_vecdot_f32(uint32_t n, const armral_cmplx_f32_t *restrict p_src_a,
                        const armral_cmplx_f32_t *restrict p_src_b,
                        armral_cmplx_f32_t *p_src_c) {
#ifdef ARMRAL_ARCH_SVE
  int64_t num_lanes = svcntd();
  svbool_t ptrue = svptrue_b32();
  svfloat32_t acc0 = svdup_n_f32(0);
  svfloat32_t acc1 = svdup_n_f32(0);

  int64_t i = 0;
  for (; (i + 2) * num_lanes <= (int64_t)n; i += 2) {
    svbool_t pg = svptrue_b32();
    svfloat32_t vec_a0 = svld1_vnum_f32(pg, (const float32_t *)p_src_a, i);
    svfloat32_t vec_b0 = svld1_vnum_f32(pg, (const float32_t *)p_src_b, i);
    svfloat32_t vec_a1 = svld1_vnum_f32(pg, (const float32_t *)p_src_a, i + 1);
    svfloat32_t vec_b1 = svld1_vnum_f32(pg, (const float32_t *)p_src_b, i + 1);

    acc0 = svcmla_f32_m(pg, acc0, vec_a0, vec_b0, 0);
    acc0 = svcmla_f32_m(pg, acc0, vec_a0, vec_b0, 90);
    acc1 = svcmla_f32_m(pg, acc1, vec_a1, vec_b1, 0);
    acc1 = svcmla_f32_m(pg, acc1, vec_a1, vec_b1, 90);
  }

  for (; i * num_lanes < (int64_t)n; ++i) {
    svbool_t pg = svwhilelt_b32(2 * i * num_lanes, 2 * (int64_t)n);
    svfloat32_t vec_a = svld1_vnum_f32(pg, (const float32_t *)p_src_a, i);
    svfloat32_t vec_b = svld1_vnum_f32(pg, (const float32_t *)p_src_b, i);

    acc0 = svcmla_f32_m(pg, acc0, vec_a, vec_b, 0);
    acc0 = svcmla_f32_m(pg, acc0, vec_a, vec_b, 90);
  }

  p_src_c->re = svaddv_f32(ptrue, svtrn1_f32(acc0, acc1));
  p_src_c->im = svaddv_f32(ptrue, svtrn2_f32(acc0, acc1));

  return ARMRAL_SUCCESS;
#else
  uint32_t blk_cnt; /* Loop counter */
  float32_t real_sum = 0.0F;
  float32_t imag_sum = 0.0F; /* Temporary result variables */
  float32_t re_a;
  float32_t im_a;
  float32_t re_b;
  float32_t im_b;

  float32x4x2_t vec1;
  float32x4x2_t vec2;
  float32x4x2_t vec3;
  float32x4x2_t vec4;

  float32x4_t acc_r = vdupq_n_f32(0.0);
  float32x4_t acc_i = vdupq_n_f32(0.0);

  /* Loop unrolling: Compute 8 outputs at a time */
  blk_cnt = n >> 3U;

  while (blk_cnt > 0U) {
    /* C = (A[0]+jA[1])*(B[0]+jB[1]) + ...  */
    /* Calculate dot product and then store the result in a temporary buffer. */

    vec1 = vld2q_f32((const float32_t *)p_src_a);
    vec2 = vld2q_f32((const float32_t *)p_src_b);

    /* Increment pointers */
    p_src_a += 4;
    p_src_b += 4;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    acc_r = vfmaq_f32(acc_r, vec1.val[0], vec2.val[0]);
    acc_r = vfmsq_f32(acc_r, vec1.val[1], vec2.val[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    acc_i = vfmaq_f32(acc_i, vec1.val[1], vec2.val[0]);
    acc_i = vfmaq_f32(acc_i, vec1.val[0], vec2.val[1]);

    vec3 = vld2q_f32((const float32_t *)p_src_a);
    vec4 = vld2q_f32((const float32_t *)p_src_b);

    /* Increment pointers */
    p_src_a += 4;
    p_src_b += 4;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    acc_r = vfmaq_f32(acc_r, vec3.val[0], vec4.val[0]);
    acc_r = vfmsq_f32(acc_r, vec3.val[1], vec4.val[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    acc_i = vfmaq_f32(acc_i, vec3.val[1], vec4.val[0]);
    acc_i = vfmaq_f32(acc_i, vec3.val[0], vec4.val[1]);

    /* Decrement the loop counter */
    blk_cnt--;
  }

  float32x2_t accum = vpadd_f32(vget_low_f32(acc_r), vget_high_f32(acc_r));
  real_sum += accum[0] + accum[1];

  accum = vpadd_f32(vget_low_f32(acc_i), vget_high_f32(acc_i));
  imag_sum += accum[0] + accum[1];

  /* Tail */
  blk_cnt = n & 0x7;

  while (blk_cnt > 0U) {
    re_a = p_src_a->re;
    im_a = p_src_a->im;
    re_b = p_src_b->re;
    im_b = p_src_b->im;

    real_sum += re_a * re_b;
    imag_sum += re_a * im_b;
    real_sum -= im_a * im_b;
    imag_sum += im_a * re_b;

    p_src_a++;
    p_src_b++;

    /* Decrement loop counter */
    blk_cnt--;
  }

  /* Store real and imaginary result in destination buffer. */
  p_src_c->re = real_sum;
  p_src_c->im = imag_sum;
  return ARMRAL_SUCCESS;
#endif
}
