/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

armral_status armral_cmplx_vecdot_i16_2(uint32_t n,
                                        const int16_t *restrict p_src_a_re,
                                        const int16_t *restrict p_src_a_im,
                                        const int16_t *restrict p_src_b_re,
                                        const int16_t *restrict p_src_b_im,
                                        int16_t *p_src_c_re,
                                        int16_t *p_src_c_im) {
#if ARMRAL_ARCH_SVE >= 2
  uint32_t num_32bit_lanes = svcntw();
  uint32_t full_vectors = n / num_32bit_lanes;
  svbool_t pg = svptrue_b32();

  svint64_t acc_real_top = svdup_s64(0);
  svint64_t acc_real_bot = svdup_s64(0);
  svint64_t acc_imag_top = svdup_s64(0);
  svint64_t acc_imag_bot = svdup_s64(0);

  for (uint32_t i = 0; i < full_vectors; ++i) {
    svint32_t vec_a_real = svld1sh_s32(pg, p_src_a_re);
    svint32_t vec_a_imag = svld1sh_s32(pg, p_src_a_im);
    svint32_t vec_b_real = svld1sh_s32(pg, p_src_b_re);
    svint32_t vec_b_imag = svld1sh_s32(pg, p_src_b_im);

    acc_real_top = svmlalt_s64(acc_real_top, vec_a_real, vec_b_real);
    acc_real_bot = svmlalb_s64(acc_real_bot, vec_a_real, vec_b_real);
    acc_real_top = svmlslt_s64(acc_real_top, vec_a_imag, vec_b_imag);
    acc_real_bot = svmlslb_s64(acc_real_bot, vec_a_imag, vec_b_imag);

    acc_imag_top = svmlalt_s64(acc_imag_top, vec_a_imag, vec_b_real);
    acc_imag_bot = svmlalb_s64(acc_imag_bot, vec_a_imag, vec_b_real);
    acc_imag_top = svmlalt_s64(acc_imag_top, vec_a_real, vec_b_imag);
    acc_imag_bot = svmlalb_s64(acc_imag_bot, vec_a_real, vec_b_imag);

    p_src_a_re += num_32bit_lanes;
    p_src_a_im += num_32bit_lanes;
    p_src_b_re += num_32bit_lanes;
    p_src_b_im += num_32bit_lanes;
  }

  uint32_t tail_size = n % num_32bit_lanes;
  if (tail_size) {
    svbool_t tail_pg = svwhilelt_b32(0U, tail_size);
    svint32_t vec_a_real = svld1sh_s32(tail_pg, p_src_a_re);
    svint32_t vec_a_imag = svld1sh_s32(tail_pg, p_src_a_im);
    svint32_t vec_b_real = svld1sh_s32(tail_pg, p_src_b_re);
    svint32_t vec_b_imag = svld1sh_s32(tail_pg, p_src_b_im);

    acc_real_top = svmlalt_s64(acc_real_top, vec_a_real, vec_b_real);
    acc_real_bot = svmlalb_s64(acc_real_bot, vec_a_real, vec_b_real);
    acc_real_top = svmlslt_s64(acc_real_top, vec_a_imag, vec_b_imag);
    acc_real_bot = svmlslb_s64(acc_real_bot, vec_a_imag, vec_b_imag);

    acc_imag_top = svmlalt_s64(acc_imag_top, vec_a_imag, vec_b_real);
    acc_imag_bot = svmlalb_s64(acc_imag_bot, vec_a_imag, vec_b_real);
    acc_imag_top = svmlalt_s64(acc_imag_top, vec_a_real, vec_b_imag);
    acc_imag_bot = svmlalb_s64(acc_imag_bot, vec_a_real, vec_b_imag);
  }

  svint64_t acc_real = svadd_s64_m(pg, acc_real_top, acc_real_bot);
  svint64_t acc_imag = svadd_s64_m(pg, acc_imag_top, acc_imag_bot);

  *p_src_c_re = (int16_t)(sat((svaddv_s64(pg, acc_real) >> 15)));
  *p_src_c_im = (int16_t)(sat((svaddv_s64(pg, acc_imag) >> 15)));

  return ARMRAL_SUCCESS;

#else
  uint32_t blk_cnt; /* Loop counter */
  int64_t real_sum = 0;
  int64_t imag_sum = 0; /* Temporary result variables */

  int16x4_t vec_a_re;
  int16x4_t vec_a_im;
  int16x4_t vec_b_re;
  int16x4_t vec_b_im;
  int32x4_t vec_a_re_ext;
  int32x4_t vec_a_im_ext;
  int32x4_t vec_b_re_ext;
  int32x4_t vec_b_im_ext;

  int64x2_t acc_r = vdupq_n_s64(0);
  int64x2_t acc_i = vdupq_n_s64(0);

  /* Compute 4 outputs at a time */
  blk_cnt = n >> 2U;

  while (blk_cnt > 0U) {
    /* C = (A[0]+jA[1])*(B[0]+jB[1]) + ...  */
    /* Load samples */
    vec_a_re = vld1_s16(p_src_a_re);
    vec_a_im = vld1_s16(p_src_a_im);

    vec_b_re = vld1_s16(p_src_b_re);
    vec_b_im = vld1_s16(p_src_b_im);

    vec_a_re_ext = vmovl_s16(vec_a_re);
    vec_a_im_ext = vmovl_s16(vec_a_im);
    vec_b_re_ext = vmovl_s16(vec_b_re);
    vec_b_im_ext = vmovl_s16(vec_b_im);

    /* Increment pointers */
    p_src_a_re += 4;
    p_src_a_im += 4;
    p_src_b_re += 4;
    p_src_b_im += 4;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    acc_r = vmlal_high_s32(acc_r, vec_a_re_ext, vec_b_re_ext);
    acc_r = vmlal_low_s32(acc_r, vec_a_re_ext, vec_b_re_ext);
    acc_r = vmlsl_high_s32(acc_r, vec_a_im_ext, vec_b_im_ext);
    acc_r = vmlsl_low_s32(acc_r, vec_a_im_ext, vec_b_im_ext);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    acc_i = vmlal_high_s32(acc_i, vec_a_im_ext, vec_b_re_ext);
    acc_i = vmlal_low_s32(acc_i, vec_a_im_ext, vec_b_re_ext);
    acc_i = vmlal_high_s32(acc_i, vec_a_re_ext, vec_b_im_ext);
    acc_i = vmlal_low_s32(acc_i, vec_a_re_ext, vec_b_im_ext);

    blk_cnt--;
  }

  real_sum += (acc_r[0] + acc_r[1]);
  imag_sum += (acc_i[0] + acc_i[1]);

  /* Tail */
  blk_cnt = n & 0x3;

  while (blk_cnt > 0U) {
    int16_t a0 = *p_src_a_re++;
    int16_t b0 = *p_src_a_im++;
    int16_t c0 = *p_src_b_re++;
    int16_t d0 = *p_src_b_im++;

    real_sum += a0 * c0;
    imag_sum += a0 * d0;
    real_sum -= b0 * d0;
    imag_sum += b0 * c0;

    /* Decrement loop counter */
    blk_cnt--;
  }

  *p_src_c_re = (int16_t)(sat(real_sum >> 15));
  *p_src_c_im = (int16_t)(sat(imag_sum >> 15));
  return ARMRAL_SUCCESS;
#endif
}
