/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

armral_status
armral_cmplx_vecdot_i16_32bit(uint32_t n,
                              const armral_cmplx_int16_t *restrict p_src_a,
                              const armral_cmplx_int16_t *restrict p_src_b,
                              armral_cmplx_int16_t *p_src_c) {
#if ARMRAL_ARCH_SVE >= 2
  svbool_t pg = svptrue_b16();
  uint32_t num_16bit_lanes = svcnth();
  uint32_t full_vectors = n / num_16bit_lanes;

  svint32_t real_acc_top = svdup_n_s32(0);
  svint32_t real_acc_bot = svdup_n_s32(0);
  svint32_t imag_acc_top = svdup_n_s32(0);
  svint32_t imag_acc_bot = svdup_n_s32(0);

  for (uint32_t i = 0; i < full_vectors; ++i) {
    svint16x2_t vec_a = svld2_s16(pg, (const int16_t *)p_src_a);
    svint16x2_t vec_b = svld2_s16(pg, (const int16_t *)p_src_b);
    p_src_a += num_16bit_lanes;
    p_src_b += num_16bit_lanes;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    real_acc_top =
        svqdmlalt_s32(real_acc_top, svget2_s16(vec_a, 0), svget2_s16(vec_b, 0));
    real_acc_bot =
        svqdmlalb_s32(real_acc_bot, svget2_s16(vec_a, 0), svget2_s16(vec_b, 0));
    real_acc_top =
        svqdmlslt_s32(real_acc_top, svget2_s16(vec_a, 1), svget2_s16(vec_b, 1));
    real_acc_bot =
        svqdmlslb_s32(real_acc_bot, svget2_s16(vec_a, 1), svget2_s16(vec_b, 1));

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    imag_acc_top =
        svqdmlalt_s32(imag_acc_top, svget2_s16(vec_a, 1), svget2_s16(vec_b, 0));
    imag_acc_bot =
        svqdmlalb_s32(imag_acc_bot, svget2_s16(vec_a, 1), svget2_s16(vec_b, 0));
    imag_acc_top =
        svqdmlalt_s32(imag_acc_top, svget2_s16(vec_a, 0), svget2_s16(vec_b, 1));
    imag_acc_bot =
        svqdmlalb_s32(imag_acc_bot, svget2_s16(vec_a, 0), svget2_s16(vec_b, 1));
  }

  uint32_t tail_size = n % num_16bit_lanes;
  if (tail_size) {
    svbool_t tail_pg = svwhilelt_b16(0U, tail_size);
    svint16x2_t vec_a = svld2_s16(tail_pg, (const int16_t *)p_src_a);
    svint16x2_t vec_b = svld2_s16(tail_pg, (const int16_t *)p_src_b);

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    real_acc_top =
        svqdmlalt_s32(real_acc_top, svget2_s16(vec_a, 0), svget2_s16(vec_b, 0));
    real_acc_bot =
        svqdmlalb_s32(real_acc_bot, svget2_s16(vec_a, 0), svget2_s16(vec_b, 0));
    real_acc_top =
        svqdmlslt_s32(real_acc_top, svget2_s16(vec_a, 1), svget2_s16(vec_b, 1));
    real_acc_bot =
        svqdmlslb_s32(real_acc_bot, svget2_s16(vec_a, 1), svget2_s16(vec_b, 1));

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    imag_acc_top =
        svqdmlalt_s32(imag_acc_top, svget2_s16(vec_a, 1), svget2_s16(vec_b, 0));
    imag_acc_bot =
        svqdmlalb_s32(imag_acc_bot, svget2_s16(vec_a, 1), svget2_s16(vec_b, 0));
    imag_acc_top =
        svqdmlalt_s32(imag_acc_top, svget2_s16(vec_a, 0), svget2_s16(vec_b, 1));
    imag_acc_bot =
        svqdmlalb_s32(imag_acc_bot, svget2_s16(vec_a, 0), svget2_s16(vec_b, 1));
  }

  svint32_t real_acc = svhadd_s32_m(pg, real_acc_top, real_acc_bot);
  svint32_t imag_acc = svhadd_s32_m(pg, imag_acc_top, imag_acc_bot);

  p_src_c->re = (int16_t)sat((svaddv_s32(pg, real_acc) >> 16));
  p_src_c->im = (int16_t)sat((svaddv_s32(pg, imag_acc) >> 16));

  return ARMRAL_SUCCESS;
#else
  const int16_t *p_ini_a = (const int16_t *)p_src_a;
  const int16_t *p_ini_b = (const int16_t *)p_src_b;

  /* Separate accumulators into high and low ends to avoid stalling on the
  critical path. The sqdmlal instruction/vqdmlal intrinsic has too high a
  latency to allow retiring of an instruction per cycle with the dependency on
  single accumulators.*/
  int32x4_t real_acc_high = vdupq_n_s32(0);
  int32x4_t real_acc_low = vdupq_n_s32(0);
  int32x4_t imag_acc_high = vdupq_n_s32(0);
  int32x4_t imag_acc_low = vdupq_n_s32(0);

  /* Compute 8 outputs at a time */
  uint32_t blk_cnt = n >> 3U;

  while (blk_cnt > 0U) {
    /* C = (A[0]+jA[1])*(B[0]+jB[1]) + ...  */
    /* Load samples */
    int16x8x2_t vec_a = vld2q_s16(p_ini_a);
    int16x8x2_t vec_b = vld2q_s16(p_ini_b);

    /* Increment pointers */
    p_ini_a += 16;
    p_ini_b += 16;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    real_acc_high = vqdmlal_high_s16(real_acc_high, vec_a.val[0], vec_b.val[0]);
    real_acc_low = vqdmlal_low_s16(real_acc_low, vec_a.val[0], vec_b.val[0]);
    real_acc_high = vqdmlsl_high_s16(real_acc_high, vec_a.val[1], vec_b.val[1]);
    real_acc_low = vqdmlsl_low_s16(real_acc_low, vec_a.val[1], vec_b.val[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    imag_acc_high = vqdmlal_high_s16(imag_acc_high, vec_a.val[1], vec_b.val[0]);
    imag_acc_low = vqdmlal_low_s16(imag_acc_low, vec_a.val[1], vec_b.val[0]);
    imag_acc_high = vqdmlal_high_s16(imag_acc_high, vec_a.val[0], vec_b.val[1]);
    imag_acc_low = vqdmlal_low_s16(imag_acc_low, vec_a.val[0], vec_b.val[1]);

    blk_cnt--;
  }

  // add the accumulators and shift to eliminate the doubling
  int32x4_t real_acc = vhaddq_s32(real_acc_high, real_acc_low);
  int32x4_t imag_acc = vhaddq_s32(imag_acc_high, imag_acc_low);

  int32_t real_sum = vqadds_s32(real_acc[0], real_acc[1]);
  real_sum = vqadds_s32(real_sum, real_acc[2]);
  real_sum = vqadds_s32(real_sum, real_acc[3]);

  int32_t imag_sum = vqadds_s32(imag_acc[0], imag_acc[1]);
  imag_sum = vqadds_s32(imag_sum, imag_acc[2]);
  imag_sum = vqadds_s32(imag_sum, imag_acc[3]);

  /* Process any remaining vectors */
  blk_cnt = n & 0x7;

  while (blk_cnt > 0U) {
    int32_t a_real = p_ini_a[0];
    int32_t a_imag = p_ini_a[1];
    p_ini_a += 2;
    int32_t b_real = p_ini_b[0];
    int32_t b_imag = p_ini_b[1];
    p_ini_b += 2;

    real_sum = vqadds_s32(real_sum, (int32_t)a_real * b_real);
    imag_sum = vqadds_s32(imag_sum, (int32_t)a_real * b_imag);
    real_sum = vqsubs_s32(real_sum, (int32_t)a_imag * b_imag);
    imag_sum = vqadds_s32(imag_sum, (int32_t)a_imag * b_real);

    blk_cnt--;
  }

  (*p_src_c).re = vqshrns_n_s32(real_sum, 16);
  (*p_src_c).im = vqshrns_n_s32(imag_sum, 16);
  return ARMRAL_SUCCESS;
#endif
}
