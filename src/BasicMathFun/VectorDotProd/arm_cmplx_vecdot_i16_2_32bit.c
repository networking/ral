/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"
#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

armral_status
armral_cmplx_vecdot_i16_2_32bit(uint32_t n, const int16_t *restrict p_src_a_re,
                                const int16_t *restrict p_src_a_im,
                                const int16_t *restrict p_src_b_re,
                                const int16_t *restrict p_src_b_im,
                                int16_t *p_src_c_re, int16_t *p_src_c_im) {
#if ARMRAL_ARCH_SVE >= 2
  uint32_t num_lanes = svcnth();
  uint32_t full_vectors = n / num_lanes;
  svbool_t pg = svptrue_b16();

  svint32_t real_acc_top = svdup_s32(0);
  svint32_t real_acc_bot = svdup_s32(0);
  svint32_t imag_acc_top = svdup_s32(0);
  svint32_t imag_acc_bot = svdup_s32(0);

  for (uint32_t i = 0; i < full_vectors; ++i) {
    svint16_t vec_a_real = svld1_s16(pg, p_src_a_re);
    svint16_t vec_a_imag = svld1_s16(pg, p_src_a_im);
    svint16_t vec_b_real = svld1_s16(pg, p_src_b_re);
    svint16_t vec_b_imag = svld1_s16(pg, p_src_b_im);

    p_src_a_re += num_lanes;
    p_src_a_im += num_lanes;
    p_src_b_re += num_lanes;
    p_src_b_im += num_lanes;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    real_acc_top = svqdmlalt_s32(real_acc_top, vec_a_real, vec_b_real);
    real_acc_bot = svqdmlalb_s32(real_acc_bot, vec_a_real, vec_b_real);
    real_acc_top = svqdmlslt_s32(real_acc_top, vec_a_imag, vec_b_imag);
    real_acc_bot = svqdmlslb_s32(real_acc_bot, vec_a_imag, vec_b_imag);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    imag_acc_top = svqdmlalt_s32(imag_acc_top, vec_a_imag, vec_b_real);
    imag_acc_bot = svqdmlalb_s32(imag_acc_bot, vec_a_imag, vec_b_real);
    imag_acc_top = svqdmlalt_s32(imag_acc_top, vec_a_real, vec_b_imag);
    imag_acc_bot = svqdmlalb_s32(imag_acc_bot, vec_a_real, vec_b_imag);
  }

  uint32_t tail_size = n % num_lanes;
  if (tail_size) {
    svbool_t tail_pg = svwhilelt_b16(0U, tail_size);
    svint16_t vec_a_real = svld1_s16(tail_pg, p_src_a_re);
    svint16_t vec_a_imag = svld1_s16(tail_pg, p_src_a_im);
    svint16_t vec_b_real = svld1_s16(tail_pg, p_src_b_re);
    svint16_t vec_b_imag = svld1_s16(tail_pg, p_src_b_im);

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    real_acc_top = svqdmlalt_s32(real_acc_top, vec_a_real, vec_b_real);
    real_acc_bot = svqdmlalb_s32(real_acc_bot, vec_a_real, vec_b_real);
    real_acc_top = svqdmlslt_s32(real_acc_top, vec_a_imag, vec_b_imag);
    real_acc_bot = svqdmlslb_s32(real_acc_bot, vec_a_imag, vec_b_imag);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    imag_acc_top = svqdmlalt_s32(imag_acc_top, vec_a_imag, vec_b_real);
    imag_acc_bot = svqdmlalb_s32(imag_acc_bot, vec_a_imag, vec_b_real);
    imag_acc_top = svqdmlalt_s32(imag_acc_top, vec_a_real, vec_b_imag);
    imag_acc_bot = svqdmlalb_s32(imag_acc_bot, vec_a_real, vec_b_imag);
  }

  svint32_t real_acc = svhadd_s32_x(pg, real_acc_top, real_acc_bot);
  svint32_t imag_acc = svhadd_s32_x(pg, imag_acc_top, imag_acc_bot);

  *p_src_c_re = (svaddv_s32(pg, real_acc) >> 16);
  *p_src_c_im = (svaddv_s32(pg, imag_acc) >> 16);

  return ARMRAL_SUCCESS;
#else
  /* Separate accumulators into high and low ends to avoid stalling on the
  critical path. The sqdmlal instruction/vqdmlal intrinsic has too high a
  latency to allow retiring of an instruction per cycle with the dependency on
  single accumulators.*/
  int32x4_t real_acc_high = vdupq_n_s32(0);
  int32x4_t real_acc_low = vdupq_n_s32(0);
  int32x4_t imag_acc_high = vdupq_n_s32(0);
  int32x4_t imag_acc_low = vdupq_n_s32(0);

  /* Compute 8 outputs at a time */
  uint32_t blk_cnt = n >> 3U;

  while (blk_cnt > 0U) {
    /* C = (A[0]+jA[1])*(B[0]+jB[1]) + ...  */
    /* Load samples */
    int16x8_t vec_a_real = vld1q_s16(p_src_a_re);
    int16x8_t vec_a_imag = vld1q_s16(p_src_a_im);

    int16x8_t vec_b_real = vld1q_s16(p_src_b_re);
    int16x8_t vec_b_imag = vld1q_s16(p_src_b_im);

    /* Increment pointers */
    p_src_a_re += 8;
    p_src_a_im += 8;
    p_src_b_re += 8;
    p_src_b_im += 8;

    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    real_acc_high = vqdmlal_high_s16(real_acc_high, vec_a_real, vec_b_real);
    real_acc_low = vqdmlal_low_s16(real_acc_low, vec_a_real, vec_b_real);
    real_acc_high = vqdmlsl_high_s16(real_acc_high, vec_a_imag, vec_b_imag);
    real_acc_low = vqdmlsl_low_s16(real_acc_low, vec_a_imag, vec_b_imag);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    imag_acc_high = vqdmlal_high_s16(imag_acc_high, vec_a_imag, vec_b_real);
    imag_acc_low = vqdmlal_low_s16(imag_acc_low, vec_a_imag, vec_b_real);
    imag_acc_high = vqdmlal_high_s16(imag_acc_high, vec_a_real, vec_b_imag);
    imag_acc_low = vqdmlal_low_s16(imag_acc_low, vec_a_real, vec_b_imag);

    blk_cnt--;
  }

  // add the accumulators and shift right to eliminate the doubling
  int32x4_t real_acc = vhaddq_s32(real_acc_high, real_acc_low);
  int32x4_t imag_acc = vhaddq_s32(imag_acc_high, imag_acc_low);

  int32_t real_sum = vqadds_s32(real_acc[0], real_acc[1]);
  real_sum = vqadds_s32(real_sum, real_acc[2]);
  real_sum = vqadds_s32(real_sum, real_acc[3]);

  int32_t imag_sum = vqadds_s32(imag_acc[0], imag_acc[1]);
  imag_sum = vqadds_s32(imag_sum, imag_acc[2]);
  imag_sum = vqadds_s32(imag_sum, imag_acc[3]);

  /* Process any remaining vectors */
  blk_cnt = n & 0x7;

  while (blk_cnt > 0U) {
    int32_t a_real = *p_src_a_re++;
    int32_t a_imag = *p_src_a_im++;
    int32_t b_real = *p_src_b_re++;
    int32_t b_imag = *p_src_b_im++;

    real_sum = vqadds_s32(real_sum, (int32_t)a_real * b_real);
    imag_sum = vqadds_s32(imag_sum, (int32_t)a_real * b_imag);
    real_sum = vqsubs_s32(real_sum, (int32_t)a_imag * b_imag);
    imag_sum = vqadds_s32(imag_sum, (int32_t)a_imag * b_real);

    blk_cnt--;
  }

  *p_src_c_re = vqshrns_n_s32(real_sum, 16);
  *p_src_c_im = vqshrns_n_s32(imag_sum, 16);
  return ARMRAL_SUCCESS;
#endif
}
