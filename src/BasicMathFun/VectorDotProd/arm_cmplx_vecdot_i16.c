/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

armral_status
armral_cmplx_vecdot_i16(uint32_t n,
                        const armral_cmplx_int16_t *restrict p_src_a,
                        const armral_cmplx_int16_t *restrict p_src_b,
                        armral_cmplx_int16_t *p_src_c) {
#if ARMRAL_ARCH_SVE >= 2
  uint32_t num_32bit_lanes = svcntw();
  uint32_t full_vectors = n / num_32bit_lanes;
  svbool_t pg = svptrue_b16();
  svint64_t acc_real = svdup_n_s64(0);
  svint64_t acc_imag = svdup_n_s64(0);

  for (uint32_t i = 0; i < full_vectors; ++i) {
    svint16_t vec_a = svld1_s16(pg, (const int16_t *)p_src_a);
    svint16_t vec_b = svld1_s16(pg, (const int16_t *)p_src_b);
    p_src_a += num_32bit_lanes;
    p_src_b += num_32bit_lanes;

    acc_real = svcdot_s64(acc_real, vec_a, vec_b, 0);
    acc_imag = svcdot_s64(acc_imag, vec_a, vec_b, 90);
  }

  uint32_t tail_size = n % num_32bit_lanes;
  if (tail_size) {
    svbool_t tail_pg = svwhilelt_b16(0U, 2 * tail_size);
    svint16_t vec_a = svld1_s16(tail_pg, (const int16_t *)p_src_a);
    svint16_t vec_b = svld1_s16(tail_pg, (const int16_t *)p_src_b);

    acc_real = svcdot_s64(acc_real, vec_a, vec_b, 0);
    acc_imag = svcdot_s64(acc_imag, vec_a, vec_b, 90);
  }

  p_src_c->re = (int16_t)(sat((svaddv_s64(pg, acc_real) >> 15)));
  p_src_c->im = (int16_t)(sat((svaddv_s64(pg, acc_imag) >> 15)));

  return ARMRAL_SUCCESS;
#else
  uint32_t blk_cnt; /* Loop counter */
  int64_t real_sum = 0;
  int64_t imag_sum = 0; /* Temporary result variables */
  const int16_t *p_ini_a = (const int16_t *)p_src_a;
  const int16_t *p_ini_b = (const int16_t *)p_src_b;

  int16x4x2_t vec1;
  int16x4x2_t vec2;
  int32x4x2_t vec1_ext;
  int32x4x2_t vec2_ext;

  int64x2_t acc_r = vdupq_n_s64(0);
  int64x2_t acc_i = vdupq_n_s64(0);

  /* Compute 8 outputs at a time */
  blk_cnt = n >> 2U;

  while (blk_cnt > 0U) {
    /* C = (A[0]+jA[1])*(B[0]+jB[1]) + ...  */
    /* Load samples */
    vec1 = vld2_s16(p_ini_a);
    vec2 = vld2_s16(p_ini_b);

    vec1_ext.val[0] = vmovl_s16(vec1.val[0]);
    vec2_ext.val[0] = vmovl_s16(vec2.val[0]);
    vec1_ext.val[1] = vmovl_s16(vec1.val[1]);
    vec2_ext.val[1] = vmovl_s16(vec2.val[1]);
    /* Increment pointers */
    p_ini_a += 8;
    p_ini_b += 8;
    /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
    acc_r = vmlal_high_s32(acc_r, vec1_ext.val[0], vec2_ext.val[0]);
    acc_r = vmlal_low_s32(acc_r, vec1_ext.val[0], vec2_ext.val[0]);
    acc_r = vmlsl_high_s32(acc_r, vec1_ext.val[1], vec2_ext.val[1]);
    acc_r = vmlsl_low_s32(acc_r, vec1_ext.val[1], vec2_ext.val[1]);

    /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
    acc_i = vmlal_high_s32(acc_i, vec1_ext.val[1], vec2_ext.val[0]);
    acc_i = vmlal_low_s32(acc_i, vec1_ext.val[1], vec2_ext.val[0]);
    acc_i = vmlal_high_s32(acc_i, vec1_ext.val[0], vec2_ext.val[1]);
    acc_i = vmlal_low_s32(acc_i, vec1_ext.val[0], vec2_ext.val[1]);

    blk_cnt--;
  }

  real_sum += (acc_r[0] + acc_r[1]);
  imag_sum += (acc_i[0] + acc_i[1]);

  /* Tail */
  blk_cnt = n & 0x3;

  while (blk_cnt > 0U) {
    int16_t a0 = p_ini_a[0];
    int16_t b0 = p_ini_a[1];
    p_ini_a += 2;
    int16_t c0 = p_ini_b[0];
    int16_t d0 = p_ini_b[1];
    p_ini_b += 2;

    real_sum += a0 * c0;
    imag_sum += a0 * d0;
    real_sum -= b0 * d0;
    imag_sum += b0 * c0;

    /* Decrement loop counter */
    blk_cnt--;
  }

  (*p_src_c).re = (int16_t)(sat(real_sum >> 15));
  (*p_src_c).im = (int16_t)(sat(imag_sum >> 15));
  return ARMRAL_SUCCESS;
#endif
}
