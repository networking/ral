/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"

/**
 * Implements the equalization step for cases with one subcarrier per 2x2 G
 * matrix.
 *
 * @param[in]  num_sub_carrier     number of sub-carriers to equalize
 * @param[in]  p_y                 points to the input received signal
 * @param[in]  p_ystride           stride between two Rx antennae
 * @param[in]  p_y_num_fract_bits  number of fractional bits in `y`
 * @param[in]  p_g_real            points to the real part of coefficient matrix
 *                                 G
 * @param[in]  p_g_imag            points to the imaginary part of coefficient
 *                                 matrix G
 * @param[in]  p_gstride           stride between elements of G
 * @param[out] p_x                 points to the output received signal
 * @param[in]  p_xstride           stride between two layers
 * @param[in]  num_fract_bits_x    number of fractional bits in `x`
 * @return ARMRAL_SUCCESS always, to enable tail calls.
 */
armral_status armral_solve_2x2_1sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    armral_fixed_point_index num_fract_bits_x);

/**
 * Implements the equalization step for cases with one subcarrier per 2x4 G
 * matrix.
 *
 * @param[in]  num_sub_carrier     number of sub-carriers to equalize
 * @param[in]  p_y                 points to the input received signal
 * @param[in]  p_ystride           stride between two Rx antennae
 * @param[in]  p_y_num_fract_bits  number of fractional bits in `y`
 * @param[in]  p_g_real            points to the real part of coefficient matrix
 *                                 G
 * @param[in]  p_g_imag            points to the imaginary part of coefficient
 *                                 matrix G
 * @param[in]  p_gstride           stride between elements of G
 * @param[out] p_x                 points to the output received signal
 * @param[in]  p_xstride           stride between two layers
 * @param[in]  num_fract_bits_x    number of fractional bits in `x`
 * @return ARMRAL_SUCCESS always, to enable tail calls.
 */
armral_status armral_solve_2x4_1sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    armral_fixed_point_index num_fract_bits_x);

/**
 * Implements the equalization step for cases with one subcarrier per 4x4 G
 * matrix.
 *
 * @param[in]  num_sub_carrier     number of sub-carriers to equalize
 * @param[in]  p_y                 points to the input received signal
 * @param[in]  p_ystride           stride between two Rx antennae
 * @param[in]  p_y_num_fract_bits  number of fractional bits in `y`
 * @param[in]  p_g_real            points to the real part of coefficient matrix
 *                                 G
 * @param[in]  p_g_imag            points to the imaginary part of coefficient
 *                                 matrix G
 * @param[in]  p_gstride           stride between elements of G
 * @param[out] p_x                 points to the output received signal
 * @param[in]  p_xstride           stride between two layers
 * @param[in]  num_fract_bits_x    number of fractional bits in `x`
 * @return ARMRAL_SUCCESS always, to enable tail calls.
 */
armral_status armral_solve_4x4_1sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride, const armral_fixed_point_index *p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    armral_fixed_point_index num_fract_bits_x);

/**
 * Implements the equalization step for cases with one subcarrier per 1x4 G
 * matrix.
 *
 * @param[in]  num_sub_carrier     number of sub-carriers to equalize
 * @param[in]  p_y                 points to the input received signal
 * @param[in]  p_ystride           stride between two Rx antennae
 * @param[in]  p_y_num_fract_bits  number of fractional bits in `y`
 * @param[in]  p_g_real            points to the real part of coefficient matrix
 *                                 G
 * @param[in]  p_g_imag            points to the imaginary part of coefficient
 *                                 matrix G
 * @param[in]  p_gstride           stride between elements of G
 * @param[out] p_x                 points to the output received signal
 * @param[in]  p_xstride           stride between two layers
 * @param[in]  num_fract_bits_x    number of fractional bits in `x`
 * @return ARMRAL_SUCCESS always, to enable tail calls.
 */
armral_status armral_solve_1x4_1sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    armral_fixed_point_index num_fract_bits_x);

/**
 * Implements the equalization step for cases with one subcarrier per 1x2 G
 * matrix.
 *
 * @param[in]  num_sub_carrier     number of sub-carriers to equalize
 * @param[in]  p_y                 points to the input received signal
 * @param[in]  p_ystride           stride between two Rx antennae
 * @param[in]  p_y_num_fract_bits  number of fractional bits in `y`
 * @param[in]  p_g_real            points to the real part of coefficient matrix
 *                                 G
 * @param[in]  p_g_imag            points to the imaginary part of coefficient
 *                                 matrix G
 * @param[in]  p_gstride           stride between elements of G
 * @param[out] p_x                 points to the output received signal
 * @param[in]  p_xstride           stride between two layers
 * @param[in]  num_fract_bits_x    number of fractional bits in `x`
 * @return ARMRAL_SUCCESS always, to enable tail calls.
 */
armral_status armral_solve_1x2_1sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    armral_fixed_point_index num_fract_bits_x);
