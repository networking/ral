/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"

#include <assert.h>

static int16x4_t vld1s_s16(const armral_cmplx_int16_t *p) {
  // there is no intrinsic for only loading 32-bits into an ACLE vector.
  int16x4_t ret;
  asm("ldr %s0, %1" : "=w"(ret) : "m"(*p));
  return ret;
}

static int16x4_t vqshrnd_16_s32(int32x2_t in) {
  // there is no intrinsic for only narrowing 64-bits of an ACLE vector.
  int16x4_t ret;
  asm("sqshrn %0.4h, %1.4s, #16" : "=w"(ret) : "w"(in));
  return ret;
}

armral_status armral_cmplx_mat_vec_mult_i16_32bit(
    const uint16_t m, const uint16_t n,
    const armral_cmplx_int16_t *restrict p_src_a,
    const armral_cmplx_int16_t *restrict p_src_x, armral_cmplx_int16_t *p_dst) {

  const armral_cmplx_int16_t *a_ptr = p_src_a;
  const armral_cmplx_int16_t *x_ptr = p_src_x;
  armral_cmplx_int16_t *out_ptr = p_dst;

  for (uint16_t a_row_cnt = m; a_row_cnt > 0;
       --a_row_cnt, a_ptr += n, ++out_ptr) {

    int32x4_t accum1;
    int32x4_t accum2;
    {
      int16x4_t a_tmp = vld1s_s16(&a_ptr[0]);
      int16x4_t x_tmp = vld1s_s16(&x_ptr[0]);
      accum1 = vqdmull_lane_s16(a_tmp, x_tmp, 0);
      accum2 = vqdmull_lane_s16(a_tmp, x_tmp, 1);
    }
    for (int i = 1; i < n; ++i) {
      int16x4_t a_tmp = vld1s_s16(&a_ptr[i]);
      int16x4_t x_tmp = vld1s_s16(&x_ptr[i]);
      accum1 = vqdmlal_lane_s16(accum1, a_tmp, x_tmp, 0);
      accum2 = vqdmlal_lane_s16(accum2, a_tmp, x_tmp, 1);
    }

    int32x2_t final_sum32 = vdup_n_s32(0);
    final_sum32[0] = vqsubs_s32(accum1[0], accum2[1]);
    final_sum32[1] = vqadds_s32(accum1[1], accum2[0]);
    int16x4_t final_sum16 = vqshrnd_16_s32(final_sum32);
    *(uint32_t *)out_ptr = vreinterpret_u32_s16(final_sum16)[0];
  }
  return ARMRAL_SUCCESS;
}

#if !(ARMRAL_ARCH_SVE >= 2)
static armral_status
cmplx_mat_vec_mult_batch_one_vec(uint16_t num_mats, uint16_t m, uint16_t n,
                                 const armral_cmplx_int16_t *restrict p_src_a,
                                 const armral_cmplx_int16_t *restrict p_src_x,
                                 armral_cmplx_int16_t *p_dst) {

  // In p_src_a the corresponding elements of matrices are stored contiguously
  // e.g. if the batch comprises 2 matrices A0, A1, p_src_a stores:
  //      A0[0,0] A1[0,0] A0[0,1] A1[0,1] A0[0,2] A1[0,2] ...
  // because the matrices are stored in row-major format.

  assert(num_mats % 4 == 0);

  for (uint16_t mat_idx = 0; mat_idx < num_mats; mat_idx += 4) {
    // Do one matrix-vector multiplication at a time

    // Point to first element of first row of mat_idx^th A
    const armral_cmplx_int16_t *a_ptr = &p_src_a[mat_idx];

    // Point to first element of vec^th x and y
    const armral_cmplx_int16_t *x_ptr = &p_src_x[mat_idx];
    armral_cmplx_int16_t *out_ptr = &p_dst[mat_idx];

    uint32x4_t mask = {~0U, 0U, ~0U, 0U};

    // Loop over A one row at a time
    for (uint16_t row_cnt = 0; row_cnt < m;
         ++row_cnt, a_ptr += num_mats * n, out_ptr += num_mats) {

      // Initialize the accumulator
      int32x4_t sum_real_lo;
      int32x4_t sum_real_hi;
      int32x4_t sum_imag_lo;
      int32x4_t sum_imag_hi;

      // Loop over the row of A one column at a time
      {
        int16x8_t a_tmp = vld1q_s16((const int16_t *)a_ptr);
        int16x8_t x_tmp = vld1q_s16((const int16_t *)x_ptr);
        int16x8_t x_rev = vrev32q_s16(x_tmp);
        sum_real_lo = vqdmull_low_s16(a_tmp, x_tmp);
        sum_real_hi = vqdmull_high_s16(a_tmp, x_tmp);
        sum_imag_lo = vqdmull_low_s16(a_tmp, x_rev);
        sum_imag_hi = vqdmull_high_s16(a_tmp, x_rev);
      }
      for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
        int16x8_t a_tmp =
            vld1q_s16((const int16_t *)&a_ptr[col_cnt * num_mats]);
        int16x8_t x_tmp =
            vld1q_s16((const int16_t *)&x_ptr[col_cnt * num_mats]);
        int16x8_t x_rev = vrev32q_s16(x_tmp);
        sum_real_lo = vqdmlal_low_s16(sum_real_lo, a_tmp, x_tmp);
        sum_real_hi = vqdmlal_high_s16(sum_real_hi, a_tmp, x_tmp);
        sum_imag_lo = vqdmlal_low_s16(sum_imag_lo, a_tmp, x_rev);
        sum_imag_hi = vqdmlal_high_s16(sum_imag_hi, a_tmp, x_rev);
      }

      int32x4_t neg_real_lo = vqnegq_s32(sum_real_lo);
      int32x4_t neg_real_hi = vqnegq_s32(sum_real_hi);
      sum_real_lo = vbslq_s32(mask, sum_real_lo, neg_real_lo);
      sum_real_hi = vbslq_s32(mask, sum_real_hi, neg_real_hi);
      // sum_real = [re1, re1, re2, re2]
      // sum_imag = [im1, im1, im2, im2]
      // vtrn1_s32(sum_real, sum_imag) = [re1, im1, re2, im2]
      // vtrn2_s32(sum_real, sum_imag) = [re1, im1, re2, im2]
      int32x4_t final_sum32_lo_1 = vtrn1q_s32(sum_real_lo, sum_imag_lo);
      int32x4_t final_sum32_lo_2 = vtrn2q_s32(sum_real_lo, sum_imag_lo);
      int32x4_t final_sum32_hi_1 = vtrn1q_s32(sum_real_hi, sum_imag_hi);
      int32x4_t final_sum32_hi_2 = vtrn2q_s32(sum_real_hi, sum_imag_hi);

      int32x4_t final_sum32_lo = vqaddq_s32(final_sum32_lo_1, final_sum32_lo_2);
      int32x4_t final_sum32_hi = vqaddq_s32(final_sum32_hi_1, final_sum32_hi_2);

      int16x8_t final_sum16 = vqshrn_high_n_s32(
          vqshrn_n_s32(final_sum32_lo, 16), final_sum32_hi, 16);
      vst1q_s16((int16_t *)out_ptr, final_sum16);
    }
  }

  return ARMRAL_SUCCESS;
}
#else
static armral_status
cmplx_mat_vec_mult_batch_one_vec(uint16_t num_mats, uint16_t m, uint16_t n,
                                 const armral_cmplx_int16_t *restrict p_src_a,
                                 const armral_cmplx_int16_t *restrict p_src_x,
                                 armral_cmplx_int16_t *p_dst) {

  // In p_src_a the corresponding elements of matrices are stored contiguously
  // e.g. if the batch comprises 2 matrices A0, A1, p_src_a stores:
  //      A0[0,0] A1[0,0] A0[0,1] A1[0,1] A0[0,2] A1[0,2] ...
  // because the matrices are stored in row-major format.

  svbool_t ptrue = svptrue_b16();

  for (uint16_t mat_idx = 0; mat_idx < num_mats; mat_idx += svcntw()) {
    svbool_t pg = svwhilelt_b16(mat_idx * 2, num_mats * 2);
    // Point to first element of first row of mat_idx^th A
    const armral_cmplx_int16_t *a_ptr = &p_src_a[mat_idx];

    // Point to first element of vec^th x and y
    const armral_cmplx_int16_t *x_ptr = &p_src_x[mat_idx];
    armral_cmplx_int16_t *out_ptr = &p_dst[mat_idx];

    // Loop over A one row at a time
    for (uint16_t row_cnt = 0; row_cnt < m;
         ++row_cnt, a_ptr += num_mats * n, out_ptr += num_mats) {

      // Initialize the accumulator
      svint32_t sum_real;
      svint32_t sum_imag;

      // Loop over the row of A one column at a time
      {
        svint16_t a_tmp = svld1_s16(pg, (const int16_t *)a_ptr);
        svint16_t x_tmp = svld1_s16(pg, (const int16_t *)x_ptr);
        svint16_t x_rev = svreinterpret_s16_s32(
            svrevh_s32_x(ptrue, svreinterpret_s32_s16(x_tmp)));
        sum_real = svqdmullb_s32(a_tmp, x_tmp);           // real * real
        sum_imag = svqdmullb_s32(a_tmp, x_rev);           // real * imag
        sum_real = svqdmlslt_s32(sum_real, a_tmp, x_tmp); // imag * imag
        sum_imag = svqdmlalt_s32(sum_imag, a_tmp, x_rev); // imag * real
      }
      for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
        svint16_t a_tmp =
            svld1_s16(pg, (const int16_t *)&a_ptr[col_cnt * num_mats]);
        svint16_t x_tmp =
            svld1_s16(pg, (const int16_t *)&x_ptr[col_cnt * num_mats]);
        svint16_t x_rev = svreinterpret_s16_s32(
            svrevh_s32_x(ptrue, svreinterpret_s32_s16(x_tmp)));
        sum_real = svqdmlalb_s32(sum_real, a_tmp, x_tmp); // real * real
        sum_imag = svqdmlalb_s32(sum_imag, a_tmp, x_rev); // real * imag
        sum_real = svqdmlslt_s32(sum_real, a_tmp, x_tmp); // imag * imag
        sum_imag = svqdmlalt_s32(sum_imag, a_tmp, x_rev); // imag * real
      }

      svint16_t final_sum =
          svqshrnt_n_s32(svqshrnb_n_s32(sum_real, 16), sum_imag, 16);
      svst1_s16(pg, (int16_t *)out_ptr, final_sum);
    }
  }

  return ARMRAL_SUCCESS;
}
#endif

#if !(ARMRAL_ARCH_SVE >= 2)
static armral_status cmplx_mat_vec_mult_batch_unroll_vec(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t *restrict p_src_a,
    const armral_cmplx_int16_t *restrict p_src_x, armral_cmplx_int16_t *p_dst) {

  assert(num_vecs_per_mat % 4 == 0);

  // In p_src_a the corresponding elements of matrices are stored contiguously
  // e.g. if the batch comprises 2 matrices A0, A1, p_src_a stores:
  //      A0[0,0] A1[0,0] A0[0,1] A1[0,1] A0[0,2] A1[0,2] ...
  // because the matrices are stored in row-major format.

  const int vec_stride = num_mats * num_vecs_per_mat;
  uint32x4_t mask = {~0U, 0U, ~0U, 0};

  for (uint16_t mat_idx = 0; mat_idx < num_mats; ++mat_idx) {
    for (uint16_t vec = 0; vec < num_vecs_per_mat; vec += 4) {
      // Do two matrix-vector multiplications at a time
      // Work out the starting index of the current vector in p_src_x
      // and p_dst
      int vec_idx = (mat_idx * num_vecs_per_mat) + vec;

      // Point to first element of first row of mat_idx^th A
      const armral_cmplx_int16_t *a_ptr = &p_src_a[mat_idx];

      // Point to first element of vec^th x and y
      const armral_cmplx_int16_t *x_ptr = &p_src_x[vec_idx];
      armral_cmplx_int16_t *out_ptr = &p_dst[vec_idx];

      // Loop over A one row at a time
      for (uint16_t row_cnt = 0; row_cnt < m;
           ++row_cnt, a_ptr += num_mats * n, out_ptr += vec_stride) {

        // Initialize the accumulator
        int32x4_t sum_real_lo;
        int32x4_t sum_real_hi;
        int32x4_t sum_imag_lo;
        int32x4_t sum_imag_hi;

        // Loop over the row of A one column at a time
        {
          int16x4_t a_tmp = vld1s_s16(a_ptr);
          int16x8_t x_tmp = vld1q_s16((const int16_t *)x_ptr);
          sum_real_lo = vqdmull_low_lane_s16(x_tmp, a_tmp, 0);
          sum_real_hi = vqdmull_high_lane_s16(x_tmp, a_tmp, 0);
          sum_imag_lo = vqdmull_low_lane_s16(x_tmp, a_tmp, 1);
          sum_imag_hi = vqdmull_high_lane_s16(x_tmp, a_tmp, 1);
        }
        for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
          int16x4_t a_tmp = vld1s_s16(&a_ptr[col_cnt * num_mats]);
          int16x8_t x_tmp =
              vld1q_s16((const int16_t *)&x_ptr[col_cnt * vec_stride]);
          sum_real_lo = vqdmlal_low_lane_s16(sum_real_lo, x_tmp, a_tmp, 0);
          sum_real_hi = vqdmlal_high_lane_s16(sum_real_hi, x_tmp, a_tmp, 0);
          sum_imag_lo = vqdmlal_low_lane_s16(sum_imag_lo, x_tmp, a_tmp, 1);
          sum_imag_hi = vqdmlal_high_lane_s16(sum_imag_hi, x_tmp, a_tmp, 1);
        }

        // For the imaginary part of the multiplication, we need to reverse the
        // order of 32-bit elements within each complex number, and then
        // negate the real portion
        sum_imag_lo = vrev64q_s32(sum_imag_lo);
        sum_imag_hi = vrev64q_s32(sum_imag_hi);
        int32x4_t neg_imag_lo = vqnegq_s32(sum_imag_lo);
        int32x4_t neg_imag_hi = vqnegq_s32(sum_imag_hi);
        sum_imag_lo = vbslq_s32(mask, neg_imag_lo, sum_imag_lo);
        sum_imag_hi = vbslq_s32(mask, neg_imag_hi, sum_imag_hi);

        int32x4_t final_sum32_lo = vqaddq_s32(sum_imag_lo, sum_real_lo);
        int32x4_t final_sum32_hi = vqaddq_s32(sum_imag_hi, sum_real_hi);
        int16x8_t final_sum16 = vqshrn_high_n_s32(
            vqshrn_n_s32(final_sum32_lo, 16), final_sum32_hi, 16);
        vst1q_s16((int16_t *)out_ptr, final_sum16);
      }
    }
  }

  return ARMRAL_SUCCESS;
}
#else
static armral_status cmplx_mat_vec_mult_batch_unroll_vec(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t *restrict p_src_a,
    const armral_cmplx_int16_t *restrict p_src_x, armral_cmplx_int16_t *p_dst) {
  // In p_src_a the corresponding elements of matrices are stored contiguously
  // e.g. if the batch comprises 2 matrices A0, A1, p_src_a stores:
  //      A0[0,0] A1[0,0] A0[0,1] A1[0,1] A0[0,2] A1[0,2] ...
  // because the matrices are stored in row-major format.

  const int vec_stride = num_mats * num_vecs_per_mat;

  for (uint16_t mat_idx = 0; mat_idx < num_mats; ++mat_idx) {
    for (uint16_t vec = 0; vec < num_vecs_per_mat; vec += svcntw()) {
      svbool_t pg = svwhilelt_b16(vec * 2, num_vecs_per_mat * 2);
      // Work out the starting index of the current vector in p_src_x
      // and p_dst
      int vec_idx = (mat_idx * num_vecs_per_mat) + vec;

      // Point to first element of first row of mat_idx^th A
      const armral_cmplx_int16_t *a_ptr = &p_src_a[mat_idx];

      // Point to first element of vec^th x and y
      const armral_cmplx_int16_t *x_ptr = &p_src_x[vec_idx];
      armral_cmplx_int16_t *out_ptr = &p_dst[vec_idx];

      // Loop over A one row at a time
      for (uint16_t row_cnt = 0; row_cnt < m;
           ++row_cnt, a_ptr += num_mats * n, out_ptr += vec_stride) {

        // Initialize the accumulator
        svint32_t sum_real;
        svint32_t sum_imag;

        // Loop over the row of A one column at a time
        {
          svint16_t a_tmp =
              svreinterpret_s16_s32(svdup_n_s32(*(const int32_t *)a_ptr));
          svint16_t x_tmp = svld1_s16(pg, (const int16_t *)x_ptr);
          sum_real = svqdmullb_lane_s32(x_tmp, a_tmp, 0); // real * real
          sum_imag = svqdmullt_lane_s32(x_tmp, a_tmp, 0); // imag * real
          sum_real =
              svqdmlslt_lane_s32(sum_real, x_tmp, a_tmp, 1); // imag * imag
          sum_imag =
              svqdmlalb_lane_s32(sum_imag, x_tmp, a_tmp, 1); // real * imag
        }
        for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
          svint16_t a_tmp = svreinterpret_s16_s32(
              svdup_n_s32(*(const int32_t *)&a_ptr[col_cnt * num_mats]));
          svint16_t x_tmp =
              svld1_s16(pg, (const int16_t *)&x_ptr[col_cnt * vec_stride]);
          sum_real =
              svqdmlalb_lane_s32(sum_real, x_tmp, a_tmp, 0); // real * real
          sum_imag =
              svqdmlalt_lane_s32(sum_imag, x_tmp, a_tmp, 0); // imag * real
          sum_real =
              svqdmlslt_lane_s32(sum_real, x_tmp, a_tmp, 1); // imag * imag
          sum_imag =
              svqdmlalb_lane_s32(sum_imag, x_tmp, a_tmp, 1); // real * imag
        }

        svint16_t final_sum =
            svqshrnt_n_s32(svqshrnb_n_s32(sum_real, 16), sum_imag, 16);
        svst1_s16(pg, (int16_t *)out_ptr, final_sum);
      }
    }
  }

  return ARMRAL_SUCCESS;
}
#endif

armral_status armral_cmplx_mat_vec_mult_batch_i16_32bit(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t *restrict p_src_a,
    const armral_cmplx_int16_t *restrict p_src_x, armral_cmplx_int16_t *p_dst) {
  if ((num_mats * num_vecs_per_mat) % 12 != 0) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  if (num_vecs_per_mat == 1) {
    return cmplx_mat_vec_mult_batch_one_vec(num_mats, m, n, p_src_a, p_src_x,
                                            p_dst);
  }
  if (num_vecs_per_mat % 4 != 0) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  return cmplx_mat_vec_mult_batch_unroll_vec(num_mats, num_vecs_per_mat, m, n,
                                             p_src_a, p_src_x, p_dst);
}

#if !(ARMRAL_ARCH_SVE >= 2)
static armral_status cmplx_mat_vec_mult_batch_pa_one_vec(
    uint16_t num_mats, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t **__restrict p_srcs_a,
    const armral_cmplx_int16_t **__restrict p_srcs_x,
    armral_cmplx_int16_t **__restrict p_dsts) {

  assert(num_mats % 2 == 0);

  for (uint16_t mat_idx = 0; mat_idx < num_mats; mat_idx += 2) {
    // Do two matrix-vector multiplications at a time

    uint32x4_t mask = {~0U, 0U, ~0U, 0U};

    // Loop over A one row at a time
    for (uint16_t row_cnt = 0; row_cnt < m; ++row_cnt) {

      // Initialize the accumulator
      int32x4_t sum_real;
      int32x4_t sum_imag;

      // Loop over the row of A one column at a time
      {
        int i = row_cnt * n;
        int16x4_t a_tmp = vld1_s16((const int16_t *)&p_srcs_a[i][mat_idx]);
        int16x4_t x_tmp = vld1_s16((const int16_t *)&p_srcs_x[0][mat_idx]);
        int16x4_t rev_x = vrev32_s16(x_tmp);
        sum_real = vqdmull_s16(a_tmp, x_tmp);
        sum_imag = vqdmull_s16(a_tmp, rev_x);
      }
      for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
        int i = (row_cnt * n) + col_cnt;
        int16x4_t a_tmp = vld1_s16((const int16_t *)&p_srcs_a[i][mat_idx]);
        int16x4_t x_tmp =
            vld1_s16((const int16_t *)&p_srcs_x[col_cnt][mat_idx]);
        int16x4_t rev_x = vrev32_s16(x_tmp);
        sum_real = vqdmlal_s16(sum_real, a_tmp, x_tmp);
        sum_imag = vqdmlal_s16(sum_imag, a_tmp, rev_x);
      }

      // sum_real = [re1, -re1, re2, -re2]
      int32x4_t neg_real = vqnegq_s32(sum_real);
      // neg_real = [-re1, re1, -re2, re2]
      sum_real = vbslq_s32(mask, sum_real, neg_real);
      // sum_real = [re1, re1, re2, re2]
      // sum_imag = [im1, im1, im2, im2]
      int32x4_t final_sum32_lo = vtrn1q_s32(sum_real, sum_imag);
      int32x4_t final_sum32_hi = vtrn2q_s32(sum_real, sum_imag);
      int32x4_t final_sum32 = vqaddq_s32(final_sum32_lo, final_sum32_hi);

      int16x4_t final_sum16 = vqshrn_n_s32(final_sum32, 16);
      vst1_s16((int16_t *)&p_dsts[row_cnt][mat_idx], final_sum16);
    }
  }

  return ARMRAL_SUCCESS;
}
#else
static armral_status cmplx_mat_vec_mult_batch_pa_one_vec(
    uint16_t num_mats, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t **__restrict p_srcs_a,
    const armral_cmplx_int16_t **__restrict p_srcs_x,
    armral_cmplx_int16_t **__restrict p_dsts) {

  svbool_t ptrue = svptrue_b16();

  for (uint16_t mat_idx = 0; mat_idx < num_mats; mat_idx += svcntw()) {
    svbool_t pg = svwhilelt_b16(mat_idx * 2, num_mats * 2);

    // Loop over A one row at a time
    for (uint16_t row_cnt = 0; row_cnt < m; ++row_cnt) {

      // Initialize the accumulator
      svint32_t sum_real;
      svint32_t sum_imag;

      // Loop over the row of A one column at a time
      {
        int i = row_cnt * n;
        svint16_t a_tmp = svld1_s16(pg, (const int16_t *)&p_srcs_a[i][mat_idx]);
        svint16_t x_tmp = svld1_s16(pg, (const int16_t *)&p_srcs_x[0][mat_idx]);
        svint16_t x_rev = svreinterpret_s16_s32(
            svrevh_s32_x(ptrue, svreinterpret_s32_s16(x_tmp)));
        sum_real = svqdmullb_s32(a_tmp, x_tmp);           // real * real
        sum_imag = svqdmullb_s32(a_tmp, x_rev);           // real * imag
        sum_real = svqdmlslt_s32(sum_real, a_tmp, x_tmp); // imag * imag
        sum_imag = svqdmlalt_s32(sum_imag, a_tmp, x_rev); // imag * real
      }
      for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
        int i = (row_cnt * n) + col_cnt;
        svint16_t a_tmp = svld1_s16(pg, (const int16_t *)&p_srcs_a[i][mat_idx]);
        svint16_t x_tmp =
            svld1_s16(pg, (const int16_t *)&p_srcs_x[col_cnt][mat_idx]);
        svint16_t x_rev = svreinterpret_s16_s32(
            svrevh_s32_x(ptrue, svreinterpret_s32_s16(x_tmp)));
        sum_real = svqdmlalb_s32(sum_real, a_tmp, x_tmp); // real * real
        sum_imag = svqdmlalb_s32(sum_imag, a_tmp, x_rev); // real * imag
        sum_real = svqdmlslt_s32(sum_real, a_tmp, x_tmp); // imag * imag
        sum_imag = svqdmlalt_s32(sum_imag, a_tmp, x_rev); // imag * real
      }

      svint16_t final_sum =
          svqshrnt_n_s32(svqshrnb_n_s32(sum_real, 16), sum_imag, 16);
      svst1_s16(pg, (int16_t *)&p_dsts[row_cnt][mat_idx], final_sum);
    }
  }

  return ARMRAL_SUCCESS;
}
#endif

#if !(ARMRAL_ARCH_SVE >= 2)
static armral_status cmplx_mat_vec_mult_batch_pa_unroll_vec(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t **__restrict p_srcs_a,
    const armral_cmplx_int16_t **__restrict p_srcs_x,
    armral_cmplx_int16_t **__restrict p_dsts) {

  assert(num_vecs_per_mat % 4 == 0);

  uint32x4_t mask = {~0U, 0U, ~0U, 0U};

  for (uint16_t mat_idx = 0; mat_idx < num_mats; ++mat_idx) {
    for (uint16_t vec = 0; vec < num_vecs_per_mat; vec += 4) {
      // Do one matrix-vector multiplication at a time
      // Work out the starting index of the current vector in p_src_x
      // and p_dst
      int vec_idx = (mat_idx * num_vecs_per_mat) + vec;

      // Loop over A one row at a time
      for (uint16_t row_cnt = 0; row_cnt < m; ++row_cnt) {

        // Initialize the accumulator
        int32x4_t sum_real_lo;
        int32x4_t sum_real_hi;
        int32x4_t sum_imag_lo;
        int32x4_t sum_imag_hi;

        // Loop over the row of A one column at a time
        {
          int i = row_cnt * n;
          int16x4_t a_tmp = vld1s_s16(&p_srcs_a[i][mat_idx]);
          int16x8_t x_tmp = vld1q_s16((const int16_t *)&p_srcs_x[0][vec_idx]);
          sum_real_lo = vqdmull_low_lane_s16(x_tmp, a_tmp, 0);
          sum_real_hi = vqdmull_high_lane_s16(x_tmp, a_tmp, 0);
          sum_imag_lo = vqdmull_low_lane_s16(x_tmp, a_tmp, 1);
          sum_imag_hi = vqdmull_high_lane_s16(x_tmp, a_tmp, 1);
        }
        for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
          int i = (row_cnt * n) + col_cnt;
          int16x4_t a_tmp = vld1s_s16(&p_srcs_a[i][mat_idx]);
          int16x8_t x_tmp =
              vld1q_s16((const int16_t *)&p_srcs_x[col_cnt][vec_idx]);
          sum_real_lo = vqdmlal_low_lane_s16(sum_real_lo, x_tmp, a_tmp, 0);
          sum_real_hi = vqdmlal_high_lane_s16(sum_real_hi, x_tmp, a_tmp, 0);
          sum_imag_lo = vqdmlal_low_lane_s16(sum_imag_lo, x_tmp, a_tmp, 1);
          sum_imag_hi = vqdmlal_high_lane_s16(sum_imag_hi, x_tmp, a_tmp, 1);
        }

        sum_imag_lo = vrev64q_s32(sum_imag_lo);
        sum_imag_hi = vrev64q_s32(sum_imag_hi);
        int32x4_t neg_imag_lo = vqnegq_s32(sum_imag_lo);
        int32x4_t neg_imag_hi = vqnegq_s32(sum_imag_hi);
        sum_imag_lo = vbslq_s32(mask, neg_imag_lo, sum_imag_lo);
        sum_imag_hi = vbslq_s32(mask, neg_imag_hi, sum_imag_hi);

        int32x4_t final_sum32_lo = vqaddq_s32(sum_real_lo, sum_imag_lo);
        int32x4_t final_sum32_hi = vqaddq_s32(sum_real_hi, sum_imag_hi);

        vst1q_s16((int16_t *)&p_dsts[row_cnt][vec_idx],
                  vqshrn_high_n_s32(vqshrn_n_s32(final_sum32_lo, 16),
                                    final_sum32_hi, 16));
      }
    }
  }

  return ARMRAL_SUCCESS;
}
#else // SVE2
static armral_status cmplx_mat_vec_mult_batch_pa_unroll_vec(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t **__restrict p_srcs_a,
    const armral_cmplx_int16_t **__restrict p_srcs_x,
    armral_cmplx_int16_t **__restrict p_dsts) {

  for (uint16_t mat_idx = 0; mat_idx < num_mats; ++mat_idx) {
    for (uint16_t vec = 0; vec < num_vecs_per_mat; vec += svcntw()) {
      svbool_t pg = svwhilelt_b16_u32(2 * vec, 2 * num_vecs_per_mat);
      // Do one matrix-vector multiplication at a time
      // Work out the starting index of the current vector in p_src_x
      // and p_dst
      int vec_idx = (mat_idx * num_vecs_per_mat) + vec;

      // Loop over A one row at a time
      for (uint16_t row_cnt = 0; row_cnt < m; ++row_cnt) {

        // Initialize the accumulator
        svint32_t sum_real;
        svint32_t sum_imag;

        // Loop over the row of A one column at a time
        {
          int i = row_cnt * n;
          svint16_t a_tmp = svreinterpret_s16_s32(
              svdup_n_s32(*(const int32_t *)&p_srcs_a[i][mat_idx]));
          svint16_t x_tmp =
              svld1_s16(pg, (const int16_t *)&p_srcs_x[0][vec_idx]);
          sum_real = svqdmullb_lane_s32(x_tmp, a_tmp, 0); // real * real
          sum_imag = svqdmullb_lane_s32(x_tmp, a_tmp, 1); // real * imag
          sum_real =
              svqdmlslt_lane_s32(sum_real, x_tmp, a_tmp, 1); // imag * imag
          sum_imag =
              svqdmlalt_lane_s32(sum_imag, x_tmp, a_tmp, 0); // imag * real
        }
        for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
          int i = (row_cnt * n) + col_cnt;
          svint16_t a_tmp = svreinterpret_s16_s32(
              svdup_n_s32(*(const int32_t *)&p_srcs_a[i][mat_idx]));
          svint16_t x_tmp =
              svld1_s16(pg, (const int16_t *)&p_srcs_x[col_cnt][vec_idx]);
          sum_real =
              svqdmlalb_lane_s32(sum_real, x_tmp, a_tmp, 0); // real * real
          sum_imag =
              svqdmlalb_lane_s32(sum_imag, x_tmp, a_tmp, 1); // real * imag
          sum_real =
              svqdmlslt_lane_s32(sum_real, x_tmp, a_tmp, 1); // imag * imag
          sum_imag =
              svqdmlalt_lane_s32(sum_imag, x_tmp, a_tmp, 0); // imag * real
        }

        svint16_t final_sum =
            svqshrnt_n_s32(svqshrnb_n_s32(sum_real, 16), sum_imag, 16);

        svst1_s16(pg, (int16_t *)&p_dsts[row_cnt][vec_idx], final_sum);
      }
    }
  }

  return ARMRAL_SUCCESS;
}
#endif

armral_status armral_cmplx_mat_vec_mult_batch_i16_32bit_pa(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t **__restrict p_srcs_a,
    const armral_cmplx_int16_t **__restrict p_srcs_x,
    armral_cmplx_int16_t **__restrict p_dsts) {

  // The i-th element of the j-th matrix in the batch is
  // located at `p_srcs[i][j]`. Similarly, the j-th matrix in a batch of `2 x 2`
  // matrices is formed as:
  //  p_srcs[0][j]  p_srcs[1][j]
  //  p_srcs[2][j]  p_srcs[3][j]
  if ((num_mats * num_vecs_per_mat) % 12 != 0) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  if (num_vecs_per_mat == 1) {
    return cmplx_mat_vec_mult_batch_pa_one_vec(num_mats, m, n, p_srcs_a,
                                               p_srcs_x, p_dsts);
  }
  if (num_vecs_per_mat % 4 != 0) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  return cmplx_mat_vec_mult_batch_pa_unroll_vec(num_mats, num_vecs_per_mat, m,
                                                n, p_srcs_a, p_srcs_x, p_dsts);
}
