/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "arm_solve_1sc.h"
#include "arm_solve_4sc.h"
#include "arm_solve_6sc.h"
#include "armral.h"
#include "intrinsics.h"

armral_status armral_solve_2x2_f32(
    uint32_t num_sub_carrier, uint32_t num_sc_per_g,
    const armral_cmplx_int16_t *restrict p_y, uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    const armral_fixed_point_index num_fract_bits_x) {

  switch (num_sc_per_g) {
  case 1:
    return armral_solve_2x2_1sc_f32(
        num_sub_carrier, p_y, p_ystride, p_y_num_fract_bits, p_g_real, p_g_imag,
        p_gstride, p_x, p_xstride, num_fract_bits_x);
  case 4:
    return armral_solve_2x2_4sc_f32(
        num_sub_carrier, p_y, p_ystride, p_y_num_fract_bits, p_g_real, p_g_imag,
        p_gstride, p_x, p_xstride, num_fract_bits_x);
  case 6:
    return armral_solve_2x2_6sc_f32(
        num_sub_carrier, p_y, p_ystride, p_y_num_fract_bits, p_g_real, p_g_imag,
        p_gstride, p_x, p_xstride, num_fract_bits_x);
  }
  return ARMRAL_ARGUMENT_ERROR;
}

armral_status armral_solve_2x4_f32(
    uint32_t num_sub_carrier, uint32_t num_sc_per_g,
    const armral_cmplx_int16_t *restrict p_y, uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    const armral_fixed_point_index num_fract_bits_x) {

  switch (num_sc_per_g) {
  case 1:
    return armral_solve_2x4_1sc_f32(
        num_sub_carrier, p_y, p_ystride, p_y_num_fract_bits, p_g_real, p_g_imag,
        p_gstride, p_x, p_xstride, num_fract_bits_x);
  case 4:
    return armral_solve_2x4_4sc_f32(
        num_sub_carrier, p_y, p_ystride, p_y_num_fract_bits, p_g_real, p_g_imag,
        p_gstride, p_x, p_xstride, num_fract_bits_x);
  case 6:
    return armral_solve_2x4_6sc_f32(
        num_sub_carrier, p_y, p_ystride, p_y_num_fract_bits, p_g_real, p_g_imag,
        p_gstride, p_x, p_xstride, num_fract_bits_x);
  }
  return ARMRAL_ARGUMENT_ERROR;
}

armral_status armral_solve_4x4_f32(
    uint32_t num_sub_carrier, uint32_t num_sc_per_g,
    const armral_cmplx_int16_t *restrict p_y, uint32_t p_ystride,
    const armral_fixed_point_index *p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    armral_fixed_point_index num_fract_bits_x) {

  switch (num_sc_per_g) {
  case 1:
    return armral_solve_4x4_1sc_f32(
        num_sub_carrier, p_y, p_ystride, p_y_num_fract_bits, p_g_real, p_g_imag,
        p_gstride, p_x, p_xstride, num_fract_bits_x);
  case 4:
    return armral_solve_4x4_4sc_f32(
        num_sub_carrier, p_y, p_ystride, p_y_num_fract_bits, p_g_real, p_g_imag,
        p_gstride, p_x, p_xstride, num_fract_bits_x);
  case 6:
    return armral_solve_4x4_6sc_f32(
        num_sub_carrier, p_y, p_ystride, p_y_num_fract_bits, p_g_real, p_g_imag,
        p_gstride, p_x, p_xstride, num_fract_bits_x);
  }
  return ARMRAL_ARGUMENT_ERROR;
}

armral_status armral_solve_1x4_f32(
    uint32_t num_sub_carrier, uint32_t num_sc_per_g,
    const armral_cmplx_int16_t *restrict p_y, uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    const armral_fixed_point_index num_fract_bits_x) {

  switch (num_sc_per_g) {
  case 1:
    return armral_solve_1x4_1sc_f32(num_sub_carrier, p_y, p_ystride,
                                    p_y_num_fract_bits, p_g_real, p_g_imag,
                                    p_gstride, p_x, num_fract_bits_x);
  case 4:
    return armral_solve_1x4_4sc_f32(num_sub_carrier, p_y, p_ystride,
                                    p_y_num_fract_bits, p_g_real, p_g_imag,
                                    p_gstride, p_x, num_fract_bits_x);
  case 6:
    return armral_solve_1x4_6sc_f32(num_sub_carrier, p_y, p_ystride,
                                    p_y_num_fract_bits, p_g_real, p_g_imag,
                                    p_gstride, p_x, num_fract_bits_x);
  }
  return ARMRAL_ARGUMENT_ERROR;
}

armral_status armral_solve_1x2_f32(
    uint32_t num_sub_carrier, uint32_t num_sc_per_g,
    const armral_cmplx_int16_t *restrict p_y, uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    armral_fixed_point_index num_fract_bits_x) {

  switch (num_sc_per_g) {
  case 1:
    return armral_solve_1x2_1sc_f32(num_sub_carrier, p_y, p_ystride,
                                    p_y_num_fract_bits, p_g_real, p_g_imag,
                                    p_gstride, p_x, num_fract_bits_x);
  case 4:
    return armral_solve_1x2_4sc_f32(num_sub_carrier, p_y, p_ystride,
                                    p_y_num_fract_bits, p_g_real, p_g_imag,
                                    p_gstride, p_x, num_fract_bits_x);
  case 6:
    return armral_solve_1x2_6sc_f32(num_sub_carrier, p_y, p_ystride,
                                    p_y_num_fract_bits, p_g_real, p_g_imag,
                                    p_gstride, p_x, num_fract_bits_x);
  }
  return ARMRAL_ARGUMENT_ERROR;
}
