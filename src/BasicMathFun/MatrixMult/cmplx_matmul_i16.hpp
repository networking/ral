/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "intrinsics.h"
#include "utils/allocators.hpp"

#if ARMRAL_ARCH_SVE >= 2
#include <arm_sve.h>
#endif

namespace {
#if ARMRAL_ARCH_SVE >= 2
template<uint16_t rows_b>
inline void compute_blocked_row(svint64_t *acc[], int16_t acc_base_index,
                                svint16_t axi, svint16_t bi0, svint16_t bi1,
                                svint16_t bi2, svint16_t bi3) {
  *acc[acc_base_index] = svcdot_s64(*acc[acc_base_index], axi, bi0, 0); // re
  *acc[acc_base_index + 1] =
      svcdot_s64(*acc[acc_base_index + 1], axi, bi0, 90); // im
  acc_base_index += 2;
  if constexpr (rows_b > 1) {
    *acc[acc_base_index] = svcdot_s64(*acc[acc_base_index], axi, bi1, 0); // re
    *acc[acc_base_index + 1] =
        svcdot_s64(*acc[acc_base_index + 1], axi, bi1, 90); // im
    acc_base_index += 2;
    if constexpr (rows_b > 2) {
      *acc[acc_base_index] =
          svcdot_s64(*acc[acc_base_index], axi, bi2, 0); // re
      *acc[acc_base_index + 1] =
          svcdot_s64(*acc[acc_base_index + 1], axi, bi2, 90); // im
      acc_base_index += 2;
      if constexpr (rows_b > 3) {
        *acc[acc_base_index] =
            svcdot_s64(*acc[acc_base_index], axi, bi3, 0); // re
        *acc[acc_base_index + 1] =
            svcdot_s64(*acc[acc_base_index + 1], axi, bi3, 90); // im
        acc_base_index += 2;
      }
    }
  }
}

template<uint16_t rows_a, uint16_t rows_b>
inline void compute_full_row(svbool_t prow,
                             const armral_cmplx_int16_t *__restrict p_src_a,
                             const armral_cmplx_int16_t *__restrict p_src_b,
                             const uint16_t k, svint64_t *acc[]) {
  // clang-format off
  svint16_t a0i = svdup_s16(0); svint16_t a1i = svdup_s16(0); svint16_t a2i = svdup_s16(0); svint16_t a3i = svdup_s16(0);
  svint16_t bi0 = svdup_s16(0); svint16_t bi1 = svdup_s16(0); svint16_t bi2 = svdup_s16(0); svint16_t bi3 = svdup_s16(0);
  // clang-format on

  bi0 = svld1_s16(prow, (const int16_t *)&p_src_b[0]);
  if constexpr (rows_b > 1) {
    bi1 = svld1_s16(prow, (const int16_t *)&p_src_b[k]);
  }
  if constexpr (rows_b > 2) {
    bi2 = svld1_s16(prow, (const int16_t *)&p_src_b[2 * k]);
  }
  if constexpr (rows_b > 3) {
    bi3 = svld1_s16(prow, (const int16_t *)&p_src_b[3 * k]);
  }

  // A matrix - based svdot
  int16_t acc_index = 0;
  // r0
  a0i = svld1_s16(prow, (const int16_t *)&p_src_a[0]);
  compute_blocked_row<rows_b>(acc, acc_index, a0i, bi0, bi1, bi2, bi3);
  acc_index += 8;
  if constexpr (rows_a > 1) {
    // r1
    a1i = svld1_s16(prow, (const int16_t *)&p_src_a[k]);
    compute_blocked_row<rows_b>(acc, acc_index, a1i, bi0, bi1, bi2, bi3);
    acc_index += 8;
  }
  if constexpr (rows_a > 2) {
    // r2
    a2i = svld1_s16(prow, (const int16_t *)&p_src_a[2 * k]);
    compute_blocked_row<rows_b>(acc, acc_index, a2i, bi0, bi1, bi2, bi3);
    acc_index += 8;
  }
  if constexpr (rows_a > 3) {
    // r3
    a3i = svld1_s16(prow, (const int16_t *)&p_src_a[3 * k]);
    compute_blocked_row<rows_b>(acc, acc_index, a3i, bi0, bi1, bi2, bi3);
    acc_index += 8;
  }
}

template<uint16_t rows_b>
inline void update_dst_based_on_rows_b(armral_cmplx_int16_t *dst,
                                       svint64_t *acc[], int16_t &dst_index,
                                       int16_t &acc_index) {
  dst[dst_index].re =
      (int16_t)(sat((svaddv_s64(svptrue_b64(), *acc[acc_index]) >> 15)));
  dst[dst_index].im =
      (int16_t)(sat((svaddv_s64(svptrue_b64(), *acc[acc_index + 1]) >> 15)));
  dst_index++;
  acc_index += 2;

  if constexpr (rows_b > 1) {
    dst[dst_index].re =
        (int16_t)(sat((svaddv_s64(svptrue_b64(), *acc[acc_index]) >> 15)));
    dst[dst_index].im =
        (int16_t)(sat((svaddv_s64(svptrue_b64(), *acc[acc_index + 1]) >> 15)));
  }
  dst_index++;
  acc_index += 2;

  if constexpr (rows_b > 2) {
    dst[dst_index].re =
        (int16_t)(sat((svaddv_s64(svptrue_b64(), *acc[acc_index]) >> 15)));
    dst[dst_index].im =
        (int16_t)(sat((svaddv_s64(svptrue_b64(), *acc[acc_index + 1]) >> 15)));
  }
  dst_index++;
  acc_index += 2;

  if constexpr (rows_b > 3) {
    dst[dst_index].re =
        (int16_t)(sat((svaddv_s64(svptrue_b64(), *acc[acc_index]) >> 15)));
    dst[dst_index].im =
        (int16_t)(sat((svaddv_s64(svptrue_b64(), *acc[acc_index + 1]) >> 15)));
  }
  dst_index++;
  acc_index += 2;
}

template<uint16_t rows_a, uint16_t rows_b>
inline void update_dst(uint16_t n, armral_cmplx_int16_t *dst,
                       svint64_t *acc[]) {
  int16_t acc_index = 0;
  int16_t dst_index = 0;
  // Update dst based on rows_a
  // r0
  update_dst_based_on_rows_b<rows_b>(dst, acc, dst_index, acc_index);
  if constexpr (rows_a > 1) {
    // r1
    dst_index = n;
    update_dst_based_on_rows_b<rows_b>(dst, acc, dst_index, acc_index);
    if constexpr (rows_a > 2) {
      // r2
      dst_index = 2 * n;
      update_dst_based_on_rows_b<rows_b>(dst, acc, dst_index, acc_index);
      if constexpr (rows_a > 3) {
        // r3
        dst_index = 3 * n;
        update_dst_based_on_rows_b<rows_b>(dst, acc, dst_index, acc_index);
      }
    }
  }
}

template<uint16_t rows_a, uint16_t rows_b>
inline void sve_mat_a_b_dot(const uint16_t m, const uint16_t n,
                            const uint16_t k,
                            const armral_cmplx_int16_t *__restrict p_src_a,
                            const armral_cmplx_int16_t *__restrict p_src_b,
                            armral_cmplx_int16_t *dst) {
  // clang-format off
  // r0
  svint64_t c00_re = svdup_s64(0); svint64_t c00_im = svdup_s64(0);
  svint64_t c01_re = svdup_s64(0); svint64_t c01_im = svdup_s64(0);
  svint64_t c02_re = svdup_s64(0); svint64_t c02_im = svdup_s64(0);
  svint64_t c03_re = svdup_s64(0); svint64_t c03_im = svdup_s64(0);

  // r1
  svint64_t c10_re = svdup_s64(0); svint64_t c10_im = svdup_s64(0);
  svint64_t c11_re = svdup_s64(0); svint64_t c11_im = svdup_s64(0);
  svint64_t c12_re = svdup_s64(0); svint64_t c12_im = svdup_s64(0);
  svint64_t c13_re = svdup_s64(0); svint64_t c13_im = svdup_s64(0);

  // r2
  svint64_t c20_re = svdup_s64(0); svint64_t c20_im = svdup_s64(0);
  svint64_t c21_re = svdup_s64(0); svint64_t c21_im = svdup_s64(0);
  svint64_t c22_re = svdup_s64(0); svint64_t c22_im = svdup_s64(0);
  svint64_t c23_re = svdup_s64(0); svint64_t c23_im = svdup_s64(0);

  // r3
  svint64_t c30_re = svdup_s64(0); svint64_t c30_im = svdup_s64(0);
  svint64_t c31_re = svdup_s64(0); svint64_t c31_im = svdup_s64(0);
  svint64_t c32_re = svdup_s64(0); svint64_t c32_im = svdup_s64(0);
  svint64_t c33_re = svdup_s64(0); svint64_t c33_im = svdup_s64(0);
  // clang-format on

  svint64_t *acc[] = {
      &c00_re, &c00_im, &c01_re, &c01_im, &c02_re, &c02_im, &c03_re, &c03_im,
      &c10_re, &c10_im, &c11_re, &c11_im, &c12_re, &c12_im, &c13_re, &c13_im,
      &c20_re, &c20_im, &c21_re, &c21_im, &c22_re, &c22_im, &c23_re, &c23_im,
      &c30_re, &c30_im, &c31_re, &c31_im, &c32_re, &c32_im, &c33_re, &c33_im};

  uint16_t full_vecs = k / svcntw(); // word - 2*16-bit elements
  uint16_t tail = k % svcntw();

  svbool_t pa = svptrue_b16();

  uint16_t h = 0;
  for (uint16_t done_vecs = 0; done_vecs < full_vecs;
       done_vecs++, h += svcntw()) {
    compute_full_row<rows_a, rows_b>(pa, &p_src_a[h], &p_src_b[h], k, acc);
  }

  if (tail) {
    pa = svwhilelt_b16(0, 2 * tail);
    compute_full_row<rows_a, rows_b>(pa, &p_src_a[h], &p_src_b[h], k, acc);
  }
  update_dst<rows_a, rows_b>(n, dst, acc);
}
#else
inline int16x4_t __attribute__((always_inline))
vzip1_u16x2(int16x4_t a, int16x4_t b) {
  // should be zip1 c.2s, a.2s, b.2s
  // but this expression appears to give better performance
  return vreinterpret_s16_u32(
      vset_lane_u32(vreinterpret_u32_s16(b)[0], vreinterpret_u32_s16(a), 1));
}

inline int16x4_t __attribute__((always_inline))
vzip2_u16x2(int16x4_t a, int16x4_t b) {
  // should be zip2 c.2s, a.2s, b.2s
  // but this expression appears to give better performance
  return vreinterpret_s16_u32(
      vset_lane_u32(vreinterpret_u32_s16(a)[1], vreinterpret_u32_s16(b), 0));
}
#endif

template<typename Allocator>
inline armral_status
cmplx_matmul_i16(const uint16_t m, const uint16_t n, const uint16_t k,
                 const armral_cmplx_int16_t *__restrict p_src_a,
                 const armral_cmplx_int16_t *__restrict p_src_b,
                 armral_cmplx_int16_t *p_dst, Allocator &allocator) {
#if ARMRAL_ARCH_SVE >= 2
  auto p_transp_b =
      allocate_uninitialized<armral_cmplx_int16_t>(allocator, n * k);
  uint16_t cnt = 0;
  for (uint16_t i = 0; i < n; i++) {
    for (uint16_t j = 0; j < k; j++) {
      p_transp_b[cnt++] = p_src_b[j * n + i];
    }
  }

  uint16_t i = 0;
  for (; i < m - 3; i += 4) {
    uint16_t j = 0;
    for (; j < n - 3; j += 4) {
      sve_mat_a_b_dot<4, 4>(m, n, k, &p_src_a[i * k], &p_transp_b[j * k],
                            &p_dst[i * n + j]);
    }

    for (; j < n - 1; j += 2) {
      sve_mat_a_b_dot<4, 2>(m, n, k, &p_src_a[i * k], &p_transp_b[j * k],
                            &p_dst[i * n + j]);
    }

    // If n is odd, we have one more row/col to go
    if (n % 2) {
      j = n - 1;
      sve_mat_a_b_dot<4, 1>(m, n, k, &p_src_a[i * k], &p_transp_b[j * k],
                            &p_dst[i * n + j]);
    }
  }

  for (; i < m - 1; i += 2) {
    uint16_t j = 0;
    for (; j < n - 3; j += 4) {
      sve_mat_a_b_dot<2, 4>(m, n, k, &p_src_a[i * k], &p_transp_b[j * k],
                            &p_dst[i * n + j]);
    }

    for (; j < n - 1; j += 2) {
      sve_mat_a_b_dot<2, 2>(m, n, k, &p_src_a[i * k], &p_transp_b[j * k],
                            &p_dst[i * n + j]);
    }

    // If n is odd, we have one more row/col to go
    if (n % 2) {
      j = n - 1;
      sve_mat_a_b_dot<2, 1>(m, n, k, &p_src_a[i * k], &p_transp_b[j * k],
                            &p_dst[i * n + j]);
    }
  }

  if (m % 2) {
    i = m - 1;
    uint16_t j = 0;
    for (; j < n - 3; j += 4) {
      sve_mat_a_b_dot<1, 4>(m, n, k, &p_src_a[i * k], &p_transp_b[j * k],
                            &p_dst[i * n + j]);
    }

    for (; j < n - 1; j += 2) {
      sve_mat_a_b_dot<1, 2>(m, n, k, &p_src_a[i * k], &p_transp_b[j * k],
                            &p_dst[i * n + j]);
    }

    // If n is odd, we have one more row/col to go
    if (n % 2) {
      j = n - 1;
      sve_mat_a_b_dot<1, 1>(m, n, k, &p_src_a[i * k], &p_transp_b[j * k],
                            &p_dst[i * n + j]);
    }
  }
#else
  const int16_t *p_in1 = (const int16_t *)p_src_a;
  const armral_cmplx_int16_t *p_in_a = p_src_a;
  armral_cmplx_int16_t *p_out = p_dst;
  uint16_t num_rows_a = m; // Number of rows of input matrix A
  uint16_t num_cols_b = n; // Number of columns of input matrix B
  uint16_t num_cols_a = k; // Number of columns of input matrix A

  const int16_t *p_in1_b = (const int16_t *)p_src_a;
  const int16_t *p_in1_b2 = (const int16_t *)p_src_b;

  // Row loop
  for (uint16_t row_cnt = num_rows_a >> 1; row_cnt > 0;
       --row_cnt, p_out += 2 * num_cols_b, p_in_a += 2 * num_cols_a) {
    // Output pointer is set to starting address of the row being processed
    armral_cmplx_int16_t *px = p_out;
    armral_cmplx_int16_t *px_b = px + num_cols_b;

    // For every row-wise process, the column loop counter is initialized
    uint16_t col = num_cols_b;

    // For every row-wise process, the pIn2 pointer is set
    // to the starting address of the pSrcB data
    const int16_t *p_in2 = (const int16_t *)p_src_b;
    p_in1_b2 = p_in2 + 2 * num_cols_b;
    uint16_t j = 0U;

    // Column loop
    while (col > 1U) {
      // Set the variable sum, that acts as accumulator, to zero
      int64_t sum_real1_ext = 0;
      int64_t sum_imag1_ext = 0;
      int64_t sum_real1_b_ext = 0;
      int64_t sum_imag1_b_ext = 0;

      int64_t sum_real_ext2 = 0;
      int64_t sum_imag_ext2 = 0;
      int64_t sum_real_b_ext2 = 0;
      int64_t sum_imag_b_ext2 = 0;

      // Initialize the pointer pIn1 to point to the starting address of the
      // column being processed
      p_in1 = (const int16_t *)p_in_a;
      p_in1_b = p_in1 + 2 * num_cols_a;

      int64x2_t acc_r0 = {0};
      int64x2_t acc_i0 = {0};
      int64x2_t acc_r1 = {0};
      int64x2_t acc_i1 = {0};
      int64x2_t acc_r2 = {0};
      int64x2_t acc_i2 = {0};
      int64x2_t acc_r3 = {0};
      int64x2_t acc_i3 = {0};

      // Matrix multiplication
      for (uint16_t col_cnt = num_cols_a >> 3; col_cnt > 0; --col_cnt) {
        // int16x8x2_t load and de-interleave
        int16x8x2_t a0_v = vld2q_s16(p_in1);
        // Extend to 32bit Real part int32x4x2_t
        int32x4_t a0_v_rextended[2];
        a0_v_rextended[0] = vmovl_low_s16(a0_v.val[0]);
        a0_v_rextended[1] = vmovl_high_s16(a0_v.val[0]);
        // Extend to 32bit Imag part int32x4x2_t
        int32x4_t a0_v_iextended[2];
        a0_v_iextended[0] = vmovl_low_s16(a0_v.val[1]);
        a0_v_iextended[1] = vmovl_high_s16(a0_v.val[1]);

        int16x8x2_t a1_v = vld2q_s16(p_in1_b);
        int32x4x2_t a1_v_rextended;
        a1_v_rextended.val[0] = vmovl_low_s16(a1_v.val[0]);  // Extend to 32bit
        a1_v_rextended.val[1] = vmovl_high_s16(a1_v.val[0]); // Extend to 32bit
        int32x4x2_t a1_v_iextended;
        a1_v_iextended.val[0] = vmovl_low_s16(a1_v.val[1]);  // Extend to 32bit
        a1_v_iextended.val[1] = vmovl_high_s16(a1_v.val[1]); // Extend to 32bit

        p_in1 += 16;
        p_in1_b += 16;
        // 4 B rows
        // Load but NOT separate real/imag
        int16x4_t b0_v = vld1_s16(p_in2);
        int16x4_t b1_v = vld1_s16(p_in1_b2);
        int16x4_t b2_v = vld1_s16(p_in2 + 4 * num_cols_b);
        int16x4_t b3_v = vld1_s16(p_in1_b2 + 4 * num_cols_b);
        p_in2 = p_in2 + 8 * num_cols_b;
        p_in1_b2 = p_in1_b2 + 8 * num_cols_b;

        // 4 B rows
        // Load but NOT separate real/imag
        int16x4_t b4_v = vld1_s16(p_in2);
        int16x4_t b5_v = vld1_s16(p_in1_b2);
        int16x4_t b6_v = vld1_s16(p_in2 + 4 * num_cols_b);
        int16x4_t b7_v = vld1_s16(p_in1_b2 + 4 * num_cols_b);
        p_in2 = p_in2 + 8 * num_cols_b;
        p_in1_b2 = p_in1_b2 + 8 * num_cols_b;

        // Even elem first 2 B rows, 4 columns
        int16x4_t b_col_real = vtrn1_s16(b0_v, b1_v);
        // Odd elem first 2 B rows, 4 columns
        int16x4_t b_col_im = vtrn2_s16(b0_v, b1_v);
        // Even elem 3rd and 4th B rows, 4 columns
        int16x4_t b_col_real2 = vtrn1_s16(b2_v, b3_v);
        // Odd elem 3rd and 4th B rows, 4 columns
        int16x4_t b_col_im2 = vtrn2_s16(b2_v, b3_v);
        // Even elem 5th and 6th B rows, 4 columns
        int16x4_t b_col_real3 = vtrn1_s16(b4_v, b5_v);
        // Odd elem 5th and 6th B rows, 4 columns
        int16x4_t b_col_im3 = vtrn2_s16(b4_v, b5_v);
        // Even elem 7th and 8th B rows, 4 columns
        int16x4_t b_col_real4 = vtrn1_s16(b6_v, b7_v);
        // Odd elem 7th and 8th B rows, 4 columns
        int16x4_t b_col_im4 = vtrn2_s16(b6_v, b7_v);

        // First column B first 4 rows
        int16x4_t temp_r0 = vzip1_u16x2(b_col_real, b_col_real2);
        int16x4_t temp_i0 = vzip1_u16x2(b_col_im, b_col_im2);

        // First column second 4 rows
        int16x4_t temp_r1 = vzip1_u16x2(b_col_real3, b_col_real4);
        int16x4_t temp_i1 = vzip1_u16x2(b_col_im3, b_col_im4);

        // Second column first four rows
        int16x4_t temp_r2 = vzip2_u16x2(b_col_real, b_col_real2);
        int16x4_t temp_i2 = vzip2_u16x2(b_col_im, b_col_im2);

        // Second column B second four rows
        int16x4_t temp_r3 = vzip2_u16x2(b_col_real3, b_col_real4);
        int16x4_t temp_i3 = vzip2_u16x2(b_col_im3, b_col_im4);

        int32x4_t temp_r0extended = vmovl_s16(temp_r0); // 32x4
        int32x4_t temp_i0extended = vmovl_s16(temp_i0); // 32x4

        int32x4_t temp_r1extended = vmovl_s16(temp_r1); // 32x4
        int32x4_t temp_i1extended = vmovl_s16(temp_i1); // 32x4

        int32x4_t temp_r2extended = vmovl_s16(temp_r2); // 32x4
        int32x4_t temp_i2extended = vmovl_s16(temp_i2); // 32x4

        int32x4_t temp_r3extended = vmovl_s16(temp_r3); // 32x4
        int32x4_t temp_i3extended = vmovl_s16(temp_i3); // 32x4

        // First row * first col (4 B rows high)
        acc_r0 = vmlal_high_s32(acc_r0, a0_v_rextended[0], temp_r0extended);
        acc_r0 = vmlal_low_s32(acc_r0, a0_v_rextended[0], temp_r0extended);
        // First row * first col (4 B rows low)
        acc_r0 = vmlal_high_s32(acc_r0, a0_v_rextended[1], temp_r1extended);
        acc_r0 = vmlal_low_s32(acc_r0, a0_v_rextended[1], temp_r1extended);
        acc_r0 = vmlsl_high_s32(acc_r0, a0_v_iextended[0], temp_i0extended);
        acc_r0 = vmlsl_low_s32(acc_r0, a0_v_iextended[0], temp_i0extended);
        acc_r0 = vmlsl_high_s32(acc_r0, a0_v_iextended[1], temp_i1extended);
        acc_r0 = vmlsl_low_s32(acc_r0, a0_v_iextended[1], temp_i1extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_v_iextended[0], temp_r0extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_iextended[0], temp_r0extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_v_iextended[1], temp_r1extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_iextended[1], temp_r1extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_v_rextended[0], temp_i0extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_rextended[0], temp_i0extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_v_rextended[1], temp_i1extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_rextended[1], temp_i1extended);

        // Second row * first col (4 B rows high)
        acc_r1 = vmlal_high_s32(acc_r1, a1_v_rextended.val[0], temp_r0extended);
        acc_r1 = vmlal_low_s32(acc_r1, a1_v_rextended.val[0], temp_r0extended);
        // Second row * first col (4 B rows high)
        acc_r1 = vmlal_high_s32(acc_r1, a1_v_rextended.val[1], temp_r1extended);
        acc_r1 = vmlal_low_s32(acc_r1, a1_v_rextended.val[1], temp_r1extended);

        acc_r1 = vmlsl_high_s32(acc_r1, a1_v_iextended.val[0], temp_i0extended);
        acc_r1 = vmlsl_low_s32(acc_r1, a1_v_iextended.val[0], temp_i0extended);

        acc_r1 = vmlsl_high_s32(acc_r1, a1_v_iextended.val[1], temp_i1extended);
        acc_r1 = vmlsl_low_s32(acc_r1, a1_v_iextended.val[1], temp_i1extended);

        acc_i1 = vmlal_high_s32(acc_i1, a1_v_iextended.val[0], temp_r0extended);
        acc_i1 = vmlal_low_s32(acc_i1, a1_v_iextended.val[0], temp_r0extended);

        acc_i1 = vmlal_high_s32(acc_i1, a1_v_iextended.val[1], temp_r1extended);
        acc_i1 = vmlal_low_s32(acc_i1, a1_v_iextended.val[1], temp_r1extended);

        acc_i1 = vmlal_high_s32(acc_i1, a1_v_rextended.val[0], temp_i0extended);
        acc_i1 = vmlal_low_s32(acc_i1, a1_v_rextended.val[0], temp_i0extended);

        acc_i1 = vmlal_high_s32(acc_i1, a1_v_rextended.val[1], temp_i1extended);
        acc_i1 = vmlal_low_s32(acc_i1, a1_v_rextended.val[1], temp_i1extended);

        // First row * second col (4 B rows high)
        acc_r2 = vmlal_high_s32(acc_r2, a0_v_rextended[0], temp_r2extended);
        acc_r2 = vmlal_low_s32(acc_r2, a0_v_rextended[0], temp_r2extended);
        // First row * first col (4 B rows low)
        acc_r2 = vmlal_high_s32(acc_r2, a0_v_rextended[1], temp_r3extended);
        acc_r2 = vmlal_low_s32(acc_r2, a0_v_rextended[1], temp_r3extended);

        acc_r2 = vmlsl_high_s32(acc_r2, a0_v_iextended[0], temp_i2extended);
        acc_r2 = vmlsl_low_s32(acc_r2, a0_v_iextended[0], temp_i2extended);

        acc_r2 = vmlsl_high_s32(acc_r2, a0_v_iextended[1], temp_i3extended);
        acc_r2 = vmlsl_low_s32(acc_r2, a0_v_iextended[1], temp_i3extended);

        acc_i2 = vmlal_high_s32(acc_i2, a0_v_iextended[0], temp_r2extended);
        acc_i2 = vmlal_low_s32(acc_i2, a0_v_iextended[0], temp_r2extended);

        acc_i2 = vmlal_high_s32(acc_i2, a0_v_iextended[1], temp_r3extended);
        acc_i2 = vmlal_low_s32(acc_i2, a0_v_iextended[1], temp_r3extended);

        acc_i2 = vmlal_high_s32(acc_i2, a0_v_rextended[0], temp_i2extended);
        acc_i2 = vmlal_low_s32(acc_i2, a0_v_rextended[0], temp_i2extended);

        acc_i2 = vmlal_high_s32(acc_i2, a0_v_rextended[1], temp_i3extended);
        acc_i2 = vmlal_low_s32(acc_i2, a0_v_rextended[1], temp_i3extended);

        // Second row * second col (4 B rows high)
        acc_r3 = vmlal_high_s32(acc_r3, a1_v_rextended.val[0], temp_r2extended);
        acc_r3 = vmlal_low_s32(acc_r3, a1_v_rextended.val[0], temp_r2extended);
        // Second row * second col (4 B rows high)
        acc_r3 = vmlal_high_s32(acc_r3, a1_v_rextended.val[1], temp_r3extended);
        acc_r3 = vmlal_low_s32(acc_r3, a1_v_rextended.val[1], temp_r3extended);

        acc_r3 = vmlsl_high_s32(acc_r3, a1_v_iextended.val[0], temp_i2extended);
        acc_r3 = vmlsl_low_s32(acc_r3, a1_v_iextended.val[0], temp_i2extended);

        acc_r3 = vmlsl_high_s32(acc_r3, a1_v_iextended.val[1], temp_i3extended);
        acc_r3 = vmlsl_low_s32(acc_r3, a1_v_iextended.val[1], temp_i3extended);

        acc_i3 = vmlal_high_s32(acc_i3, a1_v_iextended.val[0], temp_r2extended);
        acc_i3 = vmlal_low_s32(acc_i3, a1_v_iextended.val[0], temp_r2extended);

        acc_i3 = vmlal_high_s32(acc_i3, a1_v_iextended.val[1], temp_r3extended);
        acc_i3 = vmlal_low_s32(acc_i3, a1_v_iextended.val[1], temp_r3extended);

        acc_i3 = vmlal_high_s32(acc_i3, a1_v_rextended.val[0], temp_i2extended);
        acc_i3 = vmlal_low_s32(acc_i3, a1_v_rextended.val[0], temp_i2extended);

        acc_i3 = vmlal_high_s32(acc_i3, a1_v_rextended.val[1], temp_i3extended);
        acc_i3 = vmlal_low_s32(acc_i3, a1_v_rextended.val[1], temp_i3extended);
      }

      // Matrix multiplication
      if ((num_cols_a & 4) != 0) {
        int16x4x2_t a2_v = vld2_s16(p_in1);
        p_in1 += 8; // int16x4x2_t
        // Extend to 32bit int32x4x2_t
        int32x4x2_t a0_vextended;
        a0_vextended.val[0] = vmovl_s16(a2_v.val[0]);
        a0_vextended.val[1] = vmovl_s16(a2_v.val[1]);
        int16x4x2_t a3_v = vld2_s16(p_in1_b);
        p_in1_b += 8;
        int32x4x2_t a1_vextended;
        a1_vextended.val[0] = vmovl_s16(a3_v.val[0]);
        a1_vextended.val[1] = vmovl_s16(a3_v.val[1]);
        // 4 B rows
        // Load but NOT separate real/imag
        int16x4_t b0_v = vld1_s16(p_in2);
        int16x4_t b1_v = vld1_s16(p_in1_b2);
        int16x4_t b2_v = vld1_s16(p_in2 + 4 * num_cols_b);
        int16x4_t b3_v = vld1_s16(p_in1_b2 + 4 * num_cols_b);
        p_in2 = p_in2 + 8 * num_cols_b;

        // Even elem first 2 B rows, 4 columns
        int16x4_t b_col_real = vtrn1_s16(b0_v, b1_v);
        // Odd elem first 2 B rows, 4 columns
        int16x4_t b_col_im = vtrn2_s16(b0_v, b1_v);
        // Even elem 3rd and 4th B rows, 4 columns
        int16x4_t b_col_real2 = vtrn1_s16(b2_v, b3_v);
        // Odd elem 3rd and 4th B rows, 4 columns
        int16x4_t b_col_im2 = vtrn2_s16(b2_v, b3_v);

        // First column B first 4 rows
        int16x4_t temp_r0 = vzip1_u16x2(b_col_real, b_col_real2);
        int16x4_t temp_i0 = vzip1_u16x2(b_col_im, b_col_im2);

        // Second column first four rows
        int16x4_t temp_r2 = vzip2_u16x2(b_col_real, b_col_real2);
        int16x4_t temp_i2 = vzip2_u16x2(b_col_im, b_col_im2);

        int32x4_t temp_r0extended = vmovl_s16(temp_r0); // 32x4
        int32x4_t temp_i0extended = vmovl_s16(temp_i0); // 32x4
        int32x4_t temp_r2extended = vmovl_s16(temp_r2); // 32x4
        int32x4_t temp_i2extended = vmovl_s16(temp_i2); // 32x4

        // First row * first col
        acc_r0 = vmlal_high_s32(acc_r0, a0_vextended.val[0], temp_r0extended);
        acc_r0 = vmlal_low_s32(acc_r0, a0_vextended.val[0], temp_r0extended);

        acc_r0 = vmlsl_high_s32(acc_r0, a0_vextended.val[1], temp_i0extended);
        acc_r0 = vmlsl_low_s32(acc_r0, a0_vextended.val[1], temp_i0extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_vextended.val[1], temp_r0extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_vextended.val[1], temp_r0extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_vextended.val[0], temp_i0extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_vextended.val[0], temp_i0extended);
        // Second row * first col
        acc_r1 = vmlal_high_s32(acc_r1, a1_vextended.val[0], temp_r0extended);
        acc_r1 = vmlal_low_s32(acc_r1, a1_vextended.val[0], temp_r0extended);

        acc_r1 = vmlsl_high_s32(acc_r1, a1_vextended.val[1], temp_i0extended);
        acc_r1 = vmlsl_low_s32(acc_r1, a1_vextended.val[1], temp_i0extended);

        acc_i1 = vmlal_high_s32(acc_i1, a1_vextended.val[1], temp_r0extended);
        acc_i1 = vmlal_low_s32(acc_i1, a1_vextended.val[1], temp_r0extended);

        acc_i1 = vmlal_high_s32(acc_i1, a1_vextended.val[0], temp_i0extended);
        acc_i1 = vmlal_low_s32(acc_i1, a1_vextended.val[0], temp_i0extended);
        // First row * second col
        acc_r2 = vmlal_high_s32(acc_r2, a0_vextended.val[0], temp_r2extended);
        acc_r2 = vmlal_low_s32(acc_r2, a0_vextended.val[0], temp_r2extended);

        acc_r2 = vmlsl_high_s32(acc_r2, a0_vextended.val[1], temp_i2extended);
        acc_r2 = vmlsl_low_s32(acc_r2, a0_vextended.val[1], temp_i2extended);

        acc_i2 = vmlal_high_s32(acc_i2, a0_vextended.val[1], temp_r2extended);
        acc_i2 = vmlal_low_s32(acc_i2, a0_vextended.val[1], temp_r2extended);

        acc_i2 = vmlal_high_s32(acc_i2, a0_vextended.val[0], temp_i2extended);
        acc_i2 = vmlal_low_s32(acc_i2, a0_vextended.val[0], temp_i2extended);
        // Second row * second col
        acc_r3 = vmlal_high_s32(acc_r3, a1_vextended.val[0], temp_r2extended);
        acc_r3 = vmlal_low_s32(acc_r3, a1_vextended.val[0], temp_r2extended);

        acc_r3 = vmlsl_high_s32(acc_r3, a1_vextended.val[1], temp_i2extended);
        acc_r3 = vmlsl_low_s32(acc_r3, a1_vextended.val[1], temp_i2extended);

        acc_i3 = vmlal_high_s32(acc_i3, a1_vextended.val[1], temp_r2extended);
        acc_i3 = vmlal_low_s32(acc_i3, a1_vextended.val[1], temp_r2extended);

        acc_i3 = vmlal_high_s32(acc_i3, a1_vextended.val[0], temp_i2extended);
        acc_i3 = vmlal_low_s32(acc_i3, a1_vextended.val[0], temp_i2extended);
      }

      sum_real1_ext += vaddvq_s64(acc_r0);
      sum_imag1_ext += vaddvq_s64(acc_i0);
      sum_real1_b_ext += vaddvq_s64(acc_r1);
      sum_imag1_b_ext += vaddvq_s64(acc_i1);
      sum_real_ext2 += vaddvq_s64(acc_r2);
      sum_imag_ext2 += vaddvq_s64(acc_i2);
      sum_real_b_ext2 += vaddvq_s64(acc_r3);
      sum_imag_b_ext2 += vaddvq_s64(acc_i3);

      // If the columns of pSrcA is not a multiple of 4, compute any remaining
      // MACs here. No loop unrolling is used.
      for (uint16_t col_cnt = num_cols_a & 3; col_cnt > 0; --col_cnt) {
        int16_t a1 = (*p_in1);
        int16_t a1_b = (*p_in1_b);

        int16_t c1 = (*p_in2);
        int16_t c1_b = (*(p_in2 + 2U));

        int16_t b1 = (*(p_in1 + 1U));
        int16_t b1_b = (*(p_in1_b + 1U));

        int16_t d1 = (*(p_in2 + 1U));
        int16_t d1_b = (*(p_in2 + 3U));

        sum_real1_ext += a1 * c1;
        sum_imag1_ext += b1 * c1;
        sum_real1_b_ext += a1_b * c1;
        sum_imag1_b_ext += b1_b * c1;
        sum_real_ext2 += a1 * c1_b;
        sum_imag_ext2 += b1 * c1_b;
        sum_real_b_ext2 += a1_b * c1_b;
        sum_imag_b_ext2 += b1_b * c1_b;

        p_in1 += 2U;
        p_in1_b += 2U;
        p_in2 += 2 * num_cols_b;

        sum_real1_ext -= b1 * d1;
        sum_imag1_ext += a1 * d1;
        sum_real1_b_ext -= b1_b * d1;
        sum_imag1_b_ext += a1_b * d1;
        sum_real_ext2 -= b1 * d1_b;
        sum_imag_ext2 += a1 * d1_b;
        sum_real_b_ext2 -= b1_b * d1_b;
        sum_imag_b_ext2 += a1_b * d1_b;
      }

      // sumReal1Ext += sumReal2Ext;
      int16x4_t out[2] = {0};
      out[0] = vset_lane_s16(vqmovns_s32(vqshrnd_n_s64(sum_real1_ext, 15)),
                             out[0], 0);
      out[0] = vset_lane_s16(vqmovns_s32(vqshrnd_n_s64(sum_imag1_ext, 15)),
                             out[0], 1);
      out[0] = vset_lane_s16(vqmovns_s32(vqshrnd_n_s64(sum_real_ext2, 15)),
                             out[0], 2);
      out[0] = vset_lane_s16(vqmovns_s32(vqshrnd_n_s64(sum_imag_ext2, 15)),
                             out[0], 3);

      out[1] = vset_lane_s16(vqmovns_s32(vqshrnd_n_s64(sum_real1_b_ext, 15)),
                             out[1], 0);
      out[1] = vset_lane_s16(vqmovns_s32(vqshrnd_n_s64(sum_imag1_b_ext, 15)),
                             out[1], 1);
      out[1] = vset_lane_s16(vqmovns_s32(vqshrnd_n_s64(sum_real_b_ext2, 15)),
                             out[1], 2);
      out[1] = vset_lane_s16(vqmovns_s32(vqshrnd_n_s64(sum_imag_b_ext2, 15)),
                             out[1], 3);

      // Store the result in the destination buffer
      vst1_s16((int16_t *)px, out[0]);
      vst1_s16((int16_t *)px_b, out[1]);
      px += 2;
      px_b += 2;

      // Update the pointer pIn2 to point to the starting address of the next
      // column
      j++;
      p_in2 = (const int16_t *)p_src_b + 4U * j;
      p_in1_b2 = p_in2 + 2U * num_cols_b;
      col = col - 2;
    }

    // Deal with a single column of B
    if ((num_cols_b & 1) != 0) {
      // Set the variable sum, that acts as accumulator, to zero
      int64_t sum_real1_ext = 0;
      int64_t sum_imag1_ext = 0;

      int64_t sum_real1_b_ext = 0;
      int64_t sum_imag1_b_ext = 0;

      // Initialize the pointer pIn1 to point to the starting address of the
      // column being processed
      p_in1 = (const int16_t *)p_in_a;
      p_in1_b = p_in1 + 2 * num_cols_a;

      int64x2_t acc_r0 = {0};
      int64x2_t acc_i0 = {0};
      int64x2_t acc_r1 = {0};
      int64x2_t acc_i1 = {0};

      // Compute 8 MACs simultaneously
      uint16_t col_cnt = num_cols_a >> 3;

      // Matrix multiplication
      while (col_cnt > 0U) {
        // int16x8x2_t load and de-interleave
        int16x8x2_t a0_v = vld2q_s16(p_in1);
        // Extend to 32bit Real part int32x4x2_t
        int32x4_t a0_v_rextended[2];
        a0_v_rextended[0] = vmovl_low_s16(a0_v.val[0]);
        a0_v_rextended[1] = vmovl_high_s16(a0_v.val[0]);
        // Extend to 32bit Imag part int32x4x2_t
        int32x4_t a0_v_iextended[2];
        a0_v_iextended[0] = vmovl_low_s16(a0_v.val[1]);
        a0_v_iextended[1] = vmovl_high_s16(a0_v.val[1]);

        int16x8x2_t a1_v = vld2q_s16(p_in1_b);
        int32x4_t a1_v_rextended[2];
        a1_v_rextended[0] = vmovl_low_s16(a1_v.val[0]);  // Extend to 32bit
        a1_v_rextended[1] = vmovl_high_s16(a1_v.val[0]); // Extend to 32bit
        int32x4_t a1_v_iextended[2];
        a1_v_iextended[0] = vmovl_low_s16(a1_v.val[1]);  // Extend to 32bit
        a1_v_iextended[1] = vmovl_high_s16(a1_v.val[1]); // Extend to 32bit

        // Load the first four rows of B, splitting real and imaginary
        // components
        int16x4x2_t tmp_first_four = {0};
        tmp_first_four = vld2_lane_s16(p_in2, tmp_first_four, 0);
        tmp_first_four =
            vld2_lane_s16(p_in2 + 2 * num_cols_b, tmp_first_four, 1);
        tmp_first_four =
            vld2_lane_s16(p_in2 + 4 * num_cols_b, tmp_first_four, 2);
        tmp_first_four =
            vld2_lane_s16(p_in2 + 6 * num_cols_b, tmp_first_four, 3);

        // Load the next four rows of B, splitting real and imaginary components
        int16x4x2_t tmp_second_four = {0};
        tmp_second_four =
            vld2_lane_s16(p_in2 + 8 * num_cols_b, tmp_second_four, 0);
        tmp_second_four =
            vld2_lane_s16(p_in2 + 10 * num_cols_b, tmp_second_four, 1);
        tmp_second_four =
            vld2_lane_s16(p_in2 + 12 * num_cols_b, tmp_second_four, 2);
        tmp_second_four =
            vld2_lane_s16(p_in2 + 14 * num_cols_b, tmp_second_four, 3);

        int32x4_t r_32bit[2] = {0};
        r_32bit[0] = vmovl_s16(tmp_first_four.val[0]);
        r_32bit[1] = vmovl_s16(tmp_second_four.val[0]);
        int32x4_t i_32bit[2] = {0};
        i_32bit[0] = vmovl_s16(tmp_first_four.val[1]);
        i_32bit[1] = vmovl_s16(tmp_second_four.val[1]);

        // First row * column of B
        // Real * real
        acc_r0 = vmlal_low_s32(acc_r0, a0_v_rextended[0], r_32bit[0]);
        acc_r0 = vmlal_high_s32(acc_r0, a0_v_rextended[0], r_32bit[0]);
        acc_r0 = vmlal_low_s32(acc_r0, a0_v_rextended[1], r_32bit[1]);
        acc_r0 = vmlal_high_s32(acc_r0, a0_v_rextended[1], r_32bit[1]);
        // Imag * imag
        acc_r0 = vmlsl_low_s32(acc_r0, a0_v_iextended[0], i_32bit[0]);
        acc_r0 = vmlsl_high_s32(acc_r0, a0_v_iextended[0], i_32bit[0]);
        acc_r0 = vmlsl_low_s32(acc_r0, a0_v_iextended[1], i_32bit[1]);
        acc_r0 = vmlsl_high_s32(acc_r0, a0_v_iextended[1], i_32bit[1]);

        // Real * imag
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_rextended[0], i_32bit[0]);
        acc_i0 = vmlal_high_s32(acc_i0, a0_v_rextended[0], i_32bit[0]);
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_rextended[1], i_32bit[1]);
        acc_i0 = vmlal_high_s32(acc_i0, a0_v_rextended[1], i_32bit[1]);
        // Imag * real
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_iextended[0], r_32bit[0]);
        acc_i0 = vmlal_high_s32(acc_i0, a0_v_iextended[0], r_32bit[0]);
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_iextended[1], r_32bit[1]);
        acc_i0 = vmlal_high_s32(acc_i0, a0_v_iextended[1], r_32bit[1]);

        // Second row * column of B
        // Real * real
        acc_r1 = vmlal_low_s32(acc_r1, a1_v_rextended[0], r_32bit[0]);
        acc_r1 = vmlal_high_s32(acc_r1, a1_v_rextended[0], r_32bit[0]);
        acc_r1 = vmlal_low_s32(acc_r1, a1_v_rextended[1], r_32bit[1]);
        acc_r1 = vmlal_high_s32(acc_r1, a1_v_rextended[1], r_32bit[1]);
        // Imag * imag
        acc_r1 = vmlsl_low_s32(acc_r1, a1_v_iextended[0], i_32bit[0]);
        acc_r1 = vmlsl_high_s32(acc_r1, a1_v_iextended[0], i_32bit[0]);
        acc_r1 = vmlsl_low_s32(acc_r1, a1_v_iextended[1], i_32bit[1]);
        acc_r1 = vmlsl_high_s32(acc_r1, a1_v_iextended[1], i_32bit[1]);

        // Real * imag
        acc_i1 = vmlal_low_s32(acc_i1, a1_v_rextended[0], i_32bit[0]);
        acc_i1 = vmlal_high_s32(acc_i1, a1_v_rextended[0], i_32bit[0]);
        acc_i1 = vmlal_low_s32(acc_i1, a1_v_rextended[1], i_32bit[1]);
        acc_i1 = vmlal_high_s32(acc_i1, a1_v_rextended[1], i_32bit[1]);
        // Imag * real
        acc_i1 = vmlal_low_s32(acc_i1, a1_v_iextended[0], r_32bit[0]);
        acc_i1 = vmlal_high_s32(acc_i1, a1_v_iextended[0], r_32bit[0]);
        acc_i1 = vmlal_low_s32(acc_i1, a1_v_iextended[1], r_32bit[1]);
        acc_i1 = vmlal_high_s32(acc_i1, a1_v_iextended[1], r_32bit[1]);

        p_in1 += 16;
        p_in1_b += 16;
        p_in2 = p_in2 + 16 * num_cols_b;
        --col_cnt;
      }

      // If the remainder of columns in the row of A is greater than 4,
      // do an unrolled loop of size 4
      if ((num_cols_a & 4) != 0) {
        // Load four complex numbers from A. Split into real and imaginary parts
        int16x4x2_t a0_v = vld2_s16(p_in1);
        int16x4x2_t a1_v = vld2_s16(p_in1_b);

        int32x4_t a0_vextended[2] = {0};
        a0_vextended[0] = vmovl_s16(a0_v.val[0]);
        a0_vextended[1] = vmovl_s16(a0_v.val[1]);
        int32x4_t a1_vextended[2] = {0};
        a1_vextended[0] = vmovl_s16(a1_v.val[0]);
        a1_vextended[1] = vmovl_s16(a1_v.val[1]);

        int16x4x2_t tmp_b = {0};
        tmp_b = vld2_lane_s16(p_in2, tmp_b, 0);
        tmp_b = vld2_lane_s16(p_in2 + 2 * num_cols_b, tmp_b, 1);
        tmp_b = vld2_lane_s16(p_in2 + 4 * num_cols_b, tmp_b, 2);
        tmp_b = vld2_lane_s16(p_in2 + 6 * num_cols_b, tmp_b, 3);

        // Extend to 32-bit
        int32x4_t r_32bit = vmovl_s16(tmp_b.val[0]);
        int32x4_t i_32bit = vmovl_s16(tmp_b.val[1]);

        // First row of A
        // Real * real
        acc_r0 = vmlal_low_s32(acc_r0, a0_vextended[0], r_32bit);
        acc_r0 = vmlal_high_s32(acc_r0, a0_vextended[0], r_32bit);
        // Imag * imag
        acc_r0 = vmlsl_low_s32(acc_r0, a0_vextended[1], i_32bit);
        acc_r0 = vmlsl_high_s32(acc_r0, a0_vextended[1], i_32bit);
        // Real * imag
        acc_i0 = vmlal_low_s32(acc_i0, a0_vextended[0], i_32bit);
        acc_i0 = vmlal_high_s32(acc_i0, a0_vextended[0], i_32bit);
        // Imag * real
        acc_i0 = vmlal_low_s32(acc_i0, a0_vextended[1], r_32bit);
        acc_i0 = vmlal_high_s32(acc_i0, a0_vextended[1], r_32bit);

        // Second row of A
        // Real * real
        acc_r1 = vmlal_low_s32(acc_r1, a1_vextended[0], r_32bit);
        acc_r1 = vmlal_high_s32(acc_r1, a1_vextended[0], r_32bit);
        // Imag * imag
        acc_r1 = vmlsl_low_s32(acc_r1, a1_vextended[1], i_32bit);
        acc_r1 = vmlsl_high_s32(acc_r1, a1_vextended[1], i_32bit);
        // Real * imag
        acc_i1 = vmlal_low_s32(acc_i1, a1_vextended[0], i_32bit);
        acc_i1 = vmlal_high_s32(acc_i1, a1_vextended[0], i_32bit);
        // Imag * real
        acc_i1 = vmlal_low_s32(acc_i1, a1_vextended[1], r_32bit);
        acc_i1 = vmlal_high_s32(acc_i1, a1_vextended[1], r_32bit);

        p_in1 += 8;
        p_in1_b += 8;
        p_in2 += 8 * num_cols_b;
      }

      sum_real1_ext += vaddvq_s64(acc_r0);
      sum_imag1_ext += vaddvq_s64(acc_i0);
      sum_real1_b_ext += vaddvq_s64(acc_r1);
      sum_imag1_b_ext += vaddvq_s64(acc_i1);

      // If the columns of pSrcA is not a multiple of 4, compute any remaining
      // MACs here. No loop unrolling is used.
      col_cnt = num_cols_a & 3;

      while (col_cnt > 0U) {
        int16_t a_r1 = p_in1[0];
        int16_t a_i1 = p_in1[1];
        int16_t a_r2 = p_in1_b[0];
        int16_t a_i2 = p_in1_b[1];

        int16_t b_r = p_in2[0];
        int16_t b_i = p_in2[1];

        // Real * real
        sum_real1_ext += a_r1 * b_r;
        sum_real1_b_ext += a_r2 * b_r;
        // Imag * imag
        sum_real1_ext -= a_i1 * b_i;
        sum_real1_b_ext -= a_i2 * b_i;
        // Real * imag
        sum_imag1_ext += a_r1 * b_i;
        sum_imag1_b_ext += a_r2 * b_i;
        // Imag * real
        sum_imag1_ext += a_i1 * b_r;
        sum_imag1_b_ext += a_i2 * b_r;

        p_in1 += 2U;
        p_in1_b += 2U;
        p_in2 += 2 * num_cols_b;
        col_cnt--;
      }

      // sumReal1Ext += sumReal2Ext;
      armral_cmplx_int16_t out[2] = {};
      out[0].re = vqmovns_s32(vqshrnd_n_s64(sum_real1_ext, 15));
      out[0].im = vqmovns_s32(vqshrnd_n_s64(sum_imag1_ext, 15));
      out[1].re = vqmovns_s32(vqshrnd_n_s64(sum_real1_b_ext, 15));
      out[1].im = vqmovns_s32(vqshrnd_n_s64(sum_imag1_b_ext, 15));

      // Store the result in the destination buffer
      *(px++) = out[0];
      *(px_b++) = out[1];
    }
  }

  // Odd number of rows
  if ((num_rows_a & 1) != 0) {
    // Output pointer is set to starting address of the row being processed
    armral_cmplx_int16_t *px = p_out;

    // For every row-wise process, the column loop counter is initialized
    uint16_t col = num_cols_b;

    // For every row-wise process, the pIn2 pointer is set
    // to the starting address of the pSrcB data
    const int16_t *p_in2 = (const int16_t *)p_src_b;

    uint16_t j = 0U;

    // Column loop
    while (col > 0U) {
      // Set the variable sum, that acts as accumulator, to zero
      int64_t sum_real1_ext = 0;
      int64_t sum_imag1_ext = 0;

      int64_t sum_real2_ext = 0;
      int64_t sum_imag2_ext = 0;

      // Initialize the pointer pIn1 to point to the starting address of the row
      // being processed
      p_in1 = (const int16_t *)p_in_a;

      int64x2_t acc_r0 = vdupq_n_s64(0);
      int64x2_t acc_i0 = vdupq_n_s64(0);

      // Compute 8 MACs simultaneously
      uint16_t col_cnt = num_cols_a >> 3;

      // Matrix multiplication
      while (col_cnt > 0U) {
        // Load & separate real/imag pSrcA (de-interleave 2)
        int16x8x2_t a0_v = vld2q_s16(p_in1);
        // Extend to 32bit Real part int32x4x2_t
        int32x4_t a0_v_rextended[2];
        a0_v_rextended[0] = vmovl_low_s16(a0_v.val[0]);
        a0_v_rextended[1] = vmovl_high_s16(a0_v.val[0]);
        // Extend to 32bit Imag part int32x4x2_t
        int32x4_t a0_v_iextended[2];
        a0_v_iextended[0] = vmovl_low_s16(a0_v.val[1]);
        a0_v_iextended[1] = vmovl_high_s16(a0_v.val[1]);

        p_in1 += 16;

        int16x4_t temp_r0;
        int16x4_t temp_i0;
        temp_r0[0] = *p_in2;
        temp_i0[0] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        temp_r0[1] = *p_in2;
        temp_i0[1] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        temp_r0[2] = *p_in2;
        temp_i0[2] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        temp_r0[3] = *p_in2;
        temp_i0[3] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        int16x4_t temp_r1;
        int16x4_t temp_i1;
        temp_r1[0] = *p_in2;
        temp_i1[0] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        temp_r1[1] = *p_in2;
        temp_i1[1] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        temp_r1[2] = *p_in2;
        temp_i1[2] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        temp_r1[3] = *p_in2;
        temp_i1[3] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        int32x4_t temp_r0extended = vmovl_s16(temp_r0); // 32x4
        int32x4_t temp_i0extended = vmovl_s16(temp_i0); // 32x4
        int32x4_t temp_r1extended = vmovl_s16(temp_r1); // 32x4
        int32x4_t temp_i1extended = vmovl_s16(temp_i1); // 32x4

        acc_r0 = vmlal_high_s32(acc_r0, a0_v_rextended[0], temp_r0extended);
        acc_r0 = vmlal_low_s32(acc_r0, a0_v_rextended[0], temp_r0extended);

        acc_r0 = vmlal_high_s32(acc_r0, a0_v_rextended[1], temp_r1extended);
        acc_r0 = vmlal_low_s32(acc_r0, a0_v_rextended[1], temp_r1extended);

        acc_r0 = vmlsl_high_s32(acc_r0, a0_v_iextended[0], temp_i0extended);
        acc_r0 = vmlsl_low_s32(acc_r0, a0_v_iextended[0], temp_i0extended);

        acc_r0 = vmlsl_high_s32(acc_r0, a0_v_iextended[1], temp_i1extended);
        acc_r0 = vmlsl_low_s32(acc_r0, a0_v_iextended[1], temp_i1extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_v_iextended[0], temp_r0extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_iextended[0], temp_r0extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_v_iextended[1], temp_r1extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_iextended[1], temp_r1extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_v_rextended[0], temp_i0extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_rextended[0], temp_i0extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_v_rextended[1], temp_i1extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_v_rextended[1], temp_i1extended);

        col_cnt--;
      }

      // Compute 4 MACs simultaneously
      col_cnt = (num_cols_a & 7) >> 2;

      // Matrix multiplication
      while (col_cnt > 0U) {
        int16x4x2_t a2_v = vld2_s16(p_in1); // int16x4x2_t
        int32x4x2_t a0_vextended;
        a0_vextended.val[0] = vmovl_s16(a2_v.val[0]);
        a0_vextended.val[1] = vmovl_s16(a2_v.val[1]);
        p_in1 += 8;

        int16x4_t temp_r0;
        int16x4_t temp_i0;
        temp_r0[0] = *p_in2;
        temp_i0[0] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        temp_r0[1] = *p_in2;
        temp_i0[1] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        temp_r0[2] = *p_in2;
        temp_i0[2] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        temp_r0[3] = *p_in2;
        temp_i0[3] = *(p_in2 + 1U);
        p_in2 += 2 * num_cols_b;

        int32x4_t temp_r0extended = vmovl_s16(temp_r0);
        int32x4_t temp_i0extended = vmovl_s16(temp_i0);

        acc_r0 = vmlal_high_s32(acc_r0, a0_vextended.val[0], temp_r0extended);
        acc_r0 = vmlal_low_s32(acc_r0, a0_vextended.val[0], temp_r0extended);

        acc_r0 = vmlsl_high_s32(acc_r0, a0_vextended.val[1], temp_i0extended);
        acc_r0 = vmlsl_low_s32(acc_r0, a0_vextended.val[1], temp_i0extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_vextended.val[1], temp_r0extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_vextended.val[1], temp_r0extended);

        acc_i0 = vmlal_high_s32(acc_i0, a0_vextended.val[0], temp_i0extended);
        acc_i0 = vmlal_low_s32(acc_i0, a0_vextended.val[0], temp_i0extended);

        col_cnt--;
      }

      sum_real1_ext += acc_r0[0] + acc_r0[1];
      sum_imag1_ext += acc_i0[0] + acc_i0[1];

      // If the columns of pSrcA is not a multiple of 4, compute any remaining
      // MACs here. No loop unrolling is used.
      col_cnt = num_cols_a & 3;

      while (col_cnt > 0U) {
        int16_t a1 = (*p_in1);
        int16_t c1 = (*p_in2);

        int16_t b1 = (*(p_in1 + 1U));
        int16_t d1 = (*(p_in2 + 1U));

        sum_real1_ext += a1 * c1;
        sum_imag1_ext += b1 * c1;

        p_in1 += 2U;
        p_in2 += 2 * num_cols_b;

        sum_real2_ext -= b1 * d1;
        sum_imag2_ext += a1 * d1;

        col_cnt--;
      }

      sum_real1_ext += sum_real2_ext;
      int32_t sum_real1_ext32 = vqshrnd_n_s64(sum_real1_ext, 15);
      int16_t sum_real1_q15 = vqmovns_s32(sum_real1_ext32);
      sum_imag1_ext += sum_imag2_ext;
      int32_t sum_imag1_ext32 = vqshrnd_n_s64(sum_imag1_ext, 15);
      int16_t sum_imag1_q15 = vqmovns_s32(sum_imag1_ext32);

      // Store the result in the destination buffer
      px->re = sum_real1_q15;
      px->im = sum_imag1_q15;
      px++;
      // Update the pointer pIn2 to point to the starting address of the next
      // column
      j++;
      p_in2 = (const int16_t *)p_src_b + 2U * j;

      col--;
    }
  }
#endif
  return ARMRAL_SUCCESS;
}
} // anonymous namespace
