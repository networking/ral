/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#if ARMRAL_ARCH_SVE >= 2
#include <arm_sve.h>
#endif

namespace {

typedef struct {
  int32_t re; ///< 32-bit real component.
  int32_t im; ///< 32-bit imaginary component.
} cmplx_int32_t;

#if ARMRAL_ARCH_SVE >= 2
template<uint16_t rows_a>
inline void
cmplx_matmul_i16_32bit_ixkxj_sve(svbool_t pb, const uint16_t m,
                                 const uint16_t n, const uint16_t k,
                                 const armral_cmplx_int16_t *__restrict p_src_a,
                                 const armral_cmplx_int16_t *__restrict p_src_b,
                                 armral_cmplx_int16_t *p_dst) {

  // Only valid for these values of rows_a
  static_assert(rows_a == 1 || rows_a == 2);

  // clang-format off
  svint16_t a0i_0_3 = svdup_s16(0); svint16_t a1i_0_3 = svdup_s16(0);

  svint16_t b0i = svdup_s16(0); svint16_t b1i = svdup_s16(0);
  svint16_t b2i = svdup_s16(0); svint16_t b3i = svdup_s16(0);

  svint32_t c0_re = svdup_s32(0); svint32_t c0_im = svdup_s32(0);
  svint32_t c1_re = svdup_s32(0); svint32_t c1_im = svdup_s32(0);
  // clang-format on

  for (uint16_t h = 0; h < k;) {
    svbool_t pa = svwhilelt_b16(2 * h, 2 * k);

    a0i_0_3 = svld1_s16(pa, (const int16_t *)&p_src_a[h]);

    if constexpr (rows_a > 1) {
      a1i_0_3 = svld1_s16(pa, (const int16_t *)&p_src_a[h + k]);
    }

    b0i = svld1_s16(pb, (const int16_t *)&p_src_b[h * n]);
    if (h < k - 1) {
      b1i = svld1_s16(pb, (const int16_t *)&p_src_b[(h + 1) * n]);
      if (h < k - 3) {
        b2i = svld1_s16(pb, (const int16_t *)&p_src_b[(h + 2) * n]);
        b3i = svld1_s16(pb, (const int16_t *)&p_src_b[(h + 3) * n]);
      }
    }
    c0_re = svqdmlalb_lane_s32(c0_re, b0i, a0i_0_3, 0);
    c0_re = svqdmlslt_lane_s32(c0_re, b0i, a0i_0_3, 1);
    c0_im = svqdmlalt_lane_s32(c0_im, b0i, a0i_0_3, 0);
    c0_im = svqdmlalb_lane_s32(c0_im, b0i, a0i_0_3, 1);
    if (h < k - 1) {
      c0_re = svqdmlalb_lane_s32(c0_re, b1i, a0i_0_3, 2);
      c0_re = svqdmlslt_lane_s32(c0_re, b1i, a0i_0_3, 3);
      c0_im = svqdmlalt_lane_s32(c0_im, b1i, a0i_0_3, 2);
      c0_im = svqdmlalb_lane_s32(c0_im, b1i, a0i_0_3, 3);
      if (h < k - 3) {
        c0_re = svqdmlalb_lane_s32(c0_re, b2i, a0i_0_3, 4);
        c0_re = svqdmlslt_lane_s32(c0_re, b2i, a0i_0_3, 5);
        c0_im = svqdmlalt_lane_s32(c0_im, b2i, a0i_0_3, 4);
        c0_im = svqdmlalb_lane_s32(c0_im, b2i, a0i_0_3, 5);
        c0_re = svqdmlalb_lane_s32(c0_re, b3i, a0i_0_3, 6);
        c0_re = svqdmlslt_lane_s32(c0_re, b3i, a0i_0_3, 7);
        c0_im = svqdmlalt_lane_s32(c0_im, b3i, a0i_0_3, 6);
        c0_im = svqdmlalb_lane_s32(c0_im, b3i, a0i_0_3, 7);
      }
    }
    if constexpr (rows_a > 1) {
      c1_re = svqdmlalb_lane_s32(c1_re, b0i, a1i_0_3, 0);
      c1_re = svqdmlslt_lane_s32(c1_re, b0i, a1i_0_3, 1);
      c1_im = svqdmlalt_lane_s32(c1_im, b0i, a1i_0_3, 0);
      c1_im = svqdmlalb_lane_s32(c1_im, b0i, a1i_0_3, 1);
      if (h < k - 1) {
        c1_re = svqdmlalb_lane_s32(c1_re, b1i, a1i_0_3, 2);
        c1_re = svqdmlslt_lane_s32(c1_re, b1i, a1i_0_3, 3);
        c1_im = svqdmlalt_lane_s32(c1_im, b1i, a1i_0_3, 2);
        c1_im = svqdmlalb_lane_s32(c1_im, b1i, a1i_0_3, 3);
        if (h < k - 3) {
          c1_re = svqdmlalb_lane_s32(c1_re, b2i, a1i_0_3, 4);
          c1_re = svqdmlslt_lane_s32(c1_re, b2i, a1i_0_3, 5);
          c1_im = svqdmlalt_lane_s32(c1_im, b2i, a1i_0_3, 4);
          c1_im = svqdmlalb_lane_s32(c1_im, b2i, a1i_0_3, 5);
          c1_re = svqdmlalb_lane_s32(c1_re, b3i, a1i_0_3, 6);
          c1_re = svqdmlslt_lane_s32(c1_re, b3i, a1i_0_3, 7);
          c1_im = svqdmlalt_lane_s32(c1_im, b3i, a1i_0_3, 6);
          c1_im = svqdmlalb_lane_s32(c1_im, b3i, a1i_0_3, 7);
        }
      }
    }
    if (h < k - 1) {
      if (h < k - 3) {
        h += 4;
      } else {
        h += 2;
      }
    } else {
      h += 1;
    }
  }

  // saturate and right shift
  svint16_t res_r0 = svshrnt(svshrnb(c0_re, 16), c0_im, 16);
  // store the result in dst
  svst1_s16(pb, (int16_t *)&p_dst[0], res_r0);

  // one more row
  if constexpr (rows_a > 1) {
    svint16_t res_r1 = svshrnt(svshrnb(c1_re, 16), c1_im, 16);
    svst1_s16(pb, (int16_t *)&p_dst[n], res_r1);
  }
}
#endif

// nb must be even
template<uint16_t mb, uint16_t nb>
void cmplx_matmul_i16_32bit_mbxkxnb_even(
    const uint16_t k, const armral_cmplx_int16_t *const __restrict a,
    const uint16_t lda, const armral_cmplx_int16_t *const __restrict b,
    armral_cmplx_int16_t *const dst, const uint16_t ldb) {

  const int16_t *a_int16 = (const int16_t *)a;
  const int16_t *b_int16 = (const int16_t *)b;

  // We will be vectorizing in nblocks of 4, using int??x4_ts. Each vector will
  // therefore hold two complex numbers, so we will need nb/2 vectors (per m)
  constexpr uint16_t nb_vecs = nb / 2;

  int32x4_t buff[mb][nb_vecs] = {};
  int16x8_t a_rows[mb];
  int16x4x2_t a_val[mb];
  int16x4_t b_row_nrm[nb_vecs];
  int16x4_t b_row_rev[nb_vecs];

  constexpr uint16_t kb = 4; // load 4 complex A vals at a time
  uint16_t kbi = 0;
  for (; kbi < k - kb + 1; kbi += kb) {

    for (uint16_t mi = 0; mi < mb; ++mi) {
      a_rows[mi] = vld1q_s16(a_int16 + 2 * mi * lda + 2 * kbi);
    }

    for (uint16_t ki = 0; ki < kb; ++ki) {

      // a_val holds the single complex value of A we will be multiplying the
      // row of B by.
      // .val[0] is real part, .val[1] the imag part (with alternating sign)
      for (uint16_t mi = 0; mi < mb; ++mi) {
        a_val[mi].val[0] = vdup_n_s16(a_rows[mi][2 * ki]);
        a_val[mi].val[1] = vdup_n_s16(a_rows[mi][2 * ki + 1]);
        a_val[mi].val[1] =
            vzip1_s16(vneg_s16(a_val[mi].val[1]), a_val[mi].val[1]);
      }

      // b_row_nrm is 2 complex numbers from the row of B
      // b_row_rev is the same, but with each num's real and imag part switched.
      for (uint16_t nvi = 0; nvi < nb_vecs; ++nvi) {
        b_row_nrm[nvi] = vld1_s16(b_int16 + 2 * (kbi + ki) * ldb + 4 * nvi);
        b_row_rev[nvi] = vrev32_s16(b_row_nrm[nvi]);
      }

      // Do the multiplication.
      // eg. first two cmplx numbers of C:
      //  C_r0 | C_i0 | C_r1 | C_i0 = buff
      // ======|======|======|=====
      //  A_r0 | A_r0 | A_r0 | A_r0 = a_val.val[0]
      //   *   |  *   |  *   |  *
      //  B_r0 | B_i0 | B_r1 | B_i1 = b_row_nrm
      //   +   |  +   |  +   |  +
      // -A_i0 | A_i0 |-A_i0 | A_i0 = a_val.val[1]
      //   *   |  *   |  *   |  *
      //  B_i0 | B_r0 | B_i1 | B_r1 = b_row_rev
      for (uint16_t mi = 0; mi < mb; ++mi) {
        for (uint16_t nvi = 0; nvi < nb_vecs; ++nvi) {
          buff[mi][nvi] =
              vqdmlal_s16(buff[mi][nvi], a_val[mi].val[0], b_row_nrm[nvi]);
          buff[mi][nvi] =
              vqdmlal_s16(buff[mi][nvi], a_val[mi].val[1], b_row_rev[nvi]);
        }
      }
    }
  }
  for (; kbi < k; kbi++) {

    for (uint16_t mi = 0; mi < mb; ++mi) {
      a_val[mi] = vld2_dup_s16(a_int16 + 2 * kbi + 2 * mi * lda);
      a_val[mi].val[1] =
          vzip1_s16(vneg_s16(a_val[mi].val[1]), a_val[mi].val[1]);
    }

    for (uint16_t nvi = 0; nvi < nb_vecs; ++nvi) {
      b_row_nrm[nvi] = vld1_s16(b_int16 + 2 * kbi * ldb + 4 * nvi);
      b_row_rev[nvi] = vrev32_s16(b_row_nrm[nvi]);
    }

    for (uint16_t mi = 0; mi < mb; ++mi) {
      for (uint16_t nvi = 0; nvi < nb_vecs; ++nvi) {
        buff[mi][nvi] =
            vqdmlal_s16(buff[mi][nvi], a_val[mi].val[0], b_row_nrm[nvi]);
        buff[mi][nvi] =
            vqdmlal_s16(buff[mi][nvi], a_val[mi].val[1], b_row_rev[nvi]);
      }
    }
  }

  // Write to dst
  for (uint16_t mi = 0; mi < mb; ++mi) {
    for (uint16_t nvi = 0; nvi < nb_vecs; ++nvi) {
      int16x4_t out = vqshrn_n_s32(buff[mi][nvi], 16);
      vst1_s16(((int16_t *)dst) + mi * 2 * ldb + 4 * nvi, out);
    }
  }
}

template<uint16_t mb>
inline __attribute__((always_inline)) void cmplx_matmul_i16_32bit_mbxkx1(
    const uint16_t k, const armral_cmplx_int16_t *const __restrict a,
    const uint16_t lda, const armral_cmplx_int16_t *const __restrict b,
    armral_cmplx_int16_t *const dst, const uint16_t ldb) {
  // Just do a naive implementation for the mbxkx1 case. This is a simple loop,
  // and we will trust the compiler to not do something horrible with it.
  cmplx_int32_t accum[mb] = {};
  for (uint16_t mi = 0; mi < mb; ++mi) {
    for (uint16_t i = 0; i < k; ++i) {
      armral_cmplx_int16_t a_tmp = a[mi * lda + i];
      armral_cmplx_int16_t b_tmp = b[i * ldb];
      accum[mi].re = vqdmlalh_s16(accum[mi].re, a_tmp.re, b_tmp.re);
      accum[mi].re = vqdmlslh_s16(accum[mi].re, a_tmp.im, b_tmp.im);
      accum[mi].im = vqdmlalh_s16(accum[mi].im, a_tmp.re, b_tmp.im);
      accum[mi].im = vqdmlalh_s16(accum[mi].im, a_tmp.im, b_tmp.re);
    }
  }

  for (uint16_t mi = 0; mi < mb; ++mi) {
    // Narrow to 16 bits.
    dst[mi * ldb].re = vqshrns_n_s32(accum[mi].re, 16);
    dst[mi * ldb].im = vqshrns_n_s32(accum[mi].im, 16);
  }
}

template<uint16_t mb>
void cmplx_matmul_i16_32bit_mbxkxn(
    const uint16_t n, const uint16_t k,
    const armral_cmplx_int16_t *const __restrict p_src_a, const uint16_t lda,
    const armral_cmplx_int16_t *const __restrict p_src_b,
    armral_cmplx_int16_t *const dst, const uint16_t ldb) {
  const armral_cmplx_int16_t *a_ptr = p_src_a;
  armral_cmplx_int16_t *dst_ptr = dst;
  const armral_cmplx_int16_t *b_ptr = p_src_b;

  for (uint16_t b_col_cnt = n >> 3; b_col_cnt > 0;
       --b_col_cnt, b_ptr += 8, dst_ptr += 8) {
    cmplx_matmul_i16_32bit_mbxkxnb_even<mb, 8>(k, a_ptr, lda, b_ptr, dst_ptr,
                                               ldb);
  }
  if ((n & 4) != 0) {
    cmplx_matmul_i16_32bit_mbxkxnb_even<mb, 4>(k, a_ptr, lda, b_ptr, dst_ptr,
                                               ldb);
    b_ptr += 4;
    dst_ptr += 4;
  }
  if ((n & 2) != 0) {
    cmplx_matmul_i16_32bit_mbxkxnb_even<mb, 2>(k, a_ptr, lda, b_ptr, dst_ptr,
                                               ldb);
    b_ptr += 2;
    dst_ptr += 2;
  }
  if ((n & 1) != 0) {
    cmplx_matmul_i16_32bit_mbxkx1<mb>(k, a_ptr, lda, b_ptr, dst_ptr, ldb);
  }
}

template<uint16_t mb>
armral_status cmplx_matmul_i16_32bit_block_m(
    const uint16_t m, const uint16_t n, const uint16_t k,
    const armral_cmplx_int16_t *const __restrict p_src_a, const uint16_t lda,
    const armral_cmplx_int16_t *const __restrict p_src_b,
    armral_cmplx_int16_t *const dst, const uint16_t ldb) {

  uint16_t mbi = 0;

  for (; mbi < m - mb + 1; mbi += mb) {
    cmplx_matmul_i16_32bit_mbxkxn<mb>(n, k, p_src_a + mbi * lda, lda, p_src_b,
                                      dst + mbi * ldb, ldb);
  }
  for (; mbi < m; mbi++) { // remainder
    cmplx_matmul_i16_32bit_mbxkxn<1>(n, k, p_src_a + mbi * lda, lda, p_src_b,
                                     dst + mbi * ldb, ldb);
  }
  return ARMRAL_SUCCESS;
}

armral_status
cmplx_matmul_i16_32bit(const uint16_t m, const uint16_t n, const uint16_t k,
                       const armral_cmplx_int16_t *__restrict p_src_a,
                       const armral_cmplx_int16_t *__restrict p_src_b,
                       armral_cmplx_int16_t *p_dst) {
#if ARMRAL_ARCH_SVE >= 2
  if (m > 2 && n > 2) {
    svbool_t pb;
    uint16_t i = 0;
    uint16_t j = 0;
    for (; i < (m - 1); i += 2) {
      j = 0;
      pb = svptrue_pat_b16(SV_VL8);
      for (; j < (n - 3); j += 4) {
        cmplx_matmul_i16_32bit_ixkxj_sve<2>(pb, m, n, k, &p_src_a[i * k],
                                            &p_src_b[j], &p_dst[i * n + j]);
      }

      if (j < (n - 1)) {
        cmplx_matmul_i16_32bit_ixkxj_sve<2>(svptrue_pat_b16(SV_VL4), m, n, k,
                                            &p_src_a[i * k], &p_src_b[j],
                                            &p_dst[i * n + j]);
        j += 2;
      }
      // If n is odd, we have one more row/col to go
      if (n % 2 != 0) {
        j = n - 1;
        cmplx_matmul_i16_32bit_ixkxj_sve<2>(svptrue_pat_b16(SV_VL2), m, n, k,
                                            &p_src_a[i * k], &p_src_b[j],
                                            &p_dst[i * n + j]);
      }
    }

    if (m % 2 != 0) {
      i = m - 1;
      j = 0;
      pb = svptrue_pat_b16(SV_VL8);
      for (; j < (n - 3); j += 4) {
        cmplx_matmul_i16_32bit_ixkxj_sve<1>(pb, m, n, k, &p_src_a[i * k],
                                            &p_src_b[j], &p_dst[i * n + j]);
      }
      if (j < (n - 1)) {
        cmplx_matmul_i16_32bit_ixkxj_sve<1>(svptrue_pat_b16(SV_VL4), m, n, k,
                                            &p_src_a[i * k], &p_src_b[j],
                                            &p_dst[i * n + j]);
        j += 2;
      }
      // If n is odd, we have one more row/col to go
      if (n % 2 != 0) {
        j = n - 1;
        cmplx_matmul_i16_32bit_ixkxj_sve<1>(svptrue_pat_b16(SV_VL2), m, n, k,
                                            &p_src_a[i * k], &p_src_b[j],
                                            &p_dst[i * n + j]);
      }
    }
    return ARMRAL_SUCCESS;
  }
#endif
  return cmplx_matmul_i16_32bit_block_m<2>(m, n, k, p_src_a, k, p_src_b, p_dst,
                                           n);
}

} // anonymous namespace
