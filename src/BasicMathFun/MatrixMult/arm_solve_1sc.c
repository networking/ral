/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "arm_solve_1sc.h"
#include "arm_solve_convert.h"
#include "intrinsics.h"

#include <complex.h>

armral_status armral_solve_2x2_1sc_f32(
    const uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    const uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    const uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    const uint32_t p_xstride, const armral_fixed_point_index num_fract_bits_x) {

  float32_t x_mult = 1 << num_fract_bits_x;

  int16_t *p_x_int16 = (int16_t *)p_x;
  const int16_t *p_y_int16 = (const int16_t *)p_y;

#if ARMRAL_ARCH_SVE >= 2
  float32_t y_shifts[2] = {1 << p_y_num_fract_bits[0],
                           1 << p_y_num_fract_bits[1]};
  svbool_t pg32;

  for (uint32_t i = 0;
       svptest_first(svptrue_b32(), pg32 = svwhilelt_b32(i, num_sub_carrier));
       i += svcntw()) {
    svfloat32_t c0_elem_re = svld1_f32(pg32, &p_g_real[0 * p_gstride]);
    svfloat32_t c0_elem_im = svld1_f32(pg32, &p_g_imag[0 * p_gstride]);
    svint16_t y_int = svreinterpret_s16_s32(
        svld1_s32(pg32, (const int32_t *)&p_y_int16[0 * p_ystride]));
    svint32_t y_int_re = svmovlb_s32(y_int);
    svint32_t y_int_im = svmovlt_s32(y_int);
    svfloat32_t y_elem_re =
        svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_re), 1 / y_shifts[0]);
    svfloat32_t y_elem_im =
        svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_im), 1 / y_shifts[0]);
    svfloat32_t acc0_re = svmul_f32_x(pg32, y_elem_re, c0_elem_re);
    acc0_re = svmls_f32_x(pg32, acc0_re, y_elem_im, c0_elem_im);
    svfloat32_t acc0_im = svmul_f32_x(pg32, y_elem_re, c0_elem_im);
    acc0_im = svmla_f32_x(pg32, acc0_im, y_elem_im, c0_elem_re);

    svfloat32_t c1_elem_re = svld1_f32(pg32, &p_g_real[2 * p_gstride]);
    svfloat32_t c1_elem_im = svld1_f32(pg32, &p_g_imag[2 * p_gstride]);
    svfloat32_t acc1_re = svmul_f32_x(pg32, y_elem_re, c1_elem_re);
    acc1_re = svmls_f32_x(pg32, acc1_re, y_elem_im, c1_elem_im);
    svfloat32_t acc1_im = svmul_f32_x(pg32, y_elem_re, c1_elem_im);
    acc1_im = svmla_f32_x(pg32, acc1_im, y_elem_im, c1_elem_re);

    c0_elem_re = svld1_f32(pg32, &p_g_real[1 * p_gstride]);
    c0_elem_im = svld1_f32(pg32, &p_g_imag[1 * p_gstride]);
    y_int = svreinterpret_s16_s32(
        svld1_s32(pg32, (const int32_t *)&p_y_int16[1 * p_ystride]));
    y_int_re = svmovlb_s32(y_int);
    y_int_im = svmovlt_s32(y_int);
    y_elem_re =
        svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_re), 1 / y_shifts[1]);
    y_elem_im =
        svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_im), 1 / y_shifts[1]);
    acc0_re = svmla_f32_x(pg32, acc0_re, y_elem_re, c0_elem_re);
    acc0_re = svmls_f32_x(pg32, acc0_re, y_elem_im, c0_elem_im);
    acc0_im = svmla_f32_x(pg32, acc0_im, y_elem_re, c0_elem_im);
    acc0_im = svmla_f32_x(pg32, acc0_im, y_elem_im, c0_elem_re);

    c1_elem_re = svld1_f32(pg32, &p_g_real[3 * p_gstride]);
    c1_elem_im = svld1_f32(pg32, &p_g_imag[3 * p_gstride]);
    acc1_re = svmla_f32_x(pg32, acc1_re, y_elem_re, c1_elem_re);
    acc1_re = svmls_f32_x(pg32, acc1_re, y_elem_im, c1_elem_im);
    acc1_im = svmla_f32_x(pg32, acc1_im, y_elem_re, c1_elem_im);
    acc1_im = svmla_f32_x(pg32, acc1_im, y_elem_im, c1_elem_re);
    svint32_t res0_re =
        svcvt_s32_f32_x(pg32, svmul_n_f32_x(pg32, acc0_re, x_mult));
    svint32_t res0_im =
        svcvt_s32_f32_x(pg32, svmul_n_f32_x(pg32, acc0_im, x_mult));
    svint16_t res0_16_re = svqxtnb_s32(res0_re);
    svint16_t res0_16 = svqxtnt_s32(res0_16_re, res0_im);
    svst1_s32(pg32, (int32_t *)&p_x_int16[0 * p_xstride],
              svreinterpret_s32_s16(res0_16));

    svint32_t res1_re =
        svcvt_s32_f32_x(pg32, svmul_n_f32_x(pg32, acc1_re, x_mult));
    svint32_t res1_im =
        svcvt_s32_f32_x(pg32, svmul_n_f32_x(pg32, acc1_im, x_mult));
    svint16_t res1_16_re = svqxtnb_s32(res1_re);
    svint16_t res1_16 = svqxtnt_s32(res1_16_re, res1_im);
    svst1_s32(pg32, (int32_t *)&p_x_int16[1 * p_xstride],
              svreinterpret_s32_s16(res1_16));

    p_g_real += svcntw();
    p_g_imag += svcntw();
    p_y_int16 += svcnth();
    p_x_int16 += svcnth();
  }
#else
  int32x4_t y_shifts[2] = {vdupq_n_s32(-p_y_num_fract_bits[0]),
                           vdupq_n_s32(-p_y_num_fract_bits[1])};

  for (unsigned j = 0; j < num_sub_carrier; j += 4) {
    float32x4_t y_elem_re[2];
    float32x4_t y_elem_im[2];
    for (int k = 0; k < 2; ++k) {
      int16x8_t y_int = vld1q_s16(&p_y_int16[k * p_ystride]);
      int32x4_t y_int_re =
          vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), y_int));
      int32x4_t y_int_im =
          vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), y_int));
      y_elem_re[k] = vcvtq_n_f32_s32(vshlq_s32(y_int_re, y_shifts[k]), 16);
      y_elem_im[k] = vcvtq_n_f32_s32(vshlq_s32(y_int_im, y_shifts[k]), 16);
    }

    float32x4_t c0_elem_re = vld1q_f32(&p_g_real[0 * p_gstride]);
    float32x4_t c0_elem_im = vld1q_f32(&p_g_imag[0 * p_gstride]);
    float32x4_t acc0_re = vmulq_f32(y_elem_re[0], c0_elem_re);
    acc0_re = vfmsq_f32(acc0_re, y_elem_im[0], c0_elem_im);
    float32x4_t acc0_im = vmulq_f32(y_elem_re[0], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_im[0], c0_elem_re);

    float32x4_t c1_elem_re = vld1q_f32(&p_g_real[2 * p_gstride]);
    float32x4_t c1_elem_im = vld1q_f32(&p_g_imag[2 * p_gstride]);
    float32x4_t acc1_re = vmulq_f32(y_elem_re[0], c1_elem_re);
    acc1_re = vfmsq_f32(acc1_re, y_elem_im[0], c1_elem_im);
    float32x4_t acc1_im = vmulq_f32(y_elem_re[0], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_im[0], c1_elem_re);

    c0_elem_re = vld1q_f32(&p_g_real[1 * p_gstride]);
    c0_elem_im = vld1q_f32(&p_g_imag[1 * p_gstride]);
    acc0_re = vfmaq_f32(acc0_re, y_elem_re[1], c0_elem_re);
    acc0_re = vfmsq_f32(acc0_re, y_elem_im[1], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_re[1], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_im[1], c0_elem_re);

    c1_elem_re = vld1q_f32(&p_g_real[3 * p_gstride]);
    c1_elem_im = vld1q_f32(&p_g_imag[3 * p_gstride]);
    acc1_re = vfmaq_f32(acc1_re, y_elem_re[1], c1_elem_re);
    acc1_re = vfmsq_f32(acc1_re, y_elem_im[1], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_re[1], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_im[1], c1_elem_re);

    int32x4_t res0_re = vcvtq_s32_f32(acc0_re * x_mult);
    int32x4_t res0_im = vcvtq_s32_f32(acc0_im * x_mult);
    int16x4_t res0_16_re = vqmovn_s32(res0_re);
    int16x4_t res0_16_im = vqmovn_s32(res0_im);
    int16x8_t res0_16 = shuf_s16_04152637(res0_16_re, res0_16_im);
    vst1q_s16(&p_x_int16[0 * p_xstride], res0_16);

    int32x4_t res1_re = vcvtq_s32_f32(acc1_re * x_mult);
    int32x4_t res1_im = vcvtq_s32_f32(acc1_im * x_mult);
    int16x4_t res1_16_re = vqmovn_s32(res1_re);
    int16x4_t res1_16_im = vqmovn_s32(res1_im);
    int16x8_t res1_16 = shuf_s16_04152637(res1_16_re, res1_16_im);
    vst1q_s16(&p_x_int16[1 * p_xstride], res1_16);

    p_g_real += 4;
    p_g_imag += 4;
    p_y_int16 += 8;
    p_x_int16 += 8;
  }
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_2x4_1sc_f32(
    const uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    const uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    const uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    const uint32_t p_xstride, const armral_fixed_point_index num_fract_bits_x) {

  float32_t x_mult = 1 << num_fract_bits_x;

  int16_t *p_x_int16 = (int16_t *)p_x;
  const int16_t *p_y_int16 = (const int16_t *)p_y;

#if ARMRAL_ARCH_SVE >= 2
  float32_t y_shifts[4] = {
      1 << p_y_num_fract_bits[0], 1 << p_y_num_fract_bits[1],
      1 << p_y_num_fract_bits[2], 1 << p_y_num_fract_bits[3]};
  svbool_t pg32;

  for (uint32_t i = 0;
       svptest_first(svptrue_b32(), pg32 = svwhilelt_b32(i, num_sub_carrier));
       i += svcntw()) {
    svfloat32_t c0_elem_re = svld1_f32(pg32, &p_g_real[0 * p_gstride]);
    svfloat32_t c0_elem_im = svld1_f32(pg32, &p_g_imag[0 * p_gstride]);
    svint16_t y_int = svreinterpret_s16_s32(
        svld1_s32(pg32, (const int32_t *)&p_y_int16[0 * p_ystride]));
    svint32_t y_int_re = svmovlb_s32(y_int);
    svint32_t y_int_im = svmovlt_s32(y_int);
    svfloat32_t y_elem_re =
        svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_re), 1 / y_shifts[0]);
    svfloat32_t y_elem_im =
        svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_im), 1 / y_shifts[0]);
    svfloat32_t acc0_re = svmul_f32_x(pg32, y_elem_re, c0_elem_re);
    acc0_re = svmls_f32_x(pg32, acc0_re, y_elem_im, c0_elem_im);
    svfloat32_t acc0_im = svmul_f32_x(pg32, y_elem_re, c0_elem_im);
    acc0_im = svmla_f32_x(pg32, acc0_im, y_elem_im, c0_elem_re);

    svfloat32_t c1_elem_re = svld1_f32(pg32, &p_g_real[4 * p_gstride]);
    svfloat32_t c1_elem_im = svld1_f32(pg32, &p_g_imag[4 * p_gstride]);
    svfloat32_t acc1_re = svmul_f32_x(pg32, y_elem_re, c1_elem_re);
    acc1_re = svmls_f32_x(pg32, acc1_re, y_elem_im, c1_elem_im);
    svfloat32_t acc1_im = svmul_f32_x(pg32, y_elem_re, c1_elem_im);
    acc1_im = svmla_f32_x(pg32, acc1_im, y_elem_im, c1_elem_re);

    for (uint32_t j = 1; j < 4; ++j) {
      c0_elem_re = svld1_f32(pg32, &p_g_real[j * p_gstride]);
      c0_elem_im = svld1_f32(pg32, &p_g_imag[j * p_gstride]);
      y_int = svreinterpret_s16_s32(
          svld1_s32(pg32, (const int32_t *)&p_y_int16[j * p_ystride]));
      y_int_re = svmovlb_s32(y_int);
      y_int_im = svmovlt_s32(y_int);
      y_elem_re =
          svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_re), 1 / y_shifts[j]);
      y_elem_im =
          svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_im), 1 / y_shifts[j]);
      acc0_re = svmla_f32_x(pg32, acc0_re, y_elem_re, c0_elem_re);
      acc0_re = svmls_f32_x(pg32, acc0_re, y_elem_im, c0_elem_im);
      acc0_im = svmla_f32_x(pg32, acc0_im, y_elem_re, c0_elem_im);
      acc0_im = svmla_f32_x(pg32, acc0_im, y_elem_im, c0_elem_re);

      c1_elem_re = svld1_f32(pg32, &p_g_real[(j + 4) * p_gstride]);
      c1_elem_im = svld1_f32(pg32, &p_g_imag[(j + 4) * p_gstride]);
      acc1_re = svmla_f32_x(pg32, acc1_re, y_elem_re, c1_elem_re);
      acc1_re = svmls_f32_x(pg32, acc1_re, y_elem_im, c1_elem_im);
      acc1_im = svmla_f32_x(pg32, acc1_im, y_elem_re, c1_elem_im);
      acc1_im = svmla_f32_x(pg32, acc1_im, y_elem_im, c1_elem_re);
    }

    svint32_t res0_re =
        svcvt_s32_f32_x(pg32, svmul_n_f32_x(pg32, acc0_re, x_mult));
    svint32_t res0_im =
        svcvt_s32_f32_x(pg32, svmul_n_f32_x(pg32, acc0_im, x_mult));
    svint16_t res0_16_re = svqxtnb_s32(res0_re);
    svint16_t res0_16 = svqxtnt_s32(res0_16_re, res0_im);
    svst1_s32(pg32, (int32_t *)&p_x_int16[0 * p_xstride],
              svreinterpret_s32_s16(res0_16));

    svint32_t res1_re =
        svcvt_s32_f32_x(pg32, svmul_n_f32_x(pg32, acc1_re, x_mult));
    svint32_t res1_im =
        svcvt_s32_f32_x(pg32, svmul_n_f32_x(pg32, acc1_im, x_mult));
    svint16_t res1_16_re = svqxtnb_s32(res1_re);
    svint16_t res1_16 = svqxtnt_s32(res1_16_re, res1_im);
    svst1_s32(pg32, (int32_t *)&p_x_int16[1 * p_xstride],
              svreinterpret_s32_s16(res1_16));

    p_g_real += svcntw();
    p_g_imag += svcntw();
    p_y_int16 += svcnth();
    p_x_int16 += svcnth();
  }
#else
  int32x4_t y_shifts[4] = {
      vdupq_n_s32(-p_y_num_fract_bits[0]), vdupq_n_s32(-p_y_num_fract_bits[1]),
      vdupq_n_s32(-p_y_num_fract_bits[2]), vdupq_n_s32(-p_y_num_fract_bits[3])};

  for (unsigned j = 0; j < num_sub_carrier; j += 4) {
    float32x4_t y_elem_re[4];
    float32x4_t y_elem_im[4];
    for (int k = 0; k < 4; ++k) {
      int16x8_t y_int = vld1q_s16(&p_y_int16[k * p_ystride]);
      int32x4_t y_int_re =
          vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), y_int));
      int32x4_t y_int_im =
          vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), y_int));
      y_elem_re[k] = vcvtq_n_f32_s32(vshlq_s32(y_int_re, y_shifts[k]), 16);
      y_elem_im[k] = vcvtq_n_f32_s32(vshlq_s32(y_int_im, y_shifts[k]), 16);
    }

    float32x4_t c0_elem_re = vld1q_f32(&p_g_real[0 * p_gstride]);
    float32x4_t c0_elem_im = vld1q_f32(&p_g_imag[0 * p_gstride]);
    float32x4_t acc0_re = vmulq_f32(y_elem_re[0], c0_elem_re);
    acc0_re = vfmsq_f32(acc0_re, y_elem_im[0], c0_elem_im);
    float32x4_t acc0_im = vmulq_f32(y_elem_re[0], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_im[0], c0_elem_re);

    float32x4_t c1_elem_re = vld1q_f32(&p_g_real[4 * p_gstride]);
    float32x4_t c1_elem_im = vld1q_f32(&p_g_imag[4 * p_gstride]);
    float32x4_t acc1_re = vmulq_f32(y_elem_re[0], c1_elem_re);
    acc1_re = vfmsq_f32(acc1_re, y_elem_im[0], c1_elem_im);
    float32x4_t acc1_im = vmulq_f32(y_elem_re[0], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_im[0], c1_elem_re);

    c0_elem_re = vld1q_f32(&p_g_real[1 * p_gstride]);
    c0_elem_im = vld1q_f32(&p_g_imag[1 * p_gstride]);
    acc0_re = vfmaq_f32(acc0_re, y_elem_re[1], c0_elem_re);
    acc0_re = vfmsq_f32(acc0_re, y_elem_im[1], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_re[1], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_im[1], c0_elem_re);

    c1_elem_re = vld1q_f32(&p_g_real[5 * p_gstride]);
    c1_elem_im = vld1q_f32(&p_g_imag[5 * p_gstride]);
    acc1_re = vfmaq_f32(acc1_re, y_elem_re[1], c1_elem_re);
    acc1_re = vfmsq_f32(acc1_re, y_elem_im[1], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_re[1], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_im[1], c1_elem_re);

    c0_elem_re = vld1q_f32(&p_g_real[2 * p_gstride]);
    c0_elem_im = vld1q_f32(&p_g_imag[2 * p_gstride]);
    acc0_re = vfmaq_f32(acc0_re, y_elem_re[2], c0_elem_re);
    acc0_re = vfmsq_f32(acc0_re, y_elem_im[2], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_re[2], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_im[2], c0_elem_re);

    c1_elem_re = vld1q_f32(&p_g_real[6 * p_gstride]);
    c1_elem_im = vld1q_f32(&p_g_imag[6 * p_gstride]);
    acc1_re = vfmaq_f32(acc1_re, y_elem_re[2], c1_elem_re);
    acc1_re = vfmsq_f32(acc1_re, y_elem_im[2], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_re[2], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_im[2], c1_elem_re);

    c0_elem_re = vld1q_f32(&p_g_real[3 * p_gstride]);
    c0_elem_im = vld1q_f32(&p_g_imag[3 * p_gstride]);
    acc0_re = vfmaq_f32(acc0_re, y_elem_re[3], c0_elem_re);
    acc0_re = vfmsq_f32(acc0_re, y_elem_im[3], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_re[3], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_im[3], c0_elem_re);

    c1_elem_re = vld1q_f32(&p_g_real[7 * p_gstride]);
    c1_elem_im = vld1q_f32(&p_g_imag[7 * p_gstride]);
    acc1_re = vfmaq_f32(acc1_re, y_elem_re[3], c1_elem_re);
    acc1_re = vfmsq_f32(acc1_re, y_elem_im[3], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_re[3], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_im[3], c1_elem_re);

    int32x4_t res0_re = vcvtq_s32_f32(acc0_re * x_mult);
    int32x4_t res0_im = vcvtq_s32_f32(acc0_im * x_mult);
    int16x4_t res0_16_re = vqmovn_s32(res0_re);
    int16x4_t res0_16_im = vqmovn_s32(res0_im);
    int16x8_t res0_16 = shuf_s16_04152637(res0_16_re, res0_16_im);
    vst1q_s16(&p_x_int16[0 * p_xstride], res0_16);

    int32x4_t res1_re = vcvtq_s32_f32(acc1_re * x_mult);
    int32x4_t res1_im = vcvtq_s32_f32(acc1_im * x_mult);
    int16x4_t res1_16_re = vqmovn_s32(res1_re);
    int16x4_t res1_16_im = vqmovn_s32(res1_im);
    int16x8_t res1_16 = shuf_s16_04152637(res1_16_re, res1_16_im);
    vst1q_s16(&p_x_int16[1 * p_xstride], res1_16);

    p_g_real += 4;
    p_g_imag += 4;
    p_y_int16 += 8;
    p_x_int16 += 8;
  }
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_4x4_1sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride, const armral_fixed_point_index *p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    armral_fixed_point_index num_fract_bits_x) {

  float32_t x_mult = 1 << num_fract_bits_x;

  int16_t *p_x_int16 = (int16_t *)p_x;
  const int16_t *p_y_int16 = (const int16_t *)p_y;

#if ARMRAL_ARCH_SVE >= 2
  float32_t y_shifts[4] = {
      1U << (16 + p_y_num_fract_bits[0]), 1U << (16 + p_y_num_fract_bits[1]),
      1U << (16 + p_y_num_fract_bits[2]), 1U << (16 + p_y_num_fract_bits[3])};
  svbool_t pg16;

  for (uint32_t i = 0; svptest_first(
           svptrue_b16(), pg16 = svwhilelt_b16(2 * i, 2 * num_sub_carrier));
       i += svcntw()) {
    svbool_t pinterleave = svtrn1_b16(svpfalse_b(), pg16);
    svfloat32_t c0_elem_re = svld1_f32(pg16, &p_g_real[0 * p_gstride]);
    svfloat32_t c0_elem_im = svld1_f32(pg16, &p_g_imag[0 * p_gstride]);
    svint32_t y_int_re = svreinterpret_s32_s16(
        svld1_s16(pinterleave, (&p_y_int16[0 * p_ystride]) - 1));
    svfloat32_t y_elem_re =
        svmul_n_f32_x(pg16, svcvt_f32_s32_x(pg16, y_int_re), 1 / y_shifts[0]);
    svint32_t y_int_im = svreinterpret_s32_s16(
        svld1_s16(pinterleave, &p_y_int16[0 * p_ystride]));
    svfloat32_t y_elem_im =
        svmul_n_f32_x(pg16, svcvt_f32_s32_x(pg16, y_int_im), 1 / y_shifts[0]);
    svfloat32_t acc0_re = svmul_f32_x(pg16, y_elem_re, c0_elem_re);
    acc0_re = svmls_f32_x(pg16, acc0_re, y_elem_im, c0_elem_im);
    svfloat32_t acc0_im = svmul_f32_x(pg16, y_elem_re, c0_elem_im);
    acc0_im = svmla_f32_x(pg16, acc0_im, y_elem_im, c0_elem_re);

    svfloat32_t c1_elem_re = svld1_f32(pg16, &p_g_real[4 * p_gstride]);
    svfloat32_t c1_elem_im = svld1_f32(pg16, &p_g_imag[4 * p_gstride]);
    svfloat32_t acc1_re = svmul_f32_x(pg16, y_elem_re, c1_elem_re);
    acc1_re = svmls_f32_x(pg16, acc1_re, y_elem_im, c1_elem_im);
    svfloat32_t acc1_im = svmul_f32_x(pg16, y_elem_re, c1_elem_im);
    acc1_im = svmla_f32_x(pg16, acc1_im, y_elem_im, c1_elem_re);

    svfloat32_t c2_elem_re = svld1_f32(pg16, &p_g_real[8 * p_gstride]);
    svfloat32_t c2_elem_im = svld1_f32(pg16, &p_g_imag[8 * p_gstride]);
    svfloat32_t acc2_re = svmul_f32_x(pg16, y_elem_re, c2_elem_re);
    acc2_re = svmls_f32_x(pg16, acc2_re, y_elem_im, c2_elem_im);
    svfloat32_t acc2_im = svmul_f32_x(pg16, y_elem_re, c2_elem_im);
    acc2_im = svmla_f32_x(pg16, acc2_im, y_elem_im, c2_elem_re);

    svfloat32_t c3_elem_re = svld1_f32(pg16, &p_g_real[12 * p_gstride]);
    svfloat32_t c3_elem_im = svld1_f32(pg16, &p_g_imag[12 * p_gstride]);
    svfloat32_t acc3_re = svmul_f32_x(pg16, y_elem_re, c3_elem_re);
    acc3_re = svmls_f32_x(pg16, acc3_re, y_elem_im, c3_elem_im);
    svfloat32_t acc3_im = svmul_f32_x(pg16, y_elem_re, c3_elem_im);
    acc3_im = svmla_f32_x(pg16, acc3_im, y_elem_im, c3_elem_re);

    for (uint32_t j = 1; j < 4; ++j) {
      c0_elem_re = svld1_f32(pg16, &p_g_real[j * p_gstride]);
      c0_elem_im = svld1_f32(pg16, &p_g_imag[j * p_gstride]);
      y_int_re = svreinterpret_s32_s16(
          svld1_s16(pinterleave, (&p_y_int16[j * p_ystride]) - 1));
      y_elem_re =
          svmul_n_f32_x(pg16, svcvt_f32_s32_x(pg16, y_int_re), 1 / y_shifts[j]);
      y_int_im = svreinterpret_s32_s16(
          svld1_s16(pinterleave, &p_y_int16[j * p_ystride]));
      y_elem_im =
          svmul_n_f32_x(pg16, svcvt_f32_s32_x(pg16, y_int_im), 1 / y_shifts[j]);
      acc0_re = svmla_f32_x(pg16, acc0_re, y_elem_re, c0_elem_re);
      acc0_re = svmls_f32_x(pg16, acc0_re, y_elem_im, c0_elem_im);
      acc0_im = svmla_f32_x(pg16, acc0_im, y_elem_re, c0_elem_im);
      acc0_im = svmla_f32_x(pg16, acc0_im, y_elem_im, c0_elem_re);

      c1_elem_re = svld1_f32(pg16, &p_g_real[(j + 4) * p_gstride]);
      c1_elem_im = svld1_f32(pg16, &p_g_imag[(j + 4) * p_gstride]);
      acc1_re = svmla_f32_x(pg16, acc1_re, y_elem_re, c1_elem_re);
      acc1_re = svmls_f32_x(pg16, acc1_re, y_elem_im, c1_elem_im);
      acc1_im = svmla_f32_x(pg16, acc1_im, y_elem_re, c1_elem_im);
      acc1_im = svmla_f32_x(pg16, acc1_im, y_elem_im, c1_elem_re);

      c2_elem_re = svld1_f32(pg16, &p_g_real[(j + 8) * p_gstride]);
      c2_elem_im = svld1_f32(pg16, &p_g_imag[(j + 8) * p_gstride]);
      acc2_re = svmla_f32_x(pg16, acc2_re, y_elem_re, c2_elem_re);
      acc2_re = svmls_f32_x(pg16, acc2_re, y_elem_im, c2_elem_im);
      acc2_im = svmla_f32_x(pg16, acc2_im, y_elem_re, c2_elem_im);
      acc2_im = svmla_f32_x(pg16, acc2_im, y_elem_im, c2_elem_re);

      c3_elem_re = svld1_f32(pg16, &p_g_real[(j + 12) * p_gstride]);
      c3_elem_im = svld1_f32(pg16, &p_g_imag[(j + 12) * p_gstride]);
      acc3_re = svmla_f32_x(pg16, acc3_re, y_elem_re, c3_elem_re);
      acc3_re = svmls_f32_x(pg16, acc3_re, y_elem_im, c3_elem_im);
      acc3_im = svmla_f32_x(pg16, acc3_im, y_elem_re, c3_elem_im);
      acc3_im = svmla_f32_x(pg16, acc3_im, y_elem_im, c3_elem_re);
    }

    svint32_t res0_re =
        svcvt_s32_f32_x(pg16, svmul_n_f32_x(pg16, acc0_re, x_mult));
    svint32_t res0_im =
        svcvt_s32_f32_x(pg16, svmul_n_f32_x(pg16, acc0_im, x_mult));
    svint16_t res0_16_re = svqxtnb_s32(res0_re);
    svint16_t res0_16 = svqxtnt_s32(res0_16_re, res0_im);
    svst1_s16(pg16, &p_x_int16[0 * p_xstride], res0_16);

    svint32_t res1_re =
        svcvt_s32_f32_x(pg16, svmul_n_f32_x(pg16, acc1_re, x_mult));
    svint32_t res1_im =
        svcvt_s32_f32_x(pg16, svmul_n_f32_x(pg16, acc1_im, x_mult));
    svint16_t res1_16_re = svqxtnb_s32(res1_re);
    svint16_t res1_16 = svqxtnt_s32(res1_16_re, res1_im);
    svst1_s16(pg16, &p_x_int16[1 * p_xstride], res1_16);

    svint32_t res2_re =
        svcvt_s32_f32_x(pg16, svmul_n_f32_x(pg16, acc2_re, x_mult));
    svint32_t res2_im =
        svcvt_s32_f32_x(pg16, svmul_n_f32_x(pg16, acc2_im, x_mult));
    svint16_t res2_16_re = svqxtnb_s32(res2_re);
    svint16_t res2_16 = svqxtnt_s32(res2_16_re, res2_im);
    svst1_s16(pg16, &p_x_int16[2 * p_xstride], res2_16);

    svint32_t res3_re =
        svcvt_s32_f32_x(pg16, svmul_n_f32_x(pg16, acc3_re, x_mult));
    svint32_t res3_im =
        svcvt_s32_f32_x(pg16, svmul_n_f32_x(pg16, acc3_im, x_mult));
    svint16_t res3_16_re = svqxtnb_s32(res3_re);
    svint16_t res3_16 = svqxtnt_s32(res3_16_re, res3_im);
    svst1_s16(pg16, &p_x_int16[3 * p_xstride], res3_16);

    p_g_real += svcntw();
    p_g_imag += svcntw();
    p_y_int16 += svcnth();
    p_x_int16 += svcnth();
  }
#else
  int32x4_t y_shifts[4] = {
      vdupq_n_s32(-p_y_num_fract_bits[0]), vdupq_n_s32(-p_y_num_fract_bits[1]),
      vdupq_n_s32(-p_y_num_fract_bits[2]), vdupq_n_s32(-p_y_num_fract_bits[3])};

  for (unsigned j = 0; j < num_sub_carrier; j += 4) {
    float32x4_t y_elem_re[4];
    float32x4_t y_elem_im[4];
    for (int k = 0; k < 4; ++k) {
      int16x8_t y_int = vld1q_s16(&p_y_int16[k * p_ystride]);
      int32x4_t y_int_re =
          vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), y_int));
      int32x4_t y_int_im =
          vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), y_int));
      y_elem_re[k] = vcvtq_n_f32_s32(vshlq_s32(y_int_re, y_shifts[k]), 16);
      y_elem_im[k] = vcvtq_n_f32_s32(vshlq_s32(y_int_im, y_shifts[k]), 16);
    }

    float32x4_t c0_elem_re = vld1q_f32(&p_g_real[0 * p_gstride]);
    float32x4_t c0_elem_im = vld1q_f32(&p_g_imag[0 * p_gstride]);
    float32x4_t acc0_re = vmulq_f32(y_elem_re[0], c0_elem_re);
    acc0_re = vfmsq_f32(acc0_re, y_elem_im[0], c0_elem_im);
    float32x4_t acc0_im = vmulq_f32(y_elem_re[0], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_im[0], c0_elem_re);

    float32x4_t c1_elem_re = vld1q_f32(&p_g_real[4 * p_gstride]);
    float32x4_t c1_elem_im = vld1q_f32(&p_g_imag[4 * p_gstride]);
    float32x4_t acc1_re = vmulq_f32(y_elem_re[0], c1_elem_re);
    acc1_re = vfmsq_f32(acc1_re, y_elem_im[0], c1_elem_im);
    float32x4_t acc1_im = vmulq_f32(y_elem_re[0], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_im[0], c1_elem_re);

    float32x4_t c2_elem_re = vld1q_f32(&p_g_real[8 * p_gstride]);
    float32x4_t c2_elem_im = vld1q_f32(&p_g_imag[8 * p_gstride]);
    float32x4_t acc2_re = vmulq_f32(y_elem_re[0], c2_elem_re);
    acc2_re = vfmsq_f32(acc2_re, y_elem_im[0], c2_elem_im);
    float32x4_t acc2_im = vmulq_f32(y_elem_re[0], c2_elem_im);
    acc2_im = vfmaq_f32(acc2_im, y_elem_im[0], c2_elem_re);

    float32x4_t c3_elem_re = vld1q_f32(&p_g_real[12 * p_gstride]);
    float32x4_t c3_elem_im = vld1q_f32(&p_g_imag[12 * p_gstride]);
    float32x4_t acc3_re = vmulq_f32(y_elem_re[0], c3_elem_re);
    acc3_re = vfmsq_f32(acc3_re, y_elem_im[0], c3_elem_im);
    float32x4_t acc3_im = vmulq_f32(y_elem_re[0], c3_elem_im);
    acc3_im = vfmaq_f32(acc3_im, y_elem_im[0], c3_elem_re);

    c0_elem_re = vld1q_f32(&p_g_real[1 * p_gstride]);
    c0_elem_im = vld1q_f32(&p_g_imag[1 * p_gstride]);
    acc0_re = vfmaq_f32(acc0_re, y_elem_re[1], c0_elem_re);
    acc0_re = vfmsq_f32(acc0_re, y_elem_im[1], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_re[1], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_im[1], c0_elem_re);

    c1_elem_re = vld1q_f32(&p_g_real[5 * p_gstride]);
    c1_elem_im = vld1q_f32(&p_g_imag[5 * p_gstride]);
    acc1_re = vfmaq_f32(acc1_re, y_elem_re[1], c1_elem_re);
    acc1_re = vfmsq_f32(acc1_re, y_elem_im[1], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_re[1], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_im[1], c1_elem_re);

    c2_elem_re = vld1q_f32(&p_g_real[9 * p_gstride]);
    c2_elem_im = vld1q_f32(&p_g_imag[9 * p_gstride]);
    acc2_re = vfmaq_f32(acc2_re, y_elem_re[1], c2_elem_re);
    acc2_re = vfmsq_f32(acc2_re, y_elem_im[1], c2_elem_im);
    acc2_im = vfmaq_f32(acc2_im, y_elem_re[1], c2_elem_im);
    acc2_im = vfmaq_f32(acc2_im, y_elem_im[1], c2_elem_re);

    c3_elem_re = vld1q_f32(&p_g_real[13 * p_gstride]);
    c3_elem_im = vld1q_f32(&p_g_imag[13 * p_gstride]);
    acc3_re = vfmaq_f32(acc3_re, y_elem_re[1], c3_elem_re);
    acc3_re = vfmsq_f32(acc3_re, y_elem_im[1], c3_elem_im);
    acc3_im = vfmaq_f32(acc3_im, y_elem_re[1], c3_elem_im);
    acc3_im = vfmaq_f32(acc3_im, y_elem_im[1], c3_elem_re);

    c0_elem_re = vld1q_f32(&p_g_real[2 * p_gstride]);
    c0_elem_im = vld1q_f32(&p_g_imag[2 * p_gstride]);
    acc0_re = vfmaq_f32(acc0_re, y_elem_re[2], c0_elem_re);
    acc0_re = vfmsq_f32(acc0_re, y_elem_im[2], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_re[2], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_im[2], c0_elem_re);

    c1_elem_re = vld1q_f32(&p_g_real[6 * p_gstride]);
    c1_elem_im = vld1q_f32(&p_g_imag[6 * p_gstride]);
    acc1_re = vfmaq_f32(acc1_re, y_elem_re[2], c1_elem_re);
    acc1_re = vfmsq_f32(acc1_re, y_elem_im[2], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_re[2], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_im[2], c1_elem_re);

    c2_elem_re = vld1q_f32(&p_g_real[10 * p_gstride]);
    c2_elem_im = vld1q_f32(&p_g_imag[10 * p_gstride]);
    acc2_re = vfmaq_f32(acc2_re, y_elem_re[2], c2_elem_re);
    acc2_re = vfmsq_f32(acc2_re, y_elem_im[2], c2_elem_im);
    acc2_im = vfmaq_f32(acc2_im, y_elem_re[2], c2_elem_im);
    acc2_im = vfmaq_f32(acc2_im, y_elem_im[2], c2_elem_re);

    c3_elem_re = vld1q_f32(&p_g_real[14 * p_gstride]);
    c3_elem_im = vld1q_f32(&p_g_imag[14 * p_gstride]);
    acc3_re = vfmaq_f32(acc3_re, y_elem_re[2], c3_elem_re);
    acc3_re = vfmsq_f32(acc3_re, y_elem_im[2], c3_elem_im);
    acc3_im = vfmaq_f32(acc3_im, y_elem_re[2], c3_elem_im);
    acc3_im = vfmaq_f32(acc3_im, y_elem_im[2], c3_elem_re);

    c0_elem_re = vld1q_f32(&p_g_real[3 * p_gstride]);
    c0_elem_im = vld1q_f32(&p_g_imag[3 * p_gstride]);
    acc0_re = vfmaq_f32(acc0_re, y_elem_re[3], c0_elem_re);
    acc0_re = vfmsq_f32(acc0_re, y_elem_im[3], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_re[3], c0_elem_im);
    acc0_im = vfmaq_f32(acc0_im, y_elem_im[3], c0_elem_re);

    c1_elem_re = vld1q_f32(&p_g_real[7 * p_gstride]);
    c1_elem_im = vld1q_f32(&p_g_imag[7 * p_gstride]);
    acc1_re = vfmaq_f32(acc1_re, y_elem_re[3], c1_elem_re);
    acc1_re = vfmsq_f32(acc1_re, y_elem_im[3], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_re[3], c1_elem_im);
    acc1_im = vfmaq_f32(acc1_im, y_elem_im[3], c1_elem_re);

    c2_elem_re = vld1q_f32(&p_g_real[11 * p_gstride]);
    c2_elem_im = vld1q_f32(&p_g_imag[11 * p_gstride]);
    acc2_re = vfmaq_f32(acc2_re, y_elem_re[3], c2_elem_re);
    acc2_re = vfmsq_f32(acc2_re, y_elem_im[3], c2_elem_im);
    acc2_im = vfmaq_f32(acc2_im, y_elem_re[3], c2_elem_im);
    acc2_im = vfmaq_f32(acc2_im, y_elem_im[3], c2_elem_re);

    c3_elem_re = vld1q_f32(&p_g_real[15 * p_gstride]);
    c3_elem_im = vld1q_f32(&p_g_imag[15 * p_gstride]);
    acc3_re = vfmaq_f32(acc3_re, y_elem_re[3], c3_elem_re);
    acc3_re = vfmsq_f32(acc3_re, y_elem_im[3], c3_elem_im);
    acc3_im = vfmaq_f32(acc3_im, y_elem_re[3], c3_elem_im);
    acc3_im = vfmaq_f32(acc3_im, y_elem_im[3], c3_elem_re);

    int32x4_t res0_re = vcvtq_s32_f32(acc0_re * x_mult);
    int32x4_t res0_im = vcvtq_s32_f32(acc0_im * x_mult);
    int16x4_t res0_16_re = vqmovn_s32(res0_re);
    int16x4_t res0_16_im = vqmovn_s32(res0_im);
    int16x8_t res0_16 = shuf_s16_04152637(res0_16_re, res0_16_im);
    vst1q_s16(&p_x_int16[0 * p_xstride], res0_16);

    int32x4_t res1_re = vcvtq_s32_f32(acc1_re * x_mult);
    int32x4_t res1_im = vcvtq_s32_f32(acc1_im * x_mult);
    int16x4_t res1_16_re = vqmovn_s32(res1_re);
    int16x4_t res1_16_im = vqmovn_s32(res1_im);
    int16x8_t res1_16 = shuf_s16_04152637(res1_16_re, res1_16_im);
    vst1q_s16(&p_x_int16[1 * p_xstride], res1_16);

    int32x4_t res2_re = vcvtq_s32_f32(acc2_re * x_mult);
    int32x4_t res2_im = vcvtq_s32_f32(acc2_im * x_mult);
    int16x4_t res2_16_re = vqmovn_s32(res2_re);
    int16x4_t res2_16_im = vqmovn_s32(res2_im);
    int16x8_t res2_16 = shuf_s16_04152637(res2_16_re, res2_16_im);
    vst1q_s16(&p_x_int16[2 * p_xstride], res2_16);

    int32x4_t res3_re = vcvtq_s32_f32(acc3_re * x_mult);
    int32x4_t res3_im = vcvtq_s32_f32(acc3_im * x_mult);
    int16x4_t res3_16_re = vqmovn_s32(res3_re);
    int16x4_t res3_16_im = vqmovn_s32(res3_im);
    int16x8_t res3_16 = shuf_s16_04152637(res3_16_re, res3_16_im);
    vst1q_s16(&p_x_int16[3 * p_xstride], res3_16);

    p_g_real += 4;
    p_g_imag += 4;
    p_y_int16 += 8;
    p_x_int16 += 8;
  }
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_1x4_1sc_f32(
    const uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    const uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    const uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    const armral_fixed_point_index num_fract_bits_x) {

  float32_t x_mult = 1 << num_fract_bits_x;

  int16_t *p_x_int16 = (int16_t *)p_x;
  const int16_t *p_y_int16 = (const int16_t *)p_y;

#if ARMRAL_ARCH_SVE >= 2
  float32_t y_shifts[4] = {
      1U << (16 + p_y_num_fract_bits[0]), 1U << (16 + p_y_num_fract_bits[1]),
      1U << (16 + p_y_num_fract_bits[2]), 1U << (16 + p_y_num_fract_bits[3])};
  svbool_t pg16;

  for (uint32_t i = 0; svptest_first(
           svptrue_b16(), pg16 = svwhilelt_b16(2 * i, 2 * num_sub_carrier));
       i += svcntw()) {
    svbool_t pinterleave = svtrn1_b16(svpfalse_b(), pg16);
    svfloat32_t c_elem_re = svld1_f32(pg16, &p_g_real[0 * p_gstride]);
    svfloat32_t c_elem_im = svld1_f32(pg16, &p_g_imag[0 * p_gstride]);
    svint32_t y_int_re = svreinterpret_s32_s16(
        svld1_s16(pinterleave, (&p_y_int16[0 * p_ystride]) - 1));
    svfloat32_t y_elem_re =
        svmul_n_f32_x(pg16, svcvt_f32_s32_x(pg16, y_int_re), 1 / y_shifts[0]);
    svint32_t y_int_im = svreinterpret_s32_s16(
        svld1_s16(pinterleave, &p_y_int16[0 * p_ystride]));
    svfloat32_t y_elem_im =
        svmul_n_f32_x(pg16, svcvt_f32_s32_x(pg16, y_int_im), 1 / y_shifts[0]);
    svfloat32_t acc_re = svmul_f32_x(pg16, y_elem_re, c_elem_re);
    acc_re = svmls_f32_x(pg16, acc_re, y_elem_im, c_elem_im);
    svfloat32_t acc_im = svmul_f32_x(pg16, y_elem_re, c_elem_im);
    acc_im = svmla_f32_x(pg16, acc_im, y_elem_im, c_elem_re);

    for (uint32_t j = 1; j < 4; ++j) {
      c_elem_re = svld1_f32(pg16, &p_g_real[j * p_gstride]);
      c_elem_im = svld1_f32(pg16, &p_g_imag[j * p_gstride]);
      y_int_re = svreinterpret_s32_s16(
          svld1_s16(pinterleave, (&p_y_int16[j * p_ystride]) - 1));
      y_elem_re =
          svmul_n_f32_x(pg16, svcvt_f32_s32_x(pg16, y_int_re), 1 / y_shifts[j]);
      y_int_im = svreinterpret_s32_s16(
          svld1_s16(pinterleave, &p_y_int16[j * p_ystride]));
      y_elem_im =
          svmul_n_f32_x(pg16, svcvt_f32_s32_x(pg16, y_int_im), 1 / y_shifts[j]);
      acc_re = svmla_f32_x(pg16, acc_re, y_elem_re, c_elem_re);
      acc_re = svmls_f32_x(pg16, acc_re, y_elem_im, c_elem_im);
      acc_im = svmla_f32_x(pg16, acc_im, y_elem_re, c_elem_im);
      acc_im = svmla_f32_x(pg16, acc_im, y_elem_im, c_elem_re);
    }

    svint32_t res_re =
        svcvt_s32_f32_x(pg16, svmul_n_f32_x(pg16, acc_re, x_mult));
    svint32_t res_im =
        svcvt_s32_f32_x(pg16, svmul_n_f32_x(pg16, acc_im, x_mult));
    svint16_t res_16_re = svqxtnb_s32(res_re);
    svint16_t res_16 = svqxtnt_s32(res_16_re, res_im);
    svst1_s16(pg16, p_x_int16, res_16);
    p_g_real += svcntw();
    p_g_imag += svcntw();
    p_y_int16 += svcnth();
    p_x_int16 += svcnth();
  }
#else
  int32x4_t y_shifts[4] = {
      vdupq_n_s32(-p_y_num_fract_bits[0]), vdupq_n_s32(-p_y_num_fract_bits[1]),
      vdupq_n_s32(-p_y_num_fract_bits[2]), vdupq_n_s32(-p_y_num_fract_bits[3])};

  for (unsigned j = 0; j < num_sub_carrier; j += 4) {
    float32x4_t c_elem_re = vld1q_f32(&p_g_real[0 * p_gstride]);
    float32x4_t c_elem_im = vld1q_f32(&p_g_imag[0 * p_gstride]);
    int16x8_t y_int = vld1q_s16(&p_y_int16[0 * p_ystride]);
    int32x4_t y_int_re =
        vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), y_int));
    int32x4_t y_int_im =
        vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), y_int));
    float32x4_t y_elem_re =
        vcvtq_n_f32_s32(vshlq_s32(y_int_re, y_shifts[0]), 16);
    float32x4_t y_elem_im =
        vcvtq_n_f32_s32(vshlq_s32(y_int_im, y_shifts[0]), 16);
    float32x4_t acc_re = vmulq_f32(y_elem_re, c_elem_re);
    acc_re = vfmsq_f32(acc_re, y_elem_im, c_elem_im);
    float32x4_t acc_im = vmulq_f32(y_elem_re, c_elem_im);
    acc_im = vfmaq_f32(acc_im, y_elem_im, c_elem_re);

    c_elem_re = vld1q_f32(&p_g_real[1 * p_gstride]);
    c_elem_im = vld1q_f32(&p_g_imag[1 * p_gstride]);
    y_int = vld1q_s16(&p_y_int16[1 * p_ystride]);
    y_int_re = vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), y_int));
    y_int_im = vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), y_int));
    y_elem_re = vcvtq_n_f32_s32(vshlq_s32(y_int_re, y_shifts[1]), 16);
    y_elem_im = vcvtq_n_f32_s32(vshlq_s32(y_int_im, y_shifts[1]), 16);
    acc_re = vfmaq_f32(acc_re, y_elem_re, c_elem_re);
    acc_re = vfmsq_f32(acc_re, y_elem_im, c_elem_im);
    acc_im = vfmaq_f32(acc_im, y_elem_re, c_elem_im);
    acc_im = vfmaq_f32(acc_im, y_elem_im, c_elem_re);

    c_elem_re = vld1q_f32(&p_g_real[2 * p_gstride]);
    c_elem_im = vld1q_f32(&p_g_imag[2 * p_gstride]);
    y_int = vld1q_s16(&p_y_int16[2 * p_ystride]);
    y_int_re = vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), y_int));
    y_int_im = vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), y_int));
    y_elem_re = vcvtq_n_f32_s32(vshlq_s32(y_int_re, y_shifts[2]), 16);
    y_elem_im = vcvtq_n_f32_s32(vshlq_s32(y_int_im, y_shifts[2]), 16);
    acc_re = vfmaq_f32(acc_re, y_elem_re, c_elem_re);
    acc_re = vfmsq_f32(acc_re, y_elem_im, c_elem_im);
    acc_im = vfmaq_f32(acc_im, y_elem_re, c_elem_im);
    acc_im = vfmaq_f32(acc_im, y_elem_im, c_elem_re);

    c_elem_re = vld1q_f32(&p_g_real[3 * p_gstride]);
    c_elem_im = vld1q_f32(&p_g_imag[3 * p_gstride]);
    y_int = vld1q_s16(&p_y_int16[3 * p_ystride]);
    y_int_re = vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), y_int));
    y_int_im = vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), y_int));
    y_elem_re = vcvtq_n_f32_s32(vshlq_s32(y_int_re, y_shifts[3]), 16);
    y_elem_im = vcvtq_n_f32_s32(vshlq_s32(y_int_im, y_shifts[3]), 16);
    acc_re = vfmaq_f32(acc_re, y_elem_re, c_elem_re);
    acc_re = vfmsq_f32(acc_re, y_elem_im, c_elem_im);
    acc_im = vfmaq_f32(acc_im, y_elem_re, c_elem_im);
    acc_im = vfmaq_f32(acc_im, y_elem_im, c_elem_re);

    int32x4_t res_re = vcvtq_s32_f32(acc_re * x_mult);
    int32x4_t res_im = vcvtq_s32_f32(acc_im * x_mult);
    int16x4_t res16_re = vqmovn_s32(res_re);
    int16x4_t res16_im = vqmovn_s32(res_im);
    int16x8_t res16 = shuf_s16_04152637(res16_re, res16_im);
    vst1q_s16(p_x_int16, res16);
    p_g_real += 4;
    p_g_imag += 4;
    p_y_int16 += 8;
    p_x_int16 += 8;
  }
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_1x2_1sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    armral_fixed_point_index num_fract_bits_x) {

  float32_t x_mult = 1 << num_fract_bits_x;

  int16_t *p_x_int16 = (int16_t *)p_x;
  const int16_t *p_y_int16 = (const int16_t *)p_y;

#if ARMRAL_ARCH_SVE >= 2
  float32_t y_shifts[2] = {1 << p_y_num_fract_bits[0],
                           1 << p_y_num_fract_bits[1]};
  svbool_t pg32;

  for (uint32_t i = 0;
       svptest_any(svptrue_b32(), pg32 = svwhilelt_b32(i, num_sub_carrier));
       i += svcntw()) {
    svfloat32_t c_elem_re = svld1_f32(pg32, &p_g_real[0 * p_gstride]);
    svfloat32_t c_elem_im = svld1_f32(pg32, &p_g_imag[0 * p_gstride]);
    svint16_t y_int = svreinterpret_s16_s32(
        svld1_s32(pg32, (const int32_t *)&p_y_int16[0 * p_ystride]));
    svint32_t y_int_re = svmovlb_s32(y_int);
    svint32_t y_int_im = svmovlt_s32(y_int);
    svfloat32_t y_elem_re =
        svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_re), 1 / y_shifts[0]);
    svfloat32_t y_elem_im =
        svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_im), 1 / y_shifts[0]);
    svfloat32_t acc_re = svmul_f32_x(pg32, y_elem_re, c_elem_re);
    acc_re = svmls_f32_x(pg32, acc_re, y_elem_im, c_elem_im);
    svfloat32_t acc_im = svmul_f32_x(pg32, y_elem_re, c_elem_im);
    acc_im = svmla_f32_x(pg32, acc_im, y_elem_im, c_elem_re);

    c_elem_re = svld1_f32(pg32, &p_g_real[1 * p_gstride]);
    c_elem_im = svld1_f32(pg32, &p_g_imag[1 * p_gstride]);
    y_int = svreinterpret_s16_s32(
        svld1_s32(pg32, (const int32_t *)&p_y_int16[1 * p_ystride]));
    y_int_re = svmovlb_s32(y_int);
    y_int_im = svmovlt_s32(y_int);
    y_elem_re =
        svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_re), 1 / y_shifts[1]);
    y_elem_im =
        svmul_n_f32_x(pg32, svcvt_f32_s32_x(pg32, y_int_im), 1 / y_shifts[1]);
    acc_re = svmla_f32_x(pg32, acc_re, y_elem_re, c_elem_re);
    acc_re = svmls_f32_x(pg32, acc_re, y_elem_im, c_elem_im);
    acc_im = svmla_f32_x(pg32, acc_im, y_elem_re, c_elem_im);
    acc_im = svmla_f32_x(pg32, acc_im, y_elem_im, c_elem_re);

    svint32_t res_re =
        svcvt_s32_f32_x(pg32, svmul_n_f32_x(pg32, acc_re, x_mult));
    svint32_t res_im =
        svcvt_s32_f32_x(pg32, svmul_n_f32_x(pg32, acc_im, x_mult));
    svint16_t res16_re = svqxtnb_s32(res_re);
    svint16_t res16 = svqxtnt_s32(res16_re, res_im);
    svst1_s32(pg32, (int32_t *)p_x_int16, svreinterpret_s32_s16(res16));
    p_g_real += svcntw();
    p_g_imag += svcntw();
    p_y_int16 += svcnth();
    p_x_int16 += svcnth();
  }
#else
  int32x4_t y_shifts[2] = {vdupq_n_s32(-p_y_num_fract_bits[0]),
                           vdupq_n_s32(-p_y_num_fract_bits[1])};

  for (unsigned j = 0; j < num_sub_carrier; j += 4) {
    float32x4_t c_elem_re = vld1q_f32(&p_g_real[0 * p_gstride]);
    float32x4_t c_elem_im = vld1q_f32(&p_g_imag[0 * p_gstride]);
    int16x8_t y_int = vld1q_s16(&p_y_int16[0 * p_ystride]);
    int32x4_t y_int_re =
        vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), y_int));
    int32x4_t y_int_im =
        vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), y_int));
    float32x4_t y_elem_re =
        vcvtq_n_f32_s32(vshlq_s32(y_int_re, y_shifts[0]), 16);
    float32x4_t y_elem_im =
        vcvtq_n_f32_s32(vshlq_s32(y_int_im, y_shifts[0]), 16);
    float32x4_t acc_re = vmulq_f32(y_elem_re, c_elem_re);
    acc_re = vfmsq_f32(acc_re, y_elem_im, c_elem_im);
    float32x4_t acc_im = vmulq_f32(y_elem_re, c_elem_im);
    acc_im = vfmaq_f32(acc_im, y_elem_im, c_elem_re);

    c_elem_re = vld1q_f32(&p_g_real[1 * p_gstride]);
    c_elem_im = vld1q_f32(&p_g_imag[1 * p_gstride]);
    y_int = vld1q_s16(&p_y_int16[1 * p_ystride]);
    y_int_re = vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), y_int));
    y_int_im = vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), y_int));
    y_elem_re = vcvtq_n_f32_s32(vshlq_s32(y_int_re, y_shifts[1]), 16);
    y_elem_im = vcvtq_n_f32_s32(vshlq_s32(y_int_im, y_shifts[1]), 16);
    acc_re = vfmaq_f32(acc_re, y_elem_re, c_elem_re);
    acc_re = vfmsq_f32(acc_re, y_elem_im, c_elem_im);
    acc_im = vfmaq_f32(acc_im, y_elem_re, c_elem_im);
    acc_im = vfmaq_f32(acc_im, y_elem_im, c_elem_re);

    int32x4_t res_re = vcvtq_s32_f32(acc_re * x_mult);
    int32x4_t res_im = vcvtq_s32_f32(acc_im * x_mult);
    int16x4_t res16_re = vqmovn_s32(res_re);
    int16x4_t res16_im = vqmovn_s32(res_im);
    int16x8_t res16 = shuf_s16_04152637(res16_re, res16_im);
    vst1q_s16(p_x_int16, res16);
    p_g_real += 4;
    p_g_imag += 4;
    p_y_int16 += 8;
    p_x_int16 += 8;
  }
#endif
  return ARMRAL_SUCCESS;
}
