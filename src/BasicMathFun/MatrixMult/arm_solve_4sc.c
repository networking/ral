/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "arm_solve_4sc.h"
#include "arm_solve_convert.h"
#include "intrinsics.h"

armral_status armral_solve_2x2_4sc_f32(
    const uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    const uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    const uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    const uint32_t p_xstride, const armral_fixed_point_index num_fract_bits_x) {

  const int16_t *y0_ptr = (const int16_t *)p_y;
  const int16_t *y1_ptr = (const int16_t *)p_y + p_ystride;

  int16_t *x0_ptr = (int16_t *)p_x;
  int16_t *x1_ptr = (int16_t *)p_x + p_xstride;

#if ARMRAL_ARCH_SVE >= 2
  svbool_t pg8 = svptrue_pat_b16(SV_VL8);
  svbool_t pg3 = svptrue_pat_b32(SV_VL3);
  svbool_t podd = svtrn1_b16(svpfalse_b(), pg8);
  float32_t scale[] = {1. / (1U << (16 + p_y_num_fract_bits[0])),
                       1. / (1U << (16 + p_y_num_fract_bits[1]))};
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    // Load the first two y0 entries
    svint32_t y00_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr - 1));
    svint32_t y00_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr));
    svfloat32_t y00_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y00_int_re), scale[0]);
    svfloat32_t y00_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y00_int_im), scale[0]);

    svint32_t y01_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 7));
    svint32_t y01_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 8));
    svfloat32_t y01_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y01_int_re), scale[0]);
    svfloat32_t y01_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y01_int_im), scale[0]);

    // G00[0], G00[1]
    svfloat32_t g00_re = svld1_f32(pg3, &p_g_real[0 * p_gstride]);
    svfloat32_t g00_im = svld1_f32(pg3, &p_g_imag[0 * p_gstride]);
    svfloat32_t x00_re = svmul_lane_f32(y00_re, g00_re, 0);
    x00_re = svmls_lane_f32(x00_re, y00_im, g00_im, 0);
    svfloat32_t x00_im = svmul_lane_f32(y00_re, g00_im, 0);
    x00_im = svmla_lane_f32(x00_im, y00_im, g00_re, 0);

    svfloat32_t x01_re = svmul_lane_f32(y01_re, g00_re, 1);
    x01_re = svmls_lane_f32(x01_re, y01_im, g00_im, 1);
    svfloat32_t x01_im = svmul_lane_f32(y01_re, g00_im, 1);
    x01_im = svmla_lane_f32(x01_im, y01_im, g00_re, 1);

    // Load the first two y1 entries
    svint32_t y10_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr - 1));
    svint32_t y10_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr));
    svfloat32_t y10_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y10_int_re), scale[1]);
    svfloat32_t y10_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y10_int_im), scale[1]);

    svint32_t y11_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 7));
    svint32_t y11_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 8));
    svfloat32_t y11_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y11_int_re), scale[1]);
    svfloat32_t y11_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y11_int_im), scale[1]);

    // G01[0], G01[1]
    svfloat32_t g01_re = svld1_f32(pg3, &p_g_real[1 * p_gstride]);
    svfloat32_t g01_im = svld1_f32(pg3, &p_g_imag[1 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y10_re, g01_re, 0);
    x00_re = svmls_lane_f32(x00_re, y10_im, g01_im, 0);
    x00_im = svmla_lane_f32(x00_im, y10_re, g01_im, 0);
    x00_im = svmla_lane_f32(x00_im, y10_im, g01_re, 0);

    x01_re = svmla_lane_f32(x01_re, y11_re, g01_re, 1);
    x01_re = svmls_lane_f32(x01_re, y11_im, g01_im, 1);
    x01_im = svmla_lane_f32(x01_im, y11_re, g01_im, 1);
    x01_im = svmla_lane_f32(x01_im, y11_im, g01_re, 1);

    // Store the first two x0 entries
    svint16_t x00_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x00_re, x00_im, pg8);
    svst1_s16(pg8, x0_ptr, x00_bis);
    svint16_t x01_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x01_re, x01_im, pg8);
    svst1_s16(pg8, x0_ptr + 8, x01_bis);

    // G10[0], G10[1]
    svfloat32_t g10_re = svld1_f32(pg3, &p_g_real[2 * p_gstride]);
    svfloat32_t g10_im = svld1_f32(pg3, &p_g_imag[2 * p_gstride]);
    svfloat32_t x10_re = svmul_lane_f32(y00_re, g10_re, 0);
    x10_re = svmls_lane_f32(x10_re, y00_im, g10_im, 0);
    svfloat32_t x10_im = svmul_lane_f32(y00_re, g10_im, 0);
    x10_im = svmla_lane_f32(x10_im, y00_im, g10_re, 0);

    svfloat32_t x11_re = svmul_lane_f32(y01_re, g10_re, 1);
    x11_re = svmls_lane_f32(x11_re, y01_im, g10_im, 1);
    svfloat32_t x11_im = svmul_lane_f32(y01_re, g10_im, 1);
    x11_im = svmla_lane_f32(x11_im, y01_im, g10_re, 1);

    // G11[0], G11[1]
    svfloat32_t g11_re = svld1_f32(pg3, &p_g_real[3 * p_gstride]);
    svfloat32_t g11_im = svld1_f32(pg3, &p_g_imag[3 * p_gstride]);
    x10_re = svmla_lane_f32(x10_re, y10_re, g11_re, 0);
    x10_re = svmls_lane_f32(x10_re, y10_im, g11_im, 0);
    x10_im = svmla_lane_f32(x10_im, y10_re, g11_im, 0);
    x10_im = svmla_lane_f32(x10_im, y10_im, g11_re, 0);

    x11_re = svmla_lane_f32(x11_re, y11_re, g11_re, 1);
    x11_re = svmls_lane_f32(x11_re, y11_im, g11_im, 1);
    x11_im = svmla_lane_f32(x11_im, y11_re, g11_im, 1);
    x11_im = svmla_lane_f32(x11_im, y11_im, g11_re, 1);

    // Store the first two x1 entries
    svint16_t x10_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x10_re, x10_im, pg8);
    svst1_s16(pg8, x1_ptr, x10_bis);
    svint16_t x11_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x11_re, x11_im, pg8);
    svst1_s16(pg8, x1_ptr + 8, x11_bis);

    // Process the third y entries.
    svint32_t y02_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 15));
    svint32_t y02_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 16));
    svfloat32_t y02_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y02_int_re), scale[0]);
    svfloat32_t y02_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y02_int_im), scale[0]);
    y0_ptr = y0_ptr + 24;

    svint32_t y12_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 15));
    svint32_t y12_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 16));
    svfloat32_t y12_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y12_int_re), scale[1]);
    svfloat32_t y12_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y12_int_im), scale[1]);
    y1_ptr = y1_ptr + 24;

    // G00[2]
    svfloat32_t x02_re = svmul_lane_f32(y02_re, g00_re, 2);
    x02_re = svmls_lane_f32(x02_re, y02_im, g00_im, 2);
    svfloat32_t x02_im = svmul_lane_f32(y02_re, g00_im, 2);
    x02_im = svmla_lane_f32(x02_im, y02_im, g00_re, 2);

    // G01[2]
    x02_re = svmla_lane_f32(x02_re, y12_re, g01_re, 2);
    x02_re = svmls_lane_f32(x02_re, y12_im, g01_im, 2);
    x02_im = svmla_lane_f32(x02_im, y12_re, g01_im, 2);
    x02_im = svmla_lane_f32(x02_im, y12_im, g01_re, 2);

    svint16_t x02_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x02_re, x02_im, pg8);
    svst1_s16(pg8, x0_ptr + 16, x02_bis);
    x0_ptr = x0_ptr + 24;

    // G10[2]
    svfloat32_t x12_re = svmul_lane_f32(y02_re, g10_re, 2);
    x12_re = svmls_lane_f32(x12_re, y02_im, g10_im, 2);
    svfloat32_t x12_im = svmul_lane_f32(y02_re, g10_im, 2);
    x12_im = svmla_lane_f32(x12_im, y02_im, g10_re, 2);

    // G11[2]
    x12_re = svmla_lane_f32(x12_re, y12_re, g11_re, 2);
    x12_re = svmls_lane_f32(x12_re, y12_im, g11_im, 2);
    x12_im = svmla_lane_f32(x12_im, y12_re, g11_im, 2);
    x12_im = svmla_lane_f32(x12_im, y12_im, g11_re, 2);

    svint16_t x12_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x12_re, x12_im, pg8);
    svst1_s16(pg8, x1_ptr + 16, x12_bis);
    x1_ptr = x1_ptr + 24;

    p_g_real += 3;
    p_g_imag += 3;
  }
#else
  const armral_fixed_point_index num_fract_bits_y[] = {p_y_num_fract_bits[0],
                                                       p_y_num_fract_bits[1]};
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    int16x8_t y0[2];
    y0[0] = vld1q_s16(y0_ptr);
    y0[1] = vld1q_s16(y0_ptr + 8);
    float32x4_t y0_re[2];
    float32x4_t y0_im[2];
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0[0], &y0_re[0], &y0_im[0]);

    // Load G00[0] G00[1]
    float32x2_t g00_real = vld1_f32(p_g_real);
    float32x2_t g00_imag = vld1_f32(p_g_imag);

    float32x4_t x0_re[2];
    x0_re[0] = vmulq_lane_f32(y0_re[0], g00_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y0_im[0], g00_imag, 0);
    float32x4_t x0_im[2];
    x0_im[0] = vmulq_lane_f32(y0_im[0], g00_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y0_re[0], g00_imag, 0);

    int16x8_t y1[2];
    y1[0] = vld1q_s16(y1_ptr);
    y1[1] = vld1q_s16(y1_ptr + 8);
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0[1], &y0_re[1], &y0_im[1]);

    x0_re[1] = vmulq_lane_f32(y0_re[1], g00_real, 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y0_im[1], g00_imag, 1);
    x0_im[1] = vmulq_lane_f32(y0_im[1], g00_real, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y0_re[1], g00_imag, 1);

    // Load G10[0] G10[1]
    float32x2_t g10_real = vld1_f32(p_g_real + 2 * p_gstride);
    float32x2_t g10_imag = vld1_f32(p_g_imag + 2 * p_gstride);

    float32x4_t x1_re[2];
    x1_re[0] = vmulq_lane_f32(y0_re[0], g10_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y0_im[0], g10_imag, 0);
    float32x4_t x1_im[2];
    x1_im[0] = vmulq_lane_f32(y0_im[0], g10_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y0_re[0], g10_imag, 0);

    x1_re[1] = vmulq_lane_f32(y0_re[1], g10_real, 1);
    x1_re[1] = vfmsq_lane_f32(x1_re[1], y0_im[1], g10_imag, 1);
    x1_im[1] = vmulq_lane_f32(y0_im[1], g10_real, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y0_re[1], g10_imag, 1);

    float32x4_t y1_re[2];
    float32x4_t y1_im[2];
    armral_convert_i16_f32_x8(num_fract_bits_y[1], y1, y1_re, y1_im);

    // Load G01[0] G01[1]
    float32x2_t g01_real = vld1_f32(p_g_real + p_gstride);
    float32x2_t g01_imag = vld1_f32(p_g_imag + p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y1_re[0], g01_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y1_im[0], g01_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_im[0], g01_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_re[0], g01_imag, 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y1_re[1], g01_real, 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y1_im[1], g01_imag, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y1_im[1], g01_real, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y1_re[1], g01_imag, 1);

    // Load G11[0] G11[1]
    float32x2_t g11_real = vld1_f32(p_g_real + 3 * p_gstride);
    float32x2_t g11_imag = vld1_f32(p_g_imag + 3 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y1_re[0], g11_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y1_im[0], g11_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_im[0], g11_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_re[0], g11_imag, 0);

    int16x8_t x0_bis;
    int16x8_t x1_bis;
    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re[0], x0_im[0], x1_re[0],
                              x1_im[0], &x0_bis, &x1_bis);
    vst1q_s16(x0_ptr, x0_bis);
    vst1q_s16(x1_ptr, x1_bis);

    x1_re[1] = vfmaq_lane_f32(x1_re[1], y1_re[1], g11_real, 1);
    x1_re[1] = vfmsq_lane_f32(x1_re[1], y1_im[1], g11_imag, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y1_im[1], g11_real, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y1_re[1], g11_imag, 1);

    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re[1], x0_im[1], x1_re[1],
                              x1_im[1], &x0_bis, &x1_bis);
    vst1q_s16(x0_ptr + 8, x0_bis);
    vst1q_s16(x1_ptr + 8, x1_bis);

    p_g_real = p_g_real + 2;
    p_g_imag = p_g_imag + 2;

    int16x8_t y0_quadw = vld1q_s16(y0_ptr + 16);
    y0_ptr = y0_ptr + 24;
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re[0],
                              &y0_im[0]);

    // Load G00[0]
    g00_real = vld1_dup_f32(p_g_real);
    g00_imag = vld1_dup_f32(p_g_imag);

    x0_re[0] = vmulq_lane_f32(y0_re[0], g00_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y0_im[0], g00_imag, 0);
    x0_im[0] = vmulq_lane_f32(y0_im[0], g00_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y0_re[0], g00_imag, 0);

    // Load G10[0]
    g10_real = vld1_dup_f32(p_g_real + 2 * p_gstride);
    g10_imag = vld1_dup_f32(p_g_imag + 2 * p_gstride);

    x1_re[0] = vmulq_lane_f32(y0_re[0], g10_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y0_im[0], g10_imag, 0);
    x1_im[0] = vmulq_lane_f32(y0_im[0], g10_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y0_re[0], g10_imag, 0);

    int16x8_t y1_quadw = vld1q_s16(y1_ptr + 16);
    y1_ptr = y1_ptr + 24;
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re[0],
                              &y1_im[0]);

    // Load G01[0]
    g01_real = vld1_dup_f32(p_g_real + p_gstride);
    g01_imag = vld1_dup_f32(p_g_imag + p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y1_re[0], g01_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y1_im[0], g01_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_im[0], g01_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_re[0], g01_imag, 0);

    // Load G11[0]
    g11_real = vld1_dup_f32(p_g_real + 3 * p_gstride);
    g11_imag = vld1_dup_f32(p_g_imag + 3 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y1_re[0], g11_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y1_im[0], g11_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_im[0], g11_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_re[0], g11_imag, 0);

    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re[0], x0_im[0], x1_re[0],
                              x1_im[0], &x0_bis, &x1_bis);
    vst1q_s16(x0_ptr + 16, x0_bis);
    vst1q_s16(x1_ptr + 16, x1_bis);
    x0_ptr = x0_ptr + 24;
    x1_ptr = x1_ptr + 24;
    p_g_real++;
    p_g_imag++;
  }
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_2x4_4sc_f32(
    const uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    const uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    const uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    const uint32_t p_xstride, const armral_fixed_point_index num_fract_bits_x) {

  const int16_t *y0_ptr = (const int16_t *)p_y;
  const int16_t *y1_ptr = (const int16_t *)p_y + p_ystride;
  const int16_t *y2_ptr = (const int16_t *)p_y + 2 * p_ystride;
  const int16_t *y3_ptr = (const int16_t *)p_y + 3 * p_ystride;

  int16_t *x0_ptr = (int16_t *)p_x;
  int16_t *x1_ptr = (int16_t *)p_x + p_xstride;
#if ARMRAL_ARCH_SVE >= 2
  svbool_t pg8 = svptrue_pat_b16(SV_VL8);
  svbool_t pg3 = svptrue_pat_b32(SV_VL3);
  svbool_t podd = svtrn1_b16(svpfalse_b(), pg8);
  float32_t scale[] = {1. / (1U << (16 + p_y_num_fract_bits[0])),
                       1. / (1U << (16 + p_y_num_fract_bits[1])),
                       1. / (1U << (16 + p_y_num_fract_bits[2])),
                       1. / (1U << (16 + p_y_num_fract_bits[3]))};
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    // Load first y0 entry
    svint32_t y00_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr - 1));
    svint32_t y00_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr));
    svfloat32_t y00_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y00_int_re), scale[0]);
    svfloat32_t y00_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y00_int_im), scale[0]);

    // G00[0], G10[0]
    svfloat32_t g00_re = svld1_f32(pg3, &p_g_real[0 * p_gstride]);
    svfloat32_t g00_im = svld1_f32(pg3, &p_g_imag[0 * p_gstride]);
    svfloat32_t x00_re = svmul_lane_f32(y00_re, g00_re, 0);
    x00_re = svmls_lane_f32(x00_re, y00_im, g00_im, 0);
    svfloat32_t x00_im = svmul_lane_f32(y00_re, g00_im, 0);
    x00_im = svmla_lane_f32(x00_im, y00_im, g00_re, 0);
    svfloat32_t g10_re = svld1_f32(pg3, &p_g_real[4 * p_gstride]);
    svfloat32_t g10_im = svld1_f32(pg3, &p_g_imag[4 * p_gstride]);
    svfloat32_t x10_re = svmul_lane_f32(y00_re, g10_re, 0);
    x10_re = svmls_lane_f32(x10_re, y00_im, g10_im, 0);
    svfloat32_t x10_im = svmul_lane_f32(y00_re, g10_im, 0);
    x10_im = svmla_lane_f32(x10_im, y00_im, g10_re, 0);

    // Load second y0 entry
    svint32_t y01_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 7));
    svint32_t y01_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 8));
    svfloat32_t y01_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y01_int_re), scale[0]);
    svfloat32_t y01_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y01_int_im), scale[0]);

    // G00[1], G10[1]
    svfloat32_t x01_re = svmul_lane_f32(y01_re, g00_re, 1);
    x01_re = svmls_lane_f32(x01_re, y01_im, g00_im, 1);
    svfloat32_t x01_im = svmul_lane_f32(y01_re, g00_im, 1);
    x01_im = svmla_lane_f32(x01_im, y01_im, g00_re, 1);
    svfloat32_t x11_re = svmul_lane_f32(y01_re, g10_re, 1);
    x11_re = svmls_lane_f32(x11_re, y01_im, g10_im, 1);
    svfloat32_t x11_im = svmul_lane_f32(y01_re, g10_im, 1);
    x11_im = svmla_lane_f32(x11_im, y01_im, g10_re, 1);

    // Load third y0 entry
    svint32_t y02_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 15));
    svint32_t y02_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 16));
    svfloat32_t y02_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y02_int_re), scale[0]);
    svfloat32_t y02_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y02_int_im), scale[0]);
    y0_ptr = y0_ptr + 24;

    // G00[2], G10[2]
    svfloat32_t x02_re = svmul_lane_f32(y02_re, g00_re, 2);
    x02_re = svmls_lane_f32(x02_re, y02_im, g00_im, 2);
    svfloat32_t x02_im = svmul_lane_f32(y02_re, g00_im, 2);
    x02_im = svmla_lane_f32(x02_im, y02_im, g00_re, 2);
    svfloat32_t x12_re = svmul_lane_f32(y02_re, g10_re, 2);
    x12_re = svmls_lane_f32(x12_re, y02_im, g10_im, 2);
    svfloat32_t x12_im = svmul_lane_f32(y02_re, g10_im, 2);
    x12_im = svmla_lane_f32(x12_im, y02_im, g10_re, 2);

    // Load first y1 entry
    svint32_t y10_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr - 1));
    svint32_t y10_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr));
    svfloat32_t y10_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y10_int_re), scale[1]);
    svfloat32_t y10_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y10_int_im), scale[1]);

    // G01[0], G11[0]
    svfloat32_t g01_re = svld1_f32(pg3, &p_g_real[1 * p_gstride]);
    svfloat32_t g01_im = svld1_f32(pg3, &p_g_imag[1 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y10_re, g01_re, 0);
    x00_re = svmls_lane_f32(x00_re, y10_im, g01_im, 0);
    x00_im = svmla_lane_f32(x00_im, y10_re, g01_im, 0);
    x00_im = svmla_lane_f32(x00_im, y10_im, g01_re, 0);
    svfloat32_t g11_re = svld1_f32(pg3, &p_g_real[5 * p_gstride]);
    svfloat32_t g11_im = svld1_f32(pg3, &p_g_imag[5 * p_gstride]);
    x10_re = svmla_lane_f32(x10_re, y10_re, g11_re, 0);
    x10_re = svmls_lane_f32(x10_re, y10_im, g11_im, 0);
    x10_im = svmla_lane_f32(x10_im, y10_re, g11_im, 0);
    x10_im = svmla_lane_f32(x10_im, y10_im, g11_re, 0);

    // Load second y1 entry
    svint32_t y11_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 7));
    svint32_t y11_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 8));
    svfloat32_t y11_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y11_int_re), scale[1]);
    svfloat32_t y11_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y11_int_im), scale[1]);

    // G01[1], G11[1]
    x11_re = svmla_lane_f32(x11_re, y11_re, g11_re, 1);
    x11_re = svmls_lane_f32(x11_re, y11_im, g11_im, 1);
    x11_im = svmla_lane_f32(x11_im, y11_re, g11_im, 1);
    x11_im = svmla_lane_f32(x11_im, y11_im, g11_re, 1);
    x01_re = svmla_lane_f32(x01_re, y11_re, g01_re, 1);
    x01_re = svmls_lane_f32(x01_re, y11_im, g01_im, 1);
    x01_im = svmla_lane_f32(x01_im, y11_re, g01_im, 1);
    x01_im = svmla_lane_f32(x01_im, y11_im, g01_re, 1);

    // Load third y1 entry
    svint32_t y12_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 15));
    svint32_t y12_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 16));
    svfloat32_t y12_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y12_int_re), scale[1]);
    svfloat32_t y12_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y12_int_im), scale[1]);
    y1_ptr = y1_ptr + 24;

    // G01[2], G11[2]
    x12_re = svmla_lane_f32(x12_re, y12_re, g11_re, 2);
    x12_re = svmls_lane_f32(x12_re, y12_im, g11_im, 2);
    x12_im = svmla_lane_f32(x12_im, y12_re, g11_im, 2);
    x12_im = svmla_lane_f32(x12_im, y12_im, g11_re, 2);
    x02_re = svmla_lane_f32(x02_re, y12_re, g01_re, 2);
    x02_re = svmls_lane_f32(x02_re, y12_im, g01_im, 2);
    x02_im = svmla_lane_f32(x02_im, y12_re, g01_im, 2);
    x02_im = svmla_lane_f32(x02_im, y12_im, g01_re, 2);

    // Load first y2 entry
    svint32_t y20_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr - 1));
    svint32_t y20_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr));
    svfloat32_t y20_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y20_int_re), scale[2]);
    svfloat32_t y20_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y20_int_im), scale[2]);

    // G02[0], G12[0]
    svfloat32_t g02_re = svld1_f32(pg3, &p_g_real[2 * p_gstride]);
    svfloat32_t g02_im = svld1_f32(pg3, &p_g_imag[2 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y20_re, g02_re, 0);
    x00_re = svmls_lane_f32(x00_re, y20_im, g02_im, 0);
    x00_im = svmla_lane_f32(x00_im, y20_re, g02_im, 0);
    x00_im = svmla_lane_f32(x00_im, y20_im, g02_re, 0);
    svfloat32_t g12_re = svld1_f32(pg3, &p_g_real[6 * p_gstride]);
    svfloat32_t g12_im = svld1_f32(pg3, &p_g_imag[6 * p_gstride]);
    x10_re = svmla_lane_f32(x10_re, y20_re, g12_re, 0);
    x10_re = svmls_lane_f32(x10_re, y20_im, g12_im, 0);
    x10_im = svmla_lane_f32(x10_im, y20_re, g12_im, 0);
    x10_im = svmla_lane_f32(x10_im, y20_im, g12_re, 0);

    // Load second y2 entry
    svint32_t y21_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 7));
    svint32_t y21_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 8));
    svfloat32_t y21_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y21_int_re), scale[2]);
    svfloat32_t y21_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y21_int_im), scale[2]);

    // G02[1], G12[1]
    x01_re = svmla_lane_f32(x01_re, y21_re, g02_re, 1);
    x01_re = svmls_lane_f32(x01_re, y21_im, g02_im, 1);
    x01_im = svmla_lane_f32(x01_im, y21_re, g02_im, 1);
    x01_im = svmla_lane_f32(x01_im, y21_im, g02_re, 1);
    x11_re = svmla_lane_f32(x11_re, y21_re, g12_re, 1);
    x11_re = svmls_lane_f32(x11_re, y21_im, g12_im, 1);
    x11_im = svmla_lane_f32(x11_im, y21_re, g12_im, 1);
    x11_im = svmla_lane_f32(x11_im, y21_im, g12_re, 1);

    // Load third y2 entry
    svint32_t y22_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 15));
    svint32_t y22_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 16));
    svfloat32_t y22_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y22_int_re), scale[2]);
    svfloat32_t y22_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y22_int_im), scale[2]);
    y2_ptr = y2_ptr + 24;

    // G02[2], G12[2]
    x02_re = svmla_lane_f32(x02_re, y22_re, g02_re, 2);
    x02_re = svmls_lane_f32(x02_re, y22_im, g02_im, 2);
    x02_im = svmla_lane_f32(x02_im, y22_re, g02_im, 2);
    x02_im = svmla_lane_f32(x02_im, y22_im, g02_re, 2);
    x12_re = svmla_lane_f32(x12_re, y22_re, g12_re, 2);
    x12_re = svmls_lane_f32(x12_re, y22_im, g12_im, 2);
    x12_im = svmla_lane_f32(x12_im, y22_re, g12_im, 2);
    x12_im = svmla_lane_f32(x12_im, y22_im, g12_re, 2);

    // Load first y3 entry
    svint32_t y30_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr - 1));
    svint32_t y30_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr));
    svfloat32_t y30_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y30_int_re), scale[3]);
    svfloat32_t y30_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y30_int_im), scale[3]);

    // G03[0], G13[0]
    svfloat32_t g03_re = svld1_f32(pg3, &p_g_real[3 * p_gstride]);
    svfloat32_t g03_im = svld1_f32(pg3, &p_g_imag[3 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y30_re, g03_re, 0);
    x00_re = svmls_lane_f32(x00_re, y30_im, g03_im, 0);
    x00_im = svmla_lane_f32(x00_im, y30_re, g03_im, 0);
    x00_im = svmla_lane_f32(x00_im, y30_im, g03_re, 0);
    svfloat32_t g13_re = svld1_f32(pg3, &p_g_real[7 * p_gstride]);
    svfloat32_t g13_im = svld1_f32(pg3, &p_g_imag[7 * p_gstride]);
    x10_re = svmla_lane_f32(x10_re, y30_re, g13_re, 0);
    x10_re = svmls_lane_f32(x10_re, y30_im, g13_im, 0);
    x10_im = svmla_lane_f32(x10_im, y30_re, g13_im, 0);
    x10_im = svmla_lane_f32(x10_im, y30_im, g13_re, 0);

    // Load second y3 entry
    svint32_t y31_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 7));
    svint32_t y31_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 8));
    svfloat32_t y31_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y31_int_re), scale[3]);
    svfloat32_t y31_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y31_int_im), scale[3]);

    // G03[1], G13[1]
    x11_re = svmla_lane_f32(x11_re, y31_re, g13_re, 1);
    x11_re = svmls_lane_f32(x11_re, y31_im, g13_im, 1);
    x11_im = svmla_lane_f32(x11_im, y31_re, g13_im, 1);
    x11_im = svmla_lane_f32(x11_im, y31_im, g13_re, 1);
    x01_re = svmla_lane_f32(x01_re, y31_re, g03_re, 1);
    x01_re = svmls_lane_f32(x01_re, y31_im, g03_im, 1);
    x01_im = svmla_lane_f32(x01_im, y31_re, g03_im, 1);
    x01_im = svmla_lane_f32(x01_im, y31_im, g03_re, 1);

    // Load third y3 entry
    svint32_t y32_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 15));
    svint32_t y32_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 16));
    svfloat32_t y32_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y32_int_re), scale[3]);
    svfloat32_t y32_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y32_int_im), scale[3]);
    y3_ptr = y3_ptr + 24;

    // G03[2]
    x02_re = svmla_lane_f32(x02_re, y32_re, g03_re, 2);
    x02_re = svmls_lane_f32(x02_re, y32_im, g03_im, 2);
    x02_im = svmla_lane_f32(x02_im, y32_re, g03_im, 2);
    x02_im = svmla_lane_f32(x02_im, y32_im, g03_re, 2);

    // Store x0 entries
    svint16_t x00_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x00_re, x00_im, pg8);
    svst1_s16(pg8, x0_ptr, x00_bis);
    svint16_t x01_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x01_re, x01_im, pg8);
    svst1_s16(pg8, x0_ptr + 8, x01_bis);
    svint16_t x02_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x02_re, x02_im, pg8);
    svst1_s16(pg8, x0_ptr + 16, x02_bis);
    x0_ptr = x0_ptr + 24;

    // G13[2]
    x12_re = svmla_lane_f32(x12_re, y32_re, g13_re, 2);
    x12_re = svmls_lane_f32(x12_re, y32_im, g13_im, 2);
    x12_im = svmla_lane_f32(x12_im, y32_re, g13_im, 2);
    x12_im = svmla_lane_f32(x12_im, y32_im, g13_re, 2);

    // Store x1 entries
    svint16_t x10_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x10_re, x10_im, pg8);
    svst1_s16(pg8, x1_ptr, x10_bis);
    svint16_t x11_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x11_re, x11_im, pg8);
    svst1_s16(pg8, x1_ptr + 8, x11_bis);
    svint16_t x12_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x12_re, x12_im, pg8);
    svst1_s16(pg8, x1_ptr + 16, x12_bis);
    x1_ptr = x1_ptr + 24;

    p_g_real += 3;
    p_g_imag += 3;
  }
#else
  const armral_fixed_point_index num_fract_bits_y[] = {
      p_y_num_fract_bits[0], p_y_num_fract_bits[1], p_y_num_fract_bits[2],
      p_y_num_fract_bits[3]};
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    int16x8_t y0[2];
    y0[0] = vld1q_s16(y0_ptr);
    y0[1] = vld1q_s16(y0_ptr + 8);
    y0_ptr = y0_ptr + 16;
    int16x8_t y1[2];
    y1[0] = vld1q_s16(y1_ptr);
    y1[1] = vld1q_s16(y1_ptr + 8);
    y1_ptr = y1_ptr + 16;
    float32x4_t y0_re[2];
    float32x4_t y0_im[2];
    armral_convert_i16_f32_x8(num_fract_bits_y[0], y0, y0_re, y0_im);
    float32x4_t y1_re[2];
    float32x4_t y1_im[2];
    armral_convert_i16_f32_x8(num_fract_bits_y[1], y1, y1_re, y1_im);
    // Load G00[0] G00[1]
    float32x2_t g00_real = vld1_f32(p_g_real);
    float32x2_t g00_imag = vld1_f32(p_g_imag);

    float32x4_t x0_re[2];
    x0_re[0] = vmulq_lane_f32(y0_re[0], g00_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y0_im[0], g00_imag, 0);
    float32x4_t x0_im[2];
    x0_im[0] = vmulq_lane_f32(y0_im[0], g00_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y0_re[0], g00_imag, 0);

    x0_re[1] = vmulq_lane_f32(y0_re[1], g00_real, 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y0_im[1], g00_imag, 1);
    x0_im[1] = vmulq_lane_f32(y0_im[1], g00_real, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y0_re[1], g00_imag, 1);

    // Load G10[0] G10[1]
    float32x2_t g10_real = vld1_f32(p_g_real + 4 * p_gstride);
    float32x2_t g10_imag = vld1_f32(p_g_imag + 4 * p_gstride);

    float32x4_t x1_re[2];
    x1_re[0] = vmulq_lane_f32(y0_re[0], g10_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y0_im[0], g10_imag, 0);
    float32x4_t x1_im[2];
    x1_im[0] = vmulq_lane_f32(y0_im[0], g10_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y0_re[0], g10_imag, 0);

    x1_re[1] = vmulq_lane_f32(y0_re[1], g10_real, 1);
    x1_re[1] = vfmsq_lane_f32(x1_re[1], y0_im[1], g10_imag, 1);
    x1_im[1] = vmulq_lane_f32(y0_im[1], g10_real, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y0_re[1], g10_imag, 1);

    // Load G01[0] G01[1]
    float32x2_t g01_real = vld1_f32(p_g_real + p_gstride);
    float32x2_t g01_imag = vld1_f32(p_g_imag + p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y1_re[0], g01_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y1_im[0], g01_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_im[0], g01_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_re[0], g01_imag, 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y1_re[1], g01_real, 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y1_im[1], g01_imag, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y1_im[1], g01_real, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y1_re[1], g01_imag, 1);

    // Load G11[0] G11[1]
    float32x2_t g11_real = vld1_f32(p_g_real + 5 * p_gstride);
    float32x2_t g11_imag = vld1_f32(p_g_imag + 5 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y1_re[0], g11_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y1_im[0], g11_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_im[0], g11_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_re[0], g11_imag, 0);

    x1_re[1] = vfmaq_lane_f32(x1_re[1], y1_re[1], g11_real, 1);
    x1_re[1] = vfmsq_lane_f32(x1_re[1], y1_im[1], g11_imag, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y1_im[1], g11_real, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y1_re[1], g11_imag, 1);

    int16x8_t y2[2];
    y2[0] = vld1q_s16(y2_ptr);
    y2[1] = vld1q_s16(y2_ptr + 8);
    y2_ptr = y2_ptr + 16;
    int16x8_t y3[2];
    y3[0] = vld1q_s16(y3_ptr);
    y3[1] = vld1q_s16(y3_ptr + 8);
    y3_ptr = y3_ptr + 16;
    float32x4_t y2_re[2];
    float32x4_t y2_im[2];
    armral_convert_i16_f32_x8(num_fract_bits_y[2], y2, y2_re, y2_im);
    float32x4_t y3_re[2];
    float32x4_t y3_im[2];
    armral_convert_i16_f32_x8(num_fract_bits_y[3], y3, y3_re, y3_im);

    // Load G02[0] G02[1]
    float32x2_t g02_real = vld1_f32(p_g_real + 2 * p_gstride);
    float32x2_t g02_imag = vld1_f32(p_g_imag + 2 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y2_re[0], g02_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y2_im[0], g02_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y2_im[0], g02_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y2_re[0], g02_imag, 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y2_re[1], g02_real, 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y2_im[1], g02_imag, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y2_im[1], g02_real, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y2_re[1], g02_imag, 1);

    // Load G12[0] G12[1]
    float32x2_t g12_real = vld1_f32(p_g_real + 6 * p_gstride);
    float32x2_t g12_imag = vld1_f32(p_g_imag + 6 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y2_re[0], g12_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y2_im[0], g12_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y2_im[0], g12_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y2_re[0], g12_imag, 0);

    x1_re[1] = vfmaq_lane_f32(x1_re[1], y2_re[1], g12_real, 1);
    x1_re[1] = vfmsq_lane_f32(x1_re[1], y2_im[1], g12_imag, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y2_im[1], g12_real, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y2_re[1], g12_imag, 1);

    // Load G03[0] G03[1]
    float32x2_t g03_real = vld1_f32(p_g_real + 3 * p_gstride);
    float32x2_t g03_imag = vld1_f32(p_g_imag + 3 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y3_re[0], g03_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y3_im[0], g03_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y3_im[0], g03_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y3_re[0], g03_imag, 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y3_re[1], g03_real, 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y3_im[1], g03_imag, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y3_im[1], g03_real, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y3_re[1], g03_imag, 1);

    // Load G13[0] G13[1]
    float32x2_t g13_real = vld1_f32(p_g_real + 7 * p_gstride);
    float32x2_t g13_imag = vld1_f32(p_g_imag + 7 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y3_re[0], g13_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y3_im[0], g13_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y3_im[0], g13_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y3_re[0], g13_imag, 0);

    x1_re[1] = vfmaq_lane_f32(x1_re[1], y3_re[1], g13_real, 1);
    x1_re[1] = vfmsq_lane_f32(x1_re[1], y3_im[1], g13_imag, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y3_im[1], g13_real, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y3_re[1], g13_imag, 1);

    int16x8_t x0[2];
    int16x8_t x1[2];
    armral_convert_f32_i16_x8(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im, x0,
                              x1);
    vst1q_s16(x0_ptr, x0[0]);
    vst1q_s16(x0_ptr + 8, x0[1]);
    vst1q_s16(x1_ptr, x1[0]);
    vst1q_s16(x1_ptr + 8, x1[1]);
    p_g_real = p_g_real + 2;
    p_g_imag = p_g_imag + 2;
    x0_ptr = x0_ptr + 16;
    x1_ptr = x1_ptr + 16;

    int16x8_t y0_quadw = vld1q_s16(y0_ptr);
    y0_ptr = y0_ptr + 8;
    int16x8_t y1_quadw = vld1q_s16(y1_ptr);
    y1_ptr = y1_ptr + 8;

    g00_real = vld1_dup_f32(p_g_real);
    g00_imag = vld1_dup_f32(p_g_imag);
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re[0],
                              &y0_im[0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re[0],
                              &y1_im[0]);
    x0_re[0] = vmulq_lane_f32(y0_re[0], g00_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y0_im[0], g00_imag, 0);
    x0_im[0] = vmulq_lane_f32(y0_im[0], g00_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y0_re[0], g00_imag, 0);

    // Load G10[0]
    g10_real = vld1_dup_f32(p_g_real + 4 * p_gstride);
    g10_imag = vld1_dup_f32(p_g_imag + 4 * p_gstride);

    x1_re[0] = vmulq_lane_f32(y0_re[0], g10_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y0_im[0], g10_imag, 0);
    x1_im[0] = vmulq_lane_f32(y0_im[0], g10_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y0_re[0], g10_imag, 0);

    // Load G01[0]
    int16x8_t y2_quadw = vld1q_s16(y2_ptr);
    y2_ptr = y2_ptr + 8;
    g01_real = vld1_dup_f32(p_g_real + p_gstride);
    g01_imag = vld1_dup_f32(p_g_imag + p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y1_re[0], g01_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y1_im[0], g01_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_im[0], g01_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_re[0], g01_imag, 0);

    // Load G11[0]
    g11_real = vld1_dup_f32(p_g_real + 5 * p_gstride);
    g11_imag = vld1_dup_f32(p_g_imag + 5 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y1_re[0], g11_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y1_im[0], g11_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_im[0], g11_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_re[0], g11_imag, 0);

    // Load G02[0]
    int16x8_t y3_quadw = vld1q_s16(y3_ptr);
    y3_ptr = y3_ptr + 8;
    armral_convert_i16_f32_x4(num_fract_bits_y[2], y2_quadw, &y2_re[0],
                              &y2_im[0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y3_quadw, &y3_re[0],
                              &y3_im[0]);
    g02_real = vld1_dup_f32(p_g_real + 2 * p_gstride);
    g02_imag = vld1_dup_f32(p_g_imag + 2 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y2_re[0], g02_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y2_im[0], g02_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y2_im[0], g02_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y2_re[0], g02_imag, 0);

    // Load G12[0]
    g12_real = vld1_dup_f32(p_g_real + 6 * p_gstride);
    g12_imag = vld1_dup_f32(p_g_imag + 6 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y2_re[0], g12_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y2_im[0], g12_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y2_im[0], g12_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y2_re[0], g12_imag, 0);

    // Load G03[0]
    g03_real = vld1_dup_f32(p_g_real + 3 * p_gstride);
    g03_imag = vld1_dup_f32(p_g_imag + 3 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y3_re[0], g03_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y3_im[0], g03_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y3_im[0], g03_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y3_re[0], g03_imag, 0);

    // Load G13[0]
    g13_real = vld1_dup_f32(p_g_real + 7 * p_gstride);
    g13_imag = vld1_dup_f32(p_g_imag + 7 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y3_re[0], g13_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y3_im[0], g13_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y3_im[0], g13_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y3_re[0], g13_imag, 0);

    int16x8_t x0_bis;
    int16x8_t x1_bis;
    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re[0], x0_im[0], x1_re[0],
                              x1_im[0], &x0_bis, &x1_bis);

    vst1q_s16(x0_ptr, x0_bis);
    vst1q_s16(x1_ptr, x1_bis);
    x0_ptr = x0_ptr + 8;
    x1_ptr = x1_ptr + 8;
    p_g_real++;
    p_g_imag++;
  }
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_4x4_4sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride, const armral_fixed_point_index *p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    armral_fixed_point_index num_fract_bits_x) {

  const int16_t *y0_ptr = (const int16_t *)p_y;
  const int16_t *y1_ptr = (const int16_t *)p_y + p_ystride;
  const int16_t *y2_ptr = (const int16_t *)p_y + 2 * p_ystride;
  const int16_t *y3_ptr = (const int16_t *)p_y + 3 * p_ystride;

  int16_t *x0_ptr = (int16_t *)p_x;
  int16_t *x1_ptr = (int16_t *)p_x + p_xstride;
  int16_t *x2_ptr = (int16_t *)p_x + 2 * p_xstride;
  int16_t *x3_ptr = (int16_t *)p_x + 3 * p_xstride;
#if ARMRAL_ARCH_SVE >= 2
  svbool_t pg8 = svptrue_pat_b16(SV_VL8);
  svbool_t pg3 = svptrue_pat_b32(SV_VL3);
  svbool_t podd = svtrn1_b16(svpfalse_b(), pg8);
  float32_t scale[] = {1. / (1U << (16 + p_y_num_fract_bits[0])),
                       1. / (1U << (16 + p_y_num_fract_bits[1])),
                       1. / (1U << (16 + p_y_num_fract_bits[2])),
                       1. / (1U << (16 + p_y_num_fract_bits[3]))};
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    // Load first y0 entry
    svint32_t y00_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr - 1));
    svint32_t y00_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr));
    svfloat32_t y00_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y00_int_re), scale[0]);
    svfloat32_t y00_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y00_int_im), scale[0]);

    // Load second y0 entry
    svint32_t y01_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 7));
    svint32_t y01_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 8));
    svfloat32_t y01_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y01_int_re), scale[0]);
    svfloat32_t y01_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y01_int_im), scale[0]);

    // G00[0]
    svfloat32_t g00_re = svld1_f32(pg3, &p_g_real[0 * p_gstride]);
    svfloat32_t g00_im = svld1_f32(pg3, &p_g_imag[0 * p_gstride]);
    svfloat32_t x00_re = svmul_lane_f32(y00_re, g00_re, 0);
    x00_re = svmls_lane_f32(x00_re, y00_im, g00_im, 0);
    svfloat32_t x00_im = svmul_lane_f32(y00_re, g00_im, 0);
    x00_im = svmla_lane_f32(x00_im, y00_im, g00_re, 0);

    // G00[1]
    svfloat32_t x01_re = svmul_lane_f32(y01_re, g00_re, 1);
    x01_re = svmls_lane_f32(x01_re, y01_im, g00_im, 1);
    svfloat32_t x01_im = svmul_lane_f32(y01_re, g00_im, 1);
    x01_im = svmla_lane_f32(x01_im, y01_im, g00_re, 1);

    // G10[0]
    svfloat32_t g10_re = svld1_f32(pg3, &p_g_real[4 * p_gstride]);
    svfloat32_t g10_im = svld1_f32(pg3, &p_g_imag[4 * p_gstride]);
    svfloat32_t x10_re = svmul_lane_f32(y00_re, g10_re, 0);
    x10_re = svmls_lane_f32(x10_re, y00_im, g10_im, 0);
    svfloat32_t x10_im = svmul_lane_f32(y00_re, g10_im, 0);
    x10_im = svmla_lane_f32(x10_im, y00_im, g10_re, 0);

    // G10[1]
    svfloat32_t x11_re = svmul_lane_f32(y01_re, g10_re, 1);
    x11_re = svmls_lane_f32(x11_re, y01_im, g10_im, 1);
    svfloat32_t x11_im = svmul_lane_f32(y01_re, g10_im, 1);
    x11_im = svmla_lane_f32(x11_im, y01_im, g10_re, 1);

    // Load first y1 entry
    svint32_t y10_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr - 1));
    svint32_t y10_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr));
    svfloat32_t y10_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y10_int_re), scale[1]);
    svfloat32_t y10_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y10_int_im), scale[1]);

    // Load second y1 entry
    svint32_t y11_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 7));
    svint32_t y11_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 8));
    svfloat32_t y11_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y11_int_re), scale[1]);
    svfloat32_t y11_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y11_int_im), scale[1]);

    // G01[0]
    svfloat32_t g01_re = svld1_f32(pg3, &p_g_real[1 * p_gstride]);
    svfloat32_t g01_im = svld1_f32(pg3, &p_g_imag[1 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y10_re, g01_re, 0);
    x00_re = svmls_lane_f32(x00_re, y10_im, g01_im, 0);
    x00_im = svmla_lane_f32(x00_im, y10_re, g01_im, 0);
    x00_im = svmla_lane_f32(x00_im, y10_im, g01_re, 0);

    // G01[1]
    x01_re = svmla_lane_f32(x01_re, y11_re, g01_re, 1);
    x01_re = svmls_lane_f32(x01_re, y11_im, g01_im, 1);
    x01_im = svmla_lane_f32(x01_im, y11_re, g01_im, 1);
    x01_im = svmla_lane_f32(x01_im, y11_im, g01_re, 1);

    // G11[0]
    svfloat32_t g11_re = svld1_f32(pg3, &p_g_real[5 * p_gstride]);
    svfloat32_t g11_im = svld1_f32(pg3, &p_g_imag[5 * p_gstride]);
    x10_re = svmla_lane_f32(x10_re, y10_re, g11_re, 0);
    x10_re = svmls_lane_f32(x10_re, y10_im, g11_im, 0);
    x10_im = svmla_lane_f32(x10_im, y10_re, g11_im, 0);
    x10_im = svmla_lane_f32(x10_im, y10_im, g11_re, 0);

    // G11[1]
    x11_re = svmla_lane_f32(x11_re, y11_re, g11_re, 1);
    x11_re = svmls_lane_f32(x11_re, y11_im, g11_im, 1);
    x11_im = svmla_lane_f32(x11_im, y11_re, g11_im, 1);
    x11_im = svmla_lane_f32(x11_im, y11_im, g11_re, 1);

    // Load first y2 entry
    svint32_t y20_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr - 1));
    svint32_t y20_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr));
    svfloat32_t y20_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y20_int_re), scale[2]);
    svfloat32_t y20_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y20_int_im), scale[2]);

    // Load second y2 entry
    svint32_t y21_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 7));
    svint32_t y21_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 8));
    svfloat32_t y21_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y21_int_re), scale[2]);
    svfloat32_t y21_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y21_int_im), scale[2]);

    // G02[0]
    svfloat32_t g02_re = svld1_f32(pg3, &p_g_real[2 * p_gstride]);
    svfloat32_t g02_im = svld1_f32(pg3, &p_g_imag[2 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y20_re, g02_re, 0);
    x00_re = svmls_lane_f32(x00_re, y20_im, g02_im, 0);
    x00_im = svmla_lane_f32(x00_im, y20_re, g02_im, 0);
    x00_im = svmla_lane_f32(x00_im, y20_im, g02_re, 0);

    // G02[1]
    x01_re = svmla_lane_f32(x01_re, y21_re, g02_re, 1);
    x01_re = svmls_lane_f32(x01_re, y21_im, g02_im, 1);
    x01_im = svmla_lane_f32(x01_im, y21_re, g02_im, 1);
    x01_im = svmla_lane_f32(x01_im, y21_im, g02_re, 1);

    // Load first y3 entry
    svint32_t y30_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr - 1));
    svint32_t y30_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr));
    svfloat32_t y30_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y30_int_re), scale[3]);
    svfloat32_t y30_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y30_int_im), scale[3]);

    // Load second y3 entry
    svint32_t y31_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 7));
    svint32_t y31_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 8));
    svfloat32_t y31_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y31_int_re), scale[3]);
    svfloat32_t y31_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y31_int_im), scale[3]);

    // G03[0]
    svfloat32_t g03_re = svld1_f32(pg3, &p_g_real[3 * p_gstride]);
    svfloat32_t g03_im = svld1_f32(pg3, &p_g_imag[3 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y30_re, g03_re, 0);
    x00_re = svmls_lane_f32(x00_re, y30_im, g03_im, 0);
    x00_im = svmla_lane_f32(x00_im, y30_re, g03_im, 0);
    x00_im = svmla_lane_f32(x00_im, y30_im, g03_re, 0);

    // G03[1]
    x01_re = svmla_lane_f32(x01_re, y31_re, g03_re, 1);
    x01_re = svmls_lane_f32(x01_re, y31_im, g03_im, 1);
    x01_im = svmla_lane_f32(x01_im, y31_re, g03_im, 1);
    x01_im = svmla_lane_f32(x01_im, y31_im, g03_re, 1);

    // Store first and second x0 entry
    svint16_t x00_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x00_re, x00_im, pg8);
    svst1_s16(pg8, x0_ptr, x00_bis);
    svint16_t x01_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x01_re, x01_im, pg8);
    svst1_s16(pg8, x0_ptr + 8, x01_bis);

    // G12[0]
    svfloat32_t g12_re = svld1_f32(pg3, &p_g_real[6 * p_gstride]);
    svfloat32_t g12_im = svld1_f32(pg3, &p_g_imag[6 * p_gstride]);
    x10_re = svmla_lane_f32(x10_re, y20_re, g12_re, 0);
    x10_re = svmls_lane_f32(x10_re, y20_im, g12_im, 0);
    x10_im = svmla_lane_f32(x10_im, y20_re, g12_im, 0);
    x10_im = svmla_lane_f32(x10_im, y20_im, g12_re, 0);

    // G12[1]
    x11_re = svmla_lane_f32(x11_re, y21_re, g12_re, 1);
    x11_re = svmls_lane_f32(x11_re, y21_im, g12_im, 1);
    x11_im = svmla_lane_f32(x11_im, y21_re, g12_im, 1);
    x11_im = svmla_lane_f32(x11_im, y21_im, g12_re, 1);

    // G13[0]
    svfloat32_t g13_re = svld1_f32(pg3, &p_g_real[7 * p_gstride]);
    svfloat32_t g13_im = svld1_f32(pg3, &p_g_imag[7 * p_gstride]);
    x10_re = svmla_lane_f32(x10_re, y30_re, g13_re, 0);
    x10_re = svmls_lane_f32(x10_re, y30_im, g13_im, 0);
    x10_im = svmla_lane_f32(x10_im, y30_re, g13_im, 0);
    x10_im = svmla_lane_f32(x10_im, y30_im, g13_re, 0);

    // G13[1]
    x11_re = svmla_lane_f32(x11_re, y31_re, g13_re, 1);
    x11_re = svmls_lane_f32(x11_re, y31_im, g13_im, 1);
    x11_im = svmla_lane_f32(x11_im, y31_re, g13_im, 1);
    x11_im = svmla_lane_f32(x11_im, y31_im, g13_re, 1);

    // Store first and second x1 entry
    svint16_t x10_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x10_re, x10_im, pg8);
    svst1_s16(pg8, x1_ptr, x10_bis);
    svint16_t x11_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x11_re, x11_im, pg8);
    svst1_s16(pg8, x1_ptr + 8, x11_bis);

    // G20[0]
    svfloat32_t g20_re = svld1_f32(pg3, &p_g_real[8 * p_gstride]);
    svfloat32_t g20_im = svld1_f32(pg3, &p_g_imag[8 * p_gstride]);
    svfloat32_t x20_re = svmul_lane_f32(y00_re, g20_re, 0);
    x20_re = svmls_lane_f32(x20_re, y00_im, g20_im, 0);
    svfloat32_t x20_im = svmul_lane_f32(y00_re, g20_im, 0);
    x20_im = svmla_lane_f32(x20_im, y00_im, g20_re, 0);

    // G20[1]
    svfloat32_t x21_re = svmul_lane_f32(y01_re, g20_re, 1);
    x21_re = svmls_lane_f32(x21_re, y01_im, g20_im, 1);
    svfloat32_t x21_im = svmul_lane_f32(y01_re, g20_im, 1);
    x21_im = svmla_lane_f32(x21_im, y01_im, g20_re, 1);

    // G30[0]
    svfloat32_t g30_re = svld1_f32(pg3, &p_g_real[12 * p_gstride]);
    svfloat32_t g30_im = svld1_f32(pg3, &p_g_imag[12 * p_gstride]);
    svfloat32_t x30_re = svmul_lane_f32(y00_re, g30_re, 0);
    x30_re = svmls_lane_f32(x30_re, y00_im, g30_im, 0);
    svfloat32_t x30_im = svmul_lane_f32(y00_re, g30_im, 0);
    x30_im = svmla_lane_f32(x30_im, y00_im, g30_re, 0);

    // G30[1]
    svfloat32_t x31_re = svmul_lane_f32(y01_re, g30_re, 1);
    x31_re = svmls_lane_f32(x31_re, y01_im, g30_im, 1);
    svfloat32_t x31_im = svmul_lane_f32(y01_re, g30_im, 1);
    x31_im = svmla_lane_f32(x31_im, y01_im, g30_re, 1);

    // G21[0]
    svfloat32_t g21_re = svld1_f32(pg3, &p_g_real[9 * p_gstride]);
    svfloat32_t g21_im = svld1_f32(pg3, &p_g_imag[9 * p_gstride]);
    x20_re = svmla_lane_f32(x20_re, y10_re, g21_re, 0);
    x20_re = svmls_lane_f32(x20_re, y10_im, g21_im, 0);
    x20_im = svmla_lane_f32(x20_im, y10_re, g21_im, 0);
    x20_im = svmla_lane_f32(x20_im, y10_im, g21_re, 0);

    // G21[1]
    x21_re = svmla_lane_f32(x21_re, y11_re, g21_re, 1);
    x21_re = svmls_lane_f32(x21_re, y11_im, g21_im, 1);
    x21_im = svmla_lane_f32(x21_im, y11_re, g21_im, 1);
    x21_im = svmla_lane_f32(x21_im, y11_im, g21_re, 1);

    // G31[0]
    svfloat32_t g31_re = svld1_f32(pg3, &p_g_real[13 * p_gstride]);
    svfloat32_t g31_im = svld1_f32(pg3, &p_g_imag[13 * p_gstride]);
    x30_re = svmla_lane_f32(x30_re, y10_re, g31_re, 0);
    x30_re = svmls_lane_f32(x30_re, y10_im, g31_im, 0);
    x30_im = svmla_lane_f32(x30_im, y10_re, g31_im, 0);
    x30_im = svmla_lane_f32(x30_im, y10_im, g31_re, 0);

    // G31[1]
    x31_re = svmla_lane_f32(x31_re, y11_re, g31_re, 1);
    x31_re = svmls_lane_f32(x31_re, y11_im, g31_im, 1);
    x31_im = svmla_lane_f32(x31_im, y11_re, g31_im, 1);
    x31_im = svmla_lane_f32(x31_im, y11_im, g31_re, 1);

    // G22[0]
    svfloat32_t g22_re = svld1_f32(pg3, &p_g_real[10 * p_gstride]);
    svfloat32_t g22_im = svld1_f32(pg3, &p_g_imag[10 * p_gstride]);
    x20_re = svmla_lane_f32(x20_re, y20_re, g22_re, 0);
    x20_re = svmls_lane_f32(x20_re, y20_im, g22_im, 0);
    x20_im = svmla_lane_f32(x20_im, y20_re, g22_im, 0);
    x20_im = svmla_lane_f32(x20_im, y20_im, g22_re, 0);

    // G22[1]
    x21_re = svmla_lane_f32(x21_re, y21_re, g22_re, 1);
    x21_re = svmls_lane_f32(x21_re, y21_im, g22_im, 1);
    x21_im = svmla_lane_f32(x21_im, y21_re, g22_im, 1);
    x21_im = svmla_lane_f32(x21_im, y21_im, g22_re, 1);

    // G23[0]
    svfloat32_t g23_re = svld1_f32(pg3, &p_g_real[11 * p_gstride]);
    svfloat32_t g23_im = svld1_f32(pg3, &p_g_imag[11 * p_gstride]);
    x20_re = svmla_lane_f32(x20_re, y30_re, g23_re, 0);
    x20_re = svmls_lane_f32(x20_re, y30_im, g23_im, 0);
    x20_im = svmla_lane_f32(x20_im, y30_re, g23_im, 0);
    x20_im = svmla_lane_f32(x20_im, y30_im, g23_re, 0);

    // G23[1]
    x21_re = svmla_lane_f32(x21_re, y31_re, g23_re, 1);
    x21_re = svmls_lane_f32(x21_re, y31_im, g23_im, 1);
    x21_im = svmla_lane_f32(x21_im, y31_re, g23_im, 1);
    x21_im = svmla_lane_f32(x21_im, y31_im, g23_re, 1);

    // Store first and second x2 entry
    svint16_t x20_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x20_re, x20_im, pg8);
    svst1_s16(pg8, x2_ptr, x20_bis);
    svint16_t x21_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x21_re, x21_im, pg8);
    svst1_s16(pg8, x2_ptr + 8, x21_bis);

    // G32[0]
    svfloat32_t g32_re = svld1_f32(pg3, &p_g_real[14 * p_gstride]);
    svfloat32_t g32_im = svld1_f32(pg3, &p_g_imag[14 * p_gstride]);
    x30_re = svmla_lane_f32(x30_re, y20_re, g32_re, 0);
    x30_re = svmls_lane_f32(x30_re, y20_im, g32_im, 0);
    x30_im = svmla_lane_f32(x30_im, y20_re, g32_im, 0);
    x30_im = svmla_lane_f32(x30_im, y20_im, g32_re, 0);

    // G32[1]
    x31_re = svmla_lane_f32(x31_re, y21_re, g32_re, 1);
    x31_re = svmls_lane_f32(x31_re, y21_im, g32_im, 1);
    x31_im = svmla_lane_f32(x31_im, y21_re, g32_im, 1);
    x31_im = svmla_lane_f32(x31_im, y21_im, g32_re, 1);

    // G33[0]
    svfloat32_t g33_re = svld1_f32(pg3, &p_g_real[15 * p_gstride]);
    svfloat32_t g33_im = svld1_f32(pg3, &p_g_imag[15 * p_gstride]);
    x30_re = svmla_lane_f32(x30_re, y30_re, g33_re, 0);
    x30_re = svmls_lane_f32(x30_re, y30_im, g33_im, 0);
    x30_im = svmla_lane_f32(x30_im, y30_re, g33_im, 0);
    x30_im = svmla_lane_f32(x30_im, y30_im, g33_re, 0);

    // G33[1]
    x31_re = svmla_lane_f32(x31_re, y31_re, g33_re, 1);
    x31_re = svmls_lane_f32(x31_re, y31_im, g33_im, 1);
    x31_im = svmla_lane_f32(x31_im, y31_re, g33_im, 1);
    x31_im = svmla_lane_f32(x31_im, y31_im, g33_re, 1);

    // Store first and second x3 entry
    svint16_t x30_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x30_re, x30_im, pg8);
    svst1_s16(pg8, x3_ptr, x30_bis);
    svint16_t x31_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x31_re, x31_im, pg8);
    svst1_s16(pg8, x3_ptr + 8, x31_bis);

    // Load third y0, y1 entry
    svint32_t y02_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 15));
    svint32_t y02_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 16));
    svfloat32_t y02_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y02_int_re), scale[0]);
    svfloat32_t y02_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y02_int_im), scale[0]);

    svint32_t y12_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 15));
    svint32_t y12_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 16));
    svfloat32_t y12_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y12_int_re), scale[1]);
    svfloat32_t y12_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y12_int_im), scale[1]);

    // G00[2]
    svfloat32_t x02_re = svmul_lane_f32(y02_re, g00_re, 2);
    x02_re = svmls_lane_f32(x02_re, y02_im, g00_im, 2);
    svfloat32_t x02_im = svmul_lane_f32(y02_re, g00_im, 2);
    x02_im = svmla_lane_f32(x02_im, y02_im, g00_re, 2);

    // G01[2]
    x02_re = svmla_lane_f32(x02_re, y12_re, g01_re, 2);
    x02_re = svmls_lane_f32(x02_re, y12_im, g01_im, 2);
    x02_im = svmla_lane_f32(x02_im, y12_re, g01_im, 2);
    x02_im = svmla_lane_f32(x02_im, y12_im, g01_re, 2);

    // G10[2]
    svfloat32_t x12_re = svmul_lane_f32(y02_re, g10_re, 2);
    x12_re = svmls_lane_f32(x12_re, y02_im, g10_im, 2);
    svfloat32_t x12_im = svmul_lane_f32(y02_re, g10_im, 2);
    x12_im = svmla_lane_f32(x12_im, y02_im, g10_re, 2);

    // G11[2]
    x12_re = svmla_lane_f32(x12_re, y12_re, g11_re, 2);
    x12_re = svmls_lane_f32(x12_re, y12_im, g11_im, 2);
    x12_im = svmla_lane_f32(x12_im, y12_re, g11_im, 2);
    x12_im = svmla_lane_f32(x12_im, y12_im, g11_re, 2);

    // Load third y2 entry
    svint32_t y22_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 15));
    svint32_t y22_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 16));
    svfloat32_t y22_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y22_int_re), scale[2]);
    svfloat32_t y22_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y22_int_im), scale[2]);

    // G02[2]
    x02_re = svmla_lane_f32(x02_re, y22_re, g02_re, 2);
    x02_re = svmls_lane_f32(x02_re, y22_im, g02_im, 2);
    x02_im = svmla_lane_f32(x02_im, y22_re, g02_im, 2);
    x02_im = svmla_lane_f32(x02_im, y22_im, g02_re, 2);

    // G12[2]
    x12_re = svmla_lane_f32(x12_re, y22_re, g12_re, 2);
    x12_re = svmls_lane_f32(x12_re, y22_im, g12_im, 2);
    x12_im = svmla_lane_f32(x12_im, y22_re, g12_im, 2);
    x12_im = svmla_lane_f32(x12_im, y22_im, g12_re, 2);

    // Load third y3 entry
    svint32_t y32_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 15));
    svint32_t y32_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 16));
    svfloat32_t y32_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y32_int_re), scale[3]);
    svfloat32_t y32_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y32_int_im), scale[3]);

    // G03[2]
    x02_re = svmla_lane_f32(x02_re, y32_re, g03_re, 2);
    x02_re = svmls_lane_f32(x02_re, y32_im, g03_im, 2);
    x02_im = svmla_lane_f32(x02_im, y32_re, g03_im, 2);
    x02_im = svmla_lane_f32(x02_im, y32_im, g03_re, 2);

    // G13[2]
    x12_re = svmla_lane_f32(x12_re, y32_re, g13_re, 2);
    x12_re = svmls_lane_f32(x12_re, y32_im, g13_im, 2);
    x12_im = svmla_lane_f32(x12_im, y32_re, g13_im, 2);
    x12_im = svmla_lane_f32(x12_im, y32_im, g13_re, 2);

    // Store third x0, x1 entry
    svint16_t x02_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x02_re, x02_im, pg8);
    svst1_s16(pg8, x0_ptr + 16, x02_bis);
    svint16_t x12_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x12_re, x12_im, pg8);
    svst1_s16(pg8, x1_ptr + 16, x12_bis);

    // G20[2]
    svfloat32_t x22_re = svmul_lane_f32(y02_re, g20_re, 2);
    x22_re = svmls_lane_f32(x22_re, y02_im, g20_im, 2);
    svfloat32_t x22_im = svmul_lane_f32(y02_re, g20_im, 2);
    x22_im = svmla_lane_f32(x22_im, y02_im, g20_re, 2);

    // G21[2]
    x22_re = svmla_lane_f32(x22_re, y12_re, g21_re, 2);
    x22_re = svmls_lane_f32(x22_re, y12_im, g21_im, 2);
    x22_im = svmla_lane_f32(x22_im, y12_re, g21_im, 2);
    x22_im = svmla_lane_f32(x22_im, y12_im, g21_re, 2);

    // G30[2]
    svfloat32_t x32_re = svmul_lane_f32(y02_re, g30_re, 2);
    x32_re = svmls_lane_f32(x32_re, y02_im, g30_im, 2);
    svfloat32_t x32_im = svmul_lane_f32(y02_re, g30_im, 2);
    x32_im = svmla_lane_f32(x32_im, y02_im, g30_re, 2);

    // G31[2]
    x32_re = svmla_lane_f32(x32_re, y12_re, g31_re, 2);
    x32_re = svmls_lane_f32(x32_re, y12_im, g31_im, 2);
    x32_im = svmla_lane_f32(x32_im, y12_re, g31_im, 2);
    x32_im = svmla_lane_f32(x32_im, y12_im, g31_re, 2);

    // G22[2]
    x22_re = svmla_lane_f32(x22_re, y22_re, g22_re, 2);
    x22_re = svmls_lane_f32(x22_re, y22_im, g22_im, 2);
    x22_im = svmla_lane_f32(x22_im, y22_re, g22_im, 2);
    x22_im = svmla_lane_f32(x22_im, y22_im, g22_re, 2);

    // G23[2]
    x22_re = svmla_lane_f32(x22_re, y32_re, g23_re, 2);
    x22_re = svmls_lane_f32(x22_re, y32_im, g23_im, 2);
    x22_im = svmla_lane_f32(x22_im, y32_re, g23_im, 2);
    x22_im = svmla_lane_f32(x22_im, y32_im, g23_re, 2);

    // G32[2]
    x32_re = svmla_lane_f32(x32_re, y22_re, g32_re, 2);
    x32_re = svmls_lane_f32(x32_re, y22_im, g32_im, 2);
    x32_im = svmla_lane_f32(x32_im, y22_re, g32_im, 2);
    x32_im = svmla_lane_f32(x32_im, y22_im, g32_re, 2);

    // G33[2]
    x32_re = svmla_lane_f32(x32_re, y32_re, g33_re, 2);
    x32_re = svmls_lane_f32(x32_re, y32_im, g33_im, 2);
    x32_im = svmla_lane_f32(x32_im, y32_re, g33_im, 2);
    x32_im = svmla_lane_f32(x32_im, y32_im, g33_re, 2);

    // Store third x2, x3 entry
    svint16_t x22_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x22_re, x22_im, pg8);
    svst1_s16(pg8, x2_ptr + 16, x22_bis);
    svint16_t x32_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x32_re, x32_im, pg8);
    svst1_s16(pg8, x3_ptr + 16, x32_bis);

    y0_ptr = y0_ptr + 24;
    y1_ptr = y1_ptr + 24;
    y2_ptr = y2_ptr + 24;
    y3_ptr = y3_ptr + 24;
    x0_ptr = x0_ptr + 24;
    x1_ptr = x1_ptr + 24;
    x2_ptr = x2_ptr + 24;
    x3_ptr = x3_ptr + 24;
    p_g_real += 3;
    p_g_imag += 3;
  }

#else
  const armral_fixed_point_index num_fract_bits_y[] = {
      p_y_num_fract_bits[0], p_y_num_fract_bits[1], p_y_num_fract_bits[2],
      p_y_num_fract_bits[3]};

  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    int16x8_t y0[2];
    y0[0] = vld1q_s16(y0_ptr);
    y0[1] = vld1q_s16(y0_ptr + 8);
    y0_ptr = y0_ptr + 16;
    int16x8_t y1[2];
    y1[0] = vld1q_s16(y1_ptr);
    y1[1] = vld1q_s16(y1_ptr + 8);
    y1_ptr = y1_ptr + 16;
    float32x4_t y0_re[2];
    float32x4_t y0_im[2];
    armral_convert_i16_f32_x8(num_fract_bits_y[0], y0, y0_re, y0_im);
    float32x4_t y1_re[2];
    float32x4_t y1_im[2];
    armral_convert_i16_f32_x8(num_fract_bits_y[1], y1, y1_re, y1_im);
    // Load G00[0] G00[1]
    float32x2_t g00_real = vld1_f32(p_g_real);
    float32x2_t g00_imag = vld1_f32(p_g_imag);

    float32x4_t x0_re[2];
    x0_re[0] = vmulq_lane_f32(y0_re[0], g00_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y0_im[0], g00_imag, 0);
    float32x4_t x0_im[2];
    x0_im[0] = vmulq_lane_f32(y0_im[0], g00_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y0_re[0], g00_imag, 0);

    x0_re[1] = vmulq_lane_f32(y0_re[1], g00_real, 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y0_im[1], g00_imag, 1);
    x0_im[1] = vmulq_lane_f32(y0_im[1], g00_real, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y0_re[1], g00_imag, 1);

    int16x8_t y2[2];
    y2[0] = vld1q_s16(y2_ptr);
    y2[1] = vld1q_s16(y2_ptr + 8);
    y2_ptr = y2_ptr + 16;
    int16x8_t y3[2];
    y3[0] = vld1q_s16(y3_ptr);
    y3[1] = vld1q_s16(y3_ptr + 8);
    y3_ptr = y3_ptr + 16;
    float32x4_t y2_re[2];
    float32x4_t y2_im[2];
    armral_convert_i16_f32_x8(num_fract_bits_y[2], y2, y2_re, y2_im);
    float32x4_t y3_re[2];
    float32x4_t y3_im[2];
    armral_convert_i16_f32_x8(num_fract_bits_y[3], y3, y3_re, y3_im);
    // Load G01[0] G01[1]
    float32x2_t g01_real = vld1_f32(p_g_real + p_gstride);
    float32x2_t g01_imag = vld1_f32(p_g_imag + p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y1_re[0], g01_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y1_im[0], g01_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_im[0], g01_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_re[0], g01_imag, 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y1_re[1], g01_real, 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y1_im[1], g01_imag, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y1_im[1], g01_real, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y1_re[1], g01_imag, 1);

    // Load G02[0] G02[1]
    float32x2_t g02_real = vld1_f32(p_g_real + 2 * p_gstride);
    float32x2_t g02_imag = vld1_f32(p_g_imag + 2 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y2_re[0], g02_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y2_im[0], g02_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y2_im[0], g02_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y2_re[0], g02_imag, 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y2_re[1], g02_real, 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y2_im[1], g02_imag, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y2_im[1], g02_real, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y2_re[1], g02_imag, 1);

    // Load G03[0] G03[1]
    float32x2_t g03_real = vld1_f32(p_g_real + 3 * p_gstride);
    float32x2_t g03_imag = vld1_f32(p_g_imag + 3 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y3_re[0], g03_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y3_im[0], g03_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y3_im[0], g03_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y3_re[0], g03_imag, 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y3_re[1], g03_real, 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y3_im[1], g03_imag, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y3_im[1], g03_real, 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y3_re[1], g03_imag, 1);

    // Load G10[0] G10[1]
    float32x2_t g10_real = vld1_f32(p_g_real + 4 * p_gstride);
    float32x2_t g10_imag = vld1_f32(p_g_imag + 4 * p_gstride);

    float32x4_t x1_re[2];
    x1_re[0] = vmulq_lane_f32(y0_re[0], g10_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y0_im[0], g10_imag, 0);
    float32x4_t x1_im[2];
    x1_im[0] = vmulq_lane_f32(y0_im[0], g10_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y0_re[0], g10_imag, 0);

    x1_re[1] = vmulq_lane_f32(y0_re[1], g10_real, 1);
    x1_re[1] = vfmsq_lane_f32(x1_re[1], y0_im[1], g10_imag, 1);
    x1_im[1] = vmulq_lane_f32(y0_im[1], g10_real, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y0_re[1], g10_imag, 1);
    // Load G11[0] G11[1]
    float32x2_t g11_real = vld1_f32(p_g_real + 5 * p_gstride);
    float32x2_t g11_imag = vld1_f32(p_g_imag + 5 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y1_re[0], g11_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y1_im[0], g11_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_im[0], g11_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_re[0], g11_imag, 0);

    x1_re[1] = vfmaq_lane_f32(x1_re[1], y1_re[1], g11_real, 1);
    x1_re[1] = vfmsq_lane_f32(x1_re[1], y1_im[1], g11_imag, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y1_im[1], g11_real, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y1_re[1], g11_imag, 1);

    // Load G12[0] G12[1]
    float32x2_t g12_real = vld1_f32(p_g_real + 6 * p_gstride);
    float32x2_t g12_imag = vld1_f32(p_g_imag + 6 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y2_re[0], g12_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y2_im[0], g12_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y2_im[0], g12_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y2_re[0], g12_imag, 0);

    x1_re[1] = vfmaq_lane_f32(x1_re[1], y2_re[1], g12_real, 1);
    x1_re[1] = vfmsq_lane_f32(x1_re[1], y2_im[1], g12_imag, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y2_im[1], g12_real, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y2_re[1], g12_imag, 1);

    // Load G13[0] G13[1]
    float32x2_t g13_real = vld1_f32(p_g_real + 7 * p_gstride);
    float32x2_t g13_imag = vld1_f32(p_g_imag + 7 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y3_re[0], g13_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y3_im[0], g13_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y3_im[0], g13_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y3_re[0], g13_imag, 0);

    x1_re[1] = vfmaq_lane_f32(x1_re[1], y3_re[1], g13_real, 1);
    x1_re[1] = vfmsq_lane_f32(x1_re[1], y3_im[1], g13_imag, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y3_im[1], g13_real, 1);
    x1_im[1] = vfmaq_lane_f32(x1_im[1], y3_re[1], g13_imag, 1);

    int16x8_t x0[2];
    int16x8_t x1[2];
    armral_convert_f32_i16_x8(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im, x0,
                              x1);
    vst1q_s16(x0_ptr, x0[0]);
    vst1q_s16(x0_ptr + 8, x0[1]);
    vst1q_s16(x1_ptr, x1[0]);
    vst1q_s16(x1_ptr + 8, x1[1]);
    x0_ptr = x0_ptr + 16;
    x1_ptr = x1_ptr + 16;

    // Load G20[0] G20[1]
    float32x2_t g20_real = vld1_f32(p_g_real + 8 * p_gstride);
    float32x2_t g20_imag = vld1_f32(p_g_imag + 8 * p_gstride);

    float32x4_t x2_re[2];
    x2_re[0] = vmulq_lane_f32(y0_re[0], g20_real, 0);
    x2_re[0] = vfmsq_lane_f32(x2_re[0], y0_im[0], g20_imag, 0);
    float32x4_t x2_im[2];
    x2_im[0] = vmulq_lane_f32(y0_im[0], g20_real, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y0_re[0], g20_imag, 0);

    x2_re[1] = vmulq_lane_f32(y0_re[1], g20_real, 1);
    x2_re[1] = vfmsq_lane_f32(x2_re[1], y0_im[1], g20_imag, 1);
    x2_im[1] = vmulq_lane_f32(y0_im[1], g20_real, 1);
    x2_im[1] = vfmaq_lane_f32(x2_im[1], y0_re[1], g20_imag, 1);

    // Load G21[0] G21[1]
    float32x2_t g21_real = vld1_f32(p_g_real + 9 * p_gstride);
    float32x2_t g21_imag = vld1_f32(p_g_imag + 9 * p_gstride);

    x2_re[0] = vfmaq_lane_f32(x2_re[0], y1_re[0], g21_real, 0);
    x2_re[0] = vfmsq_lane_f32(x2_re[0], y1_im[0], g21_imag, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y1_im[0], g21_real, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y1_re[0], g21_imag, 0);

    x2_re[1] = vfmaq_lane_f32(x2_re[1], y1_re[1], g21_real, 1);
    x2_re[1] = vfmsq_lane_f32(x2_re[1], y1_im[1], g21_imag, 1);
    x2_im[1] = vfmaq_lane_f32(x2_im[1], y1_im[1], g21_real, 1);
    x2_im[1] = vfmaq_lane_f32(x2_im[1], y1_re[1], g21_imag, 1);

    // Load G22[0] G22[1]
    float32x2_t g22_real = vld1_f32(p_g_real + 10 * p_gstride);
    float32x2_t g22_imag = vld1_f32(p_g_imag + 10 * p_gstride);

    x2_re[0] = vfmaq_lane_f32(x2_re[0], y2_re[0], g22_real, 0);
    x2_re[0] = vfmsq_lane_f32(x2_re[0], y2_im[0], g22_imag, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y2_im[0], g22_real, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y2_re[0], g22_imag, 0);

    x2_re[1] = vfmaq_lane_f32(x2_re[1], y2_re[1], g22_real, 1);
    x2_re[1] = vfmsq_lane_f32(x2_re[1], y2_im[1], g22_imag, 1);
    x2_im[1] = vfmaq_lane_f32(x2_im[1], y2_im[1], g22_real, 1);
    x2_im[1] = vfmaq_lane_f32(x2_im[1], y2_re[1], g22_imag, 1);

    // Load G23[0] G23[1]
    float32x2_t g23_real = vld1_f32(p_g_real + 11 * p_gstride);
    float32x2_t g23_imag = vld1_f32(p_g_imag + 11 * p_gstride);

    x2_re[0] = vfmaq_lane_f32(x2_re[0], y3_re[0], g23_real, 0);
    x2_re[0] = vfmsq_lane_f32(x2_re[0], y3_im[0], g23_imag, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y3_im[0], g23_real, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y3_re[0], g23_imag, 0);

    x2_re[1] = vfmaq_lane_f32(x2_re[1], y3_re[1], g23_real, 1);
    x2_re[1] = vfmsq_lane_f32(x2_re[1], y3_im[1], g23_imag, 1);
    x2_im[1] = vfmaq_lane_f32(x2_im[1], y3_im[1], g23_real, 1);
    x2_im[1] = vfmaq_lane_f32(x2_im[1], y3_re[1], g23_imag, 1);

    // Load G30[0] G30[1]
    float32x2_t g30_real = vld1_f32(p_g_real + 12 * p_gstride);
    float32x2_t g30_imag = vld1_f32(p_g_imag + 12 * p_gstride);

    float32x4_t x3_re[2];
    x3_re[0] = vmulq_lane_f32(y0_re[0], g30_real, 0);
    x3_re[0] = vfmsq_lane_f32(x3_re[0], y0_im[0], g30_imag, 0);
    float32x4_t x3_im[2];
    x3_im[0] = vmulq_lane_f32(y0_im[0], g30_real, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y0_re[0], g30_imag, 0);

    x3_re[1] = vmulq_lane_f32(y0_re[1], g30_real, 1);
    x3_re[1] = vfmsq_lane_f32(x3_re[1], y0_im[1], g30_imag, 1);
    x3_im[1] = vmulq_lane_f32(y0_im[1], g30_real, 1);
    x3_im[1] = vfmaq_lane_f32(x3_im[1], y0_re[1], g30_imag, 1);

    // Load G31[0] G31[1]
    float32x2_t g31_real = vld1_f32(p_g_real + 13 * p_gstride);
    float32x2_t g31_imag = vld1_f32(p_g_imag + 13 * p_gstride);

    x3_re[0] = vfmaq_lane_f32(x3_re[0], y1_re[0], g31_real, 0);
    x3_re[0] = vfmsq_lane_f32(x3_re[0], y1_im[0], g31_imag, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y1_im[0], g31_real, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y1_re[0], g31_imag, 0);

    x3_re[1] = vfmaq_lane_f32(x3_re[1], y1_re[1], g31_real, 1);
    x3_re[1] = vfmsq_lane_f32(x3_re[1], y1_im[1], g31_imag, 1);
    x3_im[1] = vfmaq_lane_f32(x3_im[1], y1_im[1], g31_real, 1);
    x3_im[1] = vfmaq_lane_f32(x3_im[1], y1_re[1], g31_imag, 1);

    // Load G32[0] G32[1]
    float32x2_t g32_real = vld1_f32(p_g_real + 14 * p_gstride);
    float32x2_t g32_imag = vld1_f32(p_g_imag + 14 * p_gstride);

    x3_re[0] = vfmaq_lane_f32(x3_re[0], y2_re[0], g32_real, 0);
    x3_re[0] = vfmsq_lane_f32(x3_re[0], y2_im[0], g32_imag, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y2_im[0], g32_real, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y2_re[0], g32_imag, 0);

    x3_re[1] = vfmaq_lane_f32(x3_re[1], y2_re[1], g32_real, 1);
    x3_re[1] = vfmsq_lane_f32(x3_re[1], y2_im[1], g32_imag, 1);
    x3_im[1] = vfmaq_lane_f32(x3_im[1], y2_im[1], g32_real, 1);
    x3_im[1] = vfmaq_lane_f32(x3_im[1], y2_re[1], g32_imag, 1);

    // Load G33[0] G33[1]
    float32x2_t g33_real = vld1_f32(p_g_real + 15 * p_gstride);
    float32x2_t g33_imag = vld1_f32(p_g_imag + 15 * p_gstride);

    x3_re[0] = vfmaq_lane_f32(x3_re[0], y3_re[0], g33_real, 0);
    x3_re[0] = vfmsq_lane_f32(x3_re[0], y3_im[0], g33_imag, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y3_im[0], g33_real, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y3_re[0], g33_imag, 0);

    x3_re[1] = vfmaq_lane_f32(x3_re[1], y3_re[1], g33_real, 1);
    x3_re[1] = vfmsq_lane_f32(x3_re[1], y3_im[1], g33_imag, 1);
    x3_im[1] = vfmaq_lane_f32(x3_im[1], y3_im[1], g33_real, 1);
    x3_im[1] = vfmaq_lane_f32(x3_im[1], y3_re[1], g33_imag, 1);

    int16x8_t x2[2];
    int16x8_t x3[2];
    armral_convert_f32_i16_x8(num_fract_bits_x, x2_re, x2_im, x3_re, x3_im, x2,
                              x3);
    vst1q_s16(x2_ptr, x2[0]);
    vst1q_s16(x2_ptr + 8, x2[1]);
    vst1q_s16(x3_ptr, x3[0]);
    vst1q_s16(x3_ptr + 8, x3[1]);
    p_g_real = p_g_real + 2;
    p_g_imag = p_g_imag + 2;
    x2_ptr = x2_ptr + 16;
    x3_ptr = x3_ptr + 16;

    int16x8_t y0_quadw = vld1q_s16(y0_ptr);
    y0_ptr = y0_ptr + 8;
    int16x8_t y1_quadw = vld1q_s16(y1_ptr);
    y1_ptr = y1_ptr + 8;

    // Load G00[0]
    g00_real = vld1_dup_f32(p_g_real);
    g00_imag = vld1_dup_f32(p_g_imag);
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re[0],
                              &y0_im[0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re[0],
                              &y1_im[0]);
    x0_re[0] = vmulq_lane_f32(y0_re[0], g00_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y0_im[0], g00_imag, 0);
    x0_im[0] = vmulq_lane_f32(y0_im[0], g00_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y0_re[0], g00_imag, 0);

    // Load G01[0]
    int16x8_t y2_quadw = vld1q_s16(y2_ptr);
    y2_ptr = y2_ptr + 8;
    g01_real = vld1_dup_f32(p_g_real + p_gstride);
    g01_imag = vld1_dup_f32(p_g_imag + p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y1_re[0], g01_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y1_im[0], g01_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_im[0], g01_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y1_re[0], g01_imag, 0);

    // Load G02[0]
    int16x8_t y3_quadw = vld1q_s16(y3_ptr);
    y3_ptr = y3_ptr + 8;
    armral_convert_i16_f32_x4(num_fract_bits_y[2], y2_quadw, &y2_re[0],
                              &y2_im[0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y3_quadw, &y3_re[0],
                              &y3_im[0]);
    g02_real = vld1_dup_f32(p_g_real + 2 * p_gstride);
    g02_imag = vld1_dup_f32(p_g_imag + 2 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y2_re[0], g02_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y2_im[0], g02_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y2_im[0], g02_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y2_re[0], g02_imag, 0);

    // Load G03[0]
    g03_real = vld1_dup_f32(p_g_real + 3 * p_gstride);
    g03_imag = vld1_dup_f32(p_g_imag + 3 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y3_re[0], g03_real, 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y3_im[0], g03_imag, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y3_im[0], g03_real, 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y3_re[0], g03_imag, 0);

    // Load G10[0]
    g10_real = vld1_dup_f32(p_g_real + 4 * p_gstride);
    g10_imag = vld1_dup_f32(p_g_imag + 4 * p_gstride);

    x1_re[0] = vmulq_lane_f32(y0_re[0], g10_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y0_im[0], g10_imag, 0);
    x1_im[0] = vmulq_lane_f32(y0_im[0], g10_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y0_re[0], g10_imag, 0);

    // Load G11[0]
    g11_real = vld1_dup_f32(p_g_real + 5 * p_gstride);
    g11_imag = vld1_dup_f32(p_g_imag + 5 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y1_re[0], g11_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y1_im[0], g11_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_im[0], g11_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y1_re[0], g11_imag, 0);

    // Load G12[0]
    g12_real = vld1_dup_f32(p_g_real + 6 * p_gstride);
    g12_imag = vld1_dup_f32(p_g_imag + 6 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y2_re[0], g12_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y2_im[0], g12_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y2_im[0], g12_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y2_re[0], g12_imag, 0);

    // Load G13[0]
    g13_real = vld1_dup_f32(p_g_real + 7 * p_gstride);
    g13_imag = vld1_dup_f32(p_g_imag + 7 * p_gstride);

    x1_re[0] = vfmaq_lane_f32(x1_re[0], y3_re[0], g13_real, 0);
    x1_re[0] = vfmsq_lane_f32(x1_re[0], y3_im[0], g13_imag, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y3_im[0], g13_real, 0);
    x1_im[0] = vfmaq_lane_f32(x1_im[0], y3_re[0], g13_imag, 0);

    int16x8_t x0_bis;
    int16x8_t x1_bis;
    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re[0], x0_im[0], x1_re[0],
                              x1_im[0], &x0_bis, &x1_bis);

    vst1q_s16(x0_ptr, x0_bis);
    vst1q_s16(x1_ptr, x1_bis);
    x0_ptr = x0_ptr + 8;
    x1_ptr = x1_ptr + 8;

    // Load G20[0]
    g20_real = vld1_dup_f32(p_g_real + 8 * p_gstride);
    g20_imag = vld1_dup_f32(p_g_imag + 8 * p_gstride);
    x2_re[0] = vmulq_lane_f32(y0_re[0], g20_real, 0);
    x2_re[0] = vfmsq_lane_f32(x2_re[0], y0_im[0], g20_imag, 0);
    x2_im[0] = vmulq_lane_f32(y0_im[0], g20_real, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y0_re[0], g20_imag, 0);

    // Load G21[0]
    g21_real = vld1_dup_f32(p_g_real + 9 * p_gstride);
    g21_imag = vld1_dup_f32(p_g_imag + 9 * p_gstride);

    x2_re[0] = vfmaq_lane_f32(x2_re[0], y1_re[0], g21_real, 0);
    x2_re[0] = vfmsq_lane_f32(x2_re[0], y1_im[0], g21_imag, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y1_im[0], g21_real, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y1_re[0], g21_imag, 0);

    // Load G22[0]
    g22_real = vld1_dup_f32(p_g_real + 10 * p_gstride);
    g22_imag = vld1_dup_f32(p_g_imag + 10 * p_gstride);

    x2_re[0] = vfmaq_lane_f32(x2_re[0], y2_re[0], g22_real, 0);
    x2_re[0] = vfmsq_lane_f32(x2_re[0], y2_im[0], g22_imag, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y2_im[0], g22_real, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y2_re[0], g22_imag, 0);

    // Load G23[0]
    g23_real = vld1_dup_f32(p_g_real + 11 * p_gstride);
    g23_imag = vld1_dup_f32(p_g_imag + 11 * p_gstride);

    x2_re[0] = vfmaq_lane_f32(x2_re[0], y3_re[0], g23_real, 0);
    x2_re[0] = vfmsq_lane_f32(x2_re[0], y3_im[0], g23_imag, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y3_im[0], g23_real, 0);
    x2_im[0] = vfmaq_lane_f32(x2_im[0], y3_re[0], g23_imag, 0);

    // Load G30[0]
    g30_real = vld1_dup_f32(p_g_real + 12 * p_gstride);
    g30_imag = vld1_dup_f32(p_g_imag + 12 * p_gstride);
    x3_re[0] = vmulq_lane_f32(y0_re[0], g30_real, 0);
    x3_re[0] = vfmsq_lane_f32(x3_re[0], y0_im[0], g30_imag, 0);
    x3_im[0] = vmulq_lane_f32(y0_im[0], g30_real, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y0_re[0], g30_imag, 0);

    // Load G31[0]
    g31_real = vld1_dup_f32(p_g_real + 13 * p_gstride);
    g31_imag = vld1_dup_f32(p_g_imag + 13 * p_gstride);

    x3_re[0] = vfmaq_lane_f32(x3_re[0], y1_re[0], g31_real, 0);
    x3_re[0] = vfmsq_lane_f32(x3_re[0], y1_im[0], g31_imag, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y1_im[0], g31_real, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y1_re[0], g31_imag, 0);

    // Load G32[0]
    g32_real = vld1_dup_f32(p_g_real + 14 * p_gstride);
    g32_imag = vld1_dup_f32(p_g_imag + 14 * p_gstride);

    x3_re[0] = vfmaq_lane_f32(x3_re[0], y2_re[0], g32_real, 0);
    x3_re[0] = vfmsq_lane_f32(x3_re[0], y2_im[0], g32_imag, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y2_im[0], g32_real, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y2_re[0], g32_imag, 0);

    // Load G33[0]
    g33_real = vld1_dup_f32(p_g_real + 15 * p_gstride);
    g33_imag = vld1_dup_f32(p_g_imag + 15 * p_gstride);

    x3_re[0] = vfmaq_lane_f32(x3_re[0], y3_re[0], g33_real, 0);
    x3_re[0] = vfmsq_lane_f32(x3_re[0], y3_im[0], g33_imag, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y3_im[0], g33_real, 0);
    x3_im[0] = vfmaq_lane_f32(x3_im[0], y3_re[0], g33_imag, 0);

    int16x8_t x2_bis;
    int16x8_t x3_bis;
    armral_convert_f32_i16_x4(num_fract_bits_x, x2_re[0], x2_im[0], x3_re[0],
                              x3_im[0], &x2_bis, &x3_bis);

    vst1q_s16(x2_ptr, x2_bis);
    vst1q_s16(x3_ptr, x3_bis);
    x2_ptr = x2_ptr + 8;
    x3_ptr = x3_ptr + 8;
    p_g_real++;
    p_g_imag++;
  }
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_1x4_4sc_f32(
    const uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    const uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    const uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    const armral_fixed_point_index num_fract_bits_x) {

  const int16_t *y0_ptr = (const int16_t *)p_y;
  const int16_t *y1_ptr = (const int16_t *)p_y + p_ystride;
  const int16_t *y2_ptr = (const int16_t *)p_y + 2 * p_ystride;
  const int16_t *y3_ptr = (const int16_t *)p_y + 3 * p_ystride;

  int16_t *x0_ptr = (int16_t *)p_x;

#if ARMRAL_ARCH_SVE >= 2
  svbool_t pg8 = svptrue_pat_b16(SV_VL8);
  svbool_t pg3 = svptrue_pat_b32(SV_VL3);
  svbool_t podd = svtrn1_b16(svpfalse_b(), pg8);
  float32_t scale[] = {1. / (1U << (16 + p_y_num_fract_bits[0])),
                       1. / (1U << (16 + p_y_num_fract_bits[1])),
                       1. / (1U << (16 + p_y_num_fract_bits[2])),
                       1. / (1U << (16 + p_y_num_fract_bits[3]))};
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    // Load first two y0 entries
    svint32_t y00_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr - 1));
    svint32_t y00_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr));
    svfloat32_t y00_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y00_int_re), scale[0]);
    svfloat32_t y00_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y00_int_im), scale[0]);

    svint32_t y01_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 7));
    svint32_t y01_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 8));
    svfloat32_t y01_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y01_int_re), scale[0]);
    svfloat32_t y01_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y01_int_im), scale[0]);

    // G00[0], G00[1]
    svfloat32_t g00_re = svld1_f32(pg3, &p_g_real[0 * p_gstride]);
    svfloat32_t g00_im = svld1_f32(pg3, &p_g_imag[0 * p_gstride]);
    svfloat32_t x00_re = svmul_lane_f32(y00_re, g00_re, 0);
    x00_re = svmls_lane_f32(x00_re, y00_im, g00_im, 0);
    svfloat32_t x00_im = svmul_lane_f32(y00_re, g00_im, 0);
    x00_im = svmla_lane_f32(x00_im, y00_im, g00_re, 0);

    svfloat32_t x01_re = svmul_lane_f32(y01_re, g00_re, 1);
    x01_re = svmls_lane_f32(x01_re, y01_im, g00_im, 1);
    svfloat32_t x01_im = svmul_lane_f32(y01_re, g00_im, 1);
    x01_im = svmla_lane_f32(x01_im, y01_im, g00_re, 1);

    // Load the first two y1 entries
    svint32_t y10_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr - 1));
    svint32_t y10_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr));
    svfloat32_t y10_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y10_int_re), scale[1]);
    svfloat32_t y10_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y10_int_im), scale[1]);

    svint32_t y11_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 7));
    svint32_t y11_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 8));
    svfloat32_t y11_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y11_int_re), scale[1]);
    svfloat32_t y11_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y11_int_im), scale[1]);

    // G01[0], G01[1]
    svfloat32_t g01_re = svld1_f32(pg3, &p_g_real[1 * p_gstride]);
    svfloat32_t g01_im = svld1_f32(pg3, &p_g_imag[1 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y10_re, g01_re, 0);
    x00_re = svmls_lane_f32(x00_re, y10_im, g01_im, 0);
    x00_im = svmla_lane_f32(x00_im, y10_re, g01_im, 0);
    x00_im = svmla_lane_f32(x00_im, y10_im, g01_re, 0);

    x01_re = svmla_lane_f32(x01_re, y11_re, g01_re, 1);
    x01_re = svmls_lane_f32(x01_re, y11_im, g01_im, 1);
    x01_im = svmla_lane_f32(x01_im, y11_re, g01_im, 1);
    x01_im = svmla_lane_f32(x01_im, y11_im, g01_re, 1);

    // Load the first two y2 entries
    svint32_t y20_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr - 1));
    svint32_t y20_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr));
    svfloat32_t y20_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y20_int_re), scale[2]);
    svfloat32_t y20_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y20_int_im), scale[2]);

    svint32_t y21_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 7));
    svint32_t y21_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 8));
    svfloat32_t y21_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y21_int_re), scale[2]);
    svfloat32_t y21_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y21_int_im), scale[2]);

    // G02[0], G02[1]
    svfloat32_t g02_re = svld1_f32(pg3, &p_g_real[2 * p_gstride]);
    svfloat32_t g02_im = svld1_f32(pg3, &p_g_imag[2 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y20_re, g02_re, 0);
    x00_re = svmls_lane_f32(x00_re, y20_im, g02_im, 0);
    x00_im = svmla_lane_f32(x00_im, y20_re, g02_im, 0);
    x00_im = svmla_lane_f32(x00_im, y20_im, g02_re, 0);

    x01_re = svmla_lane_f32(x01_re, y21_re, g02_re, 1);
    x01_re = svmls_lane_f32(x01_re, y21_im, g02_im, 1);
    x01_im = svmla_lane_f32(x01_im, y21_re, g02_im, 1);
    x01_im = svmla_lane_f32(x01_im, y21_im, g02_re, 1);

    // Load the first two y3 entries
    svint32_t y30_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr - 1));
    svint32_t y30_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr));
    svfloat32_t y30_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y30_int_re), scale[3]);
    svfloat32_t y30_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y30_int_im), scale[3]);

    svint32_t y31_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 7));
    svint32_t y31_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 8));
    svfloat32_t y31_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y31_int_re), scale[3]);
    svfloat32_t y31_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y31_int_im), scale[3]);

    // G03[0], G03[1]
    svfloat32_t g03_re = svld1_f32(pg3, &p_g_real[3 * p_gstride]);
    svfloat32_t g03_im = svld1_f32(pg3, &p_g_imag[3 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y30_re, g03_re, 0);
    x00_re = svmls_lane_f32(x00_re, y30_im, g03_im, 0);
    x00_im = svmla_lane_f32(x00_im, y30_re, g03_im, 0);
    x00_im = svmla_lane_f32(x00_im, y30_im, g03_re, 0);

    x01_re = svmla_lane_f32(x01_re, y31_re, g03_re, 1);
    x01_re = svmls_lane_f32(x01_re, y31_im, g03_im, 1);
    x01_im = svmla_lane_f32(x01_im, y31_re, g03_im, 1);
    x01_im = svmla_lane_f32(x01_im, y31_im, g03_re, 1);

    // Store the first two x0 entries
    svint16_t x00_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x00_re, x00_im, pg8);
    svst1_s16(pg8, x0_ptr, x00_bis);
    svint16_t x01_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x01_re, x01_im, pg8);
    svst1_s16(pg8, x0_ptr + 8, x01_bis);

    // Process the third y entries.
    // G00[2]
    svint32_t y02_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 15));
    svint32_t y02_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 16));
    svfloat32_t y02_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y02_int_re), scale[0]);
    svfloat32_t y02_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y02_int_im), scale[0]);
    y0_ptr = y0_ptr + 24;

    svfloat32_t x02_re = svmul_lane_f32(y02_re, g00_re, 2);
    x02_re = svmls_lane_f32(x02_re, y02_im, g00_im, 2);
    svfloat32_t x02_im = svmul_lane_f32(y02_re, g00_im, 2);
    x02_im = svmla_lane_f32(x02_im, y02_im, g00_re, 2);

    // G01[2]
    svint32_t y12_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 15));
    svint32_t y12_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 16));
    svfloat32_t y12_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y12_int_re), scale[1]);
    svfloat32_t y12_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y12_int_im), scale[1]);
    y1_ptr = y1_ptr + 24;
    x02_re = svmla_lane_f32(x02_re, y12_re, g01_re, 2);
    x02_re = svmls_lane_f32(x02_re, y12_im, g01_im, 2);
    x02_im = svmla_lane_f32(x02_im, y12_re, g01_im, 2);
    x02_im = svmla_lane_f32(x02_im, y12_im, g01_re, 2);

    // G02[2]
    svint32_t y22_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 15));
    svint32_t y22_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 16));
    svfloat32_t y22_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y22_int_re), scale[2]);
    svfloat32_t y22_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y22_int_im), scale[2]);
    y2_ptr = y2_ptr + 24;
    x02_re = svmla_lane_f32(x02_re, y22_re, g02_re, 2);
    x02_re = svmls_lane_f32(x02_re, y22_im, g02_im, 2);
    x02_im = svmla_lane_f32(x02_im, y22_re, g02_im, 2);
    x02_im = svmla_lane_f32(x02_im, y22_im, g02_re, 2);

    // G03[2]
    svint32_t y32_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 15));
    svint32_t y32_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 16));
    svfloat32_t y32_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y32_int_re), scale[3]);
    svfloat32_t y32_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y32_int_im), scale[3]);
    y3_ptr = y3_ptr + 24;
    x02_re = svmla_lane_f32(x02_re, y32_re, g03_re, 2);
    x02_re = svmls_lane_f32(x02_re, y32_im, g03_im, 2);
    x02_im = svmla_lane_f32(x02_im, y32_re, g03_im, 2);
    x02_im = svmla_lane_f32(x02_im, y32_im, g03_re, 2);

    svint16_t x02_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x02_re, x02_im, pg8);
    svst1_s16(pg8, x0_ptr + 16, x02_bis);
    x0_ptr = x0_ptr + 24;

    p_g_real += 3;
    p_g_imag += 3;
  }
#else
  const armral_fixed_point_index num_fract_bits_y[] = {
      p_y_num_fract_bits[0], p_y_num_fract_bits[1], p_y_num_fract_bits[2],
      p_y_num_fract_bits[3]};

  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    int16x8_t y[4][2];
    y[0][0] = vld1q_s16(y0_ptr);
    y[0][1] = vld1q_s16(y0_ptr + 8);
    y0_ptr = y0_ptr + 16;
    y[1][0] = vld1q_s16(y1_ptr);
    y[1][1] = vld1q_s16(y1_ptr + 8);
    y1_ptr = y1_ptr + 16;
    float32x4_t y_re[4][2];
    float32x4_t y_im[4][2];
    armral_convert_i16_f32_x8(num_fract_bits_y[0], y[0], y_re[0], y_im[0]);
    armral_convert_i16_f32_x8(num_fract_bits_y[1], y[1], y_re[1], y_im[1]);
    // Load G00[0] G00[1]
    float32x2_t g_real[4]; // G00 G01 G02 G03
    g_real[0] = vld1_f32(p_g_real);
    float32x2_t g_imag[4];
    g_imag[0] = vld1_f32(p_g_imag);

    float32x4_t x0_re[2];
    x0_re[0] = vmulq_lane_f32(y_re[0][0], g_real[0], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[0][0], g_imag[0], 0);
    float32x4_t x0_im[2];
    x0_im[0] = vmulq_lane_f32(y_im[0][0], g_real[0], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[0][0], g_imag[0], 0);

    x0_re[1] = vmulq_lane_f32(y_re[0][1], g_real[0], 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y_im[0][1], g_imag[0], 1);
    x0_im[1] = vmulq_lane_f32(y_im[0][1], g_real[0], 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y_re[0][1], g_imag[0], 1);

    y[2][0] = vld1q_s16(y2_ptr);
    y[2][1] = vld1q_s16(y2_ptr + 8);
    y2_ptr = y2_ptr + 16;
    y[3][0] = vld1q_s16(y3_ptr);
    y[3][1] = vld1q_s16(y3_ptr + 8);
    y3_ptr = y3_ptr + 16;
    armral_convert_i16_f32_x8(num_fract_bits_y[2], y[2], y_re[2], y_im[2]);
    armral_convert_i16_f32_x8(num_fract_bits_y[3], y[3], y_re[3], y_im[3]);
    // Load G01[0] G01[1]
    g_real[1] = vld1_f32(p_g_real + p_gstride);
    g_imag[1] = vld1_f32(p_g_imag + p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y_re[1][0], g_real[1], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[1][0], g_imag[1], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_im[1][0], g_real[1], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[1][0], g_imag[1], 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y_re[1][1], g_real[1], 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y_im[1][1], g_imag[1], 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y_im[1][1], g_real[1], 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y_re[1][1], g_imag[1], 1);

    // Load G02[0] G02[1]
    g_real[2] = vld1_f32(p_g_real + 2 * p_gstride);
    g_imag[2] = vld1_f32(p_g_imag + 2 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y_re[2][0], g_real[2], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[2][0], g_imag[2], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_im[2][0], g_real[2], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[2][0], g_imag[2], 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y_re[2][1], g_real[2], 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y_im[2][1], g_imag[2], 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y_im[2][1], g_real[2], 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y_re[2][1], g_imag[2], 1);

    // Load G03[0] G03[1]
    g_real[3] = vld1_f32(p_g_real + 3 * p_gstride);
    g_imag[3] = vld1_f32(p_g_imag + 3 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y_re[3][0], g_real[3], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[3][0], g_imag[3], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_im[3][0], g_real[3], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[3][0], g_imag[3], 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y_re[3][1], g_real[3], 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y_im[3][1], g_imag[3], 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y_im[3][1], g_real[3], 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y_re[3][1], g_imag[3], 1);

    int16x8_t x0[2];
    armral_convert_f32_i16_x8_2(num_fract_bits_x, x0_re, x0_im, x0);
    vst1q_s16(x0_ptr, x0[0]);
    vst1q_s16(x0_ptr + 8, x0[1]);
    p_g_real = p_g_real + 2;
    p_g_imag = p_g_imag + 2;
    x0_ptr = x0_ptr + 16;

    int16x8_t y_quadw[4];
    y_quadw[0] = vld1q_s16(y0_ptr);
    y0_ptr = y0_ptr + 8;
    y_quadw[1] = vld1q_s16(y1_ptr);
    y1_ptr = y1_ptr + 8;

    // Load G00[0]
    g_real[0] = vld1_dup_f32(p_g_real);
    g_imag[0] = vld1_dup_f32(p_g_imag);
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y_quadw[0], &y_re[0][0],
                              &y_im[0][0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y_quadw[1], &y_re[1][0],
                              &y_im[1][0]);
    x0_re[0] = vmulq_lane_f32(y_re[0][0], g_real[0], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[0][0], g_imag[0], 0);
    x0_im[0] = vmulq_lane_f32(y_im[0][0], g_real[0], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[0][0], g_imag[0], 0);

    // Load G01[0]
    y_quadw[2] = vld1q_s16(y2_ptr);
    y2_ptr = y2_ptr + 8;
    g_real[1] = vld1_dup_f32(p_g_real + p_gstride);
    g_imag[1] = vld1_dup_f32(p_g_imag + p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y_re[1][0], g_real[1], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[1][0], g_imag[1], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_im[1][0], g_real[1], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[1][0], g_imag[1], 0);

    // Load G02[0]
    y_quadw[3] = vld1q_s16(y3_ptr);
    y3_ptr = y3_ptr + 8;
    armral_convert_i16_f32_x4(num_fract_bits_y[2], y_quadw[2], &y_re[2][0],
                              &y_im[2][0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y_quadw[3], &y_re[3][0],
                              &y_im[3][0]);
    g_real[2] = vld1_dup_f32(p_g_real + 2 * p_gstride);
    g_imag[2] = vld1_dup_f32(p_g_imag + 2 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y_re[2][0], g_real[2], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[2][0], g_imag[2], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_im[2][0], g_real[2], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[2][0], g_imag[2], 0);

    // Load G03[0]
    g_real[3] = vld1_dup_f32(p_g_real + 3 * p_gstride);
    g_imag[3] = vld1_dup_f32(p_g_imag + 3 * p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y_re[3][0], g_real[3], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[3][0], g_imag[3], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_im[3][0], g_real[3], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[3][0], g_imag[3], 0);

    int16x8_t x0_bis;
    armral_convert_f32_i16_x4_2(num_fract_bits_x, x0_re[0], x0_im[0], &x0_bis);

    vst1q_s16(x0_ptr, x0_bis);
    x0_ptr = x0_ptr + 8;
    p_g_real++;
    p_g_imag++;
  }
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_1x2_4sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    armral_fixed_point_index num_fract_bits_x) {

  const int16_t *y0_ptr = (const int16_t *)p_y;
  const int16_t *y1_ptr = (const int16_t *)p_y + p_ystride;

  int16_t *x0_ptr = (int16_t *)p_x;

#if ARMRAL_ARCH_SVE >= 2
  svbool_t pg8 = svptrue_pat_b16(SV_VL8);
  svbool_t pg3 = svptrue_pat_b32(SV_VL3);
  svbool_t podd = svtrn1_b16(svpfalse_b(), pg8);
  float32_t scale[] = {1. / (1U << (16 + p_y_num_fract_bits[0])),
                       1. / (1U << (16 + p_y_num_fract_bits[1]))};
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    // Load the first two y0 entries
    svint32_t y00_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr - 1));
    svint32_t y00_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr));
    svfloat32_t y00_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y00_int_re), scale[0]);
    svfloat32_t y00_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y00_int_im), scale[0]);

    svint32_t y01_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 7));
    svint32_t y01_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 8));
    svfloat32_t y01_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y01_int_re), scale[0]);
    svfloat32_t y01_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y01_int_im), scale[0]);

    // G00[0], G00[1]
    svfloat32_t g00_re = svld1_f32(pg3, &p_g_real[0 * p_gstride]);
    svfloat32_t g00_im = svld1_f32(pg3, &p_g_imag[0 * p_gstride]);
    svfloat32_t x00_re = svmul_lane_f32(y00_re, g00_re, 0);
    x00_re = svmls_lane_f32(x00_re, y00_im, g00_im, 0);
    svfloat32_t x00_im = svmul_lane_f32(y00_re, g00_im, 0);
    x00_im = svmla_lane_f32(x00_im, y00_im, g00_re, 0);

    svfloat32_t x01_re = svmul_lane_f32(y01_re, g00_re, 1);
    x01_re = svmls_lane_f32(x01_re, y01_im, g00_im, 1);
    svfloat32_t x01_im = svmul_lane_f32(y01_re, g00_im, 1);
    x01_im = svmla_lane_f32(x01_im, y01_im, g00_re, 1);

    // Load the first two y1 entries
    svint32_t y10_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr - 1));
    svint32_t y10_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr));
    svfloat32_t y10_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y10_int_re), scale[1]);
    svfloat32_t y10_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y10_int_im), scale[1]);

    svint32_t y11_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 7));
    svint32_t y11_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 8));
    svfloat32_t y11_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y11_int_re), scale[1]);
    svfloat32_t y11_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y11_int_im), scale[1]);

    // G01[0], G01[1]
    svfloat32_t g01_re = svld1_f32(pg3, &p_g_real[1 * p_gstride]);
    svfloat32_t g01_im = svld1_f32(pg3, &p_g_imag[1 * p_gstride]);
    x00_re = svmla_lane_f32(x00_re, y10_re, g01_re, 0);
    x00_re = svmls_lane_f32(x00_re, y10_im, g01_im, 0);
    x00_im = svmla_lane_f32(x00_im, y10_re, g01_im, 0);
    x00_im = svmla_lane_f32(x00_im, y10_im, g01_re, 0);

    x01_re = svmla_lane_f32(x01_re, y11_re, g01_re, 1);
    x01_re = svmls_lane_f32(x01_re, y11_im, g01_im, 1);
    x01_im = svmla_lane_f32(x01_im, y11_re, g01_im, 1);
    x01_im = svmla_lane_f32(x01_im, y11_im, g01_re, 1);

    // Store the first two x0 entries
    svint16_t x00_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x00_re, x00_im, pg8);
    svst1_s16(pg8, x0_ptr, x00_bis);
    svint16_t x01_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x01_re, x01_im, pg8);
    svst1_s16(pg8, x0_ptr + 8, x01_bis);

    // Process the third y entry.
    // Load the third y0 and y1 entry.
    svint32_t y02_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 15));
    svint32_t y02_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 16));
    svfloat32_t y02_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y02_int_re), scale[0]);
    svfloat32_t y02_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y02_int_im), scale[0]);
    y0_ptr = y0_ptr + 24;

    svint32_t y12_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 15));
    svint32_t y12_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 16));
    svfloat32_t y12_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y12_int_re), scale[1]);
    svfloat32_t y12_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y12_int_im), scale[1]);
    y1_ptr = y1_ptr + 24;

    // G00[2]
    svfloat32_t x02_re = svmul_lane_f32(y02_re, g00_re, 2);
    x02_re = svmls_lane_f32(x02_re, y02_im, g00_im, 2);
    svfloat32_t x02_im = svmul_lane_f32(y02_re, g00_im, 2);
    x02_im = svmla_lane_f32(x02_im, y02_im, g00_re, 2);

    // G01[2]
    x02_re = svmla_lane_f32(x02_re, y12_re, g01_re, 2);
    x02_re = svmls_lane_f32(x02_re, y12_im, g01_im, 2);
    x02_im = svmla_lane_f32(x02_im, y12_re, g01_im, 2);
    x02_im = svmla_lane_f32(x02_im, y12_im, g01_re, 2);

    svint16_t x02_bis =
        armral_convert_f32_fixed_i16(num_fract_bits_x, x02_re, x02_im, pg8);
    svst1_s16(pg8, x0_ptr + 16, x02_bis);
    x0_ptr = x0_ptr + 24;

    p_g_real += 3;
    p_g_imag += 3;
  }
#else
  const armral_fixed_point_index num_fract_bits_y[] = {p_y_num_fract_bits[0],
                                                       p_y_num_fract_bits[1]};
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    int16x8_t y[2][2];
    y[0][0] = vld1q_s16(y0_ptr);
    y[0][1] = vld1q_s16(y0_ptr + 8);
    y0_ptr = y0_ptr + 16;
    y[1][0] = vld1q_s16(y1_ptr);
    y[1][1] = vld1q_s16(y1_ptr + 8);
    y1_ptr = y1_ptr + 16;
    float32x4_t y_re[2][2];
    float32x4_t y_im[2][2];
    armral_convert_i16_f32_x8(num_fract_bits_y[0], y[0], y_re[0], y_im[0]);
    armral_convert_i16_f32_x8(num_fract_bits_y[1], y[1], y_re[1], y_im[1]);
    // Load G00[0] G00[1]
    float32x2_t g_real[2]; // G00 G01 G02 G03
    g_real[0] = vld1_f32(p_g_real);
    float32x2_t g_imag[2];
    g_imag[0] = vld1_f32(p_g_imag);

    float32x4_t x0_re[2];
    x0_re[0] = vmulq_lane_f32(y_re[0][0], g_real[0], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[0][0], g_imag[0], 0);
    float32x4_t x0_im[2];
    x0_im[0] = vmulq_lane_f32(y_im[0][0], g_real[0], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[0][0], g_imag[0], 0);

    // Load G00[0] G00[1]

    x0_re[1] = vmulq_lane_f32(y_re[0][1], g_real[0], 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y_im[0][1], g_imag[0], 1);
    x0_im[1] = vmulq_lane_f32(y_im[0][1], g_real[0], 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y_re[0][1], g_imag[0], 1);

    // Load G01[0] G01[1]
    g_real[1] = vld1_f32(p_g_real + p_gstride);
    g_imag[1] = vld1_f32(p_g_imag + p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y_re[1][0], g_real[1], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[1][0], g_imag[1], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_im[1][0], g_real[1], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[1][0], g_imag[1], 0);

    x0_re[1] = vfmaq_lane_f32(x0_re[1], y_re[1][1], g_real[1], 1);
    x0_re[1] = vfmsq_lane_f32(x0_re[1], y_im[1][1], g_imag[1], 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y_im[1][1], g_real[1], 1);
    x0_im[1] = vfmaq_lane_f32(x0_im[1], y_re[1][1], g_imag[1], 1);

    int16x8_t x0[2];
    armral_convert_f32_i16_x8_2(num_fract_bits_x, x0_re, x0_im, x0);
    vst1q_s16(x0_ptr, x0[0]);
    vst1q_s16(x0_ptr + 8, x0[1]);
    p_g_real = p_g_real + 2;
    p_g_imag = p_g_imag + 2;
    x0_ptr = x0_ptr + 16;

    int16x8_t y_quadw[2];
    y_quadw[0] = vld1q_s16(y0_ptr);
    y0_ptr = y0_ptr + 8;
    y_quadw[1] = vld1q_s16(y1_ptr);
    y1_ptr = y1_ptr + 8;

    // Load G00[0]
    g_real[0] = vld1_dup_f32(p_g_real);
    g_imag[0] = vld1_dup_f32(p_g_imag);
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y_quadw[0], &y_re[0][0],
                              &y_im[0][0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y_quadw[1], &y_re[1][0],
                              &y_im[1][0]);
    x0_re[0] = vmulq_lane_f32(y_re[0][0], g_real[0], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[0][0], g_imag[0], 0);
    x0_im[0] = vmulq_lane_f32(y_im[0][0], g_real[0], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[0][0], g_imag[0], 0);

    // Load G01[0]
    g_real[1] = vld1_dup_f32(p_g_real + p_gstride);
    g_imag[1] = vld1_dup_f32(p_g_imag + p_gstride);

    x0_re[0] = vfmaq_lane_f32(x0_re[0], y_re[1][0], g_real[1], 0);
    x0_re[0] = vfmsq_lane_f32(x0_re[0], y_im[1][0], g_imag[1], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_im[1][0], g_real[1], 0);
    x0_im[0] = vfmaq_lane_f32(x0_im[0], y_re[1][0], g_imag[1], 0);

    int16x8_t x0_bis;
    armral_convert_f32_i16_x4_2(num_fract_bits_x, x0_re[0], x0_im[0], &x0_bis);

    vst1q_s16(x0_ptr, x0_bis);
    x0_ptr = x0_ptr + 8;
    p_g_real++;
    p_g_imag++;
  }
#endif
  return ARMRAL_SUCCESS;
}
