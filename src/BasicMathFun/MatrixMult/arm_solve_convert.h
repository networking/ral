/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "intrinsics.h"

static inline int16x8_t __attribute__((always_inline))
shuf_s16_04152637(int16x4_t a, int16x4_t b) {
  // permute as [a[0], b[0], a[1], b[1], a[2], b[2], a[3], b[3]]
  // need to use inline asm since no ACLE zip intrinsics take int16x4_t inputs
  // and produce int16x8_t outputs.
  int16x8_t res;
  asm("zip1 %0.8h, %1.8h, %2.8h" : "=w"(res) : "w"(a), "w"(b));
  return res;
}

static inline void __attribute__((always_inline))
armral_convert_i16_f32_x8(const armral_fixed_point_index num_fract_bits,
                          const int16x8_t *in_vector,
                          float32x4_t *out_vector_re,
                          float32x4_t *out_vector_im) {
  int32x4_t in32[4] = {
      vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), in_vector[0])),
      vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), in_vector[1])),
      vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), in_vector[0])),
      vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), in_vector[1]))};

  in32[0] = vshlq_s32(in32[0], vdupq_n_s32(-num_fract_bits));
  in32[1] = vshlq_s32(in32[1], vdupq_n_s32(-num_fract_bits));
  in32[2] = vshlq_s32(in32[2], vdupq_n_s32(-num_fract_bits));
  in32[3] = vshlq_s32(in32[3], vdupq_n_s32(-num_fract_bits));

  out_vector_re[0] = vcvtq_n_f32_s32(in32[0], 16);
  out_vector_re[1] = vcvtq_n_f32_s32(in32[1], 16);
  out_vector_im[0] = vcvtq_n_f32_s32(in32[2], 16);
  out_vector_im[1] = vcvtq_n_f32_s32(in32[3], 16);
}

static inline void __attribute__((always_inline))
armral_convert_i16_f32_x4(const armral_fixed_point_index num_fract_bits,
                          int16x8_t in_vector, float32x4_t *out_vector_re,
                          float32x4_t *out_vector_im) {

  int32x4_t in32[2] = {
      vreinterpretq_s32_s16(vtrn1q_s16(vdupq_n_s16(0), in_vector)),
      vreinterpretq_s32_s16(vtrn2q_s16(vdupq_n_s16(0), in_vector))};

  in32[0] = vshlq_s32(in32[0], vdupq_n_s32(-num_fract_bits));
  in32[1] = vshlq_s32(in32[1], vdupq_n_s32(-num_fract_bits));

  *out_vector_re = vcvtq_n_f32_s32(in32[0], 16);
  *out_vector_im = vcvtq_n_f32_s32(in32[1], 16);
}

static inline void __attribute__((always_inline))
armral_convert_f32_i16_x8(const armral_fixed_point_index num_fract_bits,
                          const float32x4_t *in_vector0_re,
                          const float32x4_t *in_vector0_im,
                          const float32x4_t *in_vector1_re,
                          const float32x4_t *in_vector1_im,
                          int16x8_t *out_vector0, int16x8_t *out_vector1) {
  float32_t x_mult = 1 << num_fract_bits;

  int32x4_t res00_re = vcvtq_s32_f32(in_vector0_re[0] * x_mult);
  int32x4_t res00_im = vcvtq_s32_f32(in_vector0_im[0] * x_mult);
  int32x4_t res10_re = vcvtq_s32_f32(in_vector1_re[0] * x_mult);
  int32x4_t res10_im = vcvtq_s32_f32(in_vector1_im[0] * x_mult);
  int32x4_t res01_re = vcvtq_s32_f32(in_vector0_re[1] * x_mult);
  int32x4_t res01_im = vcvtq_s32_f32(in_vector0_im[1] * x_mult);
  int32x4_t res11_re = vcvtq_s32_f32(in_vector1_re[1] * x_mult);
  int32x4_t res11_im = vcvtq_s32_f32(in_vector1_im[1] * x_mult);

  int16x4_t res00_16_re_lo = vqmovn_s32(res00_re);
  int16x4_t res00_16_re_hi = vqmovn_s32(res01_re);
  int16x4_t res00_16_im_lo = vqmovn_s32(res00_im);
  int16x4_t res00_16_im_hi = vqmovn_s32(res01_im);
  int16x4_t res10_16_re_lo = vqmovn_s32(res10_re);
  int16x4_t res10_16_re_hi = vqmovn_s32(res11_re);
  int16x4_t res10_16_im_lo = vqmovn_s32(res10_im);
  int16x4_t res10_16_im_hi = vqmovn_s32(res11_im);

  out_vector0[0] = shuf_s16_04152637(res00_16_re_lo, res00_16_im_lo);
  out_vector0[1] = shuf_s16_04152637(res00_16_re_hi, res00_16_im_hi);
  out_vector1[0] = shuf_s16_04152637(res10_16_re_lo, res10_16_im_lo);
  out_vector1[1] = shuf_s16_04152637(res10_16_re_hi, res10_16_im_hi);
}

static inline void __attribute__((always_inline))
armral_convert_f32_i16_x8_2(const armral_fixed_point_index num_fract_bits,
                            const float32x4_t *in_vector0_re,
                            const float32x4_t *in_vector0_im,
                            int16x8_t *out_vector0) {
  float32_t x_mult = 1 << num_fract_bits;

  int32x4_t res00_re = vcvtq_s32_f32(in_vector0_re[0] * x_mult);
  int32x4_t res00_im = vcvtq_s32_f32(in_vector0_im[0] * x_mult);
  int32x4_t res01_re = vcvtq_s32_f32(in_vector0_re[1] * x_mult);
  int32x4_t res01_im = vcvtq_s32_f32(in_vector0_im[1] * x_mult);

  int16x4_t res00_16_re_lo = vqmovn_s32(res00_re);
  int16x4_t res00_16_re_hi = vqmovn_s32(res01_re);
  int16x4_t res00_16_im_lo = vqmovn_s32(res00_im);
  int16x4_t res00_16_im_hi = vqmovn_s32(res01_im);

  out_vector0[0] = shuf_s16_04152637(res00_16_re_lo, res00_16_im_lo);
  out_vector0[1] = shuf_s16_04152637(res00_16_re_hi, res00_16_im_hi);
}

static inline void __attribute__((always_inline))
armral_convert_f32_i16_x4(const armral_fixed_point_index num_fract_bits,
                          float32x4_t in_vector0_re, float32x4_t in_vector0_im,
                          float32x4_t in_vector1_re, float32x4_t in_vector1_im,
                          int16x8_t *out_vector0, int16x8_t *out_vector1) {
  float32_t x_mult = 1 << num_fract_bits;

  int32x4_t res00_re = vcvtq_s32_f32(in_vector0_re * x_mult);
  int32x4_t res00_im = vcvtq_s32_f32(in_vector0_im * x_mult);
  int32x4_t res10_re = vcvtq_s32_f32(in_vector1_re * x_mult);
  int32x4_t res10_im = vcvtq_s32_f32(in_vector1_im * x_mult);

  int16x4_t res16_00re = vqmovn_s32(res00_re);
  int16x4_t res16_00im = vqmovn_s32(res00_im);
  int16x4_t res16_10re = vqmovn_s32(res10_re);
  int16x4_t res16_10im = vqmovn_s32(res10_im);

  *out_vector0 = shuf_s16_04152637(res16_00re, res16_00im);
  *out_vector1 = shuf_s16_04152637(res16_10re, res16_10im);
}

static inline void __attribute__((always_inline))
armral_convert_f32_i16_x4_2(const armral_fixed_point_index num_fract_bits,
                            float32x4_t in_vector0_re,
                            float32x4_t in_vector0_im, int16x8_t *out_vector0) {
  float32_t x_mult = 1 << num_fract_bits;

  int32x4_t res00_re = vcvtq_s32_f32(in_vector0_re * x_mult);
  int32x4_t res00_im = vcvtq_s32_f32(in_vector0_im * x_mult);

  int16x4_t res16_00re = vqmovn_s32(res00_re);
  int16x4_t res16_00im = vqmovn_s32(res00_im);

  *out_vector0 = shuf_s16_04152637(res16_00re, res16_00im);
}

#if ARMRAL_ARCH_SVE >= 2

static inline svint16_t __attribute__((always_inline))
armral_convert_f32_fixed_i16(const armral_fixed_point_index num_fract_bits,
                             const svfloat32_t in_vector_re,
                             const svfloat32_t in_vector_im,
                             const svbool_t pg32) {
  float32_t x_mult = 1 << num_fract_bits;
  svfloat32_t in32_re = svmul_n_f32_x(pg32, in_vector_re, x_mult);
  svfloat32_t in32_im = svmul_n_f32_x(pg32, in_vector_im, x_mult);
  svint32_t res_re = svcvt_s32_f32_x(pg32, in32_re);
  svint32_t res_im = svcvt_s32_f32_x(pg32, in32_im);
  svint16_t res_16_re = svqxtnb_s32(res_re);
  return svqxtnt_s32(res_16_re, res_im);
}

static inline svint16_t __attribute__((always_inline))
armral_convert_f32_i16(const svfloat32_t in_vector_re,
                       const svfloat32_t in_vector_im, const svbool_t pg32) {
  svint32_t res_re = svcvt_s32_f32_x(pg32, in_vector_re);
  svint32_t res_im = svcvt_s32_f32_x(pg32, in_vector_im);
  svint16_t res_16_re = svqxtnb_s32(res_re);
  return svqxtnt_s32(res_16_re, res_im);
}
#endif
