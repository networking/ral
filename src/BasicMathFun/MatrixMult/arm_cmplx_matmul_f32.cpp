/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "armral.h"

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

namespace {

#ifndef ARMRAL_ARCH_SVE
inline float32x4_t __attribute__((always_inline))
vzip1q_f32x2(float32x4_t a, float32x4_t b) {
  // This zips a pair of 32-bit floats in a 128-bit vector, e.g. given 32-bit
  // vectors
  // ^: a = [a0, a1, a2, a3]
  // ^: b = [b0, b1, b2, b3]
  // ^: returns
  // ^: c = [a0, a1, b0, b1]
  return vreinterpretq_f32_f64(
      vzip1q_f64(vreinterpretq_f64_f32(a), vreinterpretq_f64_f32(b)));
}

inline float32x4_t __attribute__((always_inline))
vzip2q_f32x2(float32x4_t a, float32x4_t b) {
  // This zips a pair of 32-bit floats in 128-bit vector, e.g. given 32-bit
  // vectors
  // ^: a = [a0, a1, a2, a3]
  // ^: b = [b0, b1, b2, b3]
  // ^: returns
  // ^: c = [a2, a3, b2, b3]
  return vreinterpretq_f32_f64(
      vzip2q_f64(vreinterpretq_f64_f32(a), vreinterpretq_f64_f32(b)));
}
#endif

#ifdef ARMRAL_ARCH_SVE
// Calculates a vector width of consecutive output elements in a matrix product
// of a m x k and k x n matrix. p_src_a and p_src_b must point to the start
// row/column respectively, the operation must be valid and the result will be
// stored at exactly dst.
template<bool accumulate>
inline void sve_mat_one_row_dot(svbool_t pg, const uint16_t k,
                                const uint16_t b_dst_stride,
                                const armral_cmplx_f32_t *__restrict p_src_a,
                                const armral_cmplx_f32_t *__restrict p_src_b,
                                armral_cmplx_f32_t *dst) {

  svfloat32_t c0;
  if constexpr (accumulate) {
    c0 = svld1_f32(pg, (float32_t *)dst);
  } else {
    c0 = svdup_f32(0);
  }
  for (int h = 0; h < k - 1; h += 2) {
    svbool_t pa = svwhilelt_b32(h * 2, k * 2);
    svfloat32_t a0i = svld1rq_f32(pa, (const float32_t *)&p_src_a[h]);
    svfloat32_t bi0 =
        svld1_f32(pg, (const float32_t *)&p_src_b[h * b_dst_stride]);
    svfloat32_t bi1 = svld1_f32(
        pg, (const float32_t *)&p_src_b[h * b_dst_stride + b_dst_stride]);
    c0 = svcmla_lane_f32(c0, bi0, a0i, 0, 0);
    c0 = svcmla_lane_f32(c0, bi0, a0i, 0, 90);
    c0 = svcmla_lane_f32(c0, bi1, a0i, 1, 0);
    c0 = svcmla_lane_f32(c0, bi1, a0i, 1, 90);
  }

  // If k is odd, we have one more row/col to go
  if (k % 2) {
    svfloat32_t ak =
        svreinterpret_f32_u64(svdup_u64(*((const uint64_t *)&p_src_a[k - 1])));
    svfloat32_t bk =
        svld1_f32(pg, (const float32_t *)&p_src_b[(k - 1) * b_dst_stride]);
    c0 = svcmla_f32_x(pg, c0, ak, bk, 0);
    c0 = svcmla_f32_x(pg, c0, ak, bk, 90);
  }
  svst1_f32(pg, (float32_t *)dst, c0);
}

// Calculates 2 vector widths of consecutive output elements in a matrix product
// of a m x k and k x n matrix. p_src_a and p_src_b must point to the start
// row/column respectively, the operation must be valid and the result will be
// stored at exactly dst.
template<bool accumulate>
inline void sve_mat_two_row_dot(svbool_t pg, const uint16_t k,
                                const uint16_t a_stride,
                                const uint16_t b_dst_stride,
                                const armral_cmplx_f32_t *__restrict p_src_a,
                                const armral_cmplx_f32_t *__restrict p_src_b,
                                armral_cmplx_f32_t *dst) {

  svfloat32_t c0;
  svfloat32_t c1;
  if constexpr (accumulate) {
    c0 = svld1_f32(pg, (float32_t *)&dst[0]);
    c1 = svld1_f32(pg, (float32_t *)&dst[b_dst_stride]);
  } else {
    c0 = svdup_f32(0);
    c1 = svdup_f32(0);
  }
  for (int h = 0; h < k - 1; h += 2) {
    svbool_t pa = svwhilelt_b32(h * 2, k * 2);
    svfloat32_t a0i = svld1rq_f32(pa, (const float32_t *)&p_src_a[h]);
    svfloat32_t a1i =
        svld1rq_f32(pa, (const float32_t *)&p_src_a[h + a_stride]);
    svfloat32_t bi0 =
        svld1_f32(pg, (const float32_t *)&p_src_b[h * b_dst_stride]);
    svfloat32_t bi1 = svld1_f32(
        pg, (const float32_t *)&p_src_b[h * b_dst_stride + b_dst_stride]);

    c0 = svcmla_lane_f32(c0, bi0, a0i, 0, 0);
    c0 = svcmla_lane_f32(c0, bi0, a0i, 0, 90);
    c0 = svcmla_lane_f32(c0, bi1, a0i, 1, 0);
    c0 = svcmla_lane_f32(c0, bi1, a0i, 1, 90);
    c1 = svcmla_lane_f32(c1, bi0, a1i, 0, 0);
    c1 = svcmla_lane_f32(c1, bi0, a1i, 0, 90);
    c1 = svcmla_lane_f32(c1, bi1, a1i, 1, 0);
    c1 = svcmla_lane_f32(c1, bi1, a1i, 1, 90);
  }

  // If k is odd, we have one more row/col to go
  if (k % 2) {
    svfloat32_t a0k =
        svreinterpret_f32_u64(svdup_u64(*((const uint64_t *)&p_src_a[k - 1])));
    svfloat32_t a1k = svreinterpret_f32_u64(
        svdup_u64(*((const uint64_t *)&p_src_a[k - 1 + a_stride])));

    svfloat32_t bk =
        svld1_f32(pg, (const float32_t *)&p_src_b[(k - 1) * b_dst_stride]);
    c0 = svcmla_f32_x(pg, c0, a0k, bk, 0);
    c0 = svcmla_f32_x(pg, c0, a0k, bk, 90);
    c1 = svcmla_f32_x(pg, c1, a1k, bk, 0);
    c1 = svcmla_f32_x(pg, c1, a1k, bk, 90);
  }
  svst1_f32(pg, (float32_t *)&dst[0], c0);
  svst1_f32(pg, (float32_t *)&dst[b_dst_stride], c1);
}
#endif

// Computes c += a b or c = a b depending on whether the value of accumulate is
// true or false. a, b and c are sub-matrices of p_src_a, p_src_b and p_dst,
// respectively, where a_stride is the total number of columns of matrix
// p_src_a and b_dst_stride is the total number of columns of matrices
// p_src_b/p_dst. This function expects row-major input and gives row-major
// output.
template<bool accumulate>
inline armral_status
cmplx_matmul_f32(const uint16_t m, const uint16_t n, const uint16_t k,
                 uint16_t a_stride, uint16_t b_dst_stride,
                 const armral_cmplx_f32_t *__restrict p_src_a,
                 const armral_cmplx_f32_t *__restrict p_src_b,
                 armral_cmplx_f32_t *__restrict p_dst) {
#ifdef ARMRAL_ARCH_SVE
  for (int i = 0; i < m - 1; i += 2) {
    for (int j = 0; j < n; j += svcntd()) {
      const svbool_t pg = svwhilelt_b32(2 * j, 2 * n);
      sve_mat_two_row_dot<accumulate>(pg, k, a_stride, b_dst_stride,
                                      &p_src_a[i * a_stride], &p_src_b[j],
                                      &p_dst[i * b_dst_stride + j]);
    }
  }
  if (m % 2) {
    const int i = m - 1;
    for (int j = 0; j < n; j += svcntd()) {
      const svbool_t pg = svwhilelt_b32(2 * j, 2 * n);
      sve_mat_one_row_dot<accumulate>(pg, k, b_dst_stride,
                                      &p_src_a[i * a_stride], &p_src_b[j],
                                      &p_dst[i * b_dst_stride + j]);
    }
  }
  return ARMRAL_SUCCESS;
#else
  const float32_t *p_in1 = (const float32_t *)p_src_a;
  const float32_t *p_in2 = (const float32_t *)p_src_b;
  const armral_cmplx_f32_t *p_in_a = p_src_a;
  armral_cmplx_f32_t *p_out = p_dst;
  armral_cmplx_f32_t *px;
  uint16_t num_rows_a = m;
  uint16_t num_cols_b = n;
  uint16_t num_cols_a = k;

  float32x4x2_t a0_v;
  float32x4x2_t a1_v;
  float32x4_t temp_r2;
  float32x4_t temp_i2;
  float32x4_t b0_v;
  float32x4_t b1_v;
  float32x4_t b2_v;
  float32x4_t b3_v;
  float32x4_t b_col_real;
  float32x4_t b_col_im;
  float32x4_t b_col_real2;
  float32x4_t b_col_im2;
  const float32_t *p_in1_b = (const float32_t *)p_src_a;
  const float32_t *p_in1_b2 = (const float32_t *)p_src_b;

  uint16_t col;
  uint16_t i = 0U;
  uint16_t j;
  uint16_t row_cnt;
  uint16_t row = num_rows_a;
  uint16_t col_cnt;
  armral_cmplx_f32_t *px_b;

  // The following loop performs the dot-product of each row in pSrcA with each
  // column in pSrcB

  row_cnt = row >> 1;
  // Row loop
  while (row_cnt > 0U) {
    // Output pointer is set to starting address of the row being processed
    px = p_out + i;
    px_b = px + b_dst_stride;

    // For every row wise process, the column loop counter is to be initiated
    col = num_cols_b;

    // For every row wise process, the pIn2 pointer is set
    // to the starting address of the pSrcB data
    p_in2 = (const float32_t *)p_src_b;
    p_in1_b2 = p_in2 + 2 * b_dst_stride;

    j = 0U;

    // Column loop
    col >>= 1;
    while (col > 0U) {
      // Set the variable sum, that acts as accumulator, to zero
      float32_t sum_real1 = 0.0F;
      float32_t sum_imag1 = 0.0F;
      float32_t sum_real1_b = 0.0F;
      float32_t sum_imag1_b = 0.0F;

      float32_t sum_real2 = 0.0F;
      float32_t sum_imag2 = 0.0F;
      float32_t sum_real2_b = 0.0F;
      float32_t sum_imag2_b = 0.0F;

      float32_t sum_real3 = 0.0F;
      float32_t sum_imag3 = 0.0F;
      float32_t sum_real3_b = 0.0F;
      float32_t sum_imag3_b = 0.0F;

      float32_t sum_real4 = 0.0F;
      float32_t sum_imag4 = 0.0F;
      float32_t sum_real4_b = 0.0F;
      float32_t sum_imag4_b = 0.0F;

      // Initialize the pointer pIn1 to point to the starting address of the
      // column being processed
      p_in1 = (const float32_t *)p_in_a;
      p_in1_b = p_in1 + 2 * a_stride;

      float32x4_t acc_r0 = {};
      float32x4_t acc_i0 = {};
      float32x4_t acc_r1 = {};
      float32x4_t acc_i1 = {};
      float32x4_t acc_r2 = {};
      float32x4_t acc_i2 = {};
      float32x4_t acc_r3 = {};
      float32x4_t acc_i3 = {};

      // Compute 4 MACs simultaneously
      col_cnt = num_cols_a >> 2;

      // Matrix multiplication
      while (col_cnt > 0U) {
        // Load & separate real/imag pSrcA (de-interleave 2)
        a0_v = vld2q_f32(p_in1);
        // Load & separate real/imag pSrcA (de-interleave 2)
        a1_v = vld2q_f32(p_in1_b);

        p_in1 += 8;
        p_in1_b += 8;

        // Load but don't separate real/imag
        b0_v = vld1q_f32(p_in2);
        b1_v = vld1q_f32(p_in1_b2);
        b2_v = vld1q_f32(p_in2 + 4 * b_dst_stride);
        b3_v = vld1q_f32(p_in1_b2 + 4 * b_dst_stride);

        p_in2 = p_in2 + 8 * b_dst_stride;
        p_in1_b2 = p_in1_b2 + 8 * b_dst_stride;
        b_col_real = vtrn1q_f32(b0_v, b1_v);  // even elem
        b_col_im = vtrn2q_f32(b0_v, b1_v);    // odd elem
        b_col_real2 = vtrn1q_f32(b2_v, b3_v); // even elem
        b_col_im2 = vtrn2q_f32(b2_v, b3_v);   // odd elem

        // First column
        float32x4_t temp_r = vzip1q_f32x2(b_col_real, b_col_real2);
        float32x4_t temp_i = vzip1q_f32x2(b_col_im, b_col_im2);

        // Second column
        temp_r2 = vzip2q_f32x2(b_col_real, b_col_real2);
        temp_i2 = vzip2q_f32x2(b_col_im, b_col_im2);

        acc_r0 = vfmaq_f32(acc_r0, a0_v.val[0], temp_r);
        acc_r0 = vfmsq_f32(acc_r0, a0_v.val[1], temp_i);
        acc_i0 = vfmaq_f32(acc_i0, a0_v.val[1], temp_r);
        acc_i0 = vfmaq_f32(acc_i0, a0_v.val[0], temp_i);
        // Same row A, next column B
        acc_r2 = vfmaq_f32(acc_r2, a0_v.val[0], temp_r2);
        acc_r2 = vfmsq_f32(acc_r2, a0_v.val[1], temp_i2);

        acc_i2 = vfmaq_f32(acc_i2, a0_v.val[1], temp_r2);
        acc_i2 = vfmaq_f32(acc_i2, a0_v.val[0], temp_i2);

        acc_r1 = vfmaq_f32(acc_r1, a1_v.val[0], temp_r);
        acc_r1 = vfmsq_f32(acc_r1, a1_v.val[1], temp_i);

        acc_i1 = vfmaq_f32(acc_i1, a1_v.val[1], temp_r);
        acc_i1 = vfmaq_f32(acc_i1, a1_v.val[0], temp_i);
        // Same row A, next column B
        acc_r3 = vfmaq_f32(acc_r3, a1_v.val[0], temp_r2);
        acc_r3 = vfmsq_f32(acc_r3, a1_v.val[1], temp_i2);

        acc_i3 = vfmaq_f32(acc_i3, a1_v.val[1], temp_r2);
        acc_i3 = vfmaq_f32(acc_i3, a1_v.val[0], temp_i2);

        col_cnt--;
      }

      sum_real1 += vaddvq_f32(acc_r0);
      sum_imag1 += vaddvq_f32(acc_i0);
      sum_real3 += vaddvq_f32(acc_r2);
      sum_imag3 += vaddvq_f32(acc_i2);

      sum_real1_b += vaddvq_f32(acc_r1);
      sum_imag1_b += vaddvq_f32(acc_i1);
      sum_real3_b += vaddvq_f32(acc_r3);
      sum_imag3_b += vaddvq_f32(acc_i3);

      // If the columns of pSrcA is not a multiple of 4, compute any remaining
      // MACs here.
      // No loop unrolling is used.
      col_cnt = num_cols_a & 3;
      while (col_cnt > 0U) {

        float32_t a1 = *p_in1;
        float32_t a1_b = *p_in1_b;

        float32_t c1 = *p_in2;
        float32_t c1_b = *(p_in2 + 2U);

        float32_t b1 = *(p_in1 + 1U);
        float32_t b1_b = *(p_in1_b + 1U);

        float32_t d1 = *(p_in2 + 1U);
        float32_t d1_b = *(p_in2 + 3U);

        sum_real1 += a1 * c1;
        sum_imag1 += b1 * c1;

        sum_real3 += a1 * c1_b;
        sum_imag3 += b1 * c1_b;

        sum_real1_b += a1_b * c1;
        sum_imag1_b += b1_b * c1;

        sum_real3_b += a1_b * c1_b;
        sum_imag3_b += b1_b * c1_b;

        p_in1 += 2U;
        p_in1_b += 2U;
        p_in2 += 2 * b_dst_stride;

        sum_real2 -= b1 * d1;
        sum_imag2 += a1 * d1;

        sum_real4 -= b1 * d1_b;
        sum_imag4 += a1 * d1_b;

        sum_real2_b -= b1_b * d1;
        sum_imag2_b += a1_b * d1;

        sum_real4_b -= b1_b * d1_b;
        sum_imag4_b += a1_b * d1_b;

        col_cnt--;
      }

      sum_real1 += sum_real2;
      sum_imag1 += sum_imag2;

      sum_real3 += sum_real4;
      sum_imag3 += sum_imag4;

      sum_real1_b += sum_real2_b;
      sum_imag1_b += sum_imag2_b;

      sum_real3_b += sum_real4_b;
      sum_imag3_b += sum_imag4_b;

      // Store the result in the destination buffer
      if constexpr (accumulate) {
        (*px).re += sum_real1;
        (*px).im += sum_imag1;
        px++;
        (*px).re += sum_real3;
        (*px).im += sum_imag3;
        px++;
        (*px_b).re += sum_real1_b;
        (*px_b).im += sum_imag1_b;
        px_b++;
        (*px_b).re += sum_real3_b;
        (*px_b).im += sum_imag3_b;
        px_b++;
      } else {
        (*px).re = sum_real1;
        (*px).im = sum_imag1;
        px++;
        (*px).re = sum_real3;
        (*px).im = sum_imag3;
        px++;
        (*px_b).re = sum_real1_b;
        (*px_b).im = sum_imag1_b;
        px_b++;
        (*px_b).re = sum_real3_b;
        (*px_b).im = sum_imag3_b;
        px_b++;
      }
      // Update the pointer pIn2 to point to the starting address of the
      // next column
      j++;
      p_in2 = (const float32_t *)p_src_b + 4U * j;
      p_in1_b2 = p_in2 + 2U * b_dst_stride;
      col--;
    }

    col = num_cols_b & 1;
    if (col) {
      //  Set the variable sum, that acts as accumulator, to zero
      float32_t sum_real1 = 0.0F;
      float32_t sum_imag1 = 0.0F;
      float32_t sum_real2 = 0.0F;
      float32_t sum_imag2 = 0.0F;
      float32_t sum_real1_b = 0.0F;
      float32_t sum_imag1_b = 0.0F;
      float32_t sum_real2_b = 0.0F;
      float32_t sum_imag2_b = 0.0F;

      // Initialize the pointer pIn1 to point to the starting address of the
      // column being processed
      p_in1 = (const float32_t *)p_in_a;
      p_in1_b = p_in1 + 2 * a_stride;

      float32x4_t acc_r0 = {};
      float32x4_t acc_i0 = {};
      float32x4_t acc_r1 = {};
      float32x4_t acc_i1 = {};

      // Compute 4 MACs simultaneously
      col_cnt = num_cols_a >> 2;

      // Matrix multiplication
      while (col_cnt > 0U) {
        // Load & separate real/imag pSrcA (de-interleave 2)
        a0_v = vld2q_f32(p_in1);
        a1_v = vld2q_f32(p_in1_b);

        p_in1 += 8;
        p_in1_b += 8;

        // Load but don't separate real/imag
        float32x2_t b_four_rows[4];
        b_four_rows[0] = vld1_f32(p_in2);
        b_four_rows[1] = vld1_f32(p_in1_b2);
        b_four_rows[2] = vld1_f32(p_in2 + 4 * b_dst_stride);
        b_four_rows[3] = vld1_f32(p_in1_b2 + 4 * b_dst_stride);

        p_in2 = p_in2 + 8 * b_dst_stride;
        p_in1_b2 = p_in1_b2 + 8 * b_dst_stride;
        float32x4_t b_tmp_real;
        float32x4_t b_tmp_im;
        b_tmp_real = vcombine_f32(vtrn1_f32(b_four_rows[0], b_four_rows[1]),
                                  vtrn1_f32(b_four_rows[2], b_four_rows[3]));
        b_tmp_im = vcombine_f32(vtrn2_f32(b_four_rows[0], b_four_rows[1]),
                                vtrn2_f32(b_four_rows[2], b_four_rows[3]));

        // Real * real
        acc_r0 = vfmaq_f32(acc_r0, a0_v.val[0], b_tmp_real);
        // Imag * imag
        acc_r0 = vfmsq_f32(acc_r0, a0_v.val[1], b_tmp_im);
        // Imag * real
        acc_i0 = vfmaq_f32(acc_i0, a0_v.val[1], b_tmp_real);
        // Real * imag
        acc_i0 = vfmaq_f32(acc_i0, a0_v.val[0], b_tmp_im);

        // Real * real
        acc_r1 = vfmaq_f32(acc_r1, a1_v.val[0], b_tmp_real);
        // Imag * imag
        acc_r1 = vfmsq_f32(acc_r1, a1_v.val[1], b_tmp_im);
        // Imag * real
        acc_i1 = vfmaq_f32(acc_i1, a1_v.val[1], b_tmp_real);
        // Real * imag
        acc_i1 = vfmaq_f32(acc_i1, a1_v.val[0], b_tmp_im);

        col_cnt--;
      }

      sum_real1 += vaddvq_f32(acc_r0);
      sum_imag1 += vaddvq_f32(acc_i0);

      sum_real1_b += vaddvq_f32(acc_r1);
      sum_imag1_b += vaddvq_f32(acc_i1);

      // If the columns of pSrcA is not a multiple of 4, compute any remaining
      // MACs here.
      // No loop unrolling is used.
      col_cnt = num_cols_a & 3;
      while (col_cnt > 0U) {

        float32_t a1 = *p_in1; // real part of entry from A
        float32_t a1_b = *p_in1_b;
        float32_t c1 = *p_in2; // real part of entry from B

        float32_t b1 = *(p_in1 + 1U); // imaginary part of entry from A
        float32_t b1_b = *(p_in1_b + 1U);
        float32_t d1 = *(p_in2 + 1U); // imaginary part of entry from B

        // Real * real
        sum_real1 += a1 * c1;
        // Imag * real
        sum_imag1 += b1 * c1;

        // Imag * imag
        sum_real2 -= b1 * d1;
        // Real * imag
        sum_imag2 += a1 * d1;

        // Real * real
        sum_real1_b += a1_b * c1;
        // Imag * real
        sum_imag1_b += b1_b * c1;

        // Imag * imag
        sum_real2_b -= b1_b * d1;
        // Real * imag
        sum_imag2_b += a1_b * d1;

        p_in1 += 2U;
        p_in1_b += 2U;
        p_in2 += 2 * b_dst_stride;

        col_cnt--;
      }

      sum_real1 += sum_real2;
      sum_imag1 += sum_imag2;

      sum_real1_b += sum_real2_b;
      sum_imag1_b += sum_imag2_b;

      // Store the result in the destination buffer
      if constexpr (accumulate) {
        (*px).re += sum_real1;
        (*px).im += sum_imag1;
        (*px_b).re += sum_real1_b;
        (*px_b).im += sum_imag1_b;
      } else {
        (*px).re = sum_real1;
        (*px).im = sum_imag1;
        (*px_b).re = sum_real1_b;
        (*px_b).im = sum_imag1_b;
      }
    }

    // Update the pointer pInA to point to the  starting address of the next 2
    // row
    i = i + 2 * b_dst_stride;
    p_in_a = p_in_a + 2 * a_stride;

    row_cnt--;
  }

  row_cnt = row & 1;
  while (row_cnt > 0U) {
    // Output pointer is set to starting address of the row being processed
    px = p_out + i;

    // For every row wise process, the column loop counter is to be initiated
    col = num_cols_b;

    // For every row wise process, the pIn2 pointer is set
    // to the starting address of the pSrcB data
    p_in2 = (const float32_t *)p_src_b;

    j = 0U;

    // Column loop
    while (col > 0U) {
      // Set the variable sum, that acts as accumulator, to zero
      float32_t sum_real1 = 0.0F;
      float32_t sum_imag1 = 0.0F;

      float32_t sum_real2 = 0.0F;
      float32_t sum_imag2 = 0.0F;

      // Initialize the pointer pIn1 to point to the starting address of the
      // column being processed
      p_in1 = (const float32_t *)p_in_a;

      float32x4_t acc_r0 = {};
      float32x4_t acc_i0 = {};

      // Compute 4 MACs simultaneously
      col_cnt = num_cols_a >> 2;

      // Matrix multiplication
      while (col_cnt > 0U) {
        float32x4_t temp_r = {};
        float32x4_t temp_i = {};
        // Reading real part of complex matrix A
        // load & separate real/imag p_src_a (de-interleave 2)
        a0_v = vld2q_f32(p_in1);
        p_in1 += 8;

        temp_r[0] = *p_in2;
        temp_i[0] = *(p_in2 + 1U);
        p_in2 += 2 * b_dst_stride;

        temp_r[1] = *p_in2;
        temp_i[1] = *(p_in2 + 1U);
        p_in2 += 2 * b_dst_stride;

        temp_r[2] = *p_in2;
        temp_i[2] = *(p_in2 + 1U);
        p_in2 += 2 * b_dst_stride;

        temp_r[3] = *p_in2;
        temp_i[3] = *(p_in2 + 1U);
        p_in2 += 2 * b_dst_stride;

        acc_r0 = vfmaq_f32(acc_r0, a0_v.val[0], temp_r);
        acc_r0 = vfmsq_f32(acc_r0, a0_v.val[1], temp_i);

        acc_i0 = vfmaq_f32(acc_i0, a0_v.val[1], temp_r);
        acc_i0 = vfmaq_f32(acc_i0, a0_v.val[0], temp_i);

        col_cnt--;
      }

      float32x2_t accum =
          vpadd_f32(vget_low_f32(acc_r0), vget_high_f32(acc_r0));
      sum_real1 += accum[0] + accum[1];

      accum = vpadd_f32(vget_low_f32(acc_i0), vget_high_f32(acc_i0));
      sum_imag1 += accum[0] + accum[1];

      // If the columns of pSrcA is not a multiple of 4, compute any remaining
      // MACs here.
      // No loop unrolling is used.
      col_cnt = num_cols_a & 3;

      while (col_cnt > 0U) {
        // c(m,n) = a(1,1)*b(1,1) + a(1,2)*b(2,1) + ... + a(m,p)*b(p,n)
        float32_t a1 = *p_in1;
        float32_t c1 = *p_in2;

        float32_t b1 = *(p_in1 + 1U);
        float32_t d1 = *(p_in2 + 1U);

        sum_real1 += a1 * c1;
        sum_imag1 += b1 * c1;

        p_in1 += 2U;
        p_in2 += 2 * b_dst_stride;

        sum_real2 -= b1 * d1;
        sum_imag2 += a1 * d1;

        col_cnt--;
      }

      sum_real1 += sum_real2;
      sum_imag1 += sum_imag2;

      // Store the result in the destination buffer
      if constexpr (accumulate) {
        (*px).re += sum_real1;
        (*px).im += sum_imag1;
      } else {
        (*px).re = sum_real1;
        (*px).im = sum_imag1;
      }
      px++;
      // Update the pointer pIn2 to point to the  starting address of the next
      // column
      j++;
      p_in2 = (const float32_t *)p_src_b + 2U * j;

      col--;
    }

    // Update the pointer pInA to point to the  starting address of the next
    // row
    i = i + b_dst_stride;
    p_in_a = p_in_a + a_stride;

    row_cnt--;
  }

  return ARMRAL_SUCCESS;
#endif
}

#ifdef ARMRAL_ARCH_SVE
inline svfloat32_t __attribute__((always_inline)) svtrn2iq_f32(svfloat32_t a) {
  // Interleaves 32-bit floating point numbers at odd 64-bit indices in an
  // SVE vector, e.g. given 256-bit vector
  // ^: a = [a0, a1, a2, a3, a4, a5, a6, a7, ...]
  // ^: returns
  // ^: c = [a2, a3, a2, a3, a6, a7, a6, a7, ...]
  return svreinterpret_f32_f64(
      svtrn2_f64(svreinterpret_f64_f32(a), svreinterpret_f64_f32(a)));
}
#endif

// Computes c += a b or c = a b depending on whether the value of accumulate is
// true or false. a, b and c are sub-matrices of p_src_a, p_src_b and p_dst,
// respectively, where a, b and c have size 4-by-4. a_stride is the total
// number of columns of matrix p_src_a and b_dst_stride is the total number of
// columns of matrices p_dst_b/p_dst. This function expects row-major input and
// gives row-major output.
template<bool accumulate>
inline armral_status
cmplx_matmul_4x4_f32(uint16_t a_stride, uint16_t b_dst_stride,
                     const armral_cmplx_f32_t *__restrict p_src_a,
                     const armral_cmplx_f32_t *__restrict p_src_b,
                     armral_cmplx_f32_t *__restrict p_dst) {
  const float32_t *a_ptr = (const float32_t *)p_src_a;
  const float32_t *b_ptr = (const float32_t *)p_src_b;
  float32_t *out_ptr = (float32_t *)p_dst;

#if ARMRAL_ARCH_SVE >= 2
  svbool_t p4 = svptrue_pat_b32(SV_VL4);

  svfloat32_t b00 = svld1_f32(p4, &b_ptr[0]);
  svfloat32_t b10 = svld1_f32(p4, &b_ptr[4]);
  svfloat32_t b01 = svld1_f32(p4, &b_ptr[2 * b_dst_stride]);
  svfloat32_t b11 = svld1_f32(p4, &b_ptr[2 * b_dst_stride + 4]);
  svfloat32_t b02 = svld1_f32(p4, &b_ptr[4 * b_dst_stride]);
  svfloat32_t b12 = svld1_f32(p4, &b_ptr[4 * b_dst_stride + 4]);
  svfloat32_t b03 = svld1_f32(p4, &b_ptr[6 * b_dst_stride]);
  svfloat32_t b13 = svld1_f32(p4, &b_ptr[6 * b_dst_stride + 4]);

  svfloat32_t cj0;
  svfloat32_t cj1;
  for (int j = 0; j < 4; j++) {
    if constexpr (accumulate) {
      cj0 = svld1_f32(p4, &out_ptr[j * 2 * b_dst_stride]);
      cj1 = svld1_f32(p4, &out_ptr[4 + j * 2 * b_dst_stride]);
    } else {
      cj0 = svdup_n_f32(0);
      cj1 = svdup_n_f32(0);
    }
    svfloat32_t a0j = svld1_f32(p4, &a_ptr[j * 2 * a_stride]);
    svfloat32_t a1j = svld1_f32(p4, &a_ptr[4 + j * 2 * a_stride]);
    cj0 = svcmla_lane_f32(cj0, b00, a0j, 0, 0);
    cj0 = svcmla_lane_f32(cj0, b00, a0j, 0, 90);
    cj0 = svcmla_lane_f32(cj0, b01, a0j, 1, 0);
    cj0 = svcmla_lane_f32(cj0, b01, a0j, 1, 90);
    cj0 = svcmla_lane_f32(cj0, b02, a1j, 0, 0);
    cj0 = svcmla_lane_f32(cj0, b02, a1j, 0, 90);
    cj0 = svcmla_lane_f32(cj0, b03, a1j, 1, 0);
    cj0 = svcmla_lane_f32(cj0, b03, a1j, 1, 90);

    cj1 = svcmla_lane_f32(cj1, b10, a0j, 0, 0);
    cj1 = svcmla_lane_f32(cj1, b10, a0j, 0, 90);
    cj1 = svcmla_lane_f32(cj1, b11, a0j, 1, 0);
    cj1 = svcmla_lane_f32(cj1, b11, a0j, 1, 90);
    cj1 = svcmla_lane_f32(cj1, b12, a1j, 0, 0);
    cj1 = svcmla_lane_f32(cj1, b12, a1j, 0, 90);
    cj1 = svcmla_lane_f32(cj1, b13, a1j, 1, 0);
    cj1 = svcmla_lane_f32(cj1, b13, a1j, 1, 90);

    svst1_f32(p4, &out_ptr[j * 2 * b_dst_stride], cj0);
    svst1_f32(p4, &out_ptr[4 + j * 2 * b_dst_stride], cj1);
  }

#else
  if constexpr (accumulate) {
    // Accumulate result into p_dst array
    __asm__ __volatile__(
        "lsl  x5, %x[AStride], #3\n"
        "lsl  x6, %x[BDstStride], #3\n"
        "neg  x7, x6\n"

        "ld2  {v10.4s, v11.4s}, [%x[BPtr]], x6\n"
        "ld2  {v12.4s, v13.4s}, [%x[BPtr]], x6\n"

        "ld2  {v18.4s, v19.4s}, [%x[APtr]], x5\n"
        "ld2  {v20.4s, v21.4s}, [%x[APtr]], x5\n"

        "ld2  {v2.4s, v3.4s}, [%x[outPtr]], x6\n"
        "ld2  {v4.4s, v5.4s}, [%x[outPtr]], x7\n"

        "fmla v2.4s, v10.4s, v18.s[0]\n"
        "fmls v2.4s, v11.4s, v19.s[0]\n"
        "fmla v3.4s, v11.4s, v18.s[0]\n"
        "fmla v3.4s, v10.4s, v19.s[0]\n"

        "fmla v4.4s, v10.4s, v20.s[0]\n"
        "fmls v4.4s, v11.4s, v21.s[0]\n"
        "fmla v5.4s, v11.4s, v20.s[0]\n"
        "fmla v5.4s, v10.4s, v21.s[0]\n"

        "ld2  {v14.4s, v15.4s}, [%x[BPtr]], x6\n"
        "ld2  {v16.4s, v17.4s}, [%x[BPtr]], x6\n"

        "fmla v2.4s, v12.4s, v18.s[1]\n"
        "fmls v2.4s, v13.4s, v19.s[1]\n"
        "fmla v3.4s, v13.4s, v18.s[1]\n"
        "fmla v3.4s, v12.4s, v19.s[1]\n"

        "fmla v4.4s, v12.4s, v20.s[1]\n"
        "fmls v4.4s, v13.4s, v21.s[1]\n"
        "fmla v5.4s, v13.4s, v20.s[1]\n"
        "fmla v5.4s, v12.4s, v21.s[1]\n"

        "fmla v2.4s, v14.4s, v18.s[2]\n"
        "fmls v2.4s, v15.4s, v19.s[2]\n"
        "fmla v3.4s, v15.4s, v18.s[2]\n"
        "fmla v3.4s, v14.4s, v19.s[2]\n"

        "fmla v4.4s, v14.4s, v20.s[2]\n"
        "fmls v4.4s, v15.4s, v21.s[2]\n"
        "fmla v5.4s, v15.4s, v20.s[2]\n"
        "fmla v5.4s, v14.4s, v21.s[2]\n"

        "fmla v2.4s, v16.4s, v18.s[3]\n"
        "fmls v2.4s, v17.4s, v19.s[3]\n"
        "fmla v3.4s, v17.4s, v18.s[3]\n"
        "fmla v3.4s, v16.4s, v19.s[3]\n"

        "st2  {v2.4s, v3.4s}, [%x[outPtr]], x6\n"
        "ld2  {v18.4s, v19.4s}, [%x[APtr]], x5\n"

        "fmla v4.4s, v16.4s, v20.s[3]\n"
        "fmls v4.4s, v17.4s, v21.s[3]\n"
        "fmla v5.4s, v17.4s, v20.s[3]\n"
        "fmla v5.4s, v16.4s, v21.s[3]\n"

        "st2  {v4.4s, v5.4s}, [%x[outPtr]], x6\n"
        "ld2  {v20.4s, v21.4s}, [%x[APtr]], x5\n"

        "ld2  {v2.4s, v3.4s}, [%x[outPtr]], x6\n"
        "ld2  {v4.4s, v5.4s}, [%x[outPtr]], x7\n"

        "fmla v2.4s, v10.4s, v18.s[0]\n"
        "fmls v2.4s, v11.4s, v19.s[0]\n"
        "fmla v3.4s, v11.4s, v18.s[0]\n"
        "fmla v3.4s, v10.4s, v19.s[0]\n"

        "fmla v4.4s, v10.4s, v20.s[0]\n"
        "fmls v4.4s, v11.4s, v21.s[0]\n"
        "fmla v5.4s, v11.4s, v20.s[0]\n"
        "fmla v5.4s, v10.4s, v21.s[0]\n"

        "fmla v2.4s, v12.4s, v18.s[1]\n"
        "fmls v2.4s, v13.4s, v19.s[1]\n"
        "fmla v3.4s, v13.4s, v18.s[1]\n"
        "fmla v3.4s, v12.4s, v19.s[1]\n"

        "fmla v4.4s, v12.4s, v20.s[1]\n"
        "fmls v4.4s, v13.4s, v21.s[1]\n"
        "fmla v5.4s, v13.4s, v20.s[1]\n"
        "fmla v5.4s, v12.4s, v21.s[1]\n"

        "fmla v2.4s, v14.4s, v18.s[2]\n"
        "fmls v2.4s, v15.4s, v19.s[2]\n"
        "fmla v3.4s, v15.4s, v18.s[2]\n"
        "fmla v3.4s, v14.4s, v19.s[2]\n"

        "fmla v4.4s, v14.4s, v20.s[2]\n"
        "fmls v4.4s, v15.4s, v21.s[2]\n"
        "fmla v5.4s, v15.4s, v20.s[2]\n"
        "fmla v5.4s, v14.4s, v21.s[2]\n"

        "fmla v2.4s, v16.4s, v18.s[3]\n"
        "fmls v2.4s, v17.4s, v19.s[3]\n"
        "fmla v3.4s, v17.4s, v18.s[3]\n"
        "fmla v3.4s, v16.4s, v19.s[3]\n"

        "fmla v4.4s, v16.4s, v20.s[3]\n"
        "fmls v4.4s, v17.4s, v21.s[3]\n"
        "fmla v5.4s, v17.4s, v20.s[3]\n"
        "fmla v5.4s, v16.4s, v21.s[3]\n"

        "st2  {v2.4s, v3.4s}, [%x[outPtr]], x6\n"
        "st2  {v4.4s, v5.4s}, [%x[outPtr]]\n"

        : [APtr] "+r"(a_ptr), [BPtr] "+r"(b_ptr), [outPtr] "+r"(out_ptr)

        : [AStride] "r"(a_stride), [BDstStride] "r"(b_dst_stride)

        : "v10", "v11", "v12", "v13", "v14", "v15", "v16", "v17", "v18", "v19",
          "v20", "v21", "v22", "v23", "v2", "v3", "v4", "v5", "x5", "x6", "x7",
          "cc", "memory");
  } else {
    __asm__ __volatile__(
        "lsl  x5, %x[AStride], #3\n"
        "lsl  x6, %x[BDstStride], #3\n"

        "ld2  {v10.4s, v11.4s}, [%x[BPtr]], x6\n"
        "ld2  {v18.4s, v19.4s}, [%x[APtr]], x5\n"
        "ld2  {v20.4s, v21.4s}, [%x[APtr]], x5\n"
        "ld2  {v12.4s, v13.4s}, [%x[BPtr]], x6\n"

        "fmul v2.4s, v10.4s, v18.s[0]\n"
        "fmls v2.4s, v11.4s, v19.s[0]\n"
        "fmul v3.4s, v11.4s, v18.s[0]\n"
        "fmla v3.4s, v10.4s, v19.s[0]\n"

        "fmul v4.4s, v10.4s, v20.s[0]\n"
        "fmls v4.4s, v11.4s, v21.s[0]\n"
        "fmul v5.4s, v11.4s, v20.s[0]\n"
        "fmla v5.4s, v10.4s, v21.s[0]\n"

        "ld2  {v14.4s, v15.4s}, [%x[BPtr]], x6\n"
        "ld2  {v16.4s, v17.4s}, [%x[BPtr]], x6\n"

        "fmla v2.4s, v12.4s, v18.s[1]\n"
        "fmls v2.4s, v13.4s, v19.s[1]\n"
        "fmla v3.4s, v13.4s, v18.s[1]\n"
        "fmla v3.4s, v12.4s, v19.s[1]\n"

        "fmla v4.4s, v12.4s, v20.s[1]\n"
        "fmls v4.4s, v13.4s, v21.s[1]\n"
        "fmla v5.4s, v13.4s, v20.s[1]\n"
        "fmla v5.4s, v12.4s, v21.s[1]\n"

        "fmla v2.4s, v14.4s, v18.s[2]\n"
        "fmls v2.4s, v15.4s, v19.s[2]\n"
        "fmla v3.4s, v15.4s, v18.s[2]\n"
        "fmla v3.4s, v14.4s, v19.s[2]\n"

        "fmla v4.4s, v14.4s, v20.s[2]\n"
        "fmls v4.4s, v15.4s, v21.s[2]\n"
        "fmla v5.4s, v15.4s, v20.s[2]\n"
        "fmla v5.4s, v14.4s, v21.s[2]\n"

        "fmla v2.4s, v16.4s, v18.s[3]\n"
        "fmls v2.4s, v17.4s, v19.s[3]\n"
        "fmla v3.4s, v17.4s, v18.s[3]\n"
        "fmla v3.4s, v16.4s, v19.s[3]\n"

        "st2  {v2.4s, v3.4s}, [%x[outPtr]], x6\n"
        "ld2  {v18.4s, v19.4s}, [%x[APtr]], x5\n"

        "fmla v4.4s, v16.4s, v20.s[3]\n"
        "fmls v4.4s, v17.4s, v21.s[3]\n"
        "fmla v5.4s, v17.4s, v20.s[3]\n"
        "fmla v5.4s, v16.4s, v21.s[3]\n"

        "st2  {v4.4s, v5.4s}, [%x[outPtr]], x6\n"
        "ld2  {v20.4s, v21.4s}, [%x[APtr]], x5\n"

        "fmul v2.4s, v10.4s, v18.s[0]\n"
        "fmls v2.4s, v11.4s, v19.s[0]\n"
        "fmul v3.4s, v11.4s, v18.s[0]\n"
        "fmla v3.4s, v10.4s, v19.s[0]\n"

        "fmul v4.4s, v10.4s, v20.s[0]\n"
        "fmls v4.4s, v11.4s, v21.s[0]\n"
        "fmul v5.4s, v11.4s, v20.s[0]\n"
        "fmla v5.4s, v10.4s, v21.s[0]\n"

        "fmla v2.4s, v12.4s, v18.s[1]\n"
        "fmls v2.4s, v13.4s, v19.s[1]\n"
        "fmla v3.4s, v13.4s, v18.s[1]\n"
        "fmla v3.4s, v12.4s, v19.s[1]\n"

        "fmla v4.4s, v12.4s, v20.s[1]\n"
        "fmls v4.4s, v13.4s, v21.s[1]\n"
        "fmla v5.4s, v13.4s, v20.s[1]\n"
        "fmla v5.4s, v12.4s, v21.s[1]\n"

        "fmla v2.4s, v14.4s, v18.s[2]\n"
        "fmls v2.4s, v15.4s, v19.s[2]\n"
        "fmla v3.4s, v15.4s, v18.s[2]\n"
        "fmla v3.4s, v14.4s, v19.s[2]\n"

        "fmla v4.4s, v14.4s, v20.s[2]\n"
        "fmls v4.4s, v15.4s, v21.s[2]\n"
        "fmla v5.4s, v15.4s, v20.s[2]\n"
        "fmla v5.4s, v14.4s, v21.s[2]\n"

        "fmla v2.4s, v16.4s, v18.s[3]\n"
        "fmls v2.4s, v17.4s, v19.s[3]\n"
        "fmla v3.4s, v17.4s, v18.s[3]\n"
        "fmla v3.4s, v16.4s, v19.s[3]\n"

        "fmla v4.4s, v16.4s, v20.s[3]\n"
        "fmls v4.4s, v17.4s, v21.s[3]\n"
        "fmla v5.4s, v17.4s, v20.s[3]\n"
        "fmla v5.4s, v16.4s, v21.s[3]\n"

        "st2  {v2.4s, v3.4s}, [%x[outPtr]], x6\n"
        "st2  {v4.4s, v5.4s}, [%x[outPtr]]\n"

        : [APtr] "+r"(a_ptr), [BPtr] "+r"(b_ptr), [outPtr] "+r"(out_ptr)

        : [AStride] "r"(a_stride), [BDstStride] "r"(b_dst_stride)

        : "v10", "v11", "v12", "v13", "v14", "v15", "v16", "v17", "v18", "v19",
          "v20", "v21", "v22", "v23", "v2", "v3", "v4", "v5", "x5", "x6", "cc",
          "memory");
  }
#endif

  return ARMRAL_SUCCESS;
}

// Computes c += a b or c = a b depending on whether the value of accumulate is
// true or false. a, b and c are sub-matrices of p_src_a, p_src_b and p_dst,
// respectively, where a has size 4-by-4 and  b and c have size 4-by-8.
// a_stride is the total number of columns of matrix p_src_a and b_dst_stride
// is the total number of columns of matrices p_dst_b/p_dst. This function
// expects row-major input and gives row-major output.
template<bool accumulate>
inline armral_status
cmplx_matmul_4x4_f32_unroll(uint16_t a_stride, uint16_t b_dst_stride,
                            const armral_cmplx_f32_t *__restrict p_src_a,
                            const armral_cmplx_f32_t *__restrict p_src_b,
                            armral_cmplx_f32_t *__restrict p_dst) {
#if ARMRAL_ARCH_SVE >= 2
  cmplx_matmul_4x4_f32<accumulate>(a_stride, b_dst_stride, p_src_a, p_src_b,
                                   p_dst);
  cmplx_matmul_4x4_f32<accumulate>(a_stride, b_dst_stride, p_src_a, &p_src_b[4],
                                   &p_dst[4]);
#else
  const float32_t *a_ptr = (const float32_t *)p_src_a;
  const float32_t *b_ptr = (const float32_t *)p_src_b;
  float32_t *out_ptr = (float32_t *)p_dst;

  if constexpr (accumulate) {
    // Accumulate result into p_dst array
    __asm__ __volatile__(
        "lsl  x5, %x[AStride], #3\n"
        "lsl  x6, %x[BDstStride], #3\n"
        "add  x7, %x[BPtr], #32\n"
        "mov  x8, %x[outPtr]\n"

        "ld2  {v0.4s, v1.4s}, [%x[APtr]], x5\n"
        "ld2  {v8.4s, v9.4s}, [%x[BPtr]], x6\n"
        "ld2  {v16.4s, v17.4s}, [%x[outPtr]], x6\n"
        "ld2  {v18.4s, v19.4s}, [%x[outPtr]], x6\n"

        "fmla v16.4s, v8.4s, v0.s[0]\n"
        "fmls v16.4s, v9.4s, v1.s[0]\n"
        "fmla v17.4s, v9.4s, v0.s[0]\n"
        "fmla v17.4s, v8.4s, v1.s[0]\n"

        "ld2  {v2.4s, v3.4s}, [%x[APtr]], x5\n"
        "ld2  {v4.4s, v5.4s}, [%x[APtr]], x5\n"

        "fmla v18.4s, v8.4s, v2.s[0]\n"
        "fmls v18.4s, v9.4s, v3.s[0]\n"
        "fmla v19.4s, v9.4s, v2.s[0]\n"
        "fmla v19.4s, v8.4s, v3.s[0]\n"

        "ld2  {v20.4s, v21.4s}, [%x[outPtr]], x6\n"
        "ld2  {v22.4s, v23.4s}, [%x[outPtr]]\n"

        "mov %x[outPtr], x8\n"

        "fmla v20.4s, v8.4s, v4.s[0]\n"
        "fmls v20.4s, v9.4s, v5.s[0]\n"
        "fmla v21.4s, v9.4s, v4.s[0]\n"
        "fmla v21.4s, v8.4s, v5.s[0]\n"

        "ld2  {v6.4s, v7.4s}, [%x[APtr]], x5\n"
        "ld2  {v10.4s, v11.4s}, [%x[BPtr]], x6\n"

        "fmla v22.4s, v8.4s, v6.s[0]\n"
        "fmls v22.4s, v9.4s, v7.s[0]\n"
        "fmla v23.4s, v9.4s, v6.s[0]\n"
        "fmla v23.4s, v8.4s, v7.s[0]\n"

        "ld2  {v8.4s, v9.4s}, [x7], x6\n"
        "ld2  {v12.4s, v13.4s}, [%x[BPtr]], x6\n"

        "fmla v16.4s, v10.4s, v0.s[1]\n"
        "fmls v16.4s, v11.4s, v1.s[1]\n"
        "fmla v17.4s, v11.4s, v0.s[1]\n"
        "fmla v17.4s, v10.4s, v1.s[1]\n"

        "fmla v18.4s, v10.4s, v2.s[1]\n"
        "fmls v18.4s, v11.4s, v3.s[1]\n"
        "fmla v19.4s, v11.4s, v2.s[1]\n"
        "fmla v19.4s, v10.4s, v3.s[1]\n"

        "fmla v20.4s, v10.4s, v4.s[1]\n"
        "fmls v20.4s, v11.4s, v5.s[1]\n"
        "fmla v21.4s, v11.4s, v4.s[1]\n"
        "fmla v21.4s, v10.4s, v5.s[1]\n"

        "fmla v22.4s, v10.4s, v6.s[1]\n"
        "fmls v22.4s, v11.4s, v7.s[1]\n"
        "fmla v23.4s, v11.4s, v6.s[1]\n"
        "fmla v23.4s, v10.4s, v7.s[1]\n"

        "ld2  {v10.4s, v11.4s}, [x7], x6\n"
        "ld2  {v14.4s, v15.4s}, [%x[BPtr]]\n"

        "fmla v16.4s, v12.4s, v0.s[2]\n"
        "fmls v16.4s, v13.4s, v1.s[2]\n"
        "fmla v17.4s, v13.4s, v0.s[2]\n"
        "fmla v17.4s, v12.4s, v1.s[2]\n"

        "fmla v18.4s, v12.4s, v2.s[2]\n"
        "fmls v18.4s, v13.4s, v3.s[2]\n"
        "fmla v19.4s, v13.4s, v2.s[2]\n"
        "fmla v19.4s, v12.4s, v3.s[2]\n"

        "fmla v20.4s, v12.4s, v4.s[2]\n"
        "fmls v20.4s, v13.4s, v5.s[2]\n"
        "fmla v21.4s, v13.4s, v4.s[2]\n"
        "fmla v21.4s, v12.4s, v5.s[2]\n"

        "fmla v22.4s, v12.4s, v6.s[2]\n"
        "fmls v22.4s, v13.4s, v7.s[2]\n"
        "fmla v23.4s, v13.4s, v6.s[2]\n"
        "fmla v23.4s, v12.4s, v7.s[2]\n"

        "fmla v16.4s, v14.4s, v0.s[3]\n"
        "fmls v16.4s, v15.4s, v1.s[3]\n"
        "fmla v17.4s, v15.4s, v0.s[3]\n"
        "fmla v17.4s, v14.4s, v1.s[3]\n"

        "ld2  {v12.4s, v13.4s}, [x7], x6\n"
        "st2  {v16.4s, v17.4s}, [%x[outPtr]], x6\n"

        "fmla v18.4s, v14.4s, v2.s[3]\n"
        "fmls v18.4s, v15.4s, v3.s[3]\n"
        "fmla v19.4s, v15.4s, v2.s[3]\n"
        "fmla v19.4s, v14.4s, v3.s[3]\n"

        "fmla v20.4s, v14.4s, v4.s[3]\n"
        "fmls v20.4s, v15.4s, v5.s[3]\n"
        "fmla v21.4s, v15.4s, v4.s[3]\n"
        "fmla v21.4s, v14.4s, v5.s[3]\n"

        "st2  {v18.4s, v19.4s}, [%x[outPtr]], x6\n"
        "st2  {v20.4s, v21.4s}, [%x[outPtr]], x6\n"

        "fmla v22.4s, v14.4s, v6.s[3]\n"
        "fmls v22.4s, v15.4s, v7.s[3]\n"
        "fmla v23.4s, v15.4s, v6.s[3]\n"
        "fmla v23.4s, v14.4s, v7.s[3]\n"

        "st2  {v22.4s, v23.4s}, [%x[outPtr]]\n"
        "ld2  {v14.4s, v15.4s}, [x7]\n"

        "add %x[outPtr], x8, #32\n"

        "ld2  {v16.4s, v17.4s}, [%x[outPtr]], x6\n"
        "ld2  {v18.4s, v19.4s}, [%x[outPtr]], x6\n"

        "fmla v16.4s, v8.4s, v0.s[0]\n"
        "fmls v16.4s, v9.4s, v1.s[0]\n"
        "fmla v17.4s, v9.4s, v0.s[0]\n"
        "fmla v17.4s, v8.4s, v1.s[0]\n"

        "fmla v18.4s, v8.4s, v2.s[0]\n"
        "fmls v18.4s, v9.4s, v3.s[0]\n"
        "fmla v19.4s, v9.4s, v2.s[0]\n"
        "fmla v19.4s, v8.4s, v3.s[0]\n"

        "ld2  {v20.4s, v21.4s}, [%x[outPtr]], x6\n"
        "ld2  {v22.4s, v23.4s}, [%x[outPtr]]\n"
        "add %x[outPtr], x8, #32\n"

        "fmla v20.4s, v8.4s, v4.s[0]\n"
        "fmls v20.4s, v9.4s, v5.s[0]\n"
        "fmla v21.4s, v9.4s, v4.s[0]\n"
        "fmla v21.4s, v8.4s, v5.s[0]\n"

        "fmla v22.4s, v8.4s, v6.s[0]\n"
        "fmls v22.4s, v9.4s, v7.s[0]\n"
        "fmla v23.4s, v9.4s, v6.s[0]\n"
        "fmla v23.4s, v8.4s, v7.s[0]\n"

        "fmla v16.4s, v10.4s, v0.s[1]\n"
        "fmls v16.4s, v11.4s, v1.s[1]\n"
        "fmla v17.4s, v11.4s, v0.s[1]\n"
        "fmla v17.4s, v10.4s, v1.s[1]\n"

        "fmla v18.4s, v10.4s, v2.s[1]\n"
        "fmls v18.4s, v11.4s, v3.s[1]\n"
        "fmla v19.4s, v11.4s, v2.s[1]\n"
        "fmla v19.4s, v10.4s, v3.s[1]\n"

        "fmla v20.4s, v10.4s, v4.s[1]\n"
        "fmls v20.4s, v11.4s, v5.s[1]\n"
        "fmla v21.4s, v11.4s, v4.s[1]\n"
        "fmla v21.4s, v10.4s, v5.s[1]\n"

        "fmla v22.4s, v10.4s, v6.s[1]\n"
        "fmls v22.4s, v11.4s, v7.s[1]\n"
        "fmla v23.4s, v11.4s, v6.s[1]\n"
        "fmla v23.4s, v10.4s, v7.s[1]\n"

        "fmla v16.4s, v12.4s, v0.s[2]\n"
        "fmls v16.4s, v13.4s, v1.s[2]\n"
        "fmla v17.4s, v13.4s, v0.s[2]\n"
        "fmla v17.4s, v12.4s, v1.s[2]\n"

        "fmla v18.4s, v12.4s, v2.s[2]\n"
        "fmls v18.4s, v13.4s, v3.s[2]\n"
        "fmla v19.4s, v13.4s, v2.s[2]\n"
        "fmla v19.4s, v12.4s, v3.s[2]\n"

        "fmla v20.4s, v12.4s, v4.s[2]\n"
        "fmls v20.4s, v13.4s, v5.s[2]\n"
        "fmla v21.4s, v13.4s, v4.s[2]\n"
        "fmla v21.4s, v12.4s, v5.s[2]\n"

        "fmla v22.4s, v12.4s, v6.s[2]\n"
        "fmls v22.4s, v13.4s, v7.s[2]\n"
        "fmla v23.4s, v13.4s, v6.s[2]\n"
        "fmla v23.4s, v12.4s, v7.s[2]\n"

        "fmla v16.4s, v14.4s, v0.s[3]\n"
        "fmls v16.4s, v15.4s, v1.s[3]\n"
        "fmla v17.4s, v15.4s, v0.s[3]\n"
        "fmla v17.4s, v14.4s, v1.s[3]\n"

        "fmla v18.4s, v14.4s, v2.s[3]\n"
        "fmls v18.4s, v15.4s, v3.s[3]\n"
        "fmla v19.4s, v15.4s, v2.s[3]\n"
        "fmla v19.4s, v14.4s, v3.s[3]\n"

        "st2  {v16.4s, v17.4s}, [%x[outPtr]], x6\n"
        "st2  {v18.4s, v19.4s}, [%x[outPtr]], x6\n"

        "fmla v20.4s, v14.4s, v4.s[3]\n"
        "fmls v20.4s, v15.4s, v5.s[3]\n"
        "fmla v21.4s, v15.4s, v4.s[3]\n"
        "fmla v21.4s, v14.4s, v5.s[3]\n"

        "fmla v22.4s, v14.4s, v6.s[3]\n"
        "fmls v22.4s, v15.4s, v7.s[3]\n"
        "fmla v23.4s, v15.4s, v6.s[3]\n"
        "fmla v23.4s, v14.4s, v7.s[3]\n"

        "st2  {v20.4s, v21.4s}, [%x[outPtr]], x6\n"
        "st2  {v22.4s, v23.4s}, [%x[outPtr]]\n"

        : [APtr] "+r"(a_ptr), [BPtr] "+r"(b_ptr), [outPtr] "+r"(out_ptr)

        : [AStride] "r"(a_stride), [BDstStride] "r"(b_dst_stride)

        : "v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "v10",
          "v11", "v12", "v13", "v14", "v15", "v16", "v17", "v18", "v19", "v20",
          "v21", "v22", "v23", "x5", "x6", "x7", "x8", "cc", "memory");
  } else {
    __asm__ __volatile__(
        "lsl  x5, %x[AStride], #3\n"
        "lsl  x6, %x[BDstStride], #3\n"
        "add x7, %x[BPtr], #32\n"
        "add x8, %x[outPtr], #32\n"

        "ld2  {v0.4s, v1.4s}, [%x[APtr]], x5\n"
        "ld2  {v2.4s, v3.4s}, [%x[APtr]], x5\n"
        "ld2  {v8.4s, v9.4s}, [%x[BPtr]], x6\n"
        "ld2  {v10.4s, v11.4s}, [%x[BPtr]], x6\n"

        "fmul v16.4s, v8.4s, v0.s[0]\n"
        "fmls v16.4s, v9.4s, v1.s[0]\n"
        "fmul v17.4s, v9.4s, v0.s[0]\n"
        "fmla v17.4s, v8.4s, v1.s[0]\n"

        "fmul v18.4s, v8.4s, v2.s[0]\n"
        "fmls v18.4s, v9.4s, v3.s[0]\n"
        "fmul v19.4s, v9.4s, v2.s[0]\n"
        "fmla v19.4s, v8.4s, v3.s[0]\n"

        "ld2  {v4.4s, v5.4s}, [%x[APtr]], x5\n"
        "ld2  {v6.4s, v7.4s}, [%x[APtr]], x5\n"

        "fmul v20.4s, v8.4s, v4.s[0]\n"
        "fmls v20.4s, v9.4s, v5.s[0]\n"
        "fmul v21.4s, v9.4s, v4.s[0]\n"
        "fmla v21.4s, v8.4s, v5.s[0]\n"

        "fmul v22.4s, v8.4s, v6.s[0]\n"
        "fmls v22.4s, v9.4s, v7.s[0]\n"
        "fmul v23.4s, v9.4s, v6.s[0]\n"
        "fmla v23.4s, v8.4s, v7.s[0]\n"

        "ld2  {v8.4s, v9.4s}, [x7], x6\n"
        "ld2  {v12.4s, v13.4s}, [%x[BPtr]], x6\n"

        "fmla v16.4s, v10.4s, v0.s[1]\n"
        "fmls v16.4s, v11.4s, v1.s[1]\n"
        "fmla v17.4s, v11.4s, v0.s[1]\n"
        "fmla v17.4s, v10.4s, v1.s[1]\n"

        "fmla v18.4s, v10.4s, v2.s[1]\n"
        "fmls v18.4s, v11.4s, v3.s[1]\n"
        "fmla v19.4s, v11.4s, v2.s[1]\n"
        "fmla v19.4s, v10.4s, v3.s[1]\n"

        "fmla v20.4s, v10.4s, v4.s[1]\n"
        "fmls v20.4s, v11.4s, v5.s[1]\n"
        "fmla v21.4s, v11.4s, v4.s[1]\n"
        "fmla v21.4s, v10.4s, v5.s[1]\n"

        "fmla v22.4s, v10.4s, v6.s[1]\n"
        "fmls v22.4s, v11.4s, v7.s[1]\n"
        "fmla v23.4s, v11.4s, v6.s[1]\n"
        "fmla v23.4s, v10.4s, v7.s[1]\n"

        "ld2  {v14.4s, v15.4s}, [%x[BPtr]]\n"
        "ld2  {v10.4s, v11.4s}, [x7], x6\n"

        "fmla v16.4s, v12.4s, v0.s[2]\n"
        "fmls v16.4s, v13.4s, v1.s[2]\n"
        "fmla v17.4s, v13.4s, v0.s[2]\n"
        "fmla v17.4s, v12.4s, v1.s[2]\n"

        "fmla v18.4s, v12.4s, v2.s[2]\n"
        "fmls v18.4s, v13.4s, v3.s[2]\n"
        "fmla v19.4s, v13.4s, v2.s[2]\n"
        "fmla v19.4s, v12.4s, v3.s[2]\n"

        "fmla v20.4s, v12.4s, v4.s[2]\n"
        "fmls v20.4s, v13.4s, v5.s[2]\n"
        "fmla v21.4s, v13.4s, v4.s[2]\n"
        "fmla v21.4s, v12.4s, v5.s[2]\n"

        "fmla v22.4s, v12.4s, v6.s[2]\n"
        "fmls v22.4s, v13.4s, v7.s[2]\n"
        "fmla v23.4s, v13.4s, v6.s[2]\n"
        "fmla v23.4s, v12.4s, v7.s[2]\n"

        "fmla v16.4s, v14.4s, v0.s[3]\n"
        "fmls v16.4s, v15.4s, v1.s[3]\n"
        "fmla v17.4s, v15.4s, v0.s[3]\n"
        "fmla v17.4s, v14.4s, v1.s[3]\n"

        "fmla v18.4s, v14.4s, v2.s[3]\n"
        "fmls v18.4s, v15.4s, v3.s[3]\n"
        "fmla v19.4s, v15.4s, v2.s[3]\n"
        "fmla v19.4s, v14.4s, v3.s[3]\n"

        "ld2  {v12.4s, v13.4s}, [x7], x6\n"
        "st2  {v16.4s, v17.4s}, [%x[outPtr]], x6\n"

        "fmla v20.4s, v14.4s, v4.s[3]\n"
        "fmls v20.4s, v15.4s, v5.s[3]\n"
        "fmla v21.4s, v15.4s, v4.s[3]\n"
        "fmla v21.4s, v14.4s, v5.s[3]\n"

        "fmla v22.4s, v14.4s, v6.s[3]\n"
        "fmls v22.4s, v15.4s, v7.s[3]\n"
        "fmla v23.4s, v15.4s, v6.s[3]\n"
        "fmla v23.4s, v14.4s, v7.s[3]\n"

        "st2  {v18.4s, v19.4s}, [%x[outPtr]], x6\n"
        "st2  {v20.4s, v21.4s}, [%x[outPtr]], x6\n"

        "fmul v16.4s, v8.4s, v0.s[0]\n"
        "fmls v16.4s, v9.4s, v1.s[0]\n"
        "fmul v17.4s, v9.4s, v0.s[0]\n"
        "fmla v17.4s, v8.4s, v1.s[0]\n"

        "st2  {v22.4s, v23.4s}, [%x[outPtr]]\n"
        "ld2  {v14.4s, v15.4s}, [x7], x6\n"

        "mov %x[outPtr], x8\n"

        "fmul v18.4s, v8.4s, v2.s[0]\n"
        "fmls v18.4s, v9.4s, v3.s[0]\n"
        "fmul v19.4s, v9.4s, v2.s[0]\n"
        "fmla v19.4s, v8.4s, v3.s[0]\n"

        "fmul v20.4s, v8.4s, v4.s[0]\n"
        "fmls v20.4s, v9.4s, v5.s[0]\n"
        "fmul v21.4s, v9.4s, v4.s[0]\n"
        "fmla v21.4s, v8.4s, v5.s[0]\n"

        "fmul v22.4s, v8.4s, v6.s[0]\n"
        "fmls v22.4s, v9.4s, v7.s[0]\n"
        "fmul v23.4s, v9.4s, v6.s[0]\n"
        "fmla v23.4s, v8.4s, v7.s[0]\n"

        "fmla v16.4s, v10.4s, v0.s[1]\n"
        "fmls v16.4s, v11.4s, v1.s[1]\n"
        "fmla v17.4s, v11.4s, v0.s[1]\n"
        "fmla v17.4s, v10.4s, v1.s[1]\n"

        "fmla v18.4s, v10.4s, v2.s[1]\n"
        "fmls v18.4s, v11.4s, v3.s[1]\n"
        "fmla v19.4s, v11.4s, v2.s[1]\n"
        "fmla v19.4s, v10.4s, v3.s[1]\n"

        "fmla v20.4s, v10.4s, v4.s[1]\n"
        "fmls v20.4s, v11.4s, v5.s[1]\n"
        "fmla v21.4s, v11.4s, v4.s[1]\n"
        "fmla v21.4s, v10.4s, v5.s[1]\n"

        "fmla v22.4s, v10.4s, v6.s[1]\n"
        "fmls v22.4s, v11.4s, v7.s[1]\n"
        "fmla v23.4s, v11.4s, v6.s[1]\n"
        "fmla v23.4s, v10.4s, v7.s[1]\n"

        "fmla v16.4s, v12.4s, v0.s[2]\n"
        "fmls v16.4s, v13.4s, v1.s[2]\n"
        "fmla v17.4s, v13.4s, v0.s[2]\n"
        "fmla v17.4s, v12.4s, v1.s[2]\n"

        "fmla v18.4s, v12.4s, v2.s[2]\n"
        "fmls v18.4s, v13.4s, v3.s[2]\n"
        "fmla v19.4s, v13.4s, v2.s[2]\n"
        "fmla v19.4s, v12.4s, v3.s[2]\n"

        "fmla v20.4s, v12.4s, v4.s[2]\n"
        "fmls v20.4s, v13.4s, v5.s[2]\n"
        "fmla v21.4s, v13.4s, v4.s[2]\n"
        "fmla v21.4s, v12.4s, v5.s[2]\n"

        "fmla v22.4s, v12.4s, v6.s[2]\n"
        "fmls v22.4s, v13.4s, v7.s[2]\n"
        "fmla v23.4s, v13.4s, v6.s[2]\n"
        "fmla v23.4s, v12.4s, v7.s[2]\n"

        "fmla v16.4s, v14.4s, v0.s[3]\n"
        "fmls v16.4s, v15.4s, v1.s[3]\n"
        "fmla v17.4s, v15.4s, v0.s[3]\n"
        "fmla v17.4s, v14.4s, v1.s[3]\n"

        "fmla v18.4s, v14.4s, v2.s[3]\n"
        "fmls v18.4s, v15.4s, v3.s[3]\n"
        "fmla v19.4s, v15.4s, v2.s[3]\n"
        "fmla v19.4s, v14.4s, v3.s[3]\n"

        "st2  {v16.4s, v17.4s}, [%x[outPtr]], x6\n"
        "st2  {v18.4s, v19.4s}, [%x[outPtr]], x6\n"

        "fmla v20.4s, v14.4s, v4.s[3]\n"
        "fmls v20.4s, v15.4s, v5.s[3]\n"
        "fmla v21.4s, v15.4s, v4.s[3]\n"
        "fmla v21.4s, v14.4s, v5.s[3]\n"

        "fmla v22.4s, v14.4s, v6.s[3]\n"
        "fmls v22.4s, v15.4s, v7.s[3]\n"
        "fmla v23.4s, v15.4s, v6.s[3]\n"
        "fmla v23.4s, v14.4s, v7.s[3]\n"

        "st2  {v20.4s, v21.4s}, [%x[outPtr]], x6\n"
        "st2  {v22.4s, v23.4s}, [%x[outPtr]]\n"

        : [APtr] "+r"(a_ptr), [BPtr] "+r"(b_ptr), [outPtr] "+r"(out_ptr)

        : [AStride] "r"(a_stride), [BDstStride] "r"(b_dst_stride)

        : "v0", "v1", "v2", "v3", "v4", "v5", "v6", "v7", "v8", "v9", "v10",
          "v11", "v12", "v13", "v14", "v15", "v16", "v17", "v18", "v19", "v20",
          "v21", "v22", "v23", "x5", "x6", "x7", "x8", "cc", "memory");
  }
#endif

  return ARMRAL_SUCCESS;
}

inline void use_general_matmul(uint16_t msize, uint16_t nsize, uint16_t ksize,
                               uint16_t n, uint16_t k, uint16_t kbi,
                               uint16_t ki,
                               const armral_cmplx_f32_t *__restrict p_src_a,
                               const armral_cmplx_f32_t *__restrict p_src_b,
                               armral_cmplx_f32_t *__restrict p_dst) {
  if (kbi == 0 && ki == 0) {
    // This is the first pass, don't accumulate into dst array
    cmplx_matmul_f32<false>(msize, nsize, ksize, k, n, p_src_a, p_src_b, p_dst);
  } else {
    // Accumulate into dst array
    cmplx_matmul_f32<true>(msize, nsize, ksize, k, n, p_src_a, p_src_b, p_dst);
  }
}

inline void matmul_inner(uint16_t m, uint16_t n, uint16_t k, uint16_t kbi,
                         uint16_t kb_size, uint16_t nb_size,
                         uint16_t rem_k_inner, uint16_t rem_n_inner,
                         uint16_t rem_m_inner,
                         const armral_cmplx_f32_t *__restrict p_src_a,
                         const armral_cmplx_f32_t *__restrict p_src_b,
                         armral_cmplx_f32_t *__restrict p_dst) {
  // For every 4 rows of A/C
  for (int32_t mi = 0; mi < (m - 3); mi += 4) {
    uint32_t a_idx = mi * k;
    uint32_t dst_idx = mi * n;
    for (int32_t ki = 0; ki < (kb_size - 3); ki += 4) {
      // Unrolling 4x4 inner matmul means we do 8 cols of B/C at a time
      int32_t ni = 0;
      for (; ni < (nb_size - 7); ni += 8) {
        if (kbi == 0 && ki == 0) {
          // This is the first pass, don't accumulate into dst array
          cmplx_matmul_4x4_f32_unroll<false>(
              k, n, &p_src_a[a_idx], &p_src_b[ni], &p_dst[dst_idx + ni]);
        } else {
          // Accumulate into dst array
          cmplx_matmul_4x4_f32_unroll<true>(k, n, &p_src_a[a_idx + ki],
                                            &p_src_b[ki * n + ni],
                                            &p_dst[dst_idx + ni]);
        }
      }
      // If there are at least 4 cols of B/C remaining, use the 4x4 inner kernel
      for (; ni < (nb_size - 3); ni += 4) {
        if (kbi == 0 && ki == 0) {
          // This is the first pass, don't accumulate into dst array
          cmplx_matmul_4x4_f32<false>(k, n, &p_src_a[a_idx], &p_src_b[ni],
                                      &p_dst[dst_idx + ni]);
        } else {
          // Accumulate into dst array
          cmplx_matmul_4x4_f32<true>(k, n, &p_src_a[a_idx + ki],
                                     &p_src_b[ki * n + ni],
                                     &p_dst[dst_idx + ni]);
        }
      }
      // Do leftover ns
      if (rem_n_inner != 0U) {
        ni = nb_size - rem_n_inner;
        use_general_matmul(4, rem_n_inner, 4, n, k, kbi, ki,
                           &p_src_a[a_idx + ki], &p_src_b[ki * n + ni],
                           &p_dst[dst_idx + ni]);
      }
    }
    // Do leftover ks
    if (rem_k_inner != 0U) {
      uint16_t ki = kb_size - rem_k_inner;
      use_general_matmul(4, nb_size, rem_k_inner, n, k, kbi, ki,
                         &p_src_a[a_idx + ki], &p_src_b[ki * n],
                         &p_dst[dst_idx]);
    }
  }
  // Do leftover ms
  if (rem_m_inner != 0U) {
    uint16_t mi = m - rem_m_inner;
    use_general_matmul(rem_m_inner, nb_size, kb_size, n, k, kbi, 0,
                       &p_src_a[mi * k], p_src_b, &p_dst[mi * n]);
  }
}

inline void matmul_n_block(uint16_t m, uint16_t n, uint16_t k, uint16_t kb_size,
                           uint16_t kbi, uint16_t nb, uint16_t rem_n,
                           uint16_t n_idx, uint16_t rem_k_inner,
                           uint16_t rem_m_inner,
                           const armral_cmplx_f32_t *__restrict p_src_a,
                           const armral_cmplx_f32_t *__restrict p_src_b,
                           armral_cmplx_f32_t *__restrict p_dst) {
  uint16_t rem_n_inner = nb % 4;
  for (int32_t nbi = 0; nbi < n - (nb - 1); nbi += nb) {
    matmul_inner(m, n, k, kbi, kb_size, nb, rem_k_inner, rem_n_inner,
                 rem_m_inner, &p_src_a[kbi], &p_src_b[kbi * n + nbi],
                 &p_dst[nbi]);
  }
  if (rem_n != 0U) {
    rem_n_inner = rem_n % 4;
    matmul_inner(m, n, k, kbi, kb_size, rem_n, rem_k_inner, rem_n_inner,
                 rem_m_inner, &p_src_a[kbi], &p_src_b[kbi * n + n_idx],
                 &p_dst[n_idx]);
  }
}

} // anonymous namespace

armral_status
armral_cmplx_mat_mult_2x2_f32(const armral_cmplx_f32_t *__restrict p_src_a,
                              const armral_cmplx_f32_t *__restrict p_src_b,
                              armral_cmplx_f32_t *p_dst) {
  const float32_t *a_ptr = (const float32_t *)p_src_a;
  const float32_t *b_ptr = (const float32_t *)p_src_b;
  float32_t *out_ptr = (float32_t *)p_dst;

#ifdef ARMRAL_ARCH_SVE
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svfloat32_t a0 = svld1_f32(p4, &a_ptr[0]);
  svfloat32_t a1 = svld1_f32(p4, &a_ptr[4]);
  svfloat32_t b0 = svld1_f32(p4, &b_ptr[0]);
  svfloat32_t b1 = svld1_f32(p4, &b_ptr[4]);
  svfloat32_t c0 = svdup_n_f32(0);
  svfloat32_t c1 = svdup_n_f32(0);

  c0 = svcmla_lane_f32(c0, a0, b0, 0, 0);
  c0 = svcmla_lane_f32(c0, a0, b0, 0, 90);
  c0 = svcmla_lane_f32(c0, a1, b0, 1, 0);
  c0 = svcmla_lane_f32(c0, a1, b0, 1, 90);
  c1 = svcmla_lane_f32(c1, a0, b1, 0, 0);
  c1 = svcmla_lane_f32(c1, a0, b1, 0, 90);
  c1 = svcmla_lane_f32(c1, a1, b1, 1, 0);
  c1 = svcmla_lane_f32(c1, a1, b1, 1, 90);

  svst1_f32(p4, &out_ptr[0], c0);
  svst1_f32(p4, &out_ptr[4], c1);
#else
  float32x2x2_t a_col[2];
  float32x2x2_t b[2];
  float32x2x2_t result[2];

  a_col[0] = vld2_f32(a_ptr);
  a_ptr = a_ptr + 4;

  b[0] = vld2_f32(b_ptr);
  b_ptr = b_ptr + 4;

  // result[0] 4 rows elem 1 RE * first column elem 1 RE
  result[0].val[0] = vmul_lane_f32(a_col[0].val[0], b[0].val[0], 0);
  // result[0] 4 rows elem 1 IM * first column elem 1 IM
  result[0].val[0] =
      vfms_lane_f32(result[0].val[0], a_col[0].val[1], b[0].val[1], 0);
  b[1] = vld2_f32(b_ptr);
  b_ptr = b_ptr + 4;
  // result[1] 4 rows elem 1 IM * first row elem 1 RE
  result[0].val[1] = vmul_lane_f32(a_col[0].val[1], b[0].val[0], 0);
  // result[1] 4 rows elem 1 RE * first row elem 1 IM
  result[0].val[1] =
      vfma_lane_f32(result[0].val[1], a_col[0].val[0], b[0].val[1], 0);
  a_col[1] = vld2_f32(a_ptr);
  a_ptr = a_ptr + 4;

  // result[1].val[0] 4 rows elem 1 RE * second row elem 1 RE
  result[1].val[0] = vmul_lane_f32(a_col[0].val[0], b[1].val[0], 0);
  // result[1].val[0] 4 rows elem 1 IM * second row elem 1 IM
  result[1].val[0] =
      vfms_lane_f32(result[1].val[0], a_col[0].val[1], b[1].val[1], 0);
  result[1].val[1] = vmul_lane_f32(a_col[0].val[1], b[1].val[0], 0);
  result[1].val[1] =
      vfma_lane_f32(result[1].val[1], a_col[0].val[0], b[1].val[1], 0);

  // result[0] 4 rows elem 2 RE * first row elem 2 RE
  result[0].val[0] =
      vfma_lane_f32(result[0].val[0], a_col[1].val[0], b[0].val[0], 1);
  // result[0] 4 rows elem 2 IM * first row elem 2 IM
  result[0].val[0] =
      vfms_lane_f32(result[0].val[0], a_col[1].val[1], b[0].val[1], 1);
  // result[1] 4 rows elem 2 IM * first row elem 2 RE
  result[0].val[1] =
      vfma_lane_f32(result[0].val[1], a_col[1].val[1], b[0].val[0], 1);
  // result[1] 4 rows elem 2 RE * first row elem 2 IM
  result[0].val[1] =
      vfma_lane_f32(result[0].val[1], a_col[1].val[0], b[0].val[1], 1);

  // result[0] 4 rows elem 2 RE * second row elem 2 RE
  result[1].val[0] =
      vfma_lane_f32(result[1].val[0], a_col[1].val[0], b[1].val[0], 1);
  // result[0] 4 rows elem 2 IM * second row elem 2 IM
  result[1].val[0] =
      vfms_lane_f32(result[1].val[0], a_col[1].val[1], b[1].val[1], 1);
  // result[1] 4 rows elem 2 IM * second row elem 2 RE
  result[1].val[1] =
      vfma_lane_f32(result[1].val[1], a_col[1].val[1], b[1].val[0], 1);
  // result[1] 4 rows elem 2 RE * second row elem 2 IM
  result[1].val[1] =
      vfma_lane_f32(result[1].val[1], a_col[1].val[0], b[1].val[1], 1);

  vst2_f32(out_ptr, result[0]);
  out_ptr = out_ptr + 4;

  vst2_f32(out_ptr, result[1]);
  out_ptr = out_ptr + 4;
#endif

  return ARMRAL_SUCCESS;
}

armral_status armral_cmplx_mat_mult_2x2_f32_iq(
    const float32_t *__restrict src_a_re, const float32_t *__restrict src_a_im,
    const float32_t *__restrict src_b_re, const float32_t *__restrict src_b_im,
    float32_t *dst_re, float32_t *dst_im) {

#ifdef ARMRAL_ARCH_SVE
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svfloat32_t a_re = svld1_f32(p4, src_a_re);
  svfloat32_t a_im = svld1_f32(p4, src_a_im);
  svfloat32_t b_re = svld1_f32(p4, src_b_re);
  svfloat32_t b_im = svld1_f32(p4, src_b_im);

  svfloat32_t c_re;
  svfloat32_t c_im;

  svfloat32_t tmp_a_re = svtrn2iq_f32(a_re);
  svfloat32_t tmp_a_im = svtrn2iq_f32(a_im);
  svfloat32_t tmp_b_re = svtrn2(b_re, b_re);
  svfloat32_t tmp_b_im = svtrn2(b_im, b_im);

  c_re = svmul_f32_x(p4, tmp_a_re, tmp_b_re);
  c_re = svmls_f32_x(p4, c_re, tmp_a_im, tmp_b_im);
  c_re = svcmla_lane_f32(c_re, b_re, a_re, 0, 0);
  c_re = svcmla_lane_f32(c_re, b_im, a_im, 0, 180);

  c_im = svmul_f32_x(p4, tmp_a_re, tmp_b_im);
  c_im = svmla_f32_x(p4, c_im, tmp_a_im, tmp_b_re);
  c_im = svcmla_lane_f32(c_im, b_im, a_re, 0, 0);
  c_im = svcmla_lane_f32(c_im, b_re, a_im, 0, 0);

  svst1_f32(p4, dst_re, c_re);
  svst1_f32(p4, dst_im, c_im);
#else

  float32x2_t a_col0_re = vld1_f32(&src_a_re[0]);
  float32x2_t a_col0_im = vld1_f32(&src_a_im[0]);
  float32x2_t a_col1_re = vld1_f32(&src_a_re[2]);
  float32x2_t a_col1_im = vld1_f32(&src_a_im[2]);
  float32x2_t b0_re = vld1_f32(&src_b_re[0]);
  float32x2_t b0_im = vld1_f32(&src_b_im[0]);
  float32x2_t b1_re = vld1_f32(&src_b_re[2]);
  float32x2_t b1_im = vld1_f32(&src_b_im[2]);

  float32x2x2_t result[2];
  result[0].val[0] = vmul_lane_f32(a_col0_re, b0_re, 0);
  result[0].val[0] = vfms_lane_f32(result[0].val[0], a_col0_im, b0_im, 0);
  result[0].val[1] = vmul_lane_f32(a_col0_im, b0_re, 0);
  result[0].val[1] = vfma_lane_f32(result[0].val[1], a_col0_re, b0_im, 0);

  result[1].val[0] = vmul_lane_f32(a_col0_re, b1_re, 0);
  result[1].val[0] = vfms_lane_f32(result[1].val[0], a_col0_im, b1_im, 0);
  result[1].val[1] = vmul_lane_f32(a_col0_im, b1_re, 0);
  result[1].val[1] = vfma_lane_f32(result[1].val[1], a_col0_re, b1_im, 0);

  result[0].val[0] = vfma_lane_f32(result[0].val[0], a_col1_re, b0_re, 1);
  result[0].val[0] = vfms_lane_f32(result[0].val[0], a_col1_im, b0_im, 1);
  result[0].val[1] = vfma_lane_f32(result[0].val[1], a_col1_im, b0_re, 1);
  result[0].val[1] = vfma_lane_f32(result[0].val[1], a_col1_re, b0_im, 1);

  result[1].val[0] = vfma_lane_f32(result[1].val[0], a_col1_re, b1_re, 1);
  result[1].val[0] = vfms_lane_f32(result[1].val[0], a_col1_im, b1_im, 1);
  result[1].val[1] = vfma_lane_f32(result[1].val[1], a_col1_im, b1_re, 1);
  result[1].val[1] = vfma_lane_f32(result[1].val[1], a_col1_re, b1_im, 1);

  vst1_f32(&dst_re[0], result[0].val[0]);
  vst1_f32(&dst_im[0], result[0].val[1]);
  vst1_f32(&dst_re[2], result[1].val[0]);
  vst1_f32(&dst_im[2], result[1].val[1]);
#endif

  return ARMRAL_SUCCESS;
}

armral_status
armral_cmplx_mat_mult_4x4_f32(const armral_cmplx_f32_t *__restrict p_src_a,
                              const armral_cmplx_f32_t *__restrict p_src_b,
                              armral_cmplx_f32_t *p_dst) {
  // Note: the a/b flip is intentional since the 4x4 function expects
  //       row-major input, making all matrices transposed
  //       i.e. C = A^T * B^T = (B * A)^T
  //       Unfortunately clang-tidy thinks this is an error, because
  //       the function has parameters named p_src_a and p_src_b
  //       so we must disable checking for swapped arguments here
  // NOLINTBEGIN(readability-suspicious-call-argument)
  return cmplx_matmul_4x4_f32<false>(4, 4, p_src_b, p_src_a, p_dst);
  // NOLINTEND(readability-suspicious-call-argument)
}

armral_status armral_cmplx_mat_mult_4x4_f32_iq(
    const float32_t *__restrict src_a_re, const float32_t *__restrict src_a_im,
    const float32_t *__restrict src_b_re, const float32_t *__restrict src_b_im,
    float32_t *dst_re, float32_t *dst_im) {
#ifdef ARMRAL_ARCH_SVE
  svbool_t p4 = svptrue_pat_b32(SV_VL4);

  svfloat32_t a_col0_re = svld1_f32(p4, &src_a_re[0 * 4]);
  svfloat32_t a_col1_re = svld1_f32(p4, &src_a_re[1 * 4]);
  svfloat32_t a_col2_re = svld1_f32(p4, &src_a_re[2 * 4]);
  svfloat32_t a_col3_re = svld1_f32(p4, &src_a_re[3 * 4]);
  svfloat32_t a_col0_im = svld1_f32(p4, &src_a_im[0 * 4]);
  svfloat32_t a_col1_im = svld1_f32(p4, &src_a_im[1 * 4]);
  svfloat32_t a_col2_im = svld1_f32(p4, &src_a_im[2 * 4]);
  svfloat32_t a_col3_im = svld1_f32(p4, &src_a_im[3 * 4]);

  svfloat32_t c_re;
  svfloat32_t c_im;

  for (int j = 0; j < 4; j++) {
    svfloat32_t b_re = svld1_f32(p4, &src_b_re[j * 4]);
    svfloat32_t b_im = svld1_f32(p4, &src_b_im[j * 4]);

    c_re = svmul_lane_f32(a_col0_re, b_re, 0);
    c_re = svmla_lane_f32(c_re, a_col1_re, b_re, 1);
    c_re = svmla_lane_f32(c_re, a_col2_re, b_re, 2);
    c_re = svmla_lane_f32(c_re, a_col3_re, b_re, 3);
    c_re = svmls_lane_f32(c_re, a_col0_im, b_im, 0);
    c_re = svmls_lane_f32(c_re, a_col1_im, b_im, 1);
    c_re = svmls_lane_f32(c_re, a_col2_im, b_im, 2);
    c_re = svmls_lane_f32(c_re, a_col3_im, b_im, 3);

    c_im = svmul_lane_f32(a_col0_re, b_im, 0);
    c_im = svmla_lane_f32(c_im, a_col1_im, b_re, 1);
    c_im = svmla_lane_f32(c_im, a_col2_re, b_im, 2);
    c_im = svmla_lane_f32(c_im, a_col3_im, b_re, 3);
    c_im = svmla_lane_f32(c_im, a_col0_im, b_re, 0);
    c_im = svmla_lane_f32(c_im, a_col1_re, b_im, 1);
    c_im = svmla_lane_f32(c_im, a_col2_im, b_re, 2);
    c_im = svmla_lane_f32(c_im, a_col3_re, b_im, 3);

    svst1_f32(p4, &dst_re[j * 4], c_re);
    svst1_f32(p4, &dst_im[j * 4], c_im);
  }

#else
  const float32_t *a_ptr_re = (const float32_t *)src_a_re;
  const float32_t *a_ptr_im = (const float32_t *)src_a_im;
  const float32_t *b_ptr_re = (const float32_t *)src_b_re;
  const float32_t *b_ptr_im = (const float32_t *)src_b_im;
  float32_t *out_ptr_re = dst_re;
  float32_t *out_ptr_im = dst_im;
  __asm__ __volatile__(

      "ld1  {v10.4s},         [%x[APtr_re]], #16\n"
      "ld1  {v11.4s},         [%x[APtr_im]], #16\n"

      "ld1  {v18.4s},         [%x[BPtr_re]], #16\n"
      "ld1  {v19.4s},         [%x[BPtr_im]], #16\n"

      "ld1  {v20.4s},         [%x[BPtr_re]], #16\n"
      "ld1  {v21.4s},         [%x[BPtr_im]], #16\n"

      "fmul v2.4s, v10.4s, v18.s[0]\n"
      "fmls v2.4s, v11.4s, v19.s[0]\n"
      "ld1  {v12.4s},         [%x[APtr_re]], #16\n"
      "ld1  {v13.4s},         [%x[APtr_im]], #16\n"
      "fmul v4.4s, v10.4s, v20.s[0]\n"
      "fmls v4.4s, v11.4s, v21.s[0]\n"
      "ld1  {v14.4s},         [%x[APtr_re]], #16\n"
      "ld1  {v15.4s},         [%x[APtr_im]], #16\n"
      "fmul v3.4s, v11.4s, v18.s[0]\n"
      "fmla v3.4s, v10.4s, v19.s[0]\n"
      "ld1  {v16.4s},         [%x[APtr_re]], #16\n"
      "ld1  {v17.4s},         [%x[APtr_im]], #16\n"
      "fmul v5.4s, v11.4s, v20.s[0]\n"
      "fmla v5.4s, v10.4s, v21.s[0]\n"
      "fmla v2.4s, v12.4s, v18.s[1]\n"
      "fmls v2.4s, v13.4s, v19.s[1]\n"
      "fmla v3.4s, v13.4s, v18.s[1]\n"
      "fmla v3.4s, v12.4s, v19.s[1]\n"
      "fmla v4.4s, v12.4s, v20.s[1]\n"
      "fmls v4.4s, v13.4s, v21.s[1]\n"
      "fmla v5.4s, v13.4s, v20.s[1]\n"
      "fmla v5.4s, v12.4s, v21.s[1]\n"

      "fmla v2.4s, v14.4s, v18.s[2]\n"
      "fmls v2.4s, v15.4s, v19.s[2]\n"
      "fmla v3.4s, v15.4s, v18.s[2]\n"
      "fmla v3.4s, v14.4s, v19.s[2]\n"
      "fmla v4.4s, v14.4s, v20.s[2]\n"
      "fmls v4.4s, v15.4s, v21.s[2]\n"
      "fmla v5.4s, v15.4s, v20.s[2]\n"
      "fmla v5.4s, v14.4s, v21.s[2]\n"

      "fmla v2.4s, v16.4s, v18.s[3]\n"
      "fmls v2.4s, v17.4s, v19.s[3]\n"
      "fmla v3.4s, v17.4s, v18.s[3]\n"
      "fmla v3.4s, v16.4s, v19.s[3]\n"
      "st1  {v2.4s}, [%x[outPtr_re]], #16\n"
      "st1  {v3.4s}, [%x[outPtr_im]], #16\n"
      "fmla v4.4s, v16.4s, v20.s[3]\n"
      "fmls v4.4s, v17.4s, v21.s[3]\n"
      "ld1  {v18.4s},         [%x[BPtr_re]], #16\n"
      "ld1  {v19.4s},         [%x[BPtr_im]], #16\n"
      "fmla v5.4s, v17.4s, v20.s[3]\n"
      "fmla v5.4s, v16.4s, v21.s[3]\n"

      "st1  {v4.4s}, [%x[outPtr_re]], #16\n"
      "st1  {v5.4s}, [%x[outPtr_im]], #16\n"
      "ld1  {v20.4s},         [%x[BPtr_re]], #16\n"
      "ld1  {v21.4s},         [%x[BPtr_im]], #16\n"
      "fmul v2.4s, v10.4s, v18.s[0]\n"
      "fmls v2.4s, v11.4s, v19.s[0]\n"
      "fmul v4.4s, v10.4s, v20.s[0]\n"
      "fmls v4.4s, v11.4s, v21.s[0]\n"
      "fmul v3.4s, v11.4s, v18.s[0]\n"
      "fmla v3.4s, v10.4s, v19.s[0]\n"
      "fmul v5.4s, v11.4s, v20.s[0]\n"
      "fmla v5.4s, v10.4s, v21.s[0]\n"

      "fmla v2.4s, v12.4s, v18.s[1]\n"
      "fmls v2.4s, v13.4s, v19.s[1]\n"
      "fmla v3.4s, v13.4s, v18.s[1]\n"
      "fmla v3.4s, v12.4s, v19.s[1]\n"
      "fmla v4.4s, v12.4s, v20.s[1]\n"
      "fmls v4.4s, v13.4s, v21.s[1]\n"
      "fmla v5.4s, v13.4s, v20.s[1]\n"
      "fmla v5.4s, v12.4s, v21.s[1]\n"

      "fmla v2.4s, v14.4s, v18.s[2]\n"
      "fmls v2.4s, v15.4s, v19.s[2]\n"
      "fmla v3.4s, v15.4s, v18.s[2]\n"
      "fmla v3.4s, v14.4s, v19.s[2]\n"
      "fmla v4.4s, v14.4s, v20.s[2]\n"
      "fmls v4.4s, v15.4s, v21.s[2]\n"
      "fmla v5.4s, v15.4s, v20.s[2]\n"
      "fmla v5.4s, v14.4s, v21.s[2]\n"

      "fmla v2.4s, v16.4s, v18.s[3]\n"
      "fmls v2.4s, v17.4s, v19.s[3]\n"
      "fmla v3.4s, v17.4s, v18.s[3]\n"
      "fmla v3.4s, v16.4s, v19.s[3]\n"
      "fmla v4.4s, v16.4s, v20.s[3]\n"
      "fmls v4.4s, v17.4s, v21.s[3]\n"
      "fmla v5.4s, v17.4s, v20.s[3]\n"
      "fmla v5.4s, v16.4s, v21.s[3]\n"

      "st1  {v2.4s}, [%x[outPtr_re]], #16\n"
      "st1  {v3.4s}, [%x[outPtr_im]], #16\n"
      "st1  {v4.4s}, [%x[outPtr_re]], #16\n"
      "st1  {v5.4s}, [%x[outPtr_im]], #16\n"

      : [APtr_re] "+r"(a_ptr_re), [APtr_im] "+r"(a_ptr_im),
        [BPtr_re] "+r"(b_ptr_re), [BPtr_im] "+r"(b_ptr_im),
        [outPtr_re] "+r"(out_ptr_re), [outPtr_im] "+r"(out_ptr_im)

      :

      : "v10", "v11", "v12", "v13", "v14", "v15", "v16", "v17", "v18", "v19",
        "v20", "v21", "v2", "v3", "v4", "v5", "cc");
#endif

  return ARMRAL_SUCCESS;
}

armral_status
armral_cmplx_matmul_f32(const uint16_t m, const uint16_t n, const uint16_t k,
                        const armral_cmplx_f32_t *__restrict p_src_a,
                        const armral_cmplx_f32_t *__restrict p_src_b,
                        armral_cmplx_f32_t *p_dst) {
  // Note: the a/b flip is intentional since the 2x2 function expects
  //       column-major input, making all matrices transposed
  //       i.e. C = A^T * B^T = (B * A)^T
  //       Unfortunately clang-tidy thinks this is an error, because
  //       the function has parameters named p_src_a and p_src_b
  //       so we must disable checking for swapped arguments here
  // NOLINTBEGIN(readability-suspicious-call-argument)
  if (m == 2 && n == 2 && k == 2) {
    return armral_cmplx_mat_mult_2x2_f32(p_src_b, p_src_a, p_dst);
  }
  // NOLINTEND(readability-suspicious-call-argument)
  if (m == 4 && n == 4 && k == 4) {
    // This function expects row-major input so no need to flip a/b here
    return cmplx_matmul_4x4_f32<false>(4, 4, p_src_a, p_src_b, p_dst);
  }

  constexpr uint32_t kb = 72;
  constexpr uint32_t nb = 8;
  const bool do_k_blocking = k >= kb;
  const bool do_n_blocking = n >= nb;

  if (!do_k_blocking && !do_n_blocking) {
    // No blocking
    return cmplx_matmul_f32<false>(m, n, k, k, n, p_src_a, p_src_b, p_dst);
  }
  // Set up parameters for 4-by-4 inner blocking
  uint16_t rem_m_inner = m % 4;
  uint16_t rem_n = n % nb;
  uint32_t n_idx = (n / nb) * nb;
  uint16_t rem_k_inner = kb % 4;
  // k blocking
  for (int32_t kbi = 0; kbi < int32_t(k) - (int32_t(kb) - 1); kbi += kb) {
    // n blocking
    matmul_n_block(m, n, k, kb, kbi, nb, rem_n, n_idx, rem_k_inner, rem_m_inner,
                   p_src_a, p_src_b, p_dst);
  }
  // Clean up remaining ks
  uint16_t rem_k = k % kb;
  if (rem_k != 0U) {
    uint32_t kbi = (k / kb) * kb;
    rem_k_inner = rem_k % 4;
    matmul_n_block(m, n, k, rem_k, kbi, nb, rem_n, n_idx, rem_k_inner,
                   rem_m_inner, p_src_a, p_src_b, p_dst);
  }
  return ARMRAL_SUCCESS;
}
