/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "arm_solve_6sc.h"
#include "arm_solve_convert.h"
#include "intrinsics.h"

static inline float32x4_t __attribute__((always_inline))
shuf_f32_0011(float32x2_t a) {
  // permute as [a[0], a[0], a[1], a[1]]
  // need to use inline asm since no ACLE zip intrinsics take float32x2
  // inputs and produce float32x4 outputs.
  float32x4_t ret;
  asm("zip1 %0.4s, %1.4s, %1.4s" : "=w"(ret) : "w"(a));
  return ret;
}

armral_status armral_solve_2x2_6sc_f32(
    const uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    const uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    const uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    const uint32_t p_xstride, const armral_fixed_point_index num_fract_bits_x) {

  const int16_t *y0_ptr = (const int16_t *)p_y;
  const int16_t *y1_ptr = (const int16_t *)p_y + p_ystride;

  int16_t *x0_ptr = (int16_t *)p_x;
  int16_t *x1_ptr = (int16_t *)p_x + p_xstride;

  const armral_fixed_point_index num_fract_bits_y[] = {p_y_num_fract_bits[0],
                                                       p_y_num_fract_bits[1]};

  // G matrix valid for 6 inputs
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    int16x8_t y0_quadw = vld1q_s16(y0_ptr);
    int16x8_t y1_quadw = vld1q_s16(y1_ptr);
    float32x4_t y0_re;
    float32x4_t y0_im;
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re, &y0_im);
    float32x4_t y1_re;
    float32x4_t y1_im;
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re, &y1_im);
    // Load G00[0] G00[1]
    float32x2_t g00_real = vld1_f32(p_g_real); // 32x2
    float32x2_t g00_imag = vld1_f32(p_g_imag);

    float32x4_t x0_re = vmulq_lane_f32(y0_re, g00_real, 0);
    x0_re = vfmsq_lane_f32(x0_re, y0_im, g00_imag, 0);
    float32x4_t x0_im = vmulq_lane_f32(y0_im, g00_real, 0);
    x0_im = vfmaq_lane_f32(x0_im, y0_re, g00_imag, 0);

    // Load G01[0]
    float32x2_t g01_real = vld1_f32(p_g_real + p_gstride);
    float32x2_t g01_imag = vld1_f32(p_g_imag + p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y1_re, g01_real, 0);
    x0_re = vfmsq_lane_f32(x0_re, y1_im, g01_imag, 0);
    x0_im = vfmaq_lane_f32(x0_im, y1_im, g01_real, 0);
    x0_im = vfmaq_lane_f32(x0_im, y1_re, g01_imag, 0);

    // Load G10[0]
    float32x2_t g10_real = vld1_f32(p_g_real + 2 * p_gstride);
    float32x2_t g10_imag = vld1_f32(p_g_imag + 2 * p_gstride);

    float32x4_t x1_re = vmulq_lane_f32(y0_re, g10_real, 0);
    x1_re = vfmsq_lane_f32(x1_re, y0_im, g10_imag, 0);
    float32x4_t x1_im = vmulq_lane_f32(y0_im, g10_real, 0);
    x1_im = vfmaq_lane_f32(x1_im, y0_re, g10_imag, 0);

    // Load G11[0]
    float32x2_t g11_real = vld1_f32(p_g_real + 3 * p_gstride);
    float32x2_t g11_imag = vld1_f32(p_g_imag + 3 * p_gstride);

    x1_re = vfmaq_lane_f32(x1_re, y1_re, g11_real, 0);
    x1_re = vfmsq_lane_f32(x1_re, y1_im, g11_imag, 0);
    x1_im = vfmaq_lane_f32(x1_im, y1_im, g11_real, 0);
    x1_im = vfmaq_lane_f32(x1_im, y1_re, g11_imag, 0);

    int16x8_t x0_bis;
    int16x8_t x1_bis;
    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im,
                              &x0_bis, &x1_bis);

    vst1q_s16(x0_ptr, x0_bis);
    vst1q_s16(x1_ptr, x1_bis);

    // Load next 4
    y0_quadw = vld1q_s16(y0_ptr + 8);
    y1_quadw = vld1q_s16(y1_ptr + 8);

    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re, &y0_im);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re, &y1_im);

    float32x4_t g00_temp0 = shuf_f32_0011(g00_real);
    float32x4_t g00_temp1 = shuf_f32_0011(g00_imag);

    x0_re = vmulq_f32(y0_re, g00_temp0);
    x0_re = vfmsq_f32(x0_re, y0_im, g00_temp1);
    x0_im = vmulq_f32(y0_im, g00_temp0);
    x0_im = vfmaq_f32(x0_im, y0_re, g00_temp1);

    float32x4_t g01_temp0 = shuf_f32_0011(g01_real);
    float32x4_t g01_temp1 = shuf_f32_0011(g01_imag);

    x0_re = vfmaq_f32(x0_re, y1_re, g01_temp0);
    x0_re = vfmsq_f32(x0_re, y1_im, g01_temp1);
    x0_im = vfmaq_f32(x0_im, y1_im, g01_temp0);
    x0_im = vfmaq_f32(x0_im, y1_re, g01_temp1);

    float32x4_t g10_temp0 = shuf_f32_0011(g10_real);
    float32x4_t g10_temp1 = shuf_f32_0011(g10_imag);

    x1_re = vmulq_f32(y0_re, g10_temp0);
    x1_re = vfmsq_f32(x1_re, y0_im, g10_temp1);
    x1_im = vmulq_f32(y0_im, g10_temp0);
    x1_im = vfmaq_f32(x1_im, y0_re, g10_temp1);

    float32x4_t g11_temp0 = shuf_f32_0011(g11_real);
    float32x4_t g11_temp1 = shuf_f32_0011(g11_imag);

    x1_re = vfmaq_f32(x1_re, y1_re, g11_temp0);
    x1_re = vfmsq_f32(x1_re, y1_im, g11_temp1);
    x1_im = vfmaq_f32(x1_im, y1_im, g11_temp0);
    x1_im = vfmaq_f32(x1_im, y1_re, g11_temp1);

    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im,
                              &x0_bis, &x1_bis);

    vst1q_s16(x0_ptr + 8, x0_bis);
    vst1q_s16(x1_ptr + 8, x1_bis);

    // Load next 4
    y0_quadw = vld1q_s16(y0_ptr + 16);
    y1_quadw = vld1q_s16(y1_ptr + 16);

    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re, &y0_im);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re, &y1_im);

    x0_re = vmulq_lane_f32(y0_re, g00_real, 1);
    x0_re = vfmsq_lane_f32(x0_re, y0_im, g00_imag, 1);
    x0_im = vmulq_lane_f32(y0_im, g00_real, 1);
    x0_im = vfmaq_lane_f32(x0_im, y0_re, g00_imag, 1);

    x0_re = vfmaq_lane_f32(x0_re, y1_re, g01_real, 1);
    x0_re = vfmsq_lane_f32(x0_re, y1_im, g01_imag, 1);
    x0_im = vfmaq_lane_f32(x0_im, y1_im, g01_real, 1);
    x0_im = vfmaq_lane_f32(x0_im, y1_re, g01_imag, 1);

    x1_re = vmulq_lane_f32(y0_re, g10_real, 1);
    x1_re = vfmsq_lane_f32(x1_re, y0_im, g10_imag, 1);
    x1_im = vmulq_lane_f32(y0_im, g10_real, 1);
    x1_im = vfmaq_lane_f32(x1_im, y0_re, g10_imag, 1);

    x1_re = vfmaq_lane_f32(x1_re, y1_re, g11_real, 1);
    x1_re = vfmsq_lane_f32(x1_re, y1_im, g11_imag, 1);
    x1_im = vfmaq_lane_f32(x1_im, y1_im, g11_real, 1);
    x1_im = vfmaq_lane_f32(x1_im, y1_re, g11_imag, 1);

    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im,
                              &x0_bis, &x1_bis);

    vst1q_s16(x0_ptr + 16, x0_bis);
    vst1q_s16(x1_ptr + 16, x1_bis);
    x0_ptr = x0_ptr + 24;
    x1_ptr = x1_ptr + 24;
    p_g_real = p_g_real + 2;
    p_g_imag = p_g_imag + 2;
    y0_ptr = y0_ptr + 24;
    y1_ptr = y1_ptr + 24;
  }
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_2x4_6sc_f32(
    const uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    const uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    const uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    const uint32_t p_xstride, const armral_fixed_point_index num_fract_bits_x) {

  const int16_t *y0_ptr = (const int16_t *)p_y;
  const int16_t *y1_ptr = (const int16_t *)p_y + p_ystride;
  const int16_t *y2_ptr = (const int16_t *)p_y + 2 * p_ystride;
  const int16_t *y3_ptr = (const int16_t *)p_y + 3 * p_ystride;

  int16_t *x0_ptr = (int16_t *)p_x;
  int16_t *x1_ptr = (int16_t *)p_x + p_xstride;

  const armral_fixed_point_index num_fract_bits_y[] = {
      p_y_num_fract_bits[0], p_y_num_fract_bits[1], p_y_num_fract_bits[2],
      p_y_num_fract_bits[3]};

  // G matrix valid for 6 inputs
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    int16x8_t y0_quadw = vld1q_s16(y0_ptr);
    int16x8_t y1_quadw = vld1q_s16(y1_ptr);
    float32x4_t y0_re;
    float32x4_t y0_im;
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re, &y0_im);
    float32x4_t y1_re;
    float32x4_t y1_im;
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re, &y1_im);

    // Load G00[0] G00[1]
    float32x2_t g00_real = vld1_f32(p_g_real); // 32x2
    float32x2_t g00_imag = vld1_f32(p_g_imag);

    float32x4_t x0_re = vmulq_lane_f32(y0_re, g00_real, 0);
    x0_re = vfmsq_lane_f32(x0_re, y0_im, g00_imag, 0);
    float32x4_t x0_im = vmulq_lane_f32(y0_im, g00_real, 0);
    x0_im = vfmaq_lane_f32(x0_im, y0_re, g00_imag, 0);

    // Load G01[0] G01[1]
    int16x8_t y2_quadw = vld1q_s16(y2_ptr);
    int16x8_t y3_quadw = vld1q_s16(y3_ptr);
    float32x4_t y2_re;
    float32x4_t y2_im;
    armral_convert_i16_f32_x4(num_fract_bits_y[2], y2_quadw, &y2_re, &y2_im);
    float32x4_t y3_re;
    float32x4_t y3_im;
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y3_quadw, &y3_re, &y3_im);
    float32x2_t g01_real = vld1_f32(p_g_real + p_gstride);
    float32x2_t g01_imag = vld1_f32(p_g_imag + p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y1_re, g01_real, 0);
    x0_re = vfmsq_lane_f32(x0_re, y1_im, g01_imag, 0);
    x0_im = vfmaq_lane_f32(x0_im, y1_im, g01_real, 0);
    x0_im = vfmaq_lane_f32(x0_im, y1_re, g01_imag, 0);

    // Load G02[0] G02[1]
    float32x2_t g02_real = vld1_f32(p_g_real + 2 * p_gstride);
    float32x2_t g02_imag = vld1_f32(p_g_imag + 2 * p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y2_re, g02_real, 0);
    x0_re = vfmsq_lane_f32(x0_re, y2_im, g02_imag, 0);
    x0_im = vfmaq_lane_f32(x0_im, y2_im, g02_real, 0);
    x0_im = vfmaq_lane_f32(x0_im, y2_re, g02_imag, 0);

    // Load G03[0] G03[1]
    float32x2_t g03_real = vld1_f32(p_g_real + 3 * p_gstride);
    float32x2_t g03_imag = vld1_f32(p_g_imag + 3 * p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y3_re, g03_real, 0);
    x0_re = vfmsq_lane_f32(x0_re, y3_im, g03_imag, 0);
    x0_im = vfmaq_lane_f32(x0_im, y3_im, g03_real, 0);
    x0_im = vfmaq_lane_f32(x0_im, y3_re, g03_imag, 0);

    // Load G10[0] G10[1]
    float32x2_t g10_real = vld1_f32(p_g_real + 4 * p_gstride);
    float32x2_t g10_imag = vld1_f32(p_g_imag + 4 * p_gstride);

    float32x4_t x1_re = vmulq_lane_f32(y0_re, g10_real, 0);
    x1_re = vfmsq_lane_f32(x1_re, y0_im, g10_imag, 0);
    float32x4_t x1_im = vmulq_lane_f32(y0_im, g10_real, 0);
    x1_im = vfmaq_lane_f32(x1_im, y0_re, g10_imag, 0);

    // Load G11[0] G11[1]
    float32x2_t g11_real = vld1_f32(p_g_real + 5 * p_gstride);
    float32x2_t g11_imag = vld1_f32(p_g_imag + 5 * p_gstride);

    x1_re = vfmaq_lane_f32(x1_re, y1_re, g11_real, 0);
    x1_re = vfmsq_lane_f32(x1_re, y1_im, g11_imag, 0);
    x1_im = vfmaq_lane_f32(x1_im, y1_im, g11_real, 0);
    x1_im = vfmaq_lane_f32(x1_im, y1_re, g11_imag, 0);

    // Load G12[0] G12[1]
    float32x2_t g12_real = vld1_f32(p_g_real + 6 * p_gstride);
    float32x2_t g12_imag = vld1_f32(p_g_imag + 6 * p_gstride);

    x1_re = vfmaq_lane_f32(x1_re, y2_re, g12_real, 0);
    x1_re = vfmsq_lane_f32(x1_re, y2_im, g12_imag, 0);
    x1_im = vfmaq_lane_f32(x1_im, y2_im, g12_real, 0);
    x1_im = vfmaq_lane_f32(x1_im, y2_re, g12_imag, 0);

    // Load G13[0] G13[1]
    float32x2_t g13_real = vld1_f32(p_g_real + 7 * p_gstride);
    float32x2_t g13_imag = vld1_f32(p_g_imag + 7 * p_gstride);

    x1_re = vfmaq_lane_f32(x1_re, y3_re, g13_real, 0);
    x1_re = vfmsq_lane_f32(x1_re, y3_im, g13_imag, 0);
    x1_im = vfmaq_lane_f32(x1_im, y3_im, g13_real, 0);
    x1_im = vfmaq_lane_f32(x1_im, y3_re, g13_imag, 0);

    int16x8_t x0_bis;
    int16x8_t x1_bis;
    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im,
                              &x0_bis, &x1_bis);

    vst1q_s16(x0_ptr, x0_bis);
    vst1q_s16(x1_ptr, x1_bis);

    // Load next 4
    y0_quadw = vld1q_s16(y0_ptr + 8);
    y1_quadw = vld1q_s16(y1_ptr + 8);

    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re, &y0_im);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re, &y1_im);

    float32x4_t g00_temp0 = shuf_f32_0011(g00_real);
    float32x4_t g00_temp1 = shuf_f32_0011(g00_imag);

    y2_quadw = vld1q_s16(y2_ptr + 8);
    y3_quadw = vld1q_s16(y3_ptr + 8);

    armral_convert_i16_f32_x4(num_fract_bits_y[2], y2_quadw, &y2_re, &y2_im);
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y3_quadw, &y3_re, &y3_im);
    x0_re = vmulq_f32(y0_re, g00_temp0);
    x0_re = vfmsq_f32(x0_re, y0_im, g00_temp1);
    x0_im = vmulq_f32(y0_im, g00_temp0);
    x0_im = vfmaq_f32(x0_im, y0_re, g00_temp1);

    float32x4_t g01_temp0 = shuf_f32_0011(g01_real);
    float32x4_t g01_temp1 = shuf_f32_0011(g01_imag);

    x0_re = vfmaq_f32(x0_re, y1_re, g01_temp0);
    x0_re = vfmsq_f32(x0_re, y1_im, g01_temp1);
    x0_im = vfmaq_f32(x0_im, y1_im, g01_temp0);
    x0_im = vfmaq_f32(x0_im, y1_re, g01_temp1);

    float32x4_t g02_temp0 = shuf_f32_0011(g02_real);
    float32x4_t g02_temp1 = shuf_f32_0011(g02_imag);

    x0_re = vfmaq_f32(x0_re, y2_re, g02_temp0);
    x0_re = vfmsq_f32(x0_re, y2_im, g02_temp1);
    x0_im = vfmaq_f32(x0_im, y2_im, g02_temp0);
    x0_im = vfmaq_f32(x0_im, y2_re, g02_temp1);

    float32x4_t g03_temp0 = shuf_f32_0011(g03_real);
    float32x4_t g03_temp1 = shuf_f32_0011(g03_imag);

    x0_re = vfmaq_f32(x0_re, y3_re, g03_temp0);
    x0_re = vfmsq_f32(x0_re, y3_im, g03_temp1);
    x0_im = vfmaq_f32(x0_im, y3_im, g03_temp0);
    x0_im = vfmaq_f32(x0_im, y3_re, g03_temp1);

    float32x4_t g10_temp0 = shuf_f32_0011(g10_real);
    float32x4_t g10_temp1 = shuf_f32_0011(g10_imag);

    x1_re = vmulq_f32(y0_re, g10_temp0);
    x1_re = vfmsq_f32(x1_re, y0_im, g10_temp1);
    x1_im = vmulq_f32(y0_im, g10_temp0);
    x1_im = vfmaq_f32(x1_im, y0_re, g10_temp1);

    float32x4_t g11_temp0 = shuf_f32_0011(g11_real);
    float32x4_t g11_temp1 = shuf_f32_0011(g11_imag);

    x1_re = vfmaq_f32(x1_re, y1_re, g11_temp0);
    x1_re = vfmsq_f32(x1_re, y1_im, g11_temp1);
    x1_im = vfmaq_f32(x1_im, y1_im, g11_temp0);
    x1_im = vfmaq_f32(x1_im, y1_re, g11_temp1);

    float32x4_t g12_temp0 = shuf_f32_0011(g12_real);
    float32x4_t g12_temp1 = shuf_f32_0011(g12_imag);

    x1_re = vfmaq_f32(x1_re, y2_re, g12_temp0);
    x1_re = vfmsq_f32(x1_re, y2_im, g12_temp1);
    x1_im = vfmaq_f32(x1_im, y2_im, g12_temp0);
    x1_im = vfmaq_f32(x1_im, y2_re, g12_temp1);

    float32x4_t g13_temp0 = shuf_f32_0011(g13_real);
    float32x4_t g13_temp1 = shuf_f32_0011(g13_imag);

    x1_re = vfmaq_f32(x1_re, y3_re, g13_temp0);
    x1_re = vfmsq_f32(x1_re, y3_im, g13_temp1);
    x1_im = vfmaq_f32(x1_im, y3_im, g13_temp0);
    x1_im = vfmaq_f32(x1_im, y3_re, g13_temp1);

    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im,
                              &x0_bis, &x1_bis);

    vst1q_s16(x0_ptr + 8, x0_bis);
    vst1q_s16(x1_ptr + 8, x1_bis);

    // Load next 4
    y0_quadw = vld1q_s16(y0_ptr + 16);
    y1_quadw = vld1q_s16(y1_ptr + 16);

    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re, &y0_im);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re, &y1_im);
    x0_re = vmulq_lane_f32(y0_re, g00_real, 1);
    x0_re = vfmsq_lane_f32(x0_re, y0_im, g00_imag, 1);
    x0_im = vmulq_lane_f32(y0_im, g00_real, 1);
    x0_im = vfmaq_lane_f32(x0_im, y0_re, g00_imag, 1);

    y2_quadw = vld1q_s16(y2_ptr + 16);
    y3_quadw = vld1q_s16(y3_ptr + 16);
    armral_convert_i16_f32_x4(num_fract_bits_y[2], y2_quadw, &y2_re, &y2_im);
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y3_quadw, &y3_re, &y3_im);

    x0_re = vfmaq_lane_f32(x0_re, y1_re, g01_real, 1);
    x0_re = vfmsq_lane_f32(x0_re, y1_im, g01_imag, 1);
    x0_im = vfmaq_lane_f32(x0_im, y1_im, g01_real, 1);
    x0_im = vfmaq_lane_f32(x0_im, y1_re, g01_imag, 1);

    x0_re = vfmaq_lane_f32(x0_re, y2_re, g02_real, 1);
    x0_re = vfmsq_lane_f32(x0_re, y2_im, g02_imag, 1);
    x0_im = vfmaq_lane_f32(x0_im, y2_im, g02_real, 1);
    x0_im = vfmaq_lane_f32(x0_im, y2_re, g02_imag, 1);

    x0_re = vfmaq_lane_f32(x0_re, y3_re, g03_real, 1);
    x0_re = vfmsq_lane_f32(x0_re, y3_im, g03_imag, 1);
    x0_im = vfmaq_lane_f32(x0_im, y3_im, g03_real, 1);
    x0_im = vfmaq_lane_f32(x0_im, y3_re, g03_imag, 1);

    x1_re = vmulq_lane_f32(y0_re, g10_real, 1);
    x1_re = vfmsq_lane_f32(x1_re, y0_im, g10_imag, 1);
    x1_im = vmulq_lane_f32(y0_im, g10_real, 1);
    x1_im = vfmaq_lane_f32(x1_im, y0_re, g10_imag, 1);

    x1_re = vfmaq_lane_f32(x1_re, y1_re, g11_real, 1);
    x1_re = vfmsq_lane_f32(x1_re, y1_im, g11_imag, 1);
    x1_im = vfmaq_lane_f32(x1_im, y1_im, g11_real, 1);
    x1_im = vfmaq_lane_f32(x1_im, y1_re, g11_imag, 1);

    x1_re = vfmaq_lane_f32(x1_re, y2_re, g12_real, 1);
    x1_re = vfmsq_lane_f32(x1_re, y2_im, g12_imag, 1);
    x1_im = vfmaq_lane_f32(x1_im, y2_im, g12_real, 1);
    x1_im = vfmaq_lane_f32(x1_im, y2_re, g12_imag, 1);

    x1_re = vfmaq_lane_f32(x1_re, y3_re, g13_real, 1);
    x1_re = vfmsq_lane_f32(x1_re, y3_im, g13_imag, 1);
    x1_im = vfmaq_lane_f32(x1_im, y3_im, g13_real, 1);
    x1_im = vfmaq_lane_f32(x1_im, y3_re, g13_imag, 1);

    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im,
                              &x0_bis, &x1_bis);

    vst1q_s16(x0_ptr + 16, x0_bis);
    vst1q_s16(x1_ptr + 16, x1_bis);
    x0_ptr = x0_ptr + 24;
    x1_ptr = x1_ptr + 24;
    p_g_real = p_g_real + 2;
    p_g_imag = p_g_imag + 2;
    y0_ptr = y0_ptr + 24;
    y1_ptr = y1_ptr + 24;
    y2_ptr = y2_ptr + 24;
    y3_ptr = y3_ptr + 24;
  }
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_4x4_6sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride, const armral_fixed_point_index *p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    armral_fixed_point_index num_fract_bits_x) {

  const int16_t *y0_ptr = (const int16_t *)p_y;
  const int16_t *y1_ptr = (const int16_t *)p_y + p_ystride;
  const int16_t *y2_ptr = (const int16_t *)p_y + 2 * p_ystride;
  const int16_t *y3_ptr = (const int16_t *)p_y + 3 * p_ystride;

  int16_t *x0_ptr = (int16_t *)p_x;
  int16_t *x1_ptr = (int16_t *)p_x + p_xstride;
  int16_t *x2_ptr = (int16_t *)p_x + 2 * p_xstride;
  int16_t *x3_ptr = (int16_t *)p_x + 3 * p_xstride;

#if ARMRAL_ARCH_SVE >= 2
  svbool_t pg8 = svptrue_pat_b16(SV_VL8);
  svbool_t pg2 = svptrue_pat_b32(SV_VL2);
  svbool_t podd = svtrn1_b16(svpfalse_b(), pg8);

  // Each element of y may have a different Q representation,
  // scale is used to convert the values from Q format to f32
  float32_t scale[] = {1. / (1U << (16 + p_y_num_fract_bits[0])),
                       1. / (1U << (16 + p_y_num_fract_bits[1])),
                       1. / (1U << (16 + p_y_num_fract_bits[2])),
                       1. / (1U << (16 + p_y_num_fract_bits[3]))};

  float32_t x_mult = 1 << num_fract_bits_x;
  // The loop is unrolled so that 2 matrices are computed per iteration,
  // therefore 12 subcarriers (i.e. y vectors) are used in each iteration

  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    // Load the first 4 y vectors, real/imaginary components are stored
    // in vectors of length 4 respectively. After loading the values convert
    // to f32 and scale by the number of fractional bits for that vector.
    svint32_t y0_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr - 1));
    svint32_t y0_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr));
    svfloat32_t y0_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y0_int_re), scale[0]);
    svfloat32_t y0_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y0_int_im), scale[0]);

    svint32_t y1_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr - 1));
    svint32_t y1_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr));
    svfloat32_t y1_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y1_int_re), scale[1]);
    svfloat32_t y1_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y1_int_im), scale[1]);

    svint32_t y2_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr - 1));
    svint32_t y2_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr));
    svfloat32_t y2_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y2_int_re), scale[2]);
    svfloat32_t y2_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y2_int_im), scale[2]);

    svint32_t y3_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr - 1));
    svint32_t y3_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr));
    svfloat32_t y3_re =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y3_int_re), scale[3]);
    svfloat32_t y3_im =
        svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y3_int_im), scale[3]);

    // Load the value stored at index [0,0] in the first two matrices
    // G00[0]/G00[1], We assume that the matrices are stored such that
    // contiguous elements in memory are from the same element of different
    // matrices
    svfloat32_t g00_re = svld1_f32(pg2, &p_g_real[0 * p_gstride]);
    svfloat32_t g00_im = svld1_f32(pg2, &p_g_imag[0 * p_gstride]);

    svfloat32_t x0_re = svmul_lane_f32(y0_re, g00_re, 0);
    x0_re = svmls_lane_f32(x0_re, y0_im, g00_im, 0);
    svfloat32_t x0_im = svmul_lane_f32(y0_re, g00_im, 0);
    x0_im = svmla_lane_f32(x0_im, y0_im, g00_re, 0);

    // G10[0]
    svfloat32_t g10_re = svld1_f32(pg2, &p_g_real[4 * p_gstride]);
    svfloat32_t g10_im = svld1_f32(pg2, &p_g_imag[4 * p_gstride]);

    svfloat32_t x1_re = svmul_lane_f32(y0_re, g10_re, 0);
    x1_re = svmls_lane_f32(x1_re, y0_im, g10_im, 0);
    svfloat32_t x1_im = svmul_lane_f32(y0_re, g10_im, 0);
    x1_im = svmla_lane_f32(x1_im, y0_im, g10_re, 0);

    // G01[0]
    svfloat32_t g01_re = svld1_f32(pg2, &p_g_real[1 * p_gstride]);
    svfloat32_t g01_im = svld1_f32(pg2, &p_g_imag[1 * p_gstride]);

    x0_re = svmla_lane_f32(x0_re, y1_re, g01_re, 0);
    x0_re = svmls_lane_f32(x0_re, y1_im, g01_im, 0);
    x0_im = svmla_lane_f32(x0_im, y1_re, g01_im, 0);
    x0_im = svmla_lane_f32(x0_im, y1_im, g01_re, 0);

    // G11[0]
    svfloat32_t g11_re = svld1_f32(pg2, &p_g_real[5 * p_gstride]);
    svfloat32_t g11_im = svld1_f32(pg2, &p_g_imag[5 * p_gstride]);

    x1_re = svmla_lane_f32(x1_re, y1_re, g11_re, 0);
    x1_re = svmls_lane_f32(x1_re, y1_im, g11_im, 0);
    x1_im = svmla_lane_f32(x1_im, y1_re, g11_im, 0);
    x1_im = svmla_lane_f32(x1_im, y1_im, g11_re, 0);

    // G02[0]
    svfloat32_t g02_re = svld1_f32(pg2, &p_g_real[2 * p_gstride]);
    svfloat32_t g02_im = svld1_f32(pg2, &p_g_imag[2 * p_gstride]);

    x0_re = svmla_lane_f32(x0_re, y2_re, g02_re, 0);
    x0_re = svmls_lane_f32(x0_re, y2_im, g02_im, 0);
    x0_im = svmla_lane_f32(x0_im, y2_re, g02_im, 0);
    x0_im = svmla_lane_f32(x0_im, y2_im, g02_re, 0);

    // G12[0]
    svfloat32_t g12_re = svld1_f32(pg2, &p_g_real[6 * p_gstride]);
    svfloat32_t g12_im = svld1_f32(pg2, &p_g_imag[6 * p_gstride]);

    x1_re = svmla_lane_f32(x1_re, y2_re, g12_re, 0);
    x1_re = svmls_lane_f32(x1_re, y2_im, g12_im, 0);
    x1_im = svmla_lane_f32(x1_im, y2_re, g12_im, 0);
    x1_im = svmla_lane_f32(x1_im, y2_im, g12_re, 0);

    // G03[0]
    svfloat32_t g03_re = svld1_f32(pg2, &p_g_real[3 * p_gstride]);
    svfloat32_t g03_im = svld1_f32(pg2, &p_g_imag[3 * p_gstride]);

    x0_re = svmla_lane_f32(x0_re, y3_re, g03_re, 0);
    x0_re = svmls_lane_f32(x0_re, y3_im, g03_im, 0);
    x0_im = svmla_lane_f32(x0_im, y3_re, g03_im, 0);
    x0_im = svmla_lane_f32(x0_im, y3_im, g03_re, 0);

    // Store x0
    x0_re = svmul_n_f32_x(pg8, x0_re, x_mult);
    x0_im = svmul_n_f32_x(pg8, x0_im, x_mult);
    svint16_t x0_bis = armral_convert_f32_i16(x0_re, x0_im, pg8);
    svst1_s16(pg8, x0_ptr, x0_bis);

    // G13[0]
    svfloat32_t g13_re = svld1_f32(pg2, &p_g_real[7 * p_gstride]);
    svfloat32_t g13_im = svld1_f32(pg2, &p_g_imag[7 * p_gstride]);

    x1_re = svmla_lane_f32(x1_re, y3_re, g13_re, 0);
    x1_re = svmls_lane_f32(x1_re, y3_im, g13_im, 0);
    x1_im = svmla_lane_f32(x1_im, y3_re, g13_im, 0);
    x1_im = svmla_lane_f32(x1_im, y3_im, g13_re, 0);

    // Store x1
    x1_re = svmul_n_f32_x(pg8, x1_re, x_mult);
    x1_im = svmul_n_f32_x(pg8, x1_im, x_mult);
    svint16_t x1_bis = armral_convert_f32_i16(x1_re, x1_im, pg8);
    svst1_s16(pg8, x1_ptr, x1_bis);

    // G20[0]
    svfloat32_t g20_re = svld1_f32(pg2, &p_g_real[8 * p_gstride]);
    svfloat32_t g20_im = svld1_f32(pg2, &p_g_imag[8 * p_gstride]);

    svfloat32_t x2_re = svmul_lane_f32(y0_re, g20_re, 0);
    x2_re = svmls_lane_f32(x2_re, y0_im, g20_im, 0);
    svfloat32_t x2_im = svmul_lane_f32(y0_re, g20_im, 0);
    x2_im = svmla_lane_f32(x2_im, y0_im, g20_re, 0);

    // G30[0]
    svfloat32_t g30_re = svld1_f32(pg2, &p_g_real[12 * p_gstride]);
    svfloat32_t g30_im = svld1_f32(pg2, &p_g_imag[12 * p_gstride]);

    svfloat32_t x3_re = svmul_lane_f32(y0_re, g30_re, 0);
    x3_re = svmls_lane_f32(x3_re, y0_im, g30_im, 0);
    svfloat32_t x3_im = svmul_lane_f32(y0_re, g30_im, 0);
    x3_im = svmla_lane_f32(x3_im, y0_im, g30_re, 0);

    // G21[0]
    svfloat32_t g21_re = svld1_f32(pg2, &p_g_real[9 * p_gstride]);
    svfloat32_t g21_im = svld1_f32(pg2, &p_g_imag[9 * p_gstride]);

    x2_re = svmla_lane_f32(x2_re, y1_re, g21_re, 0);
    x2_re = svmls_lane_f32(x2_re, y1_im, g21_im, 0);
    x2_im = svmla_lane_f32(x2_im, y1_re, g21_im, 0);
    x2_im = svmla_lane_f32(x2_im, y1_im, g21_re, 0);

    // G31[0]
    svfloat32_t g31_re = svld1_f32(pg2, &p_g_real[13 * p_gstride]);
    svfloat32_t g31_im = svld1_f32(pg2, &p_g_imag[13 * p_gstride]);

    x3_re = svmla_lane_f32(x3_re, y1_re, g31_re, 0);
    x3_re = svmls_lane_f32(x3_re, y1_im, g31_im, 0);
    x3_im = svmla_lane_f32(x3_im, y1_re, g31_im, 0);
    x3_im = svmla_lane_f32(x3_im, y1_im, g31_re, 0);

    // G22[0]
    svfloat32_t g22_re = svld1_f32(pg2, &p_g_real[10 * p_gstride]);
    svfloat32_t g22_im = svld1_f32(pg2, &p_g_imag[10 * p_gstride]);

    x2_re = svmla_lane_f32(x2_re, y2_re, g22_re, 0);
    x2_re = svmls_lane_f32(x2_re, y2_im, g22_im, 0);
    x2_im = svmla_lane_f32(x2_im, y2_re, g22_im, 0);
    x2_im = svmla_lane_f32(x2_im, y2_im, g22_re, 0);

    // G32[0]
    svfloat32_t g32_re = svld1_f32(pg2, &p_g_real[14 * p_gstride]);
    svfloat32_t g32_im = svld1_f32(pg2, &p_g_imag[14 * p_gstride]);

    x3_re = svmla_lane_f32(x3_re, y2_re, g32_re, 0);
    x3_re = svmls_lane_f32(x3_re, y2_im, g32_im, 0);
    x3_im = svmla_lane_f32(x3_im, y2_re, g32_im, 0);
    x3_im = svmla_lane_f32(x3_im, y2_im, g32_re, 0);

    // G23[0]
    svfloat32_t g23_re = svld1_f32(pg2, &p_g_real[11 * p_gstride]);
    svfloat32_t g23_im = svld1_f32(pg2, &p_g_imag[11 * p_gstride]);

    x2_re = svmla_lane_f32(x2_re, y3_re, g23_re, 0);
    x2_re = svmls_lane_f32(x2_re, y3_im, g23_im, 0);
    x2_im = svmla_lane_f32(x2_im, y3_re, g23_im, 0);
    x2_im = svmla_lane_f32(x2_im, y3_im, g23_re, 0);

    // Store x2
    x2_re = svmul_n_f32_x(pg8, x2_re, x_mult);
    x2_im = svmul_n_f32_x(pg8, x2_im, x_mult);
    svint16_t x2_bis = armral_convert_f32_i16(x2_re, x2_im, pg8);
    svst1_s16(pg8, x2_ptr, x2_bis);

    // G33[0]
    svfloat32_t g33_re = svld1_f32(pg2, &p_g_real[15 * p_gstride]);
    svfloat32_t g33_im = svld1_f32(pg2, &p_g_imag[15 * p_gstride]);

    x3_re = svmla_lane_f32(x3_re, y3_re, g33_re, 0);
    x3_re = svmls_lane_f32(x3_re, y3_im, g33_im, 0);
    x3_im = svmla_lane_f32(x3_im, y3_re, g33_im, 0);
    x3_im = svmla_lane_f32(x3_im, y3_im, g33_re, 0);

    // Store x3
    x3_re = svmul_n_f32_x(pg8, x3_re, x_mult);
    x3_im = svmul_n_f32_x(pg8, x3_im, x_mult);
    svint16_t x3_bis = armral_convert_f32_i16(x3_re, x3_im, pg8);
    svst1_s16(pg8, x3_ptr, x3_bis);

    // Load next 4 y entries
    y0_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 7));
    y0_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 8));
    y0_re = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y0_int_re), scale[0]);
    y0_im = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y0_int_im), scale[0]);

    y1_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 7));
    y1_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 8));
    y1_re = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y1_int_re), scale[1]);
    y1_im = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y1_int_im), scale[1]);

    y2_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 7));
    y2_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 8));
    y2_re = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y2_int_re), scale[2]);
    y2_im = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y2_int_im), scale[2]);

    y3_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 7));
    y3_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 8));
    y3_re = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y3_int_re), scale[3]);
    y3_im = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y3_int_im), scale[3]);

    // For the second set of y vectors, 2 values are associated with the first
    // matrix and the remaining values for the second matrix
    // svzip1 is used to duplicate G values and generate a new vector in the
    // form [g00[0], g00[1]] --> [g00[0], g00[0], g00[1], g00[1]] Then each lane
    // of these vectors are multiplied with the corresponding element in y

    svfloat32_t g00_re_temp = svzip1(g00_re, g00_re);
    svfloat32_t g00_im_temp = svzip1(g00_im, g00_im);

    x0_re = svmul_f32_x(pg8, y0_re, g00_re_temp);
    x0_re = svmls_f32_x(pg8, x0_re, y0_im, g00_im_temp);
    x0_im = svmul_f32_x(pg8, y0_re, g00_im_temp);
    x0_im = svmla_f32_x(pg8, x0_im, y0_im, g00_re_temp);

    svfloat32_t g10_re_temp = svzip1(g10_re, g10_re);
    svfloat32_t g10_im_temp = svzip1(g10_im, g10_im);

    x1_re = svmul_f32_x(pg8, y0_re, g10_re_temp);
    x1_re = svmls_f32_x(pg8, x1_re, y0_im, g10_im_temp);
    x1_im = svmul_f32_x(pg8, y0_re, g10_im_temp);
    x1_im = svmla_f32_x(pg8, x1_im, y0_im, g10_re_temp);

    svfloat32_t g01_re_temp = svzip1(g01_re, g01_re);
    svfloat32_t g01_im_temp = svzip1(g01_im, g01_im);

    x0_re = svmla_f32_x(pg8, x0_re, y1_re, g01_re_temp);
    x0_re = svmls_f32_x(pg8, x0_re, y1_im, g01_im_temp);
    x0_im = svmla_f32_x(pg8, x0_im, y1_re, g01_im_temp);
    x0_im = svmla_f32_x(pg8, x0_im, y1_im, g01_re_temp);

    svfloat32_t g11_re_temp = svzip1(g11_re, g11_re);
    svfloat32_t g11_im_temp = svzip1(g11_im, g11_im);

    x1_re = svmla_f32_x(pg8, x1_re, y1_re, g11_re_temp);
    x1_re = svmls_f32_x(pg8, x1_re, y1_im, g11_im_temp);
    x1_im = svmla_f32_x(pg8, x1_im, y1_re, g11_im_temp);
    x1_im = svmla_f32_x(pg8, x1_im, y1_im, g11_re_temp);

    svfloat32_t g02_re_temp = svzip1(g02_re, g02_re);
    svfloat32_t g02_im_temp = svzip1(g02_im, g02_im);

    x0_re = svmla_f32_x(pg8, x0_re, y2_re, g02_re_temp);
    x0_re = svmls_f32_x(pg8, x0_re, y2_im, g02_im_temp);
    x0_im = svmla_f32_x(pg8, x0_im, y2_re, g02_im_temp);
    x0_im = svmla_f32_x(pg8, x0_im, y2_im, g02_re_temp);

    svfloat32_t g12_re_temp = svzip1(g12_re, g12_re);
    svfloat32_t g12_im_temp = svzip1(g12_im, g12_im);

    x1_re = svmla_f32_x(pg8, x1_re, y2_re, g12_re_temp);
    x1_re = svmls_f32_x(pg8, x1_re, y2_im, g12_im_temp);
    x1_im = svmla_f32_x(pg8, x1_im, y2_re, g12_im_temp);
    x1_im = svmla_f32_x(pg8, x1_im, y2_im, g12_re_temp);

    svfloat32_t g03_re_temp = svzip1(g03_re, g03_re);
    svfloat32_t g03_im_temp = svzip1(g03_im, g03_im);

    x0_re = svmla_f32_x(pg8, x0_re, y3_re, g03_re_temp);
    x0_re = svmls_f32_x(pg8, x0_re, y3_im, g03_im_temp);
    x0_im = svmla_f32_x(pg8, x0_im, y3_re, g03_im_temp);
    x0_im = svmla_f32_x(pg8, x0_im, y3_im, g03_re_temp);

    // Store x0
    x0_re = svmul_n_f32_x(pg8, x0_re, x_mult);
    x0_im = svmul_n_f32_x(pg8, x0_im, x_mult);
    x0_bis = armral_convert_f32_i16(x0_re, x0_im, pg8);
    svst1_s16(pg8, x0_ptr + 8, x0_bis);

    svfloat32_t g13_re_temp = svzip1(g13_re, g13_re);
    svfloat32_t g13_im_temp = svzip1(g13_im, g13_im);

    x1_re = svmla_f32_x(pg8, x1_re, y3_re, g13_re_temp);
    x1_re = svmls_f32_x(pg8, x1_re, y3_im, g13_im_temp);
    x1_im = svmla_f32_x(pg8, x1_im, y3_re, g13_im_temp);
    x1_im = svmla_f32_x(pg8, x1_im, y3_im, g13_re_temp);

    // Store x1
    x1_re = svmul_n_f32_x(pg8, x1_re, x_mult);
    x1_im = svmul_n_f32_x(pg8, x1_im, x_mult);
    x1_bis = armral_convert_f32_i16(x1_re, x1_im, pg8);
    svst1_s16(pg8, x1_ptr + 8, x1_bis);

    svfloat32_t g20_re_temp = svzip1(g20_re, g20_re);
    svfloat32_t g20_im_temp = svzip1(g20_im, g20_im);

    x2_re = svmul_f32_x(pg8, y0_re, g20_re_temp);
    x2_re = svmls_f32_x(pg8, x2_re, y0_im, g20_im_temp);
    x2_im = svmul_f32_x(pg8, y0_re, g20_im_temp);
    x2_im = svmla_f32_x(pg8, x2_im, y0_im, g20_re_temp);

    svfloat32_t g30_re_temp = svzip1(g30_re, g30_re);
    svfloat32_t g30_im_temp = svzip1(g30_im, g30_im);

    x3_re = svmul_f32_x(pg8, y0_re, g30_re_temp);
    x3_re = svmls_f32_x(pg8, x3_re, y0_im, g30_im_temp);
    x3_im = svmul_f32_x(pg8, y0_re, g30_im_temp);
    x3_im = svmla_f32_x(pg8, x3_im, y0_im, g30_re_temp);

    svfloat32_t g21_re_temp = svzip1(g21_re, g21_re);
    svfloat32_t g21_im_temp = svzip1(g21_im, g21_im);

    x2_re = svmla_f32_x(pg8, x2_re, y1_re, g21_re_temp);
    x2_re = svmls_f32_x(pg8, x2_re, y1_im, g21_im_temp);
    x2_im = svmla_f32_x(pg8, x2_im, y1_re, g21_im_temp);
    x2_im = svmla_f32_x(pg8, x2_im, y1_im, g21_re_temp);

    svfloat32_t g31_re_temp = svzip1(g31_re, g31_re);
    svfloat32_t g31_im_temp = svzip1(g31_im, g31_im);

    x3_re = svmla_f32_x(pg8, x3_re, y1_re, g31_re_temp);
    x3_re = svmls_f32_x(pg8, x3_re, y1_im, g31_im_temp);
    x3_im = svmla_f32_x(pg8, x3_im, y1_re, g31_im_temp);
    x3_im = svmla_f32_x(pg8, x3_im, y1_im, g31_re_temp);

    svfloat32_t g22_re_temp = svzip1(g22_re, g22_re);
    svfloat32_t g22_im_temp = svzip1(g22_im, g22_im);

    x2_re = svmla_f32_x(pg8, x2_re, y2_re, g22_re_temp);
    x2_re = svmls_f32_x(pg8, x2_re, y2_im, g22_im_temp);
    x2_im = svmla_f32_x(pg8, x2_im, y2_re, g22_im_temp);
    x2_im = svmla_f32_x(pg8, x2_im, y2_im, g22_re_temp);

    svfloat32_t g32_re_temp = svzip1(g32_re, g32_re);
    svfloat32_t g32_im_temp = svzip1(g32_im, g32_im);

    x3_re = svmla_f32_x(pg8, x3_re, y2_re, g32_re_temp);
    x3_re = svmls_f32_x(pg8, x3_re, y2_im, g32_im_temp);
    x3_im = svmla_f32_x(pg8, x3_im, y2_re, g32_im_temp);
    x3_im = svmla_f32_x(pg8, x3_im, y2_im, g32_re_temp);

    svfloat32_t g23_re_temp = svzip1(g23_re, g23_re);
    svfloat32_t g23_im_temp = svzip1(g23_im, g23_im);

    x2_re = svmla_f32_x(pg8, x2_re, y3_re, g23_re_temp);
    x2_re = svmls_f32_x(pg8, x2_re, y3_im, g23_im_temp);
    x2_im = svmla_f32_x(pg8, x2_im, y3_re, g23_im_temp);
    x2_im = svmla_f32_x(pg8, x2_im, y3_im, g23_re_temp);

    // Store x2
    x2_re = svmul_n_f32_x(pg8, x2_re, x_mult);
    x2_im = svmul_n_f32_x(pg8, x2_im, x_mult);
    x2_bis = armral_convert_f32_i16(x2_re, x2_im, pg8);
    svst1_s16(pg8, x2_ptr + 8, x2_bis);

    svfloat32_t g33_re_temp = svzip1(g33_re, g33_re);
    svfloat32_t g33_im_temp = svzip1(g33_im, g33_im);

    x3_re = svmla_f32_x(pg8, x3_re, y3_re, g33_re_temp);
    x3_re = svmls_f32_x(pg8, x3_re, y3_im, g33_im_temp);
    x3_im = svmla_f32_x(pg8, x3_im, y3_re, g33_im_temp);
    x3_im = svmla_f32_x(pg8, x3_im, y3_im, g33_re_temp);

    // Store x3
    x3_re = svmul_n_f32_x(pg8, x3_re, x_mult);
    x3_im = svmul_n_f32_x(pg8, x3_im, x_mult);
    x3_bis = armral_convert_f32_i16(x3_re, x3_im, pg8);
    svst1_s16(pg8, x3_ptr + 8, x3_bis);

    // Load the last 4 y vectors of the loop
    y0_int_re = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 15));
    y0_int_im = svreinterpret_s32_s16(svld1_s16(podd, y0_ptr + 16));
    y0_re = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y0_int_re), scale[0]);
    y0_im = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y0_int_im), scale[0]);

    y1_int_re = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 15));
    y1_int_im = svreinterpret_s32_s16(svld1_s16(podd, y1_ptr + 16));
    y1_re = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y1_int_re), scale[1]);
    y1_im = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y1_int_im), scale[1]);

    y2_int_re = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 15));
    y2_int_im = svreinterpret_s32_s16(svld1_s16(podd, y2_ptr + 16));
    y2_re = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y2_int_re), scale[2]);
    y2_im = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y2_int_im), scale[2]);

    y3_int_re = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 15));
    y3_int_im = svreinterpret_s32_s16(svld1_s16(podd, y3_ptr + 16));
    y3_re = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y3_int_re), scale[3]);
    y3_im = svmul_n_f32_x(pg8, svcvt_f32_s32_x(pg8, y3_int_im), scale[3]);

    // G00[1]
    x0_re = svmul_lane_f32(y0_re, g00_re, 1);
    x0_re = svmls_lane_f32(x0_re, y0_im, g00_im, 1);
    x0_im = svmul_lane_f32(y0_re, g00_im, 1);
    x0_im = svmla_lane_f32(x0_im, y0_im, g00_re, 1);

    // G10[1]
    x1_re = svmul_lane_f32(y0_re, g10_re, 1);
    x1_re = svmls_lane_f32(x1_re, y0_im, g10_im, 1);
    x1_im = svmul_lane_f32(y0_re, g10_im, 1);
    x1_im = svmla_lane_f32(x1_im, y0_im, g10_re, 1);

    // G01[1]
    x0_re = svmla_lane_f32(x0_re, y1_re, g01_re, 1);
    x0_re = svmls_lane_f32(x0_re, y1_im, g01_im, 1);
    x0_im = svmla_lane_f32(x0_im, y1_re, g01_im, 1);
    x0_im = svmla_lane_f32(x0_im, y1_im, g01_re, 1);

    // G11[1]
    x1_re = svmla_lane_f32(x1_re, y1_re, g11_re, 1);
    x1_re = svmls_lane_f32(x1_re, y1_im, g11_im, 1);
    x1_im = svmla_lane_f32(x1_im, y1_re, g11_im, 1);
    x1_im = svmla_lane_f32(x1_im, y1_im, g11_re, 1);

    // G02[1]
    x0_re = svmla_lane_f32(x0_re, y2_re, g02_re, 1);
    x0_re = svmls_lane_f32(x0_re, y2_im, g02_im, 1);
    x0_im = svmla_lane_f32(x0_im, y2_re, g02_im, 1);
    x0_im = svmla_lane_f32(x0_im, y2_im, g02_re, 1);

    // G12[1]
    x1_re = svmla_lane_f32(x1_re, y2_re, g12_re, 1);
    x1_re = svmls_lane_f32(x1_re, y2_im, g12_im, 1);
    x1_im = svmla_lane_f32(x1_im, y2_re, g12_im, 1);
    x1_im = svmla_lane_f32(x1_im, y2_im, g12_re, 1);

    // G03[1]
    x0_re = svmla_lane_f32(x0_re, y3_re, g03_re, 1);
    x0_re = svmls_lane_f32(x0_re, y3_im, g03_im, 1);
    x0_im = svmla_lane_f32(x0_im, y3_re, g03_im, 1);
    x0_im = svmla_lane_f32(x0_im, y3_im, g03_re, 1);

    // Store x0
    x0_re = svmul_n_f32_x(pg8, x0_re, x_mult);
    x0_im = svmul_n_f32_x(pg8, x0_im, x_mult);
    x0_bis = armral_convert_f32_i16(x0_re, x0_im, pg8);
    svst1_s16(pg8, x0_ptr + 16, x0_bis);

    // G13[1]
    x1_re = svmla_lane_f32(x1_re, y3_re, g13_re, 1);
    x1_re = svmls_lane_f32(x1_re, y3_im, g13_im, 1);
    x1_im = svmla_lane_f32(x1_im, y3_re, g13_im, 1);
    x1_im = svmla_lane_f32(x1_im, y3_im, g13_re, 1);

    // Store x1
    x1_re = svmul_n_f32_x(pg8, x1_re, x_mult);
    x1_im = svmul_n_f32_x(pg8, x1_im, x_mult);
    x1_bis = armral_convert_f32_i16(x1_re, x1_im, pg8);
    svst1_s16(pg8, x1_ptr + 16, x1_bis);

    // G20[1]
    x2_re = svmul_lane_f32(y0_re, g20_re, 1);
    x2_re = svmls_lane_f32(x2_re, y0_im, g20_im, 1);
    x2_im = svmul_lane_f32(y0_re, g20_im, 1);
    x2_im = svmla_lane_f32(x2_im, y0_im, g20_re, 1);

    // G30[1]
    x3_re = svmul_lane_f32(y0_re, g30_re, 1);
    x3_re = svmls_lane_f32(x3_re, y0_im, g30_im, 1);
    x3_im = svmul_lane_f32(y0_re, g30_im, 1);
    x3_im = svmla_lane_f32(x3_im, y0_im, g30_re, 1);

    // G21[1]
    x2_re = svmla_lane_f32(x2_re, y1_re, g21_re, 1);
    x2_re = svmls_lane_f32(x2_re, y1_im, g21_im, 1);
    x2_im = svmla_lane_f32(x2_im, y1_re, g21_im, 1);
    x2_im = svmla_lane_f32(x2_im, y1_im, g21_re, 1);

    // G31[1]
    x3_re = svmla_lane_f32(x3_re, y1_re, g31_re, 1);
    x3_re = svmls_lane_f32(x3_re, y1_im, g31_im, 1);
    x3_im = svmla_lane_f32(x3_im, y1_re, g31_im, 1);
    x3_im = svmla_lane_f32(x3_im, y1_im, g31_re, 1);

    // G22[1]
    x2_re = svmla_lane_f32(x2_re, y2_re, g22_re, 1);
    x2_re = svmls_lane_f32(x2_re, y2_im, g22_im, 1);
    x2_im = svmla_lane_f32(x2_im, y2_re, g22_im, 1);
    x2_im = svmla_lane_f32(x2_im, y2_im, g22_re, 1);

    // G32[1]
    x3_re = svmla_lane_f32(x3_re, y2_re, g32_re, 1);
    x3_re = svmls_lane_f32(x3_re, y2_im, g32_im, 1);
    x3_im = svmla_lane_f32(x3_im, y2_re, g32_im, 1);
    x3_im = svmla_lane_f32(x3_im, y2_im, g32_re, 1);

    // G23[1]
    x2_re = svmla_lane_f32(x2_re, y3_re, g23_re, 1);
    x2_re = svmls_lane_f32(x2_re, y3_im, g23_im, 1);
    x2_im = svmla_lane_f32(x2_im, y3_re, g23_im, 1);
    x2_im = svmla_lane_f32(x2_im, y3_im, g23_re, 1);

    // Store x2
    x2_re = svmul_n_f32_x(pg8, x2_re, x_mult);
    x2_im = svmul_n_f32_x(pg8, x2_im, x_mult);
    x2_bis = armral_convert_f32_i16(x2_re, x2_im, pg8);
    svst1_s16(pg8, x2_ptr + 16, x2_bis);

    // G33[1]
    x3_re = svmla_lane_f32(x3_re, y3_re, g33_re, 1);
    x3_re = svmls_lane_f32(x3_re, y3_im, g33_im, 1);
    x3_im = svmla_lane_f32(x3_im, y3_re, g33_im, 1);
    x3_im = svmla_lane_f32(x3_im, y3_im, g33_re, 1);

    // Store x3
    x3_re = svmul_n_f32_x(pg8, x3_re, x_mult);
    x3_im = svmul_n_f32_x(pg8, x3_im, x_mult);
    x3_bis = armral_convert_f32_i16(x3_re, x3_im, pg8);
    svst1_s16(pg8, x3_ptr + 16, x3_bis);

    y0_ptr = y0_ptr + 24;
    y1_ptr = y1_ptr + 24;
    y2_ptr = y2_ptr + 24;
    y3_ptr = y3_ptr + 24;
    x0_ptr = x0_ptr + 24;
    x1_ptr = x1_ptr + 24;
    x2_ptr = x2_ptr + 24;
    x3_ptr = x3_ptr + 24;
    p_g_real += 2;
    p_g_imag += 2;
  }

#else
  const armral_fixed_point_index num_fract_bits_y[] = {
      p_y_num_fract_bits[0], p_y_num_fract_bits[1], p_y_num_fract_bits[2],
      p_y_num_fract_bits[3]};

  // G matrix valid for 6 inputs
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    int16x8_t y0_quadw = vld1q_s16(y0_ptr);
    int16x8_t y1_quadw = vld1q_s16(y1_ptr);
    float32x4_t y0_re;
    float32x4_t y0_im;
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re, &y0_im);
    float32x4_t y1_re;
    float32x4_t y1_im;
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re, &y1_im);

    // Load G00[0] G00[1]
    float32x2_t g00_real = vld1_f32(p_g_real); // 32x2
    float32x2_t g00_imag = vld1_f32(p_g_imag);

    float32x4_t x0_re = vmulq_lane_f32(y0_re, g00_real, 0);
    x0_re = vfmsq_lane_f32(x0_re, y0_im, g00_imag, 0);
    float32x4_t x0_im = vmulq_lane_f32(y0_im, g00_real, 0);
    x0_im = vfmaq_lane_f32(x0_im, y0_re, g00_imag, 0);

    // Load G01[0] G01[1]
    int16x8_t y2_quadw = vld1q_s16(y2_ptr);
    int16x8_t y3_quadw = vld1q_s16(y3_ptr);
    float32x4_t y2_re;
    float32x4_t y2_im;
    armral_convert_i16_f32_x4(num_fract_bits_y[2], y2_quadw, &y2_re, &y2_im);
    float32x4_t y3_re;
    float32x4_t y3_im;
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y3_quadw, &y3_re, &y3_im);
    float32x2_t g01_real = vld1_f32(p_g_real + p_gstride);
    float32x2_t g01_imag = vld1_f32(p_g_imag + p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y1_re, g01_real, 0);
    x0_re = vfmsq_lane_f32(x0_re, y1_im, g01_imag, 0);
    x0_im = vfmaq_lane_f32(x0_im, y1_im, g01_real, 0);
    x0_im = vfmaq_lane_f32(x0_im, y1_re, g01_imag, 0);

    // Load G02[0] G02[1]
    float32x2_t g02_real = vld1_f32(p_g_real + 2 * p_gstride);
    float32x2_t g02_imag = vld1_f32(p_g_imag + 2 * p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y2_re, g02_real, 0);
    x0_re = vfmsq_lane_f32(x0_re, y2_im, g02_imag, 0);
    x0_im = vfmaq_lane_f32(x0_im, y2_im, g02_real, 0);
    x0_im = vfmaq_lane_f32(x0_im, y2_re, g02_imag, 0);

    // Load G03[0] G03[1]
    float32x2_t g03_real = vld1_f32(p_g_real + 3 * p_gstride);
    float32x2_t g03_imag = vld1_f32(p_g_imag + 3 * p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y3_re, g03_real, 0);
    x0_re = vfmsq_lane_f32(x0_re, y3_im, g03_imag, 0);
    x0_im = vfmaq_lane_f32(x0_im, y3_im, g03_real, 0);
    x0_im = vfmaq_lane_f32(x0_im, y3_re, g03_imag, 0);

    // Load G10[0] G10[1]
    float32x2_t g10_real = vld1_f32(p_g_real + 4 * p_gstride);
    float32x2_t g10_imag = vld1_f32(p_g_imag + 4 * p_gstride);

    float32x4_t x1_re = vmulq_lane_f32(y0_re, g10_real, 0);
    x1_re = vfmsq_lane_f32(x1_re, y0_im, g10_imag, 0);
    float32x4_t x1_im = vmulq_lane_f32(y0_im, g10_real, 0);
    x1_im = vfmaq_lane_f32(x1_im, y0_re, g10_imag, 0);

    // Load G11[0] G11[1]
    float32x2_t g11_real = vld1_f32(p_g_real + 5 * p_gstride);
    float32x2_t g11_imag = vld1_f32(p_g_imag + 5 * p_gstride);

    x1_re = vfmaq_lane_f32(x1_re, y1_re, g11_real, 0);
    x1_re = vfmsq_lane_f32(x1_re, y1_im, g11_imag, 0);
    x1_im = vfmaq_lane_f32(x1_im, y1_im, g11_real, 0);
    x1_im = vfmaq_lane_f32(x1_im, y1_re, g11_imag, 0);

    // Load G12[0] G12[1]
    float32x2_t g12_real = vld1_f32(p_g_real + 6 * p_gstride);
    float32x2_t g12_imag = vld1_f32(p_g_imag + 6 * p_gstride);

    x1_re = vfmaq_lane_f32(x1_re, y2_re, g12_real, 0);
    x1_re = vfmsq_lane_f32(x1_re, y2_im, g12_imag, 0);
    x1_im = vfmaq_lane_f32(x1_im, y2_im, g12_real, 0);
    x1_im = vfmaq_lane_f32(x1_im, y2_re, g12_imag, 0);

    // Load G13[0] G13[1]
    float32x2_t g13_real = vld1_f32(p_g_real + 7 * p_gstride);
    float32x2_t g13_imag = vld1_f32(p_g_imag + 7 * p_gstride);

    x1_re = vfmaq_lane_f32(x1_re, y3_re, g13_real, 0);
    x1_re = vfmsq_lane_f32(x1_re, y3_im, g13_imag, 0);
    x1_im = vfmaq_lane_f32(x1_im, y3_im, g13_real, 0);
    x1_im = vfmaq_lane_f32(x1_im, y3_re, g13_imag, 0);

    int16x8_t x0_bis;
    int16x8_t x1_bis;
    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im,
                              &x0_bis, &x1_bis);
    vst1q_s16(x0_ptr, x0_bis);
    vst1q_s16(x1_ptr, x1_bis);

    // Load G20[0] G20[1]
    float32x2_t g20_real = vld1_f32(p_g_real + 8 * p_gstride); // 32x2
    float32x2_t g20_imag = vld1_f32(p_g_imag + 8 * p_gstride);

    float32x4_t x2_re = vmulq_lane_f32(y0_re, g20_real, 0);
    x2_re = vfmsq_lane_f32(x2_re, y0_im, g20_imag, 0);
    float32x4_t x2_im = vmulq_lane_f32(y0_im, g20_real, 0);
    x2_im = vfmaq_lane_f32(x2_im, y0_re, g20_imag, 0);

    // Load G21[0] G21[1]
    float32x2_t g21_real = vld1_f32(p_g_real + 9 * p_gstride);
    float32x2_t g21_imag = vld1_f32(p_g_imag + 9 * p_gstride);

    x2_re = vfmaq_lane_f32(x2_re, y1_re, g21_real, 0);
    x2_re = vfmsq_lane_f32(x2_re, y1_im, g21_imag, 0);
    x2_im = vfmaq_lane_f32(x2_im, y1_im, g21_real, 0);
    x2_im = vfmaq_lane_f32(x2_im, y1_re, g21_imag, 0);

    // Load G22[0] G22[1]
    float32x2_t g22_real = vld1_f32(p_g_real + 10 * p_gstride);
    float32x2_t g22_imag = vld1_f32(p_g_imag + 10 * p_gstride);

    x2_re = vfmaq_lane_f32(x2_re, y2_re, g22_real, 0);
    x2_re = vfmsq_lane_f32(x2_re, y2_im, g22_imag, 0);
    x2_im = vfmaq_lane_f32(x2_im, y2_im, g22_real, 0);
    x2_im = vfmaq_lane_f32(x2_im, y2_re, g22_imag, 0);

    // Load G23[0] G23[1]
    float32x2_t g23_real = vld1_f32(p_g_real + 11 * p_gstride);
    float32x2_t g23_imag = vld1_f32(p_g_imag + 11 * p_gstride);

    x2_re = vfmaq_lane_f32(x2_re, y3_re, g23_real, 0);
    x2_re = vfmsq_lane_f32(x2_re, y3_im, g23_imag, 0);
    x2_im = vfmaq_lane_f32(x2_im, y3_im, g23_real, 0);
    x2_im = vfmaq_lane_f32(x2_im, y3_re, g23_imag, 0);

    // Load G30[0] G30[1]
    float32x2_t g30_real = vld1_f32(p_g_real + 12 * p_gstride); /*32x2*/
    float32x2_t g30_imag = vld1_f32(p_g_imag + 12 * p_gstride);

    float32x4_t x3_re = vmulq_lane_f32(y0_re, g30_real, 0);
    x3_re = vfmsq_lane_f32(x3_re, y0_im, g30_imag, 0);
    float32x4_t x3_im = vmulq_lane_f32(y0_im, g30_real, 0);
    x3_im = vfmaq_lane_f32(x3_im, y0_re, g30_imag, 0);

    // Load G31[0] G31[1]
    float32x2_t g31_real = vld1_f32(p_g_real + 13 * p_gstride);
    float32x2_t g31_imag = vld1_f32(p_g_imag + 13 * p_gstride);

    x3_re = vfmaq_lane_f32(x3_re, y1_re, g31_real, 0);
    x3_re = vfmsq_lane_f32(x3_re, y1_im, g31_imag, 0);
    x3_im = vfmaq_lane_f32(x3_im, y1_im, g31_real, 0);
    x3_im = vfmaq_lane_f32(x3_im, y1_re, g31_imag, 0);

    // Load G32[0] G32[1]
    float32x2_t g32_real = vld1_f32(p_g_real + 14 * p_gstride);
    float32x2_t g32_imag = vld1_f32(p_g_imag + 14 * p_gstride);

    x3_re = vfmaq_lane_f32(x3_re, y2_re, g32_real, 0);
    x3_re = vfmsq_lane_f32(x3_re, y2_im, g32_imag, 0);
    x3_im = vfmaq_lane_f32(x3_im, y2_im, g32_real, 0);
    x3_im = vfmaq_lane_f32(x3_im, y2_re, g32_imag, 0);

    // Load G33[0] G33[1]
    float32x2_t g33_real = vld1_f32(p_g_real + 15 * p_gstride);
    float32x2_t g33_imag = vld1_f32(p_g_imag + 15 * p_gstride);

    x3_re = vfmaq_lane_f32(x3_re, y3_re, g33_real, 0);
    x3_re = vfmsq_lane_f32(x3_re, y3_im, g33_imag, 0);
    x3_im = vfmaq_lane_f32(x3_im, y3_im, g33_real, 0);
    x3_im = vfmaq_lane_f32(x3_im, y3_re, g33_imag, 0);

    int16x8_t x2_bis;
    int16x8_t x3_bis;
    armral_convert_f32_i16_x4(num_fract_bits_x, x2_re, x2_im, x3_re, x3_im,
                              &x2_bis, &x3_bis);

    vst1q_s16(x2_ptr, x2_bis);
    vst1q_s16(x3_ptr, x3_bis);

    // Load next 4
    y0_quadw = vld1q_s16(y0_ptr + 8);
    y1_quadw = vld1q_s16(y1_ptr + 8);
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re, &y0_im);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re, &y1_im);

    float32x4_t g00_temp0 = shuf_f32_0011(g00_real);
    float32x4_t g00_temp1 = shuf_f32_0011(g00_imag);

    y2_quadw = vld1q_s16(y2_ptr + 8);
    y3_quadw = vld1q_s16(y3_ptr + 8);
    armral_convert_i16_f32_x4(num_fract_bits_y[2], y2_quadw, &y2_re, &y2_im);
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y3_quadw, &y3_re, &y3_im);
    x0_re = vmulq_f32(y0_re, g00_temp0);
    x0_re = vfmsq_f32(x0_re, y0_im, g00_temp1);
    x0_im = vmulq_f32(y0_im, g00_temp0);
    x0_im = vfmaq_f32(x0_im, y0_re, g00_temp1);

    float32x4_t g01_temp0 = shuf_f32_0011(g01_real);
    float32x4_t g01_temp1 = shuf_f32_0011(g01_imag);

    x0_re = vfmaq_f32(x0_re, y1_re, g01_temp0);
    x0_re = vfmsq_f32(x0_re, y1_im, g01_temp1);
    x0_im = vfmaq_f32(x0_im, y1_im, g01_temp0);
    x0_im = vfmaq_f32(x0_im, y1_re, g01_temp1);

    float32x4_t g02_temp0 = shuf_f32_0011(g02_real);
    float32x4_t g02_temp1 = shuf_f32_0011(g02_imag);

    x0_re = vfmaq_f32(x0_re, y2_re, g02_temp0);
    x0_re = vfmsq_f32(x0_re, y2_im, g02_temp1);
    x0_im = vfmaq_f32(x0_im, y2_im, g02_temp0);
    x0_im = vfmaq_f32(x0_im, y2_re, g02_temp1);

    float32x4_t g03_temp0 = shuf_f32_0011(g03_real);
    float32x4_t g03_temp1 = shuf_f32_0011(g03_imag);

    x0_re = vfmaq_f32(x0_re, y3_re, g03_temp0);
    x0_re = vfmsq_f32(x0_re, y3_im, g03_temp1);
    x0_im = vfmaq_f32(x0_im, y3_im, g03_temp0);
    x0_im = vfmaq_f32(x0_im, y3_re, g03_temp1);

    float32x4_t g10_temp0 = shuf_f32_0011(g10_real);
    float32x4_t g10_temp1 = shuf_f32_0011(g10_imag);

    x1_re = vmulq_f32(y0_re, g10_temp0);
    x1_re = vfmsq_f32(x1_re, y0_im, g10_temp1);
    x1_im = vmulq_f32(y0_im, g10_temp0);
    x1_im = vfmaq_f32(x1_im, y0_re, g10_temp1);

    float32x4_t g11_temp0 = shuf_f32_0011(g11_real);
    float32x4_t g11_temp1 = shuf_f32_0011(g11_imag);

    x1_re = vfmaq_f32(x1_re, y1_re, g11_temp0);
    x1_re = vfmsq_f32(x1_re, y1_im, g11_temp1);
    x1_im = vfmaq_f32(x1_im, y1_im, g11_temp0);
    x1_im = vfmaq_f32(x1_im, y1_re, g11_temp1);

    float32x4_t g12_temp0 = shuf_f32_0011(g12_real);
    float32x4_t g12_temp1 = shuf_f32_0011(g12_imag);

    x1_re = vfmaq_f32(x1_re, y2_re, g12_temp0);
    x1_re = vfmsq_f32(x1_re, y2_im, g12_temp1);
    x1_im = vfmaq_f32(x1_im, y2_im, g12_temp0);
    x1_im = vfmaq_f32(x1_im, y2_re, g12_temp1);

    float32x4_t g13_temp0 = shuf_f32_0011(g13_real);
    float32x4_t g13_temp1 = shuf_f32_0011(g13_imag);

    x1_re = vfmaq_f32(x1_re, y3_re, g13_temp0);
    x1_re = vfmsq_f32(x1_re, y3_im, g13_temp1);
    x1_im = vfmaq_f32(x1_im, y3_im, g13_temp0);
    x1_im = vfmaq_f32(x1_im, y3_re, g13_temp1);

    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im,
                              &x0_bis, &x1_bis);
    vst1q_s16(x0_ptr + 8, x0_bis);
    vst1q_s16(x1_ptr + 8, x1_bis);

    float32x4_t g20_temp0 = shuf_f32_0011(g20_real);
    float32x4_t g20_temp1 = shuf_f32_0011(g20_imag);

    x2_re = vmulq_f32(y0_re, g20_temp0);
    x2_re = vfmsq_f32(x2_re, y0_im, g20_temp1);
    x2_im = vmulq_f32(y0_im, g20_temp0);
    x2_im = vfmaq_f32(x2_im, y0_re, g20_temp1);

    float32x4_t g21_temp0 = shuf_f32_0011(g21_real);
    float32x4_t g21_temp1 = shuf_f32_0011(g21_imag);

    x2_re = vfmaq_f32(x2_re, y1_re, g21_temp0);
    x2_re = vfmsq_f32(x2_re, y1_im, g21_temp1);
    x2_im = vfmaq_f32(x2_im, y1_im, g21_temp0);
    x2_im = vfmaq_f32(x2_im, y1_re, g21_temp1);

    float32x4_t g22_temp0 = shuf_f32_0011(g22_real);
    float32x4_t g22_temp1 = shuf_f32_0011(g22_imag);

    x2_re = vfmaq_f32(x2_re, y2_re, g22_temp0);
    x2_re = vfmsq_f32(x2_re, y2_im, g22_temp1);
    x2_im = vfmaq_f32(x2_im, y2_im, g22_temp0);
    x2_im = vfmaq_f32(x2_im, y2_re, g22_temp1);

    float32x4_t g23_temp0 = shuf_f32_0011(g23_real);
    float32x4_t g23_temp1 = shuf_f32_0011(g23_imag);

    x2_re = vfmaq_f32(x2_re, y3_re, g23_temp0);
    x2_re = vfmsq_f32(x2_re, y3_im, g23_temp1);
    x2_im = vfmaq_f32(x2_im, y3_im, g23_temp0);
    x2_im = vfmaq_f32(x2_im, y3_re, g23_temp1);

    float32x4_t g30_temp0 = shuf_f32_0011(g30_real);
    float32x4_t g30_temp1 = shuf_f32_0011(g30_imag);

    x3_re = vmulq_f32(y0_re, g30_temp0);
    x3_re = vfmsq_f32(x3_re, y0_im, g30_temp1);
    x3_im = vmulq_f32(y0_im, g30_temp0);
    x3_im = vfmaq_f32(x3_im, y0_re, g30_temp1);

    float32x4_t g31_temp0 = shuf_f32_0011(g31_real);
    float32x4_t g31_temp1 = shuf_f32_0011(g31_imag);

    x3_re = vfmaq_f32(x3_re, y1_re, g31_temp0);
    x3_re = vfmsq_f32(x3_re, y1_im, g31_temp1);
    x3_im = vfmaq_f32(x3_im, y1_im, g31_temp0);
    x3_im = vfmaq_f32(x3_im, y1_re, g31_temp1);

    float32x4_t g32_temp0 = shuf_f32_0011(g32_real);
    float32x4_t g32_temp1 = shuf_f32_0011(g32_imag);

    x3_re = vfmaq_f32(x3_re, y2_re, g32_temp0);
    x3_re = vfmsq_f32(x3_re, y2_im, g32_temp1);
    x3_im = vfmaq_f32(x3_im, y2_im, g32_temp0);
    x3_im = vfmaq_f32(x3_im, y2_re, g32_temp1);

    float32x4_t g33_temp0 = shuf_f32_0011(g33_real);
    float32x4_t g33_temp1 = shuf_f32_0011(g33_imag);

    x3_re = vfmaq_f32(x3_re, y3_re, g33_temp0);
    x3_re = vfmsq_f32(x3_re, y3_im, g33_temp1);
    x3_im = vfmaq_f32(x3_im, y3_im, g33_temp0);
    x3_im = vfmaq_f32(x3_im, y3_re, g33_temp1);

    armral_convert_f32_i16_x4(num_fract_bits_x, x2_re, x2_im, x3_re, x3_im,
                              &x2_bis, &x3_bis);
    vst1q_s16(x2_ptr + 8, x2_bis);
    vst1q_s16(x3_ptr + 8, x3_bis);

    // Load next 4
    y0_quadw = vld1q_s16(y0_ptr + 16);
    y1_quadw = vld1q_s16(y1_ptr + 16);
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y0_quadw, &y0_re, &y0_im);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y1_quadw, &y1_re, &y1_im);

    x0_re = vmulq_lane_f32(y0_re, g00_real, 1);
    x0_re = vfmsq_lane_f32(x0_re, y0_im, g00_imag, 1);
    x0_im = vmulq_lane_f32(y0_im, g00_real, 1);
    x0_im = vfmaq_lane_f32(x0_im, y0_re, g00_imag, 1);

    y2_quadw = vld1q_s16(y2_ptr + 16);
    y3_quadw = vld1q_s16(y3_ptr + 16);
    armral_convert_i16_f32_x4(num_fract_bits_y[2], y2_quadw, &y2_re, &y2_im);
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y3_quadw, &y3_re, &y3_im);

    x0_re = vfmaq_lane_f32(x0_re, y1_re, g01_real, 1);
    x0_re = vfmsq_lane_f32(x0_re, y1_im, g01_imag, 1);
    x0_im = vfmaq_lane_f32(x0_im, y1_im, g01_real, 1);
    x0_im = vfmaq_lane_f32(x0_im, y1_re, g01_imag, 1);

    x0_re = vfmaq_lane_f32(x0_re, y2_re, g02_real, 1);
    x0_re = vfmsq_lane_f32(x0_re, y2_im, g02_imag, 1);
    x0_im = vfmaq_lane_f32(x0_im, y2_im, g02_real, 1);
    x0_im = vfmaq_lane_f32(x0_im, y2_re, g02_imag, 1);

    x0_re = vfmaq_lane_f32(x0_re, y3_re, g03_real, 1);
    x0_re = vfmsq_lane_f32(x0_re, y3_im, g03_imag, 1);
    x0_im = vfmaq_lane_f32(x0_im, y3_im, g03_real, 1);
    x0_im = vfmaq_lane_f32(x0_im, y3_re, g03_imag, 1);

    x1_re = vmulq_lane_f32(y0_re, g10_real, 1);
    x1_re = vfmsq_lane_f32(x1_re, y0_im, g10_imag, 1);
    x1_im = vmulq_lane_f32(y0_im, g10_real, 1);
    x1_im = vfmaq_lane_f32(x1_im, y0_re, g10_imag, 1);

    x1_re = vfmaq_lane_f32(x1_re, y1_re, g11_real, 1);
    x1_re = vfmsq_lane_f32(x1_re, y1_im, g11_imag, 1);
    x1_im = vfmaq_lane_f32(x1_im, y1_im, g11_real, 1);
    x1_im = vfmaq_lane_f32(x1_im, y1_re, g11_imag, 1);

    x1_re = vfmaq_lane_f32(x1_re, y2_re, g12_real, 1);
    x1_re = vfmsq_lane_f32(x1_re, y2_im, g12_imag, 1);
    x1_im = vfmaq_lane_f32(x1_im, y2_im, g12_real, 1);
    x1_im = vfmaq_lane_f32(x1_im, y2_re, g12_imag, 1);

    x1_re = vfmaq_lane_f32(x1_re, y3_re, g13_real, 1);
    x1_re = vfmsq_lane_f32(x1_re, y3_im, g13_imag, 1);
    x1_im = vfmaq_lane_f32(x1_im, y3_im, g13_real, 1);
    x1_im = vfmaq_lane_f32(x1_im, y3_re, g13_imag, 1);

    armral_convert_f32_i16_x4(num_fract_bits_x, x0_re, x0_im, x1_re, x1_im,
                              &x0_bis, &x1_bis);

    vst1q_s16(x0_ptr + 16, x0_bis);
    vst1q_s16(x1_ptr + 16, x1_bis);
    x0_ptr = x0_ptr + 24;
    x1_ptr = x1_ptr + 24;

    x2_re = vmulq_lane_f32(y0_re, g20_real, 1);
    x2_re = vfmsq_lane_f32(x2_re, y0_im, g20_imag, 1);
    x2_im = vmulq_lane_f32(y0_im, g20_real, 1);
    x2_im = vfmaq_lane_f32(x2_im, y0_re, g20_imag, 1);

    x2_re = vfmaq_lane_f32(x2_re, y1_re, g21_real, 1);
    x2_re = vfmsq_lane_f32(x2_re, y1_im, g21_imag, 1);
    x2_im = vfmaq_lane_f32(x2_im, y1_im, g21_real, 1);
    x2_im = vfmaq_lane_f32(x2_im, y1_re, g21_imag, 1);

    x2_re = vfmaq_lane_f32(x2_re, y2_re, g22_real, 1);
    x2_re = vfmsq_lane_f32(x2_re, y2_im, g22_imag, 1);
    x2_im = vfmaq_lane_f32(x2_im, y2_im, g22_real, 1);
    x2_im = vfmaq_lane_f32(x2_im, y2_re, g22_imag, 1);

    x2_re = vfmaq_lane_f32(x2_re, y3_re, g23_real, 1);
    x2_re = vfmsq_lane_f32(x2_re, y3_im, g23_imag, 1);
    x2_im = vfmaq_lane_f32(x2_im, y3_im, g23_real, 1);
    x2_im = vfmaq_lane_f32(x2_im, y3_re, g23_imag, 1);

    x3_re = vmulq_lane_f32(y0_re, g30_real, 1);
    x3_re = vfmsq_lane_f32(x3_re, y0_im, g30_imag, 1);
    x3_im = vmulq_lane_f32(y0_im, g30_real, 1);
    x3_im = vfmaq_lane_f32(x3_im, y0_re, g30_imag, 1);

    x3_re = vfmaq_lane_f32(x3_re, y1_re, g31_real, 1);
    x3_re = vfmsq_lane_f32(x3_re, y1_im, g31_imag, 1);
    x3_im = vfmaq_lane_f32(x3_im, y1_im, g31_real, 1);
    x3_im = vfmaq_lane_f32(x3_im, y1_re, g31_imag, 1);

    x3_re = vfmaq_lane_f32(x3_re, y2_re, g32_real, 1);
    x3_re = vfmsq_lane_f32(x3_re, y2_im, g32_imag, 1);
    x3_im = vfmaq_lane_f32(x3_im, y2_im, g32_real, 1);
    x3_im = vfmaq_lane_f32(x3_im, y2_re, g32_imag, 1);

    x3_re = vfmaq_lane_f32(x3_re, y3_re, g33_real, 1);
    x3_re = vfmsq_lane_f32(x3_re, y3_im, g33_imag, 1);
    x3_im = vfmaq_lane_f32(x3_im, y3_im, g33_real, 1);
    x3_im = vfmaq_lane_f32(x3_im, y3_re, g33_imag, 1);

    armral_convert_f32_i16_x4(num_fract_bits_x, x2_re, x2_im, x3_re, x3_im,
                              &x2_bis, &x3_bis);

    vst1q_s16(x2_ptr + 16, x2_bis);
    vst1q_s16(x3_ptr + 16, x3_bis);
    x2_ptr = x2_ptr + 24;
    x3_ptr = x3_ptr + 24;
    p_g_real = p_g_real + 2;
    p_g_imag = p_g_imag + 2;
    y0_ptr = y0_ptr + 24;
    y1_ptr = y1_ptr + 24;
    y2_ptr = y2_ptr + 24;
    y3_ptr = y3_ptr + 24;
  }
#endif
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_1x4_6sc_f32(
    const uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    const uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    const uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    const armral_fixed_point_index num_fract_bits_x) {

  const int16_t *y0_ptr = (const int16_t *)p_y;
  const int16_t *y1_ptr = (const int16_t *)p_y + p_ystride;
  const int16_t *y2_ptr = (const int16_t *)p_y + 2 * p_ystride;
  const int16_t *y3_ptr = (const int16_t *)p_y + 3 * p_ystride;

  int16_t *x0_ptr = (int16_t *)p_x;
  const armral_fixed_point_index num_fract_bits_y[] = {
      p_y_num_fract_bits[0], p_y_num_fract_bits[1], p_y_num_fract_bits[2],
      p_y_num_fract_bits[3]};

  // G matrix valid for 6 inputs
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    int16x8_t y_quadw0 = vld1q_s16(y0_ptr);
    int16x8_t y_quadw1 = vld1q_s16(y1_ptr);
    float32x4_t y_re[4];
    float32x4_t y_im[4];
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y_quadw0, &y_re[0],
                              &y_im[0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y_quadw1, &y_re[1],
                              &y_im[1]);

    // Load G00[0] G00[1]
    float32x2_t g_real[4];          // G00 G01 G02 G03
    g_real[0] = vld1_f32(p_g_real); // 32x2
    float32x2_t g_imag[4];
    g_imag[0] = vld1_f32(p_g_imag);

    float32x4_t x0_re = vmulq_lane_f32(y_re[0], g_real[0], 0);
    x0_re = vfmsq_lane_f32(x0_re, y_im[0], g_imag[0], 0);
    float32x4_t x0_im = vmulq_lane_f32(y_im[0], g_real[0], 0);
    x0_im = vfmaq_lane_f32(x0_im, y_re[0], g_imag[0], 0);

    // Load G01[0] G01[1]
    int16x8_t y_quadw2 = vld1q_s16(y2_ptr);
    int16x8_t y_quadw3 = vld1q_s16(y3_ptr);
    armral_convert_i16_f32_x4(num_fract_bits_y[2], y_quadw2, &y_re[2],
                              &y_im[2]);
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y_quadw3, &y_re[3],
                              &y_im[3]);
    g_real[1] = vld1_f32(p_g_real + p_gstride);
    g_imag[1] = vld1_f32(p_g_imag + p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y_re[1], g_real[1], 0);
    x0_re = vfmsq_lane_f32(x0_re, y_im[1], g_imag[1], 0);
    x0_im = vfmaq_lane_f32(x0_im, y_im[1], g_real[1], 0);
    x0_im = vfmaq_lane_f32(x0_im, y_re[1], g_imag[1], 0);

    // Load G02[0] G02[1]
    g_real[2] = vld1_f32(p_g_real + 2 * p_gstride);
    g_imag[2] = vld1_f32(p_g_imag + 2 * p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y_re[2], g_real[2], 0);
    x0_re = vfmsq_lane_f32(x0_re, y_im[2], g_imag[2], 0);
    x0_im = vfmaq_lane_f32(x0_im, y_im[2], g_real[2], 0);
    x0_im = vfmaq_lane_f32(x0_im, y_re[2], g_imag[2], 0);

    // Load G03[0] G03[1]
    g_real[3] = vld1_f32(p_g_real + 3 * p_gstride);
    g_imag[3] = vld1_f32(p_g_imag + 3 * p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y_re[3], g_real[3], 0);
    x0_re = vfmsq_lane_f32(x0_re, y_im[3], g_imag[3], 0);
    x0_im = vfmaq_lane_f32(x0_im, y_im[3], g_real[3], 0);
    x0_im = vfmaq_lane_f32(x0_im, y_re[3], g_imag[3], 0);

    int16x8_t x0_bis;
    armral_convert_f32_i16_x4_2(num_fract_bits_x, x0_re, x0_im, &x0_bis);
    vst1q_s16(x0_ptr, x0_bis);

    // Load next 4
    y_quadw0 = vld1q_s16(y0_ptr + 8);
    y_quadw1 = vld1q_s16(y1_ptr + 8);

    armral_convert_i16_f32_x4(num_fract_bits_y[0], y_quadw0, &y_re[0],
                              &y_im[0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y_quadw1, &y_re[1],
                              &y_im[1]);

    float32x4_t g00_temp0 = shuf_f32_0011(g_real[0]);
    float32x4_t g00_temp1 = shuf_f32_0011(g_imag[0]);

    y_quadw2 = vld1q_s16(y2_ptr + 8);
    y_quadw3 = vld1q_s16(y3_ptr + 8);

    armral_convert_i16_f32_x4(num_fract_bits_y[2], y_quadw2, &y_re[2],
                              &y_im[2]);
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y_quadw3, &y_re[3],
                              &y_im[3]);

    x0_re = vmulq_f32(y_re[0], g00_temp0);
    x0_re = vfmsq_f32(x0_re, y_im[0], g00_temp1);
    x0_im = vmulq_f32(y_im[0], g00_temp0);
    x0_im = vfmaq_f32(x0_im, y_re[0], g00_temp1);

    float32x4_t g01_temp0 = shuf_f32_0011(g_real[1]);
    float32x4_t g01_temp1 = shuf_f32_0011(g_imag[1]);

    x0_re = vfmaq_f32(x0_re, y_re[1], g01_temp0);
    x0_re = vfmsq_f32(x0_re, y_im[1], g01_temp1);
    x0_im = vfmaq_f32(x0_im, y_im[1], g01_temp0);
    x0_im = vfmaq_f32(x0_im, y_re[1], g01_temp1);

    float32x4_t g02_temp0 = shuf_f32_0011(g_real[2]);
    float32x4_t g02_temp1 = shuf_f32_0011(g_imag[2]);

    x0_re = vfmaq_f32(x0_re, y_re[2], g02_temp0);
    x0_re = vfmsq_f32(x0_re, y_im[2], g02_temp1);
    x0_im = vfmaq_f32(x0_im, y_im[2], g02_temp0);
    x0_im = vfmaq_f32(x0_im, y_re[2], g02_temp1);

    float32x4_t g03_temp0 = shuf_f32_0011(g_real[3]);
    float32x4_t g03_temp1 = shuf_f32_0011(g_imag[3]);

    x0_re = vfmaq_f32(x0_re, y_re[3], g03_temp0);
    x0_re = vfmsq_f32(x0_re, y_im[3], g03_temp1);
    x0_im = vfmaq_f32(x0_im, y_im[3], g03_temp0);
    x0_im = vfmaq_f32(x0_im, y_re[3], g03_temp1);

    armral_convert_f32_i16_x4_2(num_fract_bits_x, x0_re, x0_im, &x0_bis);
    vst1q_s16(x0_ptr + 8, x0_bis);

    // Load next 4
    y_quadw0 = vld1q_s16(y0_ptr + 16);
    y_quadw1 = vld1q_s16(y1_ptr + 16);
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y_quadw0, &y_re[0],
                              &y_im[0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y_quadw1, &y_re[1],
                              &y_im[1]);

    x0_re = vmulq_lane_f32(y_re[0], g_real[0], 1);
    x0_re = vfmsq_lane_f32(x0_re, y_im[0], g_imag[0], 1);

    x0_im = vmulq_lane_f32(y_im[0], g_real[0], 1);
    x0_im = vfmaq_lane_f32(x0_im, y_re[0], g_imag[0], 1);

    y_quadw2 = vld1q_s16(y2_ptr + 16);
    y_quadw3 = vld1q_s16(y3_ptr + 16);

    armral_convert_i16_f32_x4(num_fract_bits_y[2], y_quadw2, &y_re[2],
                              &y_im[2]);
    armral_convert_i16_f32_x4(num_fract_bits_y[3], y_quadw3, &y_re[3],
                              &y_im[3]);

    x0_re = vfmaq_lane_f32(x0_re, y_re[1], g_real[1], 1);
    x0_re = vfmsq_lane_f32(x0_re, y_im[1], g_imag[1], 1);

    x0_im = vfmaq_lane_f32(x0_im, y_im[1], g_real[1], 1);
    x0_im = vfmaq_lane_f32(x0_im, y_re[1], g_imag[1], 1);

    x0_re = vfmaq_lane_f32(x0_re, y_re[2], g_real[2], 1);
    x0_re = vfmsq_lane_f32(x0_re, y_im[2], g_imag[2], 1);

    x0_im = vfmaq_lane_f32(x0_im, y_im[2], g_real[2], 1);
    x0_im = vfmaq_lane_f32(x0_im, y_re[2], g_imag[2], 1);

    x0_re = vfmaq_lane_f32(x0_re, y_re[3], g_real[3], 1);
    x0_re = vfmsq_lane_f32(x0_re, y_im[3], g_imag[3], 1);

    x0_im = vfmaq_lane_f32(x0_im, y_im[3], g_real[3], 1);
    x0_im = vfmaq_lane_f32(x0_im, y_re[3], g_imag[3], 1);
    armral_convert_f32_i16_x4_2(num_fract_bits_x, x0_re, x0_im, &x0_bis);

    vst1q_s16(x0_ptr + 16, x0_bis);
    x0_ptr = x0_ptr + 24;
    p_g_real = p_g_real + 2;
    p_g_imag = p_g_imag + 2;
    y0_ptr = y0_ptr + 24;
    y1_ptr = y1_ptr + 24;
    y2_ptr = y2_ptr + 24;
    y3_ptr = y3_ptr + 24;
  }
  return ARMRAL_SUCCESS;
}

armral_status armral_solve_1x2_6sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    armral_fixed_point_index num_fract_bits_x) {

  const int16_t *y0_ptr = (const int16_t *)p_y;
  const int16_t *y1_ptr = (const int16_t *)p_y + p_ystride;

  int16_t *x0_ptr = (int16_t *)p_x;
  const armral_fixed_point_index num_fract_bits_y[] = {p_y_num_fract_bits[0],
                                                       p_y_num_fract_bits[1]};

  // G matrix valid for 6 inputs
  for (unsigned j = 0; j < num_sub_carrier; j += 12) {
    int16x8_t y_quadw0 = vld1q_s16(y0_ptr);
    int16x8_t y_quadw1 = vld1q_s16(y1_ptr);

    float32x4_t y_re[4];
    float32x4_t y_im[4];
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y_quadw0, &y_re[0],
                              &y_im[0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y_quadw1, &y_re[1],
                              &y_im[1]);

    // Load G00[0] G00[1]
    float32x2_t g_real[2];          // G00 G01 G02 G03
    g_real[0] = vld1_f32(p_g_real); // 32x2
    float32x2_t g_imag[2];
    g_imag[0] = vld1_f32(p_g_imag);

    float32x4_t x0_re = vmulq_lane_f32(y_re[0], g_real[0], 0);
    x0_re = vfmsq_lane_f32(x0_re, y_im[0], g_imag[0], 0);
    float32x4_t x0_im = vmulq_lane_f32(y_im[0], g_real[0], 0);
    x0_im = vfmaq_lane_f32(x0_im, y_re[0], g_imag[0], 0);

    // Load G01[0] G01[1]
    g_real[1] = vld1_f32(p_g_real + p_gstride);
    g_imag[1] = vld1_f32(p_g_imag + p_gstride);

    x0_re = vfmaq_lane_f32(x0_re, y_re[1], g_real[1], 0);
    x0_re = vfmsq_lane_f32(x0_re, y_im[1], g_imag[1], 0);
    x0_im = vfmaq_lane_f32(x0_im, y_im[1], g_real[1], 0);
    x0_im = vfmaq_lane_f32(x0_im, y_re[1], g_imag[1], 0);
    int16x8_t x0_bis;
    armral_convert_f32_i16_x4_2(num_fract_bits_x, x0_re, x0_im, &x0_bis);
    vst1q_s16(x0_ptr, x0_bis);

    // Load next 4
    y_quadw0 = vld1q_s16(y0_ptr + 8);
    y_quadw1 = vld1q_s16(y1_ptr + 8);

    armral_convert_i16_f32_x4(num_fract_bits_y[0], y_quadw0, &y_re[0],
                              &y_im[0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y_quadw1, &y_re[1],
                              &y_im[1]);

    float32x4_t g00_temp0 = shuf_f32_0011(g_real[0]);
    float32x4_t g00_temp1 = shuf_f32_0011(g_imag[0]);

    x0_re = vmulq_f32(y_re[0], g00_temp0);
    x0_re = vfmsq_f32(x0_re, y_im[0], g00_temp1);
    x0_im = vmulq_f32(y_im[0], g00_temp0);
    x0_im = vfmaq_f32(x0_im, y_re[0], g00_temp1);

    float32x4_t g01_temp0 = shuf_f32_0011(g_real[1]);
    float32x4_t g01_temp1 = shuf_f32_0011(g_imag[1]);

    x0_re = vfmaq_f32(x0_re, y_re[1], g01_temp0);
    x0_re = vfmsq_f32(x0_re, y_im[1], g01_temp1);
    x0_im = vfmaq_f32(x0_im, y_im[1], g01_temp0);
    x0_im = vfmaq_f32(x0_im, y_re[1], g01_temp1);

    armral_convert_f32_i16_x4_2(num_fract_bits_x, x0_re, x0_im, &x0_bis);
    vst1q_s16(x0_ptr + 8, x0_bis);

    // Load next 4
    y_quadw0 = vld1q_s16(y0_ptr + 16);
    y_quadw1 = vld1q_s16(y1_ptr + 16);
    armral_convert_i16_f32_x4(num_fract_bits_y[0], y_quadw0, &y_re[0],
                              &y_im[0]);
    armral_convert_i16_f32_x4(num_fract_bits_y[1], y_quadw1, &y_re[1],
                              &y_im[1]);
    x0_re = vmulq_lane_f32(y_re[0], g_real[0], 1);
    x0_re = vfmsq_lane_f32(x0_re, y_im[0], g_imag[0], 1);
    x0_im = vmulq_lane_f32(y_im[0], g_real[0], 1);
    x0_im = vfmaq_lane_f32(x0_im, y_re[0], g_imag[0], 1);

    x0_re = vfmaq_lane_f32(x0_re, y_re[1], g_real[1], 1);
    x0_re = vfmsq_lane_f32(x0_re, y_im[1], g_imag[1], 1);
    x0_im = vfmaq_lane_f32(x0_im, y_im[1], g_real[1], 1);
    x0_im = vfmaq_lane_f32(x0_im, y_re[1], g_imag[1], 1);

    armral_convert_f32_i16_x4_2(num_fract_bits_x, x0_re, x0_im, &x0_bis);

    vst1q_s16(x0_ptr + 16, x0_bis);
    x0_ptr = x0_ptr + 24;
    p_g_real = p_g_real + 2;
    p_g_imag = p_g_imag + 2;
    y0_ptr = y0_ptr + 24;
    y1_ptr = y1_ptr + 24;
  }
  return ARMRAL_SUCCESS;
}
