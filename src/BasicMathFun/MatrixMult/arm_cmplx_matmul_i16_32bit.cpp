/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "cmplx_matmul_i16_32bit.hpp"

armral_status
armral_cmplx_matmul_i16_32bit(const uint16_t m, const uint16_t n,
                              const uint16_t k,
                              const armral_cmplx_int16_t *__restrict p_src_a,
                              const armral_cmplx_int16_t *__restrict p_src_b,
                              armral_cmplx_int16_t *p_dst) {
  return cmplx_matmul_i16_32bit(m, n, k, p_src_a, p_src_b, p_dst);
}
