/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"

#include <assert.h>

#if !(ARMRAL_ARCH_SVE >= 2)
static int16x4_t vld1s_s16(const int16_t *p) {
  // there is no intrinsic for only loading 32-bits into an ACLE vector.
  int16x4_t ret;
  asm("ldr %s0, %1" : "=w"(ret) : "m"(*p));
  return ret;
}
#endif

armral_status
armral_cmplx_mat_vec_mult_i16(const uint16_t m, const uint16_t n,
                              const armral_cmplx_int16_t *restrict p_src_a,
                              const armral_cmplx_int16_t *restrict p_src_x,
                              armral_cmplx_int16_t *p_dst) {
  const int16_t *p_in1 = (const int16_t *)p_src_a;
  const armral_cmplx_int16_t *p_in_a = p_src_a;
  armral_cmplx_int16_t *p_out = p_dst;
  uint16_t num_rows_a = m; // number of rows of input matrix A
  uint16_t num_cols_a = n; // number of columns of input matrix A

  const int16_t *p_in1_b = (const int16_t *)p_src_a;

  // Row loop
  for (uint16_t row_cnt = num_rows_a >> 1; row_cnt > 0;
       --row_cnt, p_out += 2, p_in_a += 2 * num_cols_a) {
    // Output pointer is set to starting address of the row being processed
    armral_cmplx_int16_t *px = p_out;
    armral_cmplx_int16_t *px_b = px + 1;

    // For every row wise process, the pIn2 pointer is set
    // to the starting address of the pSrcX data
    const int16_t *p_in2 = (const int16_t *)p_src_x;

    // Set the variable sum, that acts as accumulator, to zero
    int64_t sum_real1_ext = 0;
    int64_t sum_imag1_ext = 0;

    int64_t sum_real1_b_ext = 0;
    int64_t sum_imag1_b_ext = 0;

    // Initialize the pointer pIn1 to point to the starting address of the
    // column being processed
    p_in1 = (const int16_t *)p_in_a;
    p_in1_b = p_in1 + 2 * num_cols_a;

    int64x2_t acc_r0 = {0};
    int64x2_t acc_i0 = {0};
    int64x2_t acc_r1 = {0};
    int64x2_t acc_i1 = {0};

    // Compute 8 MACs simultaneously
    uint16_t col_cnt = num_cols_a >> 3;

    // Matrix multiplication
    while (col_cnt > 0U) {
      // int16x8x2_t load and de-interleave
      int16x8x2_t a0_v = vld2q_s16(p_in1);
      // Extend to 32bit Real part int32x4x2_t
      int32x4_t a0_v_rextended[2];
      a0_v_rextended[0] = vmovl_low_s16(a0_v.val[0]);
      a0_v_rextended[1] = vmovl_high_s16(a0_v.val[0]);
      // Extend to 32bit Imag part int32x4x2_t
      int32x4_t a0_v_iextended[2];
      a0_v_iextended[0] = vmovl_low_s16(a0_v.val[1]);
      a0_v_iextended[1] = vmovl_high_s16(a0_v.val[1]);

      int16x8x2_t a1_v = vld2q_s16(p_in1_b);
      int32x4_t a1_v_rextended[2];
      a1_v_rextended[0] = vmovl_low_s16(a1_v.val[0]);
      a1_v_rextended[1] = vmovl_high_s16(a1_v.val[0]);
      int32x4_t a1_v_iextended[2];
      a1_v_iextended[0] = vmovl_low_s16(a1_v.val[1]);
      a1_v_iextended[1] = vmovl_high_s16(a1_v.val[1]);

      // Load eight entries of X, splitting real and imaginary components
      int16x8x2_t tmp_x = vld2q_s16(p_in2);
      int32x4_t r_32bit[2] = {0};
      r_32bit[0] = vmovl_low_s16(tmp_x.val[0]);
      r_32bit[1] = vmovl_high_s16(tmp_x.val[0]);
      int32x4_t i_32bit[2] = {0};
      i_32bit[0] = vmovl_low_s16(tmp_x.val[1]);
      i_32bit[1] = vmovl_high_s16(tmp_x.val[1]);

      // First row A * X
      // real * real
      acc_r0 = vmlal_low_s32(acc_r0, a0_v_rextended[0], r_32bit[0]);
      acc_r0 = vmlal_high_s32(acc_r0, a0_v_rextended[0], r_32bit[0]);
      acc_r0 = vmlal_low_s32(acc_r0, a0_v_rextended[1], r_32bit[1]);
      acc_r0 = vmlal_high_s32(acc_r0, a0_v_rextended[1], r_32bit[1]);
      // imag * imag
      acc_r0 = vmlsl_low_s32(acc_r0, a0_v_iextended[0], i_32bit[0]);
      acc_r0 = vmlsl_high_s32(acc_r0, a0_v_iextended[0], i_32bit[0]);
      acc_r0 = vmlsl_low_s32(acc_r0, a0_v_iextended[1], i_32bit[1]);
      acc_r0 = vmlsl_high_s32(acc_r0, a0_v_iextended[1], i_32bit[1]);

      // real * imag
      acc_i0 = vmlal_low_s32(acc_i0, a0_v_rextended[0], i_32bit[0]);
      acc_i0 = vmlal_high_s32(acc_i0, a0_v_rextended[0], i_32bit[0]);
      acc_i0 = vmlal_low_s32(acc_i0, a0_v_rextended[1], i_32bit[1]);
      acc_i0 = vmlal_high_s32(acc_i0, a0_v_rextended[1], i_32bit[1]);
      // imag * real
      acc_i0 = vmlal_low_s32(acc_i0, a0_v_iextended[0], r_32bit[0]);
      acc_i0 = vmlal_high_s32(acc_i0, a0_v_iextended[0], r_32bit[0]);
      acc_i0 = vmlal_low_s32(acc_i0, a0_v_iextended[1], r_32bit[1]);
      acc_i0 = vmlal_high_s32(acc_i0, a0_v_iextended[1], r_32bit[1]);

      // Second row A * X
      // real * real
      acc_r1 = vmlal_low_s32(acc_r1, a1_v_rextended[0], r_32bit[0]);
      acc_r1 = vmlal_high_s32(acc_r1, a1_v_rextended[0], r_32bit[0]);
      acc_r1 = vmlal_low_s32(acc_r1, a1_v_rextended[1], r_32bit[1]);
      acc_r1 = vmlal_high_s32(acc_r1, a1_v_rextended[1], r_32bit[1]);
      // imag * imag
      acc_r1 = vmlsl_low_s32(acc_r1, a1_v_iextended[0], i_32bit[0]);
      acc_r1 = vmlsl_high_s32(acc_r1, a1_v_iextended[0], i_32bit[0]);
      acc_r1 = vmlsl_low_s32(acc_r1, a1_v_iextended[1], i_32bit[1]);
      acc_r1 = vmlsl_high_s32(acc_r1, a1_v_iextended[1], i_32bit[1]);

      // real * imag
      acc_i1 = vmlal_low_s32(acc_i1, a1_v_rextended[0], i_32bit[0]);
      acc_i1 = vmlal_high_s32(acc_i1, a1_v_rextended[0], i_32bit[0]);
      acc_i1 = vmlal_low_s32(acc_i1, a1_v_rextended[1], i_32bit[1]);
      acc_i1 = vmlal_high_s32(acc_i1, a1_v_rextended[1], i_32bit[1]);
      // imag * real
      acc_i1 = vmlal_low_s32(acc_i1, a1_v_iextended[0], r_32bit[0]);
      acc_i1 = vmlal_high_s32(acc_i1, a1_v_iextended[0], r_32bit[0]);
      acc_i1 = vmlal_low_s32(acc_i1, a1_v_iextended[1], r_32bit[1]);
      acc_i1 = vmlal_high_s32(acc_i1, a1_v_iextended[1], r_32bit[1]);

      p_in1 += 16;
      p_in1_b += 16;
      p_in2 += 16;
      --col_cnt;
    }

    // If the remainder of columns in the row of A is greater than 4,
    // do an unrolled loop of size 4
    if (num_cols_a & 4) {
      // Load four complex numbers from A. Split into real and imaginary parts
      int16x4x2_t a0_v = vld2_s16(p_in1);
      int16x4x2_t a1_v = vld2_s16(p_in1_b);

      int32x4_t a0_vextended[2] = {0};
      a0_vextended[0] = vmovl_s16(a0_v.val[0]);
      a0_vextended[1] = vmovl_s16(a0_v.val[1]);
      int32x4_t a1_vextended[2] = {0};
      a1_vextended[0] = vmovl_s16(a1_v.val[0]);
      a1_vextended[1] = vmovl_s16(a1_v.val[1]);

      int16x4x2_t tmp_b = vld2_s16(p_in2);

      // Extend to 32-bit
      int32x4_t r_32bit = vmovl_s16(tmp_b.val[0]);
      int32x4_t i_32bit = vmovl_s16(tmp_b.val[1]);

      // First row of A
      // real * real
      acc_r0 = vmlal_low_s32(acc_r0, a0_vextended[0], r_32bit);
      acc_r0 = vmlal_high_s32(acc_r0, a0_vextended[0], r_32bit);
      // imag * imag
      acc_r0 = vmlsl_low_s32(acc_r0, a0_vextended[1], i_32bit);
      acc_r0 = vmlsl_high_s32(acc_r0, a0_vextended[1], i_32bit);
      // real * imag
      acc_i0 = vmlal_low_s32(acc_i0, a0_vextended[0], i_32bit);
      acc_i0 = vmlal_high_s32(acc_i0, a0_vextended[0], i_32bit);
      // imag * real
      acc_i0 = vmlal_low_s32(acc_i0, a0_vextended[1], r_32bit);
      acc_i0 = vmlal_high_s32(acc_i0, a0_vextended[1], r_32bit);

      // Second row of A
      // real * real
      acc_r1 = vmlal_low_s32(acc_r1, a1_vextended[0], r_32bit);
      acc_r1 = vmlal_high_s32(acc_r1, a1_vextended[0], r_32bit);
      // imag * imag
      acc_r1 = vmlsl_low_s32(acc_r1, a1_vextended[1], i_32bit);
      acc_r1 = vmlsl_high_s32(acc_r1, a1_vextended[1], i_32bit);
      // real * imag
      acc_i1 = vmlal_low_s32(acc_i1, a1_vextended[0], i_32bit);
      acc_i1 = vmlal_high_s32(acc_i1, a1_vextended[0], i_32bit);
      // imag * real
      acc_i1 = vmlal_low_s32(acc_i1, a1_vextended[1], r_32bit);
      acc_i1 = vmlal_high_s32(acc_i1, a1_vextended[1], r_32bit);

      p_in1 += 8;
      p_in1_b += 8;
      p_in2 += 8;
    }

    sum_real1_ext += vaddvq_s64(acc_r0);
    sum_imag1_ext += vaddvq_s64(acc_i0);
    sum_real1_b_ext += vaddvq_s64(acc_r1);
    sum_imag1_b_ext += vaddvq_s64(acc_i1);

    // If the columns of pSrcA is not a multiple of 4, compute any remaining
    // MACs here. No loop unrolling is used.
    col_cnt = num_cols_a & 3;

    while (col_cnt > 0U) {
      int16_t a_r1 = p_in1[0];
      int16_t a_i1 = p_in1[1];
      int16_t a_r2 = p_in1_b[0];
      int16_t a_i2 = p_in1_b[1];

      int16_t b_r = p_in2[0];
      int16_t b_i = p_in2[1];

      // real * real
      sum_real1_ext += a_r1 * b_r;
      sum_real1_b_ext += a_r2 * b_r;
      // imag * imag
      sum_real1_ext -= a_i1 * b_i;
      sum_real1_b_ext -= a_i2 * b_i;
      // real * imag
      sum_imag1_ext += a_r1 * b_i;
      sum_imag1_b_ext += a_r2 * b_i;
      // imag * real
      sum_imag1_ext += a_i1 * b_r;
      sum_imag1_b_ext += a_i2 * b_r;

      p_in1 += 2U;
      p_in1_b += 2U;
      p_in2 += 2U;
      col_cnt--;
    }

    armral_cmplx_int16_t out[2] = {0};
    out[0].re = vqmovns_s32(vqshrnd_n_s64(sum_real1_ext, 15));
    out[0].im = vqmovns_s32(vqshrnd_n_s64(sum_imag1_ext, 15));
    out[1].re = vqmovns_s32(vqshrnd_n_s64(sum_real1_b_ext, 15));
    out[1].im = vqmovns_s32(vqshrnd_n_s64(sum_imag1_b_ext, 15));

    // Store the result in the destination buffer
    *(px++) = out[0];
    *(px_b++) = out[1];
  }

  // Odd number of rows
  if (num_rows_a & 1) {
    // Output pointer is set to starting address of the row being processed
    armral_cmplx_int16_t *px = p_out;

    // For every row wise process, the pIn2 pointer is set
    // to the starting address of the pSrcX data
    const int16_t *p_in2 = (const int16_t *)p_src_x;

    // Set the variable sum, that acts as accumulator, to zero
    int64_t sum_real1_ext = 0;
    int64_t sum_imag1_ext = 0;

    int64_t sum_real2_ext = 0;
    int64_t sum_imag2_ext = 0;

    // Initialize the pointer pIn1 to point to the starting address of the row
    // being processed
    p_in1 = (const int16_t *)p_in_a;

    int64x2_t acc_r0 = vdupq_n_s64(0);
    int64x2_t acc_i0 = vdupq_n_s64(0);

    // Compute 8 MACs simultaneously
    uint16_t col_cnt = num_cols_a >> 3;

    // Matrix multiplication
    while (col_cnt > 0U) {
      // Load & separate real/imag pSrcA (de-interleave 2)
      int16x8x2_t a0_v = vld2q_s16(p_in1);
      // Extend to 32bit Real part int32x4x2_t
      int32x4x2_t a0_v_rextended;
      a0_v_rextended.val[0] = vmovl_low_s16(a0_v.val[0]);
      a0_v_rextended.val[1] = vmovl_high_s16(a0_v.val[0]);
      // Extend to 32bit Imag part int32x4x2_t
      int32x4x2_t a0_v_iextended;
      a0_v_iextended.val[0] = vmovl_low_s16(a0_v.val[1]);
      a0_v_iextended.val[1] = vmovl_high_s16(a0_v.val[1]);
      p_in1 += 16;

      // Repeat the procedure with X
      int16x8x2_t x0_v = vld2q_s16(p_in2);
      int32x4x2_t x0_v_rextended;
      x0_v_rextended.val[0] = vmovl_low_s16(x0_v.val[0]);
      x0_v_rextended.val[1] = vmovl_high_s16(x0_v.val[0]);
      int32x4x2_t x0_v_iextended;
      x0_v_iextended.val[0] = vmovl_low_s16(x0_v.val[1]);
      x0_v_iextended.val[1] = vmovl_high_s16(x0_v.val[1]);
      p_in2 += 16;

      acc_r0 =
          vmlal_high_s32(acc_r0, a0_v_rextended.val[0], x0_v_rextended.val[0]);
      acc_r0 =
          vmlal_low_s32(acc_r0, a0_v_rextended.val[0], x0_v_rextended.val[0]);

      acc_r0 =
          vmlal_high_s32(acc_r0, a0_v_rextended.val[1], x0_v_rextended.val[1]);
      acc_r0 =
          vmlal_low_s32(acc_r0, a0_v_rextended.val[1], x0_v_rextended.val[1]);

      acc_r0 =
          vmlsl_high_s32(acc_r0, a0_v_iextended.val[0], x0_v_iextended.val[0]);
      acc_r0 =
          vmlsl_low_s32(acc_r0, a0_v_iextended.val[0], x0_v_iextended.val[0]);

      acc_r0 =
          vmlsl_high_s32(acc_r0, a0_v_iextended.val[1], x0_v_iextended.val[1]);
      acc_r0 =
          vmlsl_low_s32(acc_r0, a0_v_iextended.val[1], x0_v_iextended.val[1]);

      acc_i0 =
          vmlal_high_s32(acc_i0, a0_v_iextended.val[0], x0_v_rextended.val[0]);
      acc_i0 =
          vmlal_low_s32(acc_i0, a0_v_iextended.val[0], x0_v_rextended.val[0]);

      acc_i0 =
          vmlal_high_s32(acc_i0, a0_v_iextended.val[1], x0_v_rextended.val[1]);
      acc_i0 =
          vmlal_low_s32(acc_i0, a0_v_iextended.val[1], x0_v_rextended.val[1]);

      acc_i0 =
          vmlal_high_s32(acc_i0, a0_v_rextended.val[0], x0_v_iextended.val[0]);
      acc_i0 =
          vmlal_low_s32(acc_i0, a0_v_rextended.val[0], x0_v_iextended.val[0]);

      acc_i0 =
          vmlal_high_s32(acc_i0, a0_v_rextended.val[1], x0_v_iextended.val[1]);
      acc_i0 =
          vmlal_low_s32(acc_i0, a0_v_rextended.val[1], x0_v_iextended.val[1]);

      col_cnt--;
    }

    // Matrix multiplication
    if (num_cols_a & 4) {
      int16x4x2_t a2_v = vld2_s16(p_in1);

      int32x4x2_t a0_vextended;
      a0_vextended.val[0] = vmovl_s16(a2_v.val[0]);
      a0_vextended.val[1] = vmovl_s16(a2_v.val[1]);
      p_in1 += 8;

      int16x4x2_t x2_v = vld2_s16(p_in2);
      int32x4x2_t x0_vextended;
      x0_vextended.val[0] = vmovl_s16(x2_v.val[0]);
      x0_vextended.val[1] = vmovl_s16(x2_v.val[1]);
      p_in2 += 8;

      acc_r0 = vmlal_high_s32(acc_r0, a0_vextended.val[0], x0_vextended.val[0]);
      acc_r0 = vmlal_low_s32(acc_r0, a0_vextended.val[0], x0_vextended.val[0]);

      acc_r0 = vmlsl_high_s32(acc_r0, a0_vextended.val[1], x0_vextended.val[1]);
      acc_r0 = vmlsl_low_s32(acc_r0, a0_vextended.val[1], x0_vextended.val[1]);

      acc_i0 = vmlal_high_s32(acc_i0, a0_vextended.val[1], x0_vextended.val[0]);
      acc_i0 = vmlal_low_s32(acc_i0, a0_vextended.val[1], x0_vextended.val[0]);

      acc_i0 = vmlal_high_s32(acc_i0, a0_vextended.val[0], x0_vextended.val[1]);
      acc_i0 = vmlal_low_s32(acc_i0, a0_vextended.val[0], x0_vextended.val[1]);
    }

    sum_real1_ext += acc_r0[0] + acc_r0[1];
    sum_imag1_ext += acc_i0[0] + acc_i0[1];

    // If the columns of pSrcA is not a multiple of 4, compute any remaining
    // MACs here. No loop unrolling is used.
    col_cnt = num_cols_a & 3;

    while (col_cnt > 0U) {
      int16_t a1 = (*p_in1);
      int16_t c1 = (*p_in2);

      int16_t b1 = (*(p_in1 + 1U));
      int16_t d1 = (*(p_in2 + 1U));

      sum_real1_ext += a1 * c1;
      sum_imag1_ext += b1 * c1;

      p_in1 += 2U;
      p_in2 += 2U;

      sum_real2_ext -= b1 * d1;
      sum_imag2_ext += a1 * d1;

      col_cnt--;
    }

    sum_real1_ext += sum_real2_ext;
    int32_t sum_real1_ext32 = vqshrnd_n_s64(sum_real1_ext, 15);
    int16_t sum_real1_q15 = vqmovns_s32(sum_real1_ext32);
    sum_imag1_ext += sum_imag2_ext;
    int32_t sum_imag1_ext32 = vqshrnd_n_s64(sum_imag1_ext, 15);
    int16_t sum_imag1_q15 = vqmovns_s32(sum_imag1_ext32);

    // Store the result in the destination buffer
    (*px).re = sum_real1_q15;
    (*px).im = sum_imag1_q15;
  }

  return ARMRAL_SUCCESS;
}

#if !(ARMRAL_ARCH_SVE >= 2)
// Implements an unrolled version of the matrix-vector multiplication
// in the case that there are multiple vectors per matrix. Unrolls the
// vector loop
static armral_status cmplx_mat_vec_mult_batch_unroll_vec(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t *restrict p_src_a,
    const armral_cmplx_int16_t *restrict p_src_x, armral_cmplx_int16_t *p_dst) {
  // In p_src_a the corresponding elements of matrices are stored contiguously
  // e.g. if the batch comprises 2 matrices A0, A1, p_src_a stores:
  //      A0[0,0] A1[0,0] A0[0,1] A1[0,1] A0[0,2] A1[0,2] ...
  // because the matrices are stored in row-major format.

  // For the time being we assume that we have an even number of vectors per
  // matrix, which means that we don't have to deal with a tail loop
  if (num_vecs_per_mat % 4 != 0) {
    return ARMRAL_ARGUMENT_ERROR;
  }

  const int vec_stride = num_mats * num_vecs_per_mat;

  for (uint16_t mat_idx = 0; mat_idx < num_mats; ++mat_idx) {
    for (uint16_t vec = 0; vec < num_vecs_per_mat; vec += 4) {
      // Do one matrix-vector multiplication at a time
      // Work out the starting index of the current vector in p_src_x
      // and p_dst
      int vec_idx = (mat_idx * num_vecs_per_mat) + vec;

      // Point to first element of first row of mat_idx^th A
      const armral_cmplx_int16_t *p_in_a = &p_src_a[mat_idx];

      // Point to first element of vec^th x and y
      const armral_cmplx_int16_t *p_in_x = &p_src_x[vec_idx];
      armral_cmplx_int16_t *p_out = &p_dst[vec_idx];

      // Loop over A one row at a time
      for (uint16_t row_cnt = m; row_cnt > 0;
           --row_cnt, p_out += vec_stride, p_in_a += num_mats * n) {

        // Initialize pIn1 to point to the starting address of the current row
        const int16_t *p_in1 = (const int16_t *)p_in_a;

        // For every row, pIn2 is set to the starting address of the pSrcX data
        const int16_t *p_in2 = (const int16_t *)p_in_x;

        // The accumulators are split into sums of values multiplied by the real
        // and imaginary components of a.
        int64x2_t sum_real_1;
        int64x2_t sum_real_2;
        int64x2_t sum_real_3;
        int64x2_t sum_real_4;
        int64x2_t sum_imag_1;
        int64x2_t sum_imag_2;
        int64x2_t sum_imag_3;
        int64x2_t sum_imag_4;

        // Loop over the row of A one column at a time
        {
          // Only the first two lanes for the vector load from A have useful
          // data in
          int32x4_t a_32 = vmovl_s16(vld1s_s16(p_in1));
          int16x8_t x_16 = vld1q_s16(p_in2);
          int32x4_t x_32_lo = vmovl_low_s16(x_16);
          int32x4_t x_32_hi = vmovl_high_s16(x_16);

          sum_real_1 = vmull_low_laneq_s32(x_32_lo, a_32, 0);
          sum_real_2 = vmull_high_laneq_s32(x_32_lo, a_32, 0);
          sum_real_3 = vmull_low_laneq_s32(x_32_hi, a_32, 0);
          sum_real_4 = vmull_high_laneq_s32(x_32_hi, a_32, 0);
          sum_imag_1 = vmull_low_laneq_s32(x_32_lo, a_32, 1);
          sum_imag_2 = vmull_high_laneq_s32(x_32_lo, a_32, 1);
          sum_imag_3 = vmull_low_laneq_s32(x_32_hi, a_32, 1);
          sum_imag_4 = vmull_high_laneq_s32(x_32_hi, a_32, 1);

          p_in1 += num_mats * 2U;
          p_in2 += vec_stride * 2U;
        }
        for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
          int32x4_t a_32 = vmovl_s16(vld1s_s16(p_in1));
          int16x8_t x_16 = vld1q_s16(p_in2);
          int32x4_t x_32_lo = vmovl_low_s16(x_16);
          int32x4_t x_32_hi = vmovl_high_s16(x_16);

          sum_real_1 = vmlal_low_laneq_s32(sum_real_1, x_32_lo, a_32, 0);
          sum_real_2 = vmlal_high_laneq_s32(sum_real_2, x_32_lo, a_32, 0);
          sum_real_3 = vmlal_low_laneq_s32(sum_real_3, x_32_hi, a_32, 0);
          sum_real_4 = vmlal_high_laneq_s32(sum_real_4, x_32_hi, a_32, 0);
          sum_imag_1 = vmlal_low_laneq_s32(sum_imag_1, x_32_lo, a_32, 1);
          sum_imag_2 = vmlal_high_laneq_s32(sum_imag_2, x_32_lo, a_32, 1);
          sum_imag_3 = vmlal_low_laneq_s32(sum_imag_3, x_32_hi, a_32, 1);
          sum_imag_4 = vmlal_high_laneq_s32(sum_imag_4, x_32_hi, a_32, 1);

          p_in1 += num_mats * 2U;
          p_in2 += vec_stride * 2U;
        }

        sum_real_1 = vaddq_s64(sum_real_1,
                               vextq_s64(vnegq_s64(sum_imag_1), sum_imag_1, 1));
        sum_real_2 = vaddq_s64(sum_real_2,
                               vextq_s64(vnegq_s64(sum_imag_2), sum_imag_2, 1));
        sum_real_3 = vaddq_s64(sum_real_3,
                               vextq_s64(vnegq_s64(sum_imag_3), sum_imag_3, 1));
        sum_real_4 = vaddq_s64(sum_real_4,
                               vextq_s64(vnegq_s64(sum_imag_4), sum_imag_4, 1));

        int32x4_t final_sum32_lo =
            vqshrn_high_n_s64(vqshrn_n_s64(sum_real_1, 15), sum_real_2, 15);
        int32x4_t final_sum32_hi =
            vqshrn_high_n_s64(vqshrn_n_s64(sum_real_3, 15), sum_real_4, 15);
        vst1q_s16((int16_t *)p_out,
                  vqmovn_high_s32(vqmovn_s32(final_sum32_lo), final_sum32_hi));
      }
    }
  }

  return ARMRAL_SUCCESS;
}
#else
static armral_status cmplx_mat_vec_mult_batch_unroll_vec(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t *restrict p_src_a,
    const armral_cmplx_int16_t *restrict p_src_x, armral_cmplx_int16_t *p_dst) {
  // In p_src_a the corresponding elements of matrices are stored contiguously
  // e.g. if the batch comprises 2 matrices A0, A1, p_src_a stores:
  //      A0[0,0] A1[0,0] A0[0,1] A1[0,1] A0[0,2] A1[0,2] ...
  // because the matrices are stored in row-major format.

  const int vec_stride = num_mats * num_vecs_per_mat;

  for (uint16_t mat_idx = 0; mat_idx < num_mats; ++mat_idx) {
    for (uint16_t vec = 0; vec < num_vecs_per_mat; vec += svcntw()) {
      svbool_t pg = svwhilelt_b16(vec * 2, num_vecs_per_mat * 2);
      // Do one matrix-vector multiplication at a time
      // Work out the starting index of the current vector in p_src_x
      // and p_dst
      int vec_idx = (mat_idx * num_vecs_per_mat) + vec;

      // Point to first element of first row of mat_idx^th A
      const armral_cmplx_int16_t *p_in_a = &p_src_a[mat_idx];

      // Point to first element of vec^th x and y
      const armral_cmplx_int16_t *p_in_x = &p_src_x[vec_idx];
      armral_cmplx_int16_t *p_out = &p_dst[vec_idx];

      // Loop over A one row at a time
      for (uint16_t row_cnt = m; row_cnt > 0;
           --row_cnt, p_out += vec_stride, p_in_a += num_mats * n) {

        // Initialize pIn1 to point to the starting address of the current row
        const int16_t *p_in1 = (const int16_t *)p_in_a;

        // For every row, pIn2 is set to the starting address of the pSrcX data
        const int16_t *p_in2 = (const int16_t *)p_in_x;

        // The accumulators are split into sums of values multiplied by the real
        // and imaginary components of a.
        svint64_t sum_real_lo;
        svint64_t sum_real_hi;
        svint64_t sum_imag_lo;
        svint64_t sum_imag_hi;

        // Loop over the row of A one column at a time
        {
          // Only the first two lanes for the vector load from A have useful
          // data in
          svint16_t a_16 =
              svreinterpret_s16_s32(svdup_n_s32(*(const int32_t *)p_in1));
          svint16_t x_16 = svld1_s16(pg, p_in2);
          svint32_t a_real = svmovlb_s32(a_16);
          svint32_t a_imag = svmovlt_s32(a_16);
          svint32_t x_real = svmovlb_s32(x_16);
          svint32_t x_imag = svmovlt_s32(x_16);

          sum_real_lo = svqdmullb_s64(a_real, x_real);
          sum_real_hi = svqdmullt_s64(a_real, x_real);
          sum_imag_lo = svqdmullb_s64(a_real, x_imag);
          sum_imag_hi = svqdmullt_s64(a_real, x_imag);

          sum_real_lo = svqdmlslb_s64(sum_real_lo, a_imag, x_imag);
          sum_real_hi = svqdmlslt_s64(sum_real_hi, a_imag, x_imag);
          sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_imag, x_real);
          sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_imag, x_real);

          p_in1 += num_mats * 2U;
          p_in2 += vec_stride * 2U;
        }
        for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
          svint16_t a_16 =
              svreinterpret_s16_s32(svdup_n_s32(*(const int32_t *)p_in1));
          svint16_t x_16 = svld1_s16(pg, p_in2);
          svint32_t a_real = svmovlb_s32(a_16);
          svint32_t a_imag = svmovlt_s32(a_16);
          svint32_t x_real = svmovlb_s32(x_16);
          svint32_t x_imag = svmovlt_s32(x_16);

          sum_real_lo = svqdmlalb_s64(sum_real_lo, a_real, x_real);
          sum_real_hi = svqdmlalt_s64(sum_real_hi, a_real, x_real);
          sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_real, x_imag);
          sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_real, x_imag);

          sum_real_lo = svqdmlslb_s64(sum_real_lo, a_imag, x_imag);
          sum_real_hi = svqdmlslt_s64(sum_real_hi, a_imag, x_imag);
          sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_imag, x_real);
          sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_imag, x_real);

          p_in1 += num_mats * 2U;
          p_in2 += vec_stride * 2U;
        }

        svint32_t sum_real = svqxtnt_s64(svqxtnb_s64(sum_real_lo), sum_real_hi);
        svint32_t sum_imag = svqxtnt_s64(svqxtnb_s64(sum_imag_lo), sum_imag_hi);
        svint16_t final_sum =
            svqshrnt_n_s32(svqshrnb_n_s32(sum_real, 16), sum_imag, 16);
        svst1_s16(pg, (int16_t *)p_out, final_sum);
      }
    }
  }

  return ARMRAL_SUCCESS;
}
#endif

#if !(ARMRAL_ARCH_SVE >= 2)
static armral_status
cmplx_mat_vec_mult_batch_one_vec(uint16_t num_mats, uint16_t m, uint16_t n,
                                 const armral_cmplx_int16_t *restrict p_src_a,
                                 const armral_cmplx_int16_t *restrict p_src_x,
                                 armral_cmplx_int16_t *p_dst) {
  // In p_src_a the corresponding elements of matrices are stored contiguously
  // e.g. if the batch comprises 2 matrices A0, A1, p_src_a stores:
  //      A0[0,0] A1[0,0] A0[0,1] A1[0,1] A0[0,2] A1[0,2] ...
  // because the matrices are stored in row-major format.

  assert(num_mats % 2 == 0);

  for (uint16_t mat_idx = 0; mat_idx < num_mats; mat_idx += 2) {
    // Do two matrix-vector multiplications at a time

    // Point to first element of first row of mat_idx^th A
    const armral_cmplx_int16_t *p_in_a = &p_src_a[mat_idx];

    // Point to first element of vec^th x and y
    const armral_cmplx_int16_t *p_in_x = &p_src_x[mat_idx];
    armral_cmplx_int16_t *p_out = &p_dst[mat_idx];

    // Loop over A one row at a time
    for (uint16_t row_cnt = 0; row_cnt < m;
         ++row_cnt, p_out += num_mats, p_in_a += num_mats * n) {

      // Initialize pIn1 to point to the starting address of the current row
      const int16_t *p_in1 = (const int16_t *)p_in_a;

      // For every row, pIn2 is set to the starting address of the pSrcX data
      const int16_t *p_in2 = (const int16_t *)p_in_x;

      // Set the accumulator variables to zero
      int64x2_t sum_real_1;
      int64x2_t sum_real_2;
      int64x2_t sum_imag_1;
      int64x2_t sum_imag_2;

      // Loop over the row of A one column at a time
      {
        // x_32 = [x0r, x0i, x1r, x1i]
        int32x4_t x_32 = vmovl_s16(vld1_s16(p_in2));
        // a_32 = [a0r, a0i, a1r, a1i]
        int32x4_t a_32 = vmovl_s16(vld1_s16(p_in1));

        // sum_real_1 = [x0r * a0r, x0r * a0i]
        sum_real_1 = vmull_low_laneq_s32(a_32, x_32, 0);
        // sum_real_2 = [x1r * a1r, x1r * a1i]
        sum_real_2 = vmull_high_laneq_s32(a_32, x_32, 2);
        // sum_imag_1 = [x0i * a0r, x0i * a0i]
        sum_imag_1 = vmull_low_laneq_s32(a_32, x_32, 1);
        // sum_imag_2 = [x1i * a1r, x1i * a1i]
        sum_imag_2 = vmull_high_laneq_s32(a_32, x_32, 3);
        p_in1 += num_mats * 2U;
        p_in2 += num_mats * 2U;
      }
      for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
        int32x4_t x_32 = vmovl_s16(vld1_s16(p_in2));
        int32x4_t a_32 = vmovl_s16(vld1_s16(p_in1));

        sum_real_1 = vmlal_low_laneq_s32(sum_real_1, a_32, x_32, 0);
        sum_real_2 = vmlal_high_laneq_s32(sum_real_2, a_32, x_32, 2);
        sum_imag_1 = vmlal_low_laneq_s32(sum_imag_1, a_32, x_32, 1);
        sum_imag_2 = vmlal_high_laneq_s32(sum_imag_2, a_32, x_32, 3);

        p_in1 += num_mats * 2U;
        p_in2 += num_mats * 2U;
      }

      sum_real_1 = vaddq_s64(sum_real_1,
                             vextq_s64(vnegq_s64(sum_imag_1), sum_imag_1, 1));
      sum_real_2 = vaddq_s64(sum_real_2,
                             vextq_s64(vnegq_s64(sum_imag_2), sum_imag_2, 1));

      int32x4_t final_sum32 =
          vqshrn_high_n_s64(vqshrn_n_s64(sum_real_1, 15), sum_real_2, 15);
      vst1_s16((int16_t *)p_out, vqmovn_s32(final_sum32));
    }
  }

  return ARMRAL_SUCCESS;
}
#else // SVE2
static armral_status
cmplx_mat_vec_mult_batch_one_vec(uint16_t num_mats, uint16_t m, uint16_t n,
                                 const armral_cmplx_int16_t *restrict p_src_a,
                                 const armral_cmplx_int16_t *restrict p_src_x,
                                 armral_cmplx_int16_t *p_dst) {
  // In p_src_a the corresponding elements of matrices are stored contiguously
  // e.g. if the batch comprises 2 matrices A0, A1, p_src_a stores:
  //      A0[0,0] A1[0,0] A0[0,1] A1[0,1] A0[0,2] A1[0,2] ...
  // because the matrices are stored in row-major format.

  for (uint16_t mat_idx = 0; mat_idx < num_mats; mat_idx += svcntw()) {
    svbool_t pg = svwhilelt_b16(mat_idx * 2, num_mats * 2);
    // Point to first element of first row of mat_idx^th A
    const armral_cmplx_int16_t *p_in_a = &p_src_a[mat_idx];

    // Point to first element of vec^th x and y
    const armral_cmplx_int16_t *p_in_x = &p_src_x[mat_idx];
    armral_cmplx_int16_t *p_out = &p_dst[mat_idx];

    // Loop over A one row at a time
    for (uint16_t row_cnt = 0; row_cnt < m;
         ++row_cnt, p_out += num_mats, p_in_a += num_mats * n) {

      // Initialize pIn1 to point to the starting address of the current row
      const int16_t *p_in1 = (const int16_t *)p_in_a;

      // For every row, pIn2 is set to the starting address of the pSrcX data
      const int16_t *p_in2 = (const int16_t *)p_in_x;

      // Set the accumulator variables to zero
      svint64_t sum_real_lo;
      svint64_t sum_real_hi;
      svint64_t sum_imag_lo;
      svint64_t sum_imag_hi;

      // Loop over the row of A one column at a time
      {
        svint16_t a_16 = svld1_s16(pg, p_in1);
        svint16_t x_16 = svld1_s16(pg, p_in2);
        svint32_t a_real = svmovlb_s32(a_16);
        svint32_t a_imag = svmovlt_s32(a_16);
        svint32_t x_real = svmovlb_s32(x_16);
        svint32_t x_imag = svmovlt_s32(x_16);

        sum_real_lo = svqdmullb_s64(a_real, x_real);
        sum_real_hi = svqdmullt_s64(a_real, x_real);
        sum_imag_lo = svqdmullb_s64(a_real, x_imag);
        sum_imag_hi = svqdmullt_s64(a_real, x_imag);

        sum_real_lo = svqdmlslb_s64(sum_real_lo, a_imag, x_imag);
        sum_real_hi = svqdmlslt_s64(sum_real_hi, a_imag, x_imag);
        sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_imag, x_real);
        sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_imag, x_real);

        p_in1 += num_mats * 2U;
        p_in2 += num_mats * 2U;
      }
      for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
        svint16_t a_16 = svld1_s16(pg, p_in1);
        svint16_t x_16 = svld1_s16(pg, p_in2);
        svint32_t a_real = svmovlb_s32(a_16);
        svint32_t a_imag = svmovlt_s32(a_16);
        svint32_t x_real = svmovlb_s32(x_16);
        svint32_t x_imag = svmovlt_s32(x_16);

        sum_real_lo = svqdmlalb_s64(sum_real_lo, a_real, x_real);
        sum_real_hi = svqdmlalt_s64(sum_real_hi, a_real, x_real);
        sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_real, x_imag);
        sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_real, x_imag);

        sum_real_lo = svqdmlslb_s64(sum_real_lo, a_imag, x_imag);
        sum_real_hi = svqdmlslt_s64(sum_real_hi, a_imag, x_imag);
        sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_imag, x_real);
        sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_imag, x_real);

        p_in1 += num_mats * 2U;
        p_in2 += num_mats * 2U;
      }

      svint32_t sum_real = svqxtnt_s64(svqxtnb_s64(sum_real_lo), sum_real_hi);
      svint32_t sum_imag = svqxtnt_s64(svqxtnb_s64(sum_imag_lo), sum_imag_hi);
      svst1_s16(pg, (int16_t *)p_out,
                svqshrnt_n_s32(svqshrnb_n_s32(sum_real, 16), sum_imag, 16));
    }
  }

  return ARMRAL_SUCCESS;
}
#endif

armral_status armral_cmplx_mat_vec_mult_batch_i16(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t *restrict p_src_a,
    const armral_cmplx_int16_t *restrict p_src_x, armral_cmplx_int16_t *p_dst) {
  if ((num_vecs_per_mat * num_mats) % 12 != 0) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  if (num_vecs_per_mat == 1) {
    return cmplx_mat_vec_mult_batch_one_vec(num_mats, m, n, p_src_a, p_src_x,
                                            p_dst);
  }
  return cmplx_mat_vec_mult_batch_unroll_vec(num_mats, num_vecs_per_mat, m, n,
                                             p_src_a, p_src_x, p_dst);
}

#if !(ARMRAL_ARCH_SVE >= 2)
static armral_status cmplx_mat_vec_mult_batch_pa_one_vec(
    uint16_t num_mats, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t **__restrict p_srcs_a,
    const armral_cmplx_int16_t **__restrict p_srcs_x,
    armral_cmplx_int16_t **__restrict p_dsts) {
  // The i-th element of the j-th matrix in the batch is
  // located at `p_srcs[i][j]`. Similarly, the j-th matrix in a batch of `2 x 2`
  // matrices is formed as:
  //  p_srcs[0][j]  p_srcs[1][j]
  //  p_srcs[2][j]  p_srcs[3][j]

  // We only support multiple of two matrices in a batch for the time being
  assert(num_mats % 2 == 0);

  for (uint16_t mat_idx = 0; mat_idx < num_mats; mat_idx += 2) {
    // Do two matrix-vector multiplications at a time
    // Loop over A one row at a time
    for (uint16_t row_cnt = 0; row_cnt < m; ++row_cnt) {
      // Set the accumulator variables to zero
      int64x2_t sum_real_1;
      int64x2_t sum_real_2;
      int64x2_t sum_imag_1;
      int64x2_t sum_imag_2;

      // Loop over the row of A one column at a time
      {
        int i = row_cnt * n;
        int16x4_t a_16 = vld1_s16((const int16_t *)&p_srcs_a[i][mat_idx]);
        int16x4_t x_16 = vld1_s16((const int16_t *)&p_srcs_x[0][mat_idx]);
        int32x4_t a_32 = vmovl_s16(a_16);
        int32x4_t x_32 = vmovl_s16(x_16);
        sum_real_1 = vmull_low_laneq_s32(a_32, x_32, 0);
        sum_real_2 = vmull_high_laneq_s32(a_32, x_32, 2);
        sum_imag_1 = vmull_low_laneq_s32(a_32, x_32, 1);
        sum_imag_2 = vmull_high_laneq_s32(a_32, x_32, 3);
      }
      for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
        int i = (row_cnt * n) + col_cnt;
        int16x4_t a_16 = vld1_s16((const int16_t *)&p_srcs_a[i][mat_idx]);
        int16x4_t x_16 = vld1_s16((const int16_t *)&p_srcs_x[col_cnt][mat_idx]);
        int32x4_t a_32 = vmovl_s16(a_16);
        int32x4_t x_32 = vmovl_s16(x_16);
        sum_real_1 = vmlal_low_laneq_s32(sum_real_1, a_32, x_32, 0);
        sum_real_2 = vmlal_high_laneq_s32(sum_real_2, a_32, x_32, 2);
        sum_imag_1 = vmlal_low_laneq_s32(sum_imag_1, a_32, x_32, 1);
        sum_imag_2 = vmlal_high_laneq_s32(sum_imag_2, a_32, x_32, 3);
      }

      sum_real_1 = vaddq_s64(sum_real_1,
                             vextq_s64(vnegq_s64(sum_imag_1), sum_imag_1, 1));
      sum_real_2 = vaddq_s64(sum_real_2,
                             vextq_s64(vnegq_s64(sum_imag_2), sum_imag_2, 1));
      int32x4_t final_sum32 =
          vqshrn_high_n_s64(vqshrn_n_s64(sum_real_1, 15), sum_real_2, 15);
      vst1_s16((int16_t *)&p_dsts[row_cnt][mat_idx], vqmovn_s32(final_sum32));
    }
  }

  return ARMRAL_SUCCESS;
}
#else
static armral_status cmplx_mat_vec_mult_batch_pa_one_vec(
    uint16_t num_mats, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t **__restrict p_srcs_a,
    const armral_cmplx_int16_t **__restrict p_srcs_x,
    armral_cmplx_int16_t **__restrict p_dsts) {
  // The i-th element of the j-th matrix in the batch is
  // located at `p_srcs[i][j]`. Similarly, the j-th matrix in a batch of `2 x 2`
  // matrices is formed as:
  //  p_srcs[0][j]  p_srcs[1][j]
  //  p_srcs[2][j]  p_srcs[3][j]

  for (uint16_t mat_idx = 0; mat_idx < num_mats; mat_idx += svcntw()) {
    svbool_t pg = svwhilelt_b16(mat_idx * 2, num_mats * 2);

    // Loop over A one row at a time
    for (uint16_t row_cnt = 0; row_cnt < m; ++row_cnt) {
      // Set the accumulator variables to zero
      svint64_t sum_real_lo;
      svint64_t sum_real_hi;
      svint64_t sum_imag_lo;
      svint64_t sum_imag_hi;

      // Loop over the row of A one column at a time
      {
        int i = row_cnt * n;
        svint16_t a_16 = svld1_s16(pg, (const int16_t *)&p_srcs_a[i][mat_idx]);
        svint16_t x_16 = svld1_s16(pg, (const int16_t *)&p_srcs_x[0][mat_idx]);
        svint32_t a_real = svmovlb_s32(a_16);
        svint32_t a_imag = svmovlt_s32(a_16);
        svint32_t x_real = svmovlb_s32(x_16);
        svint32_t x_imag = svmovlt_s32(x_16);

        sum_real_lo = svqdmullb_s64(a_real, x_real);
        sum_real_hi = svqdmullt_s64(a_real, x_real);
        sum_imag_lo = svqdmullb_s64(a_real, x_imag);
        sum_imag_hi = svqdmullt_s64(a_real, x_imag);

        sum_real_lo = svqdmlslb_s64(sum_real_lo, a_imag, x_imag);
        sum_real_hi = svqdmlslt_s64(sum_real_hi, a_imag, x_imag);
        sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_imag, x_real);
        sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_imag, x_real);
      }
      for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
        int i = (row_cnt * n) + col_cnt;
        svint16_t a_16 = svld1_s16(pg, (const int16_t *)&p_srcs_a[i][mat_idx]);
        svint16_t x_16 =
            svld1_s16(pg, (const int16_t *)&p_srcs_x[col_cnt][mat_idx]);
        svint32_t a_real = svmovlb_s32(a_16);
        svint32_t a_imag = svmovlt_s32(a_16);
        svint32_t x_real = svmovlb_s32(x_16);
        svint32_t x_imag = svmovlt_s32(x_16);

        sum_real_lo = svqdmlalb_s64(sum_real_lo, a_real, x_real);
        sum_real_hi = svqdmlalt_s64(sum_real_hi, a_real, x_real);
        sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_real, x_imag);
        sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_real, x_imag);

        sum_real_lo = svqdmlslb_s64(sum_real_lo, a_imag, x_imag);
        sum_real_hi = svqdmlslt_s64(sum_real_hi, a_imag, x_imag);
        sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_imag, x_real);
        sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_imag, x_real);
      }

      svint32_t sum_real = svqxtnt_s64(svqxtnb_s64(sum_real_lo), sum_real_hi);
      svint32_t sum_imag = svqxtnt_s64(svqxtnb_s64(sum_imag_lo), sum_imag_hi);
      svint16_t final_sum =
          svqshrnt_n_s32(svqshrnb_n_s32(sum_real, 16), sum_imag, 16);
      svst1_s16(pg, (int16_t *)&p_dsts[row_cnt][mat_idx], final_sum);
    }
  }

  return ARMRAL_SUCCESS;
}
#endif

#if !(ARMRAL_ARCH_SVE >= 2)
static armral_status cmplx_mat_vec_mult_batch_pa_unroll_vec(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t **__restrict p_srcs_a,
    const armral_cmplx_int16_t **__restrict p_srcs_x,
    armral_cmplx_int16_t **__restrict p_dsts) {

  // The i-th element of the j-th matrix in the batch is
  // located at `p_srcs[i][j]`. Similarly, the j-th matrix in a batch of `2 x 2`
  // matrices is formed as:
  //  p_srcs[0][j]  p_srcs[1][j]
  //  p_srcs[2][j]  p_srcs[3][j]

  for (uint16_t mat_idx = 0; mat_idx < num_mats; ++mat_idx) {
    for (uint16_t vec = 0; vec < num_vecs_per_mat; vec += 4) {
      // Do four matrix-vector multiplications at a time
      // Work out the starting index of the current vector in p_src_x
      // and p_dst
      int vec_idx = (mat_idx * num_vecs_per_mat) + vec;

      // Loop over A one row at a time
      for (uint16_t row_cnt = 0; row_cnt < m; ++row_cnt) {
        // Set the accumulator variables to zero
        int64x2_t sum_real_1;
        int64x2_t sum_real_2;
        int64x2_t sum_real_3;
        int64x2_t sum_real_4;
        int64x2_t sum_imag_1;
        int64x2_t sum_imag_2;
        int64x2_t sum_imag_3;
        int64x2_t sum_imag_4;

        // Loop over the row of A one column at a time
        {
          int i = row_cnt * n;
          int16x4_t a_16 = vld1s_s16((const int16_t *)&p_srcs_a[i][mat_idx]);
          int16x8_t x_16 = vld1q_s16((const int16_t *)&p_srcs_x[0][vec_idx]);
          int32x4_t a_32 = vmovl_s16(a_16);
          int32x4_t x_32_lo = vmovl_low_s16(x_16);
          int32x4_t x_32_hi = vmovl_high_s16(x_16);

          sum_real_1 = vmull_low_laneq_s32(x_32_lo, a_32, 0);
          sum_real_2 = vmull_high_laneq_s32(x_32_lo, a_32, 0);
          sum_real_3 = vmull_low_laneq_s32(x_32_hi, a_32, 0);
          sum_real_4 = vmull_high_laneq_s32(x_32_hi, a_32, 0);
          sum_imag_1 = vmull_low_laneq_s32(x_32_lo, a_32, 1);
          sum_imag_2 = vmull_high_laneq_s32(x_32_lo, a_32, 1);
          sum_imag_3 = vmull_low_laneq_s32(x_32_hi, a_32, 1);
          sum_imag_4 = vmull_high_laneq_s32(x_32_hi, a_32, 1);
        }
        for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
          int i = (row_cnt * n) + col_cnt;
          int16x4_t a_16 = vld1s_s16((const int16_t *)&p_srcs_a[i][mat_idx]);
          int16x8_t x_16 =
              vld1q_s16((const int16_t *)&p_srcs_x[col_cnt][vec_idx]);
          int32x4_t a_32 = vmovl_s16(a_16);
          int32x4_t x_32_lo = vmovl_low_s16(x_16);
          int32x4_t x_32_hi = vmovl_high_s16(x_16);

          sum_real_1 = vmlal_low_laneq_s32(sum_real_1, x_32_lo, a_32, 0);
          sum_real_2 = vmlal_high_laneq_s32(sum_real_2, x_32_lo, a_32, 0);
          sum_real_3 = vmlal_low_laneq_s32(sum_real_3, x_32_hi, a_32, 0);
          sum_real_4 = vmlal_high_laneq_s32(sum_real_4, x_32_hi, a_32, 0);
          sum_imag_1 = vmlal_low_laneq_s32(sum_imag_1, x_32_lo, a_32, 1);
          sum_imag_2 = vmlal_high_laneq_s32(sum_imag_2, x_32_lo, a_32, 1);
          sum_imag_3 = vmlal_low_laneq_s32(sum_imag_3, x_32_hi, a_32, 1);
          sum_imag_4 = vmlal_high_laneq_s32(sum_imag_4, x_32_hi, a_32, 1);
        }

        sum_real_1 = vaddq_s64(sum_real_1,
                               vextq_s64(vnegq_s64(sum_imag_1), sum_imag_1, 1));
        sum_real_2 = vaddq_s64(sum_real_2,
                               vextq_s64(vnegq_s64(sum_imag_2), sum_imag_2, 1));
        sum_real_3 = vaddq_s64(sum_real_3,
                               vextq_s64(vnegq_s64(sum_imag_3), sum_imag_3, 1));
        sum_real_4 = vaddq_s64(sum_real_4,
                               vextq_s64(vnegq_s64(sum_imag_4), sum_imag_4, 1));

        int32x4_t final_sum32_lo =
            vqshrn_high_n_s64(vqshrn_n_s64(sum_real_1, 15), sum_real_2, 15);
        int32x4_t final_sum32_hi =
            vqshrn_high_n_s64(vqshrn_n_s64(sum_real_3, 15), sum_real_4, 15);
        vst1q_s16((int16_t *)&p_dsts[row_cnt][vec_idx],
                  vqmovn_high_s32(vqmovn_s32(final_sum32_lo), final_sum32_hi));
      }
    }
  }

  return ARMRAL_SUCCESS;
}
#else
static armral_status cmplx_mat_vec_mult_batch_pa_unroll_vec(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t **__restrict p_srcs_a,
    const armral_cmplx_int16_t **__restrict p_srcs_x,
    armral_cmplx_int16_t **__restrict p_dsts) {

  // The i-th element of the j-th matrix in the batch is
  // located at `p_srcs[i][j]`. Similarly, the j-th matrix in a batch of `2 x 2`
  // matrices is formed as:
  //  p_srcs[0][j]  p_srcs[1][j]
  //  p_srcs[2][j]  p_srcs[3][j]

  for (uint16_t mat_idx = 0; mat_idx < num_mats; ++mat_idx) {
    for (uint16_t vec = 0; vec < num_vecs_per_mat; vec += svcntw()) {
      svbool_t pg = svwhilelt_b16(vec * 2, num_vecs_per_mat * 2);
      // Do four matrix-vector multiplications at a time
      // Work out the starting index of the current vector in p_src_x
      // and p_dst
      int vec_idx = (mat_idx * num_vecs_per_mat) + vec;

      // Loop over A one row at a time
      for (uint16_t row_cnt = 0; row_cnt < m; ++row_cnt) {
        // Set the accumulator variables to zero
        svint64_t sum_real_lo;
        svint64_t sum_real_hi;
        svint64_t sum_imag_lo;
        svint64_t sum_imag_hi;

        // Loop over the row of A one column at a time
        {
          int i = row_cnt * n;
          svint16_t a_16 = svreinterpret_s16_s32(
              svdup_n_s32(*(const int32_t *)&p_srcs_a[i][mat_idx]));
          svint16_t x_16 =
              svld1_s16(pg, (const int16_t *)&p_srcs_x[0][vec_idx]);

          svint32_t a_real = svmovlb_s32(a_16);
          svint32_t a_imag = svmovlt_s32(a_16);
          svint32_t x_real = svmovlb_s32(x_16);
          svint32_t x_imag = svmovlt_s32(x_16);

          sum_real_lo = svqdmullb_s64(a_real, x_real);
          sum_real_hi = svqdmullt_s64(a_real, x_real);
          sum_imag_lo = svqdmullb_s64(a_real, x_imag);
          sum_imag_hi = svqdmullt_s64(a_real, x_imag);

          sum_real_lo = svqdmlslb_s64(sum_real_lo, a_imag, x_imag);
          sum_real_hi = svqdmlslt_s64(sum_real_hi, a_imag, x_imag);
          sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_imag, x_real);
          sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_imag, x_real);
        }
        for (uint16_t col_cnt = 1; col_cnt < n; ++col_cnt) {
          int i = (row_cnt * n) + col_cnt;
          svint16_t a_16 = svreinterpret_s16_s32(
              svdup_n_s32(*(const int32_t *)&p_srcs_a[i][mat_idx]));
          svint16_t x_16 =
              svld1_s16(pg, (const int16_t *)&p_srcs_x[col_cnt][vec_idx]);
          svint32_t a_real = svmovlb_s32(a_16);
          svint32_t a_imag = svmovlt_s32(a_16);
          svint32_t x_real = svmovlb_s32(x_16);
          svint32_t x_imag = svmovlt_s32(x_16);

          sum_real_lo = svqdmlalb_s64(sum_real_lo, a_real, x_real);
          sum_real_hi = svqdmlalt_s64(sum_real_hi, a_real, x_real);
          sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_real, x_imag);
          sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_real, x_imag);

          sum_real_lo = svqdmlslb_s64(sum_real_lo, a_imag, x_imag);
          sum_real_hi = svqdmlslt_s64(sum_real_hi, a_imag, x_imag);
          sum_imag_lo = svqdmlalb_s64(sum_imag_lo, a_imag, x_real);
          sum_imag_hi = svqdmlalt_s64(sum_imag_hi, a_imag, x_real);
        }

        svint32_t sum_real = svqxtnt_s64(svqxtnb_s64(sum_real_lo), sum_real_hi);
        svint32_t sum_imag = svqxtnt_s64(svqxtnb_s64(sum_imag_lo), sum_imag_hi);
        svint16_t final_sum =
            svqshrnt_n_s32(svqshrnb_n_s32(sum_real, 16), sum_imag, 16);

        svst1_s16(pg, (int16_t *)&p_dsts[row_cnt][vec_idx], final_sum);
      }
    }
  }

  return ARMRAL_SUCCESS;
}
#endif

armral_status armral_cmplx_mat_vec_mult_batch_i16_pa(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_int16_t **__restrict p_srcs_a,
    const armral_cmplx_int16_t **__restrict p_srcs_x,
    armral_cmplx_int16_t **__restrict p_dsts) {
  if (num_vecs_per_mat == 1) {
    if (num_mats % 2 != 0) {
      return ARMRAL_ARGUMENT_ERROR;
    }
    return cmplx_mat_vec_mult_batch_pa_one_vec(num_mats, m, n, p_srcs_a,
                                               p_srcs_x, p_dsts);
  }
  if (num_vecs_per_mat % 4 != 0) {
    // We only support batches in multiples of four
    return ARMRAL_ARGUMENT_ERROR;
  }
  return cmplx_mat_vec_mult_batch_pa_unroll_vec(num_mats, num_vecs_per_mat, m,
                                                n, p_srcs_a, p_srcs_x, p_dsts);
}
