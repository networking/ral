/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"

#include <cstdlib>
#include <cstring>

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

namespace {

void cmplx_matmul_ahb_b2x2(uint16_t m,
                           const armral_cmplx_f32_t *__restrict p_src_a,
                           const armral_cmplx_f32_t *__restrict p_src_b,
                           armral_cmplx_f32_t *p_dst) {
  constexpr uint16_t nk = 2;

#if ARMRAL_ARCH_SVE >= 2
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svbool_t p2 = svptrue_pat_b32(SV_VL2);
  svfloat32_t b_r0 = svld1_f32(p4, (float32_t const *)p_src_b);
  svfloat32_t b_r1 = svld1_f32(p4, ((float32_t const *)p_src_b) + 4);
  svfloat32_t b_r0_rev = svrev64_f32(p4, b_r0);
  svfloat32_t b_r1_rev = svrev64_f32(p4, b_r1);

  for (uint32_t j = 0; j < m; j++) {
    svfloat32_t acc_1;
    svfloat32_t acc_2;

    // r = 0
    svfloat32_t a_r0j = svld1_f32(p2, (float32_t const *)&p_src_a[j]);
    // [R0*r, R0*i, R0*r, R0*i]
    acc_1 = svmul_lane_f32(b_r0, a_r0j, 0);
    // [I0*i, I0*r, I0*i, I0*r]
    acc_2 = svmul_lane_f32(b_r0_rev, a_r0j, 1);

    // r = 1
    svfloat32_t a_r1j = svld1_f32(p2, (float32_t const *)&p_src_a[m + j]);
    // [R0*r, R0*i, R0*r, R0*i]
    acc_1 = svmla_lane_f32(acc_1, b_r1, a_r1j, 0);
    // [I0*i, I0*r, I0*i, I0*r]
    acc_2 = svmla_lane_f32(acc_2, b_r1_rev, a_r1j, 1);

    svfloat32_t result = svadd_f32_x(
        p4, acc_1,
        svreinterpret_f32_f64(svneg_f64_x(p4, svreinterpret_f64_f32(acc_2))));
    svst1_f32(p4, (float32_t *)&p_dst[nk * j], result);
  }
#else
  float32x4_t b_r0 = vld1q_f32((float32_t const *)p_src_b);
  float32x4_t b_r1 = vld1q_f32(((float32_t const *)p_src_b) + 4);
  float32x4_t b_r0_rev = vrev64q_f32(b_r0);
  float32x4_t b_r1_rev = vrev64q_f32(b_r1);

  for (uint32_t j = 0; j < m; j++) {
    float32x4_t acc_1;
    float32x4_t acc_2;

    // r = 0
    float32x2_t a_r0j = vld1_f32((float32_t const *)&p_src_a[j]);
    // [R0*r, R0*i, R0*r, R0*i]
    acc_1 = vmulq_lane_f32(b_r0, a_r0j, 0);
    // [I0*i, I0*r, I0*i, I0*r]
    acc_2 = vmulq_lane_f32(b_r0_rev, a_r0j, 1);

    // r = 1
    float32x2_t a_r1j = vld1_f32((float32_t const *)&p_src_a[m + j]);
    // [R0*r, R0*i, R0*r, R0*i]
    acc_1 = vfmaq_lane_f32(acc_1, b_r1, a_r1j, 0);
    // [I0*i, I0*r, I0*i, I0*r]
    acc_2 = vfmaq_lane_f32(acc_2, b_r1_rev, a_r1j, 1);

    float32x4_t result = vaddq_f32(acc_1, vnegq64_f32(acc_2));

    vst1q_f32((float32_t *)&p_dst[nk * j], result);
  }
#endif
}

void cmplx_matmul_ahb_b3x3(uint16_t m,
                           const armral_cmplx_f32_t *__restrict p_src_a,
                           const armral_cmplx_f32_t *__restrict p_src_b,
                           armral_cmplx_f32_t *p_dst) {
  constexpr uint16_t nk = 3;

#if ARMRAL_ARCH_SVE >= 2
  svbool_t p3 = svptrue_pat_b32(SV_VL3);
  svbool_t p2 = svptrue_pat_b32(SV_VL2);

  svfloat32x2_t b_r0 = svld2_f32(p3, (float32_t const *)&p_src_b[0]);
  svfloat32x2_t b_r1 = svld2_f32(p3, (float32_t const *)&p_src_b[3]);
  svfloat32x2_t b_r2 = svld2_f32(p3, (float32_t const *)&p_src_b[6]);
  svfloat32x2_t *b_rows[] = {&b_r0, &b_r1, &b_r2};

  for (uint32_t j = 0; j < m; j++) {
    svfloat32_t dot_0 = svundef_f32();
    svfloat32_t dot_1 = svundef_f32();
    // Note: We leave it to the compiler to unroll this loop over nk
    for (uint32_t r = 0; r < nk; r++) {
      svfloat32_t a_rj = svld1_f32(p2, (float32_t const *)&p_src_a[r * m + j]);
      // Note: We leave it to the compiler to eliminate the following branch
      if (r == 0) {
        // dot.re += a_jr.re * b_ir.re + a_jr.im * b_ir.im;
        dot_0 = svmul_lane_f32(svget2(*b_rows[r], 0), a_rj, 0);
        dot_0 = svmla_lane_f32(dot_0, svget2(*b_rows[r], 1), a_rj, 1);
        // dot.im += a_jr.re * b_ir.im - a_jr.im * b_ir.re;
        dot_1 = svmul_lane_f32(svget2(*b_rows[r], 1), a_rj, 0);
        dot_1 = svmls_lane_f32(dot_1, svget2(*b_rows[r], 0), a_rj, 1);
      } else {
        // dot.re += a_jr.re * b_ir.re + a_jr.im * b_ir.im;
        dot_0 = svmla_lane_f32(dot_0, svget2(*b_rows[r], 0), a_rj, 0);
        dot_0 = svmla_lane_f32(dot_0, svget2(*b_rows[r], 1), a_rj, 1);
        // dot.im += a_jr.re * b_ir.im - a_jr.im * b_ir.re;
        dot_1 = svmla_lane_f32(dot_1, svget2(*b_rows[r], 1), a_rj, 0);
        dot_1 = svmls_lane_f32(dot_1, svget2(*b_rows[r], 0), a_rj, 1);
      }
    }
    svfloat32x2_t dot = svcreate2(dot_0, dot_1);
    svst2_f32(p3, (float32_t *)&p_dst[nk * j], dot);
  }
#else
  // Copy the final row of B so we can safely read one extra column:
  armral_cmplx_f32_t final_row[4];
  memcpy(final_row, &p_src_b[6], sizeof(armral_cmplx_f32_t) * 3);

  // Do three overlapping loads for the rows:
  // r0 r1 r2 x0
  //          r0 r1 r2 x0
  //                   r0 r1 r2 x0
  // The final lane of each vector is the first row for the next j. We can just
  // ignore that lane (the multiplications done for that lane will be incorrect)
  float32x4x2_t b_rows[3] = {
      vld2q_f32((float32_t const *)&p_src_b[0]),
      vld2q_f32((float32_t const *)&p_src_b[3]),
      vld2q_f32((float32_t const *)&final_row),
  };

  for (uint32_t j = 0; j < m; j++) {
    float32x4x2_t dot;
    // Note: We leave it to the compiler to unroll this loop over nk
    for (uint32_t r = 0; r < nk; r++) {
      float32x2_t a_rj = vld1_f32((float32_t const *)&p_src_a[r * m + j]);
      // Note: We leave it to the compiler to eliminate the following branch
      if (r == 0) {
        // dot.re += a_jr.re * b_ir.re + a_jr.im * b_ir.im;
        dot.val[0] = vmulq_lane_f32(b_rows[r].val[0], a_rj, 0);
        dot.val[0] = vfmaq_lane_f32(dot.val[0], b_rows[r].val[1], a_rj, 1);
        // dot.im += a_jr.re * b_ir.im - a_jr.im * b_ir.re;
        dot.val[1] = vmulq_lane_f32(b_rows[r].val[1], a_rj, 0);
        dot.val[1] = vfmsq_lane_f32(dot.val[1], b_rows[r].val[0], a_rj, 1);
      } else {
        // dot.re += a_jr.re * b_ir.re + a_jr.im * b_ir.im;
        dot.val[0] = vfmaq_lane_f32(dot.val[0], b_rows[r].val[0], a_rj, 0);
        dot.val[0] = vfmaq_lane_f32(dot.val[0], b_rows[r].val[1], a_rj, 1);
        // dot.im += a_jr.re * b_ir.im - a_jr.im * b_ir.re;
        dot.val[1] = vfmaq_lane_f32(dot.val[1], b_rows[r].val[1], a_rj, 0);
        dot.val[1] = vfmsq_lane_f32(dot.val[1], b_rows[r].val[0], a_rj, 1);
      }
    }
    // dot    = r r r X i i i X
    // result = r i r i r i X X
    float32x4x2_t result = vzipq_f32(dot.val[0], dot.val[1]);
    // Store first the first two columns:
    vst1q_f32((float32_t *)&p_dst[nk * j], result.val[0]);
    // Store the remaining column:
    vst1_f32(((float32_t *)&p_dst[nk * j]) + 4, vget_low_f32(result.val[1]));
  }
#endif
}

void cmplx_matmul_ahb_b4x4(uint16_t m,
                           const armral_cmplx_f32_t *__restrict p_src_a,
                           const armral_cmplx_f32_t *__restrict p_src_b,
                           armral_cmplx_f32_t *p_dst) {
  constexpr uint16_t nk = 4;

#if ARMRAL_ARCH_SVE >= 2
  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svbool_t p2 = svptrue_pat_b32(SV_VL2);

  svfloat32x2_t b_r0 = svld2_f32(p4, (float32_t const *)&p_src_b[0]);
  svfloat32x2_t b_r1 = svld2_f32(p4, (float32_t const *)&p_src_b[4]);
  svfloat32x2_t b_r2 = svld2_f32(p4, (float32_t const *)&p_src_b[8]);
  svfloat32x2_t b_r3 = svld2_f32(p4, (float32_t const *)&p_src_b[12]);
  svfloat32x2_t *b_rows[] = {&b_r0, &b_r1, &b_r2, &b_r3};

  for (uint32_t j = 0; j < m; j++) {
    svfloat32_t dot_0 = svundef_f32();
    svfloat32_t dot_1 = svundef_f32();
    // Note: We leave it to the compiler to unroll this loop over nk
    for (uint32_t r = 0; r < nk; r++) {
      svfloat32_t a_rj = svld1_f32(p2, (float32_t const *)&p_src_a[r * m + j]);
      // Note: We leave it to the compiler to eliminate the following branch
      if (r == 0) {
        // dot.re += a_jr.re * b_ir.re + a_jr.im * b_ir.im;
        dot_0 = svmul_lane_f32(svget2(*b_rows[r], 0), a_rj, 0);
        dot_0 = svmla_lane_f32(dot_0, svget2(*b_rows[r], 1), a_rj, 1);
        // dot.im += a_jr.re * b_ir.im - a_jr.im * b_ir.re;
        dot_1 = svmul_lane_f32(svget2(*b_rows[r], 1), a_rj, 0);
        dot_1 = svmls_lane_f32(dot_1, svget2(*b_rows[r], 0), a_rj, 1);
      } else {
        // dot.re += a_jr.re * b_ir.re + a_jr.im * b_ir.im;
        dot_0 = svmla_lane_f32(dot_0, svget2(*b_rows[r], 0), a_rj, 0);
        dot_0 = svmla_lane_f32(dot_0, svget2(*b_rows[r], 1), a_rj, 1);
        // dot.im += a_jr.re * b_ir.im - a_jr.im * b_ir.re;
        dot_1 = svmla_lane_f32(dot_1, svget2(*b_rows[r], 1), a_rj, 0);
        dot_1 = svmls_lane_f32(dot_1, svget2(*b_rows[r], 0), a_rj, 1);
      }
    }

    svfloat32x2_t dot = svcreate2(dot_0, dot_1);
    svst2_f32(p4, (float32_t *)&p_dst[nk * j], dot);
  }
#else
  float32x4x2_t b_rows[4] = {vld2q_f32((float32_t const *)&p_src_b[0]),
                             vld2q_f32((float32_t const *)&p_src_b[4]),
                             vld2q_f32((float32_t const *)&p_src_b[8]),
                             vld2q_f32((float32_t const *)&p_src_b[12])};

  for (uint32_t j = 0; j < m; j++) {
    float32x4x2_t dot;
    // Note: We leave it to the compiler to unroll this loop over nk
    for (uint32_t r = 0; r < nk; r++) {
      float32x2_t a_rj = vld1_f32((float32_t const *)&p_src_a[r * m + j]);
      // Note: We leave it to the compiler to eliminate the following branch
      if (r == 0) {
        // dot.re += a_jr.re * b_ir.re + a_jr.im * b_ir.im;
        dot.val[0] = vmulq_lane_f32(b_rows[r].val[0], a_rj, 0);
        dot.val[0] = vfmaq_lane_f32(dot.val[0], b_rows[r].val[1], a_rj, 1);
        // dot.im += a_jr.re * b_ir.im - a_jr.im * b_ir.re;
        dot.val[1] = vmulq_lane_f32(b_rows[r].val[1], a_rj, 0);
        dot.val[1] = vfmsq_lane_f32(dot.val[1], b_rows[r].val[0], a_rj, 1);
      } else {
        // dot.re += a_jr.re * b_ir.re + a_jr.im * b_ir.im;
        dot.val[0] = vfmaq_lane_f32(dot.val[0], b_rows[r].val[0], a_rj, 0);
        dot.val[0] = vfmaq_lane_f32(dot.val[0], b_rows[r].val[1], a_rj, 1);
        // dot.im += a_jr.re * b_ir.im - a_jr.im * b_ir.re;
        dot.val[1] = vfmaq_lane_f32(dot.val[1], b_rows[r].val[1], a_rj, 0);
        dot.val[1] = vfmsq_lane_f32(dot.val[1], b_rows[r].val[0], a_rj, 1);
      }
    }
    vst2q_f32((float32_t *)&p_dst[nk * j], dot);
  }
#endif
}

#ifdef ARMRAL_ARCH_SVE
inline void __attribute__((always_inline))
cmplx_matmul_ahb_dot_product_sve_predicated_unroll(
    svbool_t pg, uint16_t m, uint16_t n, uint16_t k, uint16_t i, uint16_t j,
    const armral_cmplx_f32_t *__restrict p_src_a,
    const armral_cmplx_f32_t *__restrict p_src_b, armral_cmplx_f32_t *p_dst) {

  svbool_t p2 = svptrue_pat_b32(SV_VL2);
  svfloat32_t acc_1 = svdup_n_f32(0.0F);
  svfloat32_t acc_2 = svdup_n_f32(0.0F);

  // Every row of A and B (where row of A = column of A^H)
  for (uint32_t r = 0; r < k; r++) {
    float32_t const *a_ptr = (float32_t const *)&p_src_a[r * m + j];
    svfloat32_t a_rj = svld1rq_f32(p2, a_ptr);
    float32_t const *b_ptr = (float32_t const *)&p_src_b[r * n + i];
    svfloat32_t b_vec = svld1_f32(pg, b_ptr);
    // [R0 * r, I0 * r, R0 * r, I0 * r]
    acc_1 = svcmla_lane_f32(acc_1, b_vec, a_rj, 0, 0);
    // [I0 * i, -R0 * i, I0 * i, -R0 * i]
    acc_2 = svcmla_lane_f32(acc_2, b_vec, a_rj, 0, 270);
  }

  svfloat32_t result = svreinterpret_f32_f64(
      svneg_f64_x(pg, svreinterpret_f64_f32(svadd_f32_x(pg, acc_1, acc_2))));
  float32_t *c_ptr = (float32_t *)&p_dst[n * j + i];
  svst1_f32(pg, c_ptr, result);
}

inline void __attribute__((always_inline))
cmplx_matmul_ahb_dot_product_sve_predicated_unroll_j2(
    svbool_t pg, uint16_t m, uint16_t n, uint16_t k, uint16_t i, uint16_t j,
    const armral_cmplx_f32_t *__restrict p_src_a,
    const armral_cmplx_f32_t *__restrict p_src_b, armral_cmplx_f32_t *p_dst) {

  svbool_t p4 = svptrue_pat_b32(SV_VL4);
  svfloat32_t acc_1 = svdup_n_f32(0.0F);
  svfloat32_t acc_2 = svdup_n_f32(0.0F);
  svfloat32_t acc_3 = svdup_n_f32(0.0F);
  svfloat32_t acc_4 = svdup_n_f32(0.0F);

  // Every row of A and B (where row of A = column of A^H)
  for (uint32_t r = 0; r < k; r++) {
    // Load Aij and Aij+1
    float32_t const *a_ptr = (float32_t const *)&p_src_a[r * m + j];
    svfloat32_t a_rj = svld1rq_f32(p4, a_ptr);
    float32_t const *b_ptr = (float32_t const *)&p_src_b[r * n + i];
    svfloat32_t b_vec = svld1_f32(pg, b_ptr);
    // [R0 * r, I0 * r, R0 * r, I0 * r]
    acc_1 = svcmla_lane_f32(acc_1, b_vec, a_rj, 0, 0);
    // [I0 * i, -R0 * i, I0 * i, -R0 * i]
    acc_2 = svcmla_lane_f32(acc_2, b_vec, a_rj, 0, 270);
    // [R0 * r, I0 * r, R0 * r, I0 * r]
    acc_3 = svcmla_lane_f32(acc_3, b_vec, a_rj, 1, 0);
    // [I0 * i, -R0 * i, I0 * i, -R0 * i]
    acc_4 = svcmla_lane_f32(acc_4, b_vec, a_rj, 1, 270);
  }

  svfloat32_t result_1 = svreinterpret_f32_f64(
      svneg_f64_x(pg, svreinterpret_f64_f32(svadd_f32_x(pg, acc_1, acc_2))));
  svfloat32_t result_2 = svreinterpret_f32_f64(
      svneg_f64_x(pg, svreinterpret_f64_f32(svadd_f32_x(pg, acc_3, acc_4))));
  float32_t *c_ptr_1 = (float32_t *)&p_dst[n * j + i];
  float32_t *c_ptr_2 = (float32_t *)&p_dst[n * (j + 1) + i];
  svst1_f32(pg, c_ptr_1, result_1);
  svst1_f32(pg, c_ptr_2, result_2);
}
#else
inline void __attribute__((always_inline))
cmplx_matmul_ahb_dot_product_unroll_by_i2_j2(
    uint16_t m, uint16_t n, uint16_t k, uint16_t i, uint16_t j,
    const armral_cmplx_f32_t *__restrict p_src_a,
    const armral_cmplx_f32_t *__restrict p_src_b, armral_cmplx_f32_t *p_dst) {

  float32x4_t acc_1 = vdupq_n_f32(0.0F);
  float32x4_t acc_2 = vdupq_n_f32(0.0F);
  float32x4_t acc_3 = vdupq_n_f32(0.0F);
  float32x4_t acc_4 = vdupq_n_f32(0.0F);

  // Every row of A and B (where row of A = column of A^H)
  for (uint32_t r = 0; r < k; r++) {
    // A = [R0, I0, R1, I1] (Aij and Aij+1)
    float32_t const *a_ptr = (float32_t const *)&p_src_a[r * m + j];
    float32x4_t a_rj = vld1q_f32(a_ptr);
    // B = [r, i, r, i]
    float32_t const *b_ptr = (float32_t const *)&p_src_b[r * n + i];
    float32x4_t b_vec = vld1q_f32(b_ptr);

    // [R0*r, R0*i, R0*r, R0*i]
    acc_1 = vfmaq_laneq_f32(acc_1, b_vec, a_rj, 0);
    // [I0*i, I0*r, I0*i, I0*r]
    acc_2 = vfmaq_laneq_f32(acc_2, b_vec, a_rj, 1);
    // [R1*r, R1*i, R1*r, R1*i]
    acc_3 = vfmaq_laneq_f32(acc_3, b_vec, a_rj, 2);
    // [I1*i, I1*r, I1*i, I1*r]
    acc_4 = vfmaq_laneq_f32(acc_4, b_vec, a_rj, 3);
  }

  // Correct sign of acc_2 (a*i*-1 + a*i*-1...) = -(a*i + a*i)
  float32x4_t result_1 = vaddq_f32(acc_1, vnegq64_f32(vrev64q_f32(acc_2)));
  float32x4_t result_2 = vaddq_f32(acc_3, vnegq64_f32(vrev64q_f32(acc_4)));
  float32_t *c_ptr_1 = (float32_t *)&p_dst[n * j + i];
  float32_t *c_ptr_2 = (float32_t *)&p_dst[n * (j + 1) + i];
  vst1q_f32(c_ptr_1, result_1);
  vst1q_f32(c_ptr_2, result_2);
}

inline void __attribute__((always_inline))
cmplx_matmul_ahb_dot_product_unroll_by_i2(
    uint16_t m, uint16_t n, uint16_t k, uint16_t i, uint16_t j,
    const armral_cmplx_f32_t *__restrict p_src_a,
    const armral_cmplx_f32_t *__restrict p_src_b, armral_cmplx_f32_t *p_dst) {

  float32x4_t acc_1 = vdupq_n_f32(0.0F);
  float32x4_t acc_2 = vdupq_n_f32(0.0F);

  // Every row of A and B (where row of A = column of A^H)
  for (uint32_t r = 0; r < k; r++) {
    // A = [R0, I0]
    float32_t const *a_ptr = (float32_t const *)&p_src_a[r * m + j];
    float32x2_t a_rj = vld1_f32(a_ptr);
    // B = [r, i, r, i]
    float32_t const *b_ptr = (float32_t const *)&p_src_b[r * n + i];
    float32x4_t b_vec = vld1q_f32(b_ptr);

    // [R0*r, R0*i, R0*r, R0*i]
    acc_1 = vfmaq_lane_f32(acc_1, b_vec, a_rj, 0);
    // [I0*i, I0*r, I0*i, I0*r]
    acc_2 = vfmaq_lane_f32(acc_2, b_vec, a_rj, 1);
  }

  // Correct sign of acc_2 (a*i*-1 + a*i*-1...) = -(a*i + a*i)
  float32x4_t result = vaddq_f32(acc_1, vnegq64_f32(vrev64q_f32(acc_2)));
  float32_t *c_ptr = (float32_t *)&p_dst[n * j + i];
  vst1q_f32(c_ptr, result);
}

inline void __attribute__((always_inline)) cmplx_matmul_ahb_dot_product_scalar(
    uint16_t m, uint16_t n, uint16_t k, uint16_t i, uint16_t j,
    const armral_cmplx_f32_t *__restrict p_src_a,
    const armral_cmplx_f32_t *__restrict p_src_b, armral_cmplx_f32_t *p_dst) {

  float32x2_t acc_1 = vdup_n_f32(0.0F);
  float32x2_t acc_2 = vdup_n_f32(0.0F);
  // Every row of A and B (where row of A = column of A^H)
  for (uint32_t r = 0; r < k; r++) {
    float32x2_t a_rj = vld1_f32((float32_t const *)&p_src_a[r * m + j]);
    float32x2_t b_ir = vld1_f32((float32_t const *)&p_src_b[r * n + i]);
    // [R0*r, R0*i, R0*r, R0*i]
    acc_1 = vfma_lane_f32(acc_1, b_ir, a_rj, 0);
    // [I0*i, I0*r, I0*i, I0*r]
    acc_2 = vfma_lane_f32(acc_2, b_ir, a_rj, 1);
  }

  float32x2_t result = vadd_f32(acc_1, vneg64_f32(vrev64_f32(acc_2)));
  vst1_f32((float32_t *)&p_dst[n * j + i], result);
}

inline void __attribute__((always_inline))
cmplx_matmul_ahb_dot_product_unroll_by_j2(
    uint16_t m, uint16_t n, uint16_t k, uint16_t i, uint16_t j,
    const armral_cmplx_f32_t *__restrict p_src_a,
    const armral_cmplx_f32_t *__restrict p_src_b, armral_cmplx_f32_t *p_dst) {

  float32x2_t acc_1 = vdup_n_f32(0.0F);
  float32x2_t acc_2 = vdup_n_f32(0.0F);
  float32x2_t acc_3 = vdup_n_f32(0.0F);
  float32x2_t acc_4 = vdup_n_f32(0.0F);

  // Every row of A and B (where row of A = column of A^H)
  for (uint32_t r = 0; r < k; r++) {
    float32x4_t a_rj = vld1q_f32((float32_t const *)&p_src_a[r * m + j]);
    float32x2_t b_ir = vld1_f32((float32_t const *)&p_src_b[r * n + i]);
    // [R0*r, R0*i, R0*r, R0*i]
    acc_1 = vfma_laneq_f32(acc_1, b_ir, a_rj, 0);
    // [I0*i, I0*r, I0*i, I0*r]
    acc_2 = vfma_laneq_f32(acc_2, b_ir, a_rj, 1);
    // [R1*r, R1*i, R1*r, R1*i]
    acc_3 = vfma_laneq_f32(acc_3, b_ir, a_rj, 2);
    // [I1*i, I1*r, I1*i, I1*r]
    acc_4 = vfma_laneq_f32(acc_4, b_ir, a_rj, 3);
  }

  float32x2_t result_1 = vadd_f32(acc_1, vneg64_f32(vrev64_f32(acc_2)));
  float32x2_t result_2 = vadd_f32(acc_3, vneg64_f32(vrev64_f32(acc_4)));
  vst1_f32((float32_t *)&p_dst[n * j + i], result_1);
  vst1_f32((float32_t *)&p_dst[n * (j + 1) + i], result_2);
}
#endif

inline void __attribute__((always_inline))
cmplx_matmul_ahb_general_f32(uint16_t m, uint16_t n, uint16_t k,
                             const armral_cmplx_f32_t *__restrict p_src_a,
                             const armral_cmplx_f32_t *__restrict p_src_b,
                             armral_cmplx_f32_t *p_dst) {
#ifdef ARMRAL_ARCH_SVE
  uint32_t i_unroll = svcntd();
  svbool_t ptrue = svptrue_b32();
  // For every row of output C (column of A, row of A^H)...
  uint32_t j = 0;
  for (; j + 2 <= m; j += 2) {
    uint32_t i = 0;
    // For every VL/2 columns of output C (column of B)...
    for (; i + i_unroll <= n; i += i_unroll) {
      cmplx_matmul_ahb_dot_product_sve_predicated_unroll_j2(
          ptrue, m, n, k, i, j, p_src_a, p_src_b, p_dst);
    }
    // For the remaining columns of output C...
    if (i < n) {
      svbool_t tail = svwhilelt_b32(i * 2, (uint32_t)n * 2);
      cmplx_matmul_ahb_dot_product_sve_predicated_unroll_j2(
          tail, m, n, k, i, j, p_src_a, p_src_b, p_dst);
    }
  }
  if (j != m) {
    uint32_t i = 0;
    // For every VL/2 columns of output C (column of B)...
    for (; i + i_unroll <= n; i += i_unroll) {
      cmplx_matmul_ahb_dot_product_sve_predicated_unroll(
          ptrue, m, n, k, i, j, p_src_a, p_src_b, p_dst);
    }
    // For the remaining columns of output C...
    if (i < n) {
      svbool_t tail = svwhilelt_b32(i * 2, (uint32_t)n * 2);
      cmplx_matmul_ahb_dot_product_sve_predicated_unroll(
          tail, m, n, k, i, j, p_src_a, p_src_b, p_dst);
    }
  }
#else
  // For every two rows of output C (column of A, row of A^H)...
  uint32_t j = 0;
  for (; j + 2 <= m; j += 2) {
    uint32_t i = 0;
    // For every two columns of output C (column of B)...
    for (; i + 2 <= n; i += 2) {
      cmplx_matmul_ahb_dot_product_unroll_by_i2_j2(m, n, k, i, j, p_src_a,
                                                   p_src_b, p_dst);
    }
    // For the remaining columns of output C...
    if (i != n) {
      cmplx_matmul_ahb_dot_product_unroll_by_j2(m, n, k, i, j, p_src_a, p_src_b,
                                                p_dst);
    }
  }
  // For the remaining rows of output C...
  if (j != m) {
    uint32_t i = 0;
    // For every two columns of output C (column of B)...
    for (; i + 2 <= n; i += 2) {
      cmplx_matmul_ahb_dot_product_unroll_by_i2(m, n, k, i, j, p_src_a, p_src_b,
                                                p_dst);
    }
    // For the remaining columns of output C...
    if (i != n) {
      cmplx_matmul_ahb_dot_product_scalar(m, n, k, i, j, p_src_a, p_src_b,
                                          p_dst);
    }
  }
#endif
}

} // namespace

armral_status
armral_cmplx_matmul_ahb_f32(uint16_t m, uint16_t n, uint16_t k,
                            const armral_cmplx_f32_t *__restrict p_src_a,
                            const armral_cmplx_f32_t *__restrict p_src_b,
                            armral_cmplx_f32_t *p_dst) {

  if (n == k) {
    if (n == 2) {
      cmplx_matmul_ahb_b2x2(m, p_src_a, p_src_b, p_dst);
      return ARMRAL_SUCCESS;
    }
    if (n == 3) {
      cmplx_matmul_ahb_b3x3(m, p_src_a, p_src_b, p_dst);
      return ARMRAL_SUCCESS;
    }
    if (n == 4) {
      cmplx_matmul_ahb_b4x4(m, p_src_a, p_src_b, p_dst);
      return ARMRAL_SUCCESS;
    }
    if (n == 8) {
      // Note: With compile-time constant parameters the compiler can sometimes
      // further optimize the 8x8 case.
      cmplx_matmul_ahb_general_f32(m, 8, 8, p_src_a, p_src_b, p_dst);
      return ARMRAL_SUCCESS;
    }
  }

  // Fallback: General vectorized implementation:
  cmplx_matmul_ahb_general_f32(m, n, k, p_src_a, p_src_b, p_dst);

  return ARMRAL_SUCCESS;
}
