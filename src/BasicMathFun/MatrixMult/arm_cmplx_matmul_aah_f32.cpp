/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

namespace {

#ifdef ARMRAL_ARCH_SVE
template<uint16_t M>
inline void aah_mult_iter(svbool_t pg, uint16_t n, uint16_t k,
                          const armral_cmplx_f32_t *__restrict p_src_a,
                          svfloat32_t &p_out_re0, svfloat32_t &p_out_re1,
                          svfloat32_t &p_out_re2, svfloat32_t &p_out_re3,
                          svfloat32_t &p_out_re4, svfloat32_t &p_out_re5,
                          svfloat32_t &p_out_re6, svfloat32_t &p_out_re7,
                          svfloat32_t &p_out_re8, svfloat32_t &p_out_re9,
                          svfloat32_t &p_out_im0, svfloat32_t &p_out_im1,
                          svfloat32_t &p_out_im2, svfloat32_t &p_out_im3,
                          svfloat32_t &p_out_im4, svfloat32_t &p_out_im5) {
  svfloat32_t p_in0;
  svfloat32_t p_in1;
  svfloat32_t p_in2;
  svfloat32_t p_in3;
  if constexpr (M >= 1) {
    p_in0 = svld1_f32(pg, (const float32_t *)&p_src_a[0 * n + k]);
  }
  if constexpr (M >= 2) {
    p_in1 = svld1_f32(pg, (const float32_t *)&p_src_a[1 * n + k]);
  }
  if constexpr (M >= 3) {
    p_in2 = svld1_f32(pg, (const float32_t *)&p_src_a[2 * n + k]);
  }
  if constexpr (M >= 4) {
    p_in3 = svld1_f32(pg, (const float32_t *)&p_src_a[3 * n + k]);
  }

  svfloat32_t p_in_flip0;
  svfloat32_t p_in_flip1;
  svfloat32_t p_in_flip2;
  // Compute the real and imaginary parts separately to better exploit the
  // structure of the Hermitian matrix (where `C(i,j) = conj(C(j,i))`).
  switch (M) {
  case 2:
    p_out_re0 = svmla_f32_m(pg, p_out_re0, p_in0, p_in0);
    p_out_re1 = svmla_f32_m(pg, p_out_re1, p_in0, p_in1);
    p_out_re2 = svmla_f32_m(pg, p_out_re2, p_in1, p_in1);
    p_in_flip0 =
        svreinterpret_f32_s64(svrevw_s64_x(pg, svreinterpret_s64_f32(p_in1)));
    p_out_im0 = svmls_f32_m(pg, p_out_im0, p_in0, p_in_flip0);
    break;
  case 3:
    p_out_re0 = svmla_f32_m(pg, p_out_re0, p_in0, p_in0);
    p_out_re1 = svmla_f32_m(pg, p_out_re1, p_in0, p_in1);
    p_out_re2 = svmla_f32_m(pg, p_out_re2, p_in0, p_in2);
    p_out_re3 = svmla_f32_m(pg, p_out_re3, p_in1, p_in1);
    p_out_re4 = svmla_f32_m(pg, p_out_re4, p_in1, p_in2);
    p_out_re5 = svmla_f32_m(pg, p_out_re5, p_in2, p_in2);
    p_in_flip0 =
        svreinterpret_f32_s64(svrevw_s64_x(pg, svreinterpret_s64_f32(p_in1)));
    p_in_flip1 =
        svreinterpret_f32_s64(svrevw_s64_x(pg, svreinterpret_s64_f32(p_in2)));
    p_out_im0 = svmls_f32_m(pg, p_out_im0, p_in0, p_in_flip0);
    p_out_im1 = svmls_f32_m(pg, p_out_im1, p_in0, p_in_flip1);
    p_out_im2 = svmls_f32_m(pg, p_out_im2, p_in1, p_in_flip1);
    break;
  case 4:
    p_out_re0 = svmla_f32_m(pg, p_out_re0, p_in0, p_in0);
    p_out_re1 = svmla_f32_m(pg, p_out_re1, p_in0, p_in1);
    p_out_re2 = svmla_f32_m(pg, p_out_re2, p_in0, p_in2);
    p_out_re3 = svmla_f32_m(pg, p_out_re3, p_in0, p_in3);
    p_out_re4 = svmla_f32_m(pg, p_out_re4, p_in1, p_in1);
    p_out_re5 = svmla_f32_m(pg, p_out_re5, p_in1, p_in2);
    p_out_re6 = svmla_f32_m(pg, p_out_re6, p_in1, p_in3);
    p_out_re7 = svmla_f32_m(pg, p_out_re7, p_in2, p_in2);
    p_out_re8 = svmla_f32_m(pg, p_out_re8, p_in2, p_in3);
    p_out_re9 = svmla_f32_m(pg, p_out_re9, p_in3, p_in3);
    p_in_flip0 =
        svreinterpret_f32_s64(svrevw_s64_x(pg, svreinterpret_s64_f32(p_in1)));
    p_in_flip1 =
        svreinterpret_f32_s64(svrevw_s64_x(pg, svreinterpret_s64_f32(p_in2)));
    p_in_flip2 =
        svreinterpret_f32_s64(svrevw_s64_x(pg, svreinterpret_s64_f32(p_in3)));
    p_out_im0 = svmls_f32_m(pg, p_out_im0, p_in0, p_in_flip0);
    p_out_im1 = svmls_f32_m(pg, p_out_im1, p_in0, p_in_flip1);
    p_out_im2 = svmls_f32_m(pg, p_out_im2, p_in0, p_in_flip2);
    p_out_im3 = svmls_f32_m(pg, p_out_im3, p_in1, p_in_flip1);
    p_out_im4 = svmls_f32_m(pg, p_out_im4, p_in1, p_in_flip2);
    p_out_im5 = svmls_f32_m(pg, p_out_im5, p_in2, p_in_flip2);
    break;
  }
}
#endif

template<uint16_t M>
inline armral_status
armral_cmplx_matmul_aah_f32_m(uint16_t n,
                              const armral_cmplx_f32_t *__restrict p_src_a,
                              armral_cmplx_f32_t *p_dst_c) {
  // For each row, we have a decrementing number of real values to calculate.
  constexpr uint16_t num_re = (M * (M + 1)) / 2;
  constexpr uint16_t num_im = (M * M) - num_re;

  float32_t re[num_re];
  float32_t im[num_im];

#ifdef ARMRAL_ARCH_SVE
  const uint32_t vals_per_vector = svcntd();
  const svbool_t pg_all = svptrue_b32();
  // SVE vectors can't be stored in arrays...
  // Create the maximum number of vectors we should (currently) see, and allow
  // the compiler to elide the unused ones.
  // M = 2: num_re = 3
  //        num_im = 1
  // M = 3: num_re = 6
  //        num_im = 3
  // M = 4: num_re = 10
  //        num_im = 6
  svfloat32_t p_out_re0 = svdup_n_f32(0.F);
  svfloat32_t p_out_re1 = svdup_n_f32(0.F);
  svfloat32_t p_out_re2 = svdup_n_f32(0.F);
  svfloat32_t p_out_re3 = svdup_n_f32(0.F);
  svfloat32_t p_out_re4 = svdup_n_f32(0.F);
  svfloat32_t p_out_re5 = svdup_n_f32(0.F);
  svfloat32_t p_out_re6 = svdup_n_f32(0.F);
  svfloat32_t p_out_re7 = svdup_n_f32(0.F);
  svfloat32_t p_out_re8 = svdup_n_f32(0.F);
  svfloat32_t p_out_re9 = svdup_n_f32(0.F);
  svfloat32_t p_out_im0 = svdup_n_f32(0.F);
  svfloat32_t p_out_im1 = svdup_n_f32(0.F);
  svfloat32_t p_out_im2 = svdup_n_f32(0.F);
  svfloat32_t p_out_im3 = svdup_n_f32(0.F);
  svfloat32_t p_out_im4 = svdup_n_f32(0.F);
  svfloat32_t p_out_im5 = svdup_n_f32(0.F);
  uint16_t k = 0;
  for (; k + vals_per_vector <= n; k += vals_per_vector) {
    aah_mult_iter<M>(pg_all, n, k, p_src_a, p_out_re0, p_out_re1, p_out_re2,
                     p_out_re3, p_out_re4, p_out_re5, p_out_re6, p_out_re7,
                     p_out_re8, p_out_re9, p_out_im0, p_out_im1, p_out_im2,
                     p_out_im3, p_out_im4, p_out_im5);
  }
  if (n % vals_per_vector != 0) {
    svbool_t pg_remainder = svwhilelt_b32_u32(k * 2, n * 2);
    aah_mult_iter<M>(pg_remainder, n, k, p_src_a, p_out_re0, p_out_re1,
                     p_out_re2, p_out_re3, p_out_re4, p_out_re5, p_out_re6,
                     p_out_re7, p_out_re8, p_out_re9, p_out_im0, p_out_im1,
                     p_out_im2, p_out_im3, p_out_im4, p_out_im5);
  }
  if constexpr (M >= 1) {
    re[0] = svaddv_f32(pg_all, p_out_re0);
  }
  if constexpr (M >= 2) {
    re[1] = svaddv_f32(pg_all, p_out_re1);
    re[2] = svaddv_f32(pg_all, p_out_re2);
    p_out_im0 = svreinterpret_f32_f64(
        svneg_f64_x(pg_all, svreinterpret_f64_f32(p_out_im0)));
    im[0] = svaddv_f32(pg_all, p_out_im0);
  }
  if constexpr (M >= 3) {
    re[3] = svaddv_f32(pg_all, p_out_re3);
    re[4] = svaddv_f32(pg_all, p_out_re4);
    re[5] = svaddv_f32(pg_all, p_out_re5);
    p_out_im1 = svreinterpret_f32_f64(
        svneg_f64_x(pg_all, svreinterpret_f64_f32(p_out_im1)));
    p_out_im2 = svreinterpret_f32_f64(
        svneg_f64_x(pg_all, svreinterpret_f64_f32(p_out_im2)));
    im[1] = svaddv_f32(pg_all, p_out_im1);
    im[2] = svaddv_f32(pg_all, p_out_im2);
  }
  if constexpr (M >= 4) {
    re[6] = svaddv_f32(pg_all, p_out_re6);
    re[7] = svaddv_f32(pg_all, p_out_re7);
    re[8] = svaddv_f32(pg_all, p_out_re8);
    re[9] = svaddv_f32(pg_all, p_out_re9);
    p_out_im3 = svreinterpret_f32_f64(
        svneg_f64_x(pg_all, svreinterpret_f64_f32(p_out_im3)));
    p_out_im4 = svreinterpret_f32_f64(
        svneg_f64_x(pg_all, svreinterpret_f64_f32(p_out_im4)));
    p_out_im5 = svreinterpret_f32_f64(
        svneg_f64_x(pg_all, svreinterpret_f64_f32(p_out_im5)));
    im[3] = svaddv_f32(pg_all, p_out_im3);
    im[4] = svaddv_f32(pg_all, p_out_im4);
    im[5] = svaddv_f32(pg_all, p_out_im5);
  }
  uint16_t idx_re = 0;
  uint16_t idx_im = 0;
  // For each row...
  for (uint16_t i = 0; i < M; i++) {
    // For each value...
    for (uint16_t j = i; j < M; j++) {
      p_dst_c[i * M + j].re = re[idx_re];
      if (i != j) {
        p_dst_c[j * M + i].re = re[idx_re];
        p_dst_c[j * M + i].im = -im[idx_im];
        p_dst_c[i * M + j].im = im[idx_im];
        idx_im++;
      } else {
        p_dst_c[i * M + j].im = 0.F;
      }
      idx_re++;
    }
  }
#else
  {
    float32x4_t p_out_re[num_re];
    for (int i = 0; i < num_re; i++) {
      p_out_re[i] = vdupq_n_f32(0.F);
    }
    float32x4_t p_out_im[num_im];
    for (int i = 0; i < num_im; i++) {
      p_out_im[i] = vdupq_n_f32(0.F);
    }
    for (uint16_t k = 0; k + 1 < n; k += 2) {
      float32x4_t p_in[M];
      for (int i = 0; i < M; i++) {
        p_in[i] = vld1q_f32((const float32_t *)&p_src_a[i * n + k]);
      }

      int idx = 0;
      for (int a = 0; a < M; a++) {
        for (int ah = a; ah < M; ah++) {
          p_out_re[idx] = vfmaq_f32(p_out_re[idx], p_in[a], p_in[ah]);
          idx++;
        }
      }

      float32x4_t p_in_flip[M - 1];
      for (int i = 1; i < M; i++) {
        p_in_flip[i - 1] = vrev64q_f32(p_in[i]);
      }
      idx = 0;
      for (int a = 0; a + 1 < M; a++) {
        for (int ah = a; ah + 1 < M; ah++) {
          p_out_im[idx] = vfmsq_f32(p_out_im[idx], p_in[a], p_in_flip[ah]);
          idx++;
        }
      }
    }
    for (int i = 0; i < num_re; i++) {
      re[i] = vaddvq_f32(p_out_re[i]);
    }
    for (int i = 0; i < num_im; i++) {
      p_out_im[i] =
          vreinterpretq_f32_f64(vnegq_f64(vreinterpretq_f64_f32(p_out_im[i])));
      im[i] = vaddvq_f32(p_out_im[i]);
    }
  }
  if (n % 2 != 0) {
    float32x2_t p_out_re[num_re];
    float32x2_t p_out_im[num_im];
    for (int i = 0; i < num_re; i++) {
      p_out_re[i] = vdup_n_f32(0.F);
    }
    for (int i = 0; i < num_im; i++) {
      p_out_im[i] = vdup_n_f32(0.F);
    }
    uint16_t k = n - 1;
    float32x2_t p_in[M];
    for (int i = 0; i < M; i++) {
      p_in[i] = vld1_f32((const float32_t *)&p_src_a[i * n + k]);
    }

    int idx = 0;
    for (int a = 0; a < M; a++) {
      for (int ah = a; ah < M; ah++) {
        p_out_re[idx] = vfma_f32(p_out_re[idx], p_in[a], p_in[ah]);
        idx++;
      }
    }

    float32x2_t p_in_flip[M - 1];
    for (int i = 1; i < M; i++) {
      p_in_flip[i - 1] = vrev64_f32(p_in[i]);
    }
    idx = 0;
    for (int a = 0; a + 1 < M; a++) {
      for (int ah = a; ah + 1 < M; ah++) {
        p_out_im[idx] = vfms_f32(p_out_im[idx], p_in[a], p_in_flip[ah]);
        idx++;
      }
    }

    for (int i = 0; i < num_re; i++) {
      re[i] += vaddv_f32(p_out_re[i]);
    }
    for (int i = 0; i < num_im; i++) {
      p_out_im[i] =
          vreinterpret_f32_f64(vneg_f64(vreinterpret_f64_f32(p_out_im[i])));
      im[i] += vaddv_f32(p_out_im[i]);
    }
  }
  constexpr uint16_t num_vecs = (M * M) / 2;
  float32x4_t p_out[num_vecs];
  float32x2_t p_out_extra;
  switch (M) {
  case 2:
    p_out[0] = float32x4_t{re[0], 0.F, re[1], im[0]};
    p_out[1] = float32x4_t{re[1], -im[0], re[2], 0.F};
    break;
  case 3:
    p_out[0] = float32x4_t{re[0], 0.F, re[1], im[0]};
    p_out[1] = float32x4_t{re[2], im[1], re[1], -im[0]};
    p_out[2] = float32x4_t{re[3], 0.F, re[4], im[2]};
    p_out[3] = float32x4_t{re[2], -im[1], re[4], -im[2]};
    p_out_extra = float32x2_t{re[5], 0.F};
    break;
  case 4:
    p_out[0] = float32x4_t{re[0], 0.F, re[1], im[0]};
    p_out[1] = float32x4_t{re[2], im[1], re[3], im[2]};
    p_out[2] = float32x4_t{re[1], -im[0], re[4], 0.F};
    p_out[3] = float32x4_t{re[5], im[3], re[6], im[4]};
    p_out[4] = float32x4_t{re[2], -im[1], re[5], -im[3]};
    p_out[5] = float32x4_t{re[7], 0.F, re[8], im[5]};
    p_out[6] = float32x4_t{re[3], -im[2], re[6], -im[4]};
    p_out[7] = float32x4_t{re[8], -im[5], re[9], 0.F};
    break;
  default:
    return ARMRAL_ARGUMENT_ERROR;
  }

  for (int i = 0; i < num_vecs; i++) {
    vst1q_f32((float32_t *)&p_dst_c[2 * i], p_out[i]);
  }
  if (M % 2 != 0) {
    vst1_f32((float32_t *)&p_dst_c[2 * num_vecs], p_out_extra);
  }
#endif
  return ARMRAL_SUCCESS;
}

} // namespace

armral_status
armral_cmplx_matmul_aah_f32(uint16_t m, uint16_t n,
                            const armral_cmplx_f32_t *__restrict p_src_a,
                            armral_cmplx_f32_t *p_dst_c) {
  switch (m) {
  case 2:
    return armral_cmplx_matmul_aah_f32_m<2>(n, p_src_a, p_dst_c);
  case 3:
    return armral_cmplx_matmul_aah_f32_m<3>(n, p_src_a, p_dst_c);
  case 4:
    return armral_cmplx_matmul_aah_f32_m<4>(n, p_src_a, p_dst_c);
  }
#ifdef ARMRAL_ARCH_SVE
  const uint32_t vals_per_vector = svcntd();
  const uint32_t vals_per_iter = 2 * vals_per_vector;
  const svbool_t pg_all = svptrue_b32();
  // For every row of A/row of C...
  for (uint32_t i = 0; i < m; i++) {
    // For every column of C "above" the leading diagonal...
    // We don't need to calculate the values below the diagonal, as
    // C(i,j) = conj(C(j,i)).
    for (uint32_t j = i; j < m; j++) {
      uint32_t c_idx = i * m + j;
      uint32_t ch_idx = j * m + i;
      uint32_t k = n / vals_per_vector * vals_per_vector;
      svfloat32_t p_out_re = svdup_n_f32(0.F);
      svfloat32_t p_out_im = svdup_n_f32(0.F);
      if (k < n) {
        svbool_t pg_remainder = svwhilelt_b32_u32(k * 2, n * 2);
        svfloat32_t p_in_a =
            svld1_f32(pg_remainder, (const float32_t *)&p_src_a[i * n + k]);
        svfloat32_t p_in_ah =
            svld1_f32(pg_remainder, (const float32_t *)&p_src_a[j * n + k]);

        // c.re = a.re * ah.re + a.im * ah.im
        // This performs the multiplication of the real & imaginary components
        // simultaneously, with the result stored in an accumulator vector.
        // The accumulator is ADDV'd at the end.
        p_out_re = svmla_f32_m(pg_remainder, p_out_re, p_in_a, p_in_ah);

        if (i != j) {
          p_in_ah = svreinterpret_f32_s64(
              svrevw_s64_x(pg_remainder, svreinterpret_s64_f32(p_in_ah)));
          // c.im = a.im * ah.re - a.re * ah.im
          // We've reshuffled the A^H vector such that a.re is multiplied with
          // ah.im, and vice versa...
          p_out_im = svmls_f32_m(pg_remainder, p_out_im, p_in_a, p_in_ah);
        }
      }
      k = n / vals_per_iter * vals_per_iter;
      if (k + vals_per_vector <= n) {
        svfloat32_t p_in_a =
            svld1_f32(pg_all, (const float32_t *)&p_src_a[i * n + k]);
        svfloat32_t p_in_ah =
            svld1_f32(pg_all, (const float32_t *)&p_src_a[j * n + k]);

        // c.re = a.re * ah.re + a.im * ah.im
        // This performs the multiplication of the real & imaginary components
        // simultaneously, with the result stored in an accumulator vector.
        // The accumulator is ADDV'd at the end.
        p_out_re = svmla_f32_m(pg_all, p_out_re, p_in_a, p_in_ah);

        if (i != j) {
          p_in_a = svreinterpret_f32_s64(
              svrevw_s64_x(pg_all, svreinterpret_s64_f32(p_in_a)));
          // c.im = a.im * ah.re - a.re * ah.im
          // We've reshuffled the A^H vector such that a.re is multiplied with
          // ah.im, and vice versa...
          p_out_im = svmla_f32_m(pg_all, p_out_im, p_in_a, p_in_ah);
        }
      }
      // For every column of A/row of A^H...
      for (k = 0; k + vals_per_iter <= n; k += vals_per_iter) {
        svfloat32_t p_in_a1 =
            svld1_f32(pg_all, (const float32_t *)&p_src_a[i * n + k]);
        svfloat32_t p_in_a2 = svld1_f32(
            pg_all, (const float32_t *)&p_src_a[i * n + k + vals_per_vector]);
        svfloat32_t p_in_ah1 =
            svld1_f32(pg_all, (const float32_t *)&p_src_a[j * n + k]);
        svfloat32_t p_in_ah2 = svld1_f32(
            pg_all, (const float32_t *)&p_src_a[j * n + k + vals_per_vector]);

        // c.re = a.re * ah.re + a.im * ah.im
        // This performs the multiplication of the real & imaginary components
        // simultaneously, with the result stored in an accumulator vector.
        // The accumulator is ADDV'd at the end.
        p_out_re = svmla_f32_x(pg_all, p_out_re, p_in_a1, p_in_ah1);
        p_out_re = svmla_f32_x(pg_all, p_out_re, p_in_a2, p_in_ah2);

        if (i != j) {
          p_in_ah1 = svreinterpret_f32_s64(
              svrevw_s64_x(pg_all, svreinterpret_s64_f32(p_in_ah1)));
          p_in_ah2 = svreinterpret_f32_s64(
              svrevw_s64_x(pg_all, svreinterpret_s64_f32(p_in_ah2)));
          // c.im = a.im * ah.re - a.re * ah.im
          // We've reshuffled the A^H vector such that a.re is multiplied with
          // ah.im, and vice versa...
          p_out_im = svmls_f32_x(pg_all, p_out_im, p_in_a1, p_in_ah1);
          p_out_im = svmls_f32_x(pg_all, p_out_im, p_in_a2, p_in_ah2);
        }
      }
      p_out_im = svreinterpret_f32_f64(
          svneg_f64_x(pg_all, svreinterpret_f64_f32(p_out_im)));
      p_dst_c[c_idx].re = svaddv_f32(pg_all, p_out_re);
      p_dst_c[c_idx].im = svaddv_f32(pg_all, p_out_im);
      if (i != j) {
        p_dst_c[ch_idx].re = p_dst_c[c_idx].re;
        p_dst_c[ch_idx].im = p_dst_c[c_idx].im * -1.F;
      }
    }
  }
#else
  // For every row of A/row of C...
  for (uint32_t i = 0; i < m; i++) {
    // For every column of C "above" the leading diagonal...
    // We don't need to calculate the values below the diagonal, as
    // C(i,j) = conj(C(j,i)).
    for (uint32_t j = i; j < m; j++) {
      uint32_t c_idx = i * m + j;
      uint32_t ch_idx = j * m + i;
      float32_t re = 0.F;
      float32_t im = 0.F;
      uint32_t k;
      float32x4x2_t p_out = {{vdupq_n_f32(0.F), vdupq_n_f32(0.F)}};
      // For every column of A/row of A^H...
      for (k = 0; k + 3 < n; k += 4) {
        float32x4_t p_in_a[] = {
            vld1q_f32((const float32_t *)&p_src_a[i * n + k]),
            vld1q_f32((const float32_t *)&p_src_a[i * n + k + 2])};
        float32x4_t p_in_ah[] = {
            vld1q_f32((const float32_t *)&p_src_a[j * n + k]),
            vld1q_f32((const float32_t *)&p_src_a[j * n + k + 2])};

        // c.re = a.re * ah.re + a.im * ah.im
        // This performs the multiplication of the real & imaginary components
        // simultaneously, with the result stored in an accumulator vector.
        // The accumulator is ADDV'd at the end.
        p_out.val[0] = vfmaq_f32(p_out.val[0], p_in_a[0], p_in_ah[0]);
        p_out.val[0] = vfmaq_f32(p_out.val[0], p_in_a[1], p_in_ah[1]);

        if (i != j) {
          p_in_ah[0] = vrev64q_f32(p_in_ah[0]);
          p_in_ah[1] = vrev64q_f32(p_in_ah[1]);
          // c.im = a.im * ah.re - a.re * ah.im
          // We've reshuffled the A vector such that a.re is multiplied with
          // ah.im, and vice versa...
          p_out.val[1] = vfmsq_f32(p_out.val[1], p_in_a[0], p_in_ah[0]);
          p_out.val[1] = vfmsq_f32(p_out.val[1], p_in_a[1], p_in_ah[1]);
        }
      }
      if (n % 4 >= 2) {
        float32x4_t p_in_a = vld1q_f32((const float32_t *)&p_src_a[i * n + k]);
        float32x4_t p_in_ah = vld1q_f32((const float32_t *)&p_src_a[j * n + k]);

        // c.re = a.re * ah.re + a.im * ah.im
        // This performs the multiplication of the real & imaginary components
        // simultaneously, with the result stored in an accumulator vector.
        // The accumulator is ADDV'd at the end.
        p_out.val[0] = vfmaq_f32(p_out.val[0], p_in_a, p_in_ah);

        if (i != j) {
          p_in_ah = vrev64q_f32(p_in_ah);
          // c.im = a.im * ah.re - a.re * ah.im
          // We've reshuffled the A^H vector such that a.re is multiplied with
          // ah.im, and vice versa...
          p_out.val[1] = vfmsq_f32(p_out.val[1], p_in_a, p_in_ah);
        }
      }
      re = vaddvq_f32(p_out.val[0]);
      if (i != j) {
        p_out.val[1] = vreinterpretq_f32_f64(
            vnegq_f64(vreinterpretq_f64_f32(p_out.val[1])));
        im = vaddvq_f32(p_out.val[1]);
      }
      if (n % 2 != 0) {
        k = n - 1;
        uint32_t a_idx = (i * n + k);
        uint32_t ah_idx = (j * n + k);

        re += p_src_a[a_idx].re * p_src_a[ah_idx].re +
              p_src_a[a_idx].im * p_src_a[ah_idx].im;
        if (i != j) {
          im += p_src_a[a_idx].im * p_src_a[ah_idx].re -
                p_src_a[a_idx].re * p_src_a[ah_idx].im;
        }
      }
      p_dst_c[c_idx].re = re;
      p_dst_c[c_idx].im = im;
      if (i != j) {
        p_dst_c[ch_idx].re = re;
        p_dst_c[ch_idx].im = -im;
      }
    }
  }
#endif
  return ARMRAL_SUCCESS;
}
