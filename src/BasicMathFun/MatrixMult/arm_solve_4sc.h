/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"

armral_status armral_solve_2x2_4sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    armral_fixed_point_index num_fract_bits_x);

armral_status armral_solve_2x4_4sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    armral_fixed_point_index num_fract_bits_x);

armral_status armral_solve_4x4_4sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride, const armral_fixed_point_index *p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x, uint32_t p_xstride,
    armral_fixed_point_index num_fract_bits_x);

armral_status armral_solve_1x4_4sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    armral_fixed_point_index num_fract_bits_x);

armral_status armral_solve_1x2_4sc_f32(
    uint32_t num_sub_carrier, const armral_cmplx_int16_t *restrict p_y,
    uint32_t p_ystride,
    const armral_fixed_point_index *restrict p_y_num_fract_bits,
    const float32_t *restrict p_g_real, const float32_t *restrict p_g_imag,
    uint32_t p_gstride, armral_cmplx_int16_t *p_x,
    armral_fixed_point_index num_fract_bits_x);
