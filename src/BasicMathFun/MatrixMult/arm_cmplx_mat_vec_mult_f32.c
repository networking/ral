/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"
#include <assert.h>

#if ARMRAL_ARCH_SVE >= 2
#include <arm_sve.h>
#endif

armral_status
armral_cmplx_mat_vec_mult_f32(const uint16_t m, const uint16_t n,
                              const armral_cmplx_f32_t *restrict p_src_a,
                              const armral_cmplx_f32_t *restrict p_src_x,
                              armral_cmplx_f32_t *p_dst) {
  const float32_t *p_in1 = (const float32_t *)p_src_a;
  const float32_t *p_in2 = (const float32_t *)p_src_x;
  const armral_cmplx_f32_t *p_in_a = p_src_a;
  armral_cmplx_f32_t *p_out = p_dst;
  uint16_t num_rows_a = m; // number of rows of input matrix A
  uint16_t num_cols_a = n; // number of columns of input matrix A

#if ARMRAL_ARCH_SVE >= 2
  svbool_t ptrue = svptrue_b32();
  if (num_rows_a % 2 == 0) {
    const float32_t *p_in1_2 = (const float32_t *)p_src_a;
    // Loop over A two rows at a time
    for (uint16_t row_cnt = num_rows_a; row_cnt > 0;
         row_cnt -= 2, p_out += 2, p_in_a += 2 * num_cols_a) {
      // Initialize p_in1 and p_in1_2 to point to the starting addresses of the
      // current rows
      p_in1 = (const float32_t *)p_in_a;
      p_in1_2 = p_in1 + 2 * num_cols_a;

      // For every row wise process, the pIn2 pointer is set
      // to the starting address of the pSrcX data
      p_in2 = (const float32_t *)p_src_x;

      // Initialize accumulators
      svfloat32_t sum = svdup_n_f32(0);
      svfloat32_t sum2 = svdup_n_f32(0);
      svfloat32_t sum3 = svdup_n_f32(0);
      svfloat32_t sum4 = svdup_n_f32(0);

      // Do tail loop first
      uint32_t col_cnt = num_cols_a & 3;
      svbool_t pg;
      if (col_cnt != 0) {
        uint32_t num_lanes = svcntw();
        uint32_t num_vals = col_cnt * 2;
        for (uint32_t i = 0; i < num_vals; i += num_lanes) {
          pg = svwhilelt_b32(i, num_vals);
          svfloat32_t a = svld1_f32(pg, p_in1 + i);
          svfloat32_t a2 = svld1_f32(pg, p_in1_2 + i);
          svfloat32_t x = svld1_f32(pg, p_in2 + i);
          sum = svcmla_f32_m(pg, sum, a, x, 0);
          sum = svcmla_f32_m(pg, sum, a, x, 90);
          sum3 = svcmla_f32_m(pg, sum3, a2, x, 0);
          sum3 = svcmla_f32_m(pg, sum3, a2, x, 90);
        }
        if (num_cols_a < 4) {
          p_out[0].re = svaddv_f32(ptrue, svtrn1(sum, sum2));
          p_out[0].im = svaddv_f32(ptrue, svtrn2(sum, sum2));
          p_out[1].re = svaddv_f32(ptrue, svtrn1(sum3, sum4));
          p_out[1].im = svaddv_f32(ptrue, svtrn2(sum3, sum4));
          continue;
        }
        p_in1 += num_vals;
        p_in1_2 += num_vals;
        p_in2 += num_vals;
      }

      // Do 4 columns at a time
      col_cnt = num_cols_a >> 2;
      pg = svptrue_pat_b32(SV_VL4);
      while (col_cnt > 0U) {
        svfloat32_t a = svld1_f32(pg, p_in1);
        svfloat32_t a2 = svld1_f32(pg, &p_in1[4]);
        svfloat32_t a3 = svld1_f32(pg, p_in1_2);
        svfloat32_t a4 = svld1_f32(pg, &p_in1_2[4]);
        svfloat32_t x = svld1_f32(pg, p_in2);
        svfloat32_t x2 = svld1_f32(pg, &p_in2[4]);
        sum = svcmla_f32_x(pg, sum, a, x, 0);
        sum = svcmla_f32_x(pg, sum, a, x, 90);
        sum2 = svcmla_f32_x(pg, sum2, a2, x2, 0);
        sum2 = svcmla_f32_x(pg, sum2, a2, x2, 90);
        sum3 = svcmla_f32_x(pg, sum3, a3, x, 0);
        sum3 = svcmla_f32_x(pg, sum3, a3, x, 90);
        sum4 = svcmla_f32_x(pg, sum4, a4, x2, 0);
        sum4 = svcmla_f32_x(pg, sum4, a4, x2, 90);
        p_in1 += 8U;
        p_in2 += 8U;
        p_in1_2 += 8U;
        --col_cnt;
      }

      // Store result
      p_out[0].re = svaddv_f32(ptrue, svtrn1(sum, sum2));
      p_out[0].im = svaddv_f32(ptrue, svtrn2(sum, sum2));
      p_out[1].re = svaddv_f32(ptrue, svtrn1(sum3, sum4));
      p_out[1].im = svaddv_f32(ptrue, svtrn2(sum3, sum4));
    }
  } else {
    // Loop over A one row at a time
    for (uint16_t row_cnt = num_rows_a; row_cnt > 0;
         --row_cnt, p_out++, p_in_a += num_cols_a) {

      // Initialize p_in1 to point to the starting address of the current row
      p_in1 = (const float32_t *)p_in_a;

      // For every row wise process, the pIn2 pointer is set
      // to the starting address of the pSrcX data
      p_in2 = (const float32_t *)p_src_x;

      // Initialize accumulators
      svfloat32_t sum = svdup_n_f32(0);
      svfloat32_t sum2 = svdup_n_f32(0);

      // Do tail loop first
      uint32_t col_cnt = num_cols_a & 3;
      svbool_t pg;
      if (col_cnt != 0) {
        uint32_t num_lanes = svcntw();
        uint32_t num_vals = col_cnt * 2;
        for (uint32_t i = 0; i < num_vals; i += num_lanes) {
          pg = svwhilelt_b32(i, num_vals);
          svfloat32_t a = svld1_f32(pg, p_in1 + i);
          svfloat32_t x = svld1_f32(pg, p_in2 + i);
          sum = svcmla_f32_m(pg, sum, a, x, 0);
          sum = svcmla_f32_m(pg, sum, a, x, 90);
        }
        if (num_cols_a < 4) {
          p_out->re = svaddv_f32(ptrue, svtrn1(sum, sum2));
          p_out->im = svaddv_f32(ptrue, svtrn2(sum, sum2));
          continue;
        }
        p_in1 += num_vals;
        p_in2 += num_vals;
      }

      // Do 4 columns at a time
      col_cnt = num_cols_a >> 2;

      pg = svptrue_pat_b32(SV_VL4);
      while (col_cnt > 0U) {
        svfloat32_t a = svld1_f32(pg, p_in1);
        svfloat32_t a2 = svld1_f32(pg, &p_in1[4]);
        svfloat32_t x = svld1_f32(pg, p_in2);
        svfloat32_t x2 = svld1_f32(pg, &p_in2[4]);
        sum = svcmla_f32_x(pg, sum, a, x, 0);
        sum = svcmla_f32_x(pg, sum, a, x, 90);
        sum2 = svcmla_f32_x(pg, sum2, a2, x2, 0);
        sum2 = svcmla_f32_x(pg, sum2, a2, x2, 90);
        p_in1 += 8U;
        p_in2 += 8U;
        --col_cnt;
      }

      // Store result
      p_out->re = svaddv_f32(ptrue, svtrn1(sum, sum2));
      p_out->im = svaddv_f32(ptrue, svtrn2(sum, sum2));
    }
#else
  // Loop over A one row at a time
  for (uint16_t row_cnt = num_rows_a; row_cnt > 0;
       --row_cnt, ++p_out, p_in_a += num_cols_a) {

    // Initialize p_in1 to point to the starting address of the current row
    p_in1 = (const float32_t *)p_in_a;

    // For every row wise process, the pIn2 pointer is set
    // to the starting address of the pSrcX data
    p_in2 = (const float32_t *)p_src_x;

    // Initialize accumulators
    float32_t acc_re = 0.0;
    float32_t acc_im = 0.0;

    // Do 4 columns at a time
    uint32_t col_cnt = num_cols_a >> 2;

    if (col_cnt > 0) {
      float32x4_t sum_re = vdupq_n_f32(0);
      float32x4_t sum_im = vdupq_n_f32(0);
      while (col_cnt > 0U) {
        float32x4_t a = vld1q_f32(p_in1);
        float32x4_t x = vld1q_f32(p_in2);
        sum_re = vfmaq_f32(sum_re, a, x);
        sum_im = vfmaq_f32(sum_im, a, vrev64q_f32(x));

        a = vld1q_f32(&p_in1[4]);
        x = vld1q_f32(&p_in2[4]);
        sum_re = vfmaq_f32(sum_re, a, x);
        sum_im = vfmaq_f32(sum_im, a, vrev64q_f32(x));

        p_in1 += 8U;
        p_in2 += 8U;
        --col_cnt;
      }
      acc_re += vaddvq_f32(
          vreinterpretq_f32_f64((vnegq_f64(vreinterpretq_f64_f32(sum_re)))));
      acc_im += vaddvq_f32(sum_im);
    }

    // Remaining columns
    col_cnt = num_cols_a & 3;
    float32x2_t sum1 = vdup_n_f32(0);
    float32x2_t sum2 = vdup_n_f32(0);
    if (col_cnt) {
      while (col_cnt > 0U) {
        float32x2_t a = vld1_f32(p_in1);
        float32x2_t x = vld1_f32(p_in2);
        sum1 = vmla_f32(sum1, a, x);
        sum2 = vmla_f32(sum2, a, vrev64_f32(x));
        p_in1 += 2U;
        p_in2 += 2U;
        --col_cnt;
      }
      acc_re += vaddv_f32(
          vreinterpret_f32_f64((vneg_f64(vreinterpret_f64_f32(sum1)))));
      acc_im += vaddv_f32(sum2);
    }

    // Store results
    p_out->re = acc_re;
    p_out->im = acc_im;
#endif
  }

  return ARMRAL_SUCCESS;
}

static inline armral_status __attribute__((always_inline))
cmplx_mat_vec_mult_batch_one_vec(uint16_t num_mats, uint16_t m, uint16_t n,
                                 const armral_cmplx_f32_t *restrict p_src_a,
                                 const armral_cmplx_f32_t *restrict p_src_x,
                                 armral_cmplx_f32_t *p_dst) {
  assert(num_mats % 2 == 0);
  // Process two matrix-vector multiplications at a time.
  // Note: This is the same as num_vecs_per_mat tail, but allows for some slight
  // simplification from things being more contiguous.
  for (uint32_t batch = 0; batch < num_mats; batch += 2) {
    const armral_cmplx_f32_t *a_current_row_start = &p_src_a[batch];
    const armral_cmplx_f32_t *x_first_element = &p_src_x[batch];
    armral_cmplx_f32_t *out_ptr = &p_dst[batch];

    for (uint32_t row = 0; row < m; row++) {
      const armral_cmplx_f32_t *current_a = a_current_row_start;
      const armral_cmplx_f32_t *current_x = x_first_element;

      float32x2_t acc_1;
      float32x2_t acc_2;
      float32x2_t acc_3;
      float32x2_t acc_4;
      {
        float32x4_t a_vec = vld1q_f32((float32_t const *)current_a);
        float32x2x2_t x_vec = vld1_f32_x2((float32_t const *)current_x);
        acc_1 = vmul_laneq_f32(x_vec.val[0], a_vec, 0);
        acc_2 = vmul_laneq_f32(x_vec.val[0], a_vec, 1);
        acc_3 = vmul_laneq_f32(x_vec.val[1], a_vec, 2);
        acc_4 = vmul_laneq_f32(x_vec.val[1], a_vec, 3);
        current_x += num_mats;
        current_a += num_mats;
      }
      for (uint32_t col = 1; col < n; col++) {
        float32x4_t a_vec = vld1q_f32((float32_t const *)current_a);
        float32x2x2_t x_vec = vld1_f32_x2((float32_t const *)current_x);
        acc_1 = vfma_laneq_f32(acc_1, x_vec.val[0], a_vec, 0);
        acc_2 = vfma_laneq_f32(acc_2, x_vec.val[0], a_vec, 1);
        acc_3 = vfma_laneq_f32(acc_3, x_vec.val[1], a_vec, 2);
        acc_4 = vfma_laneq_f32(acc_4, x_vec.val[1], a_vec, 3);
        current_x += num_mats;
        current_a += num_mats;
      }

      float32x2_t result_1 =
          vadd_f32(acc_1, vneg64_f32(vneg_f32(vrev64_f32(acc_2))));
      float32x2_t result_2 =
          vadd_f32(acc_3, vneg64_f32(vneg_f32(vrev64_f32(acc_4))));
      vst1_f32((float32_t *)(out_ptr + 0), result_1);
      vst1_f32((float32_t *)(out_ptr + 1), result_2);

      a_current_row_start += num_mats * n;
      out_ptr += num_mats;
    }
  }
  return ARMRAL_SUCCESS;
}

armral_status armral_cmplx_mat_vec_mult_batch_f32(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_f32_t *restrict p_src_a,
    const armral_cmplx_f32_t *restrict p_src_x, armral_cmplx_f32_t *p_dst) {
  // Note: Like armral_cmplx_mat_vec_mult_batch_i16() this assumes
  // num_vecs_per_mat * num_mats is a multiple of 12 to avoid some extra tails.
  if ((num_vecs_per_mat * num_mats) % 12 != 0) {
    return ARMRAL_ARGUMENT_ERROR;
  }
  const int vec_stride = num_mats * num_vecs_per_mat;
  if (num_vecs_per_mat == 1) {
    return cmplx_mat_vec_mult_batch_one_vec(num_mats, m, n, p_src_a, p_src_x,
                                            p_dst);
  }
  uint32_t batch = 0;
  // Process two matrices at a time.
  for (; batch + 2 <= num_mats; batch += 2) {
    uint32_t vec = 0;
    // Process two vectors per matrix each time.
    for (; vec + 2 <= num_vecs_per_mat; vec += 2) {
      int vec_idx_1 = (batch * num_vecs_per_mat) + vec;
      int vec_idx_2 = ((batch + 1) * num_vecs_per_mat) + vec;

      const armral_cmplx_f32_t *a_current_row_start = &p_src_a[batch];
      const armral_cmplx_f32_t *x_first_element_1 = &p_src_x[vec_idx_1];
      const armral_cmplx_f32_t *x_first_element_2 = &p_src_x[vec_idx_2];
      armral_cmplx_f32_t *out_ptr_1 = &p_dst[vec_idx_1];
      armral_cmplx_f32_t *out_ptr_2 = &p_dst[vec_idx_2];

      for (uint32_t row = 0; row < m; row++) {
        const armral_cmplx_f32_t *current_a = a_current_row_start;
        const armral_cmplx_f32_t *current_x_1 = x_first_element_1;
        const armral_cmplx_f32_t *current_x_2 = x_first_element_2;

        float32x4_t acc_1;
        float32x4_t acc_2;
        float32x4_t acc_3;
        float32x4_t acc_4;
        {
          float32x4_t a_vec = vld1q_f32((float32_t const *)current_a);
          float32x4_t x1_vec = vld1q_f32((float32_t const *)current_x_1);
          float32x4_t x2_vec = vld1q_f32((float32_t const *)current_x_2);
          acc_1 = vmulq_laneq_f32(x1_vec, a_vec, 0);
          acc_2 = vmulq_laneq_f32(x1_vec, a_vec, 1);
          acc_3 = vmulq_laneq_f32(x2_vec, a_vec, 2);
          acc_4 = vmulq_laneq_f32(x2_vec, a_vec, 3);
          current_x_1 += vec_stride;
          current_x_2 += vec_stride;
          current_a += num_mats;
        }
        for (uint32_t col = 1; col < n; col++) {
          float32x4_t a_vec = vld1q_f32((float32_t const *)current_a);
          float32x4_t x1_vec = vld1q_f32((float32_t const *)current_x_1);
          float32x4_t x2_vec = vld1q_f32((float32_t const *)current_x_2);
          acc_1 = vfmaq_laneq_f32(acc_1, x1_vec, a_vec, 0);
          acc_2 = vfmaq_laneq_f32(acc_2, x1_vec, a_vec, 1);
          acc_3 = vfmaq_laneq_f32(acc_3, x2_vec, a_vec, 2);
          acc_4 = vfmaq_laneq_f32(acc_4, x2_vec, a_vec, 3);
          current_x_1 += vec_stride;
          current_x_2 += vec_stride;
          current_a += num_mats;
        }

        float32x4_t result_1 =
            vaddq_f32(acc_1, vnegq64_f32(vnegq_f32(vrev64q_f32(acc_2))));
        float32x4_t result_2 =
            vaddq_f32(acc_3, vnegq64_f32(vnegq_f32(vrev64q_f32(acc_4))));
        vst1q_f32((float32_t *)out_ptr_1, result_1);
        vst1q_f32((float32_t *)out_ptr_2, result_2);

        a_current_row_start += num_mats * n;
        out_ptr_1 += vec_stride;
        out_ptr_2 += vec_stride;
      }
    }
    if (num_vecs_per_mat % 2 == 1) {
      // Tail: Process one vector per matrix.
      int vec_idx_1 = (batch * num_vecs_per_mat) + vec;
      int vec_idx_2 = ((batch + 1) * num_vecs_per_mat) + vec;

      const armral_cmplx_f32_t *a_current_row_start = &p_src_a[batch];
      const armral_cmplx_f32_t *x_first_element_1 = &p_src_x[vec_idx_1];
      const armral_cmplx_f32_t *x_first_element_2 = &p_src_x[vec_idx_2];
      armral_cmplx_f32_t *out_ptr_1 = &p_dst[vec_idx_1];
      armral_cmplx_f32_t *out_ptr_2 = &p_dst[vec_idx_2];

      for (uint32_t row = 0; row < m; row++) {
        const armral_cmplx_f32_t *current_a = a_current_row_start;
        const armral_cmplx_f32_t *current_x_1 = x_first_element_1;
        const armral_cmplx_f32_t *current_x_2 = x_first_element_2;

        float32x2_t acc_1;
        float32x2_t acc_2;
        float32x2_t acc_3;
        float32x2_t acc_4;
        {
          float32x4_t a_vec = vld1q_f32((float32_t const *)current_a);
          float32x2_t x1_vec = vld1_f32((float32_t const *)current_x_1);
          float32x2_t x2_vec = vld1_f32((float32_t const *)current_x_2);
          acc_1 = vmul_laneq_f32(x1_vec, a_vec, 0);
          acc_2 = vmul_laneq_f32(x1_vec, a_vec, 1);
          acc_3 = vmul_laneq_f32(x2_vec, a_vec, 2);
          acc_4 = vmul_laneq_f32(x2_vec, a_vec, 3);
          current_x_1 += vec_stride;
          current_x_2 += vec_stride;
          current_a += num_mats;
        }
        for (uint32_t col = 1; col < n; col++) {
          float32x4_t a_vec = vld1q_f32((float32_t const *)current_a);
          float32x2_t x1_vec = vld1_f32((float32_t const *)current_x_1);
          float32x2_t x2_vec = vld1_f32((float32_t const *)current_x_2);
          acc_1 = vfma_laneq_f32(acc_1, x1_vec, a_vec, 0);
          acc_2 = vfma_laneq_f32(acc_2, x1_vec, a_vec, 1);
          acc_3 = vfma_laneq_f32(acc_3, x2_vec, a_vec, 2);
          acc_4 = vfma_laneq_f32(acc_4, x2_vec, a_vec, 3);
          current_x_1 += vec_stride;
          current_x_2 += vec_stride;
          current_a += num_mats;
        }

        float32x2_t result_1 =
            vadd_f32(acc_1, vneg64_f32(vneg_f32(vrev64_f32(acc_2))));
        float32x2_t result_2 =
            vadd_f32(acc_3, vneg64_f32(vneg_f32(vrev64_f32(acc_4))));
        vst1_f32((float32_t *)out_ptr_1, result_1);
        vst1_f32((float32_t *)out_ptr_2, result_2);

        a_current_row_start += num_mats * n;
        out_ptr_1 += vec_stride;
        out_ptr_2 += vec_stride;
      }
    }
  }
  if (num_mats % 2 == 1) {
    // Tail: Process one matrix at a time, for two different input vectors.
    for (uint32_t vec = 0; vec < num_vecs_per_mat; vec += 2) {
      int vec_idx = (batch * num_vecs_per_mat) + vec;

      const armral_cmplx_f32_t *a_current_row_start = &p_src_a[batch];
      const armral_cmplx_f32_t *x_first_element = &p_src_x[vec_idx];
      armral_cmplx_f32_t *out_ptr = &p_dst[vec_idx];

      for (uint32_t row = 0; row < m; row++) {
        const armral_cmplx_f32_t *current_a = a_current_row_start;
        const armral_cmplx_f32_t *current_x = x_first_element;

        float32x4_t acc_1;
        float32x4_t acc_2;
        {
          float32x2_t a_vec = vld1_f32((float32_t const *)current_a);
          float32x4_t x_vec = vld1q_f32((float32_t const *)current_x);
          acc_1 = vmulq_lane_f32(x_vec, a_vec, 0);
          acc_2 = vmulq_lane_f32(x_vec, a_vec, 1);
          current_a += num_mats;
          current_x += vec_stride;
        }
        for (uint32_t col = 1; col < n; col++) {
          float32x2_t a_vec = vld1_f32((float32_t const *)current_a);
          float32x4_t x_vec = vld1q_f32((float32_t const *)current_x);
          acc_1 = vfmaq_lane_f32(acc_1, x_vec, a_vec, 0);
          acc_2 = vfmaq_lane_f32(acc_2, x_vec, a_vec, 1);
          current_a += num_mats;
          current_x += vec_stride;
        }

        float32x4_t result =
            vaddq_f32(acc_1, vnegq64_f32(vnegq_f32(vrev64q_f32(acc_2))));
        vst1q_f32((float32_t *)out_ptr, result);

        out_ptr += vec_stride;
        a_current_row_start += num_mats * n;
      }
    }
  }

  return ARMRAL_SUCCESS;
}

armral_status armral_cmplx_mat_vec_mult_batch_f32_pa(
    uint16_t num_mats, uint16_t num_vecs_per_mat, uint16_t m, uint16_t n,
    const armral_cmplx_f32_t **__restrict p_srcs_a,
    const armral_cmplx_f32_t **__restrict p_srcs_x,
    armral_cmplx_f32_t **__restrict p_dsts) {
  // The i-th element of the j-th matrix in the batch is
  // located at `p_srcs[i][j]`. Similarly, the j-th matrix in a batch of `2 x 2`
  // matrices is formed as:
  //  p_srcs[0][j]  p_srcs[1][j]
  //  p_srcs[2][j]  p_srcs[3][j]

  for (uint16_t mat_idx = 0; mat_idx < num_mats; ++mat_idx) {
    for (uint16_t vec = 0; vec < num_vecs_per_mat; ++vec) {
      // Do one matrix-vector multiplication at a time
      // Work out the starting index of the current vector in p_src_x
      // and p_dst
      int vec_idx = (mat_idx * num_vecs_per_mat) + vec;

      // Loop over A one row at a time
      for (uint16_t row_cnt = 0; row_cnt < m; ++row_cnt) {
        // Set the accumulator variables to zero
        float32_t sum_real1 = 0;
        float32_t sum_imag1 = 0;
        float32_t sum_real2 = 0;
        float32_t sum_imag2 = 0;

        // Loop over the row of A one column at a time
        for (uint16_t col_cnt = 0; col_cnt < n; ++col_cnt) {
          int i = (row_cnt * n) + col_cnt;
          float32_t a_re = p_srcs_a[i][mat_idx].re;
          float32_t a_im = p_srcs_a[i][mat_idx].im;
          float32_t x_re = p_srcs_x[col_cnt][vec_idx].re;
          float32_t x_im = p_srcs_x[col_cnt][vec_idx].im;
          sum_real1 += a_re * x_re;
          sum_imag1 += a_im * x_re;
          sum_real2 -= a_im * x_im;
          sum_imag2 += a_re * x_im;
        }

        sum_real1 += sum_real2;
        sum_imag1 += sum_imag2;

        // Store the result in the destination buffer
        p_dsts[row_cnt][vec_idx] = (armral_cmplx_f32_t){sum_real1, sum_imag1};
      }
    }
  }

  return ARMRAL_SUCCESS;
}
