/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

namespace armral::cmplx_mat_pseudo_inv {

void mat_mult_bah_f32(uint16_t m, uint16_t n,
                      const armral_cmplx_f32_t *__restrict p_src_a,
                      const armral_cmplx_f32_t *__restrict p_src_b,
                      armral_cmplx_f32_t *p_dst) {
  // For input matrices A and B, computes B * A^H
  for (uint16_t i = 0; i < n; i++) {
    for (uint16_t j = 0; j < m; j++) {
      float32_t re = 0.0;
      float32_t im = 0.0;
      float32x4x2_t p_out = {{vdupq_n_f32(0.F), vdupq_n_f32(0.F)}};

      uint16_t k = 0;
      for (; k + 3 < n; k += 4) {
        uint32_t b_idx = i * n + k;
        uint32_t ah_idx = j * n + k;

        float32x4_t p_in_b[] = {
            vld1q_f32((const float32_t *)&p_src_b[b_idx]),
            vld1q_f32((const float32_t *)&p_src_b[b_idx + 2])};
        float32x4_t p_in_ah[] = {
            vld1q_f32((const float32_t *)&p_src_a[ah_idx]),
            vld1q_f32((const float32_t *)&p_src_a[ah_idx + 2])};

        // c.re = a.re * ah.re + a.im * ah.im
        p_out.val[0] = vfmaq_f32(p_out.val[0], p_in_b[0], p_in_ah[0]);
        p_out.val[0] = vfmaq_f32(p_out.val[0], p_in_b[1], p_in_ah[1]);

        // c.im = a.im * ah.re - a.re * ah.im
        p_in_ah[0] = vrev64q_f32(p_in_ah[0]);
        p_in_ah[1] = vrev64q_f32(p_in_ah[1]);
        p_out.val[1] = vfmsq_f32(p_out.val[1], p_in_b[0], p_in_ah[0]);
        p_out.val[1] = vfmsq_f32(p_out.val[1], p_in_b[1], p_in_ah[1]);
      }
      re = vaddvq_f32(p_out.val[0]);
      p_out.val[1] =
          vreinterpretq_f32_f64(vnegq_f64(vreinterpretq_f64_f32(p_out.val[1])));
      im = vaddvq_f32(p_out.val[1]);

      if (n % 4 != 0) {
        for (; k < n; k++) {
          uint32_t b_idx = i * n + k;
          uint32_t ah_idx = j * n + k;
          re += p_src_b[b_idx].re * p_src_a[ah_idx].re +
                p_src_b[b_idx].im * p_src_a[ah_idx].im;
          im += p_src_b[b_idx].im * p_src_a[ah_idx].re -
                p_src_b[b_idx].re * p_src_a[ah_idx].im;
        }
      }
      p_dst[i * m + j] = armral_cmplx_f32_t{re, im};
    }
  }
}

template<uint16_t dim>
void add_lambda(float32_t lambda, armral_cmplx_f32_t *p_dst) {
  // Adds lambda to the diagonals of a dim-by-dim matrix
  for (uint16_t i = 0; i < dim; i++) {
    p_dst[i * (dim + 1)].re += lambda;
  }
}

} // namespace armral::cmplx_mat_pseudo_inv
