/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "utils/allocators.hpp"

#include "../MatrixInv/cmplx_hermitian_mat_inversion_f32.hpp"
#include "cmplx_mat_pseudo_inverse.hpp"

#include <cstdlib>

namespace {

template<uint16_t n, typename Allocator>
void left_pseudo_inverse(uint16_t m, const float32_t lambda,
                         const armral_cmplx_f32_t *__restrict p_src,
                         armral_cmplx_f32_t *p_dst, Allocator &allocator) {

  // Compute C = A^H * A
  // We can use p_dst as an intermediate N-by-N array since it has size N-by-M,
  // and N < M
  auto *mat_aha = p_dst;
  armral_cmplx_matmul_ahb_f32(n, n, m, p_src, p_src, mat_aha);

  // Compute C += lambda * I
  armral::cmplx_mat_pseudo_inv::add_lambda<n>(lambda, mat_aha);

  // Compute B = C^(-1)
  auto mat_inv = allocate_uninitialized<armral_cmplx_f32_t>(allocator, n * n);
  if constexpr (n == 1) {
    mat_inv[0].re = 1.F / mat_aha[0].re;
    mat_inv[0].im = 0.F;
  } else {
    armral::cmplx_herm_mat_inv::invert_hermitian_matrix<n>(mat_aha,
                                                           mat_inv.get());
  }

  // Compute B * A^H
  armral::cmplx_mat_pseudo_inv::mat_mult_bah_f32(m, n, p_src, mat_inv.get(),
                                                 p_dst);
}

template<uint16_t m, typename Allocator>
void right_pseudo_inverse(uint16_t n, const float32_t lambda,
                          const armral_cmplx_f32_t *__restrict p_src,
                          armral_cmplx_f32_t *p_dst, Allocator &allocator) {
  // Compute C = A * A^H
  // We can use p_dst as an intermediate M-by-M array since it has size N-by-M,
  // and N >= M
  auto *mat_aah = p_dst;
  armral_cmplx_matmul_aah_f32(m, n, p_src, mat_aah);

  // Compute C += lambda * I
  armral::cmplx_mat_pseudo_inv::add_lambda<m>(lambda, mat_aah);

  // Compute B = C^(-1)
  auto mat_inv = allocate_uninitialized<armral_cmplx_f32_t>(allocator, m * m);
  if constexpr (m == 1) {
    mat_inv[0].re = 1.F / mat_aah[0].re;
    mat_inv[0].im = 0.F;
  } else {
    armral::cmplx_herm_mat_inv::invert_hermitian_matrix<m>(mat_aah,
                                                           mat_inv.get());
  }

  // Compute A^H * B
  armral_cmplx_matmul_ahb_f32(n, m, m, p_src, mat_inv.get(), p_dst);
}

template<typename Allocator>
armral_status
cmplx_pseudo_inverse_direct(uint16_t m, uint16_t n, const float32_t lambda,
                            const armral_cmplx_f32_t *__restrict p_src,
                            armral_cmplx_f32_t *p_dst, Allocator &allocator) {

  // This routine uses the Hermitian matrix inversion routine defined in the
  // library (armral_cmplx_hermitian_max_inverse_f32) which is only valid for
  // particular matrix sizes. This places a restriction on the number of rows
  // that the input matrix A can have here.

  // If the number of rows in the input matrix is larger than the number of
  // columns then use the left pseudo-inverse
  if (m > n) {
    switch (n) {
    case 1: {
      left_pseudo_inverse<1>(m, lambda, p_src, p_dst, allocator);
      break;
    }
    case 2: {
      left_pseudo_inverse<2>(m, lambda, p_src, p_dst, allocator);
      break;
    }
    case 3: {
      left_pseudo_inverse<3>(m, lambda, p_src, p_dst, allocator);
      break;
    }
    case 4: {
      left_pseudo_inverse<4>(m, lambda, p_src, p_dst, allocator);
      break;
    }
    case 8: {
      left_pseudo_inverse<8>(m, lambda, p_src, p_dst, allocator);
      break;
    }
    case 16: {
      left_pseudo_inverse<16>(m, lambda, p_src, p_dst, allocator);
      break;
    }
    default:
      return ARMRAL_ARGUMENT_ERROR;
    }

    return ARMRAL_SUCCESS;
  }

  // If the number of rows in the input matrix is less than or equal to the
  // number of columns then use the right pseudo-inverse
  switch (m) {
  case 1: {
    right_pseudo_inverse<1>(n, lambda, p_src, p_dst, allocator);
    break;
  }
  case 2: {
    right_pseudo_inverse<2>(n, lambda, p_src, p_dst, allocator);
    break;
  }
  case 3: {
    right_pseudo_inverse<3>(n, lambda, p_src, p_dst, allocator);
    break;
  }
  case 4: {
    right_pseudo_inverse<4>(n, lambda, p_src, p_dst, allocator);
    break;
  }
  case 8: {
    right_pseudo_inverse<8>(n, lambda, p_src, p_dst, allocator);
    break;
  }
  case 16: {
    right_pseudo_inverse<16>(n, lambda, p_src, p_dst, allocator);
    break;
  }
  default:
    return ARMRAL_ARGUMENT_ERROR;
  }

  return ARMRAL_SUCCESS;
}

} // Anonymous namespace

armral_status armral_cmplx_pseudo_inverse_direct_f32(
    uint16_t m, uint16_t n, const float32_t lambda,
    const armral_cmplx_f32_t *__restrict p_src, armral_cmplx_f32_t *p_dst) {
  heap_allocator allocator{};
  return cmplx_pseudo_inverse_direct(m, n, lambda, p_src, p_dst, allocator);
}

// In this function's documentation we say:
//   The buffer must be at least `m * m * sizeof(armral_cmplx_f32_t) + 3` bytes.
// The 3 extra bytes are the worst case padding needed to align the
// buffer to a 4-byte boundary. This padding is not necessary if using an
// allocator that will always return aligned pointers (such as malloc()).
armral_status armral_cmplx_pseudo_inverse_direct_f32_noalloc(
    uint16_t m, uint16_t n, const float32_t lambda,
    const armral_cmplx_f32_t *__restrict p_src, armral_cmplx_f32_t *p_dst,
    void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return cmplx_pseudo_inverse_direct(m, n, lambda, p_src, p_dst, allocator);
}
