/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#pragma once

static inline armral_cmplx_f32_t
scal_mul_cmplx_f32(const armral_cmplx_f32_t &a, const armral_cmplx_f32_t &b) {
  return {a.re * b.re - a.im * b.im, a.im * b.re + a.re * b.im};
}

static inline armral_cmplx_f32_t
scal_add_cmplx_f32(const armral_cmplx_f32_t &a, const armral_cmplx_f32_t &b) {
  return {a.re + b.re, a.im + b.im};
}

static inline armral_cmplx_f32_t
scal_fmla_cmplx_f32(const armral_cmplx_f32_t &a, const armral_cmplx_f32_t &b,
                    const armral_cmplx_f32_t &c) {
  return scal_add_cmplx_f32(c, scal_mul_cmplx_f32(a, b));
}

static inline armral_cmplx_f32_t
scal_sub_cmplx_f32(const armral_cmplx_f32_t &a, const armral_cmplx_f32_t &b) {
  return {a.re - b.re, a.im - b.im};
}

static inline armral_cmplx_f32_t
scal_mod2_cmplx_f32(const armral_cmplx_f32_t &a) {
  return {a.re * a.re + a.im * a.im, 0.0F};
}

// Subtract elements in two single float arrays
template<unsigned int N>
static inline void sub_arr_f32(const float32_t *__restrict src_a,
                               const float32_t *__restrict src_b,
                               float32_t *p_dst) {
  for (unsigned i = 0; i < N; ++i) {
    p_dst[i] = src_a[i] - src_b[i];
  }
}

template<typename T>
struct equiv_64 {
  typedef T type;
};

template<>
struct equiv_64<float32x4_t> {
  typedef float64x2_t type;
};

template<>
struct equiv_64<float32x2_t> {
  typedef float64x1_t type;
};

template<typename T>
using equiv_64_t = typename equiv_64<T>::type;

template<typename T>
struct equiv_32 {
  typedef T type;
};

template<>
struct equiv_32<float64x2_t> {
  typedef float32x4_t type;
};

template<>
struct equiv_32<float64x1_t> {
  typedef float32x2_t type;
};

template<typename T>
using equiv_32_t = typename equiv_32<T>::type;

template<class T>
static T (*trn1_f32)(T, T);
template<>
inline constexpr auto trn1_f32<float32x4_t> = &vtrn1q_f32;
template<>
inline constexpr auto trn1_f32<float32x2_t> = &vtrn1_f32;

template<class T>
static T (*trn2_f32)(T, T);
template<>
inline constexpr auto trn2_f32<float32x4_t> = &vtrn2q_f32;
template<>
inline constexpr auto trn2_f32<float32x2_t> = &vtrn2_f32;

template<class T>
static T (*rev64_f32)(T);
template<>
inline constexpr auto rev64_f32<float32x4_t> = &vrev64q_f32;
template<>
inline constexpr auto rev64_f32<float32x2_t> = &vrev64_f32;

template<class T>
static T (*mul_f32)(T, T);
template<>
inline constexpr auto mul_f32<float32x4_t> = &vmulq_f32;
template<>
inline constexpr auto mul_f32<float32x2_t> = &vmul_f32;

template<class T>
static T (*fma_f32)(T, T, T);
template<>
inline constexpr auto fma_f32<float32x4_t> = &vfmaq_f32;
template<>
inline constexpr auto fma_f32<float32x2_t> = &vfma_f32;

template<class T>
static T (*fms_f32)(T, T, T);
template<>
inline constexpr auto fms_f32<float32x4_t> = &vfmsq_f32;
template<>
inline constexpr auto fms_f32<float32x2_t> = &vfms_f32;

template<class T>
static T (*neg_f64)(T);
template<>
inline constexpr auto neg_f64<float64x2_t> = &vnegq_f64;
template<>
inline constexpr auto neg_f64<float64x1_t> = &vneg_f64;

template<class T>
static equiv_64_t<T> (*reinterpret_f64_f32)(T);
template<>
inline constexpr auto reinterpret_f64_f32<float32x4_t> = &vreinterpretq_f64_f32;
template<>
inline constexpr auto reinterpret_f64_f32<float32x2_t> = &vreinterpret_f64_f32;

template<class T>
static equiv_32_t<T> (*reinterpret_f32_f64)(T);
template<>
inline constexpr auto reinterpret_f32_f64<float64x2_t> = &vreinterpretq_f32_f64;
template<>
inline constexpr auto reinterpret_f32_f64<float64x1_t> = &vreinterpret_f32_f64;

template<typename T>
static inline T neon_conjugate_f32(const T in) {
  using f64_t = equiv_64_t<T>;
  f64_t in_f64 = reinterpret_f64_f32<T>(in);
  f64_t conj = neg_f64<f64_t>(in_f64);
  T out = reinterpret_f32_f64<f64_t>(conj);
  return out;
}

template<bool conj, typename T>
static inline T neon_mul_cmplx_f32(T x, T y) {
  T yre = trn1_f32<T>(y, y);
  T yim = trn2_f32<T>(y, y);

  // Perform x * y or conj(x * y)
  T ret;
  if constexpr (conj) {
    T x_conj = neon_conjugate_f32<T>(x);
    T x_rev = rev64_f32<T>(x);

    ret = mul_f32<T>(x_conj, yre);
    ret = fms_f32<T>(ret, x_rev, yim);
  } else {
    T yim_conj = neon_conjugate_f32<T>(yim);
    T x_rev = rev64_f32<T>(x);

    ret = mul_f32<T>(x, yre);
    ret = fms_f32<T>(ret, x_rev, yim_conj);
  }

  return ret;
}

template<bool conj, typename T>
static inline T neon_mla_cmplx_f32(T z, T x, T y) {
  T yre = trn1_f32<T>(y, y);
  T yim = trn2_f32<T>(y, y);

  // Perform z + x * y or z + conj(x * y)
  T ret;
  if constexpr (conj) {
    T x_conj = neon_conjugate_f32<T>(x);
    T x_rev = rev64_f32<T>(x);

    ret = fma_f32<T>(z, x_conj, yre);
    ret = fms_f32<T>(ret, x_rev, yim);
  } else {
    T yim_conj = neon_conjugate_f32<T>(yim);
    T x_rev = rev64_f32<T>(x);

    ret = fma_f32<T>(z, x, yre);
    ret = fms_f32<T>(ret, x_rev, yim_conj);
  }

  return ret;
}

template<bool conj, typename T>
static inline T neon_mls_cmplx_f32(T z, T x, T y) {
  T yre = trn1_f32<T>(y, y);
  T yim = trn2_f32<T>(y, y);

  // Perform z - x * y or z - conj(x * y)
  T ret;
  if constexpr (conj) {
    T x_conj = neon_conjugate_f32<T>(x);
    T x_rev = rev64_f32<T>(x);

    ret = fms_f32<T>(z, x_conj, yre);
    ret = fma_f32<T>(ret, x_rev, yim);
  } else {
    T yim_conj = neon_conjugate_f32<T>(yim);
    T x_rev = rev64_f32<T>(x);

    ret = fms_f32<T>(z, x, yre);
    ret = fma_f32<T>(ret, x_rev, yim_conj);
  }

  return ret;
}

template<bool conj, typename T>
static inline T neon_minor_cmplx_f32(const T a, const T b, const T c,
                                     const T d) {
  // ret = a * d - c * b
  T ret = neon_mul_cmplx_f32<conj, T>(a, d);
  ret = neon_mls_cmplx_f32<conj, T>(ret, c, b);

  return ret;
}

template<typename T>
static inline T neon_abs2_cmplx_f32(const T a) {
  T are = trn1_f32<T>(a, a);
  T aim = trn2_f32<T>(a, a);

  T abs2 = mul_f32<T>(are, are);
  abs2 = fma_f32<T>(abs2, aim, aim);

  return abs2;
}

template<typename T>
static inline T neon_recip_cmplx_f32(const T a) {
  return neon_conjugate_f32<T>(a) / neon_abs2_cmplx_f32<T>(a);
}

// [Re(a), Im(a), Re(b), Im(b)] -> [Re(b), Im(b), Re(a), Im(a)]
static inline float32x4_t neon_rev_cmplx_f32(float32x4_t in) {
  float64x2_t in_f64 = vreinterpretq_f64_f32(in);
  return vreinterpretq_f32_f64(vextq_f64(in_f64, in_f64, 1));
}
