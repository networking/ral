/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#pragma once

#include "intrinsics.h"

#include <arm_neon.h>

#if ARMRAL_ARCH_SVE >= 2
#include <arm_sve.h>
#endif

static inline int16x8x2_t cmplx_mul_split_re_im(int16x8x2_t a, int16x8x2_t b) {
  /* Complex multiply two full vector-lengths. a and b are pairs of vectors,
   * where a.val[0] is all the real components and a.val[1] all the imag
   * components, and similarly for b (as if they have been ld2-ed). The result
   * is returned in the same format, ready for st2-ing.  */

  int16x8_t a_re = a.val[0];
  int16x8_t a_im = a.val[1];
  int16x8_t b_re = b.val[0];
  int16x8_t b_im = b.val[1];

  /* Re{C} = Re{A}*Re{B} - Im{A}*Im{B} */
  // As we extend to 32 bits, there is no possibility for saturation in the
  // 32-bit multiplication and addition for the real component. Don't use
  // saturating logic for the real part.
  int32x4_t re_lo = vmull_low_s16(a_re, b_re);
  int32x4_t re_hi = vmull_high_s16(a_re, b_re);
  re_lo = vmlsl_low_s16(re_lo, a_im, b_im);
  re_hi = vmlsl_high_s16(re_hi, a_im, b_im);

  /* Im{C} = Re{A}*Im{B} + Im{A}*Re{B} */
  int32x4_t im_lo = vqdmull_low_s16(a_re, b_im);
  int32x4_t im_hi = vqdmull_high_s16(a_re, b_im);
  im_lo = vqdmlal_low_s16(im_lo, a_im, b_re);
  im_hi = vqdmlal_high_s16(im_hi, a_im, b_re);

  int16x8x2_t out;

  // Shift >> 15, results in Q0.15 format
  int16x4_t out_re_lo = vqrshrn_n_s32(re_lo, 15);
  out.val[0] = vqrshrn_high_n_s32(out_re_lo, re_hi, 15);

  // Shift >> 16 as we use doubling multiplication and accumulation to get
  // saturation for the imaginary component
  int16x4_t out_im_lo = vqrshrn_n_s32(im_lo, 16);
  out.val[1] = vqrshrn_high_n_s32(out_im_lo, im_hi, 16);

  return out;
}

static inline int16x4x2_t cmplx_mul_split_re_im(int16x4x2_t a, int16x4x2_t b) {
  int16x4_t a_re = a.val[0];
  int16x4_t a_im = a.val[1];
  int16x4_t b_re = b.val[0];
  int16x4_t b_im = b.val[1];

  int32x4_t re_32 = vmull_s16(a_re, b_re);
  re_32 = vmlsl_s16(re_32, a_im, b_im);
  int16x4_t re = vqrshrn_n_s32(re_32, 15);

  int32x4_t im_32 = vqdmull_s16(a_re, b_im);
  im_32 = vqdmlal_s16(im_32, b_re, a_im);
  int16x4_t im = vqrshrn_n_s32(im_32, 16);

  int16x4x2_t out = {{re, im}};
  return out;
}

static inline int16x8_t cmplx_mul_combined_re_im(int16x8_t a,
                                                 armral_cmplx_int16_t scale) {
  auto a_rev = vrev32q_s16(a);

  int16x8_t cc = vdupq_n_s16(scale.re);
  int16x8_t dd = vdupq_n_s16(scale.im);
  uint16x8_t mult_mask = vreinterpretq_u16_u32(vdupq_n_u32(0xffffU));
  dd = vbslq_s16(mult_mask, vqnegq_s16(dd), dd);

  int32x4_t lo32 = vqdmull_low_s16(a, cc);
  int32x4_t hi32 = vqdmull_high_s16(a, cc);
  lo32 = vqdmlal_low_s16(lo32, a_rev, dd);
  hi32 = vqdmlal_high_s16(hi32, a_rev, dd);

  return vuzp2q_s16(vreinterpretq_s16_s32(lo32), vreinterpretq_s16_s32(hi32));
}

static inline int16x8x3_t load3_cmplx_and_scale(const int16_t *src,
                                                armral_cmplx_int16_t scale) {
  // Load 3 vectors of complex input and multiply by a complex scale factor
  int16x8_t in0 = vld1q_s16(src + 0);
  int16x8_t in1 = vld1q_s16(src + 8);
  int16x8_t in2 = vld1q_s16(src + 16);

  int16x8_t res0 = cmplx_mul_combined_re_im(in0, scale);
  int16x8_t res1 = cmplx_mul_combined_re_im(in1, scale);
  int16x8_t res2 = cmplx_mul_combined_re_im(in2, scale);

  return int16x8x3_t{res0, res1, res2};
}

static inline void scale_and_store3_cmplx(int16_t *dst, int16x8_t out_0,
                                          int16x8_t out_1, int16x8_t out_2,
                                          int16x8x2_t scale) {
  // Multiply by a complex scale factor and store three vectors of output
  vst2q_s16(dst, cmplx_mul_split_re_im(int16x8x2_t{{vuzp1q_s16(out_0, out_1),
                                                    vuzp2q_s16(out_0, out_1)}},
                                       scale));
  vst2_s16(dst + 16, cmplx_mul_split_re_im(
                         int16x4x2_t{{vget_low_s16(vuzp1q_s16(out_2, out_2)),
                                      vget_low_s16(vuzp2q_s16(out_2, out_2))}},
                         int16x4x2_t{{vget_low_s16(scale.val[0]),
                                      vget_low_s16(scale.val[1])}}));
}

#if ARMRAL_ARCH_SVE >= 2

static inline svint16_t sv_cmplx_mul_combined_re_im(svint16_t a, int16_t real,
                                                    svint16_t imag) {
  svint32_t lo32 = svqdmullb_n_s32(a, real);
  svint32_t hi32 = svqdmullt_n_s32(a, real);
  lo32 = svqdmlslbt_s32(lo32, imag, a);
  hi32 = svqdmlalbt_s32(hi32, a, imag);

  return svtrn2_s16(svreinterpret_s16_s32(lo32), svreinterpret_s16_s32(hi32));
}

static inline svint16x2_t sv_cmplx_mul_split_re_im(svint16x2_t a,
                                                   svint16x2_t b) {
  /* Complex multiply two full vector-lengths. a and b are pairs of vectors,
   * where svget2_s16(a, 0) is all the real components and svget2_s16(a, 1) all
   * the imag components, and similarly for b (as if they have been ld2-ed). The
   * result is returned in the same format, ready for st2-ing.  */

  svint16_t a_re = svget2_s16(a, 0);
  svint16_t a_im = svget2_s16(a, 1);
  svint16_t b_re = svget2_s16(b, 0);
  svint16_t b_im = svget2_s16(b, 1);

  /* re{c} = re{a}*re{b} - im{a}*im{b} */
  svint32_t re_lo = svmullb_s32(a_re, b_re);
  svint32_t re_hi = svmullt_s32(a_re, b_re);
  re_lo = svmlslb_s32(re_lo, a_im, b_im);
  re_hi = svmlslt_s32(re_hi, a_im, b_im);

  /* im{c} = re{a}*im{b} + im{a}*re{b} */
  svint32_t im_lo = svqdmullb_s32(a_re, b_im);
  svint32_t im_hi = svqdmullt_s32(a_re, b_im);
  im_lo = svqdmlalb_s32(im_lo, a_im, b_re);
  im_hi = svqdmlalt_s32(im_hi, a_im, b_re);

  /* shift >> 15, results in q15 format */
  svint16_t re = svqrshrnb_n_s32(re_lo, 15);
  re = svqrshrnt_n_s32(re, re_hi, 15);

  // shift >> 16 for imaginary component, as we use doubling multiplication
  // and addition to be able to make use of the saturating intrinsics
  svint16_t im = svqrshrnb_n_s32(im_lo, 16);
  im = svqrshrnt_n_s32(im, im_hi, 16);

  return svcreate2(re, im);
}
#endif // ARMRAL_ARCH_SVE >= 2
