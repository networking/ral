/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#pragma once

#include <cassert>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <memory>

namespace armral {

template<typename Allocator, bool allocator_is_counting>
class base_allocator {
public:
  static constexpr bool is_counting = allocator_is_counting;

  // Delete implicit copy/move constructors to prevent mistakes.
  base_allocator(base_allocator const &) = delete;
  base_allocator(base_allocator &&) = delete;
  base_allocator &operator=(base_allocator const &) = delete;
  base_allocator &operator=(base_allocator &&) = delete;

  struct deleter {
    void operator()(void *ptr) {
      Allocator::deallocate(ptr);
    }
  };

protected:
  base_allocator() = default;
};

class heap_allocator : public base_allocator<heap_allocator, false> {
public:
  template<typename T>
  static T *allocate_uninitialized(size_t nitems) {
    return static_cast<T *>(malloc(sizeof(T) * nitems));
  }

  template<typename T>
  static T *allocate_zeroed(size_t nitems) {
    return static_cast<T *>(calloc(nitems, sizeof(T)));
  }

  static void deallocate(void *ptr) {
    free(ptr);
  }
};

static constexpr bool is_pow_2(uint64_t value) {
  return (value & (value - 1)) == 0;
}

/*
  Round up to the nearest power of two alignment

  Explanation of the trick:

    If `value` is not a multiple of `align`:

      Adding `align - 1` to `value` goes to the next multiple of align + an
      overshoot (since it must be <= align - 1 from the next multiple).

      This overshoot will always be in the range 0 <= overshoot < align - 1,
      which will be in the last log2(align) bits.

      So to snap back down to a multiple of align, those bits just need to be
      cleared. This can be done by a bitwise AND with ~(align - 1).

    If `value` already is a multiple of `align` adding `align - 1` just results
    in the same multiple of `align` once the lower bits are cleared.
*/
static constexpr uint64_t align_to(uint64_t value, uint32_t align) {
  assert(is_pow_2(align) && "align_to: Expected power of 2 alignment!");
  return (value + (align - 1)) & ~static_cast<uint64_t>(align - 1);
}

static inline void *align_ptr(void *ptr, uint32_t align) {
  return reinterpret_cast<void *>(
      align_to(reinterpret_cast<uintptr_t>(ptr), align));
}

class buffer_bump_allocator
  : public base_allocator<buffer_bump_allocator, false> {
public:
  explicit buffer_bump_allocator(void *buffer) : m_next_ptr(buffer) {}

  template<typename T>
  T *allocate_uninitialized(size_t nitems) {
    auto *aligned_ptr = align_ptr(m_next_ptr, alignof(T));
    m_next_ptr = static_cast<uint8_t *>(aligned_ptr) + sizeof(T) * nitems;
    return static_cast<T *>(aligned_ptr);
  }

  template<typename T>
  T *allocate_zeroed(size_t nitems) {
    auto *ptr = allocate_uninitialized<T>(nitems);
    memset(ptr, 0, sizeof(T) * nitems);
    return ptr;
  }

  static void deallocate(void *ptr) {
    // This is a no-op.
    (void)ptr;
  }

private:
  void *m_next_ptr{nullptr};
};

class counting_allocator : public base_allocator<counting_allocator, true> {
public:
  template<typename T>
  T *allocate_uninitialized(size_t nitems) {
    m_required_bytes =
        align_to(m_required_bytes, alignof(T)) + sizeof(T) * nitems;
    return nullptr;
  }

  template<typename T>
  T *allocate_zeroed(size_t nitems) {
    return allocate_uninitialized<T>(nitems);
  }

  static void deallocate(void *ptr) {
    (void)ptr;
  }

  uint32_t required_bytes() const {
    // -1 since we start at 1 to simulate an initially misaligned pointer.
    return static_cast<uint32_t>(m_required_bytes) - 1;
  }

private:
  // For simplicity, assume we start with an unaligned pointer. This means
  // this count will always give the maximum required bytes, so the user
  // does not have to worry about alignment.
  size_t m_required_bytes{1};
};

} // namespace armral

/*
  Convenience functions to allocate some memory using an allocator, and return
  a std::unique_ptr<T[]> (which will automatically free the memory when it goes
  out of scope).

  The expected usage is:

  // cmplxs is a std::unique_ptr<T[]>
  auto cmplxs = allocate_uninitialized<armral_cmplx_f32_t>(allocator, m * n);

  // To pass a raw (non-owning) pointer to the memory just call .get() on the
  // pointer:

  do_thing_with_cmplxs(a, b, c, cmplxs.get())

  // Note: The unique_ptr needs to stay in scope for the raw pointer to be
  // valid!

  // If you want the address of the n-th item rather than doing:

  cmplxs.get() + n

  // You can instead do:

  &cmplxs[n]
*/
template<typename T, typename Allocator>
auto allocate_uninitialized(Allocator &allocator, size_t nitems) {
  return std::unique_ptr<T[], typename Allocator::deleter>{
      allocator.template allocate_uninitialized<T>(nitems)};
}

template<typename T, typename Allocator>
auto allocate_zeroed(Allocator &allocator, size_t nitems) {
  return std::unique_ptr<T[], typename Allocator::deleter>{
      allocator.template allocate_zeroed<T>(nitems)};
}

template<typename Allocator, typename T>
using unique_ptr = std::unique_ptr<T[], typename Allocator::deleter>;

using armral::buffer_bump_allocator;
using armral::counting_allocator;
using armral::heap_allocator;
