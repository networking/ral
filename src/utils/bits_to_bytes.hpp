/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "intrinsics.h"

#include <arm_neon.h>
#include <vector>

namespace armral {

// Given a byte array, where we are interested in each bit, create
// an array of bytes instead in the passed-in array "out"
// Data is read from the most significant bit in each byte to the least
// significant
inline void bits_to_bytes(uint32_t n, const uint8_t *in, uint8_t *out) {
  uint32_t full_bytes = n >> 3;
  // Set the mask
  uint8x16_t mask = vdupq_n_u8(1);
  // When shifting, we are going to be dealing with two bytes at a time
  int8x16_t shifts = {-7, -6, -5, -4, -3, -2, -1, 0,
                      -7, -6, -5, -4, -3, -2, -1, 0};
  // Set the base index for the bytes to 0 x 8, 1 x 8
  uint64x2_t base_index = {0x0, 0x0101010101010101};
  // Increment the index by two each iteration
  uint8x16_t two = vdupq_n_u8(2);
  uint32_t i = 0;
  for (; i + 8 < full_bytes; i += 8) {
    // Load 8 bytes into an uint8x16_t
    uint8x16_t bytes = vld1d_u8(&in[i]);

    uint8x16_t index = vreinterpretq_u8_u64(base_index);
    // We can unroll by a factor 2 by using vqtbl1q
    for (int byte_ind = 0; byte_ind < 8; byte_ind += 2) {
      uint8x16_t byte = vqtbl1q_u8(bytes, index);
      // Shift the bits we want to convert into the rightmost position, and mask
      // with 1
      uint8x16_t new_byte = vshlq_u8(byte, shifts);
      new_byte = vandq_u8(new_byte, mask);
      // Next loop
      index = vaddq_u8(index, two);
      vst1q_u8(&out[8 * (i + byte_ind)], new_byte);
    }
  }

  // Deal with a vector tail
  uint8x8_t mask_tail = vdup_n_u8(1);
  int8x8_t shift_tail = {-7, -6, -5, -4, -3, -2, -1, 0};
  for (; i < full_bytes; ++i) {
    // Load a byte and duplicate to 8 lanes of a vector
    uint8x8_t byte = vld1_dup_u8(&in[i]);
    // Shift the bit we want in each lane to the right-most
    // position, and mask with 1
    uint8x8_t new_bytes = vshl_u8(byte, shift_tail);
    new_bytes = vand_u8(new_bytes, mask_tail);
    vst1_u8(&out[8 * i], new_bytes);
  }

  // Now deal with a scalar tail
  if ((n & 7) != 0) {
    uint8_t byte = in[full_bytes];
    uint32_t bit_ind = 0;
    for (uint32_t j = 8 * i; j < n; ++j, ++bit_ind) {
      out[j] = (byte >> (7 - bit_ind)) & 1;
    }
  }
}

// Given a byte array, where we are interested in each bit, create
// an array of bytes instead and return it in a std::vector
inline std::vector<uint8_t> bits_to_bytes(uint32_t n, const uint8_t *in) {
  std::vector<uint8_t> out(n);
  bits_to_bytes(n, in, out.data());
  return out;
}

// Given a byte array of zeros and ones, write this out to
// consecutive bits instead. Bytes are assumed to be big endian
// so the first bit in a byte goes to the highest bit position
inline void bytes_to_bits(uint32_t n, const uint8_t *in, uint8_t *out) {
  uint32_t full_bytes = n >> 3;
  uint32_t tail_bits = n & 7;
  for (uint32_t i = 0; i < full_bytes; ++i) {
    out[i] = (in[i * 8] & 1) << 7;
    for (uint32_t j = 1; j < 8; ++j) {
      out[i] |= (in[i * 8 + j] & 1) << (7 - j);
    }
  }
  if (tail_bits != 0) {
    out[full_bytes] = (in[full_bytes * 8] & 1) << 7;
    for (uint32_t j = 1; j < tail_bits; ++j) {
      out[full_bytes] |= (in[full_bytes * 8 + j] & 1) << (7 - j);
    }
  }
}

// Loop through all of the llrs, and set the corresponding bit to 1 if LLR is
// negative, otherwise to 0. We do not assume that the data_out pointer is
// initialized
template<typename T>
inline void llrs_to_bits(uint32_t n, const T *llr, uint8_t *data_out) {
  uint32_t full_bytes = n >> 3;
  uint32_t tail_bits = n & 7;
  for (uint32_t i = 0; i < full_bytes; ++i) {
    data_out[i] = 0;
    for (uint32_t j = 0; j < 8; ++j) {
      // The first bit to write in the byte is the most significant
      if (llr[i * 8 + j] < 0) {
        uint32_t bit_ind = 7 ^ j;
        data_out[i] |= 1 << bit_ind;
      }
    }
  }
  // Deal with tail bits
  if (tail_bits != 0) {
    data_out[full_bytes] = 0;
    for (uint32_t i = 0; i < tail_bits; ++i) {
      // The first bit to write in the byte is the most significant
      if (llr[full_bytes * 8 + i] < 0) {
        uint32_t bit_ind = 7 ^ i;
        data_out[full_bytes] |= 1 << bit_ind;
      }
    }
  }
}

} // namespace armral
