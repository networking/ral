/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "armral.h"
#include "turbo_tables.hpp"
#include "utils/allocators.hpp"

#include <cassert>
#include <cmath>
#include <cstring>

namespace armral::turbo {

struct dummy_bits_work_buffers {
  int8_t *dummy0;
  int8_t *dummy1;
  int8_t *dummy2;
};

static void generate_dummy_bits_tracking(uint32_t d, uint32_t rtc,
                                         int8_t *dummy,
                                         dummy_bits_work_buffers work_buffers) {
  // The dummy bits are the first nd bits of each of the three streams of data
  // in the turbo code. Where these are in the overall input data stream is
  // determined in a manner similar to the encoding, described in section
  // 5.1.4.1.1 in 3GPP specification 36.212.
  const uint32_t kpi = rtc * armral::turbo::ctc;
  const uint32_t nd = kpi - d;

  // Tag nd elements as dummy bits.
  // dummy0 and dummy1 are permuted and transposed.
  for (uint32_t i = 0; i < nd; ++i) {
    work_buffers.dummy0[armral::turbo::p[i] * rtc] = 1;
    work_buffers.dummy1[armral::turbo::p[i] * rtc] = 1;
  }
  // Permutation for dummy2
  for (uint32_t i = 0; i < kpi; ++i) {
    // TODO: We don't need to go through all of kpi here. We should be able to
    //       identify where each of the nd < crc = 32 bits goes.
    uint32_t pi =
        (armral::turbo::p[i / rtc] + armral::turbo::ctc * (i % rtc) + 1) % kpi;
    if (pi < nd) {
      work_buffers.dummy2[i] = 1;
    }
  }

  // bit collection step for dummy
  memcpy((void *)dummy, (const void *)work_buffers.dummy0,
         sizeof(int8_t) * kpi);
  for (uint32_t i = 0; i < kpi; ++i) {
    dummy[kpi + 2 * i] = work_buffers.dummy1[i];
    dummy[kpi + 2 * i + 1] = work_buffers.dummy2[i];
  }
}

// Undo the turbo rate matching subblock interleaver defined in
// TS 36.212 Section 5.1.4.1.1
struct subblock_deinterleave_work_buffers {
  int8_t *y0;
  int8_t *y1;
  int8_t *y2;
};

static void
subblock_deinterleave(uint32_t d, uint32_t rtc, const int8_t *v0,
                      const int8_t *v1, const int8_t *v2, int8_t *d0,
                      int8_t *d1, int8_t *d2,
                      subblock_deinterleave_work_buffers work_buffers) {

  const uint32_t kpi = rtc * armral::turbo::ctc;
  const uint32_t nd = kpi - d;

  // Reverse permutation and transpose for d^(0)_k and d^(1)_k
  for (uint32_t i = 0; i < rtc; ++i) {
    for (uint32_t j = 0; j < armral::turbo::ctc; ++j) {
      uint32_t k = j + i * armral::turbo::ctc;
      work_buffers.y0[k] = v0[i + armral::turbo::p[j] * rtc];
      work_buffers.y1[k] = v1[i + armral::turbo::p[j] * rtc];
    }
  }

  // Reverse permutation for d^(2)_k
  for (uint32_t i = 0; i < kpi; ++i) {
    uint32_t pi =
        (armral::turbo::p[i / rtc] + armral::turbo::ctc * (i % rtc) + 1) % kpi;
    work_buffers.y2[pi] = v2[i];
  }

  // Ignore nd elements as they are dummy bits
  // d0, d1 and d2 may already contain LLRs so we sum into them
  // rather than overwriting
  for (uint32_t i = 0; i < d; i++) {
    d0[i] = vqaddb_s8(d0[i], work_buffers.y0[nd + i]);
    d1[i] = vqaddb_s8(d1[i], work_buffers.y1[nd + i]);
    d2[i] = vqaddb_s8(d2[i], work_buffers.y2[nd + i]);
  }
}

// Undo the turbo rate matching bit collection defined in
// TS 36.212 Section 5.1.4.1.2
static void bit_decollection(uint32_t kpi, const int8_t *w, int8_t *v0,
                             int8_t *v1, int8_t *v2) {
  // w_k = v^0_k for k = [0, kpi - 1]
  memcpy((void *)v0, (const void *)w, sizeof(int8_t) * kpi);

  // v^1_k = w_{kpi + 2k    },
  // v^2_k = w_{kpi + 2k + 1} for k = [0, kpi - 1]
  for (uint32_t k = 0; k < kpi; ++k) {
    v1[k] = w[kpi + 2 * k];
    v2[k] = w[kpi + 2 * k + 1];
  }
}

// Undo the turbo rate matching bit selection defined in
// TS 36.212 Section 5.1.4.1.2
static void bit_deselection(uint32_t ncb, uint32_t k0, uint32_t e,
                            const int8_t *ek, const int8_t *dummy, int8_t *w) {
  uint32_t k = 0;
  uint32_t j = 0;
  while (k < e) {
    uint32_t i = (k0 + j) % ncb;
    bool not_dummy = dummy[i] != 1;
    if (not_dummy) {
      w[i] = vqaddb_s8(w[i], ek[k]);
      k++;
    }
    j++;
  }
}

template<typename Allocator>
armral_status rate_recovery(uint32_t d, uint32_t e, uint32_t rv,
                            const int8_t *src, int8_t *dst0, int8_t *dst1,
                            int8_t *dst2, Allocator &allocator) {
  assert(d > 0);
  assert(e > 0);
  assert(rv <= 3);

  // The minimum number of rows which gives rtc * ctc >= d.
  const uint32_t rtc = (d + armral::turbo::ctc - 1) / armral::turbo::ctc;
  const uint32_t kpi = rtc * armral::turbo::ctc;
  const uint32_t kw = 3 * kpi;

  auto dummy = allocate_zeroed<int8_t>(allocator, kpi * 3);
  auto dummy0 = allocate_zeroed<int8_t>(allocator, kpi);
  auto dummy1 = allocate_zeroed<int8_t>(allocator, kpi);
  auto dummy2 = allocate_zeroed<int8_t>(allocator, kpi);

  auto w = allocate_zeroed<int8_t>(allocator, kpi * 3);

  auto v0 = allocate_zeroed<int8_t>(allocator, kpi);
  auto v1 = allocate_zeroed<int8_t>(allocator, kpi);
  auto v2 = allocate_zeroed<int8_t>(allocator, kpi);

  auto y0 = allocate_zeroed<int8_t>(allocator, kpi);
  auto y1 = allocate_zeroed<int8_t>(allocator, kpi);
  auto y2 = allocate_zeroed<int8_t>(allocator, kpi);

  if constexpr (Allocator::is_counting) {
    return ARMRAL_SUCCESS;
  }

  // Assume N_cb = k_w.
  const uint32_t ncb = kw;

  // Calculate k0 with the assumption N_cb = k_w, as per section 5.1.4.1.2 of
  // 3GPP specification 36.212. We can do this, as we assume that we only ever
  // deal with a single code block per transport block.
  // k0 = rtc * (2 * N_cb/(8 * rtc) * rv + 2),
  // where
  // N_cb = kw
  //      = 3 * ctc * rtc
  //      = 3 * 32 * rtc
  const uint32_t k0 = rtc * (24 * rv + 2);

  generate_dummy_bits_tracking(d, rtc, dummy.get(),
                               {dummy0.get(), dummy1.get(), dummy2.get()});

  bit_deselection(ncb, k0, e, src, dummy.get(), w.get());

  bit_decollection(kpi, w.get(), v0.get(), v1.get(), v2.get());

  subblock_deinterleave(d, rtc, v0.get(), v1.get(), v2.get(), dst0, dst1, dst2,
                        {y0.get(), y1.get(), y2.get()});

  return ARMRAL_SUCCESS;
}

} // namespace armral::turbo

armral_status armral_turbo_rate_recovery(uint32_t d, uint32_t e, uint32_t rv,
                                         const int8_t *src, int8_t *dst0,
                                         int8_t *dst1, int8_t *dst2) {
  heap_allocator allocator{};
  return armral::turbo::rate_recovery(d, e, rv, src, dst0, dst1, dst2,
                                      allocator);
}

armral_status armral_turbo_rate_recovery_noalloc(uint32_t d, uint32_t e,
                                                 uint32_t rv, const int8_t *src,
                                                 int8_t *dst0, int8_t *dst1,
                                                 int8_t *dst2, void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return armral::turbo::rate_recovery(d, e, rv, src, dst0, dst1, dst2,
                                      allocator);
}

uint32_t armral_turbo_rate_recovery_noalloc_buffer_size(uint32_t d, uint32_t e,
                                                        uint32_t rv) {
  counting_allocator allocator{};
  (void)armral::turbo::rate_recovery(d, e, rv, nullptr, nullptr, nullptr,
                                     nullptr, allocator);
  return allocator.required_bytes();
}
