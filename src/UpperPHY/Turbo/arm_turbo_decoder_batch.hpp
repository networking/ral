/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2024-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "turbo_code_common.hpp"
#include "utils/allocators.hpp"

#include <cmath>

namespace {

inline void batched_turbo_llrs_to_bits(uint32_t k, const int16x8_t *llr,
                                       uint8_t *data_out) {
  constexpr uint32_t data_t_size = 8; // data_out is uint8
  // We want to write to the upper half of the uint16 vectors, so that we can
  // easily narrow later with vaddhn_u16()
  uint16x8_t ones[data_t_size] = {
      {32768, 32768, 32768, 32768, 32768, 32768, 32768, 32768}, // 10000000 << 8
      {16384, 16384, 16384, 16384, 16384, 16384, 16384, 16384}, // 01000000 << 8
      {8192, 8192, 8192, 8192, 8192, 8192, 8192, 8192},         // 00100000 << 8
      {4096, 4096, 4096, 4096, 4096, 4096, 4096, 4096},         // 00010000 << 8
      {2048, 2048, 2048, 2048, 2048, 2048, 2048, 2048},         // 00001000 << 8
      {1024, 1024, 1024, 1024, 1024, 1024, 1024, 1024},         // 00000100 << 8
      {512, 512, 512, 512, 512, 512, 512, 512},                 // 00000010 << 8
      {256, 256, 256, 256, 256, 256, 256, 256}};                // 00000001 << 8

  for (uint32_t k8 = 0; k8 < k / data_t_size; ++k8) {
    uint16x8_t mask[data_t_size];
    for (uint32_t j = 0; j < data_t_size; ++j) {
      uint16x8_t pred = vcltzq_s16(llr[k8 * data_t_size + j]);
      // jth bit in mask[j][vi] is jth bit in data_out[vi]
      mask[j] = vandq_u16(pred, ones[j]);
    }
    ((uint8x8_t *)data_out)[k8] = vaddhn_u16(
        vqaddq_u16(vqaddq_u16(mask[0], mask[1]), vqaddq_u16(mask[2], mask[3])),
        vqaddq_u16(vqaddq_u16(mask[4], mask[5]), vqaddq_u16(mask[6], mask[7])));
  }
}

// Calculate the trellis termination values. These are independent of the
// extrinsic information and so can be done once without needing to be updated
// on every iteration.
void batched_trellis_termination(const int16x8_t *sys, const int16x8_t *par,
                                 uint32_t k, int16x8_t *beta_tail,
                                 int16x8_t l_c) {
  // We handle the gammas for the trellis termination bits separately
  // as the state transitions are different. The x_{kl} are never 1
  // here, because we always use inputs of 0 to drive the trellis back
  // to state 0 in the encoder, so we only need to consider a smaller
  // number of state transitions. We also do not have any extrinsic
  // information. Because some of the gamma terms will always be
  // -INFINITY (specifically indices [1] and [3]) we can forgo adding
  // to them to beta or taking the max with them, compared with when
  // we calculate beta in the main calculations. As above, we assume
  // that the channel reliability parameter l_c/2 = 1.
  int16x8_t pdf_00[3] = {vqaddq_s16(sys[k], par[k]),
                         vqaddq_s16(sys[k + 1], par[k + 1]),
                         vqaddq_s16(sys[k + 2], par[k + 2])};
  int16x8_t pdf_01[3] = {vqsubq_s16(sys[k], par[k]),
                         vqsubq_s16(sys[k + 1], par[k + 1]),
                         vqsubq_s16(sys[k + 2], par[k + 2])};

  // From each state, there is one path through trellis termination to state 0.
  // So no need to do any maxes, or store intermediate results in more betas
  beta_tail[0] = vqaddq_s16(pdf_00[0], vqaddq_s16(pdf_00[1], pdf_00[2]));
  beta_tail[1] = vqaddq_s16(pdf_01[0], vqaddq_s16(pdf_00[1], pdf_00[2]));
  beta_tail[2] = vqaddq_s16(pdf_00[0], vqaddq_s16(pdf_01[1], pdf_00[2]));
  beta_tail[3] = vqaddq_s16(pdf_01[0], vqaddq_s16(pdf_01[1], pdf_00[2]));
  beta_tail[4] = vqaddq_s16(pdf_01[0], vqaddq_s16(pdf_00[1], pdf_01[2]));
  beta_tail[5] = vqaddq_s16(pdf_00[0], vqaddq_s16(pdf_00[1], pdf_01[2]));
  beta_tail[6] = vqaddq_s16(pdf_01[0], vqaddq_s16(pdf_01[1], pdf_01[2]));
  beta_tail[7] = vqaddq_s16(pdf_00[0], vqaddq_s16(pdf_01[1], pdf_01[2]));
}

// Assumes l_c is 2 and can be ignored (l_c/2 = 2/2 = 1)
void decode_batch_step(const int16x8_t *sys, const int16x8_t *par,
                       const int16x8_t *extrinsic, uint32_t k, int16x8_t *llr,
                       int16x8_t *a, int16x8_t *b_tail, void *g_i16,
                       int16x8_t l_c) {

  constexpr uint32_t normalize_frequency = 4;
  constexpr uint32_t states = 8; // 8 states in the encoder

  // Gamma
  // Cast gamma to align function signature with single decode_block_step
  int16x8x4_t *g = static_cast<int16x8x4_t *>(g_i16);
  // Gamma[i] = +/- (sys[i] + extrinsic[i]/2) +/- par[i]
  // So for each k idx there are 4 Gammas (+ +, + -, - +, - -).
  // So store 4 per k and label each transition with its corresponding offset

  // + +
  constexpr uint32_t t00 = 0;
  constexpr uint32_t t14 = 0;
  constexpr uint32_t t67 = 0;
  constexpr uint32_t t73 = 0;
  // + -
  constexpr uint32_t t25 = 1;
  constexpr uint32_t t31 = 1;
  constexpr uint32_t t42 = 1;
  constexpr uint32_t t56 = 1;
  // - +
  constexpr uint32_t t21 = 2;
  constexpr uint32_t t35 = 2;
  constexpr uint32_t t46 = 2;
  constexpr uint32_t t52 = 2;
  // - -
  constexpr uint32_t t04 = 3;
  constexpr uint32_t t10 = 3;
  constexpr uint32_t t63 = 3;
  constexpr uint32_t t77 = 3;

  for (uint32_t i = 0; i < k; ++i) {
    int16x8_t term = vqaddq_s16(extrinsic[i] >> 1, sys[i]);
    g[i].val[0] = vqaddq_s16(term, par[i]);             // + +
    g[i].val[1] = vqsubq_s16(term, par[i]);             // + -
    g[i].val[2] = vqsubq_s16(par[i], term);             // - +
    g[i].val[3] = vqsubq_s16(vqnegq_s16(par[i]), term); // - -
  }

  // Alpha
  for (uint32_t i = 0; i < k; ++i) {
    uint32_t k_idx = states * i;
    uint32_t kp1_idx = states * (i + 1);

    a[kp1_idx + 0] = vmaxq_s16(vqaddq_s16(g[i].val[t00], a[k_idx + 0]),
                               vqaddq_s16(g[i].val[t10], a[k_idx + 1]));
    a[kp1_idx + 1] = vmaxq_s16(vqaddq_s16(g[i].val[t21], a[k_idx + 2]),
                               vqaddq_s16(g[i].val[t31], a[k_idx + 3]));
    a[kp1_idx + 2] = vmaxq_s16(vqaddq_s16(g[i].val[t42], a[k_idx + 4]),
                               vqaddq_s16(g[i].val[t52], a[k_idx + 5]));
    a[kp1_idx + 3] = vmaxq_s16(vqaddq_s16(g[i].val[t63], a[k_idx + 6]),
                               vqaddq_s16(g[i].val[t73], a[k_idx + 7]));
    a[kp1_idx + 4] = vmaxq_s16(vqaddq_s16(g[i].val[t04], a[k_idx + 0]),
                               vqaddq_s16(g[i].val[t14], a[k_idx + 1]));
    a[kp1_idx + 5] = vmaxq_s16(vqaddq_s16(g[i].val[t25], a[k_idx + 2]),
                               vqaddq_s16(g[i].val[t35], a[k_idx + 3]));
    a[kp1_idx + 6] = vmaxq_s16(vqaddq_s16(g[i].val[t46], a[k_idx + 4]),
                               vqaddq_s16(g[i].val[t56], a[k_idx + 5]));
    a[kp1_idx + 7] = vmaxq_s16(vqaddq_s16(g[i].val[t67], a[k_idx + 6]),
                               vqaddq_s16(g[i].val[t77], a[k_idx + 7]));
    // Normalize
    if (i % normalize_frequency == 0) {
      a[kp1_idx + 1] = vqsubq_s16(a[kp1_idx + 1], a[kp1_idx]);
      a[kp1_idx + 2] = vqsubq_s16(a[kp1_idx + 2], a[kp1_idx]);
      a[kp1_idx + 3] = vqsubq_s16(a[kp1_idx + 3], a[kp1_idx]);
      a[kp1_idx + 4] = vqsubq_s16(a[kp1_idx + 4], a[kp1_idx]);
      a[kp1_idx + 5] = vqsubq_s16(a[kp1_idx + 5], a[kp1_idx]);
      a[kp1_idx + 6] = vqsubq_s16(a[kp1_idx + 6], a[kp1_idx]);
      a[kp1_idx + 7] = vqsubq_s16(a[kp1_idx + 7], a[kp1_idx]);

      a[kp1_idx + 0] = vdupq_n_s16(0);
    }
  }

  // LLR and Beta
  // b_tail should be already initialized by trellis termination
  int16x8_t b_kp1[states];
  for (uint32_t s = 0; s < states; ++s) {
    b_kp1[s] = b_tail[s];
  }

  int16x8_t b[states];
  for (int32_t i = k - 1; i >= 0; --i) {
    uint32_t k_idx = states * i;

    // Normalize beta_kp1
    if (i % normalize_frequency == 0) {
      b_kp1[1] = vqsubq_s16(b[1], b_kp1[0]);
      b_kp1[2] = vqsubq_s16(b[2], b_kp1[0]);
      b_kp1[3] = vqsubq_s16(b[3], b_kp1[0]);
      b_kp1[4] = vqsubq_s16(b[4], b_kp1[0]);
      b_kp1[5] = vqsubq_s16(b[5], b_kp1[0]);
      b_kp1[6] = vqsubq_s16(b[6], b_kp1[0]);
      b_kp1[7] = vqsubq_s16(b[7], b_kp1[0]);
      b_kp1[0] = vdupq_n_s16(0);
    }
    // Beta
    b[0] = vmaxq_s16(vqaddq_s16(g[i].val[t00], b_kp1[0]),
                     vqaddq_s16(g[i].val[t04], b_kp1[4]));
    b[1] = vmaxq_s16(vqaddq_s16(g[i].val[t10], b_kp1[0]),
                     vqaddq_s16(g[i].val[t14], b_kp1[4]));
    b[2] = vmaxq_s16(vqaddq_s16(g[i].val[t21], b_kp1[1]),
                     vqaddq_s16(g[i].val[t25], b_kp1[5]));
    b[3] = vmaxq_s16(vqaddq_s16(g[i].val[t31], b_kp1[1]),
                     vqaddq_s16(g[i].val[t35], b_kp1[5]));
    b[4] = vmaxq_s16(vqaddq_s16(g[i].val[t42], b_kp1[2]),
                     vqaddq_s16(g[i].val[t46], b_kp1[6]));
    b[5] = vmaxq_s16(vqaddq_s16(g[i].val[t52], b_kp1[2]),
                     vqaddq_s16(g[i].val[t56], b_kp1[6]));
    b[6] = vmaxq_s16(vqaddq_s16(g[i].val[t63], b_kp1[3]),
                     vqaddq_s16(g[i].val[t67], b_kp1[7]));
    b[7] = vmaxq_s16(vqaddq_s16(g[i].val[t73], b_kp1[3]),
                     vqaddq_s16(g[i].val[t77], b_kp1[7]));

    // LLR
    int16x8_t prob_0 = vmaxq_s16(
        vmaxq_s16(
            vmaxq_s16(
                vqaddq_s16(vqaddq_s16(a[k_idx + 0], b_kp1[0]), g[i].val[t00]),
                vqaddq_s16(vqaddq_s16(a[k_idx + 1], b_kp1[4]), g[i].val[t14])),
            vmaxq_s16(
                vqaddq_s16(vqaddq_s16(a[k_idx + 2], b_kp1[5]), g[i].val[t25]),
                vqaddq_s16(vqaddq_s16(a[k_idx + 3], b_kp1[1]), g[i].val[t31]))),
        vmaxq_s16(
            vmaxq_s16(
                vqaddq_s16(vqaddq_s16(a[k_idx + 4], b_kp1[2]), g[i].val[t42]),
                vqaddq_s16(vqaddq_s16(a[k_idx + 5], b_kp1[6]), g[i].val[t56])),
            vmaxq_s16(
                vqaddq_s16(vqaddq_s16(a[k_idx + 6], b_kp1[7]), g[i].val[t67]),
                vqaddq_s16(vqaddq_s16(a[k_idx + 7], b_kp1[3]),
                           g[i].val[t73]))));
    int16x8_t prob_1 = vmaxq_s16(
        vmaxq_s16(
            vmaxq_s16(
                vqaddq_s16(vqaddq_s16(a[k_idx + 0], b_kp1[4]), g[i].val[t04]),
                vqaddq_s16(vqaddq_s16(a[k_idx + 1], b_kp1[0]), g[i].val[t10])),
            vmaxq_s16(
                vqaddq_s16(vqaddq_s16(a[k_idx + 2], b_kp1[1]), g[i].val[t21]),
                vqaddq_s16(vqaddq_s16(a[k_idx + 3], b_kp1[5]), g[i].val[t35]))),
        vmaxq_s16(
            vmaxq_s16(
                vqaddq_s16(vqaddq_s16(a[k_idx + 4], b_kp1[6]), g[i].val[t46]),
                vqaddq_s16(vqaddq_s16(a[k_idx + 5], b_kp1[2]), g[i].val[t52])),
            vmaxq_s16(
                vqaddq_s16(vqaddq_s16(a[k_idx + 6], b_kp1[3]), g[i].val[t63]),
                vqaddq_s16(vqaddq_s16(a[k_idx + 7], b_kp1[7]),
                           g[i].val[t77]))));

    llr[i] = vqsubq_s16(prob_0, prob_1);

    for (uint32_t s = 0; s < states; ++s) {
      b_kp1[s] = b[s];
    }
  }
}

} // namespace
