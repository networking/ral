/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "intrinsics.h"
#include "turbo_tables.hpp"
#include "utils/allocators.hpp"

#include <cmath>

namespace armral::turbo {

// Check that the number of bits, k, is one of the valid
// values for LTE turbo coding as specified in TS36.212
inline bool valid_num_bits(uint32_t k) {
  if (k < 40) {
    return false;
  }
  if (k <= 512) {
    return k % 8 == 0;
  }
  if (k <= 1024) {
    return k % 16 == 0;
  }
  if (k <= 2048) {
    return k % 32 == 0;
  }
  if (k <= 6144) {
    return k % 64 == 0;
  }
  return false;
}

// Generate the permuted index for given value of k using the polynomial
// scheme described in TS36.212
inline uint16_t generate_perm_idx(uint32_t i, uint16_t f1, uint16_t f2,
                                  uint32_t k) {
  // 0 <= perm < 6144 but f2*i*i may be much larger
  return static_cast<uint16_t>((uint64_t(f1) * i + uint64_t(f2) * i * i) % k);
}

struct perm_idx_lookup {
  uint16_t perm_idx;
  uint16_t vec_idx;
  uint16_t vec_lane;
};

inline void k_perm_idx_init(uint16_t k, uint16_t k_idx,
                            perm_idx_lookup *perm_idxs) {
  // Extract the correct values of f1 and f2 to build the
  // interleaving polynomial
  uint16_t f1 = perm_params[k_idx][0];
  uint16_t f2 = perm_params[k_idx][1];
  // Generate the permutation vector for the input value of k.
  for (uint16_t i = 0; i < k; ++i) {
    uint16_t perm_idx = generate_perm_idx(i, f1, f2, k);
    perm_idxs[i].perm_idx = perm_idx;
    perm_idxs[i].vec_idx = perm_idx / 8;
    perm_idxs[i].vec_lane = perm_idx % 8;
  }
}

inline armral_status all_perm_idx_init(perm_idx_lookup *buffer) {

  uint16_t k = 40;
  uint16_t k_idx = 0;
  for (; k < 512; k += 8, ++k_idx) {
    k_perm_idx_init(k, k_idx, buffer + perm_lookup_offset[k_idx]);
  }
  for (; k < 1024; k += 16, ++k_idx) {
    k_perm_idx_init(k, k_idx, buffer + perm_lookup_offset[k_idx]);
  }
  for (; k < 2048; k += 32, ++k_idx) {
    k_perm_idx_init(k, k_idx, buffer + perm_lookup_offset[k_idx]);
  }
  for (; k <= 6144; k += 64, ++k_idx) {
    k_perm_idx_init(k, k_idx, buffer + perm_lookup_offset[k_idx]);
  }

  return ARMRAL_SUCCESS;
}

// Take the input int8_t LLRs and convert them to int16x8_ts.
// This can be used as is for both the batched and the single case
inline void convert_llrs(uint32_t length, const int8_t *llrs,
                         int16x8_t *llrs_i16) {
  constexpr uint32_t vec_len = 8;
  uint32_t i = 0;
  uint32_t v = 0;
  for (; i < length - vec_len + 1; i += vec_len, ++v) {
    llrs_i16[v] = vshll_n_s8(vld1_s8(&llrs[i]), 0);
  }
  for (uint32_t r = 0; r < length % vec_len; ++r) {
    llrs_i16[v][r] = static_cast<int16_t>(llrs[i + r]);
  }
}

// Update the extrinsic information output from the decoding stage
// based on the computed LLRs, the old extrinsic information and the input
// This can be used as is for both the batched and the single case
inline void update_extrinsic(uint32_t length, const int16x8_t *llr,
                             int16x8_t *extrinsic, const int16x8_t *input) {
  for (uint32_t i = 0; i < length; i++) {
    extrinsic[i] = vqsubq_s16(vqsubq_s16(llr[i], extrinsic[i]), input[i]);
  }
}

// With Turbo codes n (=k) is always divisible by 8 so we
// do not have to worry about tail bits
inline void turbo_llrs_to_bits(uint32_t n, const int16x8_t *llr,
                               uint8_t *data_out) {
  uint32_t full_bytes = n >> 3;
  constexpr uint16x8_t ones = {128, 64, 32, 16, 8, 4, 2, 1};

  for (uint32_t i = 0; i < full_bytes; ++i) {
    // The first bit to write in the byte is the most significant
    uint16x8_t pred = vcltzq_s16(llr[i]);
    uint16x8_t mask = vandq_u16(pred, ones);
    data_out[i] = (uint8_t)vaddvq_u16(mask);
  }
}

template<bool batched, typename Vec>
void permute(perm_idx_lookup *perm_lookup, Vec *from, Vec *to,
             uint32_t arr_length, uint32_t vec_length = 8) {
  for (uint32_t i = 0; i < arr_length; ++i) {
    if constexpr (batched) {
      to[i] = from[perm_lookup[i].perm_idx];
    } else {
      for (uint32_t j = 0; j < vec_length; j++) {
        to[i][j] = from[perm_lookup[(i * vec_length) + j].vec_idx]
                       [perm_lookup[(i * vec_length) + j].vec_lane];
      }
    }
  }
}

template<bool batched, typename Vec>
void unpermute(perm_idx_lookup *perm_lookup, Vec *from, Vec *to,
               uint32_t arr_length, uint32_t vec_length = 8) {
  for (uint32_t i = 0; i < arr_length; i++) {
    if constexpr (batched) {
      to[perm_lookup[i].perm_idx] = from[i];
    } else {
      for (uint32_t j = 0; j < vec_length; j++) {
        to[perm_lookup[i * vec_length + j].vec_idx]
          [perm_lookup[i * vec_length + j].vec_lane] = from[i][j];
      }
    }
  }
}

template<typename T>
void interleave(const T *src, uint32_t ldsrc, T *dst, uint32_t lddst) {
  for (uint32_t a = 0; a < ldsrc; ++a) {
    for (uint32_t b = 0; b < lddst; ++b) {
      dst[a * lddst + b] = src[b * ldsrc + a];
    }
  }
}

using trellis_term_func_t = void (*)(const int16x8_t *sys, const int16x8_t *par,
                                     uint32_t k, int16x8_t *beta_tail,
                                     int16x8_t l_c);
using decode_step_func_t = void (*)(const int16x8_t *sys, const int16x8_t *par,
                                    const int16x8_t *extrinsic, uint32_t k,
                                    int16x8_t *llr, int16x8_t *alpha,
                                    int16x8_t *beta_tail, void *gamma,
                                    int16x8_t l_c);

// We set the channel reliability parameter l_c to a value that simplifies
// to 1 in the computation of the gamma values. This is justified because
// max-log-MAP decoding is independent of the channel reliability (which is
// itself relative to the channel SNR). For reference see:
//  N. Wehn, "Turbo-decoding without SNR estimation", IEEE Communications
//  Letters 4(6), pp. 193-195, July 2000.
template<bool batched, bool check_convergence>
armral_status
decode_loop(int16x8_t *sys, int16x8_t *pys, int16x8_t *par, int16x8_t *itl,
            int16x8_t *extrinsic, int16x8_t *perm_extrinsic, int16x8_t *llr,
            int16x8_t *perm_llr, int16x8_t *prev_perm_llr, int16x8_t *alpha,
            int16x8_t *beta_tail, int16x8_t *perm_beta_tail, int16_t *gamma,
            uint32_t k, uint8_t *dst, float32_t l_c, uint32_t max_iter,
            perm_idx_lookup *perm_lookup,
            trellis_term_func_t trellis_termination,
            decode_step_func_t decode_step) {
  // Middle decoder function for either a single block or batch of 8 blocks.
  // The inputs (sys_i8 par_i8 itl_i8) to this function are interleaved,
  // The output (dst) is returned uninterleaved.
  //
  // This function does some batch/block specific setup (permute sys into pys,
  // handle trellis termination, and alpha/beta init)
  // Then it executes the decode loop using decode_step().

  constexpr uint32_t vec_len = 8;
  const uint32_t kv = batched ? k : k / vec_len;

  // permute sys into pys
  // perm_idx is < k, so we can do this before handling trellis termination
  permute<batched>(perm_lookup, sys, pys, kv);
  // Unperturb the trellis termination bits. They are transmitted as:
  // <sys> X0 Z1 X'0 Z'1 <par> Z0 X2 Z'0 X'2 <itl> X1 Z2 X'1 Z'2
  // but need to appended to the inputs as:
  // <sys> X0 X1 X2
  // <par> Z0 Z1 Z2
  // <pys> X'0 X'1 X'2
  // <itl> Z'0 Z'1 Z'2
  // Order like this so we can copy inplace
  if constexpr (batched) {
    // sys[k] = sys[k];
    // par[k] = par[k];
    pys[k] = sys[k + 2];
    sys[k + 2] = par[k + 1];
    par[k + 1] = sys[k + 1];
    sys[k + 1] = itl[k];
    itl[k] = par[k + 2];
    par[k + 2] = itl[k + 1];
    itl[k + 1] = sys[k + 3];
    pys[k + 1] = itl[k + 2];
    itl[k + 2] = itl[k + 3];
    pys[k + 2] = par[k + 3];
  } else {
    // sys[kv][0] = sys[kv][0];
    // par[kv][0] = par[kv][0];
    pys[kv][0] = sys[kv][2];
    sys[kv][2] = par[kv][1];
    par[kv][1] = sys[kv][1];
    sys[kv][1] = itl[kv][0];
    itl[kv][0] = par[kv][2];
    par[kv][2] = itl[kv][1];
    itl[kv][1] = sys[kv][3];
    pys[kv][1] = itl[kv][2];
    itl[kv][2] = itl[kv][3];
    pys[kv][2] = par[kv][3];
  }
  // Prescale l_c to avoid doing it a bunch later
  const int16x8_t channel_reliability = vdupq_n_s16((int16_t)l_c / 2);
  // Initialize alpha (= zero or min) and beta (from the trellis termination)
  int16x8_t min = vdupq_n_s16(std::numeric_limits<int16_t>::min());
  if constexpr (batched) {
    alpha[0] = vdupq_n_s16(0);
    alpha[1] = min;
    alpha[2] = min;
    alpha[3] = min;
    alpha[4] = min;
    alpha[5] = min;
    alpha[6] = min;
    alpha[7] = min;
  } else {
    alpha[0] = min;
    alpha[0][0] = 0;
  }
  trellis_termination(sys, par, kv, beta_tail, channel_reliability);
  trellis_termination(pys, itl, kv, perm_beta_tail, channel_reliability);

  // DECODE
  for (uint32_t num_iter = 0; num_iter < max_iter; ++num_iter) {
    decode_step(sys, par, extrinsic, kv, llr, alpha, beta_tail, gamma,
                channel_reliability);

    update_extrinsic(kv, llr, extrinsic, sys);
    permute<batched>(perm_lookup, extrinsic, perm_extrinsic, kv);

    decode_step(pys, itl, perm_extrinsic, kv, perm_llr, alpha, perm_beta_tail,
                gamma, channel_reliability);

    update_extrinsic(kv, perm_llr, perm_extrinsic, pys);
    unpermute<batched>(perm_lookup, perm_extrinsic, extrinsic, kv);

    // CHECK CONVERGENCE
    int16_t max_abs_diff = 0;
    for (uint32_t i = 0; i < kv; ++i) {
      int16_t abs_diff =
          vmaxvq_s16(vqabsq_s16(vqsubq_s16(perm_llr[i], prev_perm_llr[i])));
      if (abs_diff > max_abs_diff) {
        max_abs_diff = abs_diff;
      }
    }
    // If we've converged, finish decoding
    if constexpr (check_convergence) {
      if (max_abs_diff == 0) {
        break;
      }
    }
    // Store the current "final" LLRs to use in convergence checking next
    // iteration
    for (uint32_t i = 0; i < kv; i++) {
      prev_perm_llr[i] = perm_llr[i];
    }
  }

  // Return unpermuted output (use llr as a buffer)
  unpermute<batched>(perm_lookup, perm_llr, llr, kv);

  if constexpr (batched) { // uninterleave dst (use pys as a buffer)
    interleave((int16_t *)llr, vec_len, (int16_t *)pys, k);
  }
  int16x8_t *llr_buff = batched ? pys : llr;
  constexpr uint32_t num_blocks = batched ? vec_len : 1;
  for (uint32_t b = 0; b < num_blocks; ++b) {
    turbo_llrs_to_bits(k, llr_buff + b * k / vec_len, dst + b * k / vec_len);
  }
  return ARMRAL_SUCCESS;
}

template<bool check_convergence, typename Allocator>
armral_status decode(const int8_t *sys_i8, const int8_t *par_i8,
                     const int8_t *itl_i8, uint32_t k, uint8_t *dst,
                     float32_t l_c, uint32_t max_iter, uint32_t num_blocks,
                     uint16_t *perm_idxs, Allocator &allocator,
                     trellis_term_func_t trellis_termination_single,
                     decode_step_func_t decode_step_single,
                     trellis_term_func_t trellis_termination_batched,
                     decode_step_func_t decode_step_batched) {
  // Outer decoder function for a single block or batch of `num_blocks` blocks.
  // The inputs and the output to this function are uninterleaved.
  //
  // eg. for int8_t elements ki_bj of sys_i8:
  // For batched data the vectorization strategy is to have 8 interleaved blocks
  // int16x8_t *sys: {{k0_b0, k0_b1, ... k0_b7}, {k1_b0, k1_b1, ...}, ...}
  // For single, vectorize within the block:
  // int16x8_t *sys: {{k0_b0, k1_b0, ... k7_b0}, {k8_b0, k9_b0, ...}, ...}
  // So for the batched case, interleave then decode 8 blocks. (as vec_len = 8)
  // For the single case, vectorize the data as is.

  if (!valid_num_bits(k)) {
    return ARMRAL_ARGUMENT_ERROR;
  }

  constexpr uint32_t vec_len = 8;
  // For smaller batch sizes, just run the single decoder num_blocks times.
  const bool batched = num_blocks >= vec_len;
  // k Adjusted by vec and batch size (no change when v_s = b_s)
  const uint32_t kv = batched ? k : k / vec_len;
  const uint32_t len = batched ? k + 4 : kv + 1;
  const uint32_t beta_tail_len = batched ? vec_len : 1;

  auto sys = allocate_uninitialized<int16x8_t>(allocator, len);
  auto par = allocate_uninitialized<int16x8_t>(allocator, len);
  auto pys = allocate_uninitialized<int16x8_t>(allocator, len); // permuted sys
  auto itl = allocate_uninitialized<int16x8_t>(allocator, len);
  auto extrinsic = allocate_zeroed<int16x8_t>(allocator, kv);
  auto perm_extrinsic = allocate_uninitialized<int16x8_t>(allocator, kv);
  // Allocate space for log likelihood ratios from both stages of decoding
  auto llr = allocate_uninitialized<int16x8_t>(allocator, kv);
  auto perm_llr = allocate_uninitialized<int16x8_t>(allocator, kv);
  auto prev_perm_llr = allocate_zeroed<int16x8_t>(allocator, kv);
  // Allocate space to hold alpha and gamma
  // alpha stores the forward-accumulated state probabilities for each decoded
  // bit, where the LTE encoder has 8 states and there are k bits to decode
  // plus the starting condition (no alpha for the 3 trellis termination bits)
  auto alpha = allocate_uninitialized<int16x8_t>(allocator, 8 * (kv + 1));
  auto beta_tail = allocate_uninitialized<int16x8_t>(allocator, beta_tail_len);
  auto perm_beta_tail =
      allocate_uninitialized<int16x8_t>(allocator, beta_tail_len);
  // gamma stores the conditional state transition probabilities for each of the
  // k bits to decode. There are 16 transitions per k but only 4 unique values.
  // Use int16_t so decode_step has same signature for batched and single.
  auto gamma = allocate_uninitialized<int16_t>(allocator, kv * vec_len * 4);

  // PERM_IDXS
  // Get the permutation vector for the input value of k.
  // If perm_idxs is uninitialized (==nullptr) then generate indices here.
  unique_ptr<Allocator, perm_idx_lookup> perm_lookup_unique;
  perm_idx_lookup *perm_lookup = nullptr;
  // Find the index into the array of parameter arrays corresponding
  // to the current k. Subtract 40 because k=40 is the lowest value.
  uint32_t param_idx = perm_params_lookup[(k - 40) >> 3];
  if (perm_idxs != nullptr) {
    if constexpr (Allocator::is_counting) { // NOTE: All allocations done.
      return ARMRAL_SUCCESS;
    }
    perm_lookup = (perm_idx_lookup *)perm_idxs + perm_lookup_offset[param_idx];
  } else {
    perm_lookup_unique = allocate_uninitialized<perm_idx_lookup>(allocator, k);
    if constexpr (Allocator::is_counting) { // NOTE: All allocations done.
      return ARMRAL_SUCCESS;
    }
    perm_lookup = perm_lookup_unique.get();
    // Generate the permutation vector for the input value of k.
    k_perm_idx_init(k, param_idx, perm_lookup);
  }

  // How many elements in input data?
  // This is different than `len` which accounts for vectorizing
  const uint32_t dat_len = k + 4;
  uint32_t b = 0; // block index
  uint32_t dat_offset = 0;
  uint32_t dst_offset = 0;
  for (; batched && b < num_blocks - vec_len + 1;
       b += vec_len, dat_offset += vec_len * dat_len, dst_offset += k) {
    // Decode 8 blocks

    if (b > 0) { // Re-zero buffers which should start at 0
      for (uint32_t i = 0; i < k; i++) {
        extrinsic[i] = vdupq_n_s16(0);
        prev_perm_llr[i] = vdupq_n_s16(0);
      }
    }

    // Convert type and vectorize, then interleave (use pys as a buffer)
    convert_llrs(dat_len * vec_len, sys_i8 + dat_offset, pys.get());
    interleave((int16_t *)pys.get(), dat_len, (int16_t *)sys.get(), vec_len);

    convert_llrs(dat_len * vec_len, par_i8 + dat_offset, pys.get());
    interleave((int16_t *)pys.get(), dat_len, (int16_t *)par.get(), vec_len);

    convert_llrs(dat_len * vec_len, itl_i8 + dat_offset, pys.get());
    interleave((int16_t *)pys.get(), dat_len, (int16_t *)itl.get(), vec_len);

    decode_loop<true, check_convergence>(
        sys.get(), pys.get(), par.get(), itl.get(), extrinsic.get(),
        perm_extrinsic.get(), llr.get(), perm_llr.get(), prev_perm_llr.get(),
        alpha.get(), beta_tail.get(), perm_beta_tail.get(), gamma.get(), k,
        dst + dst_offset, l_c, max_iter, perm_lookup,
        trellis_termination_batched, decode_step_batched);
  }
  for (; b < num_blocks;
       ++b, dat_offset += dat_len, dst_offset += k / vec_len) {
    // Decode 1 block

    if (b > 0) { // Re-zero buffers which should start at 0
      for (uint32_t i = 0; i < k / vec_len; i++) {
        extrinsic[i] = vdupq_n_s16(0);
        prev_perm_llr[i] = vdupq_n_s16(0);
      }
    }

    // Convert type and vectorize
    convert_llrs(dat_len, sys_i8 + dat_offset, sys.get());
    convert_llrs(dat_len, par_i8 + dat_offset, par.get());
    convert_llrs(dat_len, itl_i8 + dat_offset, itl.get());

    decode_loop<false, check_convergence>(
        sys.get(), pys.get(), par.get(), itl.get(), extrinsic.get(),
        perm_extrinsic.get(), llr.get(), perm_llr.get(), prev_perm_llr.get(),
        alpha.get(), beta_tail.get(), perm_beta_tail.get(), gamma.get(), k,
        dst + dst_offset, l_c, max_iter, perm_lookup,
        trellis_termination_single, decode_step_single);
  }
  return ARMRAL_SUCCESS;
}

} // namespace armral::turbo
