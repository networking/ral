/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "turbo_code_common.hpp"
#include "turbo_tables.hpp"
#include "utils/allocators.hpp"

#include <cstdlib>
#include <cstring>

namespace {

// implements the recursive systematic convolutional (RSC) encoder
// used in LTE turbo encoding to generate the parity bits
inline void rsc_encode(const uint8_t *c, uint32_t k_bytes, uint8_t &state,
                       uint8_t *z) {
  // for every byte in the input data
  for (uint32_t k = 0; k < k_bytes; k++) {
    uint8_t input_block = c[k];
    // use input byte
    z[k] = armral::turbo::encoded_bytes[8 * input_block + state];
    // update encoder state
    state = armral::turbo::new_state_bytes[8 * input_block + state];
  }
}

// implements the recursive systematic convolutional (RSC) encoder
// used in LTE turbo encoding to generate the trellis termination bits
inline void trellis_encode(uint8_t &state, uint8_t &x, uint8_t &z) {
  x = 0;
  z = 0;
  // generate 3 bits of output in x and z
  for (int i = 2; i >= 0; i--) {
    uint8_t symbol = armral::turbo::trellis_output_symbol[8 * i + state];
    x |= symbol;
    symbol = armral::turbo::trellis_encoded_symbol[8 * i + state];
    z |= symbol;
    // the state transitions here are:
    // old state  0 1 2 3 4 5 6 7
    // new state  0 0 1 1 2 2 3 3
    // so we just shift state right by one each time
    state >>= 1;
  }
}

// generates the trellis termination bits and stores them in the bit locations
// specified in TS 36.212 Section 5.1.3.2.2
inline void terminate_trellis(uint8_t &state0, uint8_t &state1, uint8_t *d0,
                              uint8_t *d1, uint8_t *d2, uint32_t k_bytes) {
  uint8_t x;
  uint8_t z;
  uint8_t x_prime;
  uint8_t z_prime;
  trellis_encode(state0, x, z);
  trellis_encode(state1, x_prime, z_prime);

  // the first 4 trellis termination bits are appended to d0 (systematic output)
  // they will be the in the 4 msbs of d0[6], leaving the other 4 to be zero
  // we need:
  //     x[0] (bit 5 (0 based) of x) -> msb of d0[k_bytes]
  //     z[1] (bit 6 of z)           -> msb-1 of d0[k_bytes]
  //    x'[0] (bit 5 of x_prime)     -> msb-2 of d0[k_bytes]
  //    z'[1] (bit 6 of z_prime)     -> msb-3 of d0[k_bytes]
  d0[k_bytes] |= (x & 0x4) << 5;
  d0[k_bytes] |= (z & 0x2) << 5;
  d0[k_bytes] |= (x_prime & 0x4) << 3;
  d0[k_bytes] |= (z_prime & 0x2) << 3;

  // append the next 4 trellis termination bits to d1:  z[0] x[2] z'[0] x'[2]
  d1[k_bytes] |= (z & 0x4) << 5;
  d1[k_bytes] |= (x & 0x1) << 6;
  d1[k_bytes] |= (z_prime & 0x4) << 3;
  d1[k_bytes] |= (x_prime & 0x1) << 4;

  // append the last 4 trellis termination bits to d2:  x[1] z[2] x'[1] z'[2]
  d2[k_bytes] |= (x & 0x2) << 6;
  d2[k_bytes] |= (z & 0x1) << 6;
  d2[k_bytes] |= (x_prime & 0x2) << 4;
  d2[k_bytes] |= (z_prime & 0x1) << 4;
}

// permutes the bits of c according to the permutation stored in perm_idx:
//   c_prime[i] = c[perm_idx[i]]
inline void interleave(const uint8_t *c, uint8_t *c_prime, uint32_t k) {
  // find the index into the array of parameter arrays corresponding
  // to the current k. Subtract 40 because k=40 is the lowest value.
  int param_idx = armral::turbo::perm_params_lookup[(k - 40) >> 3];
  // and extract the correct values of f1 and f2 to build the
  // interleaving polynomial
  uint16_t f1 = armral::turbo::perm_params[param_idx][0];
  uint16_t f2 = armral::turbo::perm_params[param_idx][1];
  for (uint32_t i = 0; i < k; i++) {
    // 0 <= perm_idx < 6144 but f2*i*i may be much larger
    int perm_idx = armral::turbo::generate_perm_idx(i, f1, f2, k);
    int src_byte = perm_idx >> 3;
    int src_bit = perm_idx & 7;
    int dst_byte = i >> 3;
    int dst_bit = i & 7;
    int shift = dst_bit - src_bit;
    if (shift < 0) {
      c_prime[dst_byte] |= (c[src_byte] & (128 >> src_bit)) << std::abs(shift);
    } else {
      c_prime[dst_byte] |= (c[src_byte] & (128 >> src_bit)) >> std::abs(shift);
    }
  }
}

template<typename Allocator>
armral_status turbo_encode_block(const uint8_t *src, uint32_t k, uint8_t *dst0,
                                 uint8_t *dst1, uint8_t *dst2,
                                 Allocator &allocator) {
  if (!armral::turbo::valid_num_bits(k)) {
    return ARMRAL_ARGUMENT_ERROR;
  }

  // the value of k in 8-bit bytes
  uint32_t k_bytes = k >> 3;

  auto src_prime = allocate_zeroed<uint8_t>(allocator, k_bytes);

  // the internal state of the first 3-bit (8-state) RSC encoder
  // as per the spec we start with all 3 registers empty
  uint8_t state0 = 0;

  // the internal state of the second 3-bit (8-state) RSC encoder
  // as per the spec we start with all 3 registers empty
  uint8_t state1 = 0;

  // create the systematic part of the output by copying src into dst0
  memcpy(dst0, src, k_bytes);

  // zero all the output data
  for (uint32_t i = 0; i < k_bytes; i++) {
    dst1[i] = 0;
    dst2[i] = 0;
  }
  dst0[k_bytes] = 0;
  dst1[k_bytes] = 0;
  dst2[k_bytes] = 0;

  // create the interleaved version of the input data
  // to be sent as input to the second RSC encoder
  interleave(src, src_prime.get(), k);

  // encode src through the first RSC encoder
  // and store the output in dst1
  rsc_encode(src, k_bytes, state0, dst1);

  // encode src_prime through the second RSC encoder
  // and store the output in dst2
  rsc_encode(src_prime.get(), k_bytes, state1, dst2);

  // terminate the trellis by flushing both encoders to 0
  terminate_trellis(state0, state1, dst0, dst1, dst2, k_bytes);

  return ARMRAL_SUCCESS;
}

} // namespace

armral_status armral_turbo_encode_block(const uint8_t *src, uint32_t k,
                                        uint8_t *dst0, uint8_t *dst1,
                                        uint8_t *dst2) {
  heap_allocator allocator{};
  return turbo_encode_block(src, k, dst0, dst1, dst2, allocator);
}

armral_status armral_turbo_encode_block_noalloc(const uint8_t *src, uint32_t k,
                                                uint8_t *dst0, uint8_t *dst1,
                                                uint8_t *dst2, void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return turbo_encode_block(src, k, dst0, dst1, dst2, allocator);
}
