/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "intrinsics.h"

#include <cmath>

namespace {

struct int16x4x8_t {
  int16x4_t val[8];
};

// Calculate the trellis termination values. These are independent of the
// extrinsic information and so can be done once without needing to be updated
// on every iteration.
void trellis_termination(const int16x8_t *sys, const int16x8_t *par,
                         uint32_t k8, int16x8_t *beta_tail, int16x8_t l_c) {
  // We handle the gammas for the trellis termination bits separately
  // as the state transitions are different. The x_{kl} are never 1
  // here, because we always use inputs of 0 to drive the trellis back
  // to state 0 in the encoder, so we only need to consider a smaller
  // number of state transitions. We also do not have any extrinsic
  // information. Because some of the gamma terms will always be
  // -INFINITY (specifically indices [1] and [3]) we can forgo adding
  // to them to beta or taking the max with them, compared with when
  // we calculate beta in the main calculations. As above, we assume
  // that the channel reliability parameter l_c/2 = 1.
  int16x4_t pdf_00 = vget_low_s16(vqaddq_s16(sys[k8], par[k8]));
  int16x4_t pdf_01 = vget_low_s16(vqsubq_s16(sys[k8], par[k8]));

  int16x8_t g0102 = {pdf_00[1], pdf_01[1], pdf_00[1], pdf_01[1],
                     pdf_00[1], pdf_01[1], pdf_00[1], pdf_01[1]};

  int16x8_t b01 = {pdf_00[2], pdf_00[2], pdf_01[2], pdf_01[2],
                   pdf_00[2], pdf_00[2], pdf_01[2], pdf_01[2]};

  int16x8_t beta_term = vqaddq_s16(g0102, b01);

  int16x8_t g = {pdf_00[0], pdf_01[0], pdf_00[0], pdf_01[0],
                 pdf_01[0], pdf_00[0], pdf_01[0], pdf_00[0]};

  int16x8_t b0123 = vzip1q_s16(beta_term, beta_term);

  *beta_tail = vqaddq_s16(g, b0123);
}

// A single max-log-MAP decoder that works on an array of systematic bits (sys),
// an array of parity bits (par), and an array of extrinsic values from a
// previous decoding stage (extrinsic)
void decode_block_step(const int16x8_t *sys, const int16x8_t *par,
                       const int16x8_t *extrinsic, uint32_t k8, int16x8_t *llr,
                       int16x8_t *alpha, int16x8_t *beta_tail, void *gamma_i16,
                       int16x8_t l_c) {
  constexpr uint32_t normalize_frequency = 4;
  uint32_t k_idx;
  uint32_t kp1_idx;
  // cast gamma to align function signature with batched decode_step
  int16x4x8_t *pdf4 = static_cast<int16x4x8_t *>(gamma_i16);

  // Start by computing the non-zero conditional state transition probabilities
  // from state s' to state s for every k, denoted gamma_k(s',s). In general for
  // an AWGN channel (ignoring extrinsic information in l_uk):
  //        gamma_k(s',s) = exp(L_c / 2 \sum_{l=1}^{n} x_{kl} y_{kl})
  // Here there are only 2 possible state transitions into each state
  // (corresponding to encoding a 0 bit or a 1 bit) so the summation only has 2
  // terms.
  for (uint32_t i = 0; i < k8; i++) {
    // The x_{kl} values are the actual systematic and parity values that
    // would result from the encoder having transited from state s' to s.
    // They can only ever be either 0 or 1 so we precompute the four possible
    // values in the exponential for x = (0,0), (0,1), (1,0) and (1,1). Note
    // that these 0s and 1s have to be converted to 1s and -1s to match the
    // values in y.
    //
    // The y_{kl} values are the observed systematic and parity inputs.
    // These have potentially been perturbed by noise on the channel.
    //
    // Although each of the 8 states of the encoder has in theory 8
    // predecessor states, the encoder's structure means that not all state
    // transitions are possible. Each state actually only has 2 predecessor
    // states so we only have to compute 16 non-zero values for each input
    // LLR.
    //
    // We calculate the PDF of the state transition probability on the
    // assumption that we are operating on an AWGN channel:
    //     PDF = (x1/2 (l_uk + l_c*y1)) + (l_c/2 x2 y2)
    // where l_uk is the extrinsic information, y1 is the systematic
    // input, and y2 is the parity input. We assume the channel
    // reliability, l_c, is set such that l_c/2 = 1 and therefore omit
    // it from the calculation. See arm_turbo_decoder.cpp for
    // justification.

    int16x8_t term = vqaddq_s16(extrinsic[i] >> 1, sys[i]);
    int16x8_t pdf_00 = vqaddq_s16(term, par[i]);
    int16x8_t pdf_10 = vqsubq_s16(par[i], term);
    int16x8_t pdf_01 = vqsubq_s16(term, par[i]);
    int16x8_t pdf_11 = vqsubq_s16(vqnegq_s16(term), par[i]);

    // There is considerable duplication in the values we could store. For
    // example, for a single state the 16 gamma values are:
    //
    //      gamma[g_k_idx]   = {pdf_00[j], pdf_11[j], pdf_11[j], pdf_00[j]};
    //      gamma[g_k_idx+1] = {pdf_10[j], pdf_01[j], pdf_01[j], pdf_10[j]};
    //      gamma[g_k_idx+2] = {pdf_01[j], pdf_10[j], pdf_10[j], pdf_01[j]};
    //      gamma[g_k_idx+3] = {pdf_11[j], pdf_00[j], pdf_00[j], pdf_11[j]};
    //
    // We therefore choose to store the 4 unique pdf values (using
    // st4) as this allows us to access the pdf values contiguously in
    // the calculations needed for the alpha and beta values.
    vst4q_s16((int16_t *)&pdf4[i],
              int16x8x4_t({pdf_00, pdf_10, pdf_01, pdf_11}));

    // Accumulate the state transition probabilities forwards through
    // the state transition trellis starting from the known encoder
    // start state 0.

    constexpr int8x16_t idx_0123321 = {0, 1, 2, 3, 4, 5, 6, 7,
                                       6, 7, 4, 5, 2, 3, 0, 1};

    constexpr int8x16_t idx_32100123 = {6, 7, 4, 5, 2, 3, 0, 1,
                                        0, 1, 2, 3, 4, 5, 6, 7};

    for (uint32_t j = 0; j < 8; j++) {
      k_idx = 8 * i + j;
      kp1_idx = k_idx + 1;

      // We need  g02 = {gamma[g_k_idx][0], gamma[g_k_idx + 1][0],
      //                 gamma[g_k_idx + 2][0], gamma[g_k_idx + 3][0],
      //                 gamma[g_k_idx][2], gamma[g_k_idx + 1][2],
      //                 gamma[g_k_idx + 2][2], gamma[g_k_idx + 3][2]};
      int16x8_t g02 = vreinterpretq_s16_s8(
          vtbl1q_s8(vreinterpret_s8_s16(pdf4[i].val[j]), idx_0123321));

      // We need  a02 = {alpha[k_idx][0], alpha[k_idx][2],
      //                 alpha[k_idx + 1][0], alpha[k_idx + 1][2],
      //                 alpha[k_idx][0], alpha[k_idx][2],
      //                 alpha[k_idx + 1][0], alpha[k_idx + 1][2]};
      int16x8_t a02 = vuzp1q_s16(alpha[k_idx], alpha[k_idx]);
      int16x8_t left = vqaddq_s16(g02, a02);

      // This is g02 with the 64-bit elements swapped
      int16x8_t g20 = vreinterpretq_s16_s8(
          vtbl1q_s8(vreinterpret_s8_s16(pdf4[i].val[j]), idx_32100123));

      // We need  a13 = {alpha[k_idx][1], alpha[k_idx][3],
      //                 alpha[k_idx + 1][1], alpha[k_idx + 1][3],
      //                 alpha[k_idx][1], alpha[k_idx][3],
      //                 alpha[k_idx + 1][1], alpha[k_idx + 1][3]};
      int16x8_t a13 = vuzp2q_s16(alpha[k_idx], alpha[k_idx]);
      int16x8_t right = vqaddq_s16(g20, a13);

      alpha[kp1_idx] = vmaxq_s16(left, right);

      // Normalize alpha
      if (j % normalize_frequency == 0) {
        int16x8_t alpha0 = vdupq_n_s16(alpha[kp1_idx][0]);
        alpha[kp1_idx] = vqsubq_s16(alpha[kp1_idx], alpha0);
      }
    }
  }

  // Accumulate the state transition probabilities backwards through the state
  // transition trellis starting from the beginning of the precomputed tail
  // and calculate the conditional probabilities of each bit being either 0
  // or 1.

  constexpr uint8x16_t idx_even_odd = {0, 1, 4, 5, 8,  9,  12, 13,
                                       2, 3, 6, 7, 10, 11, 14, 15};

  constexpr uint8x16_t idx_05274163 = {0, 1, 10, 11, 4,  5,  14, 15,
                                       8, 9, 2,  3,  12, 13, 6,  7};

  constexpr uint8x16_t idx_0220 = {0, 1, 4, 5, 4, 5, 0, 1,
                                   0, 1, 4, 5, 4, 5, 0, 1};

  constexpr uint8x16_t idx_3113 = {6, 7, 2, 3, 2, 3, 6, 7,
                                   6, 7, 2, 3, 2, 3, 6, 7};

  constexpr uint8x16_t idx_0213 = {0, 1, 6, 7, 2, 3, 4, 5,
                                   4, 5, 2, 3, 6, 7, 0, 1};

  constexpr uint8x16_t idx_1302 = {6, 7, 0, 1, 4, 5, 2, 3,
                                   2, 3, 4, 5, 0, 1, 6, 7};

  int16x8_t beta_kp1 = *beta_tail;

  for (int32_t i = k8 - 1; i >= 0; i--) {
    int16x8_t prob_0;
    int16x8_t prob_1;

    for (int32_t j = 7; j >= 0; j--) {
      k_idx = 8 * i + j;

      // Normalize beta
      if (j % normalize_frequency == 0) {
        int16x8_t beta0 = vdupq_n_s16(beta_kp1[0]);
        beta_kp1 = vqsubq_s16(beta_kp1, beta0);
      }

      uint8x16_t pdf8_u8 =
          vreinterpretq_u8_s16(vcombine_s16(pdf4[i].val[j], pdf4[i].val[j]));

      // g0213 = {pdf[0], pdf[3], pdf[1], pdf[2],
      //          pdf[2], pdf[1], pdf[3], pdf[0]};
      int16x8_t g0213 = vreinterpretq_s16_u8(vqtbl1q_u8(pdf8_u8, idx_0213));

      // Reverse 32-bit elements in g0213
      // g1302 = {pdf[3], pdf[0], pdf[2], pdf[1],
      //          pdf[1], pdf[2], pdf[0], pdf[3]};
      int16x8_t g1302 = vreinterpretq_s16_u8(vqtbl1q_u8(pdf8_u8, idx_1302));

      // b0123 = {beta_kp1[0], beta_kp1[0], beta_kp1[1], beta_kp1[1],
      //          beta_kp1[2], beta_kp1[2], beta_kp1[3], beta_kp1[3]};
      // b4567 = {beta_kp1[4], beta_kp1[4], beta_kp1[5], beta_kp1[5],
      //          beta_kp1[6], beta_kp1[6], beta_kp1[7], beta_kp1[7]};
      int16x8_t b0123 = vzip1q_s16(beta_kp1, beta_kp1);
      int16x8_t b4567 = vzip2q_s16(beta_kp1, beta_kp1);

      int16x8_t left = vqaddq_s16(g0213, b0123);
      int16x8_t right = vqaddq_s16(g1302, b4567);

      int16x8_t beta_k = vmaxq_s16(left, right);

      // a0213 = {alpha[k_idx][0], alpha[k_idx][2],
      //          alpha[k_idx][4], alpha[k_idx][6],
      //          alpha[k_idx][1], alpha[k_idx][3],
      //          alpha[k_idx][5], alpha[k_idx][7]};
      int16x8_t a0213 = vreinterpretq_s16_u8(
          vqtbl1q_u8(vreinterpretq_u8_s16(alpha[k_idx]), idx_even_odd));

      // b0213_1302 = {beta_kp1[0], beta_kp1[5], beta_kp1[2], beta_kp1[7],
      //               beta_kp1[4], beta_kp1[1], beta_kp1[6], beta_kp1[3]};
      int16x8_t b0213_1302 = vreinterpretq_s16_u8(
          vqtbl1q_u8(vreinterpretq_u8_s16(beta_kp1), idx_05274163));
      int16x8_t b1302_0213 = vextq_s16(b0213_1302, b0213_1302, 4);

      // g0101 = {pdf[0], pdf[2], pdf[2], pdf[0]};
      int16x8_t g0101 = vreinterpretq_s16_u8(vqtbl1q_u8(pdf8_u8, idx_0220));
      int16x8_t left_right_0 = vqaddq_s16(vqaddq_s16(a0213, b0213_1302), g0101);

      // g1010 = {pdf[3], pdf[1], pdf[1], pdf[3]};
      int16x8_t g1010 = vreinterpretq_s16_u8(vqtbl1q_u8(pdf8_u8, idx_3113));
      int16x8_t left_right_1 = vqaddq_s16(vqaddq_s16(a0213, b1302_0213), g1010);

      prob_0[j] = vmaxvq_s16(left_right_0);
      prob_1[j] = vmaxvq_s16(left_right_1);

      // Store the current value of beta to use in the next
      // round of calculations
      beta_kp1 = beta_k;
    }

    // Calculate the LLRs
    llr[i] = vqsubq_s16(prob_0, prob_1);
  }
}

} // namespace
