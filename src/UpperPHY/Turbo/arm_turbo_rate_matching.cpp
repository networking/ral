/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "turbo_tables.hpp"
#include "utils/allocators.hpp"

#include <cassert>
#include <cmath>
#include <cstring>

namespace armral::turbo {

// Apply the turbo rate matching subblock interleaver defined in
// TS 36.212 Section 5.1.4.1.1
struct subblock_interleave_work_buffers {
  uint8_t *scr;
  uint8_t *y0_perm;
  uint8_t *y1_perm;
};

static void subblock_interleave(uint32_t d, uint32_t kw, const uint8_t *d0,
                                const uint8_t *d1, const uint8_t *d2,
                                uint8_t *v0, uint8_t *v1, uint8_t *v2,
                                uint8_t *dummy0, uint8_t *dummy1,
                                uint8_t *dummy2,
                                subblock_interleave_work_buffers work_buffers) {
  // Length of each information bit stream
  assert(kw % 3 == 0);
  const uint32_t kpi = kw / 3;
  assert(kpi % 8 == 0);
  const uint32_t kpib = kpi / 8;

  uint8_t *const y0 = &work_buffers.scr[0];
  uint8_t *const y1 = &work_buffers.scr[kpib];
  uint8_t *const y2 = &work_buffers.scr[kpib * 2];
  uint8_t *const dt0 = &work_buffers.scr[kpib * 3];
  uint8_t *const dt2 = &work_buffers.scr[kpib * 4];

  // Copy input and pad dummy bits

  // Length of input bits in bytes
  const uint32_t ilen = (d + 7) / 8;

  // Calculate number of bits to be zero-padded
  // Since d = k + 4 (i.e. the number of message bits output from Turbo
  // encoding), nd should always be a multiple of 4
  const uint32_t nd = kpi - d;
  assert(nd % 4 == 0 && nd % 8 == 4);
  const uint32_t ndb = nd / 8;

  // Copy input bits and shift by ndb bytes
  memcpy((void *)&y0[ndb], (const void *)d0, sizeof(uint8_t) * ilen);
  memcpy((void *)&y1[ndb], (const void *)d1, sizeof(uint8_t) * ilen);
  memcpy((void *)&y2[ndb], (const void *)d2, sizeof(uint8_t) * ilen);
  memset((void *)dummy2, 0xFF, sizeof(uint8_t) * ndb);

  // The remaining nd % 8 = 4 bits to be shifted
  const uint8_t s = 4;

  // Right shift 4 bits, extract 4 low bits from the previous byte,
  // and set those in higher 4 bits of the current byte
  y0[ndb] >>= s;
  y1[ndb] >>= s;
  y2[ndb] >>= s;

  for (uint32_t i = 1; i < ilen; ++i) {
    y0[i + ndb] >>= s;
    y0[i + ndb] |= d0[i - 1] << s;
    y1[i + ndb] >>= s;
    y1[i + ndb] |= d1[i - 1] << s;
    y2[i + ndb] >>= s;
    y2[i + ndb] |= d2[i - 1] << s;
  }
  dummy2[ndb] = 0xF0;

  // Number of rows of the information bit matrix
  assert(kpi % armral::turbo::ctc == 0);
  const uint32_t rtc = kpi / armral::turbo::ctc;

  // Perform inter-column permutation for each row of d^(0)_k and d^(1)_k
  for (uint32_t i = 0; i < rtc; ++i) {
    for (uint32_t j = 0; j < armral::turbo::ctc; ++j) {
      uint32_t idx = (armral::turbo::p[j] + i * armral::turbo::ctc) / 8;
      uint32_t jdx = 7 & ~armral::turbo::p[j];
      uint32_t y0bit = (y0[idx] >> jdx) & 1;
      uint32_t y1bit = (y1[idx] >> jdx) & 1;
      uint32_t yperm_jdx = 7 & ~j;
      work_buffers.y0_perm[(j + i * armral::turbo::ctc) / 8] |= y0bit
                                                                << yperm_jdx;
      work_buffers.y1_perm[(j + i * armral::turbo::ctc) / 8] |= y1bit
                                                                << yperm_jdx;
    }
  }

  // Permuted first row of dummy bit matrix is known. So, we can use
  // a look up table
  constexpr uint8_t dummy_table[32][4] = {
      {0x00, 0x00, 0x00, 0x00}, {0x80, 0x00, 0x00, 0x00},
      {0x80, 0x00, 0x80, 0x00}, {0x80, 0x80, 0x80, 0x00},
      {0x80, 0x80, 0x80, 0x80}, {0x88, 0x80, 0x80, 0x80},
      {0x88, 0x80, 0x88, 0x80}, {0x88, 0x88, 0x88, 0x80},
      {0x88, 0x88, 0x88, 0x88}, {0xA8, 0x88, 0x88, 0x88},
      {0xA8, 0x88, 0xA8, 0x88}, {0xA8, 0xA8, 0xA8, 0x88},
      {0xA8, 0xA8, 0xA8, 0xA8}, {0xAA, 0xA8, 0xA8, 0xA8},
      {0xAA, 0xA8, 0xAA, 0xA8}, {0xAA, 0xAA, 0xAA, 0xA8},
      {0xAA, 0xAA, 0xAA, 0xAA}, {0xEA, 0xAA, 0xAA, 0xAA},
      {0xEA, 0xAA, 0xEA, 0xAA}, {0xEA, 0xEA, 0xEA, 0xAA},
      {0xEA, 0xEA, 0xEA, 0xEA}, {0xEE, 0xEA, 0xEA, 0xEA},
      {0xEE, 0xEA, 0xEE, 0xEA}, {0xEE, 0xEE, 0xEE, 0xEA},
      {0xEE, 0xEE, 0xEE, 0xEE}, {0xFE, 0xEE, 0xEE, 0xEE},
      {0xFE, 0xEE, 0xFE, 0xEE}, {0xFE, 0xFE, 0xFE, 0xEE},
      {0xFE, 0xFE, 0xFE, 0xFE}, {0xFF, 0xFE, 0xFE, 0xFE},
      {0xFF, 0xFE, 0xFF, 0xFE}, {0xFF, 0xFF, 0xFF, 0xFE}};

  memcpy((void *)dummy0, (const void *)&dummy_table[nd][0],
         sizeof(uint8_t) * 4);

  // Read out permuted matrix column by column for y^(0) and y^(1)
  // Perform permutation for y^(2)
  for (uint32_t j = 0; j < armral::turbo::ctc; ++j) {
    for (uint32_t i = 0; i < rtc; ++i) {
      uint32_t pi = (armral::turbo::p[j] + i * armral::turbo::ctc + 1) % kpi;
      uint32_t vidx = (i + j * rtc) / 8;
      uint32_t vjdx = 7 & ~(i + j * rtc);
      uint32_t y0idx = (j + i * armral::turbo::ctc) / 8;
      uint32_t y0jdx = 7 & ~(j + i * armral::turbo::ctc);
      uint32_t y2idx = pi / 8;
      uint32_t y2jdx = 7 & ~pi;
      v0[vidx] |= ((work_buffers.y0_perm[y0idx] >> y0jdx) & 1) << vjdx;
      v1[vidx] |= ((work_buffers.y1_perm[y0idx] >> y0jdx) & 1) << vjdx;
      v2[vidx] |= ((y2[y2idx] >> y2jdx) & 1) << vjdx;
      dt0[vidx] |= ((dummy0[y0idx] >> y0jdx) & 1) << vjdx;
      // We do not need to transpose dummy1 because it is the same as dummy0
      dt2[vidx] |= ((dummy2[y2idx] >> y2jdx) & 1) << vjdx;
    }
  }
  memcpy((void *)dummy0, (const void *)dt0, sizeof(uint8_t) * kpib);
  memcpy((void *)dummy1, (const void *)dt0, sizeof(uint8_t) * kpib);
  memcpy((void *)dummy2, (const void *)dt2, sizeof(uint8_t) * kpib);
}

static uint16_t inline interleave_bits(uint8_t a, uint8_t b) {
  uint16_t a16 = (uint16_t)a;
  uint16_t b16 = (uint16_t)b;

  a16 = (a16 | (a16 << 4)) & 0x0F0FU;
  a16 = (a16 | (a16 << 2)) & 0x3333U;
  a16 = (a16 | (a16 << 1)) & 0x5555U;

  b16 = (b16 | (b16 << 4)) & 0x0F0FU;
  b16 = (b16 | (b16 << 2)) & 0x3333U;
  b16 = (b16 | (b16 << 1)) & 0x5555U;

  return (a16 << 1) | b16;
}

static void bit_collection(uint32_t kpi, const uint8_t *v0, const uint8_t *v1,
                           const uint8_t *v2, uint8_t *w, const uint8_t *dummy0,
                           const uint8_t *dummy1, const uint8_t *dummy2,
                           uint8_t *dummy) {
  const uint32_t kpib = kpi / 8;

  // w_k = v^0_k for k = [0, kpi - 1]
  memcpy((void *)w, (const void *)v0, sizeof(uint8_t) * kpib);
  memcpy((void *)dummy, (const void *)dummy0, sizeof(uint8_t) * kpib);

  // w_{kpi + 2k    } = v^1_k,
  // w_{kpi + 2k + 1} = v^2_k for k = [0, kpi - 1]
  for (uint32_t k = 0; k < kpib; ++k) {
    uint16_t w16 = interleave_bits(v1[k], v2[k]);
    uint16_t dummy16 = interleave_bits(dummy1[k], dummy2[k]);
    w[k * 2 + kpib] = (uint8_t)(w16 >> 8);
    w[k * 2 + kpib + 1] = (uint8_t)w16;
    dummy[k * 2 + kpib] = (uint8_t)(dummy16 >> 8);
    dummy[k * 2 + kpib + 1] = (uint8_t)dummy16;
  }
}

static void bit_selection(uint32_t kw, uint32_t ncb, uint32_t k0, uint32_t e,
                          const uint8_t *w, const uint8_t *dummy,
                          uint8_t *out) {
  // This condition is implied in rate_matching() when this function is called
  // so check that it is actually true on entry
  assert(ncb == kw);

  memset((void *)out, 0, sizeof(uint8_t) * (e + 7) / 8);
  uint32_t k = 0;
  uint32_t j = 0;
  while (k < e) {
    uint32_t i = (k0 + j) % ncb;
    uint32_t widx = i / 8;
    uint32_t wjdx = 7 & ~i;
    // Check that w_i is not a dummy bit
    bool not_dummy = ((~dummy[widx] >> wjdx) & 1) == 1U;
    if (not_dummy) {
      uint32_t eidx = k / 8;
      uint32_t ejdx = 7 & ~k;
      out[eidx] |= ((w[widx] >> wjdx) & 1) << ejdx;
      k++;
    }
    j++;
  }
}

template<typename Allocator>
armral_status rate_matching(uint32_t d, uint32_t e, uint32_t rv,
                            const uint8_t *src0, const uint8_t *src1,
                            const uint8_t *src2, uint8_t *dst,
                            Allocator &allocator) {
  assert(d > 0);
  assert(e > 0);
  assert(rv <= 3);

  // The minimum number of rows which gives rtc * ctc >= d
  const uint32_t rtc = (d + armral::turbo::ctc - 1) / armral::turbo::ctc;
  const uint32_t kpi = rtc * armral::turbo::ctc;
  const uint32_t kw = 3 * kpi;
  const uint32_t kpib = kpi / 8;
  const uint32_t kwb = kw / 8;

  auto v0 = allocate_zeroed<uint8_t>(allocator, kpib);
  auto v1 = allocate_zeroed<uint8_t>(allocator, kpib);
  auto v2 = allocate_zeroed<uint8_t>(allocator, kpib);
  auto dummy0 = allocate_zeroed<uint8_t>(allocator, kpib);
  auto dummy1 = allocate_zeroed<uint8_t>(allocator, kpib);
  auto dummy2 = allocate_zeroed<uint8_t>(allocator, kpib);
  auto src = allocate_zeroed<uint8_t>(allocator, kpib * 5);
  auto y0_perm = allocate_zeroed<uint8_t>(allocator, kpib);
  auto y1_perm = allocate_zeroed<uint8_t>(allocator, kpib);
  auto w = allocate_zeroed<uint8_t>(allocator, kwb);
  auto dummy = allocate_zeroed<uint8_t>(allocator, kwb);

  if constexpr (Allocator::is_counting) {
    return ARMRAL_SUCCESS;
  }

  // Assume N_cb = k_w
  const uint32_t ncb = kw;

  // Calculate k0 with the assumption N_cb = k_w
  // k0 = rtc * (2 * N_cb/(8 * rtc) * rv + 2), with N_cb = kw = 3 * ctc * rtc
  const uint32_t k0 = rtc * (24 * rv + 2);

  subblock_interleave(d, kw, src0, src1, src2, v0.get(), v1.get(), v2.get(),
                      dummy0.get(), dummy1.get(), dummy2.get(),
                      {src.get(), y0_perm.get(), y1_perm.get()});

  bit_collection(kpi, v0.get(), v1.get(), v2.get(), w.get(), dummy0.get(),
                 dummy1.get(), dummy2.get(), dummy.get());

  bit_selection(kw, ncb, k0, e, w.get(), dummy.get(), dst);

  return ARMRAL_SUCCESS;
}

} // namespace armral::turbo

armral_status armral_turbo_rate_matching(uint32_t d, uint32_t e, uint32_t rv,
                                         const uint8_t *src0,
                                         const uint8_t *src1,
                                         const uint8_t *src2, uint8_t *dst)

{
  heap_allocator allocator{};
  return armral::turbo::rate_matching(d, e, rv, src0, src1, src2, dst,
                                      allocator);
}

armral_status armral_turbo_rate_matching_noalloc(
    uint32_t d, uint32_t e, uint32_t rv, const uint8_t *src0,
    const uint8_t *src1, const uint8_t *src2, uint8_t *dst, void *buffer)

{
  buffer_bump_allocator allocator{buffer};
  return armral::turbo::rate_matching(d, e, rv, src0, src1, src2, dst,
                                      allocator);
}

uint32_t armral_turbo_rate_matching_noalloc_buffer_size(uint32_t d, uint32_t e,
                                                        uint32_t rv) {
  counting_allocator allocator{};
  (void)armral::turbo::rate_matching(d, e, rv, nullptr, nullptr, nullptr,
                                     nullptr, allocator);
  return allocator.required_bytes();
}
