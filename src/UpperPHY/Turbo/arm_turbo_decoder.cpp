/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "turbo_code_common.hpp"
#include "turbo_tables.hpp"
#include "utils/allocators.hpp"

#include "arm_turbo_decoder_batch.hpp"
#include "arm_turbo_decoder_single.hpp"

// Declarations for no convergence checking
template armral_status armral::turbo::decode<false, heap_allocator>(
    const int8_t *sys, const int8_t *par, const int8_t *itl, uint32_t k,
    uint8_t *dst, float32_t l_c, uint32_t max_iter, uint32_t num_blocks,
    uint16_t *perm_idxs, heap_allocator &, trellis_term_func_t,
    decode_step_func_t, trellis_term_func_t, decode_step_func_t);

template armral_status armral::turbo::decode<false, buffer_bump_allocator>(
    const int8_t *sys, const int8_t *par, const int8_t *itl, uint32_t k,
    uint8_t *dst, float32_t l_c, uint32_t max_iter, uint32_t num_blocks,
    uint16_t *perm_idxs, buffer_bump_allocator &, trellis_term_func_t,
    decode_step_func_t, trellis_term_func_t, decode_step_func_t);

// Permutation indices
armral_status armral_turbo_perm_idx_init(uint16_t *buffer) {
  return armral::turbo::all_perm_idx_init(
      (armral::turbo::perm_idx_lookup *)buffer);
}

// Decode wrappers
armral_status armral_turbo_decode_block(const int8_t *sys, const int8_t *par,
                                        const int8_t *itl, uint32_t k,
                                        uint8_t *dst, uint32_t max_iter,
                                        uint16_t *perm_idxs) {
  heap_allocator allocator{};
  return armral::turbo::decode<true>(sys, par, itl, k, dst, 2.F, max_iter, 1,
                                     perm_idxs, allocator, trellis_termination,
                                     decode_block_step, nullptr, nullptr);
}

armral_status armral_turbo_decode_block_noalloc(
    const int8_t *sys, const int8_t *par, const int8_t *itl, uint32_t k,
    uint8_t *dst, uint32_t max_iter, uint16_t *perm_idxs, void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return armral::turbo::decode<true>(sys, par, itl, k, dst, 2.F, max_iter, 1,
                                     perm_idxs, allocator, trellis_termination,
                                     decode_block_step, nullptr, nullptr);
}

uint32_t armral_turbo_decode_block_noalloc_buffer_size(uint32_t k) {
  counting_allocator allocator{};
  (void)armral::turbo::decode<true>(
      nullptr, nullptr, nullptr, k, nullptr, 2.F, 1, 1, nullptr, allocator,
      trellis_termination, decode_block_step, nullptr, nullptr);
  return allocator.required_bytes();
}

// Batched decoding also requires the single functions, as any remaining blocks
// (modulo the vector size: 8) will be decoded using them.
armral_status armral_turbo_decode_batch(uint32_t num_blocks, const int8_t *sys,
                                        const int8_t *par, const int8_t *itl,
                                        uint32_t k, uint8_t *dst,
                                        uint32_t max_iter,
                                        uint16_t *perm_idxs) {
  heap_allocator allocator{};
  return armral::turbo::decode<true>(
      sys, par, itl, k, dst, 2.F, max_iter, num_blocks, perm_idxs, allocator,
      trellis_termination, decode_block_step, batched_trellis_termination,
      decode_batch_step);
}

armral_status
armral_turbo_decode_batch_noalloc(uint32_t num_blocks, const int8_t *sys,
                                  const int8_t *par, const int8_t *itl,
                                  uint32_t k, uint8_t *dst, uint32_t max_iter,
                                  uint16_t *perm_idxs, void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return armral::turbo::decode<true>(
      sys, par, itl, k, dst, 2.F, max_iter, num_blocks, perm_idxs, allocator,
      trellis_termination, decode_block_step, batched_trellis_termination,
      decode_batch_step);
}

uint32_t armral_turbo_decode_batch_noalloc_buffer_size(uint32_t k) {
  counting_allocator allocator{};
  (void)armral::turbo::decode<true>(
      nullptr, nullptr, nullptr, k, nullptr, 2.F, 0, 8, nullptr, allocator,
      trellis_termination, decode_block_step, batched_trellis_termination,
      decode_batch_step);
  return allocator.required_bytes();
}
