/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "ldpc_rate_common.hpp"
#include "utils/allocators.hpp"

#include <cassert>
#include <cmath>
#include <cstring>

namespace armral::ldpc {

void soft_combine(const int8_t *in, int8_t *out) {
  int32_t llr = *out + *in;
  if (llr < (int32_t)INT8_MIN) {
    *out = INT8_MIN;
  } else if (llr > (int32_t)INT8_MAX) {
    *out = INT8_MAX;
  } else {
    *out = (int8_t)llr;
  }
}

void undo_selection(uint32_t z, uint32_t n, uint32_t e,
                    uint32_t len_filler_bits, uint32_t k, uint32_t k0,
                    const int8_t *in, int8_t *out) {
  // performs the inverse of the bit selection as specified by
  // section 5.4.2.1 in 3GPP TS 38.212

  assert(k0 < n);
  assert(e > 0);

  // systematic bits len
  uint32_t len_s_bits = k - len_filler_bits - (2 * z);
  uint32_t k_idx = 0;
  uint32_t k0_start = k0;
  uint32_t soft_buff_len = n - len_filler_bits;

  // fill the data in output systematic region
  if (k0 < len_s_bits) {
    for (; (k0 < len_s_bits) && (k_idx < e); k0++) {
      out[k0] = in[k_idx++];
    }
  }

  // set filler bits
  if (len_filler_bits > 0) {
    memset(&out[len_s_bits], INT8_MAX, len_filler_bits * sizeof(int8_t));
  }

  // offset k0 to skip filler bits
  if (k0 >= len_s_bits && k0 < len_s_bits + len_filler_bits) {
    k0 = len_s_bits + len_filler_bits;
  }
  // fill the data in output parity region
  for (; (k0 < n) && (k_idx < e); k0++) {
    out[k0] = in[k_idx++];
  }

  if (((n != e) && (k_idx != n)) || ((n == e) && (k0 != 0))) {
    while (k_idx < e) {
      // fill the data in output systematic region
      for (k0 = 0; (k0 < len_s_bits) && (k_idx < e); k0++) {
        out[k0] = in[k_idx++];
      }
      // fill the data in output parity region, filler bits skipped
      for (k0 = len_s_bits + len_filler_bits; (k0 < n) && (k_idx < e); k0++) {
        out[k0] = in[k_idx++];
      }
    }
  }

  // soft combining
  if ((e > soft_buff_len) || ((e == n) && (len_filler_bits != 0))) {
    k_idx = soft_buff_len;
    k0 = k0_start;
    // systematic region
    if (k0 < len_s_bits) {
      for (; (k0 < len_s_bits) && (k_idx < e); k0++) {
        soft_combine(&in[k_idx++], &out[k0]);
      }
    }

    // offset k0 to skip filler bits
    if (k0 >= len_s_bits && k0 < len_s_bits + len_filler_bits) {
      k0 = len_s_bits + len_filler_bits;
    }

    // parity region
    for (; (k0 < n) && (k_idx < e); k0++) {
      soft_combine(&in[k_idx++], &out[k0]);
    }
  }
}

void undo_interleave(uint32_t e, uint32_t qm, const int8_t *in, int8_t *out) {
  // performs the inverse of the bit interleaving step of LDPC encoding,
  // as specified in section 5.4.2.2 of 3GPP TS 38.212.

  assert(e > qm);
  assert(qm > 0);
  assert(e % qm == 0);

  const uint32_t n = e / qm;

  for (uint32_t j = 0; j < n; j++) {
    for (uint32_t i = 0; i < qm; i += 2) {
      out[j + i * n] = in[i + j * qm];
      out[j + i * n + n] = in[i + 1 + j * qm];
    }
  }
}

template<typename Allocator>
armral_status rate_recovery(armral_ldpc_graph_t bg, uint32_t z, uint32_t e,
                            uint32_t nref, uint32_t len_filler_bits, uint32_t k,
                            uint32_t rv, armral_modulation_type mod,
                            const int8_t *src, int8_t *dst,
                            Allocator &allocator) {
  auto llrs = allocate_zeroed<int8_t>(allocator, e);
  uint32_t n = (bg == LDPC_BASE_GRAPH_2) ? 50 * z : 66 * z;
  uint32_t ncb = 0;
  if (nref != 0) {
    ncb = (n > nref) ? nref : n;
  } else {
    ncb = n;
  }
  uint32_t k0 = starting_position(bg, rv, n, ncb, z);
  uint32_t qm = 2 + (uint32_t)mod * 2;
  undo_interleave(e, qm, src, llrs.get());
  undo_selection(z, n, e, len_filler_bits, k, k0, llrs.get(), dst);
  return ARMRAL_SUCCESS;
}

} // namespace armral::ldpc

armral_status armral_ldpc_rate_recovery(armral_ldpc_graph_t bg, uint32_t z,
                                        uint32_t e, uint32_t nref,
                                        uint32_t len_filler_bits, uint32_t k,
                                        uint32_t rv, armral_modulation_type mod,
                                        const int8_t *src, int8_t *dst) {
  heap_allocator allocator{};
  return armral::ldpc::rate_recovery(bg, z, e, nref, len_filler_bits, k, rv,
                                     mod, src, dst, allocator);
}

armral_status armral_ldpc_rate_recovery_noalloc(
    armral_ldpc_graph_t bg, uint32_t z, uint32_t e, uint32_t nref,
    uint32_t len_filler_bits, uint32_t k, uint32_t rv,
    armral_modulation_type mod, const int8_t *src, int8_t *dst, void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return armral::ldpc::rate_recovery(bg, z, e, nref, len_filler_bits, k, rv,
                                     mod, src, dst, allocator);
}
