/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "ldpc_coding.hpp"
#include "utils/allocators.hpp"
#include "utils/bits_to_bytes.hpp"

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

#include <cmath>
#include <cstring>
#include <optional>

namespace armral::ldpc {

// compute number of half words supported
inline uint32_t get_num_lanes() {
#if ARMRAL_ARCH_SVE >= 2
  return svcnth();
#else
  return 8;
#endif
}

// Check nodes process the received information, update it, and send it back to
// the connected variable nodes.
//  l is updated belief.
//  r is extrinsic information.
//  min values, signs and sign products are passed as input argument to update
//  the belief and store the extrinsic information.
void update_l_and_r(int16_t *__restrict__ l, int16_t *__restrict__ r,
                    const armral_ldpc_base_graph_t *graph, uint16_t z,
                    uint32_t lsi, uint16_t layer,
                    const int16_t *__restrict__ row_min1_array,
                    const int16_t *__restrict__ row_min2_array,
                    const int16_t *__restrict__ row_sign_array,
                    const uint16_t *__restrict__ row_pos_array,
                    const int16_t *__restrict__ sign_scratch,
                    uint32_t *__restrict__ r_index) {

  const uint32_t *col_indices;
  uint32_t i;
  uint32_t j;
  uint32_t r_i = *r_index;
  uint32_t num_lanes = get_num_lanes();

  i = graph->row_start_inds[layer];
  // Get the number of nonzero entries in the row
  j = graph->row_start_inds[layer + 1] - i;
  col_indices = graph->col_inds + i;
  const uint32_t *shift_ptr = graph->shifts + i * 8 + lsi * j;

  const int16_t *sgn_scratch_buf = sign_scratch;

#if ARMRAL_ARCH_SVE >= 2

  svbool_t pg = svptrue_b16();

  // for each column i.e only non -1's
  for (uint16_t col = 0; col < j; col++) {
    uint32_t col_block = col_indices[col];

    int16_t *ptr_r = &r[r_i * z];
    uint32_t shift = shift_ptr[col] % z;

    const int16_t *min1_buf = row_min1_array;
    const int16_t *min2_buf = row_min2_array;
    const int16_t *sgn_buf = row_sign_array;
    const uint16_t *pos_buf = row_pos_array;

    svuint16_t pos_current = svdup_n_u16(col);

    uint32_t blk1 = (z - shift) / num_lanes;
    uint32_t blk2 = shift / num_lanes;
    uint32_t tail1 = (z - shift) & (num_lanes - 1);
    uint32_t tail2 = (shift) & (num_lanes - 1);
    svbool_t pg_tail1 = svwhilelt_b16(0U, tail1);
    svbool_t pg_tail2 = svwhilelt_b16(0U, tail2);

    // Loop over z
    // shift to z-1
    int16_t *ptr_l = &l[col_block * z + shift]; // Input,point to shift3

    for (uint32_t v_cnt = 0; v_cnt < blk1; v_cnt++) {

      svint16_t min1 = svld1_s16(pg, min1_buf);
      svint16_t min2 = svld1_s16(pg, min2_buf);
      svuint16_t pos = svld1_u16(pg, pos_buf);

      // check if this the column matching position for the min1
      svbool_t pos_mask = svcmpeq_u16(pg, pos, pos_current);

      // if yes replace min1 with min2, otherwise min1
      svint16_t merged_mins = svsel_s16(pos_mask, min2, min1);

      // apply sign
      svint16_t signs = svld1_s16(pg, sgn_scratch_buf);
      merged_mins = svmul_s16_x(pg, merged_mins, signs);

      // apply sign product
      svint16_t sign_prod = svld1_s16(pg, sgn_buf);
      merged_mins = svmul_s16_x(pg, merged_mins, sign_prod);

      // update r
      svst1_s16(pg, ptr_r, merged_mins);

      // update l
      svint16_t llrs_reg = svld1_s16(pg, ptr_l);
      llrs_reg = svadd_s16_x(pg, llrs_reg, merged_mins);
      svst1_s16(pg, ptr_l, llrs_reg);

      ptr_l += num_lanes;
      ptr_r += num_lanes;
      sgn_scratch_buf += num_lanes;
      sgn_buf += num_lanes;
      min1_buf += num_lanes;
      min2_buf += num_lanes;
      pos_buf += num_lanes;
    }

    if (tail1 > 0U) {
      svint16_t min1 = svld1_s16(pg_tail1, min1_buf);
      svint16_t min2 = svld1_s16(pg_tail1, min2_buf);
      svuint16_t pos = svld1_u16(pg_tail1, pos_buf);

      // check if this the column matching position for the min1
      svbool_t pos_mask = svcmpeq_u16(pg_tail1, pos, pos_current);

      // if yes replace min1 with min2, otherwise min1
      svint16_t merged_mins = svsel_s16(pos_mask, min2, min1);

      // apply sign
      svint16_t signs = svld1_s16(pg_tail1, sgn_scratch_buf);
      merged_mins = svmul_s16_x(pg_tail1, merged_mins, signs);

      // apply sign product
      svint16_t sign_prod = svld1_s16(pg_tail1, sgn_buf);
      merged_mins = svmul_s16_x(pg_tail1, merged_mins, sign_prod);

      // update r
      svst1_s16(pg_tail1, ptr_r, merged_mins);

      // update l
      svint16_t llrs_reg = svld1_s16(pg_tail1, ptr_l);
      llrs_reg = svadd_s16_x(pg_tail1, llrs_reg, merged_mins);
      svst1_s16(pg_tail1, ptr_l, llrs_reg);

      ptr_l += tail1;
      ptr_r += tail1;
      sgn_scratch_buf += tail1;
      sgn_buf += tail1;
      min1_buf += tail1;
      min2_buf += tail1;
      pos_buf += tail1;
    }

    // 0 to shift-1
    ptr_l = &l[col_block * z]; // point to start

    for (uint32_t v_cnt = 0; v_cnt < blk2; v_cnt++) {

      svint16_t min1 = svld1_s16(pg, min1_buf);
      svint16_t min2 = svld1_s16(pg, min2_buf);
      svuint16_t pos = svld1_u16(pg, pos_buf);

      // check if this the column matching position for the min1
      svbool_t pos_mask = svcmpeq_u16(pg, pos, pos_current);

      // if yes replace min1 with min2, otherwise min1
      svint16_t merged_mins = svsel_s16(pos_mask, min2, min1);

      // apply sign
      svint16_t signs = svld1_s16(pg, sgn_scratch_buf);
      merged_mins = svmul_s16_x(pg, merged_mins, signs);

      // apply sign product
      svint16_t sign_prod = svld1_s16(pg, sgn_buf);
      merged_mins = svmul_s16_x(pg, merged_mins, sign_prod);

      // update r
      svst1_s16(pg, ptr_r, merged_mins);

      // update l
      svint16_t llrs_reg = svld1_s16(pg, ptr_l);
      llrs_reg = svadd_s16_x(pg, llrs_reg, merged_mins);
      svst1_s16(pg, ptr_l, llrs_reg);

      ptr_l += num_lanes;
      ptr_r += num_lanes;
      sgn_scratch_buf += num_lanes;
      sgn_buf += num_lanes;
      min1_buf += num_lanes;
      min2_buf += num_lanes;
      pos_buf += num_lanes;
    }

    if (tail2 > 0U) {
      svint16_t min1 = svld1_s16(pg_tail2, min1_buf);
      svint16_t min2 = svld1_s16(pg_tail2, min2_buf);
      svuint16_t pos = svld1_u16(pg_tail2, pos_buf);

      // check if this the column matching position for the min1
      svbool_t pos_mask = svcmpeq_u16(pg_tail2, pos, pos_current);

      // if yes replace min1 with min2, otherwise min1
      svint16_t merged_mins = svsel_s16(pos_mask, min2, min1);

      // apply sign
      svint16_t signs = svld1_s16(pg_tail2, sgn_scratch_buf);
      merged_mins = svmul_s16_x(pg_tail2, merged_mins, signs);

      // apply sign product
      svint16_t sign_prod = svld1_s16(pg_tail2, sgn_buf);
      merged_mins = svmul_s16_x(pg_tail2, merged_mins, sign_prod);

      // update r
      svst1_s16(pg_tail2, ptr_r, merged_mins);

      // update l
      svint16_t llrs_reg = svld1_s16(pg_tail2, ptr_l);
      llrs_reg = svadd_s16_x(pg_tail2, llrs_reg, merged_mins);
      svst1_s16(pg_tail2, ptr_l, llrs_reg);

      ptr_l += tail2;
      ptr_r += tail2;
      sgn_scratch_buf += tail2;
      sgn_buf += tail2;
      min1_buf += tail2;
      min2_buf += tail2;
      pos_buf += tail2;
    }

    r_i++;
  }

#else

  // for each column i.e only non -1's
  for (uint16_t col = 0; col < j; col++) {
    uint32_t col_block = col_indices[col];

    int16_t *ptr_r = &r[r_i * z];
    uint32_t shift = shift_ptr[col] % z;

    const int16_t *min1_buf = row_min1_array;
    const int16_t *min2_buf = row_min2_array;
    const int16_t *sgn_buf = row_sign_array; // set to 0
    const uint16_t *pos_buf = row_pos_array;

    uint16x8_t pos_current = {col, col, col, col, col, col, col, col};
    uint16x4_t pos_current_4 = {col, col, col, col};

    uint32_t blk1 = (z - shift) / num_lanes;
    uint32_t blk2 = shift / num_lanes;
    uint32_t tail1 = (z - shift) & (num_lanes - 1);
    uint32_t tail2 = (shift) & (num_lanes - 1);

    // Loop over z
    // shift to z-1
    int16_t *ptr_l = &l[col_block * z + shift]; // Input,point to shift3

    for (uint32_t v_cnt = 0; v_cnt < blk1; v_cnt++) {

      int16x8_t min1 = vld1q_s16(min1_buf);
      int16x8_t min2 = vld1q_s16(min2_buf);
      uint16x8_t pos = vld1q_u16(pos_buf);

      // check if this the column matching position for the min1
      uint16x8_t pos_mask = vceqq_u16(pos, pos_current);

      // if yes replace min1 with min2, otherwise min1
      int16x8_t merged_mins = vbslq_s16(pos_mask, min2, min1);

      // apply sign
      int16x8_t signs = vld1q_s16(sgn_scratch_buf);
      merged_mins = vmulq_s16(merged_mins, signs);

      // apply sign product
      int16x8_t sign_prod = vld1q_s16(sgn_buf);
      merged_mins = vmulq_s16(merged_mins, sign_prod);

      // update r
      vst1q_s16(ptr_r, merged_mins);

      // update l
      int16x8_t llrs_reg = vld1q_s16(ptr_l);
      llrs_reg = vqaddq_s16(llrs_reg, merged_mins);
      vst1q_s16(ptr_l, llrs_reg);

      ptr_l += num_lanes;
      ptr_r += num_lanes;
      sgn_scratch_buf += num_lanes;
      sgn_buf += num_lanes;
      min1_buf += num_lanes;
      min2_buf += num_lanes;
      pos_buf += num_lanes;
    }

    if (tail1 > 0U) {

      if (tail1 > 3U) {

        int16x4_t min1 = vld1_s16(min1_buf);
        int16x4_t min2 = vld1_s16(min2_buf);
        uint16x4_t pos = vld1_u16(pos_buf);

        // check if this the column matching position for the min1
        uint16x4_t pos_mask = vceq_u16(pos, pos_current_4);

        // if yes replace min1 with min2, otherwise min1
        int16x4_t merged_mins = vbsl_s16(pos_mask, min2, min1);

        // apply sign
        int16x4_t signs = vld1_s16(sgn_scratch_buf);
        merged_mins = vmul_s16(merged_mins, signs);

        // apply sign product
        int16x4_t sign_prod = vld1_s16(sgn_buf);
        merged_mins = vmul_s16(merged_mins, sign_prod);

        // update r
        vst1_s16(ptr_r, merged_mins);

        // update l
        int16x4_t llrs_reg = vld1_s16(ptr_l);
        llrs_reg = vqadd_s16(llrs_reg, merged_mins);
        vst1_s16(ptr_l, llrs_reg);

        ptr_l += 4;
        ptr_r += 4;
        sgn_scratch_buf += 4;
        sgn_buf += 4;
        min1_buf += 4;
        min2_buf += 4;
        pos_buf += 4;
        tail1 = (z - shift) & 0x3;
      }

      if (tail1 > 0U) {
        for (uint32_t t_cnt = 0; t_cnt < tail1; t_cnt++) {
          uint16_t pos = pos_buf[t_cnt];
          int16_t min1 = min1_buf[t_cnt];
          int16_t min2 = min2_buf[t_cnt];
          int16_t val = (pos == col) ? min2 : min1;
          val = sgn_scratch_buf[t_cnt] * val;
          ptr_r[t_cnt] = sgn_buf[t_cnt] * val;
          ptr_l[t_cnt] = ptr_l[t_cnt] + ptr_r[t_cnt];
        }

        ptr_l += tail1;
        ptr_r += tail1;
        sgn_scratch_buf += tail1;
        sgn_buf += tail1;
        min1_buf += tail1;
        min2_buf += tail1;
        pos_buf += tail1;
      }
    }

    // 0 to shift-1
    ptr_l = &l[col_block * z]; // point to start
    for (uint32_t v_cnt = 0; v_cnt < blk2; v_cnt++) {

      int16x8_t min1 = vld1q_s16(min1_buf);
      int16x8_t min2 = vld1q_s16(min2_buf);
      uint16x8_t pos = vld1q_u16(pos_buf);

      // check if this the column matching position for the min1
      uint16x8_t pos_mask = vceqq_u16(pos, pos_current);

      // if yes replace min1 with min2, otherwise min1
      int16x8_t merged_mins = vbslq_s16(pos_mask, min2, min1);

      // apply sign
      int16x8_t signs = vld1q_s16(sgn_scratch_buf);
      merged_mins = vmulq_s16(merged_mins, signs);

      // apply sign product
      int16x8_t sign_prod = vld1q_s16(sgn_buf);
      merged_mins = vmulq_s16(merged_mins, sign_prod);

      // update r
      vst1q_s16(ptr_r, merged_mins);

      // update l
      int16x8_t llrs_reg = vld1q_s16(ptr_l);
      llrs_reg = vqaddq_s16(llrs_reg, merged_mins);
      vst1q_s16(ptr_l, llrs_reg);

      ptr_l += num_lanes;
      ptr_r += num_lanes;
      sgn_scratch_buf += num_lanes;
      sgn_buf += num_lanes;
      min1_buf += num_lanes;
      min2_buf += num_lanes;
      pos_buf += num_lanes;
    }

    if (tail2 > 0U) {

      if (tail2 > 3U) {

        int16x4_t min1 = vld1_s16(min1_buf);
        int16x4_t min2 = vld1_s16(min2_buf);
        uint16x4_t pos = vld1_u16(pos_buf);

        // check if this the column matches position for the min1
        uint16x4_t pos_mask = vceq_u16(pos, pos_current_4);

        // if yes replace min1 with min2, otherwise min1
        int16x4_t merged_mins = vbsl_s16(pos_mask, min2, min1);

        // apply sign
        int16x4_t signs = vld1_s16(sgn_scratch_buf);
        merged_mins = vmul_s16(merged_mins, signs);

        // apply sign product
        int16x4_t sign_prod = vld1_s16(sgn_buf);
        merged_mins = vmul_s16(merged_mins, sign_prod);

        // update r
        vst1_s16(ptr_r, merged_mins);

        // update l
        int16x4_t llrs_reg = vld1_s16(ptr_l);
        llrs_reg = vqadd_s16(llrs_reg, merged_mins);
        vst1_s16(ptr_l, llrs_reg);

        ptr_l += 4;
        ptr_r += 4;
        sgn_scratch_buf += 4;
        sgn_buf += 4;
        min1_buf += 4;
        min2_buf += 4;
        pos_buf += 4;
        tail2 = (shift) & 0x3;
      }

      if (tail2 > 0U) {
        for (uint32_t t_cnt = 0; t_cnt < tail2; t_cnt++) {
          uint16_t pos = pos_buf[t_cnt];
          int16_t min1 = min1_buf[t_cnt];
          int16_t min2 = min2_buf[t_cnt];
          int16_t val = (pos == col) ? min2 : min1;
          val = sgn_scratch_buf[t_cnt] * val;
          ptr_r[t_cnt] = sgn_buf[t_cnt] * val;
          ptr_l[t_cnt] = ptr_l[t_cnt] + ptr_r[t_cnt];
        }

        ptr_l += tail2;
        ptr_r += tail2;
        sgn_scratch_buf += tail2;
        sgn_buf += tail2;
        min1_buf += tail2;
        min2_buf += tail2;
        pos_buf += tail2;
      }
    }

    r_i++;
  }
#endif

  // update r index for next layer
  *r_index = r_i;
}

// Variable nodes transmit their belief information to the connected check
// nodes. Decoding alogrithm implemented is scaled offset min-sum. outputs mins,
// signs and sign product to update the total belief.
void compute_l_r_and_mins(int16_t *__restrict__ l, int16_t *__restrict__ r,
                          const armral_ldpc_base_graph_t *graph, uint16_t z,
                          uint32_t lsi, uint16_t layer,
                          int16_t *__restrict__ row_min1_array,
                          int16_t *__restrict__ row_min2_array,
                          int16_t *__restrict__ row_sign_array,
                          uint16_t *__restrict__ row_pos_array,
                          int16_t *__restrict__ sign_scratch,
                          uint32_t *__restrict__ r_index) {

  const uint32_t *col_indices;
  uint32_t i;
  uint32_t j;
  uint32_t r_i = *r_index;
  uint32_t t_i = 0;
  uint32_t num_lanes = get_num_lanes();

  i = graph->row_start_inds[layer];
  // Get the number of nonzero entries in the row
  j = graph->row_start_inds[layer + 1] - i;
  col_indices = graph->col_inds + i;
  const uint32_t *shift_ptr = graph->shifts + i * 8 + lsi * j;

  int16_t *sgn_scratch_buf = sign_scratch;

#if ARMRAL_ARCH_SVE >= 2
  svbool_t pg = svptrue_b16();
  svint16_t offset = svdup_n_s16(2);

  svint16_t plus1 = svdup_n_s16(1);
  svint16_t minus1 = svdup_n_s16(-1);

  // for each column i.e only non -1's
  for (uint32_t col = 0; col < j; col++) {
    int16_t *min1_buf = row_min1_array;
    int16_t *min2_buf = row_min2_array;
    int16_t *sgn_buf = row_sign_array; // set to 0
    uint16_t *pos_buf = row_pos_array;

    uint32_t col_block = col_indices[col];

    int16_t *ptr_r = &r[r_i * z];
    uint32_t shift = shift_ptr[col] % z;

    uint32_t blk1 = (z - shift) / num_lanes;
    uint32_t blk2 = shift / num_lanes;

    uint32_t tail1 = (z - shift) & (num_lanes - 1);
    uint32_t tail2 = (shift) & (num_lanes - 1);

    svbool_t pg_tail1 = svwhilelt_b16(0U, tail1);
    svbool_t pg_tail2 = svwhilelt_b16(0U, tail2);

    // Loop over z
    // shift to z-1
    int16_t *ptr_l = &l[col_block * z + shift]; // Input,point to shift

    for (uint32_t v_cnt = 0; v_cnt < blk1; v_cnt++) {
      svint16_t llrs_reg = svld1_s16(pg, ptr_l);
      svint16_t r_reg = svld1_s16(pg, ptr_r);

      // Subtraction
      svint16_t vec = svqsub_s16(llrs_reg, r_reg);

      // Absolute
      svint16_t abs_vec = svqabs_s16_x(pg, vec);

      // Sign product
      svint16_t sgn = svld1_s16(pg, sgn_buf);
      sgn = sveor_s16_x(pg, vec, sgn);
      // store updated sign
      svst1_s16(pg, sgn_buf, sgn);

      // store signs
      svbool_t is_positive = svcmpgt_s16(pg, vec, svdup_n_s16(-1));
      svint16_t signs = svsel_s16(is_positive, plus1, minus1);

      svst1_s16(pg, sgn_scratch_buf, signs);

      // store updated L
      svst1_s16(pg, ptr_l, vec);

      // Find min1 and min2
      svint16_t min1_old = svld1_s16(pg, min1_buf);
      svint16_t min2_old = svld1_s16(pg, min2_buf);

      svint16_t min2 =
          svmax_s16_x(pg, min1_old, svmin_s16_x(pg, min2_old, abs_vec));
      svint16_t min1 = svmin_s16_x(pg, abs_vec, min1_old);

      // find min1 position
      // check if the current min1 has changed w.r.t previous
      // if it has changed, then update the index to current pos
      svbool_t pos_mask = svcmpeq_s16(pg, min1, min1_old);
      svuint16_t pos_old = svld1_u16(pg, pos_buf);
      svuint16_t pos_cur = svdup_n_u16(col);
      svuint16_t pos_updt = svsel_u16(pos_mask, pos_old, pos_cur);

      svst1_s16(pg, min2_buf, min2);
      svst1_s16(pg, min1_buf, min1);
      svst1_u16(pg, pos_buf, pos_updt);

      ptr_l += num_lanes;
      ptr_r += num_lanes;
      min1_buf += num_lanes;
      min2_buf += num_lanes;
      sgn_buf += num_lanes;
      pos_buf += num_lanes;
      sgn_scratch_buf += num_lanes;
    }

    if (tail1 != 0U) {

      svint16_t llrs_reg = svld1_s16(pg_tail1, ptr_l);
      svint16_t r_reg = svld1_s16(pg_tail1, ptr_r);

      // Subtraction
      svint16_t vec = svqsub_s16(llrs_reg, r_reg);

      // Absolute
      svint16_t abs_vec = svqabs_s16_x(pg_tail1, vec);

      // Sign product
      svint16_t sgn = svld1_s16(pg_tail1, sgn_buf);
      sgn = sveor_s16_x(pg_tail1, vec, sgn);
      // store updated sign
      svst1_s16(pg_tail1, sgn_buf, sgn);

      // store signs
      svbool_t is_positive = svcmpgt_s16(pg_tail1, vec, svdup_n_s16(-1));
      svint16_t signs = svsel_s16(is_positive, plus1, minus1);

      svst1_s16(pg_tail1, sgn_scratch_buf, signs);

      // store updated L
      svst1_s16(pg_tail1, ptr_l, vec);

      // Find min1 and min2
      svint16_t min1_old = svld1_s16(pg_tail1, min1_buf);
      svint16_t min2_old = svld1_s16(pg_tail1, min2_buf);

      svint16_t min2 = svmax_s16_x(pg_tail1, min1_old,
                                   svmin_s16_x(pg_tail1, min2_old, abs_vec));
      svint16_t min1 = svmin_s16_x(pg_tail1, abs_vec, min1_old);

      // find min1 position
      // check if the current min1 has changed w.r.t previous
      // if it has changed, then update the index to current pos
      svbool_t pos_mask = svcmpeq_s16(pg_tail1, min1, min1_old);
      svuint16_t pos_old = svld1_u16(pg_tail1, pos_buf);
      svuint16_t pos_cur = svdup_n_u16(col);
      svuint16_t pos_updt = svsel_u16(pos_mask, pos_old, pos_cur);

      svst1_s16(pg_tail1, min2_buf, min2);
      svst1_s16(pg_tail1, min1_buf, min1);
      svst1_u16(pg_tail1, pos_buf, pos_updt);

      ptr_l += tail1;
      ptr_r += tail1;
      min1_buf += tail1;
      min2_buf += tail1;
      sgn_buf += tail1;
      pos_buf += tail1;
      sgn_scratch_buf += tail1;
    }

    // 0 to shift-1

    ptr_l = &l[col_block * z]; // point to start
    for (uint32_t v_cnt = 0; v_cnt < blk2; v_cnt++) {
      svint16_t llrs_reg = svld1_s16(pg, ptr_l);
      svint16_t r_reg = svld1_s16(pg, ptr_r);

      // Subtraction
      svint16_t vec = svqsub_s16(llrs_reg, r_reg);

      // Absolute
      svint16_t abs_vec = svqabs_s16_x(pg, vec);

      // Sign product
      svint16_t sgn = svld1_s16(pg, sgn_buf);
      sgn = sveor_s16_x(pg, vec, sgn);
      // store updated sign
      svst1_s16(pg, sgn_buf, sgn);

      // store signs
      svbool_t is_positive = svcmpgt_s16(pg, vec, svdup_n_s16(-1));
      svint16_t signs = svsel_s16(is_positive, plus1, minus1);

      svst1_s16(pg, sgn_scratch_buf, signs);

      // store updated L
      svst1_s16(pg, ptr_l, vec);

      // Find min1 and min2
      svint16_t min1_old = svld1_s16(pg, min1_buf);
      svint16_t min2_old = svld1_s16(pg, min2_buf);

      svint16_t min2 =
          svmax_s16_x(pg, min1_old, svmin_s16_x(pg, min2_old, abs_vec));
      svint16_t min1 = svmin_s16_x(pg, abs_vec, min1_old);

      // find min1 position
      // check if the current min1 has changed w.r.t previous
      // if it has changed, then update the index to current pos
      svbool_t pos_mask = svcmpeq_s16(pg, min1, min1_old);
      svuint16_t pos_old = svld1_u16(pg, pos_buf);
      svuint16_t pos_cur = svdup_n_u16(col);
      svuint16_t pos_updt = svsel_u16(pos_mask, pos_old, pos_cur);

      svst1_s16(pg, min2_buf, min2);
      svst1_s16(pg, min1_buf, min1);
      svst1_u16(pg, pos_buf, pos_updt);

      ptr_l += num_lanes;
      ptr_r += num_lanes;
      min1_buf += num_lanes;
      min2_buf += num_lanes;
      sgn_buf += num_lanes;
      pos_buf += num_lanes;
      sgn_scratch_buf += num_lanes;
    }

    if (tail2 != 0U) {

      svint16_t llrs_reg = svld1_s16(pg_tail2, ptr_l);
      svint16_t r_reg = svld1_s16(pg_tail2, ptr_r);

      // Subtraction
      svint16_t vec = svqsub_s16(llrs_reg, r_reg);

      // Absolute
      svint16_t abs_vec = svqabs_s16_x(pg_tail2, vec);

      // Sign product
      svint16_t sgn = svld1_s16(pg_tail2, sgn_buf);
      sgn = sveor_s16_x(pg_tail2, vec, sgn);
      // store updated sign
      svst1_s16(pg_tail2, sgn_buf, sgn);

      // store signs
      svbool_t is_positive = svcmpgt_s16(pg_tail2, vec, svdup_n_s16(-1));
      svint16_t signs = svsel_s16(is_positive, plus1, minus1);

      svst1_s16(pg_tail2, sgn_scratch_buf, signs);

      // store updated L
      svst1_s16(pg_tail2, ptr_l, vec);

      // Find min1 and min2
      svint16_t min1_old = svld1_s16(pg_tail2, min1_buf);
      svint16_t min2_old = svld1_s16(pg_tail2, min2_buf);

      svint16_t min2 = svmax_s16_x(pg_tail2, min1_old,
                                   svmin_s16_x(pg_tail2, min2_old, abs_vec));
      svint16_t min1 = svmin_s16_x(pg_tail2, abs_vec, min1_old);

      // find min1 position
      // check if the current min1 has changed w.r.t previous
      // if it has changed, then update the index to current pos
      svbool_t pos_mask = svcmpeq_s16(pg_tail2, min1, min1_old);
      svuint16_t pos_old = svld1_u16(pg_tail2, pos_buf);
      svuint16_t pos_cur = svdup_n_u16(col);
      svuint16_t pos_updt = svsel_u16(pos_mask, pos_old, pos_cur);

      svst1_s16(pg_tail2, min2_buf, min2);
      svst1_s16(pg_tail2, min1_buf, min1);
      svst1_u16(pg_tail2, pos_buf, pos_updt);

      ptr_l += tail2;
      ptr_r += tail2;
      min1_buf += tail2;
      min2_buf += tail2;
      sgn_buf += tail2;
      pos_buf += tail2;
      sgn_scratch_buf += tail2;
    }

    r_i++;
    t_i++;
  }

  *r_index = r_i - t_i;

  // offset and scale min1 and min2
  // in the same loop, adjust sign product
  uint32_t blk = z / num_lanes;
  uint32_t tail = z & (num_lanes - 1);
  svbool_t pg_tail = svwhilelt_b16(0U, tail);

  svint16_t scale = svdup_n_s16(24576); // 0.75

  int16_t *min1_buf = row_min1_array;
  int16_t *min2_buf = row_min2_array;
  int16_t *sgn_buf = row_sign_array; // set to 0

  for (uint16_t z1 = 0; z1 < blk; z1++) {

    svint16_t sgn = svld1_s16(pg, sgn_buf);

    svbool_t is_positive = svcmpgt_s16(pg, sgn, svdup_n_s16(-1));
    sgn = svsel_s16(is_positive, plus1, minus1);
    svst1_s16(pg, sgn_buf, sgn);

    svint16_t min1 = svld1_s16(pg, min1_buf);
    svint16_t min2 = svld1_s16(pg, min2_buf);

    // apply offset
    svint16_t vec1 = svqsub_s16(min1, offset);
    svint16_t vec2 = svqsub_s16(min2, offset);

    // if min1 < 0, then min1 = 0;
    is_positive = svcmpgt_s16(pg, vec1, svdup_n_s16(-1));
    vec1 = svsel_s16(is_positive, vec1, svdup_n_s16(0));

    // apply scale
    svint32_t res = svmullt_s32(vec1, scale);
    svint16_t mint = svqrshrnt_n_s32(svdup_n_s16(0), res, 15);
    res = svmullb_s32(vec1, scale);
    svint16_t minb = svqrshrnt_n_s32(svdup_n_s16(0), res, 15);
    min1 = svtrn2_s16(minb, mint);

    // if min2 < 0, then min1 = 0;
    is_positive = svcmpgt_s16(pg, vec2, svdup_n_s16(-1));
    vec2 = svsel_s16(is_positive, vec2, svdup_n_s16(0));

    // apply scale
    res = svmullt_s32(vec2, scale);
    mint = svqrshrnt_n_s32(svdup_n_s16(0), res, 15);
    res = svmullb_s32(vec2, scale);
    minb = svqrshrnt_n_s32(svdup_n_s16(0), res, 15);
    min2 = svtrn2_s16(minb, mint);

    // store scaled, offseted min's
    svst1_s16(pg, min1_buf, min1);
    svst1_s16(pg, min2_buf, min2);

    min1_buf += num_lanes;
    min2_buf += num_lanes;
    sgn_buf += num_lanes;
  }

  if (tail > 0U) {

    svint16_t sgn = svld1_s16(pg, sgn_buf);

    svbool_t is_positive = svcmpgt_s16(pg_tail, sgn, svdup_n_s16(-1));
    sgn = svsel_s16(is_positive, plus1, minus1);
    svst1_s16(pg_tail, sgn_buf, sgn);

    svint16_t min1 = svld1_s16(pg_tail, min1_buf);
    svint16_t min2 = svld1_s16(pg_tail, min2_buf);

    // apply offset
    svint16_t vec1 = svqsub_s16(min1, offset);
    svint16_t vec2 = svqsub_s16(min2, offset);

    // if min1 < 0, then min1 = 0;
    is_positive = svcmpgt_s16(pg_tail, vec1, svdup_n_s16(-1));
    vec1 = svsel_s16(is_positive, vec1, svdup_n_s16(0));

    // apply scale
    svint32_t res = svmullt_s32(vec1, scale);
    svint16_t mint = svqrshrnt_n_s32(svdup_n_s16(0), res, 15);
    res = svmullb_s32(vec1, scale);
    svint16_t minb = svqrshrnt_n_s32(svdup_n_s16(0), res, 15);
    min1 = svtrn2_s16(minb, mint);

    // if min2 < 0, then min1 = 0;
    is_positive = svcmpgt_s16(pg_tail, vec2, svdup_n_s16(-1));
    vec2 = svsel_s16(is_positive, vec2, svdup_n_s16(0));

    // apply scale
    res = svmullt_s32(vec2, scale);
    mint = svqrshrnt_n_s32(svdup_n_s16(0), res, 15);
    res = svmullb_s32(vec2, scale);
    minb = svqrshrnt_n_s32(svdup_n_s16(0), res, 15);
    min2 = svtrn2_s16(minb, mint);

    // store scaled, offseted min's
    svst1_s16(pg_tail, min1_buf, min1);
    svst1_s16(pg_tail, min2_buf, min2);

    min1_buf += tail;
    min2_buf += tail;
    sgn_buf += tail;
  }

#else
  int16x8_t offset8 = vdupq_n_s16(2);
  int16x4_t offset4 = vdup_n_s16(2);

  int16x8_t plus1 = vdupq_n_s16(1);
  int16x8_t minus1 = vdupq_n_s16(-1);

  int16x4_t plus1_4 = vdup_n_s16(1);
  int16x4_t minus1_4 = vdup_n_s16(-1);

  int16_t offset = 2;

  // for each column i.e only non -1's
  for (uint32_t col = 0; col < j; col++) {
    uint32_t col_block = col_indices[col];

    int16_t *ptr_r = &r[r_i * z];
    uint32_t shift = shift_ptr[col] % z;

    uint32_t blk1 = (z - shift) / num_lanes;
    uint32_t blk2 = shift / num_lanes;
    uint32_t tail1 = (z - shift) & (num_lanes - 1);
    uint32_t tail2 = (shift) & (num_lanes - 1);

    int16_t *min1_buf = row_min1_array;
    int16_t *min2_buf = row_min2_array;
    int16_t *sgn_buf = row_sign_array; // set to 0
    uint16_t *pos_buf = row_pos_array;

    // Loop over z
    // shift to z-1
    int16_t *ptr_l = &l[col_block * z + shift]; // Input,point to shift

    for (uint32_t v_cnt = 0; v_cnt < blk1; v_cnt++) {
      int16x8_t llrs_reg = vld1q_s16(ptr_l);
      int16x8_t r_reg = vld1q_s16(ptr_r);

      // Subtraction
      int16x8_t vec16 = vqsubq_s16(llrs_reg, r_reg);

      // Absoluate
      int16x8_t abs_vec16 = vqabsq_s16(vec16);

      // Store signs
      uint16x8_t is_positive = vcgtq_s16(vec16, vdupq_n_s16(-1));
      int16x8_t signs = vbslq_s16(is_positive, plus1, minus1);
      vst1q_s16(sgn_scratch_buf, signs);

      // Sign product
      int16x8_t old_sgn = vld1q_s16(sgn_buf);
      int16x8_t sgn = vmulq_s16(signs, old_sgn);
      // store updated sign
      vst1q_s16(sgn_buf, sgn);

      // store updated L
      vst1q_s16(ptr_l, vec16);

      // Find min1 and min2
      int16x8_t min1_old = vld1q_s16(min1_buf);
      int16x8_t min2_old = vld1q_s16(min2_buf);

      int16x8_t min2 = vmaxq_s16(min1_old, vminq_s16(min2_old, abs_vec16));
      int16x8_t min1 = vminq_s16(abs_vec16, min1_old);

      // find min1 position
      // check if the current min1 has changed w.r.t previous
      // if it has changed, then update the index to current pos
      uint16x8_t pos_mask = vceqq_s16(min1, min1_old);
      uint16x8_t pos_old = vld1q_u16(pos_buf);
      uint16x8_t pos_cur = vdupq_n_u16(col);
      uint16x8_t pos_updt = vbslq_u16(pos_mask, pos_old, pos_cur);

      vst1q_s16(min2_buf, min2);
      vst1q_s16(min1_buf, min1);
      vst1q_u16(pos_buf, pos_updt);

      ptr_l += num_lanes;
      ptr_r += num_lanes;
      min1_buf += num_lanes;
      min2_buf += num_lanes;
      sgn_buf += num_lanes;
      pos_buf += num_lanes;
      sgn_scratch_buf += num_lanes;
    }

    if (tail1 > 0U) {

      if (tail1 > 3U) {

        int16x4_t llrs_reg = vld1_s16(ptr_l);
        int16x4_t r_reg = vld1_s16(ptr_r);

        // Subtraction
        int16x4_t vec16 = vqsub_s16(llrs_reg, r_reg);

        // Absolute
        int16x4_t abs_vec16 = vqabs_s16(vec16);

        // Store signs
        uint16x4_t is_positive = vcgt_s16(vec16, vdup_n_s16(-1));
        int16x4_t signs = vbsl_s16(is_positive, plus1_4, minus1_4);
        vst1_s16(sgn_scratch_buf, signs);

        // Sign product
        int16x4_t old_sgn = vld1_s16(sgn_buf);
        int16x4_t sgn = vmul_s16(signs, old_sgn);
        // store updated sign
        vst1_s16(sgn_buf, sgn);

        // store updated L
        vst1_s16(ptr_l, vec16);

        // Find min1 and min2
        int16x4_t min1_old = vld1_s16(min1_buf);
        int16x4_t min2 = vld1_s16(min2_buf);

        min2 = vmax_s16(min1_old, vmin_s16(min2, abs_vec16));
        int16x4_t min1 = vmin_s16(abs_vec16, min1_old);

        // Find min1 pos
        //  check if the current min1 has changed w.r.t previous
        //  if it has changed, then update the index to current pos
        uint16x4_t pos_mask = vceq_s16(min1, min1_old);
        uint16x4_t pos_old = vld1_u16(pos_buf);
        uint16x4_t pos_cur = vdup_n_u16(col);
        uint16x4_t pos_updt = vbsl_u16(pos_mask, pos_old, pos_cur);

        vst1_s16(min2_buf, min2);
        vst1_s16(min1_buf, min1);
        vst1_u16(pos_buf, pos_updt);

        ptr_l += 4;
        ptr_r += 4;
        min1_buf += 4;
        min2_buf += 4;
        sgn_buf += 4;
        pos_buf += 4;
        sgn_scratch_buf += 4;
        tail1 = (z - shift) & 0x3;
      }

      if (tail1 > 0U) {

        int16_t val;
        int8_t sign;
        for (uint32_t t_cnt = 0; t_cnt < tail1; t_cnt++) {

          val = vqsubh_s16(ptr_l[t_cnt], ptr_r[t_cnt]);
          ptr_l[t_cnt] = val;

          sign = (int8_t)(val >= 0);

          sgn_scratch_buf[t_cnt] = vqsubh_s16(2 * sign, 1);
          sgn_buf[t_cnt] = sgn_scratch_buf[t_cnt] * sgn_buf[t_cnt];

          val = vqabsh_s16(val);

          min2_buf[t_cnt] =
              std::max(min1_buf[t_cnt], std::min(min2_buf[t_cnt], val));

          if (min1_buf[t_cnt] > val) {
            pos_buf[t_cnt] = col;
            min1_buf[t_cnt] = val;
          }
        }

        ptr_r += tail1;
        min1_buf += tail1;
        min2_buf += tail1;
        sgn_buf += tail1;
        pos_buf += tail1;
        sgn_scratch_buf += tail1;
      }
    }

    // 0 to shift-1
    ptr_l = &l[col_block * z]; // point to start
    for (uint32_t v_cnt = 0; v_cnt < blk2; v_cnt++) {
      int16x8_t llrs_reg = vld1q_s16(ptr_l);
      int16x8_t r_reg = vld1q_s16(ptr_r);

      // Subtraction
      int16x8_t vec16 = vqsubq_s16(llrs_reg, r_reg);

      // Absoluate
      int16x8_t abs_vec16 = vqabsq_s16(vec16);

      // Store signs
      uint16x8_t is_positive = vcgtq_s16(vec16, vdupq_n_s16(-1));
      int16x8_t signs = vbslq_s16(is_positive, plus1, minus1);
      vst1q_s16(sgn_scratch_buf, signs);

      // Sign product
      int16x8_t old_sgn = vld1q_s16(sgn_buf);
      int16x8_t sgn = vmulq_s16(signs, old_sgn);
      // store updated sign
      vst1q_s16(sgn_buf, sgn);

      // store updated L
      vst1q_s16(ptr_l, vec16);

      // Find min1 and min2
      int16x8_t min1_old = vld1q_s16(min1_buf);
      int16x8_t min2 = vld1q_s16(min2_buf);

      min2 = vmaxq_s16(min1_old, vminq_s16(min2, abs_vec16));
      int16x8_t min1 = vminq_s16(abs_vec16, min1_old);

      // find min1 position
      // check if the current min1 has changed w.r.t previous
      // if it has changed, then update the index to current pos
      uint16x8_t pos_mask = vceqq_s16(min1, min1_old);
      uint16x8_t pos_old = vld1q_u16(pos_buf);
      uint16x8_t pos_cur = vdupq_n_u16(col);
      uint16x8_t pos_updt = vbslq_u16(pos_mask, pos_old, pos_cur);

      vst1q_s16(min2_buf, min2);
      vst1q_s16(min1_buf, min1);
      vst1q_u16(pos_buf, pos_updt);

      ptr_l += num_lanes;
      ptr_r += num_lanes;
      min1_buf += num_lanes;
      min2_buf += num_lanes;
      sgn_buf += num_lanes;
      pos_buf += num_lanes;
      sgn_scratch_buf += num_lanes;
    }

    if (tail2 > 0U) {

      if (tail2 > 3U) {

        int16x4_t llrs_reg = vld1_s16(ptr_l);
        int16x4_t r_reg = vld1_s16(ptr_r);

        // Subtraction
        int16x4_t vec16 = vqsub_s16(llrs_reg, r_reg);

        // Absolute
        int16x4_t abs_vec16 = vqabs_s16(vec16);

        // Store signs
        uint16x4_t is_positive = vcgt_s16(vec16, vdup_n_s16(-1));
        int16x4_t signs = vbsl_s16(is_positive, plus1_4, minus1_4);
        vst1_s16(sgn_scratch_buf, signs);

        // Sign product
        int16x4_t old_sgn = vld1_s16(sgn_buf);
        int16x4_t sgn = vmul_s16(signs, old_sgn);
        // store updated sign
        vst1_s16(sgn_buf, sgn);

        // store updated L
        vst1_s16(ptr_l, vec16);

        // Find min1 and min2
        int16x4_t min1_old = vld1_s16(min1_buf);
        int16x4_t min2 = vld1_s16(min2_buf);

        min2 = vmax_s16(min1_old, vmin_s16(min2, abs_vec16));
        int16x4_t min1 = vmin_s16(abs_vec16, min1_old);

        // Find min1 pos
        //  check if the current min1 has changed w.r.t previous
        //  if it has changed, then update the index to current pos
        uint16x4_t pos_mask = vceq_s16(min1, min1_old);
        uint16x4_t pos_old = vld1_u16(pos_buf);
        uint16x4_t pos_cur = vdup_n_u16(col);
        uint16x4_t pos_updt = vbsl_u16(pos_mask, pos_old, pos_cur);

        vst1_s16(min2_buf, min2);
        vst1_s16(min1_buf, min1);
        vst1_u16(pos_buf, pos_updt);

        ptr_l += 4;
        ptr_r += 4;
        min1_buf += 4;
        min2_buf += 4;
        sgn_buf += 4;
        pos_buf += 4;
        sgn_scratch_buf += 4;
        tail2 = (shift) & 0x3;
      }

      if (tail2 > 0U) {
        int16_t val;
        int8_t sign;
        for (uint32_t t_cnt = 0; t_cnt < tail2; t_cnt++) {

          val = vqsubh_s16(ptr_l[t_cnt], ptr_r[t_cnt]);
          ptr_l[t_cnt] = val;

          sign = (int8_t)(val >= 0);

          sgn_scratch_buf[t_cnt] = vqsubh_s16(2 * sign, 1);
          sgn_buf[t_cnt] = sgn_scratch_buf[t_cnt] * sgn_buf[t_cnt];

          val = vqabsh_s16(val);

          min2_buf[t_cnt] =
              std::max(min1_buf[t_cnt], std::min(min2_buf[t_cnt], val));

          if (min1_buf[t_cnt] > val) {
            pos_buf[t_cnt] = col;
            min1_buf[t_cnt] = val;
          }
        }

        ptr_r += tail2;
        min1_buf += tail2;
        min2_buf += tail2;
        sgn_buf += tail2;
        pos_buf += tail2;
        sgn_scratch_buf += tail2;
      }
    }

    r_i++;
    t_i++;
  }

  *r_index = r_i - t_i;

  // offset and scale min1 and min2
  // in the same loop, adjust sign product
  uint32_t blk = z / num_lanes;
  uint32_t tail = z & (num_lanes - 1);

  int16x4_t scale = vdup_n_s16(24576); // 0.75

  int16_t *min1_buf = row_min1_array;
  int16_t *min2_buf = row_min2_array;

  for (uint16_t z1 = 0; z1 < blk; z1++) {

    int16x8_t min1 = vld1q_s16(min1_buf);
    int16x8_t min2 = vld1q_s16(min2_buf);

    // apply offset
    int16x8_t vec1 = vqsubq_s16(min1, offset8);
    int16x8_t vec2 = vqsubq_s16(min2, offset8);

    // if min1 < 0, then min1 = 0;
    uint16x8_t is_positive = vcgtq_s16(vec1, vdupq_n_s16(-1));
    vec1 = vbslq_s16(is_positive, vec1, vdupq_n_s16(0));

    // apply scale
    int32x4_t mul_low = vmull_s16(vget_low_s16(vec1), scale);
    int32x4_t mul_high = vmull_s16(vget_high_s16(vec1), scale);
    int32x4_t shifted_low = vrshrq_n_s32(mul_low, 15);
    int32x4_t shifted_high = vrshrq_n_s32(mul_high, 15);
    min1 = vcombine_s16(vqmovn_s32(shifted_low), vqmovn_s32(shifted_high));

    // if min2 < 0, then min1 = 0;
    is_positive = vcgtq_s16(vec2, vdupq_n_s16(-1));
    vec2 = vbslq_s16(is_positive, vec2, vdupq_n_s16(0));

    // apply scale
    mul_low = vmull_s16(vget_low_s16(vec2), scale);
    mul_high = vmull_s16(vget_high_s16(vec2), scale);
    shifted_low = vrshrq_n_s32(mul_low, 15);
    shifted_high = vrshrq_n_s32(mul_high, 15);
    min2 = vcombine_s16(vqmovn_s32(shifted_low), vqmovn_s32(shifted_high));

    // store scaled, offseted min's
    vst1q_s16(min1_buf, min1);
    vst1q_s16(min2_buf, min2);

    min1_buf += num_lanes;
    min2_buf += num_lanes;
  }

  if (tail > 0U) {

    if (tail > 3U) {

      int16x4_t min1 = vld1_s16(min1_buf);
      int16x4_t min2 = vld1_s16(min2_buf);

      int16x4_t vec1 = vqsub_s16(min1, offset4);
      int16x4_t vec2 = vqsub_s16(min2, offset4);

      uint16x4_t is_positive = vcgt_s16(vec1, vdup_n_s16(-1));
      vec1 = vbsl_s16(is_positive, vec1, vdup_n_s16(0));

      int32x4_t res = vmull_s16(vec1, scale);
      res = vrshrq_n_s32(res, 15);
      min1 = vqmovn_s32(res);

      is_positive = vcgt_s16(vec2, vdup_n_s16(-1));
      vec2 = vbsl_s16(is_positive, vec2, vdup_n_s16(0));

      res = vmull_s16(vec2, scale);
      res = vrshrq_n_s32(res, 15);
      min2 = vqmovn_s32(res);

      vst1_s16(min1_buf, min1);
      vst1_s16(min2_buf, min2);
      min1_buf += 4;
      min2_buf += 4;
      tail = (tail) & 0x3;
    }

    if (tail > 0U) {
      for (uint32_t t_cnt = 0; t_cnt < tail; t_cnt++) {

        int16_t min1 = min1_buf[t_cnt];
        int16_t min2 = min2_buf[t_cnt];

        min1 -= offset;
        min2 -= offset;

        if (min1 < 0) {
          min1 = 0;
        }
        if (min2 < 0) {
          min2 = 0;
        }
        int32_t t1 = (min1 * 24576) >> 15;
        int32_t t2 = (min2 * 24576) >> 15;
        min1_buf[t_cnt] = (int16_t)t1;
        min2_buf[t_cnt] = (int16_t)t2;
      }
    }
  }

#endif
}

bool hard_decision(int16_t *ptr_l, uint8_t *crc_buff, uint8_t *ptr_data,
                   uint32_t k, uint32_t crc_flag) {

  uint32_t num_lanes = get_num_lanes();
  uint32_t k_prime = k + 24;
  uint32_t full_vec = (k_prime) / num_lanes;
  uint32_t tail_cnt = (k_prime) & (num_lanes - 1);
  uint8_t *data = (uint8_t *)crc_buff;
  uint32_t pad_bytes = 0;

  // if the decoded data is less than 8 bytes / not multiple of 8 bytes, prefix
  // zero padding
  if (crc_flag != 0U) {
    if (((k_prime >> 3) % 16) != 0U) {
      pad_bytes = 16 - ((k_prime >> 3) % 16);
      memset(data, 0, pad_bytes);
      data = data + pad_bytes;
    }
  }

#if ARMRAL_ARCH_SVE >= 2
  svuint16_t ones = svdup_n_u16(1);
  svuint16_t zeros = svdup_n_u16(0);

  svbool_t pg = svptrue_b16();
  svbool_t pg_tail = svwhilelt_b16(0U, tail_cnt);
  svuint16_t shifts = svindex_u16(num_lanes - 1, -1);

  if (num_lanes == 8) {
    for (uint32_t v_cnt = 0; v_cnt < full_vec; v_cnt++) {

      svint16_t d = svld1_s16(pg, ptr_l);
      svbool_t is_negative = svcmpgt_s16(pg, d, svdup_n_s16(0));

      svuint16_t bits = svsel_u16(is_negative, zeros, ones);

      svuint16_t byte = svlsl_u16_m(pg, bits, shifts);
      *data++ = (uint8_t)svaddv_u16(pg, byte);
      ptr_l += num_lanes;
    }

    if (tail_cnt != 0U) {
      svint16_t d = svld1_s16(pg_tail, ptr_l);
      svbool_t is_negative = svcmpgt_s16(pg_tail, d, svdup_n_s16(0));

      svuint16_t bits = svsel_u16(is_negative, zeros, ones);

      svuint16_t byte = svlsl_u16_m(pg_tail, bits, shifts);
      *data++ = (uint8_t)svaddv_u16(pg_tail, byte);
      ptr_l += tail_cnt;
    }
  } else if (num_lanes == 16) {
    uint16_t *data_ptr = (uint16_t *)data;

    for (uint32_t v_cnt = 0; v_cnt < full_vec; v_cnt++) {

      svint16_t d = svld1_s16(pg, ptr_l);
      svbool_t is_negative = svcmpgt_s16(pg, d, svdup_n_s16(0));

      svuint16_t bits = svsel_u16(is_negative, zeros, ones);
      svuint16_t word = svlsl_u16_m(pg, bits, shifts);
      word = svrevb_u16_x(pg, word);

      *data_ptr++ = svaddv_u16(pg, word);
      ptr_l += num_lanes;
    }

    if (tail_cnt != 0U) {
      svint16_t d = svld1_s16(pg_tail, ptr_l);
      svbool_t is_negative = svcmpgt_s16(pg_tail, d, svdup_n_s16(0));

      svuint16_t bits = svsel_u16(is_negative, zeros, ones);
      svuint16_t word = svlsl_u16_m(pg_tail, bits, shifts);
      word = svrevb_u16_x(pg_tail, word);

      *data_ptr++ = svaddv_u16(pg_tail, word);
    }
  }

#else
  int8x8_t shifts = {7, 6, 5, 4, 3, 2, 1, 0};
  for (uint32_t v_cnt = 0; v_cnt < full_vec; v_cnt++) {

    int16x8_t d = vld1q_s16(ptr_l);

    uint16x4_t is_negative_high = vclt_s16(vget_high_s16(d), vdup_n_s16(0));
    is_negative_high = vand_u16(is_negative_high, vdup_n_u16(1));

    uint16x4_t is_negative_low = vclt_s16(vget_low_s16(d), vdup_n_s16(0));
    is_negative_low = vand_u16(is_negative_low, vdup_n_u16(1));

    uint16x8_t vec16 = vcombine_u16(is_negative_low, is_negative_high);
    uint8x8_t byte = vqmovn_u16(vec16);

    byte = vshl_u8(byte, shifts);
    uint8_t byte1 = vaddv_u8(byte);

    *data++ = byte1;
    ptr_l += num_lanes;
  }

  if (tail_cnt != 0U) {
    uint8_t tail_byte = 0;
    uint8_t i = 0;
    while (i < tail_cnt) {
      tail_byte |= ((uint8_t)(*ptr_l++ < 0)) << (7 - i);
      i++;
    }

    *data = tail_byte;
  }
#endif

  // Generate the CRC parity bits
  uint64_t crc = 0;
  if (crc_flag != 0U) {
    armral_crc24_b_be((k_prime >> 3) + pad_bytes, (const uint64_t *)crc_buff,
                      &crc);
    // Removing the Zero padding
    if (pad_bytes != 0U) {
      for (uint32_t i = 0; i < (k_prime >> 3); i++) {
        ptr_data[i] = crc_buff[i + pad_bytes];
      }
    }
  } else {
    memcpy(ptr_data, crc_buff, (k + 7) >> 3);
  }

  return (crc == 0U);
}

inline void load_ptr_l(int16_t *ptr_l, const int8_t *llrs_ptr,
                       uint32_t len_in) {
#if ARMRAL_ARCH_SVE >= 2
  svint8_t vec8;
  svbool_t pg = svptrue_b8();

  uint32_t num_lanes = get_num_lanes();
  uint32_t full_blk = len_in / (2 * num_lanes);
  uint32_t tail_cnt = len_in % (2 * num_lanes);

  for (uint32_t num_block = 0; num_block < full_blk; num_block++) {
    vec8 = svld1_s8(pg, llrs_ptr);

    svint16_t t1 = svmovlb_s16(vec8);
    svint16_t t2 = svmovlt_s16(vec8);

    svint16_t result1 = svzip1_s16(t1, t2);
    svint16_t result2 = svzip2_s16(t1, t2);

    svst1_s16(pg, ptr_l, result1);
    ptr_l += num_lanes;
    svst1_s16(pg, ptr_l, result2);
    ptr_l += num_lanes;
    llrs_ptr += 2 * num_lanes;
  }

  if (tail_cnt != 0U) {
    for (uint32_t i = 0; i < tail_cnt; i++) {
      ptr_l[i] = (int16_t)llrs_ptr[i];
    }
  }

#else

  uint32_t full_blk = len_in / 16;
  uint32_t tail_cnt = len_in % 16;

  for (uint32_t num_block = 0; num_block < full_blk; num_block++) {
    int8x16_t vec = vld1q_s8(llrs_ptr);
    int8x8_t vec_h = vget_high_s8(vec);
    int16x8_t vec_h_16 = vmovl_s8(vec_h);
    int8x8_t vec_l = vget_low_s8(vec);
    int16x8_t vec_l_16 = vmovl_s8(vec_l);
    vst1q_s16(ptr_l, vec_l_16);
    ptr_l += 8;
    vst1q_s16(ptr_l, vec_h_16);
    llrs_ptr += 16;
    ptr_l += 8;
  }

  if (tail_cnt != 0U) {
    for (uint32_t i = 0; i < tail_cnt; i++) {
      ptr_l[i] = (int16_t)llrs_ptr[i];
    }
  }

#endif
}

template<typename Allocator>
bool decode_block(const int8_t *llrs, armral_ldpc_graph_t bg, uint32_t z,
                  uint32_t crc_idx, uint32_t num_its, uint8_t *data_out,
                  Allocator &allocator) {

  bool crc_passed = false;

  // Get the base graph and the lifting size
  const auto *graph = armral_ldpc_get_base_graph(bg);
  uint32_t lsi = get_lifting_index(z);

  // (graph->row_start_inds[2] - graph->row_start_inds[1]) Max no of non -1 in
  // columns is 19. Note: Min is calculated over 8 byte block lengths, so need
  // lesser memory
  uint32_t layer_size = 19 * z;

  uint32_t num_llrs = (graph->ncodeword_bits + 2) * z;
  // max no of non zeros enteries in H Matrix is 316
  uint32_t r_mat_size = 316 * z;

  auto l = allocate_uninitialized<int16_t>(allocator, num_llrs);
  // matrix R (check-to-variable-node messages)
  auto r = allocate_zeroed<int16_t>(allocator, r_mat_size);

  uint32_t num_lanes = get_num_lanes();
  uint32_t z_len = z / num_lanes;
  uint32_t offset = (z % num_lanes) ? 1 : 0;
  z_len = (z_len + offset) * num_lanes;
  uint32_t k = (bg == 0) ? (z * 22) : (z * 10);

  auto row_min1_array = allocate_uninitialized<int16_t>(allocator, z_len);
  auto row_min2_array = allocate_uninitialized<int16_t>(allocator, z_len);
  auto row_sign_array = allocate_zeroed<int16_t>(allocator, z_len);
  auto row_pos_array = allocate_zeroed<uint16_t>(allocator, z);
  auto sign_scratch = allocate_uninitialized<int16_t>(allocator, layer_size);
  auto crc_buff = allocate_zeroed<uint8_t>(allocator, ((k + 7) >> 3) + 15);

  // NOTE: All allocations are now done!
  if constexpr (Allocator::is_counting) {
    return false;
  }

  uint32_t r_index = 0;

  // initialization with channel LLRs. 16-bit buffer "l" will be used for
  // in-place calculations
  int16_t *ptr_l = l.get();
  const auto *llrs_ptr = llrs;

  // 0 memset 2z LLRs from input to fill the punctured bits
  memset(ptr_l, 0, sizeof(int16_t) * 2 * z);
  ptr_l = ptr_l + 2 * z;

  load_ptr_l(ptr_l, llrs_ptr, graph->ncodeword_bits * z);

  uint32_t full_blk = z_len / num_lanes;

  for (uint32_t it = 0; it < num_its; ++it) {
    r_index = 0;
    for (uint32_t layer = 0; layer < graph->nrows; layer++) {

      // reset the sign buffer
      memset(row_sign_array.get(), 0, sizeof(int16_t) * z);

      // reset the min1 min2 buf to max
      int16_t *ptr1 = row_min1_array.get();
      int16_t *ptr2 = row_min2_array.get();
      int16_t *ptr3 = row_sign_array.get();

      for (uint32_t i = 0; i < full_blk; i++) {
#if ARMRAL_ARCH_SVE >= 2
        svbool_t pg = svptrue_b16();
        svint16_t vec = svdup_n_s16(0x7FFF);
        svint16_t vec_sign = svdup_n_s16(0x1);
        svst1_s16(pg, ptr1, vec);
        svst1_s16(pg, ptr2, vec);
        svst1_s16(pg, ptr3, vec_sign);
#else
        int16x8_t v8 = vdupq_n_s16(0x7FFF);
        int16x8_t v_sign8 = vdupq_n_s16(0x1);
        vst1q_s16(ptr1, v8);
        vst1q_s16(ptr2, v8);
        vst1q_s16(ptr3, v_sign8);
#endif
        ptr1 += num_lanes;
        ptr2 += num_lanes;
        ptr3 += num_lanes;
      }

      compute_l_r_and_mins(l.get(), r.get(), graph, z, lsi, layer,
                           row_min1_array.get(), row_min2_array.get(),
                           row_sign_array.get(), row_pos_array.get(),
                           sign_scratch.get(), &r_index);

      update_l_and_r(l.get(), r.get(), graph, z, lsi, layer,
                     row_min1_array.get(), row_min2_array.get(),
                     row_sign_array.get(), row_pos_array.get(),
                     sign_scratch.get(), &r_index);
    }

    // early exit if crc Passes
    if (crc_idx) {
      if (it < (num_its - 1)) {
        crc_passed =
            hard_decision(l.get(), crc_buff.get(), &data_out[0], crc_idx, true);
        if (crc_passed) {
          return crc_passed;
        }
      }
    }
  }

  if (crc_idx == ARMRAL_LDPC_NO_CRC) { // do only decisions
    crc_passed = hard_decision(l.get(), crc_buff.get(), &data_out[0],
                               graph->nmessage_bits * z, false);
  } else {
    crc_passed =
        hard_decision(l.get(), crc_buff.get(), &data_out[0], crc_idx, true);
  }
  return crc_passed;
}

} // namespace armral::ldpc

template bool armral::ldpc::decode_block<heap_allocator>(
    const int8_t *llrs, armral_ldpc_graph_t bg, uint32_t z, uint32_t crc_idx,
    uint32_t num_its, uint8_t *data_out, heap_allocator &);

template bool armral::ldpc::decode_block<buffer_bump_allocator>(
    const int8_t *llrs, armral_ldpc_graph_t bg, uint32_t z, uint32_t crc_idx,
    uint32_t num_its, uint8_t *data_out, buffer_bump_allocator &);

armral_status armral_ldpc_decode_block(const int8_t *llrs,
                                       armral_ldpc_graph_t bg, uint32_t z,
                                       uint32_t crc_idx, uint32_t num_its,
                                       uint8_t *data_out) {

  heap_allocator allocator{};
  bool result = armral::ldpc::decode_block(llrs, bg, z, crc_idx, num_its,
                                           data_out, allocator);
  return (result) ? ARMRAL_SUCCESS : ARMRAL_RESULT_FAIL;
}

armral_status
armral_ldpc_decode_block_noalloc(const int8_t *llrs, armral_ldpc_graph_t bg,
                                 uint32_t z, uint32_t crc_idx, uint32_t num_its,
                                 uint8_t *data_out, void *buffer) {

  buffer_bump_allocator allocator{buffer};
  bool result = armral::ldpc::decode_block(llrs, bg, z, crc_idx, num_its,
                                           data_out, allocator);
  return (result) ? ARMRAL_SUCCESS : ARMRAL_RESULT_FAIL;
}

uint32_t armral_ldpc_decode_block_noalloc_buffer_size(armral_ldpc_graph_t bg,
                                                      uint32_t z,
                                                      uint32_t crc_idx,
                                                      uint32_t num_its) {
  counting_allocator allocator{};
  armral::ldpc::decode_block(nullptr, bg, z, crc_idx, num_its, nullptr,
                             allocator);
  return allocator.required_bytes();
}
