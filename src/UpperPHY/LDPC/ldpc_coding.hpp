/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"

namespace armral::ldpc {

constexpr uint32_t num_lifting_sets = 8;

uint32_t get_lifting_index(uint32_t lifting_size);

template<typename Allocator>
bool decode_block(const int8_t *llrs, armral_ldpc_graph_t bg, uint32_t z,
                  uint32_t crc_idx, uint32_t num_its, uint8_t *data_out,
                  Allocator &allocator);

} // namespace armral::ldpc
