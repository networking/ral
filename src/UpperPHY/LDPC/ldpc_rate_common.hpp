/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"

namespace {

uint32_t starting_position(armral_ldpc_graph_t bg, uint32_t rv, uint32_t n,
                           uint32_t ncb, uint32_t z) {
  // Starting position k0 of different redundancy versions
  // given as Table 5.4.2.1-2 in 3GPP TS 38.212.
  if (rv == 0) {
    return 0;
  }
  if (rv == 1) {
    return (17 * z - (int)bg * 4 * z) * (ncb / n);
  }
  if (rv == 2) {
    return (33 * z - (int)bg * 8 * z) * (ncb / n);
  }
  if (rv == 3) {
    return (56 * z - (int)bg * 13 * z) * (ncb / n);
  }
  return 0;
}

} // anonymous namespace
