/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "ldpc_rate_common.hpp"
#include "utils/allocators.hpp"
#include "utils/bits_to_bytes.hpp"
#include <cassert>
#include <cmath>
#include <cstring>

namespace armral::ldpc {

void copy_bits(uint32_t src_bit, uint32_t start_idx, uint32_t len, uint32_t l,
               const uint8_t *in_bits, uint8_t *out) {
  for (uint32_t i = start_idx; i < len; i++) {
    uint32_t bit = (in_bits[src_bit / 8] >> (7 - src_bit % 8)) & 1;
    out[i / 8] |= (bit << (7 - (i % 8)));
    src_bit++;
    src_bit = src_bit % l;
  }
}

void bit_selection(uint32_t z, uint32_t n, uint32_t e, uint32_t len_filler_bits,
                   uint32_t k, uint32_t k0, const uint8_t *in, uint8_t *out,
                   uint8_t *scratch_buf1, uint8_t *scratch_buf2) {
  assert(n > 0);
  assert(e > 0);
  assert(k0 < n);
  assert(n % 2 == 0);

  const uint8_t *in_bits = in;
  // bit selection as specified by section 5.4.2.1 in 3GPP TS 38.212
  // remove filler bits
  if (len_filler_bits > 0) {

    uint32_t len_s_f_bits = k - z * 2; // length of systematic & filler bits
    uint32_t len_s_bits =
        len_s_f_bits - len_filler_bits;     // length of systematic bits
    uint32_t len_p_bits = n - len_s_f_bits; // length of parity bits

    if (len_filler_bits % 8 == 0) {
      uint32_t len_s_f_bytes = len_s_f_bits >> 3;
      uint32_t len_s_bytes = len_s_bits >> 3;
      uint32_t len_p_bytes = len_p_bits >> 3;
      memcpy((void *)scratch_buf1, in, len_s_bytes);
      memcpy((void *)&scratch_buf1[len_s_bytes], &in[len_s_f_bytes],
             len_p_bytes);
    } else {
      bits_to_bytes(n, (const uint8_t *)in, (uint8_t *)scratch_buf1);
      memcpy(scratch_buf2, scratch_buf1, len_s_bits);
      memcpy(&scratch_buf2[len_s_bits], &scratch_buf1[len_s_f_bits],
             len_p_bits);
      bytes_to_bits((n - len_filler_bits), (const uint8_t *)scratch_buf2,
                    (uint8_t *)scratch_buf1);
    }

    in_bits = scratch_buf1;
  }

  // k0 depends on the redundancy version id.
  // Zero out last byte in case e is not an integer multiple of 8.
  uint32_t num_bytes = (e + 7) / 8;
  out[num_bytes - 1] = 0;

  uint32_t len = ((n - len_filler_bits) <= e) ? (n - len_filler_bits) : e;
  copy_bits(k0, 0, len, (n - len_filler_bits), in_bits, out);

  // repetition
  if (len < e) {
    copy_bits(k0, (n - len_filler_bits), e, (n - len_filler_bits), in_bits,
              out);
  }
}

void bit_interleave(uint32_t e, uint32_t qm, const uint8_t *in, uint8_t *out) {
  // performs the bit interleaving step of LDPC encoding, as specified in
  // section 5.4.2.2 of 3GPP TS 38.212.

  assert(e % qm == 0);

  memset((void *)out, 0, (e + 7) / 8);

  // transpose
  uint32_t dst_bit = 0;
  for (uint32_t j = 0; j < e / qm; j++) {
    for (uint32_t i = 0; i < qm; i++) {
      uint32_t src_bit = i * (e / qm) + j;
      uint8_t src_byte = in[src_bit / 8];
      uint32_t bit = (src_byte >> (7 - (src_bit % 8))) & 1;
      out[dst_bit / 8] |= (bit << (7 - (dst_bit % 8)));
      dst_bit++;
    }
  }
}

template<typename Allocator>
armral_status rate_matching(armral_ldpc_graph_t bg, uint32_t z, uint32_t e,
                            uint32_t nref, uint32_t len_filler_bits, uint32_t k,
                            uint32_t rv, armral_modulation_type mod,
                            const uint8_t *src, uint8_t *dst,
                            Allocator &allocator) {
  auto selected = allocate_zeroed<uint8_t>(allocator, e);
  uint32_t n = (bg == LDPC_BASE_GRAPH_2) ? 50 * z : 66 * z;

  // Map the modulation type onto the modulation order.
  uint32_t qm = 2 + (uint32_t)mod * 2;
  uint32_t ncb = 0;
  if (nref != 0) {
    ncb = (n > nref) ? nref : n;
  } else {
    ncb = n;
  }

  auto scratch_buf1 = allocate_zeroed<uint8_t>(allocator, n);
  auto scratch_buf2 = allocate_zeroed<uint8_t>(allocator, n);

  uint32_t k0 = starting_position(bg, rv, n, ncb, z);
  bit_selection(z, n, e, len_filler_bits, k, k0, src, selected.get(),
                scratch_buf1.get(), scratch_buf2.get());
  bit_interleave(e, qm, selected.get(), dst);
  return ARMRAL_SUCCESS;
}

} // namespace armral::ldpc

armral_status armral_ldpc_rate_matching(armral_ldpc_graph_t bg, uint32_t z,
                                        uint32_t e, uint32_t nref,
                                        uint32_t len_filler_bits, uint32_t k,
                                        uint32_t rv, armral_modulation_type mod,
                                        const uint8_t *src, uint8_t *dst) {
  heap_allocator allocator{};
  return armral::ldpc::rate_matching(bg, z, e, nref, len_filler_bits, k, rv,
                                     mod, src, dst, allocator);
}

armral_status armral_ldpc_rate_matching_noalloc(
    armral_ldpc_graph_t bg, uint32_t z, uint32_t e, uint32_t nref,
    uint32_t len_filler_bits, uint32_t k, uint32_t rv,
    armral_modulation_type mod, const uint8_t *src, uint8_t *dst,
    void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return armral::ldpc::rate_matching(bg, z, e, nref, len_filler_bits, k, rv,
                                     mod, src, dst, allocator);
}
