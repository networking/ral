/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include <arm_neon.h>

static inline poly128_t vmull_force_low_p64(poly64x2_t a, poly64x2_t b) {
  // Sometimes compilers don't realize that they don't need an extra
  // instruction to extract the 0th lane of a vector, e.g. when doing
  // vmull_p64(a[0], b[0]), so this just gets around that.
  poly128_t res;
  asm("pmull %0.1q, %1.1d, %2.1d" : "=w"(res) : "w"(a), "w"(b));
  return res;
}

static inline poly128_t vmull_force_high_p64(poly64x2_t a, poly64x2_t b) {
  // If vmull_high_p64 is used, then clang might use a mov to general
  // purpose registers and back follow by a pmull. This forces the use
  // of a single pmull2 instruction instead.
  poly128_t res;
  asm("pmull2 %0.1q, %1.2d, %2.2d" : "=w"(res) : "w"(a), "w"(b));
  return res;
}

template<char Endianness>
static inline poly64x2_t load_p64x2(const poly64_t *p_in) {
  poly64x2_t vec = vld1q_p64(p_in);
  if (Endianness == 'b') {
    vec = (poly64x2_t)vrev64q_u8((uint8x16_t)vec);
  }
  return vec;
}

template<char Endianness>
static inline poly64x2_t load_dup_p64(const poly64_t *p_in) {
  poly64x2_t vec = vld1q_dup_p64(p_in);
  if (Endianness == 'b') {
    vec = (poly64x2_t)vrev64q_u8((uint8x16_t)vec);
  }
  return vec;
}

static inline poly64x2_t add_p64x2(poly64x2_t a, poly64x2_t b) {
  // There are two reasons why we can't just use the vaddq_p64 intrinsic:
  // 1. It isn't available on the earliest GCC version we currently support
  // 2. If GCC recognizes that this is an associative operation, then it tries
  //    to optimize the operation tree in its tree-reassoc pass, but it
  //    actually makes the performance much worse. Hiding it in assembly means
  //    that the compiler uses our carefully balanced operation tree instead.
  uint8x16_t res;
  asm("eor %0.16b, %1.16b, %2.16b"
      : "=w"(res)
      : "w"((uint8x16_t)a), "w"((uint8x16_t)b));
  return (poly64x2_t)res;
}

/**
 * Computes a CRC64 in big- or little-endian mode using the specified shifts
 * and polynomials. This can be used for smaller polynomials by shifting
 * them to a degree 64 polynomial.
 *
 * @tparam     BarretShift     the shift used when computing @c ls1_divp.
 * @param[in]  size            number of bytes of the given buffer
 * @param[in]  input           points to the input byte sequence
 * @param[out] crc             the computed CRC
 * @param[in]  constants       the constants specific to each polynomial:
                               constants[0] = padding
                               constants[1] = (1<<128) / P_CRC - (1<<64)
                               constants[2:11] = [ (1<<(64*k)) mod P_CRC,
                                 for k in [1,1,2,3,4,5,6,7,8,9] ]
 */
template<char Endianness>
static inline __attribute__((always_inline)) void
crc64(uint32_t size, const uint64_t *input, uint64_t *crc,
      const poly64_t constants[]) {
  const poly64_t *p_in = (const poly64_t *)input;

  if (size == 8) {
    // Special case for <=64 bits
    poly64x2_t divp_p = vld1q_p64(&constants[1]);

    // This might compile to a separate ldr and dup, which is
    // fine because the operation using the upper half depends
    // on the output of the operation using the lower half.
    poly64x2_t v11 = load_dup_p64<Endianness>(p_in);

    // Barret reduction
    poly64x2_t vb = (poly64x2_t)vmull_force_low_p64(v11, divp_p);
    vb = add_p64x2(vb, v11);
    poly64x2_t v0x = (poly64x2_t)vmull_force_high_p64(vb, divp_p);
    *crc = (uint64_t)(v0x[0]);
    return;
  }

  // Load constants for size = 16
  poly64x2_t lsamodp_divp = vld1q_p64(&constants[0]);
  poly64x2_t ls11modp = vld1q_p64(&constants[2]);
  poly64x2_t ls23modp = vld1q_p64(&constants[4]);

  if (size == 16) {
    poly64x2_t v21 = load_p64x2<Endianness>(p_in);
    poly64x2_t v01 = (poly64x2_t)vmull_force_low_p64(v21, ls23modp);
    poly64x2_t vx1 = add_p64x2(v01, v21);

    // Barret reduction
    poly64x2_t vb = (poly64x2_t)vmull_force_high_p64(vx1, lsamodp_divp);
    vb = add_p64x2(vb, vx1);
    poly64x2_t v0x = (poly64x2_t)vmull_force_high_p64(vb, ls11modp);
    v0x = add_p64x2(v0x, v01);
    *crc = (uint64_t)(v0x[0]);
    return;
  }

  // Load the rest of the constants
  poly64x2_t ls45modp = vld1q_p64(&constants[6]);
  poly64x2_t ls67modp = vld1q_p64(&constants[8]);
  poly64x2_t ls89modp = vld1q_p64(&constants[10]);

  if (size == 32) {
    poly64x2_t v43a = load_p64x2<Endianness>(p_in);
    poly64x2_t v19 = load_p64x2<Endianness>(p_in + 2);
    poly64x2_t v01e = (poly64x2_t)vmull_force_low_p64(v43a, ls45modp);
    poly64x2_t v01a = (poly64x2_t)vmull_force_high_p64(v43a, ls23modp);
    poly64x2_t v01 = add_p64x2(v01a, v01e);
    v01a = (poly64x2_t)vmull_force_low_p64(v19, ls23modp);
    v01 = add_p64x2(v01, v01a);
    poly64x2_t vx1 = add_p64x2(v01, v19);

    // Barret reduction
    poly64x2_t vb = (poly64x2_t)vmull_force_high_p64(vx1, lsamodp_divp);
    vb = add_p64x2(vb, vx1);
    poly64x2_t v0x = (poly64x2_t)vmull_force_high_p64(vb, ls11modp);
    v0x = add_p64x2(v0x, v01);
    *crc = (uint64_t)(v0x[0]);
    return;
  }

  // remainder of the division by 64 byte == 512 bit, i.e. 4 vectors of 128 bit
  uint32_t init_bytes = size % 64;
  const poly64_t *p_end = p_in + (size - 16) / 8;

  // These values are carried forwards to the next loop iteration each time.
  poly64x2_t v01;

  if (init_bytes == 16) {
    v01 = vdupq_n_p64(0);
    p_in += 8;
  } else if (init_bytes == 32) {
    poly64x2_t v43 = load_p64x2<Endianness>(p_in);
    p_in += 10;
    poly64x2_t v01e = (poly64x2_t)vmull_force_low_p64(v43, ls45modp);
    poly64x2_t v01a = (poly64x2_t)vmull_force_high_p64(v43, ls23modp);
    v01 = add_p64x2(v01a, v01e);
  } else if (init_bytes == 48) {
    poly64x2_t v65 = load_p64x2<Endianness>(p_in);
    poly64x2_t v43 = load_p64x2<Endianness>(p_in + 2);
    p_in += 12;
    poly64x2_t v01g = (poly64x2_t)vmull_force_low_p64(v65, ls67modp);
    poly64x2_t v01e = (poly64x2_t)vmull_force_high_p64(v65, ls45modp);
    poly64x2_t v01c = (poly64x2_t)vmull_force_low_p64(v43, ls45modp);
    poly64x2_t v01a = (poly64x2_t)vmull_force_high_p64(v43, ls23modp);
    v01e = add_p64x2(v01e, v01g);
    v01a = add_p64x2(v01a, v01c);
    v01 = add_p64x2(v01a, v01e);

  } else {
    poly64x2_t v87 = load_p64x2<Endianness>(p_in);
    poly64x2_t v65 = load_p64x2<Endianness>(p_in + 2);
    poly64x2_t v43 = load_p64x2<Endianness>(p_in + 4);
    p_in += 14;
    poly64x2_t v01d = (poly64x2_t)vmull_force_low_p64(v87, ls89modp);
    poly64x2_t v01c = (poly64x2_t)vmull_force_high_p64(v87, ls67modp);
    poly64x2_t v01b = (poly64x2_t)vmull_force_low_p64(v65, ls67modp);
    poly64x2_t v01a = (poly64x2_t)vmull_force_high_p64(v65, ls45modp);
    poly64x2_t v01g = (poly64x2_t)vmull_force_low_p64(v43, ls45modp);
    poly64x2_t v01e = (poly64x2_t)vmull_force_high_p64(v43, ls23modp);
    v01c = add_p64x2(v01c, v01d);
    v01a = add_p64x2(v01a, v01b);
    v01e = add_p64x2(v01e, v01g);
    v01a = add_p64x2(v01a, v01c);
    v01 = add_p64x2(v01a, v01e);
  }

  poly64x2_t v19 = load_p64x2<Endianness>(p_in - 8);

  if (size <= 64) {
    poly64x2_t v01a = (poly64x2_t)vmull_force_low_p64(v19, ls23modp);
    v01 = add_p64x2(v01, v01a);
    poly64x2_t vx1 = add_p64x2(v01, v19);

    // Barret reduction
    poly64x2_t vb = (poly64x2_t)vmull_force_high_p64(vx1, lsamodp_divp);
    vb = add_p64x2(vb, vx1);
    poly64x2_t v0x = (poly64x2_t)vmull_force_high_p64(vb, ls11modp);
    v0x = add_p64x2(v0x, v01);
    *crc = (uint64_t)(v0x[0]);
    return;
  }

  poly64x2_t v87 = load_p64x2<Endianness>(p_in - 6);
  poly64x2_t v65 = load_p64x2<Endianness>(p_in - 4);
  poly64x2_t v43 = load_p64x2<Endianness>(p_in - 2);

  while (p_in < p_end) {
    poly64x2_t v01bb = (poly64x2_t)vmull_force_low_p64(v19, lsamodp_divp);
    poly64x2_t v01b = (poly64x2_t)vmull_force_high_p64(v87, ls67modp);
    poly64x2_t vx9 = add_p64x2(v01, v19);
    poly64x2_t v8x = add_p64x2(v87, v01);

    v19 = load_p64x2<Endianness>(p_in);
    v87 = load_p64x2<Endianness>(p_in + 2);

    poly64x2_t v01g = (poly64x2_t)vmull_force_high_p64(vx9, ls89modp);
    poly64x2_t v01e = (poly64x2_t)vmull_force_low_p64(v8x, ls89modp);

    v01b = add_p64x2(v01b, v01bb);

    poly64x2_t v01aa = (poly64x2_t)vmull_force_low_p64(v65, ls67modp);
    poly64x2_t v01a = (poly64x2_t)vmull_force_high_p64(v65, ls45modp);
    poly64x2_t v01d = (poly64x2_t)vmull_force_low_p64(v43, ls45modp);
    poly64x2_t v01c = (poly64x2_t)vmull_force_high_p64(v43, ls23modp);

    v65 = load_p64x2<Endianness>(p_in + 4);
    v43 = load_p64x2<Endianness>(p_in + 6);
    p_in += 8;

    v01a = add_p64x2(v01a, v01aa);
    v01c = add_p64x2(v01c, v01d);
    v01a = add_p64x2(v01a, v01b);
    v01e = add_p64x2(v01e, v01g);
    v01a = add_p64x2(v01a, v01c);
    v01 = add_p64x2(v01a, v01e);
  }

  poly64x2_t v21 = load_p64x2<Endianness>(p_in);

  poly64x2_t v01ff = (poly64x2_t)vmull_force_low_p64(v19, lsamodp_divp);
  poly64x2_t v01f = (poly64x2_t)vmull_force_high_p64(v87, ls67modp);
  poly64x2_t vx9 = add_p64x2(v01, v19);
  poly64x2_t v8x = add_p64x2(v87, v01);

  poly64x2_t v01ee = (poly64x2_t)vmull_force_high_p64(vx9, ls89modp);
  poly64x2_t v01e = (poly64x2_t)vmull_force_low_p64(v8x, ls89modp);

  v01f = add_p64x2(v01f, v01ff);
  v01e = add_p64x2(v01e, v01ee);
  v01e = add_p64x2(v01e, v01f);

  poly64x2_t v01d = (poly64x2_t)vmull_force_low_p64(v65, ls67modp);
  poly64x2_t v01c = (poly64x2_t)vmull_force_high_p64(v65, ls45modp);
  poly64x2_t v01b = (poly64x2_t)vmull_force_low_p64(v43, ls45modp);
  poly64x2_t v01a = (poly64x2_t)vmull_force_high_p64(v43, ls23modp);
  poly64x2_t v01g = (poly64x2_t)vmull_force_low_p64(v21, ls23modp);

  v01c = add_p64x2(v01c, v01d);
  v01a = add_p64x2(v01a, v01b);
  v01e = add_p64x2(v01e, v01g);
  v01a = add_p64x2(v01a, v01c);
  v01 = add_p64x2(v01a, v01e);

  poly64x2_t vx1 = add_p64x2(v01, v21);

  // Barret reduction
  poly64x2_t vb = (poly64x2_t)vmull_force_high_p64(vx1, lsamodp_divp);
  vb = add_p64x2(vb, vx1);
  poly64x2_t v0x = (poly64x2_t)vmull_force_high_p64(vb, ls11modp);
  v0x = add_p64x2(v0x, v01);
  *crc = (uint64_t)(v0x[0]);
}
