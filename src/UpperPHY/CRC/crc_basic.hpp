/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include <cstdint>

/**
 * A basic "one bit at a time" CRC implementation.
 */
static inline uint32_t crc_basic_impl(uint32_t len, uint32_t gen,
                                      const uint64_t *input, uint32_t n,
                                      bool big_endian) {
  uint32_t crc = 0;
  const uint8_t *bytes = (const uint8_t *)input;
  for (uint32_t i = 0; i < n; ++i) {
    int ofs = big_endian ? i : i ^ 7;
    uint32_t elem = bytes[ofs];
    crc ^= elem << (len - 8);
    for (uint32_t j = 0; j < 8; ++j) {
      crc <<= 1;
      if ((crc & (1U << len)) != 0) {
        crc ^= gen;
      }
    }
  }
  return crc & ((1U << len) - 1U);
}
