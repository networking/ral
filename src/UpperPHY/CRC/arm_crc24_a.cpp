/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "crc_common.hpp"

// Constants used to perform reduction:
static const poly64_t crc24_a_data[] = {
    // (1<<(64*k)) mod P_CRC, for k = 10
    0xa38dea0000000000,
    // (1<<128) / P_CRC - (1<<64)
    0xf845fe2493242da4,
    // (1<<(64*k)) mod P_CRC, for k in [1,1,2,3,4,5,6,7,8,9]
    0x864cfb0000000000, 0x864cfb0000000000, 0xfd7e0c0000000000,
    0xc4b14d0000000000, 0x911cf10000000000, 0x674e180000000000,
    0x6e7a2c0000000000, 0x6708e30000000000, 0x74b44a0000000000,
    0x08289a0000000000};

template<char Endianness>
static void crc24_a(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc64<Endianness>(size, input, crc, crc24_a_data);

  // Return the upper 24 bits
  *crc >>= 40;
}

armral_status __attribute__((flatten))
armral_crc24_a_be(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc24_a<'b'>(size, input, crc);
  return ARMRAL_SUCCESS;
}

armral_status __attribute__((flatten))
armral_crc24_a_le(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc24_a<'l'>(size, input, crc);
  return ARMRAL_SUCCESS;
}
