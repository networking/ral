/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "crc_common.hpp"

// Constants used to perform reduction:
static const poly64_t crc6_data[] = {
    // (1<<(64*k)) mod P_CRC, for k = 10
    0x5400000000000000,
    // (1<<128) / P_CRC - (1<<64)
    0xfab376938bca3083,
    // (1<<(64*k)) mod P_CRC, for k in [1,1,2,3,4,5,6,7,8,9]
    0x8400000000000000, 0x8400000000000000, 0x8c00000000000000,
    0x9c00000000000000, 0xbc00000000000000, 0xfc00000000000000,
    0x7c00000000000000, 0xf800000000000000, 0x7400000000000000,
    0xe800000000000000};

template<char Endianness>
static void crc6(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc64<Endianness>(size, input, crc, crc6_data);

  // Return the upper 16 bits
  *crc >>= 58;
}

armral_status __attribute__((flatten))
armral_crc6_be(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc6<'b'>(size, input, crc);
  return ARMRAL_SUCCESS;
}

armral_status __attribute__((flatten))
armral_crc6_le(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc6<'l'>(size, input, crc);
  return ARMRAL_SUCCESS;
}
