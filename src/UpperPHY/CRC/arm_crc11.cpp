/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "crc_common.hpp"

// Constants used to perform reduction:
static const poly64_t crc11_data[] = {
    // (1<<(64*k)) mod P_CRC, for k = 10
    0xa080000000000000,
    // (1<<128) / P_CRC - (1<<64)
    0xb3fa1f48b92fa293,
    // (1<<(64*k)) mod P_CRC, for k in [1,1,2,3,4,5,6,7,8,9]
    0xc420000000000000, 0xc420000000000000, 0x5e60000000000000,
    0xa140000000000000, 0xae20000000000000, 0x5680000000000000,
    0x9e60000000000000, 0xe8e0000000000000, 0x34c0000000000000,
    0xb900000000000000};

template<char Endianness>
static void crc11(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc64<Endianness>(size, input, crc, crc11_data);

  // Return the upper 11 bits
  *crc >>= 53;
}

armral_status __attribute__((flatten))
armral_crc11_be(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc11<'b'>(size, input, crc);
  return ARMRAL_SUCCESS;
}

armral_status __attribute__((flatten))
armral_crc11_le(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc11<'l'>(size, input, crc);
  return ARMRAL_SUCCESS;
}
