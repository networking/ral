/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "crc_common.hpp"

// Constants used to perform reduction:
static const poly64_t crc16_data[] = {
    // (1<<(64*k)) mod P_CRC, for k = 10
    0x8420000000000000,
    // (1<<128) / P_CRC - (1<<64)
    0x11303471a041b343,
    // (1<<(64*k)) mod P_CRC, for k in [1,1,2,3,4,5,6,7,8,9]
    0x1021000000000000, 0x1021000000000000, 0xeb23000000000000,
    0x10e2000000000000, 0x45b4000000000000, 0x8ddc000000000000,
    0xb8e0000000000000, 0xbd64000000000000, 0x9fe5000000000000,
    0x78b3000000000000};

template<char Endianness>
static void crc16(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc64<Endianness>(size, input, crc, crc16_data);

  // Return the upper 16 bits
  *crc >>= 48;
}

armral_status __attribute__((flatten))
armral_crc16_be(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc16<'b'>(size, input, crc);
  return ARMRAL_SUCCESS;
}

armral_status __attribute__((flatten))
armral_crc16_le(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc16<'l'>(size, input, crc);
  return ARMRAL_SUCCESS;
}
