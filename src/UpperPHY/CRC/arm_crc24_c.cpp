/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "crc_common.hpp"

// Constants used to perform reduction:
static const poly64_t crc24_c_data[] = {
    // (1<<(64*k)) mod P_CRC, for k = 10
    0x563dff0000000000,
    // (1<<128) / P_CRC - (1<<64)
    0xc52cdcad524ab8e3,
    // (1<<(64*k)) mod P_CRC, for k in [1,1,2,3,4,5,6,7,8,9]
    0xb2b1170000000000, 0xb2b1170000000000, 0x1397990000000000,
    0x3a08dc0000000000, 0x50153f0000000000, 0x481d630000000000,
    0x8c64240000000000, 0xee87f20000000000, 0xf83b550000000000,
    0x9718250000000000};

template<char Endianness>
static void crc24_c(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc64<Endianness>(size, input, crc, crc24_c_data);

  // Return the upper 24 bits
  *crc >>= 40;
}

armral_status __attribute__((flatten))
armral_crc24_c_be(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc24_c<'b'>(size, input, crc);
  return ARMRAL_SUCCESS;
}

armral_status __attribute__((flatten))
armral_crc24_c_le(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc24_c<'l'>(size, input, crc);
  return ARMRAL_SUCCESS;
}
