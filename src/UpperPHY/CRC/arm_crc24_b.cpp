/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "crc_common.hpp"

// Constants used to perform reduction:
static const poly64_t crc24_b_data[] = {
    // (1<<(64*k)) mod P_CRC, for k = 10
    0xdf24f50000000000,
    // (1<<128) / P_CRC - (1<<64)
    0xffff83ffe007f83e,
    // (1<<(64*k)) mod P_CRC, for k in [1,1,2,3,4,5,6,7,8,9]
    0x8000630000000000, 0x8000630000000000, 0x0900020000000000,
    0xa1496b0000000000, 0x8463290000000000, 0x24057a0000000000,
    0x5846020000000000, 0xc214050000000000, 0xc252200000000000,
    0x4646f20000000000};

template<char Endianness>
static void crc24_b(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc64<Endianness>(size, input, crc, crc24_b_data);

  // Return the upper 24 bits
  *crc >>= 40;
}

armral_status __attribute__((flatten))
armral_crc24_b_be(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc24_b<'b'>(size, input, crc);
  return ARMRAL_SUCCESS;
}

armral_status __attribute__((flatten))
armral_crc24_b_le(const uint32_t size, const uint64_t *input, uint64_t *crc) {
  crc24_b<'l'>(size, input, crc);
  return ARMRAL_SUCCESS;
}
