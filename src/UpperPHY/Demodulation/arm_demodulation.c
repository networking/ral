/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"

#include <stdlib.h>

inline static armral_status
armral_demodulation_qpsk(const uint32_t n_symbols, const uint16_t ulp,
                         const armral_cmplx_int16_t *p_src, int8_t *p_dst) {
  // The log likelihood ratio of a bit being received as 1 is directly
  // proportional to the modulated symbol received
  const int16_t weight = (1 << 15) / ulp;

  /* Compute 8 complex symbols at a time */
  uint32_t blk_cnt = n_symbols >> 3;
  while (blk_cnt > 0U) {
    int16x8_t rec_a = vld1q_s16((const int16_t *)p_src);
    int16x8_t rec_b = vld1q_s16((const int16_t *)(p_src + 4));

    int16x8_t llr16_a = vqrdmulhq_n_s16(rec_a, weight);
    int16x8_t llr16_b = vqrdmulhq_n_s16(rec_b, weight);

    int8x8_t llr8_a = vqmovn_s16(llr16_a);
    int8x16_t llr8_b = vqmovn_high_s16(llr8_a, llr16_b);

    vst1q_s8(p_dst, llr8_b);

    p_src += 8;
    p_dst += 16;
    blk_cnt--;
  }

  uint32_t tail_cnt = n_symbols & 0x7;

  /* Compute 4 complex symbols at a time */
  if (tail_cnt >= 4U) {
    int16x8_t rec_a = vld1q_s16((const int16_t *)p_src);
    int16x8_t llr16_a = vqrdmulhq_n_s16(rec_a, weight);
    int8x8_t llr8_a = vqmovn_s16(llr16_a);

    vst1_s8(p_dst, llr8_a);

    p_src += 4;
    p_dst += 8;
    tail_cnt -= 4;
  }

  while (tail_cnt > 0U) {
    int16_t llr16_a = vqrdmulhh_s16(p_src->re, weight);
    int16_t llr16_b = vqrdmulhh_s16(p_src->im, weight);
    *p_dst++ = vqmovnh_s16(llr16_a);
    *p_dst++ = vqmovnh_s16(llr16_b);

    p_src++;
    tail_cnt--;
  }
  return ARMRAL_SUCCESS;
}

/*
  * Inline function for 16QAM demodulation
 * @par       LLRs calculation for 16QAM
              LLRs calculation for 16QAM, having received a complex symbol rx =
 rx_re + j*rx_im. The LLRs calculations are made approximately with thresholds
 method, to have a complexity of O(m). 16QAM Gray mapping following 3GPP
 TS 38.211 V15.2.0, Ch. 5.1 Modulation mapper Bits position[c0 c1 c2 c3]

              LLR(c0|r) = weight * [-rx_re]
              LLR(c1|r) = weight * [-rx_im]
              LLR(c2|r) = weight * [|rx_re| - 2/sqrt(10)]
              LLR(c3|r) = weight * [|rx_im| - 2/sqrt(10)]
  *
*/

inline static armral_status
armral_demodulation_16qam(const uint32_t n_symbols, const uint16_t ulp,
                          const armral_cmplx_int16_t *p_src, int8_t *p_dst) {
  // THR_16QAM = -2/sqrt(10) [Q(2.13)] due to 3GPP Gray Mapping for 16QAM
  // modulation
#define THR_16QAM (-5181)

  // The log likelihood ratio of a bit being received as 1 is directly
  // proportional to the modulated symbol received
  const int16_t weight = (1 << 15) / ulp;
  const int16_t neg_weight = -weight;

  const int16x8_t zeros = vdupq_n_s16(0);
  const int16x8_t thrs = vdupq_n_s16(THR_16QAM);

  /* Compute 8 complex symbols at a time */
  uint32_t blk_cnt = n_symbols >> 3;
  while (blk_cnt > 0U) {
    int16x8_t rec_a = vld1q_s16((const int16_t *)p_src);
    int16x8_t rec_b = vld1q_s16((const int16_t *)p_src + 8);

    /* Computing L(c0/r) and L(c1/r) */
    int16x8_t llr16_1a = vqrdmulhq_n_s16(rec_a, weight);
    int16x8_t llr16_1b = vqrdmulhq_n_s16(rec_b, weight);

    int8x8_t llr8_1a = vqmovn_s16(llr16_1a);
    int8x16_t llr8_1b = vqmovn_high_s16(llr8_1a, llr16_1b);

    /* Computing L(c2/r) and L(c3/r) */
    rec_a = vabaq_s16(thrs, rec_a, zeros);
    rec_b = vabaq_s16(thrs, rec_b, zeros);

    int16x8_t llr16_2a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int16x8_t llr16_2b = vqrdmulhq_n_s16(rec_b, neg_weight);

    int8x8_t llr8_2a = vqmovn_s16(llr16_2a);
    int8x16_t llr8_2b = vqmovn_high_s16(llr8_2a, llr16_2b);

    /* Store results for consecutive sets of symbols */
    uint16x8x2_t out;
    out.val[0] = vreinterpretq_u16_s8(llr8_1b);
    out.val[1] = vreinterpretq_u16_s8(llr8_2b);
    vst2q_u16((uint16_t *)p_dst, out);

    p_src += 8;
    p_dst += 32;
    blk_cnt--;
  }

  uint32_t tail_cnt = n_symbols & 0x7;

  /* Compute 4 complex symbols at a time */
  if (tail_cnt >= 4U) {
    int16x8_t rec_a = vld1q_s16((const int16_t *)p_src);

    /* Computing L(c0/r) and L(c1/r) */
    int16x8_t llr16_1a = vqrdmulhq_n_s16(rec_a, weight);
    int8x8_t llr8_1a = vqmovn_s16(llr16_1a);

    /* Computing L(c2/r) and L(c3/r) */
    rec_a = vabaq_s16(thrs, rec_a, zeros);
    int16x8_t llr16_2a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int8x8_t llr8_2a = vqmovn_s16(llr16_2a);

    /* Store results for consecutive sets of symbols */
    uint16x4x2_t out;
    out.val[0] = vreinterpret_u16_s8(llr8_1a);
    out.val[1] = vreinterpret_u16_s8(llr8_2a);
    vst2_u16((uint16_t *)p_dst, out);

    p_src += 4;
    p_dst += 16;
    tail_cnt -= 4;
  }

  while (tail_cnt > 0U) {
    // L(c0/r) and L(c1/r)
    int16_t llr16_1a = vqrdmulhh_s16(p_src->re, weight);
    int16_t llr16_1b = vqrdmulhh_s16(p_src->im, weight);
    *p_dst++ = vqmovnh_s16(llr16_1a);
    *p_dst++ = vqmovnh_s16(llr16_1b);

    // L(c2/r) and L(c3/r)
    int16_t llr16_2a = vqrdmulhh_s16(abs(p_src->re) + THR_16QAM, neg_weight);
    int16_t llr16_2b = vqrdmulhh_s16(abs(p_src->im) + THR_16QAM, neg_weight);
    *p_dst++ = vqmovnh_s16(llr16_2a);
    *p_dst++ = vqmovnh_s16(llr16_2b);

    p_src++;
    tail_cnt--;
  }
  return ARMRAL_SUCCESS;
}

/*
 * Inline function for 64QAM demodulation
 * @par       LLRs calculation for 64QAM
              LLRs calculation for 64QAM, having received a complex symbol rx =
 rx_re + j*rx_im. The LLRs calculations are made approximately with thresholds
 method, to have a complexity of O(m). 64QAM Gray mapping following 3GPP
 TS 38.211 V15.2.0, Ch. 5.1 Modulation mapper Bits position[c0 c1 c2 c3 c4 c5]

              LLR(c0|r) = weight * [-rx_re]
              LLR(c1|r) = weight * [-rx_im]
              LLR(c2|r) = weight * [|rx_re| - 4/sqrt(42)]
              LLR(c3|r) = weight * [|rx_im| - 4/sqrt(42)]
              LLR(c4|r) = weight * [||rx_re| - 4/sqrt(42)| - 2/sqrt(42)]
              LLR(c5|r) = weight * [||rx_im| - 4/sqrt(42)| - 2/sqrt(42)]
  *
*/

inline static armral_status
armral_demodulation_64qam(const uint32_t n_symbols, const uint16_t ulp,
                          const armral_cmplx_int16_t *p_src, int8_t *p_dst) {
  // THR_64QAM_1 = -4/sqrt(42) [Q(2.13) format], due to 3GPP Gray Mapping for
  // 64QAM modulation
#define THR_64QAM_1 (-5056)
  // = -2/sqrt(42) [Q(2.13) format], due to 3GPP Gray Mapping for 64QAM
  // modulation
#define THR_64QAM_2 (-2528)

  // The log likelihood ratio of a bit being received as 1 is directly
  // proportional to the modulated symbol received
  const int16_t weight = (1 << 15) / ulp;
  const int16_t neg_weight = -weight;

  const int16x8_t zeros = vdupq_n_s16(0);
  const int16x8_t thr_1 = vdupq_n_s16(THR_64QAM_1);
  const int16x8_t thr_2 = vdupq_n_s16(THR_64QAM_2);

  /* Compute 8 complex symbols at a time */
  uint32_t blk_cnt = n_symbols >> 3;
  while (blk_cnt > 0U) {
    int16x8_t rec_a = vld1q_s16((const int16_t *)p_src);
    int16x8_t rec_b = vld1q_s16((const int16_t *)p_src + 8);

    /* Computing L(c0/r) and L(c1/r) */
    int16x8_t llr16_1a = vqrdmulhq_n_s16(rec_a, weight);
    int16x8_t llr16_1b = vqrdmulhq_n_s16(rec_b, weight);

    int8x8_t llr8_1a = vqmovn_s16(llr16_1a);
    int8x16_t llr8_1b = vqmovn_high_s16(llr8_1a, llr16_1b);

    /* Computing L(c2/r) and L(c3/r) */
    rec_a = vabaq_s16(thr_1, rec_a, zeros);
    rec_b = vabaq_s16(thr_1, rec_b, zeros);

    int16x8_t llr16_2a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int16x8_t llr16_2b = vqrdmulhq_n_s16(rec_b, neg_weight);

    int8x8_t llr8_2a = vqmovn_s16(llr16_2a);
    int8x16_t llr8_2b = vqmovn_high_s16(llr8_2a, llr16_2b);

    /* Computing L(c4/r) and L(c5/r) */
    rec_a = vabaq_s16(thr_2, rec_a, zeros);
    rec_b = vabaq_s16(thr_2, rec_b, zeros);

    int16x8_t llr16_3a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int16x8_t llr16_3b = vqrdmulhq_n_s16(rec_b, neg_weight);

    int8x8_t llr8_3a = vqmovn_s16(llr16_3a);
    int8x16_t llr8_3b = vqmovn_high_s16(llr8_3a, llr16_3b);

    /* Store results for consecutive sets of symbols */
    uint16x8x3_t out;
    out.val[0] = vreinterpretq_u16_s8(llr8_1b);
    out.val[1] = vreinterpretq_u16_s8(llr8_2b);
    out.val[2] = vreinterpretq_u16_s8(llr8_3b);
    vst3q_u16((uint16_t *)p_dst, out);

    p_src += 8;
    p_dst += 48;
    blk_cnt--;
  }

  uint32_t tail_cnt = n_symbols & 0x7;

  /* Compute 4 complex symbols at a time */
  if (tail_cnt >= 4U) {
    int16x8_t rec_a = vld1q_s16((const int16_t *)p_src);

    /* Computing L(c0/r) and L(c1/r) */
    int16x8_t llr16_1a = vqrdmulhq_n_s16(rec_a, weight);
    int8x8_t llr8_1a = vqmovn_s16(llr16_1a);

    /* Computing L(c2/r) and L(c3/r) */
    rec_a = vabaq_s16(thr_1, rec_a, zeros);
    int16x8_t llr16_2a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int8x8_t llr8_2a = vqmovn_s16(llr16_2a);

    /* Computing L(c4/r) and L(c5/r) */
    rec_a = vabaq_s16(thr_2, rec_a, zeros);
    int16x8_t llr16_3a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int8x8_t llr8_3a = vqmovn_s16(llr16_3a);

    /* Store results for consecutive sets of symbols */
    uint16x4x3_t out;
    out.val[0] = vreinterpret_u16_s8(llr8_1a);
    out.val[1] = vreinterpret_u16_s8(llr8_2a);
    out.val[2] = vreinterpret_u16_s8(llr8_3a);
    vst3_u16((uint16_t *)p_dst, out);

    p_src += 4;
    p_dst += 24;
    tail_cnt -= 4;
  }

  while (tail_cnt > 0U) {
    // L(c0/r) and L(c1/r)
    int16_t llr16_1a = vqrdmulhh_s16(p_src->re, weight);
    int16_t llr16_1b = vqrdmulhh_s16(p_src->im, weight);
    *p_dst++ = vqmovnh_s16(llr16_1a);
    *p_dst++ = vqmovnh_s16(llr16_1b);

    // L(c2/r) and L(c3/r)
    int16_t tmp_1a = abs(p_src->re) + THR_64QAM_1;
    int16_t tmp_1b = abs(p_src->im) + THR_64QAM_1;
    int16_t llr16_2a = vqrdmulhh_s16(tmp_1a, neg_weight);
    int16_t llr16_2b = vqrdmulhh_s16(tmp_1b, neg_weight);
    *p_dst++ = vqmovnh_s16(llr16_2a);
    *p_dst++ = vqmovnh_s16(llr16_2b);

    // L(c4/r) and L(c5/r)
    int16_t llr16_3a = vqrdmulhh_s16(abs(tmp_1a) + THR_64QAM_2, neg_weight);
    int16_t llr16_3b = vqrdmulhh_s16(abs(tmp_1b) + THR_64QAM_2, neg_weight);
    *p_dst++ = vqmovnh_s16(llr16_3a);
    *p_dst++ = vqmovnh_s16(llr16_3b);

    p_src++;
    tail_cnt--;
  }
  return ARMRAL_SUCCESS;
}

/*
 * Inline function for 256QAM demodulation
 * @par       LLRs calculation for 256QAM
              LLRs calculation for 256QAM, having received a complex symbol rx =
 rx_re + j*rx_im. The LLRs calculations are made approximately with thresholds
 method, to have a complexity of O(m). 256QAM Gray mapping following 3GPP
 TS 38.211 V15.2.0, Ch. 5.1	Modulation mapper Bits position[c0 c1 c2 c3 c4
 c5 c6 c7]

    LLR(c0|r) = weight * [-rx_re]
    LLR(c1|r) = weight * [-rx_im]
    LLR(c2|r) = weight * [|rx_re| - 8/sqrt(170)]
    LLR(c3|r) = weight * [|rx_im| - 8/sqrt(170)]
    LLR(c4|r) = weight * [||rx_re| - 8/sqrt(170)| - 4/sqrt(170)]
    LLR(c5|r) = weight * [||rx_im| - 8/sqrt(170)| - 4/sqrt(170)]
    LLR(c6|r) = weight * [|||rx_re| - 8/sqrt(170)| - 4/sqrt(170)| - 2/sqrt(170)]
    LLR(c7|r) = weight * [|||rx_im| - 8/sqrt(170)| - 4/sqrt(170)| - 2/sqrt(170)]
  *
*/
inline static armral_status
armral_demodulation_256qam(const uint32_t n_symbols, const uint16_t ulp,
                           const armral_cmplx_int16_t *p_src, int8_t *p_dst) {
  // THR_256QAM_1 = -8/sqrt(170) [Q(2.13) format], due to 3GPP Gray Mapping for
  // 256QAM modulation
#define THR_256QAM_1 (-5026)
  // THR_256QAM_2 = -4/sqrt(170) [Q(2.13) format], due to 3GPP Gray Mapping for
  // 256QAM modulation
#define THR_256QAM_2 (-2513)
  // THR_256QAM_3 = -2/sqrt(170) [Q(2.13) format], due to 3GPP Gray Mapping for
  // 256QAM modulation
#define THR_256QAM_3 (-1257)

  // The log likelihood ratio of a bit being received as 1 is directly
  // proportional to the modulated symbol received
  const int16_t weight = (1 << 15) / ulp;
  const int16_t neg_weight = -weight;

  const int16x8_t zeros = vdupq_n_s16(0);
  const int16x8_t thr_1 = vdupq_n_s16(THR_256QAM_1);
  const int16x8_t thr_2 = vdupq_n_s16(THR_256QAM_2);
  const int16x8_t thr_3 = vdupq_n_s16(THR_256QAM_3);

  /* Compute 8 complex symbols at a time */
  uint32_t blk_cnt = n_symbols >> 3;
  while (blk_cnt > 0U) {
    int16x8_t rec_a = vld1q_s16((const int16_t *)p_src);
    int16x8_t rec_b = vld1q_s16((const int16_t *)p_src + 8);

    /* Computing L(c0/r) and L(c1/r) */
    int16x8_t llr16_1a = vqrdmulhq_n_s16(rec_a, weight);
    int16x8_t llr16_1b = vqrdmulhq_n_s16(rec_b, weight);

    int8x8_t llr8_1a = vqmovn_s16(llr16_1a);
    int8x16_t llr8_1b = vqmovn_high_s16(llr8_1a, llr16_1b);

    /* Computing L(c2/r) and L(c3/r) */
    rec_a = vabaq_s16(thr_1, rec_a, zeros);
    rec_b = vabaq_s16(thr_1, rec_b, zeros);

    int16x8_t llr16_2a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int16x8_t llr16_2b = vqrdmulhq_n_s16(rec_b, neg_weight);

    int8x8_t llr8_2a = vqmovn_s16(llr16_2a);
    int8x16_t llr8_2b = vqmovn_high_s16(llr8_2a, llr16_2b);

    /* Computing L(c4/r) and L(c5/r) */
    rec_a = vabaq_s16(thr_2, rec_a, zeros);
    rec_b = vabaq_s16(thr_2, rec_b, zeros);

    int16x8_t llr16_3a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int16x8_t llr16_3b = vqrdmulhq_n_s16(rec_b, neg_weight);

    int8x8_t llr8_3a = vqmovn_s16(llr16_3a);
    int8x16_t llr8_3b = vqmovn_high_s16(llr8_3a, llr16_3b);

    /* Computing L(c6/r) and L(c7/r) */
    rec_a = vabaq_s16(thr_3, rec_a, zeros);
    rec_b = vabaq_s16(thr_3, rec_b, zeros);

    int16x8_t llr16_4a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int16x8_t llr16_4b = vqrdmulhq_n_s16(rec_b, neg_weight);

    int8x8_t llr8_4a = vqmovn_s16(llr16_4a);
    int8x16_t llr8_4b = vqmovn_high_s16(llr8_4a, llr16_4b);

    /* Store results for consecutive sets of symbols */
    uint16x8x4_t out;
    out.val[0] = vreinterpretq_u16_s8(llr8_1b);
    out.val[1] = vreinterpretq_u16_s8(llr8_2b);
    out.val[2] = vreinterpretq_u16_s8(llr8_3b);
    out.val[3] = vreinterpretq_u16_s8(llr8_4b);
    vst4q_u16((uint16_t *)p_dst, out);

    p_src += 8;
    p_dst += 64;
    blk_cnt--;
  }

  uint32_t tail_cnt = n_symbols & 0x7;

  /* Compute 4 complex symbols at a time */
  if (tail_cnt >= 4U) {
    int16x8_t rec_a = vld1q_s16((const int16_t *)p_src);

    /* Computing L(c0/r) and L(c1/r) */
    int16x8_t llr16_1a = vqrdmulhq_n_s16(rec_a, weight);
    int8x8_t llr8_1a = vqmovn_s16(llr16_1a);

    /* Computing L(c2/r) and L(c3/r) */
    rec_a = vabaq_s16(thr_1, rec_a, zeros);
    int16x8_t llr16_2a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int8x8_t llr8_2a = vqmovn_s16(llr16_2a);

    /* Computing L(c4/r) and L(c5/r) */
    rec_a = vabaq_s16(thr_2, rec_a, zeros);
    int16x8_t llr16_3a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int8x8_t llr8_3a = vqmovn_s16(llr16_3a);

    /* Computing L(c6/r) and L(c7/r) */
    rec_a = vabaq_s16(thr_3, rec_a, zeros);
    int16x8_t llr16_4a = vqrdmulhq_n_s16(rec_a, neg_weight);
    int8x8_t llr8_4a = vqmovn_s16(llr16_4a);

    /* Store results for consecutive sets of symbols */
    uint16x4x4_t out;
    out.val[0] = vreinterpret_u16_s8(llr8_1a);
    out.val[1] = vreinterpret_u16_s8(llr8_2a);
    out.val[2] = vreinterpret_u16_s8(llr8_3a);
    out.val[3] = vreinterpret_u16_s8(llr8_4a);
    vst4_u16((uint16_t *)p_dst, out);

    p_src += 4;
    p_dst += 32;
    tail_cnt -= 4;
  }

  while (tail_cnt > 0U) {
    // L(c0/r) and L(c1/r)
    int16_t llr16_1a = vqrdmulhh_s16(p_src->re, weight);
    int16_t llr16_1b = vqrdmulhh_s16(p_src->im, weight);
    *p_dst++ = vqmovnh_s16(llr16_1a);
    *p_dst++ = vqmovnh_s16(llr16_1b);

    // L(c2/r) and L(c3/r)
    int16_t tmp_1a = abs(p_src->re) + THR_256QAM_1;
    int16_t tmp_1b = abs(p_src->im) + THR_256QAM_1;
    int16_t llr16_2a = vqrdmulhh_s16(tmp_1a, neg_weight);
    int16_t llr16_2b = vqrdmulhh_s16(tmp_1b, neg_weight);
    *p_dst++ = vqmovnh_s16(llr16_2a);
    *p_dst++ = vqmovnh_s16(llr16_2b);

    // L(c4/r) and L(c5/r)
    int16_t tmp_2a = abs(tmp_1a) + THR_256QAM_2;
    int16_t tmp_2b = abs(tmp_1b) + THR_256QAM_2;
    int16_t llr16_3a = vqrdmulhh_s16(tmp_2a, neg_weight);
    int16_t llr16_3b = vqrdmulhh_s16(tmp_2b, neg_weight);
    *p_dst++ = vqmovnh_s16(llr16_3a);
    *p_dst++ = vqmovnh_s16(llr16_3b);

    // L(c6/r) and L(c7/r)
    int16_t llr16_4a = vqrdmulhh_s16(abs(tmp_2a) + THR_256QAM_3, neg_weight);
    int16_t llr16_4b = vqrdmulhh_s16(abs(tmp_2b) + THR_256QAM_3, neg_weight);
    *p_dst++ = vqmovnh_s16(llr16_4a);
    *p_dst++ = vqmovnh_s16(llr16_4b);

    p_src++;
    tail_cnt--;
  }
  return ARMRAL_SUCCESS;
}

armral_status armral_demodulation(const uint32_t n_symbols, const uint16_t ulp,
                                  armral_modulation_type mod_type,
                                  const armral_cmplx_int16_t *p_src,
                                  int8_t *p_dst) {
  // If we don't set the return type, it's because the modType isn't recognized.
  // Therefore, we have an argument error by default.
  armral_status ret = ARMRAL_ARGUMENT_ERROR;
  switch (mod_type) {
  case ARMRAL_MOD_QPSK:
    ret = armral_demodulation_qpsk(n_symbols, ulp, p_src, p_dst);
    break;
  case ARMRAL_MOD_16QAM:
    ret = armral_demodulation_16qam(n_symbols, ulp, p_src, p_dst);
    break;
  case ARMRAL_MOD_64QAM:
    ret = armral_demodulation_64qam(n_symbols, ulp, p_src, p_dst);
    break;
  case ARMRAL_MOD_256QAM:
    ret = armral_demodulation_256qam(n_symbols, ulp, p_src, p_dst);
    break;
  }
  return ret;
}
