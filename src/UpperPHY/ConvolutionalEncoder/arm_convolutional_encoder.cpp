/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstdlib>
#include <cstring>

armral_status armral_tail_biting_convolutional_encode_block(const uint8_t *src,
                                                            uint32_t k,
                                                            uint8_t *dst0,
                                                            uint8_t *dst1,
                                                            uint8_t *dst2) {
  // y0[n] = x[n] + s[1] + s[2] + s[4] + s[5]
  // y1[n] = x[n] + s[0] + s[1] + s[2] + s[5]
  // y2[n] = x[n] + s[0] + s[1] + s[3] + s[5]
  // The shift register (s) is initialized with the last six
  // information bits of the input stream (s[i] = x[k-1-i])

  poly64_t pmask0 = 0b01011011;
  poly64_t pmask1 = 0b01111001;
  poly64_t pmask2 = 0b01110101;

  uint32_t i = 0;

  // Iterate the main loop floor((k-8)/56) times
  for (; i < (k - 8) / 56; i++) {
    // Load input samples reversed in polynomial
    uint8x8_t xv = vld1_u8(src + i * 7);
    xv = vrev64_u8(xv);
    poly64_t x = vget_lane_p64(vreinterpret_p64_u8(xv), 0);

    // Compute outputs (skip the first byte)
    uint64x2_t y0_temp = vreinterpretq_u64_p128(vmull_p64(x, pmask0));
    uint64x2_t y1_temp = vreinterpretq_u64_p128(vmull_p64(x, pmask1));
    uint64x2_t y2_temp = vreinterpretq_u64_p128(vmull_p64(x, pmask2));

    // Delete the 6 bits tail
    y0_temp = vshrq_n_u64(y0_temp, 6);
    y1_temp = vshrq_n_u64(y1_temp, 6);
    y2_temp = vshrq_n_u64(y2_temp, 6);

    // Reverse again
    uint8x16_t y0 = vreinterpretq_u8_u64(y0_temp);
    uint8x16_t y1 = vreinterpretq_u8_u64(y1_temp);
    uint8x16_t y2 = vreinterpretq_u8_u64(y2_temp);
    y0 = vrev64q_u8(y0);
    y1 = vrev64q_u8(y1);
    y2 = vrev64q_u8(y2);

    // Store 7 bytes in the output arrays
    const uint8_t *y0_ptr = (uint8_t *)&y0;
    const uint8_t *y1_ptr = (uint8_t *)&y1;
    const uint8_t *y2_ptr = (uint8_t *)&y2;
    memcpy(dst0 + 1 + i * 7, y0_ptr + 1, 8 - 1);
    memcpy(dst1 + 1 + i * 7, y1_ptr + 1, 8 - 1);
    memcpy(dst2 + 1 + i * 7, y2_ptr + 1, 8 - 1);
  }

  int rem = k - 56 * i; // Remaining bits in the input

  // Tail
  if ((k - 8) % 56 != 0) {
    // Load input samples reversed in polynomial
    uint8x8_t xv;
    memcpy(&xv, src + i * 7, (rem + 7) / 8);
    xv = vrev64_u8(xv);
    poly64_t x = vget_lane_p64(vreinterpret_p64_u8(xv), 0);

    // Compute outputs (skip the first byte)
    uint64x2_t y0_temp = vreinterpretq_u64_p128(vmull_p64(x, pmask0));
    uint64x2_t y1_temp = vreinterpretq_u64_p128(vmull_p64(x, pmask1));
    uint64x2_t y2_temp = vreinterpretq_u64_p128(vmull_p64(x, pmask2));

    // Delete zeros (64 - rem) and tail (6)
    y0_temp = vshlq_u64(y0_temp, vdupq_n_s64(-(64 - rem + 6)));
    y1_temp = vshlq_u64(y1_temp, vdupq_n_s64(-(64 - rem + 6)));
    y2_temp = vshlq_u64(y2_temp, vdupq_n_s64(-(64 - rem + 6)));

    // zero padding on the right
    y0_temp = vshlq_u64(y0_temp, vdupq_n_s64(7 - ((k - 1) % 8)));
    y1_temp = vshlq_u64(y1_temp, vdupq_n_s64(7 - ((k - 1) % 8)));
    y2_temp = vshlq_u64(y2_temp, vdupq_n_s64(7 - ((k - 1) % 8)));

    // Reverse again
    uint8x16_t y0 = vreinterpretq_u8_u64(y0_temp);
    uint8x16_t y1 = vreinterpretq_u8_u64(y1_temp);
    uint8x16_t y2 = vreinterpretq_u8_u64(y2_temp);
    y0 = vrev64q_u8(y0);
    y1 = vrev64q_u8(y1);
    y2 = vrev64q_u8(y2);

    // Store outputs
    const uint8_t *y0_ptr = (uint8_t *)&y0;
    const uint8_t *y1_ptr = (uint8_t *)&y1;
    const uint8_t *y2_ptr = (uint8_t *)&y2;
    memcpy(dst0 + 1 + i * 7, y0_ptr + (8 - (rem + 7) / 8 + 1),
           (rem + 7) / 8 - 1);
    memcpy(dst1 + 1 + i * 7, y1_ptr + (8 - (rem + 7) / 8 + 1),
           (rem + 7) / 8 - 1);
    memcpy(dst2 + 1 + i * 7, y2_ptr + (8 - (rem + 7) / 8 + 1),
           (rem + 7) / 8 - 1);
  }

  // Tail biting part (first 6 bits, i.e. the first byte)
  uint8_t xpr0 = src[0]; // x[0] ... x[7]
  uint8_t x_end;         // x[k-8] ... x[k-1]
  if (k % 8 != 0) {
    uint8_t x_end1 = src[(k + 7) / 8 - 1];
    uint8_t x_end2 = src[(k + 7) / 8 - 2];
    x_end = (x_end2 << k % 8) | (x_end1 >> (8 - k % 8));
  } else {
    x_end = src[(k + 7) / 8 - 1];
  }

  // When the first 6 output samples are computed, in the shift register
  // there are elements of the input tail (x[k-1] ... x[k-6]). For example,
  // y0[0] = x[0] + s[1] + s[2] + s[4] + s[5] =
  //       = x[0] + x[k-2] + x[k-3] + x[k-5] + x[k-6].
  // We can compute these output samples in parallel, loading and rearranging
  // the right elements in uint8x8_t types (we ignore the last 2 elements)

  uint8_t xpr1 = (xpr0 >> 1) | (x_end << 7); // x[k-1] x[0] ... x[6]
  uint8_t xpr2 = (xpr0 >> 2) | (x_end << 6); // x[k-2] x[k-1] x[0] ... x[5]
  uint8_t xpr3 = (xpr0 >> 3) | (x_end << 5); // x[k-3] ... x[4]
  uint8_t xpr4 = (xpr0 >> 4) | (x_end << 4); // x[k-4] ... x[3]
  uint8_t xpr5 = (xpr0 >> 5) | (x_end << 3); // x[k-5] ... x[2]
  uint8_t xpr6 = (xpr0 >> 6) | (x_end << 2); // x[k-6] ... x[1]

  // Compute the partial sums
  uint8_t xpr06 = xpr0 ^ xpr6; // x[0] + x[k-6]
  uint8_t xpr23 = xpr2 ^ xpr3; // x[k-2] + x[k-3]
  uint8_t xpr12 = xpr1 ^ xpr2; // x[k-1] + x[k-2]

  uint8_t xpr0236 = xpr06 ^ xpr23; // x[0] + x[k-2] + x[k-3] + x[k-6]
  uint8_t xpr0126 = xpr06 ^ xpr12; // x[0] + x[k-1] + x[k-2] + x[k-6]

  // Compute the final results
  dst0[0] = xpr0236 ^ xpr5; // x[0] + x[k-2] + x[k-3] + x[k-5] + x[k-6]
  dst1[0] = xpr0236 ^ xpr1; // x[0] + x[k-1] + x[k-2] + x[k-3] + x[k-6]
  dst2[0] = xpr0126 ^ xpr4; // x[0] + x[k-1] + x[k-2] + x[k-4] + x[k-6]

  return ARMRAL_SUCCESS;
}
