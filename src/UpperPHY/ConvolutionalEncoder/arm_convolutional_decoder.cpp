/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "utils/allocators.hpp"
#include "utils/bits_to_bytes.hpp"

#include "convolutional_code_table.hpp"

#include <cstdlib>
#include <cstring>

namespace {

struct pm_s {
  uint32_t pm;
  uint8_t i;
};

void compute_path(uint8_t *dec, uint32_t k, uint8_t states, uint8_t const *prev,
                  uint8_t *i_ptr) {
  // Final state index
  uint8_t i = *i_ptr;

  // Compute path and decoded stream
  for (uint32_t j = k; j > 0; j--) {
    // For the states belonging to the first half ([0; 31]) the decoded bit is
    // 0, for the other ones ([32; 63]) it is 1
    if (i < 32) {
      dec[j - 1] = 0;
    } else {
      dec[j - 1] = 1;
    }

    i = prev[(j - 1) * states + i];
  }

  // Initial state index
  *i_ptr = i;
}

int cmp(const void *a, const void *b) {
  int ret;
  const pm_s ia = *static_cast<const pm_s *>(a);
  const pm_s ib = *static_cast<const pm_s *>(b);

  if (ia.pm < ib.pm) {
    ret = -1;
  } else {
    ret = 1;
  }

  return ret;
}

template<typename Allocator>
armral_status tail_biting_convolutional_decode_block(
    const int8_t *__restrict src0, const int8_t *__restrict src1,
    const int8_t *__restrict src2, uint32_t k, uint32_t iter_max, uint8_t *dst,
    Allocator &allocator) {
  constexpr uint8_t states = 64; // 6 memory bits => 2^6 = 64 states

  auto initial_sm = allocate_zeroed<int32_t>(allocator, states);
  auto intermediate_sm = allocate_zeroed<int32_t>(allocator, states);
  auto final_sm = allocate_uninitialized<int32_t>(allocator, states);
  auto initial_states_i = allocate_uninitialized<uint8_t>(allocator, states);
  auto pm_v = allocate_uninitialized<pm_s>(allocator, states);
  // [states x K] matrix (row-major)
  auto bytes_dst = allocate_uninitialized<uint8_t>(allocator, states * k);
  // [K x states] matrix
  auto prev = allocate_zeroed<uint8_t>(allocator, k * states);

  if constexpr (Allocator::is_counting) {
    return ARMRAL_SUCCESS;
  }

  uint8_t ro_best_i;
  uint8_t ro_tb_best_i = states; // Initialized with impossible value

  uint8_t iter_cnt = 0;
  uint32x4_t preva_init = {0, 2, 4, 6};
  uint32x4_t prevb_init = {1, 3, 5, 7};
  uint32x4_t all_8s = vdupq_n_u32(8);
  int16x8_t all_765s = vdupq_n_s16(765);

  // Start WAVA
  do {
    iter_cnt++;

    // == Compute branch and state metrics ==
    for (uint32_t i = 0; i < k; i++) {
      // Given a state of the trellis (j), the two previous ones are always j <<
      // 1 (preva) and j << 1 + 1 (prevb). In the inner loop we iterate over all
      // the states in order, from 0 to states - 1, hence the preva states will
      // be 0, 2, 4... and the prevb states will be 1, 3, 5... In this
      // implementation we consider 16 states at a time.
      uint32x4_t preva = preva_init;
      uint32x4_t prevb = prevb_init;

      int8x16_t s0 = vdupq_n_s8(src0[i]);
      int8x16_t s1 = vdupq_n_s8(src1[i]);
      int8x16_t s2 = vdupq_n_s8(src2[i]);

      // In memory we stored the codewords for each state (table0 contains the
      // possible codewords when x[k]=0, table1 when x[k] = 1, they correspond
      // to the first and the second half of the trellis, since the first
      // element in the state is x[k] of the previous stage). Only the codewords
      // for the even states have been stored (the codewords of the odd states
      // can be computed as the inverse of the codeword of the previous odd
      // state). This is why this loop goes from 0 to states / 2.
      for (uint8_t j = 0; j < states / 2; j += 16) {

        // Compute table0 branch metrics (bma and bmb)

        int8x16_t t00 = vld1q_s8(table0_0 + j);
        int8x16_t t01 = vld1q_s8(table0_1 + j);
        int8x16_t t02 = vld1q_s8(table0_2 + j);
        // bma = abs(s0 - t0)
        int16x8_t bma0_lo = vdupq_n_s16(0);
        int16x8_t bma0_hi = vdupq_n_s16(0);
        bma0_lo = vabal_s8(bma0_lo, vget_low_s8(s0), vget_low_s8(t00));
        bma0_hi = vabal_s8(bma0_hi, vget_high_s8(s0), vget_high_s8(t00));
        // bma += abs(s1 - t1)
        bma0_lo = vabal_s8(bma0_lo, vget_low_s8(s1), vget_low_s8(t01));
        bma0_hi = vabal_s8(bma0_hi, vget_high_s8(s1), vget_high_s8(t01));
        // bma += abs(s2 - t2)
        bma0_lo = vabal_s8(bma0_lo, vget_low_s8(s2), vget_low_s8(t02));
        bma0_hi = vabal_s8(bma0_hi, vget_high_s8(s2), vget_high_s8(t02));
        // The branch metric for the prevb state is computed as 3 - the branch
        // metric of the preva state (Q format, so bmb = 765 - bma)
        int16x8_t bmb0_lo = vsubq_s16(all_765s, bma0_lo);
        int16x8_t bmb0_hi = vsubq_s16(all_765s, bma0_hi);

        // Compute table1 branch metrics (bma and bmb)

        int8x16_t t10 = vld1q_s8(table1_0 + j);
        int8x16_t t11 = vld1q_s8(table1_1 + j);
        int8x16_t t12 = vld1q_s8(table1_2 + j);
        int16x8_t bma1_lo = vdupq_n_s16(0);
        int16x8_t bma1_hi = vdupq_n_s16(0);
        bma1_lo = vabal_s8(bma1_lo, vget_low_s8(s0), vget_low_s8(t10));
        bma1_hi = vabal_s8(bma1_hi, vget_high_s8(s0), vget_high_s8(t10));
        bma1_lo = vabal_s8(bma1_lo, vget_low_s8(s1), vget_low_s8(t11));
        bma1_hi = vabal_s8(bma1_hi, vget_high_s8(s1), vget_high_s8(t11));
        bma1_lo = vabal_s8(bma1_lo, vget_low_s8(s2), vget_low_s8(t12));
        bma1_hi = vabal_s8(bma1_hi, vget_high_s8(s2), vget_high_s8(t12));
        int16x8_t bmb1_lo = vsubq_s16(all_765s, bma1_lo);
        int16x8_t bmb1_hi = vsubq_s16(all_765s, bma1_hi);

        // Compute table0 state metrics and previous states matrix

        // Possible previous states:
        // prev_state_a = 0, 2, 4, ...
        // prev_state_b = 1, 3, 5, ...
        // Load intermediate_sm[prev_state_a] and intermediate_sm[prev_state_b]
        int32x4x2_t int_sm = vld2q_s32(&intermediate_sm[2 * j]);
        // intermediate_sm[prev_state_a] + bma
        int32x4_t int_bma0 = vaddw_s16(int_sm.val[0], vget_low_s16(bma0_lo));
        // intermediate_sm[prev_state_b] + bmb
        int32x4_t int_bmb0 = vaddw_s16(int_sm.val[1], vget_low_s16(bmb0_lo));
        // if (intermediate_sm[prev_state_a] + bm_a >
        //     intermediate_sm[prev_state_b] + bm_b)
        //   prev[j][i] = prev_state_a;
        //   final_sm[j] = intermediate_sm[prev_state_a] + bm_a;
        uint32x4_t pred = vcleq_s32(int_bma0, int_bmb0);
        int32x4_t finalsm = vmaxq_s32(int_bmb0, int_bma0);
        vst1q_s32(&final_sm[j], finalsm);
        uint32x4_t prevab0ll = vbslq_u32(pred, prevb, preva);

        // Compute table1 state metrics and previous states matrix

        int32x4_t int_bma1 = vaddw_s16(int_sm.val[0], vget_low_s16(bma1_lo));
        int32x4_t int_bmb1 = vaddw_s16(int_sm.val[1], vget_low_s16(bmb1_lo));
        pred = vcleq_s32(int_bma1, int_bmb1);
        finalsm = vmaxq_s32(int_bmb1, int_bma1);
        vst1q_s32(&final_sm[j + states / 2], finalsm);
        uint32x4_t prevab1ll = vbslq_u32(pred, prevb, preva);

        // Update previous state vectors (the two vectors contain four
        // consecutive even values and four consecutive odd values, hence we can
        // add 8 to each element to obtain the next states in the sequence, e.g.
        // [0, 2, 4, 6] -> [8, 10, 12, 14])
        preva = vaddq_u32(preva, all_8s);
        prevb = vaddq_u32(prevb, all_8s);

        int_sm = vld2q_s32(&intermediate_sm[8 + 2 * j]);

        int_bma0 = vaddw_s16(int_sm.val[0], vget_high_s16(bma0_lo));
        int_bmb0 = vaddw_s16(int_sm.val[1], vget_high_s16(bmb0_lo));
        pred = vcleq_s32(int_bma0, int_bmb0);
        finalsm = vmaxq_s32(int_bmb0, int_bma0);
        vst1q_s32(&final_sm[4 + j], finalsm);
        uint32x4_t prevab0lh = vbslq_u32(pred, prevb, preva);

        int_bma1 = vaddw_s16(int_sm.val[0], vget_high_s16(bma1_lo));
        int_bmb1 = vaddw_s16(int_sm.val[1], vget_high_s16(bmb1_lo));
        pred = vcleq_s32(int_bma1, int_bmb1);
        finalsm = vmaxq_s32(int_bmb1, int_bma1);
        vst1q_s32(&final_sm[4 + j + states / 2], finalsm);
        uint32x4_t prevab1lh = vbslq_u32(pred, prevb, preva);

        preva = vaddq_u32(preva, all_8s);
        prevb = vaddq_u32(prevb, all_8s);

        uint16x8_t prevab0l =
            vcombine_u16(vmovn_u32(prevab0ll), vmovn_u32(prevab0lh));
        uint16x8_t prevab1l =
            vcombine_u16(vmovn_u32(prevab1ll), vmovn_u32(prevab1lh));

        int_sm = vld2q_s32(&intermediate_sm[16 + 2 * j]);

        int_bma0 = vaddw_s16(int_sm.val[0], vget_low_s16(bma0_hi));
        int_bmb0 = vaddw_s16(int_sm.val[1], vget_low_s16(bmb0_hi));
        pred = vcleq_s32(int_bma0, int_bmb0);
        finalsm = vmaxq_s32(int_bmb0, int_bma0);
        vst1q_s32(&final_sm[8 + j], finalsm);
        uint32x4_t prevab0hl = vbslq_u32(pred, prevb, preva);

        int_bma1 = vaddw_s16(int_sm.val[0], vget_low_s16(bma1_hi));
        int_bmb1 = vaddw_s16(int_sm.val[1], vget_low_s16(bmb1_hi));
        pred = vcleq_s32(int_bma1, int_bmb1);
        finalsm = vmaxq_s32(int_bmb1, int_bma1);
        vst1q_s32(&final_sm[8 + j + states / 2], finalsm);
        uint32x4_t prevab1hl = vbslq_u32(pred, prevb, preva);

        preva = vaddq_u32(preva, all_8s);
        prevb = vaddq_u32(prevb, all_8s);

        int_sm = vld2q_s32(&intermediate_sm[24 + 2 * j]);
        int_bma0 = vaddw_s16(int_sm.val[0], vget_high_s16(bma0_hi));
        int_bmb0 = vaddw_s16(int_sm.val[1], vget_high_s16(bmb0_hi));
        pred = vcleq_s32(int_bma0, int_bmb0);
        finalsm = vmaxq_s32(int_bmb0, int_bma0);
        vst1q_s32(&final_sm[12 + j], finalsm);
        uint32x4_t prevab0hh = vbslq_u32(pred, prevb, preva);

        int_bma1 = vaddw_s16(int_sm.val[0], vget_high_s16(bma1_hi));
        int_bmb1 = vaddw_s16(int_sm.val[1], vget_high_s16(bmb1_hi));
        pred = vcleq_s32(int_bma1, int_bmb1);
        finalsm = vmaxq_s32(int_bmb1, int_bma1);
        vst1q_s32(&final_sm[12 + j + states / 2], finalsm);
        uint32x4_t prevab1hh = vbslq_u32(pred, prevb, preva);

        uint16x8_t prevab0h =
            vcombine_u16(vmovn_u32(prevab0hl), vmovn_u32(prevab0hh));
        uint16x8_t prevab1h =
            vcombine_u16(vmovn_u32(prevab1hl), vmovn_u32(prevab1hh));

        // Finally store the previous values
        uint8x16_t prevab0 =
            vcombine_u8(vmovn_u16(prevab0l), vmovn_u16(prevab0h));
        vst1q_u8(&prev[i * states + j], prevab0);
        uint8x16_t prevab1 =
            vcombine_u8(vmovn_u16(prevab1l), vmovn_u16(prevab1h));
        vst1q_u8(&prev[i * states + j + states / 2], prevab1);

        preva = vaddq_u32(preva, all_8s);
        prevb = vaddq_u32(prevb, all_8s);
      }

      // Update previous state metrics array
      memcpy(intermediate_sm.get(), final_sm.get(), states * sizeof(uint32_t));
    }

    // == Traceback ==
    // Compute paths (path metrics and decoded stream) and initial states (state
    // metrics and indices)
    for (uint8_t i = 0; i < states; i++) {
      uint8_t state_i = i;
      compute_path(&bytes_dst[i * k], k, states, (uint8_t const *)prev.get(),
                   &state_i);
      pm_v[i].pm = final_sm[i] - initial_sm[state_i];
      initial_states_i[i] = state_i;
    }

    // Sort path metrics array and keep track of indices
    for (uint8_t i = 0; i < states; i++) {
      pm_v[i].i = i;
    }
    qsort(pm_v.get(), states, sizeof(pm_s), cmp);

    // This is the best path (is it the codeword?)
    ro_best_i = pm_v[0].i;

    // If the best path is also tailbiting (final state = initial state), it is
    // the codeword, so exit from the while loop
    if (initial_states_i[pm_v[0].i] == ro_best_i) {
      ro_tb_best_i = ro_best_i;
      break;
    }

    // Codeword not found immediately, another iteration is needed
    memcpy(initial_sm.get(), final_sm.get(), states * sizeof(uint32_t));
    memcpy(intermediate_sm.get(), final_sm.get(), states * sizeof(uint32_t));

    // Look for best TB path
    for (uint8_t i = 1; i < states; i++) {
      if (initial_states_i[pm_v[i].i] == pm_v[i].i) {
        ro_tb_best_i = pm_v[i].i;
        break; // Found, exit from for loop (but stay in while loop for another
               // WAVA iteration)
      }
    }
  } while (iter_cnt < iter_max);

  // == Output decoded stream ==
  // Convert the bytes back to bits
  if (ro_tb_best_i != states) { // if TB path found
    armral::bytes_to_bits(k, &bytes_dst[ro_tb_best_i * k], dst);
  } else {
    armral::bytes_to_bits(k, &bytes_dst[ro_best_i * k], dst);
  }

  return ARMRAL_SUCCESS;
}

} // anonymous namespace

armral_status armral_tail_biting_convolutional_decode_block(
    const int8_t *__restrict src0, const int8_t *__restrict src1,
    const int8_t *__restrict src2, uint32_t k, uint32_t iter_max,
    uint8_t *dst) {
  heap_allocator allocator{};
  return tail_biting_convolutional_decode_block(src0, src1, src2, k, iter_max,
                                                dst, allocator);
}

armral_status armral_tail_biting_convolutional_decode_block_noalloc(
    const int8_t *__restrict src0, const int8_t *__restrict src1,
    const int8_t *__restrict src2, uint32_t k, uint32_t iter_max, uint8_t *dst,
    void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return tail_biting_convolutional_decode_block(src0, src1, src2, k, iter_max,
                                                dst, allocator);
}

uint32_t armral_tail_biting_convolutional_decode_block_noalloc_buffer_size(
    uint32_t k, uint32_t iter_max) {
  counting_allocator allocator{};
  tail_biting_convolutional_decode_block(nullptr, nullptr, nullptr, k, iter_max,
                                         nullptr, allocator);
  return allocator.required_bytes();
}
