/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

namespace {

// Codewords when x[k] = 0 (LLR = -1) for each state (x[k-1], ..., x[k-6])

// table0_0 contains the first bit of the codeword, table0_1 the second one and
// table0_2 the third. We use 127 and -128 instead of 1 and 0 in order to
// compute the branch metrics more easily (e.g. the first codeword is [0, 0,
// 0]). Only the codewords for even states have been stored, since the codeword
// for state n + 1 (n = 0, 2, 4,...) is the inverse of the codeword for state n
// (eg. 101 -> 010) and the Hamming distance related to state n + 1 can be
// computed as 3 minus the Hamming distance of state n.

constexpr int8_t table0_0[32] = {-128, 127,  -128, 127,  127,  -128, 127,  -128,
                                 127,  -128, 127,  -128, -128, 127,  -128, 127,
                                 -128, 127,  -128, 127,  127,  -128, 127,  -128,
                                 127,  -128, 127,  -128, -128, 127,  -128, 127};

constexpr int8_t table0_1[32] = {-128, -128, -128, -128, 127,  127,  127,  127,
                                 127,  127,  127,  127,  -128, -128, -128, -128,
                                 127,  127,  127,  127,  -128, -128, -128, -128,
                                 -128, -128, -128, -128, 127,  127,  127,  127};

constexpr int8_t table0_2[32] = {-128, -128, 127,  127,  -128, -128, 127,  127,
                                 127,  127,  -128, -128, 127,  127,  -128, -128,
                                 127,  127,  -128, -128, 127,  127,  -128, -128,
                                 -128, -128, 127,  127,  -128, -128, 127,  127};

// Codewords when x[k] = 1

constexpr int8_t table1_0[32] = {
    127,  -128, 127,  -128, -128, 127, -128, 127,  -128, 127,  -128,
    127,  127,  -128, 127,  -128, 127, -128, 127,  -128, -128, 127,
    -128, 127,  -128, 127,  -128, 127, 127,  -128, 127,  -128};

constexpr int8_t table1_1[32] = {
    127,  127, 127, 127, -128, -128, -128, -128, -128, -128, -128,
    -128, 127, 127, 127, 127,  -128, -128, -128, -128, 127,  127,
    127,  127, 127, 127, 127,  127,  -128, -128, -128, -128};

constexpr int8_t table1_2[32] = {
    127, 127,  -128, -128, 127,  127,  -128, -128, -128, -128, 127,
    127, -128, -128, 127,  127,  -128, -128, 127,  127,  -128, -128,
    127, 127,  127,  127,  -128, -128, 127,  127,  -128, -128};

} // anonymous namespace
