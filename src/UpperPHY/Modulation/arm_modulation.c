/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"
#include <string.h>

#ifdef ARMRAL_ARCH_SVE
#include <arm_sve.h>
#endif

/* Definition of the constellation map according to 3GPP specification.
 * Gray encoding is used and
 * 0x16A1 = sqrt(2)/2 in Q2.13
 */
static const armral_cmplx_int16_t constellation_qpsk[4] = {
    {0x16A1, 0x16A1}, {0x16A1, -0x16A1}, {-0x16A1, 0x16A1}, {-0x16A1, -0x16A1}};

void armral_qpsk_modulation(uint32_t nbits, const uint8_t *p_src,
                            armral_cmplx_int16_t *p_dst) {
  /* Compute the number of blocks of 2 bits in the new tail */
  uint32_t final_blck = (nbits >> 1U) & 3;

#ifdef ARMRAL_ARCH_SVE
  svint16_t svqpsk_pos = svdup_n_s16(0x16A1);
  svint16_t svqpsk_neg = svdup_n_s16(-0x16A1);
  // shuffle to map flip of bit order within bytes of input (there is no
  // instruction for doing this on predicates directly, so we do it on the
  // result instead).
  svuint64_t shuf0_64 =
      svindex_u64(0x0001020304050607ULL, 0x0808080808080808ULL);
  svuint16_t shuf0 = svreinterpret_u16_u8(
      svzip1_u8(svreinterpret_u8_u64(shuf0_64), svdup_n_u8(0)));
  while (nbits > svcntb()) {
    // load predicate as one bit per 8-bit element, then unpack into one bit
    // per 16-bit element and use to select result.
    svbool_t in = svldr_b(p_src);
    svbool_t in_lo = svunpklo_b(in);
    svbool_t in_hi = svunpkhi_b(in);
    p_src += svcntd();
    svint16_t vals_lo = svsel_s16(in_lo, svqpsk_neg, svqpsk_pos);
    svint16_t vals_hi = svsel_s16(in_hi, svqpsk_neg, svqpsk_pos);
    svint16_t out_lo = svtbl_s16(vals_lo, shuf0);
    svint16_t out_hi = svtbl_s16(vals_hi, shuf0);
    svst1_s16(svptrue_b16(), (int16_t *)p_dst, out_lo);
    svst1_vnum_s16(svptrue_b16(), (int16_t *)p_dst, 1, out_hi);
    p_dst += svcnth();
    nbits -= svcntb();
  }

  const uint32_t bytes = nbits >> 3U;
  const uint32_t vl = svcntw() / 4;
  const uint32_t unrolls = bytes / vl;
  const svbool_t pred = svwhilelt_b16(0, (int32_t)vl);
  const svuint16_t linear_series = svindex_u16(0, 1);
  const svuint16_t mask =
      svdupq_u16(0x80, 0x40, 0x20, 0x10, 0x8, 0x4, 0x2, 0x1);
  svuint16_t index = svzip1(linear_series, linear_series);
  index = svzip1(index, index);
  index = svzip1(index, index);
  const svbool_t ptrue_b16 = svptrue_b16();
  for (uint32_t i = 0; i < unrolls; i++) {
    svuint16_t src_bytes = svld1ub_u16(pred, p_src);
    p_src += vl;
    svuint16_t tbl = svtbl_u16(src_bytes, index);
    svbool_t mask_pred =
        svcmpgt_n_u16(ptrue_b16, svand_u16_x(ptrue_b16, tbl, mask), 0);
    svint16_t points = svsel_s16(mask_pred, svqpsk_neg, svqpsk_pos);
    svst1_s16(ptrue_b16, (int16_t *)p_dst, points);
    p_dst += vl * 4;
  }

  const int32_t leftover_bytes = bytes - (unrolls * vl);
  const svbool_t load_lanes = svwhilelt_b16(0, leftover_bytes);
  const svbool_t store_lanes = svwhilelt_b16(0, leftover_bytes * 8);
  const uint32_t active_store_lanes = leftover_bytes * 8;
  if (leftover_bytes) {
    svuint16_t src_bytes = svld1ub_u16(load_lanes, p_src);
    p_src += leftover_bytes;
    svuint16_t tbl = svtbl_u16(src_bytes, index);
    svbool_t mask_pred =
        svcmpgt_n_u16(ptrue_b16, svand_u16_x(ptrue_b16, tbl, mask), 0);
    svint16_t points = svsel_s16(mask_pred, svqpsk_neg, svqpsk_pos);
    svst1_s16(store_lanes, (int16_t *)p_dst, points);
    p_dst += active_store_lanes / 2;
  }
#else
  /* Compute the number of 4-byte blocks */
  uint32_t fourbytes = nbits >> 5U;
  /* Compute number of blocks of 2 bytes in the tail */
  uint32_t twobytes_in_tail = (nbits >> 4U) & 1;
  /* Compute the number of blocks of 1 byte in the new tail */
  uint32_t bytes_in_tail = (nbits >> 3U) & 1;
  /* Process a possible tail or an input with less than 8 int */
  uint32_t blk_cnt = fourbytes;

  /* Useful masks and constants */
  const uint16x8_t bit_mask = {0x80, 0x40, 0x20, 0x10, 0x8, 0x4, 0x2, 0x1};
  const int16x8_t qpsk_pos = vdupq_n_s16(0x16A1);
  const int16x8_t qpsk_neg = vdupq_n_s16(-0x16A1);

  while (blk_cnt > 0U) {

    uint16x8_t sample0 = vreinterpretq_u16_u8(vld1q_dup_u8(p_src++));
    uint16x8_t sample1 = vreinterpretq_u16_u8(vld1q_dup_u8(p_src++));
    uint16x8_t sample2 = vreinterpretq_u16_u8(vld1q_dup_u8(p_src++));
    uint16x8_t sample3 = vreinterpretq_u16_u8(vld1q_dup_u8(p_src++));

    uint16x8_t mask0 = vtstq_u16(sample0, bit_mask);
    uint16x8_t mask1 = vtstq_u16(sample1, bit_mask);
    uint16x8_t mask2 = vtstq_u16(sample2, bit_mask);
    uint16x8_t mask3 = vtstq_u16(sample3, bit_mask);

    int16x8_t points0 = vbslq_s16(mask0, qpsk_neg, qpsk_pos);
    int16x8_t points1 = vbslq_s16(mask1, qpsk_neg, qpsk_pos);
    int16x8_t points2 = vbslq_s16(mask2, qpsk_neg, qpsk_pos);
    int16x8_t points3 = vbslq_s16(mask3, qpsk_neg, qpsk_pos);

    vst1q_s16((int16_t *)p_dst, points0);
    p_dst += 4;
    vst1q_s16((int16_t *)p_dst, points1);
    p_dst += 4;
    vst1q_s16((int16_t *)p_dst, points2);
    p_dst += 4;
    vst1q_s16((int16_t *)p_dst, points3);
    p_dst += 4;

    blk_cnt--;
  }

  blk_cnt = twobytes_in_tail;

  while (blk_cnt > 0U) {

    uint16x8_t sample0 = vreinterpretq_u16_u8(vld1q_dup_u8(p_src++));
    uint16x8_t sample1 = vreinterpretq_u16_u8(vld1q_dup_u8(p_src++));

    uint16x8_t mask0 = vtstq_u16(sample0, bit_mask);
    uint16x8_t mask1 = vtstq_u16(sample1, bit_mask);

    int16x8_t points0 = vbslq_s16(mask0, qpsk_neg, qpsk_pos);
    int16x8_t points1 = vbslq_s16(mask1, qpsk_neg, qpsk_pos);

    vst1q_s16((int16_t *)p_dst, points0);
    p_dst += 4;
    vst1q_s16((int16_t *)p_dst, points1);
    p_dst += 4;

    blk_cnt--;
  }

  blk_cnt = bytes_in_tail;

  while (blk_cnt > 0U) {

    uint16x8_t sample = vreinterpretq_u16_u8(vld1q_dup_u8(p_src++));

    uint16x8_t mask = vtstq_u16(sample, bit_mask);

    int16x8_t points = vbslq_s16(mask, qpsk_neg, qpsk_pos);

    vst1q_s16((int16_t *)p_dst, points);
    p_dst += 4;

    blk_cnt--;
  }
#endif
  /* There might be a tail having less than 8 bits */
  if (final_blck) {
    uint8_t sample = *p_src;
    uint8_t index0 = sample >> 6;
    *p_dst++ = constellation_qpsk[index0];
    final_blck--;

    /* Another possible sample on 2 bits might be present */
    if (final_blck) {
      uint8_t index1 = (sample >> 4) & 0x3;
      *p_dst++ = constellation_qpsk[index1];
      final_blck--;

      /* The very last sample on 2 bits, if needed */
      if (final_blck) {
        uint8_t index2 = (sample >> 2) & 0x3;
        *p_dst++ = constellation_qpsk[index2];
      }
    }
  }
}

/* Definition of the constellation map according to 3GPP specification.
 * Gray encoding is used and
 * 0xA1F  = 1 * sqrt(10)/10 in Q2.13
 * 0x1E5C = 3 * sqrt(10)/10 in Q2.13
 */
static const armral_cmplx_int16_t constellation_16qam[16] = {
    {0xA1F, 0xA1F},   {0xA1F, 0x1E5C},   {0x1E5C, 0xA1F},   {0x1E5C, 0x1E5C},
    {0xA1F, -0xA1F},  {0xA1F, -0x1E5C},  {0x1E5C, -0xA1F},  {0x1E5C, -0x1E5C},
    {-0xA1F, 0xA1F},  {-0xA1F, 0x1E5C},  {-0x1E5C, 0xA1F},  {-0x1E5C, 0x1E5C},
    {-0xA1F, -0xA1F}, {-0xA1F, -0x1E5C}, {-0x1E5C, -0xA1F}, {-0x1E5C, -0x1E5C}};

/* An outer-product version to compute 2 symbols at a time using a single 8-bit
 * lookup. */
static const armral_cmplx_int16_t constellation_16qam_outer_prod[256][2] = {
    {{0XA1F, 0XA1F}, {0XA1F, 0XA1F}},
    {{0XA1F, 0XA1F}, {0XA1F, 0X1E5C}},
    {{0XA1F, 0XA1F}, {0X1E5C, 0XA1F}},
    {{0XA1F, 0XA1F}, {0X1E5C, 0X1E5C}},
    {{0XA1F, 0XA1F}, {0XA1F, -0XA1F}},
    {{0XA1F, 0XA1F}, {0XA1F, -0X1E5C}},
    {{0XA1F, 0XA1F}, {0X1E5C, -0XA1F}},
    {{0XA1F, 0XA1F}, {0X1E5C, -0X1E5C}},
    {{0XA1F, 0XA1F}, {-0XA1F, 0XA1F}},
    {{0XA1F, 0XA1F}, {-0XA1F, 0X1E5C}},
    {{0XA1F, 0XA1F}, {-0X1E5C, 0XA1F}},
    {{0XA1F, 0XA1F}, {-0X1E5C, 0X1E5C}},
    {{0XA1F, 0XA1F}, {-0XA1F, -0XA1F}},
    {{0XA1F, 0XA1F}, {-0XA1F, -0X1E5C}},
    {{0XA1F, 0XA1F}, {-0X1E5C, -0XA1F}},
    {{0XA1F, 0XA1F}, {-0X1E5C, -0X1E5C}},
    {{0XA1F, 0X1E5C}, {0XA1F, 0XA1F}},
    {{0XA1F, 0X1E5C}, {0XA1F, 0X1E5C}},
    {{0XA1F, 0X1E5C}, {0X1E5C, 0XA1F}},
    {{0XA1F, 0X1E5C}, {0X1E5C, 0X1E5C}},
    {{0XA1F, 0X1E5C}, {0XA1F, -0XA1F}},
    {{0XA1F, 0X1E5C}, {0XA1F, -0X1E5C}},
    {{0XA1F, 0X1E5C}, {0X1E5C, -0XA1F}},
    {{0XA1F, 0X1E5C}, {0X1E5C, -0X1E5C}},
    {{0XA1F, 0X1E5C}, {-0XA1F, 0XA1F}},
    {{0XA1F, 0X1E5C}, {-0XA1F, 0X1E5C}},
    {{0XA1F, 0X1E5C}, {-0X1E5C, 0XA1F}},
    {{0XA1F, 0X1E5C}, {-0X1E5C, 0X1E5C}},
    {{0XA1F, 0X1E5C}, {-0XA1F, -0XA1F}},
    {{0XA1F, 0X1E5C}, {-0XA1F, -0X1E5C}},
    {{0XA1F, 0X1E5C}, {-0X1E5C, -0XA1F}},
    {{0XA1F, 0X1E5C}, {-0X1E5C, -0X1E5C}},
    {{0X1E5C, 0XA1F}, {0XA1F, 0XA1F}},
    {{0X1E5C, 0XA1F}, {0XA1F, 0X1E5C}},
    {{0X1E5C, 0XA1F}, {0X1E5C, 0XA1F}},
    {{0X1E5C, 0XA1F}, {0X1E5C, 0X1E5C}},
    {{0X1E5C, 0XA1F}, {0XA1F, -0XA1F}},
    {{0X1E5C, 0XA1F}, {0XA1F, -0X1E5C}},
    {{0X1E5C, 0XA1F}, {0X1E5C, -0XA1F}},
    {{0X1E5C, 0XA1F}, {0X1E5C, -0X1E5C}},
    {{0X1E5C, 0XA1F}, {-0XA1F, 0XA1F}},
    {{0X1E5C, 0XA1F}, {-0XA1F, 0X1E5C}},
    {{0X1E5C, 0XA1F}, {-0X1E5C, 0XA1F}},
    {{0X1E5C, 0XA1F}, {-0X1E5C, 0X1E5C}},
    {{0X1E5C, 0XA1F}, {-0XA1F, -0XA1F}},
    {{0X1E5C, 0XA1F}, {-0XA1F, -0X1E5C}},
    {{0X1E5C, 0XA1F}, {-0X1E5C, -0XA1F}},
    {{0X1E5C, 0XA1F}, {-0X1E5C, -0X1E5C}},
    {{0X1E5C, 0X1E5C}, {0XA1F, 0XA1F}},
    {{0X1E5C, 0X1E5C}, {0XA1F, 0X1E5C}},
    {{0X1E5C, 0X1E5C}, {0X1E5C, 0XA1F}},
    {{0X1E5C, 0X1E5C}, {0X1E5C, 0X1E5C}},
    {{0X1E5C, 0X1E5C}, {0XA1F, -0XA1F}},
    {{0X1E5C, 0X1E5C}, {0XA1F, -0X1E5C}},
    {{0X1E5C, 0X1E5C}, {0X1E5C, -0XA1F}},
    {{0X1E5C, 0X1E5C}, {0X1E5C, -0X1E5C}},
    {{0X1E5C, 0X1E5C}, {-0XA1F, 0XA1F}},
    {{0X1E5C, 0X1E5C}, {-0XA1F, 0X1E5C}},
    {{0X1E5C, 0X1E5C}, {-0X1E5C, 0XA1F}},
    {{0X1E5C, 0X1E5C}, {-0X1E5C, 0X1E5C}},
    {{0X1E5C, 0X1E5C}, {-0XA1F, -0XA1F}},
    {{0X1E5C, 0X1E5C}, {-0XA1F, -0X1E5C}},
    {{0X1E5C, 0X1E5C}, {-0X1E5C, -0XA1F}},
    {{0X1E5C, 0X1E5C}, {-0X1E5C, -0X1E5C}},
    {{0XA1F, -0XA1F}, {0XA1F, 0XA1F}},
    {{0XA1F, -0XA1F}, {0XA1F, 0X1E5C}},
    {{0XA1F, -0XA1F}, {0X1E5C, 0XA1F}},
    {{0XA1F, -0XA1F}, {0X1E5C, 0X1E5C}},
    {{0XA1F, -0XA1F}, {0XA1F, -0XA1F}},
    {{0XA1F, -0XA1F}, {0XA1F, -0X1E5C}},
    {{0XA1F, -0XA1F}, {0X1E5C, -0XA1F}},
    {{0XA1F, -0XA1F}, {0X1E5C, -0X1E5C}},
    {{0XA1F, -0XA1F}, {-0XA1F, 0XA1F}},
    {{0XA1F, -0XA1F}, {-0XA1F, 0X1E5C}},
    {{0XA1F, -0XA1F}, {-0X1E5C, 0XA1F}},
    {{0XA1F, -0XA1F}, {-0X1E5C, 0X1E5C}},
    {{0XA1F, -0XA1F}, {-0XA1F, -0XA1F}},
    {{0XA1F, -0XA1F}, {-0XA1F, -0X1E5C}},
    {{0XA1F, -0XA1F}, {-0X1E5C, -0XA1F}},
    {{0XA1F, -0XA1F}, {-0X1E5C, -0X1E5C}},
    {{0XA1F, -0X1E5C}, {0XA1F, 0XA1F}},
    {{0XA1F, -0X1E5C}, {0XA1F, 0X1E5C}},
    {{0XA1F, -0X1E5C}, {0X1E5C, 0XA1F}},
    {{0XA1F, -0X1E5C}, {0X1E5C, 0X1E5C}},
    {{0XA1F, -0X1E5C}, {0XA1F, -0XA1F}},
    {{0XA1F, -0X1E5C}, {0XA1F, -0X1E5C}},
    {{0XA1F, -0X1E5C}, {0X1E5C, -0XA1F}},
    {{0XA1F, -0X1E5C}, {0X1E5C, -0X1E5C}},
    {{0XA1F, -0X1E5C}, {-0XA1F, 0XA1F}},
    {{0XA1F, -0X1E5C}, {-0XA1F, 0X1E5C}},
    {{0XA1F, -0X1E5C}, {-0X1E5C, 0XA1F}},
    {{0XA1F, -0X1E5C}, {-0X1E5C, 0X1E5C}},
    {{0XA1F, -0X1E5C}, {-0XA1F, -0XA1F}},
    {{0XA1F, -0X1E5C}, {-0XA1F, -0X1E5C}},
    {{0XA1F, -0X1E5C}, {-0X1E5C, -0XA1F}},
    {{0XA1F, -0X1E5C}, {-0X1E5C, -0X1E5C}},
    {{0X1E5C, -0XA1F}, {0XA1F, 0XA1F}},
    {{0X1E5C, -0XA1F}, {0XA1F, 0X1E5C}},
    {{0X1E5C, -0XA1F}, {0X1E5C, 0XA1F}},
    {{0X1E5C, -0XA1F}, {0X1E5C, 0X1E5C}},
    {{0X1E5C, -0XA1F}, {0XA1F, -0XA1F}},
    {{0X1E5C, -0XA1F}, {0XA1F, -0X1E5C}},
    {{0X1E5C, -0XA1F}, {0X1E5C, -0XA1F}},
    {{0X1E5C, -0XA1F}, {0X1E5C, -0X1E5C}},
    {{0X1E5C, -0XA1F}, {-0XA1F, 0XA1F}},
    {{0X1E5C, -0XA1F}, {-0XA1F, 0X1E5C}},
    {{0X1E5C, -0XA1F}, {-0X1E5C, 0XA1F}},
    {{0X1E5C, -0XA1F}, {-0X1E5C, 0X1E5C}},
    {{0X1E5C, -0XA1F}, {-0XA1F, -0XA1F}},
    {{0X1E5C, -0XA1F}, {-0XA1F, -0X1E5C}},
    {{0X1E5C, -0XA1F}, {-0X1E5C, -0XA1F}},
    {{0X1E5C, -0XA1F}, {-0X1E5C, -0X1E5C}},
    {{0X1E5C, -0X1E5C}, {0XA1F, 0XA1F}},
    {{0X1E5C, -0X1E5C}, {0XA1F, 0X1E5C}},
    {{0X1E5C, -0X1E5C}, {0X1E5C, 0XA1F}},
    {{0X1E5C, -0X1E5C}, {0X1E5C, 0X1E5C}},
    {{0X1E5C, -0X1E5C}, {0XA1F, -0XA1F}},
    {{0X1E5C, -0X1E5C}, {0XA1F, -0X1E5C}},
    {{0X1E5C, -0X1E5C}, {0X1E5C, -0XA1F}},
    {{0X1E5C, -0X1E5C}, {0X1E5C, -0X1E5C}},
    {{0X1E5C, -0X1E5C}, {-0XA1F, 0XA1F}},
    {{0X1E5C, -0X1E5C}, {-0XA1F, 0X1E5C}},
    {{0X1E5C, -0X1E5C}, {-0X1E5C, 0XA1F}},
    {{0X1E5C, -0X1E5C}, {-0X1E5C, 0X1E5C}},
    {{0X1E5C, -0X1E5C}, {-0XA1F, -0XA1F}},
    {{0X1E5C, -0X1E5C}, {-0XA1F, -0X1E5C}},
    {{0X1E5C, -0X1E5C}, {-0X1E5C, -0XA1F}},
    {{0X1E5C, -0X1E5C}, {-0X1E5C, -0X1E5C}},
    {{-0XA1F, 0XA1F}, {0XA1F, 0XA1F}},
    {{-0XA1F, 0XA1F}, {0XA1F, 0X1E5C}},
    {{-0XA1F, 0XA1F}, {0X1E5C, 0XA1F}},
    {{-0XA1F, 0XA1F}, {0X1E5C, 0X1E5C}},
    {{-0XA1F, 0XA1F}, {0XA1F, -0XA1F}},
    {{-0XA1F, 0XA1F}, {0XA1F, -0X1E5C}},
    {{-0XA1F, 0XA1F}, {0X1E5C, -0XA1F}},
    {{-0XA1F, 0XA1F}, {0X1E5C, -0X1E5C}},
    {{-0XA1F, 0XA1F}, {-0XA1F, 0XA1F}},
    {{-0XA1F, 0XA1F}, {-0XA1F, 0X1E5C}},
    {{-0XA1F, 0XA1F}, {-0X1E5C, 0XA1F}},
    {{-0XA1F, 0XA1F}, {-0X1E5C, 0X1E5C}},
    {{-0XA1F, 0XA1F}, {-0XA1F, -0XA1F}},
    {{-0XA1F, 0XA1F}, {-0XA1F, -0X1E5C}},
    {{-0XA1F, 0XA1F}, {-0X1E5C, -0XA1F}},
    {{-0XA1F, 0XA1F}, {-0X1E5C, -0X1E5C}},
    {{-0XA1F, 0X1E5C}, {0XA1F, 0XA1F}},
    {{-0XA1F, 0X1E5C}, {0XA1F, 0X1E5C}},
    {{-0XA1F, 0X1E5C}, {0X1E5C, 0XA1F}},
    {{-0XA1F, 0X1E5C}, {0X1E5C, 0X1E5C}},
    {{-0XA1F, 0X1E5C}, {0XA1F, -0XA1F}},
    {{-0XA1F, 0X1E5C}, {0XA1F, -0X1E5C}},
    {{-0XA1F, 0X1E5C}, {0X1E5C, -0XA1F}},
    {{-0XA1F, 0X1E5C}, {0X1E5C, -0X1E5C}},
    {{-0XA1F, 0X1E5C}, {-0XA1F, 0XA1F}},
    {{-0XA1F, 0X1E5C}, {-0XA1F, 0X1E5C}},
    {{-0XA1F, 0X1E5C}, {-0X1E5C, 0XA1F}},
    {{-0XA1F, 0X1E5C}, {-0X1E5C, 0X1E5C}},
    {{-0XA1F, 0X1E5C}, {-0XA1F, -0XA1F}},
    {{-0XA1F, 0X1E5C}, {-0XA1F, -0X1E5C}},
    {{-0XA1F, 0X1E5C}, {-0X1E5C, -0XA1F}},
    {{-0XA1F, 0X1E5C}, {-0X1E5C, -0X1E5C}},
    {{-0X1E5C, 0XA1F}, {0XA1F, 0XA1F}},
    {{-0X1E5C, 0XA1F}, {0XA1F, 0X1E5C}},
    {{-0X1E5C, 0XA1F}, {0X1E5C, 0XA1F}},
    {{-0X1E5C, 0XA1F}, {0X1E5C, 0X1E5C}},
    {{-0X1E5C, 0XA1F}, {0XA1F, -0XA1F}},
    {{-0X1E5C, 0XA1F}, {0XA1F, -0X1E5C}},
    {{-0X1E5C, 0XA1F}, {0X1E5C, -0XA1F}},
    {{-0X1E5C, 0XA1F}, {0X1E5C, -0X1E5C}},
    {{-0X1E5C, 0XA1F}, {-0XA1F, 0XA1F}},
    {{-0X1E5C, 0XA1F}, {-0XA1F, 0X1E5C}},
    {{-0X1E5C, 0XA1F}, {-0X1E5C, 0XA1F}},
    {{-0X1E5C, 0XA1F}, {-0X1E5C, 0X1E5C}},
    {{-0X1E5C, 0XA1F}, {-0XA1F, -0XA1F}},
    {{-0X1E5C, 0XA1F}, {-0XA1F, -0X1E5C}},
    {{-0X1E5C, 0XA1F}, {-0X1E5C, -0XA1F}},
    {{-0X1E5C, 0XA1F}, {-0X1E5C, -0X1E5C}},
    {{-0X1E5C, 0X1E5C}, {0XA1F, 0XA1F}},
    {{-0X1E5C, 0X1E5C}, {0XA1F, 0X1E5C}},
    {{-0X1E5C, 0X1E5C}, {0X1E5C, 0XA1F}},
    {{-0X1E5C, 0X1E5C}, {0X1E5C, 0X1E5C}},
    {{-0X1E5C, 0X1E5C}, {0XA1F, -0XA1F}},
    {{-0X1E5C, 0X1E5C}, {0XA1F, -0X1E5C}},
    {{-0X1E5C, 0X1E5C}, {0X1E5C, -0XA1F}},
    {{-0X1E5C, 0X1E5C}, {0X1E5C, -0X1E5C}},
    {{-0X1E5C, 0X1E5C}, {-0XA1F, 0XA1F}},
    {{-0X1E5C, 0X1E5C}, {-0XA1F, 0X1E5C}},
    {{-0X1E5C, 0X1E5C}, {-0X1E5C, 0XA1F}},
    {{-0X1E5C, 0X1E5C}, {-0X1E5C, 0X1E5C}},
    {{-0X1E5C, 0X1E5C}, {-0XA1F, -0XA1F}},
    {{-0X1E5C, 0X1E5C}, {-0XA1F, -0X1E5C}},
    {{-0X1E5C, 0X1E5C}, {-0X1E5C, -0XA1F}},
    {{-0X1E5C, 0X1E5C}, {-0X1E5C, -0X1E5C}},
    {{-0XA1F, -0XA1F}, {0XA1F, 0XA1F}},
    {{-0XA1F, -0XA1F}, {0XA1F, 0X1E5C}},
    {{-0XA1F, -0XA1F}, {0X1E5C, 0XA1F}},
    {{-0XA1F, -0XA1F}, {0X1E5C, 0X1E5C}},
    {{-0XA1F, -0XA1F}, {0XA1F, -0XA1F}},
    {{-0XA1F, -0XA1F}, {0XA1F, -0X1E5C}},
    {{-0XA1F, -0XA1F}, {0X1E5C, -0XA1F}},
    {{-0XA1F, -0XA1F}, {0X1E5C, -0X1E5C}},
    {{-0XA1F, -0XA1F}, {-0XA1F, 0XA1F}},
    {{-0XA1F, -0XA1F}, {-0XA1F, 0X1E5C}},
    {{-0XA1F, -0XA1F}, {-0X1E5C, 0XA1F}},
    {{-0XA1F, -0XA1F}, {-0X1E5C, 0X1E5C}},
    {{-0XA1F, -0XA1F}, {-0XA1F, -0XA1F}},
    {{-0XA1F, -0XA1F}, {-0XA1F, -0X1E5C}},
    {{-0XA1F, -0XA1F}, {-0X1E5C, -0XA1F}},
    {{-0XA1F, -0XA1F}, {-0X1E5C, -0X1E5C}},
    {{-0XA1F, -0X1E5C}, {0XA1F, 0XA1F}},
    {{-0XA1F, -0X1E5C}, {0XA1F, 0X1E5C}},
    {{-0XA1F, -0X1E5C}, {0X1E5C, 0XA1F}},
    {{-0XA1F, -0X1E5C}, {0X1E5C, 0X1E5C}},
    {{-0XA1F, -0X1E5C}, {0XA1F, -0XA1F}},
    {{-0XA1F, -0X1E5C}, {0XA1F, -0X1E5C}},
    {{-0XA1F, -0X1E5C}, {0X1E5C, -0XA1F}},
    {{-0XA1F, -0X1E5C}, {0X1E5C, -0X1E5C}},
    {{-0XA1F, -0X1E5C}, {-0XA1F, 0XA1F}},
    {{-0XA1F, -0X1E5C}, {-0XA1F, 0X1E5C}},
    {{-0XA1F, -0X1E5C}, {-0X1E5C, 0XA1F}},
    {{-0XA1F, -0X1E5C}, {-0X1E5C, 0X1E5C}},
    {{-0XA1F, -0X1E5C}, {-0XA1F, -0XA1F}},
    {{-0XA1F, -0X1E5C}, {-0XA1F, -0X1E5C}},
    {{-0XA1F, -0X1E5C}, {-0X1E5C, -0XA1F}},
    {{-0XA1F, -0X1E5C}, {-0X1E5C, -0X1E5C}},
    {{-0X1E5C, -0XA1F}, {0XA1F, 0XA1F}},
    {{-0X1E5C, -0XA1F}, {0XA1F, 0X1E5C}},
    {{-0X1E5C, -0XA1F}, {0X1E5C, 0XA1F}},
    {{-0X1E5C, -0XA1F}, {0X1E5C, 0X1E5C}},
    {{-0X1E5C, -0XA1F}, {0XA1F, -0XA1F}},
    {{-0X1E5C, -0XA1F}, {0XA1F, -0X1E5C}},
    {{-0X1E5C, -0XA1F}, {0X1E5C, -0XA1F}},
    {{-0X1E5C, -0XA1F}, {0X1E5C, -0X1E5C}},
    {{-0X1E5C, -0XA1F}, {-0XA1F, 0XA1F}},
    {{-0X1E5C, -0XA1F}, {-0XA1F, 0X1E5C}},
    {{-0X1E5C, -0XA1F}, {-0X1E5C, 0XA1F}},
    {{-0X1E5C, -0XA1F}, {-0X1E5C, 0X1E5C}},
    {{-0X1E5C, -0XA1F}, {-0XA1F, -0XA1F}},
    {{-0X1E5C, -0XA1F}, {-0XA1F, -0X1E5C}},
    {{-0X1E5C, -0XA1F}, {-0X1E5C, -0XA1F}},
    {{-0X1E5C, -0XA1F}, {-0X1E5C, -0X1E5C}},
    {{-0X1E5C, -0X1E5C}, {0XA1F, 0XA1F}},
    {{-0X1E5C, -0X1E5C}, {0XA1F, 0X1E5C}},
    {{-0X1E5C, -0X1E5C}, {0X1E5C, 0XA1F}},
    {{-0X1E5C, -0X1E5C}, {0X1E5C, 0X1E5C}},
    {{-0X1E5C, -0X1E5C}, {0XA1F, -0XA1F}},
    {{-0X1E5C, -0X1E5C}, {0XA1F, -0X1E5C}},
    {{-0X1E5C, -0X1E5C}, {0X1E5C, -0XA1F}},
    {{-0X1E5C, -0X1E5C}, {0X1E5C, -0X1E5C}},
    {{-0X1E5C, -0X1E5C}, {-0XA1F, 0XA1F}},
    {{-0X1E5C, -0X1E5C}, {-0XA1F, 0X1E5C}},
    {{-0X1E5C, -0X1E5C}, {-0X1E5C, 0XA1F}},
    {{-0X1E5C, -0X1E5C}, {-0X1E5C, 0X1E5C}},
    {{-0X1E5C, -0X1E5C}, {-0XA1F, -0XA1F}},
    {{-0X1E5C, -0X1E5C}, {-0XA1F, -0X1E5C}},
    {{-0X1E5C, -0X1E5C}, {-0X1E5C, -0XA1F}},
    {{-0X1E5C, -0X1E5C}, {-0X1E5C, -0X1E5C}}};

void armral_16qam_modulation(const uint32_t nbits, const uint8_t *p_src,
                             armral_cmplx_int16_t *p_dst) {

  /* Compute the number of bytes */
  uint32_t bytes = nbits >> 3U;

  /* Finally get the tail (in bits) */
  uint32_t tail = nbits - (bytes * 8);
  /* Compute the number of blocks on 4 bits in the tail */
  uint32_t final_blck = tail / 4;

  /* Process a possible tail or an input with less than 8 int */
  uint32_t blk_cnt = bytes;

#ifdef ARMRAL_ARCH_SVE
  uint32_t vl = svcntd();
  svbool_t ptrue_b64 = svptrue_b64();
  uint32_t unrolls = blk_cnt / vl;
  for (uint32_t i = 0; i < unrolls; i++) {
    svuint64_t svsample = svld1ub_u64(ptrue_b64, p_src);
    p_src += vl;
    svint64_t gather = svld1_gather_index(
        ptrue_b64, (const int64_t *)constellation_16qam_outer_prod, svsample);
    svst1(ptrue_b64, (int64_t *)p_dst + i * vl, gather);
  }

  uint32_t i = unrolls * vl;
  if (i < blk_cnt) {
    svbool_t pred = svwhilelt_b64(i, blk_cnt);
    svuint64_t svsample = svld1ub_u64(pred, p_src);
    p_src += blk_cnt - i;
    svint64_t gather = svld1_gather_index(
        pred, (const int64_t *)constellation_16qam_outer_prod, svsample);
    svst1(pred, (int64_t *)p_dst + i, gather);
  }

  /* Process the very last sample on 4 bits */
  if (final_blck) {
    uint8_t sample = *p_src;

    /* compute index */
    uint8_t index = sample >> 4;
    index &= 0xF;

    p_dst[2 * blk_cnt] = constellation_16qam[index];
  }
#else
  while (blk_cnt > 0U) {
    uint8_t sample = *p_src++;
    memcpy(p_dst, constellation_16qam_outer_prod + sample,
           2 * sizeof(armral_cmplx_int16_t));
    p_dst += 2;
    blk_cnt--;
  }
  /* Process the very last sample on 4 bits */
  if (final_blck) {
    uint8_t sample = *p_src;
    uint8_t mask = 0xF;

    /* compute index */
    uint8_t index = sample >> 4;
    index &= mask;

    *p_dst = constellation_16qam[index];
  }
#endif
}

/* Definition of the constellation map according to 3GPP specification.
 * Gray encoding is used and
 * 0x4F0 = 1 * sqrt(42)/42 in Q2.13
 * 0xED0 = 3 * sqrt(42)/42 in Q2.13
 * 0x18B0 = 5 * sqrt(42)/42 in Q2.13
 * 0x2290 = 7 * sqrt(42)/42 in Q2.13
 */
static const armral_cmplx_int16_t constellation_64qam[64] = {
    {0xED0, 0xED0},     {0xED0, 0x4F0},     {0x4F0, 0xED0},
    {0x4F0, 0x4F0},     {0xED0, 0x18B0},    {0xED0, 0x2290},
    {0x4F0, 0x18B0},    {0x4F0, 0x2290},    {0x18B0, 0xED0},
    {0x18B0, 0x4F0},    {0x2290, 0xED0},    {0x2290, 0x4F0},
    {0x18B0, 0x18B0},   {0x18B0, 0x2290},   {0x2290, 0x18B0},
    {0x2290, 0x2290},   {0xED0, -0xED0},    {0xED0, -0x4F0},
    {0x4F0, -0xED0},    {0x4F0, -0x4F0},    {0xED0, -0x18B0},
    {0xED0, -0x2290},   {0x4F0, -0x18B0},   {0x4F0, -0x2290},
    {0x18B0, -0xED0},   {0x18B0, -0x4F0},   {0x2290, -0xED0},
    {0x2290, -0x4F0},   {0x18B0, -0x18B0},  {0x18B0, -0x2290},
    {0x2290, -0x18B0},  {0x2290, -0x2290},  {-0xED0, 0xED0},
    {-0xED0, 0x4F0},    {-0x4F0, 0xED0},    {-0x4F0, 0x4F0},
    {-0xED0, 0x18B0},   {-0xED0, 0x2290},   {-0x4F0, 0x18B0},
    {-0x4F0, 0x2290},   {-0x18B0, 0xED0},   {-0x18B0, 0x4F0},
    {-0x2290, 0xED0},   {-0x2290, 0x4F0},   {-0x18B0, 0x18B0},
    {-0x18B0, 0x2290},  {-0x2290, 0x18B0},  {-0x2290, 0x2290},
    {-0xED0, -0xED0},   {-0xED0, -0x4F0},   {-0x4F0, -0xED0},
    {-0x4F0, -0x4F0},   {-0xED0, -0x18B0},  {-0xED0, -0x2290},
    {-0x4F0, -0x18B0},  {-0x4F0, -0x2290},  {-0x18B0, -0xED0},
    {-0x18B0, -0x4F0},  {-0x2290, -0xED0},  {-0x2290, -0x4F0},
    {-0x18B0, -0x18B0}, {-0x18B0, -0x2290}, {-0x2290, -0x18B0},
    {-0x2290, -0x2290}};

void armral_64qam_modulation(const uint32_t nbits, const uint8_t *p_src,
                             armral_cmplx_int16_t *p_dst) {

  /* Compute the number of bytes */
  uint32_t bytes = nbits >> 3U;

  /* Compute the blocks which will be processed using loop unroll */
  uint32_t unr_cnt = bytes / 3;
  /* Compute the number of blocks on 6 bits in the tail */
#ifdef ARMRAL_ARCH_SVE
  // This implementation performs computation
  // on 24 bytes at a time, per 128 vector.
  // This follows a similar approach to the SIMD
  // version but requires exposition on how we make
  // this possible with SVE. The logic follows
  // that we load 32 bits of data, but mask against
  // the 24 bits we wish to compute against. This
  // is done in two ways, first we make sure that
  // vl is assigned value equal to how many 24 bit
  // per 128 bits we'll compute on, second we use a
  // tbl lookup to load our bytes into the correct lane
  // positions. The value given to specify index, 0xff000102
  // means that at every 32 bit lane, will look like the following
  //         [OUT_OF_RANGE|BYTE0|BYTE1|BYTE2]
  // where OUT_OF_RANGE corresponds to an out of range index
  // value, and BYTEN corresponds to the N'th byte of our
  // 3 bytes (24 bits) read in.
  const uint32_t vl = (svcntw() * 3) / 4;
  const svbool_t ptrue_b32 = svptrue_b32();
  const svbool_t pred = svwhilelt_b8((uint32_t)0, vl);
  svuint32_t index = svindex_u32(0xff000102, 0x00030303);
  index = svzip1(index, index);
  index = svzip1(index, index);
  const uint32_t svunroll_cnt = bytes / vl;
  for (uint32_t i = 0; i < svunroll_cnt; i++) {
    svuint8_t src_bytes = svld1_u8(pred, p_src);
    svuint8_t tbl = svtbl_u8(src_bytes, svreinterpret_u8(index));
    svuint32_t data = svreinterpret_u32(tbl);
    svuint32_t shift = svdupq_u32(18, 12, 6, 0);
    data = svand_n_u32_x(ptrue_b32, svlsr_u32_x(ptrue_b32, data, shift), 0x3f);
    p_src += vl;
    svuint32_t gather = svld1_gather_index(
        ptrue_b32, (const uint32_t *)constellation_64qam, data);
    svst1(ptrue_b32, (uint32_t *)p_dst, gather);
    p_dst += svcntw();
  }
  const uint32_t leftover_bytes = bytes - (svunroll_cnt * vl);
  unr_cnt = leftover_bytes / 3;
#endif
  /* Loop unroll processing */
  while (unr_cnt > 0U) {
    uint8_t first = *p_src++;
    uint8_t second = *p_src++;
    uint8_t third = *p_src++;

    uint8_t index0 = first >> 2;
    uint8_t index1 = ((first & 0x3) << 4) | (second >> 4);
    uint8_t index2 = ((second & 0xF) << 2) | (third >> 6);
    uint8_t index3 = third & 0x3F;

    *p_dst++ = constellation_64qam[index0];
    *p_dst++ = constellation_64qam[index1];
    *p_dst++ = constellation_64qam[index2];
    *p_dst++ = constellation_64qam[index3];

    unr_cnt--;
  }

  /* Finally get the tail (in bits) */
  unr_cnt = bytes / 3;
  uint32_t tail = nbits - (unr_cnt * 3 * 8);
  uint32_t final_blck = tail / 6;
  /* Process the tail which might be present */
  if (final_blck) {
    /* There's always at least one byte */
    uint8_t index0 = *p_src >> 2;
    armral_cmplx_int16_t point = constellation_64qam[index0];

    *p_dst = point;
    p_dst++;
    final_blck--;

    /* Two more samples might be present */
    if (final_blck) {
      uint8_t index1 = *p_src & 0x3;
      p_src++;
      index1 = index1 << 4;

      uint8_t scalar_tmp = *p_src & 0xF0;
      scalar_tmp = scalar_tmp >> 4;
      index1 = index1 | scalar_tmp;

      point = constellation_64qam[index1];
      *p_dst = point;
      p_dst++;

      final_blck--;

      /* The very last sample on 6 bits */
      if (final_blck) {
        uint8_t index2 = *p_src & 0xF;
        p_src++;
        index2 = index2 << 2;

        scalar_tmp = *p_src & 0xC0;
        scalar_tmp = scalar_tmp >> 6;
        index2 = index2 | scalar_tmp;

        point = constellation_64qam[index2];
        *p_dst = point;
      }
    }
  }
}

/* Definition of the constellation map according to 3GPP specification.
 * Gray encoding is used and
 * 0x274  = 1 * sqrt(170)/170 in Q2.13
 * 0x75D  = 3 * sqrt(170)/170 in Q2.13
 * 0xC45  = 5 * sqrt(170)/170 in Q2.13
 * 0x112E = 7 * sqrt(170)/170 in Q2.13
 * 0x1617 = 9 * sqrt(170)/170 in Q2.13
 * 0x1AFF = 11 * sqrt(170)/170 in Q2.13
 * 0x1FE8 = 13 * sqrt(170)/170 in Q2.13
 * 0x24D0 = 15 * sqrt(170)/170 in Q2.13
 */
static const armral_cmplx_int16_t constellation_256qam[256] = {
    {0xC45, 0xC45},     {0xC45, 0x112E},    {0x112E, 0xC45},
    {0x112E, 0x112E},   {0xC45, 0x75D},     {0xC45, 0x274},
    {0x112E, 0x75D},    {0x112E, 0x274},    {0x75D, 0xC45},
    {0x75D, 0x112E},    {0x274, 0xC45},     {0x274, 0x112E},
    {0x75D, 0x75D},     {0x75D, 0x274},     {0x274, 0x75D},
    {0x274, 0x274},     {0xC45, 0x1AFF},    {0xC45, 0x1617},
    {0x112E, 0x1AFF},   {0x112E, 0x1617},   {0xC45, 0x1FE8},
    {0xC45, 0x24D0},    {0x112E, 0x1FE8},   {0x112E, 0x24D0},
    {0x75D, 0x1AFF},    {0x75D, 0x1617},    {0x274, 0x1AFF},
    {0x274, 0x1617},    {0x75D, 0x1FE8},    {0x75D, 0x24D0},
    {0x274, 0x1FE8},    {0x274, 0x24D0},    {0x1AFF, 0xC45},
    {0x1AFF, 0x112E},   {0x1617, 0xC45},    {0x1617, 0x112E},
    {0x1AFF, 0x75D},    {0x1AFF, 0x274},    {0x1617, 0x75D},
    {0x1617, 0x274},    {0x1FE8, 0xC45},    {0x1FE8, 0x112E},
    {0x24D0, 0xC45},    {0x24D0, 0x112E},   {0x1FE8, 0x75D},
    {0x1FE8, 0x274},    {0x24D0, 0x75D},    {0x24D0, 0x274},
    {0x1AFF, 0x1AFF},   {0x1AFF, 0x1617},   {0x1617, 0x1AFF},
    {0x1617, 0x1617},   {0x1AFF, 0x1FE8},   {0x1AFF, 0x24D0},
    {0x1617, 0x1FE8},   {0x1617, 0x24D0},   {0x1FE8, 0x1AFF},
    {0x1FE8, 0x1617},   {0x24D0, 0x1AFF},   {0x24D0, 0x1617},
    {0x1FE8, 0x1FE8},   {0x1FE8, 0x24D0},   {0x24D0, 0x1FE8},
    {0x24D0, 0x24D0},

    {0xC45, -0xC45},    {0xC45, -0x112E},   {0x112E, -0xC45},
    {0x112E, -0x112E},  {0xC45, -0x75D},    {0xC45, -0x274},
    {0x112E, -0x75D},   {0x112E, -0x274},   {0x75D, -0xC45},
    {0x75D, -0x112E},   {0x274, -0xC45},    {0x274, -0x112E},
    {0x75D, -0x75D},    {0x75D, -0x274},    {0x274, -0x75D},
    {0x274, -0x274},    {0xC45, -0x1AFF},   {0xC45, -0x1617},
    {0x112E, -0x1AFF},  {0x112E, -0x1617},  {0xC45, -0x1FE8},
    {0xC45, -0x24D0},   {0x112E, -0x1FE8},  {0x112E, -0x24D0},
    {0x75D, -0x1AFF},   {0x75D, -0x1617},   {0x274, -0x1AFF},
    {0x274, -0x1617},   {0x75D, -0x1FE8},   {0x75D, -0x24D0},
    {0x274, -0x1FE8},   {0x274, -0x24D0},   {0x1AFF, -0xC45},
    {0x1AFF, -0x112E},  {0x1617, -0xC45},   {0x1617, -0x112E},
    {0x1AFF, -0x75D},   {0x1AFF, -0x274},   {0x1617, -0x75D},
    {0x1617, -0x274},   {0x1FE8, -0xC45},   {0x1FE8, -0x112E},
    {0x24D0, -0xC45},   {0x24D0, -0x112E},  {0x1FE8, -0x75D},
    {0x1FE8, -0x274},   {0x24D0, -0x75D},   {0x24D0, -0x274},
    {0x1AFF, -0x1AFF},  {0x1AFF, -0x1617},  {0x1617, -0x1AFF},
    {0x1617, -0x1617},  {0x1AFF, -0x1FE8},  {0x1AFF, -0x24D0},
    {0x1617, -0x1FE8},  {0x1617, -0x24D0},  {0x1FE8, -0x1AFF},
    {0x1FE8, -0x1617},  {0x24D0, -0x1AFF},  {0x24D0, -0x1617},
    {0x1FE8, -0x1FE8},  {0x1FE8, -0x24D0},  {0x24D0, -0x1FE8},
    {0x24D0, -0x24D0},

    {-0xC45, 0xC45},    {-0xC45, 0x112E},   {-0x112E, 0xC45},
    {-0x112E, 0x112E},  {-0xC45, 0x75D},    {-0xC45, 0x274},
    {-0x112E, 0x75D},   {-0x112E, 0x274},   {-0x75D, 0xC45},
    {-0x75D, 0x112E},   {-0x274, 0xC45},    {-0x274, 0x112E},
    {-0x75D, 0x75D},    {-0x75D, 0x274},    {-0x274, 0x75D},
    {-0x274, 0x274},    {-0xC45, 0x1AFF},   {-0xC45, 0x1617},
    {-0x112E, 0x1AFF},  {-0x112E, 0x1617},  {-0xC45, 0x1FE8},
    {-0xC45, 0x24D0},   {-0x112E, 0x1FE8},  {-0x112E, 0x24D0},
    {-0x75D, 0x1AFF},   {-0x75D, 0x1617},   {-0x274, 0x1AFF},
    {-0x274, 0x1617},   {-0x75D, 0x1FE8},   {-0x75D, 0x24D0},
    {-0x274, 0x1FE8},   {-0x274, 0x24D0},   {-0x1AFF, 0xC45},
    {-0x1AFF, 0x112E},  {-0x1617, 0xC45},   {-0x1617, 0x112E},
    {-0x1AFF, 0x75D},   {-0x1AFF, 0x274},   {-0x1617, 0x75D},
    {-0x1617, 0x274},   {-0x1FE8, 0xC45},   {-0x1FE8, 0x112E},
    {-0x24D0, 0xC45},   {-0x24D0, 0x112E},  {-0x1FE8, 0x75D},
    {-0x1FE8, 0x274},   {-0x24D0, 0x75D},   {-0x24D0, 0x274},
    {-0x1AFF, 0x1AFF},  {-0x1AFF, 0x1617},  {-0x1617, 0x1AFF},
    {-0x1617, 0x1617},  {-0x1AFF, 0x1FE8},  {-0x1AFF, 0x24D0},
    {-0x1617, 0x1FE8},  {-0x1617, 0x24D0},  {-0x1FE8, 0x1AFF},
    {-0x1FE8, 0x1617},  {-0x24D0, 0x1AFF},  {-0x24D0, 0x1617},
    {-0x1FE8, 0x1FE8},  {-0x1FE8, 0x24D0},  {-0x24D0, 0x1FE8},
    {-0x24D0, 0x24D0},

    {-0xC45, -0xC45},   {-0xC45, -0x112E},  {-0x112E, -0xC45},
    {-0x112E, -0x112E}, {-0xC45, -0x75D},   {-0xC45, -0x274},
    {-0x112E, -0x75D},  {-0x112E, -0x274},  {-0x75D, -0xC45},
    {-0x75D, -0x112E},  {-0x274, -0xC45},   {-0x274, -0x112E},
    {-0x75D, -0x75D},   {-0x75D, -0x274},   {-0x274, -0x75D},
    {-0x274, -0x274},   {-0xC45, -0x1AFF},  {-0xC45, -0x1617},
    {-0x112E, -0x1AFF}, {-0x112E, -0x1617}, {-0xC45, -0x1FE8},
    {-0xC45, -0x24D0},  {-0x112E, -0x1FE8}, {-0x112E, -0x24D0},
    {-0x75D, -0x1AFF},  {-0x75D, -0x1617},  {-0x274, -0x1AFF},
    {-0x274, -0x1617},  {-0x75D, -0x1FE8},  {-0x75D, -0x24D0},
    {-0x274, -0x1FE8},  {-0x274, -0x24D0},  {-0x1AFF, -0xC45},
    {-0x1AFF, -0x112E}, {-0x1617, -0xC45},  {-0x1617, -0x112E},
    {-0x1AFF, -0x75D},  {-0x1AFF, -0x274},  {-0x1617, -0x75D},
    {-0x1617, -0x274},  {-0x1FE8, -0xC45},  {-0x1FE8, -0x112E},
    {-0x24D0, -0xC45},  {-0x24D0, -0x112E}, {-0x1FE8, -0x75D},
    {-0x1FE8, -0x274},  {-0x24D0, -0x75D},  {-0x24D0, -0x274},
    {-0x1AFF, -0x1AFF}, {-0x1AFF, -0x1617}, {-0x1617, -0x1AFF},
    {-0x1617, -0x1617}, {-0x1AFF, -0x1FE8}, {-0x1AFF, -0x24D0},
    {-0x1617, -0x1FE8}, {-0x1617, -0x24D0}, {-0x1FE8, -0x1AFF},
    {-0x1FE8, -0x1617}, {-0x24D0, -0x1AFF}, {-0x24D0, -0x1617},
    {-0x1FE8, -0x1FE8}, {-0x1FE8, -0x24D0}, {-0x24D0, -0x1FE8},
    {-0x24D0, -0x24D0}};

void armral_256qam_modulation(const uint32_t nbits, const uint8_t *p_src,
                              armral_cmplx_int16_t *p_dst) {
  /* Compute the number of bytes */
  uint32_t bytes = nbits >> 3U;
#ifdef ARMRAL_ARCH_SVE
  const svbool_t ptrue_b32 = svptrue_b32();
  uint64_t vl = svcntw();
  /* Compute the blocks which will be processed using loop unroll */
  uint32_t unr_cnt = bytes / vl;

  for (uint32_t i = 0; i < unr_cnt; i++) {
    svuint32_t index = svld1ub_u32(ptrue_b32, p_src);
    p_src += vl;
    svint32_t gather = svld1_gather_index(
        ptrue_b32, (const int32_t *)constellation_256qam, index);
    svst1_s32(ptrue_b32, (int32_t *)p_dst, gather);
    p_dst += vl;
  }

  const uint32_t leftover_bytes = bytes - unr_cnt * vl;
  if (leftover_bytes) {
    svbool_t pred = svwhilelt_b32((uint32_t)0, leftover_bytes);
    svuint32_t index = svld1ub_u32(pred, p_src);
    svint32_t gather =
        svld1_gather_index(pred, (const int32_t *)constellation_256qam, index);
    svst1_s32(pred, (int32_t *)p_dst, gather);
  }
#else
  /* Compute the blocks which will be processed using loop unroll */
  uint32_t unr_cnt = bytes / 4;
  /* Loop unroll processing */
  while (unr_cnt > 0U) {
    uint8_t index0 = *p_src++;
    uint8_t index1 = *p_src++;
    uint8_t index2 = *p_src++;
    uint8_t index3 = *p_src++;

    *p_dst++ = constellation_256qam[index0];
    *p_dst++ = constellation_256qam[index1];
    *p_dst++ = constellation_256qam[index2];
    *p_dst++ = constellation_256qam[index3];

    unr_cnt--;
  }

  /* Compute the number of blocks of 8 bits in the tail */
  uint32_t final_blck;
  final_blck = bytes & 3;
  /* Last block */
  while (final_blck > 0U) {
    uint8_t sample = *p_src++;
    *p_dst++ = constellation_256qam[sample];
    final_blck--;
  }
#endif
}

armral_status armral_modulation(const uint32_t nbits,
                                armral_modulation_type mod_type,
                                const uint8_t *p_src,
                                armral_cmplx_int16_t *p_dst) {
  switch (mod_type) {
  case ARMRAL_MOD_QPSK:
    if ((nbits % 2) != 0) {
      return ARMRAL_ARGUMENT_ERROR;
    }
    armral_qpsk_modulation(nbits, p_src, p_dst);
    return ARMRAL_SUCCESS;
  case ARMRAL_MOD_16QAM:
    if ((nbits % 4) != 0) {
      return ARMRAL_ARGUMENT_ERROR;
    }
    armral_16qam_modulation(nbits, p_src, p_dst);
    return ARMRAL_SUCCESS;
  case ARMRAL_MOD_64QAM:
    if ((nbits % 6) != 0) {
      return ARMRAL_ARGUMENT_ERROR;
    }
    armral_64qam_modulation(nbits, p_src, p_dst);
    return ARMRAL_SUCCESS;
  case ARMRAL_MOD_256QAM:
    if ((nbits % 8) != 0) {
      return ARMRAL_ARGUMENT_ERROR;
    }
    armral_256qam_modulation(nbits, p_src, p_dst);
    return ARMRAL_SUCCESS;
  }
  return ARMRAL_ARGUMENT_ERROR;
}
