/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstring>

armral_status armral_polar_subchannel_interleave(
    uint32_t n, uint32_t kplus, const uint8_t *__restrict__ frozen,
    const uint8_t *__restrict__ c, uint8_t *__restrict__ u) {
  uint32_t parity = 0;
  uint32_t dst_bit = 0;
  uint32_t src_bit = 0;
  for (; dst_bit < n; dst_bit += 8) {
    uint32_t val = 0;
    uint64_t frozen64 = *(const uint64_t *)&frozen[dst_bit];
    for (uint32_t bit_idx = 0; bit_idx < 8; ++bit_idx) {
      parity = ((parity << 1) & 0x1fU) | (parity >> 4);
      if ((frozen64 & (0x80ULL << (bit_idx * 8))) != 0) { // frozen bit
        val <<= 1;
      } else if ((frozen64 & (0x01ULL << (bit_idx * 8))) != 0) { // parity bit
        val = (val << 1) | (parity & 1);
      } else { // information bit
        uint8_t bit = (c[src_bit / 8] >> (~src_bit & 7)) & 1;
        val = (val << 1) | bit;
        parity ^= bit;
        ++src_bit;
      }
    }
    uint16_t ofs = dst_bit / 8;
    u[ofs] = val;
  }
  return ARMRAL_SUCCESS;
}
