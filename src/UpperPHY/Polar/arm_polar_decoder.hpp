/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#pragma once

namespace {

// Extract the sign of an integer
inline int8_t __attribute__((always_inline)) sign(int32_t x) {
  return static_cast<int>(x > 0) - static_cast<int>(x < 0);
}

// calculate the minimum absolute value between two integers
inline int16_t __attribute__((always_inline))
min(const int8_t x, const int8_t y) {
  return abs(x) < abs(y) ? abs(x) : abs(y);
}

} // namespace
