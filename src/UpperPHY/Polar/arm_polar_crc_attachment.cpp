/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "utils/allocators.hpp"

#include <cassert>
#include <cstdlib>
#include <cstring>

namespace {

template<typename Allocator>
armral_status polar_crc_attachment(const uint8_t *__restrict data_in,
                                   uint32_t a, uint8_t *data_out,
                                   Allocator &allocator) {
  // Number of CRC bits
  constexpr uint32_t l = 24;

  // Number of bytes containing information bits (not including CRC bits)
  uint32_t a_bytes = (a + 7) >> 3;

  uint32_t num_pad_bits = 0;
  uint32_t buffer_size = a_bytes;
  unique_ptr<Allocator, uint8_t> buffer;

  // The CRC calculation routine expects an input buffer padded to a multiple of
  // 16 bytes (128 bits)
  if (a % 128 != 0) {
    // Pad zeros in the MSBs so we give the correct size of input to the CRC
    // calculation routine
    num_pad_bits = 128 - (a % 128);
    buffer_size = (a + num_pad_bits) >> 3;
    buffer = allocate_zeroed<uint8_t>(allocator, buffer_size);
  } else {
    buffer = allocate_uninitialized<uint8_t>(allocator, buffer_size);
  }

  if constexpr (Allocator::is_counting) {
    return ARMRAL_SUCCESS;
  }

  // Number of bits to encode
  uint32_t k = a + l;

  // Initialize the output to zero, which takes care of the filler bits
  memset(data_out, 0, sizeof(uint8_t) * ((k + 7) >> 3));

  if (num_pad_bits != 0) {
    uint32_t idx = num_pad_bits >> 3;
    uint32_t shift1 = num_pad_bits % 8;
    uint32_t shift2 = 8 - shift1;
    // Copy the information bits after the zero-padding bits
    buffer[idx] = data_in[0] >> shift1;
    for (uint32_t i = 0; i < a_bytes - 1; i++) {
      buffer[idx + i + 1] = (data_in[i] << shift2) | (data_in[i + 1] >> shift1);
    }
  } else {
    // The input is already the correct length so just copy it into the buffer
    memcpy(buffer.get(), data_in, sizeof(uint8_t) * a_bytes);
  }

  // Generate the CRC parity bits
  uint64_t crc = 0;
  armral_crc24_b_be(buffer_size, (const uint64_t *)buffer.get(), &crc);

  // Copy the bytes containing information bits
  memcpy(data_out, data_in, sizeof(uint8_t) * a_bytes);

  // Append the CRC bits to the information bits. If the information bits don't
  // align with a byte boundary then do the necessary shifts
  uint32_t info_rem_bits = a % 8;
  uint32_t shift = info_rem_bits > 0 ? 8 - info_rem_bits : 0;
  // Make sure any non-information bits in the last information byte are zeroed
  data_out[a_bytes - 1] &= (255 << shift);
  // Put the CRC bits into place
  data_out[a_bytes - 1] |= (crc >> (24 - shift));
  data_out[a_bytes] = crc >> (16 - shift);
  data_out[a_bytes + 1] = crc >> (8 - shift);
  data_out[a_bytes + 2] = crc << shift;

  return ARMRAL_SUCCESS;
}

} // anonymous namespace

armral_status armral_polar_crc_attachment(const uint8_t *__restrict data_in,
                                          uint32_t a, uint8_t *data_out) {
  heap_allocator allocator{};
  return polar_crc_attachment(data_in, a, data_out, allocator);
}

armral_status
armral_polar_crc_attachment_noalloc(const uint8_t *__restrict data_in,
                                    uint32_t a, uint8_t *data_out,
                                    void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return polar_crc_attachment(data_in, a, data_out, allocator);
}

uint32_t armral_polar_crc_attachment_noalloc_buffer_size(uint32_t a) {
  counting_allocator allocator{};
  (void)polar_crc_attachment(nullptr, a, nullptr, allocator);
  return allocator.required_bytes();
}
