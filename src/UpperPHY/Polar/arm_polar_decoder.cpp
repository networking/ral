/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "intrinsics.h"

#include <algorithm>
#include <cassert>
#include <cstring>

#include "arm_polar_decoder_neon.hpp"

namespace {

typedef struct {
  const uint8_t *frozen_bits_mask;
  uint32_t curr_bit_idx;
} sc_decoder;

inline void __attribute__((always_inline))
setup_sc_decoder(sc_decoder *decoder, const uint8_t *frozen) {
  decoder->curr_bit_idx = 0;
  decoder->frozen_bits_mask = frozen;
}

template<int Nhalf, int L>
inline void f_l(const int8_t *in, int8_t *out) {
  f<Nhalf * L>(in, &in[Nhalf * L], out);
}

template<int Nhalf, int L>
inline void g_l(const int8_t *in, const uint8_t *dec,
                [[maybe_unused]] const uint8_t *hist, int8_t *out) {
  // Calculate beliefs for right children in the successive cancellation list
  // (SCL) algorithm:
  // g(a_h, b_h, c_i=0) = a_h + b_h
  // g(a_h, b_h, c_i=1) = a_h - b_h
  // This matches the non-list version, but for L > 1 we need to take care of
  // permuting the input beliefs by the list history value rather than simply
  // vectorizing the beliefs directly.
  if constexpr (L > 1) {
    g_l_impl<Nhalf, L>::g_l(in, dec, hist, out);
  } else {
    g<Nhalf>(in, &in[Nhalf], dec, out);
  }
}

template<int Nhalf, int L>
inline void g_top(const int8_t *in, const uint8_t *dec, int8_t *out) {
  // no history here, since no differing beliefs to choose from.
  static_assert(Nhalf >= 16);
  static_assert(Nhalf % 4 == 0);
  if constexpr (L == 8) {
    uint8x16_t llr_idx = {0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1, 1, 1, 1, 1, 1};
    for (int i = 0; i < Nhalf; i += 2) {
      int8x16_t llr1 = vld1hq_s8(in + i);
      int8x16_t llr2 = vld1hq_s8(in + Nhalf + i);
      llr1 = vqtbl1q_s8(llr1, llr_idx);
      llr2 = vqtbl1q_s8(llr2, llr_idx);
      uint8x16_t bit0 = vld1q_u8(&dec[i * 8]);
      int8x16_t result0 = vbslq_s8(vceqzq_u8(bit0), vqaddq_s8(llr2, llr1),
                                   vqsubq_s8(llr2, llr1));
      vst1q_s8(&out[i * 8], result0);
    }
  } else if constexpr (L == 4) {
    uint8x16_t llr_idx = {0, 0, 0, 0, 1, 1, 1, 1, 2, 2, 2, 2, 3, 3, 3, 3};
    for (int i = 0; i < Nhalf; i += 4) {
      int8x16_t llr1 = vld1sq_s8(in + i);
      int8x16_t llr2 = vld1sq_s8(in + Nhalf + i);
      llr1 = vqtbl1q_s8(llr1, llr_idx);
      llr2 = vqtbl1q_s8(llr2, llr_idx);
      uint8x16_t bit0 = vld1q_u8(&dec[i * 4]);
      int8x16_t result0 = vbslq_s8(vceqzq_u8(bit0), vqaddq_s8(llr2, llr1),
                                   vqsubq_s8(llr2, llr1));
      vst1q_s8(&out[i * 4], result0);
    }
  } else if constexpr (L == 2) {
    for (int i = 0; i < Nhalf; i += 16) {
      int8x16_t llr1 = vld1q_s8(&in[i]);
      int8x16_t llr2 = vld1q_s8(&in[Nhalf + i]);
      uint8x16x2_t bits = vld2q_u8(&dec[i * 2]);
      uint8x16_t bit0 = bits.val[0];
      uint8x16_t bit1 = bits.val[1];
      int8x16_t result0 = vbslq_s8(vceqzq_u8(bit0), vqaddq_s8(llr2, llr1),
                                   vqsubq_s8(llr2, llr1));
      int8x16_t result1 = vbslq_s8(vceqzq_u8(bit1), vqaddq_s8(llr2, llr1),
                                   vqsubq_s8(llr2, llr1));
      int8x16x2_t res = {result0, result1};
      vst2q_s8(&out[i * 2], res);
    }
  } else {
    assert(false && "unimplemented!");
  }
}

template<int L, int N>
inline void zip1_l(const int8_t *__restrict in, int8_t *__restrict out) {
  static_assert(N % 16 == 0);
  if constexpr (L == 8) {
    for (int i = 0; i < N; i += 2) {
      int8x16_t x1 = vld1q_dup_s8(in);
      int8x16_t x2 = vld1q_dup_s8(in + 1);
      vst1q_s8(out, vextq_s8(x1, x2, 8));
      in += 2;
      out += 16;
    }
  } else if constexpr (L == 4) {
    for (int i = 0; i < N; i += 4) {
      int8x8_t x0 = vld1s_s8(in);
      int8x16_t x1 = vzip1l_s8(x0, x0);
      int8x16_t x2 = vzip1q_s8(x1, x1);
      vst1q_s8(out, x2);
      in += 4;
      out += 16;
    }
  } else if constexpr (L == 2) {
    for (int i = 0; i < N; i += 8) {
      int8x8_t x0 = vld1_s8(in);
      int8x16_t x1 = vzip1l_s8(x0, x0);
      vst1q_s8(out, x1);
      in += 8;
      out += 16;
    }
  } else {
    assert(false && "unimplemented!");
  }
}

template<int L, int K, typename = void>
struct sort_decoder_entries_impl {
  static inline void sort(uint8_t *est_bits, uint32_t *pm, uint8_t *hist) {
    // ensure decoder entries are sorted by path metric, so we keep only the
    // most-likely paths.
    int min_idx = 0;
    for (int i = 1; i < L; ++i) {
      if (pm[i] < pm[min_idx]) {
        min_idx = i;
      }
    }
    if (min_idx > 0) {
      std::swap(est_bits[0], est_bits[min_idx]);
      std::swap(pm[0], pm[min_idx]);
      std::swap(hist[0], hist[min_idx]);
    }
    using Rec = ::sort_decoder_entries_impl<L - 1, K - 1>;
    Rec::sort(est_bits + 1, pm + 1, hist + 1);
  }
};

template<int L>
struct sort_decoder_entries_impl<L, 0> {
  static inline void sort(uint8_t * /*est_bits*/, uint32_t * /*pm*/,
                          uint8_t * /*hist*/) {
    // K=0 means nothing to do.
  }
};

template<int L>
void sort_decoder_entries(uint8_t *est_bits, uint32_t *pm, uint8_t *hist) {
  sort_decoder_entries_impl<L * 2, L>::sort(est_bits, pm, hist);
}

template<int N, int L, typename = void>
struct polar_stage;

inline uint32x4_t vmlsl_u32_s16(uint32x4_t acc, uint16x4_t x, uint16x4_t y) {
  return vreinterpretq_u32_s32(vmlsl_s16(vreinterpretq_s32_u32(acc),
                                         vreinterpret_s16_u16(x),
                                         vreinterpret_s16_u16(y)));
}

template<int L>
struct polar_stage<2, L> {
  static inline void __attribute__((always_inline))
  decode(sc_decoder *decoder, const int8_t *p_beliefs_in, uint8_t *p_tmp_buf,
         uint8_t *p_u_seq_out, uint32_t *pms, uint8_t *hists) {

    uint8_t u_seq_out[L];
    uint8_t est_bits[L * 2] = {};
    uint32_t pm[L * 2];
    uint8_t hist[L * 2];
    for (int l = 0; l < L; ++l) {
      u_seq_out[l] = p_u_seq_out[l];
      pm[L + l] = pm[l] = pms[l];
      hist[L + l] = hist[l] = l;
    }

    uint32_t idx = decoder->curr_bit_idx;
    uint16_t frozen = *(const uint16_t *)&decoder->frozen_bits_mask[idx];

    // left bit estimation
    if ((frozen & 0x80) != 0) {
      for (int l = 0; l < L; ++l) {
        int8_t a = p_beliefs_in[l];
        int8_t b = p_beliefs_in[L + l];
        int32_t l_u0 = min(a, b);
        est_bits[l] = 0;
        pm[l] += a * b < 0 ? l_u0 : 0; // path metric for b=0
      }
    } else {
      for (int l = 0; l < L; ++l) {
        int8_t a = p_beliefs_in[l];
        int8_t b = p_beliefs_in[L + l];
        int32_t l_u0 = min(a, b);
        est_bits[l] = 0;
        pm[l] += a * b < 0 ? l_u0 : 0; // path metric for b=0
        est_bits[L + l] = 1 << 1;
        pm[L + l] += a * b >= 0 ? l_u0 : 0; // path metric for b=1
      }
      sort_decoder_entries<L>(est_bits, pm, hist);
    }

    // right bit estimation
    if ((frozen & 0x8000) != 0) {
      for (int l = 0; l < L; ++l) {
        uint8_t h = hist[l];
        int8_t a = p_beliefs_in[h];
        int8_t b = p_beliefs_in[L + h];
        uint8_t est_bit0 = est_bits[l] >> 1;
        int32_t l_u1 = b + (1 - 2 * est_bit0) * a;
        est_bits[l] |= 0;
        pm[l] += l_u1 < 0 ? -l_u1 : 0; // path metric for b=0
      }
    } else {
      for (int l = 0; l < L; ++l) {
        est_bits[L + l] = est_bits[l];
        hist[L + l] = hist[l];
        pm[L + l] = pm[l];
        uint8_t h = hist[l];
        int8_t a = p_beliefs_in[h];
        int8_t b = p_beliefs_in[L + h];
        uint8_t est_bit0 = est_bits[l] >> 1;
        int32_t l_u1 = b + (1 - 2 * est_bit0) * a;
        est_bits[l] |= 0;
        pm[l] += l_u1 < 0 ? -l_u1 : 0; // path metric for b=0
        est_bits[L + l] |= 1;
        pm[L + l] += l_u1 >= 0 ? l_u1 : 0; // path metric for b=1
      }
      sort_decoder_entries<L>(est_bits, pm, hist);
    }

    uint32_t shft_idx = idx % 8;
    for (int l = 0; l < L; ++l) {
      // write data
      uint8_t h = hist[l];
      uint32_t cw = est_bits[l];
      p_u_seq_out[l] = u_seq_out[h] | (cw << (6 - shft_idx));
      // combine
      p_tmp_buf[l] = ((cw >> 1) ^ cw) & 1;
      p_tmp_buf[L + l] = cw & 1;
      // path metric and history
      pms[l] = pm[l];
      hists[l] = h;
    }

    decoder->curr_bit_idx = idx + 2;
  }
};

template<>
struct polar_stage<2, 4> {
  static inline void __attribute__((always_inline))
  decode(sc_decoder *decoder, const int8_t *p_beliefs_in, uint8_t *p_tmp_buf,
         uint8_t *p_u_seq_out, uint32_t *pms, uint8_t *hists) {

    uint8x8_t est_bits;
    uint32x4_t pm[2];
    uint8x8_t hist = {0, 1, 2, 3, 0, 1, 2, 3};
    pm[0] = vld1q_u32(pms);

    // current index and frozen mask for two bits = two bytes.
    // low byte is for the first output bit since little-endian.
    uint32_t idx = decoder->curr_bit_idx;
    uint16_t frozen = *(const uint16_t *)&decoder->frozen_bits_mask[idx];

    // unpack L=4 as (first half) and bs (second half) bytes (eight total),
    // widen to int16.
    int16x8_t beliefs = vmovl_s8(vld1_s8(p_beliefs_in));
    int16x4_t as = vget_low_s16(beliefs);
    int16x4_t bs = vget_high_s16(beliefs);

    // f(a, b) = sign(a) * sign(b) * min(|a|,|b|)
    //         = sign(a ^ b) * min(|a|,|b|)
    uint16x4_t as_abs = vreinterpret_u16_s16(vqabs_s16(as));
    uint16x4_t bs_abs = vreinterpret_u16_s16(vqabs_s16(bs));
    uint16x4_t l_u0 = vmin_u16(as_abs, bs_abs);
    uint16x4_t cmp = vcltz_s16(as ^ bs);

    // pm[b == f(a,b)<0] = |l_u0|
    // pm[b != f(a,b)<0] = 0
    uint32x4_t pm_u0_b1 = vmovl_u16(vbic_u16(l_u0, cmp));

    // left bit estimation
    if ((frozen & 0x80) != 0) {
      // update path metrics for b=0
      pm[0] = vmlsl_u32_s16(pm[0], l_u0, cmp);
      est_bits = vdup_n_u8(0); // frozen bits are always zero
    } else {
      // update path metrics for b=1, b=0.
      pm[1] = vaddq_u32(pm[0], pm_u0_b1);
      pm[0] = vmlsl_u32_s16(pm[0], l_u0, cmp);

      // set high L=4 bytes to [u0,u1]=0b10=2.
      est_bits = vcreate_u8(0x0202020200000000ULL);
      // sort to keep low L=4 out of 2*L=8 paths.
      sort_decoder_entries<4>((uint8_t *)&est_bits, (uint32_t *)&pm[0],
                              (uint8_t *)&hist);

      // permute 4-byte chunks of as/bs to match new hist.
      // unpack L=4 histories from every low byte, splat over N=2-byte chunks.
      uint8x8_t h8 = vzip1_u8(hist, hist);

      // permute LLRs (as/bs) by history permute vector.
      uint8x8_t h_ofs0 = {0, 1, 0, 1, 0, 1, 0, 1};
      h_ofs0 = vsli_n_u8(h_ofs0, h8, 1);
      as = vreinterpret_s16_u8(vtbl1_u8(vreinterpret_u8_s16(as), h_ofs0));
      bs = vreinterpret_s16_u8(vtbl1_u8(vreinterpret_u8_s16(bs), h_ofs0));
    }

    // g(a, b, c=0) = a + b
    // g(a, b, c=1) = a - b
    int16x4_t ab_add = vqadd_s16(bs, as);
    int16x4_t ab_sub = vqsub_s16(bs, as);
    uint16x4_t est_bit0_16 = vget_low_u16(vmovl_u8(est_bits));
    int16x4_t l_u1 = vbsl_s16(vceqz_u16(est_bit0_16), ab_add, ab_sub);
    cmp = vcltz_s16(l_u1);
    uint16x4_t l_u1_abs = vreinterpret_u16_s16(vqabs_s16(l_u1));

    // pm[b == g(a,b,c)<0] = |l_u1|
    // pm[b != g(a,b,c)<0] = 0
    uint32x4_t pm_u1_b1 = vmovl_u16(vbic_u16(l_u1_abs, cmp));

    // right bit estimation
    if ((frozen & 0x8000) != 0) {
      // update path metrics for b=0
      pm[0] = vmlsl_u32_s16(pm[0], l_u1_abs, cmp);
    } else {
      hist = vreinterpret_u8_u32(vcopy_lane_u32(vreinterpret_u32_u8(hist), 1,
                                                vreinterpret_u32_u8(hist), 0));
      // update path metrics for b=1, b=0.
      pm[1] = vaddq_u32(pm[0], pm_u1_b1);
      pm[0] = vmlsl_u32_s16(pm[0], l_u1_abs, cmp);

      // set high L=4 bytes to [u0,u1]|=0b01=1.
      est_bits = vreinterpret_u8_u32(vcopy_lane_u32(
          vreinterpret_u32_u8(est_bits), 1, vreinterpret_u32_u8(est_bits), 0));
      est_bits |= vcreate_u8(0x0101010100000000ULL);
      // sort to keep low L=4 out of 2*L=8 paths.
      sort_decoder_entries<4>((uint8_t *)&est_bits, (uint32_t *)&pm[0],
                              (uint8_t *)&hist);
    }

    // load p_u_seq_out (only four bytes) and permute by hist
    uint8x8_t u_seq_out = vld1s_u8(p_u_seq_out);
    u_seq_out = vtbl1_u8(u_seq_out, hist);

    // add estimated bits and store four bytes
    uint32_t shft_idx = idx % 8;
    u_seq_out |= vshl_u8(est_bits, vdup_n_s8(6 - shft_idx));
    *(uint32_t *)p_u_seq_out = vreinterpret_u32_u8(u_seq_out)[0];

    // path metric and history
    *(uint32_t *)hists = vreinterpret_u32_u8(hist)[0];
    vst1q_u32(pms, pm[0]);

    // combine
    uint8x8_t comb0 = vshr_n_u8(est_bits, 1) ^ (est_bits & 1);
    uint8x8_t comb1 = est_bits & 1;
    vstps_u8(p_tmp_buf, comb0, comb1);

    decoder->curr_bit_idx = idx + 2;
  }
};

template<>
struct polar_stage<2, 8> {
  static inline void __attribute__((always_inline))
  decode(sc_decoder *decoder, const int8_t *p_beliefs_in, uint8_t *p_tmp_buf,
         uint8_t *p_u_seq_out, uint32_t *pms, uint8_t *hists) {

    // treated as a list of 2 uint32x8
    uint32x4x4_t pm_list;
    pm_list.val[0] = vld1q_u32(pms);
    pm_list.val[1] = vld1q_u32(pms + 4);
    pm_list.val[2] = vld1q_u32(pms);
    pm_list.val[3] = vld1q_u32(pms + 4);

    uint8x16_t est_bits;
    uint8x16_t hist = {0, 1, 2, 3, 4, 5, 6, 7, 0, 1, 2, 3, 4, 5, 6, 7};

    // current index and frozen mask for two bits = two bytes.
    // low byte is for the first output bit since little-endian.
    uint32_t idx = decoder->curr_bit_idx;
    uint16_t frozen = *(const uint16_t *)&decoder->frozen_bits_mask[idx];

    // unpack L=8 a, b, and widen to int16.
    int16x8_t a = vmovl_s8(vld1_s8(p_beliefs_in));
    int16x8_t b = vmovl_s8(vld1_s8(p_beliefs_in + 8));

    // f(a, b) = sign(a) * sign(b) * min(|a|,|b|)
    //         = sign(a ^ b) * min(|a|,|b|)
    uint16x8_t as_abs = vreinterpretq_u16_s16(vqabsq_s16(a));
    uint16x8_t bs_abs = vreinterpretq_u16_s16(vqabsq_s16(b));
    uint16x8_t l_u0 = vminq_u16(as_abs, bs_abs);
    uint16x8_t cmp = vcltzq_s16(a ^ b);

    // pm[b == f(a,b)<0] = |l_u0|
    // pm[b != f(a,b)<0] = 0
    uint16x8_t temp = vbicq_u16(l_u0, cmp);
    uint32x4x2_t pm_u0_b1;
    pm_u0_b1.val[0] = vmovl_u16(vget_low_u16(temp));
    pm_u0_b1.val[1] = vmovl_high_u16(temp);

    // left bit estimation
    if ((frozen & 0x80) != 0) {
      // update path metrics for b=0
      pm_list.val[0] =
          vmlsl_u32_s16(pm_list.val[0], vget_low_u16(l_u0), vget_low_u16(cmp));
      pm_list.val[1] = vmlsl_u32_s16(pm_list.val[1], vget_high_u16(l_u0),
                                     vget_high_u16(cmp));

      est_bits = vdupq_n_u8(0); // frozen bits are always zero
    } else {
      // path metric for b=1
      pm_list.val[2] = vaddq_u32(pm_list.val[0], pm_u0_b1.val[0]);
      pm_list.val[3] = vaddq_u32(pm_list.val[1], pm_u0_b1.val[1]);

      // path metric for b=0
      pm_list.val[0] =
          vmlsl_u32_s16(pm_list.val[0], vget_low_u16(l_u0), vget_low_u16(cmp));
      pm_list.val[1] = vmlsl_u32_s16(pm_list.val[1], vget_high_u16(l_u0),
                                     vget_high_u16(cmp));

      // set high L=4 bytes to [u0,u1]=0b10=2.
      est_bits = vcombine_u8(vdup_n_u8(0), vdup_n_u8(0b10));

      // sort to keep low L=8 out of 2*L=16 paths.
      sort_decoder_entries<8>((uint8_t *)&est_bits, (uint32_t *)&pm_list,
                              (uint8_t *)&hist);

      // permute 8-byte chunks of as/bs to match new hist.
      // unpack L=8 histories from every low byte, splat over N=2-byte chunks.
      uint8x16_t h16 = vzip1q_u8(hist, hist);

      // permute LLRs (as/bs) by history permute vector.
      uint8x16_t h_ofs0 = {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1};
      h_ofs0 = vsliq_n_u8(h_ofs0, h16, 1);
      a = vreinterpretq_s16_u8(vqtbl1q_u8(vreinterpretq_u8_s16(a), h_ofs0));
      b = vreinterpretq_s16_u8(vqtbl1q_u8(vreinterpretq_u8_s16(b), h_ofs0));
    }

    // g(a, b, c=0) = a + b
    // g(a, b, c=1) = a - b
    int16x8_t ab_add = vqaddq_s16(b, a);
    int16x8_t ab_sub = vqsubq_s16(b, a);
    uint16x8_t est_bit0_16 = vmovl_u8(vget_low_u8(est_bits));
    int16x8_t l_u1 = vbslq_s16(vceqzq_u16(est_bit0_16), ab_add, ab_sub);
    cmp = vcltzq_s16(l_u1);
    uint16x8_t l_u1_abs = vreinterpretq_u16_s16(vqabsq_s16(l_u1));

    // pm[b == g(a,b,c)<0] = |l_u1|
    // pm[b != g(a,b,c)<0] = 0
    temp = vbicq_u16(l_u1_abs, cmp);
    uint32x4x2_t pm_u1_b1;
    pm_u1_b1.val[0] = vmovl_u16(vget_low_u16(temp));
    pm_u1_b1.val[1] = vmovl_high_u16(temp);

    // right bit estimation
    if ((frozen & 0x8000) != 0) {
      // path metric for b=0
      pm_list.val[0] = vmlsl_u32_s16(pm_list.val[0], vget_low_u16(l_u1_abs),
                                     vget_low_u16(cmp));
      pm_list.val[1] = vmlsl_u32_s16(pm_list.val[1], vget_high_u16(l_u1_abs),
                                     vget_high_u16(cmp));

    } else {
      // path metric for b=1
      pm_list.val[2] = vaddq_u32(pm_list.val[0], pm_u1_b1.val[0]);
      pm_list.val[3] = vaddq_u32(pm_list.val[1], pm_u1_b1.val[1]);

      // path metric for b=0
      pm_list.val[0] = vmlsl_u32_s16(pm_list.val[0], vget_low_u16(l_u1_abs),
                                     vget_low_u16(cmp));
      pm_list.val[1] = vmlsl_u32_s16(pm_list.val[1], vget_high_u16(l_u1_abs),
                                     vget_high_u16(cmp));

      est_bits = vcombine_u8(vget_low_u8(est_bits), vget_low_u8(est_bits));
      hist = vcombine_u8(vget_low_u8(hist), vget_low_u8(hist));

      // est_bits[8+i] = est_bits[i], high bytes equal low bytes
      // set high L=8 bytes to [u0,u1]|=0b01=1.
      est_bits =
          vcombine_u8(vget_low_u8(est_bits), vget_low_u8(est_bits) ^ 0x01);

      sort_decoder_entries<8>((uint8_t *)&est_bits, (uint32_t *)&pm_list,
                              (uint8_t *)&hist);
    }

    // load p_u_seq_out (only eight bytes) and permute by hist
    uint8x8_t u_seq_out = vld1_u8(p_u_seq_out);
    u_seq_out = vtbl1_u8(u_seq_out, vget_low_u8(hist));

    // add estimated bits and store eight bytes
    uint32_t shft_idx = idx % 8;
    u_seq_out |= vshl_u8(vget_low_u8(est_bits), vdup_n_s8(6 - shft_idx));
    vst1_u8(p_u_seq_out, u_seq_out);

    // path metric and history
    *(uint64_t *)hists = vreinterpretq_u64_u8(hist)[0];

    vst1q_u32(pms, pm_list.val[0]);
    vst1q_u32(pms + 4, pm_list.val[1]);

    // combine
    uint8x16_t comb0 = vshrq_n_u8(est_bits, 1) ^ (est_bits & 1);
    uint8x16_t comb1 = est_bits & 1;
    vst1_u8(p_tmp_buf, vget_low_u8(comb0));
    vst1_u8(p_tmp_buf + 8, vget_low_u8(comb1));

    decoder->curr_bit_idx = idx + 2;
  }
};

inline uint8_t __attribute__((always_inline))
estimate_bit(uint32_t frozen, const int32_t l_u, uint32_t idx) {
  return (frozen & 0x80) != 0U ? 0 : static_cast<int>(l_u < 0);
}

template<>
struct polar_stage<2, 1> {
  // special case L=1, since we don't need to do any sorting.
  static inline void __attribute__((always_inline))
  decode(sc_decoder *decoder, const int8_t *p_beliefs_in, uint8_t *p_tmp_buf,
         uint8_t *p_u_seq_out, uint32_t *pms, uint8_t *hists) {
    int8_t a = p_beliefs_in[0];
    int8_t b = p_beliefs_in[1];
    uint32_t idx = decoder->curr_bit_idx;
    uint16_t frozen = *(const uint16_t *)&decoder->frozen_bits_mask[idx];

    // left bit estimation
    int8_t l_u0 = sat_8(sign(a * b) * min(a, b));
    uint8_t est_bit0 = estimate_bit(frozen, l_u0, idx);

    // right bit estimation
    int8_t l_u1 = sat_8(b + (1 - 2 * est_bit0) * a);
    uint8_t est_bit1 = estimate_bit(frozen >> 8, l_u1, idx + 1);

    uint32_t shft_idx = idx % 8;
    uint32_t cw = (est_bit0 << 1) | est_bit1;
    *p_u_seq_out |= cw << (6 - shft_idx);

    // combine
    p_tmp_buf[0] = est_bit0 ^ est_bit1;
    p_tmp_buf[1] = est_bit1;

    decoder->curr_bit_idx = idx + 2;
  }
};

template<int N, int L>
struct polar_stage<N, L, std::enable_if_t<N == 4 || N == 8>> {
  static void decode(sc_decoder *decoder, const int8_t *p_beliefs_in,
                     uint8_t *p_tmp_buf, uint8_t *p_u_seq_out, uint32_t *pms,
                     uint8_t *hist) {
    constexpr int nhalf = N / 2;

    // refine LLRs for left subchild, for each of L decoders
    int8_t new_beliefs[nhalf * L];
    f_l<nhalf, L>(p_beliefs_in, new_beliefs);

    // compute sequence for left subchild, for each of L decoders
    uint8_t comb_vect1[nhalf * L];
    uint8_t hist1[L];
    polar_stage<nhalf, L>::decode(decoder, new_beliefs, comb_vect1, p_u_seq_out,
                                  pms, hist1);
    // updated, correct order: comb_vect1, p_u_seq_out, pms, hist1
    // not updated, now incorrect order: p_beliefs_in

    // refine LLRs for right subchild (using input beliefs and comb_vect1 from
    // left subchild), for each of L decoders
    int8_t g_out[nhalf * L];
    g_l<nhalf, L>(p_beliefs_in, comb_vect1, hist1, g_out);

    // compute sequence for right subchild, for each of L decoders
    uint8_t comb_vect2[nhalf * L];
    uint8_t hist2[L];
    polar_stage<nhalf, L>::decode(decoder, g_out, comb_vect2, p_u_seq_out, pms,
                                  hist2);
    // updated, correct order: comb_vect2, p_u_seq_out, pms, hist2
    // not updated, now incorrect order: comb_vect1, hist1

    // p_tmp_buf can't be nullptr here for N={4,8} since it cannot be called by
    // polar_top_level::decode (which is the only place nullptr can come from).
    combine_l<nhalf, L>(comb_vect1, comb_vect2, p_tmp_buf, hist2);

    // construct new permutation vector for parent
    combine_hist<L>(hist1, hist2, hist);
  }
};

template<int N, int L>
struct polar_stage<N, L,
                   std::enable_if_t<(N == 16 || N == 32 || N == 64 ||
                                     N == 128 || N == 256 || N == 512)>> {
  static void decode(sc_decoder *decoder, const int8_t *p_beliefs_in,
                     uint8_t *p_tmp_buf, uint8_t *p_u_seq_out, uint32_t *pms,
                     uint8_t *hist) {
    constexpr int nhalf = N / 2;
    // the seq array is indexed as 32-bit words, but memcpy takes a number of
    // 8-bit bytes to copy. Each recursive step does half of the work, hence
    // the divisions by two.
    constexpr int nbytes = N / 8;
    int8_t new_beliefs[nhalf * L];
    f_l<nhalf, L>(p_beliefs_in, new_beliefs);

    uint8_t comb_vect1[nhalf * L];
    uint8_t hist1[L];
    uint8_t seq1[nbytes * L / 2] = {};
    polar_stage<nhalf, L>::decode(decoder, new_beliefs, comb_vect1, seq1, pms,
                                  hist1);
    // updated, correct order: comb_vect1, seq1, pms, hist1
    // not updated, now incorrect order: p_beliefs_in

    int8_t g_out[nhalf * L];
    g_l<nhalf, L>(p_beliefs_in, comb_vect1, hist1, g_out);

    uint8_t comb_vect2[nhalf * L];
    uint8_t hist2[L];
    uint8_t seq2[nbytes * L / 2] = {};
    polar_stage<nhalf, L>::decode(decoder, g_out, comb_vect2, seq2, pms, hist2);
    // updated, correct order: comb_vect2, seq2, pms, hist2
    // not updated, now incorrect order: seq1, comb_vect1, hist1

    // we pass p_tmp_buf=nullptr at polar_top_level::decode to avoid the cost
    // of combine if we are going to ignore the result.
    if (p_tmp_buf != nullptr) {
      combine_l<nhalf, L>(comb_vect1, comb_vect2, p_tmp_buf, hist2);
    }

    combine_seq_out<N / 8, L>(seq1, seq2, hist2, p_u_seq_out);
    combine_hist<L>(hist1, hist2, hist);
  }
};

template<int N, int L>
struct polar_top_level {
  static void decode(sc_decoder *decoder, const int8_t *p_beliefs_in,
                     uint8_t *p_u_seq_out) {
    constexpr int nhalf = N / 2;
    // the seq array is indexed as 32-bit words, but memcpy takes a number of
    // 8-bit bytes to copy. Each recursive step does half of the work, hence
    // the divisions by two.
    constexpr int nbytes = N / 8;
    uint32_t pms[L];
    for (int i = 0; i < L; ++i) {
      pms[i] = i != 0 ? 9999 : 0;
    }

    int8_t new_beliefs[nhalf * L];
    if constexpr (L > 1) {
      int8_t new_beliefs_l1[nhalf];
      f<nhalf>(p_beliefs_in, &p_beliefs_in[nhalf], new_beliefs_l1);
      zip1_l<L, nhalf>(new_beliefs_l1, new_beliefs);
    } else {
      f<nhalf>(p_beliefs_in, &p_beliefs_in[nhalf], new_beliefs);
    }

    uint8_t comb_vect1[nhalf * L];
    uint8_t hist1[L];
    uint8_t seq1[nbytes * L / 2] = {};
    polar_stage<nhalf, L>::decode(decoder, new_beliefs, comb_vect1, seq1, pms,
                                  hist1);
    // updated, correct order: comb_vect1, seq1, pms, hist1

    int8_t g_out[nhalf * L];
    if constexpr (L > 1) {
      g_top<nhalf, L>(p_beliefs_in, comb_vect1, g_out);
    } else {
      g<nhalf>(p_beliefs_in, &p_beliefs_in[nhalf], comb_vect1, g_out);
    }

    uint8_t hist2[L];
    uint8_t seq2[nbytes * L / 2] = {};
    polar_stage<nhalf, L>::decode(decoder, g_out, NULL, seq2, pms, hist2);
    // updated, correct order: seq2, pms, hist2
    // not updated, now incorrect order: seq1, comb_vect1, hist1

    for (int l = 0; l < L; ++l) {
      uint8_t h = L > 1 ? hist2[l] : 0;
      uint8_t *seq_out = &p_u_seq_out[l * nbytes];
      const uint8_t *s1 = &seq1[h * nbytes / 2];
      const uint8_t *s2 = &seq2[l * nbytes / 2];
      memcpy(&seq_out[0], s1, nbytes / 2);
      memcpy(&seq_out[nbytes / 2], s2, nbytes / 2);
    }
  }
};

} // namespace

template<int L>
static armral_status
armral_polar_decoder_dispatch(uint32_t n, const uint8_t *frozen,
                              const int8_t *p_llr_in, uint8_t *p_u_seq_out) {
  sc_decoder decoder;
  setup_sc_decoder(&decoder, frozen);
  switch (n) {
  case 32:
    polar_top_level<32, L>::decode(&decoder, p_llr_in, p_u_seq_out);
    break;
  case 64:
    polar_top_level<64, L>::decode(&decoder, p_llr_in, p_u_seq_out);
    break;
  case 128:
    polar_top_level<128, L>::decode(&decoder, p_llr_in, p_u_seq_out);
    break;
  case 256:
    polar_top_level<256, L>::decode(&decoder, p_llr_in, p_u_seq_out);
    break;
  case 512:
    polar_top_level<512, L>::decode(&decoder, p_llr_in, p_u_seq_out);
    break;
  case 1024:
    polar_top_level<1024, L>::decode(&decoder, p_llr_in, p_u_seq_out);
    break;
  default:
    return ARMRAL_ARGUMENT_ERROR;
  }

  return ARMRAL_SUCCESS;
}

armral_status armral_polar_decode_block(uint32_t n, const uint8_t *frozen,
                                        uint32_t l, const int8_t *p_llr_in,
                                        uint8_t *p_u_seq_out) {
  if (l == 1) {
    return armral_polar_decoder_dispatch<1>(n, frozen, p_llr_in, p_u_seq_out);
  }
  if (l == 2) {
    return armral_polar_decoder_dispatch<2>(n, frozen, p_llr_in, p_u_seq_out);
  }
  if (l == 4) {
    return armral_polar_decoder_dispatch<4>(n, frozen, p_llr_in, p_u_seq_out);
  }
  if (l == 8) {
    return armral_polar_decoder_dispatch<8>(n, frozen, p_llr_in, p_u_seq_out);
  }
  return ARMRAL_ARGUMENT_ERROR;
}
