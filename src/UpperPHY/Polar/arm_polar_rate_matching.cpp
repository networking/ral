/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "utils/allocators.hpp"

#include <cassert>
#include <cmath>
#include <cstring>

namespace armral::polar {

static int8_t subblock_interleave_pattern[32] = {
    0,  1,  2,  4,  3,  5,  6,  7,  8,  16, 9,  17, 10, 18, 11, 19,
    12, 20, 13, 21, 14, 22, 15, 23, 24, 25, 26, 28, 27, 29, 30, 31};

static inline void move_bit(const uint8_t *in, int src_idx, uint8_t *out,
                            int dst_idx) {
  uint32_t bit = (in[src_idx / 8] << (src_idx % 8)) & 0x80;
  out[dst_idx / 8] |= bit >> (dst_idx % 8);
}

static void subblock_interleave(int n, const uint8_t *in, uint8_t *out) {
  // performs the sub-block interleaving step of polar encoding, as specified
  // by section 5.4.1.1 of 3GPP TS 38.212 by interleaving 32 blocks of n/32
  // bits according to the sub-block interleaver pattern (table 5.4.1.1-1).
  assert(n % 32 == 0);
  memset((void *)out, 0, n / 8);
  int b = n / 32;
  for (int j = 0; j < n; j += b) {
    int ofs = subblock_interleave_pattern[j / b] * b;
    for (int k = 0; k < b; ++k) {
      move_bit(in, ofs + k, out, j + k);
    }
  }
}

static void bit_selection(int n, int e, int k, const uint8_t *in,
                          uint8_t *out) {
  // performs bit selection by mean of repetition, puncturing or shortening,
  // as specified by section 5.4.1.2 of 3GPP TS 38.212.
  if (e >= n) {
    // repetition
    for (int i = 0; i < e / 8; ++i) {
      out[i] = in[i % (n / 8)];
    }
    if (e % 8 != 0) {
      out[e / 8] = in[(e / 8) % (n / 8)] & (0xFE << (~e & 7));
    }
  } else if (16 * k <= 7 * e) {
    // puncturing
    for (int i = 0; i < e; ++i) {
      move_bit(in, i + (n - e), out, i);
    }
  } else {
    // shortening
    for (int i = 0; i < e; ++i) {
      move_bit(in, i, out, i);
    }
  }
}

static void channel_interleave(int e, const uint8_t *in, uint8_t *out) {
  // performs the channel interleaving step of polar encoding, as specified by
  // section 5.4.1.3 of 3GPP TS 38.212.
  // we choose t such that (t * (t + 1) / 2) ≥ E, which can be expressed
  // exactly as: t = round_up((sqrt(8E + 1) - 1) / 2).
  int t = std::ceil((std::sqrt(8.F * e + 1.F) - 1.F) / 2.F);
  memset((void *)out, 0, (e + 7) / 8);
  int dst_bit = 0;
  for (int col = 0; col < t; ++col) {
    for (int row = 0; row < (t - col); ++row) {
      int src_bit = ((t * (t + 1)) / 2) - ((t - row) * (t - row + 1) / 2) + col;
      if (src_bit < e) {
        move_bit(in, src_bit, out, dst_bit);
        ++dst_bit;
      }
    }
  }
}

template<typename Allocator>
armral_status rate_matching(uint32_t n, uint32_t e, uint32_t k,
                            armral_polar_ibil_type i_bil,
                            const uint8_t *p_d_seq_in, uint8_t *p_f_seq_out,
                            Allocator &allocator) {
  auto p_y_seq = allocate_zeroed<uint8_t>(allocator, n / 8);
  // sub-block interleaving
  armral::polar::subblock_interleave(n, p_d_seq_in, p_y_seq.get());

  if (i_bil == ARMRAL_POLAR_IBIL_DISABLE) {
    memset((void *)p_f_seq_out, 0, (e + 7) / 8);
    // bit selection
    armral::polar::bit_selection(n, e, k, p_y_seq.get(), p_f_seq_out);
  } else {
    auto p_e_seq = allocate_zeroed<uint8_t>(allocator, (e + 7) / 8);
    // bit selection
    armral::polar::bit_selection(n, e, k, p_y_seq.get(), p_e_seq.get());
    // channel interleaving
    armral::polar::channel_interleave(e, p_e_seq.get(), p_f_seq_out);
  }
  return ARMRAL_SUCCESS;
}

} // namespace armral::polar

armral_status armral_polar_rate_matching(uint32_t n, uint32_t e, uint32_t k,
                                         armral_polar_ibil_type i_bil,
                                         const uint8_t *p_d_seq_in,
                                         uint8_t *p_f_seq_out) {
  heap_allocator allocator{};
  return armral::polar::rate_matching(n, e, k, i_bil, p_d_seq_in, p_f_seq_out,
                                      allocator);
}

armral_status armral_polar_rate_matching_noalloc(
    uint32_t n, uint32_t e, uint32_t k, armral_polar_ibil_type i_bil,
    const uint8_t *p_d_seq_in, uint8_t *p_f_seq_out, void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return armral::polar::rate_matching(n, e, k, i_bil, p_d_seq_in, p_f_seq_out,
                                      allocator);
}
