/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "utils/allocators.hpp"

#include <cstdlib>
#include <cstring>

namespace {

template<typename Allocator>
bool polar_crc_check(const uint8_t *data_in, uint32_t k, Allocator &allocator) {
  uint32_t k_bytes = (k + 7) / 8;
  uint32_t buffer_size = k_bytes;
  uint32_t num_pad_bits = 0;

  if (k % 128 != 0) {
    num_pad_bits = 128 - (k % 128);
    buffer_size = (k + num_pad_bits) >> 3;
  }

  auto buffer = allocate_zeroed<uint8_t>(allocator, buffer_size);

  if constexpr (Allocator::is_counting) {
    return true;
  }

  if (k % 128 != 0) {
    // Copy the information bits after the zero-padding bits
    uint32_t idx = num_pad_bits >> 3;
    uint32_t shift1 = num_pad_bits % 8;
    uint32_t shift2 = 8 - shift1;
    buffer[idx] = data_in[0] >> shift1;
    for (uint32_t i = 0; i < k_bytes - 1; i++) {
      buffer[idx + i + 1] = (data_in[i] << shift2) | (data_in[i + 1] >> shift1);
    }
  } else {
    memcpy(buffer.get(), data_in, sizeof(uint8_t) * buffer_size);
  }

  // Generate the CRC parity bits
  uint64_t crc;
  armral_crc24_b_be(buffer_size, (const uint64_t *)buffer.get(), &crc);

  return (crc == 0);
}

} // Anonymous namespace

bool armral_polar_crc_check(const uint8_t *data_in, uint32_t k) {
  heap_allocator allocator{};
  return polar_crc_check(data_in, k, allocator);
}

bool armral_polar_crc_check_noalloc(const uint8_t *data_in, uint32_t k,
                                    void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return polar_crc_check(data_in, k, allocator);
}

uint32_t armral_polar_crc_check_noalloc_buffer_size(uint32_t k) {
  counting_allocator allocator{};
  (void)polar_crc_check(nullptr, k, allocator);
  return allocator.required_bytes();
}
