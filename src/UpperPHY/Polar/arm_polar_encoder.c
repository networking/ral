/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

static inline void __attribute__((always_inline))
polar_encoding_algo_n32(const uint32_t *u, uint32_t *d32) {
  uint32_t x = *u;
  // for an 8-bit segment: abcdefgh
  // a ^= b, c ^= d, e ^= f, g ^= h, ...
  x ^= (x << 1) & 0xaaaaaaaaU; // groups of 1b
  // ab ^= cd, ef ^= gh, ...
  x ^= (x << 2) & 0xccccccccU; // groups of 2b
  // abcd ^= efgh, ...
  x ^= (x << 4) & 0xf0f0f0f0U;  // groups of 4b
  x ^= (x >> 8) & 0x00ff00ffU;  // groups of 8b
  x ^= (x >> 16) & 0x0000ffffU; // groups of 16b
  *d32 = x;
}

static inline void __attribute__((always_inline))
polar_encoding_algo_n64(const uint32_t *u, uint32_t *d64) {
  uint64_t x = *(const uint64_t *)u;
  // for an 8-bit segment: abcdefgh
  // a ^= b, c ^= d, e ^= f, g ^= h, ...
  x ^= (x << 1) & 0xaaaaaaaaaaaaaaaaUL; // groups of 1b
  // ab ^= cd, ef ^= gh, ...
  x ^= (x << 2) & 0xccccccccccccccccUL; // groups of 2b
  // abcd ^= efgh, ...
  x ^= (x << 4) & 0xf0f0f0f0f0f0f0f0UL;  // groups of 4b
  x ^= (x >> 8) & 0x00ff00ff00ff00ffUL;  // groups of 8b
  x ^= (x >> 16) & 0x0000ffff0000ffffUL; // groups of 16b
  // 32-bit big-endian segments, so shift in the opposite direction.
  x ^= (x >> 32) & 0x00000000ffffffffUL; // groups of 32b
  *(uint64_t *)d64 = x;
}

static inline void __attribute__((always_inline))
polar_encoding_algo_n128(const uint32_t *u, uint32_t *d128) {
  uint32x2_t tmp_low;
  uint32x2_t tmp_hi;

  // Computing [dLow] = [uLow]*[G_128Low]
  polar_encoding_algo_n64(&u[0], (uint32_t *)&tmp_low);

  // Computing [dHi] = [uHi]*[G_128Hi]
  polar_encoding_algo_n64(&u[2], (uint32_t *)&tmp_hi);

  tmp_low = veor_u32(tmp_low, tmp_hi);
  vst1q_u32(d128, vcombine_u32(tmp_low, tmp_hi));
}

static inline void __attribute__((always_inline))
polar_encoding_algo_n256(const uint32_t *u, uint32_t *d256) {
  uint32x4_t tmp_low = {0};
  uint32x4_t tmp_hi = {0};

  // Computing [dLow] = [uLow]*[G_256Low]
  polar_encoding_algo_n128(u, (uint32_t *)&tmp_low);

  // Computing [dHi] = [uHi]*[G_256Hi]
  polar_encoding_algo_n128(&u[4], (uint32_t *)&tmp_hi);

  vst1q_u32(d256, veorq_u32(tmp_low, tmp_hi));
  d256 += 4;
  vst1q_u32(d256, tmp_hi);
}

static inline void __attribute__((always_inline))
polar_encoding_algo_n512(const uint32_t *u, uint32_t *d512) {
  uint32x4_t tmp_low[2] = {0};
  uint32x4_t tmp_hi[2] = {0};

  // Computing [dLow] = [uLow]*[G_512Low]
  polar_encoding_algo_n256(u, (uint32_t *)&tmp_low);

  // Computing [dHi] = [uHi]*[G_512Hi]
  polar_encoding_algo_n256((const uint32_t *)&u[8], (uint32_t *)&tmp_hi);

  vst1q_u32(d512, veorq_u32(tmp_low[0], tmp_hi[0]));
  d512 += 4;
  vst1q_u32(d512, veorq_u32(tmp_low[1], tmp_hi[1]));
  d512 += 4;
  vst1q_u32(d512, tmp_hi[0]);
  d512 += 4;
  vst1q_u32(d512, tmp_hi[1]);
}

static inline void __attribute__((always_inline))
polar_encoding_algo_n1024(const uint32_t *u, uint32_t *d1024) {
  uint32x4_t tmp_low[4] = {0};
  uint32x4_t tmp_hi[4] = {0};

  // Computing [dLow] = [uLow]*[G_1024Low]
  polar_encoding_algo_n512(u, (uint32_t *)tmp_low);

  // Computing [dHi] = [uHi]*[G_1024Hi]
  polar_encoding_algo_n512(&u[16], (uint32_t *)tmp_hi);

  vst1q_u32(d1024, veorq_u32(tmp_low[0], tmp_hi[0]));
  d1024 += 4;
  vst1q_u32(d1024, veorq_u32(tmp_low[1], tmp_hi[1]));
  d1024 += 4;
  vst1q_u32(d1024, veorq_u32(tmp_low[2], tmp_hi[2]));
  d1024 += 4;
  vst1q_u32(d1024, veorq_u32(tmp_low[3], tmp_hi[3]));
  d1024 += 4;
  vst1q_u32(d1024, tmp_hi[0]);
  d1024 += 4;
  vst1q_u32(d1024, tmp_hi[1]);
  d1024 += 4;
  vst1q_u32(d1024, tmp_hi[2]);
  d1024 += 4;
  vst1q_u32(d1024, tmp_hi[3]);
}

static inline void __attribute__((always_inline))
polar_encoder_n32(const uint32_t *p_u_seq_in, uint32_t *p_d_seq_out) {
  // [u] = [u(0), u(1), ..., u(31)]
  // Computing [d] = [u]*[G_N]
  polar_encoding_algo_n32(p_u_seq_in, p_d_seq_out);
}

static inline void __attribute__((always_inline))
polar_encoder_n64(const uint32_t *p_u_seq_in, uint32_t *p_d_seq_out) {
  // [u] = [u(0), u(1), ..., u(63)]
  // Computing [d] = [u]*[G_N]
  polar_encoding_algo_n64(p_u_seq_in, p_d_seq_out);
}

static inline void __attribute__((always_inline))
polar_encoder_n128(const uint32_t *p_u_seq_in, uint32_t *p_d_seq_out) {
  // [u] = [u(0), u(1), ..., u(127)]
  // Computing [d] = [u]*[G_N]
  polar_encoding_algo_n128(p_u_seq_in, p_d_seq_out);
}

static inline void __attribute__((always_inline))
polar_encoder_n256(const uint32_t *p_u_seq_in, uint32_t *p_d_seq_out) {
  // [u] = [u(0), u(1), ..., u(255)]
  // Computing [d] = [u]*[G_N]
  polar_encoding_algo_n256(p_u_seq_in, p_d_seq_out);
}

static inline void __attribute__((always_inline))
polar_encoder_n512(const uint32_t *p_u_seq_in, uint32_t *p_d_seq_out) {
  // [u] = [u(0), u(1), ..., u(511)]
  // Computing [d] = [u]*[G_N]
  polar_encoding_algo_n512(p_u_seq_in, p_d_seq_out);
}

static inline void __attribute__((always_inline))
polar_encoder_n1024(const uint32_t *p_u_seq_in, uint32_t *p_d_seq_out) {
  // [u] = [u(0), u(1), ..., u(1023)]
  // Computing [d] = [u]*[G_N]
  polar_encoding_algo_n1024(p_u_seq_in, p_d_seq_out);
}

armral_status armral_polar_encode_block(uint32_t n, const uint8_t *p_u_seq_in,
                                        uint8_t *p_d_seq_out) {
  switch (n) {
  case 32:
    polar_encoder_n32((const uint32_t *)p_u_seq_in, (uint32_t *)p_d_seq_out);
    break;
  case 64:
    polar_encoder_n64((const uint32_t *)p_u_seq_in, (uint32_t *)p_d_seq_out);
    break;
  case 128:
    polar_encoder_n128((const uint32_t *)p_u_seq_in, (uint32_t *)p_d_seq_out);
    break;
  case 256:
    polar_encoder_n256((const uint32_t *)p_u_seq_in, (uint32_t *)p_d_seq_out);
    break;
  case 512:
    polar_encoder_n512((const uint32_t *)p_u_seq_in, (uint32_t *)p_d_seq_out);
    break;
  case 1024:
    polar_encoder_n1024((const uint32_t *)p_u_seq_in, (uint32_t *)p_d_seq_out);
    break;
  default:
    return ARMRAL_ARGUMENT_ERROR;
  }
  return ARMRAL_SUCCESS;
}
