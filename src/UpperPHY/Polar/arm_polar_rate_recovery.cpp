/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "utils/allocators.hpp"

#include <cassert>
#include <cmath>
#include <cstring>

namespace armral::polar {

static int8_t subblock_interleave_pattern[32] = {
    0,  1,  2,  4,  3,  5,  6,  7,  8,  16, 9,  17, 10, 18, 11, 19,
    12, 20, 13, 21, 14, 22, 15, 23, 24, 25, 26, 28, 27, 29, 30, 31};

static void channel_deinterleave(int e, const int8_t *in, int8_t *out) {
  // performs the inverse of the channel interleaving step.  section 5.4.1.3 of
  // 3GPP TS 38.212. note that this function is operating on 8-bit LLR values
  // rather than individual bits.
  int t = std::ceil((std::sqrt(8.F * e + 1.F) - 1.F) / 2.F);
  int src_bit = 0;
  for (int col = 0; col < t; ++col) {
    for (int row = 0; row < (t - col); ++row) {
      int dst_bit = ((t * (t + 1)) / 2) - ((t - row) * (t - row + 1) / 2) + col;
      if (dst_bit < e) {
        out[dst_bit] = in[src_bit];
        ++src_bit;
      }
    }
  }
}

static void llr_recovery(int n, int e, int k, const int8_t *llrs_deint,
                         int8_t *llrs) {
  // run rate recovery
  if (e >= n) {
    // repetition
    for (int i = 0; i < n; i++) {
      llrs[i] = llrs_deint[i];
    }
    for (int i = n; i < e; i++) {
      int32_t comb = (int32_t)llrs[i % n] + (int32_t)llrs_deint[i];
      if (comb < INT8_MIN) {
        llrs[i % n] = INT8_MIN;
      } else if (comb > INT8_MAX) {
        llrs[i % n] = INT8_MAX;
      } else {
        llrs[i % n] = (int8_t)comb;
      }
    }
  } else if (16 * k <= 7 * e) {
    // puncturing
    for (int i = e - 1; i >= 0; --i) {
      llrs[i + n - e] = llrs_deint[i];
    }
    for (int i = n - e - 1; i >= 0; --i) {
      llrs[i] = 0;
    }
  } else {
    // shortening
    for (int i = e - 1; i >= 0; --i) {
      llrs[i] = llrs_deint[i];
    }
    for (int i = n - e - 1; i >= 0; --i) {
      llrs[i + e] = INT8_MAX;
    }
  }
}

static void subblock_deinterleave(int n, const int8_t *in, int8_t *out) {
  // performs the inverse of the sub-block interleaving step. note that this
  // function is operating on 8-bit LLR values rather than individual bits.
  assert(n % 32 == 0);
  int b = n / 32;
  for (int j = 0; j < n; j += b) {
    int ofs = subblock_interleave_pattern[j / b] * b;
    for (int k = 0; k < b; ++k) {
      out[ofs + k] = in[j + k];
    }
  }
}

template<typename Allocator>
armral_status
rate_recovery(uint32_t n, uint32_t e, uint32_t k, armral_polar_ibil_type i_bil,
              const int8_t *p_llr_in, int8_t *p_llr_out, Allocator &allocator) {
  auto p_y_seq = allocate_zeroed<int8_t>(allocator, n);

  if (i_bil == ARMRAL_POLAR_IBIL_DISABLE) {
    // llr recovery
    llr_recovery(n, e, k, p_llr_in, p_y_seq.get());
  } else {
    auto p_e_seq = allocate_zeroed<int8_t>(allocator, e);
    // channel de-interleaving
    channel_deinterleave(e, p_llr_in, p_e_seq.get());
    // llr recovery
    llr_recovery(n, e, k, p_e_seq.get(), p_y_seq.get());
  }

  // sub-block de-interleaving
  subblock_deinterleave(n, p_y_seq.get(), p_llr_out);
  return ARMRAL_SUCCESS;
}

} // namespace armral::polar

armral_status armral_polar_rate_recovery(uint32_t n, uint32_t e, uint32_t k,
                                         armral_polar_ibil_type i_bil,
                                         const int8_t *p_llr_in,
                                         int8_t *p_llr_out) {
  heap_allocator allocator{};
  return armral::polar::rate_recovery(n, e, k, i_bil, p_llr_in, p_llr_out,
                                      allocator);
}

armral_status armral_polar_rate_recovery_noalloc(
    uint32_t n, uint32_t e, uint32_t k, armral_polar_ibil_type i_bil,
    const int8_t *p_llr_in, int8_t *p_llr_out, void *buffer) {
  buffer_bump_allocator allocator{buffer};
  return armral::polar::rate_recovery(n, e, k, i_bil, p_llr_in, p_llr_out,
                                      allocator);
}
