/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#pragma once

#include "arm_polar_decoder.hpp"

namespace {

inline uint8x16_t vld_histq_l8(const uint8_t *hist) {
  return vreinterpretq_u8_u64(vld1q_dup_u64((const uint64_t *)hist));
}

inline uint8x16_t vld_histq_l4(const uint8_t *hist) {
  return vreinterpretq_u8_u32(vld1q_dup_u32((const uint32_t *)hist));
}

inline uint8x8_t vld_hist_l4(const uint8_t *hist) {
  return vreinterpret_u8_u32(vld1_dup_u32((const uint32_t *)hist));
}

inline uint8x16_t vld_histq_l2(const uint8_t *hist) {
  return vreinterpretq_u8_u16(vld1q_dup_u16((const uint16_t *)hist));
}

inline uint8x8_t vld_hist_l2(const uint8_t *hist) {
  return vreinterpret_u8_u16(vld1_dup_u16((const uint16_t *)hist));
}

template<int Nhalf, int L, typename = void>
struct g_l_impl {
  static inline void g_l(const int8_t *in, const uint8_t *dec,
                         const uint8_t *hist, int8_t *out) {
    for (int i = 0; i < Nhalf; ++i) {
      for (int j = 0; j < L; ++j) {
        uint8_t h = L > 1 ? hist[j] : 0;
        int8_t a = in[i * L + h];
        int8_t b = in[(i + Nhalf) * L + h];
        uint8_t c = dec[i * L + j];
        out[i * L + j] = sat_8((int16_t)(b + (1 - 2 * c) * a));
      }
    }
  }
};

template<int Nhalf, int L, int Max_Count>
inline void g_l_x16_loop(const int8_t *in, const uint8_t *dec,
                         const uint8x16_t h8, uint8x16_t xs_idx, int8_t *out) {
  xs_idx += h8;
  for (int i = 0; i < Nhalf; i += Max_Count) {
    int8x16_t as = vld1q_s8(&in[i * L]);
    int8x16_t bs = vld1q_s8(&in[(i + Nhalf) * L]);

    int8x16_t llr1 = vqtbl1q_s8(as, xs_idx);
    int8x16_t llr2 = vqtbl1q_s8(bs, xs_idx);

    uint8x16_t bit = vld1q_u8(&dec[i * L]);

    int8x16_t result =
        vbslq_s8(vceqzq_u8(bit), vqaddq_s8(llr2, llr1), vqsubq_s8(llr2, llr1));
    vst1q_s8(out, result);
    out += 16;
  }
}

template<int Nhalf>
struct g_l_impl<Nhalf, 8, std::enable_if_t<(Nhalf > 2)>> {
  static inline void g_l(const int8_t *in, const uint8_t *dec,
                         const uint8_t *hist, int8_t *out) {
    uint8x16_t h8 = vld_histq_l8(hist);
    uint8x16_t xs_idx = {0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8};
    g_l_x16_loop<Nhalf, 8, 2>(in, dec, h8, xs_idx, out);
  }
};

template<int Nhalf>
struct g_l_impl<Nhalf, 4, std::enable_if_t<(Nhalf > 4)>> {
  static inline void g_l(const int8_t *in, const uint8_t *dec,
                         const uint8_t *hist, int8_t *out) {
    uint8x16_t h8 = vld_histq_l4(hist);
    uint8x16_t xs_idx = {0, 0, 0, 0, 4, 4, 4, 4, 8, 8, 8, 8, 12, 12, 12, 12};
    g_l_x16_loop<Nhalf, 4, 4>(in, dec, h8, xs_idx, out);
  }
};

template<int Nhalf>
struct g_l_impl<Nhalf, 2, std::enable_if_t<(Nhalf >= 8)>> {
  static inline void g_l(const int8_t *in, const uint8_t *dec,
                         const uint8_t *hist, int8_t *out) {
    uint8x16_t h8 = vld_histq_l2(hist);
    uint8x16_t xs_idx = {0, 0, 2, 2, 4, 4, 6, 6, 8, 8, 10, 10, 12, 12, 14, 14};
    g_l_x16_loop<Nhalf, 2, 8>(in, dec, h8, xs_idx, out);
  }
};

inline void g_l_x8(const int8_t *in, const uint8_t *dec, const uint8x8_t h8,
                   uint8x8_t xs_idx, int8_t *out) {
  xs_idx += h8;
  int8x8_t as = vld1_s8(in);
  int8x8_t bs = vld1_s8(&in[8]);

  int8x8_t llr1 = vtbl1_s8(as, vreinterpret_s8_u8(xs_idx));
  int8x8_t llr2 = vtbl1_s8(bs, vreinterpret_s8_u8(xs_idx));

  uint8x8_t bit = vld1_u8(dec);

  int8x8_t result =
      vbsl_s8(vceqz_u8(bit), vqadd_s8(llr2, llr1), vqsub_s8(llr2, llr1));
  vst1_s8(out, result);
}

inline void g_l_x16(const int8_t *in, const uint8_t *dec, const uint8x16_t h8,
                    uint8x16_t xs_idx, int8_t *out) {
  xs_idx += h8;
  int8x16_t as = vld1q_s8(in);
  int8x16_t bs = vld1q_s8(&in[16]);

  int8x16_t llr1 = vqtbl1q_s8(as, xs_idx);
  int8x16_t llr2 = vqtbl1q_s8(bs, xs_idx);

  uint8x16_t bit = vld1q_u8(dec);

  int8x16_t result =
      vbslq_s8(vceqzq_u8(bit), vqaddq_s8(llr2, llr1), vqsubq_s8(llr2, llr1));
  vst1q_s8(out, result);
}

template<>
struct g_l_impl<2, 4> {
  static inline void g_l(const int8_t *in, const uint8_t *dec,
                         const uint8_t *hist, int8_t *out) {
    // specialized N=2-byte chunks interleaved (times L=4).
    uint8x8_t h8 = vld_hist_l4(hist);
    uint8x8_t xs_idx = {0, 0, 0, 0, 4, 4, 4, 4};
    g_l_x8(in, dec, h8, xs_idx, out);
  }
};

template<>
struct g_l_impl<2, 8> {
  static inline void g_l(const int8_t *in, const uint8_t *dec,
                         const uint8_t *hist, int8_t *out) {
    // specialized N=2-byte chunks interleaved (times L=8).
    uint8x16_t h8 = vld_histq_l8(hist);
    uint8x16_t xs_idx = {0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8};
    g_l_x16(in, dec, h8, xs_idx, out);
  }
};

template<>
struct g_l_impl<4, 2> {
  static inline void g_l(const int8_t *in, const uint8_t *dec,
                         const uint8_t *hist, int8_t *out) {
    uint8x8_t h8 = vld_hist_l2(hist);
    uint8x8_t xs_idx = {0, 0, 2, 2, 4, 4, 6, 6};
    g_l_x8(in, dec, h8, xs_idx, out);
  }
};

template<>
struct g_l_impl<4, 4> {
  static inline void g_l(const int8_t *in, const uint8_t *dec,
                         const uint8_t *hist, int8_t *out) {
    // specialized N=4-byte chunks interleaved (times L=4).
    uint8x16_t h8 = vld_histq_l4(hist);
    uint8x16_t xs_idx = {0, 0, 0, 0, 4, 4, 4, 4, 8, 8, 8, 8, 12, 12, 12, 12};
    g_l_x16(in, dec, h8, xs_idx, out);
  }
};

template<int Nhalf, int L>
inline void __attribute__((always_inline))
combine_l(const uint8_t *dec1, const uint8_t *dec2, uint8_t *output,
          const uint8_t *hist) {
  static_assert(Nhalf >= 2);
  if constexpr (L == 8) {
    uint8x16_t h8 = vld_histq_l8(hist);
    uint8x16_t x0_idx = {0, 0, 0, 0, 0, 0, 0, 0, 8, 8, 8, 8, 8, 8, 8, 8};
    x0_idx += h8;
    for (int i = 0; i < Nhalf; i += 2) {
      uint8x16_t x0 = vld1q_u8(dec1);
      uint8x16_t x1 = vld1q_u8(dec2);
      x0 = vqtbl1q_u8(x0, x0_idx);
      vst1q_u8(output, x0 ^ x1);
      vst1q_u8(&output[Nhalf * L], x1);
      dec1 += 16;
      dec2 += 16;
      output += 16;
    }
  } else if constexpr (L == 4 && Nhalf % 4 == 0) {
    uint8x16_t h8 = vld_histq_l4(hist);
    uint8x16_t x0_idx = {0, 0, 0, 0, 4, 4, 4, 4, 8, 8, 8, 8, 12, 12, 12, 12};
    x0_idx += h8;
    for (int i = 0; i < Nhalf; i += 4) {
      uint8x16_t x0 = vld1q_u8(dec1);
      uint8x16_t x1 = vld1q_u8(dec2);
      x0 = vqtbl1q_u8(x0, x0_idx);
      vst1q_u8(output, x0 ^ x1);
      vst1q_u8(&output[Nhalf * L], x1);
      dec1 += 16;
      dec2 += 16;
      output += 16;
    }
  } else if constexpr (L == 2 && Nhalf % 8 == 0) {
    uint8x16_t h8 = vld_histq_l2(hist);
    uint8x16_t x0_idx = {0, 0, 2, 2, 4, 4, 6, 6, 8, 8, 10, 10, 12, 12, 14, 14};
    x0_idx += h8;
    for (int i = 0; i < Nhalf; i += 8) {
      uint8x16_t x0 = vld1q_u8(dec1);
      uint8x16_t x1 = vld1q_u8(dec2);
      x0 = vqtbl1q_u8(x0, x0_idx);
      vst1q_u8(output, x0 ^ x1);
      vst1q_u8(&output[Nhalf * L], x1);
      dec1 += 16;
      dec2 += 16;
      output += 16;
    }
  } else if constexpr (L == 1) {
    for (int i = 0; i < Nhalf; ++i) {
      output[i] = dec1[i] ^ dec2[i];
      output[Nhalf + i] = dec2[i];
    }
  } else {
    uint8_t x0[Nhalf * L];
    uint8_t x1[Nhalf * L];
    for (int j = 0; j < Nhalf; ++j) {
      for (int i = 0; i < L; ++i) {
        uint8_t h = L > 1 ? hist[i] : 0;
        x0[L * j + i] = dec1[L * j + h];
        x1[L * j + i] = dec2[L * j + i];
      }
    }
    for (int i = 0; i < L * Nhalf; ++i) {
      output[i] = x0[i] ^ x1[i];
      output[L * Nhalf + i] = x1[i];
    }
  }
}

template<>
inline void __attribute__((always_inline))
combine_l<2, 2>(const uint8_t *dec1, const uint8_t *dec2, uint8_t *output,
                const uint8_t *hist) {
  uint8x8_t h8 = vld_hist_l2(hist);
  uint8x8_t x0 = vld1s_u8(dec1);
  uint8x8_t x1 = vld1s_u8(dec2);

  uint8x8_t x0_idx = {0, 0, 2, 2, 4, 4, 6, 6};
  x0_idx += h8;
  x0 = vtbl1_u8(x0, x0_idx);

  *(uint32_t *)output = vreinterpret_u32_u8(x0 ^ x1)[0];
  output += 4;
  *(uint32_t *)output = vreinterpret_u32_u8(x1)[0];
}

template<>
inline void __attribute__((always_inline))
combine_l<2, 4>(const uint8_t *dec1, const uint8_t *dec2, uint8_t *output,
                const uint8_t *hist) {
  uint8x8_t h8 = vld_hist_l4(hist);
  uint8x8_t x0 = vld1_u8(dec1);
  uint8x8_t x1 = vld1_u8(dec2);

  uint8x8_t x0_idx = {0, 0, 0, 0, 4, 4, 4, 4};
  x0_idx += h8;
  x0 = vtbl1_u8(x0, x0_idx);

  vst1_u8(output, x0 ^ x1);
  vst1_u8(&output[8], x1);
}

template<>
inline void __attribute__((always_inline))
combine_l<4, 2>(const uint8_t *dec1, const uint8_t *dec2, uint8_t *output,
                const uint8_t *hist) {
  uint8x8_t h8 = vld_hist_l2(hist);
  uint8x8_t x0 = vld1_u8(dec1);
  uint8x8_t x1 = vld1_u8(dec2);

  uint8x8_t x0_idx = {0, 0, 2, 2, 4, 4, 6, 6};
  x0_idx += h8;
  x0 = vtbl1_u8(x0, x0_idx);

  vst1_u8(output, x0 ^ x1);
  vst1_u8(&output[8], x1);
}

template<int N, int L>
inline void combine_seq_out(const uint8_t *seq1, const uint8_t *seq2,
                            const uint8_t *hist2, uint8_t *p_u_seq_out) {
  for (int i = 0; i < L; ++i) {
    uint8_t h = L > 1 ? hist2[i] : 0;
    memcpy((void *)&p_u_seq_out[i * N], (const void *)&seq1[h * N / 2], N / 2);
    memcpy((void *)&p_u_seq_out[i * N + N / 2], (const void *)&seq2[i * N / 2],
           N / 2);
  }
}

template<>
inline void combine_seq_out<2, 2>(const uint8_t *seq1, const uint8_t *seq2,
                                  const uint8_t *hist2, uint8_t *p_u_seq_out) {
  uint8x8_t h = vld1h_u8(hist2);
  uint8x8_t s1 = vtbl1_u8(vld1h_u8(seq1), h);
  uint8x8_t s2 = vld1h_u8(seq2);
  *(uint32_t *)p_u_seq_out = vreinterpret_u32_u8(vzip1_u8(s1, s2))[0];
}

template<>
inline void combine_seq_out<2, 4>(const uint8_t *seq1, const uint8_t *seq2,
                                  const uint8_t *hist2, uint8_t *p_u_seq_out) {
  uint8x8_t h = vld1s_u8(hist2);
  uint8x8_t s1 = vtbl1_u8(vld1s_u8(seq1), h);
  uint8x8_t s2 = vld1s_u8(seq2);
  vst1_u8(p_u_seq_out, vzip1_u8(s1, s2));
}

template<>
inline void combine_seq_out<2, 8>(const uint8_t *seq1, const uint8_t *seq2,
                                  const uint8_t *hist2, uint8_t *p_u_seq_out) {
  uint8x8_t h = vld1_u8(hist2);
  uint8x8_t s1 = vtbl1_u8(vld1_u8(seq1), h);
  uint8x8_t s2 = vld1_u8(seq2);
  vst1q_u8(p_u_seq_out, vzip1l_u8(s1, s2));
}

template<>
inline void combine_seq_out<4, 2>(const uint8_t *seq1, const uint8_t *seq2,
                                  const uint8_t *hist2, uint8_t *p_u_seq_out) {
  uint16x4_t in1 = vld1s_u16((const uint16_t *)seq1);
  uint16x4_t in2 = vld1s_u16((const uint16_t *)seq2);

  uint8x8_t h = vld1h_u8(hist2);
  h = vzip1_u8(h, h);
  uint8x8_t h_ofs0 = {0, 1, 0, 1, 0, 1, 0, 1};
  h_ofs0 = vsli_n_u8(h_ofs0, h, 1);

  in1 = vreinterpret_u16_u8(vtbl1_u8(vreinterpret_u8_u16(in1), h_ofs0));

  vst1_u16((uint16_t *)p_u_seq_out, vzip1_u16(in1, in2));
}

template<>
inline void combine_seq_out<4, 4>(const uint8_t *seq1, const uint8_t *seq2,
                                  const uint8_t *hist2, uint8_t *p_u_seq_out) {
  uint16x4_t in1 = vld1_u16((const uint16_t *)seq1);
  uint16x4_t in2 = vld1_u16((const uint16_t *)seq2);

  uint8x8_t h = vld1s_u8(hist2);
  h = vzip1_u8(h, h);
  uint8x8_t h_ofs0 = {0, 1, 0, 1, 0, 1, 0, 1};
  h_ofs0 = vsli_n_u8(h_ofs0, h, 1);

  in1 = vreinterpret_u16_u8(vtbl1_u8(vreinterpret_u8_u16(in1), h_ofs0));

  vst1q_u16((uint16_t *)p_u_seq_out, vzip1l_u16(in1, in2));
}

template<>
inline void combine_seq_out<4, 8>(const uint8_t *seq1, const uint8_t *seq2,
                                  const uint8_t *hist2, uint8_t *p_u_seq_out) {
  uint16x8_t in1 = vld1q_u16((const uint16_t *)seq1);
  uint16x8_t in2 = vld1q_u16((const uint16_t *)seq2);

  uint8x16_t h = vcombine_u8(vld1_u8(hist2), vdup_n_u8(0));
  h = vzip1q_u8(h, h);
  uint8x16_t h_ofs0 = {0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1, 0, 1};
  h_ofs0 = vsliq_n_u8(h_ofs0, h, 1);

  in1 = vreinterpretq_u16_u8(vqtbl1q_u8(vreinterpretq_u8_u16(in1), h_ofs0));

  vst1q_u16((uint16_t *)p_u_seq_out, vzip1q_u16(in1, in2));
  vst1q_u16((uint16_t *)p_u_seq_out + 8, vzip2q_u16(in1, in2));
}

template<int Length>
inline void g(const int8_t *r1, const int8_t *r2, const uint8_t *dec,
              int8_t *output) {
  // Calculate beliefs for right children in the successive cancellation (SC)
  // algorithm:
  // g(a, b, c=0) = a + b
  // g(a, b, c=1) = a - b
  int16_t l = Length >> 4;
  while (l > 0) {
    int8x16_t llr1 = vld1q_s8(r1);
    int8x16_t llr2 = vld1q_s8(r2);
    uint8x16_t bit = vld1q_u8(dec);
    int8x16_t result =
        vbslq_s8(vceqzq_u8(bit), vqaddq_s8(llr2, llr1), vqsubq_s8(llr2, llr1));
    vst1q_s8(output, result);
    l--;
    r1 += 16;
    r2 += 16;
    dec += 16;
    output += 16;
  }

  if ((Length >> 3) & 1) {
    int8x8_t llr1 = vld1_s8(r1);
    int8x8_t llr2 = vld1_s8(r2);
    uint8x8_t bit = vld1_u8(dec);
    int8x8_t result =
        vbsl_s8(vceqz_u8(bit), vqadd_s8(llr2, llr1), vqsub_s8(llr2, llr1));
    vst1_s8(output, result);
    r1 += 8;
    r2 += 8;
    dec += 8;
    output += 8;
  }

  l = Length & 0x7;
  while (l > 0) {
    int8_t a = *r1++;
    int8_t b = *r2++;
    int8_t c = *dec++;
    *output++ = sat_8((int16_t)(b + (1 - 2 * c) * a));
    l--;
  }
}

// calculate beliefs for left children in SCL algorithm
template<int Length>
inline void f(const int8_t *r1, const int8_t *r2, int8_t *output) {
  int16_t l = Length >> 4;
  while (l > 0) {
    int8x16_t llr1 = vld1q_s8(r1);
    int8x16_t llr2 = vld1q_s8(r2);
    uint8x16_t sign_vect = vcltzq_s8(veorq_s8(llr1, llr2));
    llr1 = vqabsq_s8(llr1);
    llr2 = vqabsq_s8(llr2);
    int8x16_t result = vminq_s8(llr1, llr2);
    int8x16_t result_neg = vnegq_s8(result);
    result = vbslq_s8(sign_vect, result_neg, result);
    vst1q_s8(output, result);
    l--;
    r1 += 16;
    r2 += 16;
    output += 16;
  }

  if ((Length >> 3) & 1) {
    int8x8_t llr1 = vld1_s8(r1);
    int8x8_t llr2 = vld1_s8(r2);
    uint8x8_t sign_vect = vcltz_s8(veor_s8(llr1, llr2));
    llr1 = vqabs_s8(llr1);
    llr2 = vqabs_s8(llr2);
    int8x8_t result = vmin_s8(llr1, llr2);
    int8x8_t result_neg = vneg_s8(result);
    result = vbsl_s8(sign_vect, result_neg, result);
    vst1_s8(output, result);
    r1 += 8;
    r2 += 8;
    output += 8;
  }

  l = Length & 0x7;
  while (l > 0) {
    int8_t a = *r1++;
    int8_t b = *r2++;
    *output++ = sat_8(sign(a * b) * min(a, b));
    l--;
  }
}

template<int L>
inline void combine_hist(const uint8_t *hist1, const uint8_t *hist2,
                         uint8_t *hist) {
  for (int i = 0; i < L; ++i) {
    hist[i] = hist1[hist2[i]];
  }
}

template<>
inline void combine_hist<8>(const uint8_t *hist1, const uint8_t *hist2,
                            uint8_t *hist) {
  uint8x8_t h1 = vld1_u8(hist1);
  uint8x8_t h2 = vld1_u8(hist2);
  uint8x8_t h = vtbl1_u8(h1, h2);
  vst1_u8(hist, h);
}

template<>
inline void combine_hist<4>(const uint8_t *hist1, const uint8_t *hist2,
                            uint8_t *hist) {
  uint8x8_t h1 = vld1s_u8(hist1);
  uint8x8_t h2 = vld1s_u8(hist2);
  uint8x8_t h = vtbl1_u8(h1, h2);
  *(uint32_t *)hist = vreinterpret_u32_u8(h)[0];
}

template<>
inline void combine_hist<2>(const uint8_t *hist1, const uint8_t *hist2,
                            uint8_t *hist) {
  uint8x8_t h1 = vld1h_u8(hist1);
  uint8x8_t h2 = vld1h_u8(hist2);
  uint8x8_t h = vtbl1_u8(h1, h2);
  *(uint16_t *)hist = vreinterpret_u16_u8(h)[0];
}

template<>
inline void combine_hist<1>(const uint8_t * /*hist1*/,
                            const uint8_t * /*hist2*/, uint8_t * /*hist*/) {
  // nothing to do if L=1, only one choice of history.
}

} // namespace
