/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include <cstring>

armral_status armral_polar_subchannel_deinterleave(uint32_t k,
                                                   const uint8_t *frozen,
                                                   const uint8_t *u,
                                                   uint8_t *c) {
  uint32_t val = 0;
  uint32_t dst_bit = 0;
  for (uint32_t src_bit = 0; dst_bit < k; src_bit += 8) {
    uint64_t frozen64 = *(const uint64_t *)&frozen[src_bit];
    uint8_t u_byte = u[src_bit / 8];
    for (uint32_t bit_idx = 0; bit_idx < 8; ++bit_idx) {
      if (((frozen64 >> (bit_idx * 8)) & 0xff) == ARMRAL_POLAR_INFO_BIT) {
        uint16_t idx = 7 - bit_idx;
        uint32_t bit = (u_byte >> idx) & 1;
        val = bit | (val << 1);
        c[dst_bit / 8] = val;
        ++dst_bit;
      }
    }
  }
  if (dst_bit % 8 != 0) {
    c[dst_bit / 8] <<= 8 - (dst_bit % 8);
  }
  return ARMRAL_SUCCESS;
}
