/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "cf32_utils.hpp"
#include "matrix_utils.hpp"

#include <cassert>
#include <complex>
#include <cstdio>
#include <vector>

using armral::utils::allocate_random_cf32_lin_ind;
using armral::utils::check_results_identity;
using armral::utils::check_results_mat_inv;
using armral::utils::gen_hermitian_matrix;
using armral::utils::gen_invertible_matrix;
using armral::utils::print_cmplx_mat;
using armral::utils::reference_matinv_block;

/*
 * Run test for Hermitian Matrix Inversion and Reference for randomly generated
 * input matrix If inputs are random enough then matrix has high probability to
 * have linearly independent rows/cols and thus be invertible.
 */
static bool run_hermitian_matinv_test(uint32_t m, bool enable_id_check,
                                      bool is_hpd, float32_t scale_re = 1.0F,
                                      float32_t scale_im = 1.0F) {
  printf("\n*****  test_hermitian_matrix_%uX%u_rand () [SINGLE] "
         "[armral_cmplx_f32_t], "
         "%s input matrix (scale={%.2f,%.2f}) *****\n",
         m, m, is_hpd ? "HPD" : "Hermitian", scale_re, scale_im);

  // Sample matrix at random and turn Hermitian
  auto a = gen_hermitian_matrix(m, is_hpd, scale_re, scale_im);

  auto ref = allocate_random_cf32_lin_ind(m * m);
  auto res = allocate_random_cf32_lin_ind(m * m);

  // Compute inverse and reference inverse
  reference_matinv_block(m, a.data(), ref.data());
  armral_cmplx_hermitian_mat_inverse_f32(m, a.data(), res.data());

  print_cmplx_mat(" In", m, a.data());
  print_cmplx_mat("Ref", m, ref.data());
  print_cmplx_mat("Res", m, res.data());

  // Perform MM{^-1}=Id test but update result only if enabled.
  bool passed = true;
  if (!check_results_identity(a.data(), res.data(), m) && enable_id_check) {
    passed = false;
  }

  passed &= check_results_mat_inv("RAND_HER_MAT_INV", (float32_t *)res.data(),
                                  (float32_t *)ref.data(), 2 * m * m,
                                  (float32_t)m, (float32_t)m);
  return passed;
}

static bool run_general_matinv_test(uint32_t m, bool enable_id_check,
                                    float32_t scale_re = 1.0F,
                                    float32_t scale_im = 1.0F) {
  printf("\n*****  test_general_mat_inverse_%uX%u_rand () [SINGLE] "
         "[armral_cmplx_f32_t], "
         "input matrix (scale={%.2f,%.2f}) *****\n",
         m, m, scale_re, scale_im);

  auto a = gen_invertible_matrix(m, scale_re, scale_im);

  auto ref = allocate_random_cf32_lin_ind(m * m);
  auto res = allocate_random_cf32_lin_ind(m * m);

  // Compute inverse and reference inverse
  reference_matinv_block(m, a.data(), ref.data());
  armral_cmplx_mat_inverse_f32(m, a.data(), res.data());

  print_cmplx_mat(" In", m, a.data());
  print_cmplx_mat("Ref", m, ref.data());
  print_cmplx_mat("Res", m, res.data());

  // Perform MM{^-1}=Id test but update result only if enabled.
  bool passed = true;
  if (!check_results_identity(a.data(), res.data(), m) && enable_id_check) {
    passed = false;
  }

  passed &= check_results_mat_inv("RAND_MAT_INV", (float32_t *)res.data(),
                                  (float32_t *)ref.data(), 2 * m * m,
                                  (float32_t)m, (float32_t)m);
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;

  // Number of test cases per variants in UT
  int num_reps = 100;

  // Important Note:
  // HPD matrices are very well behaved as opposed to pure Hermitian ones.
  // Among 100 test matrices for each configuration (complex, pure real pure
  // imaginary) we do not see any failure, even in the MM^{-1}=Id checks. That
  // tends to indicate that all submatrices involved in block inversion are
  // sufficiently well behaved. Finally, Cholesky does a good job providing
  // results within the current error tolerance.

  // However, pure Hermitian matrices will fail in about <5% of the cases due
  // to submatrices `A` and/or `E=D-Cinv(A)B` having bad condition numbers or
  // even not being invertible at all. We verified cond(A) and cond(E)
  // manually using python numpy.linalg.inv, which might have resulted in an
  // optimistic estimation. MM^{-1}=Id checks fail very often in the pure
  // Hermitian case, hence they should be disabled for now.
  bool enable_id_check = true;
  std::vector<int> m_hermitian = {2, 3, 4, 8, 16};
  // Random test cases
  for (auto m : m_hermitian) {
    for (int r = 0; r < num_reps; ++r) {
      passed &= run_hermitian_matinv_test(m, enable_id_check, true);
      passed &= run_hermitian_matinv_test(m, enable_id_check, true, 1.0F, 0.0F);
      passed &= run_hermitian_matinv_test(m, enable_id_check, true, 0.0F, 1.0F);
    }
  }

  std::vector<int> m_general = {2, 3, 4, 8, 16};
  for (auto m : m_general) {
    for (int r = 0; r < num_reps; ++r) {
      passed &= run_general_matinv_test(m, true);
      passed &= run_general_matinv_test(m, true, 1.0F, 0.0F);
      passed &= run_general_matinv_test(m, true, 0.0F, 1.0F);
    }
  }

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
