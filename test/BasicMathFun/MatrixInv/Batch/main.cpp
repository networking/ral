/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "cf32_utils.hpp"
#include "matrix_utils.hpp"

#include <cassert>
#include <complex>
#include <cstdio>
#include <vector>

using armral::utils::check_results_identity;
using armral::utils::check_results_mat_inv;
using armral::utils::gen_hermitian_matrix_batch;
using armral::utils::gen_invertible_matrix_batch;
using armral::utils::pack_data;
using armral::utils::print_cmplx_mat;
using armral::utils::reference_matinv_block;
using armral::utils::unpack_data;

/*
 * Run reference Matrix Inversion based on blockwise approach (batched/parallel
 * version)
 */
static void reference_parallel_matinv_block(uint32_t m,
                                            const armral_cmplx_f32_t *a,
                                            armral_cmplx_f32_t *b,
                                            uint32_t batch_size) {

  // Run inversion on each matrix (sequentially)
  for (unsigned batch = 0; batch < batch_size; ++batch) {

    printf(" > Check ref matrix %u/%u\n", batch + 1, batch_size);

    // allocate temporary matrices
    std::vector<armral_cmplx_f32_t> mat(m * m);
    std::vector<armral_cmplx_f32_t> res(m * m);

    // unpack matrix
    unpack_data(batch, batch_size, a, mat.data(), m * m);

    // run inversion on each matrix (sequentially)
    reference_matinv_block(m, mat.data(), res.data());

    // pack result
    pack_data(batch, batch_size, res.data(), b, m * m);
  }
}

/*
 * Run test for Batch Hermitian Matrix Inversion and Reference for randomly
 * generated input matrix
 */
static bool run_batch_hermitian_matinv_test(uint32_t batch_size, uint32_t m,
                                            bool is_hpd,
                                            float32_t scale_re = 1.0F,
                                            float32_t scale_im = 1.0F) {
  printf("\n*****  test_batch_hermitian_matrix_%uX%u_rand () [BATCH] "
         "[armral_cmplx_f32_t], %s input matrix (scale={%.2f,%.2f}) *****\n",
         m, m, is_hpd ? "HPD" : "Hermitian", scale_re, scale_im);

  assert(m == 2 || m == 3 || m == 4);

  // Generate and pack batch of matrices
  std::vector<armral_cmplx_f32_t> ref(batch_size * m * m);
  std::vector<armral_cmplx_f32_t> res(batch_size * m * m);
  const auto a =
      gen_hermitian_matrix_batch(batch_size, m, is_hpd, scale_re, scale_im);

  reference_parallel_matinv_block(m, a.data(), ref.data(), batch_size);
  armral_cmplx_hermitian_mat_inverse_batch_f32(batch_size, m, a.data(),
                                               res.data());

  print_cmplx_mat(" In", m, a.data(), batch_size);
  print_cmplx_mat("Ref", m, ref.data(), batch_size);
  print_cmplx_mat("Res", m, res.data(), batch_size);

  bool passed = true;
  for (unsigned batch = 0; batch < batch_size; ++batch) {

    printf(" > Check res matrix %u/%u\n", batch + 1, batch_size);
    // unpack
    std::vector<armral_cmplx_f32_t> src(m * m);
    std::vector<armral_cmplx_f32_t> dst(m * m);
    unpack_data(batch, batch_size, a.data(), src.data(), m * m);
    unpack_data(batch, batch_size, res.data(), dst.data(), m * m);

    // Perform MM{^-1}=Id test but update result only if enabled.
    if (!check_results_identity(src.data(), dst.data(), m)) {
      passed = false; // GCOVR_EXCL_LINE
    }
  }
  passed &= check_results_mat_inv(
      "RAND_PARA_HER_MAT_INV", (float32_t *)res.data(), (float32_t *)ref.data(),
      batch_size * 2 * m * m, (float32_t)m, (float32_t)m);
  return passed;
}

/*
 * Run test for Batch Pointer-Array Hermitian Matrix Inversion and Reference
 * for randomly generated input matrix
 */
static bool run_batch_pa_hermitian_matinv_test(uint32_t batch_size, uint32_t m,
                                               bool is_hpd,
                                               float32_t scale_re = 1.0F,
                                               float32_t scale_im = 1.0F) {
  printf("\n*****  test_batch_pa_hermitian_matrix_%uX%u_rand () [BATCH PA] "
         "[armral_cmplx_f32_t], %s input matrix (scale={%.2f,%.2f}) *****\n",
         m, m, is_hpd ? "HPD" : "Hermitian", scale_re, scale_im);

  assert(m == 2 || m == 3 || m == 4);

  // Generate and pack batch of matrices
  std::vector<armral_cmplx_f32_t> ref(batch_size * m * m);
  std::vector<armral_cmplx_f32_t> res(batch_size * m * m);
  const auto a =
      gen_hermitian_matrix_batch(batch_size, m, is_hpd, scale_re, scale_im);

  std::vector<const armral_cmplx_f32_t *> a_ptrs(m * m);
  std::vector<armral_cmplx_f32_t *> res_ptrs(m * m);
  for (unsigned i = 0; i < m * m; ++i) {
    a_ptrs[i] = &a[batch_size * i];
    res_ptrs[i] = &res[batch_size * i];
  }

  reference_parallel_matinv_block(m, a.data(), ref.data(), batch_size);
  armral_cmplx_hermitian_mat_inverse_batch_f32_pa(batch_size, m, a_ptrs.data(),
                                                  res_ptrs.data());

  print_cmplx_mat(" In", m, a.data(), batch_size);
  print_cmplx_mat("Ref", m, ref.data(), batch_size);
  print_cmplx_mat("Res", m, res.data(), batch_size);

  bool passed = true;
  for (unsigned batch = 0; batch < batch_size; ++batch) {

    printf(" > Check res matrix %u/%u\n", batch + 1, batch_size);
    // unpack
    std::vector<armral_cmplx_f32_t> src(m * m);
    std::vector<armral_cmplx_f32_t> dst(m * m);
    unpack_data(batch, batch_size, a.data(), src.data(), m * m);
    unpack_data(batch, batch_size, res.data(), dst.data(), m * m);

    // Perform MM{^-1}=Id test but update result only if enabled.
    if (!check_results_identity(src.data(), dst.data(), m)) {
      passed = false; // GCOVR_EXCL_LINE
    }
  }
  passed &= check_results_mat_inv(
      "RAND_PARA_HER_MAT_INV", (float32_t *)res.data(), (float32_t *)ref.data(),
      batch_size * 2 * m * m, (float32_t)m, (float32_t)m);
  return passed;
}

/*
 * Run test for Batch General Matrix Inversion and Reference for randomly
 * generated input matrix
 */
static bool run_batch_matinv_test(uint32_t batch_size, uint32_t m,
                                  float32_t scale_re = 1.0F,
                                  float32_t scale_im = 1.0F) {
  printf(
      "\n*****  test_batch_matrix_%uX%u_rand () [BATCH] "
      "[armral_cmplx_f32_t], general input matrix (scale={%.2f,%.2f}) *****\n",
      m, m, scale_re, scale_im);

  assert(m == 2 || m == 3 || m == 4);

  // Generate and pack batch of matrices
  std::vector<armral_cmplx_f32_t> ref(batch_size * m * m);
  std::vector<armral_cmplx_f32_t> res(batch_size * m * m);
  const auto a = gen_invertible_matrix_batch(batch_size, m, scale_re, scale_im);

  reference_parallel_matinv_block(m, a.data(), ref.data(), batch_size);
  armral_cmplx_mat_inverse_batch_f32(batch_size, m, a.data(), res.data());

  print_cmplx_mat(" In", m, a.data(), batch_size);
  print_cmplx_mat("Ref", m, ref.data(), batch_size);
  print_cmplx_mat("Res", m, res.data(), batch_size);

  bool passed = true;
  for (unsigned batch = 0; batch < batch_size; ++batch) {

    printf(" > Check res matrix %u/%u\n", batch + 1, batch_size);
    // unpack
    std::vector<armral_cmplx_f32_t> src(m * m);
    std::vector<armral_cmplx_f32_t> dst(m * m);
    unpack_data(batch, batch_size, a.data(), src.data(), m * m);
    unpack_data(batch, batch_size, res.data(), dst.data(), m * m);

    // Perform MM{^-1}=Id test but update result only if enabled.
    if (!check_results_identity(src.data(), dst.data(), m)) {
      passed = false; // GCOVR_EXCL_LINE
    }
  }
  passed &= check_results_mat_inv(
      "RAND_PARA_GEN_MAT_INV", (float32_t *)res.data(), (float32_t *)ref.data(),
      batch_size * 2 * m * m, (float32_t)m, (float32_t)m);
  return passed;
}

/*
 * Run test for Batch Pointer-Array General Matrix Inversion and Reference
 * for randomly generated input matrix
 */
static bool run_batch_pa_matinv_test(uint32_t batch_size, uint32_t m,
                                     float32_t scale_re = 1.0F,
                                     float32_t scale_im = 1.0F) {
  printf(
      "\n*****  test_batch_pa_matrix_%uX%u_rand () [BATCH PA] "
      "[armral_cmplx_f32_t], general input matrix (scale={%.2f,%.2f}) *****\n",
      m, m, scale_re, scale_im);

  assert(m == 2 || m == 3 || m == 4);

  // Generate and pack batch of matrices
  std::vector<armral_cmplx_f32_t> ref(batch_size * m * m);
  std::vector<armral_cmplx_f32_t> res(batch_size * m * m);
  const auto a = gen_invertible_matrix_batch(batch_size, m, scale_re, scale_im);

  std::vector<const armral_cmplx_f32_t *> a_ptrs(m * m);
  std::vector<armral_cmplx_f32_t *> res_ptrs(m * m);
  for (unsigned i = 0; i < m * m; ++i) {
    a_ptrs[i] = &a[batch_size * i];
    res_ptrs[i] = &res[batch_size * i];
  }

  reference_parallel_matinv_block(m, a.data(), ref.data(), batch_size);
  armral_cmplx_mat_inverse_batch_f32_pa(batch_size, m, a_ptrs.data(),
                                        res_ptrs.data());

  print_cmplx_mat(" In", m, a.data(), batch_size);
  print_cmplx_mat("Ref", m, ref.data(), batch_size);
  print_cmplx_mat("Res", m, res.data(), batch_size);

  bool passed = true;
  for (unsigned batch = 0; batch < batch_size; ++batch) {

    printf(" > Check res matrix %u/%u\n", batch + 1, batch_size);
    // unpack
    std::vector<armral_cmplx_f32_t> src(m * m);
    std::vector<armral_cmplx_f32_t> dst(m * m);
    unpack_data(batch, batch_size, a.data(), src.data(), m * m);
    unpack_data(batch, batch_size, res.data(), dst.data(), m * m);

    // Perform MM{^-1}=Id test but update result only if enabled.
    if (!check_results_identity(src.data(), dst.data(), m)) {
      passed = false; // GCOVR_EXCL_LINE
    }
  }
  passed &= check_results_mat_inv(
      "RAND_PARA_GEN_MAT_INV", (float32_t *)res.data(), (float32_t *)ref.data(),
      batch_size * 2 * m * m, (float32_t)m, (float32_t)m);
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;

  // Number of test cases per variants in UT
  int num_reps = 3;

  // Important Note:
  // HPD matrices are very well behaved as opposed to pure Hermitian ones.
  // However, pure Hermitian matrices will fail in about <5% of the cases due
  // to submatrices `A` and/or `E=D-Cinv(A)B` having bad condition numbers or
  // even not being invertible at all. We verified cond(A) and cond(E)
  // manually using python numpy.linalg.inv, which might have resulted in an
  // optimistic estimation.

  // Random test cases
  std::vector<unsigned> m_hermitian = {2, 3, 4};
  for (auto m : m_hermitian) {
    // Minimum number of matrices in batch
    // Required because m=3 and m=4 unroll by b=4,
    // while m=2 unroll by b=2
    // and none of them handle num_mats % b != 0
    unsigned b = m > 2 ? 4 : 2;
    for (unsigned num_mats : {b, b * 2, b * 7, b * 100}) {
      for (int r = 0; r < num_reps; ++r) {
        // generates HPD matrices
        passed &= run_batch_hermitian_matinv_test(num_mats, m, true);
        passed &=
            run_batch_hermitian_matinv_test(num_mats, m, true, 1.0F, 0.0F);
        passed &=
            run_batch_hermitian_matinv_test(num_mats, m, true, 0.0F, 1.0F);
        passed &= run_batch_pa_hermitian_matinv_test(num_mats, m, true);
        passed &=
            run_batch_pa_hermitian_matinv_test(num_mats, m, true, 1.0F, 0.0F);
        passed &=
            run_batch_pa_hermitian_matinv_test(num_mats, m, true, 0.0F, 1.0F);
      }
    }
  }
  std::vector<unsigned> m_general = {2, 3, 4};
  for (auto m : m_general) {
    unsigned b = m > 2 ? 4 : 2;
    for (unsigned num_mats : {1U, m, m + 1, b * 2, b * 7, b * 100}) {
      for (int r = 0; r < num_reps; ++r) {
        passed &= run_batch_matinv_test(num_mats, m);
        passed &= run_batch_matinv_test(num_mats, m, 1.0F, 0.0F);
        passed &= run_batch_matinv_test(num_mats, m, 0.0F, 1.0F);
        passed &= run_batch_pa_matinv_test(num_mats, m);
        passed &= run_batch_pa_matinv_test(num_mats, m, 1.0F, 0.0F);
        passed &= run_batch_pa_matinv_test(num_mats, m, 0.0F, 1.0F);
      }
    }
  }

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
