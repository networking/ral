/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "qint64.hpp"

#define NAME "VECDOT armral_cmplx_int16_t 32"

static bool run_vec_dot_test(uint32_t num_samples) {
  // restrict min/max to avoid hitting saturation in accumulator.
  constexpr armral_cmplx_int16_t min = {-4096, -4096};
  constexpr armral_cmplx_int16_t max = {4095, 4095};
  armral::utils::cs16_random random;
  const auto a = random.vector(num_samples, min, max);
  const auto b = random.vector(num_samples, min, max);
  auto c = random.one();

  printf("[" NAME "] - %u samples\n", num_samples);

  armral_cmplx_vecdot_i16_32bit(num_samples, a.data(), b.data(), &c);

  std::complex<armral::utils::qint64_t> acc;
  for (uint32_t i = 0; i < num_samples; ++i) {
    acc += armral::utils::cmplx_mul_widen_cs16(a[i], b[i]);
  }
  armral_cmplx_int16_t ref{(acc.real() >> 16).get16(),
                           (acc.imag() >> 16).get16()};

  return armral::utils::check_results_cs16(NAME, &c, &ref, 1);
}

int main(int argc, char **argv) {
  std::vector<uint32_t> params;
  for (uint32_t i = 1; i <= 33; ++i) {
    params.push_back(i);
  }
  params.push_back(64);
  params.push_back(100);
  params.push_back(115);
  params.push_back(128);
  params.push_back(151);
  params.push_back(256);
  params.push_back(512);
  params.push_back(1024);
  bool passed = true;
  for (const auto &n : params) {
    passed &= run_vec_dot_test(n);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
