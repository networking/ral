/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cf32_utils.hpp"

#define NAME "VECDOT armral_cmplx_f32_t"

static bool run_vec_dot_test(uint32_t num_samples) {
  armral::utils::cf32_random random;
  const auto a = random.vector(num_samples);
  const auto b = random.vector(num_samples);
  auto c = random.one();

  printf("[" NAME "] - %u samples\n", num_samples);

  armral_cmplx_vecdot_f32(num_samples, a.data(), b.data(), &c);

  std::complex<double> acc;
  for (uint32_t i = 0; i < num_samples; ++i) {
    acc += armral::utils::cmplx_mul_widen_cf32(a[i], b[i]);
  }
  armral_cmplx_f32_t ref{(float32_t)acc.real(), (float32_t)acc.imag()};

  return armral::utils::check_results_cf32(NAME, &c, &ref, 1);
}

int main(int argc, char **argv) {
  const uint32_t params[] = {
      1, 2, 3, 4, 5, 7, 8, 15, 16, 32, 64, 100, 128, 151, 256, 512, 1024,
  };
  bool passed = true;
  for (const auto &n : params) {
    passed &= run_vec_dot_test(n);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
