/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "qint64.hpp"

#define NAME "VECDOT armral_cmplx_int16_t IQ 32"

static bool run_vec_dot_test(uint32_t num_samples) {
  // restrict min/max to avoid hitting saturation in accumulator.
  constexpr armral_cmplx_int16_t min = {-4096, -4096};
  constexpr armral_cmplx_int16_t max = {4095, 4095};
  armral::utils::cs16_random random;
  const auto a = random.vector(num_samples, min, max);
  const auto b = random.vector(num_samples, min, max);
  auto c = random.vector(1);

  printf("[" NAME "] - %u samples\n", num_samples);

  const auto a_re = armral::utils::unpack_real_cs16(a);
  const auto a_im = armral::utils::unpack_imag_cs16(a);
  const auto b_re = armral::utils::unpack_real_cs16(b);
  const auto b_im = armral::utils::unpack_imag_cs16(b);
  auto c_re = armral::utils::unpack_real_cs16(c);
  auto c_im = armral::utils::unpack_imag_cs16(c);

  armral_cmplx_vecdot_i16_2_32bit(num_samples, a_re.data(), a_im.data(),
                                  b_re.data(), b_im.data(), c_re.data(),
                                  c_im.data());
  c = armral::utils::pack_cs16(c_re, c_im);

  std::complex<armral::utils::qint64_t> acc;
  for (uint32_t i = 0; i < num_samples; ++i) {
    acc += armral::utils::cmplx_mul_widen_cs16(a[i], b[i]);
  }
  armral_cmplx_int16_t ref{(acc.real() >> 16).get16(),
                           (acc.imag() >> 16).get16()};

  return armral::utils::check_results_cs16(NAME, c.data(), &ref, 1);
}

int main(int argc, char **argv) {
  const uint32_t params[] = {
      1, 2, 3, 4, 5, 7, 8, 15, 16, 32, 64, 100, 128, 151, 256, 512, 1024,
  };
  bool passed = true;
  for (const auto &n : params) {
    passed &= run_vec_dot_test(n);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
