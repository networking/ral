/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cf32_utils.hpp"
#include "cs16_utils.hpp"
#include "int_utils.hpp"
#include "qint64.hpp"

#include <vector>

static bool check_tolerance_cs16(const char *name, const int16_t *result,
                                 const int16_t *expected, uint32_t n,
                                 int16_t tolerance) {
  bool passed = true;
  for (uint32_t i = 0; i < n; ++i) {
    if (std::abs(result[i] - expected[i]) > tolerance) {
      // GCOVR_EXCL_START
      passed = false;
      printf("Error! [%s] result[%u]= %d and expected[%u] = %d\n", name, i,
             result[i], i, expected[i]);
      // GCOVR_EXCL_STOP
    }
  }

  printf("[%s] - check result: %s\n", name, passed ? "OK" : "ERROR");

  return passed;
}

static bool check_tolerance_cs16(const char *name,
                                 const armral_cmplx_int16_t *result,
                                 const armral_cmplx_int16_t *expected,
                                 uint32_t n, int16_t tolerance) {
  return check_tolerance_cs16(name, (const int16_t *)result,
                              (const int16_t *)expected, n * 2, tolerance);
}

template<int X, int Y>
struct solver;

template<>
struct solver<1, 2> {
  static constexpr auto fn = armral_solve_1x2_f32;
};

template<>
struct solver<1, 4> {
  static constexpr auto fn = armral_solve_1x4_f32;
};

template<>
struct solver<2, 2> {
  static constexpr auto fn = armral_solve_2x2_f32;
};

template<>
struct solver<2, 4> {
  static constexpr auto fn = armral_solve_2x4_f32;
};

template<>
struct solver<4, 4> {
  static constexpr auto fn = armral_solve_4x4_f32;
};

static std::complex<double> convert_cf64_cs16(armral_cmplx_int16_t x,
                                              armral_fixed_point_index i) {
  int sh = (int)i; // number of decimal bits
  std::complex<double> ret{(double)x.re, (double)x.im};
  return ret / (double)(1 << sh);
}

static armral_cmplx_int16_t convert_cs16_cf64(std::complex<double> x,
                                              armral_fixed_point_index i) {
  int sh = (int)i; // number of decimal bits
  x *= (1 << sh);
  armral::utils::qint64_t re{(int64_t)x.real()};
  armral::utils::qint64_t im{(int64_t)x.imag()};
  return {re.get16(), im.get16()};
}

template<int X, int Y>
static void
run_reference_solve(uint32_t num_sub_carrier, uint32_t sc_per_g,
                    const armral_fixed_point_index *p_y_num_fract_bits,
                    const armral_cmplx_int16_t *p_y, uint32_t p_ystride,
                    float32_t *p_g_real, float32_t *p_g_imag,
                    uint32_t p_gstride,
                    armral_fixed_point_index num_fract_bits_x,
                    armral_cmplx_int16_t *p_x, uint32_t p_xstride) {
  int total_cols = num_sub_carrier;
  for (int j = 0; j < total_cols; ++j) {
    float32_t *g_re = &p_g_real[j / sc_per_g];
    float32_t *g_im = &p_g_imag[j / sc_per_g];
    for (int i = 0; i < X; ++i) {
      std::complex<double> acc = 0;
      for (int k = 0; k < Y; ++k) {
        std::complex<double> g_elem{g_re[(i * Y + k) * p_gstride],
                                    g_im[(i * Y + k) * p_gstride]};
        armral_cmplx_int16_t y_elem = p_y[j + k * p_ystride];
        acc += g_elem * convert_cf64_cs16(y_elem, p_y_num_fract_bits[k]);
      }
      p_x[i * p_xstride + j] = convert_cs16_cf64(acc, num_fract_bits_x);
    }
  }
}

template<int X, int Y>
static bool run_solve_test(int sc_per_g, int num_samples,
                           bool use_zero_x_stride, bool use_zero_y_stride,
                           bool use_zero_g_stride) {
  assert(num_samples % 12 == 0);
  constexpr int x_rows = X;
  constexpr int y_rows = Y;
  int num_blocks = num_samples / sc_per_g;
  int g_stride = use_zero_g_stride ? 0 : num_blocks;
  int x_stride_cmplx = use_zero_x_stride ? 0 : num_samples;
  int y_stride_cmplx = use_zero_y_stride ? 0 : num_samples;

  printf(
      "[Solve] sc_per_g=%d num_samples=%d G=%dx%d strides(g,x,y)=(%d,%d,%d)\n",
      sc_per_g, num_samples, X, Y, g_stride, x_stride_cmplx, y_stride_cmplx);

  // These tests are pretty numerically unstable when the multiplier for the
  // input vector (y) is significantly larger than the output vector (x) due to
  // the tests/impl using different intermediate storage formats and fused
  // versus unfused arithmetic.
  armral::utils::int_random<uint8_t> random_u8;
  auto num_fract_bits_x = (armral_fixed_point_index)random_u8.one(0, 15);
  std::vector<armral_fixed_point_index> num_fract_bits_y(Y);
  for (int i = 0; i < Y; ++i) {
    int min_y = std::max(0, (int)num_fract_bits_x - 6);
    num_fract_bits_y[i] = (armral_fixed_point_index)random_u8.one(min_y, 15);
  }

  // arrangement is batches of num_samples, each row/col separated.
  armral::utils::cs16_random random_cs16;
  auto x = random_cs16.vector(num_samples * x_rows);
  auto y = random_cs16.vector(num_samples * y_rows);

  // arrangement is batches of num_blocks, each row/col separated.
  auto g = armral::utils::allocate_random_cf32(x_rows * y_rows * num_blocks);

  auto g_real = armral::utils::unpack_real_cf32(g);
  auto g_imag = armral::utils::unpack_imag_cf32(g);

  auto x_ref = x;
  run_reference_solve<X, Y>(num_samples, sc_per_g, num_fract_bits_y.data(),
                            y.data(), y_stride_cmplx, g_real.data(),
                            g_imag.data(), g_stride, num_fract_bits_x,
                            x_ref.data(), x_stride_cmplx);

  int y_stride_real = use_zero_y_stride ? 0 : num_samples * 2;
  if constexpr (X > 1) {
    int x_stride_real = use_zero_x_stride ? 0 : num_samples * 2;
    solver<X, Y>::fn(num_samples, sc_per_g, y.data(), y_stride_real,
                     num_fract_bits_y.data(), g_real.data(), g_imag.data(),
                     g_stride, x.data(), x_stride_real, num_fract_bits_x);
  } else {
    solver<X, Y>::fn(num_samples, sc_per_g, y.data(), y_stride_real,
                     num_fract_bits_y.data(), g_real.data(), g_imag.data(),
                     g_stride, x.data(), num_fract_bits_x);
  }

  return check_tolerance_cs16("SOLVE", x.data(), x_ref.data(), x.size(), 1);
}

int main(int argc, char **argv) {
  bool passed = true;
  // We test these a few times since there's quite a bit of choice in terms
  // of fixed point multiplier inputs and strides to vary.
  for (bool use_zero_x_stride : {false, true}) {
    for (bool use_zero_y_stride : {false, true}) {
      for (bool use_zero_g_stride : {false, true}) {
        for (int num_samples = 12; num_samples <= 144; num_samples += 12) {
          for (int sc_per_g : {1, 4, 6}) {
            passed &=
                run_solve_test<1, 2>(sc_per_g, num_samples, use_zero_x_stride,
                                     use_zero_y_stride, use_zero_g_stride);
            passed &=
                run_solve_test<1, 4>(sc_per_g, num_samples, use_zero_x_stride,
                                     use_zero_y_stride, use_zero_g_stride);
            passed &=
                run_solve_test<2, 2>(sc_per_g, num_samples, use_zero_x_stride,
                                     use_zero_y_stride, use_zero_g_stride);
            passed &=
                run_solve_test<2, 4>(sc_per_g, num_samples, use_zero_x_stride,
                                     use_zero_y_stride, use_zero_g_stride);
            passed &=
                run_solve_test<4, 4>(sc_per_g, num_samples, use_zero_x_stride,
                                     use_zero_y_stride, use_zero_g_stride);
          }
        }
      }
    }
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
