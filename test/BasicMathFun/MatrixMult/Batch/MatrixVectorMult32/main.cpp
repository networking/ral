/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "cf32_utils.hpp"
#include "matrix_utils.hpp"

static void reference_batch_matvecmul_cf32(uint16_t num_mats,
                                           uint16_t vecs_per_mat, uint16_t m,
                                           uint16_t n,
                                           const armral_cmplx_f32_t *a,
                                           const armral_cmplx_f32_t *x,
                                           armral_cmplx_f32_t *ref) {
  auto total_vectors = num_mats * vecs_per_mat;
  for (unsigned mat = 0; mat < num_mats; ++mat) {
    for (unsigned vec = 0; vec < vecs_per_mat; ++vec) {
      std::vector<armral_cmplx_f32_t> single_a(m * n);
      std::vector<armral_cmplx_f32_t> single_x(n);
      std::vector<armral_cmplx_f32_t> single_ref(m);

      auto vec_batch_start = mat * vecs_per_mat + vec;

      // unpack a, x, and ref into local buffers
      armral::utils::unpack_data(mat, num_mats, a, single_a.data(), m * n);
      armral::utils::unpack_data(vec_batch_start, total_vectors, x,
                                 single_x.data(), n);
      armral::utils::unpack_data(vec_batch_start, total_vectors, ref,
                                 single_ref.data(), m);

      // do one mxn matrix-vector multiplication
      armral::utils::reference_matmul_cf32(m, 1, n, single_a.data(),
                                           single_x.data(), single_ref.data());

      // pack the answer back into ref for comparison with the batched results
      armral::utils::pack_data(vec_batch_start, total_vectors,
                               single_ref.data(), ref, m);
    }
  }
}

static bool run_general_matvecmul_batch_test(uint16_t num_mats,
                                             uint16_t vecs_per_mat, uint16_t m,
                                             uint16_t n) {
  const char *name = "MATVECMULBATCH armral_cmplx_f32_t";
  auto total_vectors = num_mats * vecs_per_mat;
  armral::utils::cf32_random random;
  const auto a = random.vector(num_mats * m * n);
  const auto x = random.vector(total_vectors * n);
  auto y = random.vector(total_vectors * m);
  auto ref = y;

  printf("[%s] - num_mats %u vecs_per_mat %u dimension %u %u\n", name, num_mats,
         vecs_per_mat, m, n);

  // call the batched routine
  armral_cmplx_mat_vec_mult_batch_f32(num_mats, vecs_per_mat, m, n, a.data(),
                                      x.data(), y.data());

  // and call the reference version
  reference_batch_matvecmul_cf32(num_mats, vecs_per_mat, m, n, a.data(),
                                 x.data(), ref.data());

  return armral::utils::check_results_cf32(name, y.data(), ref.data(),
                                           total_vectors * m);
}

static bool run_general_matvecmul_batch_pa_test(uint16_t num_mats,
                                                uint16_t vecs_per_mat,
                                                uint16_t m, uint16_t n) {
  const char *name = "MATVECMULBATCHPA armral_cmplx_f32_t";
  auto total_vectors = num_mats * vecs_per_mat;
  armral::utils::cf32_random random;
  const auto a = random.vector(num_mats * m * n);
  const auto x = random.vector(total_vectors * n);
  auto y = random.vector(total_vectors * m);
  auto ref = y;

  printf("[%s] - num_mats %u vecs_per_mat %u dimension %u %u\n", name, num_mats,
         vecs_per_mat, m, n);

  // construct the pointer arrays into the input data
  std::vector<const armral_cmplx_f32_t *> a_ptrs(m * n);
  std::vector<const armral_cmplx_f32_t *> x_ptrs(n);
  std::vector<armral_cmplx_f32_t *> y_ptrs(m);
  for (int i = 0; i < m * n; ++i) {
    a_ptrs[i] = &a[num_mats * i];
  }
  for (int i = 0; i < n; i++) {
    x_ptrs[i] = &x[total_vectors * i];
  }
  for (int i = 0; i < m; i++) {
    y_ptrs[i] = &y[total_vectors * i];
  }

  // call the batched routine
  armral_cmplx_mat_vec_mult_batch_f32_pa(num_mats, vecs_per_mat, m, n,
                                         a_ptrs.data(), x_ptrs.data(),
                                         y_ptrs.data());

  // and call the reference version
  reference_batch_matvecmul_cf32(num_mats, vecs_per_mat, m, n, a.data(),
                                 x.data(), ref.data());

  return armral::utils::check_results_cf32(name, y.data(), ref.data(),
                                           total_vectors * m);
}

// Entry point for unit testing for cf32 batched matrix-vector multiplication
int main(int argc, char **argv) {
  bool passed = true;

  std::vector<unsigned> num_vecs = {1, 3, 4, 12, 24};
  std::vector<unsigned> mats = {1, 4, 12, 24};

  for (auto num_mats : mats) {
    for (auto vecs_per_mat : num_vecs) {
      if ((num_mats * vecs_per_mat) % 12 != 0) {
        continue;
      }
      for (unsigned m = 1; m <= 16; ++m) {
        for (unsigned n = 1; n <= 16; ++n) {
          passed &=
              run_general_matvecmul_batch_test(num_mats, vecs_per_mat, m, n);
          passed &=
              run_general_matvecmul_batch_pa_test(num_mats, vecs_per_mat, m, n);
        }
      }
    }
  }

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
