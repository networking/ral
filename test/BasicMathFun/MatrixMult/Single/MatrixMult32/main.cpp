/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cf32_utils.hpp"
#include "reference_linalg.hpp"

using armral::utils::cf32_random;
using armral::utils::check_results_cf32;
using armral::utils::pack_cf32;
using armral::utils::reference_matmul_cf32;
using armral::utils::unpack_imag_cf32;
using armral::utils::unpack_real_cf32;

static bool run_general_matmul_test(uint16_t m, uint16_t n, uint16_t k) {
  cf32_random random;
  const auto a = random.vector(m * k);
  const auto b = random.vector(k * n);
  auto c = random.vector(m * n);
  auto ref = c;

  reference_matmul_cf32(m, n, k, a.data(), b.data(), ref.data());
  armral_cmplx_matmul_f32(m, n, k, a.data(), b.data(), c.data());
  return check_results_cf32("MATMUL armral_cmplx_f32_t", c.data(), ref.data(),
                            m * n);
}

static bool run_specific_2x2_matmul_test() {
  cf32_random random;
  const auto a = random.vector(4);
  const auto b = random.vector(4);
  auto c = random.vector(4);
  auto ref = c;

  // note: the a/b flip is intentional since all matrices are given transposed.
  //       i.e. C = A^T * B^T = (B * A)^T
  reference_matmul_cf32(2, 2, 2, b.data(), a.data(), ref.data());
  armral_cmplx_mat_mult_2x2_f32(a.data(), b.data(), c.data());
  return check_results_cf32("MATMUL armral_cmplx_f32_t 2x2", c.data(),
                            ref.data(), 4);
}

static bool run_specific_2x2_iq_matmul_test() {
  cf32_random random;
  const auto a = random.vector(4);
  const auto b = random.vector(4);
  auto ref = random.vector(4);

  // note: the a/b flip is intentional since all matrices are given transposed.
  //       i.e. C = A^T * B^T = (B * A)^T
  reference_matmul_cf32(2, 2, 2, b.data(), a.data(), ref.data());

  const auto a_re = unpack_real_cf32(a);
  const auto a_im = unpack_imag_cf32(a);
  const auto b_re = unpack_real_cf32(b);
  const auto b_im = unpack_imag_cf32(b);
  std::vector<float32_t> c_re(4);
  std::vector<float32_t> c_im(4);
  armral_cmplx_mat_mult_2x2_f32_iq(a_re.data(), a_im.data(), b_re.data(),
                                   b_im.data(), c_re.data(), c_im.data());
  const auto c = pack_cf32(c_re, c_im);
  return check_results_cf32("MATMUL armral_cmplx_f32_t 2x2 IQ", c.data(),
                            ref.data(), 4);
}

static bool run_specific_4x4_matmul_test() {
  cf32_random random;
  const auto a = random.vector(16);
  const auto b = random.vector(16);
  auto c = random.vector(16);
  auto ref = c;

  // note: the a/b flip is intentional since all matrices are given transposed.
  //       i.e. C = A^T * B^T = (B * A)^T
  reference_matmul_cf32(4, 4, 4, b.data(), a.data(), ref.data());
  armral_cmplx_mat_mult_4x4_f32(a.data(), b.data(), c.data());
  return check_results_cf32("MATMUL armral_cmplx_f32_t 4x4", c.data(),
                            ref.data(), 16);
}

static bool run_specific_4x4_iq_matmul_test() {
  cf32_random random;
  const auto a = random.vector(16);
  const auto b = random.vector(16);
  auto ref = random.vector(16);

  // note: the a/b flip is intentional since all matrices are given transposed.
  //       i.e. C = A^T * B^T = (B * A)^T
  reference_matmul_cf32(4, 4, 4, b.data(), a.data(), ref.data());

  const auto a_re = unpack_real_cf32(a);
  const auto a_im = unpack_imag_cf32(a);
  const auto b_re = unpack_real_cf32(b);
  const auto b_im = unpack_imag_cf32(b);
  std::vector<float32_t> c_re(16);
  std::vector<float32_t> c_im(16);
  armral_cmplx_mat_mult_4x4_f32_iq(a_re.data(), a_im.data(), b_re.data(),
                                   b_im.data(), c_re.data(), c_im.data());
  const auto c = pack_cf32(c_re, c_im);
  return check_results_cf32("MATMUL armral_cmplx_f32_t 4x4 IQ", c.data(),
                            ref.data(), 16);
}

int main(int argc, char **argv) {
  bool passed = true;
  for (unsigned m = 1; m <= 16; m++) {
    for (unsigned n = 1; n <= 16; ++n) {
      for (unsigned k = 1; k <= 16; ++k) {
        passed &= run_general_matmul_test(m, n, k);
      }
    }
  }
  const uint16_t sizes[] = {32, 64, 128, 255};
  for (uint16_t n : sizes) {
    for (uint16_t k : sizes) {
      passed &= run_general_matmul_test(4, n, k);
      passed &= run_general_matmul_test(9, n, k);
    }
  }
  passed &= run_specific_2x2_matmul_test();
  passed &= run_specific_2x2_iq_matmul_test();
  passed &= run_specific_4x4_matmul_test();
  passed &= run_specific_4x4_iq_matmul_test();
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
