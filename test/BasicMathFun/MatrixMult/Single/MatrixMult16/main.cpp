/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "reference_linalg.hpp"
#include <vector>

namespace {
template<typename CmplxMatmulMatchingFunction>
bool run_general_matmul_test_64(
    char const *name, uint16_t m, uint16_t n, uint16_t k,
    CmplxMatmulMatchingFunction cmplx_matmul_matching_under_test) {
  armral::utils::cs16_random random;
  const auto a = random.vector(m * k);
  const auto b = random.vector(k * n);
  auto c = random.vector(m * n);
  auto ref = c;

  printf("[%s] - dimension %u %u %u\n", name, m, n, k);

  armral::utils::reference_matmul_cs16(m, n, k, a.data(), b.data(), ref.data(),
                                       0);
  cmplx_matmul_matching_under_test(m, n, k, a.data(), b.data(), c.data());
  return armral::utils::check_results_cs16(name, c.data(), ref.data(), m * n);
}

template<typename CmplxMatmulMatchingFunction>
bool run_general_matmul_test_32(
    char const *name, uint16_t m, uint16_t n, uint16_t k,
    CmplxMatmulMatchingFunction cmplx_matmul_matching_under_test) {
  // choose min/max values to avoid hitting saturation on the problems
  // we care about (m,n,k <= 16).
  constexpr armral_cmplx_int16_t min = {-4096, -4096};
  constexpr armral_cmplx_int16_t max = {4095, 4095};
  armral::utils::cs16_random random;
  const auto a = random.vector(m * k, min, max);
  const auto b = random.vector(k * n, min, max);
  auto c = random.vector(m * n, min, max);
  auto ref = c;

  printf("[%s] - dimension %u %u %u\n", name, m, n, k);

  armral::utils::reference_matmul_cs16(m, n, k, a.data(), b.data(), ref.data(),
                                       0);
  cmplx_matmul_matching_under_test(m, n, k, a.data(), b.data(), c.data());
  return armral::utils::check_results_cs16(name, c.data(), ref.data(), m * n);
}

template<typename CmplxMatmulMatchingFunction>
bool run_all_tests_64bit(
    char const *name,
    CmplxMatmulMatchingFunction cmplx_matmul_matching_under_test) {
  bool passed = true;
  for (uint16_t m = 1; m <= 16; ++m) {
    for (uint16_t n = 1; n <= 16; ++n) {
      for (uint16_t k = 1; k <= 16; ++k) {
        passed &= run_general_matmul_test_64(name, m, n, k,
                                             cmplx_matmul_matching_under_test);
      }
    }
  }
  return passed;
}

template<typename CmplxMatmulMatchingFunction>
bool run_all_tests_32bit(
    char const *name,
    CmplxMatmulMatchingFunction cmplx_matmul_matching_under_test) {
  bool passed = true;
  for (uint16_t m = 1; m <= 16; ++m) {
    for (uint16_t n = 1; n <= 16; ++n) {
      for (uint16_t k = 1; k <= 16; ++k) {
        passed &= run_general_matmul_test_32(name, m, n, k,
                                             cmplx_matmul_matching_under_test);
      }
    }
  }
  return passed;
}
} // anonymous namespace

// Entry point for unit testing for 16-bit matrix multiplication
int main(int argc, char **argv) {
  bool passed = true;

  passed &= run_all_tests_64bit("MATMUL64 armral_cmplx_int16_t",
                                armral_cmplx_matmul_i16);
  passed &= run_all_tests_64bit(
      "MATMUL64 armral_cmplx_int16_t NoAlloc",
      [](uint16_t m, uint16_t n, uint16_t k, auto... args) {
        std::vector<int16_t> buffer(k * n * sizeof(armral_cmplx_int16_t));
        return armral_cmplx_matmul_i16_noalloc(m, n, k, args..., buffer.data());
      });

  passed &= run_all_tests_32bit("MATMUL32 armral_cmplx_int16_t",
                                armral_cmplx_matmul_i16_32bit);

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
