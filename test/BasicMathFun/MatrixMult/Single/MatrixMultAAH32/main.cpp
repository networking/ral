/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cf32_utils.hpp"
#include "matrix_utils.hpp"
#include "reference_linalg.hpp"

static bool run_matmul_aah_cf32_test(uint16_t m, uint16_t n) {
  armral::utils::cf32_random random;
  const auto a = random.vector(m * n);
  auto c = random.vector(m * m);
  auto ref = c;

  armral::utils::reference_matmul_aah_cf32(m, n, a.data(), ref.data());
  armral_cmplx_matmul_aah_f32(m, n, a.data(), c.data());
  printf("%ix%i -> %ix%i\n", m, n, m, m);

  // Each element in c is computed by a length-n complex dot product
  return armral::utils::check_results_cf32("MATMUL_AAH armral_cmplx_f32_t",
                                           c.data(), ref.data(), m * m,
                                           armral::utils::cmplx_dot_nflops(n));
}

int main(int argc, char **argv) {
  bool passed = true;
  for (int m = 1; m <= 16; m++) {
    for (int n = 1; n <= 16; n++) {
      passed &= run_matmul_aah_cf32_test(m, n);
    }
  }

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
