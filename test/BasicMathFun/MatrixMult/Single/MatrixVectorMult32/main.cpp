/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cf32_utils.hpp"
#include "reference_linalg.hpp"

static bool run_general_matvecmul_test(uint16_t m, uint16_t n) {
  armral::utils::cf32_random random;
  const auto a = random.vector(m * n);
  const auto x = random.vector(n);
  auto y = random.vector(m);
  auto ref = y;

  armral::utils::reference_matmul_cf32(m, 1, n, a.data(), x.data(), ref.data());
  armral_cmplx_mat_vec_mult_f32(m, n, a.data(), x.data(), y.data());
  return armral::utils::check_results_cf32("MATVECMUL armral_cmplx_f32_t",
                                           y.data(), ref.data(), m);
}

int main(int argc, char **argv) {
  bool passed = true;
  for (unsigned m = 1; m <= 16; m++) {
    for (unsigned n = 1; n <= 16; ++n) {
      passed &= run_general_matvecmul_test(m, n);
    }
  }
  const uint16_t cols[] = {32, 64, 128, 256};
  for (uint16_t n : cols) {
    passed &= run_general_matvecmul_test(4, n);
    passed &= run_general_matvecmul_test(8, n);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
