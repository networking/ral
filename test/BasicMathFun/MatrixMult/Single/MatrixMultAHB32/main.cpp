/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include <array>
#include <cstdio>

#include "cf32_utils.hpp"
#include "matrix_utils.hpp"
#include "reference_linalg.hpp"

static bool run_matmul_ahb_cf32_test(uint16_t m, uint16_t n, uint16_t k) {

  const char *name = "MATMUL_AHB armral_cmplx_f32_t";
  printf("[%s] m=%d n=%d k=%d\n", name, m, n, k);

  armral::utils::cf32_random random;
  const auto a = random.flip_signs(random.vector(k * m));
  const auto b = random.flip_signs(random.vector(k * n));
  auto output = random.vector(m * n);
  auto reference_output = output;

  armral::utils::reference_matmul_ahb_cf32(m, n, k, a.data(), b.data(),
                                           reference_output.data());

  armral_cmplx_matmul_ahb_f32(m, n, k, a.data(), b.data(), output.data());

  // Each element in the output is computed by a length-k complex dot product
  return armral::utils::check_results_cf32(name, output.data(),
                                           reference_output.data(), m * n,
                                           armral::utils::cmplx_dot_nflops(k));
}

int main() {
  bool passed = true;
  for (unsigned m = 1; m <= 16; m++) {
    for (unsigned n = 1; n <= 16; ++n) {
      for (unsigned k = 1; k <= 16; ++k) {
        passed &= run_matmul_ahb_cf32_test(m, n, k);
      }
    }
  }
  std::array m_sizes{32, 64, 128, 256};
  std::array nk_sizes{2, 3, 4, 8, 16};
  for (auto m : m_sizes) {
    for (auto nk : nk_sizes) {
      // Larger A matrix, square B matrix
      passed &= run_matmul_ahb_cf32_test(m, nk, nk);
      // Larger A matrix, rectangular B matrix
      passed &= run_matmul_ahb_cf32_test(m, nk, nk * 2);
    }
  }

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
