/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "reference_linalg.hpp"

static bool run_general_matvecmul_test_64(uint16_t m, uint16_t n) {
  const char *name = "MATVECMUL64 armral_cmplx_int16_t";
  armral::utils::cs16_random random;
  const auto a = random.vector(m * n);
  const auto x = random.vector(n);
  auto y = random.vector(m);
  auto ref = y;

  printf("[%s] - dimension %u %u\n", name, m, n);

  armral::utils::reference_matmul_cs16(m, 1, n, a.data(), x.data(), ref.data(),
                                       0);
  armral_cmplx_mat_vec_mult_i16(m, n, a.data(), x.data(), y.data());
  return armral::utils::check_results_cs16(name, y.data(), ref.data(), m);
}

static bool run_general_matvecmul_test_32(uint16_t m, uint16_t n) {
  const char *name = "MATVECMUL32 armral_cmplx_int16_t";
  // choose min/max values to avoid hitting saturation on the problems
  // we care about (m,n <= 16).
  constexpr armral_cmplx_int16_t min = {-4096, -4096};
  constexpr armral_cmplx_int16_t max = {4095, 4095};
  armral::utils::cs16_random random;
  const auto a = random.vector(m * n, min, max);
  const auto x = random.vector(n, min, max);
  auto y = random.vector(m, min, max);
  auto ref = y;

  printf("[%s] - dimension %u %u\n", name, m, n);

  armral::utils::reference_matmul_cs16(m, 1, n, a.data(), x.data(), ref.data(),
                                       0);
  armral_cmplx_mat_vec_mult_i16_32bit(m, n, a.data(), x.data(), y.data());
  return armral::utils::check_results_cs16(name, y.data(), ref.data(), m);
}

// Entry point for unit testing for 16-bit matrix-vector multiplication
int main(int argc, char **argv) {
  bool passed = true;
  for (unsigned m = 1; m <= 16; ++m) {
    for (unsigned n = 1; n <= 16; ++n) {
      passed &= run_general_matvecmul_test_64(m, n);
    }
  }
  for (unsigned m = 1; m <= 16; ++m) {
    for (unsigned n = 1; n <= 16; ++n) {
      passed &= run_general_matvecmul_test_32(m, n);
    }
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
