/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "int_utils.hpp"
#include "qint64.hpp"

#define NAME "VECMUL armral_cmplx_int16_t IQ"

static bool run_vec_mul_test(uint32_t num_samples) {
  constexpr armral_cmplx_int16_t min = {-4096, -4096};
  constexpr armral_cmplx_int16_t max = {4095, 4095};
  armral::utils::cs16_random random_cs16;
  const auto a = random_cs16.vector(num_samples, min, max);
  const auto b = random_cs16.vector(num_samples, min, max);
  auto c = random_cs16.vector(num_samples, min, max);
  auto ref = c;

  const auto a_re = armral::utils::unpack_real_cs16(a);
  const auto a_im = armral::utils::unpack_imag_cs16(a);
  const auto b_re = armral::utils::unpack_real_cs16(b);
  const auto b_im = armral::utils::unpack_imag_cs16(b);
  auto c_re = armral::utils::unpack_real_cs16(c);
  auto c_im = armral::utils::unpack_imag_cs16(c);

  printf("[" NAME "] - %u samples\n", num_samples);

  armral_cmplx_vecmul_i16_2(num_samples, a_re.data(), a_im.data(), b_re.data(),
                            b_im.data(), c_re.data(), c_im.data());
  c = armral::utils::pack_cs16(c_re, c_im);

  for (uint32_t i = 0; i < num_samples; ++i) {
    std::complex<armral::utils::qint64_t> res =
        armral::utils::cmplx_mul_widen_cs16(a[i], b[i]);
    // add one to intermediate result to ensure correct rounding
    ref[i].re = (((res.real() >> 14) + 1) >> 1).get16();
    ref[i].im = (((res.imag() >> 14) + 1) >> 1).get16();
  }

  return armral::utils::check_results_cs16(NAME, c.data(), ref.data(),
                                           num_samples);
}

static bool vec_mul_single_val_test(uint32_t num_samples,
                                    armral_cmplx_int16_t a_val,
                                    armral_cmplx_int16_t b_val) {
  // In this test, the values in a vector of length num_samples are set to the
  // same value. This is useful when testing saturation, as we want to be able
  // to force saturation of real and imaginary components. We need different
  // lengths to check that the arithmetic in different unrolled loops in the
  // implementation is correct.

  const auto a_re = std::vector<int16_t>(num_samples, a_val.re);
  const auto a_im = std::vector<int16_t>(num_samples, a_val.im);
  const auto b_re = std::vector<int16_t>(num_samples, b_val.re);
  const auto b_im = std::vector<int16_t>(num_samples, b_val.im);

  auto c = armral::utils::allocate_random_cs16(num_samples);

  armral::utils::int_random<int16_t> random_i16;
  auto c_re = random_i16.vector(num_samples);
  auto c_im = random_i16.vector(num_samples);

  armral_cmplx_vecmul_i16_2(num_samples, a_re.data(), a_im.data(), b_re.data(),
                            b_im.data(), c_re.data(), c_im.data());
  c = armral::utils::pack_cs16(c_re, c_im);

  std::complex<armral::utils::qint64_t> res =
      armral::utils::cmplx_mul_widen_cs16(a_val, b_val);
  armral_cmplx_int16_t res_int16;
  res_int16.re = (((res.real() >> 14) + 1) >> 1).get16();
  res_int16.im = (((res.imag() >> 14) + 1) >> 1).get16();
  std::vector<armral_cmplx_int16_t> ref(num_samples, res_int16);
  if (!armral::utils::check_results_cs16(NAME, c.data(), ref.data(),
                                         num_samples)) {
    // GCOVR_EXCL_START
    printf("Error for saturating multiplication with values:\n\t "
           "(%d + %di) * (%d + %di)\n",
           a_val.re, a_val.im, b_val.re, b_val.im);
    return false;
    // GCOVR_EXCL_STOP
  }
  return true;
}

static bool run_vec_mul_saturation_test(uint32_t num_samples) {
  std::vector<std::pair<armral_cmplx_int16_t, armral_cmplx_int16_t>> vals = {
      // real component of multiplication > INT16_MAX
      {{INT16_MIN, INT16_MIN}, {INT16_MIN, INT16_MAX}},
      // real component of multiplication < INT16_MIN
      {{INT16_MIN, INT16_MIN}, {INT16_MAX, INT16_MIN}},
      // imaginary component of multiplication > INT16_MAX and <= INT32_MAX
      {{INT16_MAX, INT16_MAX}, {INT16_MAX, INT16_MAX}},
      // imaginary component of multiplication > INT32_MAX
      {{INT16_MIN, INT16_MIN}, {INT16_MIN, INT16_MIN}},
      // imaginary component of multiplication < INT16_MIN
      {{INT16_MIN, INT16_MIN}, {INT16_MAX, INT16_MAX}},
  };

  printf("[" NAME "] saturating - %u samples\n", num_samples);
  bool passed = true;
  for (const auto &[a, b] : vals) {
    passed &= vec_mul_single_val_test(num_samples, a, b);
  }

  return passed;
}

int main(int argc, char **argv) {
  const uint32_t params[] = {
      1, 2, 3, 4, 5, 7, 8, 15, 16, 32, 64, 100, 128, 151, 256, 512, 1024,
  };
  bool passed = true;
  for (const auto &n : params) {
    passed &= run_vec_mul_test(n);
  }
  const uint32_t saturation_len[] = {
      1, 3, 4, 5, 8, 16, 17,
  };
  for (auto n : saturation_len) {
    passed &= run_vec_mul_saturation_test(n);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
