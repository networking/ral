/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2023-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cf32_utils.hpp"
#include "matrix_utils.hpp"
#include "reference_linalg.hpp"

#include <tuple>
#include <vector>

static inline void
reference_left_pseudo_inverse_direct(uint32_t m, uint32_t n, float32_t lambda,
                                     const armral_cmplx_f32_t *__restrict p_src,
                                     armral_cmplx_f32_t *p_dst) {
  // Compute C = A^H * A
  // We can use p_dst as an intermediate N-by-N array since it has size N-by-M,
  // and N < M
  auto *mat_aha = p_dst;
  armral::utils::reference_matmul_aha_cf32(m, n, p_src, mat_aha);

  // Compute C + lambda * I
  for (uint32_t i = 0; i < n; i++) {
    uint32_t idx = i * n + i;
    mat_aha[idx].re += lambda;
  }

  // Compute B = C^(-1)
  std::vector<armral_cmplx_f32_t> mat_inv(n * n);
  if (n == 1) {
    mat_inv[0].re = 1.F / mat_aha[0].re;
    mat_inv[0].im = 0.F;
  } else {
    armral::utils::reference_matinv_block(n, mat_aha, mat_inv.data());
  }

  // Compute B * A^H
  armral::utils::reference_matmul_bah_cf32(m, n, p_src, mat_inv.data(), p_dst);
}

static inline void reference_right_pseudo_inverse_direct(
    uint32_t m, uint32_t n, float32_t lambda,
    const armral_cmplx_f32_t *__restrict p_src, armral_cmplx_f32_t *p_dst) {
  // Compute C = A * A^H
  // We can use p_dst as an intermediate M-by-M array since it has size N-by-M,
  // and N >= M
  auto *mat_aah = p_dst;
  armral::utils::reference_matmul_aah_cf32(m, n, p_src, mat_aah);

  // Compute C + lambda * I
  for (uint32_t i = 0; i < m; i++) {
    uint32_t idx = i * m + i;
    mat_aah[idx].re += lambda;
  }

  // Compute B = C^(-1)
  std::vector<armral_cmplx_f32_t> mat_inv(m * m);
  if (m == 1) {
    mat_inv[0].re = 1.F / mat_aah[0].re;
    mat_inv[0].im = 0.F;
  } else {
    armral::utils::reference_matinv_block(m, mat_aah, mat_inv.data());
  }

  // Compute A^H * B
  armral::utils::reference_matmul_ahb_cf32(n, m, m, p_src, mat_inv.data(),
                                           p_dst);
}

static inline void
reference_pseudo_inverse_direct(uint32_t m, uint32_t n, float32_t lambda,
                                const armral_cmplx_f32_t *__restrict p_src,
                                armral_cmplx_f32_t *p_dst) {
  if (m > n) {
    return reference_left_pseudo_inverse_direct(m, n, lambda, p_src, p_dst);
  }
  return reference_right_pseudo_inverse_direct(m, n, lambda, p_src, p_dst);
}

template<typename PseudoInverseFunction>
static bool run_pseudo_inverse_direct_cf32_test(
    const char *name, uint32_t m, uint32_t n, float32_t lambda,
    PseudoInverseFunction pseudo_inverse_under_test) {
  armral::utils::cf32_random random;
  const auto src = random.flip_signs(random.vector(m * n));
  std::vector<armral_cmplx_f32_t> ans(n * m);
  std::vector<armral_cmplx_f32_t> ref(n * m);

  pseudo_inverse_under_test(m, n, lambda, src.data(), ans.data());

  reference_pseudo_inverse_direct(m, n, lambda, src.data(), ref.data());

  return armral::utils::check_results_cf32(name, ans.data(), ref.data(), n * m,
                                           armral::utils::cmplx_dot_nflops(n));
}

template<typename PseudoInverseFunction>
bool run_all_tests(char const *test_name, char const *function_name,
                   PseudoInverseFunction pseudo_inverse_under_test) {
  bool passed = true;

  const std::tuple<uint32_t, uint32_t, float32_t> params[] = {
      {1, 1, 0.186745},   {1, 21, -0.314205},  {1, 66, 1.495806},
      {1, 121, 0.0},      {2, 5, -0.968591},   {2, 84, 0.191647},
      {2, 2, 1.457848},   {2, 67, 0.0},        {3, 18, -1.218053},
      {3, 138, 1.597186}, {3, 3, -1.2435186},  {3, 161, 0.0},
      {4, 20, -0.474817}, {4, 105, 0.944802},  {4, 4, 1.645646},
      {4, 94, 0.0},       {8, 35, -1.991369},  {8, 200, -1.244298},
      {8, 8, 1.445767},   {8, 190, 0.0},       {16, 32, 0.809352},
      {16, 80, 1.810591}, {16, 16, -0.426745}, {16, 117, 0.0}};
  for (const auto &[dim1, dim2, l] : params) {
    printf("[%s] m=%d, n=%d, l=%f\n", function_name, dim1, dim2, l);
    passed &= run_pseudo_inverse_direct_cf32_test(function_name, dim1, dim2, l,
                                                  pseudo_inverse_under_test);

    // There is no need to test the square input cases again
    if (dim1 != dim2) {
      printf("[%s] m=%d, n=%d, l=%f\n", function_name, dim2, dim1, l);
      passed &= run_pseudo_inverse_direct_cf32_test(
          function_name, dim2, dim1, l, pseudo_inverse_under_test);
    }
  }

  if (!passed) {
    // GCOVR_EXCL_START
    printf("[%s] one or more tests failed!\n", test_name);
    // GCOVR_EXCL_STOP
  }

  return passed;
}

int main() {
  bool passed = true;

  // Tests for pseudo-inverse
  passed &= run_all_tests("PseudoInverseDirect",
                          "armral_cmplx_pseudo_inverse_direct_f32",
                          armral_cmplx_pseudo_inverse_direct_f32);

  // Tests for non-allocating pseudo-inverse
  passed &=
      run_all_tests("PseudoInverseDirectNoAlloc",
                    "armral_cmplx_pseudo_inverse_direct_f32_noalloc",
                    [](uint32_t m, uint32_t n, auto... args) {
                      uint32_t size = m > n ? n : m;
                      std::vector<uint8_t> buffer(
                          size * size * sizeof(armral_cmplx_f32_t) + 3);
                      return armral_cmplx_pseudo_inverse_direct_f32_noalloc(
                          m, n, args..., buffer.data());
                    });

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
