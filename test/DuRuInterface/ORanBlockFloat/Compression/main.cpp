/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "int_utils.hpp"
#include "qint64.hpp"

#include <cassert>
#include <cstring>

using armral::utils::allocate_random_cs16;
using armral::utils::allocate_random_i8;
using armral::utils::allocate_random_shifted_cs16;
using armral::utils::cmplx_mul_widen_cs16;
using armral::utils::qint64_t;

template<typename T>
static bool check_results_cd(const char *name, const T *result,
                             const T *expected, uint32_t n) {
  bool passed = true;
  for (uint32_t i = 0; i < n; ++i) {
    if (result[i].exp != expected[i].exp) {
      // GCOVR_EXCL_START
      passed = false;
      printf("Error! [%s] result[%u].exp= %d and expected[%u].exp= %d\n", name,
             i, result[i].exp, i, expected[i].exp);
      // GCOVR_EXCL_STOP
    } else {
      for (uint32_t j = 0; j < sizeof(T) - 1; ++j) {
        if (result[i].mantissa[j] != expected[i].mantissa[j]) {
          // GCOVR_EXCL_START
          passed = false;
          printf("Error! [%s] result[%u][%u] = 0x%02x and expected[%u][%u] = "
                 "0x%02x\n",
                 name, i, j, (uint8_t)result[i].mantissa[j], i, j,
                 (uint8_t)expected[i].mantissa[j]);
          // GCOVR_EXCL_STOP
        }
      }
    }
  }
  if (passed) {
    printf("[%s] - check result: OK\n", name);
  }
  return passed;
}

template<typename T>
static bool check_results_cd(const char *name, const std::vector<T> &result,
                             const std::vector<T> &expected) {
  assert(result.size() == expected.size());
  return check_results_cd(name, result.data(), expected.data(), result.size());
}

template<typename T>
static std::vector<T> allocate_random_cd(uint32_t len) {
  const auto bytes = allocate_random_i8(len * sizeof(T));
  std::vector<T> ret(len);
  memcpy(ret.data(), bytes.data(), len * sizeof(T));
  // we don't make any attempt to ensure the exp values are valid here,
  // since we're testing that the compression code overwrites it.
  return ret;
}

static std::vector<armral_compressed_data_8bit>
allocate_random_cd8(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_8bit>(len);
}

static std::vector<armral_compressed_data_9bit>
allocate_random_cd9(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_9bit>(len);
}

static std::vector<armral_compressed_data_12bit>
allocate_random_cd12(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_12bit>(len);
}

static std::vector<armral_compressed_data_14bit>
allocate_random_cd14(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_14bit>(len);
}

static int calc_exp(int16_t x, int max_sh) {
  // calculate the maximum shift that would preserve the value of x.
  for (int sh = 1; sh <= max_sh; ++sh) {
    if (((x << (16 + sh)) >> (16 + sh)) != x) {
      return sh - 1;
    }
  }
  return max_sh;
}

static int calc_block_exp(const armral_cmplx_int16_t *xs, uint32_t n,
                          int max_sh) {
  // calculate the maximum shift for a block of complex values
  for (uint32_t i = 0; i < n; ++i) {
    max_sh = calc_exp(xs[i].re, max_sh);
    max_sh = calc_exp(xs[i].im, max_sh);
  }
  return max_sh;
}

template<typename T, int N>
static std::vector<T>
compression_reference(const std::vector<armral_cmplx_int16_t> &src,
                      const armral_cmplx_int16_t *user_scale) {
  // compress packs of ARMRAL_NUM_COMPLEX_SAMPLES complex 16b signed integers
  // into a compressed representation of N-bit integers and a single exponent.

  // First, apply the user-supplied phase compensation term if present
  auto src_scaled = src;
  if (user_scale) {
    for (unsigned i = 0; i < src.size(); ++i) {
      std::complex<qint64_t> res = cmplx_mul_widen_cs16(src[i], *user_scale);
      // truncate to Q15 directly, no rounding.
      src_scaled[i].re = (res.real() >> 15).get16();
      src_scaled[i].im = (res.imag() >> 15).get16();
    }
  }

  constexpr int nelems = ARMRAL_NUM_COMPLEX_SAMPLES;
  assert(src_scaled.size() % nelems == 0);
  std::vector<T> ret(src_scaled.size() / nelems);
  for (uint32_t i = 0; i < src_scaled.size() / nelems; ++i) {
    ret[i].exp =
        (16 - N) - calc_block_exp(&src_scaled[i * nelems], nelems, 16 - N);
    int m_idx = 0;
    int n_carry_bits = 0;
    for (int j = 0; j < nelems; ++j) {
      int16_t re = src_scaled[i * nelems + j].re >> ret[i].exp;
      int16_t im = src_scaled[i * nelems + j].im >> ret[i].exp;
      // pack two Nb numbers into up to four 8b in a big-endian representation.
      // e.g. for N=12 given re = ABCD, im = WXYZ
      //               pack into BC DX YZ

      // first, figure out how many bits we have available for each segment.
      int nbits_re0 = 8 - n_carry_bits;
      int nbits_re1 = std::min(8, N - nbits_re0);
      int nbits_re2 = N - nbits_re0 - nbits_re1;
      int nbits_im1 = 8 - nbits_re1;
      int nbits_im2 = std::min(8 - nbits_re2, N - nbits_im1);
      int nbits_im3 = N - nbits_im1 - nbits_im2;

      // the input elements are picked from the most-significant bit down
      // (i.e. a big endian ordering).
      int shift_in_re0 = nbits_re1 + nbits_re2;
      int shift_in_re1 = nbits_re2;
      int shift_in_re2 = 0;
      int shift_in_im1 = nbits_im2 + nbits_im3;
      int shift_in_im2 = nbits_im3;
      int shift_in_im3 = 0;

      // compute masks for picking out the re/im parts for each byte.
      uint16_t mask_in_re0 = ((1U << nbits_re0) - 1) << shift_in_re0;
      uint16_t mask_in_re1 = ((1U << nbits_re1) - 1) << shift_in_re1;
      uint16_t mask_in_re2 = ((1U << nbits_re2) - 1) << shift_in_re2;
      uint16_t mask_in_im1 = ((1U << nbits_im1) - 1) << shift_in_im1;
      uint16_t mask_in_im2 = ((1U << nbits_im2) - 1) << shift_in_im2;
      uint16_t mask_in_im3 = ((1U << nbits_im3) - 1) << shift_in_im3;

      // after shift_in, elements are the low nbits, so shift back up to
      // wherever they are needed in the byte.
      int shift_out_re0 = 0;
      int shift_out_re1 = 8 - nbits_re1;
      int shift_out_re2 = 8 - nbits_re2;
      int shift_out_im1 = 0;
      int shift_out_im2 = 8 - nbits_re2 - nbits_im2;
      int shift_out_im3 = 8 - nbits_im3;

      // actually do the compression.
      // m+2 and onwards is not guaranteed to be valid memory, so guard writing
      // that.
      ret[i].mantissa[m_idx + 0] |= ((re & mask_in_re0) >> shift_in_re0)
                                    << shift_out_re0;
      ret[i].mantissa[m_idx + 1] |= ((re & mask_in_re1) >> shift_in_re1)
                                    << shift_out_re1;
      if (nbits_re2) {
        ret[i].mantissa[m_idx + 2] |= ((re & mask_in_re2) >> shift_in_re2)
                                      << shift_out_re2;
      }
      ret[i].mantissa[m_idx + 1] |= ((im & mask_in_im1) >> shift_in_im1)
                                    << shift_out_im1;
      if (nbits_im2) {
        ret[i].mantissa[m_idx + 2] |= ((im & mask_in_im2) >> shift_in_im2)
                                      << shift_out_im2;
      }
      if (nbits_im3) {
        ret[i].mantissa[m_idx + 3] |= ((im & mask_in_im3) >> shift_in_im3)
                                      << shift_out_im3;
      }

      // figure out where to start the next element
      if (nbits_re2 + nbits_im2 != 8) {
        m_idx += 2;
        n_carry_bits = nbits_re2 + nbits_im2;
      } else if (nbits_im3 != 8) {
        m_idx += 3;
        n_carry_bits = nbits_im3;
      } else {
        m_idx += 4;
        n_carry_bits = 0;
      }
    }
  }
  return ret;
}

static std::vector<armral_compressed_data_8bit>
compression_reference_8b(const std::vector<armral_cmplx_int16_t> &src,
                         const armral_cmplx_int16_t *scale) {
  return compression_reference<armral_compressed_data_8bit, 8>(src, scale);
}

static std::vector<armral_compressed_data_9bit>
compression_reference_9b(const std::vector<armral_cmplx_int16_t> &src,
                         const armral_cmplx_int16_t *scale) {
  return compression_reference<armral_compressed_data_9bit, 9>(src, scale);
}

static std::vector<armral_compressed_data_12bit>
compression_reference_12b(const std::vector<armral_cmplx_int16_t> &src,
                          const armral_cmplx_int16_t *scale) {
  return compression_reference<armral_compressed_data_12bit, 12>(src, scale);
}

static std::vector<armral_compressed_data_14bit>
compression_reference_14b(const std::vector<armral_cmplx_int16_t> &src,
                          const armral_cmplx_int16_t *scale) {
  return compression_reference<armral_compressed_data_14bit, 14>(src, scale);
}

static bool run_compression_test_8b(const int num_prbs, int16_t min,
                                    int16_t max,
                                    const armral_cmplx_int16_t *scale) {
  const char *name = "XRAN8_Compression";
  const auto src = allocate_random_shifted_cs16(
      num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES, min, max);
  const auto ref = compression_reference_8b(src, scale);
  auto dst = allocate_random_cd8(num_prbs);

  if (scale != NULL) {
    printf("[%s] - scale = (%d, %d)\n", name, scale->re, scale->im);
  }
  printf("[%s] - %d resources\n", name, num_prbs);
  armral_block_float_compr_8bit(num_prbs, src.data(), dst.data(), scale);
  return check_results_cd(name, dst, ref);
}

static bool run_compression_test_9b(const int num_prbs, int16_t min,
                                    int16_t max,
                                    const armral_cmplx_int16_t *scale) {
  const char *name = "XRAN9_Compression";
  const auto src = allocate_random_shifted_cs16(
      num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES, min, max);
  const auto ref = compression_reference_9b(src, scale);
  auto dst = allocate_random_cd9(num_prbs);

  if (scale != NULL) {
    printf("[%s] - scale = (%d, %d)\n", name, scale->re, scale->im);
  }
  printf("[%s] - %d resources\n", name, num_prbs);
  armral_block_float_compr_9bit(num_prbs, src.data(), dst.data(), scale);
  return check_results_cd(name, dst, ref);
}

static bool run_compression_test_12b(const int num_prbs, int16_t min,
                                     int16_t max,
                                     const armral_cmplx_int16_t *scale) {
  const char *name = "XRAN12_Compression";
  const auto src = allocate_random_shifted_cs16(
      num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES, min, max);
  const auto ref = compression_reference_12b(src, scale);
  auto dst = allocate_random_cd12(num_prbs);

  if (scale != NULL) {
    printf("[%s] - scale = (%d, %d)\n", name, scale->re, scale->im);
  }
  printf("[%s] - %d resources\n", name, num_prbs);
  armral_block_float_compr_12bit(num_prbs, src.data(), dst.data(), scale);
  return check_results_cd(name, dst, ref);
}

static bool run_compression_test_14b(const int num_prbs, int16_t min,
                                     int16_t max,
                                     const armral_cmplx_int16_t *scale) {
  const char *name = "XRAN14_Compression";
  const auto src = allocate_random_shifted_cs16(
      num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES, min, max);
  const auto ref = compression_reference_14b(src, scale);
  auto dst = allocate_random_cd14(num_prbs);

  if (scale != NULL) {
    printf("[%s] - scale = (%d, %d)\n", name, scale->re, scale->im);
  }
  printf("[%s] - %d resources\n", name, num_prbs);
  armral_block_float_compr_14bit(num_prbs, src.data(), dst.data(), scale);
  return check_results_cd(name, dst, ref);
}

int main(int argc, char **argv) {
  std::vector<int> params{
      0, 1, 2, 7, 10, 15, 100, 151,
  };

  // Magnitude of scale factor is not expected to be greater than 1, so get
  // a random val in range [(-sqrt(0.5),-sqrt(0.5)), (sqrt(0.5),sqrt(0.5))]
  armral_cmplx_int16_t scale = allocate_random_cs16(1, 0xA581, 0x5A7F)[0];
  std::vector<armral_cmplx_int16_t *> scales{NULL, &scale};
  bool passed = true;

  for (auto *const s : scales) {
    for (int nprbs : params) {
      passed &= run_compression_test_8b(nprbs, INT16_MIN, INT16_MAX, s);
      passed &= run_compression_test_9b(nprbs, INT16_MIN, INT16_MAX, s);
      passed &= run_compression_test_12b(nprbs, INT16_MIN, INT16_MAX, s);
      passed &= run_compression_test_14b(nprbs, INT16_MIN, INT16_MAX, s);
      // also test extreme values to check exp is set correctly
      passed &= run_compression_test_8b(nprbs, 0, 0, s);
      passed &= run_compression_test_9b(nprbs, 0, 0, s);
      passed &= run_compression_test_12b(nprbs, 0, 0, s);
      passed &= run_compression_test_14b(nprbs, 0, 0, s);
      passed &= run_compression_test_8b(nprbs, INT16_MIN, INT16_MIN, s);
      passed &= run_compression_test_9b(nprbs, INT16_MIN, INT16_MIN, s);
      passed &= run_compression_test_12b(nprbs, INT16_MIN, INT16_MIN, s);
      passed &= run_compression_test_14b(nprbs, INT16_MIN, INT16_MIN, s);
      passed &= run_compression_test_8b(nprbs, INT16_MAX, INT16_MAX, s);
      passed &= run_compression_test_9b(nprbs, INT16_MAX, INT16_MAX, s);
      passed &= run_compression_test_12b(nprbs, INT16_MAX, INT16_MAX, s);
      passed &= run_compression_test_14b(nprbs, INT16_MAX, INT16_MAX, s);
    }
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
