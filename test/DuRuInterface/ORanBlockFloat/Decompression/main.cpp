/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "int_utils.hpp"

#include <cstring>

using armral::utils::allocate_random_cs16;
using armral::utils::allocate_random_i8;
using armral::utils::check_results_cs16;
using armral::utils::scale_and_truncate_cs16;

template<typename T, int N>
static std::vector<T> allocate_random_cd(uint32_t len) {
  const auto bytes = allocate_random_i8(len * sizeof(T));
  std::vector<T> ret(len);
  memcpy(ret.data(), bytes.data(), len * sizeof(T));
  for (auto &e : ret) {
    // ensure exp stays in a valid range (e.g. 0-8 for N=8, 0-4 for N=12).
    e.exp = std::abs(e.exp) % (16 - (N - 1));
  }
  return ret;
}

static std::vector<armral_compressed_data_8bit>
allocate_random_cd8(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_8bit, 8>(len);
}

static std::vector<armral_compressed_data_9bit>
allocate_random_cd9(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_9bit, 9>(len);
}

static std::vector<armral_compressed_data_12bit>
allocate_random_cd12(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_12bit, 12>(len);
}

static std::vector<armral_compressed_data_14bit>
allocate_random_cd14(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_14bit, 14>(len);
}

template<typename T, int N>
static std::vector<armral_cmplx_int16_t>
decompression_reference(const std::vector<T> &src,
                        const armral_cmplx_int16_t *scale) {
  constexpr int nelems = ARMRAL_NUM_COMPLEX_SAMPLES;
  std::vector<armral_cmplx_int16_t> ret(nelems * src.size());
  for (uint32_t i = 0; i < src.size(); ++i) {
    int m_idx = 0;
    // keep track of the number of bits carried over between elements
    // e.g. for two elements 'a' and 'b', and N=9,
    //      each byte would contain a subset of the elements:
    //       > aaaaaaaa <- on input, n_carry_bits=0
    //       > abbbbbbb <- after decompressing 'a', n_carry_bits=1
    //       > bb       <- after decompressing 'b', n_carry_bits=2
    int n_carry_bits = 0;
    const auto &src_e = src[i];
    for (uint32_t j = 0; j < nelems; ++j) {
      int elems[2];
      for (int k = 0; k < 2; ++k) {
        // figure out the number of bits to take from each byte.
        // for smaller N values, some of these will be zero.
        int nbits0 = 8 - n_carry_bits;
        int nbits1 = std::min(8, N - nbits0);
        int nbits2 = N - nbits0 - nbits1;

        // figure out the shift to apply to each byte such that the lsb
        // is populated in each case. One of these may be non-zero if the
        // input only partially uses the final byte.
        int shift_in0 = 0;
        int shift_in1 = 8 - nbits1;
        int shift_in2 = 8 - nbits2;

        // figure out the mask to apply to take only the relevant bits
        // from each byte.
        uint8_t mask_in0 = ((1 << nbits0) - 1) << shift_in0;
        uint8_t mask_in1 = ((1 << nbits1) - 1) << shift_in1;
        uint8_t mask_in2 = ((1 << nbits2) - 1) << shift_in2;

        // load each byte if needed. if a particular byte contributes
        // no bits to the final result then just zero it to avoid loading
        // off the end of the array.
        uint8_t in0 = (mask_in0 & src_e.mantissa[m_idx]) >> shift_in0;
        uint8_t in1 = nbits1 == 0
                          ? 0
                          : (mask_in1 & src_e.mantissa[m_idx + 1]) >> shift_in1;
        uint8_t in2 = nbits2 == 0
                          ? 0
                          : (mask_in2 & src_e.mantissa[m_idx + 2]) >> shift_in2;

        // figure out the shift to apply to create the final binary
        // representation, starting with all components having the lsb
        // populated.
        int shift_out0 = nbits1 + nbits2;
        int shift_out1 = nbits2;
        //  shift_out2 = 0;

        // actually build the final number!
        int elem = (in0 << shift_out0) | (in1 << shift_out1) | in2;

        // do sign extension and apply exponent
        elem = (elem << (32 - N)) >> (32 - N);
        elems[k] = elem << src_e.exp;

        // update m_idx and n_carry_bits
        if (nbits1 != 8) {
          ++m_idx;
          n_carry_bits = nbits1;
        } else {
          m_idx += 2;
          n_carry_bits = nbits2;
        }
      }
      ret[i * nelems + j].re = elems[0];
      ret[i * nelems + j].im = elems[1];
    }
  }

  if (scale) {
    scale_and_truncate_cs16(ret, scale);
  }
  return ret;
}

static std::vector<armral_cmplx_int16_t>
decompression_reference_8b(const std::vector<armral_compressed_data_8bit> &src,
                           const armral_cmplx_int16_t *scale) {
  return decompression_reference<armral_compressed_data_8bit, 8>(src, scale);
}

static std::vector<armral_cmplx_int16_t>
decompression_reference_9b(const std::vector<armral_compressed_data_9bit> &src,
                           const armral_cmplx_int16_t *scale) {
  return decompression_reference<armral_compressed_data_9bit, 9>(src, scale);
}

static std::vector<armral_cmplx_int16_t> decompression_reference_12b(
    const std::vector<armral_compressed_data_12bit> &src,
    const armral_cmplx_int16_t *scale) {
  return decompression_reference<armral_compressed_data_12bit, 12>(src, scale);
}

static std::vector<armral_cmplx_int16_t> decompression_reference_14b(
    const std::vector<armral_compressed_data_14bit> &src,
    const armral_cmplx_int16_t *scale) {
  return decompression_reference<armral_compressed_data_14bit, 14>(src, scale);
}

static bool run_decompression_test_8b(const int num_prbs,
                                      const armral_cmplx_int16_t *scale) {
  const char *name = "XRAN8_Decompression";
  const auto src = allocate_random_cd8(num_prbs);
  const auto ref = decompression_reference_8b(src, scale);
  auto dst = allocate_random_cs16(num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES);
  assert(ref.size() == dst.size());

  if (scale != NULL) {
    printf("[%s] - scale = (%d, %d)\n", name, scale->re, scale->im);
  }

  printf("[%s] - %d resources\n", name, num_prbs);
  armral_block_float_decompr_8bit(num_prbs, src.data(), dst.data(), scale);
  return check_results_cs16(name, dst.data(), ref.data(), ref.size());
}

static bool run_decompression_test_9b(const int num_prbs,
                                      const armral_cmplx_int16_t *scale) {
  const char *name = "XRAN9_Decompression";
  const auto src = allocate_random_cd9(num_prbs);
  const auto ref = decompression_reference_9b(src, scale);
  auto dst = allocate_random_cs16(num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES);
  assert(ref.size() == dst.size());

  if (scale != NULL) {
    printf("[%s] - scale = (%d, %d)\n", name, scale->re, scale->im);
  }

  printf("[%s] - %d resources\n", name, num_prbs);
  armral_block_float_decompr_9bit(num_prbs, src.data(), dst.data(), scale);
  return check_results_cs16(name, dst.data(), ref.data(), ref.size());
}

static bool run_decompression_test_12b(const int num_prbs,
                                       const armral_cmplx_int16_t *scale) {
  const char *name = "XRAN12_Decompression";
  const auto src = allocate_random_cd12(num_prbs);
  const auto ref = decompression_reference_12b(src, scale);
  auto dst = allocate_random_cs16(num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES);
  assert(ref.size() == dst.size());

  if (scale != NULL) {
    printf("[%s] - scale = (%d, %d)\n", name, scale->re, scale->im);
  }

  printf("[%s] - %d resources\n", name, num_prbs);
  armral_block_float_decompr_12bit(num_prbs, src.data(), dst.data(), scale);
  return check_results_cs16(name, dst.data(), ref.data(), ref.size());
}

static bool run_decompression_test_14b(const int num_prbs,
                                       const armral_cmplx_int16_t *scale) {
  const char *name = "XRAN14_Decompression";
  const auto src = allocate_random_cd14(num_prbs);
  const auto ref = decompression_reference_14b(src, scale);
  auto dst = allocate_random_cs16(num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES);
  assert(ref.size() == dst.size());

  if (scale != NULL) {
    printf("[%s] - scale = (%d, %d)\n", name, scale->re, scale->im);
  }

  printf("[%s] - %d resources\n", name, num_prbs);
  armral_block_float_decompr_14bit(num_prbs, src.data(), dst.data(), scale);
  return check_results_cs16(name, dst.data(), ref.data(), ref.size());
}

int main(int argc, char **argv) {
  std::vector<int> params{
      1, 2, 7, 10, 15, 100, 151,
  };
  bool passed = true;
  for (int nprbs : params) {
    passed &= run_decompression_test_8b(nprbs, NULL);
    passed &= run_decompression_test_9b(nprbs, NULL);
    passed &= run_decompression_test_12b(nprbs, NULL);
    passed &= run_decompression_test_14b(nprbs, NULL);

    armral_cmplx_int16_t scale = allocate_random_cs16(1)[0];
    passed &= run_decompression_test_8b(nprbs, &scale);
    passed &= run_decompression_test_9b(nprbs, &scale);
    passed &= run_decompression_test_12b(nprbs, &scale);
    passed &= run_decompression_test_14b(nprbs, &scale);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
