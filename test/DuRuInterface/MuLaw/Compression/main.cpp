/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "cs16_utils.hpp"
#include "int_utils.hpp"
#include "qint64.hpp"

#include <algorithm>
#include <cstring>

using armral::utils::allocate_random_cs16;
using armral::utils::allocate_random_i8;
using armral::utils::allocate_random_shifted_cs16;
using armral::utils::cmplx_mul_widen_cs16;
using armral::utils::qint64_t;

namespace {
int16_t sign(int16_t x) {
  return x >= 0 ? 1 : -1;
}

int calculate_shift(const armral_cmplx_int16_t *prb, uint32_t n,
                    int16_t bit_width) {

  if ((bit_width != 8) && (bit_width != 9) && (bit_width != 14)) {
    assert(false && "Unsupported bit_width"); // GCOVR_EXCL_LINE
  }
  // extract the  absolute value for the PRB
  std::vector<int> prb_abs(2 * n);

  for (uint32_t i = 0; i < n; i++) {
    prb_abs[i * 2] = vqabsh_s16(prb[i].re);
    prb_abs[i * 2 + 1] = vqabsh_s16(prb[i].im);
  }
  // Determine the shift to be applied to the entire PRB
  int max_abs_val = *std::max_element(prb_abs.begin(), prb_abs.end());

  if (bit_width == 14) {
    if (max_abs_val < (1 << 2)) {
      return 13;
    }
    if (max_abs_val < (1 << 3)) {
      return 12;
    }
    if (max_abs_val < (1 << 4)) {
      return 11;
    }
    if (max_abs_val < (1 << 5)) {
      return 10;
    }
    if (max_abs_val < (1 << 6)) {
      return 9;
    }
    if (max_abs_val < (1 << 7)) {
      return 8;
    }
  }
  if (bit_width == 9 && (max_abs_val < (1 << 7))) {
    return 8;
  }
  if (max_abs_val < (1 << 8)) {
    return 7;
  }
  if (max_abs_val < (1 << 9)) {
    return 6;
  }
  if (max_abs_val < (1 << 10)) {
    return 5;
  }
  if (max_abs_val < (1 << 11)) {
    return 4;
  }
  if (max_abs_val < (1 << 12)) {
    return 3;
  }
  if (max_abs_val < (1 << 13)) {
    return 2;
  }
  if (max_abs_val < (1 << 14)) {
    return 1;
  }
  return 0;
}

inline int16_t compress_val(const int16_t val, const int16_t sign,
                            const int16_t output_bits) {

  // we use the form (((x >> (y-1)) + 1) >> 1) to ensure correct rounding
  // when doing a shift right by y bits.
  constexpr int input_bits = 15;
  int16_t ret;
  if (val <= (1 << (input_bits - 2))) {
    ret = ((val >> (input_bits - output_bits - 1)) + 1) >> 1;
  } else if (val <= (1 << (input_bits - 1))) {
    ret = (((val >> (input_bits - output_bits)) + 1) >> 1) +
          (1 << (output_bits - 3));
  } else {
    ret = (((val >> (input_bits - output_bits + 1)) + 1) >> 1) +
          (1 << (output_bits - 2));
  }
  // re-apply sign and saturate to [-2^(output_bits-1)+1, 2^(output_bits-1)-1]
  int sat = (1U << (output_bits - 1)) - 1;
  ret = std::max(-sat, std::min(sat, (sign * ret)));
  return ret;
}

template<typename T, int N>
std::vector<T>
reference_compression(const std::vector<armral_cmplx_int16_t> &src,
                      const armral_cmplx_int16_t *scale) {
  // compress packs of ARMRAL_NUM_COMPLEX_SAMPLES complex 16b signed integers
  // into a compressed representation of N-bit signed integers and a shift

  // First, apply the user-supplied phase compensation term if present
  auto src_scaled = src;
  if (scale) {
    for (unsigned i = 0; i < src.size(); ++i) {
      std::complex<qint64_t> res = cmplx_mul_widen_cs16(src[i], *scale);
      // truncate to Q15 directly, no rounding.
      src_scaled[i].re = (res.real() >> 15).get16();
      src_scaled[i].im = (res.imag() >> 15).get16();
    }
  }

  constexpr int nelems = ARMRAL_NUM_COMPLEX_SAMPLES;
  assert(src_scaled.size() % nelems == 0);
  std::vector<T> ret(src_scaled.size() / nelems);
  for (uint32_t i = 0; i < src_scaled.size() / nelems; ++i) {
    // Calculate shift to apply to the full prb
    ret[i].exp = calculate_shift(&src_scaled[i * nelems], nelems, N);
    int m_idx = 0;
    int n_carry_bits = 0;
    for (int j = 0; j < nelems; ++j) {
      // Compress real part
      int16_t sign_re = sign(src_scaled[i * nelems + j].re);
      int16_t abs_shfit_re = vqabsh_s16(src_scaled[i * nelems + j].re)
                             << ret[i].exp;
      int16_t comp_re = compress_val(abs_shfit_re, sign_re, N);

      // Compress imaginary part
      int16_t sign_im = sign(src_scaled[i * nelems + j].im);
      int16_t abs_shfit_im = vqabsh_s16(src_scaled[i * nelems + j].im)
                             << ret[i].exp;
      int16_t comp_im = compress_val(abs_shfit_im, sign_im, N);

      // pack two Nb numbers into up to four 8b in a big-endian representation.
      int nbits_re0 = 8 - n_carry_bits;
      int nbits_re1 = std::min(8, N - nbits_re0);
      int nbits_re2 = N - nbits_re0 - nbits_re1;
      int nbits_im1 = 8 - nbits_re1;
      int nbits_im2 = std::min(8 - nbits_re2, N - nbits_im1);
      int nbits_im3 = N - nbits_im1 - nbits_im2;

      // The input elements are picked from the most-significant bit down
      int shift_in_re0 = nbits_re1 + nbits_re2;
      int shift_in_re1 = nbits_re2;
      int shift_in_re2 = 0;
      int shift_in_im1 = nbits_im2 + nbits_im3;
      int shift_in_im2 = nbits_im3;
      int shift_in_im3 = 0;

      // compute masks for picking out the re/im parts for each byte.
      uint16_t mask_in_re0 = ((1U << nbits_re0) - 1) << shift_in_re0;
      uint16_t mask_in_re1 = ((1U << nbits_re1) - 1) << shift_in_re1;
      uint16_t mask_in_re2 = ((1U << nbits_re2) - 1) << shift_in_re2;
      uint16_t mask_in_im1 = ((1U << nbits_im1) - 1) << shift_in_im1;
      uint16_t mask_in_im2 = ((1U << nbits_im2) - 1) << shift_in_im2;
      uint16_t mask_in_im3 = ((1U << nbits_im3) - 1) << shift_in_im3;

      // after shift_in, elements are the low nbits, so shift back up to
      // wherever they are needed in the byte.
      int shift_out_re0 = 0;
      int shift_out_re1 = 8 - nbits_re1;
      int shift_out_re2 = 8 - nbits_re2;
      int shift_out_im1 = 0;
      int shift_out_im2 = 8 - nbits_re2 - nbits_im2;
      int shift_out_im3 = 8 - nbits_im3;

      // Write compressed data
      ret[i].mantissa[m_idx + 0] |= ((comp_re & mask_in_re0) >> shift_in_re0)
                                    << shift_out_re0;
      ret[i].mantissa[m_idx + 1] |= ((comp_re & mask_in_re1) >> shift_in_re1)
                                    << shift_out_re1;
      if (nbits_re2) {
        ret[i].mantissa[m_idx + 2] |= ((comp_re & mask_in_re2) >> shift_in_re2)
                                      << shift_out_re2;
      }
      ret[i].mantissa[m_idx + 1] |= ((comp_im & mask_in_im1) >> shift_in_im1)
                                    << shift_out_im1;
      if (nbits_im2) {
        ret[i].mantissa[m_idx + 2] |= ((comp_im & mask_in_im2) >> shift_in_im2)
                                      << shift_out_im2;
      }
      if (nbits_im3) {
        ret[i].mantissa[m_idx + 3] |= ((comp_im & mask_in_im3) >> shift_in_im3)
                                      << shift_out_im3;
      }
      //  Starting point of the next element
      if (nbits_re2 + nbits_im2 != 8) {
        m_idx += 2;
        n_carry_bits = nbits_re2 + nbits_im2;
      } else if (nbits_im3 != 8) {
        m_idx += 3;
        n_carry_bits = nbits_im3;
      } else {
        m_idx += 4;
        n_carry_bits = 0;
      }
    }
  }
  return ret;
}

std::vector<armral_compressed_data_8bit>
reference_compression_8b(const std::vector<armral_cmplx_int16_t> &src,
                         const armral_cmplx_int16_t *scale) {
  return reference_compression<armral_compressed_data_8bit, 8>(src, scale);
}

std::vector<armral_compressed_data_9bit>
reference_compression_9b(const std::vector<armral_cmplx_int16_t> &src,
                         const armral_cmplx_int16_t *scale) {
  return reference_compression<armral_compressed_data_9bit, 9>(src, scale);
}

std::vector<armral_compressed_data_14bit>
reference_compression_14b(const std::vector<armral_cmplx_int16_t> &src,
                          const armral_cmplx_int16_t *scale) {
  return reference_compression<armral_compressed_data_14bit, 14>(src, scale);
}

template<typename T>
bool check_mu_law_comp(const char *name, const T *result, const T *expected,
                       uint32_t n) {
  bool passed = true;
  for (uint32_t i = 0; i < n; ++i) {
    if (result[i].exp != expected[i].exp) {
      // GCOVR_EXCL_START
      passed = false;
      printf("Error! [%s] result[%u].exp= %d and expected[%u].exp= %d\n", name,
             i, result[i].exp, i, expected[i].exp);
      // GCOVR_EXCL_STOP
    } else {
      for (uint32_t j = 0; j < sizeof(T) - 1; ++j) {
        if (result[i].mantissa[j] != expected[i].mantissa[j]) {
          // GCOVR_EXCL_START
          passed = false;
          printf("Error! [%s] result[%u][%u] = 0x%02x and expected[%u][%u] = "
                 "0x%02x\n",
                 name, i, j, (uint8_t)result[i].mantissa[j], i, j,
                 (uint8_t)expected[i].mantissa[j]);
          // GCOVR_EXCL_STOP
        }
      }
    }
  }
  if (passed) {
    printf("[%s] - check result: OK\n", name);
  }
  return passed;
}

template<typename T>
bool check_mu_law_comp(const char *name, const std::vector<T> &result,
                       const std::vector<T> &expected) {
  assert(result.size() == expected.size());
  return check_mu_law_comp(name, result.data(), expected.data(), result.size());
}

template<typename T>
std::vector<T> allocate_random_cd(uint32_t len) {
  const auto bytes = allocate_random_i8(len * sizeof(T));
  std::vector<T> ret(len);
  memcpy(ret.data(), bytes.data(), len * sizeof(T));
  return ret;
}

std::vector<armral_compressed_data_8bit> allocate_random_cd8(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_8bit>(len);
}

std::vector<armral_compressed_data_9bit> allocate_random_cd9(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_9bit>(len);
}

std::vector<armral_compressed_data_14bit> allocate_random_cd14(uint32_t len) {
  return allocate_random_cd<armral_compressed_data_14bit>(len);
}

// Accuracy test  routines
bool run_compression_test_8b(const int num_prbs, int min, int max,
                             const armral_cmplx_int16_t *scale) {
  const char *name = "MuLaw_Compression_8b";
  const auto src = allocate_random_shifted_cs16(
      num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES, min, max);
  auto compressed = allocate_random_cd8(num_prbs);

  auto compressed_ref = reference_compression_8b(src, scale);

  printf("[%s] - %d resources\n", name, num_prbs);
  armral_mu_law_compr_8bit(num_prbs, src.data(), compressed.data(), scale);
  return check_mu_law_comp(name, compressed, compressed_ref);
}

bool run_compression_test_9b(const int num_prbs, int16_t min, int16_t max,
                             const armral_cmplx_int16_t *scale) {
  const char *name = "MuLaw_Compression_9b";
  const auto src = allocate_random_shifted_cs16(
      num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES, min, max);
  auto compressed = allocate_random_cd9(num_prbs);

  auto compressed_ref = reference_compression_9b(src, scale);

  printf("[%s] - %d resources\n", name, num_prbs);
  armral_mu_law_compr_9bit(num_prbs, src.data(), compressed.data(), scale);
  return check_mu_law_comp(name, compressed, compressed_ref);
}

bool run_compression_test_14b(const int num_prbs, int16_t min, int16_t max,
                              const armral_cmplx_int16_t *scale) {
  const char *name = "MuLaw_Compression_14b";
  const auto src = allocate_random_shifted_cs16(
      num_prbs * ARMRAL_NUM_COMPLEX_SAMPLES, min, max);
  auto compressed = allocate_random_cd14(num_prbs);

  auto compressed_ref = reference_compression_14b(src, scale);

  printf("[%s] - %d resources\n", name, num_prbs);
  armral_mu_law_compr_14bit(num_prbs, src.data(), compressed.data(), scale);
  return check_mu_law_comp(name, compressed, compressed_ref);
}

} // anonymous namespace

// Entry point for the unit testing of Mu Law compression
int main(int argc, char **argv) {
  // Set num_prbs values
  std::vector<int> params{0, 1, 2, 7, 10, 15, 100, 151};
  std::vector<std::pair<int, int>> intervals = {{0, 1},
                                                {-1, 0},
                                                {-2, 1},
                                                {-16, 15},
                                                {INT8_MIN, INT8_MAX},
                                                {INT16_MIN, INT16_MAX},
                                                // extreme c cases
                                                {0, 0},
                                                {INT16_MIN, INT16_MIN},
                                                {INT16_MAX, INT16_MAX}};
  bool passed = true;
  for (int num_prbs : params) {
    for (auto [interval_min, interval_max] : intervals) {
      printf("Testing (%d, %d)\n", interval_min, interval_max);
      passed &=
          run_compression_test_8b(num_prbs, interval_min, interval_max, NULL);
      passed &=
          run_compression_test_9b(num_prbs, interval_min, interval_max, NULL);
      passed &=
          run_compression_test_14b(num_prbs, interval_min, interval_max, NULL);

      // Magnitude of scale factor is not expected to be greater than 1, so get
      // a random val in range [(-sqrt(0.5),-sqrt(0.5)), (sqrt(0.5),sqrt(0.5))]
      armral_cmplx_int16_t scale = allocate_random_cs16(1, 0xA581, 0x5A7F)[0];
      printf("Testing (%d, %d), scale=(%d,%d)\n", interval_min, interval_max,
             scale.re, scale.im);
      passed &=
          run_compression_test_8b(num_prbs, interval_min, interval_max, &scale);
      passed &=
          run_compression_test_9b(num_prbs, interval_min, interval_max, &scale);
      passed &= run_compression_test_14b(num_prbs, interval_min, interval_max,
                                         &scale);
    }
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
