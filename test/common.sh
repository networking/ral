PERF_REPEATS=11
PERF_MEDIAN_LINE=$(expr $PERF_REPEATS / 2 + 1)

EXEC="$1"

if [[ "$(type -t run_perf_sample)" != 'function' ]]; then
	function run_perf_sample {
		set +e
		perf_str=$(taskset -c 0 perf stat -x' ' -e cycles:u $@ 2>&1 >/dev/null)
		ec=$?
		set -e
		# Deal with the case that something goes wrong with the perf command
		# (either because we exited cleanly with a non-zero status, or because we
		# printed an error message to stderr). In these cases, we want the error
		# output to be printed to stderr.
		if [[ $ec -ne 0 ]] || [[ $perf_str == *$'\n'* ]]
		then
			>&2 echo "Error: run_perf_sample exited with status code $ec"
			>&2 echo "$perf_str"
			exit 1
		fi
		echo "$perf_str" | awk '{print $1}'
	}
fi

if [[ "$(type -t run_perf)" != 'function' ]]; then
	function run_perf {
		reps="${@: -1}"
		cycles_arr=()
		md5="$(md5sum ${EXEC} | awk '{print $1}')"
		for it in $(seq 1 $PERF_REPEATS)
		do
			cycles_arr+=("$(run_perf_sample ${EXEC} $@)")
		done
		all_cycles="$(printf '%s\n' "${cycles_arr[@]}" | awk '{ret=ret", "($1/'$reps')} END{print substr(ret,3)}')"
		kernel_cycles="$(printf '%s\n' "${cycles_arr[@]}" | sort -g | sed -n "${PERF_MEDIAN_LINE}p" | awk '{print $1/'$reps'}')"
		echo "{ \"name\": \"$EXEC_STR\", \"median_cycles\": $kernel_cycles, \"all_cycles\": [$all_cycles], \"md5\": \"$md5\" }"
	}
fi
