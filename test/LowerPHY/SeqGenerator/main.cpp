/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "int_utils.hpp"

static std::vector<uint8_t> reference_sequence_generator(uint32_t seed,
                                                         uint32_t len_bytes) {
  // algorithm is derived from section 5.2.1 of
  // https://www.etsi.org/deliver/etsi_ts/138200_138299/138211/15.02.00_60/ts_138211v150200p.pdf
  // c(n) = x_1(n+Nc) ^ x_2(n+Nc);  Nc=1600
  // x_1(n) = x_1(n-28) ^ x_1(n-31)
  // x_2(n) = x_2(n-28) ^ x_2(n-29) ^ x_2(n-30) ^ x_2(n-31)
  uint16_t nc = 1600;
  uint64_t len_bits = ((uint64_t)len_bytes) << 3;
  std::vector<uint8_t> x_1(len_bits + nc);
  std::vector<uint8_t> x_2(len_bits + nc);

  // init
  x_1[0] = 1;
  for (uint8_t i = 0; i < 31; ++i) {
    x_2[i] = (seed >> i) & 1;
  }

  // run sequence generating x_1/x_2.
  for (uint64_t i = 31; i < len_bits + nc; ++i) {
    x_1[i] = x_1[i - 28] ^ x_1[i - 31];
    x_2[i] = x_2[i - 28] ^ x_2[i - 29] ^ x_2[i - 30] ^ x_2[i - 31];
  }

  // compute final compressed output
  std::vector<uint8_t> ret(len_bytes);
  for (uint64_t i = 0; i < len_bits; ++i) {
    ret[i / 8] |= (x_1[i + nc] ^ x_2[i + nc]) << (i % 8);
  }

  return ret;
}

static bool run_sequence_generator_test(uint32_t seed, uint32_t len) {
  uint64_t len_bytes = (((uint64_t)len) + 7) / 8;
  // in principle it's possible for the algorithm to support non-byte lengths
  // of sequence, but we don't support that so just round up to the next
  // multiple of 8 and use the byte count instead.
  const auto ref = reference_sequence_generator(seed, len_bytes);
  auto dst = armral::utils::allocate_random_u8(len_bytes);
  printf("[SEQUENCE GENERATOR] seed=%u, len=%u\n", seed, len);
  armral_seq_generator(len, seed, dst.data());
  return armral::utils::check_results_u8("SEQUENCE GENERATOR", dst.data(),
                                         ref.data(), (uint32_t)len_bytes);
}

int main(int argc, char **argv) {
  bool passed = true;
  for (uint32_t seed : {0U, 1U, 5U, 175U, 0x20000000U, 0xffffffffU}) {
    for (uint32_t len = 0; len < 128; ++len) {
      passed &= run_sequence_generator_test(seed, len);
    }
    passed &= run_sequence_generator_test(seed, 1023);
    passed &= run_sequence_generator_test(seed, 1024);
    passed &= run_sequence_generator_test(seed, 1025);
    passed &= run_sequence_generator_test(seed, 2047);
    passed &= run_sequence_generator_test(seed, 2048);
    passed &= run_sequence_generator_test(seed, 2049);
    passed &= run_sequence_generator_test(seed, 65535);
    passed &= run_sequence_generator_test(seed, 131072);
    passed &= run_sequence_generator_test(seed, 262144);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
