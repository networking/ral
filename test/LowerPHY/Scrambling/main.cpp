/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"

static std::vector<uint8_t> reference_scrambler(const uint8_t *input,
                                                const uint8_t *seq,
                                                uint32_t len_bytes) {
  // The scrambling algorithm is given in TS 38.211 (e.g. section 7.3.1.1). The
  // scrambled block of bits s(0), ..., s(M_bit -1), where M_bit is the
  // number of input bits, results from scrambling the input bits b(0), ...,
  // b(M_bit - 1) using the Gold sequence c according to:
  //    s(i) = (b(i) + c(i)) mod 2

  std::vector<uint8_t> ret(len_bytes);
  for (uint32_t i = 0; i < len_bytes; ++i) {
    ret[i] = input[i] ^ seq[i];
  }
  return ret;
}

static bool run_scrambling_test(uint32_t len) {
  uint32_t len_bytes = (((uint64_t)len) + 7) / 8;
  // Random source and destination data.
  armral::utils::int_random<uint8_t> random;
  auto in = random.vector(len_bytes);
  auto dst = random.vector(len_bytes);

  // Generate Gold sequence.
  std::vector<uint8_t> sequence(len_bytes);
  armral_seq_generator(len, 0U, sequence.data());

  const auto ref = reference_scrambler(in.data(), sequence.data(), len_bytes);

  printf("[SCRAMBLING] len = %u\n", len);
  armral_scramble_code_block(in.data(), sequence.data(), len, dst.data());
  return armral::utils::check_results_u8("SCRAMBLING", dst.data(), ref.data(),
                                         len_bytes);
}

int main(int argc, char **argv) {
  bool passed = true;
  const uint32_t len[] = {30,  64,  72,  128, 136, 192, 200,
                          256, 280, 320, 392, 448, 464};
  for (uint32_t n : len) {
    passed &= run_scrambling_test(n);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
