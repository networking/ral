/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cf32_utils.hpp"

#define NAME "FIR armral_cmplx_f32_t"

static uint32_t iround_4(uint32_t x) {
  // round up to the next multiple of four.
  return (x + 3) & ~0x3;
}

static bool run_fir_test(uint32_t num_samples, uint32_t num_taps) {
  assert(num_samples % 4 == 0);
  armral::utils::cf32_random random;
  auto input = random.vector(iround_4(num_samples + num_taps));
  auto coeffs = random.vector(iround_4(num_taps));
  auto output = random.vector(num_samples);
  auto ref = random.vector(num_samples);

  printf("[" NAME "] - %u samples - %u taps\n", num_samples, num_taps);

  armral_fir_filter_cf32(num_samples, num_taps, input.data(), coeffs.data(),
                         output.data());

  //  y[n] = b_0 x[n] + b_1 x[n-1] + ... + b_N x[n-N]
  //       = \sum_{i=0}^{N} b_i x[n-i]
  for (uint32_t i = 0; i < num_samples; ++i) {
    std::complex<double> acc = 0.0;
    for (uint32_t j = 0; j < num_taps; ++j) {
      // note: this is i+j since the FIR coefficients are assumed to be
      //       swapped in memory (part of the interface).
      acc += armral::utils::cmplx_mul_widen_cf32(input[i + j], coeffs[j]);
    }
    ref[i].re = acc.real();
    ref[i].im = acc.imag();
  }

  return armral::utils::check_results_cf32(NAME, output.data(), ref.data(),
                                           num_samples);
}

int main(int argc, char **argv) {
  const std::pair<int, int> params[] = {
      {8, 3},     {8, 4},     {16, 4},    {16, 5},    {16, 7},    {64, 7},
      {64, 9},    {128, 8},   {128, 10},  {256, 11},  {256, 13},  {512, 13},
      {512, 15},  {1024, 13}, {1024, 16}, {1024, 17}, {2048, 17}, {2048, 19},
      {2048, 20}, {4096, 19}, {4096, 22}, {4096, 25}, {8192, 24}, {8192, 30},
      {8192, 32}, {10240, 4},
  };
  bool passed = true;
  for (const auto &p : params) {
    passed &= run_fir_test(p.first, p.second);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
