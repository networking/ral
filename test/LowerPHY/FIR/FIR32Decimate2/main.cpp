/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cf32_utils.hpp"

#define NAME "FIR armral_cmplx_f32_t dec2"

static uint32_t iround_4(uint32_t x) {
  // round up to the next multiple of four.
  return (x + 3) & ~0x3;
}

static uint32_t iround_8(uint32_t x) {
  // round up to the next multiple of eight.
  return (x + 7) & ~0x7;
}

static bool run_fir_test(int num_samples, int num_taps) {
  assert(num_samples % 8 == 0);
  armral::utils::cf32_random random;
  auto input = random.vector(iround_8(num_samples + num_taps));
  auto coeffs = random.vector(iround_4(num_taps));
  auto output = random.vector(num_samples / 2);
  auto ref = random.vector(num_samples / 2);

  printf("[" NAME "] - %d samples - %d taps\n", num_samples, num_taps);

  armral_fir_filter_cf32_decimate_2(num_samples, num_taps, input.data(),
                                    coeffs.data(), output.data());

  //  y[n] = b_0 x[n] + b_1 x[n-1] + ... + b_N x[n-N]
  //       = \sum_{i=0}^{N} b_i x[n-i]
  // additionally, downsample by a factor of two (so skip every other element).
  for (int i = 0; i < num_samples / 2; ++i) {
    std::complex<double> acc = 0.0;
    for (int j = 0; j < num_taps; ++j) {
      // note: this is i*2+j since the FIR coefficients are assumed to be
      //       swapped in memory (part of the interface).
      acc += armral::utils::cmplx_mul_widen_cf32(input[i * 2 + j], coeffs[j]);
    }
    ref[i].re = acc.real();
    ref[i].im = acc.imag();
  }

  return armral::utils::check_results_cf32(NAME, output.data(), ref.data(),
                                           num_samples / 2);
}

int main(int argc, char **argv) {
  const std::pair<int, int> params[] = {
      {8, 4},     {8, 1},      {8, 3},     {8, 5},     {16, 8},    {16, 9},
      {128, 8},   {128, 10},   {256, 11},  {256, 12},  {512, 13},  {512, 15},
      {1024, 11}, {1024, 17},  {1024, 19}, {2048, 14}, {2048, 16}, {2048, 18},
      {4096, 20}, {4096, 21},  {4096, 23}, {8192, 24}, {8192, 27}, {8192, 31},
      {8192, 32}, {10240, 32},
  };
  bool passed = true;
  for (const auto &p : params) {
    passed &= run_fir_test(p.first, p.second);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
