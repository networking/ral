/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"

#define NAME "FIR armral_cmplx_int16_t"

static uint32_t iround_8(uint32_t x) {
  // round up to the next multiple of eight.
  return (x + 7) & ~0x7;
}

static bool run_fir_test(uint32_t num_samples, uint32_t num_taps) {
  assert(num_samples % 8 == 0);
  armral::utils::cs16_random random;
  auto input = random.vector(iround_8(num_samples + num_taps));
  auto coeffs = random.vector(iround_8(num_taps));
  auto output = random.vector(num_samples);
  auto ref = random.vector(num_samples);

  printf("[" NAME "] - %u samples - %u taps\n", num_samples, num_taps);

  armral_fir_filter_cs16(num_samples, num_taps, input.data(), coeffs.data(),
                         output.data());

  //  y[n] = b_0 x[n] + b_1 x[n-1] + ... + b_N x[n-N]
  //       = \sum_{i=0}^{N} b_i x[n-i]
  for (uint32_t i = 0; i < num_samples; ++i) {
    std::complex<int64_t> acc = 0.0;
    for (uint32_t j = 0; j < num_taps; ++j) {
      // note: this is i+j since the FIR coefficients are assumed to be
      //       swapped in memory (part of the interface).
      acc += armral::utils::cmplx_mul_widen_cs16(input[i + j], coeffs[j]);
    }
    ref[i].re = acc.real() >> 16;
    ref[i].im = acc.imag() >> 16;
  }

  return armral::utils::check_results_cs16(NAME, (int16_t *)output.data(),
                                           (int16_t *)ref.data(), num_samples);
}

int main(int argc, char **argv) {
  const std::pair<int, int> params[] = {
      {16, 8},    {16, 16},    {128, 8},   {128, 9},   {128, 10},  {184, 12},
      {184, 13},  {184, 16},   {256, 14},  {256, 16},  {512, 16},  {512, 15},
      {512, 19},  {1024, 19},  {1024, 23}, {2048, 23}, {2048, 24}, {4096, 25},
      {4096, 28}, {8192, 30},  {8192, 32}, {128, 3},   {1024, 5},  {2048, 6},
      {4096, 7},  {10240, 32},
  };
  bool passed = true;
  for (const auto &p : params) {
    passed &= run_fir_test(p.first, p.second);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
