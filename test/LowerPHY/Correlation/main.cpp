/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "qint64.hpp"

using armral::utils::qint64_t;

static bool check_single_cs16(const char *name, armral_cmplx_int16_t result,
                              armral_cmplx_int16_t expected) {
  bool passed = true;
  if (result.re != expected.re) {
    // GCOVR_EXCL_START
    passed = false;
    printf("Error! [%s] result.re = %d and expected.re = %d\n", name, result.re,
           expected.re);
    // GCOVR_EXCL_STOP
  }
  if (result.im != expected.im) {
    // GCOVR_EXCL_START
    passed = false;
    printf("Error! [%s] result.im = %d and expected.im = %d\n", name, result.im,
           expected.im);
    // GCOVR_EXCL_STOP
  }

  printf("[%s] - check result: %s\n", name, passed ? "OK" : "ERROR");

  return passed;
}

static std::complex<qint64_t> ref_avg(int n, const armral_cmplx_int16_t *a) {
  std::complex<qint64_t> acc;
  for (int i = 0; i < n; ++i) {
    acc += std::complex<int16_t>{a[i].re, a[i].im};
  }
  return {acc.real() / n, acc.imag() / n};
}

static std::complex<qint64_t> ref_sum_a_conj_b(int n,
                                               const armral_cmplx_int16_t *a,
                                               const armral_cmplx_int16_t *b) {
  std::complex<qint64_t> acc;
  for (int i = 0; i < n; ++i) {
    std::complex<qint64_t> a_elem{qint64_t{a[i].re}, qint64_t{a[i].im}};
    std::complex<qint64_t> b_elem{qint64_t{b[i].re}, qint64_t{-b[i].im}};
    acc += a_elem * b_elem;
  }
  return acc;
}

static qint64_t ref_sum_a_conj_a(int n, const armral_cmplx_int16_t *a) {
  qint64_t acc{0};
  for (int i = 0; i < n; ++i) {
    std::complex<qint64_t> a_elem{qint64_t{a[i].re}, qint64_t{a[i].im}};
    std::complex<qint64_t> b_elem{qint64_t{a[i].re}, qint64_t{-a[i].im}};
    acc += (a_elem * b_elem).real();
  }
  return acc;
}

static qint64_t ref_sqrt(qint64_t x) {
  double q33_30_to_fp = 1.0 / (32768.0 * 32768.0);
  double fp_to_q15 = 32768.0;
  return (qint64_t)(sqrt(x.get64() * q33_30_to_fp) * fp_to_q15 + 0.5);
}

static armral_cmplx_int16_t
reference_correlation(int n, const armral_cmplx_int16_t *a,
                      const armral_cmplx_int16_t *b) {
  //       SUM(a*conj(b)) - n* avg(a)*avg(b)
  // -----------------------------------------------
  // (SQRT(SUM(a*conj(a)) - n*avg(a)*conj(avg(a))) *
  //  SQRT(SUM(b*conj(b)) - n*avg(b)*conj(avg(b))))

  // intermediate fixed-point formats are annotated, note that multiplication
  // sums the number of fractional bits and division subtracts.
  auto avg_a = ref_avg(n, a);                    // 48.15
  auto avg_b = ref_avg(n, b);                    // 48.15
  auto xyconj = ref_sum_a_conj_b(n, a, b);       // 33.30
  auto x = xyconj - (qint64_t)n * avg_a * avg_b; // 33.30
  qint64_t y_1 = ref_sum_a_conj_a(n, a) -
                 (qint64_t)n * (avg_a * std::conj(avg_a)).real(); // 33.30
  qint64_t y_2 = ref_sum_a_conj_a(n, b) -
                 (qint64_t)n * (avg_b * std::conj(avg_b)).real(); // 33.30
  y_1 >>= 15;                                                     // 48.15
  y_2 >>= 15;                                                     // 48.15
  auto ret64 = x / ref_sqrt(y_1 * y_2);                           // 48.15
  return {ret64.real().get16(), ret64.imag().get16()};
}

static bool run_correlation_test(uint32_t n) {
  // use a smaller domain of inputs to avoid hitting saturation
  constexpr armral_cmplx_int16_t min = {-1024, -1024};
  constexpr armral_cmplx_int16_t max = {1024, 1024};
  armral::utils::cs16_random random;
  const auto a = random.vector(n, min, max);
  const auto b = random.vector(n, min, max);
  const auto ref = reference_correlation(n, a.data(), b.data());
  auto c = random.one();

  printf("[CORRELATION] - %u samples\n", n);
  armral_corr_coeff_i16(n, a.data(), b.data(), &c);
  return check_single_cs16("CORRELATION", c, ref);
}

int main(int argc, char **argv) {
  bool passed = true;
  for (uint32_t n = 1; n < 1026; ++n) {
    passed &= run_correlation_test(n);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
