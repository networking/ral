/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "cf32_utils.hpp"
#include "fft_utils.hpp"

#include <cfloat>
#include <complex>
#include <vector>

#ifdef ARMRAL_SEMIHOSTING
#define M_PI 3.14159265358979323846
#endif

namespace {

bool check_fft_results(const char *name, const armral_cmplx_f32_t *result,
                       const armral_cmplx_f32_t *expected, uint32_t n) {
  bool passed = true;
  float max_error = 0;

  // check absolute tolerance against eps scaled by the problem size (since
  // error will naturally grow as problem size increases). We have a fudge
  // factor set to 10, which will be updated with a more accurate count of
  // operations in the future
  float tol = FLT_EPSILON * (4 * n) * 10;

  for (uint32_t i = 0; i < n; ++i) {
    std::complex<float32_t> res = {result[i].re, result[i].im};
    std::complex<float32_t> exp = {expected[i].re, expected[i].im};
    float err = std::abs(res - exp);
    max_error = std::max(err, max_error);
    if (err > tol) {
      // GCOVR_EXCL_START
      passed = false;
      printf("Error! [%s] result[%u]= %.10f+%.10fi and expected[%u]= "
             "%.10f+%.10fi, diff: %.10f is greater than %.10f\n",
             name, i, res.real(), res.imag(), i, exp.real(), exp.imag(), err,
             tol);
      // GCOVR_EXCL_STOP
    }
  }

  printf("[%s] - check result: %s, max error was %.10f vs tolerance of %.10f\n",
         name, passed ? "OK" : "ERROR", max_error, tol);

  return passed;
}

std::vector<armral_cmplx_f32_t> run_fft_ref(int n, armral_fft_direction_t dir,
                                            const armral_cmplx_f32_t *x) {
  std::vector<std::complex<double>> in = armral::utils::widen_cf32(x, n);
  std::vector<std::complex<double>> out(n);
  armral::utils::fft_ref(n, 1, dir, in.data(), out.data());
  return armral::utils::narrow_to_cf32(out);
}

bool check_status(const armral_status ret_status, const char *message) {
  if (ret_status == ARMRAL_ARGUMENT_ERROR) {
    // GCOVR_EXCL_START
    printf("Error! %s\n", message);
    return false;
    // GCOVR_EXCL_STOP
  }
  return true;
}

bool run_fft_test(int n, armral_fft_direction_t dir) {
  printf("Testing FFT n=%d dir=%d\n", n, (int)dir);
  armral::utils::cf32_random random;
  const auto x = random.vector(n);
  auto y = random.vector(n);
  const auto y_ref = run_fft_ref(n, dir, x.data());

  armral_fft_plan_t *p = nullptr;
  auto plan_status = armral_fft_create_plan_cf32(&p, n, dir);
  if (!check_status(plan_status, "Failed to create a plan")) {
    // GCOVR_EXCL_START
    return false;
    // GCOVR_EXCL_STOP
  }

  auto execute_status = armral_fft_execute_cf32(p, x.data(), y.data());
  if (!check_status(execute_status, "Failed to execute plan")) {
    // GCOVR_EXCL_START
    return false;
    // GCOVR_EXCL_STOP
  }

  auto destroy_status = armral_fft_destroy_plan_cf32(&p);
  if (!check_status(destroy_status, "Failed to destroy plan")) {
    // GCOVR_EXCL_START
    return false;
    // GCOVR_EXCL_STOP
  }

  return check_fft_results("FFT", y.data(), y_ref.data(), n);
}

} // Anonymous namespace

int main(int argc, char **argv) {
  bool passed = true;
  constexpr int ns[] = {
      2,    3,    4,    5,    6,    7,    8,    9,    10,   11,   12,   13,
      14,   15,   16,   17,   18,   19,   20,   21,   22,   23,   24,   25,
      28,   29,   30,   31,   32,   37,   40,   41,   62,   74,   82,   86,
      106,  142,  202,  206,  226,  274,  314,  326,  394,  484,  542,  573,
      614,  626,  706,  758,  800,  802,  821,  838,  842,  926,  968,  1024,
      1063, 1198, 1202, 1366, 1728, 2013, 2025, 2030, 2128, 2209, 2401, 2557,
      3001, 3226, 3240, 3309, 3482, 3998, 4096, 9413};
  for (int n : ns) {
    for (auto dir : {ARMRAL_FFT_FORWARDS, ARMRAL_FFT_BACKWARDS}) {
      passed &= run_fft_test(n, dir);
    }
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
