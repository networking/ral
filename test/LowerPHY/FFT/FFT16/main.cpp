/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "cf32_utils.hpp"
#include "cs16_utils.hpp"
#include "fft_utils.hpp"
#include "qint64.hpp"

#include <cfloat>
#include <complex>
#include <vector>

#ifdef ARMRAL_SEMIHOSTING
#define M_PI 3.14159265358979323846
#endif

namespace {

float clamp_neg1_to_1(float x) {
  float low = -1.0;
  float high = (float32_t)((1 << 15) - 1) / (1 << 15);
  return std::max(low, std::min(high, x));
}

bool check_fft_results(const char *name, const armral_cmplx_int16_t *result,
                       const armral_cmplx_f32_t *expected, uint32_t n) {
  bool passed = true;
  float max_error = 0;

  // check absolute tolerance against eps scaled by the problem size (since
  // error will naturally grow as problem size increases).
  float tol = FLT_EPSILON * (4 * n - 1);
  // since the final result is rounded to Q0.15 format, this is also a
  // potential source of large error (especially for smaller problem sizes).
  tol = std::max((float32_t)2 / (1 << 15), tol);

  for (uint32_t i = 0; i < n; ++i) {
    auto res = std::complex<float32_t>((float32_t)result[i].re / (1 << 15),
                                       (float32_t)result[i].im / (1 << 15));
    auto exp = std::complex<float32_t>(clamp_neg1_to_1(expected[i].re),
                                       clamp_neg1_to_1(expected[i].im));
    auto err = std::abs(res - exp);
    max_error = std::max(max_error, err);
    if (err > tol) {
      // GCOVR_EXCL_START
      passed = false;
      printf("Error! [%s] result[%u]= %.10f+%.10fi and expected[%u]= "
             "%.10f+%.10fi, diff: %.10f is greater than %.10f\n",
             name, i, res.real(), res.imag(), i, exp.real(), exp.imag(), err,
             tol);
      // GCOVR_EXCL_STOP
    }
  }

  printf("[%s] - check result: %s, max error was %.10f vs tolerance of %.10f\n",
         name, passed ? "OK" : "ERROR", max_error, tol);

  return passed;
}

std::vector<armral_cmplx_f32_t> run_fft_ref(int n, armral_fft_direction_t dir,
                                            const armral_cmplx_int16_t *x) {
  std::vector<std::complex<double>> in(n);
  std::vector<std::complex<double>> out(n);
  for (int i = 0; i < n; i++) {
    in[i].real(x[i].re / (double)(1 << 15));
    in[i].imag(x[i].im / (double)(1 << 15));
  }
  armral::utils::fft_ref(n, 1, dir, in.data(), out.data());
  return armral::utils::narrow_to_cf32(out);
}

bool check_status(const armral_status ret_status, const char *message) {
  if (ret_status == ARMRAL_ARGUMENT_ERROR) {
    // GCOVR_EXCL_START
    printf("Error! %s\n", message);
    return false;
    // GCOVR_EXCL_STOP
  }
  return true;
}

bool run_fft_test(int n, armral_fft_direction_t dir) {
  printf("Testing FFT n=%d dir=%d\n", n, (int)dir);
  constexpr armral_cmplx_int16_t min = {-4096, -4096};
  constexpr armral_cmplx_int16_t max = {4095, 4095};
  armral::utils::cs16_random random;
  const auto x = random.vector(n, min, max);
  auto y = random.vector(n, min, max);
  const auto y_ref = run_fft_ref(n, dir, x.data());

  armral_fft_plan_t *p = nullptr;
  auto plan_status = armral_fft_create_plan_cs16(&p, n, dir);
  if (!check_status(plan_status, "Failed to create a plan")) {
    // GCOVR_EXCL_START
    return false;
    // GCOVR_EXCL_STOP
  }

  auto execute_status = armral_fft_execute_cs16(p, x.data(), y.data());
  if (!check_status(execute_status, "Failed to execute plan")) {
    // GCOVR_EXCL_START
    return false;
    // GCOVR_EXCL_STOP
  }

  auto destroy_status = armral_fft_destroy_plan_cs16(&p);
  if (!check_status(destroy_status, "Failed to destroy plan")) {
    // GCOVR_EXCL_START
    return false;
    // GCOVR_EXCL_STOP
  }

  return check_fft_results("FFT", y.data(), y_ref.data(), n);
}

} // Anonymous namespace

int main(int argc, char **argv) {
  bool passed = true;
  constexpr int ns[] = {2,   3,    4,    5,    6,    7,    8,    9,   10,  11,
                        12,  13,   14,   15,   16,   17,   18,   19,  20,  21,
                        22,  23,   24,   25,   32,   35,   36,   40,  45,  46,
                        47,  48,   50,   64,   65,   66,   68,   77,  81,  98,
                        99,  102,  112,  136,  121,  169,  170,  204, 238, 255,
                        272, 289,  342,  361,  440,  441,  484,  529, 552, 768,
                        800, 1024, 1125, 1140, 1170, 1104, 2048, 2401};
  for (int n : ns) {
    for (auto dir : {ARMRAL_FFT_FORWARDS, ARMRAL_FFT_BACKWARDS}) {
      passed &= run_fft_test(n, dir);
    }
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
