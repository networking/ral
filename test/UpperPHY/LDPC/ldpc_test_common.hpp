/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "armral.h"
#include "ldpc_coding.hpp"

#include <cassert>
#include <iostream>
#include <vector>

uint32_t get_ldpc_lifting_index(uint32_t lifting_size) {
  // Each lifting size is either a power of two,
  // or an odd multiple (up to 15) of a power of two. Find the first odd
  // number when shifting right, then divide that by two to get the index from
  // the mapping:
  // 2 -> 0
  // 3 -> 1
  // 5 -> 2
  // 7 -> 3
  // 9 -> 4
  // 11 -> 5
  // 13 -> 6
  // 15 -> 7
  assert(lifting_size > 0);
  auto lifting_set_index = lifting_size >> __builtin_ctz(lifting_size);
  assert(lifting_set_index <= 15);
  lifting_set_index >>= 1;
  return lifting_set_index;
}

bool perform_parity_check(const uint8_t *c, uint32_t z,
                          armral_ldpc_graph_t bg) {
  const auto *graph = armral_ldpc_get_base_graph(bg);
  std::vector<uint8_t> check(z * graph->nrows);
  auto *check_ptr = check.data();
  auto lsi = get_ldpc_lifting_index(z);
  // Loop through the rows in the base graph
  for (uint32_t row = 0; row < graph->nrows; ++row) {
    auto row_start_ind = graph->row_start_inds[row];
    auto num_cols = graph->row_start_inds[row + 1] - row_start_ind;
    const auto *col_ptr = graph->col_inds + row_start_ind;
    const auto *shift_ptr = graph->shifts +
                            row_start_ind * armral::ldpc::num_lifting_sets +
                            lsi * num_cols;
    // Loop through the rows in the block
    for (uint32_t zb = 0; zb < z; ++zb) {
      // Loop through the columns in the row
      for (uint32_t col = 0; col < num_cols; ++col) {
        auto shift = (shift_ptr[col] + zb) % z;
        auto col_ind = col_ptr[col] * z + shift;
        *check_ptr ^= c[col_ind];
      }
      check_ptr++;
    }
  }
  // Now check that all elements in the check are zero
  std::vector<uint32_t> error_checks;
  for (uint32_t i = 0; i < z * graph->nrows; ++i) {
    if (check[i] != 0) {
      error_checks.push_back(i); // GCOVR_EXCL_LINE
    }
  }
  const std::string graph_str = (bg == LDPC_BASE_GRAPH_1) ? "1" : "2";
  if (!error_checks.empty()) {
    // GCOVR_EXCL_START
    std::cout << "Errors in parity for base graph " << graph_str
              << "\nParity check failed for " << error_checks.size()
              << " checks. Indices are" << std::endl;
    for (auto check_ind : error_checks) {
      std::cout << check_ind << ", ";
    }
    std::cout << std::endl;
    // GCOVR_EXCL_STOP
  } else {
    std::cout << "Base graph " << graph_str << ": Parity check passed"
              << std::endl;
  }
  return error_checks.empty();
}
