/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"
#include "utils/bits_to_bytes.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstring>
#include <vector>

namespace {

void copy_bits(uint32_t src_bit, uint32_t start_idx, uint32_t len, uint32_t l,
               const uint8_t *in_bits, uint8_t *out) {
  for (uint32_t i = start_idx; i < len; i++) {
    uint32_t bit = (in_bits[src_bit / 8] >> (7 - src_bit % 8)) & 1;
    out[i / 8] |= (bit << (7 - (i % 8)));
    src_bit++;
    src_bit = src_bit % l;
  }
}

void ref_bit_selection(uint32_t z, uint32_t n, uint32_t e,
                       uint32_t len_filler_bits, uint32_t k, uint32_t k0,
                       const uint8_t *in, uint8_t *out) {

  const uint8_t *in_bits = in;
  std::vector<uint8_t> scratch_buf1(n);
  std::vector<uint8_t> scratch_buf2(n);
  auto *scratch_ptr1 = scratch_buf1.data();
  auto *scratch_ptr2 = scratch_buf2.data();

  // bit selection as specified by section 5.4.2.1 in 3GPP TS 38.212
  // remove filler bits
  if (len_filler_bits > 0) {

    uint32_t len_s_f_bits = k - z * 2; // length of systematic & filler bits
    uint32_t len_s_bits =
        len_s_f_bits - len_filler_bits;     // length of systematic bits
    uint32_t len_p_bits = n - len_s_f_bits; // length of parity bits

    if (len_filler_bits % 8 == 0) {
      uint32_t len_s_f_bytes = len_s_f_bits >> 3;
      uint32_t len_s_bytes = len_s_bits >> 3;
      uint32_t len_p_bytes = len_p_bits >> 3;

      memcpy(scratch_ptr1, in, len_s_bytes); // skip filler bits
      memcpy(&scratch_ptr1[len_s_bytes], &in[len_s_f_bytes],
             len_p_bytes); // skip filler bits

    } else {

      armral::bits_to_bytes(n, (const uint8_t *)in, (uint8_t *)scratch_ptr1);
      memcpy(scratch_ptr2, scratch_ptr1, len_s_bits);
      memcpy(&scratch_ptr2[len_s_bits], &scratch_ptr1[len_s_f_bits],
             len_p_bits);
      armral::bytes_to_bits((n - len_filler_bits),
                            (const uint8_t *)scratch_ptr2, scratch_ptr1);
    }

    in_bits = scratch_ptr1;
  }

  // k0 depends on the redundancy version id.
  assert(n > 0);
  assert(e > 0);
  assert(k0 < n);
  assert(n % 2 == 0);

  for (uint32_t i = 0; i < (e + 7) / 8; i++) {
    out[i] = 0;
  }

  uint32_t len = ((n - len_filler_bits) <= e) ? (n - len_filler_bits) : e;
  copy_bits(k0, 0, len, (n - len_filler_bits), in_bits, out);

  // repetition
  if (len < e) {
    copy_bits(k0, (n - len_filler_bits), e, (n - len_filler_bits), in_bits,
              out);
  }
}

void ref_bit_interleave(uint32_t e, uint32_t qm, const uint8_t *in,
                        uint8_t *out) {
  // performs the bit interleaving step of LDPC encoding, as specified in
  // section 5.4.2.2 of 3GPP TS 38.212.

  assert(e % qm == 0);

  // zero-initialise
  for (uint32_t i = 0; i < (e + 7) / 8; i++) {
    out[i] = 0;
  }

  // transpose
  int dst_bit = 0;
  for (uint32_t j = 0; j < e / qm; j++) {
    for (uint32_t i = 0; i < qm; i++) {
      uint32_t src_bit = i * (e / qm) + j;
      uint8_t src_byte = in[src_bit / 8];
      uint32_t bit = (src_byte >> (7 - (src_bit % 8))) & 1;
      out[dst_bit / 8] |= (bit << (7 - (dst_bit % 8)));
      dst_bit++;
    }
  }
}

bool test_ref_rate_matching() {
  bool passed = true;
  constexpr uint8_t in[4] = {0xA7, 0xFF, 0xFF, 0xA9};
  uint8_t out[4] = {0};

  // Test bit selection for k0 = 0.
  // Copy the first 30 bits. Expect the final 2 bits
  // to be pruned.
  ref_bit_selection(0, 30, 30, 0, 0, 0, in, out);
  for (int i = 0; i < 3; i++) {
    passed &= (in[i] == out[i]);
  }
  passed &= (out[3] == 0xA8);

  // Expect the first 23 bits of the input sequence.
  ref_bit_selection(0, 30, 23, 0, 0, 0, in, out);
  passed &= (out[0] == in[0]);
  passed &= (out[1] == in[1]);
  passed &= (out[2] == 0xFE);

  // Expect the first 26 bits of the input sequence.
  ref_bit_selection(0, 30, 26, 0, 0, 0, in, out);
  passed &= (out[0] == in[0]);
  passed &= (out[1] == in[1]);
  passed &= (out[2] == in[2]);
  passed &= (out[3] == 0x80);

  // Expect repetition of the first 8 bits.
  ref_bit_selection(0, 16, 24, 0, 0, 0, in, out);
  passed &= (out[0] == in[0]);
  passed &= (out[1] == in[1]);
  passed &= (out[2] == in[0]);

  // Expect input repeated twice.
  ref_bit_selection(0, 16, 32, 0, 0, 0, in, out);
  passed &= (out[0] == in[0]) && (out[2] == in[0]);
  passed &= (out[1] == in[1]) && (out[3] == in[1]);

  // Expect repetition of the first 6 bits.
  ref_bit_selection(0, 16, 22, 0, 0, 0, in, out);
  passed &= (out[0] == in[0]);
  passed &= (out[1] == in[1]);
  passed &= (out[2] == 0xA4);

  // A7 = 1010 01  11
  // Expect repetition of 1010 01
  ref_bit_selection(0, 6, 16, 0, 0, 0, in, out);
  passed &= (out[0] == 0xA6);
  passed &= (out[1] == 0x9A);

  // A7F = 1010 0111 11
  // Expect repetition of 1010 0111 1110 1001
  ref_bit_selection(0, 10, 16, 0, 0, 0, in, out);
  passed &= (out[0] == in[0]);
  passed &= (out[1] == 0xE9);

  // rv_id = 1, bg = 1, N_cb = 32, Z_C = 2 gives k0 = 16
  ref_bit_selection(0, 30, 30, 0, 0, 16, in, out);
  passed &= (out[0] == in[2]);
  passed &= (out[1] == 0xAA);
  passed &= (out[2] == 0x9F);
  passed &= (out[3] == 0xFC);

  ref_bit_selection(0, 30, 23, 0, 0, 16, in, out);
  passed &= (out[0] == in[2]);
  passed &= (out[1] == 0xAA);
  passed &= (out[2] == 0x9E);

  // Expect input repeated twice.
  ref_bit_selection(0, 24, 32, 0, 0, 16, in, out);
  passed &= (out[0] == in[2]);
  passed &= (out[1] == in[0]);
  passed &= (out[2] == in[1]);
  passed &= (out[3] == in[2]);

  // rv_id = 0  z = 7 , n = 350, e  = 328, len_filler_bits = 16 k = 70
  constexpr uint8_t in_filler[] = {
      0x22, 0x35, 0x72, 0xd4, 0xb5, 0x00, 0x00, 0x9a, 0x32, 0xd0, 0x45,
      0x6d, 0x18, 0x10, 0xfa, 0xf8, 0xa4, 0x5e, 0x8c, 0x88, 0x1f, 0x8a,
      0xf6, 0x66, 0xad, 0xc8, 0xb0, 0xc8, 0xe6, 0xca, 0x5c, 0x4e, 0x0a,
      0x59, 0x47, 0x33, 0xb8, 0x61, 0x0c, 0x6c, 0x8c, 0xa8, 0xa8, 0xb0};
  uint8_t out_filler[328 >> 3] = {0};
  ref_bit_selection(7, 350, 328, 16, 70, 0, in_filler, out_filler);

  for (uint16_t i = 0, j = 0; i < (328 >> 3); i++) {
    if ((i != 5) && (i != 6)) {
      passed &= (in_filler[i] == out_filler[j++]);
    }
  }

  // Test bit interleaving.
  // in:  1010 0111 1111
  // out = 1011 0011 1111
  int qm = 4;
  ref_bit_interleave(12, qm, in, out);
  passed &= (out[0] == 0xB3);
  passed &= (out[1] == 0xF0);

  // in:  1111 1111 1010
  // out: 1110 1111 1110
  ref_bit_interleave(12, qm, in + 2, out);
  passed &= (out[0] == 0xEF);
  passed &= (out[1] == 0xE0);

  // e = 16 bits, qm = 4.
  // in:  1010 0111 1111 1111
  // out: 1011 0111 1111 0111
  ref_bit_interleave(16, qm, in, out);
  passed &= (out[0] == 0xB7);
  passed &= (out[1] == 0xF7);

  // e = 32 bits, qm = 4.
  // in:  1010 0111 1111 1111 1111 1111 1010 1001
  // out: 1111 0110 1111 0110 0111 1110 1110 1111
  ref_bit_interleave(32, qm, in, out);
  passed &= (out[0] == 0xF6);
  passed &= (out[1] == 0xF6);
  passed &= (out[2] == 0x7E);
  passed &= (out[3] == 0xEF);

  // e = 32 bits, qm = 8.
  // in:  1010 0111 1111 1111 1111 1111 1010 1001
  // out: 1111 0110 1111 0110 0111 1110 1110 1111
  ref_bit_interleave(32, qm, in, out);
  passed &= (out[0] == 0xF6);
  passed &= (out[1] == 0xF6);
  passed &= (out[2] == 0x7E);
  passed &= (out[3] == 0xEF);

  return passed;
}

uint32_t starting_position(armral_ldpc_graph_t bg, uint32_t rv, uint32_t n,
                           uint32_t ncb, uint32_t z) {
  // Starting position k0 of different redundancy versions
  // given as Table 5.4.2.1-2 in 3GPP TS 38.212, simplified
  // using the assumption N_cb = 66 * Z_c (base graph 1) or
  // N_cb = 50 * Z_c (base graph 2).
  switch (rv) {
  case 0:
  default:
    return 0;
  case 1:
    switch (bg) {
    case LDPC_BASE_GRAPH_1:
    default:
      return 17 * z * (ncb / n);
    case LDPC_BASE_GRAPH_2:
      return 13 * z * (ncb / n);
    }
  case 2:
    switch (bg) {
    case LDPC_BASE_GRAPH_1:
    default:
      return 33 * z * (ncb / n);
    case LDPC_BASE_GRAPH_2:
      return 25 * z * (ncb / n);
    }
  case 3:
    switch (bg) {
    case LDPC_BASE_GRAPH_1:
    default:
      return 56 * z * (ncb / n);
    case LDPC_BASE_GRAPH_2:
      return 43 * z * (ncb / n);
    }
  }
}

uint32_t src_length(armral_ldpc_graph_t bg, uint32_t z) {
  // N as defined in subclause 5.3.2 in 3GPP TS 38.212.
  switch (bg) {
  case LDPC_BASE_GRAPH_1:
  default:
    return 66 * z;
  case LDPC_BASE_GRAPH_2:
    return 50 * z;
  }
}

uint32_t num_bit_per_symbol(armral_modulation_type mod_type) {
  // Duplicate in simulation/turbo_awgn/turbo_awgn.cpp
  switch (mod_type) {
  case ARMRAL_MOD_QPSK:
    return 2;
  case ARMRAL_MOD_16QAM:
    return 4;
  case ARMRAL_MOD_64QAM:
    return 6;
  case ARMRAL_MOD_256QAM:
    return 8;
  default:
    printf("Invalid mod_type\n");
    exit(EXIT_FAILURE);
  }
}

void armral_ref_rate_matching(armral_ldpc_graph_t bg, uint32_t z, uint32_t e,
                              uint32_t nref, uint32_t len_filler_bits,
                              uint32_t k, uint32_t rv,
                              armral_modulation_type mod, const uint8_t *src,
                              uint8_t *dst) {
  uint32_t ncb;
  uint32_t qm = num_bit_per_symbol(mod);
  uint32_t n = src_length(bg, z);

  if (nref != 0) {
    ncb = (n > nref) ? nref : n;
  } else {
    ncb = n;
  }
  uint32_t k0 = starting_position(bg, rv, n, ncb, z);
  std::vector<uint8_t> selected(e);
  ref_bit_selection(z, n, e, len_filler_bits, k, k0, src, selected.data());
  ref_bit_interleave(e, qm, selected.data(), dst);
}

template<typename LDPCRateMatchingFunction>
bool test_ldpc_rate_matching(
    char const *test_name,
    LDPCRateMatchingFunction ldpc_rate_matching_under_test) {
  bool passed = true;
  const armral_ldpc_graph_t base_graph_list[] = {LDPC_BASE_GRAPH_1,
                                                 LDPC_BASE_GRAPH_2};
  // Arbitrary subset of lifting sizes given in Table 5.3.2-1.
  const uint32_t lifting_size_list[] = {2, 3, 8, 11, 28, 32, 104};
  const uint32_t rv_list[] = {0, 1, 2, 3};
  const uint8_t rb_list[] = {1, 1, 1, 3, 4, 7, 24};
  const uint32_t filler_bits_list[] = {0, 28, 32};
  uint32_t lifting_size_list_len =
      sizeof(lifting_size_list) / sizeof(lifting_size_list[0]);
  uint32_t lifting_size_min = *std::min_element(
      lifting_size_list, lifting_size_list + lifting_size_list_len);
  uint32_t lifting_size_max = *std::max_element(
      lifting_size_list, lifting_size_list + lifting_size_list_len);
  // Prepare nref_list for both BG1 and BG2 using lifting_size_list
  const uint32_t nref_list[2][3] = {
      {0, (66 * lifting_size_min), (66 * lifting_size_max)}, // BG1
      {0, (50 * lifting_size_min), (50 * lifting_size_max)}, // BG2
  };

  // Arbitrary subset of modulation schemes.
  const armral_modulation_type mod_list[] = {ARMRAL_MOD_QPSK,
                                             ARMRAL_MOD_256QAM};
  for (auto z : lifting_size_list) {
    static uint8_t rb_idx = 0;
    for (auto bg : base_graph_list) {
      for (auto len_filler_bits : filler_bits_list) {
        if (z < 28) {
          len_filler_bits = 0;
        }
        // Default to base graph 1
        uint32_t num_bits = src_length(bg, z);
        uint32_t num_bytes_enc_out = (num_bits + 7) / 8;
        std::vector<uint8_t> src_store =
            armral::utils::allocate_random_u8(num_bytes_enc_out, 0U, 1U);
        std::vector<uint8_t> src = std::vector<uint8_t>(num_bytes_enc_out);
        for (auto mod : mod_list) {
          uint32_t qm = num_bit_per_symbol(mod);
          // Choose G as number of coded bits. The specs define
          // 'G is the total number of coded bits available for transmission
          // of the transpose block'.
          // G should be revised once a better understanding of what
          // values G attains has been obtained.
          uint32_t g = 22 * z;
          if (bg == LDPC_BASE_GRAPH_2) {
            g = 10 * z;
          }
          // Consider single layer, single CB.
          uint32_t num_res =
              qm *
              (mod == ARMRAL_MOD_QPSK ? 144 : 32); // 12 symbols or 3 symbols
          uint32_t e = rb_list[rb_idx] * num_res;
          uint32_t num_bytes_ratematch_out = (e + 7) / 8;
          for (auto rv : rv_list) {
            for (auto nref : nref_list[bg]) {
              std::vector<uint8_t> ref =
                  std::vector<uint8_t>(num_bytes_ratematch_out);
              std::vector<uint8_t> dst =
                  std::vector<uint8_t>(num_bytes_ratematch_out);

              memcpy(src.data(), src_store.data(), num_bytes_enc_out);
              ldpc_rate_matching_under_test(bg, z, e, nref, len_filler_bits, g,
                                            rv, mod, src.data(), dst.data());
              memcpy(src.data(), src_store.data(), num_bytes_enc_out);
              armral_ref_rate_matching(bg, z, e, nref, len_filler_bits, g, rv,
                                       mod, src.data(), ref.data());
              passed &= armral::utils::check_results_u8(
                  test_name, dst.data(), ref.data(), num_bytes_ratematch_out);
            }
          }
        }
      }
    }
    rb_idx++;
  }
  return passed;
}

} // anonymous namespace

int main(int argc, char **argv) {
  bool passed = test_ref_rate_matching();
  assert(passed && "ldpc_ref_rate_matching failed");
  passed =
      test_ldpc_rate_matching("ldpc_rate_matching", armral_ldpc_rate_matching);
  passed &= test_ldpc_rate_matching(
      "ldpc_rate_matching_noalloc",
      [](armral_ldpc_graph_t bg, uint32_t z, uint32_t e, auto... args) {
        std::vector<uint8_t> buffer(e + 2 * z * 66);
        return armral_ldpc_rate_matching_noalloc(bg, z, e, args...,
                                                 buffer.data());
      });
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
