/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "../ldpc_test_common.hpp"
#include "armral.h"
#include "int_utils.hpp"
#include "utils/bits_to_bytes.hpp"

#include <algorithm>
#include <arm_neon.h>
#include <array>
#include <cstring>
#include <vector>

namespace {

void ldpc_crc_attachment(const uint8_t *data_in, uint32_t k_prime, uint32_t k,
                         uint8_t *data_out) {
  // Initialize the output to zero, which takes care of the filler bits
  memset(data_out, 0, sizeof(uint8_t) * ((k + 7) >> 3));

  // Number of crc bits to be attached to the code block. For LDPC this is 24.
  constexpr uint32_t l = 24;

  // Number of bytes containing information bits (not including CRC bits)
  uint32_t num_info_bits = k_prime - l;
  uint32_t num_info_bytes = (num_info_bits + 7) >> 3;

  // The routine we call to generate the CRC (armral_crc24b_be) expects a
  // particular size of input (either n = 8, or n % 16 = 0 where n is the number
  // of bytes). We know that the input data for CRC attachment is always greater
  // than 8 bytes, so we don't need to deal with cases where n <= 8. For n > 8,
  // we pad the input to be a multiple of 16 bytes.
  std::vector<uint8_t> buffer;
  uint32_t buffer_size = num_info_bytes;
  if (num_info_bits % 128 != 0) {
    // Pad zeros in the MSBs so we give the correct size of input to the CRC
    // calculation routine
    uint32_t num_pad_bits = 128 - (num_info_bits % 128);
    buffer_size = (num_info_bits + num_pad_bits) >> 3;
    buffer = std::vector<uint8_t>(buffer_size);

    // Copy the information bits after the zero-padding bits
    uint32_t idx = num_pad_bits >> 3;
    uint32_t shift1 = num_pad_bits % 8;
    uint32_t shift2 = 8 - shift1;
    buffer[idx] = data_in[0] >> shift1;
    for (uint32_t i = 0; i < num_info_bytes - 1; i++) {
      buffer[idx + i + 1] = (data_in[i] << shift2) | (data_in[i + 1] >> shift1);
    }
  } else {
    // The input is already the correct length so just copy it into the buffer
    buffer = std::vector<uint8_t>(num_info_bytes);
    memcpy(buffer.data(), data_in, sizeof(uint8_t) * num_info_bytes);
  }

  // Generate the CRC parity bits
  uint64_t crc = 0;
  armral_crc24_b_be(buffer_size, (const uint64_t *)buffer.data(), &crc);

  // Copy the bytes containing information bits
  memcpy(data_out, data_in, sizeof(uint8_t) * num_info_bytes);

  // Append the CRC bits to the information bits. If the information bits don't
  // align with a byte boundary then do the necessary shifts
  uint32_t info_rem_bits = num_info_bits % 8;
  uint32_t shift = info_rem_bits > 0 ? 8 - info_rem_bits : 0;

  // Make sure any non-information bits in the last information byte are zeroed
  data_out[num_info_bytes - 1] &= (255 << shift);

  // Put the CRC bits into place
  data_out[num_info_bytes - 1] |= (crc >> (24 - shift));
  data_out[num_info_bytes] = crc >> (16 - shift);
  data_out[num_info_bytes + 1] = crc >> (8 - shift);
  data_out[num_info_bytes + 2] = crc << shift;
}

bool check_decoded_message(uint32_t len, const uint8_t *orig,
                           const uint8_t *decoded) {
  // We assume that the messages passed in are not packed bits, and that each
  // byte represents a bit in a message. That makes identification of the bits
  // which differ a little easier than going through packed bytes.
  bool passed = true;
  for (uint32_t i = 0; i < len; ++i) {
    if (orig[i] != decoded[i]) {
      // GCOVR_EXCL_START
      printf("Bit at index %u does not match. Expected %u, but got %u\n", i,
             orig[i], decoded[i]);
      passed = false;
      // GCOVR_EXCL_STOP
    }
  }
  return passed;
}

template<typename LDPCDecodingFunction>
bool run_ldpc_decoding_test(uint32_t its, uint32_t z, armral_ldpc_graph_t bg,
                            uint32_t crc_idx,
                            LDPCDecodingFunction ldpc_decoding_under_test) {

  bool passed = true;
  const auto *graph = armral_ldpc_get_base_graph(bg);

  // Allocate a random input to be encoded
  uint32_t len_in = z * graph->nmessage_bits;
  armral::utils::int_random<uint8_t> random;
  auto to_encode = random.vector((len_in + 7) / 8);

  // If we are doing CRC checking, then we need to attach CRC bits to the input
  if (crc_idx != ARMRAL_LDPC_NO_CRC) {
    auto info_to_encode = random.vector((len_in + 7) / 8);
    len_in = crc_idx + 24;
    ldpc_crc_attachment(info_to_encode.data(), len_in, z * graph->nmessage_bits,
                        to_encode.data());
  }

  uint32_t encoded_len = z * graph->ncodeword_bits;
  auto encoded = random.vector((encoded_len + 7) / 8);
  uint32_t len_filler_bits = 0;
  // Encode the data
  armral_ldpc_encode_block(to_encode.data(), bg, z, len_filler_bits,
                           encoded.data());

  // run modulation
  armral_modulation_type mod_type = ARMRAL_MOD_16QAM;
  int mod_num_symbols = (encoded_len + 3) / 4;
  std::vector<armral_cmplx_int16_t> data_mod(mod_num_symbols);
  armral_modulation(mod_num_symbols * 4, mod_type, encoded.data(),
                    data_mod.data());

  // Now demodulate to get LLRs
  // The value of ulp shouldn't matter for a noiseless channel, but a small
  // ulp helps test that overflow in the llrs is handled correctly.
  uint16_t ulp = 11;
  std::vector<int8_t> data_demod_soft(mod_num_symbols * 4);
  armral_demodulation(mod_num_symbols, ulp, mod_type, data_mod.data(),
                      data_demod_soft.data());

  auto decoded = random.vector((len_in + 7) / 8);
  if (ldpc_decoding_under_test(data_demod_soft.data(), bg, z, crc_idx, its,
                               decoded.data()) != ARMRAL_SUCCESS) {
    return false;
  }
  auto decoded_bytes = armral::bits_to_bytes(len_in, decoded.data());

  // Also check that the decoded message is equal to the original message
  auto bytes_in = armral::bits_to_bytes(len_in, to_encode.data());
  passed &=
      check_decoded_message(len_in, bytes_in.data(), decoded_bytes.data());
  return passed;
}

} // anonymous namespace

template<typename LDPCDecodingFunction>
bool run_all_tests(char const *name,
                   LDPCDecodingFunction ldpc_decoding_under_test) {
  bool passed = true;
  std::array bgs{LDPC_BASE_GRAPH_1, LDPC_BASE_GRAPH_2};
  std::array num_its{1, 2, 5, 10};
  std::array zs{2, 6, 13, 20, 30, 56, 144, 208, 224, 256, 320, 384};
  // Crc-index is zero based indexing
  std::array crc_idx_1{4225, 4553, 5257, 6313, 7721};
  std::array crc_idx_2{1921, 2057, 2377, 2857, 3497};
  for (auto bg : bgs) {
    const auto &crc_ids = (bg == 0) ? crc_idx_1 : crc_idx_2;
    for (uint32_t i = 0; i < zs.size(); i++) {
      auto z = zs[i];
      assert(z < 208 || i >= 7);
      auto crc_idx = (z >= 208) ? (crc_ids[i - 7] - 1) : ARMRAL_LDPC_NO_CRC;
      for (auto its : num_its) {
        printf("[%s] z = %d, crc_idx = %u, its = %d\n", name, z, crc_idx, its);
        auto check = run_ldpc_decoding_test(its, z, bg, crc_idx,
                                            ldpc_decoding_under_test);
        if (!check) {
          // GCOVR_EXCL_START
          printf("Decoding failed\n");
          // GCOVR_EXCL_STOP
        }
        passed &= check;
      }
    }
  }
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;

  passed &= run_all_tests("LDPCDecoding", armral_ldpc_decode_block);

  passed &= run_all_tests(
      "LDPCDecodingNoAlloc",
      [](const int8_t *llrs, armral_ldpc_graph_t bg, uint32_t z,
         uint32_t crc_idx, uint32_t num_its, uint8_t *data_out) {
        auto buffer_size = armral_ldpc_decode_block_noalloc_buffer_size(
            bg, z, crc_idx, num_its);
        std::vector<uint8_t> buffer(buffer_size);
        return armral_ldpc_decode_block_noalloc(llrs, bg, z, crc_idx, num_its,
                                                data_out, buffer.data());
      });

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
