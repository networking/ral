/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <cstring>
#include <vector>

namespace {

void ref_undo_selection(uint32_t z, uint32_t n, uint32_t e,
                        uint32_t len_filler_bits, uint32_t k, uint32_t k0,
                        const int8_t *in, int8_t *out) {
  // performs the inverse of the bit selection as specified by
  // section 5.4.2.1 in 3GPP TS 38.212
  assert(k0 < n);
  assert(e > 0);

  // As we aggregate LLRs, for a single message, out should be zero on entry.
  for (uint32_t i = 0; i < n; i++) {
    assert(out[i] == 0);
  }

  // systematic bits len
  uint32_t len_s_bits = k - len_filler_bits - (2 * z);
  int32_t llr;
  uint32_t k_idx = 0;
  uint32_t k0_start = k0;
  uint32_t soft_buff_len = n - len_filler_bits;

  // fill the data in output systematic region
  if (k0 < len_s_bits) {
    for (; (k0 < len_s_bits) && (k_idx < e); k0++) {
      out[k0] = in[k_idx++];
    }
  }

  // set filler bits
  if (len_filler_bits > 0) {
    memset(&out[len_s_bits], INT8_MAX, len_filler_bits * sizeof(int8_t));
  }

  // offset k0 to skip filler bits
  if (k0 >= len_s_bits && k0 < len_s_bits + len_filler_bits) {
    k0 = len_s_bits + len_filler_bits;
  }
  // fill the data in output parity region
  for (; (k0 < n) && (k_idx < e); k0++) {
    out[k0] = in[k_idx++];
  }

  if (((n != e) && (k_idx != n)) || ((n == e) && (k0 != 0))) {
    while (k_idx < e) {
      // fill the data in output systematic region
      for (k0 = 0; (k0 < len_s_bits) && (k_idx < e); k0++) {
        out[k0] = in[k_idx++];
      }
      // fill the data in output parity region, filler bits skipped
      for (k0 = len_s_bits + len_filler_bits; (k0 < n) && (k_idx < e); k0++) {
        out[k0] = in[k_idx++];
      }
    }
  }

  // soft combining
  if ((e > soft_buff_len) || ((e == n) && (len_filler_bits != 0))) {
    k_idx = soft_buff_len;
    k0 = k0_start;
    // systematic region
    if (k0 < len_s_bits) {
      for (; (k0 < len_s_bits) && (k_idx < e); k0++) {
        llr = out[k0] + in[k_idx++];
        if (llr < (int32_t)INT8_MIN) {
          out[k0] = INT8_MIN;
        } else if (llr > (int32_t)INT8_MAX) {
          out[k0] = INT8_MAX;
        } else {
          out[k0] = (int8_t)llr;
        }
      }
    }

    // offset k0 to skip filler bits
    if (k0 >= len_s_bits && k0 < len_s_bits + len_filler_bits) {
      k0 = len_s_bits + len_filler_bits;
    }

    // parity region
    for (; (k0 < n) && (k_idx < e); k0++) {
      llr = out[k0] + in[k_idx++];
      if (llr < (int32_t)INT8_MIN) {
        out[k0] = INT8_MIN;
      } else if (llr > (int32_t)INT8_MAX) {
        out[k0] = INT8_MAX;
      } else {
        out[k0] = (int8_t)llr;
      }
    }
  }
}

void ref_undo_interleave(uint32_t e, uint32_t qm, const int8_t *in,
                         int8_t *out) {
  // performs the inverse of the bit interleaving step of LDPC encoding,
  // as specified in section 5.4.2.2 of 3GPP TS 38.212.
  assert(e > qm);
  assert(qm > 0);
  assert(e % qm == 0);
  assert(in != out);

  for (uint32_t j = 0; j < e / qm; j++) {
    for (uint32_t i = 0; i < qm; i++) {
      assert(j + i * (e / qm) < e);
      assert(i + j * qm < e);
      out[j + i * (e / qm)] = in[i + j * qm];
    }
  }
}

uint32_t starting_position(armral_ldpc_graph_t bg, uint32_t rv, uint32_t n,
                           uint32_t ncb, uint32_t z) {
  // Duplicate of function with same name in rate_matching

  // Starting position k0 of different redundancy versions
  // given as Table 5.4.2.1-2 in 3GPP TS 38.212, simplified
  // using the assumption N_cb = 66 * Z_c (base graph 1) or
  // N_cb = 50 * Z_c (base graph 2).
  switch (rv) {
  case 0:
  default:
    return 0;
  case 1:
    switch (bg) {
    case LDPC_BASE_GRAPH_1:
    default:
      return 17 * z * (ncb / n);
    case LDPC_BASE_GRAPH_2:
      return 13 * z * (ncb / n);
    }
  case 2:
    switch (bg) {
    case LDPC_BASE_GRAPH_1:
    default:
      return 33 * z * (ncb / n);
    case LDPC_BASE_GRAPH_2:
      return 25 * z * (ncb / n);
    }
  case 3:
    switch (bg) {
    case LDPC_BASE_GRAPH_1:
    default:
      return 56 * z * (ncb / n);
    case LDPC_BASE_GRAPH_2:
      return 43 * z * (ncb / n);
    }
  }
}

uint32_t num_bit_per_symbol(armral_modulation_type mod_type) {
  // Duplicate in armral/simulation/include/simulation_common.hpp
  switch (mod_type) {
  case ARMRAL_MOD_QPSK:
    return 2;
  case ARMRAL_MOD_16QAM:
    return 4;
  case ARMRAL_MOD_64QAM:
    return 6;
  case ARMRAL_MOD_256QAM:
    return 8;
  default:
    printf("Invalid mod_type\n");
    exit(EXIT_FAILURE);
  }
}

void armral_ref_rate_recovery(armral_ldpc_graph_t bg, uint32_t z, uint32_t e,
                              uint32_t nref, uint32_t len_filler_bits,
                              uint32_t k, uint32_t rv,
                              armral_modulation_type mod, const int8_t *src,
                              int8_t *dst) {
  uint32_t ncb = 0;
  uint32_t n = (bg == LDPC_BASE_GRAPH_2) ? 50 * z : 66 * z;

  if (nref != 0) {
    ncb = (n > nref) ? nref : n;
  } else {
    ncb = n;
  }

  uint32_t k0 = starting_position(bg, rv, n, ncb, z);
  uint32_t qm = num_bit_per_symbol(mod);
  std::vector<int8_t> llrs(e);
  ref_undo_interleave(e, qm, src, llrs.data());
  ref_undo_selection(z, n, e, len_filler_bits, k, k0, llrs.data(), dst);
}

bool test_ref_rate_recovery() {
  bool passed = true;

  // Test ring behavior of selection process.
  uint32_t e = 100;
  uint32_t n = 100;
  uint32_t k0 = 16;
  uint32_t z = 2;
  uint32_t k = 20;
  auto in = armral::utils::allocate_random_i8(e);
  std::vector<int8_t> out(n);
  ref_undo_selection(z, n, e, 0, k, k0, in.data(), out.data());
  passed &= std::equal(out.begin() + k0, out.begin() + n, in.begin());
  passed &= std::equal(out.begin(), out.begin() + k0, in.begin() + (e - k0));

  // Test selection process with shortening
  e = 80;
  n = 100;
  k0 = 16;
  k = 20;
  in = armral::utils::allocate_random_i8(e);
  memset(out.data(), 0, n * sizeof(int8_t));
  ref_undo_selection(z, n, e, 0, k, k0, in.data(), out.data());
  passed &= std::all_of(out.begin(), out.begin() + k0,
                        [](uint8_t i) { return i == 0; });
  passed &= std::equal(out.begin() + k0, out.begin() + k0 + e, in.begin());
  passed &= std::all_of(out.begin() + (e + k0), out.begin() + n,
                        [](uint8_t i) { return i == 0; });

  // Test summation of llrs
  e = 100;
  // Every llr is transmitted twice
  n = 50;
  k0 = 0;
  k = 20;
  in = armral::utils::allocate_random_i8(e);
  // The final llrs is the sum of the i-th and (i+16)-th llr.
  // Ensure that saturation is tested at least once.
  in[0] = INT8_MIN;
  in[50] = -1;
  in[1] = INT8_MAX;
  in[51] = 1;
  // Half the values so that saturation cannot be reached.
  for (uint32_t i = 2; i < 50; i++) {
    in[i] = in[i] / 2;
    in[50 + i] = in[50 + i] / 2;
  }

  memset(out.data(), 0, n * sizeof(int8_t));
  ref_undo_selection(z, n, e, 0, k, k0, in.data(), out.data());
  passed &= (out[0] == INT8_MIN);
  passed &= (out[1] == INT8_MAX);
  for (uint32_t i = 2; i < 50; i++) {
    int32_t sol = (int32_t)in[i] + (int32_t)in[i + 50];
    passed &= (out[i] == (int8_t)sol);
  }

  // Test selection process with filler bits
  e = 80;
  n = 100;
  k0 = 0;
  uint32_t f = 8;
  k = 20;
  auto in_filler = armral::utils::allocate_random_i8(e);
  std::vector<int8_t> out_filler(n);
  ref_undo_selection(z, n, e, f, z * 10, k0, in_filler.data(),
                     out_filler.data());

  for (uint16_t i = 0, j = 0; i < n; i++) {
    if (i < (z * 10 - 2 * z - f)) {
      passed &= (in_filler[j] == out_filler[i]);
      j++;
    } else if ((i >= (z * 10 - 2 * z - f)) && (i < (z * 10 - 2 * z))) {
      passed &= (INT8_MAX == out_filler[i]);
    } else if (n <= e) {
      passed &= (in_filler[j] == out_filler[i]);
      j++;
    } else {
      if (i < (e + f)) {
        passed &= (in_filler[j] == out_filler[i]);
        j++;
      } else {
        passed &= (0 == out_filler[i]);
      }
    }
  }

  // Test reversal of interleaving
  e = 16;
  std::iota(in.begin(), in.begin() + 16, 1);
  ref_undo_interleave(e, 4, in.data(), out.data());
  std::vector<int8_t> sol = {1, 5, 9,  13, 2, 6, 10, 14,
                             3, 7, 11, 15, 4, 8, 12, 16};
  passed &= std::equal(out.begin(), out.begin() + e, sol.begin());
  ref_undo_interleave(e, 2, in.data(), out.data());
  sol = {1, 3, 5, 7, 9, 11, 13, 15, 2, 4, 6, 8, 10, 12, 14, 16};
  passed &= std::equal(out.begin(), out.begin() + e, sol.begin());

  return passed;
}

template<typename LDPCRateRecoveryFunction>
bool test_ldpc_rate_recovery(
    char const *name, LDPCRateRecoveryFunction ldpc_rate_recovery_under_test) {
  bool passed = true;
  const armral_ldpc_graph_t base_graph_list[] = {LDPC_BASE_GRAPH_1,
                                                 LDPC_BASE_GRAPH_2};
  // Arbitrary subset of lifting sizes given in Table 5.3.2-1.
  const uint32_t lifting_size_list[] = {2, 3, 8, 11, 28, 32, 104};
  uint32_t lifting_size_list_len =
      sizeof(lifting_size_list) / sizeof(lifting_size_list[0]);
  uint32_t lifting_size_min = *std::min_element(
      lifting_size_list, lifting_size_list + lifting_size_list_len);
  uint32_t lifting_size_max = *std::max_element(
      lifting_size_list, lifting_size_list + lifting_size_list_len);
  // Prepare nref_list for both BG1 and BG2 using lifting_size_list
  const uint32_t nref_list[2][3] = {
      {0, (66 * lifting_size_min), (66 * lifting_size_max)}, // BG1
      {0, (50 * lifting_size_min), (50 * lifting_size_max)}, // BG2
  };
  const uint8_t rb_list[] = {1, 1, 1, 3, 4, 7, 24};
  const uint32_t filler_bits_list[] = {0, 28, 36};
  // Arbitrary subset of modulation schemes.
  const armral_modulation_type mod_list[] = {ARMRAL_MOD_QPSK,
                                             ARMRAL_MOD_256QAM};
  // Arbitrary subset of redundancy versions.
  const uint32_t rv_list[] = {0, 1};

  for (auto z : lifting_size_list) {
    static uint8_t rb_idx = 0;
    for (auto bg : base_graph_list) {
      uint32_t n = (bg == LDPC_BASE_GRAPH_2) ? 50 * z : 66 * z;
      for (auto len_filler_bits : filler_bits_list) {
        if (z < 28) {
          len_filler_bits = 0;
        }
        for (auto mod : mod_list) {
          uint32_t qm = num_bit_per_symbol(mod);
          uint32_t g = (bg == LDPC_BASE_GRAPH_2) ? 10 * z : 22 * z;
          uint32_t num_res =
              qm *
              (mod == ARMRAL_MOD_QPSK ? 144 : 32); // 12 symbols or 3 symbols
          uint32_t e = rb_list[rb_idx] * num_res;
          for (auto rv : rv_list) {
            for (auto nref : nref_list[bg]) {
              std::vector<int8_t> llrs_in =
                  armral::utils::allocate_random_i8(e);
              std::vector<int8_t> llrs_out(n);
              std::vector<int8_t> llrs_ref(n);
              armral_ref_rate_recovery(bg, z, e, nref, len_filler_bits, g, rv,
                                       mod, llrs_in.data(), llrs_ref.data());
              ldpc_rate_recovery_under_test(bg, z, e, nref, len_filler_bits, g,
                                            rv, mod, llrs_in.data(),
                                            llrs_out.data());
              passed &= (llrs_ref == llrs_out);
            }
          }
        }
      }
    }
    rb_idx++;
  }

  if (!passed) {
    // GCOVR_EXCL_START
    printf("[%s] one or more tests failed!\n", name);
    // GCOVR_EXCL_STOP
  }
  return passed;
}
} // anonymous namespace

int main(int argc, char **argv) {
  bool passed = test_ref_rate_recovery();
  passed &=
      test_ldpc_rate_recovery("LDPCRateRecovery", armral_ldpc_rate_recovery);
  passed &= test_ldpc_rate_recovery(
      "LDPCRateRecoveryNoAlloc",
      [](armral_ldpc_graph_t bg, uint32_t z, uint32_t e, auto... args) {
        std::vector<uint8_t> buffer(e + z * 66);
        return armral_ldpc_rate_recovery_noalloc(bg, z, e, args...,
                                                 buffer.data());
      });
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
