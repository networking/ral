/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "../ldpc_test_common.hpp"
#include "armral.h"
#include "int_utils.hpp"
#include "ldpc_coding.hpp"
#include "ldpc_encoding_test_data.h"
#include "utils/bits_to_bytes.hpp"

#include <cstdlib>
#include <cstring>

namespace {

inline void set_parity_hdsm_bg1_lsi_not_6(
    uint32_t z, const std::vector<uint8_t> &parity_hdsm,
    const std::vector<uint8_t> &agg_parity, std::vector<uint8_t> &codeword) {

  for (uint32_t zb = 0; zb < z - 1; ++zb) {
    codeword[(22 * z) + zb] = agg_parity[zb];
    codeword[(23 * z) + zb] = parity_hdsm[zb] ^ agg_parity[zb + 1];
    codeword[(24 * z) + zb] =
        parity_hdsm[2 * z + zb] ^ parity_hdsm[3 * z + zb] ^ agg_parity[zb + 1];
    codeword[(25 * z) + zb] = parity_hdsm[3 * z + zb] ^ agg_parity[zb + 1];
  }
  // Deal with the final row outside of the loop, as we need to loop around
  {
    codeword[(23 * z) - 1] = agg_parity[z - 1];
    codeword[(24 * z) - 1] = parity_hdsm[z - 1] ^ agg_parity[0];
    codeword[(25 * z) - 1] =
        parity_hdsm[3 * z - 1] ^ parity_hdsm[4 * z - 1] ^ agg_parity[0];
    codeword[(26 * z) - 1] = parity_hdsm[4 * z - 1] ^ agg_parity[0];
  }
}

inline void set_parity_hdsm_bg1_lsi_6(uint32_t z,
                                      const std::vector<uint8_t> &parity_hdsm,
                                      const std::vector<uint8_t> &agg_parity,
                                      std::vector<uint8_t> &codeword) {
  if (z == 208) {
    // Set the parity bits in the high-density sub-matrix
    for (uint32_t zb = 0; zb < 105; ++zb) {
      codeword[(22 * z) + zb] = agg_parity[zb + 103];
      codeword[(23 * z) + zb] = parity_hdsm[zb] ^ agg_parity[zb + 103];
      codeword[(24 * z) + zb] = parity_hdsm[2 * z + zb] ^
                                parity_hdsm[3 * z + zb] ^ agg_parity[zb + 103];
      codeword[(25 * z) + zb] = parity_hdsm[3 * z + zb] ^ agg_parity[zb + 103];
    }
    for (uint32_t zb = 105; zb < z; ++zb) {
      codeword[(22 * z) + zb] = agg_parity[zb - 105];
      codeword[(23 * z) + zb] = parity_hdsm[zb] ^ agg_parity[zb - 105];
      codeword[(24 * z) + zb] = parity_hdsm[2 * z + zb] ^
                                parity_hdsm[3 * z + zb] ^ agg_parity[zb - 105];
      codeword[(25 * z) + zb] = parity_hdsm[3 * z + zb] ^ agg_parity[zb - 105];
    }
  } else { // z in {13, 26, 52, 104}
    {      // Deal with the first row  of the loop (zb =0)
      codeword[22 * z] = agg_parity[z - 1];
      codeword[23 * z] = parity_hdsm[0] ^ agg_parity[z - 1];
      codeword[24 * z] =
          parity_hdsm[2 * z] ^ parity_hdsm[3 * z] ^ agg_parity[z - 1];
      codeword[25 * z] = parity_hdsm[3 * z] ^ agg_parity[z - 1];
    }
    for (uint32_t zb = 1; zb < z; ++zb) {
      codeword[(22 * z) + zb] = agg_parity[zb - 1];
      codeword[(23 * z) + zb] = parity_hdsm[zb] ^ agg_parity[zb - 1];
      codeword[(24 * z) + zb] = parity_hdsm[2 * z + zb] ^
                                parity_hdsm[3 * z + zb] ^ agg_parity[zb - 1];
      codeword[(25 * z) + zb] = parity_hdsm[3 * z + zb] ^ agg_parity[zb - 1];
    }
  }
}

inline void set_parity_hdsm_bg2_lsi_not_3_nor_7(
    uint32_t z, const std::vector<uint8_t> &parity_hdsm,
    const std::vector<uint8_t> &agg_parity, std::vector<uint8_t> &codeword) {
  // Deal with the first row  of the loop (zb =0)
  {
    codeword[10 * z] = agg_parity[z - 1];
    codeword[11 * z] = parity_hdsm[0] ^ agg_parity[z - 1];
    codeword[12 * z] = parity_hdsm[0] ^ parity_hdsm[z] ^ agg_parity[z - 1];
    codeword[13 * z] = parity_hdsm[3 * z] ^ agg_parity[z - 1];
  }
  for (uint32_t zb = 1; zb < z; ++zb) {
    codeword[(10 * z) + zb] = agg_parity[zb - 1];
    codeword[(11 * z) + zb] = parity_hdsm[zb] ^ agg_parity[zb - 1];
    codeword[(12 * z) + zb] =
        parity_hdsm[zb] ^ parity_hdsm[z + zb] ^ agg_parity[zb - 1];
    codeword[(13 * z) + zb] = parity_hdsm[3 * z + zb] ^ agg_parity[zb - 1];
  }
}

inline void set_parity_hdsm_bg2_lsi_3_or_7(
    uint32_t z, const std::vector<uint8_t> &parity_hdsm,
    const std::vector<uint8_t> &agg_parity, std::vector<uint8_t> &codeword) {

  for (uint32_t zb = 0; zb < z - 1; ++zb) {
    codeword[(10 * z) + zb] = agg_parity[zb];
    codeword[(11 * z) + zb] = parity_hdsm[zb] ^ agg_parity[zb + 1];
    codeword[(12 * z) + zb] =
        parity_hdsm[zb] ^ parity_hdsm[z + zb] ^ agg_parity[zb + 1];
    codeword[(13 * z) + zb] = parity_hdsm[3 * z + zb] ^ agg_parity[zb + 1];
  }
  // Deal with the final row outside of the loop, as we need to loop around
  {
    codeword[(11 * z) - 1] = agg_parity[z - 1];
    codeword[(12 * z) - 1] = parity_hdsm[z - 1] ^ agg_parity[0];
    codeword[(13 * z) - 1] =
        parity_hdsm[z - 1] ^ parity_hdsm[2 * z - 1] ^ agg_parity[0];
    codeword[(14 * z) - 1] = parity_hdsm[4 * z - 1] ^ agg_parity[0];
  }
}

// Set parity for base graph 1
inline void set_parity_hdsm_bg1(uint32_t z, uint32_t lsi,
                                const std::vector<uint8_t> &parity_hdsm,
                                const std::vector<uint8_t> &agg_parity,
                                std::vector<uint8_t> &codeword) {
  if (lsi == 6) {
    set_parity_hdsm_bg1_lsi_6(z, parity_hdsm, agg_parity, codeword);
  } else {
    set_parity_hdsm_bg1_lsi_not_6(z, parity_hdsm, agg_parity, codeword);
  }
}

// Set parity for base graph 2
inline void set_parity_hdsm_bg2(uint32_t z, uint32_t lsi,
                                const std::vector<uint8_t> &parity_hdsm,
                                const std::vector<uint8_t> &agg_parity,
                                std::vector<uint8_t> &codeword) {
  if ((lsi == 3) || (lsi == 7)) {
    set_parity_hdsm_bg2_lsi_3_or_7(z, parity_hdsm, agg_parity, codeword);
  } else {
    set_parity_hdsm_bg2_lsi_not_3_nor_7(z, parity_hdsm, agg_parity, codeword);
  }
}

// Setting of the remaining codeword bits (parity bits) is a matter of
// performing the sparse matrix vector multiplication on the remaining
// rows of the matrix and writing into the codeword at the same row
// as we are currently working on
inline void set_remaining_bits(armral_ldpc_graph_t bg, uint32_t z, uint32_t lsi,
                               std::vector<uint8_t> &codeword) {
  const auto *graph = armral_ldpc_get_base_graph(bg);

  auto max_ind = graph->nmessage_bits + 4;
  for (uint32_t i = 4; i < graph->nrows; ++i) {
    auto row_start_ind = graph->row_start_inds[i];
    const auto *col_ptr = graph->col_inds + row_start_ind;
    // Get the number of nonzero entries in the row
    auto col_entries = graph->row_start_inds[i + 1] - row_start_ind;
    // The shifts are stored for all index sets, so the pointer
    // is first offset by the row start index multiplied by
    // the number of index sets (8), and then the lifting set index
    // is added to  this
    const auto *shift_ptr = graph->shifts +
                            row_start_ind * armral::ldpc::num_lifting_sets +
                            lsi * col_entries;
    uint32_t j = 0;
    for (; j < col_entries && col_ptr[j] < max_ind; ++j) {
      // Perform the multiplication for each of the rows in the current block
      auto block_col = col_ptr[j];
      // Shift the first row by the appropriate amount, and then
      // wrap around when we reach the block size
      auto shift = shift_ptr[j] % z;
      auto *out_ptr = codeword.data() + z * (graph->nmessage_bits + i);
      for (uint32_t zb = shift; zb < z; ++zb) {
        *(out_ptr++) ^= codeword[block_col * z + zb];
      }
      for (uint32_t zb = 0; zb < shift; ++zb) {
        *(out_ptr++) ^= codeword[block_col * z + zb];
      }
    }
    // We should have used every column apart from the last one
    assert(j == col_entries - 1);
  }
}

std::vector<uint8_t> armral_ldpc_encode_block_ref(const uint8_t *data_in,
                                                  armral_ldpc_graph_t bg,
                                                  uint32_t z,
                                                  uint32_t len_filler_bits) {

  // Get a pointer to the graph to be working with
  const auto *graph = armral_ldpc_get_base_graph(bg);

  // Cast the bits to bytes for easier handling of data
  auto bytes_in = armral::bits_to_bytes(z * graph->nmessage_bits, data_in);

  // Get the lifting set index
  auto lsi = get_ldpc_lifting_index(z);

  // Perform the encoding
  // We need to invert a system of equations for the
  // first 4 * z rows, which correspond to the high-density
  // sub-matrix portion. Initialization is to zero
  std::vector<uint8_t> parity_hdsm(4 * z);

  // Rename a variable for clarity how it is used in this function
  uint32_t max_message_ind = graph->nmessage_bits;

  // First get the parity of the message bits for the high-density sub-matrix
  for (uint32_t i = 0; i < 4; ++i) {
    auto row_start_ind = graph->row_start_inds[i];
    const auto *col_ptr = graph->col_inds + row_start_ind;
    // Get the number of nonzero entries in the row
    auto col_entries = graph->row_start_inds[i + 1] - row_start_ind;
    // The shifts are stored for all index sets, so the pointer
    // is first offset by the row start index multiplied by
    // the number of index sets (8), and then
    const auto *shift_ptr = graph->shifts +
                            row_start_ind * armral::ldpc::num_lifting_sets +
                            lsi * col_entries;
    uint32_t j = 0;
    for (; j < col_entries && col_ptr[j] < max_message_ind; ++j) {
      // Perform the multiplication for each of the rows in the current block
      auto block_col = col_ptr[j];
      // Shift the first row by the appropriate amount, and then
      // wrap around when we reach the block size
      auto shift = shift_ptr[j] % z;
      auto *out_ptr = parity_hdsm.data() + z * i;
      for (uint32_t zb = shift; zb < z; ++zb) {
        *(out_ptr++) ^= bytes_in[block_col * z + zb];
      }
      for (uint32_t zb = 0; zb < shift; ++zb) {
        *(out_ptr++) ^= bytes_in[block_col * z + zb];
      }
    }
    assert(j < col_entries && col_ptr[j] >= max_message_ind);
  }

  // Now we set the parity bits in the out message to what they are supposed to
  // be. TODO: Actually puncture the first two columns. For now we don't, and
  // store all of the columns in the codeword
  std::vector<uint8_t> codeword((graph->ncodeword_bits + 2) * z);

  for (uint32_t j = 0; j < graph->nmessage_bits; ++j) {
    for (uint32_t zb = 0; zb < z; ++zb) {
      codeword[j * z + zb] = bytes_in[j * z + zb];
    }
  }

  // To set the parity bits for the codeword bits for the parity in the
  // high-density submatrix, we require another temporary store
  std::vector<uint8_t> tmp_parity(z);

  for (uint32_t zb = 0; zb < z; ++zb) {
    tmp_parity[zb] = 0;
    for (uint32_t j = 0; j < 4; ++j) {
      tmp_parity[zb] ^= parity_hdsm[j * z + zb];
    }
  }

  // Set the parity bits in the high-density sub-matrix
  switch (bg) {
  case LDPC_BASE_GRAPH_1:
    set_parity_hdsm_bg1(z, lsi, parity_hdsm, tmp_parity, codeword);
    break;
  case LDPC_BASE_GRAPH_2:
    set_parity_hdsm_bg2(z, lsi, parity_hdsm, tmp_parity, codeword);
    break;
  }

  // Setting of the remaining codeword bits (parity bits)
  set_remaining_bits(bg, z, lsi, codeword);

  return codeword;
}

bool check_bytes_equal(const std::vector<uint8_t> &enc,
                       const std::vector<uint8_t> &expected,
                       const ldpc_test_param_t &tc) {
  if (enc.size() != expected.size()) {
    // GCOVR_EXCL_START
    std::cout << "LDPC: z=" << tc.lifting_size << ", length=" << tc.length
              << ", base_graph=" << (int)tc.graph_type + 1
              << ". Length of reference and calculated encoding are not equal"
              << std::endl;
    return false;
    // GCOVR_EXCL_STOP
  }

  for (uint32_t i = 0; i < enc.size(); ++i) {
    if (enc[i] != expected[i]) {
      // GCOVR_EXCL_START
      std::cout << "LDPC: z=" << tc.lifting_size << ", length=" << tc.length
                << ", filler bits length=" << tc.length_of_filler_bits
                << ", base_graph=" << (int)tc.graph_type + 1
                << ". Bit at position " << i << " is " << (int)enc[i]
                << ", but expected " << (int)expected[i] << std::endl;
      return false;
      // GCOVR_EXCL_STOP
    }
  }
  return true;
}

// This testing routine compares the computed result
// with the solution from the reference implementation
template<typename LDPCEncodeBlockFunction>
bool test_ldpc_encode_block(
    char const *name, LDPCEncodeBlockFunction ldpc_encode_block_under_test) {

  armral::utils::bit_random random;
  bool passed = true;
  // Check that the implementation matches expected results
  for (const auto &tc : ldpc_tests) {
    // Generate some random data to encode. This should be in a single
    // block
    auto data_in = random.bit_vector(tc.length - tc.length_of_filler_bits);

    // Zero input vector's end portion of length length_of_filler_bits
    data_in.resize((tc.length + 7) / 8, 0);

    const auto *bg = armral_ldpc_get_base_graph(tc.graph_type);

    std::vector<uint8_t> encoding((tc.lifting_size * bg->ncodeword_bits + 7) /
                                  8);

    ldpc_encode_block_under_test(data_in.data(), tc.graph_type, tc.lifting_size,
                                 tc.length_of_filler_bits, encoding.data());

    auto encoding_bytes = armral::bits_to_bytes(
        tc.lifting_size * bg->ncodeword_bits, encoding.data());

    // Compute full block data by reference implementation
    // and check its validity
    auto encoding_ref =
        armral_ldpc_encode_block_ref(data_in.data(), tc.graph_type,
                                     tc.lifting_size, tc.length_of_filler_bits);

    // Check the validity of the reference implementation: H*c = 0
    // Can be removed once the code is stable
    passed &= perform_parity_check(encoding_ref.data(), tc.lifting_size,
                                   tc.graph_type);
    assert(passed && "LDPC encoding: Invalid reference implementation");

    // Puncture solution from reference and compared with computed
    const std::vector<uint8_t> punctured = {
        encoding_ref.begin() + 2 * tc.lifting_size, encoding_ref.end()};
    passed &= check_bytes_equal(encoding_bytes, punctured, tc);
  }
  if (!passed) {
    // GCOVR_EXCL_START
    printf("[%s] one or more tests failed!\n", name);
    // GCOVR_EXCL_STOP
  }
  return passed;
}

} // anonymous namespace

int main(int argc, char **argv) {
  bool passed = true;

  passed &= test_ldpc_encode_block("LDPCEncodeBlock", armral_ldpc_encode_block);

  passed &= test_ldpc_encode_block(
      "LDPCEncodeBlockNoAlloc",
      [](uint8_t const *data_in, armral_ldpc_graph_t bg, uint32_t z,
         uint32_t len_filler_bits, uint8_t *data_out) {
        auto buffer_size = armral_ldpc_encode_block_noalloc_buffer_size(
            bg, z, len_filler_bits);
        std::vector<uint8_t> buffer(buffer_size);
        return armral_ldpc_encode_block_noalloc(data_in, bg, z, len_filler_bits,
                                                data_out, buffer.data());
      });

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
