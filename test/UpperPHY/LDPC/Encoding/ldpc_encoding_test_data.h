/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"

#include <vector>

typedef enum { LDPC_DATA_ALTERNATING_EVEN, LDPC_DATA_RANDOM } ldpc_test_data_t;

struct ldpc_test_param_t {
  uint32_t lifting_size;
  uint32_t length;
  armral_ldpc_graph_t graph_type;
  uint32_t length_of_filler_bits;
};

std::vector<ldpc_test_param_t> ldpc_tests = {{
                                                 // index set 0, base graph 1
                                                 2,
                                                 2 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 0,
                                             },
                                             {
                                                 // index set 1, base graph 1
                                                 3,
                                                 3 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 0,
                                             },
                                             {
                                                 // index set 2, base graph 1
                                                 5,
                                                 5 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 0,
                                             },
                                             {
                                                 // index set 3, base graph 1
                                                 7,
                                                 7 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 0,
                                             },
                                             {
                                                 // index set 4, base graph 1
                                                 9,
                                                 9 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 0,
                                             },
                                             {
                                                 // index set 5, base graph 1
                                                 11,
                                                 11 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 0,
                                             },
                                             {
                                                 // index set 6, base graph 1
                                                 13,
                                                 13 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 0,
                                             },
                                             {
                                                 // index set 7, base graph 1
                                                 208,
                                                 208 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 0,
                                             },
                                             {
                                                 // index set 8, base graph 1
                                                 15,
                                                 15 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 0,
                                             },
                                             {
                                                 // index set 9, base graph 1
                                                 18,
                                                 18 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 76,
                                             },
                                             {
                                                 // index set 10, base graph 1
                                                 112,
                                                 112 * 22,
                                                 LDPC_BASE_GRAPH_1,
                                                 72,
                                             },
                                             {
                                                 // index set 0, base graph 2
                                                 2,
                                                 2 * 10,
                                                 LDPC_BASE_GRAPH_2,
                                                 0,
                                             },
                                             {
                                                 // index set 1, base graph 2
                                                 3,
                                                 3 * 10,
                                                 LDPC_BASE_GRAPH_2,
                                                 0,
                                             },
                                             {
                                                 // index set 2, base graph 2
                                                 5,
                                                 5 * 10,
                                                 LDPC_BASE_GRAPH_2,
                                                 0,
                                             },
                                             {
                                                 // index set 3, base graph 2
                                                 7,
                                                 7 * 10,
                                                 LDPC_BASE_GRAPH_2,
                                                 0,
                                             },
                                             {
                                                 // index set 4, base graph 2
                                                 9,
                                                 9 * 10,
                                                 LDPC_BASE_GRAPH_2,
                                                 0,
                                             },
                                             {
                                                 // index set 5, base graph 2
                                                 11,
                                                 11 * 10,
                                                 LDPC_BASE_GRAPH_2,
                                                 0,
                                             },
                                             {
                                                 // index set 6, base graph 2
                                                 13,
                                                 13 * 10,
                                                 LDPC_BASE_GRAPH_2,
                                                 0,
                                             },
                                             {
                                                 // index set 7, base graph 2
                                                 15,
                                                 15 * 10,
                                                 LDPC_BASE_GRAPH_2,
                                                 0,
                                             },
                                             {
                                                 // index set 8, base graph 2
                                                 18,
                                                 18 * 10,
                                                 LDPC_BASE_GRAPH_2,
                                                 36,
                                             },
                                             {
                                                 // index set 9, base graph 2
                                                 112,
                                                 112 * 10,
                                                 LDPC_BASE_GRAPH_2,
                                                 76,
                                             }};
