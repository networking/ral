/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "cs16_utils.hpp"
#include "int_utils.hpp"

#include <cassert>
#include <cstdio>
#include <vector>

using mapping_func_t = armral_cmplx_int16_t(const std::vector<uint16_t> &,
                                            uint8_t);

static std::vector<armral_cmplx_int16_t>
gen_qam_constellation(mapping_func_t &map_func,
                      const std::vector<uint16_t> &values,
                      uint16_t num_points) {
  assert(num_points <= 256);
  std::vector<armral_cmplx_int16_t> vec_out(num_points);
  for (uint16_t i = 0; i < num_points; ++i) {
    vec_out[i] = map_func(values, (uint8_t)i);
  }
  return vec_out;
}

static armral_cmplx_int16_t mapping_qpsk(const std::vector<uint16_t> &values,
                                         uint8_t ind) {
  int16_t sign_real = 1 - 2 * (ind >> 1);
  int16_t sign_im = 1 - 2 * (ind & 0x1);
  return {(int16_t)(sign_real * values[0]), (int16_t)(sign_im * values[0])};
}

static void modulation_ref_qpsk(const uint32_t nbits, const int8_t *p_src,
                                armral_cmplx_int16_t *p_dst) {
  // We assume that pSrc is a byte stream, where the most significant bits of
  // the byte are in the lowest positions
  // Output is in Q2.13 format

  // Get the constellation points in Q2.13 format
  constexpr uint16_t sqrt_2 = 0x16A1;
  auto constellation = gen_qam_constellation(mapping_qpsk, {sqrt_2}, 4);

  // How many bytes do we have in the input?
  uint32_t num_bytes = nbits >> 3;
  uint16_t tail_bits = nbits & 0x7;
  // Each signal is split into in-phase and quadrature bits (alternating), so
  // there must be an even number of bits
  assert(tail_bits % 2 == 0);

  // Loop over full bytes
  for (uint32_t i = 0; i < num_bytes; ++i) {
    // We get pairs of bits from the current byte, starting from the most
    // significant bits
    uint8_t curr_byte = p_src[i];
    for (uint32_t j = 0; j < 4; ++j) {
      uint8_t bits = (curr_byte >> (6 - 2 * j)) & 0x3;
      p_dst[i * 4 + j] = constellation[bits];
    }
  }

  // We deal with a tail, which has fewer than four pairs of bits to consider
  uint8_t last_byte = tail_bits != 0U ? p_src[num_bytes] : 0;
  for (uint32_t i = 0; i < tail_bits / 2; ++i) {
    uint8_t bits = (last_byte >> (6 - 2 * i)) & 0x3;
    p_dst[num_bytes * 4 + i] = constellation[bits];
  }
}

static armral_cmplx_int16_t mapping_16qam(const std::vector<uint16_t> &values,
                                          uint8_t ind) {
  int16_t sign_real = 1 - 2 * (ind >> 3);
  int16_t sign_im = 1 - 2 * ((ind >> 2) & 0x1);
  auto ind_func = [](uint8_t bit) { return (1 - (1 - 2 * bit)) / 2; };
  return {(int16_t)(sign_real * values[ind_func((ind >> 1) & 0x1)]),
          (int16_t)(sign_im * values[ind_func((ind & 0x1))])};
}

static void modulation_ref_16qam(const uint32_t nbits, const int8_t *p_src,
                                 armral_cmplx_int16_t *p_dst) {
  // Magic values for the constellation are taken from armral_modulation.c
  // These are sqrt(10)/10 and 3 * sqrt(10)/10 in Q2.13 format
  std::vector<uint16_t> constellation_values = {0xA1F, 0x1E5C};
  auto constellation =
      gen_qam_constellation(mapping_16qam, constellation_values, 16);

  // Each symbol is four bits long, so the input must be a multiple of four bits
  assert(nbits % 4 == 0);
  // How many bytes do we have in the input?
  uint32_t num_bytes = nbits >> 3;
  uint16_t tail_bits = nbits & 0x4;

  // The signal is split up into groups of four bits as follows:
  // - leftmost two bits identify the quadrant
  // - next bit is the magnitude of the real component
  // - rightmost bit is the magnitude of the complex component
  // The constellation has been set up to return the correct complex number
  // given the four-bit number
  // Loop over full bytes
  for (uint32_t i = 0; i < num_bytes; ++i) {
    uint8_t curr_byte = p_src[i];
    // Start with the left-most four bits
    uint8_t ind = curr_byte >> 4;
    p_dst[i * 2] = constellation[ind];
    ind = curr_byte & 0xF;
    p_dst[i * 2 + 1] = constellation[ind];
  }

  // If there is a half-full tail, deal with that
  if (tail_bits != 0U) {
    uint8_t curr_byte = p_src[num_bytes];
    uint8_t ind = curr_byte >> 4;
    p_dst[num_bytes * 2] = constellation[ind];
  }
}

static armral_cmplx_int16_t mapping_64qam(const std::vector<uint16_t> &values,
                                          uint8_t ind) {
  // The mapping can be found in the latest document at
  // https://www.3gpp.org/ftp/Specs/archive/38_series/38.211/

  // Get the sign bits
  int16_t sign_real = 1 - 2 * (ind >> 5);
  int16_t sign_im = 1 - 2 * ((ind >> 4) & 0x1);
  auto ind_func = [](uint8_t left_bit, uint8_t right_bit) {
    return (3 - (1 - 2 * left_bit) * (2 - (1 - 2 * right_bit))) / 2;
  };
  // Get the index into the vector of values. The real index is calculated from
  // the combination of the 3rd and 5th bits from the right
  auto real_ind = ind_func((ind >> 3) & 0x1, (ind >> 1) & 0x1);
  // The imaginary index is calculated from the combination of the 4th and 6th
  // bits from the right
  auto imag_ind = ind_func((ind >> 2) & 0x1, ind & 0x1);

  return {(int16_t)(sign_real * values[real_ind]),
          (int16_t)(sign_im * values[imag_ind])};
}

static void modulation_ref_64qam(const uint32_t nbits, const int8_t *p_src,
                                 armral_cmplx_int16_t *p_dst) {
  // These are the Q2.13 representation of 1, 3, 5 and 7 times (1/sqrt(42)), as
  // given in latest document at
  // https://www.3gpp.org/ftp/Specs/archive/38_series/38.211/
  std::vector<uint16_t> constellation_values = {0x4F0, 0xED0, 0x18B0, 0x2290};
  auto constellation =
      gen_qam_constellation(mapping_64qam, constellation_values, 64);

  // Each symbol contains 6 bits so the input must be a multiple of 6 bits
  assert(nbits % 6 == 0);
  // Try and load 4 symbols at a time, which are stored in 24 bytes
  auto num_group_symbols = nbits / 24;
  auto num_tail_symbols = (nbits % 24) / 6;

  // Loop over groups of four symbols
  for (uint32_t i = 0; i < num_group_symbols; ++i) {
    // Each group consists of three bytes
    const uint8_t *bytes = (const uint8_t *)(p_src + i * 3);

    uint16_t symbols[4];
    symbols[0] = bytes[0] >> 2;
    symbols[1] = ((bytes[0] & 0x3) << 4) | (bytes[1] >> 4);
    symbols[2] = ((bytes[1] & 0xF) << 2) | (bytes[2] >> 6);
    symbols[3] = bytes[2] & 0x3F;

    auto base_ind = i * 4;
    p_dst[base_ind] = constellation[symbols[0]];
    p_dst[base_ind + 1] = constellation[symbols[1]];
    p_dst[base_ind + 2] = constellation[symbols[2]];
    p_dst[base_ind + 3] = constellation[symbols[3]];
  }

  if (num_tail_symbols != 0U) {
    const uint8_t *bytes = (const uint8_t *)(p_src + num_group_symbols * 3);

    uint64_t symbols[3];
    symbols[0] = bytes[0] >> 2;
    auto base_ind = num_group_symbols * 4;
    p_dst[base_ind] = constellation[symbols[0]];
    if (num_tail_symbols > 1) {
      symbols[1] = ((bytes[0] & 0x3) << 4) | (bytes[1] >> 4);
      p_dst[base_ind + 1] = constellation[symbols[1]];
      if (num_tail_symbols > 2) {
        symbols[2] = ((bytes[1] & 0xF) << 2) | (bytes[2] >> 6);
        p_dst[base_ind + 2] = constellation[symbols[2]];
      }
    }
  }
}

static armral_cmplx_int16_t mapping_256qam(const std::vector<uint16_t> &values,
                                           uint8_t ind) {
  // The mapping can be found in the latest document at
  // https://www.3gpp.org/ftp/Specs/archive/38_series/38.211/

  // Get the sign bits
  int16_t sign_real = 1 - 2 * (ind >> 7);
  int16_t sign_im = 1 - 2 * ((ind >> 6) & 0x1);
  auto ind_func = [](uint8_t bit_l, uint8_t bit_m, uint8_t bit_r) {
    return (7 -
            (1 - 2 * bit_l) * (4 - (1 - 2 * bit_m) * (2 - (1 - 2 * bit_r)))) /
           2;
  };
  // Get the index into the vector of values. The real index is calculated from
  // the combination of the 3rd and 5th bits from the right
  auto real_ind =
      ind_func((ind >> 5) & 0x1, (ind >> 3) & 0x1, (ind >> 1) & 0x1);
  // The imaginary index is calculated from the combination of the 4th and 6th
  // bits from the right
  auto imag_ind = ind_func((ind >> 4) & 0x1, (ind >> 2) & 0x1, ind & 0x1);

  return {(int16_t)(sign_real * values[real_ind]),
          (int16_t)(sign_im * values[imag_ind])};
}

static void modulation_ref_256qam(const uint32_t nbits, const int8_t *p_src,
                                  armral_cmplx_int16_t *p_dst) {
  // Each symbol is a byte (8-bits) long. Make sure that the number of bits is a
  // multiple of 8
  assert(nbits % 8 == 0);

  // These are the Q2.13 representation of 1, 3, 5, 7, 9, 11, 13 and 15 times
  // (1/sqrt(170)), as given in the latest document at
  // https://www.3gpp.org/ftp/Specs/archive/38_series/38.211/
  std::vector<uint16_t> values = {0x274,  0x75D,  0xC45,  0x112E,
                                  0x1617, 0x1AFF, 0x1FE8, 0x24D0};
  auto constellation = gen_qam_constellation(mapping_256qam, values, 256);

  auto num_symbols = nbits / 8;
  for (uint32_t i = 0; i < num_symbols; ++i) {
    p_dst[i] = constellation[(uint8_t)p_src[i]];
  }
}

static bool check_results(uint32_t out_size,
                          const armral_cmplx_int16_t *computed_out,
                          const armral_cmplx_int16_t *expected_out) {
  bool passed = true;

  for (uint32_t i = 0; i < out_size; i++) {
    if ((computed_out[i].re != expected_out[i].re) ||
        (computed_out[i].im != expected_out[i].im)) {
      // GCOVR_EXCL_START
      printf("[%u]Out.re = %d - Exp.re = %d || Out.im = %d - Exp.im = %d || ",
             i, computed_out[i].re, expected_out[i].re, computed_out[i].im,
             expected_out[i].im);
      printf("Result: Error\n");
      passed = false;
      // GCOVR_EXCL_STOP
    }
  }

  printf("Check %s!\n", passed ? "successful" : "failed");

  return passed;
}

bool test_qpsk(void) {
  printf("QPSK Unit test: ");

  bool passed = true;

  // Parameters are pairs of number of bytes to allocate, and number of bits in
  // those bytes to use as data. The bits come in pairs of quadrature (real) and
  // input (imaginary), so we need an even number of bits
  std::vector<int32_t> bits_in = {
      2,  4, 6, 8, 10,
      26, // Half full last byte (two Q, I pairs in last byte)
      32, // Full bytes
      80, // Full bytes
      74, // Last byte has one Q, I pair
      76, // Half full last byte
      798 // Last byte has three Q, I pairs
  };

  for (auto num_bits : bits_in) {
    auto num_bytes = (num_bits + 7) / 8;
    // One complex number is generated per Q, I pair of bits (i.e. symbol) in
    // the input
    auto num_symbols = num_bits / 2;
    auto res =
        armral::utils::allocate_random_cs16(num_symbols, INT16_MIN, INT16_MAX);
    std::vector<armral_cmplx_int16_t> res_exp(res);
    auto input = armral::utils::allocate_random_i8(num_bytes);

    modulation_ref_qpsk(num_bits, input.data(), res_exp.data());
    armral_modulation(num_bits, ARMRAL_MOD_QPSK, (uint8_t *)input.data(),
                      res.data());
    passed &= check_results(num_symbols, res.data(), res_exp.data());
  }

  return passed;
}

bool test_16_qam(void) {
  printf("16QAM Unit test: ");
  bool passed = true;

  std::vector<int32_t> bits_in = {4, // Half full last byte
                                  8, // Full last byte
                                  12, 16, 80, 76, 32, 28, 800, 796};

  for (auto num_bits : bits_in) {
    auto num_bytes = (num_bits + 7) / 8;
    auto num_symbols = (num_bits / 4);
    auto res = armral::utils::allocate_random_cs16(num_symbols);
    std::vector<armral_cmplx_int16_t> res_exp(res);
    auto input = armral::utils::allocate_random_i8(num_bytes);
    modulation_ref_16qam(num_bits, input.data(), res_exp.data());
    armral_modulation(num_bits, ARMRAL_MOD_16QAM, (uint8_t *)input.data(),
                      res.data());
    passed &= check_results(num_symbols, res.data(), res_exp.data());
  }

  return passed;
}

bool test_64_qam(void) {
  printf("64QAM Unit test: ");
  bool passed = true;

  std::vector<int32_t> bits_in = {
      6,  12, 18, 24,
      78, // last byte with 6 bits filled
      84, // last byte with four bits filled
      96, // full bytes
      90, // last byte with two bits filled
      798 // Larger input. Last byte with four bits filled
  };

  for (auto num_bits : bits_in) {
    auto num_bytes = (num_bits + 7) / 8;
    auto num_symbols = num_bits / 6;
    auto res = armral::utils::allocate_random_cs16(num_symbols);
    std::vector<armral_cmplx_int16_t> res_exp(res);
    auto input = armral::utils::allocate_random_i8(num_bytes);
    modulation_ref_64qam(num_bits, input.data(), res_exp.data());
    armral_modulation(num_bits, ARMRAL_MOD_64QAM, (uint8_t *)input.data(),
                      res.data());
    passed &= check_results(num_symbols, res.data(), res_exp.data());
  }

  return passed;
}

bool test_256_qam(void) {
  printf("256QAM Unit test: ");
  bool passed = true;

  // We parameterise using the number of bytes in a message
  std::vector<int32_t> bits_in = {8,   16,  24,  80,   160, 240,
                                  320, 400, 800, 1600, 3200};

  for (auto num_bits : bits_in) {
    auto num_bytes = num_bits / 8;
    auto res = armral::utils::allocate_random_cs16(num_bytes);
    std::vector<armral_cmplx_int16_t> res_exp(res);
    auto input = armral::utils::allocate_random_i8(num_bytes);
    modulation_ref_256qam(num_bits, input.data(), res_exp.data());
    armral_modulation(num_bits, ARMRAL_MOD_256QAM, (uint8_t *)input.data(),
                      res.data());
    passed &= check_results(num_bytes, res.data(), res_exp.data());
  }

  return passed;
}

bool test_invalid_lengths() {
  using length_test = std::pair<uint32_t, armral_modulation_type>;
  printf("Testing of invalid lengths for modulation\n");
  bool passed = true;
  std::vector<length_test> test_data = {{1, ARMRAL_MOD_QPSK},
                                        {2, ARMRAL_MOD_16QAM},
                                        {4, ARMRAL_MOD_64QAM},
                                        {7, ARMRAL_MOD_256QAM}};
  for (auto [num_bits, mod_type] : test_data) {
    auto retval = armral_modulation(num_bits, mod_type, nullptr, nullptr);
    if (retval == ARMRAL_ARGUMENT_ERROR) {
      printf("Check successful!\n");
    } else {
      // GCOVR_EXCL_START
      printf("Check failed!\n");
      passed = false;
      // GCOVR_EXCL_STOP
    }
  }
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;
  passed &= test_qpsk();
  passed &= test_16_qam();
  passed &= test_64_qam();
  passed &= test_256_qam();
  passed &= test_invalid_lengths();
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
