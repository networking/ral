/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "cs16_utils.hpp"
#include "int_utils.hpp"

#include <cassert>
#include <cstdlib>
#include <vector>

// Function to check results of armral_demodulation
static bool check_llrs_equal(const armral_cmplx_int16_t *p_src,
                             const int8_t *result, const int8_t *expected,
                             const uint32_t n_values,
                             const uint32_t bits_per_symbol) {
  bool passed = true;
  for (uint32_t i = 0; i < n_values; ++i) {
    int symbol_ind = i / bits_per_symbol;
    if (result[i] != expected[i]) {
      // GCOVR_EXCL_START
      printf("Sample %u, Symbol (%d, %d): LLR calculated = %d != LLR expected "
             "= %d --> ERROR \n",
             i, p_src[symbol_ind].re, p_src[symbol_ind].im, result[i],
             expected[i]);
      passed = false;
      // GCOVR_EXCL_STOP
    }
  }

  printf("Check %s!\n", passed ? "successful" : "failed");

  return passed;
}

static int8_t offset_to_llr(int16_t offset, uint16_t weight) {
  // Convert an int16_t offset into an int8_t llr by scaling by
  // (2 * weight) = (2 * (1 << 15 / ulp)), and extracting the the second most
  // significant byte with rounding and saturation.
  // This precise operation is chosen to facilitate use of the SQRDMULH
  // instruction.
  int32_t value = (2 * (int32_t)weight) * (int32_t)offset;
  value = (value + 0x8000) >> 16;
  if (value > INT8_MAX) {
    return INT8_MAX;
  }
  if (value < INT8_MIN) {
    return INT8_MIN;
  }
  return value;
}

using demod_ref_func_t = void (*)(const uint32_t, const uint16_t,
                                  const armral_cmplx_int16_t *, int8_t *);

static void demodulation_qpsk_ref(const uint32_t n_symbols, const uint16_t ulp,
                                  const armral_cmplx_int16_t *p_src,
                                  int8_t *p_dst) {
  // The log likelihood ratio of a bit being received as 1 is directly
  // proportional to the modulated symbol received
  uint16_t weight = (1 << 15) / ulp;

  // Go through each of the complex symbols received in pSrc, and multiply these
  // and narrow to 8 bits. Each complex number in pSrc is stored as a pair of
  // Q2.13 16 bit integers
  int dst_ind = 0;
  for (uint32_t i = 0; i < n_symbols; ++i) {
    p_dst[dst_ind++] = offset_to_llr(p_src[i].re, weight);
    p_dst[dst_ind++] = offset_to_llr(p_src[i].im, weight);
  }
}

static void demodulation_16qam_ref(const uint32_t n_symbols, const uint16_t ulp,
                                   const armral_cmplx_int16_t *p_src,
                                   int8_t *p_dst) {
  // The log likelihood ratio of a bit being received as 1 is approximately
  // proportional to an offset of the modulated symbol received
  uint16_t weight = (1 << 15) / ulp;
  // The absolute amplitudes are {1, 3} / sqrt(10). To figure out which
  // amplitude we are using, we take off 2 / sqrt(10) from the received
  // absolute amplitude, and check if this is positive or negative
  constexpr uint16_t double_root_10 = 0x143D; // 2 / sqrt(10) in Q2.13

  int dst_ind = 0;
  for (uint32_t i = 0; i < n_symbols; ++i) {
    // Get sign of the Q and I phases. The log likelihood ratio will be less
    // than one if the bit is zero (i.e. sign is positive)
    p_dst[dst_ind++] = offset_to_llr(p_src[i].re, weight);
    p_dst[dst_ind++] = offset_to_llr(p_src[i].im, weight);

    // Get the log likelihood ratio of the magnitude being 1 / sqrt(10), or 3 /
    // sqrt(10). Log likelihood is negative in the case of 1 / sqrt(10)
    p_dst[dst_ind++] =
        offset_to_llr((double_root_10 - abs(p_src[i].re)), weight);
    p_dst[dst_ind++] =
        offset_to_llr((double_root_10 - abs(p_src[i].im)), weight);
  }
}

static void demodulation_64qam_ref(const uint32_t n_symbols, const uint16_t ulp,
                                   const armral_cmplx_int16_t *p_src,
                                   int8_t *p_dst) {
  // The log likelihood ratio of a bit being received as 1 is approximately
  // proportional to an offset of the modulated symbol received
  uint16_t weight = (1 << 15) / ulp;
  // The amplitudes are in {1, 3, 5, 7} / sqrt(42)
  // These are organized using a Gray encoding, in the following manner
  // 01 -> 1/sqrt(42)
  // 00 -> 3/sqrt(42)
  // 10 -> 5/sqrt(42)
  // 11 -> 7/sqrt(42)
  // Given the symbol magnitude we can get the value of the first bit by
  // checking the sign of s \in {sym.re, sym.im} |s| - 4 / sqrt(42) and the
  // second bit we can check the sign of
  // ||s| - 4 / sqrt(42)| - 2/sqrt(42)
  // All arithmetic is done in Q2.13
  constexpr uint32_t thresholds[2] = {0x9e0 /* 2 /sqrt(42) */,
                                      0x13c0 /* 4/sqrt(42) */};

  int dst_ind = 0;
  for (uint32_t i = 0; i < n_symbols; ++i) {
    // The sign of the real component is encoded in the first byte of the output
    // LLR The sign of the imaginary component is encoded in the second byte of
    // the LLR The magnitude of the real component is encoded in the third and
    // fifth bytes The magnitude of the imaginary component is encoded in the
    // fourth and sixth bytes
    p_dst[dst_ind++] = offset_to_llr(p_src[i].re, weight);
    p_dst[dst_ind++] = offset_to_llr(p_src[i].im, weight);
    int32_t tmp1 = thresholds[1] - abs(p_src[i].re);
    int32_t tmp2 = thresholds[1] - abs(p_src[i].im);
    p_dst[dst_ind++] = offset_to_llr(tmp1, weight);
    p_dst[dst_ind++] = offset_to_llr(tmp2, weight);
    p_dst[dst_ind++] = offset_to_llr((thresholds[0] - abs(tmp1)), weight);
    p_dst[dst_ind++] = offset_to_llr((thresholds[0] - abs(tmp2)), weight);
  }
}

static void demodulation_256qam_ref(const uint32_t n_symbols,
                                    const uint16_t ulp,
                                    const armral_cmplx_int16_t *p_src,
                                    int8_t *p_dst) {
  // The log likelihood ratio of a bit being received as 1 is approximately
  // proportional to an offset of the modulated symbol received
  uint16_t weight = (1 << 15) / ulp;

  // The amplitudes are in {1, 3, 5, 7, 9, 11, 13, 15} / sqrt(170)
  // These are organized in a Gray encoding, and we can get the
  // log-likelihood ratios by performing the following operations
  // for each of the 8 bits encoded in the symbol s = {s.re, s.im}
  // LLR(b0|s) = weight * -s.re
  // LLR(b1|s) = weight * -s.im
  // LLR(b2|s) = weight * (|s.re| - 8/sqrt(170))
  // LLR(b3|s) = weight * (|s.im| - 8/sqrt(170))
  // LLR(b4|s) = weight * (||s.re| - 8/sqrt(170)| - 4/sqrt(170))
  // LLR(b5|s) = weight * (||s.im| - 8/sqrt(170)| - 4/sqrt(170))
  // LLR(b6|s) = weight * (|||s.re| - 8/sqrt(170)| - 4/sqrt(170)| - 2/sqrt(170))
  // LLR(b7|s) = weight * (|||s.im| - 8/sqrt(170)| - 4/sqrt(170)| - 2/sqrt(170))
  constexpr uint32_t thresholds[3] = {0x4e9 /* 2/sqrt(170) */,
                                      0x9d1 /* 4/sqrt(170) */,
                                      0x13a2 /* 8/sqrt(170) */};
  int dst_ind = 0;
  for (uint32_t i = 0; i < n_symbols; ++i) {
    p_dst[dst_ind++] = offset_to_llr(p_src[i].re, weight);
    p_dst[dst_ind++] = offset_to_llr(p_src[i].im, weight);
    int32_t tmp1 = thresholds[2] - abs(p_src[i].re);
    int32_t tmp2 = thresholds[2] - abs(p_src[i].im);
    p_dst[dst_ind++] = offset_to_llr(tmp1, weight);
    p_dst[dst_ind++] = offset_to_llr(tmp2, weight);
    tmp1 = thresholds[1] - abs(tmp1);
    tmp2 = thresholds[1] - abs(tmp2);
    p_dst[dst_ind++] = offset_to_llr(tmp1, weight);
    p_dst[dst_ind++] = offset_to_llr(tmp2, weight);
    p_dst[dst_ind++] = offset_to_llr(thresholds[0] - (abs(tmp1)), weight);
    p_dst[dst_ind++] = offset_to_llr(thresholds[0] - (abs(tmp2)), weight);
  }
}

// Helper class to store information about the parameters used when testing
// demodulation
struct demod_test_params {
  /// The type of modulation to perform
  armral_modulation_type mod_type;
  /// The number of bits per symbol for the modulation
  unsigned bits_per_symbol;
  /// The name of the modulation. Used for printing out useful information to
  /// the user
  std::string name;
  /// The llr ulps to use in the tests
  std::vector<uint16_t> llr_ulps;
  /// The number of symbols to use in the tests
  std::vector<uint32_t> num_symbols;
  /// Function pointer to a reference implementation
  demod_ref_func_t ref_func;

  /// Default constructor
  demod_test_params() = default;

  /// Constructor from a set of parameters
  demod_test_params(armral_modulation_type m, unsigned bps, std::string n,
                    std::vector<uint16_t> ulps, std::vector<uint32_t> ns,
                    const demod_ref_func_t &f)
    : mod_type(m), bits_per_symbol(bps), name(std::move(n)),
      llr_ulps(std::move(ulps)), num_symbols(std::move(ns)), ref_func(f) {}
};

static demod_test_params get_test_params(armral_modulation_type mod_type) {
  switch (mod_type) {
  case ARMRAL_MOD_QPSK:
    return demod_test_params(mod_type, 2, "QPSK",
                             {2, 27, 2936},              // llr ulp in Q2.13
                             {4, 9, 15, 276, 512, 1024}, // Number of symbols
                             demodulation_qpsk_ref);
  case ARMRAL_MOD_16QAM:
    return demod_test_params(mod_type, 4, "16QAM",
                             {2, 37, 4915},              // llr ulp in Q2.13
                             {4, 9, 15, 276, 512, 1024}, // Number of symbols
                             demodulation_16qam_ref);
  case ARMRAL_MOD_64QAM:
    return demod_test_params(
        mod_type, 6, "64QAM", {2, 66, 1045}, // llr ulp in Q2.13
        {9, 48, 15, 99, 276, 512, 1024},     // Number of symbols
        demodulation_64qam_ref);
  case ARMRAL_MOD_256QAM:
    return demod_test_params(mod_type, 8, "256QAM",
                             {2, 19, 523},                 // llr ulp in Q2.13
                             {9, 15, 256, 276, 512, 1024}, // Number of symbols
                             demodulation_256qam_ref);
  }
  assert(false); // GCOVR_EXCL_LINE
  return {};
}

static bool test_demod(armral_modulation_type mod) {
  auto test_params = get_test_params(mod);
  bool passed = true;

  printf("\n***** Test demodulation %s *****\n", test_params.name.c_str());

  for (auto n : test_params.num_symbols) {
    auto num_llrs = n * test_params.bits_per_symbol;
    auto expected_llr = armral::utils::allocate_random_i8(num_llrs);
    auto lib_llr = armral::utils::allocate_random_i8(num_llrs);
    auto symbols = armral::utils::allocate_random_cs16(n);
    for (auto ulp : test_params.llr_ulps) {
      // Perform the reference demodulation
      test_params.ref_func(n, ulp, symbols.data(), expected_llr.data());
      // Perform the demodulation from the library
      armral_demodulation(n, ulp, mod, symbols.data(), lib_llr.data());
      // Check the results
      passed &=
          check_llrs_equal(symbols.data(), lib_llr.data(), expected_llr.data(),
                           num_llrs, test_params.bits_per_symbol);
    }
  }
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;
  passed &= test_demod(ARMRAL_MOD_QPSK);
  passed &= test_demod(ARMRAL_MOD_16QAM);
  passed &= test_demod(ARMRAL_MOD_64QAM);
  passed &= test_demod(ARMRAL_MOD_256QAM);
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
