/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"
#include "utils/bits_to_bytes.hpp"

template<typename ConvolutionalDecodingFunction>
static bool run_convolutional_decoding_test(
    char const *name, const uint32_t k,
    ConvolutionalDecodingFunction convolutional_decoding_under_test) {

  // Generate random input
  std::vector<uint8_t> src_bytes(k);
  std::vector<uint8_t> src((k + 7) / 8);
  for (uint32_t i = 0; i < k; i++) {
    src_bytes[i] = rand() % 2;
  }
  armral::bytes_to_bits(k, src_bytes.data(), src.data());

  // Encoding
  std::vector<uint8_t> dst0(k + 1);
  std::vector<uint8_t> dst1(k + 1);
  std::vector<uint8_t> dst2(k + 1);

  armral_status ret = armral_tail_biting_convolutional_encode_block(
      (const uint8_t *)src.data(), k, dst0.data(), dst1.data(), dst2.data());

  std::vector<uint8_t> dst0_bytes(k);
  std::vector<uint8_t> dst1_bytes(k);
  std::vector<uint8_t> dst2_bytes(k);

  armral::bits_to_bytes(k, (const uint8_t *)dst0.data(), dst0_bytes.data());
  armral::bits_to_bytes(k, (const uint8_t *)dst1.data(), dst1_bytes.data());
  armral::bits_to_bytes(k, (const uint8_t *)dst2.data(), dst2_bytes.data());

  // Modulation
  armral_modulation_type mod_type = ARMRAL_MOD_16QAM;
  uint32_t bits_per_symbol = 4;
  auto num_mod_symbols = (k + bits_per_symbol - 1) / bits_per_symbol;
  auto num_bits = num_mod_symbols * bits_per_symbol;
  std::vector<armral_cmplx_int16_t> dst0_mod(num_mod_symbols);
  std::vector<armral_cmplx_int16_t> dst1_mod(num_mod_symbols);
  std::vector<armral_cmplx_int16_t> dst2_mod(num_mod_symbols);
  armral_modulation(num_bits, mod_type, dst0.data(), dst0_mod.data());
  armral_modulation(num_bits, mod_type, dst1.data(), dst1_mod.data());
  armral_modulation(num_bits, mod_type, dst2.data(), dst2_mod.data());

  // Demodulation
  uint16_t ulp = 11;
  std::vector<int8_t> dst0_demod_soft(num_bits);
  std::vector<int8_t> dst1_demod_soft(num_bits);
  std::vector<int8_t> dst2_demod_soft(num_bits);
  armral_demodulation(num_mod_symbols, ulp, mod_type, dst0_mod.data(),
                      dst0_demod_soft.data());
  armral_demodulation(num_mod_symbols, ulp, mod_type, dst1_mod.data(),
                      dst1_demod_soft.data());
  armral_demodulation(num_mod_symbols, ulp, mod_type, dst2_mod.data(),
                      dst2_demod_soft.data());

  // Decoding
  std::vector<uint8_t> out((k + 7) / 8);
  uint32_t iter_max = 3;
  ret = convolutional_decoding_under_test(
      dst0_demod_soft.data(), dst1_demod_soft.data(), dst2_demod_soft.data(), k,
      iter_max, out.data());

  std::vector<uint8_t> out_bytes(k);
  armral::bits_to_bytes(k, out.data(), out_bytes.data());

  // Check result
  bool passed = true;

  if (ret != ARMRAL_SUCCESS) {
    // GCOVR_EXCL_START
    printf("Error! [%s] k=%u did not return ARMRAL_SUCCESS\n", name, k);
    passed = false;
    // GCOVR_EXCL_STOP
  } else {
    printf("[%s] k=%u\n", name, k);
    auto check_dst = armral::utils::check_results_u8(name, out_bytes.data(),
                                                     src_bytes.data(), k);
    passed = check_dst;
  }

  return passed;
}

template<typename ConvolutionalDecodingFunction>
bool run_all_tests(
    char const *name,
    ConvolutionalDecodingFunction convolutional_decoding_under_test) {
  bool passed = true;
  for (auto k : {8, 11, 16, 32, 64, 85, 128, 201, 256, 300, 512, 799, 1024}) {
    passed &= run_convolutional_decoding_test(
        name, k, convolutional_decoding_under_test);
  }
  return passed;
}

// Entry point for unit testing of convolutional decoding
int main(int argc, char **argv) {
  bool passed = true;

  passed &= run_all_tests("CONVOLUTIONAL_DECODING",
                          armral_tail_biting_convolutional_decode_block);

  passed &= run_all_tests(
      "CONVOLUTIONAL_DECODING_NOALLOC",
      [](auto *src0, auto *src1, auto *src2, uint32_t k, uint32_t iter_max,
         auto *dst) {
        auto buffer_size =
            armral_tail_biting_convolutional_decode_block_noalloc_buffer_size(
                k, iter_max);
        std::vector<uint8_t> buffer(buffer_size);
        return armral_tail_biting_convolutional_decode_block_noalloc(
            src0, src1, src2, k, iter_max, dst, buffer.data());
      });

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
