/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "int_utils.hpp"

// CRC24A polynomial = x^24 + x^23 + x^18 + x^17 + x^14 + x^11 + x^10 + x^7 +
//                     x^6 + x^5 + x^4 + x^3 + x + 1
constexpr int crc24a_poly = 0x864cfb;
// CRC24B polynomial = x^24 + x^23 + x^6 + x^5 + x + 1
constexpr int crc24b_poly = 0x800063;
// CRC24C polynomial = x^24 + x^23 + x^21 + x^20 + x^17 + x^15 + x^13 + x^12 +
//                     x^8 + x^4 + x^2 + x + 1
constexpr int crc24c_poly = 0xb2b117;
// CRC16 polynomial  = x^16 + x^12 + x^5 + 1
constexpr int crc16_poly = 0x1021;
// CRC11 polynomial  = x^11 + x^10 + x^9 + x^5 + 1
constexpr int crc11_poly = 0x621;
// CRC6 polynomial   = x^6 + x^5 + 1
constexpr int crc6_poly = 0x21;

enum crc_type {
  CRC24A = 0,
  CRC24B = 1,
  CRC24C = 2,
  CRC16 = 3,
  CRC11 = 4,
  CRC6 = 5
};

static uint32_t crc_simple(uint32_t len, uint32_t gen, const uint8_t *bytes,
                           int n, bool big_endian) {
  // for a vague understanding of the algorithm, see
  // http://www.sunshine2k.de/articles/coding/crc/understanding_crc.html
  uint32_t crc = 0;
  for (int i = 0; i < n; ++i) {
    int ofs = big_endian ? i : (i & ~7) | (7 - (i % 8));
    uint8_t elem = bytes[ofs];
    for (int j = 0; j < 8; ++j) {
      crc <<= 1;
      crc ^= ((elem >> (7 - j)) & 1) << len;
      if ((crc & (1U << len)) != 0) {
        crc ^= gen;
      }
    }
  }
  return crc & ((1U << len) - 1U);
}

static bool crc_check(const char *name, const uint8_t *buf, int n,
                      uint32_t crc_res, uint32_t len, uint32_t gen,
                      bool big_endian) {
  auto crc_exp = crc_simple(len, gen, buf, n, big_endian);
  bool passed = crc_exp == crc_res;
  if (passed) {
    printf("[%s %d] - check result: OK\n", name, n);
  } else {
    // GCOVR_EXCL_START
    printf("[%s %d] - check result: ERROR (result = 0x%06x and expected = "
           "0x%06x\n",
           name, n, crc_res, crc_exp);
    // GCOVR_EXCL_STOP
  }
  return passed;
}

static bool run_crc_test(int n) {
  // from the interface doxygen:
  // a) It is assumed that input data is padded to 64bits, when the original
  // size is 32bits. b) If the input size is > 64bits, then a padding to 128bits
  // is assumed.
  assert(n == 8 || n % 16 == 0);
  const auto buf = armral::utils::allocate_random_u8(n);
  uint64_t crc_res = 0;
  bool passed = true;
  armral_crc24_a_le(n, (const uint64_t *)buf.data(), &crc_res);
  passed &=
      crc_check("CRC24A_LE", buf.data(), n, crc_res, 24, crc24a_poly, false);
  armral_crc24_b_le(n, (const uint64_t *)buf.data(), &crc_res);
  passed &=
      crc_check("CRC24B_LE", buf.data(), n, crc_res, 24, crc24b_poly, false);
  armral_crc24_c_le(n, (const uint64_t *)buf.data(), &crc_res);
  passed &=
      crc_check("CRC24C_LE", buf.data(), n, crc_res, 24, crc24c_poly, false);
  armral_crc16_le(n, (const uint64_t *)buf.data(), &crc_res);
  passed &=
      crc_check("CRC16_LE", buf.data(), n, crc_res, 16, crc16_poly, false);
  armral_crc11_le(n, (const uint64_t *)buf.data(), &crc_res);
  passed &=
      crc_check("CRC11_LE", buf.data(), n, crc_res, 11, crc11_poly, false);
  armral_crc6_le(n, (const uint64_t *)buf.data(), &crc_res);
  passed &= crc_check("CRC6_LE", buf.data(), n, crc_res, 6, crc6_poly, false);
  armral_crc24_a_be(n, (const uint64_t *)buf.data(), &crc_res);
  passed &=
      crc_check("CRC24A_BE", buf.data(), n, crc_res, 24, crc24a_poly, true);
  armral_crc24_b_be(n, (const uint64_t *)buf.data(), &crc_res);
  passed &=
      crc_check("CRC24B_BE", buf.data(), n, crc_res, 24, crc24b_poly, true);
  armral_crc24_c_be(n, (const uint64_t *)buf.data(), &crc_res);
  passed &=
      crc_check("CRC24C_BE", buf.data(), n, crc_res, 24, crc24c_poly, true);
  armral_crc16_be(n, (const uint64_t *)buf.data(), &crc_res);
  passed &= crc_check("CRC16_BE", buf.data(), n, crc_res, 16, crc16_poly, true);
  armral_crc11_be(n, (const uint64_t *)buf.data(), &crc_res);
  passed &= crc_check("CRC11_BE", buf.data(), n, crc_res, 11, crc11_poly, true);
  armral_crc6_be(n, (const uint64_t *)buf.data(), &crc_res);
  passed &= crc_check("CRC6_BE", buf.data(), n, crc_res, 6, crc6_poly, true);
  return passed;
}

int main(int argc, char **argv) {
  // Functional UTs
  std::vector<int> params{
      8,   16,  32,  48,   64,   80,   96,    112,   128,
      240, 256, 272, 1264, 1280, 1296, 10480, 10496, 10512,
  };
  bool passed = true;
  for (int n : params) {
    passed &= run_crc_test(n);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
