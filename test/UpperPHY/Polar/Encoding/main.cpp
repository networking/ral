/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "int_utils.hpp"

static void reference_polar_encoding(int nbits, const uint8_t *a, uint8_t *b) {
  int nbytes = nbits / 8;
  // xor pairs of groups of bits into high halves
  for (int i = 0; i < nbytes; ++i) {
    uint8_t x = a[i];
    x ^= (x << 1) & 0xaaU; // groups of 1b
    x ^= (x << 2) & 0xccU; // groups of 2b
    x ^= (x << 4) & 0xf0U; // groups of 4b
    b[i] = x;
  }
  // xor pairs of groups of bytes into low halves.
  for (int step = 1; step < nbytes; step *= 2) {
    for (int i = 0; i < nbytes - step; i += 2 * step) {
      for (int j = 0; j < step; ++j) {
        b[i + j] ^= b[i + j + step];
      }
    }
  }
}

static bool run_polar_encoding_test(int n) {
  assert(n % 32 == 0);
  armral::utils::int_random<uint8_t> random;
  const auto a = random.vector(n / 8);
  auto b = random.vector(n / 8);
  auto ref = b;
  reference_polar_encoding(n, a.data(), ref.data());

  printf("[POLAR ENCODING] n=%d\n", n);
  armral_polar_encode_block(n, a.data(), b.data());
  return armral::utils::check_results_u8("POLAR ENCODING", b.data(), ref.data(),
                                         n / 8);
}

// Entry point for unit testing of polar coding
int main(int argc, char **argv) {
  bool passed = true;
  for (int n : {32, 64, 128, 256, 512, 1024}) {
    passed &= run_polar_encoding_test(n);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
