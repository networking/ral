/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <vector>

static std::vector<uint8_t>
reference_subchannel_interleave(int n, int kplus, const uint8_t *frozen,
                                const uint8_t *c) {
  // this is a naive implementation of subchannel allocation based on the
  // pseudocode provided in section 5.3.1.2 of 3GPP TS 38.212.
  std::vector<uint8_t> u(n);
  int y0 = 0;
  int y1 = 0;
  int y2 = 0;
  int y3 = 0;
  int y4 = 0;
  int k = 0;
  for (int i = 0; i < n; ++i) {
    int yt = y0;
    y0 = y1;
    y1 = y2;
    y2 = y3;
    y3 = y4;
    y4 = yt;
    uint8_t bit;
    if (frozen[i] == ARMRAL_POLAR_PARITY_BIT) {
      bit = y0;
    } else if (frozen[i] == ARMRAL_POLAR_INFO_BIT) {
      bit = (c[k / 8] >> (7 - (k % 8))) & 1;
      y0 ^= bit;
      ++k;
    } else {
      // frozen
      continue;
    }
    uint16_t ofs = i / 8;
    uint16_t idx = 7 - (i % 8);
    u[ofs] |= bit << idx;
  }
  return u;
}

static bool run_polar_subchannel_interleave_test(int n, int k, int n_pc,
                                                 int n_pc_wm) {
  assert(n % 32 == 0);
  assert(k <= n);

  armral::utils::bit_random random;
  const auto c = random.bit_vector(k);
  auto u = random.bit_vector(n);

  // build a frozen mask to pass to the interleaving.
  // not handling parity bits for now.
  std::vector<uint8_t> frozen(n);
  armral_polar_frozen_mask(n, n, k, n_pc, n_pc_wm, frozen.data());

  auto ref_u =
      reference_subchannel_interleave(n, k + n_pc, frozen.data(), c.data());
  printf("[polar subchannel interleave] n=%d, k=%d\n", n, k);
  armral_polar_subchannel_interleave(n, k + n_pc, frozen.data(), c.data(),
                                     u.data());
  return armral::utils::check_results_u8("polar subchannel interleave",
                                         u.data(), ref_u.data(), n / 8);
}

int main(int argc, char **argv) {
  bool passed = true;
  for (auto n : {32, 64, 128, 256, 512, 1024}) {
    for (int k = 0; k <= n - 3; k += 5) {
      passed &= run_polar_subchannel_interleave_test(n, k, 0, 0);
      passed &= run_polar_subchannel_interleave_test(n, k, 3, 0);
      passed &= run_polar_subchannel_interleave_test(n, k, 3, 1);
    }
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
