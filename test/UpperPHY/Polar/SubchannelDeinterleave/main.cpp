/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"

#include <algorithm>
#include <cassert>
#include <cmath>
#include <vector>

static bool run_polar_subchannel_deinterleave_test(int n, int k, int n_pc,
                                                   int n_pc_wm) {
  assert(n % 32 == 0);
  assert(k + n_pc <= n);
  assert(n_pc_wm <= n_pc);

  armral::utils::bit_random random;
  auto ref_c = random.bit_vector(k);
  auto c = ref_c;
  auto u = random.bit_vector(n);

  // build a frozen mask to pass to the interleaving.
  // not handling parity bits for now.
  std::vector<uint8_t> frozen(n);
  armral_polar_frozen_mask(n, n, k, n_pc, n_pc_wm, frozen.data());

  printf("[polar subchannel deinterleave] n=%d, k=%d\n", n, k);
  armral_polar_subchannel_interleave(n, k + n_pc, frozen.data(), ref_c.data(),
                                     u.data());
  armral_polar_subchannel_deinterleave(k, frozen.data(), u.data(), c.data());

  return armral::utils::check_results_u8("polar subchannel deinterleave",
                                         c.data(), ref_c.data(), (k + 7) / 8);
}

int main(int argc, char **argv) {
  bool passed = true;
  for (auto n : {32, 64, 128, 256, 512, 1024}) {
    for (int k = 0; k <= n - 3; k += 5) {
      passed &= run_polar_subchannel_deinterleave_test(n, k, 0, 0);
      passed &= run_polar_subchannel_deinterleave_test(n, k, 3, 0);
      passed &= run_polar_subchannel_deinterleave_test(n, k, 3, 1);
    }
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
