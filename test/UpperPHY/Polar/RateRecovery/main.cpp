/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"

#include <cassert>
#include <cmath>
#include <cstring>

// Function to check log-likelihood results
static bool check_llrs_equal(const int8_t *result, const int8_t *expected,
                             const uint32_t n_values) {
  bool passed = true;
  for (uint32_t i = 0; i < n_values; ++i) {
    if (result[i] != expected[i]) {
      // GCOVR_EXCL_START
      printf("Sample %u: LLR calculated = %d != LLR expected "
             "= %d --> ERROR \n",
             i, result[i], expected[i]);
      passed = false;
      // GCOVR_EXCL_STOP
    }
  }

  if (!passed) {
    // GCOVR_EXCL_START
    printf("Check failed!\n");
    // GCOVR_EXCL_STOP
  } else {
    printf("Check successful!\n");
  }
  return passed;
}

int8_t ref_subblock_interleave_pattern[32] = {
    0,  1,  2,  4,  3,  5,  6,  7,  8,  16, 9,  17, 10, 18, 11, 19,
    12, 20, 13, 21, 14, 22, 15, 23, 24, 25, 26, 28, 27, 29, 30, 31};

static void ref_channel_deinterleave(int e, const int8_t *in, int8_t *out) {
  // performs the inverse of the channel interleaving step.  section 5.4.1.3 of
  // 3GPP TS 38.212. note that this function is operating on 8-bit LLR values
  // rather than individual bits.
  int t = std::ceil((std::sqrt(8.F * e + 1.F) - 1.F) / 2.F);
  int src_bit = 0;
  for (int col = 0; col < t; ++col) {
    for (int row = 0; row < (t - col); ++row) {
      int dst_bit = ((t * (t + 1)) / 2) - ((t - row) * (t - row + 1) / 2) + col;
      if (dst_bit < e) {
        out[dst_bit] = in[src_bit];
        ++src_bit;
      }
    }
  }
}

static void ref_llr_recovery(int n, int e, int k, const int8_t *llrs_deint,
                             int8_t *llrs) {
  // run rate recovery
  if (e >= n) {
    // repetition
    for (int i = 0; i < n; i++) {
      llrs[i] = llrs_deint[i];
    }
    for (int i = n; i < e; i++) {
      int32_t comb = (int32_t)llrs[i % n] + (int32_t)llrs_deint[i];
      if (comb < INT8_MIN) {
        llrs[i % n] = INT8_MIN;
      } else if (comb > INT8_MAX) {
        llrs[i % n] = INT8_MAX;
      } else {
        llrs[i % n] = (int8_t)comb;
      }
    }
  } else if (16 * k <= 7 * e) {
    // puncturing
    for (int i = e - 1; i >= 0; --i) {
      llrs[i + n - e] = llrs_deint[i];
    }
    for (int i = n - e - 1; i >= 0; --i) {
      llrs[i] = 0;
    }
  } else {
    // shortening
    for (int i = e - 1; i >= 0; --i) {
      llrs[i] = llrs_deint[i];
    }
    for (int i = n - e - 1; i >= 0; --i) {
      llrs[i + e] = INT8_MAX;
    }
  }
}

static void ref_subblock_deinterleave(int n, const int8_t *in, int8_t *out) {
  // performs the inverse of the sub-block interleaving step. note that this
  // function is operating on 8-bit LLR values rather than individual bits.
  assert(n % 32 == 0);
  int b = n / 32;
  for (int j = 0; j < n; j += b) {
    int ofs = ref_subblock_interleave_pattern[j / b] * b;
    for (int k = 0; k < b; ++k) {
      out[ofs + k] = in[j + k];
    }
  }
}

static void reference_polar_rate_recovery(int n, int e, int k,
                                          armral_polar_ibil_type i_bil,
                                          const int8_t *p_f, int8_t *p_d) {
  std::vector<int8_t> p_y(n, 0);

  if (i_bil == ARMRAL_POLAR_IBIL_DISABLE) {
    // llr recovery
    ref_llr_recovery(n, e, k, p_f, p_y.data());

  } else {
    std::vector<int8_t> p_e(e, 0);
    // channel de-interleaving
    ref_channel_deinterleave(e, p_f, p_e.data());
    // llr recovery
    ref_llr_recovery(n, e, k, p_e.data(), p_y.data());
  }
  // sub-block de-interleaving
  ref_subblock_deinterleave(n, p_y.data(), p_d);
}

template<typename PolarRateRecoveryFunction>
static bool run_polar_rate_recovery_test(
    char const *name, int n, int e, int k, armral_polar_ibil_type i_bil,
    PolarRateRecoveryFunction polar_rate_recovery_under_test) {
  assert(n % 32 == 0);
  assert(k <= e);

  auto ref_llr_demod = armral::utils::allocate_random_i8(e);
  auto ref_llr = armral::utils::allocate_random_i8(n);
  auto llr = ref_llr;

  printf("[%s] i_bil=%d n=%d, e=%d, k=%d\n", name, i_bil, n, e, k);
  polar_rate_recovery_under_test(n, e, k, i_bil, ref_llr_demod.data(),
                                 llr.data());
  reference_polar_rate_recovery(n, e, k, i_bil, ref_llr_demod.data(),
                                ref_llr.data());

  return check_llrs_equal(llr.data(), ref_llr.data(), n);
}

template<typename PolarRateRecoveryFunction>
static bool
run_all_tests(char const *name,
              PolarRateRecoveryFunction polar_rate_recovery_under_test) {
  bool passed = true;
  for (auto n : {32, 64, 128, 256, 512, 1024}) {
    for (int k = 7; k <= n; k += 7) {
      // We need to test at least 3 cases:
      // -     0 < e < 2.3 k (shortening)
      // - 2.3 k < e < n     (puncturing)
      // -         e >= n    (repetition)
      for (int e = k; e <= 2 * n; e += k) {
        passed &= run_polar_rate_recovery_test(name, n, e, k,
                                               ARMRAL_POLAR_IBIL_DISABLE,
                                               polar_rate_recovery_under_test);
        passed &= run_polar_rate_recovery_test(name, n, e, k,
                                               ARMRAL_POLAR_IBIL_ENABLE,
                                               polar_rate_recovery_under_test);
      }
    }
  }
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;

  passed &= run_all_tests("PolarRateRecovery", armral_polar_rate_recovery);
  passed &= run_all_tests(
      "PolarRateRecoveryNoAlloc", [](auto n, auto e, auto... args) {
        std::vector<uint8_t> buffer(n + e);
        return armral_polar_rate_recovery_noalloc(n, e, args..., buffer.data());
      });

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
