/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "cs16_utils.hpp"
#include "int_utils.hpp"
#include <cstdio>
#include <cstring>

template<bool test_noalloc>
static bool run_polar_decoding_test(uint32_t n, uint32_t e, uint32_t k,
                                    uint32_t n_pc, uint32_t n_pc_wm,
                                    uint32_t l) {
  assert(n % 32 == 0);
  assert(k + n_pc <= n);
  assert(k + n_pc <= e);
  assert(n_pc <= n);
  assert(n_pc_wm <= n_pc);

  printf("[POLAR DECODING] n=%u, e=%u, k=%u, l=%u, n_pc=%u, n_pc_wm=%u\n", n, e,
         k, l, n_pc, n_pc_wm);
  std::vector<uint8_t> frozen_mask(n);
  // not testing parity bits for now
  armral_polar_frozen_mask(n, e, k, n_pc, n_pc_wm, frozen_mask.data());

  // generate a random input
  uint32_t crc_bits = 24;           // 24-CRC (L = 24)
  uint32_t msg_bits = k - crc_bits; // message length (A = K - L)
  armral::utils::bit_random random;
  auto msg = random.bit_vector(msg_bits);

  // attach CRC bits
  auto in = random.bit_vector(k);
  armral_polar_crc_attachment(msg.data(), msg_bits, in.data());

  // run interleaving
  auto ref = random.bit_vector(n);
  armral_polar_subchannel_interleave(n, k + n_pc, frozen_mask.data(), in.data(),
                                     ref.data());

  // run encoding
  auto encoded = random.bit_vector(n);
  armral_polar_encode_block(n, ref.data(), encoded.data());

  // run modulation
  armral_modulation_type mod_type = ARMRAL_MOD_256QAM;
  int mod_num_symbols = n / 8;
  std::vector<armral_cmplx_int16_t> data_mod(mod_num_symbols);
  armral_modulation(n, mod_type, encoded.data(), data_mod.data());

  // run demodulation
  // The value of ulp shouldn't matter for a noiseless channel, but a small
  // ulp helps test that overflow in the llrs is handled correctly.
  uint16_t ulp = 11;
  std::vector<int8_t> data_demod_soft(n);
  armral_demodulation(mod_num_symbols, ulp, mod_type, data_mod.data(),
                      data_demod_soft.data());

  std::vector<uint8_t> out(l * n / 8);
  assert(l * ref.size() == out.size());

  // run decoding
  armral_polar_decode_block(n, frozen_mask.data(), l, data_demod_soft.data(),
                            out.data());

  // run deinterleaving and check CRC bits: if CRC == 0 compare the codeword
  // with the reference and stop.
  // deinterleaving ignores parity bits, so K rather than K + n_pc.
  std::vector<uint8_t> data_deint((k + 7) / 8);
  for (uint32_t i = 0; i < l; i++) {
    armral_polar_subchannel_deinterleave(
        k, frozen_mask.data(), out.data() + i * n / 8, data_deint.data());

    bool crc_pass;
    if constexpr (test_noalloc) {
      std::vector<uint8_t> buffer(
          armral_polar_crc_check_noalloc_buffer_size(k));
      crc_pass =
          armral_polar_crc_check_noalloc(data_deint.data(), k, buffer.data());
    } else {
      crc_pass = armral_polar_crc_check(data_deint.data(), k);
    }

    if (crc_pass) {
      return armral::utils::check_results_u8(
          "POLAR DECODING", data_deint.data(), in.data(), (k + 7) / 8);
    }
  }

  // compare ML codeword with reference if no codeword passed the CRC check
  armral_polar_subchannel_deinterleave(k, frozen_mask.data(), out.data(),
                                       data_deint.data());
  return armral::utils::check_results_u8("POLAR DECODING", data_deint.data(),
                                         in.data(), (k + 7) / 8);
}

// Entry point for unit testing of polar coding
int main(int argc, char **argv) {
  bool passed = true;
  for (auto l : {1, 2, 4, 8}) {
    for (auto n : {32, 64, 128, 256, 512, 1024}) {
      for (int k = 25; k <= n; k += 11) {
        // test e >= n to check repetition doesn't affect the frozen mask.
        for (int e = k; e <= n + 17; e += 17) {
          passed &= run_polar_decoding_test<false>(n, e, k, 0, 0, l);
          passed &= run_polar_decoding_test<true>(n, e, k, 0, 0, l);
          if (k + 3 <= n && k + 3 <= e) {
            passed &= run_polar_decoding_test<false>(n, e, k, 3, 0, l);
            passed &= run_polar_decoding_test<false>(n, e, k, 3, 1, l);
            passed &= run_polar_decoding_test<true>(n, e, k, 3, 0, l);
            passed &= run_polar_decoding_test<true>(n, e, k, 3, 1, l);
          }
        }
      }
    }
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
