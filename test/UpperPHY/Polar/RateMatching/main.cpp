/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"

#include <cassert>
#include <cmath>
#include <cstring>
#include <vector>

int8_t ref_subblock_interleave_pattern[32] = {
    0,  1,  2,  4,  3,  5,  6,  7,  8,  16, 9,  17, 10, 18, 11, 19,
    12, 20, 13, 21, 14, 22, 15, 23, 24, 25, 26, 28, 27, 29, 30, 31};

static void ref_subblock_interleave(int n, const uint8_t *in, uint8_t *out) {
  // performs the sub-block interleaving step of polar encoding, as specified
  // by section 5.4.1.1 of 3GPP TS 38.212 by interleaving 32 blocks of n/32
  // bits according to the sub-block interleaver pattern (table 5.4.1.1-1).
  assert(n % 32 == 0);
  memset((void *)out, 0, n / 8);
  int b = n / 32;
  for (int j = 0; j < n; j += b) {
    int ofs = ref_subblock_interleave_pattern[j / b] * b;
    for (int k = 0; k < b; ++k) {
      int src_idx = (ofs + k) / 8;
      int src_bit = 7 - (ofs + k) % 8;
      int dst_idx = (j + k) / 8;
      int dst_bit = 7 - (j + k) % 8;
      uint32_t bit = (in[src_idx] >> src_bit) & 1;
      out[dst_idx] |= bit << dst_bit;
    }
  }
}

static void ref_bit_selection(int n, int e, int k, const uint8_t *in,
                              uint8_t *out) {
  // performs bit selection by mean of repetition, puncturing or shortening,
  // as specified by section 5.4.1.2 of 3GPP TS 38.212.
  if (e >= n) {
    // repetition
    for (int i = 0; i < e / 8; ++i) {
      out[i] = in[i % (n / 8)];
    }
    if (e % 8 != 0) {
      out[e / 8] = in[(e / 8) % (n / 8)] & (0xFF << (8 - (e % 8)));
    }
  } else if (16 * k <= 7 * e) {
    // puncturing
    for (int i = 0; i < e; ++i) {
      int src_bit = i + (n - e);
      uint32_t bit = (in[src_bit / 8] >> (7 - (src_bit % 8))) & 1;
      out[i / 8] |= bit << (7 - (i % 8));
    }
  } else {
    // shortening
    for (int i = 0; i < e; ++i) {
      int src_bit = i;
      uint32_t bit = (in[src_bit / 8] >> (7 - (src_bit % 8))) & 1;
      out[i / 8] |= bit << (7 - (i % 8));
    }
  }
}

static void ref_channel_interleave(int e, const uint8_t *in, uint8_t *out) {
  // performs the channel interleaving step of polar encoding, as specified by
  // section 5.4.1.3 of 3GPP TS 38.212.
  // we choose T such that (T * (T + 1) / 2) ≥ E, which can be expressed
  // exactly as: T = round_up((sqrt(8E + 1) - 1) / 2).
  int t = std::ceil((std::sqrt(8.F * e + 1.F) - 1.F) / 2.F);
  memset((void *)out, 0, (e + 7) / 8);
  int dst_bit = 0;
  for (int col = 0; col < t; ++col) {
    for (int row = 0; row < (t - col); ++row) {
      int src_bit = ((t * (t + 1)) / 2) - ((t - row) * (t - row + 1) / 2) + col;
      if (src_bit < e) {
        uint8_t bit = (in[src_bit / 8] >> (7 - (src_bit % 8))) & 1;
        out[dst_bit / 8] |= bit << (7 - (dst_bit % 8));
        ++dst_bit;
      }
    }
  }
}

static void reference_polar_rate_matching(int n, int e, int k,
                                          armral_polar_ibil_type i_bil,
                                          const uint8_t *p_d, uint8_t *p_f) {
  std::vector<uint8_t> p_y(n / 8, 0);
  // sub-block interleaving
  ref_subblock_interleave(n, p_d, p_y.data());
  if (i_bil == ARMRAL_POLAR_IBIL_DISABLE) {
    memset((void *)p_f, 0, (e + 7) / 8);
    // bit selection
    ref_bit_selection(n, e, k, p_y.data(), p_f);
  } else {
    std::vector<uint8_t> p_e((e + 7) / 8, 0);
    // bit selection
    ref_bit_selection(n, e, k, p_y.data(), p_e.data());
    // channel interleaving
    ref_channel_interleave(e, p_e.data(), p_f);
  }
}

template<typename PolarRateMatchingFunction>
static bool run_polar_rate_matching_test(
    char const *name, int n, int e, int k, armral_polar_ibil_type i_bil,
    PolarRateMatchingFunction polar_rate_matching_under_test) {
  assert(n % 32 == 0);
  assert(k <= e);

  armral::utils::bit_random random;
  auto ref_d = random.bit_vector(n);
  auto ref_f = random.bit_vector(e);
  auto f = ref_f;

  printf("[%s]  i_bil=%d n=%d, e=%d, k=%d\n", name, i_bil, n, e, k);
  polar_rate_matching_under_test(n, e, k, i_bil, ref_d.data(), f.data());
  reference_polar_rate_matching(n, e, k, i_bil, ref_d.data(), ref_f.data());

  return armral::utils::check_results_u8(name, f.data(), ref_f.data(), e / 8);
}

template<typename PolarRateMatchingFunction>
static bool
run_all_tests(char const *name,
              PolarRateMatchingFunction polar_rate_matching_under_test) {
  bool passed = true;
  for (auto n : {32, 64, 128, 256, 512, 1024}) {
    for (int k = 7; k <= n; k += 7) {
      // We need to test at least 3 cases:
      // -     0 < e < 2.3 k (shortening)
      // - 2.3 k < e < n     (puncturing)
      // -         e >= n    (repetition)
      for (int e = k; e <= 2 * n; e += k) {
        passed &= run_polar_rate_matching_test(name, n, e, k,
                                               ARMRAL_POLAR_IBIL_DISABLE,
                                               polar_rate_matching_under_test);

        passed &= run_polar_rate_matching_test(name, n, e, k,
                                               ARMRAL_POLAR_IBIL_ENABLE,
                                               polar_rate_matching_under_test);
      }
    }
  }
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;

  passed &= run_all_tests("PolarRateMatching", armral_polar_rate_matching);
  passed &= run_all_tests(
      "PolarRateMatchingNoAlloc", [](auto n, auto e, auto... args) {
        std::vector<uint8_t> buffer((n + e + 7) / 8);
        return armral_polar_rate_matching_noalloc(n, e, args..., buffer.data());
      });

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
