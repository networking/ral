/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "int_utils.hpp"
#include "polar_crc_attach_data.hpp"

template<typename PolarCRCAttachFunction>
static bool run_polar_crc_attachment_test(
    char const *name, struct polar_crc_attach_param_t t,
    PolarCRCAttachFunction polar_crc_attach_under_test) {

  // Number of CRC bits
  constexpr uint32_t l = 24;

  // Number of bits to encode
  uint32_t k = t.a + l;
  uint32_t k_bytes = (k + 7) / 8;

  auto out = armral::utils::allocate_random_u8(k_bytes);
  polar_crc_attach_under_test(t.in.data(), t.a, out.data());

  return armral::utils::check_results_u8(name, out.data(), t.out.data(),
                                         k_bytes);
}

template<typename PolarCRCAttachFunction>
static bool run_all_tests(char const *name,
                          PolarCRCAttachFunction polar_crc_attach_under_test) {
  bool passed = true;
  for (const auto &test : polar_crc_attach_tests) {
    passed &=
        run_polar_crc_attachment_test(name, test, polar_crc_attach_under_test);
  }
  return passed;
}

// Entry point for unit testing of polar coding
int main(int argc, char **argv) {
  bool passed = true;

  passed &= run_all_tests("PolarCRCAttachment", armral_polar_crc_attachment);
  passed &= run_all_tests(
      "PolarCRCAttachmentNoAlloc",
      [](uint8_t const *data_in, uint32_t a, uint8_t *data_out) {
        auto buffer_size = armral_polar_crc_attachment_noalloc_buffer_size(a);
        std::vector<uint8_t> buffer(buffer_size);
        return armral_polar_crc_attachment_noalloc(data_in, a, data_out,
                                                   buffer.data());
      });

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
