/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include <vector>

struct polar_crc_attach_param_t {
  // The total number of bits in the code block
  uint32_t a;
  // The input code block
  std::vector<uint8_t> in;
  // The output code block
  std::vector<uint8_t> out;
};

std::vector<polar_crc_attach_param_t> polar_crc_attach_tests = {
    {88,
     {208, 34, 231, 213, 32, 248, 233, 56, 161, 78, 24},
     {208, 34, 231, 213, 32, 248, 233, 56, 161, 78, 24, 114, 62, 96}},
    {83,
     {152, 124, 56, 99, 82, 134, 110, 25, 3, 10, 224},
     {152, 124, 56, 99, 82, 134, 110, 25, 3, 10, 250, 170, 73, 0}},
};
