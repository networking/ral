/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "../../turbo_decode_test_utils.hpp"

int main(int argc, char **argv) {

  constexpr uint32_t num_blocks_arr[3] = {7, 8, 18};

  bool passed = true;
  for (auto num_blocks : num_blocks_arr) {
    // There is a signature difference for batched vs single
    // so create lambda wrappers to handle this difference
    auto decode_batch_alloc = [&](const int8_t *sys, const int8_t *par,
                                  const int8_t *itl, uint32_t passed_k,
                                  uint8_t *dst, uint32_t max_iter,
                                  uint16_t *perm_idxs) {
      return armral_turbo_decode_batch(num_blocks, sys, par, itl, passed_k, dst,
                                       max_iter, perm_idxs);
    };
    auto decode_batch_noalloc = [&](const int8_t *sys, const int8_t *par,
                                    const int8_t *itl, uint32_t passed_k,
                                    uint8_t *dst, uint32_t max_iter,
                                    uint16_t *perm_idxs, void *buffer) {
      return armral_turbo_decode_batch_noalloc(num_blocks, sys, par, itl,
                                               passed_k, dst, max_iter,
                                               perm_idxs, buffer);
    };
    passed &= run_all_turbo_decoding_tests(
        num_blocks, decode_batch_alloc, decode_batch_noalloc,
        armral_turbo_decode_batch_noalloc_buffer_size, "TurboDecodingBatch",
        "TurboDecodingBatchNoPermIdxs", "TurboDecodingBatchNoAlloc",
        "TurboDecodingBatchNoAllocNoPermIdxs");
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
