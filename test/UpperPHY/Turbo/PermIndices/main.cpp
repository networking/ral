/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include "../turbo_test_data.hpp"

#include <cstdio>

static bool run_perm_idxs_test(uint16_t *perm_idxs_buff) {
  bool ret = true;

  uint16_t *perm_idx = (uint16_t *)perm_idxs_buff;
  for (auto k : valid_ks) {
    bool passed = true;

    for (uint16_t i = 0; i < k; ++i, perm_idx += 3) {
      uint16_t f1 = perm_params.at(k).first;
      uint16_t f2 = perm_params.at(k).second;
      uint16_t test_perm_idx =
          static_cast<uint16_t>((uint64_t(f1) * i + uint64_t(f2) * i * i) % k);
      passed &= test_perm_idx == *perm_idx;
      passed &= test_perm_idx / 8 == *(perm_idx + 1);
      passed &= test_perm_idx % 8 == *(perm_idx + 2);
    }

    if (passed) {
      printf("[SetupPermIdxs_%u] - check result: OK\n", k);
    } else {
      // GCOVR_EXCL_START
      printf("Error! [SetupPermIdxs_%u]\n", k);
      ret = false;
      // GCOVR_EXCL_STOP
    }
  }

  return ret;
}

int main(int argc, char **argv) {
  uint32_t buff_size = 0;
  for (auto k : valid_ks) {
    buff_size += k;
  }
  buff_size *= 3; // perm_idx, vec_idx, and vec_lane
  std::vector<uint16_t> perm_idxs_buff(buff_size);

  armral_turbo_perm_idx_init(perm_idxs_buff.data());
  bool passed = run_perm_idxs_test(perm_idxs_buff.data());

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
