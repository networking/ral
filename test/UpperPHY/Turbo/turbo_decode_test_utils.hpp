/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "turbo_test_data.hpp"

#include <cstdio>

template<typename TurboDecodeFunction>
static inline bool
run_turbo_decoding_parameter_test(TurboDecodeFunction turbo_decode_under_test) {
  return turbo_decode_under_test(NULL, NULL, NULL, 1040, NULL, 0, NULL) ==
         ARMRAL_ARGUMENT_ERROR;
}

template<typename T>
void interleave(T *src, uint32_t ldsrc, T *dst, uint32_t lddst) {
  for (uint32_t a = 0; a < ldsrc; ++a) {
    for (uint32_t b = 0; b < lddst; ++b) {
      dst[a * lddst + b] = src[b * ldsrc + a];
    }
  }
}

static inline void setup_block_data(uint32_t k, uint32_t b, uint8_t *src,
                                    int8_t *sys, int8_t *par, int8_t *itl) {
  auto k_bytes = k / 8;

  std::vector<uint8_t> sys_encode(k_bytes + 1, 255);
  std::vector<uint8_t> par_encode(k_bytes + 1, 255);
  std::vector<uint8_t> itl_encode(k_bytes + 1, 255);

  // Generate the input test data
  generate_turbo_test_data(src, k, b);

  // Encode the test data
  armral_turbo_encode_block(src, k, sys_encode.data(), par_encode.data(),
                            itl_encode.data());

  // Run modulation on the three output vectors
  armral_modulation_type mod_type = ARMRAL_MOD_16QAM;
  auto encoded_len = k + 4;
  auto mod_num_symbols = encoded_len / 4;
  std::vector<armral_cmplx_int16_t> sys_mod(mod_num_symbols);
  std::vector<armral_cmplx_int16_t> par_mod(mod_num_symbols);
  std::vector<armral_cmplx_int16_t> itl_mod(mod_num_symbols);
  armral_modulation(encoded_len, mod_type, sys_encode.data(), sys_mod.data());
  armral_modulation(encoded_len, mod_type, par_encode.data(), par_mod.data());
  armral_modulation(encoded_len, mod_type, itl_encode.data(), itl_mod.data());

  // Now demodulate to get LLRs
  // The value of ulp shouldn't matter for a noiseless channel, but a small
  // ulp helps test that overflow in the llrs is handled correctly.
  uint16_t ulp = 11;
  armral_demodulation(mod_num_symbols, ulp, mod_type, sys_mod.data(), sys);
  armral_demodulation(mod_num_symbols, ulp, mod_type, par_mod.data(), par);
  armral_demodulation(mod_num_symbols, ulp, mod_type, itl_mod.data(), itl);
}

// Check that the decoder returns the original
// unencoded input for valid values of k.
template<typename TurboDecodeFunction>
static bool
run_one_turbo_decoding_test(uint32_t num_blocks, char const *name, uint32_t k,
                            uint16_t *perm_idxs,
                            TurboDecodeFunction turbo_decode_under_test) {

  uint32_t len = k + 4;
  auto k_bytes = k >> 3;

  std::vector<uint8_t> src(num_blocks * (k_bytes + 1));
  std::vector<uint8_t> ans(num_blocks * k_bytes);
  std::vector<int8_t> sys(num_blocks * len);
  std::vector<int8_t> par(num_blocks * len);
  std::vector<int8_t> itl(num_blocks * len);

  for (uint32_t b = 0; b < num_blocks; ++b) {
    setup_block_data(k, b, &src[b * (k_bytes + 1)], &sys[b * len],
                     &par[b * len], &itl[b * len]);
  }

  // Decode the encoded data. We set the maximum number of decoder iterations to
  // 6, without noise this should be enough.
  armral_status ret = turbo_decode_under_test(
      sys.data(), par.data(), itl.data(), k, ans.data(), 6, perm_idxs);

  bool passed = true;
  if (ret != ARMRAL_SUCCESS) {
    // GCOVR_EXCL_START
    printf("Error! [%s_BatchSize-%u_k-%u] did not return ARMRAL_SUCCESS\n",
           name, num_blocks, k);
    passed = false;
    // GCOVR_EXCL_STOP
  }

  // Check the decoded data matches the original.
  // We are checking uint8_ts here not individual bits. The only valid values
  // for k in the spec are multiples of 8 so no need to worry about remainders.
  for (uint32_t b = 0; b < num_blocks; ++b) {
    for (uint32_t i = 0; i < k_bytes; ++i) {
      if (ans[b * k_bytes + i] != src[b * (k_bytes + 1) + i]) {
        // GCOVR_EXCL_START
        printf(
            "Error! [%s_BatchSize-%u_k-%u] result[b_i=%u][k8_i=%u] = 0x%x and "
            "expected[b_i=%u][k8_i=%u] = 0x%x. Diff = 0x%x\n",
            name, num_blocks, k, b, i, ans[b * k_bytes + i], b, i,
            src[b * (k_bytes + 1) + i],
            std::abs(ans[b * k_bytes + i] - src[b * (k_bytes + 1) + i]));
        passed = false;
        // GCOVR_EXCL_STOP
      }
    }
  }
  if (passed) {
    printf("[%s_BatchSize-%u_k-%u] - check result: OK\n", name, num_blocks, k);
  }
  return passed;
}

template<typename TurboDecodeAlloc, typename TurboDecodeNoAlloc,
         typename TurboBuffSize>
static bool
run_all_turbo_decoding_tests(uint32_t num_blocks, TurboDecodeAlloc alloc_fn,
                             TurboDecodeNoAlloc noalloc_fn,
                             TurboBuffSize buff_size_fn, char const *base,
                             char const *no_perm_idxs, char const *no_alloc,
                             char const *no_alloc_no_perm_idxs) {
  bool passed = true;

  // Check invalid k is detected
  passed &= run_turbo_decoding_parameter_test(alloc_fn);

  // init the buffer for the perm_idxs
  uint32_t perm_buff_size = 0;
  for (auto k : valid_ks) {
    perm_buff_size += k;
  }
  perm_buff_size *= 3; // perm_idx, vec_idx, and vec_lane
  std::vector<uint16_t> perm_idxs_buff(perm_buff_size);
  armral_turbo_perm_idx_init(perm_idxs_buff.data());

  // Check decoder decodes correctly
  // Allocating
  for (auto k : valid_ks) {
    passed &= run_one_turbo_decoding_test(num_blocks, base, k,
                                          perm_idxs_buff.data(), alloc_fn);
  }
  for (auto k : valid_ks) {
    passed &= run_one_turbo_decoding_test(num_blocks, no_perm_idxs, k, nullptr,
                                          alloc_fn);
  }

  // No allocating
  auto no_alloc_test = [&](const int8_t *sys, const int8_t *par,
                           const int8_t *itl, uint32_t passed_k, uint8_t *dst,
                           uint32_t max_iter, uint16_t *perm_idxs) {
    auto buffer_size = buff_size_fn(passed_k);
    std::vector<uint8_t> buffer(buffer_size);
    return noalloc_fn(sys, par, itl, passed_k, dst, max_iter, perm_idxs,
                      buffer.data());
  };
  for (auto k : valid_ks) {
    passed &= run_one_turbo_decoding_test(num_blocks, no_alloc, k,
                                          perm_idxs_buff.data(), no_alloc_test);
  }

  for (auto k : valid_ks) {
    passed &= run_one_turbo_decoding_test(num_blocks, no_alloc_no_perm_idxs, k,
                                          nullptr, no_alloc_test);
  }

  return passed;
}
