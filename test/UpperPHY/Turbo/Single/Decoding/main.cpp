/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "../../turbo_decode_test_utils.hpp"

int main(int argc, char **argv) {
  bool passed = run_all_turbo_decoding_tests(
      1, armral_turbo_decode_block, armral_turbo_decode_block_noalloc,
      armral_turbo_decode_block_noalloc_buffer_size, "TurboDecoding",
      "TurboDecodingNoPermIdxs", "TurboDecodingNoAlloc",
      "TurboDecodingNoAllocNoPermIdxs");
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
