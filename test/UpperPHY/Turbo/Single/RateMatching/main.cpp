/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"

#include <cassert>
#include <cmath>
#include <cstring>
#include <vector>

// The number of bits in the encoded message passed to rate matching is the
// length of the code block given to Turbo encoding (k) plus 4 termination bits.
static constexpr uint32_t valid_ds[188] = {
    44,   52,   60,   68,   76,   84,   92,   100,  108,  116,  124,  132,
    140,  148,  156,  164,  172,  180,  188,  196,  204,  212,  220,  228,
    236,  244,  252,  260,  268,  276,  284,  292,  300,  308,  316,  324,
    332,  340,  348,  356,  364,  372,  380,  388,  396,  404,  412,  420,
    428,  436,  444,  452,  460,  468,  476,  484,  492,  500,  508,  516,
    532,  548,  564,  580,  596,  612,  628,  644,  660,  676,  692,  708,
    724,  740,  756,  772,  788,  804,  820,  836,  852,  868,  884,  900,
    916,  932,  948,  964,  980,  996,  1012, 1028, 1060, 1092, 1124, 1156,
    1188, 1220, 1252, 1284, 1316, 1348, 1380, 1412, 1444, 1476, 1508, 1540,
    1572, 1604, 1636, 1668, 1700, 1732, 1764, 1796, 1828, 1860, 1892, 1924,
    1956, 1988, 2020, 2052, 2116, 2180, 2244, 2308, 2372, 2436, 2500, 2564,
    2628, 2692, 2756, 2820, 2884, 2948, 3012, 3076, 3140, 3204, 3268, 3332,
    3396, 3460, 3524, 3588, 3652, 3716, 3780, 3844, 3908, 3972, 4036, 4100,
    4164, 4228, 4292, 4356, 4420, 4484, 4548, 4612, 4676, 4740, 4804, 4868,
    4932, 4996, 5060, 5124, 5188, 5252, 5316, 5380, 5444, 5508, 5572, 5636,
    5700, 5764, 5828, 5892, 5956, 6020, 6084, 6148};

static void reference_subblock_interleave(uint32_t d, uint32_t kw,
                                          const uint8_t *d0, const uint8_t *d1,
                                          const uint8_t *d2, uint8_t *v0,
                                          uint8_t *v1, uint8_t *v2,
                                          uint8_t *dummy0, uint8_t *dummy1,
                                          uint8_t *dummy2) {
  // Length of each information bit stream
  assert(kw % 3 == 0);
  const uint32_t kpi = kw / 3;
  assert(kpi % 8 == 0);
  const uint32_t kpib = kpi / 8;

  std::vector<uint8_t> y0(kpib);
  std::vector<uint8_t> y1(kpib);
  std::vector<uint8_t> y2(kpib);

  // Copy input and pad dummy bits

  // Length of input bits in bytes
  const uint32_t ilen = (d + 7) / 8;

  // Calculate number of bits to be zero-padded
  const uint32_t nd = kpi - d;
  assert(nd % 4 == 0 && nd % 8 == 4);
  const uint32_t ndb = nd / 8;

  // Copy input bits and shift by ndb bytes
  memcpy((void *)&y0[ndb], (const void *)d0, sizeof(uint8_t) * ilen);
  memcpy((void *)&y1[ndb], (const void *)d1, sizeof(uint8_t) * ilen);
  memcpy((void *)&y2[ndb], (const void *)d2, sizeof(uint8_t) * ilen);
  memset((void *)dummy0, 0xFF, sizeof(uint8_t) * ndb);
  memset((void *)dummy1, 0xFF, sizeof(uint8_t) * ndb);
  memset((void *)dummy2, 0xFF, sizeof(uint8_t) * ndb);

  // The remaining nd % 8 = 4 bits to be shifted
  const uint8_t s = 4;

  // Right shift 4 bits, extract 4 low bits from the previous byte,
  // and set those in higher 4 bits of the current byte
  y0[ndb] >>= s;
  y1[ndb] >>= s;
  y2[ndb] >>= s;
  for (uint32_t i = 1; i < ilen; ++i) {
    y0[i + ndb] >>= s;
    y0[i + ndb] |= d0[i - 1] << s;
    y1[i + ndb] >>= s;
    y1[i + ndb] |= d1[i - 1] << s;
    y2[i + ndb] >>= s;
    y2[i + ndb] |= d2[i - 1] << s;
  }
  dummy0[ndb] = 0xF0;
  dummy1[ndb] = 0xF0;
  dummy2[ndb] = 0xF0;

  // Number of rows and columns of the information bit matrix
  constexpr uint32_t ctc = 32;
  const uint32_t rtc = kpi / ctc;

  // Inter-column permutation pattern (Table 5.1.4-1, TS 36.212)
  constexpr uint8_t p[ctc] = {0,  16, 8,  24, 4,  20, 12, 28, 2,  18, 10,
                              26, 6,  22, 14, 30, 1,  17, 9,  25, 5,  21,
                              13, 29, 3,  19, 11, 27, 7,  23, 15, 31};

  // Perform inter-column permutation for each row of d^(0)_k and d^(1)_k
  std::vector<uint8_t> y0_perm(kpib);
  std::vector<uint8_t> y1_perm(kpib);
  for (uint32_t i = 0; i < rtc; ++i) {
    for (uint8_t j = 0; j < ctc; ++j) {
      uint32_t idx = (p[j] + i * ctc) / 8;
      uint32_t jdx = 7 - (p[j] % 8);
      uint32_t y0bit = (y0[idx] >> jdx) & 1;
      uint32_t y1bit = (y1[idx] >> jdx) & 1;
      y0_perm[(j + i * ctc) / 8] |= y0bit << (7 - (j % 8));
      y1_perm[(j + i * ctc) / 8] |= y1bit << (7 - (j % 8));
    }
  }

  // Permute only the first row of the dummy bit tracker for y^0 and y^1
  uint8_t dummy0_perm[4] = {0};
  for (uint8_t j = 0; j < ctc; ++j) {
    uint32_t idx = p[j] / 8;
    uint32_t jdx = p[j] % 8;
    dummy0_perm[j / 8] |= ((dummy0[idx] >> (7 - jdx)) & 1) << (7 - (j % 8));
  }
  memcpy((void *)dummy0, (const void *)dummy0_perm, sizeof(uint8_t) * 4);
  memcpy((void *)dummy1, (const void *)dummy0_perm, sizeof(uint8_t) * 4);

  // Read out permuted matrix column by column
  std::vector<uint8_t> dummyt(kpib);
  for (uint32_t j = 0; j < ctc; ++j) {
    for (uint32_t i = 0; i < rtc; ++i) {
      uint32_t vidx = (i + j * rtc) / 8;
      uint32_t vjdx = 7 - ((i + j * rtc) % 8);
      uint32_t yidx = (j + i * ctc) / 8;
      uint32_t yjdx = 7 - ((j + i * ctc) % 8);
      v0[vidx] |= ((y0_perm[yidx] >> yjdx) & 1) << vjdx;
      v1[vidx] |= ((y1_perm[yidx] >> yjdx) & 1) << vjdx;
      dummyt[vidx] |= ((dummy0[yidx] >> yjdx) & 1) << vjdx;
    }
  }
  memcpy((void *)dummy0, (const void *)dummyt.data(), sizeof(uint8_t) * kpib);
  memcpy((void *)dummy1, (const void *)dummyt.data(), sizeof(uint8_t) * kpib);

  // Perform permutation for d^(2)_k
  memset((void *)dummyt.data(), 0, sizeof(uint8_t) * kpib);
  for (uint32_t i = 0; i < kpi; ++i) {
    uint32_t pi = (p[i / rtc] + ctc * (i % rtc) + 1) % kpi;
    uint32_t yidx = pi / 8;
    uint32_t yjdx = 7 - (pi % 8);
    uint32_t vidx = i / 8;
    uint32_t vjdx = 7 - (i % 8);
    v2[vidx] |= ((y2[yidx] >> yjdx) & 1) << vjdx;
    dummyt[vidx] |= ((dummy2[yidx] >> yjdx) & 1) << vjdx;
  }
  memcpy((void *)dummy2, (const void *)dummyt.data(), sizeof(uint8_t) * kpib);
}

static void reference_bit_collection(const uint32_t kpi, const uint8_t *v0,
                                     const uint8_t *v1, const uint8_t *v2,
                                     uint8_t *w, const uint8_t *dummy0,
                                     const uint8_t *dummy1,
                                     const uint8_t *dummy2, uint8_t *dummy) {
  const uint32_t kpib = kpi / 8;

  // w_k = v^0_k for k = [0, kpi - 1]
  memcpy((void *)w, (const void *)v0, sizeof(uint8_t) * kpib);
  memcpy((void *)dummy, (const void *)dummy0, sizeof(uint8_t) * kpib);

  // w_{kpi + 2k    } = v^1_k,
  // w_{kpi + 2k + 1} = v^2_k for k = [0, kpi - 1]
  for (uint32_t k = 0; k < kpi; ++k) {
    uint32_t widx1 = (kpi + 2 * k) / 8;
    uint32_t widx2 = (kpi + 2 * k + 1) / 8;
    uint32_t wjdx1 = 7 - ((kpi + 2 * k) % 8);
    uint32_t wjdx2 = 7 - ((kpi + 2 * k + 1) % 8);
    uint32_t vidx = k / 8;
    uint32_t vjdx = 7 - (k % 8);
    w[widx1] |= ((v1[vidx] >> vjdx) & 1) << wjdx1;
    w[widx2] |= ((v2[vidx] >> vjdx) & 1) << wjdx2;
    dummy[widx1] |= ((dummy1[vidx] >> vjdx) & 1) << wjdx1;
    dummy[widx2] |= ((dummy2[vidx] >> vjdx) & 1) << wjdx2;
  }
}

static void reference_bit_selection(const uint32_t kw, const uint32_t ncb,
                                    const uint32_t k0, const uint32_t e,
                                    const uint8_t *w, const uint8_t *dummy,
                                    uint8_t *out) {
  memset((void *)out, 0, sizeof(uint8_t) * (e + 7) / 8);
  uint32_t k = 0;
  uint32_t j = 0;
  while (k < e) {
    uint32_t i = (k0 + j) % ncb;
    uint32_t widx = i / 8;
    uint32_t wjdx = 7 - (i % 8);
    // Check that w_i is not a dummy bit
    bool not_dummy = ((~dummy[widx] >> wjdx) & 1) == 1U;
    if (not_dummy) {
      uint32_t eidx = k / 8;
      uint32_t ejdx = 7 - (k % 8);
      out[eidx] |= ((w[widx] >> wjdx) & 1) << ejdx;
      k++;
    }
    j++;
  }
}

static void reference_turbo_rate_matching(uint32_t d, uint32_t e, uint32_t rv,
                                          const uint8_t *src0,
                                          const uint8_t *src1,
                                          const uint8_t *src2, uint8_t *dst) {
  assert(d > 0);
  assert(e > 0);
  assert(rv <= 3);

  constexpr uint32_t ctc = 32;
  // The minimum number of rows which gives rtc * ctc >= d.
  const uint32_t rtc = (d + ctc - 1) / ctc;
  const uint32_t kpi = rtc * ctc;
  const uint32_t kw = 3 * kpi;
  const uint32_t kpib = kpi / 8;
  const uint32_t kwb = kw / 8;

  // Assume N_cb = k_w.
  const uint32_t ncb = kw;

  // Calculate k0 with the assumption N_cb = k_w.
  const uint32_t k0 = rtc * (24 * rv + 2);

  std::vector<uint8_t> v0(kpib);
  std::vector<uint8_t> v1(kpib);
  std::vector<uint8_t> v2(kpib);
  std::vector<uint8_t> dummy0(kpib);
  std::vector<uint8_t> dummy1(kpib);
  std::vector<uint8_t> dummy2(kpib);
  reference_subblock_interleave(d, kw, src0, src1, src2, v0.data(), v1.data(),
                                v2.data(), dummy0.data(), dummy1.data(),
                                dummy2.data());

  std::vector<uint8_t> w(kwb);
  std::vector<uint8_t> dummy(kwb);
  reference_bit_collection(kpi, v0.data(), v1.data(), v2.data(), w.data(),
                           dummy0.data(), dummy1.data(), dummy2.data(),
                           dummy.data());

  reference_bit_selection(kw, ncb, k0, e, w.data(), dummy.data(), dst);
}

template<typename TurboRateMatchingFunction>
static bool run_turbo_rate_matching_test(
    char const *name, uint32_t d, uint32_t e, uint32_t rv,
    TurboRateMatchingFunction turbo_rate_matching_under_test) {
  bool passed = true;

  printf("[%s] d=%u, e=%u, rv=%u\n", name, d, e, rv);

  armral::utils::bit_random random;
  auto ref_src0 = random.bit_vector(d);
  auto ref_src1 = random.bit_vector(d);
  auto ref_src2 = random.bit_vector(d);
  auto ref_dst = random.bit_vector(e);
  auto dst = ref_dst;

  turbo_rate_matching_under_test(d, e, rv, ref_src0.data(), ref_src1.data(),
                                 ref_src2.data(), dst.data());
  reference_turbo_rate_matching(d, e, rv, ref_src0.data(), ref_src1.data(),
                                ref_src2.data(), ref_dst.data());
  passed &= armral::utils::check_results_u8(name, dst.data(), ref_dst.data(),
                                            (e + 7) / 8);

  return passed;
}

template<typename TurboRateMatchingFunction>
static bool
run_all_tests(char const *name,
              TurboRateMatchingFunction turbo_rate_matching_under_test) {
  bool passed = true;
  for (auto d : valid_ds) {
    // Test 3 values of e (the number of bits forming the output of rate
    // matching), e < d, d < e < 3d, and e > 3d.
    const uint32_t e_vals[3] = {d / 2, d * 2, d * 4};
    for (auto e : e_vals) {
      for (uint32_t rv : {0, 1, 2, 3}) {
        passed &= run_turbo_rate_matching_test(name, d, e, rv,
                                               turbo_rate_matching_under_test);
      }
    }
  }
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;

  passed &= run_all_tests("TurboRateMatching", armral_turbo_rate_matching);
  passed &= run_all_tests("TurboRateMatchingNoAlloc", [](uint32_t d, uint32_t e,
                                                         uint32_t rv,
                                                         auto... args) {
    auto buffer_size = armral_turbo_rate_matching_noalloc_buffer_size(d, e, rv);
    std::vector<uint8_t> buffer(buffer_size);
    return armral_turbo_rate_matching_noalloc(d, e, rv, args..., buffer.data());
  });

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
