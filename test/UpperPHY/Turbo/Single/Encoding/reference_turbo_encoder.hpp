/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include <cstdlib>
#include <cstring>
#include <vector>

// Precomputed encoded outputs from the RSC encoder indexed by the
// current state of the internal registers (curr_state) and which bit
// of the output byte is being set (i).
static constexpr uint8_t encoded_symbol[128] = {
    0,   0,   1,   1,   1,   1,   0,   0,    // i=0
    0,   0,   2,   2,   2,   2,   0,   0,    // i=1
    0,   0,   4,   4,   4,   4,   0,   0,    // i=2
    0,   0,   8,   8,   8,   8,   0,   0,    // i=3
    0,   0,   16,  16,  16,  16,  0,   0,    // i=4
    0,   0,   32,  32,  32,  32,  0,   0,    // i=5
    0,   0,   64,  64,  64,  64,  0,   0,    // i=6
    0,   0,   128, 128, 128, 128, 0,   0,    // i=7
    1,   1,   0,   0,   0,   0,   1,   1,    // i=0
    2,   2,   0,   0,   0,   0,   2,   2,    // i=1
    4,   4,   0,   0,   0,   0,   4,   4,    // i=2
    8,   8,   0,   0,   0,   0,   8,   8,    // i=3
    16,  16,  0,   0,   0,   0,   16,  16,   // i=4
    32,  32,  0,   0,   0,   0,   32,  32,   // i=5
    64,  64,  0,   0,   0,   0,   64,  64,   // i=6
    128, 128, 0,   0,   0,   0,   128, 128}; // i=7
//   0    1    2    3    4    5    6    7    curr_state

// Precomputed state transitions of the RSC encoder indexed by the
// current state and whether the input bit is a 0 or 1.
static constexpr uint8_t new_state[16] = {0, 4, 5, 1, 2, 6, 7, 3,  // bit=0
                                          4, 0, 1, 5, 6, 2, 3, 7}; // bit=1
//                            curr_state  0  1  2  3  4  5  6  7

// Precomputed systematic outputs from the RSC encoder when computing
// the trellis termination bits. The array is indexed by the current
// trellis state (trellis_state) and which bit of the output is being
// set (i).
static constexpr uint8_t trellis_output_symbol[24] = {
    0, 1, 1, 0, 0, 1, 1, 0,  // i=0
    0, 2, 2, 0, 0, 2, 2, 0,  // i=1
    0, 4, 4, 0, 0, 4, 4, 0}; // i=2
//  0  1  2  3  4  5  6  7   trellis_state

// Precomputed encoded outputs from the RSC encoder when computing the
// trellis termination bits. The array is indexed by the current
// trellis state (trellis_state) and which bit of the output is being
// set (i).
static constexpr uint8_t trellis_encoded_symbol[24] = {
    0, 1, 0, 1, 1, 0, 1, 0,  // i=0
    0, 2, 0, 2, 2, 0, 2, 0,  // i=1
    0, 4, 0, 4, 4, 0, 4, 0}; // i=2

//  0  1  2  3  4  5  6  7   trellis_state

// implements the recursive systematic convolutional (RSC) encoder
// used in LTE turbo encoding to generate the parity bits
static inline void rsc_encode(const uint8_t *c, uint32_t k_bytes,
                              uint8_t &state, uint8_t *z) {
  // for every byte in the input data
  for (uint32_t k = 0; k < k_bytes; k++) {
    uint8_t input_block = c[k];

    for (int i = 7; i >= 0; i--) {
      uint8_t curr_bit = (input_block >> i) & 0x1;
      // use curr_bit and state of the encoder to emit z[k][i]
      uint8_t symbol = encoded_symbol[64 * curr_bit + 8 * i + state];
      z[k] |= symbol;
      // update encoder state
      state = new_state[8 * curr_bit + state];
    }
  }
}

// implements the recursive systematic convolutional (RSC) encoder
// used in LTE turbo encoding to generate the trellis termination bits
static inline void trellis_encode(uint8_t &state, uint8_t &x, uint8_t &z) {
  x = 0;
  z = 0;
  // generate 3 bits of output in x and z
  for (int i = 2; i >= 0; i--) {
    uint8_t symbol = trellis_output_symbol[8 * i + state];
    x |= symbol;
    symbol = trellis_encoded_symbol[8 * i + state];
    z |= symbol;
    // the state transitions here are:
    // old state  0 1 2 3 4 5 6 7
    // new state  0 0 1 1 2 2 3 3
    // so we just shift state right by one each time
    state >>= 1;
  }
}

// generates the trellis termination bits and stores them in the bit locations
// specified in TS 36.212 Section 5.1.3.2.2
static inline void terminate_trellis(uint8_t &state0, uint8_t &state1,
                                     uint8_t *d0, uint8_t *d1, uint8_t *d2,
                                     uint32_t k_bytes) {
  uint8_t x;
  uint8_t z;
  uint8_t x_prime;
  uint8_t z_prime;

  trellis_encode(state0, x, z);
  trellis_encode(state1, x_prime, z_prime);

  // the first 4 trellis termination bits are appended to d0 (systematic output)
  // they will be the in the 4 msbs of d0[6], leaving the other 4 to be zero
  // we need:
  //     x[0] (bit 5 (0 based) of x) -> msb of d0[k_bytes]
  //     z[1] (bit 6 of z)           -> msb-1 of d0[k_bytes]
  //    x'[0] (bit 5 of x_prime)     -> msb-2 of d0[k_bytes]
  //    z'[1] (bit 6 of z_prime)     -> msb-3 of d0[k_bytes]
  d0[k_bytes] |= (x & 0x4) << 5;
  d0[k_bytes] |= (z & 0x2) << 5;
  d0[k_bytes] |= (x_prime & 0x4) << 3;
  d0[k_bytes] |= (z_prime & 0x2) << 3;

  // append the next 4 trellis termination bits to d1:  z[0] x[2] z'[0] x'[2]
  d1[k_bytes] |= (z & 0x4) << 5;
  d1[k_bytes] |= (x & 0x1) << 6;
  d1[k_bytes] |= (z_prime & 0x4) << 3;
  d1[k_bytes] |= (x_prime & 0x1) << 4;

  // append the last 4 trellis termination bits to d2:  x[1] z[2] x'[1] z'[2]
  d2[k_bytes] |= (x & 0x2) << 6;
  d2[k_bytes] |= (z & 0x1) << 6;
  d2[k_bytes] |= (x_prime & 0x2) << 4;
  d2[k_bytes] |= (z_prime & 0x1) << 4;
}

// Generate the permuted index for given value of k using the polynomial
// scheme described in TS36.212
static inline uint32_t generate_perm_idx(uint32_t i, uint16_t f1, uint16_t f2,
                                         uint32_t k) {
  // 0 <= perm < 6144 but f2*i*i may be much larger
  return static_cast<uint32_t>((uint64_t(f1) * i + uint64_t(f2) * i * i) % k);
}

// permutes the bits of c according to the permutation stored in perm_idx:
//   c_prime[i] = c[perm_idx[i]]
static inline void interleave(const uint8_t *c, uint8_t *c_prime, uint32_t k) {
  // extract the correct values of f1 and f2 to build the
  // interleaving polynomial
  uint16_t f1 = perm_params.at(k).first;
  uint16_t f2 = perm_params.at(k).second;
  for (uint32_t i = 0; i < k; i++) {
    // 0 <= perm_idx < 6144 but f2*i*i may be much larger
    int perm_idx = generate_perm_idx(i, f1, f2, k);
    int src_byte = perm_idx >> 3;
    int src_bit = perm_idx & 7;
    int dst_byte = i >> 3;
    int dst_bit = i & 7;
    int shift = dst_bit - src_bit;
    if (shift < 0) {
      c_prime[dst_byte] |= (c[src_byte] & (128 >> src_bit)) << std::abs(shift);
    } else {
      c_prime[dst_byte] |= (c[src_byte] & (128 >> src_bit)) >> std::abs(shift);
    }
  }
}

static void reference_turbo_encode(const uint8_t *src, uint32_t k,
                                   uint8_t *dst0, uint8_t *dst1,
                                   uint8_t *dst2) {
  // the value of k in 8-bit bytes
  uint32_t k_bytes = k >> 3;

  // the internal state of the first 3-bit (8-state) RSC encoder
  // as per the spec we start with all 3 registers empty
  uint8_t state0 = 0;

  // the internal state of the second 3-bit (8-state) RSC encoder
  // as per the spec we start with all 3 registers empty
  uint8_t state1 = 0;

  // create the systematic part of the output by copying src into dst0
  memcpy(dst0, src, k_bytes);

  // zero all the output data
  for (uint32_t i = 0; i < k_bytes; i++) {
    dst1[i] = 0;
    dst2[i] = 0;
  }
  dst0[k_bytes] = 0;
  dst1[k_bytes] = 0;
  dst2[k_bytes] = 0;

  // create the interleaved version of the input data
  // to be sent as input to the second RSC encoder
  std::vector<uint8_t> src_prime(k_bytes);
  interleave(src, src_prime.data(), k);

  // encode src through the first RSC encoder
  // and store the output in dst1
  rsc_encode(src, k_bytes, state0, dst1);

  // encode src_prime through the second RSC encoder
  // and store the output in dst2
  rsc_encode(src_prime.data(), k_bytes, state1, dst2);

  // terminate the trellis by flushing both encoders to 0
  terminate_trellis(state0, state1, dst0, dst1, dst2, k_bytes);
}
