/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"

#include "../../turbo_test_data.hpp"
#include "reference_turbo_encoder.hpp"

#include <cstdio>
#include <cstdlib>
#include <vector>

// Check that the encoder returns the expected error code when
// passed an invalid value of k. We can safely pass uninitialized
// memory to the routine as the parameter test is the first thing
// it does and it will return immediately when k is invalid.
static bool run_turbo_encoding_parameter_test() {
  return armral_turbo_encode_block(NULL, 2056, NULL, NULL, NULL) ==
         ARMRAL_ARGUMENT_ERROR;
}

// Check that the encoder produces the correct encoded output
// for valid values of k.
template<typename TurboEncodeFunction>
static bool
run_turbo_encoding_test(char const *name, uint32_t k,
                        TurboEncodeFunction turbo_encode_block_under_test) {
  auto k_bytes = k >> 3;

  std::vector<uint8_t> src(k_bytes + 1, 255);
  std::vector<uint8_t> sys(k_bytes + 1, 255);
  std::vector<uint8_t> par(k_bytes + 1, 255);
  std::vector<uint8_t> itl(k_bytes + 1, 255);

  generate_turbo_test_data(src.data(), k);

  armral_status ret = turbo_encode_block_under_test(src.data(), k, sys.data(),
                                                    par.data(), itl.data());
  bool passed = true;

  if (ret != ARMRAL_SUCCESS) {
    // GCOVR_EXCL_START
    printf("Error! [%s_%u] did not return ARMRAL_SUCCESS\n", name, k);
    passed = false;
    // GCOVR_EXCL_STOP
  }

  std::vector<uint8_t> sys_ref(k_bytes + 1, 255);
  std::vector<uint8_t> par_ref(k_bytes + 1, 255);
  std::vector<uint8_t> itl_ref(k_bytes + 1, 255);

  reference_turbo_encode(src.data(), k, sys_ref.data(), par_ref.data(),
                         itl_ref.data());

  // We are checking uint8_ts here not individual bits
  // so the loop bound is (k/8)+1 because we need to check the
  // trellis termination byte too. The only valid values for
  // k in the spec are multiples of 8 so no need to worry
  // about remainders.
  for (uint32_t i = 0; i < k_bytes + 1; i++) {
    if (sys[i] != sys_ref[i]) {
      // GCOVR_EXCL_START
      printf(
          "Error! [%s_%u] result_sys[%u] = 0x%x and expected_sys[%u] = 0x%x\n",
          name, k, i, sys[i], i, sys_ref[i]);
      passed = false;
      // GCOVR_EXCL_STOP
    }
    if (par[i] != par_ref[i]) {
      // GCOVR_EXCL_START
      printf(
          "Error! [%s_%u] result_par[%u] = 0x%x and expected_par[%u] = 0x%x\n",
          name, k, i, par[i], i, par_ref[i]);
      passed = false;
      // GCOVR_EXCL_STOP
    }
    if (itl[i] != itl_ref[i]) {
      // GCOVR_EXCL_START
      printf(
          "Error! [%s_%u] result_itl[%u] = 0x%x and expected_itl[%u] = 0x%x\n",
          name, k, i, itl[i], i, itl_ref[i]);
      passed = false;
      // GCOVR_EXCL_STOP
    }
  }
  if (passed) {
    printf("[%s_%u] - check result: OK\n", name, k);
  }
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;

  // Check invalid k is detected
  passed &= run_turbo_encoding_parameter_test();

  // Check encoder encodes correctly
  for (auto k : valid_ks) {
    passed &=
        run_turbo_encoding_test("TurboEncoding", k, armral_turbo_encode_block);
  }

  for (auto k : valid_ks) {
    passed &=
        run_turbo_encoding_test("TurboEncodingNoAlloc", k, [=](auto... args) {
          std::vector<uint8_t> buffer(k / 8);
          return armral_turbo_encode_block_noalloc(args..., buffer.data());
        });
  }

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
