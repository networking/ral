/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"
#include "rate_recovery_data.hpp"

template<typename TurboRateRecoveryFunction>
static bool run_turbo_rate_recovery_test(
    char const *name,
    TurboRateRecoveryFunction turbo_rate_recovery_under_test) {
  bool passed = true;
  for (const auto &test : turbo_rate_recovery_tests) {
    printf("[%s] d=%u, e=%u, rv=%u\n", name, test.d, test.e, test.rv);

    std::vector<int8_t> dst0 = test.in0;
    std::vector<int8_t> dst1 = test.in1;
    std::vector<int8_t> dst2 = test.in2;

    turbo_rate_recovery_under_test(test.d, test.e, test.rv, test.src.data(),
                                   dst0.data(), dst1.data(), dst2.data());
    passed &= armral::utils::check_results_i8(
        "turbo rate recovery dst0", dst0.data(), test.out0.data(), test.d);
    passed &= armral::utils::check_results_i8(
        "turbo rate recovery dst1", dst1.data(), test.out1.data(), test.d);
    passed &= armral::utils::check_results_i8(
        "turbo rate recovery dst2", dst2.data(), test.out2.data(), test.d);
  }
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;

  passed &= run_turbo_rate_recovery_test("TurboRateRecovery",
                                         armral_turbo_rate_recovery);

  passed &= run_turbo_rate_recovery_test(
      "TurboRateRecoveryNoAlloc",
      [](uint32_t d, uint32_t e, uint32_t rv, auto... args) {
        auto buffer_size =
            armral_turbo_rate_recovery_noalloc_buffer_size(d, e, rv);
        std::vector<uint8_t> buffer(buffer_size);
        return armral_turbo_rate_recovery_noalloc(d, e, rv, args...,
                                                  buffer.data());
      });

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
