/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include "armral.h"
#include "int_utils.hpp"
#include "utils/bits_to_bytes.hpp"
#include <vector>

static void reference_tail_biting_convolutional_encode_block(const uint8_t *src,
                                                             uint32_t k,
                                                             uint8_t *dst0,
                                                             uint8_t *dst1,
                                                             uint8_t *dst2) {
  // Cast the bits of the input stream to bytes
  std::vector<uint8_t> bytes_src(k);
  armral::bits_to_bytes(k, src, bytes_src.data());

  // Initialize the shift register with the last six
  // information bits of the input stream (s_i = src_(k-1-i))
  uint8_t s[6];
  s[0] = bytes_src[k - 1];
  s[1] = bytes_src[k - 2];
  s[2] = bytes_src[k - 3];
  s[3] = bytes_src[k - 4];
  s[4] = bytes_src[k - 5];
  s[5] = bytes_src[k - 6];

  std::vector<uint8_t> bytes_dst0(k);
  std::vector<uint8_t> bytes_dst1(k);
  std::vector<uint8_t> bytes_dst2(k);

  for (uint32_t i = 0; i < k; i++) {
    // Compute output streams
    bytes_dst0[i] = (bytes_src[i] + s[1] + s[2] + s[4] + s[5]) % 2;
    bytes_dst1[i] = (bytes_src[i] + s[0] + s[1] + s[2] + s[5]) % 2;
    bytes_dst2[i] = (bytes_src[i] + s[0] + s[1] + s[3] + s[5]) % 2;

    // Update shift register
    s[5] = s[4];
    s[4] = s[3];
    s[3] = s[2];
    s[2] = s[1];
    s[1] = s[0];
    s[0] = bytes_src[i];
  }

  // Convert the bytes back to bits
  armral::bytes_to_bits(k, bytes_dst0.data(), dst0);
  armral::bytes_to_bits(k, bytes_dst1.data(), dst1);
  armral::bytes_to_bits(k, bytes_dst2.data(), dst2);
}

static bool run_convolutional_encoding_test(int k) {
  assert(k >= 8);
  const auto src = armral::utils::allocate_random_u8((k + 7) / 8);

  std::vector<uint8_t> dst0_ref((k + 7) / 8);
  std::vector<uint8_t> dst1_ref((k + 7) / 8);
  std::vector<uint8_t> dst2_ref((k + 7) / 8);
  reference_tail_biting_convolutional_encode_block(
      (const uint8_t *)src.data(), k, dst0_ref.data(), dst1_ref.data(),
      dst2_ref.data());

  std::vector<uint8_t> dst0((k + 7) / 8);
  std::vector<uint8_t> dst1((k + 7) / 8);
  std::vector<uint8_t> dst2((k + 7) / 8);
  armral_status ret = armral_tail_biting_convolutional_encode_block(
      (const uint8_t *)src.data(), k, dst0.data(), dst1.data(), dst2.data());

  bool passed = true;
  if (ret != ARMRAL_SUCCESS) {
    // GCOVR_EXCL_START
    const char *name = "Convolutional_Encoding";
    printf("Error! [%s_%d] did not return ARMRAL_SUCCESS\n", name, k);
    passed = false;
    // GCOVR_EXCL_STOP
  } else {
    auto check_dst0 = armral::utils::check_results_u8(
        "CONVOLUTIONAL ENCODING (STREAM D0)", dst0.data(), dst0_ref.data(),
        (k + 7) / 8);
    auto check_dst1 = armral::utils::check_results_u8(
        "CONVOLUTIONAL ENCODING (STREAM D1)", dst1.data(), dst1_ref.data(),
        (k + 7) / 8);
    auto check_dst2 = armral::utils::check_results_u8(
        "CONVOLUTIONAL ENCODING (STREAM D2)", dst2.data(), dst2_ref.data(),
        (k + 7) / 8);
    passed = check_dst0 && check_dst1 && check_dst2;
  }

  return passed;
}

// Entry point for unit testing of convolutional coding
int main(int argc, char **argv) {
  bool passed = true;
  for (int k : {8, 16, 32, 64, 128, 199, 200, 201, 256, 512, 1024}) {
    passed &= run_convolutional_encoding_test(k);
  }
  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
