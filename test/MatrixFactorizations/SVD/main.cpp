/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#include "reference_linalg.hpp"
#include "svd_sample_data.h"
#include "svd_test.hpp"

using armral::utils::convert_cf32_array_to_vector;

namespace {

// Check the accuracy of our implementation
// using sample data instead of randomly generated
// matrices. We use singular values computed
// using Octave as the "exact" solution. The
// accuracy of the full decomposition is also
// evaluated.
template<typename SVDFunction>
bool test_svd_with_sample(SVDFunction svd_function_under_test) {

  bool passed = true;
  // Loop through the test matrices
  for (const auto &test : svd_sample_tests) {
    int n = test.n;
    int m = test.m;
    int size = m * n;
    std::vector<armral_cmplx_f32_t> a = test.a;
    std::vector<float32_t> s(n);

    // Left and right singular vectors.
    std::vector<armral_cmplx_f32_t> u(size);
    std::vector<armral_cmplx_f32_t> vt(n * n);

    // SVD decomposition
    bool gen_singular_vectors = true;
    svd_function_under_test(gen_singular_vectors, m, n, a.data(), s.data(),
                            u.data(), vt.data());

    // Check the accuracy of the singular
    // values computed.
    passed &= check_singular_values(n, test.s, s);

    // Convert data to complex<float32_t> for testing
    auto aref_cmplx =
        convert_cf32_array_to_vector<float32_t>(size, test.a.data());
    auto u_cmplx = convert_cf32_array_to_vector<float32_t>(size, u.data());
    auto vt_cmplx = convert_cf32_array_to_vector<float32_t>(n * n, vt.data());

    // Check the accuracy of the full decomposition
    passed &= check_svd_decomposition(m, n, aref_cmplx, s, u_cmplx, vt_cmplx);
    // Check the orthogonality of the left
    // singular vectors updated.
    passed &= check_orthogonality(m, n, u_cmplx);
    // Check the orthogonality of the right
    // singular vectors updated.
    passed &= check_orthogonality(n, n, vt_cmplx);
  }
  return passed;
}

template<typename SVDFunction>
bool test_svd(bool gen_singular_vectors, int m, int n, float cond,
              SVDFunction svd_function_under_test) {
  int size = m * n;
  // Generate test matrix with prescribed
  // singular values and condition number
  std::vector<armral_cmplx_f32_t> a(size);
  std::vector<float32_t> s(n);
  std::vector<float32_t> sref(n);
  generate_svd_matrix(m, n, a, sref, cond);

  // Make copy of A.
  std::vector<armral_cmplx_f32_t> aref = a;

  // Left and right singular vectors.
  std::vector<armral_cmplx_f32_t> u;
  std::vector<armral_cmplx_f32_t> vt;
  if (gen_singular_vectors) {
    u.resize(size);
    vt.resize(n * n);
  }
  // SVD decomposition.
  svd_function_under_test(gen_singular_vectors, m, n, a.data(), s.data(),
                          u.data(), vt.data());

  // Check the accuracy of the singular
  // values computed.
  bool passed = check_singular_values(n, sref, s);

  if (gen_singular_vectors) {
    // Convert data to complex<float32_t> for testing
    auto aref_cmplx =
        convert_cf32_array_to_vector<float32_t>(size, aref.data());
    auto u_cmplx = convert_cf32_array_to_vector<float32_t>(size, u.data());
    auto vt_cmplx = convert_cf32_array_to_vector<float32_t>(n * n, vt.data());

    // Check the accuracy of the full decomposition
    passed &= check_svd_decomposition(m, n, aref_cmplx, s, u_cmplx, vt_cmplx);

    // Check the orthogonality of the left
    // singular vectors updated.
    passed &= check_orthogonality(m, n, u_cmplx);

    // Check the orthogonality of the right
    // singular vectors updated.
    passed &= check_orthogonality(n, n, vt_cmplx);
  }

  return passed;
}
} // anonymous namespace

template<typename SVDFunction>
bool run_all_tests(char const *name, SVDFunction svd_function) {
  bool passed = true;
  // Test using sample matrices
  passed &= test_svd_with_sample(svd_function);
  // Test using random matrices
  std::vector<int> nb_row = {32, 50, 64, 128};
  std::vector<int> nb_col = {4, 8, 16, 20, 28, 32};
  std::vector<bool> check_full_decomposition = {true, false};
  std::vector<float32_t> cond_number{4, 32, 100, 100, 1000, 10000};
  for (auto m : nb_row) {
    for (auto n : nb_col) {
      for (auto cond : cond_number) {
        for (auto check_full_svd : check_full_decomposition) {
          passed &= test_svd(check_full_svd, m, n, cond, svd_function);
        }
      }
    }
  }

  if (!passed) {
    // GCOVR_EXCL_START
    printf("[%s] one or more tests failed!\n", name);
    // GCOVR_EXCL_STOP
  }
  return passed;
}

int main(int argc, char **argv) {
  bool passed = true;

  passed &= run_all_tests("SVD", armral_svd_cf32);

  passed &= run_all_tests(
      "SVD_NOALLOC", [](bool gen_singular_vect, int m, int n, auto... args) {
        auto buffer_size =
            armral_svd_cf32_noalloc_buffer_size(gen_singular_vect, m, n);

        std::vector<uint8_t> buffer(buffer_size);
        return armral_svd_cf32_noalloc(gen_singular_vect, m, n, args...,
                                       buffer.data());
      });

  exit(passed ? EXIT_SUCCESS : EXIT_FAILURE);
}
