/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include <cmath>
#include <complex>
#include <cstdlib>
#include <cstring>
#include <limits>
#include <tuple>
#include <vector>

#include "MatrixFactorizations/SVD/matrix_view.hpp"
#include "cf32_utils.hpp"
#include "reference_linalg.hpp"

// In the accuracy tests, a computed solution
// is acceptable if the relative error is less
// than 10x the unit of roundoff or machine precision.
#define THRESHOLD 10

// SAFEMIN is the smallest floating point
// number such as 1/SAFEMIN does not overflow.
// It is used to check and guard against overflow
// in division by a small floating point number.
#define SAFEMIN 1.17549E-38

typedef std::complex<float32_t> cf32_t;

using armral::utils::cf32_random;
using armral::utils::convert_cf32_array_to_vector;

// Generate m-by-n, single complex random matrix
static inline std::vector<cf32_t> generate_rand(cf32_random &random,
                                                const int m, const int n) {
  int size = m * n;
  return convert_cf32_array_to_vector<float32_t>(size,
                                                 random.vector(size).data());
}

static inline float32_t infinity_norm(int m, int n, const cf32_t *a) {
  column_major_matrix_view a_mat{a, static_cast<uint32_t>(m)};
  float32_t inorm = 0;
  for (int i = 0; i < m; i++) {
    float32_t tmp = 0;
    for (int j = 0; j < n; j++) {
      tmp += std::abs(a_mat(i, j));
    }
    if (tmp > inorm) {
      inorm = tmp;
    }
  }
  return inorm;
}

// Overload infinity_norm with an interface
// with  std::vector data type as input
static inline float32_t infinity_norm(int m, int n,
                                      const std::vector<cf32_t> &a) {
  return infinity_norm(m, n, a.data());
}

// clarfg computes the householder reflectors of
// a given vector to annihilate all the entries
// except the first. The second entry to the
// last are overwritten by the reflectors.
static inline cf32_t clarfg(const int n, cf32_t &aii, cf32_t *x,
                            const int incx) {

  cf32_t alpha = aii;
  // Sum of x[i] * conj(x[i])
  float32_t sum = 0.0F;
  for (int i = 0; i < n * incx; i += incx) {
    sum += real(x[i] * conj(x[i]));
  }

  // check if the elts to annihilate are all zero
  if (sum == 0 && imag(alpha) == 0) {
    return 0.0F;
  }
  // Add alpha * conj(alpha) to sum
  // to compute the 2 norm of the full vector
  sum += real(alpha * conj(alpha));
  float32_t beta = -copysign(sqrt(sum), real(alpha));
  float32_t safmin = SAFEMIN / std::numeric_limits<float32_t>::epsilon();
  float32_t rsafmin = 1.0F / safmin;
  int cnt = 0;
  int max_attempt = 10;
  float32_t scale = 1.0F;
  // Check if beta is small enough to induce
  // overflow when taking the inverse, and
  // if it is the case, scale to avoid overflow
  while ((std::abs(beta) < safmin) && (cnt < max_attempt)) {
    beta *= rsafmin;
    scale *= rsafmin;
    cnt++;
  }
  alpha *= scale;
  for (int i = 0; i < n * incx; i += incx) {
    x[i] *= scale;
  }
  // The new beta is at most 1, at least safmin,
  sum = real(alpha * conj(alpha));
  for (int i = 0; i < n * incx; i += incx) {
    sum += real(x[i] * conj(x[i]));
  }
  beta = -copysign(sqrt(sum), real(alpha));

  // Compute tau and update aii
  cf32_t tau = (beta - alpha) / beta;
  cf32_t normalization_factor = 1.0F / (alpha - beta);
  for (int i = 0; i < n * incx; i += incx) {
    x[i] = normalization_factor * x[i];
  }
  beta /= scale;
  aii = beta;
  return tau;
}

// householder_qr computes the QR factorization A = QR.
// On exit, the elements on and above the diagonal
// of the A contain the upper triangular matrix R.
// The elements below the diagonal, with the array tau,
// represent the orthogonal matrx Q. Note that Q is not
// stored explicitly, but can be formed using
// the get_q routine, or applied, using apply_q.
static inline void householder_qr(const int m, const int n, cf32_t *a,
                                  std::vector<cf32_t> &tau) {

  if (m < n) {
    // GCOVR_EXCL_START
    printf("Invalid argument error: m < n not supported\n");
    return;
    // GCOVR_EXCL_STOP
  }
  column_major_matrix_view a_mat{a, static_cast<uint32_t>(m)};
  for (int i = 0; i < n; i++) {
    int k = std::min(i + 1, m - 1);
    tau[i] =
        clarfg(m - i - 1, a_mat(i, i), &a_mat(k, i), a_mat.row_increment());
    cf32_t tmp = a_mat(i, i);
    a_mat(i, i) = 1.0F;
    if (i < n - 1) {
      for (int col = i + 1; col < n; col++) {
        cf32_t w = 0;
        for (int row = i; row < m; row++) {
          w += a_mat(row, col) * conj(a_mat(row, i));
        }
        cf32_t tmp2 = conj(tau[i]) * w;
        for (int row = i; row < m; row++) {
          a_mat(row, col) -= a_mat(row, i) * tmp2;
        }
      }
    }
    a_mat(i, i) = tmp;
  }
}

// Apply implicitly Q to an input matrix C of the same dimension
// as the matrix A that has been factorized into QR or bidiagonalization.
static inline void apply_q(int m, int n, const cf32_t *a, const cf32_t *tau,
                           cf32_t *c) {
  if (m < n) {
    // GCOVR_EXCL_START
    printf("Invalid argument error: m < n not supported\n");
    return;
    // GCOVR_EXCL_STOP
  }
  std::vector<cf32_t> q(m * n);
  memcpy(q.data(), a, m * n * sizeof(cf32_t));
  column_major_matrix_view q_mat{q.data(), static_cast<uint32_t>(m)};
  column_major_matrix_view c_mat{c, static_cast<uint32_t>(m)};
  for (int i = n - 1; i >= 0; i--) {
    q_mat(i, i) = 1.0F;
    // Apply reflector from the left to all columns
    //  of C from row index i, to the end.
    for (int col = 0; col < n; col++) {
      cf32_t w = 0;
      for (int row = i; row < m; row++) {
        w += c_mat(row, col) * conj(q_mat(row, i));
      }
      cf32_t tmp = tau[i] * w;
      for (int row = i; row < m; row++) {
        c_mat(row, col) -= q_mat(row, i) * tmp;
      }
    }
  }
}

// Overload apply_q with an interface
// with std::vector  data type as input
static inline void apply_q(int m, int n, const std::vector<cf32_t> &a,
                           const std::vector<cf32_t> &tau,
                           std::vector<cf32_t> &c) {

  apply_q(m, n, a.data(), tau.data(), c.data());
}

// Generate explicitly Q from QR factorization or from
// the bidiagonalization A = Q * B * P^H
static inline std::vector<cf32_t> get_q(const int m, const int n,
                                        const std::vector<cf32_t> &a,
                                        const std::vector<cf32_t> &tau) {

  if (m < n) {
    // GCOVR_EXCL_START
    printf("Invalid argument error: m < n not supported\n");
    return a;
    // GCOVR_EXCL_STOP
  }
  std::vector<cf32_t> q = a;
  column_major_matrix_view q_mat{q.data(), static_cast<uint32_t>(m)};
  // Accumulate reflectors from right to left
  // Q = H1 * H2....* Hn. They are applied to identity.
  for (int i = n - 1; i >= 0; i--) {
    if (i < n - 1) {
      q_mat(i, i) = 1.0F;
      // Apply reflector from the left
      for (int col = i + 1; col < n; col++) {
        cf32_t w = 0;
        for (int row = i; row < m; row++) {
          w += q_mat(row, col) * conj(q_mat(row, i));
        }
        cf32_t tmp = tau[i] * w;
        for (int row = i; row < m; row++) {
          q_mat(row, col) -= q_mat(row, i) * tmp;
        }
      }
    }
    if (i < m - 1) {
      // Scale entries i+1 to m-1 of the i-th column
      for (int r = i + 1; r < m; r++) {
        q_mat(r, i) *= -tau[i];
      }
    }
    q_mat(i, i) = 1.0F - tau[i];
    // Set the entries 0 to i-1 of the i-th column to zero
    for (int r = 0; r <= i - 1; r++) {
      q_mat(r, i) = 0.0F;
    }
  }
  return q;
}

// Generate the orthogonal matrix P from
// the bidiagonalization  A = Q * B * P^H,
// note that P^H is generated directly
// instead of P
static inline void get_p(int m, int n, const cf32_t *a, const cf32_t *tau,
                         cf32_t *p) {

  column_major_matrix_view a_mat{a, static_cast<uint32_t>(m)};
  column_major_matrix_view p_mat{p, static_cast<uint32_t>(n)};

  // Make a copy of reflectors.
  // P and A are not of same dimension
  for (int i = 0; i < n; i++) {
    for (int j = i; j < n; j++) {
      p_mat(i, j) = a_mat(i, j);
    }
  }

  // Shift the vectors which define the elementary reflectors one
  // row downward, and set the first row and column of P**H to
  // those of the unit matrix
  p[0] = 1.0F; // p[0][0] = 1
  for (int j = 1; j < n; j++) {
    p_mat(j, 0) = 0.0F; // set first column to zero
    for (int i = j; i > 0; i--) {
      p_mat(i, j) = p_mat(i - 1, j);
    }
    p_mat(0, j) = 0.0F; // set first row to zero
  }
  // Work on shifted matrix with reflector
  // just above the diagonal to fall back a
  // case similar to QR
  int n1 = n - 1;
  // This shift is the same in row or column major
  cf32_t *p1 = p + p_mat.stride() + 1;
  column_major_matrix_view p1_mat{p1, static_cast<uint32_t>(n)};

  // Apply householder reflectors from the right
  for (int i = n1 - 1; i >= 0; i--) {
    if (i < n1 - 1) {
      p1_mat(i, i) = 1.0F;
      for (int row = i + 1; row < n1; row++) {
        cf32_t w = 0;
        for (int col = i; col < n1; col++) {
          w += p1_mat(row, col) * conj(p1_mat(i, col));
        }
        cf32_t tmp = conj(tau[i]) * w;
        for (int col = i; col < n1; col++) {
          p1_mat(row, col) -= p1_mat(i, col) * tmp;
        }
      }
      // Scale
      for (int col = i + 1; col < n1; col++) {
        p1_mat(i, col) *= -conj(tau[i]);
      }
    }
    p1_mat(i, i) = 1.0F - conj(tau[i]);
    // Set entries  0 to i-1 of the i-th row to zero
    for (int col = 0; col < i; col++) {
      p1_mat(i, col) = 0.0F;
    }
  }
}

// Overload get_p with an interface
// with std::vector data type as inputs
static inline void get_p(int m, int n, const std::vector<cf32_t> &a,
                         const std::vector<cf32_t> &tau,
                         std::vector<cf32_t> &p) {

  get_p(m, n, a.data(), tau.data(), p.data());
}

// Generate a rectangular matrix with prescribed
// singular values and condition number.
// This routine first sets the singular values in
// the array S, then generates two orthogonal matrices
// Q1 and Q2 using QR factorization, and form the
// final matrix as Q 1* S * Q2.
static inline void generate_svd_matrix(const int m, const int n,
                                       std::vector<armral_cmplx_f32_t> &a,
                                       std::vector<float32_t> &s,
                                       const float32_t cond) {

  // Generate singular values from 1 to 1/cond
  // where cond is the condition number of the matrix
  for (int i = 0; i < n; i++) {
    float32_t rcond = (1 - 1 / cond);
    s[i] = 1 - (float32_t)i / (n - 1) * rcond;
  }

  cf32_random random;
  std::vector<cf32_t> a1 = generate_rand(random, m, n);
  std::vector<cf32_t> a2 = generate_rand(random, n, n);

  // Perform QR of A1
  std::vector<cf32_t> tau1(n);
  householder_qr(m, n, a1.data(), tau1);

  // Perform QR of A2
  std::vector<cf32_t> tau2(n);
  householder_qr(n, n, a2.data(), tau2);

  // Generate Q2
  std::vector<cf32_t> q2 = get_q(n, n, a2, tau2);

  // multiply left orthogonal matrix by S
  column_major_matrix_view q2_mat{q2.data(), static_cast<uint32_t>(n)};
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      q2_mat(i, j) *= s[i];
    }
  }
  // Apply Q1 to S*Q2, but first copy Q2 in an m * n matrix
  std::vector<cf32_t> a_cmplx(m * n);
  column_major_matrix_view q2_mat_mn{a_cmplx.data(), static_cast<uint32_t>(m)};
  for (int i = 0; i < n; i++) {
    for (int j = 0; j < n; j++) {
      q2_mat_mn(i, j) = q2_mat(i, j);
    }
  }
  apply_q(m, n, a1, tau1, a_cmplx);

  // Convert vector<complex<float32_t>> to vector<armral_cmplx_f32_t>
  for (int i = 0; i < m * n; ++i) {
    a[i] = {real(a_cmplx[i]), imag(a_cmplx[i])};
  }
}

// This routine reduces a general complex m-by-n matrix A
// to upper bidiagonal form B by a unitary transformation:
// Q**H * A * P = B. On exit, the diagonal and the first
// superdiagonal are overwritten with the upper bidiagonal
// matrix B. The elements below the diagonal, with
// the array tauq, represent the unitary matrix Q as a
// product of elementary reflectors, and the elements
// above the first superdiagonal, with the array taup,
// represent the unitary matrix P as a product of
// elementary reflectors. On exit, D contains the
// diagonal elements of the bidiagonal matrix B,
// and E contains the off-diagonal elements of
// the bidiagonal matrix B. Note that this routine
// returns directly the conjugate transpose of the
// left orthogonal matrix.
static inline void bidiagonalization(const int m, const int n, cf32_t *a,
                                     std::vector<float32_t> &d,
                                     std::vector<float32_t> &e,
                                     std::vector<cf32_t> &tauq,
                                     std::vector<cf32_t> &taup) {

  if (m < n) {
    // GCOVR_EXCL_START
    printf("Invalid argument error: m < n not supported\n");
    return;
    // GCOVR_EXCL_STOP
  }
  column_major_matrix_view a_mat{a, static_cast<uint32_t>(m)};
  for (int i = 0; i < n; i++) {
    // QR steps, generate elementary reflector H(i) to annihilate
    // the entries i+1 to the last of the i-th column
    int k = std::min(i + 1, m - 1);
    tauq[i] =
        clarfg(m - i - 1, a_mat(i, i), &a_mat(k, i), a_mat.row_increment());
    d[i] = real(a_mat(i, i));
    a_mat(i, i) = 1.0F;
    // Apply householder reflectors from left
    if (i < n - 1) {
      for (int col = i + 1; col < n; col++) {
        cf32_t w = 0;
        for (int row = i; row < m; row++) {
          w += a_mat(row, col) * conj(a_mat(row, i));
        }
        cf32_t tmp = conj(tauq[i]) * w;
        for (int row = i; row < m; row++) {
          a_mat(row, col) -= a_mat(row, i) * tmp;
        }
      }
    }
    a_mat(i, i) = d[i];

    // LQ steps
    if (i == n - 1) {
      taup[i] = 0.0F;
      continue;
    }

    // Transpose conjugate entries i+1 to the last
    // of the i-th row
    for (int col = i + 1; col < n; col++) {
      a_mat(i, col) = conj(a_mat(i, col));
    }

    // Generate reflectors to annihilate the entries i+2
    // to the last of the i-th row.
    int j = std::min(i + 2, n - 1);
    taup[i] = clarfg(n - i - 2, a_mat(i, i + 1), &a_mat(i, j),
                     a_mat.column_increment());
    e[i] = real(a_mat(i, i + 1));
    a_mat(i, i + 1) = 1.0F;
    // Apply the reflectors
    for (int row = i + 1; row < m; row++) {
      cf32_t w = 0;
      for (int col = i + 1; col < n; col++) {
        w += a_mat(row, col) * a_mat(i, col);
      }
      cf32_t tmp = taup[i] * w;
      for (int col = i + 1; col < n; col++) {
        a_mat(row, col) -= conj(a_mat(i, col)) * tmp;
      }
    }
    // Conjugate transpose the reflectors
    for (int col = i + 1; col < n; col++) {
      a_mat(i, col) = conj(a_mat(i, col));
    }
    a_mat(i, i + 1) = e[i];
  }
}

// Computation of Givens rotation components.
inline static std::tuple<float32_t, float32_t, float32_t>
rotg(const float32_t f, const float32_t g) {
  if (f == 0) {
    float32_t cs = 0.0F;
    float32_t sn = 1.0F;
    return std::make_tuple(cs, sn, g);
  }
  if (std::abs(f) > std::abs(g)) {
    float32_t t = g / f;
    float32_t tt = sqrt(1 + t * t);
    float32_t cs = 1 / tt;
    float32_t sn = t / tt;
    return std::make_tuple(cs, sn, f * tt);
  }
  float32_t t = f / g;
  float32_t tt = sqrt(1 + t * t);
  float32_t sn = 1 / tt;
  float32_t cs = t / tt;
  return std::make_tuple(cs, sn, g * tt);
}

// This routine updates singular vectors
// by applying the Givens rotations
// used to update the bidiagonal matrix
inline static void update_sigvect(const int m, const float32_t cs,
                                  const float32_t sn, cf32_t *v1, cf32_t *v2,
                                  const int incv) {
  for (int i = 0; i < m * incv; i += incv) {
    cf32_t t = v1[i];
    v1[i] = cs * t + sn * v2[i];
    v2[i] = -sn * t + cs * v2[i];
  }
}

// svd_bidiagonal computes the singular values and,
// optionally, the right and left singular vectors
// from the SVD of a real n-by-n upper bidiagonal
//  matrix B using the Wilkinson shift QR algorithm.
// The SVD of B has the form  B = Q * S * P**H.
//  On entry, D and E form the bidiagonal matrix, and
//  on exit, D contains the singular values. The
//  left and right singular vectors are updated if required.
//  This algorithm is developed by G. H. Golub  and C. Reinsch.
//  For more detail, the algorithm is well explained in
// "Singular Value Decomposition and Least Squares Solutions"
//  published in Numer. Math. 14, 403--420 (1970).
inline static int svd_bidiagonal(const bool gen_singular_vectors, const int m,
                                 const int n, std::vector<float32_t> &d,
                                 std::vector<float32_t> &e, cf32_t *u,
                                 cf32_t *vt, const int u_stride) {

  if (m < n) {
    // GCOVR_EXCL_START
    printf("Invalid argument error: m < n not supported\n");
    return -1;
    // GCOVR_EXCL_STOP
  }
  // Shift the off-diagonal elements down by 1
  // This helps to have D[i] and E[i] as the i-th
  // column of the bidiagonal matrix B.
  for (int i = n - 1; i > 0; i--) {
    e[i] = e[i - 1];
  }
  e[0] = 0.0F;

  // Compute the 1-norm of the bidiagonal matrix
  // for the computation of the stopping criteria.
  float32_t anorm = 0;
  for (int i = 0; i < n; i++) {
    float32_t tmp = std::abs(d[i]) + std::abs(e[i]);
    if (anorm < tmp) {
      anorm = tmp;
    }
  }
  float32_t tol = THRESHOLD * anorm * std::numeric_limits<float32_t>::epsilon();

  int maxiter = 2 * n;
  // Loop over the columns
  column_major_matrix_view u_mat{u, static_cast<uint32_t>(u_stride)};
  column_major_matrix_view vt_mat{vt, static_cast<uint32_t>(n)};
  for (int curr_col = n - 1; curr_col >= 0; curr_col--) {
    // iteration to annihilate the off-diagonal E[curr_col].
    int iter = 0;
    for (; iter <= maxiter; iter++) {
      bool diag_is_zero = false;
      int next_col;

      // Check if an off-diagonal is zero.
      for (next_col = curr_col; next_col >= 0; next_col--) {
        if (std::abs(e[next_col]) < tol) {
          break;
        }
        // Check if a diagonal is zero.
        if (std::abs(d[next_col - 1]) < tol) {
          diag_is_zero = true;
          break;
        }
      }
      // If the diagonal D[next_col] = 0; then at least one  singular
      // value must be equal to zero. In the absence of roundoff error,
      // the matrix will break if a shift of zero is performed.
      // In this case, an extra sequence of Givens rotations is
      // applied from the left to annihilate the off-diagonal E[next_col].
      if (diag_is_zero) {
        float32_t cs = 0.0;
        float32_t sn = 1.0;
        for (int i = next_col; i < curr_col; i++) {
          float32_t f = sn * e[i];
          e[i] *= cs;
          if (std::abs(f) <= tol) {
            break;
          }
          float32_t g = d[i];
          float32_t h;
          std::tie(cs, sn, h) = rotg(f, g);
          d[i] = h;
          // Update left singular vectors.
          if (gen_singular_vectors) {
            update_sigvect(m, cs, -sn, &u_mat(0, next_col - 1), &u_mat(0, i),
                           u_mat.row_increment());
          }
        }
      }
      float32_t z = d[curr_col];
      if (next_col == curr_col) {
        // Make singular value nonnegative and update
        // the corresponding right singular vectors.
        if (z < 0.0) {
          d[curr_col] = -z;
          if (gen_singular_vectors) {
            // For the sake of performance we copy data that is contiguous in
            // memory
            for (int row = 0; row < m; row++) {
              u_mat(row, curr_col) = -u_mat(row, curr_col);
            }
          }
        }
        break;
      }
      if (iter == maxiter) {
        printf("Max iteration (%d) exceeded without convergence\n", maxiter);
        return -1;
      }

      // Shifted QR iteration with wilkinson shift
      // that is determined by the eigenvalue of the bottom
      // right 2 × 2 block of the unreduced matrix.
      // As the algorithm works on a bidiagonal matrix,
      // the entries of the corresponding symmetric
      // triadigonal matrix B^H * B of interest are computed implicitly.
      // Note that for a 2-by-2 symmetric tridiagonal matrix
      // with d1 and d2 the diagonals and e1 the off diagonal,
      // the 2 eigenvalues are (d1 + d2)/2 +/- sqrt(((d1 - d2)/2)^2 + e1^2).
      // The choice of this shift accelerates the convergence of the
      // most bottom off-diagonal E[curr_col] to zero.
      float32_t x = d[next_col];
      float32_t y = d[curr_col - 1];
      float32_t g = e[curr_col - 1];
      float32_t h = e[curr_col];
      // a^2 - b^2 operations are computed as
      // (a - b)* (a + b) to avoid overflow.
      float32_t f = ((y - z) * (y + z) + (g - h) * (g + h)) / (2.0 * h * y);
      g = sqrt(f * f + 1);
      f = ((x - z) * (x + z) + h * (y / (f + copysign(g, f)) - h)) / x;

      // Shifted QR iteration, bulge chasing, applying
      // successive Givens rotations from right then from left.
      float32_t c = 1.0F;
      float32_t s = 1.0F;
      for (int i = next_col + 1; i <= curr_col; i++) {
        g = e[i];
        y = d[i];
        h = s * g;
        g = c * g;
        // Apply Givens rotation from right.
        std::tie(c, s, z) = rotg(f, h);
        f = x * c + g * s;
        g = g * c - x * s;
        h = y * s;
        y = y * c;
        e[i - 1] = z;
        // Update right singular vector.
        if (gen_singular_vectors) {
          update_sigvect(n, c, s, &vt_mat(i - 1, 0), &vt_mat(i, 0),
                         vt_mat.column_increment());
        }
        // Apply Givens rotation from left.
        std::tie(c, s, z) = rotg(f, h);
        d[i - 1] = z;
        f = (c * g) + (s * y);
        x = (c * y) - (s * g);
        // Update left singular vectors.
        if (gen_singular_vectors) {
          update_sigvect(m, c, s, &u_mat(0, i - 1), &u_mat(0, i),
                         u_mat.row_increment());
        }
      }
      e[next_col] = 0.0;
      e[curr_col] = f;
      d[curr_col] = x;
    }
  }
  // Sort the singular values in decreasing order
  // and the singular vectors if required.
  for (int i = 0; i < n - 1; i++) {
    int max_pos = i;
    for (int j = i + 1; j < n; j++) {
      if (d[j] > d[max_pos]) {
        max_pos = j;
      }
    }
    if (max_pos != i) {
      std::swap(d[i], d[max_pos]);
      if (gen_singular_vectors) {
        // Swap corresponding columns in left singular vectors.
        for (int row = 0; row < m; row++) {
          std::swap(u_mat(row, i), u_mat(row, max_pos));
        }
        // Swap corresponding rows in right singular vectors.
        for (int col = 0; col < n; col++) {
          std::swap(vt_mat(i, col), vt_mat(max_pos, col));
        }
      }
    }
  }
  return 0;
}

// armral_svd computes the SVD decomposition
// of an m-by-n matrix A by first performing
// the bidigonalization of A, then computing
// the SVD of the bidiagonal matrix and update
// the singular vectors if required.
static inline int svd_cf32(bool gen_singular_vect, const int m, const int n,
                           std::vector<cf32_t> &a, std::vector<float32_t> &s,
                           std::vector<cf32_t> &u, std::vector<cf32_t> &vt) {

  // Bidiagonalization: A = Q * B * P^H.
  std::vector<cf32_t> tauq(n);
  std::vector<cf32_t> taup(n);
  std::vector<float32_t> e(n);
  bidiagonalization(m, n, a.data(), s, e, tauq, taup);

  // Generate left and right orthogonal vectors if required.
  if (gen_singular_vect) {
    // Generate Q and store it in u.
    u = get_q(m, n, a, tauq);
    // Generate P and store it in vt.
    get_p(m, n, a, taup, vt);
  }
  // Compute the singular values and singular vectors
  // if required.
  svd_bidiagonal(gen_singular_vect, m, n, s, e, u.data(), vt.data(), m);

  return 0;
}

// Check ||Id - Q^H * Q||_∞/n < THRESHOLD * epsilon
static inline bool check_orthogonality(const int m, const int n, cf32_t *q) {

  float32_t tol = THRESHOLD * n * std::numeric_limits<float32_t>::epsilon();

  // Build an identity matrix Id
  std::vector<cf32_t> a(n * n);
  column_major_matrix_view a_mat{a.data(), static_cast<uint32_t>(n)};
  for (int i = 0; i < n; i++) {
    a_mat(i, i) = 1.0F;
  }
  // Perform Id - Q^H * Q
  column_major_matrix_view q_mat{q, static_cast<uint32_t>(m)};
  for (int j = 0; j < n; j++) {
    for (int i = 0; i < n; i++) {
      for (int k = 0; k < m; k++) {
        a_mat(i, j) -= conj(q_mat(k, i)) * q_mat(k, j);
      }
    }
  }
  // Compute the infinity norm ||Id - Q^H * Q||_∞
  float32_t inorm = infinity_norm(n, n, a);
  return inorm < tol;
}

// Overload check_orthogonality
// with an interface taking std::vector
// data type input
static inline bool check_orthogonality(int m, int n, std::vector<cf32_t> &q) {
  return check_orthogonality(m, n, q.data());
}

// Check || A - Q * R||_∞/ || A ||_∞ < tol
// A contain the factors of QR factorization
// and Aref is the initial matrix
static inline bool check_qr_decomposition(int m, int n, const cf32_t *aref,
                                          const cf32_t *a, const cf32_t *tau) {
  column_major_matrix_view a_mat{a, static_cast<uint32_t>(m)};

  // Infinity norm of Aref
  float32_t anorm = infinity_norm(m, n, aref);
  anorm = anorm > 1 ? anorm : 1;
  float32_t tol =
      THRESHOLD * anorm * m * std::numeric_limits<float32_t>::epsilon();

  // Extract R, allocate m-by-n memory for
  // the multiplication by A later
  std::vector<cf32_t> r(m * n);
  column_major_matrix_view r_mat{r.data(), static_cast<uint32_t>(m)};
  for (int i = 0; i < n; i++) {
    for (int j = i; j < n; j++) {
      r_mat(i, j) = a_mat(i, j);
    }
  }
  // R = Q*R
  apply_q(m, n, a, tau, r.data());

  // Copy Aref
  std::vector<cf32_t> c(m * n);
  memcpy(c.data(), aref, m * n * sizeof(cf32_t));
  column_major_matrix_view c_mat{c.data(), static_cast<uint32_t>(m)};

  // Compute Aref = Aref - QR
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      c_mat(i, j) -= r_mat(i, j);
    }
  }
  // Compute the norm of Aref - Q * R
  float32_t error = infinity_norm(m, n, c);
  return error < tol;
}

// Overload check_qr_decomposition with an interface
// with std::vector data type  as inputs
static inline bool check_qr_decomposition(int m, int n,
                                          const std::vector<cf32_t> &aref,
                                          const std::vector<cf32_t> &a,
                                          const std::vector<cf32_t> &tau) {

  return check_qr_decomposition(m, n, aref.data(), a.data(), tau.data());
}

// Compute C = A * B + beta *C
static inline void matmul(int m, int n, int k, const cf32_t *a, const cf32_t *b,
                          const cf32_t beta, cf32_t *c) {
  column_major_matrix_view a_mat{a, static_cast<uint32_t>(m)};
  column_major_matrix_view b_mat{b, static_cast<uint32_t>(k)};
  column_major_matrix_view c_mat{c, static_cast<uint32_t>(m)};
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      c_mat(i, j) *= beta;
      for (int p = 0; p < k; p++) {
        c_mat(i, j) += a_mat(i, p) * b_mat(p, j);
      }
    }
  }
}

// Overload matmul with an interface
// with std::vector data type as inputs
static inline void matmul(int m, int n, int k, const std::vector<cf32_t> &a,
                          const std::vector<cf32_t> &b, const cf32_t beta,
                          std::vector<cf32_t> &c) {

  matmul(m, n, k, a.data(), b.data(), beta, c.data());
}

// Check the accuracy of the bidiagonal decomposition.
// Check || A - Q * B * P^H||_∞/ || A ||_∞ < tol
// where B is an upper bidiagonal matrix with diagonal
// entries in D, and superdiagonal entries in E.
static inline bool
check_bidiag_decomposition(int m, int n, const cf32_t *aref, const cf32_t *a,
                           const float32_t *d, const float32_t *e,
                           const cf32_t *tauq, const cf32_t *taup) {

  // Infinity norm of aref
  float32_t anorm = infinity_norm(m, n, aref);
  anorm = anorm > 1 ? anorm : 1;
  float32_t tol =
      THRESHOLD * anorm * m * std::numeric_limits<float32_t>::epsilon();

  // Generate right orthogonal vectors

  std::vector<cf32_t> p(n * n);
  get_p(m, n, a, taup, p.data());
  // Build explicitly the n-by-n bidiagonal matrix B
  std::vector<cf32_t> b(n * n);
  column_major_matrix_view b_mat{b.data(), static_cast<uint32_t>(n)};
  for (int i = 0; i < n - 1; i++) {
    b_mat(i, i) = d[i];
    b_mat(i, i + 1) = e[i];
  }
  b_mat(n - 1, n - 1) = d[n - 1];

  // Compute B * VT, allocate m-by-n matrix and
  // fill the top n-by-n with B * VT
  std::vector<cf32_t> c(m * n);
  matmul(n, n, n, b.data(), p.data(), 0, c.data());

  // Apply Q to form Q * B * VT
  apply_q(m, n, a, tauq, c.data());

  // Compute Aref - Q * B * VT
  column_major_matrix_view aref_mat{aref, static_cast<uint32_t>(m)};
  column_major_matrix_view c_mat{c.data(), static_cast<uint32_t>(m)};
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      c_mat(i, j) -= aref_mat(i, j);
    }
  }
  // Infinity norm of error
  float32_t error = infinity_norm(m, n, c);
  return error < tol;
}

// Overload check_bidiag_decomposition with an interface
// with std::vector data type as input
static inline bool check_bidiag_decomposition(
    int m, int n, const std::vector<cf32_t> &aref, const std::vector<cf32_t> &a,
    const std::vector<float32_t> &d, const std::vector<float32_t> &e,
    const std::vector<cf32_t> &tauq, const std::vector<cf32_t> &taup) {

  return check_bidiag_decomposition(m, n, aref.data(), a.data(), d.data(),
                                    e.data(), tauq.data(), taup.data());
}

static inline bool check_singular_values(int n, const float32_t *sref,
                                         const float32_t *s) {
  float32_t tol = THRESHOLD * n * std::numeric_limits<float32_t>::epsilon();
  float32_t error = 0.0F;
  for (int i = 0; i < n; i++) {
    float32_t tmp = std::abs(sref[i] - s[i]);
    if (tmp > error) {
      error = tmp;
    }
  }
  return error < tol;
}

// Overload check_singular_values with an interface
// with std::vector data type as inputs
static inline bool check_singular_values(int n,
                                         const std::vector<float32_t> &sref,
                                         const std::vector<float32_t> &s) {
  return check_singular_values(n, sref.data(), s.data());
}

// Check the accuracy of SVD decomposition
//  error = ||A - U * S *VT^H||_∞/ (||A||_∞ * m)
static inline bool check_svd_decomposition(int m, int n, const cf32_t *a,
                                           const float32_t *s, const cf32_t *u,
                                           const cf32_t *vt) {

  // Infinity norm of A
  float32_t anorm = infinity_norm(m, n, a);
  anorm = anorm > 1 ? anorm : 1;
  float32_t tol =
      THRESHOLD * anorm * m * std::numeric_limits<float32_t>::epsilon();

  // U1 = U * S
  std::vector<cf32_t> u1(m * n);
  column_major_matrix_view u_mat{u, static_cast<uint32_t>(m)};
  column_major_matrix_view u1_mat{u1.data(), static_cast<uint32_t>(m)};
  for (int i = 0; i < m; i++) {
    for (int j = 0; j < n; j++) {
      u1_mat(i, j) = u_mat(i, j) * s[j];
    }
  }
  // Make a copy of A
  std::vector<cf32_t> c(m * n);
  memcpy(c.data(), a, m * n * sizeof(cf32_t));

  // C = A - U * S * VT
  matmul(m, n, n, u1.data(), vt, -1.0F, c.data());

  // Compute the infinity norm ||A  - U * S * VT^H||_oo
  float32_t error = infinity_norm(m, n, c);
  return error < tol;
}

// Overload check_svd_decomposition with an interface
// with std::vector data type as inputs
static inline bool check_svd_decomposition(int m, int n,
                                           const std::vector<cf32_t> &a,
                                           const std::vector<float32_t> &s,
                                           const std::vector<cf32_t> &u,
                                           const std::vector<cf32_t> &vt) {
  return check_svd_decomposition(m, n, a.data(), s.data(), u.data(), vt.data());
}
