/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/

#pragma once

#include <cstdint>
#include <cstdlib>
#include <initializer_list>
#include <type_traits>
#include <vector>

namespace armral::utils {

struct random_state;

class linear_congruential_generator {
public:
  /**
   * Returns a single pseudo-random value using a simple linear congruential
   * generator based on the specified state. The state is updated as part of
   * the call.
   *
   * For `uint32_t`, the range of the returned value is [0, 0x80000000U).
   * For `double`, the range of the returned value is [0, 1).
   * For `bool`, the returned value is either `true` or `false`.
   *
   * @param[in,out] state   The state to use as a seed for the generator.
   * @returns a pseudo-randomly generated integer.
   */
  template<typename T>
  T one(random_state *state);

  /**
   * Returns a single pseudo-random value using a simple linear congruential
   * generator based on the specified state. The state is updated as part of
   * the call.
   *
   * The range of the returned value is bounded by [min, max). Note that this
   * is only defined for floating-point types.
   *
   * @param[in,out] state   The state to use as a seed for the generator.
   * @param[in]     min     The lower bound of the generated value.
   * @param[out]    max     The upper bound of the generated value.
   * @returns a pseudo-randomly generated floating-point value.
   */
  template<typename T,
           std::enable_if_t<std::is_floating_point_v<T>, bool> = true>
  T one(random_state *state, const T min, const T max) {
    return (max - min) * one<T>(state) + min;
  }

  /**
   * Returns a single pseudo-random value using a simple linear congruential
   * generator based on the specified state. The state is updated as part of
   * the call.
   *
   * The range of the returned value is bounded by [min, max]. Note that this
   * is only defined for integer types.
   *
   * @param[in,out] state   The state to use as a seed for the generator.
   * @param[in]     min     The lower bound of the generated value.
   * @param[out]    max     The upper bound of the generated value.
   * @returns a pseudo-randomly generated integer.
   */
  template<typename T, std::enable_if_t<std::is_integral_v<T>, bool> = true>
  T one(random_state *state, const T min, const T max) {
    return ((max - min + 1) * one<float>(state)) + min;
  }

  /**
   * Updates the state as if `one` was called `n` times and discarded the
   * result.
   *
   * @param[in,out] state   The state to use as a seed for the generator.
   * @param[in] n           The number of update steps to perform.
   */
  void advance_state(random_state *state, uint32_t n);
};

struct random_state {
  uint64_t seed = 0;

  /**
   * Create a random_state directly from the specified seed. It is recommended
   * to use `random_state::from_seeds` instead to ensure that the seed bits are
   * fully mixed.
   *
   * @param[in] seed_in   The seed to use.
   */
  explicit random_state(uint64_t seed_in) : seed(seed_in) {}

  /**
   * Create a `random_state` object from the specified seed values by mixing
   * them between iterations of a linear congruential generator.
   *
   * @param[in] seeds  The seeds to base the new random state object on.
   * @returns a mixed `random_state` object.
   */
  static random_state from_seeds(std::initializer_list<uint64_t> seeds);
};

// An abstract base class from which other stateful RNG helper classes are
// defined
template<typename RealType, typename CmplxType>
class base_random {
public:
  base_random(std::initializer_list<uint64_t> seeds = {42})
    : m_state(random_state::from_seeds(seeds)) {}

  virtual CmplxType one(CmplxType min, CmplxType max) = 0;

protected:
  std::vector<CmplxType> vector_impl(size_t len, CmplxType min, CmplxType max) {
    std::vector<CmplxType> ret(len);
    for (auto &value : ret) {
      value = one(min, max);
    }
    return ret;
  }

  RealType rand(RealType min, RealType max) {
    linear_congruential_generator lcg;
    return lcg.one<RealType>(&m_state, min, max);
  }

  random_state m_state;
};

} // namespace armral::utils
