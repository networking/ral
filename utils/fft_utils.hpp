/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"

#include <complex>

#if defined(ARMRAL_SEMIHOSTING) || !defined(_GNU_SOURCE)
#define M_PI 3.14159265358979323846
#endif

namespace armral::utils {

inline void fft_ref(int n, int s, armral_fft_direction_t dir,
                    const std::complex<double> *x, std::complex<double> *y) {
  using namespace std::complex_literals;
  if (n % 2 == 0) {
    fft_ref(n / 2, 2 * s, dir, x, y);
    fft_ref(n / 2, 2 * s, dir, x + s, y + n / 2);

    for (int k = 0; k < n / 2; k++) {
      auto m = 2i * (dir * M_PI * k / n);
      auto p = y[k];
      auto q = y[k + n / 2] * std::exp(m);
      y[k] = p + q;
      y[k + n / 2] = p - q;
    }
  } else {
    for (int i = 0; i < n; ++i) {
      auto acc = 0i;
      for (int j = 0; j < n; ++j) {
        auto m = 2i * (dir * M_PI * i * j / n);
        acc += x[j * s] * std::exp(m);
      }
      y[i] = acc;
    }
  }
}

} // namespace armral::utils
