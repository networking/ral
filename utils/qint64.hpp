/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include <climits>
#include <complex>

namespace armral::utils {

/// A saturating 64b signed integer.
class qint64_t {
  int64_t val;

public:
  /// Default constructor
  qint64_t() : val(0) {}

  /// Conversion from signed integer
  explicit qint64_t(int64_t val_in) : val(val_in) {}

  /// Get the value as a 16b saturated signed integer.
  int16_t get16() const {
    if ((int16_t)val != val) {
      return (val >> 63) != 0 ? -32768 : 32767;
    }
    return val;
  }

  /// Get the value as a 64b saturated signed integer.
  int64_t get64() const {
    return val;
  }

  /// Given a 128-bit integer, returns the saturated 64-bit value.
  static qint64_t saturate(__int128_t in) {
    if (in <= LONG_MIN) {
      return qint64_t{LONG_MIN};
    }
    if (in >= LONG_MAX) {
      return qint64_t{LONG_MAX};
    }
    return static_cast<qint64_t>(in);
  }
};

/// Add two integer types, producing a saturated 64b signed result.
qint64_t operator+(qint64_t l, qint64_t r) {
  __int128_t res = l.get64();
  res += r.get64();
  return qint64_t::saturate(res);
}

/// Add two integer types, producing a saturated 64b signed result.
template<typename R>
qint64_t operator+(qint64_t l, R r) {
  __int128_t res = l.get64();
  res += (int64_t)r;
  return qint64_t::saturate(res);
}

/// Negate a integer types, producing a saturated 64b signed result.
qint64_t operator-(qint64_t l) {
  __int128_t res = l.get64();
  res = -res;
  return qint64_t::saturate(res);
}

/// Subtract two integer types, producing a saturated 64b signed result.
qint64_t operator-(qint64_t l, qint64_t r) {
  __int128_t res = l.get64();
  res -= r.get64();
  return qint64_t::saturate(res);
}

/// Subtract two integer types, producing a saturated 64b signed result.
template<typename R>
qint64_t operator-(qint64_t l, R r) {
  __int128_t res = l.get64();
  res -= (int64_t)r;
  return qint64_t::saturate(res);
}

/// Multiply two integer types, producing a saturated 64b signed result.
qint64_t operator*(qint64_t l, qint64_t r) {
  __int128_t res = l.get64();
  res *= r.get64();
  return qint64_t::saturate(res);
}

/// Multiply two integer types, producing a saturated 64b signed result.
template<typename R>
typename std::enable_if<std::is_integral<R>::value, qint64_t>::type
operator*(qint64_t l, R r) {
  __int128_t res = l.get64();
  res *= (int64_t)r;
  return qint64_t::saturate(res);
}

#ifdef _GNU_SOURCE
/// Divide two integer types, producing a saturated 64b signed result.
inline qint64_t operator/(qint64_t l, qint64_t r) {
  __int128_t res = l.get64();
  res /= r.get64();
  return qint64_t::saturate(res);
}

/// Divide two integer types, producing a saturated 64b signed result.
template<typename R>
typename std::enable_if<std::is_integral<R>::value, qint64_t>::type
operator/(qint64_t l, R r) {
  __int128_t res = l.get64();
  res /= (int64_t)r;
  return qint64_t::saturate(res);
}

/// Divide two integer types, producing a saturated 64b signed result.
template<typename R>
void operator/=(qint64_t &l, R r) {
  l = l / r;
}
#endif

/// Add two integer types, producing a saturated 64b signed result.
template<typename R>
void operator+=(qint64_t &l, R r) {
  l = l + r;
}

/// Subtract two integer types, producing a saturated 64b signed result.
template<typename R>
void operator-=(qint64_t &l, R r) {
  l = l - r;
}

/// Multiply two integer types, producing a saturated 64b signed result.
template<typename R>
void operator*=(qint64_t &l, R r) {
  l = l * r;
}

/*
 * Shift a saturated signed integer by an integer amount, producing a
 * saturated 64b signed result.
 */
template<typename R>
qint64_t operator>>(qint64_t l, R r) {
  return qint64_t{l.get64() >> r};
}

/*
 * Shift a saturated signed integer by an integer amount, producing a
 * saturated 64b signed result.
 */
template<typename R>
void operator>>=(qint64_t &l, R r) {
  l = l >> r;
}

/*
 * Shift a complex saturated signed integer by an integer amount, producing a
 * saturated 64b signed result.
 */
template<typename R>
std::complex<qint64_t> operator>>(std::complex<qint64_t> l, R r) {
  return {l.real() >> r, l.imag() >> r};
}

/*
 * Shift a saturated signed integer by an integer amount, producing a
 * saturated 64b signed result.
 */
template<typename R>
qint64_t operator<<(qint64_t l, R r) {
  return l.get64() << r;
}

/*
 * Shift a complex saturated signed integer by an integer amount, producing a
 * saturated 64b signed result.
 */
template<typename R>
std::complex<qint64_t> operator<<(std::complex<qint64_t> l, R r) {
  return {l.real() << r, l.imag() << r};
}

/*
 * Multiply two complex integer types, producing a saturated 64b signed result.
 */
std::complex<qint64_t> operator*(std::complex<qint64_t> a,
                                 std::complex<qint64_t> b) {
  return {a.real() * b.real() - a.imag() * b.imag(),
          a.real() * b.imag() + a.imag() * b.real()};
}

} // namespace armral::utils
