/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "rng.hpp"

#include <cassert>
#include <complex>
#include <random>
#include <vector>

namespace armral::utils {

/*
 * A helper class for generating random complex numbers. This class preserves
 * the random state between calls, so if repeatedly requesting vectors of the
 * same length a uniquely random vector is returned each time. Note this is not
 * the case for the allocate_random_cf32() helper function.
 *
 * Example usage:
 *
 *   cf32_random random;
 *   auto a = random.vector(m * n); // returns std::vector<armral_cmplx_f32_t>
 *   auto b = random.vector(m * k);
 *   armral_cmplx_f32_t foo = random.one()
 *
 * You can also specify a min/max if necessary:
 *
 *   cf32_random random;
 *   auto c = random.vector(n * k, {1, 10}, {10, 20});
 *   auto bar = random.one({-10, -10}, {10, 10});
 *
 *   Note: the default is 1 + 2j to 2 + 4j
 *
 * To use custom seeds just pass them to the constructor:
 *
 *  cf32_random random({ 1337, 42 });
 *
 * Note: You must use the same cf32_random instance between calls to reuse the
 * random state.
 */
class cf32_random : public base_random<float32_t, armral_cmplx_f32_t> {
public:
  cf32_random(std::initializer_list<uint64_t> seeds = {42})
    : base_random<float32_t, armral_cmplx_f32_t>(seeds) {}

  static constexpr armral_cmplx_f32_t default_min{1.0F, 2.0F};
  static constexpr armral_cmplx_f32_t default_max{2.0F, 4.0F};

  armral_cmplx_f32_t one(armral_cmplx_f32_t min = default_min,
                         armral_cmplx_f32_t max = default_max) {
    return armral_cmplx_f32_t{rand(min.re, max.re), rand(min.im, max.im)};
  }

  std::vector<armral_cmplx_f32_t> vector(size_t len,
                                         armral_cmplx_f32_t min = default_min,
                                         armral_cmplx_f32_t max = default_max) {
    return base_random<float32_t, armral_cmplx_f32_t>::vector_impl(len, min,
                                                                   max);
  }

  std::vector<armral_cmplx_f32_t> &
  flip_signs(std::vector<armral_cmplx_f32_t> &vector,
             float32_t chance_re = 0.5F, float32_t chance_im = 0.5F) {
    for (auto &cmplx : vector) {
      bool re_flip = rand(0, 1) < chance_re;
      bool im_flip = rand(0, 1) < chance_im;
      cmplx.re = re_flip ? -cmplx.re : cmplx.re;
      cmplx.im = im_flip ? -cmplx.im : cmplx.im;
    }
    return vector;
  }

  std::vector<armral_cmplx_f32_t>
  flip_signs(std::vector<armral_cmplx_f32_t> &&vector,
             float32_t chance_re = 0.5F, float32_t chance_im = 0.5F) {
    auto result = std::move(vector);
    return flip_signs(result);
  }
};

inline std::vector<armral_cmplx_f32_t> allocate_random_cf32(uint32_t len) {
  return cf32_random().vector(len);
}

inline std::complex<double> cmplx_mul_widen_cf32(armral_cmplx_f32_t a,
                                                 armral_cmplx_f32_t b) {
  std::complex<double> a_wide = {a.re, a.im};
  std::complex<double> b_wide = {b.re, b.im};
  return a_wide * b_wide;
}

inline std::vector<std::complex<double>> widen_cf32(const armral_cmplx_f32_t *a,
                                                    int n) {
  std::vector<std::complex<double>> a_wide(n);
  for (int i = 0; i < n; i++) {
    a_wide[i] = {a[i].re, a[i].im};
  }
  return a_wide;
}

inline std::vector<armral_cmplx_f32_t>
narrow_to_cf32(const std::vector<std::complex<double>> &a) {
  int n = a.size();
  std::vector<armral_cmplx_f32_t> a_narrow(n);
  for (int i = 0; i < n; i++) {
    a_narrow[i].re = a[i].real();
    a_narrow[i].im = a[i].imag();
  }
  return a_narrow;
}

inline std::vector<armral_cmplx_f32_t>
pack_cf32(const std::vector<float32_t> &re, const std::vector<float32_t> &im) {
  assert(re.size() == im.size());
  std::vector<armral_cmplx_f32_t> ret(re.size());
  for (unsigned i = 0; i < ret.size(); ++i) {
    ret[i] = {re[i], im[i]};
  }
  return ret;
}

inline std::vector<float32_t>
unpack_real_cf32(const std::vector<armral_cmplx_f32_t> &in) {
  std::vector<float32_t> ret(in.size());
  for (unsigned i = 0; i < ret.size(); ++i) {
    ret[i] = in[i].re;
  }
  return ret;
}

inline std::vector<float32_t>
unpack_imag_cf32(const std::vector<armral_cmplx_f32_t> &in) {
  std::vector<float32_t> ret(in.size());
  for (unsigned i = 0; i < ret.size(); ++i) {
    ret[i] = in[i].im;
  }
  return ret;
}

/*
 * Check an array of results against an array of expected values.
 *
 * name:     The name of the function, used when printing errors.
 * result:   A pointer to an array of results, length n.
 * expected: A pointer to an array of expected values, length n.
 * n:        The length of the two arrays, in elements.
 * op_count: An optional value specifying the number of operations
 *           required to calculate each element of `expected`. This
 *           is used to scale the relative tolerance. The default
 *           value is 400, which gives a relative tolerance of
 *           4.76837e-05. This is an arbitrarily chosen constant
 *           which allows for meaningful testing, in general.
 *           In the future, we would like to tighten the error bounds,
 *           which requires problem-specific information, as well as
 *           restrictions on input values and sizes.
 *
 * Returns true if the elements match elementwise, within tolerance.
 */
inline bool check_results_cf32(const char *name, const float32_t *result,
                               const float32_t *expected, uint32_t n,
                               uint32_t op_count = 400) {
  bool passed = true;
  float32_t max_error = 0;
  float32_t diff_at_max_error = 0;
  float32_t max_diff = 0;
  float32_t error_at_max_diff = 0;

  float32_t relative_tol = op_count * std::numeric_limits<float32_t>::epsilon();

  // This is an arbitrarily chosen constant.
  // In the future, we would like to tighten the error bounds, which requires
  // problem-specific information, as well as restrictions on input values and
  // sizes.
  float32_t abs_tol = 0.000015;

  for (uint32_t i = 0; i < n; ++i) {
    float32_t diff = fabs(result[i] - expected[i]);
    float32_t err =
        expected[i] != 0 ? fabs(diff / expected[i]) : fabs(result[i]);
    if (err > max_error) {
      max_error = err;
      diff_at_max_error = diff;
    }
    if (diff > max_diff) {
      max_diff = diff;
      error_at_max_diff = err;
    }

    bool passed_relative_tol = err <= relative_tol;
    bool passed_abs_tol = diff <= abs_tol;

    if (!passed_relative_tol && !passed_abs_tol) {
      // GCOVR_EXCL_START
      passed = false;
      printf(
          "Error! [%s] result[%u]= %.10f and expected[%u]= %.10f, diff: "
          "%.10f, relative error %.10f > %.10f and absolute difference %.10f > "
          "%.10f\n",
          name, i, result[i], i, expected[i], diff, err, relative_tol, diff,
          abs_tol);
      // GCOVR_EXCL_STOP
    }
  }

  if (passed) {
    printf("[%s] - check result: OK, max error was %.10f and max diff: %.10f",
           name, max_error, max_diff);
    if (max_error > relative_tol) {
      printf(", which is above the relative error tolerance of %.10f, "
             "but passed as the diff then was %.10f (< %.10f)\n",
             relative_tol, diff_at_max_error, abs_tol);
    } else if (max_diff > abs_tol) {
      printf(", which is above the absolute tolerance of %.10f, "
             "but passed as the error then was %.10f (< %.10f)\n",
             abs_tol, error_at_max_diff, relative_tol);
    } else {
      printf(" vs relative error tolerance of %.10f and absolute tolerance "
             "of %.10f\n",
             relative_tol, abs_tol);
    }
  } else {
    printf("[%s] - check result: ERROR\n", name); // GCOVR_EXCL_LINE
  }
  return passed;
}

/*
 * Check an array of results against an array of expected values.
 *
 * name:     The name of the function, used when printing errors.
 * result:   A pointer to an array of results, length n.
 * expected: A pointer to an array of expected values, length n.
 * n:        The length of the two arrays, in elements.
 * op_count: An optional value specifying the number of operations
 *           required to calculate each element of `expected`. This
 *           is used to scale the relative tolerance. The default
 *           value is 400, which gives a relative tolerance of
 *           4.76837e-05. This is an arbitrarily chosen constant
 *           which allows for meaningful testing, in general.
 *           In the future, we would like to tighten the error bounds,
 *           which requires problem-specific information, as well as
 *           restrictions on input values and sizes.
 *
 * Returns true if the elements match elementwise, within tolerance.
 */
inline bool check_results_cf32(const char *name,
                               const armral_cmplx_f32_t *result,
                               const armral_cmplx_f32_t *expected, uint32_t n,
                               uint32_t op_count = 400) {
  return check_results_cf32(name, (const float32_t *)result,
                            (const float32_t *)expected, n * 2, op_count);
}

} // namespace armral::utils
