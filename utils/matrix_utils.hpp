/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "reference_linalg.hpp"
#include "rng.hpp"

namespace armral::utils {

/*
 * Generate random values, the resulting matrix will have linearly independent
 * columns with probability almost 1.
 */
inline std::vector<armral_cmplx_f32_t>
allocate_random_cf32_lin_ind(uint32_t len) {
  static linear_congruential_generator lcg;
  auto state = random_state::from_seeds({42});

  std::vector<armral_cmplx_f32_t> ret(len);
  for (uint32_t i = 0; i < len; ++i) {
    ret[i].re = lcg.one<float32_t>(&state, -100., 100.);
    ret[i].im = lcg.one<float32_t>(&state, -100., 100.);
  }
  return ret;
}

/*
 * Generate random invertible matrices.
 */
inline std::vector<armral_cmplx_f32_t>
gen_invertible_matrix(uint32_t m, float32_t scale_re = 1.0F,
                      float32_t scale_im = 1.0F) {

  auto a = allocate_random_cf32_lin_ind(m * m);

  // Make matrix diagonally dominant with non-negative diagonal.
  // It is non-singular with high probability by virtue of sampling randomly.

  // If real-part is zeroed-out increase fac to avoid det(a)=0
  float32_t fac = (scale_re == 0.0F) ? 2.0F : 1.0F;

  for (unsigned i = 0; i < m; ++i) {
    // force non-negative diagonal entries
    a[i * m + i].re = std::abs(a[i * m + i].re);
    for (unsigned j = 0; j < m; ++j) {
      if (i != j) {
        a[i * m + i].re +=
            fac *
            std::abs(std::complex<float32_t>(a[i * m + j].re, a[i * m + j].im));
      }
    }
  }
  return a;
}

/*
 * Generate a batch of random invertible matrices
 */
inline std::vector<armral_cmplx_f32_t>
gen_invertible_matrix_batch(uint32_t batch_size, uint32_t m,
                            float32_t scale_re = 1.0F,
                            float32_t scale_im = 1.0F) {

  // Generate batch of matrices
  std::vector<armral_cmplx_f32_t> a(batch_size * m * m);
  for (unsigned batch = 0; batch < batch_size; ++batch) {
    auto src = gen_invertible_matrix(m, scale_re, scale_im);
    // pack
    for (unsigned i = 0; i < m * m; ++i) {
      a[i * batch_size + batch] = src[i];
    }
  }
  return a;
}

/*
 * Generate random Hermitian matrices (with option to force positive
 * definiteness)
 */
inline std::vector<armral_cmplx_f32_t>
gen_hermitian_matrix(uint32_t m, bool is_hpd = false, float32_t scale_re = 1.0F,
                     float32_t scale_im = 1.0F, bool perf = false) {

  auto a = perf ? std::vector<armral_cmplx_f32_t>(m * m)
                : allocate_random_cf32_lin_ind(m * m);
  if (perf) {
    auto *a_ptr = a.data();
    for (uint32_t i = 0; i < m; ++i) {
      for (uint32_t j = 0; j < m; ++j) {
        a_ptr->re = i * j;
        a_ptr->im = j;
        a_ptr++;
      }
    }
  }

  // Make matrix Hermitian
  for (unsigned i = 0; i < m; ++i) {
    // Hermitian matrices cannot have non-zero imag. part on diagonal
    a[i * m + i] = {a[i * m + i].re, 0.0F};
    for (unsigned j = 0; j < i; ++j) {
      a[j * m + i] = {a[i * m + j].re, -a[i * m + j].im};
    }
  }

  // scale real and imaginary part
  cscal(m * m, a.data(), {scale_re, scale_im});

  // Assert that matrix is Hermitian
  for (unsigned i = 0; i < m; ++i) {
    // real diagonal
    assert(a[i * m + i].im == 0.0F);
    // transpose => conjugate
    for (unsigned j = 0; j < i; ++j) {
      assert(a[j * m + i].re == a[i * m + j].re &&
             a[j * m + i].im == -a[i * m + j].im);
    }
  }

  if (is_hpd) {
    // Make Hermitian matrix diagonally dominant with non-negative diagonal,
    // hence positive semi-definite It is non-singular with high probability by
    // virtue of sampling randomly

    // If real-part is zeroed-out increase fac to avoid det(a)=0
    float32_t fac = (scale_re == 0.0F) ? 2.0F : 1.0F;

    for (unsigned i = 0; i < m; ++i) {
      // force non-negative diagonal entries
      a[i * m + i].re = std::abs(a[i * m + i].re);
      for (unsigned j = 0; j < m; ++j) {
        if (i != j) {
          a[i * m + i].re += fac * std::abs(std::complex<float32_t>(
                                       a[i * m + j].re, a[i * m + j].im));
        }
      }
    }
  }

  return a;
}

/*
 * Generate a batch of random Hermitian matrices (with option to force positive
 * definiteness)
 */
inline std::vector<armral_cmplx_f32_t>
gen_hermitian_matrix_batch(uint32_t batch_size, uint32_t m, bool is_hpd = false,
                           float32_t scale_re = 1.0F, float32_t scale_im = 1.0F,
                           bool perf = false) {

  // Generate batch of matrices
  std::vector<armral_cmplx_f32_t> a(batch_size * m * m);
  for (unsigned batch = 0; batch < batch_size; ++batch) {
    auto src = gen_hermitian_matrix(m, is_hpd, scale_re, scale_im, perf);
    // pack
    for (unsigned i = 0; i < m * m; ++i) {
      a[i * batch_size + batch] = src[i];
    }
  }
  return a;
}

/*
 * Function to print check results of matrix inversion UTs.
 */
static bool check_results_mat_inv(
    const std::string &name, const float32_t *result, const float32_t *expected,
    const uint32_t n_values, /*n_values = 2 * nSamples, due to RE and IM part)*/
    const float32_t rel_tol_mult = 1.0F, const float32_t abs_tol_mult = 1.0F,
    int verbose = 0) {
  bool passed = true;
  float32_t max_error = 0;
  // TODO: arbitrarily chosen constant. we should probably do better than this,
  //       but until we actually talk to people and get an idea of acceptable
  //       tolerances then there's not much point in being too exact here.
  float32_t relative_tol = 0.00001;  // 10^-5
  float32_t diff_tolerance = 0.0001; // 10^-4
  relative_tol *= rel_tol_mult;
  diff_tolerance *= abs_tol_mult;

  for (uint32_t i = 0; i < n_values; ++i) {
    float32_t diff_abs = fabs(result[i] - expected[i]);
    float32_t error =
        (expected[i] != 0) ? fabs(diff_abs / expected[i]) : fabs(result[i]);
    max_error = std::max(error, max_error);

    if (!std::isfinite(error) || !std::isfinite(diff_abs)) {
      // GCOVR_EXCL_START
      passed = false;
      printf("Error!! [%s armral_cmplx_f32_t] result[%u]= %.10f and "
             "expected[%u]= "
             "%.10f, error=%f, diff_abs=%f\n",
             name.c_str(), i, result[i], i, expected[i], error, diff_abs);
      // GCOVR_EXCL_STOP
    } else if (error > relative_tol) {
      if (diff_abs < diff_tolerance) {
        if (verbose != 0) {
          // GCOVR_EXCL_START
          printf(
              "[%s armral_cmplx_f32_t] result[%u]= %.10f, expected[%u]= %.10f "
              "abs(difference) %.10f is less than %f --> OK\n",
              name.c_str(), i, result[i], i, expected[i], diff_abs,
              diff_tolerance);
          // GCOVR_EXCL_STOP
        }
      } else {
        // GCOVR_EXCL_START
        passed = false;
        printf("Error!! [%s armral_cmplx_f32_t] result[%u]= %.10f and "
               "expected[%u]= "
               "%.10f diffAbs %.10f is greater than %f\n",
               name.c_str(), i, result[i], i, expected[i], diff_abs,
               diff_tolerance);
        // GCOVR_EXCL_STOP
      }
    } else {
      if (verbose != 0) {
        // GCOVR_EXCL_START
        printf(
            "[%s armral_cmplx_f32_t] result[%u]= %.10f and expected[%u]= %.10f "
            "relative error %.10f is less than %f --> OK\n",
            name.c_str(), i, result[i], i, expected[i], error, relative_tol);
        // GCOVR_EXCL_STOP
      }
    }
  }

  if (!passed) {
    // GCOVR_EXCL_START
    printf("[%s armral_cmplx_f32_t] - check result: ERROR\n", name.c_str());
    // GCOVR_EXCL_STOP
  } else {
    printf("[%s armral_cmplx_f32_t] - check result: OK, max error was %e vs "
           "tolerance "
           "of %e\n",
           name.c_str(), max_error, relative_tol);
  }
  return passed;
}

/*
 * Check that MM^{-1} = M^{-1}M = Id.
 */
inline bool check_results_identity(const armral_cmplx_f32_t *mat,
                                   const armral_cmplx_f32_t *inv_m, uint32_t m,
                                   int verbose = 0) {
  bool passed = true;
  // Init arrays
  std::vector<armral_cmplx_f32_t> id(m * m);
  std::vector<armral_cmplx_f32_t> mm(m * m);
  for (unsigned i = 0; i < m; ++i) {
    for (unsigned j = 0; j < m; ++j) {
      if (i == j) {
        id[i * m + j] = {1.0F, 0.0F};
      } else {
        id[i * m + j] = {0.0F, 0.0F};
      }
    }
  };

  // Convert matrix and inverse to double precision
  auto m64 = convert_cf32_array_to_vector<double>(m * m, mat);
  auto inv_m64 = convert_cf32_array_to_vector<double>(m * m, inv_m);
  // Check M^{-1}M
  {
    std::vector<std::complex<double>> mm64(m * m);
    reference_zgemm(m, m, m, 1.0F, m64, inv_m64, 0.0F, mm64);
    convert_vector_to_cf32_array(m * m, mm64, mm.data());
    passed &= check_results_mat_inv("MM^{-1} - Id", (float32_t *)mm.data(),
                                    (float32_t *)id.data(), 2 * m * m,
                                    (float32_t)m, (float32_t)m, verbose);
  }
  // MM^{-1}
  {
    std::vector<std::complex<double>> mm64(m * m);
    reference_zgemm(m, m, m, 1.0F, inv_m64, m64, 0.0F, mm64);
    convert_vector_to_cf32_array(m * m, mm64, mm.data());
    passed &= check_results_mat_inv("M^{-1}M - Id", (float32_t *)mm.data(),
                                    (float32_t *)id.data(), 2 * m * m,
                                    (float32_t)m, (float32_t)m, verbose);
  }
  return passed;
}

/*
 * Unpack data from batched format into a contiguous array
 */
template<typename T>
inline void unpack_data(unsigned batch, unsigned batch_size, const T *src,
                        T *dst, unsigned length) {
  for (unsigned i = 0; i < length; ++i) {
    dst[i] = src[i * batch_size + batch];
  }
}

/*
 * Pack data from a contiguous array into batched format
 */
template<typename T>
inline void pack_data(unsigned batch, unsigned batch_size, const T *src, T *dst,
                      unsigned length) {
  for (unsigned i = 0; i < length; ++i) {
    dst[i * batch_size + batch] = src[i];
  }
}

inline void print_cmplx_row(uint32_t m, const armral_cmplx_f32_t *row,
                            uint32_t stride = 1) {
  printf("[%.3g+%.3gj", row[0].re, row[0].im);
  for (unsigned i = stride; i < m * stride; i += stride) {
    printf(", %.3g+%.3gj", row[i].re, row[i].im);
  }
  printf("]");
}

inline void print_cmplx_mat(const std::string &ref, uint32_t m,
                            const armral_cmplx_f32_t *a,
                            uint32_t batch_size = 1) {
  for (unsigned i = 0; i < batch_size; i++) {
    printf("%s[%u]=[", ref.c_str(), i);
    print_cmplx_row(m, a + i, batch_size);
    for (unsigned j = 1; j < m; j++) {
      printf(", ");
      print_cmplx_row(m, a + j * m * batch_size + i, batch_size);
    }
    printf("]\n");
  }
}

/*
 * Return the number of floating-point operations required to calculate a
 * length-n complex dot product
 */
inline uint32_t cmplx_dot_nflops(uint32_t n) {
  uint32_t nflops = 0;
  if (n > 0) {
    // A complex multiplication requires 6 floating-point operations
    uint32_t op_mul = 6;

    // A complex multiply-accumulate requires 8 floating-point operations
    uint32_t op_mla = 8;

    // The cost of multiplying the first two vector entries together
    nflops += op_mul;

    // The cost of multiplying the remaining (n-1) vector entries
    // and accumulating into the dot product
    nflops += (n - 1) * op_mla;
  }
  return nflops;
}

} // namespace armral::utils
