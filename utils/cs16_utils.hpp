/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "qint64.hpp"
#include "rng.hpp"

#include <cassert>
#include <complex>
#include <random>
#include <vector>

namespace armral::utils {

/*
 * A helper class for generating random complex numbers. This class preserves
 * the random state between calls, so if repeatedly requesting vectors of the
 * same length a uniquely random vector is returned each time. Note this is not
 * the case for the allocate_random_cf32() helper function.
 *
 * Example usage:
 *
 *   cs16_random random;
 *   auto a = random.vector(m * n); // returns std::vector<armral_cmplx_int16_t>
 *   auto b = random.vector(m * k);
 *   armral_cmplx_int16_t foo = random.one()
 *
 * You can also specify a min/max if necessary:
 *
 *   cs16_random random;
 *   auto c = random.vector(n * k, {1, 10}, {10, 20});
 *   auto bar = random.one({-10, -10}, {10, 10});
 *
 *   Note: the default is INT16_MIN + INT16_MINj to INT16_MAX + INT16_MAXj
 *
 * To use custom seeds just pass them to the constructor:
 *
 *  cs16_random random({ 1337, 42 });
 *
 * Note: You must use the same cs16_random instance between calls to reuse the
 * random state.
 */
class cs16_random : public base_random<int16_t, armral_cmplx_int16_t> {
public:
  cs16_random(std::initializer_list<uint64_t> seeds = {42})
    : base_random<int16_t, armral_cmplx_int16_t>(seeds) {}

  static constexpr armral_cmplx_int16_t default_min{INT16_MIN, INT16_MIN};
  static constexpr armral_cmplx_int16_t default_max{INT16_MAX, INT16_MAX};

  armral_cmplx_int16_t one(armral_cmplx_int16_t min = default_min,
                           armral_cmplx_int16_t max = default_max) {
    return armral_cmplx_int16_t{rand(min.re, max.re), rand(min.im, max.im)};
  }

  std::vector<armral_cmplx_int16_t>
  vector(size_t len, armral_cmplx_int16_t min = default_min,
         armral_cmplx_int16_t max = default_max) {
    return base_random<int16_t, armral_cmplx_int16_t>::vector_impl(len, min,
                                                                   max);
  }

  std::vector<armral_cmplx_int16_t>
  shifted_vector(size_t len, armral_cmplx_int16_t min = default_min,
                 armral_cmplx_int16_t max = default_max) {
    std::vector<armral_cmplx_int16_t> ret(len);
    for (uint32_t i = 0; i < len; ++i) {
      ret[i] = one(min, max);
      int16_t shift = rand(0, 16);
      ret[i].re >>= shift;
      ret[i].im >>= shift;
    }
    return ret;
  }
};

inline std::vector<armral_cmplx_int16_t>
allocate_random_cs16(uint32_t len, int16_t min, int16_t max) {
  return cs16_random().vector(len, armral_cmplx_int16_t{min, min},
                              armral_cmplx_int16_t{max, max});
}

inline std::vector<armral_cmplx_int16_t>
allocate_random_shifted_cs16(uint32_t len, int16_t min, int16_t max) {
  return cs16_random().shifted_vector(len, armral_cmplx_int16_t{min, min},
                                      armral_cmplx_int16_t{max, max});
}

inline std::vector<armral_cmplx_int16_t> allocate_random_cs16(uint32_t len) {
  return allocate_random_cs16(len, INT16_MIN, INT16_MAX);
}

inline std::complex<int64_t> cmplx_mul_widen_cs16(armral_cmplx_int16_t a,
                                                  armral_cmplx_int16_t b) {
  std::complex<int64_t> a_wide = {a.re, a.im};
  std::complex<int64_t> b_wide = {b.re, b.im};
  return a_wide * b_wide;
}

inline void scale_and_truncate_cs16(std::vector<armral_cmplx_int16_t> &ret,
                                    const armral_cmplx_int16_t *scale) {
  for (unsigned i = 0; i < ret.size(); ++i) {
    std::complex<qint64_t> res = cmplx_mul_widen_cs16(ret[i], *scale);
    // truncate to Q15 directly, no rounding.
    ret[i].re = (res.real() >> 15).get16();
    ret[i].im = (res.imag() >> 15).get16();
  }
}

inline std::vector<armral_cmplx_int16_t>
pack_cs16(const std::vector<int16_t> &re, const std::vector<int16_t> &im) {
  assert(re.size() == im.size());
  std::vector<armral_cmplx_int16_t> ret(re.size());
  for (unsigned i = 0; i < ret.size(); ++i) {
    ret[i] = {re[i], im[i]};
  }
  return ret;
}

inline std::vector<int16_t>
unpack_real_cs16(const std::vector<armral_cmplx_int16_t> &in) {
  std::vector<int16_t> ret(in.size());
  for (unsigned i = 0; i < ret.size(); ++i) {
    ret[i] = in[i].re;
  }
  return ret;
}

inline std::vector<int16_t>
unpack_imag_cs16(const std::vector<armral_cmplx_int16_t> &in) {
  std::vector<int16_t> ret(in.size());
  for (unsigned i = 0; i < ret.size(); ++i) {
    ret[i] = in[i].im;
  }
  return ret;
}

/*
 * Check an array of results against an array of expected values.
 *
 * name:     The name of the function, used when printing errors.
 * result:   A pointer to an array of results, length n.
 * expected: A pointer to an array of expected values, length n.
 * n:        The length of the two arrays, in elements.
 *
 * Returns true if the elements match elementwise, within tolerance.
 */
inline bool check_results_cs16(const char *name, const int16_t *result,
                               const int16_t *expected, uint32_t n) {
  bool passed = true;

  for (uint32_t i = 0; i < n; ++i) {
    int diff_abs = abs(result[i] - expected[i]);
    if (diff_abs > 0) {
      // GCOVR_EXCL_START
      passed = false;
      printf("Error! [%s] result[%u]= %d and expected[%u]= %d\n", name, i,
             result[i], i, expected[i]);
      // GCOVR_EXCL_STOP
    }
  }

  if (passed) {
    printf("[%s] - check result: OK\n", name);
  } else {
    printf("[%s] - check result: ERROR\n", name); // GCOVR_EXCL_LINE
  }
  return passed;
}

/*
 * Check an array of results against an array of expected values.
 *
 * name:     The name of the function, used when printing errors.
 * result:   A pointer to an array of results, length n.
 * expected: A pointer to an array of expected values, length n.
 * n:        The length of the two arrays, in elements.
 *
 * Returns true if the elements match elementwise, within tolerance.
 */
inline bool check_results_cs16(const char *name,
                               const armral_cmplx_int16_t *result,
                               const armral_cmplx_int16_t *expected,
                               uint32_t n) {
  return check_results_cs16(name, (const int16_t *)result,
                            (const int16_t *)expected, n * 2);
}

} // namespace armral::utils
