/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "armral.h"
#include "cs16_utils.hpp"
#include "qint64.hpp"

#include <cassert>
#include <complex>
#include <vector>

namespace armral::utils {

/*
 * Multiply a vector by a uniform scaling factor.
 *
 * This is explicitly noinline since it avoids a compiler bug with GCC 8.2.0
 * where the code is incorrectly inlined into gen_hermitian_matrix.
 */
inline void __attribute__((noinline))
cscal(uint32_t n, armral_cmplx_f32_t *a, armral_cmplx_f32_t s) {
  for (unsigned i = 0; i < n; ++i) {
    a[i].re *= s.re;
    a[i].im *= s.im;
  }
}

/*
 * ZGEMM: General complex double matrix multiplication C = beta*C + alpha*A*B
 */
inline void reference_zgemm(uint16_t m, uint16_t n, uint16_t k,
                            const double alpha,
                            const std::vector<std::complex<double>> &a,
                            const std::vector<std::complex<double>> &b,
                            const double beta,
                            std::vector<std::complex<double>> &c) {
  for (int i = 0; i < m; ++i) {
    for (int nn = 0; nn < n; ++nn) {
      c[i * n + nn] = beta * c[i * n + nn];
      for (int j = 0; j < k; ++j) {
        c[i * n + nn] += alpha * a[i * k + j] * b[j * n + nn];
      }
    }
  }
}

/*
 * Reorder matrices to allow easy access to blocks.
 */
unsigned zorder_y_of(unsigned index) {
  unsigned y = 0;
  for (unsigned b = 0, k = 0; (1U << b) <= index; b += 2, k++) {
    y += static_cast<unsigned>((index & (1U << b)) != 0) << k;
  }
  return y;
}

unsigned zorder_x_of(unsigned index) {
  return zorder_y_of(index >> 1);
}

/*
 * Convert from z-order to row-major.
 */
std::vector<std::complex<double>>
zorder_to_rowmajor(uint32_t m, const std::vector<std::complex<double>> &z) {
  std::vector<std::complex<double>> a(m * m);
  for (unsigned i = 0; i < m; ++i) {
    for (unsigned j = 0; j < m; ++j) {
      unsigned ijx = zorder_x_of(i * m + j);
      unsigned ijy = zorder_y_of(i * m + j);
      a[ijx * m + ijy] = z[i * m + j];
    }
  }
  return a;
}

/*
 * Convert from row-major to z-order.
 */
std::vector<std::complex<double>>
rowmajor_to_zorder(uint32_t m, const std::vector<std::complex<double>> &a) {
  std::vector<std::complex<double>> z(m * m);
  for (unsigned i = 0; i < m; ++i) {
    for (unsigned j = 0; j < m; ++j) {
      unsigned ijx = zorder_x_of(i * m + j);
      unsigned ijy = zorder_y_of(i * m + j);
      z[i * m + j] = a[ijx * m + ijy];
    }
  }
  return z;
}

/*
 * General matrix multiplication on matrices stored in z-order.
 */
void reference_zgemm_zorder(uint32_t m, const double alpha,
                            const std::vector<std::complex<double>> &a,
                            const std::vector<std::complex<double>> &b,
                            const double beta,
                            std::vector<std::complex<double>> &c) {
  // Convert to row-major
  auto a64 = zorder_to_rowmajor(m, a);
  auto b64 = zorder_to_rowmajor(m, b);
  auto c64 = zorder_to_rowmajor(m, c);

  // Evaluate double precision matrix multiply
  reference_zgemm(m, m, m, alpha, a64, b64, beta, c64);

  // Convert back to original order
  c = rowmajor_to_zorder(m, c64);
}

std::vector<std::complex<double>>
reference_zgeinv_2x2(uint32_t m, const std::vector<std::complex<double>> &mat) {
  std::vector<std::complex<double>> inv_m(m * m);
  // Inverse 2x2 matrix using analytic expression
  std::complex<double> rdet = 1.0 / (mat[0] * mat[3] - mat[1] * mat[2]);
  inv_m[0] = +rdet * mat[3];
  inv_m[1] = -rdet * mat[1];
  inv_m[2] = -rdet * mat[2];
  inv_m[3] = +rdet * mat[0];
  return inv_m;
}

std::vector<std::complex<double>>
reference_zgeinv_3x3(uint32_t m, const std::vector<std::complex<double>> &mat) {
  std::vector<std::complex<double>> inv_m(m * m);
  auto a0 = mat[0];
  auto a1 = mat[1];
  auto a2 = mat[2];
  auto a3 = mat[4];
  auto a4 = mat[5];
  auto a5 = mat[8];

  auto c1 = mat[3];
  auto c2 = mat[6];
  auto c4 = mat[7];

  auto adj00 = a3 * a5 - a4 * c4;
  auto adj11 = a0 * a5 - a2 * c2;
  auto adj22 = a0 * a3 - a1 * c1;

  auto adj10 = c1 * a5 - c2 * a4;
  auto adj20 = c1 * c4 - c2 * a3;
  auto adj01 = a1 * a5 - c4 * a2;
  auto adj21 = a0 * c4 - c2 * a1;
  auto adj02 = a1 * a4 - a3 * a2;
  auto adj12 = a0 * a4 - c1 * a2;

  // Compute cofactors (apply negative signs)
  adj01 = -adj01;
  adj10 = -adj10;
  adj12 = -adj12;
  adj21 = -adj21;

  // Determinant: A_{0:} * adj(A)_{:0}
  auto inv_det = 1.0 / (a0 * adj00 + a1 * adj10 + a2 * adj20);

  // Write into output array
  inv_m[0] = adj00 * inv_det;
  inv_m[1] = adj01 * inv_det;
  inv_m[2] = adj02 * inv_det;
  inv_m[3] = adj10 * inv_det;
  inv_m[4] = adj11 * inv_det;
  inv_m[5] = adj12 * inv_det;
  inv_m[6] = adj20 * inv_det;
  inv_m[7] = adj21 * inv_det;
  inv_m[8] = adj22 * inv_det;
  return inv_m;
}

/*
 * Matrix Inversion using blockwise approach (recursive implementation).
 *
 * M = [A B]   M^{-1} = [X^{-1}         -A^{-1}BU^{-1}]
 *     [C D]            [-D^{-1}CX^{-1} U^{-1}        ]
 */
std::vector<std::complex<double>>
reference_zgeinv(uint32_t m, const std::vector<std::complex<double>> &mat) {
  if (m == 2) {
    return reference_zgeinv_2x2(m, mat);
  }
  std::vector<std::complex<double>> inv_m(m * m);
  // Compute each block separately using reference matrix inversion (recursive
  // process)
  unsigned mm = m / 2;
  std::vector<std::complex<double>> a(mat.begin() + 0 * mm * mm,
                                      mat.begin() + 1 * mm * mm);
  std::vector<std::complex<double>> b(mat.begin() + 1 * mm * mm,
                                      mat.begin() + 2 * mm * mm);
  std::vector<std::complex<double>> c(mat.begin() + 2 * mm * mm,
                                      mat.begin() + 3 * mm * mm);
  std::vector<std::complex<double>> d(mat.begin() + 3 * mm * mm,
                                      mat.begin() + 4 * mm * mm);

  // Inverse of A and D
  auto inv_a = reference_zgeinv(mm, a);
  auto inv_d = reference_zgeinv(mm, d);

  // M00^{-1} = X^{-1} = (A - BD^{-1}C)^{-1}
  std::vector<std::complex<double>> y(mm * mm);
  auto x = a;
  reference_zgemm_zorder(mm, 1.0, inv_d, c, 0.0, y);
  reference_zgemm_zorder(mm, -1.0, b, y, 1.0, x);
  auto inv_m00 = reference_zgeinv(mm, x);

  // M10^{-1} = -D^{-1}C X^{-1}
  std::vector<std::complex<double>> inv_m10(mm * mm);
  reference_zgemm_zorder(mm, -1.0, y, inv_m00, 0.0, inv_m10);

  // M11^{-1} = U^{-1} = (D - CA^{-1}B)^{-1}
  std::vector<std::complex<double>> v(mm * mm);
  auto u = d;
  reference_zgemm_zorder(mm, 1.0, inv_a, b, 0.0, v);
  reference_zgemm_zorder(mm, -1.0, c, v, 1.0, u);
  auto inv_m11 = reference_zgeinv(mm, u);

  // M01 = -A^{-1}B U^{-1}
  std::vector<std::complex<double>> inv_m01(mm * mm);
  reference_zgemm_zorder(mm, -1.0, v, inv_m11, 0.0, inv_m01);

  // Set inverse matrix block per block
  inv_m.clear();
  inv_m.insert(inv_m.end(), inv_m00.begin(), inv_m00.end());
  inv_m.insert(inv_m.end(), inv_m01.begin(), inv_m01.end());
  inv_m.insert(inv_m.end(), inv_m10.begin(), inv_m10.end());
  inv_m.insert(inv_m.end(), inv_m11.begin(), inv_m11.end());

  return inv_m;
}

inline std::vector<std::complex<double>>
reference_zgeinv_small(uint32_t m,
                       const std::vector<std::complex<double>> &mat) {
  if (m == 2) {
    return reference_zgeinv_2x2(m, mat);
  }
  if (m == 3) {
    return reference_zgeinv_3x3(m, mat);
  }
  // GCOVR_EXCL_START
  assert(false && "Small matrix inverse only defined for m = 2 or m = 3");
  // GCOVR_EXCL_STOP
  return {};
}

/*
 * Converting between armral_cmplx_f32_t and std::complex<double/float>
 */

template<typename T>
std::complex<T> complex_convert(armral_cmplx_f32_t cmplx) {
  return std::complex<T>(static_cast<T>(cmplx.re), static_cast<T>(cmplx.im));
}

template<typename T>
armral_cmplx_f32_t complex_convert(std::complex<T> cmplx) {
  return armral_cmplx_f32_t{static_cast<float32_t>(cmplx.real()),
                            static_cast<float32_t>(cmplx.imag())};
}

/*
 * Converting between armral_cmplx_f32_t* and std::complex<double/float>
 */
template<typename T>
std::vector<std::complex<T>>
convert_cf32_array_to_vector(uint32_t nvalues, const armral_cmplx_f32_t *a) {
  std::vector<std::complex<T>> out(nvalues);
  for (unsigned i = 0; i < nvalues; ++i) {
    out[i] = std::complex<T>(a[i].re, a[i].im);
  }
  return out;
}

template<typename T>
void convert_vector_to_cf32_array(uint32_t nvalues,
                                  const std::vector<std::complex<T>> &a,
                                  armral_cmplx_f32_t *b) {
  for (unsigned i = 0; i < nvalues; ++i) {
    b[i] = {(float32_t)a[i].real(), (float32_t)a[i].imag()};
  }
}

/*
 * Reference matrix multiplication (C=A*B) on cs16 input matrices
 */
inline void reference_matmul_cs16(uint16_t m, uint16_t n, uint16_t k,
                                  const armral_cmplx_int16_t *a,
                                  const armral_cmplx_int16_t *b,
                                  armral_cmplx_int16_t *c, int round) {
  assert(round == 0 || round == 1);
  for (unsigned i = 0; i < m; ++i) {
    for (unsigned nn = 0; nn < n; ++nn) {
      std::complex<qint64_t> acc;
      for (unsigned j = 0; j < k; ++j) {
        auto ae = a[i * k + j];
        auto be = b[j * n + nn];
        auto intermed = armral::utils::cmplx_mul_widen_cs16(ae, be);
        acc += intermed;
      }
      // round works by adding one to the intermediate result, to ensure we get
      // exact rounding if required (e.g. such that 0b011.1 rounds to 0b100.0).
      c[i * n + nn].re = (((acc.real() >> 14) + round) >> 1).get16();
      c[i * n + nn].im = (((acc.imag() >> 14) + round) >> 1).get16();
    }
  }
}

/*
 * Reference matrix multiplication (C=A*B) on cf32 input matrices
 */
inline void reference_matmul_cf32(uint16_t m, uint16_t n, uint16_t k,
                                  const armral_cmplx_f32_t *a,
                                  const armral_cmplx_f32_t *b,
                                  armral_cmplx_f32_t *c) {
  // Convert float to double
  auto a64 = convert_cf32_array_to_vector<double>(m * k, a);
  auto b64 = convert_cf32_array_to_vector<double>(k * n, b);
  auto c64 = convert_cf32_array_to_vector<double>(m * n, c);

  // Double precision matrix multiply
  reference_zgemm(m, n, k, 1.0, a64, b64, 0.0, c64);

  // Convert back to float
  for (unsigned i = 0; i < m; ++i) {
    for (unsigned nn = 0; nn < n; ++nn) {
      c[i * n + nn].re = c64[i * n + nn].real();
      c[i * n + nn].im = c64[i * n + nn].imag();
    }
  }
}

/*
 * Reference conjugate transpose matrix multiplication (C=B * A^H) on cf32 input
 * matrices
 */
inline void reference_matmul_bah_cf32(
    uint16_t m, uint16_t n, const armral_cmplx_f32_t *__restrict p_src_a,
    const armral_cmplx_f32_t *__restrict p_src_b, armral_cmplx_f32_t *p_dst) {
  for (uint16_t i = 0; i < n; i++) {
    for (uint16_t j = 0; j < m; j++) {
      std::complex<double> dot = 0.;
      for (uint16_t k = 0; k < n; k++) {
        auto ah_jk = complex_convert<double>(p_src_a[j * n + k]);
        auto b_ik = complex_convert<double>(p_src_b[i * n + k]);
        dot += b_ik * std::conj(ah_jk);
      }
      p_dst[i * m + j] = complex_convert(dot);
    }
  }
}

/*
 * Reference conjugate transpose matrix multiplication (C=A^H * B) on cf32 input
 * matrices
 */
inline void
reference_matmul_ahb_cf32(uint16_t m, uint16_t n, uint16_t k,
                          const armral_cmplx_f32_t *__restrict p_src_a,
                          const armral_cmplx_f32_t *__restrict p_src_b,
                          armral_cmplx_f32_t *p_dst) {

  // For every row of output C (column of A, row of A^H)...
  for (uint32_t j = 0; j < m; j++) {

    // For every column of output C (column of B)...
    for (uint32_t i = 0; i < n; i++) {

      // Every row of A and B (where row of A = column of A^H)
      std::complex<double> dot = 0.;
      for (uint32_t r = 0; r < k; r++) {
        auto a_jr = complex_convert<double>(p_src_a[r * m + j]);
        auto b_ir = complex_convert<double>(p_src_b[r * n + i]);
        dot += std::conj(a_jr) * b_ir;
      }

      p_dst[n * j + i] = complex_convert(dot);
    }
  }
}

/*
 * Reference matrix multiplication (C=A*A^H) on a cf32 input matrix
 */
inline void
reference_matmul_aah_cf32(uint16_t m, uint16_t n,
                          const armral_cmplx_f32_t *__restrict p_src_a,
                          armral_cmplx_f32_t *p_dst_c) {
  // For every row of A/row of C...
  for (uint32_t i = 0; i < m; i++) {

    // For every column of C...
    for (uint32_t j = 0; j < m; j++) {
      std::complex<double> dot = 0.;

      // For every column of A/row of A^H...
      for (uint32_t k = 0; k < n; k++) {
        uint32_t a_idx = (i * n + k);
        uint32_t ah_idx = (j * n + k);

        dot += complex_convert<double>(p_src_a[a_idx]) *
               std::conj(complex_convert<double>(p_src_a[ah_idx]));
      }
      if (i == j) {
        dot.imag(0.);
      }

      uint32_t c_idx = i * m + j;
      p_dst_c[c_idx] = complex_convert(dot);
    }
  }
}

/*
 * Reference matrix multiplication (C=A^H*A) on a cf32 input matrix
 */
inline void
reference_matmul_aha_cf32(uint16_t m, uint16_t n,
                          const armral_cmplx_f32_t *__restrict p_src,
                          armral_cmplx_f32_t *p_dst) {
  for (uint16_t i = 0; i < n; i++) {
    for (uint16_t j = 0; j < n; j++) {
      std::complex<double> dot = 0.;
      for (uint16_t k = 0; k < m; k++) {
        uint32_t ah_idx = k * n + i;
        uint32_t a_idx = k * n + j;
        dot += std::conj(complex_convert<double>(p_src[ah_idx])) *
               complex_convert<double>(p_src[a_idx]);
      }
      if (i == j) {
        dot.imag(0.);
      }
      p_dst[i * n + j] = complex_convert(dot);
    }
  }
}

/*
 * Run reference Matrix Inversion based on blockwise approach.
 */
inline void reference_matinv_block(uint32_t m, const armral_cmplx_f32_t *a,
                                   armral_cmplx_f32_t *b) {

  // Init double precision input matrix (use z-order for easy access to blocks)
  auto a_tmp = convert_cf32_array_to_vector<double>(m * m, a);

  // Bypass z-ordering for small cases
  if (m == 2 || m == 3) {
    auto b_tmp = reference_zgeinv_small(m, a_tmp);
    convert_vector_to_cf32_array(m * m, b_tmp, b);
  } else {
    auto a64 = rowmajor_to_zorder(m, a_tmp);

    // Evaluate double precision inverse
    auto b64 = reference_zgeinv(m, a64);

    // Round back to single precision
    auto b_tmp = zorder_to_rowmajor(m, b64);
    convert_vector_to_cf32_array(m * m, b_tmp, b);
  }
}

} // namespace armral::utils
