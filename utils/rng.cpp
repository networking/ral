/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#include <arm_neon.h>

#include "rng.hpp"

namespace armral::utils {

static inline uint64_t lcg_step(uint64_t x) {
  x = (x * 1103515245 + 12345) & 0x7fffffffU;
  return x;
}

template<>
void linear_congruential_generator::one<void>(random_state *state) {
  state->seed = lcg_step(state->seed);
}

template<>
bool linear_congruential_generator::one<bool>(random_state *state) {
  auto x = lcg_step(state->seed);
  bool ret = x < state->seed;
  state->seed = x;
  return ret;
}

template<>
uint32_t linear_congruential_generator::one<uint32_t>(random_state *state) {
  auto x = lcg_step(state->seed);
  state->seed = x;
  return x;
}

template<>
float32_t linear_congruential_generator::one<float32_t>(random_state *state) {
  auto x = lcg_step(state->seed);
  state->seed = x;
  return (float32_t)x / 0x80000000U;
}

template<>
double linear_congruential_generator::one<double>(random_state *state) {
  auto x = lcg_step(state->seed);
  state->seed = x;
  return (double)x / 0x80000000U;
}

void linear_congruential_generator::advance_state(random_state *state,
                                                  uint32_t n) {
  for (uint32_t i = 0; i < n; ++i) {
    one<void>(state);
  }
}

random_state
random_state::from_seeds(const std::initializer_list<uint64_t> seeds) {
  random_state state{0};
  linear_congruential_generator lcg;
  for (const auto seed : seeds) {
    // fold next element of args into seed, then step to mix bits.
    state.seed ^= seed;
    lcg.advance_state(&state, 1);
  }
  // a few more steps to finish mixing.
  lcg.advance_state(&state, 3);
  return state;
}

} // namespace armral::utils
