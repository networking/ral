/*
    Arm RAN Acceleration Library
    SPDX-FileCopyrightText: <text>Copyright 2020-2025 Arm Limited and/or its
    affiliates <open-source-office@arm.com></text>
    SPDX-License-Identifier: BSD-3-Clause
*/
#pragma once

#include "rng.hpp"

#include <cassert>
#include <complex>
#include <random>
#include <vector>

namespace armral::utils {

/*
 * A helper class for generating random integers. This class preserves
 * the random state between calls, so if repeatedly requesting vectors of the
 * same length a uniquely random vector is returned each time. Note this is not
 * the case for the allocate_random_X() helper functions.
 *
 * Example usage:
 *
 *   int_random<int16_t> random;
 *   auto a = random.vector(m * n); // returns std::vector<int16_t>
 *   auto b = random.vector(m * k);
 *   int16_t foo = random.one()
 *
 * You can also specify a min/max if necessary:
 *
 *   random<int16_t> random;
 *   auto c = random.vector(n * k, 1, 10});
 *   auto bar = random.one(-10, 10);
 *
 *   Note: the default is std::numeric_limits<T>::min() to
 *   std::numeric_limits<T>::max().
 *
 * To use custom seeds just pass them to the constructor:
 *
 *  int_random<int16_t> random({ 1337, 42 });
 *
 * Note: You must use the same int_random instance between calls to reuse the
 * random state.
 */
template<typename T>
class int_random : public base_random<T, T> {
public:
  int_random(std::initializer_list<uint64_t> seeds = {42})
    : base_random<T, T>(seeds) {}

  static constexpr T default_min{std::numeric_limits<T>::min()};
  static constexpr T default_max{std::numeric_limits<T>::max()};

  T one(T min = default_min, T max = default_max) {
    return base_random<T, T>::rand(min, max);
  }

  std::vector<T> vector(size_t len, T min = default_min, T max = default_max) {
    return base_random<T, T>::vector_impl(len, min, max);
  }
};

inline std::vector<uint8_t> allocate_random_u8(uint32_t len, uint8_t min,
                                               uint8_t max) {
  return int_random<uint8_t>().vector(len, min, max);
}

inline std::vector<uint8_t> allocate_random_u8(uint32_t len) {
  return allocate_random_u8(len, 0, UINT8_MAX);
}

inline std::vector<int8_t> allocate_random_i8(uint32_t len) {
  return int_random<int8_t>().vector(len, INT8_MIN, INT8_MAX);
}

inline std::vector<int16_t> allocate_random_i16(uint32_t len, int16_t min,
                                                int16_t max) {
  return int_random<int16_t>().vector(len, min, max);
}

inline std::vector<int16_t> allocate_random_i16(uint32_t len) {
  return allocate_random_i16(len, INT16_MIN, INT16_MAX);
}

/*
 * A helper class for generating random bit vectors. It differs from
 * the above int_random<uint8_t> in that if the number of bits requested
 * does not align with a byte boundary it only sets (number of bits % 8)
 * bits of the output in the final byte.
 *
 * We inherit privately so that we do not expose the vector() methods
 * from the base classes.
 */
class bit_random : private int_random<uint8_t> {
public:
  bit_random(std::initializer_list<uint64_t> seeds = {42})
    : int_random<uint8_t>(seeds) {}

  std::vector<uint8_t> bit_vector(uint32_t nbits) {
    auto ret = this->vector((nbits + 7) / 8);
    // if nbits is not a byte boundary, ensure only (nbits % 8) bits are set.
    if (nbits % 8 != 0) {
      ret[nbits / 8] &= 0xff00U >> (nbits % 8);
    }
    return ret;
  }
};

inline std::vector<uint8_t> allocate_random_bits(uint32_t nbits) {
  return bit_random().bit_vector(nbits);
}

/*
 * Check an array of results against an array of expected values.
 *
 * name:     The name of the function, used when printing errors.
 * result:   A pointer to an array of results, length n.
 * expected: A pointer to an array of expected values, length n.
 * n:        The length of the two arrays, in elements.
 *
 * Returns true if the elements match elementwise.
 */
template<typename T>
inline bool check_results(const char *name, const T *result, const T *expected,
                          uint32_t n) {
  bool passed = true;

  for (uint32_t i = 0; i < n; ++i) {
    if (result[i] != expected[i]) {
      // GCOVR_EXCL_START
      passed = false;
      printf("Error! [%s] result[%u]= %d (0x%x) and expected[%u]= %d (0x%x)\n",
             name, i, result[i], result[i], i, expected[i], expected[i]);
      // GCOVR_EXCL_STOP
    }
  }

  if (passed) {
    printf("[%s] - check result: OK\n", name);
  } else {
    printf("[%s] - check result: ERROR\n", name); // GCOVR_EXCL_LINE
  }
  return passed;
}

constexpr inline auto check_results_u8 = check_results<uint8_t>;
constexpr inline auto check_results_i8 = check_results<int8_t>;

} // namespace armral::utils
