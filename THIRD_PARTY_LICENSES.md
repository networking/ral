This file lists the package level copyright and license information for third
party software included in this release of 'Arm RAN Acceleration Library'.

The information is grouped into two sections. The first section lists out
details of third party software projects, including names of the applicable
licenses as per the SPDX format (<http://spdx.org/licenses>). The second section
includes the full license text of all applicable licenses referenced in the
first section.

### SECTION 1: THIRD PARTY SOFTWARE PROJECTS

This package does not depend on third party software.

---

### SECTION 2: APPLICABLE LICENSES

No applicable licenses.

---
END OF FILE: THIRD_PARTY_LICENSES.md
