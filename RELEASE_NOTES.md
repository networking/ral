# Arm RAN Acceleration Library 25.01 Release Notes

Non-Confidential
Copyright © 2020-2025 Arm Limited (or its affiliates). All rights reserved.

Arm conventions and proprietary notices, including confidentiality status,
terminology statement, and product release status, can be found at the end of
this document.

## Contents

These Release Notes contain the following sections:

- Release overview
- Release contents
- Support
- Release history
- Conventions
- Proprietary notices

## Release overview

This section describes the product to which these release notes relate and
provides information about its license.

### Product description

The Arm RAN Acceleration Library (ArmRAL) contains a set of functions for
accelerating telecommunications applications such as, but not limited to, 5G
Radio Access Networks (RANs). These functions are optimized for Arm AArch64-based
processors.

ArmRAL provides:

- Vector functions
- Matrix functions
- Lower physical layer (Lower PHY) support functions
- Upper physical layer (Upper PHY) support functions
- Distributed Unit-Radio Unit (DU-RU) Interface support functions

ArmRAL includes functions that operate on 16-bit signed integers and 16-bit and
32-bit floating-point values.

### Release status

This is the 25.01 release of ArmRAL.

### Licensing information

Use of ArmRAL is subject to a BSD-3-Clause license. See the `LICENSE.md` file
in your product installation for the license text. We will receive inbound
contributions under the same license.

If you require a different license than BSD-3-Clause for compatibility with your
end product, please get in contact via <open-source-office@arm.com> including
"[ArmRAL]" in the email subject line.

## Release contents

ArmRAL releases contain documentation and source files.

This section describes:

- Cloning the product's git repository from Arm's GitLab
- The contents of this release
- The changes since the previous release
- Any known issues and limitations that exist at the time of this release

### Cloning the source repository

ArmRAL is available on
[Arm's GitLab website](https://gitlab.arm.com/networking/ral).

**To access this release, clone the following repository using HTTPS:**

    git clone -b armral-25.01 https://git.gitlab.arm.com/networking/ral

### Deliverables

The downloaded product includes the following deliverables:

- ArmRAL 25.01
- Release Notes (this document)
- Documentation

Product documentation is available on the
[Arm Developer website](https://developer.arm.com/documentation/102249/2501).

**Note:** Documentation, errata and release notes might change between product
releases. For the latest documentation bundle, check the product download
page.

### Differences from previous release

The following sections describe differences from the previous release of
ArmRAL.

#### Additions and functionality changes

This section describes new features or any technical changes to features or
components in this release.

- The functions `armral_turbo_decode_batch`, and
  `armral_turbo_decode_batch_noalloc` have been added. These functions implement
  a maximum a posteriori (MAP) algorithm to decode the output of the LTE Turbo
  encoding scheme on a batch of encoded data.

- The function `armral_turbo_decode_batch_noalloc_buffer_size` has been added,
  which returns the size of buffer required for
  `armral_turbo_decode_batch_noalloc`.

- FFT lengths up to 42012 are now supported, although lengths greater
  than 4096 are mostly untested.

- Unused FFT kernels have been removed.

#### Performance improvements

This section describes any features or components with improved performance.

- Neon and SVE performance improvements for the following routines:

   - `armral_fft_execute_cf32` and `armral_fft_execute_cs16`.

#### Changes to simulation programs

This section describes any changes, new features or components added to the
channel simulation programs in this release.

- The LTE Turbo coding Additive White Gaussian Noise (AWGN) simulation now
  supports the decoding of batches of data, using `armral_turbo_decode_batch`.
  The number of batches is specified using the flag "`-b <n>`".

#### Resolved issues

This section describes any known issues resolved in the current release.

- Improved error correction of LDPC decoding (`armral_ldpc_decode_block`) in
  the presence of channel noise. The function now uses 16-bit signed integers
  internally rather than 8-bit signed integers. This may result in decreased
  performance.

- The arguments to the function `armral_turbo_decode_block_noalloc_buffer_size`
  have been changed to remove the unused second argument, `max_iter`.

- When planning FFTs with an unsupported length, `armral_fft_create_plan_cf32`
  and `armral_fft_create_plan_cs16` now return `ARMRAL_ARGUMENT_ERROR`.

### Known limitations

This section describes any known limitations of the current release.

- There are no known limitations in this release.

## Support

If you have any issues with the installation, content, or use of this release,
raise an issue on [Arm's GitLab website](https://gitlab.arm.com/networking/ral/-/issues).
Arm will respond as soon as possible.

### Tools

To build or run ArmRAL you will need:

- A C/C++ compiler, such as GCC. ArmRAL has been tested with GCC 7.5.0, 8.5.0,
  9.5.0, 10.5.0, 11.5.0, 12.4.0, 13.3.0, and 14.2.0.

  **Note:** If you are cross-compiling, you need a cross-toolchain compiler that
  targets AArch64. You can download open-source cross-toolchain builds of the
  GCC compiler on the [Arm Developer website](https://developer.arm.com/tools-and-software/open-source-software/developer-tools/gnu-toolchain/gnu-a/downloads).
  The variant to use for an AArch64 GNU/Linux target is `aarch64-none-linux-gnu`.

- CMake version 3.3.0 or higher.

Additionally:

- To run the benchmarks, you must have the Linux utility tool `perf` installed
  and a recent version of Python 3. ArmRAL has been tested with Python 3.11.11.

- To build a local version of the documentation, you must have Doxygen
  installed. ArmRAL has been tested with Doxygen version 1.8.13.

- To generate code coverage HTML pages, you must have `gcovr` installed. The
  library has been tested with `gcovr` version 4.2.

**Note:** ArmRAL runs on AArch64 cores, however to use the convolutional
encoder, CRC, and sequence generator functions you must run on a core that
supports the AArch64 PMULL extension. If the system you are using supports the
PMULL extension, `pmull` will be included in the "Features" list in the
`/proc/cpuinfo` file.

## Release history

ArmRAL's release history is available on the [Arm Developer website](https://developer.arm.com/downloads/-/arm-ran-acceleration-library/previous-releases-of-the-arm-ran-acceleration-library).

## Conventions

The following sections describe conventions used in Arm documents.

### Glossary

The Arm Glossary is a list of terms that are used in Arm documentation, together
with definitions for those terms. The Arm Glossary does not contain terms that
are industry standard unless the Arm meaning differs from the generally accepted
meaning.

See the [Arm Glossary](https://developer.arm.com/glossary) for more information.

## Non-Confidential Proprietary Notice

This document is protected by copyright and other related rights and the
practice or implementation of the information contained in this document may be
protected by one or more patents or pending patent applications. No part of this
document may be reproduced in any form by any means without the express prior
written permission of Arm. No license, express or implied, by estoppel or
otherwise to any intellectual property rights is granted by this document unless
specifically stated.

Your access to the information in this document is conditional upon your
acceptance that you will not use or permit others to use the information for the
purposes of determining whether implementations infringe any third party
patents.

THIS DOCUMENT IS PROVIDED “AS IS”. ARM PROVIDES NO REPRESENTATIONS AND NO
WARRANTIES, EXPRESS, IMPLIED OR STATUTORY, INCLUDING, WITHOUT LIMITATION, THE
IMPLIED WARRANTIES OF MERCHANTABILITY, SATISFACTORY QUALITY, NON-INFRINGEMENT OR
FITNESS FOR A PARTICULAR PURPOSE WITH RESPECT TO THE DOCUMENT. For the avoidance
of doubt, Arm makes no representation with respect to, has undertaken no
analysis to identify or understand the scope and content of, patents,
copyrights, trade secrets, or other rights.

This document may include technical inaccuracies or typographical errors.

TO THE EXTENT NOT PROHIBITED BY LAW, IN NO EVENT WILL ARM BE LIABLE FOR ANY
DAMAGES, INCLUDING WITHOUT LIMITATION ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL,
PUNITIVE, OR CONSEQUENTIAL DAMAGES, HOWEVER CAUSED AND REGARDLESS OF THE THEORY
OF LIABILITY, ARISING OUT OF ANY USE OF THIS DOCUMENT, EVEN IF ARM HAS BEEN
ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.

This document consists solely of commercial items. You shall be responsible for
ensuring that any use, duplication or disclosure of this document complies fully
with any relevant export laws and regulations to assure that this document or
any portion thereof is not exported, directly or indirectly, in violation of
such export laws. Use of the word “partner” in reference to Arm's customers is
not intended to create or refer to any partnership relationship with any other
company. Arm may make changes to this document at any time and without notice.

This document may be translated into other languages for convenience, and you
agree that if there is any conflict between the English version of this document
and any translation, the terms of the English version of the Agreement shall
prevail.

The Arm corporate logo and words marked with ® or ™ are registered trademarks or
trademarks of Arm Limited (or its affiliates) in the US and/or elsewhere. All
rights reserved. Other brands and names mentioned in this document may be the
trademarks of their respective owners. Please follow Arm’s trademark usage
guidelines at <https://www.arm.com/company/policies/trademarks>.

Copyright © 2020-2025 Arm Limited (or its affiliates). All rights reserved.

Arm Limited. Company 02557590 registered in England.
110 Fulbourn Road, Cambridge, England CB1 9NJ.
(LES-PRE-20349)

### Confidentiality Status

This document is Non-Confidential. The right to use, copy and disclose this
document may be subject to license restrictions in accordance with the terms of
the agreement entered into by Arm and the party that Arm delivered this document
to.

Unrestricted Access is an Arm internal classification.

### Product Status

The information in this document is Final, that is for a developed product.

### Web Address

<https://developer.arm.com>

### Inclusive language commitment

Arm values inclusive communities. Arm recognizes that we and our industry have
used language that can be offensive. Arm strives to lead the industry and create
change.

To report offensive language in this document, email terms@arm.com.
