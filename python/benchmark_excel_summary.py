#!/usr/bin/env python3
# Arm RAN Acceleration Library
# SPDX-FileCopyrightText: <text>Copyright 2024-2025 Arm Limited and/or its
# affiliates <open-source-office@arm.com></text>
# SPDX-License-Identifier: BSD-3-Clause

import argparse
import json
import pandas as pd
import re
import sys


class UnpackedName:
    """
    A helper class for giving a nice sort order over benchmark names.
    This essentially just splits the string on underscores and converts what it
    can to integers for comparison, such that
    e.g. decompression_9b_2 < decompression_9b_10
    """

    def __init__(self, name):
        self.name = name
        self.arr = re.findall(r"[^_.]+", name)
        for i, elem in enumerate(self.arr):
            nums = list(map(int, re.findall(r"[0-9]+", elem)))
            self.arr[i] = (nums, elem)

    def __lt__(self, other):
        """
        Lexographic comparison, with type-mismatches handled arbitrarily
        """
        for x, y in zip(self.arr, other.arr):
            assert type(x) is type(y)
            if x < y:
                return True
            if x > y:
                return False
        return len(self.arr) < len(other.arr)

    def __eq__(self, other):
        return self.name == other.name

    def __hash__(self):
        return self.name.__hash__()

    def __len__(self):
        return len(self.name)

    def __str__(self):
        return self.name


def format_headers_set_widths(worksheet, workbook, df):
    header_format = workbook.add_format({
        'bold': True,
        'align': 'center',
        'border': 1})

    # Write headers
    for col_num, header in enumerate(df.columns.values):
        worksheet.write(0, col_num, header, header_format)

    # Set width for benchmark name column
    worksheet.set_column(0, 0, max(len(x) for x in df.name))

    # Set width for data columns
    max_width = max(len(x) for x in df.columns.values)
    worksheet.set_column(1, 1, max_width + 2)


def write_worksheet(writer, workbook, df):
    name = "results"

    # Write the table to an Excel worksheet, leaving room to add headers
    df.to_excel(
        writer, sheet_name=name, startrow=1,
        float_format="%.2f", header=False, index=False)

    # Write headers and set column widths
    worksheet = writer.sheets[name]
    format_headers_set_widths(worksheet, workbook, df)


def sort_table(df):
    # Sort by the benchmark names
    df = df.set_index(df.name)
    new_ind = sorted(df.index.values, key=lambda x: UnpackedName(x))
    return df.reindex(new_ind)


def get_json_results(src):
    json_data = []
    with open(src, 'rb') as f:
        for line in f:
            json_data.append(json.loads(line))

    # Create dataframe of results
    df = pd.json_normalize(json_data)

    # Drop columns that won't be used
    drop_cols = df.columns.difference(['name', 'median_cycles'])
    return df.drop(drop_cols, axis=1)


def write_workbook(src, dst):
    # Get dataframes for data and sort by benchmark name
    df_results = get_json_results(src)
    df_results = sort_table(df_results)

    # Create a workbook and add worksheets
    writer = pd.ExcelWriter(dst, engine="xlsxwriter")
    workbook = writer.book
    write_worksheet(writer, workbook, df_results)

    # Write the file
    writer.close()


def main():
    parser = argparse.ArgumentParser()
    parser.add_argument("-o", "--output", metavar="path",
                        default="results.xlsx",
                        help="specify output workbook path")
    parser.add_argument("json_file", help="JSON benchmark results file")
    args = parser.parse_args()

    print("Writing Excel workbook to {}".format(args.output))
    write_workbook(args.json_file, args.output)

    return 0


if __name__ == '__main__':
    sys.exit(main())
